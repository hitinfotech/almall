<?php

header('Content-Type: application/json; charset=utf-8');

///// Version
define('VERSION', '2.1.0.1');
define('ENVIROMENT', getenv('ENVIROMENT'));

// Configuration
require_once('config/config.php');
$conf_file = 'config/config_' . ENVIROMENT . '.php';
if (is_file($conf_file)) {
    require_once($conf_file);
}

//auth api
require_once DIR_SYSTEM . 'api_auth.php';

// Startup
require_once(DIR_SYSTEM . 'startup.php');

// Registry
$registry = Registry::getInstance();

// Loader
$loader = Loader::getInstance($registry);
$registry->set('load', $loader);

// Config
$config = Config::getInstance();
$registry->set('config', $config);

// Database
$db = DB::getInstance(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
$registry->set('db', $db);

/*
  // Store
  if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
  $store_query = $db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`ssl`, 'www.', '') = '" . $db->escape('https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
  } else {
  $store_query = $db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`url`, 'www.', '') = '" . $db->escape('http://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
  }

  if ($store_query->num_rows) {
  $config->set('config_store_id', $store_query->row['store_id']);
  } else {
 * 
 */
$config->set('config_store_id', 0);
/* }
 * 
 */

// Settings
$query = $db->query("SELECT * FROM `" . DB_PREFIX . "setting` WHERE store_id = '0' OR store_id = '" . (int) $config->get('config_store_id') . "' ORDER BY store_id ASC");

foreach ($query->rows as $result) {
    if (!$result['serialized']) {
        $config->set($result['key'], $result['value']);
    } else {
        $config->set($result['key'], json_decode($result['value'], true));
    }
}

$config->set('config_url', HTTP_SERVER);
$config->set('config_ssl', HTTPS_SERVER);

// Url
$url = new Url($config->get('config_url'), $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url'));
$registry->set('url', $url);

// Log
$log = new Log($config->get('config_error_filename'));
$registry->set('log', $log);

function error_handler($code, $message, $file, $line) {
    global $log, $config;

    // error suppressed with @
    if (error_reporting() === 0) {
        return false;
    }

    switch ($code) {
        case E_NOTICE:
        case E_USER_NOTICE:
            $error = 'Notice';
            break;
        case E_WARNING:
        case E_USER_WARNING:
            $error = 'Warning';
            break;
        case E_ERROR:
        case E_USER_ERROR:
            $error = 'Fatal Error';
            break;
        default:
            $error = 'Unknown';
            break;
    }

    if ($config->get('config_error_display')) {
        echo '<b>' . $error . '</b>: ' . $message . ' in <b>' . $file . '</b> on line <b>' . $line . '</b>';
    }

    if ($config->get('config_error_log')) {
        $log->write('PHP ' . $error . ':  ' . $message . ' in ' . $file . ' on line ' . $line);
    }

    return true;
}

// Error Handler
set_error_handler('error_handler');

// Event
$event = new Event($registry);
$registry->set('event', $event);
$query = $db->query("SELECT * FROM " . DB_PREFIX . "event");

foreach ($query->rows as $result) {
    $event->register($result['trigger'], $result['action']);
}

// Facebook
$registry->set('facebook', new FacebookLibrary());

// Request
$request = new Request();
$registry->set('request', $request);


// Response
$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$response->setCompression($config->get('config_compression'));
$registry->set('response', $response);

// Cache
$cache = Cache::getInstance(CONFIG_CACHE);
$registry->set('cache', $cache);

$lng = (isset($request->server['HTTP_LANG']) ? $request->server['HTTP_LANG'] : "2");
if (!in_array($lng, array(1, 2)) || $lng == 2) {
    $language_code = 'ar';
} else {
    $language_code = 'en';
}


// Language Detection
$languages = array();
$query = $db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE status = '1'");
foreach ($query->rows as $result) {
    $languages[$result['code']] = $result;
}

$detect = isset($request->post['LANG']) ? $request->post['LANG'] : 'ar';

$language_code = in_array($detect, array('en', 'ar')) ? $detect : $config->get('config_language');


$config->set('config_language_id', $languages[$language_code]['language_id']);
$config->set('config_language', $languages[$language_code]['code']);

// Language
$language = new Language($languages[$language_code]['directory']);
$language->load($languages[$language_code]['directory']);
$registry->set('language', $language);

// Customer
$customer = new Customer($registry);
$registry->set('customer', $customer);

// Currency
$registry->set('currency', new Currency($registry));

// Tax
$registry->set('tax', new Tax($registry));

// Cart
$registry->set('cart', new Cart($registry));

// Country
$country = new Country($registry);
$country_code = (isset($request->post['COUNTRY']) ? $request->post['COUNTRY'] : "sa");

if (in_array($country_code, array('ae', 'sa')) && $country_code == 'ae') {
    $country_code = 'ae';
} else {
    $country_code = 'sa';
}
$country->set($country_code);
$registry->set('country', $country);
$registry->get('currency')->set($registry->get('country')->getCurcode());

// Front Controller
$controller = new Front($registry);

// SEO URL's
$controller->addPreAction(new Action('common/seo_url'));

// Router
if (isset($request->get['route'])) {
    $action = new Action($request->get['route']);
} else {
    $action = new Action('error/not_found');
}

// Dispatch
$controller->dispatch($action, new Action('error/not_found'));

// Output
$response->output();
