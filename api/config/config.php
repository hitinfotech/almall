<?php

// DIR
define('DIR_ROOT', __DIR__ . '/../../');
define('DIR_APPLICATION', DIR_ROOT . 'api/');
define('DIR_DB_API_PATH', DIR_ROOT.'catalog/');
define('DIR_SYSTEM', DIR_ROOT . 'system/');
define('DIR_LANGUAGE', DIR_ROOT . 'catalog/language/');
define('DIR_TEMPLATE', DIR_ROOT . 'api/view/theme/');
define('DIR_CONFIG', DIR_ROOT . 'system/config/');
define('DIR_CACHE', DIR_ROOT . 'system/storage/cache/');
define('DIR_DOWNLOAD', DIR_ROOT . 'system/storage/download/');
define('DIR_LOGS', DIR_ROOT . 'system/storage/logs/');
define('DIR_MODIFICATION', DIR_ROOT . 'system/storage/modification/');
define('DIR_UPLOAD', DIR_ROOT . 'system/storage/upload/');

define('EMAIL_COOKIE_NAME', '5crplp7oqlbjjl0clpa3iflal0');
define('COOKIES_PERIOD', (time() + 60 * 60 * 24 * 365));

// HOSTING
if (file_exists(DIR_ROOT . 'config/config_' . ENVIROMENT . '.php')) {
    require_once(DIR_ROOT . 'config/config_' . ENVIROMENT . '.php');
}

// REDIS
if (file_exists(DIR_ROOT . 'config/config_redis_' . ENVIROMENT . '.php')) {
    require_once(DIR_ROOT . 'config/config_redis_' . ENVIROMENT . '.php');
}

// ALGOLIAS
if (file_exists(DIR_ROOT . 'config/config_algolia.php')) {
    require_once DIR_ROOT . 'config/config_algolia.php';
}

// SHIPPING COMPANIES
if (file_exists(DIR_ROOT . 'config/config_shipping.php')) {
    require_once DIR_ROOT . 'config/config_shipping.php';
}

// IMAGE SIZES
if (file_exists(DIR_ROOT . 'config/config_image.php')) {
    require_once DIR_ROOT . 'config/config_image.php';
}

// CACHING TYPE
if (file_exists(DIR_ROOT . 'config/config_cache.php')) {
    require_once DIR_ROOT . 'config/config_cache.php';
}

// FACEBOOK CON
if (file_exists(DIR_ROOT . 'config/config_facebook.php')) {
    require_once DIR_ROOT . 'config/config_facebook.php';
}

// AWS CREDINTIALS
if (file_exists(DIR_ROOT . 'config/config_aws.php')) {
    require_once DIR_ROOT . 'config/config_aws.php';
}

// MAIL GUN CREDINTIALS
if (file_exists(DIR_ROOT . 'config/config_mailgun.php')) {
    require_once DIR_ROOT . 'config/config_mailgun.php';
}

// MAIL TEMPLATES
if (file_exists(DIR_ROOT . 'config/config_email_template.php')) {
    require_once DIR_ROOT . 'config/config_email_template.php';
}
