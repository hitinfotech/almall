<?php


class ControllerMallRequests extends Controller{

    public function getCountries(){

      $this->load->model("localisation/zone");
      $this->load->model("localisation/country");
      $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
      $language_id = 2;
      if ($language == 'en'){
        $language_id=1;
      }
      $this->config->set('config_language_id',$language_id);

      $countries = $this->model_localisation_country->getDeatailedCountries($language_id);
      echo json_encode($countries);
    }

    public function getNews(){

      $this->load->model('malls/news');
      $this->load->model('tool/image');

      $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

      $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
      $language_id = 2;
      if ($language == 'en'){
        $language_id=1;
      }
      $this->config->set('config_language_id',$language_id);


      $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
      $country_code = 'sa';
      if ($country == 'ae'){
        $country_code = 'ae';
      }
      $this->country->set($country_code);

      if (isset($this->request->get['filter_filter'])) {
          $filter_filter = $this->request->get['filter_filter'];
      } else {
          $filter_filter = NULL;
      }

      if (isset($this->request->get['filter_zone'])) {
          $filter_zone = $this->request->get['filter_zone'];
      } else {
          $filter_zone = NULL;
      }

      if (isset($this->request->get['filter_mall'])) {
          $filter_mall = $this->request->get['filter_mall'];
      } else {
          $filter_mall = NULL;
      }

      if (isset($this->request->get['filter_category'])) {
          $filter_category = $this->request->get['filter_category'];
      } else {
          $filter_category = NULL;
      }

      if (isset($this->request->get['sort'])) {
          $sort = $this->request->get['sort'];
      } else {
          $sort = 'n.start_date';
      }

      if (isset($this->request->get['order'])) {
          $order = $this->request->get['order'];
      } else {
          $order = 'ASC';
      }

      if (isset($this->request->get['page'])) {
          $page = $this->request->get['page'];
      } else {
          $page = 1;
      }

      if (isset($this->request->get['limit'])) {
          $limit = (int) $this->request->get['limit'];
      } else {
          $limit = 8;
      }

      $filter_data = array(
          'filter_filter' => $filter_filter,
          'filter_mall' => $filter_mall,
          'filter_zone' => $filter_zone,
          'filter_country' => $this->country->getid(),
          'filter_category' => $filter_category,
          'sort' => $sort,
          'order' => $order,
          'start' => ($page - 1) * $limit,
          'limit' => $limit
      );

      $results = $this->model_malls_news->getNewsList($filter_data);
      foreach($results as $id => $news){
        $news_array [] = array (
        'news_id' => $news['news_id'],
        'title' => $news['title'],
        'image' => $this->model_tool_image->resize($news['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght')),
      );
      }

      $result_array = array(
        'news_array'=>$news_array,
        'filters' => $filter_data
      );

      echo json_encode($result_array);

    }

    public function getNewsDetails(){

      $this->load->model('malls/news');
      $this->load->model('tool/image');

      if (isset($this->request->get['news_id'])) {
          $news_id = $this->request->get['news_id'];
      } else {
          $news_id = 0;
      }

      $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

      $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
      $language_id = 2;
      if ($language == 'en'){
        $language_id=1;
      }
      $this->config->set('config_language_id',$language_id);


      $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
      $country_code = 'sa';
      if ($country == 'ae'){
        $country_code = 'ae';
      }
      $this->country->set($country_code);

      $news_details = $this->model_malls_news->getNews($news_id);
      if(empty($news_details)){
        echo json_encode(array("Error" => "Page not found"));
        return;
      }
      $news_array = array (
        'news_id' => $news_details['news_id'],
        'title' => $news_details['title'],
        'description' => $news_details['body_text'],
        'date' => $news_details['date_added'],
        'image' => $this->model_tool_image->resize($news_details['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght'))
      );

      $relared_news = $this->model_malls_news->getRelatedNews($news_id,$news_details['start_date']);
      foreach ($relared_news as $key => $rel_news){
        $relared_news_array [] = array (
          'news_id' => $rel_news['news_id'],
          'title' => $rel_news['title'],
          'image' => $this->model_tool_image->resize($rel_news['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght'))
        );
      }

      $related_shops = $this->model_malls_news->getNewsShops($news_id);

      $return_array = array (
        'news_details' => $news_array,
        'related_news' => $relared_news_array,
        'related_shops' =>$related_shops
      );
      echo json_encode($return_array);


    }

    public function getMalls(){

      $this->load->model("malls/mall");
      $this->load->model('tool/image');

      $json = array();
      $filter_date = array();

      $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

      $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
      $language_id = 2;
      if ($language == 'en'){
        $language_id=1;
      }
      $this->config->set('config_language_id',$language_id);


      $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
      $country_code = 'sa';
      if ($country == 'ae'){
        $country_code = 'ae';
      }
      $this->country->set($country_code);
      $filter_country_id = $this->country->getId();

      if (isset($this->request->get['order_by'])) {
          $order = $this->request->get['order_by'];
      } else {
          $order = 'ASC';
      }

      if (isset($this->request->get['limit'])) {
          $limit = (int) $this->request->get['limit'];
      } else {
          $limit = 8;
      }

      if (isset($this->request->get['filter_filter'])) {
          $filter_filter = $this->request->get['filter_filter'];
      } else {
          $filter_filter = NULL;
      }

      if (isset($this->request->get['filter_zone_id'])) {
          $filter_zone_id = $this->request->get['filter_zone_id'];
      } else {
          $filter_zone_id = NULL;
      }

      $filter_data = array(
          'filter_zone_id' => $filter_zone_id,
          'filter_filter' => $filter_filter,
          'filter_country_id' => $filter_country_id,
          'order' => $order,
          'limit' => $limit
        );

      $requestMalls = $this->model_malls_mall->getMalls($filter_data);
      foreach ($requestMalls as $key => $mall){
          $return_malls[] = array (
            'mall_id' => $mall['mall_id'],
            'title' => $mall['name'],
            'image' => $this->model_tool_image->resize($mall['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght'))
          );

      }
      $return_array = array (
      'filter_array' => $filter_data,
      'filter_mall' => $return_malls
      );

      echo json_encode($return_array);
    }

    public function getMallDetails(){

      $this->load->model('malls/mall');
      $this->load->model('catalog/offer');
      $this->load->model('tool/image');

      $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

      $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
      $language_id = 2;
      if ($language == 'en'){
        $language_id=1;
      }
      $this->config->set('config_language_id',$language_id);


      $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
      $country_code = 'sa';
      if ($country == 'ae'){
        $country_code = 'ae';
      }
      $this->country->set($country_code);

      if (isset($this->request->get['mall_id'])) {
          $mall_id = $this->request->get['mall_id'];
      } else {
          echo json_encode(array("Error:" => "Please send a mall ID"));
          return;
      }


      $mall_data = $this->model_malls_mall->getMall($mall_id);
      if (empty($mall_data)){
        echo json_encode(array("Error:" => "mall ID was not found"));
        return;
      }
      $mall_categories = $this->model_malls_mall->getMallCategories($mall_id);
      $malls_total = $this->model_malls_mall->getTotalMalls(array('filter_mall_id'=>$mall_id));
      $latest_offers = $this->model_catalog_offer->getOffersByMall($mall_id,0,4);// 0 and 4 are the start and the limit of the number of offers

      foreach($latest_offers as $k => $offer){
        $offers_array [] = array (
          'offer_id' => $offer['offer_id'],
          'title' => $offer['title'],
          'image' => $this->model_tool_image->resize($offer['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght'))
        );
      }

      $return_array = array(
        'mall_id' => $mall_data['mall_id'],
        'title' => $mall_data['name'],
        'image_url' => $this->model_tool_image->resize($mall_data['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght')),
        'description' => $mall_data['description'],
        'social_jeeran' => $mall_data['social_jeeran'],
        'social_fb' => $mall_data['social_fb'],
        'social_tw' => $mall_data['social_tw'],
        'latitude' => $mall_data['latitude'],
        'longitude' => $mall_data['longitude'],
        'shop_categories' => $mall_categories,
        'total_of_shops'=>$mall_data['shop_count'],
        'similar_offers'=>$offers_array
      );

      echo json_encode($return_array);

    }

    public function getShops(){

      $this->load->model("malls/shop");
      $this->load->model('tool/image');

      $json = array();
      $filter_date = array();

      $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

      $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
      $language_id = 2;
      if ($language == 'en'){
        $language_id=1;
      }
      $this->config->set('config_language_id',$language_id);


      $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
      $country_code = 'sa';
      if ($country == 'ae'){
        $country_code = 'ae';
      }
      $this->country->set($country_code);
      $filter_country_id = $this->country->getId();

      if (isset($this->request->get['filter_filter'])) {
          $filter_filter = $this->request->get['filter_filter'];
      } else {
          $filter_filter = NULL;
      }

      if (isset($this->request->get['filter_zone'])) {
          $filter_zone_id = $this->request->get['filter_zone'];
      } else {
          $filter_zone_id = NULL;
      }

      if (isset($this->request->get['filter_mall'])) {
          $filter_mall_id = $this->request->get['filter_mall'];
      } else {
          $filter_mall_id = '';
      }

      if (isset($this->request->get['filter_category'])) {
          $filter_category = $this->request->get['filter_category'];
      } else {
          $filter_category = NULL;
      }

      if (isset($this->request->get['filter_brand'])) {
          $filter_brand = $this->request->get['filter_brand'];
      } else {
          $filter_brand = NULL;
      }

      if (isset($this->request->get['order_by'])) {
          $order = $this->request->get['order_by'];
      } else {
          $order = 'ASC';
      }

      if (isset($this->request->get['limit'])) {
          $limit = (int) $this->request->get['limit'];
      } else {
          $limit = 8;
      }

      if (isset($this->request->get['page'])) {
          $page = $this->request->get['page'];
      } else {
          $page = 1;
      }

      if (isset($this->request->get['latitude'])) {
          $latitude = $this->request->get['latitude'];
      } else {
          $latitude = 1;
      }

      if (isset($this->request->get['longitude'])) {
          $longitude = $this->request->get['longitude'];
      } else {
          $longitude = 1;
      }

      $filter_data = array(
          'filter_filter' => $filter_filter,
          'filter_country_id' => $filter_country_id,
          'filter_zone_id' => $filter_zone_id,
          'filter_category' => $filter_category,
          'filter_mall_id' => $filter_mall_id,
          'filter_brand' => $filter_brand,
          'order' => $order,
          'start' => ($page - 1) * $limit,
          'limit' => $limit,
          'longitude' => $longitude,
          'latitude' => $latitude
      );


      $requestShops = $this->model_malls_shop->getShopsAPI($filter_data);
      $return_shops = array ();
      foreach ($requestShops as $key => $shop){
        $return_shops[] = array (
          'shop_id' => $shop['shop_id'],
          'title' =>  $shop['name'],
          'image' => $this->model_tool_image->resize($shop['image'],$this->config_image->get('shops_brands_designer', 'listing', 'width'), $this->config_image->get('shops_brands_designer', 'listing', 'hieght')),
          'latitude' => $shop['latitude'],
          'longitude' => $shop['longitude'],
          'zoom' => $shop['zoom'],
          'distance' => $shop['distance']
        );
      }

      $retun_array = array (
        'filters_array' => $filter_data,
        'shops_array' => $return_shops
      );

      echo json_encode($retun_array);
    }

    public function getShopDetails(){

      $this->load->model('malls/shop');
      $this->load->model('catalog/offer');
      $this->load->model('tool/image');

      if (isset($this->request->get['shop_id'])) {
          $shop_id = $this->request->get['shop_id'];
      } else {
          echo json_encode(array("Error" => "No shop ID was entered"));
          return;
      }

      $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
      $language_id = 2;
      if ($language == 'en'){
        $language_id=1;
      }
      $this->config->set('config_language_id',$language_id);


      $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
      $country_code = 'sa';
      if ($country == 'ae'){
        $country_code = 'ae';
      }
      $this->country->set($country_code);
      $filter_country_id = $this->country->getId();


      $shop_data = $this->model_malls_shop->getShop($shop_id);
      if(empty($shop_data)){
        echo json_encode(array("Error" => "shop ID was not found"));
        return;
      }
      $shop_location = $this->model_malls_shop->getMap($shop_id);
      $shop_brands = $this->model_malls_shop->getShopBrands($shop_id);
      $shop_malls = $this->model_malls_shop->getShopBranches($shop_id);
      $malls = array();

      foreach ($shop_malls as $row) {
          $malls1 = explode(",", $row['zones']);
          foreach ($malls1 as $mall) {
              $malls[] = array(
                  'mall_id' => strstr($mall, '-', true),
                  'name' => substr($mall, 1 + strpos($mall, '-')),
              );
          }
        }
        $products = array();
        $shop_products = $this->model_malls_shop->getStoreProducts($shop_id);
        foreach($shop_products as $product_id => $product){
            $products[] = array (
            'product_id' => $product['product_id'],
            'name' => $product['name'],
            'image' => $this->model_tool_image->resize($product['image'],$this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght')),

          );
        }

      $return_array = array(
        'news_id' => $shop_data['store_id'],
        'title' => $shop_data['name'],
        'image_url' => $this->model_tool_image->resize($shop_data['image'],$this->config_image->get('shops_brands_designer', 'listing', 'width'), $this->config_image->get('shops_brands_designer', 'listing', 'hieght')),
        'description' => $shop_data['description'],
        'social_jeeran' => $shop_data['social_jeeran'],
        'social_fb' => $shop_data['social_facebook'],
        'social_tw' => $shop_data['social_twitter'],
        'social_website' => $shop_data['social_website'],
        'latitude' => $shop_location['latitude'],
        'longitude' => $shop_location['longitude'],
        'shop_malls' => $shop_malls,
        'shop_brands' =>$shop_brands,
        'latest_products' => $products
      );
      echo json_encode($return_array);

    }

    public function getOffers(){

      $this->load->model("catalog/offer");
      $this->load->model('tool/image');

      $json = array();
      $filter_date = array();

      $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

      $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
      $language_id = 2;
      if ($language == 'en'){
        $language_id=1;
      }
      $this->config->set('config_language_id',$language_id);


      $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
      $country_code = 'sa';
      if ($country == 'ae'){
        $country_code = 'ae';
      }
      $this->country->set($country_code);
      $filter_country_id = $this->country->getId();

      if (isset($this->request->get['filter_filter'])) {
          $filter_filter = $this->request->get['filter_filter'];
      } else {
          $filter_filter = NULL;
      }

      if (isset($this->request->get['filter_mall'])) {
          $filter_mall = $this->request->get['filter_mall'];
      } else {
          $filter_mall = NULL;
      }

      if (isset($this->request->get['filter_zone'])) {
          $filter_zone = $this->request->get['filter_zone'];
      } else {
          $filter_zone = NULL;
      }

      if (isset($this->request->get['filter_category'])) {
          $filter_category = $this->request->get['filter_category'];
      } else {
          $filter_category = NULL;
      }

      if (isset($this->request->get['sort'])) {
          $sort = $this->request->get['sort'];
      } else {
          $sort = 'o.start_date';
      }

      if (isset($this->request->get['order'])) {
          $order = $this->request->get['order'];
      } else {
          $order = 'ASC';
      }

      if (isset($this->request->get['page'])) {
          $page = $this->request->get['page'];
      } else {
          $page = 1;
      }

      if (isset($this->request->get['limit'])) {
          $limit = (int) $this->request->get['limit'];
      } else {
          $limit = 8;
      }

      $filter_data = array(
          'filter_filter' => $filter_filter,
          'filter_country' => $filter_country_id,
          'filter_zone' => $filter_zone,
          'filter_category' => $filter_category,
          'filter_mall' => $filter_mall,
          'sort' => $sort,
          'order' => $order,
          'start' => ($page - 1) * $limit,
          'limit' => $limit
      );

      $requestOffers = $this->model_catalog_offer->getOffers($filter_data);
      $offer_malls = $this->model_catalog_offer->getOfferMalls();

      $return_offers = array ();

      foreach ($requestOffers as $key => $offer){
        $return_offers [] = array(
          'offer_id' => $offer['offer_id'],
          'title' => $offer['title'],
          'start_date' => $offer['start_date'],
          'end_date' => $offer['end_date'],
          'body_text' => $offer['body_text'],
          'image' => $this->model_tool_image->resize($offer['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght')),
        );
      }

      $retun_array = array (
        'filters_array' => $filter_data,
        'offers_array' => $return_offers,
        'offer_malls' => $offer_malls

      );
      echo json_encode($retun_array);

    }

    public function getOfferDetails(){


        $this->load->model('catalog/offer');
        $this->load->model('malls/shop');
        $this->load->model('tool/image');

        if (isset($this->request->get['offer_id'])) {
            $offer_id = $this->request->get['offer_id'];
        } else {
            echo json_encode(array("Error" => "Please Send an offer Id"));
            return ;
        }

        $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

        $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
        $language_id = 2;
        if ($language == 'en'){
          $language_id=1;
        }
        $this->config->set('config_language_id',$language_id);

        $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
        $country_code = 'sa';
        if ($country == 'ae'){
          $country_code = 'ae';
        }
        $this->country->set($country_code);
        $filter_country_id = $this->country->getId();

        $offer_data = $this->model_catalog_offer->getOffer($offer_id);
        if (empty($offer_data)){
          echo json_encode(array("Error" => "Offer Id was not found"));
          return ;
        }
        $shop_location = $this->model_malls_shop->getMap($offer_data['store_id']);
        $offer_malls = $this->model_catalog_offer->getOfferMalls();
        $related_offers = $this->model_catalog_offer->getRelatedOffers($offer_id,$offer_data['start_date']);
        foreach ($related_offers as $id => $offer1){
          $related_offers_array [] = array (
            'offer_id' => $offer1['offer_id'],
            'title' => $offer1['title'],
            'start_date' => $offer1['start_date'],
            'end_date' => $offer1['end_date'],
            'period' => $offer1['period'],
            'image' => $this->model_tool_image->resize($offer1['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght')),
          );
        }

        $return_array = array(
          'offer_id' => $offer_data['offer_id'],
          'start_date' => $offer_data['start_date'],
          'end_date' => $offer_data['end_date'],
          'title' => $offer_data['title'],
          'image_url' => $this->model_tool_image->resize($offer_data['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght')),
          'description' => $offer_data['body_text'],
          'latitude' => $shop_location['latitude'],
          'longitude' => $shop_location['longitude'],
          'similar_products' => '',
          'similar_offers' => $related_offers_array,
          'offer_malls' => $offer_malls
        );
        echo json_encode($return_array);
    }

    public function mall_check_in(){

      $this->load->model("account/customer");
      $this->load->model("account/favorites");

      $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

      if ($access_token == '' || $access_token == NULL || empty($access_token)){
        echo json_encode(array("Error" => "No Access token was recieved"));
        return;
      }
      $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);
      if (empty($customer_data)){
        echo json_encode(array("Error" => "No Access token matched"));
        return;
      }

      if (isset($this->request->get['mall_id'])) {
          $mall_id = $this->request->get['mall_id'];
      } else {
          echo json_encode(array("Error" => "Please Send the Mall Id"));
          return ;
      }

      $this->model_account_favorites->check_in($customer_data['customer_id'],$mall_id,'mall');
      $json['status'] = 'Success';
      echo json_encode($json);

    }

    public function shop_check_in(){

      $this->load->model("account/customer");
      $this->load->model("account/favorites");

      $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

      if ($access_token == '' || $access_token == NULL || empty($access_token)){
        echo json_encode(array("Error" => "No Access token was recieved"));
        return;
      }
      $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);
      if (empty($customer_data)){
        echo json_encode(array("Error" => "No Access token matched"));
        return;
      }

      if (isset($this->request->get['shop_id'])) {
          $shop_id = $this->request->get['shop_id'];
      } else {
          echo json_encode(array("Error" => "Please Send the Shop Id"));
          return ;
      }

      $this->model_account_favorites->check_in($customer_data['customer_id'],$shop_id,'shop');
      $json['status'] = 'Success';
      echo json_encode($json);

    }

  }
