<?php

class ControllerMallTips extends Controller{

  public function gettips(){

    $this->load->model("tips/tips");
    $this->load->model('tool/image');

    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

    $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
    $language_id = 2;
    if ($language == 'en'){
      $language_id=1;
    }
    $this->config->set('config_language_id',$language_id);

    $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
    $country_code = 'sa';
    if ($country == 'ae'){
      $country_code = 'ae';
    }
    $this->country->set($country_code);


    if (isset($this->request->get['filter_group_id'])) {
        $filter_group_id = $this->request->get['filter_group_id'];
    } else {
        $filter_group_id = '';
    }

    if (isset($this->request->get['sort'])) {
        $sort = $this->request->get['sort'];
    } else {
        $sort = 't.date_added ';
    }

    if (isset($this->request->get['order'])) {
        $order = $this->request->get['order'];
    } else {
        $order = 'DESC';
    }

    if (isset($this->request->get['page'])) {
        $page = $this->request->get['page'];
    } else {
        $page = 1;
    }

    if (isset($this->request->get['limit'])) {
        $limit = (int) $this->request->get['limit'];
    } else {
        $limit = 8;
    }

    $filter_data = array(
        'filter_group_id' =>$filter_group_id,
        'sort' => $sort,
        'order' => $order,
        'start' => ($page - 1) * $limit,
        'limit' => $limit
    );

    $results = $this->model_tips_tips->getTips($filter_data);

    foreach ($results as $key => $result){
      $results[$key]['image'] = $this->model_tool_image->resize($result['image'],$this->config_image->get('tips', 'listing', 'width'), $this->config_image->get('tips', 'listing', 'hieght'));
    }

    $return_array = array(
      'tips_data' => $results,
      'filter_array' => $filter_data
    );

    echo json_encode($return_array);

  }


  public function gettipDetails(){

    $this->load->model("tips/tips");
    $this->load->model('tool/image');

    if (isset($this->request->get['tip_id'])) {
        $tip_id = $this->request->get['tip_id'];
    } else {
        echo json_encode(array("Error" => "No tip is was recieved"));
        return;
    }

    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

    $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
    $language_id = 2;
    $writer_company = 'سيدتي مول.نت';
    if ($language == 'en'){
      $language_id=1;
      $writer_company = 'sayidaty.net';
    }
    $this->config->set('config_language_id',$language_id);

    $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
    $country_code = 'sa';
    if ($country == 'ae'){
      $country_code = 'ae';
    }
    $this->country->set($country_code);

    $tip_data = $this->model_tips_tips->gettip($tip_id);
    if (empty($tip_data)){
      echo json_encode(array("Error" => "Tip was not found"));
      return;
    }
    $tip_sections = $this->model_tips_tips->getTipSection($tip_id);
    foreach($tip_sections as $key => $section){
      $tip_sections[$key]['image'] = $this->model_tool_image->resize($section['image'],$this->config_image->get('tips', 'listing', 'width'), $this->config_image->get('tips', 'listing', 'hieght'));
    }

    $other_tip = $this->model_tips_tips->getOtherTips(array('tip_id'=>$tip_id,'order_by' => 't.date_added'));
    foreach ($other_tip as $k => $tip ){
      $related_tip[$k] = array(
        'tip_id' => $tip['tip_id'],
        'title' => $tip['name'],
        'image' => $this->model_tool_image->resize($tip['tip_id'],$this->config_image->get('tips', 'listing', 'width'), $this->config_image->get('tips', 'listing', 'hieght'))
      );
    }

    $tip_categories = $this->model_tips_tips->getTipsGroupsByTip($tip_id);

    $return_array = array(
      'tip_id' => $tip_data['tip_id'],
      'title' => $tip_data['name'],
      'tip_categories' => $tip_categories,
      'date_added' => $tip_data['date_added'],
      'writer' => $tip_data['author'],
      'writer_company' =>$writer_company,
      'image_url' => $this->model_tool_image->resize($tip_data['image'],$this->config_image->get('tips', 'details', 'width'), $this->config_image->get('tips', 'details', 'hieght')),
      'description' => $tip_sections,
      'related_tip' => $related_tip
    );

      echo json_encode($return_array);

  }

}
