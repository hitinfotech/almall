<?php


class ControllerMallFavorites extends Controller{

  public function save_size(){

    $json = array();
    $this->load->model('account/customer');
    $this->load->model('account/favorites');
    $access_token = isset($this->request->get['access_token']) ? $this->request->get['access_token'] : '';

    $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);

    if(isset($customer_data) && !empty($customer_data)){

      $customer_id = $customer_data['customer_id'];
      $options_values = isset($this->request->get['options_values'])?$this->request->get['options_values']:'';
      if ($options_values != ''){
        $this->model_account_favorites->save_sizes($customer_id,$options_values);
        $json['status'] = 'Success';
        echo json_encode($json);
      }else{
        $json['error'] = 'There is no added options to be saved';
        echo json_encode($json);
      }
    }else{
      $json['status'] = 'Failed';
      $json['error'] = 'Wrong access Token was given';
      echo json_encode($json);
    }
  }

  public function getAllSizes(){

    $json = array();
    $this->load->model('account/customer');
    $this->load->model('account/favorites');

    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';
    $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);

    $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
    $language_id = 2;
    if ($language == 'en'){
      $language_id=1;
    }
    $this->config->set('config_language_id',$language_id);

    echo json_encode( $this->model_account_favorites->getFavoriteSizes($customer_data['customer_id']));
  }


}
