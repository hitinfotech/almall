<?php

class ControllerMallHomepage extends Controller {

  public function index(){

    $this->load->model('home/home');
    $this->load->model('look/look');
    $this->load->model('tips/tips');
    $this->load->model('tool/image');
    $this->load->model('malls/shop');
    $this->load->model('malls/mall');
    $this->load->model('malls/news');
    $this->load->model('account/cart');
    $this->load->model('catalog/offer');
    $this->load->model('catalog/product');
    $this->load->model('account/wishlist');
    $this->load->model('account/customer');

    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';
    $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);

    $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
    $language_id = 2;
    if ($language == 'en'){
      $language_id=1;
    }
    $this->config->set('config_language_id',$language_id);


    $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
    $country_code = 'sa';
    if ($country == 'ae'){
      $country_code = 'ae';
    }
    $this->country->set($country_code);

    $favorite_products = array_column($this->model_account_wishlist->getWishlistIDs($customer_data['customer_id']), 'product_id');
    $incart_products = array_column($this->model_account_cart->getProducts($customer_data['customer_id']), 'product_id');
    $cart_items_count  = count($incart_products);
    $categories = $this->model_home_home->getSelectedCategories();

    $filter_data = array(
        'sort' => 'p.date_added',
        'order' => 'DESC',
        'start' => 0,
        'limit' => 8
    );

    $homepage_products = $this->model_catalog_product->getCustLatestProducts2($filter_data);
    foreach($homepage_products as $id => $prod){
      $is_favorite = 0;
      $is_in_cart = 0;
      if(in_array($prod['product_id'],$favorite_products)){
        $is_favorite = 1;
      }
      if(in_array($prod['product_id'],$incart_products)){
        $is_in_cart = 1;
      }

      $result_products [] = array (
      'product_id' => $prod['product_id'],
      'product_name' => $prod['name'],
      'product_description' => $prod['description'],
      'product_image' => $this->model_tool_image->resize($prod['image'],$this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght')),
      'product_price' => $prod['price'],
      'product_special' => $prod['special'],
      'is_favorite' => $is_favorite,
      'is_in_cart' => $is_in_cart,
      'stock_status' => $prod['stock_status'],
    );
    }

    $homepage_offers = $this->model_catalog_offer->getOffers(array('limit' => 8));
    foreach($homepage_offers as $id=> $offer){
      $return_offer [] = array (
      'offer_id' => $offer['offer_id'],
      'start_date' => $offer['start_date'],
      'end_date' => $offer['end_date'],
      'title' => $offer['title'],
      'type' => $offer['period'],
      'image' => $this->model_tool_image->resize($offer['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght')),

    );
    }

    $homepage_shops = $this->model_malls_shop->getShopsAPI(array('limit' => 8));
    foreach($homepage_shops as $id => $shop){
      $homepage_shops[$id]['image'] =$this->model_tool_image->resize($shop['image'],$this->config_image->get('shops_brands_designer', 'listing', 'width'), $this->config_image->get('shops_brands_designer', 'listing', 'hieght'));
    }

    $homepage_malls = $this->model_malls_mall->getMalls(array('limit' => 8));
    foreach($homepage_malls as $key => $mall){
        $malls_array [] = array (
        'mall_id' => $mall['mall_id'],
        'name' => $mall['name'],
        'latitude' => $mall['latitude'],
        'longitude' => $mall['longitude'],
        'zoom' => $mall['zoom'],
      );
    }

    $homepage_news = $this->model_malls_news->getNewsList(array('limit' => 8));
    foreach($homepage_news as $id => $news){
        $news_array[] = array(
          'news_id' => $news['news_id'],
          'title' => $news['title'],
          'image' => $this->model_tool_image->resize($news['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght')),

        );
    }

    $homepage_looks = $this->model_look_look->getLooks(array('limit' => 8));
    foreach($homepage_looks as $key => $look){
        $array_looks [] = array (
          'look_id' =>$look['look_id'],
          'title' =>$look['title'],
          'image'=> $this->model_tool_image->resize($look['image'],$this->config_image->get('looks', 'listing', 'width'), $this->config_image->get('looks', 'listing', 'hieght')),
      );
    }

    $homepage_tips = $this->model_tips_tips->getTips(array('limit' => 8));
    foreach($homepage_tips as $key => $tip){
        $array_tips [] = array (
          'tip_id' =>$tip['tip_id'],
          'title' =>$tip['name'],
          'image'=> $this->model_tool_image->resize($tip['image'],$this->config_image->get('tips', 'listing', 'width'), $this->config_image->get('tips', 'listing', 'hieght')),
      );
    }



    $return_array = array(
      'homepage_products' => $result_products,
      'homepage_offers' => $return_offer,
      'homepage_shops' => $homepage_shops,
      'homepage_malls' => $malls_array,
      'homepage_news' => $news_array,
      'homepage_looks' => $array_looks,
      'homepage_tips' => $array_tips,
      'categories'=>$categories,
      'cart_items_count' =>$cart_items_count
    );
 echo json_encode($return_array);

  }



}
