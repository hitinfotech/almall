<?php

class ControllerMallLogin extends Controller {

    public function index() {

        $this->load->model('account/customer');
        header('Content-Type: application/json');

        $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
        $language_id = 2;
        if ($language == 'en'){
          $language_id=1;
        }
        $this->config->set('config_language_id',$language_id);



        if (($this->request->server['REQUEST_METHOD'] == 'GET') && $this->validate()) {
            $email = isset($this->request->get['email']) ? $this->request->get['email'] : '';
            $password = isset($this->request->get['password']) ? $this->request->get['password'] : '';

            $udid = isset($this->request->get['udid']) ? $this->request->get['udid'] : '';
            if ($udid == ''){
              echo json_encode(array("Error" => "UDID must be sent"));
              return;
            }

            $notification_token = isset($this->request->get['notification_token']) ? $this->request->get['notification_token'] : '';

            $customerinfo = $this->model_account_customer->getCustomerByEmail($email);

            $access_token = bin2hex(openssl_random_pseudo_bytes(16));
            $this->model_account_customer->updateCustomerToken($customerinfo['customer_id'],$access_token,$udid);

            $customer = array(
                'filter_category_id' => $segment_category,
                'image_url' => $customerinfo['image'],
                'gender' => $customerinfo['gender'],
                'country_id' => $customerinfo['country_id'],
                'email' => $customerinfo['email'],
                'city_id' => $customerinfo['zone_id'],
                't-shirt_size' => $customerinfo[''],
                't-shirt_unit' => $customerinfo[''],
                'dress_size' => $customerinfo[''],
                'dress_unit' => $customerinfo[''],
                'plant_size' => $customerinfo[''],
                'plant_unit' => $customerinfo[''],
                'shoes_size' => $customerinfo[''],
                'shoesunit' => $customerinfo[''],
                'firstname' => $customerinfo['firstname'],
                'lastname' => $customerinfo['lastname'],
                'birthdate' => $customerinfo['birthdate'],
                'access_token' => $access_token
            );
            echo json_encode($customer);
        }else{
          echo json_encode(array("Error" => "User name and password were not matched !"));
        }
    }

    protected function validate() {
        $this->event->trigger('pre.customer.login');

        if(! isset($this->request->get['email']) || ($this->request->get['email'] =='')){
          $this->request->get['email'] = '';
        }
        if(! isset($this->request->get['password']) || ($this->request->get['password'] =='')){
          $this->request->get['password'] = '';
        }

        // Check how many login attempts have been made.
        $login_info = $this->model_account_customer->getLoginAttempts($this->request->get['email']);

        if (empty($error) ){
            if (!$this->customer->login($this->request->get['email'], $this->request->get['password'])) {

                $error['warning'] = $this->language->get('error_login');

                $this->model_account_customer->addLoginAttempt($this->request->get['email']);
            } else {
                $this->model_account_customer->deleteLoginAttempts($this->request->get['email']);
            }
        }
        return empty($error);
    }

    public function logout(){

      $this->load->model('account/customer');
      $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

      if($access_token == ''){
        echo json_encode(array("Error" => "Access token must be sent"));
        return;
      }

      $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);
      if (empty($customer_data)){
        echo json_encode(array("Error" => "Access token didnt match for any user"));
        return;
      }

      $this->model_account_customer->deleteCustomerToken($customer_data['customer_id'],$access_token);
      echo json_encode("Success");
      return;
    }

    public function forget_password(){

      $this->load->model('account/customer');

      if (!isset($this->request->get['email']) || $this->request->get['email'] == '') {
          echo json_encode (array('Error' => 'Email must be added'));
          return;
      } elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->get['email'])) {
          echo json_encode (array('Error' => 'User was not found'));
        return ;
      }

      $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->get['email']);

      if ($customer_info && !$customer_info['approved']) {
          $json['Error'] = 'User is not approved';
      }
      $this->load->language('mail/forgotten');

      $password = substr(sha1(uniqid(mt_rand(), true)), 0, 10);

      $this->model_account_customer->editPassword($this->request->get['email'], $password);

      $subject = sprintf($this->language->get('text_subject'), $customer_info['firstname']);

      $message = $this->language->get('text_greeting_p1') . $customer_info['firstname'] . $this->language->get('text_greeting_p2') . $password . $this->language->get('text_greeting_p3');

      $mail = new Mail();
      $mail->protocol = $this->config->get('config_mail_protocol');
      $mail->parameter = $this->config->get('config_mail_parameter');
      $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
      $mail->smtp_username = $this->config->get('config_mail_smtp_username');
      $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
      $mail->smtp_port = $this->config->get('config_mail_smtp_port');
      $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

      $mail->setTo($this->request->get['email']);
      $mail->setFrom($this->config->get('config_email'));
      $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
      $mail->setSubject($subject);
      $mail->setHtml($message);
      $mail->send();

      echo json_encode( "Success");

      if ($customer_info) {
          $this->load->model('account/activity');

          $activity_data = array(
              'customer_id' => $customer_info['customer_id'],
              'name' => $customer_info['firstname'] . ' ' . $customer_info['lastname']
          );

          $this->model_account_activity->addActivity('forgotten', $activity_data);
      }

    }

}
