<?php

class ControllerMallRegister extends Controller{

    public function index(){

      $this->load->model('account/customer');

      header('Content-Type: application/json');

      $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
      $language_id = 2;
      if ($language == 'en'){
        $language_id=1;
      }
      $this->config->set('config_language_id',$language_id);


      $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
      $country_code = 'sa';
      if ($country == 'ae'){
        $country_code = 'ae';
      }
      $this->country->set($country_code);

      if(isset($this->request->get['code'])){//this variable should be code instead of facebook_token.
        $social_id = isset($this->request->get['social_id']) ? $this->request->get['social_id'] : '';
        $notification_token = isset($this->request->get['notification_token']) ? $this->request->get['notification_token'] : '';
        $facebook_token = isset($this->request->get['facebook_token']) ? $this->request->get['facebook_token'] : '';
        $lang = isset($this->request->get['lang']) ? $this->request->get['lang'] : 'ar';
        $result = $this->doSocialAction();
      }else{
        $result = $this->register_via_email();
        echo json_encode($result);
        return;
      }
  }


  public function register_via_email(){

    $json = array();

    if(! isset($this->request->get['firstname']) || $this->request->get['firstname'] ==''){
      $json['error'] = 'Error: You have to add the firsname';
    }elseif(! isset($this->request->get['lastname']) || $this->request->get['lastname'] ==''){
      $json['error'] = 'Error: You have to add the lastname';
    }elseif(! isset($this->request->get['email']) || $this->request->get['email'] ==''){
      $json['error'] = 'Error: You have to add the Email!';
    }elseif (!filter_var($this->request->get['email'], FILTER_VALIDATE_EMAIL)){
      $json['error'] = 'Error: wrong email syntax';
    }elseif(! isset($this->request->get['password']) || $this->request->get['password'] ==''){
      $json['error'] = 'Error: You have to add the Password!';
    }elseif(! isset($this->request->get['udid']) || $this->request->get['udid'] ==''){
      $json['error'] = 'Error: You have to add the UDID';
    }

    if(empty($json)){
      $firstname = isset($this->request->get['firstname']) ? $this->request->get['firstname'] : '';
      $lastname = isset($this->request->get['lastname']) ? $this->request->get['lastname'] : '';
      $email = isset($this->request->get['email']) ? $this->request->get['email'] : '';
      $password = isset($this->request->get['password']) ? $this->request->get['password'] : '';
      $country_id = $this->country->getid();
      $gender = isset($this->request->get['gender']) ? $this->request->get['gender'] : 1;
      $notification_token = isset($this->request->get['notification_token']) ? $this->request->get['notification_token'] : '';
      $udid = isset($this->request->get['udid']) ? $this->request->get['udid'] : '';

      $data = array(
          'firstname' => $firstname,
          'lastname' => $lastname,
          'email' => $email,
          'password' => $password,
          'country_id' => $country_id,
          'gender' => $gender,
          'notification_token' => $notification_token,
          'zone_id' => '',
          'birthdate' => '',
          'gender' => ''
      );
      $new_customer_id = $this->model_account_customer->addCustomer($data);
      if($new_customer_id > 0){
          $access_token = bin2hex(openssl_random_pseudo_bytes(16));
          $this->model_account_customer->updateCustomerToken($new_customer_id,$access_token,$udid);

          return $access_token;
        }else{
          $json['error'] = 'Error: Email is already in use';
          return json_encode($json);
        }
    }else{
      return json_encode($json);
    }
  }

  public function doSocialAction() {

    $udid = isset($this->request->get['udid']) ? $this->request->get['udid'] : '';
    if ($udid == ''){
      echo json_encode(array("Error" => "UDID must be sent"));
      return;
    }

    $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
    $country_code = 'sa';
    if ($country == 'ae'){
      $country_code = 'ae';
    }
    $this->country->set($country_code);

    $notification_token = isset($this->request->get['notification_token']) ? $this->request->get['notification_token'] : '';
    $access_token = bin2hex(openssl_random_pseudo_bytes(16));

    $aCustomerInfo = array();
    $user_profile = array();
    if (isset($this->request->get['facebook_token']) && $this->request->get['facebook_token'] != ''){
      $this->facebook->setAccessToken($this->request->get['facebook_token']);
    }

    try {
        // 1. post Facebook API Data
        $user_profile = $this->facebook->api('/me?fields=id,first_name,last_name,email,gender,birthday,hometown,picture');
        $dob = date('Y-m-d', strtotime($user_profile['birthday']));
        $return_array = array();
        if (!empty($user_profile)) {
            $this->load->model('account/customer');

            // 2. Save the user to the database (Register)
            $aCustomerInfo['firstname'] = $return_array['firstname'] = $user_profile['first_name'];
            $aCustomerInfo['lastname'] = $return_array['lastname'] = $user_profile['last_name'];
            $aCustomerInfo['email'] = $return_array['email'] = $user_profile['email'];
            $aCustomerInfo['image'] = $return_array['image'] = $user_profile['picture']['data']['url'];
            $aCustomerInfo['birthdate'] = $return_array['birthdate'] = $dob;
            $aCustomerInfo['password'] = "";
            $aCustomerInfo['telephone'] = "";
            $aCustomerInfo['fax'] = "";
            $aCustomerInfo['custom_field'] = "";
            $aCustomerInfo['company'] = "";
            $aCustomerInfo['address_1'] = $user_profile['hometown']['name'];
            $aCustomerInfo['address_2'] = "";
            $aCustomerInfo['city'] = $return_array['city_id'] ="";
            $aCustomerInfo['gender'] = $return_array['gender'] = $user_profile['gender'];
            $aCustomerInfo['postcode'] = "";
            $aCustomerInfo['country_id'] = $return_array['country_id'] = $this->country->getId();
            $aCustomerInfo['zone_id'] = "";
            $aCustomerInfo['source'] = 'facebook';
            $aCustomerInfo['access_token'] = $return_array['access_token'] = $access_token;
            $return_array['tshirt_size'] = '' ;
            $return_array['tshirt_unit'] = '' ;
            $return_array['dress_size'] = '' ;
            $return_array['plant_size'] = '' ;
            $return_array['plant_unit'] = '' ;
            $return_array['shoes_size'] = '' ;
            $return_array['shoes_unit'] = '' ;

            $checkCustomer = $this->model_account_customer->getCustomerByEmail($aCustomerInfo['email']);

            if (!empty($checkCustomer)) {
              $this->model_account_customer->updateCustomerToken($checkCustomer['customer_id'],$access_token,$udid);

                $this->customer->login($aCustomerInfo['email'], '', true);
                echo  json_encode($return_array);
            } else {
                $customer_id = $this->model_account_customer->addCustomer($aCustomerInfo);
                $this->model_account_customer->updateCustomerToken($customer_id,$access_token,$udid);

                // 3. Login the user to the site
                $this->customer->login($aCustomerInfo['email'], '', true);
                echo  json_encode($return_array);
            }
        }
    } catch (Exception $e) {
        die($e->getMessage());
    }
    exit;
  }

  public function setup_user_profile(){

    $json = array();
    $this->load->model('account/customer');
    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';
    if($access_token == ''){
      echo json_encode(array("Error" => "No access_token was sent"));
      return;
    }

    //base64_decode()
    $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);
    if(isset($customer_data) && !empty($customer_data)){
      $customer_data['birthdate']= isset($this->request->get['date_of_birth']) ? $this->request->get['date_of_birth'] : '';
      $customer_data['zone_id'] = isset($this->request->get['city_id']) ? $this->request->get['city_id'] : '';
      $this->model_account_customer->editCustomer($customer_data['customer_id'],$customer_data);
      $json['status'] = 'Success';
      echo json_encode($json);
    }else{
      $json['status'] = 'Failed';
      $json['error'] = 'Wrong access Token was given';
      echo json_encode($json);
    }


  }

}
