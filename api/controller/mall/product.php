<?php

class ControllerMallProduct extends Controller{

  public function getProducts(){

    $this->load->model('account/wishlist');
    $this->load->model('catalog/product');
    $this->load->model('account/customer');
    $this->load->model('tool/image');

    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';
    $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);

    $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
    $language_id = 2;
    if ($language == 'en'){
      $language_id=1;
    }
    $this->config->set('config_language_id',$language_id);

    $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
    $country_code = 'sa';
    if ($country == 'ae'){
      $country_code = 'ae';
    }
    $this->country->set($country_code);


    if (isset($this->request->get['filter_name'])) {
        $filter_name = $this->request->get['filter_name'];
    } else {
        $filter_name = null;
    }

    if (isset($this->request->get['filter_model'])) {
        $filter_model = $this->request->get['filter_model'];
    } else {
        $filter_model = null;
    }

    if (isset($this->request->get['filter_price'])) {
        $filter_price = $this->request->get['filter_price'];
    } else {
        $filter_price = null;
    }

    if (isset($this->request->get['filter_brand'])) {
        $filter_brand = $this->request->get['filter_brand'];
    } else {
        $filter_brand = null;
    }

    if (isset($this->request->get['filter_quantity'])) {
        $filter_quantity = $this->request->get['filter_quantity'];
    } else {
        $filter_quantity = null;
    }

    if (isset($this->request->get['filter_status'])) {
        $filter_status = $this->request->get['filter_status'];
    } else {
        $filter_status = null;
    }

    if (isset($this->request->get['sort'])) {
        $sort = $this->request->get['sort'];
    } else {
        $sort = 'p.date_added';
    }

    if (isset($this->request->get['order'])) {
        $order = $this->request->get['order'];
    } else {
        $order = 'DESC';
    }

    if (isset($this->request->get['page'])) {
        $page = $this->request->get['page'];
    } else {
        $page = 1;
    }

    if (isset($this->request->get['limit'])) {
        $limit = (int) $this->request->get['limit'];
    } else {
        $limit = 15;
    }

    $filter_data = array(
      'filter_name' => $filter_name,
      'filter_model' => $filter_model,
      'filter_price' => $filter_price,
      'filter_brand' => $filter_brand,
      'filter_quantity' => $filter_quantity,
      'filter_status' => $filter_status,
      'sort' => $sort,
      'order' => $order,
      'start' => ($page - 1) * $limit,
      'limit' => $limit
    );

    $products = $this->model_catalog_product->getProducts($filter_data);
    foreach($products as $id => $product){

      $favorite_products = array_column($this->model_account_wishlist->getWishlistIDs($customer_data['customer_id']), 'product_id');
      $in_cart_products = $this->model_catalog_product->getInCartProducts($customer_data['customer_id']);

      $is_favorite = 0;
      $is_in_cart = 0;
      if (!empty($customer_data) && $customer_data['customer_id'] != ''){
        if (in_array($product['product_id'],$favorite_products)){
          $is_favorite = 1;
        }
        if (in_array($product['product_id'],$in_cart_products)){
          $is_in_cart = 1;
        }
      }
      $product_array []=array(
        'product_id' => $product['product_id'],
        'title' => $product['name'],
        'price' => $product['price'],
        'special' =>     $product['special'],
        'image' => $this->model_tool_image->resize($product['image'],$this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght')),
        'stock_status' => $product['stock_status'],
        'is_favorite' => $is_favorite,
        'is_in_cart' => $is_in_cart
      );
    }
    $return_array = array (
    'product_array' => $product_array,
    'filter_array' => $filter_data
    );

    echo json_encode($return_array);
  }

  public function getProduct(){

    $this->load->model('catalog/product');
    $this->load->model('account/wishlist');
    $this->load->model('account/customer');
    $this->load->model('tool/image');

    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';
    $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);

    $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
    $language_id = 2;
    if ($language == 'en'){
      $language_id=1;
    }
    $this->config->set('config_language_id',$language_id);

    $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
    $country_code = 'sa';
    if ($country == 'ae'){
      $country_code = 'ae';
    }
    $this->country->set($country_code);


    if (isset($this->request->get['product_id'])) {
        $product_id = $this->request->get['product_id'];
    } else {
        echo json_encode(array("Error" => "no product Id was sent"));
        return;
    }

    $product = $this->model_catalog_product->getProduct($product_id);
    if(empty($product)){
      echo json_encode(array("Error" => "Product id was not found"));
      return;
    }
    $product_category = $this->model_catalog_product->getCategories($product_id);
    $last_category_id = $product_category[count($product_category)-1]['category_id'];

    $product_option_value_data = array();
    foreach ($this->model_catalog_product->getProductOptions($product_id) as $option) {
        foreach ($option['product_option_value'] as $option_value) {
            if (!$option_value['subtract'] || ($option_value['quantity'] >= 0)) {
                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float) $option_value['price']) {
                    $price = $this->tax->calculate($option_value['price'], $product['tax_class_id'], $this->config->get('config_tax') ? 'P' : false);
                } else {
                    $price = false;
                }
                $image_link = '';
                if (isset($option_value['image']) && $option_value['image'] != ''){
                  $image_link =$this->model_tool_image->resize($option_value['image'],$this->config_image->get('product', 'config_image_cart', 'width'), $this->config_image->get('product', 'config_image_cart', 'hieght'));
                }

                $product_option_value_data[] = array(
                    'product_option_value_id' => $option_value['product_option_value_id'],
                    'option_value_id' => $option_value['option_value_id'],
                    'name' => $option_value['name'],
                    'image' =>$image_link,
                    'price' => $price,
                    'price_prefix' => $option_value['price_prefix'],
                    'quantity' => $option_value['quantity']
                );
            }
        }
    }

    $favorite_products = array_column($this->model_account_wishlist->getWishlistIDs($customer_data['customer_id']), 'product_id');
    $in_cart_products = $this->model_catalog_product->getInCartProducts($customer_data['customer_id']);

    $product_is_favorite = 0;
    $product_is_in_cart = 0;
    if (!empty($customer_data) && $customer_data['customer_id'] != ''){
      if (in_array($product['product_id'],$favorite_products)){
        $product_is_favorite = 1;
      }
      if (in_array($product['product_id'],$in_cart_products)){
        $product_is_in_cart = 1;
      }
    }

    $attribute_groups = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);
    $product_color = '';
    foreach ($attribute_groups as $key => $row) {
        $atr_name = $row['name'] . ",";
        foreach ($row['attribute'] as $key2 => $row2) {
            if ($row['attribute_group_id'] == 1) {
                $product_color .= $row2['name'] . " ,";
            }
        }
    }

    $filters['filter_brand_id'] = $product['brand_id'];
    $filters['ex_product_id'] = $product['product_id'];
    $filters['start'] = 0;
    $filters['limit'] = 4;
    $brand_products = $this->model_catalog_product->getProducts($filters);

    $similar_brands_products = array ();
    foreach($brand_products as $id => $prod){

      $is_favorite = 0;
      $is_in_cart = 0;
      if (in_array($prod['product_id'],$favorite_products)){
        $is_favorite = 1;
      }
      if (in_array($prod['product_id'],$in_cart_products)){
        $is_in_cart = 1;
      }
      $similar_brands_products [] = array(
        'product id' => $prod['product_id'],
        'title' => $prod['name'],
        'image_url' => $this->model_tool_image->resize($prod['image'],$this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght')),
        'is_favorite' => $is_favorite,
        'is_in_cart' => $is_in_cart,
        'store_status' => $prod['stock_status']
      );
    }

    $filters = array ();
    $filters['filter_category_id'] = $last_category_id;
    $filters['ex_product_id'] = $product['product_id'];
    $filters['start'] = 0;
    $filters['limit'] = 4;
    $similar_products = $this->model_catalog_product->getProducts($filters);

    foreach($similar_products as $id => $prod){

      $is_favorite = 0;
      $is_in_cart = 0;
      if (in_array($prod['product_id'],$favorite_products)){
        $is_favorite = 1;
      }
      if (in_array($prod['product_id'],$in_cart_products)){
        $is_in_cart = 1;
      }
      $product_similar_products [] = array(
        'product id' => $prod['product_id'],
        'title' => $prod['name'],
        'image_url' => $this->model_tool_image->resize($prod['image'], $this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght')),
        'is_favorite' => $is_favorite,
        'is_in_cart' => $is_in_cart,
        'store_status' => $prod['stock_status']
      );
    }

    $retun_array = array (
      'product_id' =>  $product['product_id'],
      'title' =>       $product['name'],
      'description' => $product['description'],
      'price' =>       $product['price'],
      'special' =>     $product['special'],
      'image' =>       $this->model_tool_image->resize($product['image'],$this->config_image->get('product', 'zooming', 'width'), $this->config_image->get('product', 'zooming', 'hieght')),
      'product_id' =>  $product['product_id'],
      'is_in_shop' =>  $product['stock_status'],
      'is_favorite' => $product_is_favorite,
      'is_in_cart' =>  $product_is_in_cart,
      'options' => $product_option_value_data,
      'products_attribute' => $attribute_groups,
      'similar_brands_products' => $similar_brands_products,
      'similar_products' => $product_similar_products
    );
    echo json_encode($retun_array);

  }

  public function add_to_wishlist(){

    $this->load->model('account/customer');
    $this->load->model('account/wishlist');

    $json = array();
    if(isset($this->request->get['product_id']) && $this->request->get['product_id'] != ''){
      $product_id = $this->request->get['product_id'];
    }else{
      $json['Error'] = 'No product id was sent !';
    }
    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

    if ($access_token == '' || $access_token == NULL || empty($access_token)){
      echo json_encode(array("Error" => "No Access token was recieved"));
      return;
    }
    $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);

    $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
    $language_id = 2;
    if ($language == 'en'){
      $language_id=1;
    }
    $this->config->set('config_language_id',$language_id);

    $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
    $country_code = 'sa';
    if ($country == 'ae'){
      $country_code = 'ae';
    }
    $this->country->set($country_code);


    if (! $json){
      if(!empty($customer_data)){
        $this->model_account_wishlist->addWishlist($product_id,$customer_data['customer_id']);
        $json['status'] = 'Success';
        echo json_encode($json);
      }else{
        $json['Error'] = 'No access token was matched!';
        echo json_encode($json);
      }
    }else{
      echo json_encode($json);
    }
  }

  public function remove_from_wishlist(){

    $this->load->model('account/wishlist');
    $this->load->model('account/customer');

    $json = array();
    if(isset($this->request->get['product_id']) && $this->request->get['product_id'] != ''){
      $product_id = $this->request->get['product_id'];
    }else{
      $json['Error'] = 'No product id was sent !';
    }
    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';
    if ($access_token == '' || $access_token == NULL || empty($access_token)){
      echo json_encode(array("Error" => "No Access token was recieved"));
      return;
    }
    $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);

    $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
    $language_id = 2;
    if ($language == 'en'){
      $language_id=1;
    }
    $this->config->set('config_language_id',$language_id);

    $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
    $country_code = 'sa';
    if ($country == 'ae'){
      $country_code = 'ae';
    }
    $this->country->set($country_code);


    if (! $json){
      if(!empty($customer_data)){
        $this->model_account_wishlist->deleteWishlist($product_id,$customer_data['customer_id']);
        $json['status'] = 'Success';
        echo json_encode($json);
      }else{
        $json['Error'] = 'No access token was matched!';
        echo json_encode($json);
      }
    }else{
      echo json_encode($json);
    }
  }


}
