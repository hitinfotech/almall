<?php

class ControllerMallDevicelogin extends Controller {

    public function index() {
        $this->load->model('account/customer');

        header('Content-Type: application/json');
        $udid = isset($this->request->get['udid']) ? $this->request->get['udid'] : '';
        $notification_token = isset($this->request->get['notification_token']) ? $this->request->get['notification_token'] : '';
        $lang = isset($this->request->get['lang']) ? $this->request->get['lang'] : 'ar';
        $device_type = isset($this->request->get['device_type']) ? $this->request->get['device_type'] : '';

        $is_valid_udid = $this->model_account_customer->check_if_valid_udid($udid);

//Detect special conditions devices
        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");

        if ($is_valid_udid) {
            $data = array(
                'notification_token' => $notification_token,
                'lang' => $lang,
                'device_type' => $device_type
            );
            if ($device_type == $iPhone || $device_type == $Android) {

                $response_array['status'] = 'success';
            } else {
                $response_array['status'] = 'faild';
            }
            echo json_encode($response_array);
        } else {
            echo json_encode(['Error: Wrong oauth_uid']);
        }
    }

}
