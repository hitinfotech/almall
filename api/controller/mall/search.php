<?php
class ControllerMallSearch extends Controller{

  public function index(){

    $this->load->model('tool/image');
    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

    $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
    $language_id = 2;
    if ($language == 'en'){
      $language_id=1;
    }
    $this->config->set('config_language_id',$language_id);

    $search = '';
    if (isset($this->request->get['search_string']) && $this->request->get['search_string'] != ''){
      $search = $this->request->get['search_string'];
    }

    $search_types = 1;
    if (isset($this->request->get['search_types']) && $this->request->get['search_types'] != '' && is_numeric($this->request->get['search_types']) && (int)$this->request->get['search_types'] <9){
      $search_types = (int)$this->request->get['search_types'];
    }

    $filters = array(
      'filter_filter' => $search,
      'order' => 'DESC',
      'limit' => 4,
      'start' => 0
    );

    /*
    Search Types:
      1- ALL
      2- Products
      3-Offers
      4- News
      5- shops
      6- malls
      7-looks
      8- tips
    */


    switch ($search_types) {
      case 1:
            $return_array['product'] = $this->getLatestProducts($filters);
            $return_array['offer'] = $this->getLatestOffers($filters);
            $return_array['news'] = $this->getLatestNews($filters);
            $return_array['shops'] = $this->getLatestShops($filters);
            $return_array['malls'] = $this->getLatestMalls($filters);
            $return_array['looks'] = $this->getLatestLooks($filters);
            $return_array['tips'] = $this->getLatestTips($filters);
        break;
      case 2:
            $return_array['product'] = $this->getLatestProducts($filters);
        break;
      case 3:
            $return_array['offer'] = $this->getLatestOffers($filters);
        break;
      case 4:
            $return_array['news'] = $this->getLatestNews($filters);
        break;
      case 5:
          $return_array['shops'] = $this->getLatestShops($filters);
        break;
      case 6:
          $return_array['malls'] = $this->getLatestMalls($filters);
        break;
      case 7:
          $return_array['looks'] = $this->getLatestLooks($filters);
      break;
      case 8:
          $return_array['tips'] = $this->getLatestTips($filters);
      break;

    }
    print_r($return_array);

  }

  public function getLatestProducts($filters){
    $filters['sort'] = 'p.date_added';
    $this->load->model('catalog/product');
    $this->load->model('account/wishlist');
    $this->load->model('account/customer');

    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';
    $customer_data= $this->model_account_customer->getCustomerByAccesstoken($access_token);

    $favorite_products = array_column($this->model_account_wishlist->getWishlistIDs($customer_data['customer_id']), 'product_id');
    $products_array = $this->model_catalog_product->getProducts($filters);

    foreach($products_array as $id => $product){
      $is_favorite = 0;
      if (in_array($id,$favorite_products)){
        $is_favorite = 1;
      }
        $return_product[] = array (
          'product_id' => $product['product_id'],
          'product_title' => $product['name'],
          'image_url' => $this->model_tool_image->resize($product['image'],$this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght')),
          'price' => $product['price'] ,
          'special' => $product['special'],
          'description' => $product['description'],
          'is_in_store' => $product['stock_status'],
          'is_favorite' => $is_favorite
        );
    }
    return $return_product;
  }

  public function getLatestOffers($filters){
    $this->load->model('catalog/offer');
    $offers_array = $this->model_catalog_offer->getOffers($filters);
    foreach($offers_array as $id => $offer){
        $return_offer[] = array (
          'offer_id' => $offer['offer_id'],
          'offer_title' => $offer['title'],
          'image_url' => $this->model_tool_image->resize($offer['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght')),
          'start_date' => $offer['start_date'],
          'end_date' => $offer['end_date'],
          'period' => $offer['period']
        );
      }
      return $return_offer;
  }

  public function getLatestNews($filters){
    $this->load->model('malls/news');
    $news_array = $this->model_malls_news->getNewsList($filters);
    foreach($news_array as $id => $news){
        $return_news[] = array (
          'news_id' => $news['news_id'],
          'news_title' => $news['title'],
          'image_url' => $this->model_tool_image->resize($news['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght'))
        );
    }
    return $return_news;
  }

  public function getLatestMalls($filters){
    $this->load->model('malls/mall');
    $malls_array = $this->model_malls_mall->getMalls($filters);
    foreach($malls_array as $id => $mall){
        $return_mall[] = array (
          'mall_id' => $mall['mall_id'],
          'mall_title' => $mall['name'],
          'image_url' => $this->model_tool_image->resize($mall['image'],$this->config_image->get('news_offers_malls', 'listing', 'width'), $this->config_image->get('news_offers_malls', 'listing', 'hieght')),
          'latitude' => $mall['latitude'],
          'longitude' => $mall['longitude'],
          'zoom' =>$mall['zoom']
        );
    }
    return $return_mall;
  }

  public function getLatestShops($filters){
    $this->load->model('malls/shop');
    $shops_array = $this->model_malls_shop->getShopsAPI($filters);
    foreach($shops_array as $id => $shop){
        $return_shop[] = array (
          'shop_id' => $id,
          'shop_title' => $shop['name'],
          'image_url' => $this->model_tool_image->resize($shop['image'],$this->config_image->get('shops_brands_designer', 'listing', 'width'), $this->config_image->get('shops_brands_designer', 'listing', 'hieght')),
          'latitude' => $shop['latitude'],
          'longitude' => $shop['longitude'],
          'zoom' =>$shop['zoom']
        );
    }
    return $return_shop;
  }

  public function getLatestLooks($filters){
    $this->load->model('look/look');
    $looks_array = $this->model_look_look->getLooks($filters);
    foreach($looks_array as $id => $look){
        $return_look[] = array (
          'look_id' => $look['look_id'],
          'look_title' => $look['title'],
          'image_url' => $this->model_tool_image->resize($look['image'],$this->config_image->get('looks', 'listing', 'width'), $this->config_image->get('looks', 'listing', 'hieght'))
        );
    }
    return $return_look;
  }

  public function getLatestTips($filters){
    $this->load->model('tips/tips');
    $tips_array = $this->model_tips_tips->getTips($filters);
    foreach($tips_array as $id => $tip){
        $return_tip[] = array (
          'tip_id' => $tip['tip_id'],
          'look_title' => $tip['name'],
          'image_url' => $this->model_tool_image->resize($tip['image'],$this->config_image->get('tips', 'details', 'width'), $this->config_image->get('tips', 'details', 'hieght')),
        );
    }
    return $return_tip;
  }

}
