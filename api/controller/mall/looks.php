<?php

class ControllerMallLooks extends Controller{

  public function getlooks(){

    $this->load->model("look/look");
    $this->load->model('tool/image');

    $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

    $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
    $language_id = 2;
    if ($language == 'en'){
      $language_id=1;
    }
    $this->config->set('config_language_id',$language_id);

    $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
    $country_code = 'sa';
    if ($country == 'ae'){
      $country_code = 'ae';
    }
    $this->country->set($country_code);

    if (isset($this->request->get['filter_group_id'])) {
        $filter_group_id = $this->request->get['filter_group_id'];
    } else {
        $filter_group_id = '';
    }

    if (isset($this->request->get['order'])) {
        $order = $this->request->get['order'];
    } else {
        $order = 'DESC';
    }

    if (isset($this->request->get['limit'])) {
        $limit = (int) $this->request->get['limit'];
    } else {
        $limit = 8;
    }

    $filter_data = array(
      'filter_group_id',$filter_group_id,
      'order' => $order,
      'limit' => $limit
    );

    $results = $this->model_look_look->getLooks($filter_data);

    foreach ($results as $key => $result){
      $looks_array[] = array (
      'image' => $this->model_tool_image->resize($result['image'],$this->config_image->get('looks', 'listing', 'width'), $this->config_image->get('looks', 'listing', 'hieght')),
      'look_id' => $result['look_id'],
      'title' => $result['title'],
      'group_id' => $result['group_id']
      );
    }
    $return_array = array (
      'looks_array' => $looks_array,
      'filters_array' => $filter_data
    );

    echo json_encode($return_array);

  }


  public function getlookDetails(){

    $this->load->model("look/look");
    $this->load->model('tool/image');

      if (isset($this->request->get['look_id'])) {
          $look_id = $this->request->get['look_id'];
      } else {
          $look_id = 0;
      }

      $access_token = isset($this->request->server['HTTP_TOKEN']) ? $this->request->server['HTTP_TOKEN'] : '';

      $language = (isset($this->request->server['HTTP_LANG'])?$this->request->server['HTTP_LANG']:"1");
      $language_id = 2;
      if ($language == 'en'){
        $language_id=1;
      }
      $this->config->set('config_language_id',$language_id);

      $country = (isset($this->request->server['HTTP_COUNTRY'])?$this->request->server['HTTP_COUNTRY']:"sa");
      $country_code = 'sa';
      if ($country == 'ae'){
        $country_code = 'ae';
      }
      $this->country->set($country_code);


      $look_data = $this->model_look_look->getlook($look_id);

      $look_products = $this->model_look_look->getLookProducts($look_id);
      foreach($look_products as $id => $product) {
        $related_products [] = array (
        'product_id' => $product['product_id'],
        'name' => $product['name'],
        'image_url' => $this->model_tool_image->resize($product['image'],$this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght')),
        );
      }

      $other_looks = $this->model_look_look->getRelatedLooks($look_id,array('start' => 0,'limit'=>4));
      foreach ($other_looks as $k => $look){
        $related_looks[] = array (
          'look_id' => $look['look_id'],
          'title' => $look['title'],
          'image_url' => $this->model_tool_image->resize($product['image'],$this->config_image->get('looks', 'listing', 'width'), $this->config_image->get('looks', 'listing', 'hieght')),
        );
      }

      $return_array = array(
        'tip_id' => $look_data['look_id'],
        'title' => $look_data['title'],
        'description' => $look_data['description'],
        'date_added' => $look_data['date_added'],
        'image_url' => $this->model_tool_image->resize($look_data['image'],$this->config_image->get('looks', 'details', 'width'), $this->config_image->get('looks', 'details', 'hieght')),
        'related_products' => $related_products,
        'related_looks' =>$related_looks
      );
      echo json_encode($return_array);

  }

}
