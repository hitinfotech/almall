<?php

class ControllerSayidatyFamous extends Controller {

    public function index() {
        echo json_encode("Welcome in SayidatyMall");
    }

    public function look() {
        header('Content-Type: application/json');
        
        if (isset($this->request->get['look_id'])) {
            $look_id = $this->request->get['look_id'];
        } else {
            $look_id = '';
        }

        if (isset($this->request->get['mall_id'])) {
            $mall_id = $this->request->get['mall_id'];
        } else {
            $mall_id = '';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int) $this->request->get['limit'];
        } else {
            $limit = 3;
        }

        $filter_data = array(
            'filter_look_id' => $look_id,
            'filter_mall_id' => $mall_id,
            'sort' => 'l.date_added',
            'order' => $order,
            'limit' => $limit
        );

        $data = array();

        $this->load->model('look/look');

        $results = $this->model_look_look->getLooks($filter_data);

        $this->load->model('tool/image');

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config_image->get('looks', 'listing', 'width'), $this->config_image->get('looks', 'listing', 'hieght'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('looks', 'listing', 'width'), $this->config_image->get('looks', 'listing', 'hieght'));
            }

            // $products = $this->model_look_look->getLookProducts($result['look_id']);

            /* $products_info = array();

              $count = 0;
              foreach ($products as $product) {
              if ($count < 3) {
              if ($product['image']) {
              $product_image = $this->model_tool_image->resize($product['image'], $this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght'));
              } else {
              $product_image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght'));
              }

              $products_info[] = array(
              'name' => utf8_substr(strip_tags(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8')), 0, 40) . "...",
              'price' => $product['price'],
              'thumb' => $product_image,
              'href' => $this->url->link('product/product', '&product_id=' . $product['product_id'])
              );
              }
              $count++;
              } */

            $data['looks'][] = array(
                'look_id' => $result['look_id'],
                'thumb' => $image,
                'title' => $result['title'],
                'date' => $result['date_added'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0),
                'href' => $this->url->link('look/details', '&look_id=' . $result['look_id'] , 'SSL'),
                    //'products' => $products_info
            );
        }

        echo json_encode($data);
    }

}
