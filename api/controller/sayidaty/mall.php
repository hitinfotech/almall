<?php

class ControllerSayidatyMall extends Controller
{

    public function index()
    {
        $client = new \AlgoliaSearch\Client(ALGOLIA_APPID, ALGOLIA_SEARCH_API_KEY);

        $images = ['zooming', 'details', 'listing', 'thumbnail', 'bundle', 'config_image_cart', 'config_image_compare',
            'mobileapp_s', 'mobileapp_l', 'tablet_s', 'tablet_l', 'ratio'];

        $SEARCHFOR = isset($this->request->post['SEARCHFOR']) ? $this->request->post['SEARCHFOR'] : '';
        $image_size = (isset($this->request->post['IMAGESIZE']) && in_array($this->request->post['IMAGESIZE'], $images)) ? $this->request->post['IMAGESIZE'] : 'listing';
        $LIMIT = (int)isset($this->request->post['LIMIT']) ? $this->request->post['LIMIT'] : 20;
        $KEYWORD = isset($this->request->post['KEYWORD']) ? $this->request->post['KEYWORD'] : '';
        $FullList = isset($this->request->post['FullList']) ? $this->request->post['FullList'] : '';
        $catIds = explode(",", $SEARCHFOR);
        $ids = array_filter($catIds, 'ctype_digit');

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $this->url->link('product/');

        $filter_data = array(
            'source' => 'api',
            'sort' => 'p.algolia',
            'order' => 'DESC',
            'start' => 0,
            'limit' => $LIMIT
        );

        $search_name=[];
        $brand_ids=[];
        $product_array = array();

        if (isset($SEARCHFOR) && !empty($SEARCHFOR) && !empty($ids)) {
            $filter_data['filter_sub_category'] = implode(",", $ids);
            $filter_data['filter_category_id'] = implode(",", $ids);
            $search_name = $this->model_catalog_category->getCategorySearchName(!empty($filter_data['filter_sub_category'])? $filter_data['filter_sub_category'] : 0);
        }

        if (isset($KEYWORD) && !empty($KEYWORD)) {

            $index = $client->initIndex($this->country->getcode());

            $lang = $this->config->get('config_languege_id') == 1 ? 'en' : 'ar'; // 1 = en , 2 = ar

            $res = $index->search($KEYWORD, ['hitsPerPage' => $LIMIT,'facetFilters' => '["show_api:yes"]']);

            $aRandomNumbers = $this->getRandoms(count($res['hits'])-1);
            foreach ($res['hits'] as $key => $rows) {
                if (in_array($key, $aRandomNumbers)) {
                    $ids[] = $rows['objectID'];

                    $product_array [] = array(
                        'product' => $rows['name'][$lang],
                        'brand' => $rows['brand']['name'][$lang],
                        'brand_link' => 'https://mall.sayidaty.net/' . $rows['brand']['url'][$lang],
                        'price' => !empty($rows['specials'][$this->country->getcode()]) ? $rows['specials'][$this->country->getcode()] : $rows['prices'][$this->country->getcode()],
                        'old_price' => empty($rows['specials'][$this->country->getcode()]) ? '' : $rows['prices'][$this->country->getcode()],
                        'image_path' => HTTPS_IMAGE_S3 . $rows['image'],
                        'link' => $this->url->link('product/product', 'product_id=' . $rows['objectID'], 'SSL')
                    );
                }
            }
        }else{

            $products = $this->model_catalog_product->getProductsForAPI($filter_data);

            $brand_ids = array();
            $aRandomNumbers = $this->getRandoms(count($products)-1);
            foreach ($products as $key => $product) {
                if (in_array($key, $aRandomNumbers)) {
                    $brand_ids[] = $product['brand_id'];
                    $product_array [] = array(
                        // 'product_id' => $product['product_id'],
                        'product' => $product['name'],
                        'brand' => $product['brand_name'],
                        'brand_link' => $this->url->link('search/category', '&b=' . urlencode($product['brand_name']) . "&"),
                        'price' => empty($product['special']) ? $product['price'] : $product['special'],
                        'old_price' => empty($product['special']) ? '' : $product['price'],
                        'image_path' => $this->model_tool_image->resize($product['image'], $this->config_image->get('product', $image_size, 'width'), $this->config_image->get('product', $image_size, 'hieght')),
                        'link' => $this->url->link('product/product', 'product_id=' . $product['product_id'])

                    );
                }
            }
        }

        // if result less than 4 complete to 4
        if (strtolower($FullList) !='no'){
            $size = sizeof($product_array);
            if ($size < 4) {
                $res = $this->model_catalog_product->getProductsForAPI($filter_data);
                $sizeRes = sizeof($res);
                if ($sizeRes <= 0) {
                    $products = $this->model_catalog_product->getProductsFromCatagory($filter_data);
                    $aRandomNumbers = $this->getRandoms(count($products)-1);
                    foreach ($products as $key => $product) {
                        if (in_array($key, $aRandomNumbers)) {
                            if (in_array($product['product_id'], $ids) || in_array($product['brand_id'], $brand_ids))
                                continue;
                            $size++;
                            $product_array [] = array(
                                // 'product_id' => $product['product_id'],
                                'product' => $product['name'],
                                'brand' => $product['brand_name'],
                                'brand_link' => $this->url->link('search/category', '&b=' . urlencode($product['brand_name']) . "&"),
                                'price' => empty($product['special']) ? $product['price'] : $product['special'],
                                'old_price' => empty($product['special']) ? '' : $product['price'],
                                'image_path' => $this->model_tool_image->resize($product['image'], $this->config_image->get('product', $image_size, 'width'), $this->config_image->get('product', $image_size, 'hieght')),
                                'link' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
                            );
                            $brand_ids[] = $product['brand_id'];
                            if ($size == 4)
                                break;
                        }
                    }
                } else {
                    $aRandomNumbers = $this->getRandoms(count($res)-1);
                    foreach ($res as $key => $product) {
                        if (in_array($key, $aRandomNumbers)) {
                            //to not repeat data
                            if (in_array($product['product_id'], $ids) || in_array($product['brand_id'], $brand_ids))
                                continue;

                            $size++;
                            $product_array [] = array(
                                'product' => $product['name'],
                                'brand' => $product['brand_name'],
                                'brand_link' => $this->url->link('search/category', '&b=' . urlencode($product['brand_name']) . "&"),
                                'price' => empty($product['special']) ? $product['price'] : $product['special'],
                                'old_price' => empty($product['special']) ? '' : $product['price'],
                                'image_path' => $this->model_tool_image->resize($product['image'], $this->config_image->get('product', $image_size, 'width'), $this->config_image->get('product', $image_size, 'hieght')),
                                'link' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
                            );
                            $brand_ids[] = $product['brand_id'];
                            if ($size == 4)
                                break;
                        }
                    }
                }

                if (count($product_array) < 4) {
                    $http_referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
                    if (strpos($http_referer, 'www.sayidaty.net') !== false ||
                        strpos($http_referer, 'www.aljamila.com') !== false
                    ) {
                        $filter_data['filter_category_id'] = '2';
                    } elseif (strpos($http_referer, 'www.sayidy.net') !== false ||
                        strpos($http_referer, 'www.arrajol.com') !== false
                    ) {
                        $filter_data['filter_category_id'] = '1';
                    } elseif (strpos($http_referer, 'kitchen.sayidaty.net') !== false) {
                        $filter_data['filter_category_id'] = '4';
                    }

                    $filter_data['limit'] = 20;
                    $res = $this->model_catalog_product->getProductsFromCatagory($filter_data);
                    $aRandomNumbers = $this->getRandoms(count($res) - 1);

                    foreach ($res as $key => $product) {
                        if (in_array($key, $aRandomNumbers)) {
                            //to not repeat data
                            if (in_array($product['product_id'], $ids) || in_array($product['brand_id'], $brand_ids))
                                continue;

                            $size++;
                            $product_array [] = array(
                                'product' => $product['name'],
                                'brand' => $product['brand_name'],
                                'brand_link' => $this->url->link('search/category', '&b=' . urlencode($product['brand_name']) . "&"),
                                'price' => empty($product['special']) ? $product['price'] : $product['special'],
                                'old_price' => empty($product['special']) ? '' : $product['price'],
                                'image_path' => $this->model_tool_image->resize($product['image'], $this->config_image->get('product', $image_size, 'width'), $this->config_image->get('product', $image_size, 'hieght')),
                                'link' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
                            );
                            $brand_ids[] = $product['brand_id'];
                            if ($size == 4)
                                break;
                        }
                    }
                }
            }

        }
	if (!isset($search_name['name']))
		$search_name['name']="";
        $morelink = $this->url->link('search/category','sort='.$this->country->getcode().'&page=1&q='.$KEYWORD.'&c='.$search_name['name'],'SSL');

        $product_array['morelink'] = str_replace("&amp;","&",$morelink);

        echo json_encode($product_array, JSON_PRETTY_PRINT);
        return;
    }

    public function getRandoms($quantity)
    {
        $aRangeNumbers = range(0,$quantity);
        if (count($aRangeNumbers) >= 4)
            return array_rand($aRangeNumbers,4);
        else
            return array_rand($aRangeNumbers,count($aRangeNumbers));
    }

}