<?php

class ControllerSayidatyHomepage extends Controller {

    public function index() {
        echo json_encode("Welcome in SayidatyMall");
    }

    public function categories() {
        header('Content-Type: application/json');
        if ($this->config->get('config_language_id') == 2) {
            $result = array(
                'womenclothes' => array(
                    'url' => 'https://mall.sayidaty.net/products/%D8%A7%D9%84%D9%85%D9%86%D8%AA%D8%AC%D8%A7%D8%AA/%D8%AA%D8%B3%D9%88%D9%82-%D8%A3%D9%88%D9%86-%D9%84%D8%A7%D9%8A%D9%86?q=&c=%D9%85%D8%B1%D8%A3%D8%A9:%D9%85%D9%84%D8%A7%D8%A8%D8%B3%20%D9%86%D8%B3%D8%A7%D8%A6%D9%8A%D8%A9&b=&color=&path=&cpath=',
                    'name' => 'ملابس  نسائية'
                ),
                'womenbags' => array(
                    'url' => 'https://mall.sayidaty.net/products/%D8%A7%D9%84%D9%85%D9%86%D8%AA%D8%AC%D8%A7%D8%AA/%D8%AA%D8%B3%D9%88%D9%82-%D8%A3%D9%88%D9%86-%D9%84%D8%A7%D9%8A%D9%86?q=&c=%D9%85%D8%B1%D8%A3%D8%A9:%D8%B4%D9%86%D8%B7%20%D9%88%D8%AD%D9%82%D8%A7%D8%A6%D8%A8&b=&color=&path=&cpath=',
                    'name' => 'شنط وحقائب'),
                'womenaccessories' => array(
                    'url' => 'https://mall.sayidaty.net/products/%D8%A7%D9%84%D9%85%D9%86%D8%AA%D8%AC%D8%A7%D8%AA/%D8%AA%D8%B3%D9%88%D9%82-%D8%A3%D9%88%D9%86-%D9%84%D8%A7%D9%8A%D9%86?q=&c=%D9%85%D8%B1%D8%A3%D8%A9:%D9%85%D8%AC%D9%88%D9%87%D8%B1%D8%A7%D8%AA%20%D9%88%D8%B3%D8%A7%D8%B9%D8%A7%D8%AA&b=&color=&path=&cpath=',
                    'name' => 'مجوهرات وساعات'),
                'womenbeauty' => array(
                    'url' => 'https://mall.sayidaty.net/products/%D8%A7%D9%84%D9%85%D9%86%D8%AA%D8%AC%D8%A7%D8%AA/%D8%AA%D8%B3%D9%88%D9%82-%D8%A3%D9%88%D9%86-%D9%84%D8%A7%D9%8A%D9%86?q=&c=%D8%B1%D8%AC%D9%84:%D9%85%D9%84%D8%A7%D8%A8%D8%B3%20%D8%B1%D8%AC%D8%A7%D9%84%D9%8A%D8%A9&b=&color=&path=&cpath=',
                    'name' => 'تجميل')
            );
        } else {
            $result = array(
                'womenclothes' => array(
                    'url' => 'https://mall.sayidaty.net/en/products/%D8%A7%D9%84%D9%85%D9%86%D8%AA%D8%AC%D8%A7%D8%AA/%D8%AA%D8%B3%D9%88%D9%82-%D8%A3%D9%88%D9%86-%D9%84%D8%A7%D9%8A%D9%86?q=&c=Women:Clothing&b=&color=&path=&cpath=',  'name' => 'Clothing'
                ),
                'womenbags' => array(
                    'url' => 'https://mall.sayidaty.net/en/products/%D8%A7%D9%84%D9%85%D9%86%D8%AA%D8%AC%D8%A7%D8%AA/%D8%AA%D8%B3%D9%88%D9%82-%D8%A3%D9%88%D9%86-%D9%84%D8%A7%D9%8A%D9%86?q=&c=Women:Bags&b=&color=&path=&cpath=',
                    'name' => 'Bags'
                ),
                'womenaccessories' => array(
                    'url' => 'https://mall.sayidaty.net/en/products/%D8%A7%D9%84%D9%85%D9%86%D8%AA%D8%AC%D8%A7%D8%AA/%D8%AA%D8%B3%D9%88%D9%82-%D8%A3%D9%88%D9%86-%D9%84%D8%A7%D9%8A%D9%86?q=&c=Women:Jewellery%20and%20Watches&b=&color=&path=&cpath=',
                    'name' => 'Jewellery and Watches'
                ),
                'womenbeauty' => array(
                    'url' => 'https://mall.sayidaty.net/en/products/%D8%A7%D9%84%D9%85%D9%86%D8%AA%D8%AC%D8%A7%D8%AA/%D8%AA%D8%B3%D9%88%D9%82-%D8%A3%D9%88%D9%86-%D9%84%D8%A7%D9%8A%D9%86?q=&c=Women:beauty&b=&color=&path=&cpath=',
                    'name' => 'beauty'
                )
            );
        }
        echo json_encode($result);
    }

    public function look() {
        header('Content-Type: application/json');

        $this->load->model('look/look');

        if (isset($this->request->get['look_id'])) {
            $look_id = $this->request->get['look_id'];
        } else {
            $look_id = '';
        }
        if (isset($this->request->get['mall_id'])) {
            $mall_id = $this->request->get['mall_id'];
        } else {
            $mall_id = '';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int) $this->request->get['limit'];
        } else {
            $limit = 1;
        }

        $filter_data = array(
            'filter_look_id' => $look_id,
            'filter_mall_id' => $mall_id,
            'sort' => 'l.date_added',
            'order' => $order,
            'limit' => $limit
        );
        $results = $this->model_look_look->getLooks($filter_data);

        $this->load->model('tool/image');

        $data = array();

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config_image->get('looks', 'listing', 'width'), $this->config_image->get('looks', 'listing', 'hieght'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('looks', 'listing', 'width'), $this->config_image->get('looks', 'listing', 'hieght'));
            }

            $products = $this->model_look_look->getLookProducts($result['look_id']);
            $products_info = array();

            $count = 0;
            foreach ($products as $product) {
                if ($count < 3) {
                    if ($product['image']) {
                        $product_image = $this->model_tool_image->resize($product['image'], $this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght'));
                    } else {
                        $product_image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght'));
                    }

                    $products_info[] = array(
                        'name' => utf8_substr(strip_tags(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8')), 0, 40) . "...",
                        'price' => $product['price'],
                        'thumb' => $product_image,
                        'href' => $this->url->link('product/product', '&product_id=' . $product['product_id'])
                    );
                }
                $count++;
            }

            $data['looks'][] = array(
                'look_id' => $result['look_id'],
                'thumb' => $image,
                'title' => $result['title'],
                'date' => $result['date_added'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0),
                'href' => $this->url->link('look/details', 'look_id=' . $result['look_id'], 'SSL'),
                'products' => $products_info
            );
        }

        echo json_encode($data);
    }

    public function tips() {
        header('Content-Type: application/json');

        $this->load->model('tips/tips');

        $this->load->model('tool/image');

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 't.date_added';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int) $this->request->get['limit'];
        } else {
            $limit = 2;
        }
        $filter_data = array(
            'sort' => $sort,
            'order' => $order,
            'limit' => $limit
        );

        $results = $this->model_tips_tips->getTips($filter_data);

        $data = array();
        $data['tips'] = array();

        foreach ($results as $result) {

            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config_image->get('tips', 'details', 'width'), $this->config_image->get('tips', 'details', 'hieght'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('tips', 'details', 'width'), $this->config_image->get('tips', 'details', 'hieght'));
            }

            $data['tips'][] = array(
                'tip_id' => $result['tip_id'],
                'thumb' => $image,
                'name' => $result['name'],
                'date' => date("d-m-Y", strtotime($result['date_added'])),
                'description' => utf8_substr(strip_tags(html_entity_decode($result['body'], ENT_QUOTES, 'UTF-8')), 0, 250) . " ... ",
                'href' => $this->url->link('tips/details', '&tip_id=' . $result['tip_id'], 'SSL')
            );
        }

        echo json_encode($data);
    }

    public function brands() {
        header('Content-Type: application/json');

        $this->load->model('brand/brand');
        $this->load->model('tool/image');

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'b.sort_order';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int) $this->request->get['limit'];
        } else {
            $limit = 2;
        }


        $results = $this->model_brand_brand->getApiBrands();

        $data['brands'] = array();

        $this->load->model('catalog/product');

        foreach ($results as $result) {

            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config_image->get('shops_brands_designer', 'listing', 'width'), $this->config_image->get('shops_brands_designer', 'listing', 'hieght'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('shops_brands_designer', 'listing', 'width'), $this->config_image->get('shops_brands_designer', 'listing', 'hieght'));
            }

            $filter_products = array(
                'filter_brand_id' => $result['brand_id'],
                'is_shopable' => 1,
                'sort' => 'p.date_added',
                'order' => 'DESC',
                'start' => ($page - 1) * $limit,
                'limit' => 3
            );

            $products = $this->model_catalog_product->getBrandProducts($filter_products);

            $products_info = array();

            foreach ($products as $product) {
                if ($product['image']) {
                    $product_image = $this->model_tool_image->resize($product['image'], $this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght'));
                } else {
                    $product_image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('product', 'listing', 'width'), $this->config_image->get('product', 'listing', 'hieght'));
                }

                $products_info[] = array(
                    'product_id' => $product['product_id'],
                    'name' => utf8_substr(strip_tags(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8')), 0, 40) . "...",
                    'price' => $product['price'],
                    'thumb' => $product_image,
                    'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
                );
            }

            $data['brands'][] = array(
                'brand_id' => $result['brand_id'],
                'thumb' => $image,
                'name' => $result['name'],
                'href' => $this->url->link('search/category', '&b=' . urlencode($result['name'])."&", 'SSL'),
                'products' => $products_info
            );
        }

        echo json_encode($data);
    }

}
