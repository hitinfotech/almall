<?php

class ControllerSayidatyProductsLanding extends Controller {

    public function index() {
        $res = $this->db->query("SELECT page_id FROM page_settings WHERE country_id = '" . (int) $this->country->getid() . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "' AND code='ramadan' order by page_id DESC limit 1");
        $page_id = 0;
        if ($res->num_rows) {
            $page_id = $res->row['page_id'];
        }

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_land` WHERE page_id='" . (int) $page_id . "'");
        $setting = array();

        foreach ($query->rows as $row) {
            if (!$row['serialized']) {
                $setting[$row['key']] = $row['value'];
            } else {
                $setting[$row['key']] = json_decode($row['value']);
            }
        }

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        $additional_products = array();
        $products = array();
        foreach ($setting as $key => $value) {

            if (preg_match('/ramadan_additional_/', $key)) {
                $additional_bloacks[$key] = $value;
            }
        }

        $firstkey = false;
        if (!empty($additional_bloacks)) {
            $additional_products_array = $this->model_catalog_product->getfeaturesAdditionalProducts('ramadan', $page_id);

            foreach ($additional_products_array as $key => $prod) {
                if (!$firstkey) {
                    $firstkey = $additional_bloacks['ramadan_additional_block' . $prod['block_id'] . '_title' . $prod['block_id']];
                }

                if ($firstkey == $additional_bloacks['ramadan_additional_block' . $prod['block_id'] . '_title' . $prod['block_id']]) {
                    if (isset($additional_bloacks['ramadan_additional_block' . $prod['block_id'] . '_title' . $prod['block_id']])) {
                        $additional_products[$prod['block_id']]['title'] = $additional_bloacks['ramadan_additional_block' . $prod['block_id'] . '_title' . $prod['block_id']];
                        $additional_products[$prod['block_id']]['action'] = $additional_bloacks['ramadan_additional_block' . $prod['block_id'] . '_action' . $prod['block_id']];
                    }
                    $tmp = $this->model_catalog_product->getProduct($prod['product_id']);
                    $categories = $this->model_catalog_product->getCategories($prod['product_id']);
                    $category_id = 0;
                    foreach ($categories as $row) {
                        if ($row['category_id']) {
                            $category_id = $row['category_id'];
                        }
                    }

                    $price = $this->currency->format($this->tax->calculate($tmp['price'], $tmp['tax_class_id'], $this->config->get('config_tax')));

                    if ((float) $tmp['special']) {
                        $newprice = $this->currency->format($this->tax->calculate($tmp['special'], $tmp['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $price = false;
                        $newprice = $this->currency->format($this->tax->calculate($tmp['price'], $tmp['tax_class_id'], $this->config->get('config_tax')));
                    }

                    $products[] = array(
                        'product_id' => $tmp['product_id'],
                        'product_name' => $tmp['name'],
                        'product_description' =>$tmp['description'],
                        'product_link' => $this->url->link('product/product', 'product_id=' . $tmp['product_id'] . '&utm_source=sayidaty&utm_medium=landingpage&utm_campaign=ksanationalday', 'SSL'),
                        'product_image' => $this->model_tool_image->resize($tmp['image'], $this->config_image->get('product', 'details', 'width'), $this->config_image->get('product', 'details', 'hieght')),
                        'old_price' => $price,
                        'new_price' => $newprice,
                        'category_id' => $category_id,
                        'brand_name' => $tmp['brand_name'],
                        'brand_link' => $this->url->link('search/category', '&utm_source=sayidaty&utm_medium=landingpage&utm_campaign=ksanationalday&b='. $tmp['brand_name'], 'SSL'),
                    );
                }
            }
        }

        echo json_encode($products, JSON_PRETTY_PRINT);
        return 1;
    }

}
