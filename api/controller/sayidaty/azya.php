<?php

class ControllerSayidatyAzya extends Controller {

    public function index() {
        echo json_encode("Welcome in SayidatyMall");
    }

    public function products() {
        header('Content-Type: application/json');

        $this->load->model('catalog/product');
        $limit = 4;

        if (isset($this->request->get['category_id'])) {
            $filter_category_id = $this->request->get['category_id'];
        } else {
            $filter_category_id = null;
        }

        if ($filter_category_id) {
            $filter_data = array(
                'filter_category_id' => $filter_category_id,
                'filter_sub_category' => $filter_category_id,
                'filter_stock_status_id' => 1,
                'limit' => $limit
            );
            $results = $this->model_catalog_product->getProducts($filter_data);
        } else {
            $results = $this->model_catalog_product->getCustLatestProducts2($limit);
        }

        $this->load->model('tool/image');

        $data['products'] = array();
        $count = 0;
        foreach ($results as $result) {

            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config_image->get('product', 'zooming', 'width'), $this->config_image->get('product', 'zooming', 'hieght'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('product', 'zooming', 'width'), $this->config_image->get('product', 'zooming', 'hieght'));
            }

            if ($result['brand_logo']) {
                $brand_logo = $this->model_tool_image->resize($result['brand_logo'], $this->config_image->get('shops_brands_designer', 'listing', 'width'), $this->config_image->get('shops_brands_designer', 'listing', 'hieght'));
            } else {
                $brand_logo = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('shops_brands_designer', 'listing', 'width'), $this->config_image->get('shops_brands_designer', 'listing', 'hieght'));
            }
            if ($count < 4) {
                $data['products'][] = array(
                    'name' => utf8_substr(strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')), 0, 40) . "...",
                    'price' => $result['price'],
                    'thumb' => $image,
                    'brand_logo' => $brand_logo,
                    'brand_href' => $this->url->link('search/category', '&b=' . urlencode($result['name'])."&"),
                    'brand' => $result['brand_id'],
                    'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                );
            } $count++;
        }
        echo json_encode($data);
    }

    public function look() {

        header('Content-Type: application/json');

        $this->load->model('look/look');

        if (isset($this->request->get['look_id'])) {
            $look_id = $this->request->get['look_id'];
        } else {
            $look_id = '';
        }

        if (isset($this->request->get['mall_id'])) {
            $mall_id = $this->request->get['mall_id'];
        } else {
            $mall_id = '';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int) $this->request->get['limit'];
        } else {
            $limit = 1;
        }

        $data = array();

        $filter_data = array(
            'filter_look_id' => $look_id,
            'filter_mall_id' => $mall_id,
            'filter_group_id' => 2,
            'sort' => 'l.date_added',
            'order' => $order,
            'limit' => $limit
        );
        $results = $this->model_look_look->getLooks($filter_data);

        $this->load->model('tool/image');
        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config_image->get('looks', 'listing', 'width'), $this->config_image->get('looks', 'listing', 'hieght'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('looks', 'listing', 'width'), $this->config_image->get('looks', 'listing', 'hieght'));
            }

            $data['looks'][] = array(
                'look_id' => $result['look_id'],
                'thumb' => $image,
                'title' => $result['title'],
                'date' => $result['date_added'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0),
                'href' => $this->url->link('look/details', '&look_id=' . $result['look_id'], 'SSL')
            );
        }

        echo json_encode($data);
    }

}
