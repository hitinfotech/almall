<?php

class ControllerBeautyCategories extends Controller {


    public function index()
    {
        $this->load->model('catalog/category');

        header('Content-Type: application/json');

        $limit = intval(isset($this->request->get['limit'])?$this->request->get['limit']:20)?:20;
        $page = intval(isset($this->request->get['page'])?$this->request->get['page']:1)?:1;
        $from = ($page - 1) * $limit;

        $path = trim(strval(isset($this->request->get['path'])?$this->request->get['path']:'')?:'', '/');
        $segments = explode('/', $path);
        $segment_category = isset($segments[0])?$segments[0]:0;

        if($segment_category) {
            $items = $this->model_catalog_category->getList($from, $limit, [
                'category_id'   => $segment_category
            ], false);

            echo json_encode(end($items->rows));
            return;
        }else{
            $items = $this->model_catalog_category->getPathList($from, $limit);
        }

        $pagination = array(
            'current_page'  => $page,
            'data'          => array(),
            'from'          => $from,
            'to'            => $from + $limit,
            'per_page'      => $limit,
            'total'         => $items->total
        );
        foreach ($items->rows as $item) {
            $pagination['data'][] = $item;
        }

        echo json_encode($pagination);
    }

}