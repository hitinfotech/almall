<?php

class ControllerBeautyProducts extends Controller {

    public function index() {

        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        header('Content-Type: application/json');

        $limit = intval(isset($this->request->get['limit']) ? $this->request->get['limit'] : 20)? : 20;

        $page = intval(isset($this->request->get['page']) ? $this->request->get['page'] : 1)? : 1;
        $from = ($page - 1) * $limit;

        $path = trim(strval(isset($this->request->get['path']) ? $this->request->get['path'] : '')? : '', '/');

        $segments = explode('/', $path);
        $segment_category = isset($segments[0]) ? $segments[0] : 0;
        $segment_category = $this->model_catalog_category->getIgnoredCategoryId($segment_category);

        $data = array(
            'filter_category_id' => $segment_category,
            'start' => $from,
            'limit' => $limit
        );
        $products = $this->model_catalog_product->getProducts($data);
        $total_products = $this->model_catalog_product->getTotalProducts($data);
        $pagination = array(
            'current_page' => $page,
            'data' => $products,
            'from' => $from,
            'to' => $from + $limit,
            'per_page' => $limit,
            'total' => $total_products
        );

        echo json_encode($pagination);
    }

}
