<?php

/**
 * Created by PhpStorm.
 * User: Mahmoud
 * Date: 4/27/16
 * Time: 9:45 AM
 */
class ControllerErrorNotFound extends Controller
{
    public function index()
    {
        header('Content-Type: application/json');

        echo json_encode([
            'error' => 404
        ]);
    }
}