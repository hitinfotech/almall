<?php

class ControllerCommonSeoUrl extends Controller {

    private $landing_pages_routes = array(
        'common/howtosell',
        'brands/brands',
        'brands/brand',
        'account/login',
        'account/register',
        'account/logout',
        'account/forgotten',
        'account/wishlist',
        'account/password',
        'account/address',
        'account/address/add',
        'account/address/edit',
        'account/success',
        'account/language',
        'account/notifications',
        'account/rma/rma',
        'account/order',
        'account/newsletter',
        'look/looks',
        'tips/tips',
        'total/coupon/coupon',
        'checkout/success',
        'malls/malls',
        'ticketsystem/generatetickets',
        'malls/news',
        'malls/shops',
        'search/product',
        'search/shopable',
        'search/category',
        'search/landing_pages',
        'search/ramadan',
        'search/search',
        'search/offer',
        'search/mother_day',
        'product/offer',
        'module/productbundles/listing',
        'tags/tags',
        'checkout/cart',
        'checkout/checkout',
        'checkout/checkout/confirm',
        'checkout/checkout/country',
        'toppages/toppages',
        'toppages/details',
        'shoppingsurvey/shoppingsurvey'
    );
    private $exceptions = array('sign', 'seller', 'bundles', 'نصائح التسوق', 'shopping-tips', 'الماركات', 'looks', 'products', 'brands', 'has_tags', 'malls', 'المولات', 'shops', 'الأخبار', 'news', 'offers-promotions', 'pages', 'top5');
    private $exceptions_ids = array('tip_id', 'partner_id', 'bundle_id', 'tag_id', 'look_id', 'product_id', 'category_id', 'brand_id', 'mall_id', 'shop_id', 'news_id', 'offer_id', 'information_id', 'manufacturer_id', 'top_pages_id', 'return_id', 'page_id');

    public function index() {
        // Add rewrite to url class
        if ($this->config->get('config_seo_url')) {
            $this->url->addRewrite($this);
        }

        // Decode URL
        if (isset($this->request->get['route'])) {
            $parts = explode('/', $this->request->get['route']);
            if (is_array($parts)) {
                $route = "/";
                foreach ($parts as $row) {
                    if (is_numeric($row)) {
                        $this->request->get['category_id'] = $row;
                    } else {
                        $route .= $row . "/";
                    }
                }
                $route = rtrim($route, "/");
                return new Action($route);
            }

            $parts = explode('/', $this->request->get['route']);


            // remove any empty arrays from trailing
            if (utf8_strlen(end($parts)) == 0) {
                array_pop($parts);
            }

            foreach ($parts as $part) {

                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_" . $this->language->get('code') . " WHERE keyword = '" . $this->db->escape($part) . "'");

                if ($query->num_rows) {
                    $url = explode('=', $query->row['query']);

                    if ($url[0] == 'product_id') {
                        $this->request->get['product_id'] = $url[1];
                    }

                    if ($url[0] == 'category_id') {
                        if (!isset($this->request->get['path'])) {
                            $this->request->get['path'] = $url[1];
                        } else {
                            $this->request->get['path'] .= '_' . $url[1];
                        }
                    }

                    if ($url[0] == 'manufacturer_id') {
                        $this->request->get['manufacturer_id'] = $url[1];
                    }

                    if ($url[0] == 'information_id') {
                        $this->request->get['information_id'] = $url[1];
                    }

                    if ($query->row['query'] && $url[0] != 'information_id' && $url[0] != 'manufacturer_id' && $url[0] != 'category_id' && $url[0] != 'product_id') {
                        $this->request->get['route'] = $query->row['query'];
                    }
                } else {
                    $this->request->get['route'] = 'error/not_found';

                    break;
                }
            }

            if (!isset($this->request->get['route'])) {
                if (isset($this->request->get['product_id'])) {
                    $this->request->get['route'] = 'product/product';
                } elseif (isset($this->request->get['path'])) {
                    $this->request->get['route'] = 'product/category';
                } elseif (isset($this->request->get['manufacturer_id'])) {
                    $this->request->get['route'] = 'product/manufacturer/info';
                } elseif (isset($this->request->get['information_id'])) {
                    $this->request->get['route'] = 'information/information';
                }
            }

            if (isset($this->request->get['route'])) {
                return new Action($this->request->get['route']);
            }
        }
    }

    public function rewrite($link) {
        $url_info = parse_url(str_replace('&amp;', '&', $link));

        $url = '';
        $language_code = ($this->language->get('code') == "ar" ? "ar" : "en");
        $table_name = "url_alias_" . ($language_code == "en" ? "en" : "ar");
        $data = array();

        parse_str($url_info['query'], $data);

        foreach ($data as $key => $value) {
            if (isset($data['route'])) {
                if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . "  WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'toppages/toppages')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . " WHERE `query` = 'toppages/toppages'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'toppages/details' && $key == 'top_pages_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . "  WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'malls/mall' && $key == 'mall_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . "  WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'customerpartner/profile' && $key == 'id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_partner WHERE `query` = 'partner_" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'account/customerpartner/addproduct' && $key == 'product_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_partner WHERE `query` = 'account/customerpartner/addproduct'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'] . '?product_id=' . $value;

                        unset($data[$key]);
                    }
                } elseif ($data['route'] == 'search/category' && $key == 'path') {
                    $categories = explode('_', $value);
                    $last_idx = end($categories);

                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . "  WHERE `query` = 'category_id=" . (int) $last_idx . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . $language_code . '/' . $query->row['keyword'];
                    } else {
                        $url = '';

                        break;
                    }

                    unset($data[$key]);
                } elseif (($data['route'] == 'account/customerpartner/orderinfo' && $key == 'order_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_partner WHERE `query` = 'account/customerpartner/orderinfo'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'] . '?order_id=' . $value;

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'account/customerpartner/wk_mprma_manage/getForm' && $key == 'return_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_partner WHERE `query` = 'account/customerpartner/wk_mprma_manage/getForm'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'] . '?return_id=' . $value;

                        unset($data[$key]);
                    }
                } elseif (in_array($data['route'], array('account/customerpartner/dashboard', 'account/customerpartner/orderlist', 'account/customerpartner/transaction', 'account/customerpartner/productlist', 'account/customerpartner/wk_mprma_manage'))) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_partner WHERE `query` = '" . $this->db->escape($data['route']) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'feed/google_sitemap')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . "  WHERE `query` = 'feed/google_sitemap'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'tags/details' && $key == 'tag_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . "  WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'malls/shop' && $key == 'shop_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . "  WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'look/details' && $key == 'look_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . "  WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'tips/details' && $key == 'tip_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . "  WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'brands/brand' && $key == 'brand_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . "  WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                }elseif (($data['route'] == 'search/landing_pages' && $key == 'page_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX."page_settings  WHERE `page_id` = '" .(int) $value. "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (in_array($data['route'], $this->landing_pages_routes)) {

                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . $table_name . "  WHERE `query` = '" . $this->db->escape($data['route']) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                }
            }
        }

        if ($url) {
            unset($data['route']);

            $query = '';

            if ($data) {
                foreach ($data as $key => $value) {
                    $query .= '&' . rawurlencode((string) $key) . '=' . rawurlencode((string) $value);
                }

                if ($query) {
                    $query = '?' . str_replace('&', '&amp;', trim($query, '&'));
                }
            }

            return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
        } else {
            return $link;
        }
    }

}
