<?php

class ControllerGallariaShops extends Controller {

    public function index()
    {
        $this->load->model('malls/shop');

        header('Content-Type: application/json');

        $limit = intval(isset($this->request->get['limit']) ? $this->request->get['limit'] : 20)? : 20;
        $page = intval(isset($this->request->get['page']) ? $this->request->get['page'] : 1)? : 1;
        $from = ($page - 1) * $limit;
       $items = $this->model_malls_shop->getShops($from, $limit);

        $pagination = array(
            'current_page' => $page,
            'data' => $items->rows,
            'from' => $from,
            'to' => $from + $limit,
            'per_page' => $limit,
            'total' => $items->total
        );

        echo json_encode($pagination);
    }

}
