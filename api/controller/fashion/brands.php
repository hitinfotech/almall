<?php

class ControllerFashionBrands extends Controller {


    protected function getBrandDescriptions($brand_id)
    {
        $query = "SELECT %s FROM brand_description ";
        $query .= "WHERE brand_description.brand_id=%d";

        $db_query = $this->db->query(sprintf(
            $query, 'brand_description.*', $brand_id
        ));

        $names = array();
        foreach($db_query->rows as $for_item) {
            $key_suffix = ($for_item['language_id']==1)?'en':'ar';
            $names["name_{$key_suffix}"] = $for_item['name'];

            if($for_item['language_id'] == 2) {
                $names['describe'] = $for_item['description'];
            }
        }
        return $names;
    }

    protected function getBrands($from = null, $limit = null, $categories = [])
    {
        $query = "
          SELECT %s FROM brand
          LEFT JOIN brand_to_category ON brand_to_category.brand_id=brand.brand_id
          LEFT JOIN category ON category.category_id=brand_to_category.store_category_id
        ";

        if(! empty($categories)) {
            $query .= "WHERE category.category_id IN(" . implode(',', $categories) . ") ";
            $query .= " OR category.parent_id IN(" . implode(',', $categories) . ") ";
        }
        $db_total = $this->db->query(sprintf($query, 'COUNT(brand.brand_id) AS total'));

        if(isset($from) || isset($limit)) {
            $limit = $limit?:0; $query .= "LIMIT ";

            if(isset($from)) { $query .= "{$from}, "; }
            if($limit) { $query .= "{$limit} "; }
        }
        $db_query = $this->db->query(sprintf($query, 'brand.*'));

        $rows = array();
        foreach($db_query->rows as $for_item) {
            $for_item_desc = $this->getBrandDescriptions($for_item['brand_id']);

            $row['id'] = $for_item['brand_id'];
            $row['priority'] = $for_item['sort_order'];

            $row['link'] = $for_item['link'];
            $row['social_tw'] = $for_item['social_tw'];
            $row['social_fb'] = $for_item['social_fb'];

            $row['created_at'] = $for_item['date_added'];
            $row['updated_at'] = $for_item['date_modified'];


            $row['images'] = [];

            if($for_item['image']) {
                $row['images'][] = [
                    'src'   => $for_item['image'],
                    'url'   => "http://{$_SERVER['HTTP_HOST']}/image/{$for_item['image']}"

                ];
            }

            $row = array_merge($row, $for_item_desc);

            $rows[] = $row;
        }

        return (object) array(
            'rows'  => $rows,
            'total' => $db_total->row['total']
        );
    }

    public function index()
    {
        header('Content-Type: application/json');

        $limit = intval(isset($this->request->get['limit'])?$this->request->get['limit']:20)?:20;
        $page = intval(isset($this->request->get['page'])?$this->request->get['page']:1)?:1;
        $from = ($page - 1) * $limit;
        $items = $this->getBrands($from, $limit); // categories not exists [716, 714, 712, 710]


        $page_num = ceil($items->total / $limit);
        $page_prev = max($page - 1, 1);
        $page_next = min($page + 1, $page_num);

        $http_host = $_SERVER['HTTP_HOST'];
        $http_api_url = "http://{$http_host}/api/fashion/brands?limit={$limit}";

        $pagination = array(
            'current_page'  => $page,
            'data'          => $items->rows,
            'from'          => $from,
            'to'            => $from + $limit,
            'per_page'      => $limit,
            'total'         => $items->total,

            'prev_page_url' => "{$http_api_url}&page={$page_prev}",
            'next_page_url' => "{$http_api_url}&page={$page_next}",

            'last_page'     => "{$http_api_url}&page={$page_num}",
        );

        echo json_encode($pagination);
    }

}