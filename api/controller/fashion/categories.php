<?php

class ControllerFashionCategories extends Controller {


    public function index()
    {
        $this->load->model('catalog/category');

        header('Content-Type: application/json');

        $limit = intval(isset($this->request->get['limit'])?$this->request->get['limit']:20)?:20;
        $page = intval(isset($this->request->get['page'])?$this->request->get['page']:1)?:1;
        $from = ($page - 1) * $limit;
        $items = $this->model_catalog_category->getList($from, $limit);

        $page_num = ceil($items->total / $limit);
        $page_prev = max($page - 1, 1);
        $page_next = min($page + 1, $page_num);

        $http_host = $_SERVER['HTTP_HOST'];
        $http_api_url = "http://{$http_host}/api/fashion/categories?limit={$limit}";

        $pagination = array(
            'current_page'  => $page,
            'data'          => array(),
            'from'          => $from,
            'to'            => $from + $limit,
            'per_page'      => $limit,
            'total'         => $items->total,

            'prev_page_url' => "{$http_api_url}&page={$page_prev}",
            'next_page_url' => "{$http_api_url}&page={$page_next}",

            'last_page'     => "{$http_api_url}&page={$page_num}",
        );
        foreach ($items->rows as $item) {
            $pagination['data'][] = $item;
        }

        echo json_encode($pagination);
    }

}