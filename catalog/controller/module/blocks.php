<?php
class ControllerModuleBlocks extends Controller {
	public function index($setting) {
		$this->load->language('module/blocks');
		 
		$data['heading_title'] = $setting['name'];
		$data['sub_title'] = $setting['sub_title'];

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}
		$products= array();
		if($setting['product'])
		$products = array_slice($setting['product'], 0, (int)$setting['limit']);

		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}
				$special_per = 0;
				if ((float)$product_info['special']) {
					$special_per = '-'.(100-(100 * $product_info['special']) / $product_info['price']).'%';
				}
				$h = $m = $s = '';
				 
				/*if($special_per){
					$datetime1 = new DateTime();
					$datetime2 = new DateTime('2011-01-03 17:13:00');
					$interval = $datetime1->diff($datetime2);
					$h = $interval->format('%h');
					$m = $interval->format('%i');
					$s = $interval->format('%s');					
				}*/
				$data['products'][] = array(
					'product_id'  => $product_info['product_id'],
					'thumb'       => $image,
					'name'        => $product_info['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'special_per'     => $special_per,
					'tax'         => $tax,
					'rating'      => $rating,
					'h'      => $h,
					'm'      => $m,
					's'      => $s,
					'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
				);
			}
		}

		if (true || $data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/blocks.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/blocks.tpl', $data);
			} else {
				return $this->load->view('default/template/module/blocks.tpl', $data);
			}
		}
	}
}