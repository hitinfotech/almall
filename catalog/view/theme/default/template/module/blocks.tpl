<div class="row">
  <div class="col px-md-0">
    <h2 class="stuff-block__title-main"><?php echo $heading_title ?></h2>
    <?php if($sub_title) {?>
      <h3 class="stuff-block__title-hand"><?php echo $sub_title ?></h3>
    <?php } ?>
  </div>
</div>
<div class="row">
  <div class="col-12 col-md-6 pl-md-0 pr-md-2">
    <div class="stuff-block">
      <a href="#" class="stuff-block-parent-link">
        <div class="stuff-block__item">
          <h4 class="stuff-block__title">Sport collection</h4>
          <div href="#" class="stuff-block__link">Shop now
            <img src="catalog/view/javascript/home/assets/img/arrow.svg" width="17" height="9" class="arrow">
          </div>
          <img src="catalog/view/javascript/home/assets/img/arrivals-1.jpg" alt="image_desc" width="577" height="453" class="stuff-block__image">
        </div>
      </a>
      <a href="#" class="stuff-block-parent-link">
        <div class="stuff-block__item">
          <h4 class="stuff-block__title">Trendy Watches</h4>
          <p class="stuff-block__subtitle">For him</p>
          <div href="#" class="stuff-block__link">Shop now
            <img src="catalog/view/javascript/home/assets/img/arrow.svg" width="17" height="9" class="arrow"></div>
            <img src="catalog/view/javascript/home/assets/img/arrivals-2.jpg" alt="image_desc" width="577" height="453" class="stuff-block__image">
        </div>
      </a>
    </div>
  </div>
  <div class="col-12 col-md-6 pr-md-0 pl-md-2">
    <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
        <div class="stuff-block__item stuff-block__item_short">
          <h4 class="stuff-block__title">Sport collection</h4>
          <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/arrivals-3.jpg" alt="image_desc" width="577" height="297" class="stuff-block__image">
        </div></a><a href="#" class="stuff-block-parent-link">
        <div class="stuff-block__item stuff-block__item_short">
          <h4 class="stuff-block__title">Trendy Handbags20char</h4>
          <p class="stuff-block__subtitle">Suit all tastes</p>
          <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/arrivals-4.jpg" alt="image_desc" width="577" height="297" class="stuff-block__image">
        </div></a><a href="#" class="stuff-block-parent-link">
        <div class="stuff-block__item stuff-block__item_short">
          <h4 class="stuff-block__title">Shoes Collection</h4>
          <p class="stuff-block__subtitle">For her</p>
          <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/arrivals-5.jpg" alt="image_desc" width="577" height="297" class="stuff-block__image">
        </div></a></div>
  </div>
</div>