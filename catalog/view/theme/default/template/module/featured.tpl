<div class="row">
  <div class="col-12 px-md-0">
    <h2 class="product-block__title"><?php echo $heading_title; ?></h2><a href="#" target="_blank" class="product-block__all">See all</a>
  </div>
</div>
<div class="row">
  <div class="col-12 px-md-0">
    <div class="product-block clearfix">
      <div class="product-block-items owl-theme">
        <?php if($heading_title == "Deals of the Day") {?>
        <div class="product-block__item product-block__item_first">
            <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/deals-1.jpg" alt="image_desc" width="" height="" class="product-block__image_filter"></a></div>
            <p class="product-block__item-title">Deals up<br><span>to&nbsp;<b>70%</b>&nbsp;OFF!</span></p>
            <div class="product-block-info"><a href="#" class="product-block-info__link_title">SALE!</a><a href="#" class="product-block-info__link_subtitle">Deals up to 70% OFF!</a>
              <button class="btn product-block-info__btn_first-btn">Shop NOW</button>
            </div>
          </div>
        <?php } ?>
        <?php foreach ($products as $product) { ?>
          <div class="product-block__item">
            <div class="product-block__image">
              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="image_desc" width="" height=""></a>
            </div>
            <?php if($product['special_per']) { ?> <p class="product-block__discount-sum"><?php echo $product['special_per']; ?></p> <?php } ?>
            <?php if ($product['special'] && $product['s']) { ?> 
            <div class="product-block__timer">
              <p class="product-block__timer_h"><?php echo $product['h'] ?>&nbsp;<span>h :&nbsp;</span></p>
              <p class="product-block__timer_m"><?php echo $product['m'] ?>&nbsp;<span>m :&nbsp;</span></p>
              <p class="product-block__timer_s"><?php echo $product['s'] ?>&nbsp;<span>s</span></p>
            </div>
            <?php } ?>
            <div class="product-block-info">
              <a href="<?php echo $product['href']; ?>" class="product-block-info__link_title">
                <?php echo $product['name']; ?>                  
              </a>
              <!-- <a href="#" class="product-block-info__link_subtitle">Modest Pink Zigzag Abaya X</a> -->
              <?php if (!$product['special']) { ?>              
                <p class="product-block-info__price_new"><?php echo $product['price']; ?></p>
              <?php } else { ?>              
                <p class="product-block-info__price_new"><?php echo $product['special']; ?></p>
                <p class="product-block-info__price_old"><?php echo $product['price']; ?></p>
              <?php } ?>
              <button class="product-block-info__btn" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                <img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon">
                <img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
              </button>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>