 <div class="row">
  <div class="col px-md-0">
    <h2 class="product-block__title product-block__title_smaller"><?php echo $heading; ?></h2>
  </div>
</div>
<div class="row">
  <div class="col px-md-0">
    <div class="brands-slider owl-theme" id="carousel<?php echo $module; ?>">
      <?php foreach ($banners as $banner) { ?>
        <div class="brands-slider__item">
          <?php if ($banner['link']) { ?>
          <a href="<?php echo $banner['link']; ?>" class="brands-slider__link">            
            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="brands-slider__image">
          </a>
          <?php } else { ?>
          <a href="#" class="brands-slider__link">
            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="brands-slider__image">
          </a>
          <?php } ?>
        </div>    
      <?php } ?>      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
  mydir = $("html").attr("dir");

  if (mydir == 'rtl') {
    rtlVal=true
  }else {
    rtlVal=false
  }
  $('#carousel<?php echo $module; ?>').owlCarousel({
        rtl:rtlVal,
        loop: false,
        nav: true,
        margin: 7,
        responsive: {
            0: {
                items: 3
            },
            1000: {
                items: 6
            }
        }
    });
--></script>
