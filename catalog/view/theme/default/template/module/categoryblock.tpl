<div class="product-block__item product-block__item_first shop-menu_special">
  <div class="product-block__image"><a href="#"></a></div>
  <p class="product-block__item-title"><?php echo html_entity_decode(htmlspecialchars_decode($heading_title)) ?></p>
  <a href="<?php echo $link ?>" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow-black.svg" width="17" height="9" class="arrow"></a>
</div>