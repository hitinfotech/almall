<div class="row">
  <div class="col-12 px-0 ov-f-x-hide">
    <div class="slider">
      <div class="socials-icons"><a href="https://www.facebook.com/" target="_blank" class="socials-icons__link"><span class="fa fa-facebook"></span></a><a href="https://twitter.com/" target="_blank" class="socials-icons__link"><span class="fa fa-twitter"></span></a><a href="https://instagram.com/" target="_blank" class="socials-icons__link"><span class="fa fa-youtube"></span></a><a href="https://instagram.com/" target="_blank" class="socials-icons__link"><span class="fa fa-instagram"></span></a></div>
      <div class="owl-carousel owl-theme slideshow<?php echo $module; ?>">
        <?php 
        $total = count($banners);
        $i = 1;
        foreach ($banners as $banner) { ?>
           <div class="item">
            <div class="slider__title">
              <p class="slider__title_add">Hot & Trends for Summer</p>
              <p class="slider__title_main">Get the look</p>
              <p class="slider__title_add"> Men & Women</p>
            </div>
            <?php if ($banner['link']) { ?>
              <a href="<?php echo $banner['link']; ?>">
                <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>"  width="" height="" class="slider__image" />
                <button  class="btn slider__btn"><a href="<?php echo $banner['link']; ?>">Explore</a></button>
                <div class="slider__count">
                  <p></p><span>0<?php echo $i; ?></span>/0<?php echo $total; ?>
                </div>
              </a>
            <?php } else { ?>
              <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>"  width="" height="" class="slider__image" />              
              <button class="btn slider__btn">Explore</button>
              <div class="slider__count">
                <p></p><span>0<?php echo $i; ?></span>/0<?php echo $total; ?>
              </div>
            <?php  } ?>            
          </div>
        <?php $i++; } ?>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .slideshow<?php echo $module; ?>{box-shadow: none !important;}  
</style>
<script type="text/javascript"><!--
 mydir = $("html").attr("dir");
  if (mydir == 'rtl') {
      rtlVal=true
  }
  else {
      rtlVal=false
  }
$(".slideshow<?php echo $module; ?>").owlCarousel({
    rtl:rtlVal,
    items: 1,
    lazyLoad: true,
    margin: 30,
    stagePadding: 30,
    smartSpeed: 450
});
--></script>
