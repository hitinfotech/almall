<div class="shop-menu">
  <ul class="navbar-nav">
    <?php foreach ($categories as $category) { ?>      
      
       <?php if ($category['children']) { ?>
         <li class="nav-item <?php echo ($category['category_id'] == $category_id) ? 'active' : ''; ?> dropdown">
          <a id="navbarDropdownMenuLink" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><?php echo $category['name']; ?></span></a>
          <div aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu">
            <?php foreach ($category['children'] as $child) { ?>
              <a href="<?php echo $child['href']; ?>" class="dropdown-item"><?php echo $child['name']; ?></a>
            <?php } ?>
          </div>
        </li>
           <?php //if ($child['category_id'] == $child_id) { ?>
      <?php }else{ ?>
      <li class="nav-item <?php echo ($category['category_id'] == $category_id) ? 'active' : ''; ?>">
        <a href="<?php echo $category['href']; ?>" class="nav-link"><?php echo $category['name']; ?></a>
      </li>
    <?php } ?>
      
    <?php } ?>
  </ul>
</div><!-- 
<div class="list-group">
  <?php foreach ($categories as $category) { ?>
  <?php if ($category['category_id'] == $category_id) { ?>
  <a href="<?php echo $category['href']; ?>" class="list-group-item active"><?php echo $category['name']; ?></a>
  <?php if ($category['children']) { ?>
  <?php foreach ($category['children'] as $child) { ?>
  <?php if ($child['category_id'] == $child_id) { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } else { ?>
  <a href="<?php echo $child['href']; ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
  <?php } ?>
  <?php } ?>
  <?php } ?>
  <?php } else { ?>
  <a href="<?php echo $category['href']; ?>" class="list-group-item"><?php echo $category['name']; ?></a>
  <?php } ?>
  <?php } ?>
</div>
 -->