<div class="header-dropdown-block">
<?php if (count($languages) > 1) { ?>
<form id="form-language2" action="" method="post" enctype="multipart/form-data">
  <?php foreach ($languages as $language) { 
  if ($language['code'] == $code) { ?>
  <div class="header-dropdown-block__selected"><span><?php echo $language['name']; ?></span></div>
  <?php } else {?>
  <div class="header-dropdown-block"><a href="<?php echo $language['code']; ?>"><span><?php echo $language['name']; ?></span></a></div>
  <?php }} ?>
  <input type="hidden" name="code" value="">
  <input type="hidden" name="redirect" value="{{ redirect }}">
</form>
<?php }  ?>
</div>