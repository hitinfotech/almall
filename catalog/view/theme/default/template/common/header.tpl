<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<link href="catalog/view/javascript/home/assets/css/important.css" rel="stylesheet" media="screen" />
<?php if (!isset($this->request->get['route']) || isset($this->request->get['route']) && $this->request->get['route'] == 'common/home') { ?>
  <!-- <link href="catalog/view/javascript/home/assets/css/bootstrap.min.css" rel="stylesheet" media="screen" /> -->
  <script src="catalog/view/javascript/home/assets/js/jquery-3.3.1.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/home/assets/js/tether.min.js" type="text/javascript"></script>  
  <script src="catalog/view/javascript/home/assets/js/bootstrap.min.js" type="text/javascript"></script> 
  <?php if($direction == "rtl"){ ?>
  <link href="atalog/view/javascript/home/assets/css/bootstrap.rtl.css" rel="stylesheet" media="screen" />
  <link href="atalog/view/javascript/home/assets/css/bootstrap-grid.rtl.css" rel="stylesheet" media="screen" />    
<?php } } else{ ?>
  <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<?php }  ?>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<header>
  <div class="header-top-block">
    <div class="container">
      <div class="row">
        <div class="col-xl-8 col-lg-7 col-md-5 col-sm-5 col-4 mr-auto pl-md-0">
          <?php echo $country; ?>
          <?php echo $language; ?>
            <ul class="navbar-nav header-top-block__menu-left mtop8rem">
              <li class="nav-item header-top-block__item"><a href="#" class="nav-link">free shipping.</a></li>
              <li class="nav-item header-top-block__item"><a href="#" class="nav-link">free returns.</a></li>
              <li class="nav-item header-top-block__item"><a href="#" class="nav-link">cash on delivery.</a></li>
            </ul>
        </div>
        <div class="col-xl-4 col-lg-5 col-md-6 col-sm-7 col-6 ml-auto pr-md-0">
          <div class="float-right">
            <div class="header-dropdown-block">
              <div id="account-block" class="header-dropdown-block">
                <div class="header-dropdown-block__selected">
                  <!--i.fa.fa-user(aria-hidden='true')-->
                  <?php if ($logged) { ?>
                    <p>Welcome&nbsp;<span><?php echo $customer_name; ?></span></p>
                    <div class="header-dropdown-block__dropdown">
                      <ul>
                        <li class="header-dropdown-block__link"><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                        <li class="header-dropdown-block__link"><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                        <li class="header-dropdown-block__link"><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                        <li class="header-dropdown-block__link"><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                        <li class="header-dropdown-block__link"><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                      </ul>
                    </div>
                      <?php } else { ?>
                    <p>Account</p>
                    <div class="header-dropdown-block__dropdown">
                      <ul>
                        <li class="header-dropdown-block__link"><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                        <li class="header-dropdown-block__link"><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                      </ul>
                    </div>
                      <?php } ?>
                </div>
              </div>
            </div><a href="#" class="header-top-block__link">Sell with us</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="header-topbar">
      <div class="container px-0 mx-0 px-sm-3 mx-sm-auto px-xl-0 mx-xl-0">
        <div class="row">
          <div class="col-4 col-sm-4 col-md-3 col-lg-4 col-xl-2 mx-auto ml-xl-0 pl-0 order-2 order-xl-1">
            <div class="header-logo">
              <a href="<?php echo $home; ?>" class="header-logo__link">
                <img src="<?php echo $logo; ?>" width="110" height="22" class="header-logo__image">
              </a>
            </div>
          </div>
          <div class="col-2 col-sm-1 col-md-1 col-lg-1 col-xl-6 mx-xl-auto order-3 order-xl-2 pr-0 mr-0">
            <?php echo $search; ?>
          </div>
          <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 mr-xl-0 pr-xl-0 order-4 order-xl-3 px-0 mx-0 mr-3">
            <?php echo $cart; ?>          
          </div>
          <?php if ($categories) { ?>
          <div class="col-2 col-sm-3 col-md-1 col-lg-3 col-xl-12 ml-0 mx-xl-auto mr-auto px-xl-0 order-1 order-xl-4">
            <div class="menu-header-block">
              <nav class="navbar navbar-expand-xl">
                <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span class="navbar-toggler-icon"><img src="catalog/view/javascript/home/assets/img/ic-menu.svg" width="20" height="18"></span></button>
                <div id="navbarSupportedContent" class="collapse navbar-collapse">
                  <div class="header-logo-parent">
                    <div class="header-logo"><a href="#" class="header-logo__link"><img src="catalog/view/javascript/home/assets/img/almall-logo.png" srcset="catalog/view/javascript/home/assets/img/almall-logo@2x.png 2x, catalog/view/javascript/home/assets/img/almall-logo@3x.png 3x" width="110" height="22" class="header-logo__image"></a></div>
                    <button type="button" aria-label="Close" data-dismiss="modal" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="true" class="close"></button>
                  </div>
                  <div class="navbar-nav-scrolling">
                    <ul class="navbar-nav">
                      <li class="nav-item d-xl-none">
                        <a href="#" class="nav-link nav-link-back"><i class="icon-back d-none"></i>Categories</a>
                      </li>
                      <?php foreach ($categories as $category) { ?>        
                        <li class="nav-item">
                          <a href="<?php echo $category['href']; ?>" class="nav-link"><?php echo $category['name']; ?><i class="icon-forward d-xl-none"></i></a>
                          <div class="menu-header-block__dropdown">
                            <?php  $iii = 0;               
                            foreach ($category['children'] as $child) { ?>
                            <div class="menu-header-block__column-parent">
                              <?php if(!$iii) {?>
                              <h2 class="menu-header-block__title-back d-xl-none"><i class="icon-back d-none"></i><?php echo $category['name']; ?></h2>
                              <?php } ?>
                              <a href="<?php echo $child['href']; ?>"><h2 class="menu-header-block__title"><?php echo $child['name']; ?></h2></a>
                              <?php if ($child['children']) { 
                                foreach (array_chunk($child['children'], ceil(count($child['children']) / $child['column'])) as $children) { ?>
                                  <div class="menu-header-block__column">
                                    <ul class="navbar-nav">
                                      <?php foreach ($children as $key => $value) { ?>                        
                                      <li class="nav-item">
                                        <a href="<?php echo $value['href'] ?>" class="nav-link"><?php echo $value['name'] ?></a>
                                      </li>
                                      <?php } ?>
                                    </ul>
                                  </div>
                                <?php } 
                                } else{ ?>
                                  <div class="menu-header-block__column menu-header-block__column_short">
                                    <ul class="navbar-nav">
                                      <li class="nav-item"><a href="#" class="nav-link">View all</a></li>
                                    </ul>
                                  </div>
                              <?php } ?>
                            </div>                           
                          <?php $iii++; } ?>
                            <div class="menu-header-block__column-parent mr-0">
                              <h2 class="menu-header-block__title">Top Offers</h2>
                              <div class="stuff-block stuff-block-menu flex-column">
                                <div class="stuff-block__item mr-0">
                                  <h4 class="stuff-block__title">Sport collection</h4><a href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></a><img src="catalog/view/javascript/home/assets/img/arrivals-1.jpg" alt="image_desc" width="" height="" class="stuff-block__image">
                                </div>
                                <div class="stuff-block__item mr-0">
                                  <h4 class="stuff-block__title">Trendy Watches</h4>
                                  <p class="stuff-block__subtitle">For him</p><a href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></a><img src="catalog/view/javascript/home/assets/img/arrivals-2.jpg" alt="image_desc" width="" height="" class="stuff-block__image">
                                </div>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="menu-header-block__column-parent mb-0">
                              <h2 class="menu-header-block__title">Top Brands</h2>
                              <div class="menu-header-block-brands">
                                <div class="menu-header-block-brands__item"><a href="#" class="menu-header-block-brands__link"><img src="catalog/view/javascript/home/assets/img/brand1.png" alt="image_desc" width="" height="" class="menu-header-block-brands__image"></a></div>
                                <div class="menu-header-block-brands__item"><a href="#" class="menu-header-block-brands__link"><img src="catalog/view/javascript/home/assets/img/brand2.png" alt="image_desc" width="" height="" class="menu-header-block-brands__image"></a></div>
                                <div class="menu-header-block-brands__item"><a href="#" class="menu-header-block-brands__link"><img src="catalog/view/javascript/home/assets/img/brand3.png" alt="image_desc" width="" height="" class="menu-header-block-brands__image"></a></div>
                                <div class="menu-header-block-brands__item"><a href="#" class="menu-header-block-brands__link"><img src="catalog/view/javascript/home/assets/img/brand4.png" alt="image_desc" width="" height="" class="menu-header-block-brands__image"></a></div>
                                <div class="menu-header-block-brands__item"><a href="#" class="menu-header-block-brands__link"><img src="catalog/view/javascript/home/assets/img/brand5.png" alt="image_desc" width="" height="" class="menu-header-block-brands__image"></a></div>
                              </div>
                            </div>
                          </div>
                        </li>
                      <?php } ?>
                        <li class="nav-item nav-item-bold nav-item-hot"><a href="#" data-sale="Hot" class="nav-link">Deal of the day</a></li>
                    </ul>
                    <?php if ($logged) { ?>                    
                     <div class="header-dropdown-block">
                        <div id="account-block" class="header-dropdown-block">
                          <div class="header-dropdown-block__selected">
                            <p>Welcome&nbsp;<span><?php echo $customer_name; ?></span></p><i class="icon-forward"></i>
                            <div class="menu-header-block__dropdown">
                              <div class="menu-header-block__column-parent">
                                <h2 class="menu-header-block__title-back">Welcome <?php echo $customer_name; ?></h2>
                                <ul class="navbar-nav">
                                  <li class="nav-item"><a href="<?php echo $account; ?>" class="nav-link"><?php echo $text_account; ?></a></li>
                                  <li class="nav-item"><a href="<?php echo $order; ?>" class="nav-link"><?php echo $text_order; ?></a></li>
                                  <li class="nav-item"><a href="<?php echo $transaction; ?>" class="nav-link"><?php echo $text_transaction; ?></a></li>
                                  <li class="nav-item"><a href="<?php echo $download; ?>" class="nav-link"><?php echo $text_download; ?></a></li>
                                  <li class="nav-item"><a href="<?php echo $logout; ?>" class="nav-link"><?php echo $text_logout; ?></a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php } else { ?>
                      <div class="header-dropdown-block">
                          <div id="account-block" class="header-dropdown-block">
                            <div class="header-dropdown-block__selected">
                              <p><a href="<?php echo $login; ?>">Sign In</a> / <a href="<?php echo $register; ?>">Sign Up</a></p>
                            </div>
                          </div>
                        </div>
                    <?php } ?>
                    <?php echo $language_mob; ?>
                    <?php echo $country_mob; ?>
                    <a href="#" class="header-top-block__link">Sell with us</a>
                  </div>
                  <ul class="navbar-nav header-top-block__menu-left">
                      <li class="nav-item header-top-block__item"><a href="#" class="nav-link">free shipping.</a></li>
                      <li class="nav-item header-top-block__item"><a href="#" class="nav-link">free returns.</a></li>
                      <li class="nav-item header-top-block__item"><a href="#" class="nav-link">cash on delivery.</a></li>
                    </ul>
                </div>
              </nav>
              <div aria-label="Close" data-dismiss="modal" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="true" class="overlay"></div>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header> 
<?php } ?>
