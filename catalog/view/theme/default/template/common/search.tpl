<div id="search" class="search-parent">
	<button type="button" class="search-btn d-xl-none">
	  <img src="catalog/view/javascript/home/assets/img/ic-search.svg" width="20" height="18">
	</button>
	<div id="form-search" class="header-dropdown-block header-dropdown-block_search">  
	    <div class="header-dropdown-block__selected d-none d-xl-block"><span><?php echo $categories ? $categories[0]['cname'] : '' ?></span>
	      <div class="header-dropdown-block__dropdown">
	        <ul>
	        	<?php foreach ($categories as $key => $value) { ?>
		          <li>
		            <button type="button" name="<?php echo $value['category_id'] ?>" class="header-dropdown-block__btn"><?php echo $value['cname'] ?></button>
		          </li>	        		
	        	<?php } ?>
	        </ul>
	      </div>
	    </div>
	    <select class="header-dropdown-block__selected d-xl-none">
	    	<?php foreach ($categories as $key => $value) { ?>
	          <option value="<?php echo $value['category_id'] ?>"><?php echo $value['cname'] ?></option>
        	<?php } ?>	      
	    </select>  
	  <input name="search" id="searchFormControlInput" type="text" placeholder="I’m shopping for..." value="<?php echo $search; ?>" class="form-control">
	  <button type="button" class="header-dropdown-block_search-btn">
	    <img src="catalog/view/javascript/home/assets/img/ic-search.svg" width="20" height="18">
	  </button>
	</div>
</div>
<!-- <div id="search" class="input-group">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" />
  <span class="input-group-btn">
    <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
  </span>
</div> -->