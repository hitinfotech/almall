<?php if (count($languages) > 1) { $lang_language = '';?>
<div class="header-dropdown-block">
<form id="form-language" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
  <div class="btn-group">    
    <?php foreach ($languages as $language) { ?>
    <?php if ($language['code'] == $code) { $lang_language = $language['name']; } ?>
    <?php } ?>
    <div class="header-dropdown-block__selected"><span><?php echo $lang_language; ?></span>
      <div class="header-dropdown-block__dropdown">      
        <ul>
          <?php foreach ($languages as $language) { ?>
          <li>
            <a href="<?php echo $language['code']; ?>"><?php echo $language['name']; ?></a>
          </li>
          <?php } ?>
        </ul>
      </div>
      </div>
  </div>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
</div>
<?php } ?>