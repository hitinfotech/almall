<div class="sign-up">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6">
          <form class="sign-up-form">
            <div class="form-group">
              <h3 class="sign-up-form__title">Sign up to newsletter<br>and get&nbsp;<span>10% discount!</span></h3>
              <label for="InputEmail1">Get latest trends, fashion & offers weekly.</label>
              <input id="InputEmail1" type="email" aria-describedby="emailHelp" placeholder="Your email addres" class="form-control">
            </div>
            <button type="submit" class="btn sign-up-form__btn">Male</button>
            <button type="submit" class="btn sign-up-form__btn">Female</button>
          </form>
        </div>
        <div class="col-md-6 d-md-block d-none"><img src="catalog/view/javascript/home/assets/img/sign_up_image.png" alt="image_desc" width="380" height="452" class="sign-up__image"></div>
      </div>
    </div>
</div>
<div class="clear"></div>
<div class="ftv-block-parent">
    <div class="container">
      <div class="row">
        <div class="col-6 col-xl-3">
          <div class="ftv-block ftv-block-1">
            <h3 class="ftv-block__title">100%</h3>
            <p class="ftv-block__subtitle">Genuine!</p>
          </div>
        </div>
        <div class="col-6 col-xl-3">
          <div class="ftv-block ftv-block-2">
            <h3 class="ftv-block__title">Cash</h3>
            <p class="ftv-block__subtitle">on delivery</p>
          </div>
        </div>
        <div class="col-6 col-xl-3">
          <div class="ftv-block ftv-block-3">
            <h3 class="ftv-block__title">14 days</h3>
            <p class="ftv-block__subtitle">free exchange</p>
          </div>
        </div>
        <div class="col-6 col-xl-3">
          <div class="ftv-block ftv-block-4">
            <h3 class="ftv-block__title">free</h3>
            <p class="ftv-block__subtitle">delivery!</p>
          </div>
        </div>
      </div>
    </div>
</div>
<div class="container">
  <footer>
    <div class="row">
      <div class="col-12 col-xl-2">
        <div class="footer-logo"><a href="#" class="footer-logo__link"><img src="<?php echo $logo; ?>" width="110" height="22" class="footer-logo__image"></a></div>
        <?php if ($informations) { ?>      
          <ul class="navbar-nav">
            <?php foreach ($informations as $information) { ?>
            <li class="nav-item"><a href="<?php echo $information['href']; ?>" class="nav-link"><?php echo $information['title']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      <div class="col-12 col-xl-2 d-xl-block d-none">
        <h3 class="footer__title">Shopping</h3>
        <ul class="navbar-nav navbar-nav__second">
          <?php foreach ($categories as $categorie) { ?>
            <li class="nav-item"><a href="<?php echo $categorie['href'] ?>" class="nav-link"><?php echo $categorie['name'] ?></a></li>
          <?php } ?>
          <li class="nav-item"><a href="#" class="nav-link">Deals of the Day</a></li>
          <li class="nav-item"><a href="#" class="nav-link">Ramadan Sale</a></li>
        </ul>
      </div>
      <div class="col-12 col-xl-4">
        <h3 class="footer__title">Do you have any questions?</h3><a href="tel:8004330033" class="footer__tel">8004330033</a>
        <p class="footer__work-time">From Sunday - Thursday 9:00AM - 5:00PM</p>
        <div class="footer-socials">
          <h3 class="footer__title">Follow us on:</h3>
          <div class="socials-icons"><a href="https://www.facebook.com/" target="_blank"><img src="catalog/view/javascript/home/assets/img/ic-fb.svg" alt="image_desc" width="9" height="18" class="social-item grounded-radiants"></a><a href="https://twitter.com/" target="_blank"><img src="catalog/view/javascript/home/assets/img/ic-twitter.svg" alt="image_desc" width="17" height="14" class="social-item grounded-radiants"></a><a href="https://www.youtube.com/" target="_blank"><img src="catalog/view/javascript/home/assets/img/ic-youtube.svg" alt="image_desc" width="17" height="18" class="social-item grounded-radiants"></a><a href="http://instagram.com/" target="_blank"><img src="catalog/view/javascript/home/assets/img/ic-instagram.svg" alt="image_desc" width="17" height="17" class="social-item grounded-radiants"></a></div>
        </div>
      </div>
      <div class="col-12 col-xl-4">
        <!--form.sign-up-form-->
        <!--    .form-group-->
        <!--        h3.sign-up-form__title-->
        <!--        | Sign up to newsletter and get&nbsp;-->
        <!--        span 10% discount!-->
        <!--        label(for='InputEmail2') Get latest trends, fashion & offers weekly.-->
        <!--        input#InputEmail2.form-control(type='email', aria-describedby='emailHelp', placeholder='Your email addres')-->
        <!--    button.btn.sign-up-form__btn(type='submit') Male-->
        <!--    button.btn.sign-up-form__btn(type='submit') Female-->
        <h3 class="footer__title">Try out our mobile apps</h3><a class="footer__subtitle">Click and get started in seconds!</a>
        <div class="clearfix"></div><a href="#" target="_blank" class="footer__link"><img src="catalog/view/javascript/home/assets/img/-e-google-play-badge.png" alt="image_desc" width="135" height="40" class="footer__img"></a><a href="#" target="_blank" class="footer__link"><img src="catalog/view/javascript/home/assets/img/-e-apple store.png" alt="image_desc" width="135" height="40" class="footer__img"></a>
        <button id="up" class="footer__up-btn"></button>
      </div>
    </div>
  </footer>
</div>
<div class="footer-rights">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h5>2018 Almall All rights reserved</h5>
      </div>
    </div>
  </div>
</div>
</body></html>
<!-- <footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p><?php echo $powered; ?></p>
  </div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html> 