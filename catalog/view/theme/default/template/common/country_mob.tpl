 <div class="header-dropdown-block">
  <form id="form-country" action="" method="post" enctype="multipart/form-data">
    <div class="header-dropdown-block__selected"><img src="catalog/view/javascript/home/assets/img/UAE.png" alt="image_desc" width="" height="" class="header-dropdown-block__selected-flag"><span>UAE</span><i class="icon-forward"></i>
      <div class="header-dropdown-block__dropdown">
        <div class="menu-header-block__column-parent">
          <h2 class="menu-header-block__title-back">Language</h2>
          <ul class="navbar-nav">
            <li class="nav-item"><a href="#" class="nav-link"><img src="catalog/view/javascript/home/assets/img/UAE.png" alt="image_desc" width="" height="">
                <button type="button" name="{{ language.code }}" class="header-dropdown-block__btn">UAE</button></a></li>
            <li class="nav-item"><a href="#" class="nav-link"><img src="catalog/view/javascript/home/assets/img/UAE.png" alt="image_desc" width="" height="">
                <button type="button" name="{{ language.code }}" class="header-dropdown-block__btn">ENG</button></a></li>
          </ul>
        </div>
      </div>
    </div>
    <input type="hidden" name="code" value="">
    <input type="hidden" name="redirect" value="{{ redirect }}">
  </form>
</div>