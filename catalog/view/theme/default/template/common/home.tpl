<?php echo $header; ?>
<!-- <div class="container">
  <div class="row"><?php //echo $column_left; ?>
    <?php //if ($column_left && $column_right) { ?>
    <?php //$class = 'col-sm-6'; ?>
    <?php //} elseif ($column_left || $column_right) { ?>
    <?php //$class = 'col-sm-9'; ?>
    <?php //} else { ?>
    <?php //$class = 'col-sm-12'; ?>
    <?php //} ?>
    <div id="content" class="<?php //echo $class; ?>"><?php //echo $content_top; ?><?php //echo $content_bottom; ?></div>
    <?php //echo $column_right; ?></div>
</div> -->
<div class="container-fluid px-0 mx-0"> 
  <div class="container">
    <div class="row">
      <div class="col-12 px-0">
        <div class="slider">
          <div class="socials-icons"><a href="https://www.facebook.com/" target="_blank" class="socials-icons__link"><span class="fa fa-facebook"></span></a><a href="https://twitter.com/" target="_blank" class="socials-icons__link"><span class="fa fa-twitter"></span></a><a href="https://instagram.com/" target="_blank" class="socials-icons__link"><span class="fa fa-youtube"></span></a><a href="https://instagram.com/" target="_blank" class="socials-icons__link"><span class="fa fa-instagram"></span></a></div>
          <div class="owl-carousel owl-theme">
            <div class="item">
              <div class="slider__title">
                <p class="slider__title_add">Hot & Trends for Summer</p>
                <p class="slider__title_main">Get the look</p>
                <p class="slider__title_add"> Men & Women</p>
              </div><img src="catalog/view/javascript/home/assets/img/banner-top.jpg" alt="image_desc" width="" height="" class="slider__image">
              <button class="btn slider__btn">Explore</button>
              <div class="slider__count">
                <p></p><span>01</span>/03
              </div>
            </div>
            <div class="item">
              <div class="slider__title">
                <p class="slider__title_add">Hot & Trends for Summer</p>
                <p class="slider__title_main">Get the look</p>
                <p class="slider__title_add"> Men & Women</p>
              </div><img src="catalog/view/javascript/home/assets/img/banner-top.jpg" alt="image_desc" width="" height="" class="slider__image">
              <button class="btn slider__btn">Explore</button>
              <div class="slider__count">
                <p></p><span>02</span>/03
              </div>
            </div>
            <div class="item">
              <div class="slider__title">
                <p class="slider__title_add">Hot & Trends for Summer</p>
                <p class="slider__title_main">Get the look</p>
                <p class="slider__title_add"> Men & Women</p>
              </div><img src="catalog/view/javascript/home/assets/img/banner-top.jpg" alt="image_desc" width="" height="" class="slider__image">
              <button class="btn slider__btn">Explore</button>
              <div class="slider__count">
                <p></p><span>03</span>/03
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <h2 class="product-block__title">Deals of the Day</h2><a href="#" target="_blank" class="product-block__all">See all</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <div class="product-block clearfix">
          <div class="product-block-items owl-theme">
            <div class="product-block__item product-block__item_first">
              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/deals-1.jpg" alt="image_desc" width="" height="" class="product-block__image_filter"></a></div>
              <p class="product-block__item-title">Deals up<br><span>to&nbsp;<b>70%</b>&nbsp;OFF!</span></p>
              <div class="product-block-info"><a href="#" class="product-block-info__link_title">SALE!</a><a href="#" class="product-block-info__link_subtitle">Deals up to 70% OFF!</a>
                <button class="btn product-block-info__btn_first-btn">Shop NOW</button>
              </div>
            </div>
            <div class="product-block__item">
              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/deals-2.jpg" alt="image_desc" width="" height=""></a></div>
              <p class="product-block__discount-sum">-50%</p>
              <div class="product-block__timer">
                <p class="product-block__timer_h">11&nbsp;<span>h :&nbsp;</span></p>
                <p class="product-block__timer_m">18&nbsp;<span>m :&nbsp;</span></p>
                <p class="product-block__timer_s">39&nbsp;<span>s</span></p>
              </div>
              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl Queen</a><a href="#" class="product-block-info__link_subtitle">Modest Pink Zigzag Abaya X</a>
                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                <p class="product-block-info__price_old">195.97 AED</p>
                <button class="product-block-info__btn">
                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                </button>
              </div>
            </div>
            <div class="product-block__item">
              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/deals-3.jpg" alt="image_desc" width="" height=""></a></div>
              <p class="product-block__discount-sum">-50%</p>
              <div class="product-block__timer">
                <p class="product-block__timer_h">11&nbsp;<span>h :&nbsp;</span></p>
                <p class="product-block__timer_m">18&nbsp;<span>m :&nbsp;</span></p>
                <p class="product-block__timer_s">39&nbsp;<span>s</span></p>
              </div>
              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl Queen</a><a href="#" class="product-block-info__link_subtitle">Modest Pink Zigzag Abaya X</a>
                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                <p class="product-block-info__price_old">195.97 AED</p>
                <button class="product-block-info__btn">
                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                </button>
              </div>
            </div>
            <div class="product-block__item">
              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/deals-4.jpg" alt="image_desc" width="" height=""></a></div>
              <p class="product-block__discount-sum">-50%</p>
              <div class="product-block__timer">
                <p class="product-block__timer_h">11&nbsp;<span>h :&nbsp;</span></p>
                <p class="product-block__timer_m">18&nbsp;<span>m :&nbsp;</span></p>
                <p class="product-block__timer_s">39&nbsp;<span>s</span></p>
              </div>
              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl Queen</a><a href="#" class="product-block-info__link_subtitle">Modest Pink Zigzag Abaya X</a>
                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                <p class="product-block-info__price_old">195.97 AED</p>
                <button class="product-block-info__btn">
                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                </button>
              </div>
            </div>
            <div class="product-block__item">
              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/deals-3.jpg" alt="image_desc" width="" height=""></a></div>
              <p class="product-block__discount-sum">-50%</p>
              <div class="product-block__timer">
                <p class="product-block__timer_h">11&nbsp;<span>h :&nbsp;</span></p>
                <p class="product-block__timer_m">18&nbsp;<span>m :&nbsp;</span></p>
                <p class="product-block__timer_s">39&nbsp;<span>s</span></p>
              </div>
              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl Queen</a><a href="#" class="product-block-info__link_subtitle">Modest Pink Zigzag Abaya X</a>
                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                <p class="product-block-info__price_old">195.97 AED</p>
                <button class="product-block-info__btn">
                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                </button>
              </div>
            </div>
            <div class="product-block__item">
              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/deals-4.jpg" alt="image_desc" width="" height=""></a></div>
              <p class="product-block__discount-sum">-50%</p>
              <div class="product-block__timer">
                <p class="product-block__timer_h">11&nbsp;<span>h :&nbsp;</span></p>
                <p class="product-block__timer_m">18&nbsp;<span>m :&nbsp;</span></p>
                <p class="product-block__timer_s">39&nbsp;<span>s</span></p>
              </div>
              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl Queen</a><a href="#" class="product-block-info__link_subtitle">Modest Pink Zigzag Abaya X</a>
                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                <p class="product-block-info__price_old">195.97 AED</p>
                <button class="product-block-info__btn">
                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                </button>
              </div>
            </div>
            <div class="product-block__item">
              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/deals-2.jpg" alt="image_desc" width="" height=""></a></div>
              <p class="product-block__discount-sum">-50%</p>
              <div class="product-block__timer">
                <p class="product-block__timer_h">11&nbsp;<span>h :&nbsp;</span></p>
                <p class="product-block__timer_m">18&nbsp;<span>m :&nbsp;</span></p>
                <p class="product-block__timer_s">39&nbsp;<span>s</span></p>
              </div>
              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl Queen</a><a href="#" class="product-block-info__link_subtitle">Modest Pink Zigzag Abaya X</a>
                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                <p class="product-block-info__price_old">195.97 AED</p>
                <button class="product-block-info__btn">
                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h2 class="stuff-block__title-main">New Arrivals</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-6 pl-md-0 pr-md-2">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item">
              <h4 class="stuff-block__title">Sport collection</h4>
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/arrivals-1.jpg" alt="image_desc" width="577" height="453" class="stuff-block__image">
            </div></a><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item">
              <h4 class="stuff-block__title">Trendy Watches</h4>
              <p class="stuff-block__subtitle">For him</p>
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/arrivals-2.jpg" alt="image_desc" width="577" height="453" class="stuff-block__image">
            </div></a></div>
      </div>
      <div class="col-12 col-md-6 pr-md-0 pl-md-2">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_short">
              <h4 class="stuff-block__title">Sport collection</h4>
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/arrivals-3.jpg" alt="image_desc" width="577" height="297" class="stuff-block__image">
            </div></a><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_short">
              <h4 class="stuff-block__title">Trendy Handbags20char</h4>
              <p class="stuff-block__subtitle">Suit all tastes</p>
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/arrivals-4.jpg" alt="image_desc" width="577" height="297" class="stuff-block__image">
            </div></a><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_short">
              <h4 class="stuff-block__title">Shoes Collection</h4>
              <p class="stuff-block__subtitle">For her</p>
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/arrivals-5.jpg" alt="image_desc" width="577" height="297" class="stuff-block__image">
            </div></a></div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h2 class="stuff-block__title-main">Men</h2>
        <h3 class="stuff-block__title-hand">Watches</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-7 pl-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item"><img src="catalog/view/javascript/home/assets/img/casio.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/watches-1.jpg" alt="image_desc" width="665" height="453" class="stuff-block__image">
            </div></a></div>
      </div>
      <div class="col-12 col-md-5 pl-md-0 pr-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/armani_exchange-logo.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/watches-2.jpg" alt="image_desc" width="489" height="219" class="stuff-block__image">
            </div></a><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/skagen.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/watches-3.jpg" alt="image_desc" width="489" height="219" class="stuff-block__image">
            </div></a></div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h2 class="product-block__title product-block__title_smaller">More brands</h2>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <div class="brands-slider owl-theme">
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <h2 class="product-block__title product-block__title_smaller">Our selection</h2><a href="#" target="_blank" class="product-block__all">See all</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <div class="product-block clearfix">
          <div class="product-block-items owl-theme">
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/casio1.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/casio2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/casio3.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/casio4.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/casio2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/casio2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/casio2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/casio4.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h3 class="stuff-block__title-hand">Perfumes</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-7 pl-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item"><img src="catalog/view/javascript/home/assets/img/logo12.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/perfumes1.png" alt="image_desc" width="" height="" class="stuff-block__image">
            </div></a></div>
      </div>
      <div class="col-12 col-md-5 pl-md-0 pr-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/logo13.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/perfumes2.png" alt="image_desc" width="" height="" class="stuff-block__image">
            </div></a><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/logo14.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/perfumes3.png" alt="image_desc" width="" height="" class="stuff-block__image">
            </div></a></div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h2 class="product-block__title product-block__title_smaller">More brands</h2>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <div class="brands-slider owl-theme">
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <h2 class="product-block__title product-block__title_smaller">Our selection</h2><a href="#" target="_blank" class="product-block__all">See all</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <div class="product-block clearfix">
          <div class="product-block-items owl-theme">
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/perfumes_product1.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                                <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/perfumes_product2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                                <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/perfumes_product3.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                                <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/perfumes_product4.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                                <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/perfumes_product1.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                                <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/perfumes_product4.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                                <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/perfumes_product1.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                                <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/perfumes_product2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                                <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h2 class="stuff-block__title-main">Women</h2>
        <h3 class="stuff-block__title-hand">Clothing</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-7 pl-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item"><img src="catalog/view/javascript/home/assets/img/logo10.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/clothes1.png" alt="image_desc" width="665" height="453" class="stuff-block__image">
            </div></a></div>
      </div>
      <div class="col-12 col-md-5 pl-md-0 pr-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/logo11.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/clothes2.png" alt="image_desc" width="665" height="453" class="stuff-block__image">
            </div></a><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/bysi.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/clothes3.png" alt="image_desc" width="665" height="453" class="stuff-block__image">
            </div></a></div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h2 class="product-block__title product-block__title_smaller">More brands</h2>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <div class="brands-slider owl-theme">
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <h2 class="product-block__title product-block__title_smaller">Our selection</h2><a href="#" target="_blank" class="product-block__all">See all</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <div class="product-block clearfix">
          <div class="product-block-items owl-theme">
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/clothing_product1.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/clothing_product2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/clothing_product3.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/clothing_product4.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/clothing_product2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/clothing_product3.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Casio</a><a href="#" class="product-block-info__link_subtitle">EDIFICE Analog Silver Frame Blue Dial with Brown Leather Strap Watch</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h3 class="stuff-block__title-hand">Skin Care</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-7 pl-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item"><img src="catalog/view/javascript/home/assets/img/Layer 3186.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/skin1.jpg" alt="image_desc" width="" height="" class="stuff-block__image">
            </div></a></div>
      </div>
      <div class="col-12 col-md-5 pl-md-0 pr-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/Obagi_Medical_R_logo_color.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/skin2.jpg" alt="image_desc" width="" height="" class="stuff-block__image">
            </div></a><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/Layer 3186.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/skin3.jpg" alt="image_desc" width="" height="" class="stuff-block__image">
            </div></a></div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h2 class="product-block__title product-block__title_smaller">More brands</h2>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <div class="brands-slider owl-theme">
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <h2 class="product-block__title product-block__title_smaller">Our selection</h2><a href="#" target="_blank" class="product-block__all">See all</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <div class="product-block clearfix">
          <div class="product-block-items owl-theme">
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/skin_product1.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/skin_product2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/skin_product3.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/skin_product4.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/skin_product2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/skin_product3.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h2 class="stuff-block__title-main">Sport</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-7 pl-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item"><img src="catalog/view/javascript/home/assets/img/logo4.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/sport1.jpg" alt="image_desc" width="665" height="453" class="stuff-block__image">
            </div></a></div>
      </div>
      <div class="col-12 col-md-5 pl-md-0 pr-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/logo5.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/sport2.jpg" alt="image_desc" width="489" height="219" class="stuff-block__image">
            </div></a><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/logo6.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/sport3.jpg" alt="image_desc" width="489" height="219" class="stuff-block__image">
            </div></a></div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h2 class="product-block__title product-block__title_smaller">More brands</h2>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <div class="brands-slider owl-theme">
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <h2 class="product-block__title product-block__title_smaller">Our selection</h2><a href="#" target="_blank" class="product-block__all">See all</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <div class="product-block clearfix">
          <div class="product-block-items owl-theme">
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/sport_product1.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Adidas</a><a href="#" class="product-block-info__link_subtitle">Gold Plated Watch With a Black Dial For Men</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/skin_product2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Adidas</a><a href="#" class="product-block-info__link_subtitle">Gold Plated Watch With a Black Dial For Men</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/sport_product2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Adidas</a><a href="#" class="product-block-info__link_subtitle">Gold Plated Watch With a Black Dial For Men</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/sport_product4.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Adidas</a><a href="#" class="product-block-info__link_subtitle">Gold Plated Watch With a Black Dial For Men</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/sport_product2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Adidas</a><a href="#" class="product-block-info__link_subtitle">Gold Plated Watch With a Black Dial For Men</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/sport_product1.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Adidas</a><a href="#" class="product-block-info__link_subtitle">Gold Plated Watch With a Black Dial For Men</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/sport_product4.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Adidas</a><a href="#" class="product-block-info__link_subtitle">Gold Plated Watch With a Black Dial For Men</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h2 class="stuff-block__title-main">Home</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-7 pl-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item"><img src="catalog/view/javascript/home/assets/img/logo1.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/home1.png" alt="image_desc" width="665" height="453" class="stuff-block__image">
            </div></a></div>
      </div>
      <div class="col-12 col-md-5 pl-md-0 pr-md-0">
        <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/logo2.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/home2.png" alt="image_desc" width="665" height="453" class="stuff-block__image">
            </div></a><a href="#" class="stuff-block-parent-link">
            <div class="stuff-block__item stuff-block__item_very-short"><img src="catalog/view/javascript/home/assets/img/logo3.png" alt="image_desc" width="" height="" class="stuff-block__title-image">
              <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" alt="image_desc" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/home3.png" alt="image_desc" width="665" height="453" class="stuff-block__image">
            </div></a></div>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <h2 class="product-block__title product-block__title_smaller">More brands</h2>
      </div>
    </div>
    <div class="row">
      <div class="col px-md-0">
        <div class="brands-slider owl-theme">
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand3.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand1.jpg" alt="image_desc" class="brands-slider__image"></a></div>
                        <div class="brands-slider__item"><a href="#" class="brands-slider__link"><img src="catalog/view/javascript/home/assets/img/brand2.jpg" alt="image_desc" class="brands-slider__image"></a></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <h2 class="product-block__title product-block__title_smaller">Our selection</h2><a href="#" target="_blank" class="product-block__all">See all</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12 px-md-0">
        <div class="product-block clearfix">
          <div class="product-block-items owl-theme">
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/home_product1.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/home_product2.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/home_product3.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/home_product4.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/home_product3.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
                            <div class="product-block__item">
                              <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/home_product1.jpg" alt="image_desc" width="" height=""></a></div>
                              <p class="product-block__discount-sum">-50%</p>
                              <div class="product-block-info"><a href="#" class="product-block-info__link_title">Obagi</a><a href="#" class="product-block-info__link_subtitle">Gentle Rejuvenation Skin Calming Cream - 2.8 oz (80 g)</a>
                                <p class="product-block-info__price_new">128.97&nbsp;<span>AED</span></p>
                                <p class="product-block-info__price_old">195.97 AED</p>
                                <button class="product-block-info__btn">
                                  <!--i.fa.fa-heart-o(aria-hidden='true')--><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active">
                                </button>
                              </div>
                            </div>
          </div>
        </div>
      </div>
    </div>
  </div>   
</div>
<?php echo $footer; ?>