<?php if (count($countries) > 1) { ?>
  <div class="header-dropdown-block">
    <form id="form-country" action="" method="post" enctype="multipart/form-data">
      <div class="header-dropdown-block__selected"><img src="catalog/view/javascript/home/assets/img/UAE.png" alt="image_desc" width="" height="" class="header-dropdown-block__selected-flag"><span>UAE</span>
        <div class="header-dropdown-block__dropdown">
          <ul>
            <?php foreach ($countries as $country) { ?>
            <li><a href="<?php echo $country['redirect'] ?>" class='contry_flex_a'><img src="<?php echo $country['flag']; ?>" alt="<?php echo $country['name']; ?>" width="" height="">
              <button type="button" name="{{ language.code }}" class="header-dropdown-block__btn"><?php echo $country['name']; ?></button></a>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <input type="hidden" name="code" value="">
      <input type="hidden" name="redirect" value="{{ redirect }}">
    </form>
  </div>
<?php } ?>