<?php echo $header; ?>
<div class="container-fluid px-0 mx-0">
  <div class="navigation" style="background: url('<?php echo $cat_img; ?>') center no-repeat;">
      <div class="container px-xl-0">
        <div class="row">
          <div class="col-12">
            <h1><?php echo $heading_title; ?></h1>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <?php 
                $last = count($breadcrumbs);
                $i = 1;
                foreach ($breadcrumbs as $breadcrumb) { ?>
                <li <?php echo ($i == $last) ? 'aria-current="page"' : '' ?> class="breadcrumb-item <?php echo ($i == $last) ? 'active' : '' ?>"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php $i++; } ?>
              </ol>
            </nav>
          </div>
        </div>
      </div>
  </div>
</div>
<div class="container px-xl-0">
  <div class="row">
    <div class="col-12 col-md-6 pr-xl-0">
      <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
          <div class="stuff-block__item stuff-block__item_short">
            <h4 class="stuff-block__title stuff-block__title_shop-top">Accessories</h4>
            <p class="stuff-block__subtitle stuff-block__subtitle_shop-top">New Collection 2018 from Zyros</p>
            <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/women-1.jpg" alt="image_desc" width="570" height="235" class="stuff-block__image">
          </div></a></div>
    </div>
    <div class="col-12 col-md-6">
      <div class="stuff-block"><a href="#" class="stuff-block-parent-link">
          <div class="stuff-block__item stuff-block__item_short">
            <h4 class="stuff-block__title stuff-block__title_shop-top">Shoes summer Collection</h4>
            <div href="#" class="stuff-block__link">Shop now<img src="catalog/view/javascript/home/assets/img/arrow.svg" width="17" height="9" class="arrow"></div><img src="catalog/view/javascript/home/assets/img/banner2_Shoes.jpg" alt="image_desc" width="570" height="235" class="stuff-block__image">
          </div></a></div>
    </div>
  </div>
   <div class="row">
      <div class="col-12">
        <h2 class="product-block__title product-block__title_smaller"><?php echo $heading_title; ?> Shop Categories</h2>
      </div>
    </div>
    <div class="row">
    <div class="col-12 mx-xl-3">
      <div class="product-block product-block_shop-categories clearfix mb-0">
        <div class="shop-categories-carousel">
          <?php foreach ($categories as $key => $value) { ?>            
            <div class="stuff-block">
              <a href="<?php echo $value['href'] ?>" class="stuff-block-parent-link">
                <div class="stuff-block__item">
                  <h4 class="stuff-block__title stuff-block__title_shop-categories"><?php echo $value['cname']; ?></h4>
                  <img src="<?php echo $value['image']; ?>" alt="image_desc" width="271" height="142" class="stuff-block__image">
                </div>
              </a>
            </div>          
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
 <!--  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul> -->
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>      
      <?php if ($products) { ?>
        <div class="filters">
              <form id="form-filters" action="" method="post" enctype="multipart/form-data">
                <div class="filters-dropdown-block_mobile d-xl-none clearfix">
                  <select id="sort" class="form-control_select" onchange="location = this.value;">                    
                    <?php $sort_text = ''; foreach ($sorts as $sortss) { ?>
                   <?php if ($sortss['value'] == $sort . '-' . $order) { $sort_text = $sortss['text'];?>
                    <option value="<?php echo $sort['href']; ?>" selected="selected"><?php echo $sortss['text']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $sort['href']; ?>"><?php echo $sortss['text']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                  <button type="button" data-toggle="collapse" data-target="#filterSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="filters-dropdown-block__selected_filter-btn"><i class="icon-filter"></i><span>Filter</span><i class="icon-filter-ok"></i></button>
                </div>
                <div id="filterSupportedContent" class="filters-dropdown-block">
                  <div class="filters-dropdown-block__title d-xl-none">
                    <button type="button" aria-label="Close" data-dismiss="modal" data-toggle="collapse" data-target="#filterSupportedContent" aria-expanded="true"><i class="icon-close"></i>Filter</button>
                    <p>Reset all filters</p>
                  </div>
                  <div class="filters-dropdown-block__scrolling-area">
                    <div class="filters-dropdown-block__selected d-xl-block d-none">
                      <i class="icon-sort"></i>
                      <span><?php echo ($sort_text) ? $sort_text : $text_sort; ?></span>
                      <i class="icon-down"></i>
                      <div class="filters-dropdown-block__dropdown sort_filter">
                        <ul class="navbar-nav">
                          <?php foreach ($sorts as $sortss) { ?>
                            <li class="nav-item">
                              <div class="nav-link" data-href='<?php echo $sortss['href']; ?>'><?php echo $sortss['text']; ?></div>
                            </li>
                          <?php } ?>                          
                        </ul>
                      </div>
                    </div>
                    <div class="filters-dropdown-block__selected"><span>Brands</span><i class="icon-down d-none d-xl-block"></i><i class="icon-forward d-xl-none"></i>
                      <div class="filters-selected-desc d-xl-none">
                        <ul class="filters-selected-desc__list">
                          <li class="filters-selected-desc__item">Abeer Al Sutwadi</li>
                          <li class="filters-selected-desc__item">Alta Moda</li>
                          <li class="filters-selected-desc__item">Black Swan</li>
                          <li class="filters-selected-desc__item">Couture</li>
                          <li class="filters-selected-desc__item">Bysi</li>
                          <li class="filters-selected-desc__item">Abeer Al Sutwadi</li>
                          <li class="filters-selected-desc__item">Alta Moda</li>
                          <li class="filters-selected-desc__item">Black Swan</li>
                          <li class="filters-selected-desc__item">Couture</li>
                          <li class="filters-selected-desc__item">Bysi</li>
                        </ul>
                      </div>
                      <div class="filters-dropdown-block__dropdown">
                        <div class="filters-dropdown-block__title d-xl-none">
                          <button><i class="icon-back"></i> Brands</button>
                          <p>Reset all filters</p>
                        </div>
                        <div class="form-inline">
                          <input type="button" name="search_brand" class="btn btn-outline-danger brand_filter" value="Filter Brand">
                        </div>
                        <div class="form-inline">
                          <input id="brandsInput" type="search" onkeyup="searchInBrands()" placeholder="Search in brands" aria-label="Search" class="form-control">
                          <button type="submit" class="filters__btn"><img src="catalog/view/javascript/home/assets/img/ic-search.svg" width="12" heigh="13"></button>
                        </div>
                        <div id="brandsList" class="filters-dropdown-block__scrolling">
                          <?php foreach ($manufacturer as $key => $value) { ?>                            
                            <input id="barndsCheckbox" type="checkbox" value="<?php echo $key; ?>">
                            <label for="barndsCheckbox"><?php echo $value; ?></label>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                    <div class="filters-dropdown-block__selected"><span>Size</span><i class="icon-down d-none d-xl-block"></i><i class="icon-forward d-xl-none"></i>
                      <div class="filters-selected-desc d-xl-none">
                        <ul class="filters-selected-desc__list">
                          <li class="filters-selected-desc__item">EUR 44</li>
                          <li class="filters-selected-desc__item">EUR 50</li>
                        </ul>
                      </div>
                      <div class="filters-dropdown-block__dropdown">
                        <div class="filters-dropdown-block__title d-xl-none">
                          <button><i class="icon-back"></i> Size</button>
                          <p>Reset all filters</p>
                        </div>
                        <ul class="nav nav-tabs">
                          <li class="nav-item"><a data-toggle="tab" href="#home" class="nav-link">US</a></li>
                          <li class="nav-item"><a data-toggle="tab" href="#menu1" class="nav-link">UK</a></li>
                          <li class="nav-item"><a data-toggle="tab" href="#menu2" class="nav-link active">EUR</a></li>
                          <li class="nav-item"><a data-toggle="tab" href="#menu3" class="nav-link">Intl</a></li>
                          <li class="nav-item"><a data-toggle="tab" href="#menu4" class="nav-link">IT</a></li>
                        </ul>
                        <div class="tab-content">
                          <div id="home" class="tab-pane fade">
                            <div class="filters-dropdown-block__scrolling filters-dropdown-block__scrolling_rigth">
                              <div class="filters-dropdown-block__size">11</div>
                              <div class="filters-dropdown-block__size">12</div>
                              <div class="filters-dropdown-block__size">13</div>
                              <div class="filters-dropdown-block__size">14</div>
                              <div class="filters-dropdown-block__size">15</div>
                              <div class="filters-dropdown-block__size">15</div>
                            </div>
                          </div>
                          <div id="menu1" class="tab-pane fade">
                            <div class="filters-dropdown-block__scrolling filters-dropdown-block__scrolling_rigth">
                              <div class="filters-dropdown-block__size">20</div>
                              <div class="filters-dropdown-block__size">20</div>
                              <div class="filters-dropdown-block__size">22</div>
                              <div class="filters-dropdown-block__size">23</div>
                              <div class="filters-dropdown-block__size">24</div>
                              <div class="filters-dropdown-block__size">25</div>
                            </div>
                          </div>
                          <div id="menu2" class="tab-pane fade show active">
                            <div class="filters-dropdown-block__scrolling filters-dropdown-block__scrolling_rigth">
                              <div class="filters-dropdown-block__size">42</div>
                              <div class="filters-dropdown-block__size">44</div>
                              <div class="filters-dropdown-block__size">46</div>
                              <div class="filters-dropdown-block__size">48</div>
                              <div class="filters-dropdown-block__size">50</div>
                              <div class="filters-dropdown-block__size">52</div>
                            </div>
                          </div>
                          <div id="menu3" class="tab-pane fade">
                            <div class="filters-dropdown-block__scrolling filters-dropdown-block__scrolling_rigth">
                              <div class="filters-dropdown-block__size">20</div>
                              <div class="filters-dropdown-block__size">20</div>
                              <div class="filters-dropdown-block__size">22</div>
                              <div class="filters-dropdown-block__size">23</div>
                              <div class="filters-dropdown-block__size">24</div>
                              <div class="filters-dropdown-block__size">25</div>
                            </div>
                          </div>
                          <div id="menu4" class="tab-pane fade">
                            <div class="filters-dropdown-block__scrolling filters-dropdown-block__scrolling_rigth">
                              <div class="filters-dropdown-block__size">20</div>
                              <div class="filters-dropdown-block__size">20</div>
                              <div class="filters-dropdown-block__size">22</div>
                              <div class="filters-dropdown-block__size">23</div>
                              <div class="filters-dropdown-block__size">24</div>
                              <div class="filters-dropdown-block__size">25</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="filters-dropdown-block__selected"><span>Colors</span><i class="icon-down d-none d-xl-block"></i><i class="icon-forward d-xl-none"></i>
                      <div class="filters-selected-desc d-xl-none">
                        <ul class="filters-selected-desc__list">
                          <li class="filters-selected-desc__item"><i data-color="golden" class="icon-color"></i>Golden</li>
                          <li class="filters-selected-desc__item"><i data-color="red" class="icon-color"></i>Red</li>
                        </ul>
                      </div>
                      <div class="filters-dropdown-block__dropdown">
                        <div class="filters-dropdown-block__title d-xl-none">
                          <button><i class="icon-back"></i> Colors</button>
                          <p>Reset all filters</p>
                        </div>
                        <div class="form-inline">
                          <input id="colorsInput" type="search" onkeyup="searchInColors()" placeholder="Search in colors" aria-label="Search" class="form-control">
                          <button type="submit" class="filters__btn"><img src="catalog/view/javascript/home/assets/img/ic-search.svg" width="12" heigh="13"></button>
                        </div>
                        <div id="colorsList" class="filters-dropdown-block__scrolling">
                          <input id="colorcheckbox" type="checkbox">
                          <label for="colorcheckbox" data-color="golden" class="color">Golden</label>
                          <input id="colorcheckbox1" type="checkbox">
                          <label for="colorcheckbox1" data-color="pink" class="color">Pink</label>
                          <input id="colorcheckbox2" type="checkbox">
                          <label for="colorcheckbox2" data-color="brown" class="color">Brown</label>
                          <input id="colorcheckbox3" type="checkbox">
                          <label for="colorcheckbox3" data-color="red" class="color">Red</label>
                          <input id="colorcheckbox4" type="checkbox">
                          <label for="colorcheckbox4" data-color="blue" class="color">Blue</label>
                          <input id="colorcheckbox5" type="checkbox">
                          <label for="colorcheckbox5" data-color="beige" class="color">Beige</label>
                          <input id="colorcheckbox6" type="checkbox">
                          <label for="colorcheckbox6" data-color="white" class="color">White</label>
                          <input id="colorcheckbox7" type="checkbox">
                          <label for="colorcheckbox7" data-color="velvet" class="color">Velvet</label>
                        </div>
                      </div>
                    </div>
                    <div class="filters-dropdown-block__selected"><span>Price</span><i class="icon-down d-none d-xl-block"></i><i class="icon-forward d-xl-none"></i>
                      <div class="filters-selected-desc d-xl-none">
                        <ul class="filters-selected-desc__list">
                          <li class="filters-selected-desc__item">Highest price:&nbsp;<span>500&nbsp;</span>SAR</li>
                          <li class="filters-selected-desc__item">Only Discounted</li>
                        </ul>
                      </div>
                      <div class="filters-dropdown-block__dropdown">
                        <div class="filters-dropdown-block__title d-xl-none">
                          <button><i class="icon-back"></i> Price</button>
                          <p>Reset all filters</p>
                        </div>
                        <p class="price__desc">Select min and max price</p>
                        <div class="range-controls">
                          <div class="scale">
                            <div id="slider-range"></div>
                          </div>
                          <!--    .bar(style='margin-left:0px;width:90px;')-->
                          <!--.toggle.min-toggle-->
                          <!--.toggle.max-toggle-->
                        </div>
                        <div class="price-controls">
                          <label id="label-min">0</label><span>Sar</span>
                          <label id="label-max">2000</label>
                        </div>
                        <input id="priceCheckbox" type="checkbox">
                        <label for="priceCheckbox">Show only discounted</label>
                      </div>
                    </div>
                    <div class="filters-dropdown-block__selected"><span>Fabric</span><i class="icon-down d-none d-xl-block"></i><i class="icon-forward d-xl-none"></i>
                      <div class="filters-selected-desc d-xl-none">
                        <ul class="filters-selected-desc__list">
                          <li class="filters-selected-desc__item">Cotton</li>
                        </ul>
                      </div>
                      <div class="filters-dropdown-block__dropdown">
                        <div class="filters-dropdown-block__title d-xl-none">
                          <button><i class="icon-back"></i> Fabric</button>
                          <p>Reset all filters</p>
                        </div>
                        <div class="filters-dropdown-block__scrolling">
                          <input id="fabricCheckbox" type="checkbox">
                          <label for="fabricCheckbox">Cotton</label>
                          <input id="fabricCheckbox1" type="checkbox">
                          <label for="fabricCheckbox1">Silk</label>
                          <input id="fabricCheckbox2" type="checkbox">
                          <label for="fabricCheckbox2">Linen</label>
                          <input id="fabricCheckbox3" type="checkbox">
                          <label for="fabricCheckbox3">Wool</label>
                          <input id="fabricCheckbox4" type="checkbox">
                          <label for="fabricCheckbox4">Acrylic</label>
                        </div>
                      </div>
                    </div>
                    <div class="filters-dropdown-block__selected"><span>Length</span><i class="icon-down d-none d-xl-block"></i><i class="icon-forward d-xl-none"></i>
                      <div class="filters-selected-desc d-xl-none">
                        <ul class="filters-selected-desc__list">
                          <li class="filters-selected-desc__item">Mini</li>
                          <li class="filters-selected-desc__item">Above knee</li>
                        </ul>
                      </div>
                      <div class="filters-dropdown-block__dropdown">
                        <div class="filters-dropdown-block__title d-xl-none">
                          <button><i class="icon-back"></i> Length</button>
                          <p>Reset all filters</p>
                        </div>
                        <div class="filters-dropdown-block__scrolling">
                          <input id="lengthCheckbox" type="checkbox">
                          <label for="lengthCheckbox">Mini</label>
                          <input id="lengthCheckbox1" type="checkbox">
                          <label for="lengthCheckbox1">Above Knee</label>
                          <input id="lengthCheckbox2" type="checkbox">
                          <label for="lengthCheckbox2">Knee</label>
                          <input id="lengthCheckbox3" type="checkbox">
                          <label for="lengthCheckbox3">Midi</label>
                          <input id="lengthCheckbox4" type="checkbox">
                          <label for="lengthCheckbox4">Maxi</label>
                          <input id="lengthCheckbox5" type="checkbox">
                          <label for="lengthCheckbox5">Floor Length</label>
                        </div>
                      </div>
                    </div>
                    <div class="filters-dropdown-block__selected"><span>Style</span><i class="icon-down d-none d-xl-block"></i><i class="icon-forward d-xl-none"></i>
                      <div class="filters-selected-desc d-xl-none">
                        <ul class="filters-selected-desc__list">
                          <li class="filters-selected-desc__item">Brief</li>
                        </ul>
                      </div>
                      <div class="filters-dropdown-block__dropdown">
                        <div class="filters-dropdown-block__title d-xl-none">
                          <button><i class="icon-back"></i> Style</button>
                          <p>Reset all filters</p>
                        </div>
                        <div class="filters-dropdown-block__scrolling">
                          <input id="styleCheckbox" type="checkbox">
                          <label for="styleCheckbox">Brief</label>
                          <input id="styleCheckbox1" type="checkbox">
                          <label for="styleCheckbox1">Casual</label>
                          <input id="styleCheckbox2" type="checkbox">
                          <label for="styleCheckbox2">Cute</label>
                          <input id="styleCheckbox3" type="checkbox">
                          <label for="styleCheckbox3">Novelty</label>
                          <input id="styleCheckbox4" type="checkbox">
                          <label for="styleCheckbox4">PreppyStyle</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button class="filters-dropdown-block__show-results d-xl-none">Show results</button>
                </div>
                <!--.overlay(aria-label='Close', data-dismiss='modal', data-toggle='collapse', data-target='#filterSupportedContent', aria-expanded='true')-->
                <input type="hidden" name="code" value="">
                <input type="hidden" name="redirect" value="{{ redirect }}">
                <div class="form-group form-inline d-none">
                  <select id="sort" class="form-control form-control_select">
                    <option>Sort by</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                  <select id="brands" class="form-control form-control_select">
                    <option>Brands</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                  <select id="size" class="form-control form-control_select">
                    <option>Size</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                  <select id="color" class="form-control form-control_select">
                    <option>Color</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <input type="submit" name="send" value="send" class="d-none">
              </form>
              <button class="show-all-filters">Show all filters<i class="icon-down"></i></button>
              <div class="filters-selected">
                <ul class="filters-selected__list">
                  <li id="reset-btn" class="filters-selected__item">Reset all filters</li>
                  <li class="filters-selected__item">Abeer Al Suwadi<i class="icon-close"></i></li>
                  <li class="filters-selected__item">EUR 44<i class="icon-close"></i></li>
                  <li class="filters-selected__item">EUR 44<i class="icon-close"></i></li>
                  <li class="filters-selected__item">EUR 50<i class="icon-close"></i></li>
                  <li class="filters-selected__item"><i data-color="golden" class="icon-color"></i>Golden<i class="icon-close"></i></li>
                  <li class="filters-selected__item">Lowest price:&nbsp;<span>260&nbsp;</span>SAR<i class="icon-close"></i></li>
                  <li class="filters-selected__item">Highest price:&nbsp;<span>500&nbsp;</span>SAR<i class="icon-close"></i></li>
                  <li class="filters-selected__item">Cotton<i class="icon-close"></i></li>
                  <li class="filters-selected__item">Mini<i class="icon-close"></i></li>
                </ul>
              </div>
        </div>
        <div class="row">
          <div class="col-12">
            <h2 class="product-block__title product-block__title_smaller">New Arrivals</h2><a href="#" target="_blank" class="product-block__all">See all</a>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="product-block clearfix">
              <div class="product-block-items owl-theme">
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528189163-5831-1080x1200.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl in Oyster</a><a href="#" class="product-block-info__link_subtitle">Blue Lapis Beaded Bracelet with Hand of Fatima</a>
                    <p class="product-block-info__price">363.00&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1525069412-2584-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl in Oyster</a><a href="#" class="product-block-info__link_subtitle">Blue Lapis Beaded Bracelet with Hand of Fatima</a>
                    <p class="product-block-info__price">363.00&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1525380471-672-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl in Oyster</a><a href="#" class="product-block-info__link_subtitle">Blue Lapis Beaded Bracelet with Hand of Fatima</a>
                    <p class="product-block-info__price">363.00&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1527609535-6773-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl in Oyster</a><a href="#" class="product-block-info__link_subtitle">Blue Lapis Beaded Bracelet with Hand of Fatima</a>
                    <p class="product-block-info__price">363.00&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1527696574-0364-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl in Oyster</a><a href="#" class="product-block-info__link_subtitle">Blue Lapis Beaded Bracelet with Hand of Fatima</a>
                    <p class="product-block-info__price">363.00&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1527696618-5484-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl in Oyster</a><a href="#" class="product-block-info__link_subtitle">Blue Lapis Beaded Bracelet with Hand of Fatima</a>
                    <p class="product-block-info__price">363.00&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1527697012-7175-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl in Oyster</a><a href="#" class="product-block-info__link_subtitle">Blue Lapis Beaded Bracelet with Hand of Fatima</a>
                    <p class="product-block-info__price">363.00&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1527697686-1773-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Pearl in Oyster</a><a href="#" class="product-block-info__link_subtitle">Blue Lapis Beaded Bracelet with Hand of Fatima</a>
                    <p class="product-block-info__price">363.00&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <h2 class="product-block__title product-block__title_smaller">Selected Products</h2><a href="#" target="_blank" class="product-block__all">See all</a>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="product-block clearfix">
              <div class="product-block-items owl-theme">
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1494610220-540x600.jpg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1494610220-540x600.jpg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1494610568-540x600.jpg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1500317672-1080x1200.jpg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1508254517-540x600.jpg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1508255338-540x600.jpg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <h2 class="product-block__title product-block__title_smaller">Dresses</h2><a href="#" target="_blank" class="product-block__all">See all</a>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="product-block clearfix">
              <div class="product-block-items owl-theme">
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528189163-5831-1080x1200.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528293184-855-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528293218-518-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528293250-8642-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528293390-3016-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528293788-1807-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528294045-1631-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <h2 class="product-block__title product-block__title_smaller">Special Watches from Zyros</h2><a href="#" target="_blank" class="product-block__all">See all</a>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="product-block clearfix">
              <div class="product-block-items owl-theme">
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528190617-8999-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528190999-2213-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1517398822-9654-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528190617-8999-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1528190999-2213-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
                <div class="product-block__item">
                  <div class="product-block__image"><a href="#"><img src="catalog/view/javascript/home/assets/img/product_images/1517398822-9654-540x600.jpeg" alt="image_desc" width="" height=""></a></div>
                  <p class="product-block__discount-sum">-50%</p>
                  <div class="product-block-info"><a href="#" class="product-block-info__link_title">Hugo Boss</a><a href="#" class="product-block-info__link_subtitle">Men Boss Bottled Collector's Edition Eau de Toilette - 100ml</a>
                    <p class="product-block-info__price">128.97&nbsp;<span>AED</span></p>
                    <button class="product-block-info__btn"><img src="catalog/view/javascript/home/assets/img/ic-heart-line.svg" width="20" heigh="18" class="product-block-info__btn-icon"><img src="catalog/view/javascript/home/assets/img/ic-heart-full.svg" width="20" heigh="18" class="product-block-info__btn-icon active"></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>    
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript">
   mydir = $("html").attr("dir");

  if (mydir == 'rtl') {
      rtlVal=true
  }
  else {
      rtlVal=false
  }
  $('.shop-categories-carousel').owlCarousel({
        rtl:rtlVal,
        margin: 9,
        autoWidth: true,
        nav: true,
        responsive: {
            0: {
                items: 2
            },
            980: {
                items: 3
            },
            1170: {
                items: 4
            }
        }
    });
  $(document).ready(function(){
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 7500,
        values: [0, 2000],
        slide: function (event, ui) {
            // $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            $("#label-min").text(ui.values[0]);
            $("#label-max").text(ui.values[1]);
        }
    });
  })
</script>
<?php echo $footer; ?>
