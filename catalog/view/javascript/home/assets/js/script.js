// console.log("script connected");
//
// window.onload = function () {
//     console.log('script loaded');
// };

//when doc ready add captcha
$(document).ready(function () {
    console.log("document ready");

    //add active class to dropdowns
    $('.header-dropdown-block__selected').click(function () {
        $('.header-dropdown-block__selected').removeClass('active');
        $(this).addClass('active');
    });


    //for search field
    $('.search-btn').click(function () {
        $('.header-dropdown-block_search').toggleClass('active');
    });

    $('.header-dropdown-block__btn').click(function () {
        selected_Item = $(this).text();
        $(this).closest('.header-dropdown-block__selected').children('span').text(selected_Item);
        $('.header-dropdown-block__selected').removeClass('active');
        $('#searchFormControlInput').select();
    });

    //for detecting active class by click on dropdown menus
    jQuery(document).click(function (e) {
        var target = e.target; //target div recorded

        if (jQuery(target).closest('.header-dropdown-block__selected, .filters-dropdown-block__selected').is('.active')) {
            // console.log('Класс .active есть ');
            $(this).addClass('active');
        } else {
            // console.log('Класс .active нету ');
            $('.header-dropdown-block__selected, .filters-dropdown-block__selected').removeClass('active');
        }
    });

    $('.nav-item, #filter-btn').click(function () {
        $(this).toggleClass('active');
    });

    $(".nav-item").click(function () {
        $(".menu-header-block__dropdown").animate({
            display: "block"
        }, 200, function () {
            // Animation complete.
            $(this).toggleClass('active');
        });
    });

    //mobile back button
    $('h2.menu-header-block__title-back, button.icon-back').click(function () {
        // $(this).closest('.menu-header-block__dropdown').css('transform', 'translateX(105%)');
        // $(this).closest('.menu-header-block__dropdown').css('opacity', '0');
        $(this).closest('.filters-dropdown-block__selected').removeClass('active');
        $(this).closest('li').removeClass('diactive');
        $(this).closest('.header-dropdown-block').children().removeClass('active');
    });

    // Open overlay when button is clicked
    $('button.navbar-toggler, button.filters-dropdown-block__selected_filter-btn').on('click', function () {
        // $('#navbarSide').addClass('reveal');
        $('.overlay').show();
        $('.overlay').on('scroll mousewheel touchmove', stopScrolling);
    });

    $('.navbar-nav-scrolling, .filters-dropdown-block__scrolling-area').on("scroll mousewheel touchmove", function() {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
            // when scroll to bottom of the page
            $('body .navbar-nav-scrolling .container-fluid').on('scroll mousewheel touchmove', stopScrolling);
        }
    });

    // Close overlay when the outside of menu is clicked
    $('.overlay, .close, .icon-close').on('click', function () {
        // $('#navbarSide').removeClass('reveal');
        $('.overlay').hide();
    });

    function stopScrolling(e) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    }

    mydir = $("html").attr("dir");

    if (mydir == 'rtl') {
        rtlVal=true
    }
    else {
        rtlVal=false
    }

    //main Carousel slider
    $(".owl-carousel").owlCarousel({
        rtl:rtlVal,
        items: 1,
        lazyLoad: true,
        margin: 30,
        stagePadding: 30,
        smartSpeed: 450
    });

    //brands slider
    $('.brands-slider').owlCarousel({
        rtl:rtlVal,
        loop: false,
        nav: true,
        margin: 7,
        responsive: {
            0: {
                items: 3
            },
            1000: {
                items: 6
            }
        }
    });

    //products slider
    $('.product-block-items').owlCarousel({
        rtl:rtlVal,
        lazyLoad: true,
        loop: true,
        autoWidth: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 2
            },
            980: {
                items: 3
            },
            1170: {
                items: 4
            }
        }
    });

    //shop categories carousel
    $('.shop-categories-carousel').owlCarousel({
        rtl:rtlVal,
        margin: 9,
        autoWidth: true,
        nav: true,
        responsive: {
            0: {
                items: 2
            },
            980: {
                items: 3
            },
            1170: {
                items: 4
            }
        }
    });



    //favorite icon
    $('.product-block-info__btn').click(function () {
        if ($(this).hasClass('active')) {
            console.log('yes');
        } else {
            console.log('no');
        }
        $(this).children('.product-block-info__btn-icon').css('opacity', '1');
    });


    //RTL
    $('#form-language, #form-language2').click(function () {
        $('html').attr( "dir", "rtl");
        $(this).children().children('span').text('English');
        $('head').append('<link rel="stylesheet" href="assets/css/bootstrap.rtl.css">');
        $('head').append('<link rel="stylesheet" href="assets/css/bootstrap-grid.rtl.css">');
        $('#searchFormControlInput').attr('placeholder', 'أنا تسوق ل ...')

        // $('.test-row').find('.mr-0, .ml-auto').removeClass().addClass('ml-0 ml-auto');
        $('.stuff-block').parent('.pl-md-0').removeClass('pl-md-0');
    });


    //for slider range
    $(function () {
       /* $("#slider-range").slider({
            range: true,
            min: 0,
            max: 7500,
            values: [0, 2000],
            slide: function (event, ui) {
                // $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                $("#label-min").text(ui.values[0]);
                $("#label-max").text(ui.values[1]);
            }
        });*/
        // $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
        //     " - $" + $( "#slider-range" ).slider( "values", 1 ) );
        // $("#label-min").val($( "#slider-range" ).slider( "values", 0 ));
        // $("#label-min").text($( "#slider-range" ).slider( "values", 0 ));
    });


    // show filters

    var selectedFilterDropdown = $(this).find('span').text();

    $('.filters-dropdown-block__selected').click(function () {
        $('.filters-dropdown-block__selected').removeClass('active');
        $(this).addClass('active');
    });

    $('.show-all-filters').click(function () {
        $('.filters-dropdown-block').toggleClass('active');
        $(this).toggleClass('active');
        $(this).next('.filters-selected').toggleClass('active');
    });


    //filters menu
    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');

        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
            $('.dropdown-submenu .show').removeClass("show");
        });


        return false;
    });

    //reset filters

    $('#reset-btn').click(function () {
        $(this).nextAll('.filters-selected__item').remove();
        $('label').prev().prop("checked", false);
    });

    $('.icon-close').on('click', '',function () {
        $(this).parent('.filters-selected__item').remove();
    });

    $(document).on('click', '.icon-close', function(e) {
        deletingElement = $(this).parent('.filters-selected__item').text();
        console.log(deletingElement);
        // $( "label:contains(deletingElement)").css( "border", "solid 1px red" );
        $( "label:contains(deletingElement)").prev().prop("checked", false);

        $(this).parent('.filters-selected__item').remove();
    });

    //send filters to all checked filters area

    $(':checkbox').click(function () {
        value = $(this).next().text();
        colorValue = $(this).next().attr("data-color");

        if ($(this).prop( "checked" ) ) {
            var className = $(this).next().attr('class');
            switch (className) {
                case 'color':
                    // console.log('hasClass: ' + className);
                    $('.filters-selected__list').append('<li class="filters-selected__item"><i data-color="'+ colorValue +'" class="icon-color"></i>'+ value +'<i class="icon-close"></i></li>');
                    break;
                default:
                    // console.log(className);
                    $('.filters-selected__list').append('<li class="filters-selected__item">'+ value +'<i class="icon-close"></i></li>');
            }
        } else {
            $('.filters-selected__list > .filters-selected__item').remove(':last-child');
        }
    });

    $('.filters-dropdown-block__size').click(function () {
        value = $(this).text();
        taglieName =  $('.nav-tabs').children().find('.active').text();
        // console.log(taglieName);
        $('.filters-selected__list').append('<li class="filters-selected__item">'+taglieName+ ' ' + value +'<i class="icon-close"></i></li>');
    });

    $('.filters-dropdown-block__size').click(function () {
        // $('.nav-link').removeClass('active');
    });

    $('.filters-dropdown-block__dropdown div.nav-link').click(function () {
        value = $(this).text();
        $('.filters-selected__list').append('<li class="filters-selected__item">'+ value +'<i class="icon-close"></i></li>');
    });
    $("#up").click(function () {
        $("html,body").animate({scrollTop: 0}, "slow");
        return false;
    });
});





//quick search in brands, took from w3school, fixed 1 stroke
function searchInBrands() {
    // Declare variables
    var input, filter, labelsAll, li, a, i;
    input = document.getElementById('brandsInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("brandsList");
    li = ul.getElementsByTagName('label');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        // a = li[i].getElementsByTagName("a")[0];
        if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function searchInColors() {
    // Declare variables
    var input, filter, labelsAll, li, i;
    input = document.getElementById('colorsInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("colorsList");
    li = ul.getElementsByTagName('label');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

// $(window).load(function() {
//up button
if ($(window).scrollTop() >= "250") $("#up").fadeIn("slow");
$(window).scroll(function () {
    if ($(window).scrollTop() <= "250") $("#up").fadeOut("slow")
    else $("#up").fadeIn("slow");
});

// });



