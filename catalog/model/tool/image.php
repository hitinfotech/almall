<?php
/*class ModelToolImage extends Model {
	public function resize($filename, $width, $height) {
		if (!is_file(DIR_IMAGE . $filename)) {
			return;
		}

		$extension = pathinfo($filename, PATHINFO_EXTENSION);

		$old_image = $filename;
		$new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(DIR_IMAGE . $new_image) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
			$path = '';

			$directories = explode('/', dirname(str_replace('../', '', $new_image)));

			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;

				if (!is_dir(DIR_IMAGE . $path)) {
					@mkdir(DIR_IMAGE . $path, 0777);
				}
			}

			list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

			if ($width_orig != $width || $height_orig != $height) {
				$image = new Image(DIR_IMAGE . $old_image);
				$image->resize($width, $height);
				$image->save(DIR_IMAGE . $new_image);
			} else {
				copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
			}
		}

		if ($this->request->server['HTTPS']) {
			return $this->config->get('config_ssl') . 'image/' . $new_image;
		} else {
			return $this->config->get('config_url') . 'image/' . $new_image;
		}
	}
}*/


class ModelToolImage extends Model {

    public function resize($filename, $width, $height, $force = false) {
        //$this->uploadImage($filename,$width,$height,true);
        $old_image = $filename;
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
        
        if (is_file(DIR_IMAGE . $filename)) {
           if (!is_file(DIR_IMAGE . $new_image) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
                $path = '';

                $directories = explode('/', dirname(str_replace('../', '', $new_image)));

                foreach ($directories as $directory) {
                    $path = $path . '/' . $directory;

                    if (!is_dir(DIR_IMAGE . $path)) {
                        @mkdir(DIR_IMAGE . $path, 0777);
                    }
                }

                list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

                if ($width_orig != $width || $height_orig != $height) {
                    $image = new Imagenewsletter(DIR_IMAGE . $old_image);
                    $image->resize($width, $height);
                    $image->save(DIR_IMAGE . $new_image);
                } else {
                    copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
                }
            }
        }
        if ($this->request->server['HTTPS']) {
            $imgg = $this->config->get('config_ssl') . 'image/' . $new_image;
        } else {
            $imgg = $this->config->get('config_url') . 'image/' . $new_image;
        }
        if (!is_file(DIR_IMAGE . $new_image) && defined('HTTPS_IMAGE_S3')) {
            return HTTPS_IMAGE_S3 . $new_image;
        } else {
        	return $imgg;
        }
    }

    public function uploadImage($filename, $width, $height, $upload = true) {
        if (!is_file(DIR_IMAGE . $filename)) {
            return;
        }
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $old_image = $filename;
        $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
        if ($upload) {
            $this->db->query("INSERT INTO image_s3 SET image='" . $this->db->escape($old_image) . "' ,
                                                   new_image_path='" . $this->db->escape($new_image) . "' ,
                                                   width='" . $this->db->escape($width) . "' ,
                                                   height='" . $this->db->escape($height) . "' , 
                                                   date_added=NOW(), user_id=NULL ");
            //  return HTTPS_SERVER . (ltrim(DIR_IMAGE, DIR_ROOT)) . $new_image;
            return HTTPS_SERVER . substr(DIR_IMAGE, strlen(DIR_ROOT)) . $new_image;
        } else {
            $this->db->query("INSERT INTO image_s3 SET image='" . $this->db->escape($old_image) . "' ,
                                                   new_image_path='" . $this->db->escape($new_image) . "' ,
                                                   width='" . $this->db->escape($width) . "' ,
                                                   height='" . $this->db->escape($height) . "' , 
                                                   upload_image='0', 
                                                   date_added=NOW(), user_id=NULL ");
        }
    }

    public function ne_newsletter($filename, $width, $height) {
        return $this->newresize($filename, $width, $height);
    }

    public function newresize($filename, $width, $height) {
        return $this->resize($filename, $width, $height);
        if (!is_file(DIR_IMAGE . $filename)) {
            return;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $old_image = $filename;
        $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (!is_file(DIR_IMAGE . $new_image) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
            $path = '';

            $directories = explode('/', dirname(str_replace('../', '', $new_image)));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir(DIR_IMAGE . $path)) {
                    @mkdir(DIR_IMAGE . $path, 0777);
                }
            }

            list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

            if ($width_orig != $width || $height_orig != $height) {
                $image = new Imagenewsletter(DIR_IMAGE . $old_image);
                $image->resize($width, $height);
                $image->save(DIR_IMAGE . $new_image);
            } else {
                copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
            }
        }

        if ($this->request->server['HTTPS']) {
            return $this->config->get('config_ssl') . 'image/' . $new_image;
        } else {
            return $this->config->get('config_url') . 'image/' . $new_image;
        }
    }

}

