<?php

header('Content-Type: text/html; charset=UTF-8');
// Version
define('VERSION', '2.1.0.1');
define('ENVIROMENT', getenv('ENVIROMENT'));

// Configuration
require_once('config/config.php');

// Install
if (!defined('DIR_APPLICATION')) {
    header('Location: ../install/index.php');
    exit;
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

// Registry
$registry = Registry::getInstance();

// Config
$config = Config::getInstance();
$registry->set('config', $config);

// Database
$db = DB::getInstance(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
//$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
$registry->set('db', $db);

// Settings
$query = $db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0'");

foreach ($query->rows as $setting) {
    if (!$setting['serialized']) {
        $config->set($setting['key'], $setting['value']);
    } else {
        $config->set($setting['key'], json_decode($setting['value'], true));
    }
}

// Loader
$loader = Loader::getInstance($registry);
$registry->set('load', $loader);

// Url
$url = new Url(HTTP_SERVER, $config->get('config_secure') ? HTTPS_SERVER : HTTP_SERVER);
$registry->set('url', $url);

// Log
$log = new Log($config->get('config_error_filename'));
$registry->set('log', $log);

function error_handler($code, $message, $file, $line) {
    global $log, $config;

    // error suppressed with @
    if (error_reporting() === 0) {
        return false;
    }

    switch ($code) {
        case E_NOTICE:
        case E_USER_NOTICE:
            $error = 'Notice';
            break;
        case E_WARNING:
        case E_USER_WARNING:
            $error = 'Warning';
            break;
        case E_ERROR:
        case E_USER_ERROR:
            $error = 'Fatal Error';
            break;
        default:
            $error = 'Unknown';
            break;
    }

    if ($config->get('config_error_display')) {
        echo '<b>' . $error . '</b>: ' . $message . ' in <b>' . $file . '</b> on line <b>' . $line . '</b>';
    }

    if ($config->get('config_error_log')) {
        $log->write('PHP ' . $error . ':  ' . $message . ' in ' . $file . ' on line ' . $line);
    }

    return true;
}

// Error Handler
set_error_handler('error_handler');

// Request
$request = new Request();
$registry->set('request', $request);

// Response
$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$registry->set('response', $response);

// Cache
$cache = Cache::getInstance(CONFIG_CACHE);
$registry->set('cache', $cache);

// Session
$session = Session::getInstance();
$registry->set('session', $session);

if (isset($session->data['token'])){
    $request->get['token'] = $session->data['token'];
}

// Language
$languages = array();
$query = $db->query("SELECT * FROM `" . DB_PREFIX . "language`");

foreach ($query->rows as $result) {
    $languages[$result['code']] = $result;
}

$config->set('config_language_id', $languages[$config->get('config_admin_language')]['language_id']);

// Language
$language = new Language($languages[$config->get('config_admin_language')]['directory']);
$language->load($languages[$config->get('config_admin_language')]['directory']);
$registry->set('language', $language);

// Document
$registry->set('document', new Document());

// AWS_S3
$registry->set('aws_s3', new Aws_S3($registry));
/*
//// First Flight
$registry->set('FFServices', new FirstFlight($registry, SHIPPING_FIRST_FLIGHT_ID));
//
//// Naqel
$registry->set('NaqelServices', new Naqel($registry, SHIPPING_NAQEL_ID));

//SamsaShipping
$registry->set('SamsaShipping', new Samsashipping($registry, SHIPPING_SMSA_ID));

//PostPlus
$registry->set('PostPlus',new Postplus($registry,SHIPPING_POST_PLUS_ID));
*/
// Currency
$registry->set('currency', new Currency($registry));

// Weight
$registry->set('weight', new Weight($registry));

// Custom URL
$registry->set('url_custom', new Url_custom($registry));

// Length
$registry->set('length', new Length($registry));

// Algolia
$registry->set('algolia', new AlgoIndexer($registry));

// Dynamic Values
$registry->set('dynamic_values', new DynamicValues($registry));

// User
$registry->set('user', new User($registry));

// OpenBay Pro
$registry->set('openbay', new Openbay($registry));

$country = new Country($registry);
$registry->set('country', $country);

// Event
$event = new Event($registry);
$registry->set('event', $event);

//Common function
$registry->set('commonfunctions', new commonfunctions($registry));


$query = $db->query("SELECT * FROM " . DB_PREFIX . "event");

foreach ($query->rows as $result) {
    $event->register($result['trigger'], $result['action']);
}

// Front Controller
$controller = new Front($registry);

// Compile Sass
$controller->addPreAction(new Action('common/sass'));

// Login
$controller->addPreAction(new Action('common/login/check'));

// Permission
$controller->addPreAction(new Action('error/permission/check'));

// Router
if (isset($request->get['route'])) {
    $action = new Action($request->get['route']);
} else {
    if (!$registry->get('user')->hasPermission('access','common/dashboard')){
        $action = new Action('common/login/blank');
    } else {
        $action = new Action('common/dashboard');
    }
}

// Dispatch
$controller->dispatch($action, new Action('error/not_found'));

// Output
$response->output();
