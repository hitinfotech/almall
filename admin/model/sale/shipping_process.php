<?php

class ModelSaleShippingProcess extends Model {

    public function getOrder($order_id) {
        $order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int) $order_id . "'  AND o.deleted='0' ");

        if ($order_query->num_rows) {
            $reward = 0;

            $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int) $order_id . "'");

            foreach ($order_product_query->rows as $product) {
                $reward += $product['reward'];
            }

            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int) $order_query->row['payment_country_id'] . "'");

            if ($country_query->num_rows) {
                $payment_iso_code_2 = $country_query->row['iso_code_2'];
                $payment_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $payment_iso_code_2 = '';
                $payment_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int) $order_query->row['payment_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $payment_zone_code = $zone_query->row['code'];
            } else {
                $payment_zone_code = '';
            }

            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int) $order_query->row['shipping_country_id'] . "'");

            if ($country_query->num_rows) {
                $shipping_iso_code_2 = $country_query->row['iso_code_2'];
                $shipping_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $shipping_iso_code_2 = '';
                $shipping_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int) $order_query->row['shipping_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $shipping_zone_code = $zone_query->row['code'];
            } else {
                $shipping_zone_code = '';
            }

            $shipping_companies = $this->db->query("SELECT DISTINCT shipping_company FROM `" . DB_PREFIX . "order_booking` WHERE order_id = '" . (int) $order_id . "'");

            if ($shipping_companies->num_rows) {
                $shipping_companies = $shipping_companies->rows;
            } else {
                $shipping_companies = array();
            }

            if ($order_query->row['affiliate_id']) {
                $affiliate_id = $order_query->row['affiliate_id'];
            } else {
                $affiliate_id = 0;
            }

            $this->load->model('marketing/affiliate');

            $affiliate_info = $this->model_marketing_affiliate->getAffiliate($affiliate_id);

            if ($affiliate_info) {
                $affiliate_firstname = $affiliate_info['firstname'];
                $affiliate_lastname = $affiliate_info['lastname'];
            } else {
                $affiliate_firstname = '';
                $affiliate_lastname = '';
            }

            $this->load->model('localisation/language');

            $language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

            if ($language_info) {
                $language_code = $language_info['code'];
                $language_directory = $language_info['directory'];
            } else {
                $language_code = '';
                $language_directory = '';
            }

            return array(
                'order_id' => $order_query->row['order_id'],
                'invoice_no' => $order_query->row['invoice_no'],
                'invoice_prefix' => $order_query->row['invoice_prefix'],
                'store_id' => $order_query->row['store_id'],
                'store_name' => $order_query->row['store_name'],
                'store_url' => $order_query->row['store_url'],
                'customer_id' => $order_query->row['customer_id'],
                'customer' => $order_query->row['customer'],
                'customer_group_id' => $order_query->row['customer_group_id'],
                'firstname' => $order_query->row['firstname'],
                'lastname' => $order_query->row['lastname'],
                'email' => $order_query->row['email'],
                'telephone' => $order_query->row['telephone'],
                'fax' => $order_query->row['fax'],
                'custom_field' => json_decode($order_query->row['custom_field'], true),
                'payment_firstname' => $order_query->row['payment_firstname'],
                'payment_lastname' => $order_query->row['payment_lastname'],
                'payment_company' => $order_query->row['payment_company'],
                'payment_address_1' => $order_query->row['payment_address_1'],
                'payment_address_2' => $order_query->row['payment_address_2'],
                'payment_postcode' => $order_query->row['payment_postcode'],
                'payment_city' => $order_query->row['payment_city'],
                'payment_zone_id' => $order_query->row['payment_zone_id'],
                'payment_zone' => $order_query->row['payment_zone'],
                'payment_zone_code' => $payment_zone_code,
                'payment_country_id' => $order_query->row['payment_country_id'],
                'payment_country' => $order_query->row['payment_country'],
                'payment_iso_code_2' => $payment_iso_code_2,
                'payment_iso_code_3' => $payment_iso_code_3,
                'payment_address_format' => $order_query->row['payment_address_format'],
                'payment_custom_field' => json_decode($order_query->row['payment_custom_field'], true),
                'payment_method' => $order_query->row['payment_method'],
                'payment_code' => $order_query->row['payment_code'],
                'shipping_firstname' => $order_query->row['shipping_firstname'],
                'shipping_lastname' => $order_query->row['shipping_lastname'],
                'shipping_company' => $order_query->row['shipping_company'],
                'shipping_address_1' => $order_query->row['shipping_address_1'],
                'shipping_address_2' => $order_query->row['shipping_address_2'],
                'shipping_postcode' => $order_query->row['shipping_postcode'],
                'shipping_city' => $order_query->row['shipping_city'],
                'shipping_zone_id' => $order_query->row['shipping_zone_id'],
                'shipping_zone' => $order_query->row['shipping_zone'],
                'shipping_zone_code' => $shipping_zone_code,
                'shipping_country_id' => $order_query->row['shipping_country_id'],
                'shipping_country' => $order_query->row['shipping_country'],
                'shipping_iso_code_2' => $shipping_iso_code_2,
                'shipping_iso_code_3' => $shipping_iso_code_3,
                'shipping_address_format' => $order_query->row['shipping_address_format'],
                'shipping_custom_field' => json_decode($order_query->row['shipping_custom_field'], true),
                'shipping_method' => $order_query->row['shipping_method'],
                'shipping_code' => $order_query->row['shipping_code'],
                'comment' => $order_query->row['comment'],
                'total' => $order_query->row['total'],
                'reward' => $reward,
                'order_status_id' => $order_query->row['order_status_id'],
                'affiliate_id' => $order_query->row['affiliate_id'],
                'affiliate_firstname' => $affiliate_firstname,
                'affiliate_lastname' => $affiliate_lastname,
                'commission' => $order_query->row['commission'],
                'language_id' => $order_query->row['language_id'],
                'language_code' => $language_code,
                'language_directory' => $language_directory,
                'currency_id' => $order_query->row['currency_id'],
                'currency_code' => $order_query->row['currency_code'],
                'currency_value' => $order_query->row['currency_value'],
                'ip' => $order_query->row['ip'],
                'forwarded_ip' => $order_query->row['forwarded_ip'],
                'user_agent' => $order_query->row['user_agent'],
                'accept_language' => $order_query->row['accept_language'],
                'shipping_companies' => $shipping_companies,
                'date_added' => $order_query->row['date_added'],
                'date_modified' => $order_query->row['date_modified']
            );
        } else {
            return;
        }
    }

    public function getOrderCount ($order_id){
        $sql="SELECT COUNT(*) AS total FROM " . DB_PREFIX . " customerpartner_to_order WHERE order_id = '" . (int) $order_id . "'  AND deleted='0'  AND order_product_status NOT IN (10, 7);";
         $query = $this->db->query($sql);

          return $query->row['total'];
    }

    public function getOrders($data = array()) {
        $sql = "SELECT o.order_id, CONCAT(o.firstname, ' ', o.lastname) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int) $this->config->get('config_language_id') . "') AS status, o.shipping_code, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified FROM `" . DB_PREFIX . "order` o";

        if (isset($data['filter_order_status'])) {
            $implode = array();

            $order_statuses = explode(',', $data['filter_order_status']);

            foreach ($order_statuses as $order_status_id) {
                $implode[] = "o.order_status_id = '" . (int) $order_status_id . "'";
            }

            if ($implode) {
                $sql .= " WHERE (" . implode(" OR ", $implode) . ")";
            }
        } else {
            $sql .= " WHERE o.order_status_id > '0'";
        }

        $sql.= "  AND o.deleted='0'  ";

        if (!empty($data['filter_order_id'])) {
            $sql .= " AND o.order_id = '" . (int) $data['filter_order_id'] . "'";
        }

        if (!empty($data['filter_customer'])) {
            $sql .= " AND CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if (!empty($data['filter_date_modified'])) {
            $sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
        }

        if (!empty($data['filter_total'])) {
            $sql .= " AND o.total = '" . (float) $data['filter_total'] . "'";
        }

        $sort_data = array(
            'o.order_id',
            'customer',
            'status',
            'o.date_added',
            'o.date_modified',
            'o.total'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY o.order_id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }
    public function getOrderProducts($order_id) {

        $query = $this->db->query("SELECT c2o.order_product_id, p.model, c2o.quantity, op.price, op.total, p.sku, p.product_id, pd.name,c2c.customer_id,CONCAT(c2c.firstname , ' ' , c2c.lastname) as seller_name,c2c.country_id,cd.name as seller_country FROM " . DB_PREFIX . "customerpartner_to_order c2o LEFT JOIN order_product op ON (c2o.order_product_id = op.order_product_id) LEFT JOIN product p ON (op.product_id=p.product_id) LEFT JOIN product_description pd ON(op.product_id=pd.product_id AND pd.language_id='" . (int) $this->config->get('config_language_id') . "') LEFT JOIN customerpartner_to_customer c2c ON (c2o.customer_id = c2c.customer_id) LEFT JOIN country_description cd ON (c2c.country_id = cd.country_id AND cd.language_id='".(int) $this->config->get('config_language_id') ."') WHERE c2o.order_id = '" . (int) $order_id . "'  AND c2o.deleted='0'  AND c2o.order_product_status <> '".(int)$this->config->get('marketplace_cancel_order_status')."' AND op.deleted='0' ");
        return $query->rows;
    }

    public function getOrderOptions($order_id, $order_product_id) {
        //$query = $this->db->query("SELECT oo.* FROM " . DB_PREFIX . "order_option oo WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . (int) $order_product_id . "'");
        $query = $this->db->query(" SELECT oo.*, op.product_id, od.front_name FROM order_option oo left join order_product op on (oo.order_product_id=op.order_product_id) left join product_option po on(oo.product_option_id=po.product_option_id) left join option_description od on(po.option_id=od.option_id and od.language_id=1) WHERE po.deleted !='1' AND oo.order_id = '" . (int) $order_id . "'  AND oo.deleted='0' AND oo.order_product_id = '" . (int) $order_product_id . "' ORDER BY oo.order_option_id ASC");

        return $query->rows;
    }

    public function getOrderVouchers($order_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int) $order_id . "'  AND deleted='0' ");

        return $query->rows;
    }

    public function getOrderVoucherByVoucherId($voucher_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_voucher` WHERE voucher_id = '" . (int) $voucher_id . "'  AND deleted='0'");

        return $query->row;
    }

    public function getOrderTotals($order_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int) $order_id . "' ORDER BY sort_order");

        return $query->rows;
    }

    public function getTotalOrders($data = array()) {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order`";

        if (isset($data['filter_order_status'])) {
            $implode = array();

            $order_statuses = explode(',', $data['filter_order_status']);

            foreach ($order_statuses as $order_status_id) {
                $implode[] = "order_status_id = '" . (int) $order_status_id . "'";
            }

            if ($implode) {
                $sql .= " WHERE (" . implode(" OR ", $implode) . ")";
            }
        } else {
            $sql .= " WHERE order_status_id > '0'";
        }
        $sql .= "  AND deleted='0'  ";

        if (!empty($data['filter_order_id'])) {
            $sql .= " AND order_id = '" . (int) $data['filter_order_id'] . "'";
        }

        if (!empty($data['filter_customer'])) {
            $sql .= " AND CONCAT(firstname, ' ', lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if (!empty($data['filter_date_modified'])) {
            $sql .= " AND DATE(date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
        }

        if (!empty($data['filter_total'])) {
            $sql .= " AND total = '" . (float) $data['filter_total'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalOrdersByStoreId($store_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE store_id = '" . (int) $store_id . "'  AND deleted='0' ");

        return $query->row['total'];
    }

    public function getTotalOrdersByOrderStatusId($order_status_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id = '" . (int) $order_status_id . "' AND order_status_id > '0'  AND deleted='0' ");

        return $query->row['total'];
    }

    public function getTotalOrdersByProcessingStatus() {
        $implode = array();

        $order_statuses = $this->config->get('config_processing_status');

        foreach ($order_statuses as $order_status_id) {
            $implode[] = "order_status_id = '" . (int) $order_status_id . "'";
        }

        if ($implode) {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode) ."  AND o.deleted='0' ");

            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function getTotalOrdersByCompleteStatus() {
        $implode = array();

        $order_statuses = $this->config->get('config_complete_status');

        foreach ($order_statuses as $order_status_id) {
            $implode[] = "order_status_id = '" . (int) $order_status_id . "'";
        }

        if ($implode) {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode) . "  AND deleted='0' ");

            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function getTotalOrdersByLanguageId($language_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE language_id = '" . (int) $language_id . "' AND order_status_id > '0'  AND deleted='0' ");

        return $query->row['total'];
    }

    public function getTotalOrdersByCurrencyId($currency_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE currency_id = '" . (int) $currency_id . "' AND order_status_id > '0'  AND deleted='0' ");

        return $query->row['total'];
    }

    public function createInvoiceNo($order_id) {
        $order_info = $this->getOrder($order_id);

        if ($order_info && !$order_info['invoice_no']) {
            $query = $this->db->query("SELECT MAX(invoice_no) AS invoice_no FROM `" . DB_PREFIX . "order` WHERE invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "'  AND deleted='0' ");

            if ($query->row['invoice_no']) {
                $invoice_no = $query->row['invoice_no'] + 1;
            } else {
                $invoice_no = 1;
            }

            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_no = '" . (int) $invoice_no . "', invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "' WHERE order_id = '" . (int) $order_id . "'  AND deleted='0' ");

            return $order_info['invoice_prefix'] . $invoice_no;
        }
    }

    public function getOrderHistories($order_id, $start = 0, $limit = 10) {
        if ($start < 0) {
            $start = 0;
        }

        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.order_id = '" . (int) $order_id . "' AND os.language_id = '" . (int) $this->config->get('config_language_id') . "'   AND oh.deleted='0'  ORDER BY oh.date_added DESC LIMIT " . (int) $start . "," . (int) $limit);

        return $query->rows;
    }

    public function getTotalOrderHistories($order_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_id = '" . (int) $order_id . "'   AND deleted='0' ");

        return $query->row['total'];
    }

    public function getTotalOrderHistoriesByOrderStatusId($order_status_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_status_id = '" . (int) $order_status_id . "'   AND deleted='0' ");

        return $query->row['total'];
    }

    public function getEmailsByProductsOrdered($products, $start, $end) {
        $implode = array();

        foreach ($products as $product_id) {
            $implode[] = "op.product_id = '" . (int) $product_id . "'";
        }

        $query = $this->db->query("SELECT DISTINCT email FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0'  AND o.deleted='0'  LIMIT " . (int) $start . "," . (int) $end);

        return $query->rows;
    }

    public function getTotalEmailsByProductsOrdered($products) {
        $implode = array();

        foreach ($products as $product_id) {
            $implode[] = "op.product_id = '" . (int) $product_id . "'";
        }

        $query = $this->db->query("SELECT DISTINCT email FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0'  AND o.deleted='0' ");

        return $query->row['total'];
    }

    public function getOrderProductsforShipment($order_id) {
        $query = $this->db->query("SELECT op.order_product_id, op.order_id, op.product_id, op.name ,op.quantity ,op.price, op.total, op.tax, op.reward, IFNULL(cp2p.customer_id,0) AS customer_id, CONCAT(c.firstname,' ',c.lastname) AS seller_name,p.model FROM order_product op LEFT JOIN customerpartner_to_product cp2p on (op.product_id=cp2p.product_id) LEFT JOIN product p on (op.product_id=p.product_id) LEFT JOIN customer c ON (cp2p.customer_id = c.customer_id) WHERE order_id = '" . (int) $order_id . "' ORDER BY seller_name ");

        return $query->rows;
    }

    public function check_products_seller($product_ids, $seller_id) {
        $query = $this->db->query("SELECT product_id FROM customerpartner_to_product WHERE customer_id='" . $seller_id . "' and product_id in (" . $product_ids . ")");

        return $query->rows;
    }

    public function check_shipment($order_id) {
        $query = $this->db->query("SELECT count(*) as count FROM order_shipping WHERE order_id='" . $order_id . "'");

        return $query->row['count'];
    }

    public function get_destination_code($zone_id) {
        $query = $this->db->query("SELECT destination_code FROM firstflight_destination_code where zone_id='" . $zone_id . "' ");
        return $query->rows[0];
    }

    public function add_shipment($data = array()) {
        /* $sql = "INSERT INTO order_shipping SET date_added = NOW() , date_modified= NOW() ";

          if (!empty($data)) {
          foreach ($data as $key => $value) {
          $sql .= "$key = '$value' , ";
          }

          $this->db->query($sql);
          } */
    }

    public function add_booking($data = array()) {
        $sql = "INSERT INTO order_booking SET ";

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $sql .= "$key = '$value' , ";
            }
            $sql .= "date_added = NOW() , date_modified= NOW()";

            $this->db->query($sql);
        }
        $id = $this->db->getLastId();
        $this->db->query("UPDATE order_booking set AWBNO='" . $data['AWBNO'] . "_$id" . "' where order_booking_id='$id'");
    }

    public function get_booking_count($awb_no) {
        $query = $this->db->query("SELECT count(*) as count FROM order_booking where AWBNO LIKE '%$awb_no%'");
        return $query->row['count'];
    }

    public function getAWBNo($order_id) {
        $query = $this->db->query("SELECT AWBNO FROM order_shipping WHERE order_id='" . (int) $order_id . "'");
        return isset($query->row['AWBNO']) ? $query->row['AWBNO'] : "";
    }

    public function getSeller($seller_id){
      $query = $this->db->query("SELECT c2cd.companyname,c2cd.screenname,cd.name as country_name,zd.name as zone_name,c2c.warehouse_address,c2c.opening_time,c2c.telephone  FROM customerpartner_to_customer c2c  LEFT JOIN customerpartner_to_customer_description c2cd ON (c2c.customer_id=c2cd.customer_id) LEFT JOIN country_description cd ON (c2c.country_id = cd.country_id AND cd.language_id='".(int) $this->config->get('config_language_id') ."') LEFT JOIN zone_description zd ON (c2c.zone_id = zd.zone_id AND zd.language_id='".(int) $this->config->get('config_language_id') ."') where c2c.customer_id='".(int) $seller_id ."'");
      return $query->row;
    }

    public function getProductInfo($product_id){
      $query = $this->db->query("SELECT pd.name,p.model,p.sku,c2o.quantity,c2o.price FROM customerpartner_to_order c2o LEFT JOIN product p ON (c2o.product_id = p.product_id) LEFT JOIN product_description pd ON (p.product_id = pd.product_id AND pd.language_id='".(int)$this->config->get('config_language_id')."') WHERE p.product_id='".(int) $product_id."'  AND c2o.deleted='0' ");
      return $query->row;
    }

    public function getSellerOrderProducts($order_id,$customer_id) {

        $sql = $this->db->query("SELECT op.*,c2o.price c2oprice,p.sku FROM " . DB_PREFIX . "customerpartner_to_order c2o LEFT JOIN " . DB_PREFIX . "order_product op ON (c2o.order_product_id = op.order_product_id AND c2o.order_id = op.order_id) LEFT JOIN product p ON (c2o.product_id=p.product_id)  WHERE c2o.order_id = '" . (int)$order_id . "'  AND c2o.deleted='0'   AND c2o.customer_id = '" . (int)$customer_id . "' ORDER BY op.product_id ");

        return($sql->rows);
    }

    public function getOrderClosedCountries($order_id){
      $query = $this->db->query("SELECT country_id FROM order_booking WHERE order_id = '".(int)$order_id."' AND is_closed='1'");
      return $query->rows;
    }

    public function getReturn($id){
      $query = $this->db->query("SELECT wro.id,wro.order_id,wrp.product_id,is_closed,wro.customer_id FROM wk_rma_order wro LEFT JOIN wk_rma_product wrp ON (wro.id = wrp.rma_id) LEFT JOIN return_booking rb ON (wro.id=rb.return_id) WHERE wro.id='".(int) $id."'");
      return $query->row ;
    }

    public function getOrderProduct($order_id,$product_id) {
        $query = $this->db->query("SELECT c2o.order_product_id, p.model, c2o.quantity, op.price, op.total, p.sku, p.product_id, pd.name,c2c.customer_id,CONCAT(c2c.firstname , ' ' , c2c.lastname) as seller_name,c2c.country_id,cd.name as seller_country FROM " . DB_PREFIX . "customerpartner_to_order c2o LEFT JOIN order_product op ON (c2o.order_product_id = op.order_product_id) LEFT JOIN product p ON (op.product_id=p.product_id) LEFT JOIN product_description pd ON(op.product_id=pd.product_id AND pd.language_id='" . (int) $this->config->get('config_language_id') . "') LEFT JOIN customerpartner_to_customer c2c ON (c2o.customer_id = c2c.customer_id) LEFT JOIN country_description cd ON (c2c.country_id = cd.country_id AND cd.language_id='".(int) $this->config->get('config_language_id') ."') WHERE c2o.order_id = '" . (int) $order_id . "'  AND c2o.deleted='0'  AND c2o.product_id='".(int)$product_id."'");

        return $query->row;
    }


}
