<?php
class ModelShippingShipping extends Model {

    public function getShippingCompanies(){
      $query = $this->db->query("SELECT sc.ship_company_id,scd.name FROM ship_company sc LEFT JOIN ship_company_description scd ON (sc.ship_company_id=scd.ship_company_id AND scd.language_id='".(int) $this->config->get('config_language_id') ."') WHERE sc.status = '1'");
      return $query->rows ;
    }

    public function getShippingCompaniesForOrder($order_id){
        $query = $this->db->query("SELECT DISTINCT SD.name
                                        FROM order_booking OB 
                                        LEFT JOIN ship_company SC ON OB.shipping_company=SC.ship_company_id
                                        LEFT JOIN ship_company_description SD ON OB.shipping_company=SD.ship_company_id 
                                        WHERE SD.language_id='".(int) $this->config->get('config_language_id') ."' 
                                        AND SC.status = '1'
                                        AND OB.order_id = '". (int )$order_id ."' ");
        return $query->rows ;
    }
}