<?php

class ModelCustomerpartnerOrdertracking extends Model {


    public function getOrders($data = array()) {
        $sql = "SELECT c2o.order_id,order_product_status as status,c2o.date_added, CONCAT(c2c.firstname,' ', c2c.lastname) AS seller_name , CONCAT(o.firstname,' ' , o.lastname) as customer_name,ob.book_response as awbno,os.name as order_status FROM customerpartner_to_order c2o LEFT JOIN customerpartner_to_customer c2c ON (c2o.customer_id=c2c.customer_id) LEFT JOIN customerpartner_to_customer_description c2cd ON (c2o.customer_id=c2cd.customer_id AND c2cd.language_id='".(int)$this->config->get('config_language_id')."') LEFT JOIN `order` o ON (c2o.order_id = o.order_id) LEFT JOIN `order_status` os ON (order_product_status = os.order_status_id and os.language_id='".(int) $this->config->get('config_language_id') ."')  LEFT JOIN order_booking ob ON (c2o.order_id = ob.order_id and c2o.customer_id=ob.seller_id) WHERE o.order_status_id <> 0  AND c2o.deleted='0' AND o.deleted='0' ";

        if (!empty($data['filter_order_id'])) {
            $sql .= " AND c2o.order_id = '" . (int) $data['filter_order_id'] . "'";
        }

        if (!empty($data['filter_seller'])) {
            $sql .= " AND CONCAT(c2cd.companyname, ' ', c2cd.screenname) LIKE '%" . $this->db->escape($data['filter_seller']) . "%'";
        }

        if (!empty($data['filter_seller_country'])) {
            $sql .= " AND   c2c.country_id ='".(int)$data['filter_seller_country']."'";
        }

        if (!empty($data['filter_order_status'])) {
            $sql .= " AND  o.order_status_id  = '".(int) $data['filter_order_status']."'";
        }

        if (!empty($data['filter_awbno'])) {
            $sql .= " AND  ob.book_response like '%".$data['filter_awbno']."%'";
        }

        $sort_data = array(
            'c2o.order_id',
            'awbno',
            'seller_name',
            'customer_name',
            'c2o.date_added',
            'status'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY o.order_id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalOrders($data = array()) {
      $sql = "SELECT COUNT(*) as total FROM customerpartner_to_order c2o LEFT JOIN customerpartner_to_customer c2c ON (c2o.customer_id=c2c.customer_id) LEFT JOIN customerpartner_to_customer_description c2cd ON (c2o.customer_id=c2cd.customer_id AND c2cd.language_id='".(int)$this->config->get('config_language_id')."') LEFT JOIN `order` o ON (c2o.order_id = o.order_id) LEFT JOIN order_booking ob ON (c2o.order_id = ob.order_id and c2o.customer_id=ob.seller_id) WHERE o.order_status_id <> 0  AND c2o.deleted='0' AND o.deleted='0' ";
      if (!empty($data['filter_order_id'])) {
          $sql .= " AND c2o.order_id = '" . (int) $data['filter_order_id'] . "'";
      }

      if (!empty($data['filter_seller'])) {
          $sql .= " AND CONCAT(c2cd.companyname, ' ', c2cd.screenname) LIKE '%" . $this->db->escape($data['filter_seller']) . "%'";
      }

      if (!empty($data['filter_seller_country'])) {
          $sql .= " AND   c2c.country_id ='".(int)$data['filter_seller_country']."'";
      }

      if (!empty($data['filter_order_status'])) {
          $sql .= " AND  o.order_status_id  = '".(int) $data['filter_order_status']."'";
      }

      if (!empty($data['filter_awbno'])) {
          $sql .= " AND  ob.book_response like '%".$data['filter_awbno']."%'";
      }

      $query = $this->db->query($sql);

      return $query->row['total'];
    }
}
