<?php

class ModelCustomerpartnerShippingReturns extends Model {

    public function getOrders($data = array()) {

        $sql = "SELECT rb.return_id, o.order_id, CONCAT(o.firstname, ' ', o.lastname) AS customer,o.payment_country, (SELECT cdd.name FROM " . DB_PREFIX . "country_description cdd WHERE cdd.country_id = cp2c.country_id AND cdd.language_id = '" . (int) $this->config->get('config_language_id') . "') AS seller_country, CONCAT(cp2c.firstname , ' ',cp2c.lastname ) as seller_name , (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int) $this->config->get('config_language_id') . "') AS status, o.shipping_country, cd.name as shipping_country,o.shipping_code, o.total, o.currency_code, o.currency_value,rb.AWBNo as return_awbno,rs.AWBNO as ship_awbno, o.date_added, o.date_modified,p.sku FROM " . DB_PREFIX . "return_booking rb LEFT JOIN " . DB_PREFIX . "customerpartner_to_order c2o ON (c2o.order_id = rb.order_id) LEFT JOIN `" . DB_PREFIX . "order` o ON (rb.order_id = o.order_id AND o.deleted='0' )  LEFT JOIN customerpartner_to_product c2p ON(c2o.product_id=c2p.product_id) LEFT JOIN customerpartner_to_customer_description cp2d ON(c2p.customer_id=cp2d.customer_id) LEFT JOIN customerpartner_to_customer cp2c ON(c2p.customer_id=cp2c.customer_id) LEFT JOIN country_description cd ON (cd.country_id=o.shipping_country_id) LEFT JOIN wk_rma_product wrp ON (rb.return_id=wrp.rma_id) LEFT JOIN return_shipping rs ON (rb.return_id=rs.return_id)  LEFT JOIN product p ON (wrp.product_id=p.product_id) ";

        $where = ' WHERE c2o.deleted="0" AND ';

        if (!empty($data['filter_shipping_company'])) {
            $sql .= $where . " rb.shipping_company = '" . (int) $data['filter_shipping_company'] . "'";
            $where = ' AND ';
        }

        if (!empty($data['filter_return_case'])) {
            if ($data['filter_return_case'] == 1) {
                $sql .= $where . " rb.is_closed = '0'";
            } elseif ($data['filter_return_case'] == 2) {
                $sql .= $where . " rb.is_closed = '1' AND rb.return_id NOT IN (SELECT return_id from `return_shipping` where return_id = rb.return_id)";
            } elseif ($data['filter_return_case'] == 3) {
                $sql .= $where . " rs.is_closed = '0'";
            }
        }

        $sql .= ' group by rb.return_id ';

        $sort_data = array(
            'o.order_id',
            'customer',
            'status',
            'o.date_added',
            'o.date_modified',
            'o.total'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY o.order_id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        //echo $sql;die;
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalOrders($data = array()) {
        $sql = "SELECT COUNT(o.order_id) FROM " . DB_PREFIX . "return_booking rb LEFT JOIN " . DB_PREFIX . "customerpartner_to_order c2o ON (c2o.order_id = rb.order_id) LEFT JOIN `" . DB_PREFIX . "order` o ON (rb.order_id = o.order_id AND o.deleted='0' )  LEFT JOIN customerpartner_to_product c2p ON(c2o.product_id=c2p.product_id) LEFT JOIN customerpartner_to_customer_description cp2d ON(c2p.customer_id=cp2d.customer_id) LEFT JOIN customerpartner_to_customer cp2c ON(c2p.customer_id=cp2c.customer_id) LEFT JOIN country_description cd ON (cd.country_id=o.shipping_country_id)";

        if (!empty($data['filter_shipping_company'])) {
            $sql .= " AND rb.shipping_company = '" . (int) $data['filter_shipping_company'] . "'";
        }

        $sql .= ' AND c2o.deleted="0"  group by rb.return_id ';

        $query = $this->db->query($sql);

        return count($query->rows);
    }

}
