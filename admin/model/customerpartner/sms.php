<?php
class ModelCustomerpartnerSms extends Model {	

	private $data;
	private $customer_applied_for_partnership_to_seller = 'customer_applied_for_partnership_to_seller';
	private $seller_product_approve = 'seller_product_approve';
	private $seller_transaction_add = 'seller_transaction_add';

	public function createTable(){
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX ."customerpartner_sms` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `name` varchar(100) NOT NULL,
		  `message` varchar(5000) NOT NULL,	  
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1") ;
	}

	public function removeTable(){
		$this->db->query("DROP TABLE IF EXISTS `".DB_PREFIX ."customerpartner_sms`");
	}
	/**
	 * [getSmsData to get the sms template data by id]
	 * @param  [int] $id [sms template id]
	 * @return [array]     [record of sms template]
	 */
	public function getSmsData($id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_sms WHERE id='".(int)$id."'");		
		return $query->row;
	}

	public function deleteentry($id){
		$this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_sms WHERE id='".(int)$id."'");		
	}

	/*public function gettotal(){

		$sql ="SELECT * FROM " . DB_PREFIX . "customerpartner_sms WHERE 1 ";	
		
		$result = $this->db->query($sql);

		return $result->rows;
	}*/
	/**
	 * [viewtotal to view sms templates]
	 * @param  array  $data [to filter the output]
	 * @return [array]       [records of sms templates]
	 */
	public function viewtotal($data = array()){

		$sql ="SELECT * FROM " . DB_PREFIX . "customerpartner_sms WHERE 1 ";	
		 			
		if (!empty($data['filter_id'])) {
			$sql .= " AND id = '" . (float)$this->db->escape($data['filter_id']) . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		if (!empty($data['filter_message'])) {
			$sql .= " AND LCASE(message) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_message'])) . "%'";
		}
						
		$sort_data = array(
			'id',
			'name',
			'message',	
		);	
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY id";	
		}
		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
	
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$result=$this->db->query($sql);

		return $result->rows;		
	}
	/**
	 * [viewtotalentry To get the number of total sms templates]
	 * @param  array  $data [To filter the outut]
	 * @return [type]       [total number of sms templates]
	 */
	public function viewtotalentry($data = array()){

		$sql ="SELECT * FROM " . DB_PREFIX . "customerpartner_sms WHERE 1 ";	
		 			
		if (!empty($data['filter_id'])) {
			$sql .= " AND id = '" . (float)$this->db->escape($data['filter_id']) . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		if (!empty($data['filter_message'])) {
			$sql .= " AND LCASE(message) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_message'])) . "%'";
		}

		$result = $this->db->query($sql);	

		return count($result->rows);		
	}
	/**
	 * [addSms To add or edit the sms template]
	 * @param [array] $data [sms template data like sms_id,name,message]
	 */
	public function addSms($data) {

		if($data['sms_id'])
			$this->db->query("UPDATE " . DB_PREFIX . "customerpartner_sms SET name = '" . $this->db->escape($data['name']) . "', message = '" . $this->db->escape($data['message']) . "' WHERE id='".(int)$data['sms_id']."'");
		else
			$this->db->query("INSERT INTO " . DB_PREFIX . "customerpartner_sms SET name = '" . $this->db->escape($data['name']) . "', message = '" . $this->db->escape($data['message']) . "'");
		
	}
}
?>