<?php

class ModelCustomerpartnerShippingOrder extends Model {

    public function getOrders($data = array()) {
        $sql = "SELECT ob.country_id,o.order_id, CONCAT(o.firstname, ' ', o.lastname) AS customer ,CONCAT(cp2c.firstname , ' ',cp2c.lastname ) as seller_name , o.payment_code, count(c2o.product_id) as number_of_items ,(SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int) $this->config->get('config_language_id') . "') AS status, (SELECT cdd.name FROM " . DB_PREFIX . "country_description cdd WHERE cdd.country_id = cp2c.country_id AND cdd.language_id = '" . (int) $this->config->get('config_language_id') . "') AS seller_country, o.shipping_country, cd.name as shipping_country,o.shipping_code, o.total, o.currency_code, o.currency_value,ob.book_response as book_awbno,osh.AWBNo as ship_awbno, o.date_added, o.date_modified FROM " . DB_PREFIX . "customerpartner_to_order c2o LEFT JOIN `" . DB_PREFIX . "order` o ON (c2o.order_id = o.order_id  AND o.deleted='0' ) LEFT JOIN customerpartner_to_product c2p ON(c2o.product_id=c2p.product_id) LEFT JOIN customerpartner_to_customer_description cp2d ON(c2p.customer_id=cp2d.customer_id) LEFT JOIN customerpartner_to_customer cp2c ON(c2p.customer_id=cp2c.customer_id) LEFT JOIN country_description cd ON (cd.country_id=o.shipping_country_id) LEFT JOIN order_booking ob ON(c2o.order_id=ob.order_id) LEFT JOIN order_shipping osh ON(c2o.order_id=osh.order_id) WHERE cp2d.language_id = '" . (int) $this->config->get('config_language_id') . "'  AND c2o.deleted='0'  ";

        if (isset($data['filter_order_case'])) {
            if ($data['filter_order_case'] == 1) {
                $sql .= " AND o.order_status_id ='18' AND cd.language_id='1'  ";
            } elseif ($data['filter_order_case'] == 2) {
                $sql .= " AND o.order_status_id ='3' AND cd.language_id='1' ";
            } elseif ($data['filter_order_case'] == 3) {
                $sql .= " AND o.order_status_id ='17' AND cd.language_id='1'";
            } elseif ($data['filter_order_case'] == 4) {
                $sql .= " AND o.order_status_id ='5' AND cd.language_id='1'";
            }
        }

        if (!empty($data['filter_shipping_company'])) {
            $sql .= " AND ob.shipping_company = '" . (int) $data['filter_shipping_company'] . "'";
        }

        $sql .= ' group by o.order_id ';

        $sort_data = array(
            'o.order_id',
            'customer',
            'status',
            'o.date_added',
            'o.date_modified',
            'o.total'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY o.order_id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

         //   $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalOrders($data = array()) {
        $sql = "SELECT COUNT(o.order_id) FROM " . DB_PREFIX . "customerpartner_to_order c2o LEFT JOIN `" . DB_PREFIX . "order` o ON (c2o.order_id = o.order_id  AND o.deleted='0' ) LEFT JOIN customerpartner_to_product c2p ON(c2o.product_id=c2p.product_id) LEFT JOIN customerpartner_to_customer_description cp2d ON(c2p.customer_id=cp2d.customer_id) LEFT JOIN customerpartner_to_customer cp2c ON(c2p.customer_id=cp2c.customer_id) LEFT JOIN country_description cd ON (cd.country_id=o.shipping_country_id) LEFT JOIN order_booking ob ON(c2o.order_id=ob.order_id) WHERE cp2d.language_id = '" . (int) $this->config->get('config_language_id') . "'  AND c2o.deleted='0' ";

        if (isset($data['filter_order_case'])) {
            if ($data['filter_order_case'] == 1) {
                $sql .= " AND o.order_status_id ='17' AND cd.language_id='1' AND ob.is_closed=1 ";
            } elseif ($data['filter_order_case'] == 2) {
                $sql .= " AND o.order_status_id ='18' AND cd.language_id='1' ";
            } elseif ($data['filter_order_case'] == 3) {
                $sql .= " AND o.order_status_id ='17' AND cd.language_id='1' AND ob.is_closed=0 ";
            }
        }

        if (!empty($data['filter_shipping_company'])) {
            $sql .= " AND ob.shipping_company = '" . (int) $data['filter_shipping_company'] . "'";
        }

        $sql .= ' group by o.order_id ';

        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function close_order_country($order_id, $country_id) {

        $this->db->query("UPDATE " . DB_PREFIX . "order_booking set is_closed=1 where country_id='" . (int) $country_id . "' AND order_id='" . (int) $order_id . "'");
    }

    public function close_return($id) {

        $this->db->query("UPDATE " . DB_PREFIX . "return_booking set is_closed=1 where return_id='" . (int) $id . "'");
    }

}
