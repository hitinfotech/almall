<?php

class ModelCustomerpartnerPartner extends Model {

    private $data = array();

    public function removeCustomerpartnerTable() {
        /*
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_download`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_feedbacks`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_flatshipping`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_orders_pay`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_attributes`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_description`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_image`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_to_category`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_shipping`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_sold_tracking`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_to_customer`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customer_partner_rel`");

          // old v2 tables
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_attribute`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_discount`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_filter`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_option`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_option_value`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_related`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_reward");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_special`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_product_to_download`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_commission_manual`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_to_product`");
          $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "customerpartner_mail`");

          $this->createCustomerpartnerTable();
         */
    }

    public function upgradeMarketplace() {
        $query = $this->db->query("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '" . DB_DATABASE . "' AND TABLE_NAME = '" . DB_PREFIX . "customerpartner_to_order' AND COLUMN_NAME = 'order_product_status'")->row;

        if (!$query) {

            $this->db->query("ALTER TABLE  " . DB_PREFIX . "customerpartner_to_order
				ADD COLUMN order_product_status INT(11) NOT NULL AFTER date_added;
		   ");

            //Table structure for table `customerpartner_to_order_status`
            $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customerpartner_to_order_status` (
			  `change_orderstatus_id` int(11) NOT NULL AUTO_INCREMENT,
			  `order_id` int(11) NOT NULL,
			  `product_id` int(11) NOT NULL,
			  `customer_id` int(11) NOT NULL,
			  `order_status_id` int(11) NOT NULL,
			  `comment` text NOT NULL,
			  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
			  PRIMARY KEY (`change_orderstatus_id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");

            $orderStatuses = $this->db->query("SELECT order_id,order_status_id FROM " . DB_PREFIX . "order")->rows;

            foreach ($orderStatuses as $key => $value) {

                $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_order SET order_product_status = '" . $value['order_status_id'] . "' WHERE order_id = '" . $value['order_id'] . "'");
            }
        }
    }

    public function createCustomerpartnerTable() {

        //Table structure for table `customerpartner_commission_manual`
        // $this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX ."customerpartner_commission_manual` (
        //   `id` int(11) NOT NULL AUTO_INCREMENT,
        //   `name` varchar(100) NOT NULL,
        //   `fixed` float NOT NULL,
        //   `percentage` float NOT NULL,
        //   PRIMARY KEY (`id`)
        // ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1") ;
        //Table structure for table `customerpartner_commission_category`
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customerpartner_commission_category` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `category_id` int(100) NOT NULL,
		  `fixed` float NOT NULL,
		  `percentage` float NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");


        //Table structure for table `customerpartner_to_customer`
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customerpartner_to_customer` (
		  `customer_id` int(11) NOT NULL,
		  `is_partner` int(1) NOT NULL,
		  `screenname` varchar(255) NOT NULL,
		  `gender` varchar(255) NOT NULL,
		  `shortprofile` text NOT NULL,
		  `avatar` varchar(255) NOT NULL,
		  `twitterid` varchar(255) NOT NULL,
		  `paypalid` varchar(255) NOT NULL,
		  `country` varchar(255) NOT NULL,
		  `facebookid` varchar(255) NOT NULL,
		  `backgroundcolor` varchar(255) NOT NULL,
		  `companybanner` varchar(255) NOT NULL,
		  `companylogo` varchar(255) NOT NULL,
		  `companylocality` varchar(255) NOT NULL,
		  `companyname` varchar(255) NOT NULL,
		  `companydescription` text NOT NULL,
		  `countrylogo` varchar(1000) NOT NULL,
		  `otherpayment` text NOT NULL,
		  `commission` decimal(10,2) NOT NULL,
		  PRIMARY KEY (`customer_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");

        //Table structure for table `customerpartner_to_commission`
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customerpartner_to_commission` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_id` int(100) NOT NULL,
		  `commission_id` int(100) NOT NULL,
		  `fixed` float NOT NULL,
		  `percentage` float NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");

        //Table structure for table `customerpartner_to_product`
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customerpartner_to_product` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_id` int(100) NOT NULL,
		  `product_id` int(100) NOT NULL,
		  `price` float NOT NULL,
		  `seller_price` float NOT NULL,
		  `currency_code` varchar(11) NOT NULL,
		  `quantity` int(100) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");

        //Table structure for table `customerpartner_feedbacks`
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customerpartner_to_feedback` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_id` smallint(6) NOT NULL,
		  `seller_id` smallint(6) NOT NULL,
		  `feedprice` smallint(6) NOT NULL,
		  `feedvalue` smallint(6) NOT NULL,
		  `feedquality` smallint(6) NOT NULL,
		  `nickname` varchar(255) NOT NULL,
		  `summary` text NOT NULL,
		  `review` text NOT NULL,
		  `createdate` datetime NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");

        //Table structure for table `customerpartner_orders`
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customerpartner_to_order` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `order_id` int(11) NOT NULL,
		  `customer_id` int(11) NOT NULL,
		  `product_id` int(11) NOT NULL,
		  `order_product_id` int(100) NOT NULL,
		  `price` float NOT NULL,
		  `quantity` float(11) NOT NULL,
		  `shipping` varchar(255) NOT NULL,
		  `shipping_rate` float NOT NULL,
		  `payment` varchar(255) NOT NULL,
		  `payment_rate` float NOT NULL,
		  `admin` float NOT NULL,
		  `customer` float NOT NULL,
		  `shipping_applied` float NOT NULL,
		  `commission_applied` decimal(10,2) NOT NULL,
		  `currency_code` varchar(10) NOT NULL,
		  `currency_value` decimal(15,8) NOT NULL,
		  `details` varchar(255) NOT NULL,
		  `paid_status` tinyint(1) NOT NULL,
		  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  `order_product_status` INT(10) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");

        //Table structure for table `customerpartner_to_order_status`
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customerpartner_to_order_status` (
		  `change_orderstatus_id` int(11) NOT NULL AUTO_INCREMENT,
		  `order_id` int(11) NOT NULL,
		  `product_id` int(11) NOT NULL,
		  `customer_id` int(11) NOT NULL,
		  `order_status_id` int(11) NOT NULL,
		  `comment` text NOT NULL,
		  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  PRIMARY KEY (`change_orderstatus_id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");

        //Table structure for table `customerpartner_transaction`
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customerpartner_to_transaction` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_id` int(11) NOT NULL,
		  `order_id` varchar(500) NOT NULL,
		  `order_product_id` varchar(500) NOT NULL,
		  `amount` float NOT NULL,
		  `text` varchar(255) NOT NULL,
		  `details` varchar(255) NOT NULL,
		  `date_added` date NOT NULL DEFAULT '0000-00-00',
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");

        //Table structure for table `customerpartner_payment`
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customerpartner_to_payment` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `customer_id` int(11) NOT NULL,
		  `amount` float NOT NULL,
		  `text` varchar(255) NOT NULL,
		  `details` varchar(255) NOT NULL,
		  `request_type` varchar(255) NOT NULL,
		  `paid` int(10) NOT NULL,
		  `balance_reduced` int(10) NOT NULL,
		  `payment` varchar(255) NOT NULL,
		  `date_added` date NOT NULL DEFAULT '0000-00-00',
		  `date_modified` date NOT NULL DEFAULT '0000-00-00',
		  `added_by` varchar(255) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");

        //Table structure for table `customerpartner_download
        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "customerpartner_download (
		  `download_id` int(11) NOT NULL AUTO_INCREMENT,
		  `seller_id` int(11) NOT NULL,
		  PRIMARY KEY (`download_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");

        //Table structure for table `customerpartner_flatshipping
        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "customerpartner_flatshipping (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`partner_id` int(11) NOT NULL,
			`amount` float,
			`tax_class_id` float NOT NULL,
			PRIMARY KEY (`id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");

        //Table structure for table `customerpartner_shipping
        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "customerpartner_shipping (
            `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
            `seller_id` int(10) NOT NULL ,
            `country_code` varchar(100) NOT NULL ,
            `zip_to` varchar(100) NOT NULL ,
            `zip_from` varchar(100) NOT NULL ,
            `price` float NOT NULL ,
            `weight_from` decimal (10,2) NOT NULL ,
            `weight_to` decimal (10,2) NOT NULL ,
            PRIMARY KEY (`id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");

        //Table structure for table `customerpartner_sold_tracking
        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "customerpartner_sold_tracking (
			`product_id` int(11) NOT NULL,
			`order_id` int(11) NOT NULL,
			`customer_id` int(11) NOT NULL,
			`date_added` date NOT NULL DEFAULT '0000-00-00',
			`tracking` varchar(100) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");


        //Table structure for table `customerpartner_mail`
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customerpartner_mail` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `name` varchar(100) NOT NULL,
		  `subject` varchar(1000) NOT NULL,
		  `message` varchar(5000) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1");

        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "wk_custom_field (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `forSeller` varchar(10) NOT NULL,
			  `fieldType` varchar(100) NOT NULL,
			  `isRequired` varchar(10) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ");

        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "wk_custom_field_description (
			  `fieldId` int(10) NOT NULL,
			  `fieldDescription` varchar(5000) NOT NULL,
			  `fieldName` varchar(100) NOT NULL,
			  `language_id` int(10) NOT NULL
			) ENGINE=MyISAM DEFAULT CHARSET=utf8");

        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "wk_custom_field_options (
			  `optionId` int(11) NOT NULL AUTO_INCREMENT,
			  `fieldId` int(11) NOT NULL,
			  PRIMARY KEY (`optionId`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ");

        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "wk_custom_field_option_description (
			  `optionId` int(10) NOT NULL,
			  `optionValue` varchar(100) NOT NULL,
			  `language_id` int(10) NOT NULL
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");

        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "wk_custom_field_product (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `fieldId` int(11) NOT NULL,
			  `productId` int(11) NOT NULL,
			  `fieldType` varchar(100) NOT NULL,
			  `fieldDescription` varchar(5000) NOT NULL,
			  `fieldName` varchar(100) NOT NULL,
			  `isRequired` varchar(50) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ");

        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "wk_custom_field_product_options (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `pro_id` int(100) NOT NULL,
			  `product_id` int(100) NOT NULL,
			  `fieldId` int(100) NOT NULL,
			  `option_id` mediumtext CHARACTER SET utf8 NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ");
    }

    public function deleteCustomer($customer_id) {
        $partner_products = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "customerpartner_to_product WHERE customer_id = '" . (int) $customer_id . "'")->rows;

        $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_download WHERE seller_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_flatshipping WHERE partner_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_shipping WHERE seller_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_to_feedback WHERE seller_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_to_customer WHERE customer_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_sold_tracking WHERE customer_id = '" . (int) $customer_id . "'");
        $this->db->query("UPDATE  " . DB_PREFIX . "customerpartner_to_order set deleted='1', deleting_user_id = ".$this->user->getId()." , daleting_date= NOW() WHERE customer_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_to_payment WHERE customer_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_to_product WHERE customer_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_to_transaction WHERE customer_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_to_customer_description WHERE customer_id = '" . (int) $customer_id . "'");

        if ($partner_products) {
            foreach ($partner_products as $products) {
                if ($this->config->get('wk_mpaddproduct_status')) {
                    $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_to_product WHERE product_id = '" . (int) $products['product_id'] . "' AND customer_id = '" . $customer_id . "' ");
                } else if (!$this->config->get('marketplace_sellerproductdelete')) {
                    $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_to_product WHERE product_id = '" . (int) $products['product_id'] . "'");

                    $this->db->query("UPDATE " . DB_PREFIX . "product SET status = '0' WHERE product_id = '" . (int) $products['product_id'] . "'");
                } else {

                    $this->load->model('catalog/product');
                    $this->model_catalog_product->deleteProduct($products['product_id']);
                }
            }
        }
        $this->db->query(" INSERT IGNORE INTO algolia SET `type`='seller', type_id= '" . (int) $customer_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function getCustomerCompany($customer_id) {
        $query = $this->db->query("SELECT DISTINCT customer_id, CONCAT(companyname, ' | ', screenname) AS company FROM " . DB_PREFIX . "customerpartner_to_customer_description WHERE language_id = '" . (int) $this->config->get('config_language_id') . "' AND customer_id = '" . (int) $customer_id . "'");

        return $query->row;
    }

    public function getCustomer($customer_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customerpartner_to_customer WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->row;
    }

    public function getAllCustomers() {

        $sql = $this->db->query("SELECT c.email FROM " . DB_PREFIX . "customerpartner_to_customer c2c LEFT JOIN " . DB_PREFIX . "customer c ON (c2c.customer_id = c.customer_id) WHERE c2c.is_partner = '1'");

        return $sql->rows;
    }

    public function getCustomers($data = array()) {

        $is_partner = 1;

        if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
            $is_partner = (int) $data['filter_approved'];
        }

        $sql = "SELECT cp2c.customer_id,cp2c.is_partner, concat(cp2c.firstname,' ',cp2c.lastname) AS name,cp2c.email,cp2c.status,cp2c.date_added,cp2c.commission , cp2cd.companyname AS company, CONCAT(u.firstname,' ', u.lastname) as account_manager from customerpartner_to_customer cp2c LEFT JOIN customerpartner_to_customer_description cp2cd ON (cp2c.customer_id=cp2cd.customer_id AND cp2cd.language_id='" . (int) $this->config->get('config_language_id') . "') LEFT JOIN user u ON (cp2c.user_id = u.user_id) WHERE cp2c.is_partner='" . $is_partner . "' ";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(CONCAT(cp2c.firstname, ' ', cp2c.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "LCASE(cp2c.email) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
        }

        if (!empty($data['filter_company'])) {
            $implode[] = "LCASE(cp2cd.companyname) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_company'])) . "%'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "cp2c.status = '" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_date_added']) && !empty($data['filter_date_added'])) {
            $implode[] = "DATE(cp2c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }
        if (isset($data['filter_country_id'])) {
            $sql .= " AND cp2c.country_id = " . (int) $data['filter_country_id'] . " ";
        }
        if (isset($data['filter_account_manager'])) {
            $sql .= " AND cp2c.user_id = " . (int) $data['filter_account_manager'] . " ";
        }
        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'cp2c.customer_id',
            'cp2cd.companyname',
            'name',
            'cp2c.email',
            'customer_group',
            'cp2c.status',
            'cp2c.date_added'
        );


        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY cp2c.customer_id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalCustomers($data = array()) {

        $is_partner = 1;
        if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
            $is_partner = (int) $data['filter_approved'];
        }

        $sql = "SELECT count(*) as total FROM " . DB_PREFIX . "customerpartner_to_customer cp2c INNER JOIN customerpartner_to_customer_description cp2cd ON(cp2c.customer_id=cp2cd.customer_id ) WHERE cp2c.is_partner='" . $is_partner . "' ";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(CONCAT(cp2c.firstname, ' ', cp2c.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "LCASE(cp2c.email) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "cp2c.status = '" . (int) $data['filter_status'] . "'";
        }

        if (!empty($data['filter_company'])) {
            $implode[] = "LCASE(cp2cd.companyname) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_company'])) . "%'";
        }

        if (isset($data['filter_date_added']) && !empty($data['filter_date_added'])) {
            $implode[] = "DATE(cp2c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if (isset($data['filter_country_id'])) {
            $sql .= " AND cp2c.country_id = " . (int) $data['filter_country_id'] . " ";
        }

        if (isset($data['filter_account_manager'])) {
            $sql .= " AND cp2c.user_id = " . (int) $data['filter_account_manager'] . " ";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function approve($customer_id, $setstatus = 1) {

        if ($this->config->get('wk_mpaddproduct_status')) {
            $this->load->model('module/wk_mpaddproduct');
            if ($setstatus == 1) {
                $this->model_module_wk_mpaddproduct->insertSeller($customer_id);
            } else {
                $this->model_module_wk_mpaddproduct->deleteSeller($customer_id);
            }
        }

        $customer_info = $this->getCustomer($customer_id);

        if ($customer_info) {

            $commission = $this->config->get('marketplace_commission') ? $this->config->get('marketplace_commission') : 0;
            $country_id = $customer_info['country_id'] ? $customer_info['country_id'] : 0;

            $seller_info = $this->getPartner($customer_id);
            if( $setstatus == 0 ) {
                $this->db->query("UPDATE product SET status = 0 WHERE product_id IN (SELECT product_id FROM customerpartner_to_product WHERE customer_id = '" . (int) $customer_id . "') ");
            }

            if ($seller_info) {
                $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_customer SET is_partner='" . (int) $setstatus . "',commission = '" . (float) $commission . "',country_id='" . (int) $country_id . "', date_approved=NOW() WHERE customer_id = '" . (int) $customer_id . "'");

                // membership modification after partner
                if ($this->config->get('wk_seller_group_status')) {
                    $this->load->model('customerpartner/sellergroup');
                    $freeMembership = $this->db->query("SELECT * FROM " . DB_PREFIX . "seller_group WHERE autoApprove = 1 ")->row;

                    if ($freeMembership && isset($freeMembership['product_id']) && $freeMembership['product_id']) {
                        $sellerDetails = array(
                            'seller_id' => $customer_id,
                            'product_id' => $freeMembership['product_id'],
                            'doMail' => true,
                        );

                        $quantity = $freeMembership['gquantity'];
                        $customerid = $customer_id;
                        $group_id = $freeMembership['groupid'];
                        $status = 'paid';
                        $price = $freeMembership['gprice'];
                        $this->model_customerpartner_sellergroup->update_customer_quantity($quantity, $customerid, $group_id, $status, $price);
                        $this->model_customerpartner_sellergroup->insert_payment($customerid, $group_id);
                    }
                }
            } else {
                $this->db->query("INSERT INTO " . DB_PREFIX . "customerpartner_to_customer(`customer_id`, `is_partner`, `commission`) VALUES('" . $customer_id . "','" . (int) $setstatus . "','" . (float) $commission . "')");

                // membership modification after partner
                if ($this->config->get('wk_seller_group_status')) {
                    $this->load->model('customerpartner/sellergroup');
                    $freeMembership = $this->db->query("SELECT * FROM " . DB_PREFIX . "seller_group WHERE autoApprove = 1 ")->row;

                    if ($freeMembership && isset($freeMembership['product_id']) && $freeMembership['product_id']) {
                        $sellerDetails = array(
                            'seller_id' => $customer_id,
                            'product_id' => $freeMembership['product_id'],
                            'doMail' => true,
                        );

                        $quantity = $freeMembership['gquantity'];
                        $customerid = $customer_id;
                        $group_id = $freeMembership['groupid'];
                        $status = 'paid';
                        $price = $freeMembership['gprice'];
                        $this->model_customerpartner_sellergroup->update_customer_quantity($quantity, $customerid, $group_id, $status, $price);
                        $this->model_customerpartner_sellergroup->insert_payment($customerid, $group_id);
                    }
                }
            }
            $data = array_merge($customer_info, $seller_info);

            //send sms to Customer after request for Partnership
            $this->load->model('customerpartner/mail');
            $this->model_customerpartner_mail->sms($data, 'customer_applied_for_partnership_to_seller');
            if (!$this->config->get('marketplace_mail_partner_approve'))
                return;

            $data = array_merge($customer_info, $seller_info);

            //send mail to Customer after request for Partnership
            $this->load->model('customerpartner/mail');
            $this->model_customerpartner_mail->mail($data, 'customer_applied_for_partnership_to_seller');
        }
        $this->db->query(" INSERT IGNORE INTO algolia SET `type`='seller', type_id= '" . (int) $customer_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    //for get commission
    public function getPartner($partner_id) {
        $query = $this->db->query("SELECT cp2c.trackcode, cp2c.trackcodeview, cp2c.trackcodeadd,cp2c.is_partner, cp2c.companylocality, cp2c.return_policy_id, cp2c.commission, cp2c.paypalid, cp2c.otherpayment,cp2c.website, cp2c.twitterid, cp2c.facebookid,cp2c.instagramid, cp2c.gender, cp2c.country_id, cp2c.avatar, cp2c.companybanner, cp2c.flat_rate, cp2c.backgroundcolor, keyword as seo_url, cp2c.contract_date,cp2c.zone_id , cp2c.city, cp2c.address,cp2c.warehouse_address,cp2c.num_of_branches, cp2c.opening_time, cp2c.email, cp2c.firstname,cp2c.lastname,cp2c.telephone as phone, cp2c.telecode,cp2c.admin_telecode, cp2c.available_country,cp2c.admin_name, cp2c.admin_email, cp2c.admin_phone, cp2c.date_approved, cp2c.is_fbs, cp2c.fbs_inventory_volume, cp2c.fbs_inventory_volume_unit, cp2c.fbs_fullfillment_charges, cp2c.fbs_storage_charges, cp2c.fbs_store_location, cp2c.fbs_date_in, cp2c.fbs_date_out, cp2c.fbs_free_storage_days,cp2c.fbs_email,cp2c.user_id , li.legal_name, li.company_name,li.trade_license_number,li.address_license_number, li.nationality ,li.email as legal_email, li.direct_officer_number,li.direct_officer_number_telecode, li.telephone as legal_phone,li.telephone_telecode,li.passport,li.license,li.passport_num, fi.company_officer_name, fi.email as financial_email, fi.telephone as financial_phone,fi.fin_telephone_telecode, fi.beneficiary_name, fi.bank_name, fi.bank_account, fi.iban_code, fi.swift_code, fi.currency, ev.document_path as document_path FROM " . DB_PREFIX . "customerpartner_to_customer cp2c LEFT JOIN url_partner ON (query='partner_id=" . $partner_id . "') LEFT JOIN legal_info li ON (cp2c.customer_id = li.partner_id) LEFT JOIN financial_info fi ON (cp2c.customer_id = fi.partner_id) LEFT JOIN eligible_vat ev ON (cp2c.customer_id = ev.customer_id) WHERE cp2c.customer_id='" . $partner_id . "'");
        return $query->row;
    }

    public function getPartnerDescription($partner_id) {
        $sql = "SELECT cp2cd.* FROM " . DB_PREFIX . "customerpartner_to_customer_description cp2cd  where customer_id='" . $partner_id . "'";
        $query = $this->db->query($sql);

        $partner_description_data = array();
        foreach ($query->rows as $result) {
            $partner_description_data[$result['language_id']] = array(
                'screenname' => $result['screenname'],
                'shortprofile' => $result['shortprofile'],
                'companyname' => $result['companyname'],
            );
        }

        return($partner_description_data);
    }

    public function getPartnerCustomerInfo($partner_id) {
        $query = $this->db->query("SELECT c2c.*, c.curcode, fi.beneficiary_name, fi.bank_name, fi.bank_account, fi.iban_code, fi.swift_code, fi.email as fi_email, li.company_name AS company_name, li.address_license_number AS address_license_number
                                        FROM " . DB_PREFIX . "customerpartner_to_customer c2c 
                                        LEFT JOIN " . DB_PREFIX . "country c ON (c2c.country_id = c.country_id) 
                                        LEFT JOIN " . DB_PREFIX . "financial_info fi ON (c2c.customer_id = fi.partner_id) 
                                        LEFT JOIN " . DB_PREFIX . "legal_info li ON (c2c.customer_id = li.partner_id)
                                        where c2c.customer_id='" . $partner_id . "'");
        return($query->row);
    }

    public function addPartner($data) {
        $this->load->model('tool/image');
        $description_data = $data['description'];

        if (isset($data['customer']) AND $data['customer']) {
            $partner_info = $data;
            $data = $data['customer'];


            $sql = "INSERT INTO " . DB_PREFIX . "customerpartner_to_customer SET is_partner='1', date_added=NOW(), date_approved=NOW(),  ";
            foreach ($data as $key => $value) {
                if ($key == 'clienturl') {
                    $customer_url = $value;
                    continue;
                }
                if ($key == 'legal_info' || $key == 'financial_info') {
                    continue;
                }
                $sql .= $key . " = '" . $this->db->escape($value) . "' ,";
            }
        }

        $sql = substr($sql, 0, -2);

        $this->db->query($sql);

        $partner_id = $this->db->getLastId();

        if (isset($customer_url) && $customer_url != '') {
            $this->db->query("INSERT INTO url_partner SET query='partner_id=" . $partner_id . "', keyword='seller/" . $customer_url . "'");
        }


        if (isset($description_data) && !empty($description_data)) {
            foreach ($description_data as $language_id => $value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "customerpartner_to_customer_description SET customer_id = '" . (int) $partner_id . "', language_id = '" . (int) $language_id . "', `screenname` = '" . $this->db->escape($value['screenname']) . "', shortprofile = '" . $this->db->escape($value['shortprofile']) . "', companyname = '" . $this->db->escape($value['companyname']) . "'");
            }
            $this->db->query("INSERT INTO " . DB_PREFIX . "legal_info SET partner_id = '" . (int) $partner_id . "', company_name = '" . $this->db->escape($description_data[1]['companyname']) . "'");
        }

        $this->updatePartner($partner_id, $partner_info);
    }

    public function updatePartner($partner_id, $data) {
        $this->load->model('tool/image');

        if (isset($data['eligible_check'])) {
            $path=$company_name=$business_address=$tax_registration_number=$issue_date='';

            if (isset($this->request->files['document']) && $this->request->files['document']['size'] > 0) {
                $doc_path = $this->upload_file('customerpartner_vat_document', 'document', $partner_id);
                $path = ",document_path='$doc_path'";
            }
            if (trim($data['doc_company_name']) != '') {
                $company_name = $this->db->escape($data['doc_company_name']);
            }
            if (trim($data['business_address']) != '') {
                $business_address = $this->db->escape($data['business_address']);
            }
            if (trim($data['tax_reg_num']) != '') {
                $tax_registration_number = $this->db->escape($data['tax_reg_num']);
            }
            if (trim($data['issue_date']) != '') {
                $issue_date = $this->db->escape($data['issue_date']);
            }
            $eligible_vat_info = $this->db->query("SELECT customer_id FROM eligible_vat WHERE customer_id = '" . (int) $partner_id . "'");

            if ($eligible_vat_info->num_rows) {
                $eligible_sql = "UPDATE eligible_vat SET company_name='".$company_name."',business_address='".$business_address."',tax_registration_number='".$tax_registration_number."',issue_date='$issue_date',checked=1 $path WHERE customer_id='" . (int) $partner_id . "' ";
            } else {
                $eligible_sql = "INSERT INTO eligible_vat SET company_name='".$company_name."',business_address='".$business_address."',tax_registration_number='".$tax_registration_number."',issue_date='$issue_date',customer_id='" . (int) $partner_id . "',checked=1 $path";
            }
            $this->db->query($eligible_sql);

        } else {
            $eligible_sql = "UPDATE eligible_vat SET checked=0 WHERE customer_id='" . (int) $partner_id . "' ";
            $this->db->query($eligible_sql);
        }

        $companyname = '';
        if (isset($data['customer']['legal_info']['company_name'])) {
            $companyname = $data['customer']['legal_info']['company_name'];
        }
        if (isset($data['customer']['telephone'])) {
            $data['customer']['telephone'] = isset(explode(' ', $data['customer']['telephone'])[1]) ? explode(' ', $data['customer']['telephone'])[1] : $data['customer']['telephone'];
        }
        if (isset($data['customer']['admin_phone'])) {
            $data['customer']['admin_phone'] = isset(explode(' ', $data['customer']['admin_phone'])[1]) ? explode(' ', $data['customer']['admin_phone'])[1] : $data['customer']['admin_phone'];
        }
        if (isset($data['customer']['legal_info']['direct_officer_number'])) {
            $data['customer']['legal_info']['direct_officer_number'] = isset(explode(' ', $data['customer']['legal_info']['direct_officer_number'])[1]) ? explode(' ', $data['customer']['legal_info']['direct_officer_number'])[1] : $data['customer']['legal_info']['direct_officer_number'];
        }
        if (isset($data['customer']['legal_info']['telephone'])) {
            $data['customer']['legal_info']['telephone'] = isset(explode(' ', $data['customer']['legal_info']['telephone'])[1]) ? explode(' ', $data['customer']['legal_info']['telephone'])[1] : $data['customer']['legal_info']['telephone'];
        }
        if (isset($data['customer']['financial_info']['telephone'])) {
            $data['customer']['financial_info']['telephone'] = isset(explode(' ', $data['customer']['financial_info']['telephone'])[1]) ? explode(' ', $data['customer']['financial_info']['telephone'])[1] : $data['customer']['legal_info']['direct_officer_number'];
        }

        if (isset($data['customer']['is_fbs'])) {
            $data['customer']['is_fbs'] = ($data['customer']['is_fbs']=='on')?1:0;
        }else{
          $data['customer']['is_fbs'] = 0;
        }

        if (isset($data['description']) && !empty($data['description'])) {
            $description = $data['description'];

            $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_to_customer_description WHERE customer_id = '" . (int) $partner_id . "'");
            foreach ($description as $language_id => $value) {
                // echo "INSERT INTO " . DB_PREFIX . "brand_description SET brand_id = '" . (int) $brand_id . "', language_id = '" . (int) $language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', body_text = '" . $this->db->escape($value['body_text']) . "' <br>";
                $this->db->query("INSERT INTO " . DB_PREFIX . "customerpartner_to_customer_description SET customer_id = '" . (int) $partner_id . "', language_id = '" . (int) $language_id . "', `screenname` = '" . $this->db->escape($value['screenname']) . "', shortprofile = '" . $this->db->escape($value['shortprofile']) . "', companyname = '" . $this->db->escape($companyname) . "'");
            }
        }

        if (isset($data['customer']) AND $data['customer']) {
            $data = $data['customer'];
            $available_countries = array_column($this->getExecludedCountries($partner_id),'country_id');
            if(empty($data['execluded_countries'])){
              $this->db->query("DELETE FROM seller_execluded_countries WHERE  customer_id=".$partner_id);
            }
            $sql = "UPDATE " . DB_PREFIX . "customerpartner_to_customer SET date_modified=NOW(), last_mod_id='" . (int) $this->user->getid() . "', ";
            foreach ($data as $key => $value) {
                if ($key == 'clienturl') {
                    $this->db->query("DELETE FROM url_partner WHERE query='partner_id=" . $partner_id . "'");
                    $this->db->query("INSERT INTO url_partner SET query='partner_id=" . $partner_id . "', keyword='seller/" . $value . "'");
                    continue;
                }

                if ($key == 'legal_info') {
                    $legal_data = $this->db->query("SELECT * FROM legal_info WHERE partner_id ='" . (int) $partner_id . "'")->num_rows;
                    $legal_sql = "UPDATE " . DB_PREFIX . "legal_info SET  ";
                    if ($legal_data == 0) {
                        $legal_sql = "INSERT INTO  " . DB_PREFIX . "legal_info SET  ";
                    }

                    foreach ($value as $legal_key => $legal_value) {
                        $legal_sql .= $legal_key . " = '" . $this->db->escape($legal_value) . "' ,";
                    }

                    if (isset($this->request->files) and ! empty($this->request->files)) {
                        foreach ($this->request->files as $img => $val) {
                            $image = '';
                            if ($img == 'passport' && $val['name'] != '') {
                                $image = $this->upload_file('customerpartner_passport', $img, $partner_id);

                                if ($image) {
                                    foreach ($this->config_image->get('partner_passport') as $row) {
                                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                                    }
                                }
                                $legal_sql .= $img . " = '" . $this->db->escape($image) . "' ,";
                            } elseif ($img == 'license' && $val['name'] != '') {
                                $image = $this->upload_file('customerpartner_license', $img, $partner_id);

                                if ($image) {
                                    foreach ($this->config_image->get('partner_license') as $row) {
                                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                                    }
                                }
                                $legal_sql .= $img . " = '" . $this->db->escape($image) . "' ,";
                            }
                        }
                    }

                    if ($legal_data == 1) {
                        $legal_sql = trim($legal_sql, ',') . " WHERE partner_id='" . $partner_id . "'";
                    } else {
                        $legal_sql .= "  partner_id='" . $partner_id . "'";
                    }

                    $this->db->query($legal_sql);
                    continue;
                }
                if ($key == 'financial_info') {

                    $fin_data = $this->db->query("SELECT * FROM financial_info WHERE partner_id ='" . (int) $partner_id . "'")->num_rows;
                    $financial_sql = "UPDATE " . DB_PREFIX . "financial_info SET  ";
                    if ($fin_data == 0) {
                        $financial_sql = "INSERT INTO  " . DB_PREFIX . "financial_info SET  ";
                    }

                    foreach ($value as $financial_key => $financial_value) {
                        $financial_sql .= $financial_key . " = '" . $this->db->escape($financial_value) . "' ,";
                    }

                    if ($fin_data == 1) {
                        $financial_sql = trim($financial_sql, ',') . " WHERE partner_id='" . $partner_id . "'";
                    } else {
                        $financial_sql .= "  partner_id='" . $partner_id . "'";
                    }
                    $this->db->query($financial_sql);
                    continue;
                }

                if($key == 'execluded_countries'){
                  foreach($value as $key2 => $country){
                    if(! in_array($country,$available_countries)){
                      $this->db->query("INSERT IGNORE INTO seller_execluded_countries (customer_id,country_id) values (".$partner_id.",".$country.")");
                    }
                  }
                  foreach($available_countries as $key1 => $country1){
                    if(!in_array($country1,$value)){
                      $this->db->query("DELETE FROM seller_execluded_countries WHERE country_id=".$country1." AND customer_id=".$partner_id);
                    }
                  }
                  continue;
                }

                $sql .= $key . " = '" . $this->db->escape($value) . "' ,";
            }


        }

        if (isset($this->request->files) and ! empty($this->request->files)) {
            foreach ($this->request->files as $img => $val) {
                $image = '';
                if ($img == 'avatar' && $val['name'] != '') {
                    $image = $this->upload_file('customerpartner_avatar', $img, $partner_id);
                    if ($image) {
                        foreach ($this->config_image->get('partner_avatar') as $row) {
                            $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                        }
                    } else {
                        $image = '';
                    }
                    $sql .= $img . " = '" . $this->db->escape($image) . "' ,";
                } elseif ($img == 'companybanner' && $val['name'] != '') {
                    $image = $this->upload_file('customerpartner_companybanner', $img, $partner_id);

                    if ($image) {
                        foreach ($this->config_image->get('partner_companybanner') as $row) {
                            $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                        }
                    }
                    $sql .= $img . " = '" . $this->db->escape($image) . "' ,";
                }
            }
        }

        $sql = substr($sql, 0, -2);
        $sql .= " WHERE customer_id = '" . (int) $partner_id . "'";

        $this->db->query($sql);

        $this->db->query(" update customerpartner_to_customer set show_in_ksa = 1 where available_country = 184 ");
        $this->db->query(" update customerpartner_to_customer set show_in_uae = 1 where available_country = 221 ");
        $this->db->query(" update customerpartner_to_customer set show_in_uae = 1, show_in_ksa = 1  where available_country=0 ");

        $this->db->query(" INSERT IGNORE INTO algolia SET `type`='seller', type_id= '" . (int) $partner_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    //for update product seller
    public function updateProductSeller($partner_id, $product) {

        $product_ids = array();

        if ($product AND is_array($product)) {
            foreach ($product as $individual_product) {
                $product_ids[] = $individual_product['selected'];
            }
        } else
            $product_ids[] = $product;

        foreach ($product_ids as $product_id) {

            $status = $this->chkProduct($product_id);

            if ($status == 1) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "customerpartner_to_product SET product_id = '" . (int) $product_id . "', customer_id = '" . (int) $partner_id . "'");
            } else {
                $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_product SET customer_id = '" . (int) $partner_id . "' WHERE product_id = '" . (int) $product_id . "' ORDER BY id ASC LIMIT 1 ");
            }
        }
        $this->db->query(" INSERT IGNORE INTO algolia SET `type`='seller', type_id= '" . (int) $partner_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function addproduct($partner_id, $data) {
        $insert_str = '';
        $product_ids = array_unique($data['product_ids']);
        $this->db->query("DELETE FROM " . DB_PREFIX . "customerpartner_to_product WHERE customer_id = '" . (int) $partner_id . "'");
        if (!empty($product_ids)) {
            foreach ($product_ids as $product_id) {
                $insert_str .= "('" . (int) $product_id . "','" . (int) $partner_id . "'),";
            }
            $this->db->query("INSERT INTO  " . DB_PREFIX . "customerpartner_to_product ( product_id, customer_id ) VALUES " . trim($insert_str, ",") . "");
        }
    }

    public function chkProduct($pid) {

        $sql = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "product WHERE product_id='" . (int) $pid . "'");

        if (isset($sql->row['quantity'])) {
            $sql = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customerpartner_to_product WHERE product_id='" . (int) $pid . "'");
            if (isset($sql->row['customer_id'])) {
                if ($sql->row['customer_id'])
                    return 0; //already exists
                else
                    return 2; //for update return cp product id
            }else {
                return 1; //not exists so copy
            }
        } else {
            return 0; //already exists
        }
    }

    public function getPartnerAmount($partner_id) {
        $total = $this->db->query("SELECT SUM(c2o.quantity) quantity,SUM(c2o.price) total,SUM(c2o.admin) admin,SUM(c2o.customer) customer FROM " . DB_PREFIX . "customerpartner_to_order c2o WHERE c2o.customer_id ='" . (int) $partner_id . "'  AND c2o.deleted='0' ")->row;

        $paid = $this->db->query("SELECT SUM(c2t.amount) total FROM " . DB_PREFIX . "customerpartner_to_transaction c2t WHERE c2t.customer_id ='" . (int) $partner_id . "'")->row;

        $total['paid'] = $paid['total'];

        return($total);
    }

    public function getPartnerTotal($partner_id, $filter_data = array()) {

        $sub_query = "(SELECT SUM(c2t.amount) as total FROM " . DB_PREFIX . "customerpartner_to_transaction c2t WHERE c2t.customer_id ='" . (int) $partner_id . "'";

        if (isset($filter_data['date_added_from']) || isset($filter_data['date_added_to'])) {
            if ($filter_data['date_added_from'] && $filter_data['date_added_to']) {
                $sub_query .= " AND c2t.date_added >= '" . $filter_data['date_added_from'] . "' && c2t.date_added <= '" . $filter_data['date_added_to'] . "' ";
            } else if ($filter_data['date_added_from'] && !$filter_data['date_added_to']) {
                $sub_query .= " AND c2t.date_added >= '" . $filter_data['date_added_from'] . "' ";
            } else if (!$filter_data['date_added_from'] && $filter_data['date_added_to']) {
                $sub_query .= " AND c2t.date_added <= '" . $filter_data['date_added_to'] . "' ";
            }
        }

        if (isset($filter_data['paid_to_seller_from']) || isset($filter_data['paid_to_seller_to'])) {
            if ($filter_data['paid_to_seller_from'] && $filter_data['paid_to_seller_to']) {
                $sub_query .= " HAVING SUM(c2t.amount) > " . $filter_data['paid_to_seller_from'] . " && SUM(c2t.amount) < " . $filter_data['paid_to_seller_to'] . " )";
            } else if ($filter_data['paid_to_seller_from'] && !$filter_data['paid_to_seller_to']) {
                $sub_query .= " HAVING SUM(c2t.amount) > " . $filter_data['paid_to_seller_from'] . " )";
            } else if (!$filter_data['paid_to_seller_from'] && $filter_data['paid_to_seller_to']) {
                $sub_query .= " HAVING SUM(c2t.amount) > " . $filter_data['paid_to_seller_to'] . " )";
            } else {
                $sub_query .= " )";
            }
        } else {
            $sub_query .= " )";
        }

        $sql = "SELECT SUM(c2o.quantity) quantity,(SUM(c2o.customer) + SUM(c2o.admin)) as total,SUM(c2o.admin) admin,SUM(c2o.customer) as customer, " . $sub_query . " as paid FROM " . DB_PREFIX . "customerpartner_to_order c2o WHERE c2o.customer_id ='" . (int) $partner_id . "'  AND c2o.deleted='0'  ";

        if (isset($filter_data['date_added_from']) || isset($filter_data['date_added_to'])) {
            if ($filter_data['date_added_from'] && $filter_data['date_added_to']) {
                $sql .= " AND c2o.date_added >= '" . $filter_data['date_added_from'] . "' && c2o.date_added <= '" . $filter_data['date_added_to'] . "' ";
            } else if ($filter_data['date_added_from'] && !$filter_data['date_added_to']) {
                $sql .= " AND c2o.date_added >= '" . $filter_data['date_added_from'] . "' ";
            } else if (!$filter_data['date_added_from'] && $filter_data['date_added_to']) {
                $sql .= " AND c2o.date_added <= '" . $filter_data['date_added_to'] . "' ";
            }
        }

        $sql .= " HAVING SUM(c2o.quantity) >= 0 ";

        if (isset($filter_data['total_amount_from']) || isset($filter_data['total_amount_to'])) {
            if ($filter_data['total_amount_from'] && $filter_data['total_amount_to']) {
                $sql .= " AND (SUM(c2o.customer) + SUM(c2o.admin)) > " . $filter_data['total_amount_from'] . " && (SUM(c2o.customer) + SUM(c2o.admin)) < " . $filter_data['total_amount_to'] . " ";
            } else if ($filter_data['total_amount_from'] && !$filter_data['total_amount_to']) {
                $sql .= " AND (SUM(c2o.customer) + SUM(c2o.admin)) > " . $filter_data['total_amount_from'] . " ";
            } else if (!$filter_data['total_amount_from'] && $filter_data['total_amount_to']) {
                $sql .= " AND (SUM(c2o.customer) + SUM(c2o.admin)) < " . $filter_data['total_amount_to'] . "";
            }
        }

        if (isset($filter_data['seller_amount_from']) || isset($filter_data['seller_amount_to'])) {
            if ($filter_data['seller_amount_from'] && $filter_data['seller_amount_to']) {
                $sql .= " AND SUM(c2o.customer) > " . $filter_data['seller_amount_from'] . " && SUM(c2o.customer) < " . $filter_data['seller_amount_to'] . " ";
            } else if ($filter_data['seller_amount_from'] && !$filter_data['seller_amount_to']) {
                $sql .= " AND SUM(c2o.customer) > " . $filter_data['seller_amount_from'] . " ";
            } else if (!$filter_data['seller_amount_from'] && $filter_data['seller_amount_to']) {
                $sql .= " AND SUM(c2o.customer) < " . $filter_data['seller_amount_to'] . "";
            }
        }

        if (isset($filter_data['admin_amount_from']) || isset($filter_data['admin_amount_to'])) {
            if ($filter_data['admin_amount_from'] && $filter_data['admin_amount_to']) {
                $sql .= " AND SUM(c2o.admin) > " . $filter_data['admin_amount_from'] . " && SUM(c2o.admin) < " . $filter_data['admin_amount_to'] . " ";
            } else if ($filter_data['admin_amount_from'] && !$filter_data['admin_amount_to']) {
                $sql .= " AND SUM(c2o.admin) > " . $filter_data['admin_amount_from'] . " ";
            } else if (!$filter_data['admin_amount_from'] && $filter_data['admin_amount_to']) {
                $sql .= " AND SUM(c2o.admin) < " . $filter_data['admin_amount_to'] . "";
            }
        }

        // echo $sql;
        $total = $this->db->query($sql)->row;

        return($total);
    }

    public function getPartnerOrders($partner_id, $date_start, $date_end) {

        $sql = "SELECT DISTINCT(cp2o.id),cp2o.*,(cp2o.price * cp2o.currency_value) 'price', o.currency_code, o.vat_info, o.currency_value as currency_value, o.date_added, p.sku, p.model,p.is_fbs,p.fbs_bin,p.fbs_stack, pd.name, (SELECT os.name FROM order_status os WHERE os.order_status_id=cp2o.order_product_status AND os.language_id='1') status 
FROM order_history oh 
LEFT JOIN customerpartner_to_order cp2o ON oh.order_id = cp2o.order_id   AND cp2o.deleted='0' 
LEFT JOIN `order` o ON(oh.order_id=o.order_id AND o.deleted='0') 
LEFT JOIN product p ON(cp2o.product_id=p.product_id) 
LEFT JOIN product_description pd ON(p.product_id=pd.product_id) 
LEFT JOIN customerpartner_to_order_status cp2s ON(cp2o.order_id = cp2s.order_id) 
WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' 
AND cp2o.customer_id = '" . (int) $partner_id . "'
AND ( (o.date_added >= '" . $this->db->escape($date_start) . "' 
AND o.date_added < '" . $this->db->escape($date_end) . "' ) 
OR (SELECT count(*) FROM customerpartner_to_order_status WHERE order_id = cp2o.order_id AND oh.deleted='0'  AND oh.order_status_id <> 0 AND product_id = cp2o.product_id AND date_added BETWEEN '" . $this->db->escape($date_start) . "' AND '" . $this->db->escape($date_end) . "' AND order_status_id = cp2o.order_product_status AND order_status_id in (5,22) ) > 0 
OR ( oh.date_added >= '" . $this->db->escape($date_start) . "' AND oh.date_added < '" . $this->db->escape($date_end) . "' AND oh.deleted='0' AND oh.order_status_id in (5,22) )
)";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function statusChangedBeforeRange($order_id, $product_id, $status, $from, $to) {
        $order_id = (int) $order_id;
        $product_id = (int) $product_id;
        $status = (int) $status;
        $from = $this->db->escape($from);
        $to = $this->db->escape($to);
        $sql = "SELECT
  COUNT(*) AS statuscount
FROM
  customerpartner_to_order_status
WHERE
  order_id = '$order_id' AND product_id = '$product_id' AND date_added BETWEEN '$from' AND  DATE_ADD('$to', INTERVAL 1 DAY)  AND order_status_id = '$status' AND customerpartner_to_order_status.order_id NOT IN(
  SELECT
    c.order_id
  FROM
    customerpartner_to_order_status c
  WHERE
    c.order_id = ".$order_id." AND c.product_id = ".$product_id." AND c.order_status_id = '$status' AND c.date_added < '$from'
)";
        $result = $this->db->query($sql);
        // print_r($result);die;
        // if($order_id == 1360)
        // {
        //     print_r($result);die;
        // }
        return !($result->row['statuscount']);
    }

    public function getPartnerAmountTotal($partner_id, $filter_data = array()) {
        $sql = "SELECT SUM(c2o.quantity) quantity,SUM(c2o.price) total,SUM(c2o.admin) admin,SUM(c2o.customer) customer FROM " . DB_PREFIX . "customerpartner_to_order c2o WHERE c2o.customer_id ='" . (int) $partner_id . "'  AND c2o.deleted='0'  ";
        //echo $sql;

        if ($filter_data['commission_from'] && $filter_data['commission_to']) {
            $url .= '';
        } else if ($filter_data['commission_from'] && !$filter_data['commission_to']) {
            $url .= '';
        } else if (!$filter_data['commission_from'] && $filter_data['commission_to']) {
            $url .= '';
        }

        $total = $this->db->query($sql)->row;

        $paid = $this->db->query("SELECT SUM(c2t.amount) total FROM " . DB_PREFIX . "customerpartner_to_transaction c2t WHERE c2t.customer_id ='" . (int) $partner_id . "'")->row;

        $total['paid'] = $paid['total'];

        return(count($total));
    }

    public function getSellerOrdersList($seller_id, $filter_data) {

        $sql = "SELECT DISTINCT op.order_id,c2o.paid_status,c2o.commission_applied,c2o.customer as need_to_pay,o.date_added, CONCAT(o.firstname ,' ',o.lastname) name ,os.name orderstatus,op.*, (SELECT group_concat( concat( value) SEPARATOR ', ') FROM " . DB_PREFIX . "order_option oo WHERE oo.order_product_id=c2o.order_product_id ) as value  FROM " . DB_PREFIX . "order_status os LEFT JOIN `" . DB_PREFIX . "order` o ON (os.order_status_id = o.order_status_id  AND o.deleted='0' ) LEFT JOIN " . DB_PREFIX . "customerpartner_to_order c2o ON (o.order_id = c2o.order_id) LEFT JOIN " . DB_PREFIX . "order_product op ON op.order_product_id=c2o.order_product_id WHERE c2o.customer_id = '" . (int) $seller_id . "' AND os.language_id = '" . $this->config->get('config_language_id') . "'  AND c2o.deleted='0'  AND oo.deleted='0'   ";

        if (isset($filter_data['date_added_from']) || isset($filter_data['date_added_to'])) {
            if ($filter_data['date_added_from'] && $filter_data['date_added_to']) {
                $sql .= " AND o.date_added >= '" . $filter_data['date_added_from'] . "' && o.date_added <= '" . $filter_data['date_added_to'] . "' ";
            } else if ($filter_data['date_added_from'] && !$filter_data['date_added_to']) {
                $sql .= " AND o.date_added >= '" . $filter_data['date_added_from'] . "' ";
            } else if (!$filter_data['date_added_from'] && $filter_data['date_added_to']) {
                $sql .= " AND o.date_added <= '" . $filter_data['date_added_to'] . "' ";
            }
        }

        if ($filter_data['order_id']) {
            $sql .= " AND op.order_id = '" . $filter_data['order_id'] . "' ";
        }

        if ($filter_data['payable_amount']) {
            $sql .= " AND c2o.customer = " . (float) $filter_data['payable_amount'] . " ";
        }

        if ($filter_data['quantity']) {
            $sql .= " AND op.quantity = '" . $filter_data['quantity'] . "' ";
        }

        // if($filter_data['date_added']) {
        // 	$sql .= " AND o.date_added like '%".$filter_data['date_added']."' " ;
        // }

        if ($filter_data['order_status']) {
            $sql .= " AND os.name = '" . $filter_data['order_status'] . "' ";
        }

        if ($filter_data['paid_status']) {
            if ($filter_data['paid_status'] == 'paid')
                $filter_data['paid_status'] = 1;
            else
                $filter_data['paid_status'] = 0;
            $sql .= " AND c2o.paid_status = '" . $filter_data['paid_status'] . "' ";
        }
        if ($filter_data['order_by'] && $filter_data['sort_by']) {
            $sql .= "ORDER BY " . $filter_data['order_by'] . " " . $filter_data['sort_by'] . " LIMIT " . $filter_data['start'] . ", " . $filter_data['limit'] . "";
        } else {
            $sql .= "ORDER BY o.order_id asc LIMIT " . $filter_data['start'] . ", " . $filter_data['limit'] . "";
        }

        $result = $this->db->query($sql);
        return($result->rows);
    }

    public function getProductOptions($order_product_id) {
        return $this->db->query("SELECT oo.value FROM " . DB_PREFIX . "order_option oo WHERE oo.order_product_id = '" . (int) $order_product_id . "'   AND oo.deleted='0'")->rows;
    }

    public function getSellerOrders($seller_id) {

        $sql = $this->db->query("SELECT DISTINCT o.order_id ,o.date_added, CONCAT(o.firstname ,' ',o.lastname) name ,os.name orderstatus  FROM " . DB_PREFIX . "order_status os LEFT JOIN " . DB_PREFIX . "`order` o ON (os.order_status_id = o.order_status_id AND o.deleted='0' ) LEFT JOIN " . DB_PREFIX . "customerpartner_to_order c2o ON (o.order_id = c2o.order_id) WHERE c2o.customer_id = '" . (int) $seller_id . "'  AND c2o.deleted='0' AND os.language_id = '" . $this->config->get('config_language_id') . "' ORDER BY o.order_id DESC ");

        return($sql->rows);
    }

    public function getTotalSellerOrders($seller_id, $filter_data) {

        $sql = "SELECT DISTINCT op.order_id,c2o.paid_status,c2o.customer as need_to_pay,o.date_added, CONCAT(o.firstname ,' ',o.lastname) name ,os.name orderstatus,op.*, (SELECT group_concat( concat( value) SEPARATOR ', ') FROM " . DB_PREFIX . "order_option oo WHERE oo.order_product_id=c2o.order_product_id   AND oo.deleted='0' ) as value  FROM " . DB_PREFIX . "order_status os LEFT JOIN `" . DB_PREFIX . "order` o ON (os.order_status_id = o.order_status_id  AND o.deleted='0' ) LEFT JOIN " . DB_PREFIX . "customerpartner_to_order c2o ON (o.order_id = c2o.order_id) LEFT JOIN " . DB_PREFIX . "order_product op ON op.order_product_id=c2o.order_product_id WHERE c2o.customer_id = '" . (int) $seller_id . "'  AND c2o.deleted='0'  AND os.language_id = '" . $this->config->get('config_language_id') . "' ";

        if (isset($filter_data['date_added_from']) || isset($filter_data['date_added_to'])) {
            if ($filter_data['date_added_from'] && $filter_data['date_added_to']) {
                $sql .= " AND o.date_added >= '" . $filter_data['date_added_from'] . "' && o.date_added <= '" . $filter_data['date_added_to'] . "' ";
            } else if ($filter_data['date_added_from'] && !$filter_data['date_added_to']) {
                $sql .= " AND o.date_added >= '" . $filter_data['date_added_from'] . "' ";
            } else if (!$filter_data['date_added_from'] && $filter_data['date_added_to']) {
                $sql .= " AND o.date_added <= '" . $filter_data['date_added_to'] . "' ";
            }
        }

        if ($filter_data['order_id']) {
            $sql .= " AND op.order_id = '" . $filter_data['order_id'] . "' ";
        }

        if ($filter_data['payable_amount']) {
            $sql .= " AND c2o.customer = '" . $filter_data['payable_amount'] . "' ";
        }

        if ($filter_data['quantity']) {
            $sql .= " AND op.quantity = '" . $filter_data['quantity'] . "' ";
        }

        if ($filter_data['order_status']) {
            $sql .= " AND os.name = '" . $filter_data['order_status'] . "' ";
        }

        if ($filter_data['paid_status']) {
            if ($filter_data['paid_status'] == 'paid')
                $filter_data['paid_status'] = 1;
            else
                $filter_data['paid_status'] = 0;
            $sql .= " AND c2o.paid_status = '" . $filter_data['paid_status'] . "' ";
        }

        $result = $this->db->query($sql);

        return(count($result->rows));
    }

    public function getSellerOrderProducts($order_id) {

        $sql = $this->db->query("SELECT op.*,c2o.price c2oprice FROM " . DB_PREFIX . "customerpartner_to_order c2o LEFT JOIN " . DB_PREFIX . "order_product op ON (c2o.order_product_id = op.order_product_id AND c2o.order_id = op.order_id) WHERE c2o.order_id = '" . $order_id . "'  AND c2o.deleted='0'  ORDER BY op.product_id ");

        return($sql->rows);
    }

    public function validate_url($partner_id, $clienturl) {
        //this function is added for the validation of the seo_url
        $is_unique = $this->db->query("SELECT * FROM url_partner where query <> 'partner_id=" . $partner_id . "' and keyword='seller/" . $clienturl . "'");
        return empty($is_unique->rows);
    }

    public function getPartnerProducts($customer_id) {
        $query = $this->db->query("select cp2p.product_id,name FROM customerpartner_to_product cp2p LEFT JOIN product_description pd ON (cp2p.product_id=pd.product_id) where customer_id='" . $customer_id . "' and pd.language_id='" . (int) $this->config->get('config_language_id') . "'");
        return $query->rows;
    }

    public function validate_products($product_ids = array(), $customer_id) {
        $query = $this->db->query("select cp2p.product_id,cp2p.customer_id,concat(c.firstname,' ',c.lastname) as name FROM customerpartner_to_product cp2p LEFT JOIN customer c ON (cp2p.customer_id=c.customer_id) where product_id IN (" . join(',', $product_ids) . ") AND cp2p.customer_id<>'" . $customer_id . "'");
        return $query->rows;
    }

    public function getCustomersForProducts($data = array()) {

        $sql = "SELECT cp2c.customer_id, cp2cd.companyname FROM " . DB_PREFIX . "customerpartner_to_customer cp2c LEFT JOIN customerpartner_to_customer_description cp2cd ON(cp2c.customer_id = cp2cd.customer_id) WHERE cp2cd.language_id = '" . (int) $this->config->get('config_language_id') . "' ";

        if (!empty($data['seller_id'])) {
            $sql .= " AND cp2c.customer_id= '" . (int) $data['seller_id'] . "'";
        }
        if (!empty($data['filter_name'])) {
            $sql .= " AND LCASE(CONCAT(cp2cd.screenname, ' ', cp2cd.companyname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        $sort_data = array(
            'cp2c.customer_id',
            'name'
        );


        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY cp2c.customer_id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getSellerList($customer_id) {
        $sql = "SELECT cp2c.customer_id as customer_id,cp2c.commission,c.firstname,c.lastname,c.email,c.telephone as phone ,cp2c.paypalid as bank ,cp2cd.companyname as company FROM " . DB_PREFIX . "customerpartner_to_customer cp2c LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer_description cp2cd ON cp2c.customer_id = cp2cd.customer_id LEFT JOIN " . DB_PREFIX . "customer c ON cp2c.customer_id = c.customer_id WHERE c.customer_id = '" . $customer_id . "' AND cp2cd.language_id='" . (int) $this->config->get('config_language_id') . "'";

        // $sql .= " LIMIT ".$data_filter['start'].",".$data_filter['limit']." ";
        // echo $sql;
        $result = $this->db->query($sql)->rows;
        return $result;
    }

    public function getReturns($customer_id, $month, $year) {
        $result = $this->db->query("SELECT wro.id,wro.order_id,wrr.reason,wrp.quantity,pd.name AS product_name FROM wk_rma_order wro LEFT JOIN wk_rma_product wrp ON (wro.id=wrp.rma_id) LEFT JOIN wk_rma_reason wrr ON (wrp.reason = wrr.reason_id AND wrr.language_id='" . (int) $this->config->get('config_language_id') . "') LEFT JOIN product_description pd ON (wrp.product_id = pd.product_id AND pd.language_id='" . (int) $this->config->get('config_language_id') . "') WHERE  wro.customer_id='" . (int) $customer_id . "' AND '" . (int) $month . "'=month(wro.date) AND '" . (int) $year . "' = year(wro.date)");
        return $result->rows;
    }

    public function editCustomerPass($customer_id, $data) {

        if ($data['password']) {
            $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE customer_id = '" . (int) $customer_id . "'");
        }
    }

    public function editToken($customer_id, $token) {
        $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_customer SET token = '" . $this->db->escape($token) . "' WHERE customer_id = '" . (int) $customer_id . "'");
    }

    public function getCompletedMonthlyOrders($date_start, $date_end, $account_manager) {
        $account_manager_query = ($account_manager != '*') ? " AND c2c.user_id = " . (int) $account_manager : "";
        $query = $this->db->query(" SELECT o.currency_code, c.curcode as seller_currency, o.date_added, c2c.customer_id, c2cd.companyname AS companyname, c2c.country_id AS seller_country_id, c.iso_code_2 AS seller_country, c2c.is_fbs as is_seller_fbs ,c2c.fbs_date_in,c2c.fbs_free_storage_days, c2c.fbs_inventory_volume_unit, c2c.fbs_inventory_volume , c2o.order_id, c2c.commission, (c2o.price * c2o.currency_value)  AS total_price, c2c.flat_rate AS flat_rate, order_product_status, o.payment_country_id, oc.iso_code_2 order_country,c2o.product_id, c2c.fbs_storage_charges as fbs_storage_charges , c2c.fbs_fullfillment_charges as fbs_fullfillment_charges, p.is_fbs as is_product_fbs 
                                        FROM `order_history` oh 
                                        LEFT JOIN `order` o ON oh.order_id = o.order_id AND o.deleted='0'  
                                        LEFT JOIN customerpartner_to_order c2o ON(o.order_id = c2o.order_id) 
                                        LEFT JOIN product p ON(c2o.product_id = p.product_id) 
                                        LEFT JOIN customerpartner_to_customer c2c ON c2o.customer_id=c2c.customer_id 
                                        LEFT JOIN customerpartner_to_customer_description c2cd ON (c2c.customer_id = c2cd.customer_id) 
                                        LEFT JOIN country c ON (c2c.country_id=c.country_id) 
                                        LEFT JOIN country oc ON o.payment_country_id=oc.country_id 
                                        WHERE oh.date_added BETWEEN '" . $this->db->escape($date_start) . "' AND DATE_ADD('" . $this->db->escape($date_end) . "', INTERVAL 1 DAY) 
                                        AND c2o.deleted='0' AND oh.deleted='0'  AND c2cd.language_id='" . (int) $this->config->get('config_language_id') . "' 
                                        AND (oh.order_status_id = 5 OR oh.order_status_id = 22) AND (c2o.order_product_status = 5 OR c2o.order_product_status = 22) " .
                                        $account_manager_query . " group by c2o.id");
        return $query->rows;
    }

    public function getMonthlyOrders($date_start, $date_end, $partner_id, $country_id) {
        $query = $this->db->query(" SELECT c2c.customer_id, c2cd.companyname AS companyname, c2o.order_id FROM `customerpartner_to_order` c2o LEFT JOIN customerpartner_to_customer c2c ON(c2o.customer_id = c2c.customer_id) LEFT JOIN customerpartner_to_customer_description c2cd ON (c2c.customer_id = c2cd.customer_id) LEFT JOIN `order` o ON(c2o.order_id=o.order_id AND o.deleted='0') WHERE c2o.date_added >= '" . $this->db->escape($date_start) . "'  AND c2o.deleted='0'  AND c2o.date_added < '" . $this->db->escape($date_end) . "' AND c2o.customer_id = '" . (int) $partner_id . "' AND o.payment_country_id = '" . (int) $country_id . "' GROUP BY c2o.order_id ");
        return $query->rows;
    }

    public function getCompletedOrdersByPeriod($date_start, $date_end, $order_status) {
        $query = $this->db->query(" SELECT
          o.order_id,
          c2o.customer_id,
          c2o.order_product_status ,
          c2o.product_id,
          c2cd.companyname,
          CONCAT(o.payment_firstname,' ',o.payment_lastname) as buyer_name,
          cd.name as payment_country,
          o.payment_code,
          ifnull(ot.value,0) as codfees,
          o.currency_code as currency,
          o.currency_value as currency_value,
          o.vat_info as vat_info,
          o.date_added as date_added
      FROM `customerpartner_to_order_status` COS
      LEFT JOIN order_total  ot
        ON (COS.order_id = ot.order_id AND ot.code = 'codfees' AND ot.deleted='0')
      LEFT JOIN `order` o
        ON COS.order_id = o.order_id AND o.deleted='0'
      LEFT JOIN `customerpartner_to_order` c2o
        ON o.order_id=c2o.order_id
      LEFT JOIN `customerpartner_to_customer_description` c2cd
        ON c2o.customer_id = c2cd.customer_id AND c2cd.language_id=" . (int) $this->config->get('config_language_id') . "
      LEFT JOIN country c
        ON o.payment_country_id = c.country_id
      LEFT JOIN country_description cd
        ON (c.country_id = cd.country_id AND cd.language_id='" . (int) $this->config->get('config_language_id') . "')
      WHERE  COS.date_added BETWEEN '" . $this->db->escape($date_start) . "' AND  DATE_ADD('" . $this->db->escape($date_end) . "', INTERVAL 1 DAY)
             AND COS.order_status_id=" . (int) $order_status . "  AND c2o.deleted='0'  AND COS.deleted='0'  GROUP by o.order_id,c2o.product_id");

        return $query->rows;
    }

    public function getOrderTotals($order_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int) $order_id . "' AND deleted='0' ORDER BY sort_order");

        return $query->rows;
    }

    public function getUnCompltedCount($order_id) {
        //select id,order_id,customer_id,product_id,price,quantity,order_product_status from customerpartner_to_order where order_id = 1973
        //select cto.id,cto.order_id,cto.customer_id,cto.product_id,cto.price,op.quantity,cto.order_product_status from customerpartner_to_order cto LEFT JOIN order_product op ON (cto.order_product_id = op.order_product_id)   where cto.order_id = 1812;
        $query = $this->db->query("select op.order_id,sum(cto.price) as total from customerpartner_to_order cto LEFT JOIN order_product op ON (cto.order_product_id = op.order_product_id)   where cto.order_id = ".(int)$order_id."  AND cto.deleted='0'  AND op.quantity > 0 AND cto.order_product_status <> 5 GROUP BY order_id;");
        if(isset($query->row['total']))
              return $query->row['total'];
        return 0;
    }

    public function update_order_product_filename($order_id ,$order_product_id ,$customer_id ,$file_name){
      $this->db->query("UPDATE customerpartner_to_order set invoice_name = '".$this->db->escape($file_name)."',order_product_status=22 WHERE order_id=".(int) $order_id ." AND customer_id=".(int)$customer_id." AND order_product_id=".(int)$order_product_id ." AND order_product_status=5");
    }

    public function getSellerInvoicename($customer_id , $month , $year){
      $query = $this->db->query("SELECT invoice_name FROM customerpartner_to_invoice where customer_id = $customer_id AND month= $month AND year= $year");
      if($query->num_rows){
        return $query->row['invoice_name'];
      }
      return '';
    }

    public function getExecludedCountries($customer_id){
      $query = $this->db->query("SELECT * FROM seller_execluded_countries WHERE customer_id=".(int)$customer_id);
      if(!empty($query->num_rows)){
        return $query->rows;
      }
      return array();
    }

    public function getCompletedOrderDate($order_id,$product_id)
    {
        $query = $this->db->query("SELECT date_added FROM customerpartner_to_order_status WHERE order_status_id = 5 AND order_id = '". (int) $order_id ."' AND product_id = '". (int) $product_id  ."';");
        if($query->num_rows){
            return $query->row['date_added'];
        }
        return false;
    }

    public function getPendingOrderDate($order_id)
    {
        $query = $this->db->query("SELECT date_added FROM order_history WHERE order_status_id = 1 AND order_id = '". (int) $order_id ."' ORDER BY order_history_id DESC lIMIT 1;");
        if($query->num_rows){
            return $query->row['date_added'];
        }
        return false;
    }

    public function getTotalNotInRange($order_id,$date_start,$date_end) {
        $total_price = 0;
        $total = $this->db->query("SELECT CO.price
                                        FROM customerpartner_to_order CO 
                                        INNER JOIN customerpartner_to_order_status COS ON (CO.product_id=COS.product_id)
                                        WHERE COS.order_id ='" . (int) $order_id . "' AND CO.order_id='" . (int) $order_id . "' AND COS.order_status_id=5 
                                        AND (COS.date_added < '$date_start 00:00:00' OR COS.date_added > '$date_end 23:59:59')
                                        GROUP BY CO.order_product_id
                                        ")->rows;
        foreach ($total as $product) {
            $total_price += $product['price'];
        }
        return $total_price;
    }

}
