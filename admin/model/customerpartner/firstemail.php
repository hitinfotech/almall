<?php

class ModelCustomerpartnerFirstemail extends Model {

    public function customerpartner($order_id) {
        $order_status = 'Pending';
        $order_info = $this->getOrder($order_id);

        $shipping_address = $this->getShippingAddressId($order_id);
        $shipping_address = $this->getAddress($shipping_address['address_id']);

        $order_product_query = $this->db->query("SELECT op.product_id,op.order_product_id,op.total,op.price, op.name, op.model, op.quantity,p.sku FROM " . DB_PREFIX . "order_product op LEFT JOIN product p ON (op.product_id = p.product_id) WHERE order_id = '" . (int) $order_id . "'");

        $mailToSellers = array();

        $shipping = 0;
        $seller_total_price = array();
        $paid_shipping = false;
        $admin_shipping_method = false;
        $resultData = $this->sellerAdminData($this->getOrderProducts($order_id));

        if ($this->config->get('marketplace_allowed_shipping_method')) {
            if (in_array($order_info['shipping_code'], $this->config->get('marketplace_allowed_shipping_method'))) {
                $admin_shipping_method = true;

                // if($this->config->get('marketplace_divide_shipping') &&  $this->config->get('marketplace_divide_shipping') == 'yes'){
                $sellers_count = count($resultData);
                foreach ($resultData as $key => $value) {
                    if ($value['seller'] == 'admin') {
                        $sellers_count = count($resultData) - 1;
                    }
                }

                if ($sellers_count == 1) {
                    $shipping = $this->session->data['shipping_method']['cost'];
                }

                // }
            }
        }

        foreach ($order_product_query->rows as $product) {
            $prsql = '';
            $mpSellers = $this->db->query(" SELECT c2c.email, c2c.customer_id,CONCAT(c2c.firstname,' ' ,c2c.lastname) as seller_name,p.product_id,p.subtract,c2c.commission FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "customerpartner_to_product c2p ON (p.product_id = c2p.product_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer c2c ON (c2c.customer_id = c2p.customer_id) WHERE p.product_id = '" . $product['product_id'] . "' $prsql ORDER BY c2p.id ASC ")->row;

            if (isset($mpSellers['email']) AND ! empty($mpSellers['email'])) {
                if (!isset($seller_total_price[$mpSellers['customer_id']]) || $seller_total_price[$mpSellers['customer_id']] == '') {
                    $seller_total_price[$mpSellers['customer_id']] = 0;
                }

                $option_data = array();

                $order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int) $order_id . "'  AND deleted='0'  AND order_product_id = '" . (int) $product['order_product_id'] . "'");

                foreach ($order_option_query->rows as $option) {
                    if ($option['type'] != 'file') {
                        $value = $option['value'];
                    } else {
                        $value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
                    }

                    $option_data[] = array(
                        'name' => $option['name'],
                        'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                    );
                }

                $product_total = $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0);


                $products = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'SKU' => $product['sku'],
                    'option' => $option_data,
                    'quantity' => $product['quantity'],
                    'product_total' => $product_total, // without symbol
                    'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
                );

                $i = 0;

                //add commission entry
                $commission_array = $this->calculateCommission($products, $mpSellers['customer_id']);

                $shipping_applied = '';


                if ($order_info['shipping_code'] != 'wk_custom_shipping.wk_custom_shipping' && $admin_shipping_method) {
                    if (!$paid_shipping) {
                        $commission_array['customer'] = $commission_array['customer'] + $shipping;
                        $paid_shipping = true;

                        $shipping_applied = $shipping;
                    }
                } else if ($admin_shipping_method) {
                    $sellerDetails = array();

                    $sellerDetails[$mpSellers['customer_id']] = $resultData[$mpSellers['customer_id']];

                    $shipping_quote = $this->model_shipping_wk_custom_shipping->getQuote($shipping_address, $sellerDetails);
                }

                if (isset($shipping_quote['quote']['wk_custom_shipping']['cost']) && $shipping_quote['quote']['wk_custom_shipping']['cost']) {
                    $commission_array['customer'] = $commission_array['customer'] + $shipping_quote['quote']['wk_custom_shipping']['cost'];

                    $shipping_applied = $shipping_quote['quote']['wk_custom_shipping']['cost'];
                }

                $seller_total_price[$mpSellers['customer_id']] += $product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0);

                if ($mailToSellers) {

                    foreach ($mailToSellers as $key => $value) {
                        foreach ($value as $key1 => $value1) {
                            $i = 0;
                            if ($key1 == 'email' AND $value1 == $mpSellers['email']) {
                                $mailToSellers[$key]['products'][] = $products;
                                $mailToSellers[$key]['total'] = $product_total + $mailToSellers[$key]['total'];
                                break;
                            } else {
                                $i++;
                            }
                        }
                    }
                } else {
                    $mailToSellers[] = array('email' => $mpSellers['email'],
                        'customer_id' => $mpSellers['customer_id'],
                        'seller_email' => $mpSellers['email'],
                        'seller_name' => $mpSellers['seller_name'],
                        'products' => array(0 => $products),
                        'total' => $product_total
                    );
                }

                if ($i > 0) {
                    $mailToSellers[] = array('email' => $mpSellers['email'],
                        'customer_id' => $mpSellers['customer_id'],
                        'seller_email' => $mpSellers['email'],
                        'seller_name' => $mpSellers['seller_name'],
                        'products' => array(0 => $products),
                        'total' => $product_total
                    );
                }
            }
        }

        $task_w11mds242 = true;

        if ($order_info['payment_code'] != 'cod') {
            $task_w11mds242 = false;
        }

        if ($this->config->get('marketplace_mailtoseller') && $task_w11mds242 == true) {

            // Send out order confirmation mail
            $language = new Language($order_info['language_directory']);
            $language->load($order_info['language_directory']);
            $language->load('default');
            $language->load('mail/seller_order');

            $subject = sprintf($language->get('text_new_subject_seller'), $order_id);

            // HTML Mail for seller
            $data = array();

            $data['title'] = sprintf($language->get('text_new_subject_seller'), $order_id);

            $data['text_link'] = '';//$language->get('text_new_link');
            $data['text_download'] = $language->get('text_new_download');
            $data['text_order_detail'] = $language->get('text_new_order_detail');
            $data['text_order_status'] = $language->get('text_order_status');

            $data['text_instruction'] = $language->get('text_new_instruction');
            $data['text_order_id'] = $language->get('text_new_order_id');
            $data['text_date_added'] = $language->get('text_new_date_added');
            $data['text_payment_method'] = $language->get('text_new_payment_method');
            $data['text_shipping_method'] = $language->get('text_new_shipping_method');
            $data['text_email'] = $language->get('text_new_email');
            $data['text_telephone'] = $language->get('text_new_telephone');
            $data['text_ip'] = $language->get('text_new_ip');
            $data['text_payment_address'] = $language->get('text_new_payment_address');
            $data['text_shipping_address'] = $language->get('text_new_shipping_address');
            $data['text_product'] = $language->get('text_new_product');
            $data['text_model'] = $language->get('text_new_model');
            $data['text_SKU'] = $language->get('text_new_sku');
            $data['text_quantity'] = $language->get('text_new_quantity');
            $data['text_price'] = $language->get('text_new_price');
            $data['text_total'] = $language->get('text_new_total');
            $data['text_powered'] = $language->get('text_new_powered');
            $data['text_buyer_name'] = $language->get('text_buyer_name');
            $data['text_buyer_country'] = $language->get('text_buyer_country');

            $data['logo'] = HTTPS_IMAGE_S3 . $this->config->get('config_logo_ltr');
            $data['store_name'] = $order_info['store_name'];
            $data['store_url'] = $order_info['store_url'];
            $data['customer_id'] = $order_info['customer_id'];
            $data['link'] = '';
            $data['linkurl'] = $order_info['store_url'] . 'partner/index.php?route=orderinfo&order_id=' . $order_id;

            $data['download'] = '';

            $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int) $order_id . "'");

            foreach ($order_product_query->rows as $order_product) {
                // Check if there are any linked downloads
                $product_download_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_download` WHERE product_id = '" . (int) $order_product['product_id'] . "'");

                if ($product_download_query->row['total']) {
                    $data['download'] = $order_info['store_url'] . 'index.php?route=account/customerpartner/download';
                }
            }

            $data['order_id'] = $order_id;
            $data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));
            $data['payment_method'] = $order_info['payment_method'];
            $data['shipping_method'] = $order_info['shipping_method'];
            $data['email'] = $order_info['email'];
            $data['telephone'] = $order_info['telephone'];
            $data['ip'] = $order_info['ip'];
            $data['order_status'] = $order_status;
            $data['buyer_name'] = $order_info['payment_firstname'] . " " . $order_info['payment_lastname'];
            $data['buyer_country'] = $order_info['payment_country'];
           // $order_page_link = $order_info['store_url'] . 'partner/index.php?route=orderlist&filter_status=Pending';

            // Products
            $data['products'] = array();

            // Text Mail for seller
            $textBasic = $language->get('text_new_order_id') . ' ' . $order_id . "\n";
            $textBasic .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
            $textBasic .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

            $data['vouchers'] = array();
            // $this->load->model('payment/amazon_checkout');
            // if (!$this->model_payment_amazon_checkout->isAmazonOrder($order_info['order_id'])) {

            $this->load->model('customerpartner/firstemailmail');
            //$this->load->model('account/customerpartner');
            //if order is failed dont send an email to the Seller.
            if ($order_status == 10) {
                return false;
            }

            foreach ($mailToSellers as $value) {
                //for template for seller
                $data['products'] = $value['products'];
                $data['text_greeting'] = sprintf($language->get('text_greeting'), $value['seller_name'], $data['linkurl']);
                // Order Totals
                $data['totals'][0] = array(
                    'title' => 'Total',
                    'text' => $this->currency->format($value['total']),
                );


                $html = $this->load->view('mail/seller_order.tpl', $data);

                //for text for seller
                $products = $value['products'];
                $text = $textBasic;
                foreach ($products as $product) {
                    $text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($product['total']) . "\n";

                    foreach ($product['option'] as $option) {
                        $text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($option['value'])) . "\n";
                    }
                }

                $text .= $language->get('text_new_order_total') . "\n";

                $text .= $this->currency->format($value['total'], $order_info['currency_code'], 1) . ': ' . html_entity_decode($this->currency->format($value['total'], $order_info['currency_code'], 1), ENT_NOQUOTES, 'UTF-8') . "\n";

                //$text .= $language->get('text_new_footer') . "\n\n";

                $mailData = array(
                    'seller_id' => $value['customer_id'],
                    'text' => $text,
                    'customer_id' => false,
                    'mail_id' => $this->config->get('marketplace_mail_order'),
                    'mail_from' => $this->config->get('marketplace_adminmail'),
                    'mail_to' => $value['seller_email'],
                    'title' => $data['title']
                );

                $values = array(
                    'order' => $html
                );

                $this->model_customerpartner_firstemailmail->mail($mailData, $values);
            }
        }
    }

    public function calculateCommission($product, $customer_id) {

        if ($product) {
            $categories_array = $this->db->query("SELECT p2c.category_id,c.parent_id FROM " . DB_PREFIX . "product_to_category p2c LEFT JOIN " . DB_PREFIX . "category c ON (p2c.category_id = c.category_id) WHERE p2c.product_id = '" . (int) $product['product_id'] . "' ORDER BY p2c.product_id ");

            if ($this->config->get('marketplace_commissionworkedon'))
                $categories = $categories_array->rows;
            else
                $categories = array($categories_array->row);

            //get commission array for priority
            $commission = $this->config->get('marketplace_boxcommission');
            $commission_amount = 0;
            $commission_type = '';

            if ($commission)
                foreach ($commission as $various) {
                    switch ($various) {
                        case 'category': //get all parent category according to product and process
                            if (isset($categories[0]) && $categories[0]) {

                                foreach ($categories as $category) {
                                    if ($category['parent_id'] == 0) {
                                        $category_commission = $this->getCategoryCommission($category['category_id']);
                                        if ($category_commission) {
                                            $commission_amount += ( $category_commission['percentage'] ? ($category_commission['percentage'] * $product['product_total'] / 100) : 0 ) + $category_commission['fixed'];
                                        }
                                    }
                                }
                                $commission_type = 'Category Based';
                                if ($commission_amount)
                                    break;
                            }

                        case 'category_child': //get all child category according to product and process
                            if (isset($categories[0]) && $categories[0]) {

                                foreach ($categories as $category) {
                                    if ($category['parent_id'] > 0) {
                                        $category_commission = $this->getCategoryCommission($category['category_id']);
                                        if ($category_commission) {
                                            $commission_amount += ( $category_commission['percentage'] ? ($category_commission['percentage'] * $product['product_total'] / 100) : 0 ) + $category_commission['fixed'];
                                        }
                                    }
                                }

                                $commission_type = 'Category Child Based';
                                if ($commission_amount)
                                    break;
                            }

                        default: //just get all amount and process on that (precentage based)
                            $customer_commission = $this->getCustomerCommission($customer_id);
                            if ($customer_commission) {
                                $commission_amount += $customer_commission['commission'] ? ($customer_commission['commission'] * $product['product_total'] / 100) : 0;
                            }

                            $commission_type = 'Partner Fixed Based';
                            break;
                    }
                    if ($commission_amount)
                        break;
                }

            /**
             * To calculate commission as per membership of seller
             */
            if ($this->config->get('wk_seller_group_status')) {
                $this->load->model('account/customer_group');
                $isMember = $this->model_account_customer_group->getSellerMembershipGroup($customer_id);
                if ($isMember) {
                    $membershipCommission = $this->model_account_customer_group->getMembershipGroupCommission($isMember['gid'], $product['product_id']);
                    if ($membershipCommission) {
                        $commission_amount += ( $membershipCommission['percentage'] ? ($membershipCommission['percentage'] * $product['product_total'] / 100) : 0 ) + $membershipCommission['fixed'];
                    }
                    $commission_type = 'Membership Based';
                }
            }

            $return_array = array(
                'commission' => $commission_amount,
                'customer' => $product['product_total'] - $commission_amount,
                'type' => $commission_type,
            );
            return($return_array);
        }
    }

    public function sellerAdminData($cart, $zip = '', $payment = false) {
        //price for payment
        //total for shipping

        $seller = array();

        if ($cart AND is_array($cart))
            foreach ($cart as $product) {

                $entry = 0;

                if (!$product['weight_class_id']) {
                    $product['weight_class_id'] = $this->config->get('config_weight_class_id');
                }

                if ($product['weight']) {
                    $weight = $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
                } else {
                    $weight = 0;
                }

                $weight = ($weight < 0.1 ? 0.1 : $weight);

                $seller_zip = $this->db->query(" SELECT c2c.customer_id, c2c.city, c.iso_code_2 as country, z.code as state, c2c.paypalid FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "customerpartner_to_product c2p ON (p.product_id = c2p.product_id) LEFT JOIN customerpartner_to_customer c2c ON (c2p.customer_id = c2c.customer_id) LEFT JOIN customer cu ON(cu.customer_id = c2p.customer_id) LEFT JOIN zone z ON (c2c.zone_id = z.zone_id) LEFT JOIN country c ON (c2c.country_id = c.country_id) WHERE p.product_id='" . $product['product_id'] . "'")->row;
                $seller_zip['postcode'] = '';
                if ($seller_zip) {

                    //partner will get product tax not admin or paste this line after - after me and comment line - comment me
                    $commission_array = $this->calculateCommission(array('product_id' => $product['product_id'], 'product_total' => $product['total']), $seller_zip['customer_id']);

                    //add taxes to seller amount
                    if ($this->config->get('config_tax')) {
                        $commission_array['customer'] += $this->tax->getTax($product['total'], $product['tax_class_id']); //comment me
                        $product['total'] = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
                    } // after me

                    if ($seller) {
                        foreach ($seller as $index => $sellers) {
                            if ($sellers['seller'] == $seller_zip['customer_id']) {
                                $seller[$index]['weight'] = (float) $sellers['weight'] + (float) $weight;
                                $seller[$index]['name'] = $sellers['name'] . ', ' . $product['name'];
                                $seller[$index]['total'] = $sellers['total'] + $product['total'];
                                $seller[$index]['price'] = (float) $commission_array['customer'] + (float) $sellers['price'];
                                $entry = 1;
                            }
                        }
                        if ($entry == 0) {
                            $zipCode = substr($seller_zip['postcode'], 0, 8);
                            $seller[$seller_zip['customer_id']] = array(
                                'seller' => $seller_zip['customer_id'],
                                'zip' => $zipCode,
                                'weight' => $weight,
                                'name' => $product['name'],
                                'city' => $seller_zip['city'],
                                'country' => $seller_zip['country'],
                                'state' => $seller_zip['state'],
                                'price' => $commission_array['customer'],
                                'total' => $product['total'],
                                'paypalid' => $seller_zip['paypalid'],
                                'primary' => false
                            );
                        }
                    } else {
                        $zipCode = substr($seller_zip['postcode'], 0, 8);
                        $seller[$seller_zip['customer_id']] = array(
                            'seller' => $seller_zip['customer_id'],
                            'zip' => $zipCode,
                            'weight' => $weight,
                            'name' => $product['name'],
                            'city' => $seller_zip['city'],
                            'country' => $seller_zip['country'],
                            'state' => $seller_zip['state'],
                            'price' => $commission_array['customer'],
                            'total' => $product['total'],
                            'paypalid' => $seller_zip['paypalid'],
                            'primary' => false
                        );
                    }

                    //admin -> if exists seller
                    if ($payment) {
                        foreach ($seller as $index => $sellers) {
                            if ($sellers['seller'] == 'Admin') {
                                $seller[$index]['price'] = (float) $sellers['price'] + (float) $commission_array['commission'];
                                // $seller[$index]['total'] = (float)$sellers['total'] + (float)$commission_array['commission'];
                                $seller[$index]['name'] = $sellers['name'] . ', Commission';
                                $entry = 1;
                            }
                        }
                        if ($entry == 0) {
                            $zipCode = substr($this->config->get($zip), 0, 8);
                            $seller[] = array(
                                'seller' => 'Admin',
                                'zip' => $zipCode,
                                'weight' => 0,
                                'name' => 'Commission',
                                'city' => $this->config->get('wkmpups_city'),
                                'country' => $this->config->get('wkmpups_country'),
                                'state' => $this->config->get('wkmpups_state'),
                                'price' => (float) $commission_array['commission'],
                                'total' => 0,
                                'paypalid' => $this->config->get('wk_adaptive_pay_email'),
                                'primary' => true
                            );
                        }
                    }
                } else {
                    //add taxes to seller amount
                    if ($this->config->get('config_tax'))
                        $product['total'] = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
                    // $product['total'] += $this->tax->getTax($product['total'], $product['tax_class_id']);

                    if ($seller) {
                        foreach ($seller as $index => $sellers) {
                            if ($sellers['seller'] == 'Admin') {
                                $seller[$index]['total'] = $sellers['total'] + $product['total'];
                                $seller[$index]['weight'] = (float) $sellers['weight'] + (float) $weight;
                                $seller[$index]['name'] = $sellers['name'] . ', ' . $product['name'];
                                $seller[$index]['price'] = (float) $sellers['price'] + (float) $product['total'];
                                $entry = 1;
                            }
                        }
                        if ($entry == 0) {
                            $zipCode = substr($this->config->get($zip), 0, 8);
                            $seller[] = array(
                                'seller' => 'Admin',
                                'zip' => $zipCode,
                                'weight' => $weight,
                                'name' => $product['name'],
                                'city' => $this->config->get('wkmpups_city'),
                                'country' => $this->config->get('wkmpups_country'),
                                'state' => $this->config->get('wkmpups_state'),
                                'price' => $product['total'],
                                'total' => $product['total'],
                                'paypalid' => $this->config->get('wk_adaptive_pay_email'),
                                'primary' => true,
                            );
                        }
                    } else {
                        $zipCode = substr($this->config->get($zip), 0, 8);
                        $seller[] = array(
                            'seller' => 'Admin',
                            'zip' => $zipCode,
                            'weight' => $weight,
                            'name' => $product['name'],
                            'city' => $this->config->get('wkmpups_city'),
                            'country' => $this->config->get('wkmpups_country'),
                            'state' => $this->config->get('wkmpups_state'),
                            'price' => $product['total'],
                            'total' => $product['total'],
                            'paypalid' => $this->config->get('wk_adaptive_pay_email'),
                            'primary' => true,
                        );
                    }
                }
            }


        return $seller;
    }

    public function getCategoryCommission($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_commission_category WHERE category_id = '" . (int) $category_id . "'");
        return $query->row;
    }

    public function getCustomerCommission($customer_id) {
        $query = $this->db->query("SELECT commission FROM " . DB_PREFIX . "customerpartner_to_customer WHERE customer_id = '" . (int) $customer_id . "'");
        return $query->row;
    }

    public function getShippingAddressId($order_id) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "customer c ON o.customer_id=c.customer_id WHERE order_id = '" . $order_id . "'  AND o.deleted='0' ";

        $result = $this->db->query($sql)->row;
        return $result;
    }

    public function getOrder($order_id) {
        $order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int) $order_id . "'  AND o.deleted='0' ");

        if ($order_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int) $order_query->row['payment_country_id'] . "'");

            if ($country_query->num_rows) {
                $payment_iso_code_2 = $country_query->row['iso_code_2'];
                $payment_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $payment_iso_code_2 = '';
                $payment_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int) $order_query->row['payment_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $payment_zone_code = $zone_query->row['code'];
            } else {
                $payment_zone_code = '';
            }

            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int) $order_query->row['shipping_country_id'] . "'");

            if ($country_query->num_rows) {
                $shipping_iso_code_2 = $country_query->row['iso_code_2'];
                $shipping_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $shipping_iso_code_2 = '';
                $shipping_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int) $order_query->row['shipping_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $shipping_zone_code = $zone_query->row['code'];
            } else {
                $shipping_zone_code = '';
            }

            $this->load->model('localisation/language');

            $language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

            if ($language_info) {
                $language_code = $language_info['code'];
                $language_directory = $language_info['directory'];
            } else {
                $language_code = '';
                $language_directory = '';
            }

            return array(
                'order_id' => $order_query->row['order_id'],
                'invoice_no' => $order_query->row['invoice_no'],
                'invoice_prefix' => $order_query->row['invoice_prefix'],
                'store_id' => $order_query->row['store_id'],
                'store_name' => $order_query->row['store_name'],
                'store_url' => $order_query->row['store_url'],
                'customer_id' => $order_query->row['customer_id'],
                'firstname' => $order_query->row['firstname'],
                'lastname' => $order_query->row['lastname'],
                'email' => $order_query->row['email'],
                'telephone' => $order_query->row['telephone'],
                'fax' => $order_query->row['fax'],
                'custom_field' => json_decode($order_query->row['custom_field'], true),
                'payment_firstname' => $order_query->row['payment_firstname'],
                'payment_lastname' => $order_query->row['payment_lastname'],
                'payment_company' => $order_query->row['payment_company'],
                'payment_address_1' => $order_query->row['payment_address_1'],
                'payment_address_2' => $order_query->row['payment_address_2'],
                'payment_postcode' => $order_query->row['payment_postcode'],
                'payment_city' => $order_query->row['payment_city'],
                'payment_zone_id' => $order_query->row['payment_zone_id'],
                'payment_zone' => $order_query->row['payment_zone'],
                'payment_zone_code' => $payment_zone_code,
                'payment_country_id' => $order_query->row['payment_country_id'],
                'payment_country' => $order_query->row['payment_country'],
                'payment_iso_code_2' => $payment_iso_code_2,
                'payment_iso_code_3' => $payment_iso_code_3,
                'payment_address_format' => $order_query->row['payment_address_format'],
                'payment_custom_field' => json_decode($order_query->row['payment_custom_field'], true),
                'payment_method' => $order_query->row['payment_method'],
                'payment_code' => $order_query->row['payment_code'],
                'shipping_firstname' => $order_query->row['shipping_firstname'],
                'shipping_lastname' => $order_query->row['shipping_lastname'],
                'shipping_company' => $order_query->row['shipping_company'],
                'shipping_address_1' => $order_query->row['shipping_address_1'],
                'shipping_address_2' => $order_query->row['shipping_address_2'],
                'shipping_postcode' => $order_query->row['shipping_postcode'],
                'shipping_city' => $order_query->row['shipping_city'],
                'shipping_zone_id' => $order_query->row['shipping_zone_id'],
                'shipping_zone' => $order_query->row['shipping_zone'],
                'shipping_zone_code' => $shipping_zone_code,
                'shipping_country_id' => $order_query->row['shipping_country_id'],
                'shipping_country' => $order_query->row['shipping_country'],
                'shipping_iso_code_2' => $shipping_iso_code_2,
                'shipping_iso_code_3' => $shipping_iso_code_3,
                'shipping_address_format' => $order_query->row['shipping_address_format'],
                'shipping_custom_field' => json_decode($order_query->row['shipping_custom_field'], true),
                'shipping_method' => $order_query->row['shipping_method'],
                'shipping_code' => $order_query->row['shipping_code'],
                'comment' => $order_query->row['comment'],
                'total' => $order_query->row['total'],
                'order_status_id' => $order_query->row['order_status_id'],
                'order_status' => $order_query->row['order_status'],
                'affiliate_id' => $order_query->row['affiliate_id'],
                'commission' => $order_query->row['commission'],
                'language_id' => $order_query->row['language_id'],
                'language_code' => $language_code,
                'language_directory' => $language_directory,
                'currency_id' => $order_query->row['currency_id'],
                'currency_code' => $order_query->row['currency_code'],
                'currency_value' => $order_query->row['currency_value'],
                'ip' => $order_query->row['ip'],
                'forwarded_ip' => $order_query->row['forwarded_ip'],
                'user_agent' => $order_query->row['user_agent'],
                'accept_language' => $order_query->row['accept_language'],
                'date_modified' => $order_query->row['date_modified'],
                'date_added' => $order_query->row['date_added']
            );
        } else {
            return false;
        }
    }

    public function getOrderProducts($order_id) {

        $product_data = array();

        $cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int) $order_id . "'");

        foreach ($cart_query->rows as $cart) {
            $stock = true;

            $product_query = $this->db->query("SELECT p.*,pd.* FROM " . DB_PREFIX . "order_product op INNER JOIN " . DB_PREFIX . "product p ON (op.product_id = p.product_id) INNER JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id='" . (int) $cart['product_id'] . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "' ");
            $order_options = $this->db->query("SELECT product_option_id, product_option_value_id FROM order_option WHERE order_id='" . (int) $order_id . "'  AND deleted='0'  AND order_product_id = '" . (int) $cart['order_product_id'] . "' ");
            $options = array();
            if ($order_options->num_rows) {
                foreach ($order_options->rows as $row) {
                    $options[$row['product_option_id']] = $row['product_option_value_id'];
                }
            }

            $cart['option'] = json_encode($options);

            $option_data = array();
            $otp_data = array();
            if ($cart['otp_id']) {

                $otpcount = 0;
                $otp_options = array();
                $otp_check_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "otp_option_value WHERE `id` = '" . $cart['otp_id'] . "' LIMIT 1");
                if ($otp_check_query->num_rows) {
                    $otp_options = array(
                        'parent' => $otp_check_query->row['parent_option_id'],
                        'child' => $otp_check_query->row['child_option_id'],
                        'grandchild' => $otp_check_query->row['grandchild_option_id']
                    );
                    foreach (json_decode($cart['option']) as $otp_option_id => $otp_option_value_id) {
                        if (in_array($otp_option_id, $otp_options)) {
                            $otpcount++;
                            $otp_option_query = $this->db->query("SELECT o.type,o.cod, od.name FROM `" . DB_PREFIX . "option` o LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE o.option_id = '" . $otp_option_id . "' AND od.language_id = '" . $this->config->get('config_language_id') . "'");
                            if ($otp_option_id == $otp_options['parent']) {
                                $otp_option_value_query = $this->db->query("SELECT ovd.name, otpd.model, otpd.quantity, otpd.subtract, otpd.price_prefix, otpd.price, otpd.special, otpd.weight_prefix, otpd.weight FROM " . DB_PREFIX . "otp_option_value otp INNER JOIN " . DB_PREFIX . "otp_data otpd ON otp.id = otpd.otp_id LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (otp.parent_option_value_id = ovd.option_value_id) WHERE otp.id = '" . $cart['otp_id'] . "' AND ovd.language_id = '" . $this->config->get('config_language_id') . "'");
                                if ($otp_option_value_query->row['subtract'] == 1 && (!$otp_option_value_query->row['quantity'] || ($otp_option_value_query->row['quantity'] < $cart['quantity']))) {
                                    $stock = false;
                                }
                                $option_data[] = array(
                                    'product_option_id' => $otp_option_id,
                                    'product_option_value_id' => $otp_option_value_id,
                                    'option_id' => $otp_option_id,
                                    'option_value_id' => $otp_option_value_id,
                                    'name' => $otp_option_query->row['name'],
                                    'value' => $otp_option_value_query->row['name'],
                                    'cod' => $otp_option_value_query->row['cod'],
                                    'type' => $otp_option_query->row['type']
                                );
                                $otp_data['otp_id'] = $cart['otp_id'];
                                $otp_data['model'] = $otp_option_value_query->row['model'];
                                $otp_data['price_prefix'] = $otp_option_value_query->row['price_prefix'];
                                $otp_data['price'] = $otp_option_value_query->row['price'];
                                $otp_data['special'] = $otp_option_value_query->row['special'];
                                $otp_data['weight_prefix'] = $otp_option_value_query->row['weight_prefix'];
                                $otp_data['weight'] = $otp_option_value_query->row['weight'];
                            } elseif ($otp_option_id == $otp_options['child']) {
                                $otp_option_value_query = $this->db->query("SELECT ovd.name, otpd.quantity, otpd.subtract FROM " . DB_PREFIX . "otp_option_value otp INNER JOIN " . DB_PREFIX . "otp_data otpd ON otp.id = otpd.otp_id LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (otp.child_option_value_id = ovd.option_value_id) WHERE otp.id = '" . $cart['otp_id'] . "' AND ovd.language_id = '" . $this->config->get('config_language_id') . "'");
                                if ($otp_option_value_query->row['subtract'] && (!$otp_option_value_query->row['quantity'] || ($otp_option_value_query->row['quantity'] < $cart['quantity']))) {
                                    $stock = false;
                                }
                                $option_data[] = array(
                                    'product_option_id' => $otp_option_id,
                                    'product_option_value_id' => $otp_option_value_id,
                                    'option_id' => $otp_option_id,
                                    'option_value_id' => $otp_option_value_id,
                                    'name' => $otp_option_query->row['name'],
                                    'value' => $otp_option_value_query->row['name'],
                                    'cod' => $otp_option_value_query->row['cod'],
                                    'type' => $otp_option_query->row['type']
                                );
                            } elseif ($otp_option_id == $otp_options['grandchild']) {
                                $otp_option_value_query = $this->db->query("SELECT ovd.name, otpd.quantity, otpd.subtract FROM " . DB_PREFIX . "otp_option_value otp INNER JOIN " . DB_PREFIX . "otp_data otpd ON otp.id = otpd.otp_id LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (otp.grandchild_option_value_id = ovd.option_value_id) WHERE otp.id = '" . $cart['otp_id'] . "' AND ovd.language_id = '" . $this->config->get('config_language_id') . "'");
                                if ($otp_option_value_query->row['subtract'] && (!$otp_option_value_query->row['quantity'] || ($otp_option_value_query->row['quantity'] < $cart['quantity']))) {
                                    $stock = false;
                                }
                                $option_data[] = array(
                                    'product_option_id' => $otp_option_id,
                                    'product_option_value_id' => $otp_option_value_id,
                                    'option_id' => $otp_option_id,
                                    'option_value_id' => $otp_option_value_id,
                                    'name' => $otp_option_query->row['name'],
                                    'value' => $otp_option_value_query->row['name'],
                                    'type' => $otp_option_query->row['type']
                                );
                            }
                        }
                    }
                } else {
                    continue;
                }
            }

            if ($product_query->num_rows && ($cart['quantity'] > 0)) {
                $option_price = 0;
                $option_points = 0;
                $option_weight = 0;

                //$option_data = array();

                foreach (json_decode($cart['option']) as $product_option_id => $value) {
                    $option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.front_name as name, o.type,o.cod FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.deleted != '1' AND po.product_option_id = '" . (int) $product_option_id . "' AND po.product_id = '" . (int) $cart['product_id'] . "' AND od.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                    if ($option_query->num_rows) {
                        if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image' || $option_query->row['type'] == 'list' || $option_query->row['type'] == 'size') {
                            $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.deleted != '1' AND pov.product_option_value_id = '" . (int) $value . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                            if ($option_value_query->num_rows) {
                                if ($option_value_query->row['price_prefix'] == '+') {
                                    $option_price += $option_value_query->row['price'];
                                } elseif ($option_value_query->row['price_prefix'] == '-') {
                                    $option_price -= $option_value_query->row['price'];
                                }

                                if ($option_value_query->row['points_prefix'] == '+') {
                                    $option_points += $option_value_query->row['points'];
                                } elseif ($option_value_query->row['points_prefix'] == '-') {
                                    $option_points -= $option_value_query->row['points'];
                                }

                                if ($option_value_query->row['weight_prefix'] == '+') {
                                    $option_weight += $option_value_query->row['weight'];
                                } elseif ($option_value_query->row['weight_prefix'] == '-') {
                                    $option_weight -= $option_value_query->row['weight'];
                                }

                                if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
                                    $stock = false;
                                }

                                $option_data[] = array(
                                    'product_option_id' => $product_option_id,
                                    'product_option_value_id' => $value,
                                    'option_id' => $option_query->row['option_id'],
                                    'option_value_id' => $option_value_query->row['option_value_id'],
                                    'name' => $option_query->row['name'],
                                    'value' => $option_value_query->row['name'],
                                    'cod' => $option_query->row['cod'],
                                    'type' => $option_query->row['type'],
                                    'quantity' => $option_value_query->row['quantity'],
                                    'subtract' => $option_value_query->row['subtract'],
                                    'price' => $option_value_query->row['price'],
                                    'price_prefix' => $option_value_query->row['price_prefix'],
                                    'points' => $option_value_query->row['points'],
                                    'points_prefix' => $option_value_query->row['points_prefix'],
                                    'weight' => $option_value_query->row['weight'],
                                    'weight_prefix' => $option_value_query->row['weight_prefix']
                                );
                            }
                        } elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
                            foreach ($value as $product_option_value_id) {
                                $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.deleted != '1' AND pov.product_option_value_id = '" . (int) $product_option_value_id . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                                if ($option_value_query->num_rows) {
                                    if ($option_value_query->row['price_prefix'] == '+') {
                                        $option_price += $option_value_query->row['price'];
                                    } elseif ($option_value_query->row['price_prefix'] == '-') {
                                        $option_price -= $option_value_query->row['price'];
                                    }

                                    if ($option_value_query->row['points_prefix'] == '+') {
                                        $option_points += $option_value_query->row['points'];
                                    } elseif ($option_value_query->row['points_prefix'] == '-') {
                                        $option_points -= $option_value_query->row['points'];
                                    }

                                    if ($option_value_query->row['weight_prefix'] == '+') {
                                        $option_weight += $option_value_query->row['weight'];
                                    } elseif ($option_value_query->row['weight_prefix'] == '-') {
                                        $option_weight -= $option_value_query->row['weight'];
                                    }

                                    if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart['quantity']))) {
                                        $stock = false;
                                    }

                                    $option_data[] = array(
                                        'product_option_id' => $product_option_id,
                                        'product_option_value_id' => $product_option_value_id,
                                        'option_id' => $option_query->row['option_id'],
                                        'option_value_id' => $option_value_query->row['option_value_id'],
                                        'name' => $option_query->row['name'],
                                        'value' => $option_value_query->row['name'],
                                        'type' => $option_query->row['type'],
                                        'quantity' => $option_value_query->row['quantity'],
                                        'subtract' => $option_value_query->row['subtract'],
                                        'price' => $option_value_query->row['price'],
                                        'price_prefix' => $option_value_query->row['price_prefix'],
                                        'points' => $option_value_query->row['points'],
                                        'points_prefix' => $option_value_query->row['points_prefix'],
                                        'weight' => $option_value_query->row['weight'],
                                        'weight_prefix' => $option_value_query->row['weight_prefix']
                                    );
                                }
                            }
                        } elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
                            $option_data[] = array(
                                'product_option_id' => $product_option_id,
                                'product_option_value_id' => '',
                                'option_id' => $option_query->row['option_id'],
                                'option_value_id' => '',
                                'name' => $option_query->row['name'],
                                'value' => $value,
                                'type' => $option_query->row['type'],
                                'quantity' => '',
                                'subtract' => '',
                                'price' => '',
                                'price_prefix' => '',
                                'points' => '',
                                'points_prefix' => '',
                                'weight' => '',
                                'weight_prefix' => ''
                            );
                        }
                    }
                }

                $price = $product_query->row['price'];


                // Reward Points
                $reward = 0;

                // Downloads
                $download_data = array();

                $recurring = false;

                if ($cart['otp_id'] == 0) {
                    $final_model = $product_query->row['model'];
                    $final_price = $price + $option_price;
                    $final_total = ($price + $option_price) * $cart['quantity'];
                    $final_weight = ($product_query->row['weight'] + $option_weight) * $cart['quantity'];
                } else {
                    if ($otp_data['model'] != '') {
                        $final_model = $otp_data['model'];
                    } else {
                        $final_model = $product_query->row['model'];
                    }
                    if ($product_discount_query->num_rows) {
                        $final_price = $product_discount_query->row['price'] + $option_price;
                        $final_total = ($product_discount_query->row['price'] + $option_price) * $cart['quantity'];
                    } else {
                        if ($this->config->get('config_otp_special') && $otp_data['special'] > 0) {
                            $final_price = $otp_data['special'] + $option_price;
                            $final_total = ($otp_data['special'] + $option_price) * $cart['quantity'];
                        } elseif ($this->config->get('config_otp_price') && $otp_data['price'] > 0) {
                            if ($otp_data['price_prefix'] == "=") {
                                $final_price = $otp_data['price'] + $option_price;
                                $final_total = ($otp_data['price'] + $option_price) * $cart['quantity'];
                            } elseif ($otp_data['price_prefix'] == "+") {
                                $final_price = $price + $otp_data['price'] + $option_price;
                                $final_total = ($price + $otp_data['price'] + $option_price) * $cart['quantity'];
                            } else {
                                $final_price = $price - $otp_data['price'] + $option_price;
                                $final_total = ($price - $otp_data['price'] + $option_price) * $cart['quantity'];
                            }
                        } else {
                            $final_price = $price + $option_price;
                            $final_total = ($price + $option_price) * $cart['quantity'];
                        }
                    }
                    if ($this->config->get('config_otp_weight') && $otp_data['weight'] > 0) {
                        if ($otp_data['weight_prefix'] == "=") {
                            $final_weight = ($otp_data['weight'] + $option_weight) * $cart['quantity'];
                        } elseif ($otp_data['weight_prefix'] == "+") {
                            $final_weight = ($product_query->row['weight'] + $otp_data['weight'] + $option_weight) * $cart['quantity'];
                        } else {
                            $final_weight = ($product_query->row['weight'] - $otp_data['weight'] + $option_weight) * $cart['quantity'];
                        }
                    } else {
                        $final_weight = ($product_query->row['weight'] + $option_weight) * $cart['quantity'];
                    }
                }

                $final_image = $product_query->row['image'];


                $product_data[] = array(
                    'otp_id' => $cart['otp_id'],
                    'product_id' => $product_query->row['product_id'],
                    'name' => $product_query->row['name'],
                    'model' => $final_model, //$product_query->row['model'],
                    'shipping' => $product_query->row['shipping'],
                    'image' => $final_image, //$product_query->row['image'],
                    'option' => $option_data,
                    'download' => $download_data,
                    'quantity' => $cart['quantity'],
                    'minimum' => $product_query->row['minimum'],
                    'subtract' => $product_query->row['subtract'],
                    'stock' => $stock,
                    'price' => $final_price, //($price + $option_price),
                    'total' => $final_total, //($price + $option_price) * $cart['quantity'],
                    'reward' => $reward * $cart['quantity'],
                    'points' => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $cart['quantity'] : 0),
                    'tax_class_id' => $product_query->row['tax_class_id'],
                    'weight' => $final_weight, //($product_query->row['weight'] + $option_weight) * $cart['quantity'],
                    'weight_class_id' => $product_query->row['weight_class_id'],
                    'length' => $product_query->row['length'],
                    'width' => $product_query->row['width'],
                    'height' => $product_query->row['height'],
                    'length_class_id' => $product_query->row['length_class_id'],
                    'recurring' => $recurring
                );
            } else {
                continue;
            }
        }

        return $product_data;
    }

    public function getAddress($address_id) {
        $address_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int) $address_id . "' ");

        if ($address_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE cd.language_id= '" . (int) $this->config->get('config_language_id') . "' AND c.country_id = '" . (int) $address_query->row['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` z LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE zd.language_id= '" . (int) $this->config->get('config_language_id') . "' AND z.zone_id = '" . (int) $address_query->row['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }

            $is_default = false;


            $address_data = array(
                'address_id' => $address_query->row['address_id'],
                'firstname' => $address_query->row['firstname'],
                'lastname' => $address_query->row['lastname'],
                'telephone' => $address_query->row['telephone'],
                'telephone2' => $address_query->row['telephone2'],
                'company' => $address_query->row['company'],
                'address_1' => $address_query->row['address_1'],
                'address_2' => $address_query->row['address_2'],
                'postcode' => $address_query->row['postcode'],
                'city' => $address_query->row['city'],
                'zone_id' => $address_query->row['zone_id'],
                'zone' => $zone,
                'zone_code' => $zone_code,
                'country_id' => $address_query->row['country_id'],
                'country' => $country,
                'iso_code_2' => $iso_code_2,
                'iso_code_3' => $iso_code_3,
                'address_format' => $address_format,
                'default_address' => $is_default,
                'custom_field' => json_decode($address_query->row['custom_field'], true),
            );

            return $address_data;
        } else {
            return false;
        }
    }

}
