<?php

class ModelCustomerCustomer extends Model {

    public function addCustomer($data) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int) $data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "', newsletter = '" . (int) $data['newsletter'] . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', status = '" . (int) $data['status'] . "', approved = '" . (int) $data['approved'] . "', safe = '" . (int) $data['safe'] . "', date_added = NOW()");

        $customer_id = $this->db->getLastId();

        if (isset($data['address'])) {
            foreach ($data['address'] as $address) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int) $customer_id . "', firstname = '" . $this->db->escape($address['firstname']) . "', lastname = '" . $this->db->escape($address['lastname']) . "', company = '" . $this->db->escape($address['company']) . "', address_1 = '" . $this->db->escape($address['address_1']) . "', address_2 = '" . $this->db->escape($address['address_2']) . "', city = '" . $this->db->escape($address['city']) . "', postcode = '" . $this->db->escape($address['postcode']) . "', country_id = '" . (int) $address['country_id'] . "', zone_id = '" . (int) $address['zone_id'] . "', custom_field = '" . $this->db->escape(isset($address['custom_field']) ? json_encode($address['custom_field']) : '') . "'");

                if (isset($address['default'])) {
                    $address_id = $this->db->getLastId();

                    $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int) $address_id . "' WHERE customer_id = '" . (int) $customer_id . "'");
                }
            }
        }
        if (isset($data['newsletter_check']) && !empty($data['newsletter_check'])) {
                $customer_id = (int)$customer_id;
            foreach ($data['newsletter_check'] as $value) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "newsletter_category_to_customer` (newsletter_category_id, customer_id) VALUE (" . $value . ", " . $customer_id . ")");
            }
        }
    }

    public function editCustomer($customer_id, $data) {
        if (!isset($data['custom_field'])) {
            $data['custom_field'] = array();
        }

        $this->db->query("UPDATE " . DB_PREFIX . "customer SET customer_group_id = '" . (int) $data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "', newsletter = '" . (int) $data['newsletter'] . "', status = '" . (int) $data['status'] . "', approved = '" . (int) $data['approved'] . "', safe = '" . (int) $data['safe'] . "', language_id = '" . (int) $data['language'] . "', gender = '" . $data['gender'] . "', country_id = '" . (int) $data['country'] . "', interface = '" . (int) $data['country_interface'] . "' WHERE customer_id = '" . (int) $customer_id . "'");

        if (isset($data['password'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE customer_id = '" . (int) $customer_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int) $customer_id . "'");

        if (isset($data['address'])) {
            foreach ($data['address'] as $address) {
                if (!isset($address['custom_field'])) {
                    $address['custom_field'] = array();
                }

                $this->db->query("INSERT INTO " . DB_PREFIX . "address SET address_id = '" . (int) $address['address_id'] . "', customer_id = '" . (int) $customer_id . "', firstname = '" . $this->db->escape($address['firstname']) . "', lastname = '" . $this->db->escape($address['lastname']) . "', company = '" . $this->db->escape($address['company']) . "', address_1 = '" . $this->db->escape($address['address_1']) . "', telephone = '" . (isset($data['telephone']) ? $this->db->escape($data['telephone']) : "") . "', address_2 = '" . $this->db->escape($address['address_2']) . "', city = '" . $this->db->escape($address['city']) . "', postcode = '" . $this->db->escape($address['postcode']) . "', country_id = '" . (int) $address['country_id'] . "', zone_id = '" . (int) $address['zone_id'] . "', custom_field = '" . $this->db->escape(isset($address['custom_field']) ? json_encode($address['custom_field']) : '') . "'");

                if (isset($address['default'])) {
                    $address_id = $this->db->getLastId();

                    $this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int) $address_id . "' WHERE customer_id = '" . (int) $customer_id . "'");
                }
            }
        }

        if (isset($data['newsletter'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int) $data['newsletter'] . "' WHERE customer_id = '" . (int) $customer_id . "'");
        }

            $customer_id = (int)$customer_id;
        $this->db->query("DELETE FROM " . DB_PREFIX . "newsletter_category_to_customer WHERE customer_id = '" .  $customer_id . "'");

        if (isset($data['newsletter_check'])) {

            foreach ($data['newsletter_check'] as $newsletter_category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "newsletter_category_to_customer SET customer_id = '" .  $customer_id . "', newsletter_category_id = '" . (int) $newsletter_category_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int) $customer_id . "'");

        if (isset($data['product_ids'])) {
            $str_wishlist = '';
            foreach ($data['product_ids'] as $wishlist_prod_id) {
                $str_wishlist .= '(' . (int) $customer_id . ',' . (int) $wishlist_prod_id . ',NOW()),';
            }
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_wishlist (customer_id, product_id,date_added) values " . trim($str_wishlist, ",") . "ON DUPLICATE KEY UPDATE date_added = NOW()");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_brands WHERE customer_id = '" . (int) $customer_id . "'");

        if (isset($data['brand_ids'])) {
            $str_brands = '';
            foreach ($data['brand_ids'] as $brand_id) {
                $str_brands .= '(' . (int) $customer_id . ',' . (int) $brand_id . ',NOW()),';
            }
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_brands (customer_id, brand_id,date_added) values " . trim($str_brands, ",") . "ON DUPLICATE KEY UPDATE date_added = NOW()");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_sellers WHERE customer_id = '" . (int) $customer_id . "'");

        if (isset($data['seller_ids'])) {
            $str_sellers = '';
            foreach ($data['seller_ids'] as $seller_id) {
                $str_sellers .= '(' . (int) $customer_id . ',' . (int) $seller_id . ',NOW()),';
            }
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_sellers (customer_id, seller_id,date_added) values " . trim($str_sellers, ",") . "ON DUPLICATE KEY UPDATE date_added = NOW()");
        }
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_categories WHERE customer_id = '" . (int) $customer_id . "'");

        if (isset($data['category_ids'])) {
            $str_categories = '';
            foreach ($data['category_ids'] as $category_id) {
                $str_categories .= '(' . (int) $customer_id . ',' . (int) $category_id . ',NOW()),';
            }
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_categories (customer_id, category_id,date_added) values " . trim($str_categories, ",") . "ON DUPLICATE KEY UPDATE date_added = NOW()");
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_looks WHERE customer_id = '" . (int) $customer_id . "'");

        if (isset($data['look_ids'])) {
            $str_looks = '';
            foreach ($data['look_ids'] as $look_id) {
                $str_looks .= '(' . (int) $customer_id . ',' . (int) $look_id . ',NOW()),';
            }
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_looks (customer_id, look_id,date_added) values " . trim($str_looks, ",") . "ON DUPLICATE KEY UPDATE date_added = NOW()");
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_tips WHERE customer_id = '" . (int) $customer_id . "'");

        if (isset($data['tip_ids'])) {
            $str_tips = '';
            foreach ($data['tip_ids'] as $tip_id) {
                $str_tips .= '(' . (int) $customer_id . ',' . (int) $tip_id . ',NOW()),';
            }
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_tips (customer_id, tip_id,date_added) values " . trim($str_tips, ",") . "ON DUPLICATE KEY UPDATE date_added = NOW()");
        }
    }

    public function editCustomerPass($customer_id, $data) {

        if ($data['password']) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE customer_id = '" . (int) $customer_id . "'");
        }
    }

    public function editToken($customer_id, $token) {
        $this->db->query("UPDATE " . DB_PREFIX . "customer SET token = '" . $this->db->escape($token) . "' WHERE customer_id = '" . (int) $customer_id . "'");
    }

    public function deleteCustomer($customer_id) {
        /**
         * Marketplace
         */
        if ($this->config->get('marketplace_status')) {

            $this->load->model('customerpartner/partner');
            $this->model_customerpartner_partner->deleteCustomer($customer_id);
        }
        /**
         * Marketplace
         */
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int) $customer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int) $customer_id . "'");
    }

    public function getCustomer($customer_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int) $customer_id . "'");
        return $query->row;
    }

    public function getCustomerByEmail($email) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        return $query->row;
    }

    public function getCustomers($data = array()) {
        //$sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname, ' - ', c.email) AS name, cgd.name AS customer_group FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (c.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int) $this->config->get('config_language_id') . "'";
        $sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cgd.name AS customer_group FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (c.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "c.email LIKE '" . $this->db->escape($data['filter_email']) . "%'";
        }

        if (isset($data['filter_newsletter']) && !is_null($data['filter_newsletter'])) {
            $implode[] = "c.newsletter = '" . (int) $data['filter_newsletter'] . "'";
        }

        if (!empty($data['filter_customer_group_id'])) {
            $implode[] = "c.customer_group_id = '" . (int) $data['filter_customer_group_id'] . "'";
        }

        if (!empty($data['filter_ip'])) {
            $implode[] = "c.customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "c.status = '" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
            $implode[] = "c.approved = '" . (int) $data['filter_approved'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(c.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }
        $sort_data = array(
            'name',
            'c.email',
            'customer_group',
            'c.status',
            'c.approved',
            'c.ip',
            'c.date_added'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function approve($customer_id) {
        $customer_info = $this->getCustomer($customer_id);

        if ($customer_info) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET approved = '1' WHERE customer_id = '" . (int) $customer_id . "'");

            $this->load->language('mail/customer');

            $this->load->model('setting/store');

            $store_info = $this->model_setting_store->getStore($customer_info['store_id']);

            if ($store_info) {
                $store_name = $store_info['name'];
                $store_url = $store_info['url'] . 'index.php?route=account/login';
            } else {
                $store_name = $this->config->get('config_name');
                $store_url = HTTP_CATALOG . 'index.php?route=account/login';
            }

            $message = sprintf($this->language->get('text_approve_welcome'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8')) . "\n\n";
            $message .= $this->language->get('text_approve_login') . "\n";
            $message .= $store_url . "\n\n";
            $message .= $this->language->get('text_approve_services') . "\n\n";
            $message .= $this->language->get('text_approve_thanks') . "\n";
            $message .= html_entity_decode($store_name, ENT_QUOTES, 'UTF-8');

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($customer_info['email']);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(sprintf($this->language->get('text_approve_subject'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8')));
            $mail->setText($message);
            $mail->send();
        }
    }

    public function getAddress($address_id) {
        $address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int) $address_id . "'");

        if ($address_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c.country_id = '" . (int) $address_query->row['country_id'] . "'");

            if ($country_query->num_rows) {
                $country = $country_query->row['name'];
                $iso_code_2 = $country_query->row['iso_code_2'];
                $iso_code_3 = $country_query->row['iso_code_3'];
                $address_format = $country_query->row['address_format'];
            } else {
                $country = '';
                $iso_code_2 = '';
                $iso_code_3 = '';
                $address_format = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` z LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE zd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND z.zone_id = '" . (int) $address_query->row['zone_id'] . "'");

            if ($zone_query->num_rows) {
                $zone = $zone_query->row['name'];
                $zone_code = $zone_query->row['code'];
            } else {
                $zone = '';
                $zone_code = '';
            }

            return array(
                'address_id' => $address_query->row['address_id'],
                'customer_id' => $address_query->row['customer_id'],
                'firstname' => $address_query->row['firstname'],
                'lastname' => $address_query->row['lastname'],
                'company' => $address_query->row['company'],
                'address_1' => $address_query->row['address_1'],
                'address_2' => $address_query->row['address_2'],
                'postcode' => $address_query->row['postcode'],
                'city' => $address_query->row['city'],
                'zone_id' => $address_query->row['zone_id'],
                'zone' => $zone,
                'zone_code' => $zone_code,
                'country_id' => $address_query->row['country_id'],
                'country' => $country,
                'iso_code_2' => $iso_code_2,
                'iso_code_3' => $iso_code_3,
                'address_format' => $address_format,
                'custom_field' => json_decode($address_query->row['custom_field'], true)
            );
        }
    }

    public function getAddresses($customer_id) {
        $address_data = array();

        $query = $this->db->query("SELECT address_id FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int) $customer_id . "'");

        foreach ($query->rows as $result) {
            $address_info = $this->getAddress($result['address_id']);

            if ($address_info) {
                $address_data[$result['address_id']] = $address_info;
            }
        }

        return $address_data;
    }

    public function getTotalCustomers($data = array()) {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "CONCAT(firstname, ' ', lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "email LIKE '" . $this->db->escape($data['filter_email']) . "%'";
        }

        if (isset($data['filter_newsletter']) && !is_null($data['filter_newsletter'])) {
            $implode[] = "newsletter = '" . (int) $data['filter_newsletter'] . "'";
        }

        if (!empty($data['filter_customer_group_id'])) {
            $implode[] = "customer_group_id = '" . (int) $data['filter_customer_group_id'] . "'";
        }

        if (!empty($data['filter_ip'])) {
            $implode[] = "customer_id IN (SELECT customer_id FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($data['filter_ip']) . "')";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "status = '" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_approved']) && !is_null($data['filter_approved'])) {
            $implode[] = "approved = '" . (int) $data['filter_approved'] . "'";
        }

        if (!empty($data['filter_date_added'])) {
            $implode[] = "DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        if ($implode) {
            $sql .= " WHERE " . implode(" AND ", $implode);
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalCustomersAwaitingApproval() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE status = '0' OR approved = '0'");

        return $query->row['total'];
    }

    public function getTotalAddressesByCustomerId($customer_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalAddressesByCountryId($country_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE country_id = '" . (int) $country_id . "'");

        return $query->row['total'];
    }

    public function getTotalAddressesByZoneId($zone_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE zone_id = '" . (int) $zone_id . "'");

        return $query->row['total'];
    }

    public function getTotalCustomersByCustomerGroupId($customer_group_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE customer_group_id = '" . (int) $customer_group_id . "'");

        return $query->row['total'];
    }

    public function addHistory($customer_id, $comment) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer_history SET customer_id = '" . (int) $customer_id . "', comment = '" . $this->db->escape(strip_tags($comment)) . "', date_added = NOW()");
    }

    public function getHistories($customer_id, $start = 0, $limit = 10) {
        if ($start < 0) {
            $start = 0;
        }

        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT comment, date_added FROM " . DB_PREFIX . "customer_history WHERE customer_id = '" . (int) $customer_id . "' ORDER BY date_added DESC LIMIT " . (int) $start . "," . (int) $limit);

        return $query->rows;
    }

    public function getTotalHistories($customer_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_history WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->row['total'];
    }

    public function addTransaction($customer_id, $description = '', $amount = '', $order_id = null) {
        $customer_info = $this->getCustomer($customer_id);

        if ($customer_info) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int) $customer_id . "', order_id = " . ($order_id===null?'null':(int)$order_id) . ", description = '" . $this->db->escape($description) . "', amount = '" . (float) $amount . "', user_id = '" . (int) $this->user->getid() . "', date_added = NOW()");

            $this->load->language('mail/customer');

            $this->load->model('setting/store');

            $store_info = $this->model_setting_store->getStore($customer_info['store_id']);

            if ($store_info) {
                $store_name = $store_info['name'];
            } else {
                $store_name = $this->config->get('config_name');
            }

            $message = sprintf($this->language->get('text_transaction_received'), $this->currency->format($amount, $this->config->get('config_currency'))) . "\n\n";
            $message .= sprintf($this->language->get('text_transaction_total'), $this->currency->format($this->getTransactionTotal($customer_id)));

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($customer_info['email']);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(sprintf($this->language->get('text_transaction_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
            $mail->setText($message);
            //$mail->send();
        }
    }

    public function deleteTransaction($order_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int) $order_id . "'");
    }

    public function getTransactions($customer_id, $start = 0, $limit = 10) {
        if ($start < 0) {
            $start = 0;
        }

        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT tc.customer_transaction_id, tc.customer_id, tc.order_id, tc.description, tc.amount, tc.date_added, u.username user_add, mu.username user_modify FROM " . DB_PREFIX . "customer_transaction tc  LEFT JOIN " . DB_PREFIX . "user u ON (tc.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (tc.last_mod_id = mu.user_id) WHERE tc.customer_id = '" . (int) $customer_id . "' ORDER BY tc.date_added DESC LIMIT " . (int) $start . "," . (int) $limit);
        return $query->rows;
    }

    public function getTotalTransactions($customer_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total  FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->row['total'];
    }

    public function getTransactionTotal($customer_id) {
        $query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalTransactionsByOrderId($order_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int) $order_id . "'");

        return $query->row['total'];
    }

    public function addReward($customer_id, $description = '', $points = '', $order_id = 0) {
        $customer_info = $this->getCustomer($customer_id);

        if ($customer_info) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_reward SET customer_id = '" . (int) $customer_id . "', order_id = '" . (int) $order_id . "', points = '" . (int) $points . "', description = '" . $this->db->escape($description) . "', date_added = NOW()");

            $this->load->language('mail/customer');

            $this->load->model('setting/store');

            $store_info = $this->model_setting_store->getStore($customer_info['store_id']);

            if ($store_info) {
                $store_name = $store_info['name'];
            } else {
                $store_name = $this->config->get('config_name');
            }

            $message = sprintf($this->language->get('text_reward_received'), $points) . "\n\n";
            $message .= sprintf($this->language->get('text_reward_total'), $this->getRewardTotal($customer_id));

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($customer_info['email']);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(sprintf($this->language->get('text_reward_subject'), html_entity_decode($store_name, ENT_QUOTES, 'UTF-8')));
            $mail->setText($message);
            $mail->send();
        }
    }

    public function deleteReward($order_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int) $order_id . "' AND points > 0");
    }

    public function getRewards($customer_id, $start = 0, $limit = 10) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int) $customer_id . "' ORDER BY date_added DESC LIMIT " . (int) $start . "," . (int) $limit);

        return $query->rows;
    }

    public function getTotalRewards($customer_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->row['total'];
    }

    public function getRewardTotal($customer_id) {
        $query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalCustomerRewardsByOrderId($order_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int) $order_id . "' AND points > 0");

        return $query->row['total'];
    }

    public function getIps($customer_id, $start = 0, $limit = 10) {
        if ($start < 0) {
            $start = 0;
        }
        if ($limit < 1) {
            $limit = 10;
        }

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int) $customer_id . "' ORDER BY date_added DESC LIMIT " . (int) $start . "," . (int) $limit);
        return $query->rows;
    }

    public function getTotalIps($customer_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int) $customer_id . "'");

        return $query->row['total'];
    }

    public function getTotalCustomersByIp($ip) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_ip WHERE ip = '" . $this->db->escape($ip) . "'");

        return $query->row['total'];
    }

    public function getTotalLoginAttempts($email) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE `email` = '" . $this->db->escape($email) . "'");

        return $query->row;
    }

    public function deleteLoginAttempts($email) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE `email` = '" . $this->db->escape($email) . "'");
    }

    public function getNewsletterCategories($customer_id) {
        $newsletter_category = array();
            $customer_id = (int)$customer_id;

        $query = $this->db->query("SELECT newsletter_category_id FROM " . DB_PREFIX . "newsletter_category_to_customer WHERE customer_id ='" .  $customer_id . "' ");
        foreach ($query->rows as $row) {
            $newsletter_category[] = $row['newsletter_category_id'];
        }
        return $newsletter_category;
    }

    public function getNewsletterCategoriesGroup($customer_id) {
        $newsletter_category_Group = array();

        $customer = $this->getCustomer($customer_id);
        if (!empty($customer)) {

            $query = $this->db->query("SELECT nc.newsletter_category_id, ncd.name FROM " . DB_PREFIX . "newsletter_category nc LEFT JOIN newsletter_category_description ncd ON (nc.newsletter_category_id = ncd.newsletter_category_id) WHERE ncd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND nc.country_id='" . (int) $customer["country_id"] . "' ");

            foreach ($query->rows as $result) {
                $newsletter_category_Group[] = array(
                    'newsletter_category_id' => $result['newsletter_category_id'],
                    'name' => $result['name']
                );
            }
        }
        return $newsletter_category_Group;
    }

    public function addCustomerNewsletter($customer_id, $data) {
            $customer_id = (int)$customer_id;
        if (isset($data['newsletter_category']) && !empty($data['newsletter_category'])) {
            foreach ($data['newsletter_category'] as $value) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "newsletter_category_to_customer` (newsletter_category_id, customer_id) VALUE (" . $value . ", " . $customer_id . ")");
            }
        }
    }

    public function getNewsletterList($id) {
        $getemail = $this->db->query("select email from customer where customer_id = '" . (int) $id . "'");
        $email = $getemail->row['email'];
        $sql = " SELECT DISTINCT n.id,n.email, n.status, n.confirm_sent, n.created_date, n.updated_date, n.status, n.gender,n.language_id ,n.country_id FROM " . DB_PREFIX . "popup_email n where n.email like '" . $email . "' ";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getFavoriteProducts($customer_id) {
        return $this->db->query("SELECT DISTINCT cw.product_id,pd.name FROM " . DB_PREFIX . " customer c LEFT JOIN " . DB_PREFIX . "customer_wishlist cw ON (c.customer_id = cw.customer_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (cw.product_id=pd.product_id) WHERE c.customer_id='" . (int) $customer_id . "' and pd.language_id='" . (int) $this->config->get('config_language_id') . "'")->rows;
    }

    public function getFavoriteBrands($customer_id) {
        return $this->db->query("SELECT DISTINCT cb.brand_id,bd.name FROM " . DB_PREFIX . " customer c LEFT JOIN " . DB_PREFIX . "customer_brands cb ON (c.customer_id = cb.customer_id) LEFT JOIN " . DB_PREFIX . "brand_description bd ON (cb.brand_id=bd.brand_id) WHERE c.customer_id='" . (int) $customer_id . "' and bd.language_id='" . (int) $this->config->get('config_language_id') . "'")->rows;
    }

    public function getFavoriteSellers($customer_id) {
        return $this->db->query("SELECT DISTINCT cs.seller_id,cp2cd.screenname FROM " . DB_PREFIX . " customer c LEFT JOIN " . DB_PREFIX . "customer_sellers cs ON (c.customer_id = cs.customer_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer_description cp2cd ON (cs.seller_id=cp2cd.customer_id) where c.customer_id='" . (int) $customer_id . "' AND cs.seller_id IS NOT NULL")->rows;
    }

    public function getFavoriteCatgories($customer_id) {
        return $this->db->query("SELECT cc.category_id,cd.name FROM " . DB_PREFIX . " customer_categories cc LEFT JOIN " . DB_PREFIX . "category_description cd ON (cc.category_id = cd.category_id) WHERE cc.customer_id='" . (int) $customer_id . "' and cd.language_id='" . (int) $this->config->get('config_language_id') . "'")->rows;
    }

    public function getFavoriteLooks($customer_id) {

        return $this->db->query("SELECT cl.look_id,ld.title AS name FROM " . DB_PREFIX . " customer_looks cl LEFT JOIN " . DB_PREFIX . "look_description ld ON (cl.look_id = ld.look_id) where cl.customer_id='" . (int) $customer_id . "' AND ld.language_id='" . (int) $this->config->get('config_language_id') . "'")->rows;
    }

    public function getFavoriteTips($customer_id) {
        return $this->db->query("SELECT ct.tip_id,td.name FROM " . DB_PREFIX . " customer_tips ct LEFT JOIN " . DB_PREFIX . "tip_description td ON (ct.tip_id = td.tip_id) where ct.customer_id='" . (int) $customer_id . "' AND td.language_id='" . (int) $this->config->get('config_language_id') . "'")->rows;
    }

    public function getOrder($email) {
        $order_query = $this->db->query("SELECT *,(SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer FROM `" . DB_PREFIX . "order` o WHERE o.email = '" . $email . "'  AND o.deleted='0'  order by date_added DESC");
        $order_query2 = $this->db->query("SELECT sum(o.total) as sumtotal, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer FROM `" . DB_PREFIX . "order` o WHERE o.email = '" . $email . "'  AND o.deleted='0'  order by date_added DESC");
        if ($order_query->num_rows) {
        $count_order = $order_query2->row['sumtotal'] / $order_query->num_rows;

            $this->load->model('localisation/language');

            $language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

            if ($language_info) {
                $language_code = $language_info['code'];
                $language_directory = $language_info['directory'];
            } else {
                $language_code = '';
                $language_directory = '';
            }
            return array(
                'order_id' => $order_query->row['order_id'],
                'num_order' => $order_query->num_rows,
                'store_id' => $order_query->row['store_id'],
                'customer_id' => $order_query->row['customer_id'],
                'total' => $order_query->row['total'],
                'sumtotal' => $order_query2->row['sumtotal'],
                'counttotal' => $count_order,
                'order_status_id' => $order_query->row['order_status_id'],
                'language_id' => $order_query->row['language_id'],
                'currency_id' => $order_query->row['currency_id'],
                'currency_code' => $order_query->row['currency_code'],
                'currency_value' => $order_query->row['currency_value'],
                'date_added' => $order_query->row['date_added'],
                'date_modified' => $order_query->row['date_modified']
            );
        } else {
            return;
        }
    }

    public function getCustomerview($viewcustomer_id) {
        $customer_view = $this->db->query("SELECT p2c.category_id, cv.product_id, pd.name, cv.count,cv.date_added,cv.utm_source , cv.utm_medium FROM " . DB_PREFIX . "customer_views cv  LEFT JOIN product_description pd ON (cv.product_id = pd.product_id) LEFT JOIN product_to_category p2c ON (pd.product_id = p2c.product_id ) WHERE customer_id = '" . $viewcustomer_id . "' AND language_id = 2");
        if ($customer_view->num_rows) {
            $reward = 0;

            foreach ($customer_view->rows as $result) {
                $customer_product_view[] = array(
                    'product_view_id' => $result['product_id'],
                    'product_view_name' => $result['name'],
                    'category_id' => $result['category_id'],
                    'date_added' => $result['date_added'],
                    'product_view_count' => $result['count'],
                    'utm_meduim' => $result['utm_medium'],
                    'utm_source' => $result['utm_source']
                );
            }
            return $customer_product_view;
        } else {
            return;
        }
    }


     public function getCustomerproduct($viewcustomer_id) {
        $customer_purchaseds = $this->db->query("SELECT p2c.category_id, op.product_id, op.name FROM " . DB_PREFIX . "`order` o  LEFT JOIN `order_product` op ON (op.order_id = o.order_id AND op.deleted='0') LEFT JOIN product_to_category p2c ON (p2c.product_id = op.product_id ) WHERE o.customer_id = '" . $viewcustomer_id . "' AND o.deleted='0'");

        if ($customer_purchaseds->num_rows) {
            $reward = 0;

            foreach ($customer_purchaseds->rows as $result) {
                $customer_purchased[] = array(
                    'customer_product_id' => $result['product_id'],
                    'pro_category_id' => $result['category_id'],
                    'customer_product_name' => $result['name']
                );
            }
            return $customer_purchased;
        } else {
            return;
        }
    }

}
