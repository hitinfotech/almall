<?php

class ModelToppagesToppages extends Model {

    public function addToppages($data) {
        $this->db->query(" INSERT INTO " . DB_PREFIX . "top_pages SET `day`='" . $data['date'] . "',   `status` ='" . $data['status'] . "'  , user_id = '" . (int) $this->user->getid() . "', is_english = '" . $this->db->escape($data['is_english']) . "', is_arabic = '" . $this->db->escape($data['is_arabic']) . "',  `date_added`=NOW(), `date_modified`=NOW()");
        $toppages_id = $this->db->getLastId();

        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('toppages', 'image', $toppages_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('toppages') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE top_pages SET `image` = '" . $this->db->escape($image) . "' WHERE top_pages_id='" . (int) $toppages_id . "' ");
        }


        $filter_name_array = $data['filter_name'];
        $type_id_array = $data['type_id'];
        $sort_order_array = $data['sort_order'];
        $top_pages_str = '';
        foreach ($filter_name_array as $k => $val) {
            $top_pages_str .= "('" . $toppages_id . "','product'," . $type_id_array[$k] . "," . $sort_order_array[$k] . "),";
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . " top_pages_detail (top_pages_id, type  ,type_id,sort_order) VALUES " . trim($top_pages_str, ','));
        $describtion_string = '';
        foreach ($data['top_pages_description'] as $language_id => $value) {
            $describtion_string .= "(" . (int) $toppages_id . "," . (int) $language_id . ",'" . $this->db->escape($value['name']) . "','" . $this->db->escape($value['body']) . "'),";
        }
        $dqldetails = $this->db->query("INSERT INTO " . DB_PREFIX . "top_pages_description (top_pages_id,language_id,name,body) values " . trim($describtion_string, ","));


        foreach ($data['top_pages_link'] as $key => $value) {
            if ($key == 'country' && !empty($value)) {

                foreach ($value as $index => $country) {
                    $country_string .= "(" . (int) $toppages_id . "," . (int) $country . "),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " top_pages_to_country  (top_pages_id,country_id) values " . trim($country_string, ","));
            } elseif ($key == 'group' && !empty($value)) {

                foreach ($value as $index => $group) {
                    $group_string .= "(" . (int) $toppages_id . "," . (int) $group . "),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " top_pages_to_group  (top_pages_id,top_pages_group_id) values " . trim($group_string, ","));
            } elseif ($key == 'keyword' && !empty($value)) {

                $keyword_string = '';
                foreach ($value as $index => $keyword) {
                    $keyword_string .= "(" . (int) $toppages_id . "," . (int) $keyword . ",NOW()),";
                }

                $this->db->query("INSERT INTO " . DB_PREFIX . " top_pages_keyword  (top_pages_id,keyword_id,date_added) values " . trim($keyword_string, ","));
            }
        }
        $this->url_custom->create_URL('top_pages', $toppages_id);

        $this->cache->deletekeys('admin.toppages');
        $this->cache->deletekeys('catalog.toppages');
        return $top_pages_id;
    }

    public function editToppages($toppages_id, $data) {
        $this->db->query(" UPDATE `top_pages` SET `day` = '" . $data['date'] . "', is_english = '" . $this->db->escape($data['is_english']) . "', is_arabic = '" . $this->db->escape($data['is_arabic']) . "', `status` = '" . $data['status'] . "',  `last_mod_id` ='" . (int) $this->user->getid() . "' WHERE `top_pages_id`='" . (int) $toppages_id . "' ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "top_pages_detail WHERE top_pages_id = '" . (int) $toppages_id . "'");




        foreach ($data['top_pages_description'] as $language_id => $value) {
            $this->db->query("UPDATE " . DB_PREFIX . "top_pages_description SET name = '" . $value['name'] . "', body = '" . $this->stripTagsStyle(htmlspecialchars_decode($value['body'])) . "' WHERE `top_pages_id` = '" . (int) $toppages_id . "' AND language_id='" . $language_id . "'");
        }
        $this->db->query("DELETE FROM " . DB_PREFIX . "top_pages_to_country WHERE top_pages_id = '" . (int) $toppages_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "top_pages_to_group where top_pages_id = '" . (int) $toppages_id . "' ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "top_pages_keyword where top_pages_id = '" . (int) $toppages_id . "' ");
        foreach ($data['top_pages_link'] as $key => $value) {
            if ($key == 'country' && !empty($value)) {
                $country_string = '';
                foreach ($value as $index => $country) {
                    $country_string .= "(" . (int) $toppages_id . "," . (int) $country . "),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " top_pages_to_country  (top_pages_id,country_id) values " . trim($country_string, ","));
            } elseif ($key == 'group' && !empty($value)) {
                $group_string = '';
                foreach ($value as $index => $group) {
                    $group_string .= "(" . (int) $toppages_id . "," . (int) $group . "),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " top_pages_to_group  (top_pages_id,top_pages_group_id) values " . trim($group_string, ","));
            } elseif ($key == 'keyword' && !empty($value)) {
                $keyword_string = '';
                foreach ($value as $index => $keyword) {
                    $keyword_string .= "(" . (int) $toppages_id . "," . (int) $keyword . ",NOW()),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " top_pages_keyword (top_pages_id,keyword_id,date_added) values " . trim($keyword_string, ","));
            }
        }
        $filter_name_array = $data['filter_name'];
        $type_id_array = $data['type_id'];
        $sort_order_array = $data['sort_order'];
        $top_pages_str = '';
        foreach ($filter_name_array as $k => $val) {
            $top_pages_str .= "('" . $toppages_id . "','product'," . $type_id_array[$k] . "," . $sort_order_array[$k] . "),";
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . " top_pages_detail (top_pages_id, type  ,type_id,sort_order) VALUES " . trim($top_pages_str, ','));

        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('toppages', 'image', $toppages_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('toppages') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE top_pages SET `image` = '" . $this->db->escape($image) . "' WHERE top_pages_id='" . (int) $toppages_id . "' ");
        }
        $this->url_custom->create_URL('top_pages', $toppages_id);

        $this->cache->deletekeys('admin.toppages');
        $this->cache->deletekeys('catalog.toppages');
    }

    public function deleteToppages($top_pages_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "top_pages_link WHERE top_pages_id = '" . (int) $top_pages_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "top_pages_image WHERE top_pages_id = '" . (int) $top_pages_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "top_pages_detail WHERE top_pages_id = '" . (int) $top_pages_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "top_pages WHERE top_pages_id = '" . (int) $top_pages_id . "'");

        $this->cache->deletekeys('admin.toppages');
        $this->cache->deletekeys('catalog.toppages');
        return $top_pages_id;
    }

    public function getToppagesList($data = array()) {
        $sql = " SELECT DISTINCT t.image, CONCAT(td1.name, ' - ',td2.name ) name,t.status, CONCAT(td1.body, ' - ',td2.body ) body, t.top_pages_id, t.day,tp2c.country_id ,t.date_added,t.date_modified, u.username user_add, mu.username user_modify  FROM " . DB_PREFIX . "top_pages t  LEFT JOIN " . DB_PREFIX . "top_pages_description td1 ON (t.top_pages_id = td1.top_pages_id) LEFT JOIN " . DB_PREFIX . "top_pages_description td2 ON (t.top_pages_id = td2.top_pages_id) LEFT JOIN " . DB_PREFIX . "top_pages_to_country tp2c ON (t.top_pages_id = tp2c.top_pages_id)  LEFT JOIN " . DB_PREFIX . "top_pages_to_country tc ON (t.top_pages_id = tc.top_pages_id) LEFT JOIN " . DB_PREFIX . "top_pages_to_group t2g ON (t.top_pages_id = t2g.top_pages_id) LEFT JOIN " . DB_PREFIX . "user u ON (t.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (t.last_mod_id = mu.user_id) WHERE td1.language_id='1' AND td2.language_id='2'";

        $bLimitFlag = true;

        if (isset($data['filter_country_id'])) {
            $sql .= " AND tc.country_id = '" . (int) $data['filter_country_id'] . "'";
        }

        if (isset($data['filter_status'])) {
            $sql .= " AND t.status ='" . (int) $data['filter_status'] . "'";
        }

        if (!empty($data['filter_group'])) {
            $sql .= " AND t2g.top_pages_group_id ='" . (int) $data['filter_group'] . "'";
        }

        if (!empty($data['filter_filter'])) {
            $sql .= " AND CONCAT(td1.name,' ',td1.body, ' ',td2.name,' ',td2.body) LIKE '%" . $this->db->escape($data['filter_filter']) . "%'";
        } else if (isset($data['filter']['name']) && !empty($data['filter']['name'])) {
            $sql .= " AND CONCAT(td1.name,' ',td1.body, ' ',td2.name,' ',td2.body) LIKE '%" . $this->db->escape($data['filter']['name']) . "%'";
        }
        $sql .= "GROUP BY top_pages_id";
        $sql .= " ORDER BY date_added DESC ";


        $sort = array(
            'image',
            'body',
            'day',
            'name',
            'status',
            'country_id',
            'date_added',
            'date_modified'
        );



        if ((isset($data['start']) || isset($data['limit'])) && $bLimitFlag) {
            if ($data['start'] <= 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getToppages($top_pages_id) {

        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "top_pages n LEFT JOIN " . DB_PREFIX . "top_pages_detail nd ON (n.top_pages_id = nd.top_pages_id) LEFT JOIN " . DB_PREFIX . "top_pages_description td ON (n.top_pages_id = td.top_pages_id) WHERE n.top_pages_id = '" . (int) $top_pages_id . "' AND td.language_id = '" . $this->config->get('config_language_id') . "' ");
        return $query->row;
    }

    public function getToppagessGroups() {
        $key = 'toppages.groups.' . (int) $this->config->get('config_language_id');
        $result = $this->cache->get($key);
        // if (!$result) {
        $sql = "SELECT * FROM " . DB_PREFIX . "top_pages_group_description  WHERE  language_id = '" . (int) $this->config->get('config_language_id') . "' ";

        $query = $this->db->query($sql);
        if ($query->num_rows) {
            $this->cache->set($key, $query->rows);
        }

        return $query->rows;
        // }
        // return $result;
    }

    public function getToppagesDetails($top_pages_id) {
        $toppages_description_data = array();
        $query = $this->db->query(" SELECT top_pages_detail_id,top_pages_id,type,type_id,sort_order FROM " . DB_PREFIX . "top_pages_detail WHERE top_pages_id = '" . (int) $top_pages_id . "' ");

        foreach ($query->rows as $result) {
            $type_id = $this->db->query(" select name from " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $result['type_id'] . "' ");

            $toppages_description_data[] = array(
                'top_pages_id' => $result['top_pages_id'],
                'type' => $result['type'],
                'type_id' => $type_id->row['name'],
                'id' => $result['type_id'],
                'sort_order' => $result['sort_order']
            );
        }
        return $toppages_description_data;
    }

    public function getToppagesDescriptions($top_pages_id) {
        $description_data = array();
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "top_pages_description
                                            WHERE top_pages_id = '" . (int) $top_pages_id . "' ");

        foreach ($query->rows as $result) {
            $description_data[$result['language_id']] = array(
                'name' => isset($result['name']) ? $result['name'] : (isset($result['name']) ? $result['name'] : ''),
                'description' => isset($result['description']) ? $result['description'] : (isset($result['body']) ? $result['body'] : '')
            );
        }
        return $description_data;
    }

    public function getToppagesLink($top_pages_id) {
        $result_array = array();
        $country = $this->db->query("SELECT top_pages_to_country.country_id as id ,cd.name FROM top_pages_to_country left join country c on (top_pages_to_country.country_id =c.country_id) LEFT JOIN country_description cd ON(c.country_id=cd.country_id)  WHERE language_id = '1' AND c.country_id IN(top_pages_to_country.country_id) AND top_pages_to_country.top_pages_id='" . $top_pages_id . "'");
        foreach ($country->rows as $country_link_row) {
            $result_array['country'][] = array('id' => $country_link_row['id'], 'name' => $country_link_row['name']);
        }
        $groups = $this->db->query("SELECT g.top_pages_group_id as id, CONCAT(gd1.name, ' - ', gd2.name) as name FROM top_pages_to_group g left join look_group_description gd1 on (g.top_pages_group_id = gd1.look_group_id) left join look_group_description gd2 on (g.top_pages_group_id = gd2.look_group_id) WHERE gd1.look_group_id in (g.top_pages_group_id) AND gd1.language_id = '1' AND gd2.language_id = '2' and g.top_pages_id = '" . $top_pages_id . "'");
        foreach ($groups->rows as $groups_link_row) {
            $result_array['group'][] = array('id' => $groups_link_row['id'], 'name' => $groups_link_row['name']);
        }

        $keywords = $this->db->query("SELECT tk.keyword_id as id, kd.name From top_pages_keyword tk LEFT JOIN keyword_description kd on (tk.keyword_id = kd.keyword_id) WHERE kd.language_id = '" . (int) $this->config->get('config_language_id') . "'and tk.top_pages_id = '" . $top_pages_id . "'");
        foreach ($keywords->rows as $keywords_link_row) {
            $result_array['keyword'][] = array('id' => $keywords_link_row['id'], 'name' => str_replace('"', '', $keywords_link_row['name']));
        }

        return $result_array;
    }

    public function getTotalToppages($data = array()) {

        $sql = " SELECT COUNT(*) AS total  FROM " . DB_PREFIX . "top_pages n ";

        if (isset($data['filter_top_pages_id']) && !empty($data['filter_top_pages_id'])) {
            $sql .= " WHERE n.top_pages_id = '" . $data['filter_top_pages_id'] . "'";
        }

        if (isset($data['filter_description']) && !empty($data['filter_description'])) {
            $sql .= " AND nd.name like '%" . $data['filter_description'] . "%'";
        }

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

}
