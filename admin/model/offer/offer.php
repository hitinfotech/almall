<?php

class ModelOfferOffer extends Model {

    public function addOffer($data) {

        if ($data['end_date'] != '' && $data['end_date'] != '0000-00-00') {
            $end_date_value = date('Y-m-d H:i:s', strtotime($data['end_date']));
        } else {
            $end_date_value = '';
        }

        if ($data['start_date'] != '' && $data['start_date'] != '0000-00-00') {
            $start_date_value = date('Y-m-d H:i:s', strtotime($data['start_date']));
        } else {
            $start_date_value = '';
        }

        $this->db->query(" INSERT INTO " . DB_PREFIX . "offer SET `start_date`='" . $start_date_value . "', `end_date` = '" . $end_date_value . "', `sort_order` = '" . (int) $data['sort_order'] . "',`status` = '" . (int) $data['status'] . "', user_id = '" . (int) $this->user->getid() . "', `date_added`=NOW(), `date_modified`=NOW()");

        $offer_id = $this->db->getLastId();

        foreach ($data['offer_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "offer_description SET offer_id = '" . (int) $offer_id . "', language_id = '" . (int) $language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', body_text = '" . $this->db->escape($value['body_text']) . "', period = '" . $this->db->escape($value['period']) . "'");
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "offer_image WHERE offer_id = '" . (int) $offer_id . "'");

        if (isset($data['offer_image'])) {
            $images = $this->upload_images('offer', 'offer_image', $offer_id);
            if (is_array($images)) {
                $this->load->model('tool/image');
                foreach ($data['offer_image'] as $key => $offer_image) {
                    $image = ((strlen($offer_image['image_text']) > 0) ? $offer_image['image_text'] : $images[$key]);

                    $this->db->query("INSERT INTO " . DB_PREFIX . "offer_image SET offer_id = '" . (int) $offer_id . "', image = '" . $this->db->escape($image) . "', sort_order = '" . (int) $offer_image['sort_order'] . "'");

                    foreach ($this->config_image->get('news_offers_malls') as $row) {
                        if ($row['width'] > 10) {
                            $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                        }
                    }
                }
            }
        }

        $this->url_custom->create_URL('offer', $offer_id);

        $this->db->query("DELETE FROM offer_to_shop WHERE offer_id='" . (int) $offer_id . "'");
        foreach ($data['shops'] as $shop_id) {
            $this->db->query("INSERT INTO offer_to_shop SET shop_id='" . (int) $shop_id . "', offer_id='" . (int) $offer_id . "' ");
        }
        $this->db->query("DELETE FROM offer_to_brand WHERE offer_id='" . (int) $offer_id . "'");
        foreach ($data['brands'] as $brand_id) {
            $this->db->query("INSERT INTO offer_to_brand SET brand_id='" . (int) $brand_id . "', offer_id='" . (int) $offer_id . "' ");
        }
        $this->db->query("DELETE FROM offer_to_category WHERE offer_id='" . (int) $offer_id . "'");
        foreach ($data['category'] as $category_id) {
            $this->db->query("INSERT INTO offer_to_category SET category_id='" . (int) $category_id . "', offer_id='" . (int) $offer_id . "' ");
        }

        //$this->cache->deletekeys('admin.offer');
        //$this->cache->deletekeys('catalog.offer');
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='offer', type_id='" . (int) $offer_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $offer_id;
    }

    public function editOffer($offer_id, $data) {

        if ($data['end_date'] != '' && $data['end_date'] != '0000-00-00') {
            $end_date_value = date('Y-m-d H:i:s', strtotime($data['end_date']));
        } else {
            $end_date_value = '';
        }

        if ($data['start_date'] != '' && $data['start_date'] != '0000-00-00') {
            $start_date_value = date('Y-m-d H:i:s', strtotime($data['start_date']));
        } else {
            $start_date_value = '';
        }

        $this->db->query(" UPDATE " . DB_PREFIX . "offer SET `start_date`='" . $start_date_value . "', `end_date` = '" . $end_date_value . "', `sort_order` = '" . (int) $data['sort_order'] . "',`home` = '" . $data['home'] . "',`status` = '" . (int) $data['status'] . "',last_mod_id = '" . (int) $this->user->getid() . "', `date_modified`= NOW() WHERE offer_id ='" . (int) $offer_id . "'");

        $this->db->query("DELETE FROM offer_description WHERE offer_id='" . (int) $offer_id . "'");
        foreach ($data['offer_description'] as $language_id => $value) {
            // echo "INSERT INTO " . DB_PREFIX . "offer_description SET offer_id = '" . (int) $offer_id . "', language_id = '" . (int) $language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', body_text = '" . $this->db->escape($value['body_text']) . "' <br>";
            $this->db->query("INSERT INTO " . DB_PREFIX . "offer_description SET offer_id = '" . (int) $offer_id . "', language_id = '" . (int) $language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', body_text = '" . $this->db->escape($value['body_text']) . "', period = '" . $this->db->escape($value['period']) . "'");
        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "offer_image WHERE offer_id = '" . (int) $offer_id . "'");

        if (isset($data['offer_image'])) {
            $images = $this->upload_images('offer', 'offer_image', $offer_id);
            $this->load->model('tool/image');
            foreach ($data['offer_image'] as $key => $offer_image) {
                $image = ((strlen($offer_image['image_text']) > 0) ? $offer_image['image_text'] : $images[$key]);

                $this->db->query("INSERT INTO " . DB_PREFIX . "offer_image SET offer_id = '" . (int) $offer_id . "', image = '" . $this->db->escape($image) . "', sort_order = '" . (int) $offer_image['sort_order'] . "'");

                foreach ($this->config_image->get('news_offers_malls') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            }
        }


        $this->db->query("DELETE FROM offer_to_shop WHERE offer_id='" . (int) $offer_id . "'");
        foreach ($data['shops'] as $shop_id) {
            $this->db->query("INSERT INTO offer_to_shop SET shop_id='" . (int) $shop_id . "', offer_id='" . (int) $offer_id . "' ");
        }
        $this->db->query("DELETE FROM offer_to_brand WHERE offer_id='" . (int) $offer_id . "'");
        foreach ($data['brands'] as $brand_id) {
            $this->db->query("INSERT INTO offer_to_brand SET brand_id='" . (int) $brand_id . "', offer_id='" . (int) $offer_id . "' ");
        }
        $this->db->query("DELETE FROM offer_to_category WHERE offer_id='" . (int) $offer_id . "'");
        foreach ($data['category'] as $category_id) {
            $this->db->query("INSERT INTO offer_to_category SET category_id='" . (int) $category_id . "', offer_id='" . (int) $offer_id . "' ");
        }
        
        $this->url_custom->create_URL('offer', $offer_id);

        //$this->cache->deletekeys('admin.offer');
        //$this->cache->deletekeys('catalog.offer');
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='offer', type_id='" . (int) $offer_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
        
        return $offer_id;
    }

    public function deleteOffer($offer_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "offer_link WHERE offer_id = '" . (int) $offer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "offer_image WHERE offer_id = '" . (int) $offer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "offer_description WHERE offer_id = '" . (int) $offer_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "offer WHERE offer_id = '" . (int) $offer_id . "'");

        //$this->cache->deletekeys('admin.offer');
        //$this->cache->deletekeys('catalog.offer');
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='offer', type_id='" . (int) $offer_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
        return $offer_id;
    }

    public function getOfferList($data = array()) {
        $sql = " SELECT DISTINCT o.offer_id, CONCAT(od1.title, ' - ',od2.title ) name,  o.status, o.sort_order, o.date_added, o.date_modified, u.username user_add, mu.username user_modify  FROM " . DB_PREFIX . "offer o LEFT JOIN " . DB_PREFIX . "offer_description od1 ON (o.offer_id = od1.offer_id) LEFT JOIN " . DB_PREFIX . "offer_description od2 ON (o.offer_id = od2.offer_id) LEFT JOIN " . DB_PREFIX . "user u ON (o.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (o.last_mod_id = mu.user_id)   WHERE od1.language_id='1' AND od2.language_id='2' ";

        $bLimitFlag = true;

        if (!empty($data['filter_offer_id'])) {
            $sql .= " AND o.offer_id = '" . (int) $data['filter_offer_id'] . "'";
            $bLimitFlag = false;
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND o.status = '" . (int) $data['filter_status'] . "'";
        }

        if (!empty($data['filter_filter'])) {
            $sql .= " AND CONCAT(od1.title, ' ',od2.title) LIKE '%" . $this->db->escape($data['filter_filter']) . "%'";
        } else if (isset($data['filter']['name']) && !empty($data['filter']['name'])) {
            $sql .= " AND CONCAT(od1.title, ' ',od2.title) LIKE '%" . $this->db->escape($data['filter']['name']) . "%'";
        }

        $sort = array(
            'od' . (int) $this->config->get('config_language_id') . '.title',
            'date_added',
            'date_modified',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY LCASE(od" . (int) $this->config->get('config_language_id') . ".title)";
        }

        if (isset($data['order']) && $data['order'] == "DESC") {
            $sql .= " DESC ";
        } else {
            $sql .= " ASC ";
        }

        if ((isset($data['start']) || isset($data['limit'])) && $bLimitFlag) {
            if ($data['start'] <= 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getOffer($offer_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "offer n LEFT JOIN " . DB_PREFIX . "offer_description nd ON (n.offer_id = nd.offer_id)  WHERE n.offer_id = '" . (int) $offer_id . "' AND nd.language_id = '" . $this->config->get('config_language_id') . "' ");

        return $query->row;
    }

    public function getOfferDescriptions($offer_id) {
        $offer_description_data = array();
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "offer_description WHERE offer_id = '" . (int) $offer_id . "' ");

        foreach ($query->rows as $result) {
            $offer_description_data[$result['language_id']] = array(
                'title' => $result['title'],
                'description' => $result['description'],
                'body_text' => $result['body_text'],
                'period' => $result['period']
            );
        }

        return $offer_description_data;
    }

    public function getOfferBrands($offer_id) {
        $query = $this->db->query(" SELECT n2b.brand_id, CONCAT(bd1.name, ' - ',bd2.name) name "
                . "FROM " . DB_PREFIX . "offer_to_brand n2b "
                . "LEFT JOIN brand b ON(n2b.brand_id=b.brand_id) "
                . "LEFT JOIN brand_description bd1 ON(b.brand_id=bd1.brand_id) "
                . "LEFT JOIN brand_description bd2 ON(b.brand_id=bd2.brand_id) "
                . "WHERE bd1.language_id='1' AND bd2.language_id='2' AND offer_id='" . (int) $offer_id . "' ");
        return $query->rows;
    }

    public function getOfferCategory($offer_id) {
        $query = $this->db->query(" SELECT sc.store_category_id AS category_id, CONCAT(scd1.name ,' | ',scd2.name) AS name FROM " . DB_PREFIX . "offer_to_category o2sc LEFT JOIN " . DB_PREFIX . "store_category sc ON (o2sc.category_id = sc.store_category_id) LEFT JOIN " . DB_PREFIX . "store_category_description scd1 ON (sc.store_category_id = scd1.store_category_id) LEFT JOIN " . DB_PREFIX . "store_category_description scd2 ON (sc.store_category_id = scd2.store_category_id) WHERE scd1.language_id = '1' AND scd2.language_id = '2' AND o2sc.offer_id='" . (int) $offer_id . "' ");

        return $query->rows;
    }

    public function getOfferShops($offer_id) {
        $query = $this->db->query(" SELECT n2s.shop_id, CONCAT(sd1.name, ' - ',sd2.name,' | ',md.name, ' | ',zd.name) name FROM " . DB_PREFIX . "offer_to_shop n2s LEFT JOIN store s ON(n2s.shop_id=s.store_id) LEFT JOIN store_description sd1 ON(s.store_id=sd1.store_id) LEFT JOIN store_description sd2 ON(s.store_id=sd2.store_id) LEFT JOIN mall m ON(s.mall_id=m.mall_id) LEFT JOIN mall_description md ON(m.mall_id=md.mall_id) LEFT JOIN zone z ON(s.zone_id=z.zone_id) LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE sd1.language_id='1' AND sd2.language_id='2' AND md.language_id='" . (int) $this->config->get('config_language_id') . "' AND zd.language_id='" . (int) $this->config->get('config_language_id') . "' AND offer_id='" . (int) $offer_id . "' ");
        return $query->rows;
    }

    public function getOfferImages($offer_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "offer_image WHERE offer_id = '" . (int) $offer_id . "'  ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getTotalOffer($data = array()) {
        // $query = $this->db->query(" SELECT COUNT(*) AS total FROM " . DB_PREFIX . "offer");
        $sOfferLinkTable = '';
        $sWhere = '';

        if (isset($data['filter_offer_id']) && !empty($data['filter_offer_id'])) {
            $sWhere = " AND n.offer_id = '" . $data['filter_offer_id'] . "' ";
        } else {
//            if (isset($data['filter_country_id']) && !empty($data['filter_country_id'])) {
//                $sOfferLinkTable = " LEFT JOIN " . DB_PREFIX . "offer_link ol ON (n.offer_id = ol.offer_id) ";
//                $sWhere = " AND ol.link_id = '" . $data['filter_country_id'] . "' AND ol.link_type='country' ";
//            }

            if (isset($data['filter_description']) && !empty($data['filter_description'])) {
                $sWhere = " AND nd.title like '%" . $data['filter_description'] . "%'";
            }
        }

        $sql = " SELECT COUNT(*) AS total 
                 FROM " . DB_PREFIX . "offer n 
                 LEFT JOIN " . DB_PREFIX . "offer_description nd ON (n.offer_id = nd.offer_id) 
                 {$sOfferLinkTable}  
                 WHERE nd.language_id = '" . $this->config->get('config_language_id') . "' 
                 {$sWhere} ";
        // die($sql);
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

}
