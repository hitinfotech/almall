<?php

class ModelMallLook extends Model {

    public function add($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "look SET status = '" . $this->db->escape($data['status']) . "',home = '" . $this->db->escape($data['home']) . "',  sort_order = '" . $this->db->escape($data['sort_order']) . "', is_english = '" . (int)(isset($data['is_english'])?$data['is_english']:0) . "', is_arabic = '" . (int)(isset($data['is_arabic'])?$data['is_arabic']:0) . "',  date_added =NOW(),  user_id = '" . (int) $this->user->getid() . "' ");

        $look_id = $this->db->getLastId();

        $describtion_string = '';
        foreach ($data['look_description'] as $language_id => $value) {
            $describtion_string .= "(" . (int) $look_id . "," . (int) $language_id . ",'" . $this->db->escape($value['name']) . "','" . $this->db->escape($value['description']) . "'),";
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . "look_description (look_id,language_id,title,description) values " . trim($describtion_string, ","));

        $this->db->query("DELETE FROM " . DB_PREFIX . "look_image WHERE look_id = '" . (int) $look_id . "'");
        if (isset($data['look_image'])) {
            $images = $this->upload_images('look', 'look_image', $look_id);
            $this->load->model('tool/image');
            foreach ($data['look_image'] as $key => $look_image) {

                $image = (isset($look_image['image_text']) && (strlen($look_image['image_text']) > 0) ? $look_image['image_text'] : $images[$key]);

                $this->db->query("INSERT INTO " . DB_PREFIX . "look_image SET look_id = '" . (int) $look_id . "', image = '" . $this->db->escape($image) . "', sort_order = '" . (int) $look_image['sort_order'] . "'");

                foreach ($this->config_image->get('looks') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            }
        }

        if (!isset($data['look_link'])) {
            $data['look_link'] = array();
        }

        foreach ($data['look_link'] as $key => $value) {
            if ($key == 'country' && !empty($value)) {
                $country_string = '';
                foreach ($value as $index => $country) {
                    $country_string .= "(" . (int) $look_id . "," . (int) $country . "),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " look_to_country  (look_id,country_id) values " . trim($country_string, ","));
            } elseif ($key == 'group' && !empty($value)) {

                foreach ($value as $index => $group) {
                    $group_string .= "(" . (int) $look_id . "," . (int) $group . "),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " look_to_group  (look_id,look_group_id) values " . trim($group_string, ","));
            } elseif ($key == 'keyword' && !empty($value)) {

                foreach ($value as $index => $keyword) {
                    $keyword_string .= "(" . (int) $look_id . "," . (int) $keyword . ",NOW()),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " looks_keyword  (look_id, keyword_id, date_added) values " . trim($keyword_string, ","));
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . " look_product where look_id = '" . (int) $look_id . "' ");
        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $row) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "look_product SET look_id = '" . (int) $look_id . "', product_id = '" . (int) $row['product_id'] . "', sort_order='" . (int) $row['sort_order'] . "'");
            }
        }
        
        $this->url_custom->create_URL('look', $look_id);

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='look', type_id='" . (int) $look_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $look_id;
    }

    public function edit($look_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "look SET status = '" . $this->db->escape($data['status']) . "', home = '" . $this->db->escape($data['home']) . "', sort_order = '" . $this->db->escape($data['sort_order']) . "', is_english = '" . (int)(isset($data['is_english'])?$data['is_english']:0) . "', is_arabic = '" . (int)(isset($data['is_arabic'])?$data['is_arabic']:0) . "', date_modified = NOW(), last_mod_id = '" . (int) $this->user->getid() . "' WHERE look_id = '" . (int) $look_id . "'");

        $this->db->query(" DELETE FROM look_description WHERE look_id='" . (int) $look_id . "' ");
        $describtion_string = '';
        foreach ($data['look_description'] as $language_id => $value) {
            $describtion_string .= "(" . (int) $look_id . "," . (int) $language_id . ",'" . $this->db->escape($value['name']) . "','" . $this->db->escape($value['description']) . "'),";
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . "look_description (look_id,language_id,title,description) values " . trim($describtion_string, ","));

        $this->db->query("DELETE FROM " . DB_PREFIX . "look_image WHERE look_id = '" . (int) $look_id . "'");
        if (isset($data['look_image'])) {
            $images = $this->upload_images('look', 'look_image', $look_id);

            $this->load->model('tool/image');
            foreach ($data['look_image'] as $key => $look_image) {

                $image = (isset($look_image['image_text']) && (strlen($look_image['image_text']) > 0) ? $look_image['image_text'] : $images[$key]);

                $this->db->query("INSERT INTO " . DB_PREFIX . "look_image SET look_id = '" . (int) $look_id . "', image = '" . $this->db->escape($image) . "', sort_order = '" . (int) $look_image['sort_order'] . "'");
                foreach ($this->config_image->get('looks') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . " look_to_country where look_id = '" . (int) $look_id . "' ");
        $this->db->query("DELETE FROM " . DB_PREFIX . " look_to_group where look_id = '" . (int) $look_id . "' ");
        $this->db->query("DELETE FROM " . DB_PREFIX . " looks_keyword where look_id = '" . (int) $look_id . "' ");

        foreach ($data['look_link'] as $key => $value) {
            if ($key == 'country' && !empty($value)) {
                $country_string = '';
                foreach ($value as $index => $country) {
                    $country_string .= "(" . (int) $look_id . "," . (int) $country . "),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " look_to_country  (look_id,country_id) values " . trim($country_string, ","));
            } elseif ($key == 'group' && !empty($value)) {
                $group_string = '';
                foreach ($value as $index => $group) {
                    $group_string .= "(" . (int) $look_id . "," . (int) $group . "),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " look_to_group  (look_id,look_group_id) values " . trim($group_string, ","));
            } elseif ($key == 'keyword' && !empty($value)) {
                $keyword_string = '';
                foreach ($value as $index => $keyword) {
                    $keyword_string .= "(" . (int) $look_id . "," . (int) $keyword . ",NOW()),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " looks_keyword  (look_id, keyword_id, date_added) values " . trim($keyword_string, ","));
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . " look_product where look_id = '" . (int) $look_id . "' ");
        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $row) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "look_product SET look_id = '" . (int) $look_id . "', product_id = '" . (int) $row['product_id'] . "', sort_order='" . (int) $row['sort_order'] . "'");
            }
        }

        $this->url_custom->create_URL('look', $look_id);


        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='look', type_id='" . (int) $look_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $look_id;
    }

    public function getLookImages($look_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "look_image WHERE look_id = '" . (int) $look_id . "'  ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getProductRelated($look_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "look_product WHERE look_id = '" . (int) $look_id . "' ORDER BY sort_order ASC ");

        foreach ($query->rows as $result) {
            $product_related_data[] = array(
                'product_id' => $result['product_id'],
                'sort_order' => $result['sort_order']
            );
        }

        return $product_related_data;
    }

    public function getAll($data = array()) {
        $sql = " SELECT DISTINCT l.look_id, CONCAT(ld1.title, ' - ',ld2.title ) name,  l.status, l.sort_order, l.date_added, l.date_modified, u.username user_add, mu.username user_modify FROM " . DB_PREFIX . "look l LEFT JOIN " . DB_PREFIX . "look_description ld1 ON (l.look_id = ld1.look_id) LEFT JOIN " . DB_PREFIX . "look_description ld2 ON (l.look_id = ld2.look_id) LEFT JOIN " . DB_PREFIX . "look_to_country lc ON (l.look_id = lc.look_id) LEFT JOIN " . DB_PREFIX . "look_to_group l2g ON (l.look_id = l2g.look_id) LEFT JOIN " . DB_PREFIX . "user u ON (l.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (l.last_mod_id = mu.user_id) WHERE ld1.language_id='1' AND ld2.language_id='2' ";

        $bLimitFlag = true;

        if (!empty($data['filter_look_id'])) {
            $sql .= " AND l.look_id = " . $data['filter_look_id'];
        } else {
            if (!empty($data['filter_filter'])) {
                $sql .= " AND CONCAT(ld1.title,' ',ld2.title)  LIKE '%" .
                        $this->db->escape(trim($data['filter_filter'])) . "%' ";
            }

            if (isset($data['filter_status'])) {
                $sql .= " AND l.status = " . (int) $data['filter_status'] . " ";
            }

            if (!empty($data['filter_group'])) {
                $sql .= " AND l2g.look_group_id ='" . (int) $data['filter_group'] . "'";
            }

            if (isset($data['filter_country_id'])) {
                $sql .= " AND lc.country_id = " . (int) $data['filter_country_id'] . " ";
            }


            $sort_data = array(
                'ld1.title',
                'l.date_modified',
                'l.status',
                'l.date_added',
                'l.sort_order'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY ld1.title";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] <= 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getItem($item_id) {
        $sql = " SELECT * FROM " . DB_PREFIX . "look l LEFT JOIN " . DB_PREFIX .
                "look_description ld ON (l.look_id = ld.look_id) " .
                " WHERE l.look_id = '" . (int) $item_id .
                "' AND ld.language_id = '" . $this->config->get('config_language_id') . "' ";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getItemDescription($item_id) {

        $description_data = array();
        $query = $this->db->query(" SELECT title,description,meta_title,meta_description,meta_keywords,language_id
                                            FROM " . DB_PREFIX . "look_description
                                            WHERE look_id = '" . (int) $item_id . "' ");

        foreach ($query->rows as $result) {
            $description_data[$result['language_id']] = array(
                'name' => isset($result['title']) ? $result['title'] : '',
                'description' => isset($result['description']) ? $result['description'] : (isset($result['body']) ? $result['body'] : ''),
                'meta_title' => isset($result['meta_title']) ? $result['meta_title'] : '',
                'meta_description' => isset($result['meta_description']) ? $result['meta_description'] : '',
                'meta_keywords' => isset($result['meta_keywords']) ? $result['meta_keywords'] : '',
            );
        }
        return $description_data;
    }

    public function getItemLinks($item_id) {

        $result_array = array();
        $country = $this->db->query("SELECT look_to_country.country_id as id ,cd.name FROM look_to_country left join country c on (look_to_country.country_id =c.country_id) LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE language_id = '1' AND c.country_id IN(look_to_country.country_id)AND look_to_country.look_id='" . $item_id . "'");

        foreach ($country->rows as $country_link_row) {
            $result_array['country'][] = array('id' => $country_link_row['id'], 'name' => $country_link_row['name']);
        }


        $groups = $this->db->query("SELECT scd1.look_group_id as id, CONCAT(scd1.name, ' - ', scd2.name) as name FROM look_to_group left join look_group_description scd1 on (look_to_group.look_group_id = scd1.look_group_id) INNER JOIN look_group_description scd2 ON (scd1.look_group_id=scd2.look_group_id) WHERE scd1.look_group_id in (look_to_group.look_group_id) AND scd1.language_id = '1' AND scd2.language_id = '2' and look_to_group.look_id = '" . $item_id . "'");
        foreach ($groups->rows as $groups_link_row) {
            $result_array['group'][] = array('id' => $groups_link_row['id'], 'name' => $groups_link_row['name']);
        }

        $keywords = $this->db->query("SELECT kd1.keyword_id as id, CONCAT(kd1.name, ' - ', kd2.name) as name FROM looks_keyword lk left join keyword_description kd1 on (lk.keyword_id = kd1.keyword_id) INNER JOIN keyword_description kd2 ON (kd1.keyword_id=kd2.keyword_id) WHERE kd1.keyword_id in (lk.keyword_id) AND kd1.language_id = '1' AND kd2.language_id = '2' and lk.look_id = '" . $item_id . "'");
        foreach ($keywords->rows as $keywords_link_row) {
            $result_array['keyword'][] = array('id' => $keywords_link_row['id'], 'name' => $keywords_link_row['name']);
        }


        return $result_array;
    }

    public function getLookGroups($look_id = 0) {
        $sWhereCondition = '';
        if ($look_id > 0) {
            $sWhereCondition = "AND look_id = '" . (int) $look_id . "'";
        }
        $query = $this->db->query(" SELECT * "
                . "FROM " . DB_PREFIX . "look_group g "
                . "LEFT JOIN " . DB_PREFIX . "look_group_description gd ON (g.look_group_id = gd.look_group_id) "
                . "WHERE gd.language_id='" . $this->config->get('config_language_id') . "' {$sWhereCondition} "
                . "ORDER BY g.sort_order ASC");

        return $query->rows;
    }

    public function getLookGroupsByName($data) {
        if ($data['filter_name']) {
            $query = $this->db->query(" SELECT s.look_group_id , CONCAT(sd1.name, ' - ', sd2.name) as name FROM " . DB_PREFIX . "look_group s LEFT JOIN " . DB_PREFIX . "look_group_description sd1 ON (s.look_group_id = sd1.look_group_id) LEFT JOIN " . DB_PREFIX . "look_group_description sd2 ON (s.look_group_id = sd2.look_group_id) WHERE  CONCAT(sd1.name,' ',sd2.name) LIKE '%" . $this->db->escape($data['filter_name']) . "%' AND sd1.language_id = '1' AND sd2.language_id = '2' ");
            return $query->rows;
        } else {
            return array();
        }
    }

    public function getTotalLooks($data) {
        $sql = " SELECT COUNT(DISTINCT l.look_id) total FROM " . DB_PREFIX . "look l LEFT JOIN " . DB_PREFIX . "look_description ld1 ON (l.look_id = ld1.look_id) LEFT JOIN " . DB_PREFIX . "look_description ld2 ON (l.look_id = ld2.look_id) LEFT JOIN " . DB_PREFIX . "look_to_country lc ON (l.look_id = lc.look_id) LEFT JOIN " . DB_PREFIX . "look_to_group t2g ON (l.look_id = t2g.look_id) WHERE ld1.language_id='1' AND ld2.language_id='2' ";

        if (isset($data['filter_country_id'])) {
            $sql .= " AND lc.country_id = '" . (int) $data['filter_country_id'] . "'";
        }

        if (isset($data['filter_look_id'])) {
            $sql .= " AND l.look_id = '" . (int) $data['filter_look_id'] . "'";
        }

        if (isset($data['filter_status'])) {
            $sql .= " AND l.status ='" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_group'])) {
            $sql .= " AND t2g.look_group_id ='" . (int) $data['filter_group'] . "'";
        }


        if (!empty($data['filter_filter'])) {
            $sql .= " AND CONCAT(ld1.title, ' - ',ld2.title ) LIKE '%" . $this->db->escape($data['filter_filter']) . "%'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function delete($look_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "look_description WHERE look_id = '" . (int) $look_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "look_image WHERE look_id = '" . (int) $look_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "look_to_country WHERE look_id = '" . (int) $look_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "look_to_group WHERE look_id = '" . (int) $look_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "look_product WHERE look_id = '" . (int) $look_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "look WHERE look_id = '" . (int) $look_id . "'");

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='look', type_id='" . (int) $look_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function Publish($look_id) {

        $this->db->query("UPDATE " . DB_PREFIX . "look SET status = IF(status=1, 0, 1), date_modified = NOW(), last_mod_id='" . (int) $this->user->getid() . "' WHERE look_id = '" . (int) $look_id . "'");
    }

}
