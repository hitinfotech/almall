<?php

class ModelMallShopdesigner extends Model {

    public function addShop($data) {
        $this->db->query(" INSERT INTO " . DB_PREFIX . "store "
                . "SET "
                . "`is_designer`= '1', "
                . "`url` = '" . $this->db->escape($data['url']) . "', "
                . "`ssl` = '" . $this->db->escape($data['ssl']) . "', "
                // . "`mall_id` = '" . (int) $data['mall_id'] . "', "
                . "`zone_id` = '" . (int) $data['city_id'] . "', "
                . "`phone` ='" . $this->db->escape($data['phone']) . "', "
                . "`main_store_id` = '" . (int) $data['main_store_id'] . "', "
                . "`user_id` = '" . (int) $this->user->getid() . "', "
                . "`status` = '" . (int) $data['status'] . "', "
                . "`landing` = '" . $this->db->escape($data['landing']) . "', "
                . "`sort_order` = '" . (int) $data['sort_order'] . "', "
                . "`display` = '" . $this->db->escape($data['display']) . "', "
                . "`social_facebook` = '" . $this->db->escape($data['social_fb']) . "', "
                . "`social_jeeran` = '" . $this->db->escape($data['social_jeeran']) . "', "
                . "`social_twitter` = '" . $this->db->escape($data['social_tw']) . "', "
                . "`social_website` = '" . $this->db->escape($data['website']) . "', "
                . "`date_added` = NOW() ");

        $shop_id = $this->db->getLastId();

        $this->load->model('tool/image');
        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('Designer', 'image', $shop_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('shops_brands_designer') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query("UPDATE " . DB_PREFIX . "store SET `image` = '" . $this->db->escape($image) . "' WHERE store_id = '" . (int) $shop_id . "' ");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "store_description WHERE store_id = '" . (int) $shop_id . "'");
        foreach ($data['shop_description'] as $language_id => $value) {
            $this->db->query(" INSERT INTO " . DB_PREFIX . "store_description SET "
                    . "store_id = '" . (int) $shop_id . "', "
                    . "language_id = '" . (int) $language_id . "', "
                    . "name = '" . $this->db->escape($value['name']) . "', "
                    . "description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', "
                    . "open = '" . $this->db->escape($value['open']) . "', "
                    . "location = '" . $this->db->escape($value['location']) . "', "
                    . "`meta_title` = '" . $this->db->escape($value['meta_title']) . "', "
                    . "`meta_description` = '" . $this->db->escape($value['meta_description']) . "', "
                    . "`meta_keyword` = '" . $this->db->escape($value['meta_keyword']) . "' ");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "maps WHERE link_id = '" . (int) $shop_id . "' AND link_type='shop'");
        $this->db->query("INSERT INTO " . DB_PREFIX . "maps SET   link_id = '" . (int) $shop_id . "', link_type='shop', `longitude` = '" . $this->db->escape($data['longitude']) . "', `latitude` = '" . $this->db->escape($data['latitude']) . "', `zoom` = '" . (float) $data['zoom'] . "'");

        $this->db->query("DELETE FROM store_category_to_store WHERE store_id='" . (int) $shop_id . "'");
        if (isset($data['store_category'])) {
            foreach ($data['store_category'] as $store_category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "store_category_to_store SET store_id = '" . (int) $shop_id . "', store_category_id = '" . (int) $store_category_id . "'");
            }
        }

        $main_store_id = $this->addMainShop($data);
        $this->db->query("UPDATE store SET main_store_id = '" . (int) $main_store_id . "' WHERE main_store_id = '" . (int) $shop_id . "' ");

        $this->db->query("DELETE FROM brand_to_store WHERE store_id='" . (int) $shop_id . "'");
        if (isset($data['store_brands'])) {
            foreach ($data['store_brands'] as $brand_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_store SET store_id = '" . (int) $shop_id . "', brand_id = '" . (int) $brand_id . "'");
            }
        }

        $this->url_custom->create_URL('store', $shop_id);

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='shop', type_id='" . (int) $shop_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $shop_id;
    }
    
    public function deleteMainStore($main_store_id) {
        $this->event->trigger('pre.admin.main_store.delete', $main_store_id);

        $this->db->query("DELETE FROM " . DB_PREFIX . "main_store_description WHERE main_store_id = '" . (int) $main_store_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "main_store WHERE main_store_id = '" . (int) $main_store_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "main_store_description WHERE main_store_id = '" . (int) $main_store_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "maps WHERE link_type='mainstore' AND link_id = '" . (int) $main_store_id . "'");
        $this->cache->delete('main_store');

        $this->event->trigger('post.admin.main_store.delete', $main_store_id);
    }

    protected function addMainShop($data, $old_main_store_id = 0) {
        $this->deleteMainStore($old_main_store_id);
        
        $this->db->query(" INSERT INTO " . DB_PREFIX . "main_store "
                . "SET "
                . " is_designer = 1, "
                //. "`url` = '" . $this->db->escape($data['url']) . "', "
                // . "`ssl` = '" . $this->db->escape($data['ssl']) . "', "
                // . "`mall_id` = '" . (int) $data['mall_id'] . "', "
                . "`city_id` = '" . (int) $data['city_id'] . "', "
                . "`phone` ='" . $this->db->escape($data['phone']) . "', "
                // . "`main_store_id` = '" . (int) $data['main_store_id'] . "', "
                . "`user_id` = '" . (int) $this->user->getid() . "', "
                . "`status` = '" . (int) $data['status'] . "', "
                // . "`landing` = '" . $this->db->escape($data['landing']) . "', "
                . "`sort_order` = '" . (int) $data['sort_order'] . "', "
                //. "`display` = '" . $this->db->escape($data['display']) . "', "
                . "`social_fb` = '" . $this->db->escape($data['social_fb']) . "', "
                . "`social_jeeran` = '" . $this->db->escape($data['social_jeeran']) . "', "
                . "`social_tw` = '" . $this->db->escape($data['social_tw']) . "', "
                . "`website` = '" . $this->db->escape($data['website']) . "', "
                . "`date_added` = NOW() ");

        $shop_id = $this->db->getLastId();

        $this->load->model('tool/image');
        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('DesignerMain', 'image', $shop_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('shops_brands_designer') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query("UPDATE " . DB_PREFIX . "main_store SET `image` = '" . $this->db->escape($image) . "' WHERE main_store_id = '" . (int) $shop_id . "' ");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "main_store_description WHERE main_store_id = '" . (int) $shop_id . "'");
        foreach ($data['shop_description'] as $language_id => $value) {
            $this->db->query(" INSERT INTO " . DB_PREFIX . "main_store_description SET "
                    . "main_store_id = '" . (int) $shop_id . "', "
                    . "language_id = '" . (int) $language_id . "', "
                    . "name = '" . $this->db->escape($value['name']) . "', "
                    . "description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', "
                    // . "open = '" . $this->db->escape($value['open']) . "', "
                    . "location = '" . $this->db->escape($value['location']) . "' "
            );
        }

        //$this->db->query("DELETE FROM " . DB_PREFIX . "maps WHERE link_id = '" . (int) $shop_id . "' AND link_type='shop'");
        //$this->db->query("INSERT INTO " . DB_PREFIX . "maps SET   link_id = '" . (int) $shop_id . "', link_type='shop', `longitude` = '" . $this->db->escape($data['longitude']) . "', `latitude` = '" . $this->db->escape($data['latitude']) . "', `zoom` = '" . (float) $data['zoom'] . "'");
        //$this->db->query("DELETE FROM store_category_to_store WHERE store_id='" . (int) $shop_id . "'");
        //if (isset($data['store_category'])) {
        //    foreach ($data['store_category'] as $store_category_id) {
        //        $this->db->query("INSERT INTO " . DB_PREFIX . "store_category_to_store SET store_id = '" . (int) $shop_id . "', store_category_id = '" . (int) $store_category_id . "'");
        //    }
        //}
        //$this->db->query("DELETE FROM brand_to_store WHERE store_id='" . (int) $shop_id . "'");
        //if (isset($data['store_brands'])) {
        //    foreach ($data['store_brands'] as $brand_id) {
        //        $this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_store SET store_id = '" . (int) $shop_id . "', brand_id = '" . (int) $brand_id . "'");
        //    }
        //}
        //$this->url_custom->create_URL('main_store', $shop_id);

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='shop', type_id='" . (int) $shop_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $shop_id;
    }

    public function editShop($shop_id, $data) {
        $this->db->query(" UPDATE " . DB_PREFIX . "store  SET `url` = '" . $this->db->escape($data['url']) . "', `ssl` = '" . $this->db->escape($data['ssl']) . "', `zone_id` = '" . (int) $data['city_id'] . "', phone='" . $this->db->escape($data['phone']) . "', `last_mod_id` = '" . (int) $this->user->getid() . "', `status` = '" . (int) $data['status'] . "', `landing` = '" . (int) $data['landing'] . "', `sort_order` = '" . (int) $data['sort_order'] . "', `display` = '" . $this->db->escape($data['display']) . "', `social_facebook` = '" . $this->db->escape($data['social_fb']) . "', `social_jeeran` = '" . $this->db->escape($data['social_jeeran']) . "', `social_twitter` = '" . $this->db->escape($data['social_tw']) . "',  `social_website` = '" . $this->db->escape($data['website']) . "', `date_modified` = NOW() WHERE  store_id = '" . (int) $shop_id . "'");

        // `meta_tag` = '" . $this->db->escape($data['meta_tag']) . "', 
        // `meta_title` = '" . $this->db->escape($data['meta_title']) . "', 
        // `meta_description` = '" . $this->db->escape($data['meta_description']) . "', 
        // `meta_keyword` = '" . $this->db->escape($data['meta_keyword']) . "',

        $this->load->model('tool/image');
        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('Designer', 'image', $shop_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('shops_brands_designer') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query("UPDATE " . DB_PREFIX . "store SET `image` = '" . $this->db->escape($image) . "' WHERE store_id = '" . (int) $shop_id . "' ");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "store_description WHERE store_id = '" . (int) $shop_id . "'");
        foreach ($data['shop_description'] as $language_id => $value) {
            $this->db->query(" INSERT INTO " . DB_PREFIX . "store_description SET 
                                store_id = '" . (int) $shop_id . "', 
                                language_id = '" . (int) $language_id . "', 
                                name = '" . $this->db->escape($value['name']) . "', 
                                description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', 
                                open = '" . $this->db->escape($value['open']) . "', 
                                location = '" . $this->db->escape($value['location']) . "',
                                `meta_title` = '" . $this->db->escape($value['meta_title']) . "', 
                                `meta_description` = '" . $this->db->escape($value['meta_description']) . "', 
                                `meta_keyword` = '" . $this->db->escape($value['meta_keyword']) . "'
                            ");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "maps WHERE link_id = '" . (int) $shop_id . "' AND link_type='shop'");
        $this->db->query("INSERT INTO " . DB_PREFIX . "maps SET   link_id = '" . (int) $shop_id . "', link_type='shop', `longitude` = '" . $this->db->escape($data['longitude']) . "', `latitude` = '" . $this->db->escape($data['latitude']) . "', `zoom` = '" . (float) $data['zoom'] . "'");

        // Update Products count
        $query_products_count = $this->db->query("SELECT count(*) products_count FROM " . DB_PREFIX . "product_to_store p2s inner join " . DB_PREFIX . "product p ON (p2s.product_id = p.product_id) where p2s.store_id = '" . (int) $shop_id . "' and p.status = '1'");
        $products_count = $query_products_count->row['products_count'];
        $this->db->query(" UPDATE " . DB_PREFIX . "store SET products_count = '" . $products_count . "' WHERE store_id = '" . (int) $shop_id . "'");

        $this->db->query("DELETE FROM store_category_to_store WHERE store_id='" . (int) $shop_id . "'");
        if (isset($data['store_category'])) {
            foreach ($data['store_category'] as $store_category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "store_category_to_store SET store_id = '" . (int) $shop_id . "', store_category_id = '" . (int) $store_category_id . "'");
            }
        }

        $old_main_store_id = 0;
        $old_main_store = $this->db->query(" SELECT main_store_id FROM store WHERE store_id = '" . (int) $shop_id . "' ");
        if ($old_main_store->num_rows) {
            $old_main_store_id = $old_main_store->row['main_store_id'];
        }

        $main_store_id = $this->addMainShop($data, $old_main_store_id);
        $this->db->query("UPDATE store SET main_store_id = '" . (int) $main_store_id . "' WHERE store_id = '" . (int) $shop_id . "' ");

        $this->url_custom->create_URL('store', $shop_id);

        $this->db->query("DELETE FROM brand_to_store WHERE store_id='" . (int) $shop_id . "'");
        if (isset($data['store_brands'])) {
            foreach ($data['store_brands'] as $brand_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_store SET store_id = '" . (int) $shop_id . "', brand_id = '" . (int) $brand_id . "'");
            }
        }

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='shop', type_id='" . (int) $shop_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function deleteShop($shop_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "store WHERE store_id = '" . (int) $shop_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "store_description WHERE store_id = '" . (int) $shop_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "maps WHERE link_id = '" . (int) $shop_id . "' AND link_type='shop'");

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='shop', type_id='" . (int) $shop_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function getShop($shop_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "store s LEFT JOIN " . DB_PREFIX . "store_description sd ON(s.store_id = sd.store_id) WHERE sd.language_id = '" . $this->config->get('config_language_id') . "' AND s.store_id = '" . (int) $shop_id . "'");

        return $query->row;
    }

    public function getGoogle($shop_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "maps WHERE link_id = '" . (int) $shop_id . "' AND link_type= 'shop'");

        return $query->row;
    }

    public function getShopDescriptions($shop_id) {
        $shop_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store_description WHERE store_id = '" . (int) $shop_id . "'");

        foreach ($query->rows as $result) {
            $shop_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'open' => $result['open'],
                'location' => $result['location'],
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword']
            );
        }
        return $shop_description_data;
    }

    public function getShops($data) {

        $cache = 'shopdesigner.getshops.' . md5(http_build_query($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $sql = " SELECT DISTINCT s.store_id, CONCAT(sd.name, ' - ',sd2.name) AS name, s.status, s.zone_id,  z.country_id,  zd.name city,  s.products_count,  s.sort_order ,u.username user_add, mu.username user_modify  FROM " . DB_PREFIX . "store s LEFT JOIN " . DB_PREFIX . "store_description sd ON (s.store_id=sd.store_id) LEFT JOIN " . DB_PREFIX . "store_description sd2 ON (s.store_id=sd2.store_id) LEFT JOIN " . DB_PREFIX . "zone z ON( s.zone_id=z.zone_id ) LEFT JOIN " . DB_PREFIX . "zone_description zd ON(s.zone_id=zd.zone_id) LEFT JOIN " . DB_PREFIX . "user u ON (s.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (s.last_mod_id = mu.user_id) ";
            $sql .= " WHERE is_designer='1' AND sd.language_id = '1' AND sd2.language_id = '2' AND zd.language_id = '" . (int) $this->config->get('config_language_id') . "' ";

            if (isset($data['filter_shop_id']) && !empty($data['filter_shop_id'])) {
                $sql .= " AND s.store_id = '" . $data['filter_shop_id'] . "' ";
            } else {
                if (isset($data['filter_filter']) && !empty($data['filter_filter'])) {
                    $sql.= " AND CONCAT(sd.name,' ',' ',sd2.name) LIKE '%" . $this->db->escape($data['filter_filter']) . "%' ";
                }
                if (isset($data['filter_name']) && !empty($data['filter_name'])) {
                    $sql.= " AND CONCAT(sd.name,' ',' ',sd2.name) LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
                }

                if (isset($data['filter_city_id']) && !empty($data['filter_city_id'])) {
                    $sql.= " AND s.zone_id = '" . (int) $data['filter_city_id'] . "' ";
                }

                if (isset($data['filter_country_id']) && !empty($data['filter_country_id'])) {
                    $sql.= " AND z.country_id = '" . (int) $data['filter_country_id'] . "' ";
                }
            }

            $sort_data = array(
                'sd.name',
                's.products_count',
                's.sort_order',
                'zd.name',
                'msd.main_store'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY s.sort_order ";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        }
        return $result;
    }

    public function getTotalShops($data) {
        unset($data['sort']);
        unset($data['order']);
        unset($data['start']);
        unset($data['limit']);

        $cache = 'shopdesigner.total_store_' . md5(http_build_query($data));

        $total = $this->cache->get($cache);
        if (!$total) {
            $sql = " SELECT COUNT(DISTINCT s.store_id) AS total FROM " . DB_PREFIX . "store s LEFT JOIN " . DB_PREFIX . "store_description sd ON (s.store_id=sd.store_id) LEFT JOIN " . DB_PREFIX . "store_description sd2 ON (s.store_id=sd2.store_id) LEFT JOIN " . DB_PREFIX . "zone z ON( s.zone_id=z.zone_id ) LEFT JOIN " . DB_PREFIX . "zone_description zd ON(s.zone_id=zd.zone_id) LEFT JOIN " . DB_PREFIX . "user u ON (s.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (s.last_mod_id = mu.user_id) ";
            $sql .= " WHERE is_designer='1' AND sd.language_id = '1' AND sd2.language_id = '2' AND zd.language_id = '" . (int) $this->config->get('config_language_id') . "' ";

            if (isset($data['filter_shop_id']) && !empty($data['filter_shop_id'])) {
                $sql .= " AND s.store_id = '" . $data['filter_shop_id'] . "' ";
            } else {
                if (isset($data['filter_filter']) && !empty($data['filter_filter'])) {
                    $sql.= " AND CONCAT(sd.name,' ',' ',sd2.name) LIKE '%" . $this->db->escape($data['filter_filter']) . "%' ";
                }
                if (isset($data['filter_name']) && !empty($data['filter_name'])) {
                    $sql.= " AND CONCAT(sd.name,' ',' ',sd2.name) LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
                }

                if (isset($data['filter_city_id']) && !empty($data['filter_city_id'])) {
                    $sql.= " AND s.zone_id = '" . (int) $data['filter_city_id'] . "' ";
                }

                if (isset($data['filter_country_id']) && !empty($data['filter_country_id'])) {
                    $sql.= " AND z.country_id = '" . (int) $data['filter_country_id'] . "' ";
                }
            }

            $query = $this->db->query($sql);

            $this->cache->set($cache, $query->row['total']);

            return $query->row['total'];
        }

        return $total;
    }

    public function getshopsByName($data) {
        if ($data['filter_name']) {
            $query = $this->db->query(" SELECT s.store_id , CONCAT(sd1.name, ' - ', sd2.name) as name
                                        FROM " . DB_PREFIX . "store s 
                                        LEFT JOIN " . DB_PREFIX . "store_description sd1 ON (s.store_id = sd1.store_id) 
                                        LEFT JOIN " . DB_PREFIX . "store_description sd2 ON (s.store_id = sd2.store_id) 
                                        
                                        WHERE is_designer=1 AND
                                            CONCAT(sd1.name,' ',sd2.name) LIKE '%" . $this->db->escape($data['filter_name']) . "%' 
                                        AND sd1.language_id = '1'
                                        AND sd2.language_id = '2'
                                        AND s.status = 1 ");
            return $query->rows;
        } else {
            return array();
        }
    }

    public function getStoresByMainStore($main_store_id) {
        $query = $this->db->query(" SELECT s.store_id, CONCAT(sd1.name, ' - ', sd2.name) as name
                                    FROM " . DB_PREFIX . "store s 
                                    LEFT JOIN " . DB_PREFIX . "store_description sd1 ON (s.store_id=sd1.store_id) 
                                    LEFT JOIN " . DB_PREFIX . "store_description sd2 ON (s.store_id=sd2.store_id) 
                                    
                                    WHERE  
                                        s.main_store_id = '" . (int) $main_store_id . "'  
                                    AND sd1.language_id = '1'
                                    AND sd2.language_id = '2'");

        return $query->rows;
    }

}
