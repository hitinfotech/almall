<?php

class ModelMallKeyword extends Model {

    public function add($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "keyword SET  date_added =NOW(),  date_modified =NOW(), user_id = '" . (int)$this->user->getid() . "' , last_mod_id = '" . (int)$this->user->getid() . "' ");

        $keyword_id = $this->db->getLastId();
        $describtion_string = '';
        foreach ($data['keyword_description'] as $language_id => $value) {
            $describtion_string .= "(" . (int) $keyword_id . "," . (int) $language_id . ",'" . $this->db->escape($value['name']) . "','" . $this->db->escape($value['description']) . "'),";
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . "keyword_description (keyword_id,language_id,name,body) values " . trim($describtion_string, ","));

        $this->cache->deletekeys("catalog.keyword");
        $this->cache->deletekeys("admin.keyword");
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='keywrd', keyword_id='" . (int) $keyword_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $keyword_id;
    }

    public function edit($keyword_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "keyword SET date_modified = NOW(), last_mod_id = '" . (int)$this->user->getid() . "' WHERE keyword_id = '" . (int) $keyword_id . "'");

        $this->db->query(" DELETE FROM keyword_description WHERE keyword_id='" . (int) $keyword_id . "' ");
        $describtion_string = '';
        foreach ($data['keyword_description'] as $language_id => $value) {
            $describtion_string .= "(" . (int) $keyword_id . "," . (int) $language_id . ",'" . $this->db->escape($value['name']) . "','" . $this->db->escape($value['description']) . "'),";
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . "keyword_description (keyword_id,language_id,name,body) values " . trim($describtion_string, ","));


        $this->cache->deletekeys("catalog.keyword");
        $this->cache->deletekeys("admin.keyword");
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='keyword', type_id='" . (int) $keyword_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $keyword_id;
    }

    public function getAll($data = array()) {
        $sql = " SELECT DISTINCT k.keyword_id, CONCAT(kd1.name, ' - ',kd2.name ) name, k.date_added, k.date_modified,u.username user_add, mu.username user_modify FROM " . DB_PREFIX . "keyword k LEFT JOIN " . DB_PREFIX . "keyword_description kd1 ON (k.keyword_id = kd1.keyword_id) LEFT JOIN " . DB_PREFIX . "keyword_description kd2 ON (k.keyword_id = kd2.keyword_id) LEFT JOIN " . DB_PREFIX . "user u ON (k.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (k.last_mod_id = mu.user_id) WHERE kd1.language_id='1' AND kd2.language_id='2' ";

        $bLimitFlag = true;

        if (!empty($data['filter_keyword_id'])) {
            $sql .= " AND k.keyword_id = " . $data['filter_keyword_id'];
        } else {
            if (!empty($data['filter_filter'])) {
                $sql .= " AND CONCAT(kd1.name,' ',kd2.name)  LIKE '%" .
                        $this->db->escape(trim($data['filter_filter'])) . "%' ";
            }

            $sort_data = array(
                'kd1.name',
                'k.date_modified',
                'k.date_added',
                'k.sort_order'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY kd1.name";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] <= 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getItem($item_id) {
        $sql = " SELECT * FROM " . DB_PREFIX . "keyword k LEFT JOIN " . DB_PREFIX .
                "keyword_description kd ON (k.keyword_id = kd.keyword_id) " .
                " WHERE k.keyword_id = '" . (int) $item_id .
                "' AND kd.language_id = '" . $this->config->get('config_language_id') . "' ";
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getItemDescription($item_id) {

        $description_data = array();
        $query = $this->db->query(" SELECT name,body AS description,language_id
                                            FROM " . DB_PREFIX . "keyword_description
                                            WHERE keyword_id = '" . (int) $item_id . "' ");

        foreach ($query->rows as $result) {
            $description_data[$result['language_id']] = array(
                'name' => isset($result['name']) ? $result['name'] : '',
                'description' => isset($result['description']) ? $result['description'] : '',
            );
        }
        return $description_data;
    }

        public function getTotalKeywords($data) {
        $sql = " SELECT COUNT(DISTINCT k.keyword_id) total FROM " . DB_PREFIX . "keyword k LEFT JOIN " . DB_PREFIX . "keyword_description kd1 ON (k.keyword_id = kd1.keyword_id) LEFT JOIN " . DB_PREFIX . "keyword_description kd2 ON (k.keyword_id = kd2.keyword_id) WHERE kd1.language_id='1' AND kd2.language_id='2' ";

        if (isset($data['filter_look_id'])) {
            $sql .= " AND k.keyword_id = '" . (int) $data['filter_look_id'] . "'";
        }

        if (!empty($data['filter_filter'])) {
            $sql .= " AND CONCAT(kd1.name, ' - ',kd2.name ) LIKE '%" . $this->db->escape($data['filter_filter']) . "%'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function delete($item_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "keyword_description WHERE keyword_id = '" . (int) $item_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "keyword WHERE keyword_id = '" . (int) $item_id . "'");


        $this->cache->deletekeys("catalog.keyword");
        $this->cache->deletekeys("admin.keyword");
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='keyword', type_id='" . (int) $item_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

    }

    public function getKeywordsByName($data) {
        if ($data['filter_name']) {
          $query = $this->db->query(" SELECT k.keyword_id , CONCAT(kd1.name, ' - ', kd2.name) as name FROM " . DB_PREFIX . "keyword k LEFT JOIN " . DB_PREFIX . "keyword_description kd1 ON (k.keyword_id = kd1.keyword_id) LEFT JOIN " . DB_PREFIX . "keyword_description kd2 ON (k.keyword_id = kd2.keyword_id) WHERE CONCAT(kd1.name,' ',kd2.name) LIKE '%" . $this->db->escape($data['filter_name']) . "%' AND kd1.language_id = '1'  AND kd2.language_id = '2'");
            return $query->rows;
        } else {
            return array();
        }
    }

}
