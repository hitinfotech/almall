<?php

class ModelMallMainGroup extends Model {

    public function addMainGroup($data) {
        $this->event->trigger('pre.admin.main_group.add', $data);

        $this->db->query("INSERT INTO " . DB_PREFIX . "main_group SET website='". $this->db->escape($data['website']) ."', social_fb='". $this->db->escape($data['social_fb']) ."', social_tw='". $this->db->escape($data['social_tw']) ."', social_jeeran='". $this->db->escape($data['social_jeeran']) ."', sort_order = '" . (int) $data['sort_order'] . "', date_modified = NOW(), date_added = NOW(), user_id = '" .(int) $this->user->getid() . "'");

        $main_group_id = $this->db->getLastId();

        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('main_group', 'image', $main_group_id);
            if($image){
                $this->load->model('tool/image');
                foreach ($this->config_image->get('shops_brands_designer') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE main_group SET `image` = '" . $this->db->escape($image) . "' WHERE main_group_id='". (int)$main_group_id ."' ");
        }

        foreach ($data['main_group_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "main_group_description SET main_group_id = '" . (int) $main_group_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', location = '" . $this->db->escape($value['location']) . "'");
        }

        $this->cache->delete('main_group');

        $this->event->trigger('post.admin.main_group.add', $main_group_id);

        return $main_group_id;
    }

    public function editMainGroup($main_group_id, $data) {
        $this->event->trigger('pre.admin.main_group.edit', $data);

        $this->db->query("UPDATE " . DB_PREFIX . "main_group SET website='". $this->db->escape($data['website']) ."', social_fb='". $this->db->escape($data['social_fb']) ."', social_tw='". $this->db->escape($data['social_tw']) ."', social_jeeran='". $this->db->escape($data['social_jeeran']) ."', sort_order = '" . (int) $data['sort_order'] . "', date_modified = NOW(), last_mod_id = '" .(int) $this->user->getid() . "'   WHERE main_group_id = '" . (int) $main_group_id . "'");

       
        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('main_group', 'image', $main_group_id);
            if($image){
                $this->load->model('tool/image');
                foreach ($this->config_image->get('shops_brands_designer') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE main_group SET `image` = '" . $this->db->escape($image) . "' WHERE main_group_id='". (int)$main_group_id ."' ");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "main_group_description WHERE main_group_id = '" . (int) $main_group_id . "'");

        foreach ($data['main_group_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "main_group_description SET main_group_id = '" . (int) $main_group_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', location = '" . $this->db->escape($value['location']) . "'");
        }

    }

    public function deleteMainGroup($main_group_id) {
        $this->event->trigger('pre.admin.main_group.delete', $main_group_id);

        $this->db->query("DELETE FROM " . DB_PREFIX . "main_group_description WHERE main_group_id = '" . (int) $main_group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "main_group WHERE main_group_id = '" . (int) $main_group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "main_group_description WHERE main_group_id = '" . (int) $main_group_id . "'");
        $this->cache->delete('main_group');

        $this->event->trigger('post.admin.main_group.delete', $main_group_id);
    }
    
    public function getMainGroupDescriptions($main_group_id){
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "main_group_description WHERE main_group_id='". (int)$main_group_id ."' ");

        $main_group_description_data = array();

        foreach ($query->rows as $result) {
            $main_group_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'location' => $result['location']
            );
        }

        return $main_group_description_data;
    }

    public function getMainGroup($main_group_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "main_group mg LEFT JOIN " . DB_PREFIX . "main_group_description mgd ON(mg.main_group_id = mgd.main_group_id) WHERE mgd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND mg.main_group_id='". (int)$main_group_id ."' ");

        return $query->row;
    }

    public function getMainGroups($data = array()) {
        $sql = "SELECT mg.main_group_id, mg.image, mgd.name, mg.sort_order , mg.website , mg.social_fb , mg.social_tw, mg.social_jeeran, mg.date_modified,mg.date_added,u.username user_add, mu.username user_modify  FROM " . DB_PREFIX . "main_group mg LEFT JOIN " . DB_PREFIX . "main_group_description mgd ON(mg.main_group_id = mgd.main_group_id) LEFT JOIN " . DB_PREFIX . "user u ON (mg.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (mg.last_mod_id = mu.user_id) WHERE mgd.language_id = '" . (int) $this->config->get('config_language_id') . "' ";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalMainGroups() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "main_group ");

        return $query->row['total'];
    }

    public function getMainGroupsByName($data) {
        if($data['filter_name']){
            $query = $this->db->query("SELECT mgd.main_group_id, mgd.name FROM " . DB_PREFIX . "main_group mg LEFT JOIN " . DB_PREFIX . "main_group_description mgd ON(mg.main_group_id = mgd.main_group_id) WHERE mgd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND mgd.name LIKE '%". $this->db->escape($data['filter_name']) ."%' ");
            return $query->rows;
        } else {
            return array();
        }
    }
}