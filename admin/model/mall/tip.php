<?php

class ModelMallTip extends Model {

    public function addTip($data) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "tip SET status = '" . $this->db->escape($data['status']) . "', home =  '" . $this->db->escape($data['home']) . "',sort_order = '" . $this->db->escape($data['sort_order']) . "', is_english = '" . $this->db->escape(isset($data['is_english'])?$data['is_english']:0) . "', is_arabic = '" . $this->db->escape(isset($data['is_arabic'])?$data['is_arabic']:0) . "', date_added= NOW(),user_id = '" . (int) $this->user->getid() . "'");

        $tip_id = $this->db->getLastId();

        if (isset($data['tip_section'])) {
            $this->load->model('tool/image');
            $images = $this->upload_images_lan('tip', 'tip_section', $tip_id);

            foreach ($data['tip_section'] as $key => $row) {
                foreach ($row as $language_id => $tip_section_row) {
                    $image = (isset($tip_section_row['image_text']) && (strlen($tip_section_row['image_text']) > 0) ? $tip_section_row['image_text'] : $images[$language_id][$key]);
                    foreach ($this->config_image->get('tips') as $rowm) {
                        if ($rowm['width'] > 10) {
                            $this->model_tool_image->resize($image, $rowm['width'], $rowm['hieght']);
                        }
                    }
                    $this->db->query("INSERT INTO " . DB_PREFIX . "tip_section SET tip_id = '" . (int) $tip_id . "', image = '" . $this->db->escape($image) . "', title='" . $this->db->escape($tip_section_row['title']) . "', description='" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($tip_section_row['description']))) . "', sort_order = '" . (int) $tip_section_row['sort_order'] . "', product_id = '" . (int) $tip_section_row['product_id'] . "', language_id = '" . (int) $language_id . "'");

                    /* foreach ($this->config_image->get('tip') as $row) {
                      if ($row['width'] > 10) {
                      $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                      }
                      } */
                }
            }
        }


        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('tip', 'image', $tip_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('tips') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE tip SET `image` = '" . $this->db->escape($image) . "' WHERE tip_id='" . (int) $tip_id . "' ");
        }

        $describtion_string = '';
        foreach ($data['tip_description'] as $language_id => $value) {
            $describtion_string .= "(" . (int) $tip_id . "," . (int) $language_id . ",'" . $this->db->escape($value['name']) . "','" . $this->db->escape($value['author']) . "','" . $this->db->escape($value['description']) . "'),";
        }
        $this->db->query("INSERT INTO " . DB_PREFIX . "tip_description (tip_id,language_id,name,author,body) values " . trim($describtion_string, ","));

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "tip_product SET tip_id = '" . (int) $tip_id . "', product_id = '" . (int) $related_id . "'");
            }
        }

        foreach ($data['tip_link'] as $key => $value) {
            if ($key == 'country' && !empty($value)) {
                $country_string = '';
                foreach ($value as $index => $country) {
                    $country_string .= "(" . (int) $tip_id . "," . (int) $country . "),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " tip_to_country  (tip_id,country_id) values " . trim($country_string, ","));
            } elseif ($key == 'group' && !empty($value)) {
                $group_string = '';
                foreach ($value as $index => $group) {
                    $group_string .= "(" . (int) $tip_id . "," . (int) $group . "),";
                }

                $this->db->query("INSERT INTO " . DB_PREFIX . " tip_to_group  (tip_id,tip_group_id) values " . trim($group_string, ","));
            } elseif ($key == 'product' && !empty($value)) {

                $product_string = '';
                foreach ($value as $index => $product) {
                    $product_string .= "(" . (int) $tip_id . "," . (int) $product . "),";
                }

                $this->db->query("INSERT INTO " . DB_PREFIX . " tip_product  (tip_id,product_id) values " . trim($product_string, ","));
            } elseif ($key == 'keyword' && !empty($value)) {

                $keyword_string = '';
                foreach ($value as $index => $keyword) {
                    $keyword_string .= "(" . (int) $tip_id . "," . (int) $keyword . ",NOW()),";
                }

                $this->db->query("INSERT INTO " . DB_PREFIX . " tips_keyword  (tip_id,keyword_id,date_added) values " . trim($keyword_string, ","));
            }
        }
        $this->url_custom->create_URL('tip', $tip_id);


        $this->db->query(" INSERT IGNORE INTO cache SET `type`='tip', type_id='" . (int) $tip_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $tip_id;
    }

    public function editTip($tip_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "tip SET sort_order = '" . $this->db->escape($data['sort_order']) . "', is_english = '" . $this->db->escape(isset($data['is_english'])?$data['is_english']:0) . "', is_arabic = '" . $this->db->escape(isset($data['is_arabic'])?$data['is_arabic']:0) . "', status = '" . $this->db->escape($data['status']) . "', home = '" . $this->db->escape($data['home']) . "',  date_modified = NOW(), last_mod_id = '" . (int) $this->user->getid() . "' WHERE tip_id = '" . (int) $tip_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "tip_section WHERE tip_id = '" . (int) $tip_id . "'");
        if (isset($data['tip_section'])) {
            $this->load->model('tool/image');
            $images = $this->upload_images_lan('tip', 'tip_section', $tip_id);

            foreach ($data['tip_section'] as $key => $row) {
                foreach ($row as $language_id => $tip_section_row) {
                    $image = (isset($tip_section_row['image_text']) && (strlen($tip_section_row['image_text']) > 0) ? $tip_section_row['image_text'] : '');
                    if(isset($images[$language_id][$key]))
                        $image = $images[$language_id][$key];

                    if(isset($tip_section_row['no_image']) && $tip_section_row['no_image'] == 'on')
                        $image = '';

                    foreach ($this->config_image->get('tips') as $rowm) {
                        if ($rowm['width'] > 10) {
                            $this->model_tool_image->resize($image, $rowm['width'], $rowm['hieght']);
                        }
                    }
                    $this->db->query("INSERT INTO " . DB_PREFIX . "tip_section SET tip_id = '" . (int) $tip_id . "', image = '" . $this->db->escape($image) . "', title='" . $this->db->escape($tip_section_row['title']) . "', description='" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($tip_section_row['description']))) . "', sort_order = '" . (int) $tip_section_row['sort_order'] . "', product_id = '" . (int) $tip_section_row['product_id'] . "', language_id = '" . (int) $language_id . "'");
                }
            }
        }
        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('tip', 'image', $tip_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('tips') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE tip SET `image` = '" . $this->db->escape($image) . "' WHERE tip_id='" . (int) $tip_id . "' ");
        }

        foreach ($data['tip_description'] as $language_id => $value) {
		$this->db->query("UPDATE " . DB_PREFIX . "tip_description SET name = '" . $this->db->escape($value['name']) . "', author = '" . $this->db->escape($value['author']) . "', body = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "' WHERE tip_id = '" . (int) $tip_id . "' AND language_id='" . $language_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . " tip_to_country where tip_id = '" . (int) $tip_id . "' ");
        $this->db->query("DELETE FROM " . DB_PREFIX . " tip_to_group where tip_id = '" . (int) $tip_id . "' ");
        $this->db->query("DELETE FROM " . DB_PREFIX . " tip_product where tip_id = '" . (int) $tip_id . "' ");
        $this->db->query("DELETE FROM " . DB_PREFIX . " tips_keyword where tip_id = '" . (int) $tip_id . "' ");

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "tip_product SET tip_id = '" . (int) $tip_id . "', product_id = '" . (int) $related_id . "'");
            }
        }

        foreach ($data['tip_link'] as $key => $value) {
            if ($key == 'country' && !empty($value)) {
                $country_string = '';
                foreach ($value as $index => $country) {
                    $country_string .= "(" . (int) $tip_id . "," . (int) $country . "),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " tip_to_country  (tip_id,country_id) values " . trim($country_string, ","));
            } elseif ($key == 'group' && !empty($value)) {
                $group_string = '';
                foreach ($value as $index => $group) {
                    $group_string .= "(" . (int) $tip_id . "," . (int) $group . "),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " tip_to_group  (tip_id,tip_group_id) VALUES " . trim($group_string, ","));
            } elseif ($key == 'keyword' && !empty($value)) {
                $keyword_string = '';
                foreach ($value as $index => $keyword) {
                    $keyword_string .= "(" . (int) $tip_id . "," . (int) $keyword . ",NOW()),";
                }
                $this->db->query("INSERT INTO " . DB_PREFIX . " tips_keyword (tip_id,keyword_id,date_added) values " . trim($keyword_string, ","));
            }
        }

        $this->url_custom->create_URL('tip', $tip_id);

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='tip', type_id='" . (int) $tip_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
        return $tip_id;
    }

    public function getTips($data = array()) {

        $sql = " SELECT DISTINCT t.tip_id, CONCAT(td1.name, ' - ',td2.name ) name, td1.author author, t.image, t.status, t.sort_order, t.date_added, t.date_modified ,u.username user_add, mu.username user_modify FROM " . DB_PREFIX . "tip t LEFT JOIN " . DB_PREFIX . "tip_description td1 ON (t.tip_id = td1.tip_id) LEFT JOIN " . DB_PREFIX . "tip_description td2 ON (t.tip_id = td2.tip_id) LEFT JOIN " . DB_PREFIX . "tip_to_country tc ON (t.tip_id = tc.tip_id) LEFT JOIN " . DB_PREFIX . "tip_to_group t2g ON (t.tip_id = t2g.tip_id) LEFT JOIN " . DB_PREFIX . "user u ON (t.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (t.last_mod_id = mu.user_id) WHERE td1.language_id='1' AND td2.language_id='2' ";

        $bLimitFlag = true;

        if (isset($data['filter_country_id'])) {
            $sql .= " AND tc.country_id = '" . (int) $data['filter_country_id'] . "'";
        }

        if (isset($data['filter_tip_id'])) {
            $sql .= " AND t.tip_id = '" . (int) $data['filter_tip_id'] . "'";
            $bLimitFlag = false;
        }

        if (isset($data['filter_status'])) {
            $sql .= " AND t.status ='" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_group'])) {
            $sql .= " AND t2g.tip_group_id ='" . (int) $data['filter_group'] . "'";
        }

        if (!empty($data['filter_author'])) {
            $sql .= " AND t.author ='" . $this->db->escape($data['filter_author']) . "'";
        }

        if (!empty($data['filter_filter'])) {
            $sql .= " AND CONCAT(t.author, ' ',td1.name,' ',td1.body, ' ',td2.name,' ',td2.body) LIKE '%" . $this->db->escape($data['filter_filter']) . "%'";
        } else if (isset($data['filter']['name']) && !empty($data['filter']['name'])) {
            $sql .= " AND CONCAT(t.author, ' ',td1.name,' ',td1.body, ' ',td2.name,' ',td2.body) LIKE '%" . $this->db->escape($data['filter']['name']) . "%'";
        }

        $sort = array(
            'td' . (int) $this->config->get('config_language_id') . '.name',
            't.status',
            'td1.author',
            't.date_added',
            't.date_modified',
            't.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY LCASE(td" . (int) $this->config->get('config_language_id') . ".name)";
        }

        if (isset($data['order']) && $data['order'] == "DESC") {
            $sql .= " DESC ";
        } else {
            $sql .= " ASC ";
        }

        if ((isset($data['start']) || isset($data['limit'])) && $bLimitFlag) {
            if ($data['start'] <= 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
//        print_r($data);
//        die($sql);
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTip($tip_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "tip t "
                . " LEFT JOIN " . DB_PREFIX . "tip_description td ON (t.tip_id = td.tip_id) "
                . " WHERE t.tip_id = '" . (int) $tip_id . "' "
                . " AND td.language_id = '" . $this->config->get('config_language_id') . "' ");

        return $query->row;
    }

    public function getTipDescriptions($tip_id) {
        $description_data = array();
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "tip_description "
                . " WHERE tip_id = '" . (int) $tip_id . "' ");

        foreach ($query->rows as $result) {
            $description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'author' => $result['author'],
                'description' => $result['body']);
        }

        return $description_data;
    }

    public function getTipLink($tip_id) {
        $result_array = array();
        $country = $this->db->query("SELECT tip_to_country.country_id as id ,cd.name "
                . " FROM tip_to_country "
                . " LEFT JOIN country c on (tip_to_country.country_id =c.country_id) "
                . " LEFT JOIN country_description cd ON(c.country_id=cd.country_id) "
                . " WHERE language_id = '" . (int) $this->config->get('config_language_id') . "' "
                . " AND c.country_id IN(tip_to_country.country_id) "
                . " AND tip_to_country.tip_id='" . $tip_id . "'");

        foreach ($country->rows as $country_link_row) {
            $result_array['country'][] = array(
                'id' => $country_link_row['id'],
                'name' => $country_link_row['name']
            );
        }


        $groups = $this->db->query("SELECT scd1.tip_group_id as id, CONCAT(scd1.name, ' - ', scd2.name) as name "
                . " FROM tip_to_group t2g LEFT JOIN tip_group tg ON(t2g.tip_group_id=tg.tip_group_id) "
                . " LEFT JOIN tip_group_description scd1 ON (tg.tip_group_id = scd1.tip_group_id) "
                . " LEFT JOIN tip_group_description scd2 ON (tg.tip_group_id = scd2.tip_group_id) "
                . " WHERE scd1.language_id = '1' AND scd2.language_id = '2' "
                . " AND t2g.tip_id = '" . $tip_id . "'");

        foreach ($groups->rows as $row) {
            $result_array['group'][] = array(
                'id' => $row['id'],
                'name' => $row['name']
            );
        }

        $products = $this->db->query("SELECT tp.product_id as id, pd.name "
                . " FROM tip_product tp "
                . " LEFT JOIN product p ON (p.product_id = tp.product_id) "
                . " LEFT JOIN product_description pd ON (p.product_id = pd.product_id) "
                . " WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' "
                . " AND tip_id = '" . $tip_id . "'");

        foreach ($products->rows as $row) {
            $result_array['product'][] = array(
                'id' => $row['id'],
                'name' => $row['name']
            );
        }

        $keywords = $this->db->query("SELECT tk.keyword_id as id, kd.name "
                . " FROM tips_keyword tk LEFT JOIN keyword_description kd ON (tk.keyword_id = kd.keyword_id) "
                . " WHERE kd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND tk.tip_id = '" . $tip_id . "'");

        foreach ($keywords->rows as $row) {
            $result_array['keyword'][] = array(
                'id' => $row['id'],
                'name' => $row['name']
            );
        }
        foreach ($result_array as $key => $rows) {
            foreach ($rows as $key2 => $row2) {
                $result_array[$key][$key2]['name'] = str_replace(array("'", '"', '`'), '', $row2['name']);
            }
        }
        return $result_array;
    }

    public function getTipImages($tip_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "brand_image WHERE tip_id = '" . (int) $tip_id . "'  ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getTotalTip($data) {
        $sql = " SELECT COUNT(DISTINCT t.tip_id) total FROM " . DB_PREFIX . "tip t LEFT JOIN " . DB_PREFIX . "tip_description td1 ON (t.tip_id = td1.tip_id) LEFT JOIN " . DB_PREFIX . "tip_description td2 ON (t.tip_id = td2.tip_id) LEFT JOIN " . DB_PREFIX . "tip_to_country tc ON (t.tip_id = tc.tip_id) LEFT JOIN " . DB_PREFIX . "tip_to_group t2g ON (t.tip_id = t2g.tip_id) WHERE td1.language_id='1' AND td2.language_id='2' ";

        if (isset($data['filter_country_id'])) {
            $sql .= " AND tc.country_id = '" . (int) $data['filter_country_id'] . "'";
        }

        if (isset($data['filter_tip_id'])) {
            $sql .= " AND t.tip_id = '" . (int) $data['filter_tip_id'] . "'";
        }

        if (isset($data['filter_status'])) {
            $sql .= " AND t.status ='" . (int) $data['filter_status'] . "'";
        }

        if (isset($data['filter_group'])) {
            $sql .= " AND t2g.tip_group_id ='" . (int) $data['filter_group'] . "'";
        }

        if (!empty($data['filter_author'])) {
            $sql .= " AND t.author ='" . $this->db->escape($data['filter_author']) . "'";
        }

        if (!empty($data['filter_filter'])) {
            $sql .= " AND CONCAT(t.author, ' ',td1.name,' ',td1.body, ' ',td2.name,' ',td2.body) LIKE '%" . $this->db->escape($data['filter_filter']) . "%'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function deleteTip($tip_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "tip_description WHERE tip_id = '" . (int) $tip_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "tip_section WHERE tip_id = '" . (int) $tip_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "tip_to_country WHERE tip_id = '" . (int) $tip_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "tip_to_group WHERE tip_id = '" . (int) $tip_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "tip_product WHERE tip_id = '" . (int) $tip_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "tip WHERE tip_id = '" . (int) $tip_id . "'");

        //$this->cache->deletekeys("catalog.tip");
        //$this->cache->deletekeys("admin.tip");
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='tip', type_id='" . (int) $tip_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function getRelatedProduct($tip_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "tip_product WHERE tip_id = '" . (int) $tip_id . "'");

        foreach ($query->rows as $result) {
            $product_related_data[] = $result['product_id'];
        }

        return $product_related_data;
    }

    public function getTipSections($tip_id) {
        $section_data = array();
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "tip_section WHERE tip_id = '" . (int) $tip_id . "' ");

        foreach ($query->rows as $result) {
            $productrow = $this->db->query("SELECT name FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $result['product_id'] . "' AND language_id = '" . (int) $result['language_id'] . "'");
            $product_name = isset($productrow->rows[0]['name']) ? $productrow->rows[0]['name'] : '';
            $section_data[$result['language_id']][] = array(
                'title' => isset($result['title']) ? $result['title'] : '',
                'description' => isset($result['description']) ? $result['description'] : (isset($result['body']) ? $result['body'] : ''),
                'product_id' => $result['product_id'],
                'image' => $result['image'],
                'sort_order' => $result['sort_order'],
                'product_name' => $product_name
            );
        }
        return $section_data;
    }

}
