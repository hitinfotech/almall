<?php

class ModelMallTipGroup extends Model {

    public function getByName($filter_data) {
        $results = array();

        if (isset($filter_data['filter_name']) && !empty($filter_data['filter_name'])) {
            $query = " SELECT CONCAT(tgd1.name,' | ',tgd2.name) name, tgd1.description, tg.* FROM tip_group tg LEFT JOIN tip_group_description tgd1 ON(tg.tip_group_id = tgd1.tip_group_id) LEFT JOIN tip_group_description tgd2 ON(tg.tip_group_id = tgd2.tip_group_id) WHERE tgd1.language_id = 1 AND tgd2.language_id = 2 AND CONCAT(tgd1.name,' ',tgd2.name) LIKE '%%%s%%' ";

            $db_query = $this->db->query(sprintf($query, $filter_data['filter_name']));

            $results = $db_query->rows;
        }


        return $results;
    }

}
