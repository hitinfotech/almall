<?php

class ModelMallShop extends Model {

    public function addShop($data) {
        $this->db->query(" INSERT INTO " . DB_PREFIX . "store "
                . "SET "
                . "`url` = '" . $this->db->escape($data['url']) . "', "
                . "`ssl` = '" . $this->db->escape($data['ssl']) . "', "
                . "`mall_id` = '" . (int) $data['mall_id'] . "', "
                . "`zone_id` = '" . (int) $data['city_id'] . "', "
                . "`phone` ='" . $this->db->escape($data['phone']) . "', "
                . "`main_store_id` = '" . (int) $data['main_store_id'] . "', "
                . "`user_id` = '" . (int) $this->user->getid() . "', "
                . "`status` = '" . (int) $data['status'] . "', "
                . "`landing` = '" . $this->db->escape($data['landing']) . "', "
                . "`sort_order` = '" . (int) $data['sort_order'] . "', "
                . "`display` = '" . $this->db->escape($data['display']) . "', "
                . "`social_facebook` = '" . $this->db->escape($data['social_fb']) . "', "
                . "`social_jeeran` = '" . $this->db->escape($data['social_jeeran']) . "', "
                . "`social_twitter` = '" . $this->db->escape($data['social_tw']) . "', "
                . "`social_website` = '" . $this->db->escape($data['website']) . "', "
                . "`date_added` = NOW() ");

        $shop_id = $this->db->getLastId();

        $this->db->query(" UPDATE store SET image = (SELECT image FROM main_store WHERE main_store_id = '" . (int) $data['main_store_id'] . "') WHERE store_id = '" . (int) $shop_id . "' ");

        $this->db->query("DELETE FROM " . DB_PREFIX . "store_description WHERE store_id = '" . (int) $shop_id . "'");
        foreach ($data['shop_description'] as $language_id => $value) {
            $this->db->query(" INSERT INTO " . DB_PREFIX . "store_description SET "
                    . "store_id = '" . (int) $shop_id . "', "
                    . "language_id = '" . (int) $language_id . "', "
                    . "name = '" . $this->db->escape($value['name']) . "', "
                    . "description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', "
                    . "open = '" . $this->db->escape($value['open']) . "', "
                    . "location = '" . $this->db->escape($value['location']) . "', "
                    . "`meta_title` = '" . $this->db->escape($value['meta_title']) . "', "
                    . "`meta_description` = '" . $this->db->escape($value['meta_description']) . "', "
                    . "`meta_keyword` = '" . $this->db->escape($value['meta_keyword']) . "' ");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "maps WHERE link_id = '" . (int) $shop_id . "' AND link_type='shop'");
        $this->db->query("INSERT INTO " . DB_PREFIX . "maps SET   link_id = '" . (int) $shop_id . "', link_type='shop', `longitude` = '" . $this->db->escape($data['longitude']) . "', `latitude` = '" . $this->db->escape($data['latitude']) . "', `zoom` = '" . (float) $data['zoom'] . "'");

        $this->db->query("DELETE FROM store_category_to_store WHERE store_id='" . (int) $shop_id . "'");
        if (isset($data['store_category'])) {
            foreach ($data['store_category'] as $store_category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "store_category_to_store SET store_id = '" . (int) $shop_id . "', store_category_id = '" . (int) $store_category_id . "'");
            }
        }

        $this->db->query("DELETE FROM brand_to_store WHERE store_id='" . (int) $shop_id . "'");
        if (isset($data['store_brands'])) {
            foreach ($data['store_brands'] as $brand_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_store SET store_id = '" . (int) $shop_id . "', brand_id = '" . (int) $brand_id . "'");
            }
        }

        $this->url_custom->create_URL('store', $shop_id);

//$this->cache->deletekeys('catalog.shop');
//$this->cache->deletekeys("admin.shop");
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='shop', type_id='" . (int) $shop_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $shop_id;
    }

    public function editShop($shop_id, $data) {
        $this->db->query(" UPDATE " . DB_PREFIX . "store  SET `url` = '" . $this->db->escape($data['url']) . "', `ssl` = '" . $this->db->escape($data['ssl']) . "',  `mall_id` = '" . (int) $data['mall_id'] . "',  `zone_id` = '" . (int) $data['city_id'] . "', phone='" . $this->db->escape($data['phone']) . "', `main_store_id` = '" . (int) $data['main_store_id'] . "', `last_mod_id` = '" . (int) $this->user->getid() . "', `status` = '" . (int) $data['status'] . "', `landing` = '" . (int) $data['landing'] . "', `sort_order` = '" . (int) $data['sort_order'] . "', `display` = '" . $this->db->escape($data['display']) . "', `social_facebook` = '" . $this->db->escape($data['social_fb']) . "', `social_jeeran` = '" . $this->db->escape($data['social_jeeran']) . "', `social_twitter` = '" . $this->db->escape($data['social_tw']) . "',  `social_website` = '" . $this->db->escape($data['website']) . "', `date_modified` = NOW() WHERE  store_id = '" . (int) $shop_id . "'");

// `meta_tag` = '" . $this->db->escape($data['meta_tag']) . "', 
// `meta_title` = '" . $this->db->escape($data['meta_title']) . "', 
// `meta_description` = '" . $this->db->escape($data['meta_description']) . "', 
// `meta_keyword` = '" . $this->db->escape($data['meta_keyword']) . "',

        $this->db->query(" UPDATE store SET image = (SELECT image FROM main_store WHERE main_store_id = '" . (int) $data['main_store_id'] . "') WHERE store_id = '" . (int) $shop_id . "' ");

        $this->db->query("DELETE FROM " . DB_PREFIX . "store_description WHERE store_id = '" . (int) $shop_id . "'");
        foreach ($data['shop_description'] as $language_id => $value) {
            $this->db->query(" INSERT INTO " . DB_PREFIX . "store_description SET 
                                store_id = '" . (int) $shop_id . "', 
                                language_id = '" . (int) $language_id . "', 
                                name = '" . $this->db->escape($value['name']) . "', 
                                description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', 
                                open = '" . $this->db->escape($value['open']) . "', 
                                location = '" . $this->db->escape($value['location']) . "',
                                `meta_title` = '" . $this->db->escape($value['meta_title']) . "', 
                                `meta_description` = '" . $this->db->escape($value['meta_description']) . "', 
                                `meta_keyword` = '" . $this->db->escape($value['meta_keyword']) . "'
                            ");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "maps WHERE link_id = '" . (int) $shop_id . "' AND link_type='shop'");
        $this->db->query("INSERT INTO " . DB_PREFIX . "maps SET   link_id = '" . (int) $shop_id . "', link_type='shop', `longitude` = '" . $this->db->escape($data['longitude']) . "', `latitude` = '" . $this->db->escape($data['latitude']) . "', `zoom` = '" . (float) $data['zoom'] . "'");

// Update Products count
        $query_products_count = $this->db->query("SELECT count(*) products_count FROM " . DB_PREFIX . "product_to_store p2s inner join " . DB_PREFIX . "product p ON (p2s.product_id = p.product_id) where p2s.store_id = '" . (int) $shop_id . "' and p.status = '1'");
        $products_count = $query_products_count->row['products_count'];
        $this->db->query(" UPDATE " . DB_PREFIX . "store SET products_count = '" . $products_count . "' WHERE store_id = '" . (int) $shop_id . "'");

        $this->db->query("DELETE FROM store_category_to_store WHERE store_id='" . (int) $shop_id . "'");
        if (isset($data['store_category'])) {
            foreach ($data['store_category'] as $store_category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "store_category_to_store SET store_id = '" . (int) $shop_id . "', store_category_id = '" . (int) $store_category_id . "'");
            }
        }

        $this->url_custom->create_URL('store', $shop_id);

        $this->db->query("DELETE FROM brand_to_store WHERE store_id='" . (int) $shop_id . "'");
        if (isset($data['store_brands'])) {
            foreach ($data['store_brands'] as $brand_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_store SET store_id = '" . (int) $shop_id . "', brand_id = '" . (int) $brand_id . "'");
            }
        }

//$this->cache->deletekeys('catalog.shop');
//$this->cache->deletekeys("admin.shop");
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='shop', type_id='" . (int) $shop_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function deleteShop($shop_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "store WHERE store_id = '" . (int) $shop_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "store_description WHERE store_id = '" . (int) $shop_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "maps WHERE link_id = '" . (int) $shop_id . "' AND link_type='shop'");

//$this->cache->deletekeys('catalog.shop');
//$this->cache->deletekeys("admin.shop");
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='shop', type_id='" . (int) $shop_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function getShop($shop_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "store s LEFT JOIN " . DB_PREFIX . "store_description sd ON(s.store_id = sd.store_id) WHERE sd.language_id = '" . $this->config->get('config_language_id') . "' AND s.store_id = '" . (int) $shop_id . "'");

        return $query->row;
    }

    public function getGoogle($shop_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "maps WHERE link_id = '" . (int) $shop_id . "' AND link_type= 'shop'");

        return $query->row;
    }

    public function getShopDescriptions($shop_id) {
        $shop_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store_description WHERE store_id = '" . (int) $shop_id . "'");

        foreach ($query->rows as $result) {
            $shop_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'open' => $result['open'],
                'location' => $result['location'],
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword']
            );
        }
        return $shop_description_data;
    }

    public function getShops($data) {

        $cache = 'shop.getshops.' . md5(http_build_query($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $sql = " SELECT s.store_id, CONCAT(msd1.name, ' - ', msd2.name) as main_store, CONCAT(sd1.name, ' - ',sd2.name) AS name,  s.status, md.name as mall_name, s.mall_id, s.zone_id, z.country_id, zd.name city, s.products_count, s.sort_order, u.username user_add, mu.username user_modify "
                    . " FROM " . DB_PREFIX . "store s "
                    . " LEFT JOIN " . DB_PREFIX . "store_description sd1 ON (s.store_id=sd1.store_id) "
                    . " LEFT JOIN " . DB_PREFIX . "store_description sd2 ON (s.store_id=sd2.store_id) "
                    . " LEFT JOIN " . DB_PREFIX . "mall_description md ON(s.mall_id = md.mall_id AND md.language_id='" . (int) $this->config->get('config_language_id') . "') "
                    . " LEFT JOIN " . DB_PREFIX . "main_store mg ON(mg.main_store_id=s.main_store_id) "
                    . " LEFT JOIN " . DB_PREFIX . "main_store_description msd1 ON(msd1.main_store_id = mg.main_store_id) "
                    . " LEFT JOIN " . DB_PREFIX . "main_store_description msd2 ON(msd2.main_store_id = mg.main_store_id) "
                    . " LEFT JOIN " . DB_PREFIX . "zone z ON( s.zone_id=z.zone_id ) "
                    . " LEFT JOIN " . DB_PREFIX . "zone_description zd ON(s.zone_id = zd.zone_id) "
                    //. " LEFT JOIN " . DB_PREFIX . "store_category_to_store sc2s ON (s.store_id=sc2s.store_id) "
                    . " LEFT JOIN " . DB_PREFIX . "user u ON (s.user_id = u.user_id) "
                    . " LEFT JOIN " . DB_PREFIX . "user mu ON (s.last_mod_id = mu.user_id) ";


            $sql .= " WHERE s.is_designer = '0' AND sd1.language_id = '1' AND sd2.language_id = '2' AND zd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND msd1.language_id = '1' AND msd2.language_id = '2' ";

            if (isset($data['filter_shop_id']) && !empty($data['filter_shop_id'])) {
                $sql .= " AND s.store_id = '" . $data['filter_shop_id'] . "' ";
            }

            if (isset($data['filter_filter']) && !empty($data['filter_filter'])) {
                $sql.= " AND CONCAT(sd1.name,' ',' ',sd2.name) LIKE '%" . $this->db->escape($data['filter_filter']) . "%' ";
            }
            if (isset($data['filter_name']) && !empty($data['filter_name'])) {
                $sql.= " AND CONCAT(sd1.name,' ',' ',sd2.name) LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
            }

            if (isset($data['filter_city_id']) && !empty($data['filter_city_id'])) {
                $sql.= " AND s.zone_id = '" . (int) $data['filter_city_id'] . "' ";
            }

            if (isset($data['filter_country_id']) && !empty($data['filter_country_id'])) {
                $sql.= " AND z.country_id = '" . (int) $data['filter_country_id'] . "' ";
            }

            if (isset($data['filter_mall_id']) && !empty($data['filter_mall_id'])) {
                $sql.= " AND s.mall_id = '" . (int) $data['filter_mall_id'] . "' ";
            }

            if (isset($data['filter_main_store_id']) && !empty($data['filter_main_store_id'])) {
                $sql.= " AND s.main_store_id = '" . (int) $data['filter_main_store_id'] . "' ";
            }

            if (isset($data['filter_store_category_id']) && !empty($data['filter_store_category_id'])) {
                $sql.= " AND sc2s.store_category_id = '" . (int) $data['filter_store_category_id'] . "' ";
            }


            $sort_data = array(
                'sd1.name',
                's.products_count',
                's.sort_order',
                'zd.name',
                'md.name',
                'msd.main_store'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY s.sort_order ";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        }

        return $result;
    }

    public function getTotalShops($data) {

        $sql = " SELECT COUNT(s.store_id) AS total FROM " . DB_PREFIX . "store s "
                . " LEFT JOIN " . DB_PREFIX . "store_description sd1 ON (s.store_id=sd1.store_id) "
                . " LEFT JOIN " . DB_PREFIX . "store_description sd2 ON (s.store_id=sd2.store_id) "
                . " LEFT JOIN " . DB_PREFIX . "mall_description md ON(s.mall_id = md.mall_id AND md.language_id='" . (int) $this->config->get('config_language_id') . "') "
                . " LEFT JOIN " . DB_PREFIX . "main_store mg ON(mg.main_store_id=s.main_store_id) "
                . " LEFT JOIN " . DB_PREFIX . "main_store_description msd1 ON(msd1.main_store_id = mg.main_store_id) "
                . " LEFT JOIN " . DB_PREFIX . "main_store_description msd2 ON(msd2.main_store_id = mg.main_store_id) "
                . " LEFT JOIN " . DB_PREFIX . "zone z ON( s.zone_id=z.zone_id ) "
                . " LEFT JOIN " . DB_PREFIX . "zone_description zd ON(s.zone_id = zd.zone_id) "
                //. " LEFT JOIN " . DB_PREFIX . "store_category_to_store sc2s ON (s.store_id=sc2s.store_id) "
                . " LEFT JOIN " . DB_PREFIX . "user u ON (s.user_id = u.user_id) "
                . " LEFT JOIN " . DB_PREFIX . "user mu ON (s.last_mod_id = mu.user_id) ";


        $sql .= " WHERE s.is_designer = '0' AND sd1.language_id = '1' AND sd2.language_id = '2' AND zd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND msd1.language_id = '1' AND msd2.language_id = '2' ";

        if (isset($data['filter_shop_id']) && !empty($data['filter_shop_id'])) {
            $sql .= " AND s.store_id = '" . $data['filter_shop_id'] . "' ";
        }

        if (isset($data['filter_filter']) && !empty($data['filter_filter'])) {
            $sql.= " AND CONCAT(sd1.name,' ',' ',sd2.name) LIKE '%" . $this->db->escape($data['filter_filter']) . "%' ";
        }
        if (isset($data['filter_name']) && !empty($data['filter_name'])) {
            $sql.= " AND CONCAT(sd1.name,' ',' ',sd2.name) LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
        }

        if (isset($data['filter_city_id']) && !empty($data['filter_city_id'])) {
            $sql.= " AND s.zone_id = '" . (int) $data['filter_city_id'] . "' ";
        }

        if (isset($data['filter_country_id']) && !empty($data['filter_country_id'])) {
            $sql.= " AND z.country_id = '" . (int) $data['filter_country_id'] . "' ";
        }

        if (isset($data['filter_mall_id']) && !empty($data['filter_mall_id'])) {
            $sql.= " AND s.mall_id = '" . (int) $data['filter_mall_id'] . "' ";
        }

        if (isset($data['filter_main_store_id']) && !empty($data['filter_main_store_id'])) {
            $sql.= " AND s.main_store_id = '" . (int) $data['filter_main_store_id'] . "' ";
        }

        if (isset($data['filter_store_category_id']) && !empty($data['filter_store_category_id'])) {
            $sql.= " AND sc2s.store_category_id = '" . (int) $data['filter_store_category_id'] . "' ";
        }


        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getshopsByName($data) {
        $result = array();
        if ($data['filter_name']) {
            $sql = " SELECT s.store_id , CONCAT(sd1.name, ' - ', sd2.name) as name,  (select md.name from mall_description md where md.mall_id=s.mall_id and md.language_id = 1 ) as mall_name FROM " . DB_PREFIX . "store s LEFT JOIN " . DB_PREFIX . "store_description sd1 ON (s.store_id = sd1.store_id) LEFT JOIN " . DB_PREFIX . "store_description sd2 ON (s.store_id = sd2.store_id) WHERE CONCAT(sd1.name,' ',sd2.name) LIKE '%" . $this->db->escape($data['filter_name']) . "%' AND sd1.language_id = '1' AND sd2.language_id = '2' AND s.status = 1 ";
            $query = $this->db->query($sql);
            foreach ($query->rows as $row) {
                $result[] = array('store_id' => $row['store_id'], 'name' => $row['name'] . '( ' . $row['mall_name'] . ' )');
            }
        }

        return $result;
    }

    public function getStoresByMainStore($main_store_id) {
        $query = $this->db->query(" SELECT s.store_id, CONCAT(sd1.name, ' - ', sd2.name) as name, md.name as mall_name  
                                    FROM " . DB_PREFIX . "store s 
                                    LEFT JOIN " . DB_PREFIX . "store_description sd1 ON (s.store_id=sd1.store_id) 
                                    LEFT JOIN " . DB_PREFIX . "store_description sd2 ON (s.store_id=sd2.store_id) 
                                    LEFT JOIN " . DB_PREFIX . "mall_description md ON (s.mall_id = md.mall_id) 
                                    WHERE  is_designer=0 AND
                                        s.main_store_id = '" . (int) $main_store_id . "'  
                                    AND sd1.language_id = '1'
                                    AND sd2.language_id = '2' 
                                    AND md.language_id='1'
                                 ");

        return $query->rows;
    }

}
