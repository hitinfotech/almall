<?php

class ModelMallMall extends Model {

    public function addMall($data) {
        $this->db->query(" INSERT INTO " . DB_PREFIX . "mall SET `city_id` = '" . (int) $data['city_id'] . "', `phone`= '" . $this->db->escape($data['phone']) . "', `fax` = '" . $this->db->escape($data['fax']) . "', `website` = '" . $this->db->escape($data['website']) . "', `social_jeeran` = '" . $this->db->escape($data['social_jeeran']) . "', `social_fb` = '" . $this->db->escape($data['social_fb']) . "', `social_tw` = '" . $this->db->escape($data['social_tw']) . "', `status` = '" . (int) $data['status'] . "', `user_id` = '" . (int) $this->user->getid() . "', `sort_order` = '" . (int) $data['sort_order'] . "', `date_added` = NOW(), `date_modified` = NOW() ");

        $mall_id = $this->db->getLastId();

        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('mall', 'image', $mall_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('news_offers_malls') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE mall SET `image` = '" . $this->db->escape($image) . "' WHERE mall_id='" . (int) $mall_id . "' ");
        }

        $this->url_custom->create_URL('mall', $mall_id);

        foreach ($data['mall_description'] as $language_id => $value) {
            $this->db->query(" INSERT INTO " . DB_PREFIX . "mall_description SET mall_id = '" . (int) $mall_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', location = '" . $this->db->escape($value['location']) . "', open = '" . $this->db->escape($value['open']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        $this->db->query(" DELETE FROM maps WHERE link_type='mall' AND link_id='" . (int) $mall_id . "' ");
        $this->db->query(" INSERT INTO maps SET   link_type='mall' , link_id='" . (int) $mall_id . "', latitude='" . (float) $data['latitude'] . "', longitude='" . (float) $data['longitude'] . "', zoom='" . (int) $data['zoom'] . "' ");

        $this->db->query(" UPDATE mall m SET m.shop_count =  (SELECT COUNT(DISTINCT s.store_id) FROM store s WHERE s.status = '1' AND s.mall_id='" . (int) $mall_id . "') WHERE m.mall_id = '" . (int) $mall_id . "' ");

        //$this->cache->deletekeys('catalog.mall');
        //$this->cache->deletekeys("admin.mall");

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='mall', type_id='" . (int) $mall_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
        return $mall_id;
    }

    public function editMall($mall_id, $data) {
        $this->db->query(" UPDATE " . DB_PREFIX . "mall SET `city_id` = '" . (int) $data['city_id'] . "', `phone` = '" . $this->db->escape($data['phone']) . "', `fax` = '" . $this->db->escape($data['fax']) . "', `website` = '" . $this->db->escape($data['website']) . "', `social_jeeran` = '" . $this->db->escape($data['social_jeeran']) . "', `social_fb` = '" . $this->db->escape($data['social_fb']) . "', `social_tw` = '" . $this->db->escape($data['social_tw']) . "', `status` = '" . (int) $data['status'] . "', last_mod_id='" . (int) $this->user->getid() . "', `sort_order` = '" . (int) $data['sort_order'] . "', `date_modified` = NOW() WHERE mall_id = '" . (int) $mall_id . "'");

        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('mall', 'image', $mall_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('news_offers_malls') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE mall SET `image` = '" . $this->db->escape($image) . "' WHERE mall_id='" . (int) $mall_id . "' ");
        }

        $this->url_custom->create_URL('mall', $mall_id);

        $this->db->query("DELETE FROM " . DB_PREFIX . "mall_description WHERE mall_id = '" . (int) $mall_id . "'");
        foreach ($data['mall_description'] as $language_id => $value) {
            $this->db->query(" INSERT INTO " . DB_PREFIX . "mall_description SET mall_id = '" . (int) $mall_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', location = '" . $this->db->escape($value['location']) . "', open = '" . $this->db->escape($value['open']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }
        $this->db->query(" DELETE FROM maps WHERE link_type='mall' AND link_id='" . (int) $mall_id . "' ");
        $this->db->query(" INSERT INTO maps SET   link_type='mall' , link_id='" . (int) $mall_id . "', latitude='" . (float) $data['latitude'] . "', longitude='" . (float) $data['longitude'] . "', zoom='" . (int) $data['zoom'] . "' ");

        $this->db->query(" UPDATE mall m SET m.shop_count =  (SELECT COUNT(DISTINCT s.store_id) FROM store s WHERE s.status = '1' AND s.mall_id='" . (int) $mall_id . "') WHERE m.mall_id = '" . (int) $mall_id . "' ");


        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='mall', type_id='" . (int) $mall_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function deleteMall($mall_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "mall WHERE mall_id = '" . (int) $mall_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "mall_description WHERE mall_id = '" . (int) $mall_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "maps WHERE link_type='mall' AND link_id = '" . (int) $mall_id . "'");
        $this->db->query("UPDATE " . DB_PREFIX . "store SET mall_id WHERE mall_id = '" . (int) $mall_id . "'");


        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='mall', type_id='" . (int) $mall_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function getMall($mall_id) {

        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "mall m LEFT JOIN " . DB_PREFIX . "mall_description md ON(m.mall_id = md.mall_id)  WHERE md.language_id = '" . $this->config->get('config_language_id') . "' AND m.mall_id = '" . (int) $mall_id . "'");

        return $query->row;
    }

    public function getMallDescriptions($mall_id) {
        $mall_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "mall_description WHERE mall_id = '" . (int) $mall_id . "'");

        foreach ($query->rows as $result) {
            $mall_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'location' => $result['location'],
                'open' => $result['open'],
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
                'description' => $result['description']
            );
        }
        return $mall_description_data;
    }

    public function getMap($mall_id) {
        $query = $this->db->query(" SELECT * FROM maps WHERE link_type='mall' AND link_id='" . (int) $mall_id . "' ");
        return $query->row;
    }

    public function getMalls($data) {

        $sql = " SELECT m.mall_id, CONCAT(md.name, ' - ', md2.name) AS name, z.zone_id, z.country_id, zd.name city, m.shop_count, m.sort_order, m.date_added, m.date_modified, u.username user_add, mu.username user_modify "
                . " FROM " . DB_PREFIX . "mall m "
                . " LEFT JOIN " . DB_PREFIX . "mall_description md ON(m.mall_id=md.mall_id)  "
                . " LEFT JOIN " . DB_PREFIX . "mall_description md2 ON(m.mall_id=md2.mall_id) "
                . " LEFT JOIN zone z ON( m.city_id=z.zone_id ) "
                . " LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) "
                . " LEFT JOIN country c ON(z.country_id = c.country_id ) LEFT JOIN " . DB_PREFIX . "user u ON (m.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (m.last_mod_id = mu.user_id) ";

        //$sql .= " WHERE md.language_id = '1' AND md2.language_id = '2' AND zd.language_id = '" . (int) $this->config->get('config_language_id') . "' ";
        $sql .= " WHERE md.language_id = '1' AND md2.language_id = '2' AND zd.language_id = '2' ";

        if (isset($data['filter_mall_id']) && !empty($data['filter_mall_id'])) {
            $sql .= " AND m.mall_id = '" . $data['filter_mall_id'] . "' ";
        }

        if (isset($data['filter_filter']) && !empty($data['filter_filter'])) {
            $sql.= " AND CONCAT(md.name,' ',md.description,' ',md.location,' ',md.open,' ',md.meta_title,' ',md2.name,' ',md2.description,' ',md2.location,' ',md2.open,' ',md2.meta_title) LIKE '%" . $this->db->escape($data['filter_filter']) . "%' ";
        }

        if (isset($data['filter_city_id']) && !empty($data['filter_city_id'])) {
            $sql.= " AND m.city_id = '" . (int) $data['filter_city_id'] . "' ";
        }

        if (isset($data['filter_country_id']) && !empty($data['filter_country_id'])) {
            $sql.= " AND c.country_id = '" . (int) $data['filter_country_id'] . "' ";
        }


        $sort_data = array(
            'md.name',
            'm.shop_count',
            'm.sort_order',
            'zd.name'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY m.sort_order ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        // echo "<pre>"; die($sql);

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalMall($data) {
        $sql = " SELECT COUNT(*) AS total FROM " . DB_PREFIX . "mall m LEFT JOIN " . DB_PREFIX . "mall_description md ON(m.mall_id=md.mall_id) LEFT JOIN zone z ON( m.city_id=z.zone_id ) LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) LEFT JOIN country c ON(z.country_id = c.country_id ) ";

        $sql .= " WHERE md.language_id = '" . (int) $this->config->get('config_language_id') . "' AND zd.language_id = '" . (int) $this->config->get('config_language_id') . "' ";

        if (isset($data['filter_mall_id']) && !empty($data['filter_mall_id'])) {
            $sql .= " AND m.mall_id = '" . $data['filter_mall_id'] . "' ";
        } else {
            if (isset($data['filter_filter']) && !empty($data['filter_filter'])) {
                $sql.= " AND CONCAT(md.name,' ',md.description,' ',md.location,' ',md.open,' ',md.meta_title) LIKE '%" . $this->db->escape($data['filter_filter']) . "%' ";
            }

            if (isset($data['filter_city_id']) && !empty($data['filter_city_id'])) {
                $sql.= " AND m.city_id = '" . (int) $data['filter_city_id'] . "' ";
            }

            if (isset($data['filter_country_id']) && !empty($data['filter_country_id'])) {
                $sql.= " AND c.country_id = '" . (int) $data['filter_country_id'] . "' ";
            }
        }

        $sort_data = array(
            'md.name',
            'm.shop_count',
            'm.sort_order',
            'zd.name'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY m.sort_order ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getMallsByName($data) {
        if ($data['filter_name']) {
            $query = $this->db->query("SELECT m.mall_id, md.name "
                    . "FROM " . DB_PREFIX . "mall m "
                    . "LEFT JOIN mall_description md ON(m.mall_id=md.mall_id) "
                    . "WHERE md.language_id = '" . (int) $this->config->get('config_language_id') . "' "
                    . "AND md.name LIKE '%" . $this->db->escape($data['filter_name']) . "%' "
                    . "AND m.status = 1");
            return $query->rows;
        } else {
            return array();
        }
    }

    public function getZones($country_id = 0) {
        $query = $this->db->query("SELECT DISTINCT z.zone_id, zd.name FROM " . DB_PREFIX . "mall m LEFT JOIN zone z ON(m.city_id=z.zone_id) LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE zd.language_id='2' AND z.country_id = '" . (int) $country_id . "' ORDER BY zd.name");

        return $query->rows;
    }

}
