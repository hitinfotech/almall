<?php

class ModelMallBrand extends Model {

    private $errors;

    public function addBrand($data) {

        $this->db->query(" INSERT INTO " . DB_PREFIX . "brand SET `link`='" . $this->db->escape($data['website']) . "', `social_tw` = '" . $this->db->escape($data['social_tw']) . "', `social_fb` = '" . $this->db->escape($data['social_fb']) . "', `status` = '" . (int) $data['status'] . "', `fbfeeds` = '" . (int) $data['fbfeeds'] . "', `is_algolia` = '" . (int) $data['is_algolia'] . "' , `show_api` = '" . ((isset($data['show_api'])&& $data['show_api'] == 'yes') ? 'yes' : 'no') . "' , `sort_order` = '" . (int) $data['sort_order'] . "', `date_added`=NOW(), `date_modified`=NOW(), user_id = '" . $this->user->getid() . "' ");
        $brand_id = $this->db->getLastId();

        foreach ($data['brand_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "brand_description SET brand_id = '" . (int) $brand_id . "', language_id = '" . (int) $language_id . "', `name` = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('brand', 'image', $brand_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('shops_brands_designer') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE brand SET `image` = '" . $this->db->escape($image) . "' WHERE brand_id='" . (int) $brand_id . "' ");
        }

        $this->url_custom->create_URL('brand', $brand_id);

        if ($data['brand_link'] = array_merge(array('country' => array(), 'category' => array(), 'shop' => array()), isset($data['brand_link']) ? $data['brand_link'] : array())) {
//            die(print_r($data['brand_link']));
            foreach ($data['brand_link'] as $link_type => $link) {
                if ($link_type == 'shop') {
                    $link_type = 'store';
                    $link_type_id = $link_type . '_id';
                } elseif ($link_type == 'category') {
                    $link_type = 'category';
                    $link_type_id = 'store_' . $link_type . '_id';
                } else {
                    $link_type = $link_type;
                    $link_type_id = $link_type . '_id';
                }

                $this->db->query("DELETE FROM " . DB_PREFIX . "brand_to_{$link_type} WHERE brand_id='" . (int) $brand_id . "'");

                foreach ($link as $link_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_{$link_type} SET brand_id = '" . (int) $brand_id . "', {$link_type_id} = '" . $this->db->escape($link_id) . "'");
                }
            }
        }

        $this->db->query(" INSERT IGNORE INTO algolia SET `type`='brand', type_id='" . (int) $brand_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='brand', type_id='" . (int) $brand_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $brand_id;
    }

    public function editBrand($brand_id, $data) {

        // echo " UPDATE " . DB_PREFIX . "brand SET `start_date`='" . date('Y-m-d H:i:s', strtotime($data['start_date'])) . "', `end_date` = '" . date('Y-m-d H:i:s', strtotime($data['end_date'])) . "', `user_id` = '" . (int) $data['user_id'] . "', `sort_order` = '" . (int) $data['sort_order'] . "', `date_modified`=NOW() WHERE brand_id ='" . (int) $brand_id . "' <br>";
        $this->db->query(" UPDATE " . DB_PREFIX . "brand SET `sort_order` = '" . (int) $data['sort_order'] . "', `status` = '" . (intval($data['status']) ? 1 : 0) . "',`link` = '" . $this->db->escape($data['website']) . "', `is_algolia` = '" . (intval($data['is_algolia']) ? 1 : 0) . "', `show_api` = '" . ((isset($data['show_api'])&& $data['show_api'] == 'yes') ? 'yes' : 'no') . "', `fbfeeds` = '" . (int) $data['fbfeeds'] . "', `social_tw` = '" . $this->db->escape($data['social_tw']) . "', `social_fb` = '" . $this->db->escape($data['social_fb']) . "', `date_modified`=NOW(), last_mod_id = '" . $this->user->getid() . "' WHERE brand_id ='" . (int) $brand_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "brand_description WHERE brand_id='" . (int) $brand_id . "'");
        foreach ($data['brand_description'] as $language_id => $value) {
            // echo "INSERT INTO " . DB_PREFIX . "brand_description SET brand_id = '" . (int) $brand_id . "', language_id = '" . (int) $language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', body_text = '" . $this->db->escape($value['body_text']) . "' <br>";
            $this->db->query("INSERT INTO " . DB_PREFIX . "brand_description SET brand_id = '" . (int) $brand_id . "', language_id = '" . (int) $language_id . "', `name` = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('brand', 'image', $brand_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('shops_brands_designer') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE brand SET `image` = '" . $this->db->escape($image) . "' WHERE brand_id='" . (int) $brand_id . "' ");
        }

        $this->url_custom->create_URL('brand', $brand_id);

        if ($data['brand_link'] = array_merge(array('country' => array(), 'category' => array(), 'shop' => array()), isset($data['brand_link']) ? $data['brand_link'] : array())) {
//            die(print_r($data['brand_link']));
            foreach ($data['brand_link'] as $link_type => $link) {
                if ($link_type == 'shop') {
                    $link_type = 'store';
                    $link_type_id = $link_type . '_id';
                } elseif ($link_type == 'category') {
                    $link_type = 'category';
                    $link_type_id = 'store_' . $link_type . '_id';
                } else {
                    $link_type = $link_type;
                    $link_type_id = $link_type . '_id';
                }

                $this->db->query("DELETE FROM " . DB_PREFIX . "brand_to_{$link_type} WHERE brand_id='" . (int) $brand_id . "'");

                foreach ($link as $link_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_{$link_type} SET brand_id = '" . (int) $brand_id . "', {$link_type_id} = '" . $this->db->escape($link_id) . "'");
                }
            }
        }
        $this->db->query(" INSERT IGNORE INTO algolia SET `type`='brand', type_id='" . (int) $brand_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='brand', type_id='" . (int) $brand_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $brand_id;
    }

    public function deleteBrand($brand_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "brand_to_country WHERE brand_id = '" . (int) $brand_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "brand_to_category WHERE brand_id = '" . (int) $brand_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "brand_description WHERE brand_id = '" . (int) $brand_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "brand WHERE brand_id = '" . (int) $brand_id . "'");

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='brand', type_id='" . (int) $brand_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function getBrands($data = array()) {

        $sql = " SELECT DISTINCT b.brand_id, b.image, CONCAT(bd1.name,' - ',bd2.name) name,  b.status, b.sort_order, b.date_added, b.date_modified ,u.username user_add, mu.username user_modify FROM " . DB_PREFIX . "brand b  LEFT JOIN " . DB_PREFIX . "brand_description bd1 ON (b.brand_id = bd1.brand_id) LEFT JOIN " . DB_PREFIX . "brand_description bd2 ON (b.brand_id = bd2.brand_id) LEFT JOIN " . DB_PREFIX . "product p ON(b.brand_id=p.brand_id) LEFT JOIN " . DB_PREFIX . "product_to_category pc ON(pc.product_id=p.product_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_product cp2p ON(p.product_id=cp2p.product_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer cp2c ON(cp2p.customer_id=cp2c.customer_id) LEFT JOIN " . DB_PREFIX . "user u ON (b.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (b.last_mod_id = mu.user_id) WHERE bd1.language_id = '1' AND bd2.language_id = '2' ";

        if (!empty($data['filter_brand_id'])) {
            $sql .= " AND b.brand_id = '" . (int) $data['filter_brand_id'] . "' ";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND CONCAT(bd1.name,' ',bd2.name)  LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
        }

        if (!empty($data['filter_filter'])) {
            $sql .= " AND CONCAT(bd1.name,' ',bd2.name)  LIKE '%" . $this->db->escape($data['filter_filter']) . "%' ";
        }

        if (isset($data['filter_status'])) {
            $sql .= " AND b.status = '" . (int) $data['filter_status'] . "' ";
        }

        if (!empty($data['filter_shopable'])) {
            $sql .= " AND p.stock_status_id = '" . (int) $data['filter_shopable'] . "' ";
        }

        if (isset($data['filter_country']) AND $data['filter_country']!=0) {
            $sql .= " AND cp2c.available_country IN(0, '" . (int) $data['filter_country'] . "') ";
        }

        if (!empty($data['filter_category'])) {
            $sql .= " AND pc.category_id = '" . (int) $data['filter_category'] . "' ";
        }

        $sort_data = array(
            'bd1.name',
            'bd2.name',
            'b.date_added',
            'b.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY b.brand_id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] <= 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalBrand($data) {
        $sql = " SELECT COUNT(DISTINCT b.brand_id) AS total FROM " . DB_PREFIX . "brand b LEFT JOIN " . DB_PREFIX . "brand_description bd1 ON (b.brand_id = bd1.brand_id) LEFT JOIN " . DB_PREFIX . "brand_description bd2 ON (b.brand_id = bd2.brand_id) LEFT JOIN " . DB_PREFIX . "product p ON(b.brand_id=p.brand_id) LEFT JOIN " . DB_PREFIX . "product_to_category pc ON(pc.product_id=p.product_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_product cp2p ON(p.product_id=cp2p.product_id) INNER JOIN " . DB_PREFIX . "customerpartner_to_customer cp2c ON(cp2p.customer_id=cp2c.customer_id) LEFT JOIN " . DB_PREFIX . "user u ON (b.user_id = u.user_id) LEFT JOIN " . DB_PREFIX . "user mu ON (b.last_mod_id = mu.user_id)  WHERE bd1.language_id = '1' AND bd2.language_id = '2' ";

        if (isset($data['filter_brand_id'])) {
            $sql .= " AND b.brand_id = '" . $data['filter_brand_id'] . "' ";
        } else {
            if (isset($data['filter_filter'])) {
                $sql .= " AND CONCAT(bd1.name,' ',bd2.name)  LIKE '%" . $this->db->escape($data['filter_filter']) . "%' ";
            }

            if (isset($data['filter_status'])) {
                $sql .= " AND b.status = '" . (int) $data['filter_status'] . "' ";
            }

            if (!empty($data['filter_shopable'])) {
                $sql .= " AND p.stock_status_id = '" . (int) $data['filter_shopable'] . "' ";
            }

            if (isset($data['filter_category'])) {
                $sql .= " AND pc.category_id = '" . (int) $data['filter_category'] . "' ";
            }

            if (isset($data['filter_country'])) {
                $sql .= " AND cp2c.available_country IN(0, '" . (int) $data['filter_country'] . "') ";
            }
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getAutoCompleteBrandsByName($data = array()) {
        $sql = " SELECT b.brand_id , CONCAT(bd1.name,' - ',bd2.name) name FROM brand b LEFT JOIN brand_description bd1 ON(b.brand_id=bd1.brand_id) LEFT JOIN brand_description bd2 ON(b.brand_id=bd2.brand_id) WHERE bd1.language_id='1' AND bd2.language_id='2' ";
        if (!empty($data['filter_name'])) {
            $sql .= " AND CONCAT(bd1.name,' ',bd2.name) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        $sql .= " LIMIT 0,10 ";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getBrandsByName($data = array()) {

        $sOfferLinkTable = '';
        $sWhere = '';
        $sGroupBy = '';

        if (isset($data['filter_keyword']) && !empty($data['filter_keyword'])) {
            $sWhere = " AND CONCAT(nd1.name,' ',nd2.name) LIKE '%" . $this->db->escape($data['filter_keyword']) . "%'";
        }

        $sql = " SELECT
                        n.brand_id, CONCAT(nd1.name, ' - ', nd2.name) AS name
                 FROM
                        " . DB_PREFIX . "brand n
                        LEFT JOIN " . DB_PREFIX . "brand_description nd1 ON (n.brand_id = nd1.brand_id)
                        LEFT JOIN " . DB_PREFIX . "brand_description nd2 ON (n.brand_id = nd2.brand_id)
                 WHERE
                        nd1.language_id = '1'
                 AND    nd2.language_id = '2'
                 AND    n.status = 1
                        {$sWhere}";
        // echo "<pre>"; die($sql);
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getBrandsByStore($filter_store_id) {
        if ($filter_store_id) {
            $query = $this->db->query(" SELECT mg.`brand_id`, CONCAT (mgd.`name`,' | ',mgd2.`name`) as name
                                        FROM " . DB_PREFIX . "brand mg
                                        LEFT JOIN " . DB_PREFIX . "brand_description mgd ON(mg.brand_id = mgd.brand_id)
                                        LEFT JOIN " . DB_PREFIX . "brand_description mgd2 ON(mg.brand_id = mgd2.brand_id)
                                        LEFT JOIN " . DB_PREFIX . "brand_to_store mgd3 ON(mg.brand_id = mgd3.brand_id)
                                        WHERE
                                            mgd.language_id = '1'
                                        AND mgd2.language_id = '2'
                                        AND mgd3.store_id = '" . $filter_store_id . "' ");
            return $query->rows;
        } else {
            return array();
        }
    }

    public function getBrand($brand_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "brand n LEFT JOIN " . DB_PREFIX . "brand_description nd ON (n.brand_id = nd.brand_id)  WHERE n.brand_id = '" . (int) $brand_id . "' AND nd.language_id = '" . $this->config->get('config_language_id') . "' ");

        return $query->row;
    }

    public function getBrandDescriptions($brand_id) {
        $brand_description_data = array();
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "brand_description WHERE brand_id = '" . (int) $brand_id . "' ");

        foreach ($query->rows as $result) {
            $brand_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
                'description' => $result['description']
            );
        }

        return $brand_description_data;
    }

    public function getBrandLink($brand_id) {
        $aOutPut = array();

        foreach (array('country', 'category', 'store') as $link_type) {
            $links = $this->db->query(" SELECT * FROM " . DB_PREFIX . "brand_to_{$link_type} WHERE brand_id = '" . (int) $brand_id . "' ");

            switch ($link_type) {
                case 'country':
                    foreach ($links->rows as $for_link_row) {
                        $query1 = $this->db->query(" SELECT * FROM " . DB_PREFIX . "country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE language_id = '" . (int) $this->config->get('config_language_id') . "' AND c.country_id = '" . (int) $for_link_row['country_id'] . "' ");
                        if (isset($query1->row) && !empty($query1->row)) {

                            $aOutPut[$link_type][] = array('id' => $query1->row['country_id'], 'name' => $query1->row['name']);
                        }
                    }
                    break;
                case 'category':
                    foreach ($links->rows as $for_link_row) {
                        $query5 = $this->db->query(" SELECT scd1.store_category_id, CONCAT(scd1.name, ' - ', scd2.name) as name
                                                     FROM " . DB_PREFIX . "store_category_description scd1
                                                     INNER JOIN  " . DB_PREFIX . "store_category_description scd2 ON (scd1.store_category_id=scd2.store_category_id)
                                                     WHERE scd1.store_category_id = '" . (int) $for_link_row['store_category_id'] . "'
                                                     AND scd1.language_id = '1'
                                                     AND scd2.language_id = '2'
                                                     ");
                        if (isset($query5->row) && !empty($query5->row)) {
                            $aOutPut[$link_type][] = array('id' => $query5->row['store_category_id'], 'name' => $query5->row['name']);
                        }
                    }
                    break;
                case 'store':
                    foreach ($links->rows as $for_link_row) {
                        $query4 = $this->db->query(" SELECT s.store_id , CONCAT(sd1.name, ' - ',sd2.name, ' ( ',md.name, ' ) ') as name, md.name as mall_name
                                                     FROM " . DB_PREFIX . "store s
                                                     LEFT JOIN " . DB_PREFIX . "store_description sd1 ON (s.store_id = sd1.store_id)
                                                     LEFT JOIN " . DB_PREFIX . "store_description sd2 ON (s.store_id = sd2.store_id)
                                                     LEFT JOIN " . DB_PREFIX . "mall_description md ON (s.mall_id = md.mall_id)
                                                     WHERE
                                                     sd1.language_id = '1'
                                                     AND sd2.language_id = '2'
                                                     AND md.language_id = '1'
                                                     AND s.store_id = '" . (int) $for_link_row['store_id'] . "' ");
                        if (isset($query4->row) && !empty($query4->row)) {
                            $aOutPut['shop'][] = array('id' => $query4->row['store_id'], 'name' => $query4->row['name']);
                        }
                    }
                    break;
                default:

                    break;
            }
        }
//        echo "<pre>"; die(print_r($aOutPut));
        return $aOutPut;
    }

    public function getFilterCategories($data = array()) {
        $sql = "
            SELECT DISTINCT C.category_id, D.name AS name
            FROM " . DB_PREFIX . "category AS C
            JOIN " . DB_PREFIX . "category_description AS D ON D.category_id=C.category_id
            JOIN " . DB_PREFIX . "product_to_category AS PC ON D.category_id=PC.category_id
            JOIN " . DB_PREFIX . "product AS P ON P.product_id=PC.product_id
            JOIN " . DB_PREFIX . "brand AS B ON B.brand_id=P.brand_id
            WHERE D.language_id=1
            GROUP BY C.category_id
        ";

        return $this->db->query($sql)->rows;
    }

    public function getBrandImages($brand_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "brand_image WHERE brand_id = '" . (int) $brand_id . "'  ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getBrandsByProducts($data = array()) {

        $sql = " SELECT b.brand_id, CONCAT(bd1.name,' - ',bd2.name) name, count(p.product_id) as products FROM " . DB_PREFIX . "brand b LEFT JOIN " . DB_PREFIX . "product p ON(b.brand_id=p.brand_id) LEFT JOIN " . DB_PREFIX . "brand_description bd1 ON (b.brand_id = bd1.brand_id) LEFT JOIN " . DB_PREFIX . "brand_description bd2 ON (b.brand_id = bd2.brand_id) WHERE bd1.language_id = '1' AND bd2.language_id = '2' ";

        if (!empty($data['filter_brand_id'])) {
            $sql .= " AND b.brand_id = " . $data['filter_brand_id'];
        } else {
            if (!empty($data['filter_filter'])) {
                $sql .= " AND CONCAT(bd1.name,' ',bd2.name)  LIKE '%" .
                        $this->db->escape($data['filter_filter']) . "%' ";
            }

            if (isset($data['filter_status'])) {
                $sql .= " AND b.status = " . (int) $data['filter_status'] . " ";
            }

            if (!empty($data['filter_shopable'])) {
                $sql .= " AND p.stock_status_id = " . (int) $data['filter_shopable'] . " ";
            }

            if (isset($data['filter_country'])) {
                $sql .= " AND z.country_id = " . (int) $data['filter_country'] . " ";
            }

            if (!empty($data['filter_category'])) {
                $sql .= " AND pc.category_id = " . (int) $data['filter_category'] . " ";
            }

            $sql.=' GROUP BY b.brand_id ';
            $sort_data = array(
                'bd1.name',
                'bd2.name',
                'b.date_added',
                'b.sort_order'
            );


            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                if (isset($data['force_sort']) && $data['force_sort'] == 1) {
                    $sql .= " ORDER BY " . $data['sort'];
                } else {
                    $sql .= " ORDER BY products DESC," . $data['sort'];
                }
            } else {
                $sql .= " ORDER BY products DESC, bd1.name";
            }


            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }


            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] <= 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

}
