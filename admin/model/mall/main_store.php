<?php

class ModelMallMainStore extends Model {

    public function addMainStore($data) {
        $this->event->trigger('pre.admin.main_store.add', $data);
        if (!isset($data['category_id'])) {
            $data['category_id'] = 0;
        }
        $sBrands = '';
        if (isset($data['brands']) && $data['show_related_brand']=='on'){
            $data['show_related_brand'] = 1;
            $sBrands = implode(',',$data['brands']);
        }
        $sCategories = '';
        if (isset($data['related_categories']) && $data['show_related_category']=='on'){
            $data['show_related_category'] = 1;
            $sCategories = implode(',',$data['related_categories']);
        }

        $this->db->query("INSERT INTO " . DB_PREFIX . "main_store SET show_related_brand='" . (int) $data['show_related_brand'] . "', related_brand='" . $sBrands . "' , show_related_category='" . (int) $data['show_related_category'] . "',related_category='" . $sCategories . "', city_id='" . (int) $data['city_id'] . "', phone='" . $this->db->escape($data['phone']) . "', main_group_id='" . (int) $data['main_group_id'] . "', category_id='" . (int) $data['category_id'] . "', website='" . $this->db->escape($data['website']) . "', social_fb='" . $this->db->escape($data['social_fb']) . "', social_tw='" . $this->db->escape($data['social_tw']) . "', social_jeeran='" . $this->db->escape($data['social_jeeran']) . "', sort_order = '" . (int) $data['sort_order'] . "', date_modified = NOW(), status='" . (int) $data['status'] . "', date_added = NOW(), user_id = '" . (int) $this->user->getid() . "'");

        $main_store_id = $this->db->getLastId();

        $this->load->model('tool/image');
        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('main_store', 'image', $main_store_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('shops_brands_designer') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query("UPDATE " . DB_PREFIX . "main_store SET `image` = '" . $this->db->escape($image) . "' WHERE main_store_id = '" . (int) $main_store_id . "' ");
        }

        foreach ($data['main_store_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "main_store_description SET main_store_id = '" . (int) $main_store_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', location = '" . $this->db->escape($value['location']) . "'");
        }

        if (isset($data['sub_stores'])) {
            $counter = 0;
            foreach ($data['sub_stores'] as $store_id) {
                $counter++;
                //$this->db->query("UPDATE " . DB_PREFIX . "store s SET s.sort_order= '" . (int) ($counter * 16 + (int) $data['sort_order']) . "', s.main_store_id = '" . (int) $main_store_id . "' WHERE store_id = '" . (int) $store_id . "'");
                $this->db->query("UPDATE " . DB_PREFIX . "store s SET s.main_store_id = '" . (int) $main_store_id . "' WHERE store_id = '" . (int) $store_id . "'");
                $this->db->query("UPDATE " . DB_PREFIX . "store s SET s.`image`   = '" . $this->db->escape($image) . "' WHERE main_store_id = '" . (int) $main_store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "store_category_to_main_store WHERE main_store_id = '" . (int) $main_store_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "store_category_to_store      WHERE store_id IN ( SELECT GROUP_CONCAT(s.store_id) FROM store s WHERE s.main_store_id = '" . (int) $main_store_id . "' ) ");

        if (isset($data['store_category'])) {
            foreach ($data['store_category'] as $store_category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "store_category_to_main_store SET main_store_id = '" . (int) $main_store_id . "', store_category_id = '" . (int) $store_category_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "store_category_to_store(store_category_id, store_id) ( SELECT store_id,'" . (int) $store_category_id . "'  FROM store WHERE  main_store_id = '" . (int) $main_store_id . "') ");
            }
        }

        $this->db->query(" DELETE FROM maps WHERE link_type='mainstore' AND link_id='" . (int) $main_store_id . "' ");
        $this->db->query(" INSERT INTO maps SET   link_type='mainstore' , link_id='" . (int) $main_store_id . "', latitude='" . (float) $data['latitude'] . "', longitude='" . (float) $data['longitude'] . "', zoom='" . (int) $data['zoom'] . "' ");

        //$this->cache->delete('main_store');
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='shop', type_id='" . (int) $store_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        $this->event->trigger('post.admin.main_store.add', $main_store_id);

        return $main_store_id;
    }

    public function editMainStore($main_store_id, $data) {
        $this->event->trigger('pre.admin.main_store.edit', $data);
        if (!isset($data['category_id'])) {
            $data['category_id'] = 0;
        }


        if (isset($data['show_related_category']) && $data['show_related_category']=='on') {
            $data['show_related_category'] = 1;
        }else{
          $data['show_related_category'] = 0;
          $data['related_category'] = 0;
        }
        if (!isset($data['related_category'])) {
            $data['related_category'] = 0;
        }


        if (isset($data['show_related_brand']) && $data['show_related_brand']=='on') {
            $data['show_related_brand'] = 1;
        }else{
          $data['show_related_brand'] = 0;
          $data['brand_id'] = 0;
        }
        if (!isset($data['brand_id'])) {
            $data['brand_id'] = 0;
        }
        $sBrands = '';
        if (isset($data['brands'])){
            $sBrands = implode(',',$data['brands']);
        }
        $sCategories = '';
        if (isset($data['related_categories'])){
            $sCategories = implode(',',$data['related_categories']);
        }

        $this->db->query("UPDATE " . DB_PREFIX . "main_store SET city_id='" . (int) $data['city_id'] . "', show_related_brand='" . (int) $data['show_related_brand'] . "', related_brand='" . $sBrands . "' , show_related_category='" . (int) $data['show_related_category'] . "',related_category='" . $sCategories . "', phone='" . $this->db->escape($data['phone']) . "', main_group_id='" . (int) $data['main_group_id'] . "', website='" . $this->db->escape($data['website']) . "', social_fb='" . $this->db->escape($data['social_fb']) . "', social_tw='" . $this->db->escape($data['social_tw']) . "', social_jeeran='" . $this->db->escape($data['social_jeeran']) . "', sort_order = '" . (int) $data['sort_order'] . "', status='" . (int) $data['status'] . "', date_modified = NOW(), last_mod_id = '" . (int) $this->user->getid() . "'  WHERE main_store_id = '" . (int) $main_store_id . "'");
        
        $this->load->model('tool/image');
        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('main_store', 'image', $main_store_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('shops_brands_designer') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query("UPDATE " . DB_PREFIX . "main_store SET `image` = '" . $this->db->escape($image) . "' WHERE main_store_id = '" . (int) $main_store_id . "' ");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "main_store_description WHERE main_store_id = '" . (int) $main_store_id . "'");
        foreach ($data['main_store_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "main_store_description SET main_store_id = '" . (int) $main_store_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', location = '" . $this->db->escape($value['location']) . "'");
        }

        $this->db->query("UPDATE " . DB_PREFIX . "store s SET s.main_store_id = '0', s.image = (SELECT ms.image FROM main_store ms WHERE ms.main_store_id='" . (int) $main_store_id . "' ) WHERE main_store_id = '" . (int) $main_store_id . "'");
        if (isset($data['sub_stores'])) {
            $counter = 0;
            foreach ($data['sub_stores'] as $store_id) {
                $counter++;
                //$this->db->query("UPDATE " . DB_PREFIX . "store s SET s.sort_order='" . ($counter * 16 + (int) $data['sort_order']) . "', s.main_store_id = '" . (int) $main_store_id . "' WHERE store_id = '" . (int) $store_id . "'");
                $this->db->query("UPDATE " . DB_PREFIX . "store s SET s.main_store_id = '" . (int) $main_store_id . "' WHERE store_id = '" . (int) $store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "store_category_to_main_store WHERE main_store_id = '" . (int) $main_store_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "store_category_to_store WHERE store_id IN ( SELECT s.store_id FROM store s WHERE s.main_store_id = '" . (int) $main_store_id . "' ) ");

        if (isset($data['store_category'])) {
            foreach ($data['store_category'] as $store_category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "store_category_to_main_store SET main_store_id = '" . (int) $main_store_id . "', store_category_id = '" . (int) $store_category_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "store_category_to_store(store_category_id, store_id) ( SELECT store_id,'" . (int) $store_category_id . "'  FROM store WHERE  main_store_id = '" . (int) $main_store_id . "') ");
            }
        }

        $this->db->query(" DELETE FROM maps WHERE link_type='mainstore' AND link_id='" . (int) $main_store_id . "' ");
        $this->db->query(" INSERT INTO maps SET   link_type='mainstore' , link_id='" . (int) $main_store_id . "', latitude='" . (float) $data['latitude'] . "', longitude='" . (float) $data['longitude'] . "', zoom='" . (int) $data['zoom'] . "' ");

        //$this->cache->delete('main_store');
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='shop', type_id='" . (int) $store_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        $this->event->trigger('post.admin.main_store.update', $main_store_id);
    }

    public function deleteMainStore($main_store_id) {
        $this->event->trigger('pre.admin.main_store.delete', $main_store_id);

        $this->db->query("DELETE FROM " . DB_PREFIX . "main_store_description WHERE main_store_id = '" . (int) $main_store_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "main_store WHERE main_store_id = '" . (int) $main_store_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "main_store_description WHERE main_store_id = '" . (int) $main_store_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "maps WHERE link_type='mainstore' AND link_id = '" . (int) $main_store_id . "'");
        $this->cache->delete('main_store');

        $this->event->trigger('post.admin.main_store.delete', $main_store_id);
    }

    public function getMainStoreDescriptions($main_store_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "main_store_description WHERE main_store_id='" . (int) $main_store_id . "' ");

        $main_store_description_data = array();

        foreach ($query->rows as $result) {
            $main_store_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'location' => $result['location']
            );
        }

        return $main_store_description_data;
    }

    public function getMainStore($main_store_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "main_store mg LEFT JOIN " . DB_PREFIX . "main_store_description mgd ON(mg.main_store_id = mgd.main_store_id) WHERE mgd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND mg.main_store_id='" . (int) $main_store_id . "' ");

        return $query->row;
    }

    public function getMap($main_store_id) {
        $query = $this->db->query(" SELECT * FROM maps WHERE link_type='mainstore' AND link_id='" . (int) $main_store_id . "' ");
        return $query->row;
    }

    public function getMainStores($data = array()) {
        
        $sKeyWordConditionSqlExtension = $sSettingsTableExtension = $sCityConditionSqlExtension = $sCategoryConditionSqlExtension = $sCountryConditionSqlExtension = $sStoreTableExtension = $sMallConditionSqlExtension = $sCountryCityLanguageSqlExtension = "";

        if (isset($data['filter_keyword'])) {
            $sKeyWordConditionSqlExtension = " AND CONCAT(mgd1.name,' ',mgd2.name,' ',mgd1.description,' ',mgd2.description,' ',mgd1.location,' ',mgd2.location) LIKE '%" . $this->db->escape($data['filter_keyword']) . "%' ";
        }

        if (isset($data['filter_category'])) {
            $sCategoryConditionSqlExtension = " AND category_id = '" . (int) $data['filter_category'] . "' ";
        }

        if (isset($data['filter_mall_id'])) {
            $sStoreTableExtension = " LEFT JOIN " . DB_PREFIX . "store s ON (mg.main_store_id = s.store_id) ";
            $sMallConditionSqlExtension = " AND s.mall_id = '" . (int) $data['filter_mall_id'] . "' ";
        }

        if (isset($data['filter_city_id']) || isset($data['filter_country_id'])) {

            /*
              $sSettingsTableExtension = " LEFT JOIN " . DB_PREFIX . "setting mss ON (mg.main_store_id = mss.store_id) ";
              if (isset($data['filter_city_id'])) {
              $sCityConditionSqlExtension = " AND mss.key = 'config_zone_id' AND mss.value = '" . (int) $data['filter_city_id'] . "' ";
              }
              if (isset($data['filter_country_id'])) {
              $sCountryConditionSqlExtension = " AND mss.key = 'config_country_id' AND mss.value = '" . (int) $data['filter_country_id'] . "' ";
              }
             */
            $sSettingsTableExtension = " LEFT JOIN " . DB_PREFIX . "zone z ON( mg.city_id=z.zone_id ) 
                                        LEFT JOIN " . DB_PREFIX . "zone_description zd ON(z.zone_id=zd.zone_id) 
                                        LEFT JOIN " . DB_PREFIX . "country c ON(z.country_id = c.country_id ) ";
            if (isset($data['filter_city_id']) && !empty($data['filter_city_id'])) {
                $sCityConditionSqlExtension = " AND z.zone_id = '" . (int) $data['filter_city_id'] . "' ";
            }
            if (isset($data['filter_country_id']) && !empty($data['filter_country_id'])) {
                $sCountryConditionSqlExtension = " AND c.country_id = '" . (int) $data['filter_country_id'] . "' ";
            }

            $sCountryCityLanguageSqlExtension = " AND zd.language_id='1' ";
        }

        $sql = "SELECT mg.main_store_id, CONCAT(mgd1.name, ' - ', mgd2.name) AS name, mg.sort_order, mg.products_count, mg.date_added, mg.date_modified ,u.username user_add, mu.username user_modify  
                FROM " . DB_PREFIX . "main_store mg 
                LEFT JOIN " . DB_PREFIX . "main_store_description mgd1 ON (mg.main_store_id = mgd1.main_store_id) 
                LEFT JOIN " . DB_PREFIX . "main_store_description mgd2 ON (mg.main_store_id = mgd2.main_store_id)
                LEFT JOIN " . DB_PREFIX . "user u ON (mg.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (mg.last_mod_id = mu.user_id) 
                {$sSettingsTableExtension}
                {$sStoreTableExtension}
                WHERE mg.is_designer = 0 AND 
                mgd1.language_id = '1' AND mgd2.language_id = '2' 
                {$sKeyWordConditionSqlExtension}
                {$sCityConditionSqlExtension}
                {$sCountryConditionSqlExtension}
                {$sCountryCityLanguageSqlExtension}
                {$sCategoryConditionSqlExtension}
                {$sMallConditionSqlExtension}
                ";

        $sort_data = array(
            'mgd1.name'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY mg.sort_order ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalMainStores($data = array()) {
// $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "main_store ");

        $sKeyWordConditionSqlExtension = $sSettingsTableExtension = $sCityConditionSqlExtension = $sCategoryConditionSqlExtension = $sCountryConditionSqlExtension = $sStoreTableExtension = $sMallConditionSqlExtension = $sCountryCityLanguageSqlExtension = "";

        if (isset($data['filter_keyword'])) {
            $sKeyWordConditionSqlExtension = " AND CONCAT(mgd1.name,' ',mgd2.name,' ',mgd1.description,' ',mgd2.description,' ',mgd1.location,' ',mgd2.location) LIKE '%" . $this->db->escape($data['filter_keyword']) . "%' ";
        }

        if (isset($data['filter_category'])) {
            $sCategoryConditionSqlExtension = " AND category_id = '" . (int) $data['filter_category'] . "' ";
        }

        if (isset($data['filter_mall_id'])) {
            $sStoreTableExtension = " LEFT JOIN " . DB_PREFIX . "store s ON (mg.main_store_id = s.store_id) ";
            $sMallConditionSqlExtension = " AND s.mall_id = '" . (int) $data['filter_mall_id'] . "' ";
        }

        if (isset($data['filter_city_id']) || isset($data['filter_country_id'])) {

            $sSettingsTableExtension = " LEFT JOIN " . DB_PREFIX . "zone z ON( mg.city_id=z.zone_id ) 
                                        LEFT JOIN " . DB_PREFIX . "zone_description zd ON(z.zone_id=zd.zone_id) 
                                        LEFT JOIN " . DB_PREFIX . "country c ON(z.country_id = c.country_id ) ";
            if (isset($data['filter_city_id']) && !empty($data['filter_city_id'])) {
                $sCityConditionSqlExtension = " AND z.zone_id = '" . (int) $data['filter_city_id'] . "' ";
            }
            if (isset($data['filter_country_id']) && !empty($data['filter_country_id'])) {
                $sCountryConditionSqlExtension = " AND c.country_id = '" . (int) $data['filter_country_id'] . "' ";
            }

            $sCountryCityLanguageSqlExtension = " AND zd.language_id='1' ";
        }

        $sql = "SELECT COUNT(*) AS total  
                FROM " . DB_PREFIX . "main_store mg 
                LEFT JOIN " . DB_PREFIX . "main_store_description mgd1 ON (mg.main_store_id = mgd1.main_store_id) 
                LEFT JOIN " . DB_PREFIX . "main_store_description mgd2 ON (mg.main_store_id = mgd2.main_store_id) 
                {$sSettingsTableExtension}
                {$sStoreTableExtension}
                WHERE mg.is_designer=0 AND 
                mgd1.language_id = '1' AND mgd2.language_id = '2' 
                {$sKeyWordConditionSqlExtension}
                {$sCityConditionSqlExtension}
                {$sCountryConditionSqlExtension}
                {$sCountryCityLanguageSqlExtension}
                {$sCategoryConditionSqlExtension}
                {$sMallConditionSqlExtension}
                ";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

}
