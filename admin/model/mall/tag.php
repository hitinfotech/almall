<?php

class ModelMallTag extends Model {

    protected $table = 'tag';
    protected $sql_avoid_escape = array(
        'columns' => array(
            'date_added',
            'date_modified',
        ),
        'values' => array()
    );
    protected $columns = array(
        'id' => array(
            'name' => 't.tag_id',
            'type' => 'integer',
        ),
        'name' => array(
            'name' => 'd.name',
            'type' => 'string',
        ),
        'status' => array(
            'name' => 't.status',
            'type' => 'boolean',
        ),
        'country' => array(
            'name' => 'c.country_id',
            'type' => 'integer',
        ),
        'home' => array(
            'name' => 't.home',
            'type' => 'boolean',
        ),
        'author' => array(
            'name' => 't.author',
            'type' => 'string',
        ),
        'sort_order' => array(
            'name' => 't.sort_order',
            'type' => 'integer',
        ),
    );
    protected $links = array('country', 'product', 'tip', 'look');
    protected $link_tables = array(
        'country' => 'tag_to_country',
        'product' => 'tag_product',
        'tip' => 'tag_tip',
        'look' => 'tag_look',
    );

    public function addTag($data) {
        $item_id = $this->insertOrUpdateItem(array_merge($data, array(
            'date_added' => 'NOW()',
            'date_modified' => 'NOW()',
        )));

        $this->updateDescription($item_id, $data);
        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('tag', 'image', $item_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('tag') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
        }
        $this->updateLinks($item_id, $data);
        $this->cache->deletekeys('admin.tag');
        $this->cache->deletekeys('catalog.tag');

        return $item_id;
    }

    public function editTag($item_id, $data) {

        $this->insertOrUpdateItem(array_merge($data, array(
            "{$this->table}_id" => $item_id,
            'date_modified' => 'NOW()',
        )));

        $this->updateDescription($item_id, $data);
        //$this->updateImages($item_id, $data);
        $this->updateLinks($item_id, $data);

        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('tag', 'image', $item_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('tag') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE tag SET `image` = '" . $this->db->escape($image) . "' WHERE tag_id='" . (int) $item_id . "' ");
        }

        $this->cache->deletekeys('admin.tag');
        $this->cache->deletekeys('catalog.tag');
    }

    public function getTags($data = array()) {
        return $this->sqlExecuteSelectItemsWithDescriptions($this->table, $data, [
                    '*', 't.tag_id tag_id'
        ]);
    }

    public function getTag($item_id) {
        $sql = " SELECT * FROM " . DB_PREFIX . "{$this->table} t LEFT JOIN " . DB_PREFIX .
                "{$this->table}_description td ON (t.{$this->table}_id = td.{$this->table}_id) " .
                " WHERE t.{$this->table}_id = '" . (int) $item_id .
                "' AND td.language_id = '" .
                $this->config->get('config_language_id') . "' ";
//        die($sql);
        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getTagDescriptions($item_id) {
        return $this->sqlExecuteSelectDescription($this->table, $item_id);
    }

    public function getTagLink($item_id) {
        return $this->sqlExecuteSelectLinks($this->table, $item_id);
    }

    public function getTagImages($item_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "{$this->table}_image WHERE {$this->table}_id = '" . (int) $item_id . "'  ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getTotalTag($data) {
        unset($data['start'], $data['limit']);
        $rows = $this->sqlExecuteSelectItemsWithDescriptions($this->table, $data, array('COUNT(*) total'));

        if (!isset($rows[0], $rows[0]['total'])) {
            return 0;
        }

        return intval($rows[0]['total']);
    }

    /* Optimized methods */

    /**
     * @param $item_id
     */
    public function deleteTag($item_id) {
        $table_name = $this->table;

        foreach (array_merge($this->link_tables, array("{$table_name}", "{$table_name}_description")) as $table) {
            $this->sqlExecuteDelete($table, array("{$table_name}_id" => (int) $item_id));
        }
        $this->cache->deletekeys('admin.tag');
        $this->cache->deletekeys('catalog.tag');
    }

    /**
     * @param Array $data
     */
    protected function insertOrUpdateItem(Array $data) {
        return $this->sqlExecuteReplace($this->table, array(
                    "{$this->table}_id" => isset($data["{$this->table}_id"]) ? $data["{$this->table}_id"] : null,
                    'status' => $data['status'],
//            'image'     => $data['image'],
                    'home' => $data['home'],
                    'author' => $data['author'],
//             'user_id'   => $data['user_id'],
                    'sort_order' => $data['sort_order'],
                    'meta_title' => $data['meta_title'],
                    'meta_description' => $data['meta_description'],
                    'meta_keywords' => $data['meta_keywords'],
        ));
    }

    /**
     * @param $item_id
     * @param $data
     */
    protected function updateDescription($item_id, $data) {
        $table_name = $this->table;

        foreach ($data["item_description"] as $language_id => $value) {
            $this->sqlExecuteDelete("{$table_name}_description", array(
                "{$table_name}_id" => $item_id,
                'language_id' => (int) $language_id,
            ));

            $this->sqlExecuteReplace("{$this->table}_description", array(
                "{$table_name}_id" => (int) $item_id,
                'language_id' => (int) $language_id,
                'name' => $value['name'],
                'body' => $this->stripTagsStyle(htmlspecialchars_decode($value['description']))
            ));
        }
    }

    /**
     * @param $item_id
     * @param $data
     */
    protected function updateImage($item_id, $data) {
        $table_name = $this->table;

        if (isset($data["image"])) {
            $this->db->query("UPDATE " . DB_PREFIX . "{$table_name} SET image = '" . $this->db->escape($data["image"]) . "' WHERE {$table_name}_id = '" . (int) $item_id . "'");
        }
    }

    /**
     * @param $item_id
     * @param $data
     */
    protected function updateImages($item_id, $data) {
        $table_name = $this->table;

        if (isset($data["image"])) {
            $this->db->query("UPDATE " . DB_PREFIX . "{$table_name} SET image = '" . $this->db->escape($data["image"]) . "' WHERE {$table_name}_id = '" . (int) $item_id . "'");
            $config_image = new config_image();
            $this->load->model('tool/image');

            foreach ($config_image->get('tags') as $row) {
                if ($row['width'] > 10) {
                    $this->model_tool_image->resize($data['image'], $row['width'], $row['hieght']);
                }
            }
        }
        $this->url_custom->create_URL('tag', $item_id);
    }

    /**
     * @param $item_id
     * @param $data
     */
    protected function updateLinks($item_id, $data) {
        $this->sqlExecuteUpdateLinks($this->table, $item_id, $data);
    }

    /**
     * @return array
     */
    public function getColumns() {
        return $this->columns;
    }

    /**
     * @param $name
     * @return bool
     */
    private function sqlAvoidEscapeColumn($name) {
        return in_array($name, $this->sql_avoid_escape['columns']);
    }

    /**
     * @param $value
     * @return bool
     */
    private function sqlAvoidEscapeValue($value) {
        return in_array($value, $this->sql_avoid_escape['values']);
    }

    /**
     * @param $value
     * @return int|string
     */
    private function sqlEscapeValue($value) {
        if (!isset($value)) {
            return 'NULL';
        }

        return (is_numeric($value) || $this->sqlAvoidEscapeValue($value)) ? $value : ("'" . $this->db->escape($value) . "'");
    }

    /**
     * @param $values
     * @return string
     */
    private function sqlGetColumnsBlockFromValues($values) {
        return '(' . implode(',', array_keys(reset($values))) . ')';
    }

    /**
     * @param $value
     * @return string
     */
    private function sqlValueToStringBlock($value) {
        $escaped_values = array();
        foreach ($value as $value_column => $value_data) {
            if (!$this->sqlAvoidEscapeColumn($value_column)) {
                $value_data = $this->sqlEscapeValue($value_data);
            }
            $escaped_values[] = $value_data;
        }
        return '(' . implode(',', $escaped_values) . ')';
    }

    /**
     * @param $type
     * @param $value
     * @return string
     */
    private function sqlBuildWhereConditionOperatorAndValue($type, $value) {
        switch ($type) {
            case 'integer':
            case 'boolean':
                $parsed_value = intval($value);
                return "= {$parsed_value}";
            case 'text':
            case 'string':
                $parsed_value = $this->db->escape($value);
                return "LIKE '%{$parsed_value}%'";
        }
    }

    /**
     * @param $type
     * @param $name
     * @param $value
     * @return string
     */
    private function sqlBuildWhereConditionOneBlock($type, $name, $value) {
        $operation_and_value = $this->sqlBuildWhereConditionOperatorAndValue($type, $value);

        return "{$name} {$operation_and_value}";
    }

    /**
     * @param $name
     * @param $value
     * @return string
     */
    private function sqlBuildWhereConditionBlock($name, $value) {
        if ($value !== '' && key_exists($name, $this->columns)) {
            $column = $this->columns[$name];
            $column_names = (array) $column['name'];

            $conditions = array();
            foreach ($column_names as $column_name) {
                if (isset($value)) {
                    $conditions[] = $this->sqlBuildWhereConditionOneBlock($column['type'], $column_name, $value);
                }
            }

            if (!empty($conditions)) {
                return "(" . implode(' OR ', $conditions) . ")";
            }
        }
    }

    /**
     * @param $filters
     * @return string
     */
    private function sqlBuildWhereBlock($filters) {
        $conditions = array();
        foreach ((array) $filters as $filter_name => $filter_value) {
            $condition_block = $this->sqlBuildWhereConditionBlock($filter_name, $filter_value);
            if ($condition_block) {
                $conditions[] = $condition_block;
            }
        }

        return implode(' AND ', $conditions);
    }

    /**
     * @param $sort
     * @return string
     */
    private function sqlBuildOrderByBlock($sort) {
        if (key_exists($sort['column'], $this->columns)) {
            $column = $this->columns[$sort['column']];
            $column_order = (strtolower($sort['order']) == 'desc') ? 'DESC' : 'ASC';

            return "ORDER BY {$column['name']} {$column_order}";
        }
    }

    /**
     * @param $table_name
     */
    protected function sqlExecuteReplace($table_name) {
        $values = array_slice(func_get_args(), 1);

        $sql = "REPLACE INTO `{$table_name}` ";
        $sql.= $this->sqlGetColumnsBlockFromValues($values) . "VALUES";

        foreach ($values as $value) {
            $sql .= $this->sqlValueToStringBlock($value);
        }
        $this->db->query($sql);

        return $this->db->getLastId();
    }

    /**
     * @param $table_name
     * @param array $data
     * @param array $columns
     * @return
     */
    protected function sqlExecuteSelectItemsWithDescriptions($table_name, Array $data = array(), Array $columns = array('*')) {
        $country_table = $this->link_tables['country'];

        $select_columns = implode(',', $columns);
        $sql = "SELECT {$select_columns} FROM " . DB_PREFIX . "{$table_name} t " .
                "LEFT JOIN " . DB_PREFIX . "{$table_name}_description d ON t.{$table_name}_id = d.{$table_name}_id " .
                "LEFT JOIN " . DB_PREFIX . "{$country_table} c ON c.{$table_name}_id = t.{$table_name}_id " .
                "WHERE d.language_id = '" . $this->config->get('config_language_id') . "' ";

        if (isset($data['filter'])) {
            $sql_filters = $this->sqlBuildWhereBlock($data['filter']);
            $sql .= $sql_filters ? " AND {$sql_filters}" : '';
        }

        if (isset($data['sort'])) {
            $sql .= " " . $this->sqlBuildOrderByBlock($data['sort']) . " ";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] <= 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . ", " . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

//        if($select_columns!='COUNT(*) total'){
//            print_r($query->rows);
//            die($sql);
//        }

        return $query->rows;
    }

    /**
     * @param $table_name
     * @param $item_id
     * @return array
     */
    protected function sqlExecuteSelectDescription($table_name, $item_id) {
        $description_data = array();
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "{$table_name}_description WHERE {$table_name}_id = '" . (int) $item_id . "' ");

        foreach ($query->rows as $result) {
            $description_data[$result['language_id']] = array(
                'name' => isset($result['name']) ? $result['name'] : (isset($result['title']) ? $result['title'] : ''),
                'description' => isset($result['description']) ? $result['description'] : (isset($result['body']) ? $result['body'] : ''),
                'meta_title' => isset($result['meta_title']) ? $result['meta_title'] : (isset($result['meta_title']) ? $result['meta_title'] : ''),
                'meta_description' => isset($result['meta_description']) ? $result['meta_description'] : (isset($result['meta_description']) ? $result['meta_description'] : ''),
                'meta_keywords' => isset($result['meta_keywords']) ? $result['meta_keywords'] : (isset($result['meta_keywords']) ? $result['meta_keywords'] : '')
            );
        }

        return $description_data;
    }

    /**
     * @param $table_name
     * @param $item_id
     * @return array
     */
    protected function sqlExecuteSelectLinks($table_name, $item_id) {
        $aOutPut = array();

        foreach ($this->links as $link_type) {
            if (!isset($this->link_tables[$link_type]))
                continue;

            $link_table_name = $this->link_tables[$link_type];
            $links = $this->db->query(" SELECT * FROM " . DB_PREFIX . "{$link_table_name} WHERE {$table_name}_id = '" . (int) $item_id . "' ");

            switch ($link_type) {
                case 'country':
                    foreach ($links->rows as $for_link_row) {
                        $query1 = $this->db->query(" SELECT * FROM " . DB_PREFIX . "country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE language_id = '" . (int) $this->config->get('config_language_id') . "' AND c.country_id = '" . (int) $for_link_row['country_id'] . "' ");
                        if (isset($query1->row) && !empty($query1->row)) {

                            $aOutPut[$link_type][] = array('id' => $query1->row['country_id'], 'name' => $query1->row['name']);
                        }
                    }
                    break;
                case 'category':
                    foreach ($links->rows as $for_link_row) {
                        $query5 = $this->db->query(" SELECT scd1.store_category_id, CONCAT(scd1.name, ' - ', scd2.name) as name
                                                     FROM " . DB_PREFIX . "store_category_description scd1
                                                     INNER JOIN  " . DB_PREFIX . "store_category_description scd2 ON (scd1.store_category_id=scd2.store_category_id)
                                                     WHERE scd1.store_category_id = '" . (int) $for_link_row['category_id'] . "'
                                                     AND scd1.language_id = '1'
                                                     AND scd2.language_id = '2'
                                                     ");
                        if (isset($query5->row) && !empty($query5->row)) {
                            $aOutPut[$link_type][] = array('id' => $query5->row['store_category_id'], 'name' => $query5->row['name']);
                        }
                    }
                    break;
                case 'shop':
                case 'store':
                    foreach ($links->rows as $for_link_row) {
                        $query4 = $this->db->query(" SELECT s.store_id , CONCAT(sd1.name, ' - ',sd2.name, ' ( ',md.name, ' ) ') as name, md.name as mall_name
                                                     FROM " . DB_PREFIX . "store s
                                                     LEFT JOIN " . DB_PREFIX . "store_description sd1 ON (s.store_id = sd1.store_id)
                                                     LEFT JOIN " . DB_PREFIX . "store_description sd2 ON (s.store_id = sd2.store_id)
                                                     LEFT JOIN " . DB_PREFIX . "mall_description md ON (s.mall_id = md.mall_id)
                                                     WHERE
                                                     sd1.language_id = '1'
                                                     AND sd2.language_id = '2'
                                                     AND md.language_id = '1'
                                                     AND s.store_id = '" . (int) $for_link_row['store_id'] . "' ");
                        if (isset($query4->row) && !empty($query4->row)) {
                            $aOutPut['shop'][] = array('id' => $query4->row['store_id'], 'name' => $query4->row['name']);
                        }
                    }
                    break;
                case 'product':
                    $products_ids = implode(',', array_map(function($val) {
                                return $val['product_id'];
                            }, $links->rows)? : array(0));
                    $query4 = $this->db->query(" SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN product_description pd ON(p.product_id=pd.product_id) WHERE p.product_id IN({$products_ids}) AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "' ");
                    foreach ($query4->rows as $for_product) {
                        $aOutPut['product'][] = array('id' => $for_product['product_id'], 'name' => $for_product['name']);
                    }
                    break;
                case 'tip':
                    $item_ids = implode(',', array_map(function($val) {
                                return $val['tip_id'];
                            }, $links->rows)? : array(0));
                    $query4 = $this->db->query("SELECT * FROM " . DB_PREFIX . "tip t LEFT JOIN tip_description td ON(t.tip_id=td.tip_id) WHERE t.tip_id IN({$item_ids}) AND td.language_id = '" . (int) $this->config->get('config_language_id') . "' ");
                    foreach ($query4->rows as $for_item) {
                        $aOutPut['tip'][] = array('id' => $for_item['tip_id'], 'name' => $for_item['name']);
                    }
                    break;
                case 'look':
                    $item_ids = implode(',', array_map(function($val) {
                                return $val['look_id'];
                            }, $links->rows)? : array(0));
                    $query4 = $this->db->query("SELECT * FROM " . DB_PREFIX . "look t LEFT JOIN look_description td ON(t.look_id=td.look_id) WHERE t.look_id IN({$item_ids}) AND td.language_id = '" . (int) $this->config->get('config_language_id') . "' ");
                    foreach ($query4->rows as $for_item) {
                        $aOutPut['look'][] = array('id' => $for_item['look_id'], 'name' => $for_item['title']);
                    }
                    break;
                default:

                    break;
            }
        }
        return $aOutPut;
    }

    /**
     * @param $table_name
     * @param $item_id
     * @param array $data
     */
    protected function sqlExecuteUpdateLinks($table_name, $item_id, Array $data) {
        $default_links = array_combine($this->links, array_pad(array(), count($this->links), array()));

        if ($data["item_link"] = array_merge($default_links, isset($data["item_link"]) ? $data["item_link"] : array())) {
            foreach ($data["item_link"] as $link_type => $link) {
                if (isset($this->link_tables[$link_type])) {
                    $link_table_name = $this->link_tables[$link_type];

                    $this->db->query("DELETE FROM " . DB_PREFIX . "{$link_table_name} WHERE {$table_name}_id='" . (int) $item_id . "'");

                    foreach ($link as $link_id) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "{$link_table_name} SET {$table_name}_id = '" . (int) $item_id . "', {$link_type}_id = '" . $this->db->escape($link_id) . "'");
                    }
                }
            }
        }
    }

    /**
     * @param $table_name
     * @param $where
     */
    protected function sqlExecuteDelete($table_name, $where) {
        $sql = "DELETE FROM " . DB_PREFIX . "{$table_name} WHERE 1";

        foreach ($where as $column_name => $column_value) {
            $condition = array_merge(array(
                'type' => 'AND',
                'operator' => '='
                    ), (array) $column_value);

            $sql .= " {$condition['type']} {$column_name} {$condition['operator']} " .
                    $this->sqlEscapeValue($column_value);
        }
        $this->db->query($sql);
    }

}
