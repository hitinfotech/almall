<?php

class ModelMallStoreCategory extends Model {

    public function addStoreCategory($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "store_category SET parent_id='" . (int) $data['parent_id'] . "', 
                            group_id='" . (int) $data['group_id'] . "',  
                            sort_order = '" . (int) $data['sort_order'] . "', 
                            status='" . (int) $data['status'] . "', 
                            date_modified = NOW(), 
                            date_added = NOW()");

        $store_category_id = $this->db->getLastId();

        foreach ($data['store_category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO 
                            " . DB_PREFIX . "store_category_description 
                            SET 
                                store_category_id = '" . (int) $store_category_id . "', 
                                language_id = '" . (int) $language_id . "', 
                                name = '" . $this->db->escape($value['name']) . "', 
                                description = '" . $this->db->escape($value['description']) . "', 
                                meta_title = '" . $this->db->escape($value['meta_title']) . "', 
                                meta_keywords = '" . $this->db->escape($value['meta_keywords']) . "', 
                                meta_tag = '" . $this->db->escape($value['meta_tags']) . "' ");
        }
// die("<br>OK...");
        return $store_category_id;
    }

    public function editStoreCategory($store_category_id, $data) {
        $this->db->query("UPDATE 
                        " . DB_PREFIX . "store_category 
                        SET 
                            parent_id='" . (int) $data['parent_id'] . "', 
                            group_id='" . (int) $data['group_id'] . "',  
                            sort_order = '" . (int) $data['sort_order'] . "', 
                            status='" . (int) $data['status'] . "', 
                            date_modified = NOW() 
                        WHERE 
                            store_category_id = '" . (int) $store_category_id . "' ");

        $this->db->query("DELETE FROM " . DB_PREFIX . "store_category_description WHERE store_category_id = '" . (int) $store_category_id . "'");

        foreach ($data['store_category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO 
                            " . DB_PREFIX . "store_category_description 
                            SET 
                                store_category_id = '" . (int) $store_category_id . "', 
                                language_id = '" . (int) $language_id . "', 
                                name = '" . $this->db->escape($value['name']) . "', 
                                description = '" . $this->db->escape($value['description']) . "', 
                                meta_title = '" . $this->db->escape($value['meta_title']) . "', 
                                meta_keywords = '" . $this->db->escape($value['meta_keywords']) . "', 
                                meta_tag = '" . $this->db->escape($value['meta_tag']) . "' ");
        }
    }

    public function deleteStoreCategory($store_category_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "store_category_description WHERE store_category_id = '" . (int) $store_category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "store_category WHERE store_category_id = '" . (int) $store_category_id . "'");
    }

    public function getStoreCategoriesDescriptions($store_category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store_category_description WHERE store_category_id='" . (int) $store_category_id . "' ");

        $store_category_description_data = array();

        foreach ($query->rows as $result) {
            $store_category_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'meta_title' => $result['meta_title'],
                'meta_keywords' => $result['meta_keywords'],
                'meta_tag' => $result['meta_tag']
            );
        }

        return $store_category_description_data;
    }

    public function getStoreCategory($store_category_id) {
        $query = $this->db->query("SELECT * 
                                    FROM " . DB_PREFIX . "store_category mg 
                                    LEFT JOIN " . DB_PREFIX . "store_category_description mgd ON(mg.store_category_id = mgd.store_category_id) 
                                    WHERE 
                                        mgd.language_id = '" . (int) $this->config->get('config_language_id') . "' 
                                    AND mg.store_category_id='" . (int) $store_category_id . "' ");

        return $query->row;
    }

    public function getStoreCategories($data = array()) {

        $sSqlExtension = '';
        if (isset($data['filter_keyword']) && !empty($data['filter_keyword'])) {
            $sSqlExtension = "AND CONCAT(mgd.name,' ',mgd.description) LIKE '%" . $this->db->escape($data['filter_keyword']) . "%' ";
        }

        $sql = "SELECT mg.store_category_id, mg.parent_id, mg.group_id, mg.status, mg.sort_order, mg.date_added, mg.date_modified, mgd.name, mgd.description, mgd.meta_title, mgd.meta_keywords, mgd.meta_tag, mgd2.name as parent_category_name  
                FROM " . DB_PREFIX . "store_category mg 
                LEFT JOIN " . DB_PREFIX . "store_category_description mgd ON(mg.store_category_id = mgd.store_category_id) 
                LEFT JOIN " . DB_PREFIX . "store_category_description mgd2 ON(mg.parent_id = mgd2.store_category_id AND mgd2.language_id = '" . (int) $this->config->get('config_language_id') . "') 
                WHERE 
                    mgd.language_id = '" . (int) $this->config->get('config_language_id') . "' 
                {$sSqlExtension}";

        $sort_data = array(
            'name'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY mgd." . $data['sort'];
        } else {
            $sql .= " ORDER BY mg.sort_order ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
// die($sql);
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getStoreParentCategories($data = array()) {
        $sSqlExtension = "";
        if (isset($data['store_category_id'])) {
            $sSqlExtension .= " AND mg.store_category_id <> '{$data['store_category_id']}' ";
        }
        $sql = "SELECT * 
                FROM " . DB_PREFIX . "store_category mg 
                LEFT JOIN " . DB_PREFIX . "store_category_description mgd ON(mg.store_category_id = mgd.store_category_id) 
                WHERE 
                    mgd.language_id = '" . (int) $this->config->get('config_language_id') . "' 
                AND 
                    (mg.parent_id is null OR mg.parent_id = 0 ) 
                {$sSqlExtension} ";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY mgd." . $data['sort'];
        } else {
            $sql .= " ORDER BY mg.sort_order ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
// die($sql);
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalStoreCategories($data = array()) {

        $sSqlExtension = '';
        if (isset($data['filter_keyword']) && !empty($data['filter_keyword'])) {
            $sSqlExtension = "AND CONCAT(mgd.name,' ',mgd.description) LIKE '%" . $this->db->escape($data['filter_keyword']) . "%' ";
        }

        $sql = "SELECT COUNT(*) AS total  
                FROM " . DB_PREFIX . "store_category mg 
                LEFT JOIN " . DB_PREFIX . "store_category_description mgd ON(mg.store_category_id = mgd.store_category_id) 
                LEFT JOIN " . DB_PREFIX . "store_category_description mgd2 ON(mg.parent_id = mgd2.store_category_id AND mgd2.language_id = '" . (int) $this->config->get('config_language_id') . "') 
                WHERE 
                    mgd.language_id = '" . (int) $this->config->get('config_language_id') . "' 
                {$sSqlExtension}";

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getStoreCategoryByName($data) {
        if ($data['filter_name']) {
            $query = $this->db->query("SELECT mg.`store_category_id`, CONCAT (mgd.`name`,' | ',mgd2.`name`) as name 
                                        FROM " . DB_PREFIX . "store_category mg 
                                        LEFT JOIN " . DB_PREFIX . "store_category_description mgd ON(mg.store_category_id = mgd.store_category_id) 
                                        LEFT JOIN " . DB_PREFIX . "store_category_description mgd2 ON(mg.store_category_id = mgd2.store_category_id) 
                                        WHERE 
                                            mgd.language_id = '1' 
                                        AND mgd2.language_id = '2' 
                                        AND CONCAT (mgd.`name`,' ',mgd2.`name`) LIKE '%" . $this->db->escape($data['filter_name']) . "%' "
                    . " AND mg.status = 1");
            return $query->rows;
        } else {
            return array();
        }
    }

    public function getStoreCategoriesByMainStore($filter_main_store_id) {
        if ($filter_main_store_id) {

            $query = $this->db->query("SELECT mg.`store_category_id`, CONCAT (mgd.`name`,' | ',mgd2.`name`) as name 
                                        FROM " . DB_PREFIX . "store_category mg 
                                        LEFT JOIN " . DB_PREFIX . "store_category_description mgd ON(mg.store_category_id = mgd.store_category_id) 
                                        LEFT JOIN " . DB_PREFIX . "store_category_description mgd2 ON(mg.store_category_id = mgd2.store_category_id) 
                                        LEFT JOIN " . DB_PREFIX . "store_category_to_main_store mgd3 ON(mg.store_category_id = mgd3.store_category_id) 
                                        WHERE 
                                            mgd.language_id = '1' 
                                        AND mgd2.language_id = '2' 
                                        AND mgd3.main_store_id = '" . $filter_main_store_id . "' ");
            return $query->rows;
        } else {
            return array();
        }
    }

    public function getStoreCategoriesByStore($filter_store_id) {
        if ($filter_store_id) {

            $query = $this->db->query("SELECT mg.`store_category_id`, CONCAT (mgd.`name`,' | ',mgd2.`name`) as name 
                                        FROM " . DB_PREFIX . "store_category mg 
                                        LEFT JOIN " . DB_PREFIX . "store_category_description mgd ON(mg.store_category_id = mgd.store_category_id) 
                                        LEFT JOIN " . DB_PREFIX . "store_category_description mgd2 ON(mg.store_category_id = mgd2.store_category_id) 
                                        LEFT JOIN " . DB_PREFIX . "store_category_to_store mgd3 ON(mg.store_category_id = mgd3.store_category_id) 
                                        WHERE 
                                            mgd.language_id = '1' 
                                        AND mgd2.language_id = '2' 
                                        AND mgd3.store_id = '" . $filter_store_id . "' ");
            return $query->rows;
        } else {
            return array();
        }
    }

}
