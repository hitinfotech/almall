<?php

class ModelCmsHomepageFeaturedCategories extends Model {

    public function getHomePageFeaturedCategories($data = array()) {
        $sql = " SELECT sc.selected_categories_id, sc.image, sc.text, ctd.name category, cd.name country,  sc.country_id, sc.category_id, sc.user_id, sc.sort_order, sc.date_added, sc.date_modified FROM  " . DB_PREFIX . "selected_categories sc LEFT JOIN " . DB_PREFIX . "country c ON(sc.country_id=c.country_id) LEFT JOIN " . DB_PREFIX . "country_description cd ON(c.country_id=cd.country_id)  LEFT JOIN " . DB_PREFIX . "category ct ON(sc.category_id=ct.category_id)  LEFT JOIN " . DB_PREFIX . "category_description ctd ON(ct.category_id=ctd.category_id) WHERE cd.language_id='" . (int) $this->config->get('config_language_id') . "' AND ctd.language_id='" . (int) $this->config->get('config_language_id') . "' ";

        $sort_data = array(
            'sort_order'
        );

        if (!empty($data['filter_country_id'])) {
            $sql.=" AND sc.country_id='" . (int) $data['filter_country_id'] . "'";
        }

        if (!empty($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY `" . $data['sort'] . "` ";
        } else {
            $sql .= " ORDER BY sc.sort_order ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC ";
        } else {
            $sql .= " ASC ";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function deleteHomepageFeaturedCategories($selected_categories_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "selected_categories WHERE selected_categories_id = '" . (int) $selected_categories_id . "'");
    }

    public function addHomepageFeaturedCategories($data) {
        foreach ($data['category_related_0'] as $category_id) {

            $this->db->query("INSERT IGNORE INTO " . DB_PREFIX . "selected_categories SET `country_id`='" . (int) $data['country_id'] . "', `category_id`='" . (int) $category_id . "', `text`='" . $this->db->escape($data['text']) . "', `user_id`='" . (int) $this->user->getid() . "',`sort_order`='" . (int) $data['sort_order'] . "', `date_added`=NOW(), `date_modified`=NOW() ");

            $selected_categories_id = $this->db->getLastId();

            
            $this->load->model('tool/image');

            if (isset($data['image'])) {
                $this->db->query("UPDATE " . DB_PREFIX . "selected_categories SET image = '" . $this->db->escape($data['image']) . "' WHERE selected_categories_id = '" . (int) $selected_categories_id . "'");
                foreach ($this->config_image->get('featuredcategories') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($data['image'], $row['width'], $row['hieght']);
                    }
                }
            }

            return $selected_categories_id;
        }
    }

    public function editHomepageFeaturedCategories($selected_categories_id, $data) {
        foreach ($data['category_related_0'] as $category_id) {

            $this->db->query("UPDATE " . DB_PREFIX . "selected_categories SET `country_id`='" . (int) $data['country_id'] . "', `category_id`='" . (int) $category_id . "', `text`='" . $this->db->escape($data['text']) . "', `user_id`='" . (int) $this->user->getid() . "',`sort_order`='" . (int) $data['sort_order'] . "', `date_modified`=NOW() WHERE selected_categories_id = '" . (int) $selected_categories_id . "' ");

            if (isset($data['image'])) {
                $this->db->query("UPDATE " . DB_PREFIX . "selected_categories SET image = '" . $this->db->escape($data['image']) . "' WHERE selected_categories_id = '" . (int) $selected_categories_id . "'");
            }
        }
    }

}
