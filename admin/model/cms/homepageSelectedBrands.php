<?php

class ModelCmsHomepageSelectedBrands extends Model {

    public function getHomepageSelectedBrands($data = array()) {
        $sql = " SELECT bd.name brand, b.image, cd.name country, sb.selected_brands_id, sb.country_id, sb.brand_id, sb.user_id, sb.sort_order, sb.date_added, sb.date_modified FROM  " . DB_PREFIX . "selected_brands sb LEFT JOIN " . DB_PREFIX . "country c ON(sb.country_id=c.country_id) LEFT JOIN " . DB_PREFIX . "country_description cd ON(c.country_id=cd.country_id)  LEFT JOIN " . DB_PREFIX . "brand b ON(sb.brand_id=b.brand_id)  LEFT JOIN " . DB_PREFIX . "brand_description bd ON(b.brand_id=bd.brand_id) WHERE cd.language_id='" . (int) $this->config->get('config_language_id') . "' AND bd.language_id='" . (int) $this->config->get('config_language_id') . "' ";

        $sort_data = array(
            'sort_order'
        );

        if (!empty($data['filter_country_id'])) {
            $sql.=" AND sb.country_id='" . (int) $data['filter_country_id'] . "'";
        }

        if (!empty($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY `" . $data['sort'] . "` ";
        } else {
            $sql .= " ORDER BY sb.sort_order ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC ";
        } else {
            $sql .= " ASC ";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] <= 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function deleteHomepageSelectedBrands($selected_brands_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "selected_brands WHERE selected_brands_id = '" . (int) $selected_brands_id . "'");
    }

    public function addHomepageSelectedBrands($data) {
        foreach ($data['brand_related'] as $brand_id) {
            
            $sInsertSql = "INSERT IGNORE INTO " . DB_PREFIX . "selected_brands SET `country_id`='". (int)$data['country_id'] ."', `brand_id`='". (int)$brand_id ."', `user_id`='". (int)$this->user->getid() ."',`sort_order`='". (int)$data['sort_order'] ."', `date_added`=NOW(), `date_modified`=NOW() ";
            
            $this->db->query($sInsertSql);
            $selected_brands_id = $this->db->getLastId();

            return $selected_brands_id;
        }

        // die($sInsertSql);
    }

}
