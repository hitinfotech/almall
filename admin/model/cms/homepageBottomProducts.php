<?php

/**
 * 
 * CMS - Homepage Bottom Products Module
 * Tables: 
 * cms_homepage_bottom_products
 * 
 * @author Muayad Qanadilo [muayad.qanadilo@sayidaty.net]
 * 
 */
class ModelCmsHomepageBottomProducts extends Model {

    public function getHomepageBottomProducts($data = array()) {
        $sql = "
				SELECT 
					chtp.cms_homepage_bottom_products_id, 
					chtp.country_id, 
					chtp.product_id, 
					chtp.featured,
					chtp.user_id,
					chtp.sort_order,
					chtp.date_added,
					chtp.date_modified
				FROM 
					" . DB_PREFIX . "cms_homepage_bottom_products chtp 
				";

        $sort_data = array(
            'sort_order'
        );

        if (isset($data) && !empty($data)) {
            if (isset($data['where']) && !empty($data['where'])) {
                $sql .= " WHERE ";
                $dConditionsCounter = 0;
                if (isset($data['where']['cms_homepage_bottom_products_id']) && !empty($data['where']['cms_homepage_bottom_products_id']) && $data['where']['cms_homepage_bottom_products_id'] > 0) {
                    if ($dConditionsCounter > 0) {
                        $sql .= " AND ";
                    }
                    $sql .= " chtp.cms_homepage_bottom_products_id = '" . $data['where']['cms_homepage_bottom_products_id'] . "'";
                    $dConditionsCounter++;
                }
            }
        }

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY `" . $data['sort'] . "` ";
        } else {
            $sql .= " ORDER BY chtp.sort_order ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC ";
        } else {
            $sql .= " ASC ";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] <= 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        // die($sql);
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalHomepageBottomProducts() {
        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cms_homepage_bottom_products chtp";
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function deleteHomepageBottomProducts($cms_homepage_bottom_products_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "cms_homepage_bottom_products WHERE cms_homepage_bottom_products_id = '" . (int) $cms_homepage_bottom_products_id . "'");
    }

    public function addHomepageBottomProducts($data) {
        $sInsertSql = "
						INSERT INTO 
						" . DB_PREFIX . "cms_homepage_bottom_products (`country_id`, `product_id`, `featured`, `user_id`,`sort_order`,`date_added`, `date_modified`)
						VALUE ( '" . $data['country_id'] . "', '" . (int) $data['product_id'] . "', '" . (isset($data['featured']) ? (int) $data['featured'] : 0) . "', '" . (int) $data['user_id'] . "', '" . (int) $data['sort_order'] . "', NOW(), NOW() ) 
						";
        // die($sInsertSql);
        $this->db->query($sInsertSql);
        $cms_homepage_bottom_products_id = $this->db->getLastId();

        return $cms_homepage_slider_images_id;
    }

    public function editHomepageBottomProducts($cms_homepage_bottom_products_id, $data) {
        $sUpdateSql = "
						UPDATE 
							" . DB_PREFIX . "cms_homepage_bottom_products 
						SET 
							`country_id` = '" . $data['country_id'] . "', 
							`product_id` = '" . $data['product_id'] . "', 
							`user_id` = '" . $data['user_id'] . "', 
							`sort_order` = '" . (int) $data['sort_order'] . "', 
							`featured` = '" . (isset($data['featured']) ? (int) $data['featured'] : 0) . "', 
							`date_modified` = NOW()
						WHERE
							cms_homepage_bottom_products_id = '" . $cms_homepage_bottom_products_id . "'
						";
        // die($sUpdateSql);
        $this->db->query($sUpdateSql);
    }

}
