<?php

/**
 * 
 * CMS - Homepage Featured Categories Module
 * Tables: 
 * cms_homepage_featured_categories
 * 
 * @author Muayad Qanadilo [muayad.qanadilo@sayidaty.net]
 * 
 */
class ModelCmsHomepageFeaturedCategories extends Model {

    public function getHomepageFeaturedCategories($data = array()) {
        $sql = "
				SELECT 
					chtp.cms_homepage_featured_categories_id, 
					chtp.country_id, 
					chtp.category_id, 
					chtp.featured,
					chtp.user_id,
					chtp.sort_order,
					chtp.date_added,
					chtp.date_modified
				FROM 
					" . DB_PREFIX . "cms_homepage_featured_categories chtp 
				";

        $sort_data = array(
            'sort_order'
        );

        if (isset($data) && !empty($data)) {
            if (isset($data['where']) && !empty($data['where'])) {
                $sql .= " WHERE ";
                $dConditionsCounter = 0;
                if (isset($data['where']['cms_homepage_featured_categories_id']) && !empty($data['where']['cms_homepage_featured_categories_id']) && $data['where']['cms_homepage_featured_categories_id'] > 0) {
                    if ($dConditionsCounter > 0) {
                        $sql .= " AND ";
                    }
                    $sql .= " chtp.cms_homepage_featured_categories_id = '" . $data['where']['cms_homepage_featured_categories_id'] . "'";
                    $dConditionsCounter++;
                }
            }
        }

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY `" . $data['sort'] . "` ";
        } else {
            $sql .= " ORDER BY chtp.sort_order ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC ";
        } else {
            $sql .= " ASC ";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] <= 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        // die($sql);
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalHomepageFeaturedCategories() {
        $sql = "
			SELECT 
				COUNT(*) AS total
			FROM 
					" . DB_PREFIX . "cms_homepage_featured_categories chtp";
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function deleteHomepageFeaturedCategories($cms_homepage_featured_categories_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "cms_homepage_featured_categories WHERE cms_homepage_featured_categories_id = '" . (int) $cms_homepage_featured_categories_id . "'");
    }

    public function addHomepageFeaturedCategories($data) {
        $sInsertSql = "
						INSERT INTO 
						" . DB_PREFIX . "cms_homepage_featured_categories (`country_id`, `category_id`, `featured`, `user_id`,`sort_order`,`date_added`, `date_modified`)
						VALUE ( '" . $data['country_id'] . "', '" . (int) $data['category_id'] . "', '" . (isset($data['featured']) ? (int) $data['featured'] : 0) . "', '" . (int) $data['user_id'] . "', '" . (int) $data['sort_order'] . "', NOW(), NOW() ) 
						";
        // die($sInsertSql);
        $this->db->query($sInsertSql);
        $cms_homepage_featured_categories_id = $this->db->getLastId();

        return $cms_homepage_featured_categories_id;
    }

    public function editHomepageFeaturedCategories($cms_homepage_featured_categories_id, $data) {
        $sUpdateSql = "
						UPDATE 
							" . DB_PREFIX . "cms_homepage_featured_categories 
						SET 
							`country_id` = '" . $data['country_id'] . "', 
							`category_id` = '" . $data['category_id'] . "', 
							`user_id` = '" . $data['user_id'] . "', 
							`sort_order` = '" . (int) $data['sort_order'] . "', 
							`featured` = '" . (isset($data['featured']) ? (int) $data['featured'] : 0) . "', 
							`date_modified` = NOW()
						WHERE
							cms_homepage_featured_categories_id = '" . $cms_homepage_featured_categories_id . "'
						";
        // die($sUpdateSql);
        $this->db->query($sUpdateSql);
    }

}
