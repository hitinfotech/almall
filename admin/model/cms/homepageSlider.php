<?php
/**
 * 
 * CMS - Homepage Slider Module
 * Tables: 
 * cms_homepage_slider_to_country
 * cms_homepage_slider_images
 * cms_homepage_slider_images_details
 * 
 * @author Muayad Qanadilo [muayad.qanadilo@sayidaty.net]
 * 
 */

class ModelCmsHomePageSlider extends Model {

	public function getSliders($data = array()) {
		// echo("<pre>");print_r($data);die("<br> OK !!!");
		$sql = "
				SELECT 
					chsi.cms_homepage_slider_images_id, chsi.enable, chsi.order, chsi.image, 
					GROUP_CONCAT(chsd.description SEPARATOR ', ') AS description, 
					GROUP_CONCAT(chsd.link_url SEPARATOR ', ') AS link_url, 
					GROUP_CONCAT(chsc.country_id SEPARATOR ', ') AS slider_countries 
				FROM 
					" . DB_PREFIX . "cms_homepage_slider_images chsi
					INNER JOIN " . DB_PREFIX . "cms_homepage_slider_to_country chsc ON chsi.cms_homepage_slider_images_id = chsc.cms_homepage_slider_images_id 
					INNER JOIN " . DB_PREFIX . "cms_homepage_slider_images_details chsd ON chsi.cms_homepage_slider_images_id = chsd.cms_homepage_slider_images_id  
				";

		$sort_data = array(
			'description',
			'enable',
			'order'
		);

		if (isset($data) && !empty($data)) {
			if (isset($data['where']) && !empty($data['where'])) {
				$sql .= " WHERE ";
				$dConditionsCounter = 0;
				if (isset($data['where']['cms_homepage_slider_images_id']) && !empty($data['where']['cms_homepage_slider_images_id']) && $data['where']['cms_homepage_slider_images_id']>0) {
					if ($dConditionsCounter>0) {
						$sql .= " AND ";
					}
					$sql .= " chsi.cms_homepage_slider_images_id = '" . $data['where']['cms_homepage_slider_images_id'] ."'";
					$dConditionsCounter++;
				}
			}
		}

		$sql .= " GROUP BY chsi.cms_homepage_slider_images_id ";

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY `" . $data['sort'] . "` ";
		} else {
			$sql .= " ORDER BY chsi.order ";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC ";
		} else {
			$sql .= " ASC ";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] <= 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
		}
// die($sql);
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalSliders() {
		$sql="
			SELECT 
				COUNT(*) AS total
			FROM 
				" . DB_PREFIX . "cms_homepage_slider_images chsi
				INNER JOIN " . DB_PREFIX . "cms_homepage_slider_to_country chsc ON chsi.cms_homepage_slider_images_id = chsc.cms_homepage_slider_images_id 
				INNER JOIN " . DB_PREFIX . "cms_homepage_slider_images_details chsd ON chsi.cms_homepage_slider_images_id = chsd.cms_homepage_slider_images_id  
			GROUP BY chsi.cms_homepage_slider_images_id";
		$query = $this->db->query($sql);

        return $query->row['total'];
	}

	public function deleteSlider($cms_homepage_slider_images_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cms_homepage_slider_to_country WHERE cms_homepage_slider_images_id = '" . (int) $cms_homepage_slider_images_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "cms_homepage_slider_images_details WHERE cms_homepage_slider_images_id = '" . (int) $cms_homepage_slider_images_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "cms_homepage_slider_images WHERE cms_homepage_slider_images_id = '" . (int) $cms_homepage_slider_images_id . "'");
	}

	public function addSlider($data) {
		$sInsertSql = "
						INSERT INTO 
						" . DB_PREFIX . "cms_homepage_slider_images (`image`, `order`, `enable`, `date_added`, `date_modified`)
						VALUE ( '". $data['slider_image'] ."', '" . (int) $data['slider_order'] . "', '" . (isset($data['slider_enable']) ? (int) $data['slider_enable'] : 0) . "', NOW(), NOW() ) 
						";
		$this->db->query($sInsertSql);
		$cms_homepage_slider_images_id = $this->db->getLastId();

		if (isset($data['slider_countries']) && !empty($data['slider_countries'])) {
			foreach ($data['slider_countries'] as $country_id) {
				$sInsertSliderToCountrySql = " 
										INSERT INTO 
										" . DB_PREFIX . "cms_homepage_slider_to_country (`cms_homepage_slider_images_id`, `country_id`) 
										VALUE ('".$cms_homepage_slider_images_id."', '".$country_id."')
										";
				$this->db->query($sInsertSliderToCountrySql);
			}
		}

		$sInsertSliderDetailsValuesSql = '';
		$_aInsertSliderDetailsValuesSql = array();
		if (isset($data['slider_image_attribute']) && !empty($data['slider_image_attribute'])) {
			foreach ($data['slider_image_attribute'] as $key => $value) {
				$_aInsertSliderDetailsValuesSql[] = "('".$cms_homepage_slider_images_id."', '".$key."', '".$value['\'image_description\'']."', '".$value['\'link_url\'']."')";
			}
		}
		if (isset($_aInsertSliderDetailsValuesSql) && !empty($_aInsertSliderDetailsValuesSql)) {
			$sInsertSliderDetailsValuesSql = implode(",", $_aInsertSliderDetailsValuesSql);
		}
		$sInsertSliderDetailsSql = " 
									INSERT INTO 
									" . DB_PREFIX . "cms_homepage_slider_images_details (`cms_homepage_slider_images_id`, `language_id`, `description`, `link_url`) 
									VALUE 
									" . $sInsertSliderDetailsValuesSql;
		$this->db->query($sInsertSliderDetailsSql);
		return $cms_homepage_slider_images_id;
	}

	public function editSlider($cms_homepage_slider_images_id, $data) {
		$sUpdateSql = "
						UPDATE 
							" . DB_PREFIX . "cms_homepage_slider_images 
						SET 
							`image` = '". $data['slider_image'] ."', 
							`order` = '" . (int) $data['slider_order'] . "', 
							`enable` = '" . (isset($data['slider_enable']) ? (int) $data['slider_enable'] : 0) . "', 
							`date_modified` = NOW()
						WHERE
							cms_homepage_slider_images_id = '".$cms_homepage_slider_images_id."'
						";
		$this->db->query($sUpdateSql);

		$this->db->query("DELETE FROM " . DB_PREFIX . "cms_homepage_slider_to_country WHERE cms_homepage_slider_images_id = '" . (int) $cms_homepage_slider_images_id . "'");
		if (isset($data['slider_countries']) && !empty($data['slider_countries'])) {
			foreach ($data['slider_countries'] as $country_id) {
				$sInsertSliderToCountrySql = " 
										INSERT INTO 
										" . DB_PREFIX . "cms_homepage_slider_to_country (`cms_homepage_slider_images_id`, `country_id`) 
										VALUE ('".$cms_homepage_slider_images_id."', '".$country_id."')
										";
				$this->db->query($sInsertSliderToCountrySql);
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "cms_homepage_slider_images WHERE cms_homepage_slider_images_id = '" . (int) $cms_homepage_slider_images_id . "'");
		$sInsertSliderDetailsValuesSql = '';
		$_aInsertSliderDetailsValuesSql = array();
		if (isset($data['slider_image_attribute']) && !empty($data['slider_image_attribute'])) {
			foreach ($data['slider_image_attribute'] as $key => $value) {
				$_aInsertSliderDetailsValuesSql[] = "('".$cms_homepage_slider_images_id."', '".$key."', '".$value['\'image_description\'']."', '".$value['\'link_url\'']."')";
			}
		}
		if (isset($_aInsertSliderDetailsValuesSql) && !empty($_aInsertSliderDetailsValuesSql)) {
			$sInsertSliderDetailsValuesSql = implode(",", $_aInsertSliderDetailsValuesSql);
		}
		$sInsertSliderDetailsSql = " 
									INSERT INTO 
									" . DB_PREFIX . "cms_homepage_slider_images_details (`cms_homepage_slider_images_id`, `language_id`, `description`, `link_url`) 
									VALUE 
									" . $sInsertSliderDetailsValuesSql;
		$this->db->query($sInsertSliderDetailsSql);
	}


}

?>