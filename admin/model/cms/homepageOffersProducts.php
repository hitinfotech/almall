<?php

class ModelCmsHomePageOffersProducts extends Model {

    public function addProduct($data) {       // var_dump($data) ;die;    
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_offers WHERE country_id = '184'");

        if (isset($data['product-related_sa'])) {
            foreach ($data['product-related_sa'] as $related_ids) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_offers SET product_id = '" . (int) $related_ids . "', country_id = '184',sort_order = '0'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_offers WHERE country_id = '221'");

        if (isset($data['product-related_ae'])) {// var_dump($data) ;die;    
            foreach ($data['product-related_ae'] as $related_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_offers SET product_id = '" . (int) $related_id . "', country_id = '221',sort_order = '0'");
            }
        }
        
        $this->cache->deletekeys('catalog.product.getcustlatestproducts2');
    }

    public function getCountryProduct($country_id) {
        $query = $this->db->query("SELECT p.product_id, CONCAT(pd1.name,' ', pd2.name) name FROM product_offers p LEFT JOIN product_description pd1 ON(p.product_id=pd1.product_id ) LEFT JOIN product_description pd2 ON(p.product_id = pd2.product_id) WHERE p.country_id = '" . (int) $country_id . "' AND pd1.language_id='1' AND pd2.language_id='2' ");
        return $query->rows;
    }

    public function getProducts($data = array()) {
        $product_related_data = array();

        $query = $this->db->query("SELECT p.product_id, CONCAT(pd1.name,' ',pd2.name) name FROM " . DB_PREFIX . "product p LEFT JOIN product_description pd1 ON(p.product_id=pd1.product_id) LEFT JOIN product_description pd2 ON(p.product_id=pd2.product_id) WHERE pd1.language_id='1' AND pd2.language_id='2' AND CONCAT(pd1.name, ' ' , pd2.name) LIKE '%" . $this->db->escape($data['filter_name']) . "%' LIMIT 20 ");

        foreach ($query->rows as $result) {
            $product_related_data[] = array('product_id' => $result['product_id'], 'name' => $result['name']);
        }

        return $product_related_data;
    }

}
