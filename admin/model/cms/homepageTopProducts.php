<?php

/**
 * 
 * CMS - Homepage Top Products Module
 * Tables: 
 * cms_homepage_top_products
 * 
 * @author Muayad Qanadilo [muayad.qanadilo@sayidaty.net]
 * 
 */
class ModelCmsHomePageTopProducts extends Model {

    public function getHomepageTopProducts($data = array()) {
        /* $sql = "
          SELECT
          chtp.cms_homepage_top_products_id,
          chtp.country_id,
          chtp.product_id,
          chtp.featured,
          chtp.user_id,
          chtp.sort_order,
          chtp.date_added,
          chtp.date_modified
          FROM
          " . DB_PREFIX . "cms_homepage_top_products chtp
          "; */

        $sql = "
				SELECT 
					chtp.homepage_id, 
					chtp.country_id, 
					chtp.link_type, 
					chtp.link_id, 
					chtp.image,
					chtp.user_id,
					chtp.sort_order,
					chtp.date_added,
					chtp.date_modified
				FROM 
					" . DB_PREFIX . "homepage chtp 
				";

        $sort_data = array(
            'sort_order'
        );

        if (isset($data) && !empty($data)) {
            if (isset($data['where']) && !empty($data['where'])) {
                $sql .= " WHERE ";
                $dConditionsCounter = 0;
                if (isset($data['where']['homepage_id']) && !empty($data['where']['homepage_id']) && $data['where']['homepage_id'] > 0) {
                    if ($dConditionsCounter > 0) {
                        $sql .= " AND ";
                    }
                    $sql .= " chtp.homepage_id = '" . $data['where']['homepage_id'] . "'";
                    $dConditionsCounter++;
                }
            }
        }

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY `" . $data['sort'] . "` ";
        } else {
            $sql .= " ORDER BY chtp.sort_order ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC ";
        } else {
            $sql .= " ASC ";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] <= 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        // die($sql);
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getHomepageTopProductsTitle($data = array()) {

        $sExtraField = $sExtraJoin = $sExtraWhere = '';

        if ($data['link_type'] == 'product') {
            $sExtraField = ",pd.name as name";
            $sExtraJoin = "LEFT JOIN product_description pd ON (chtp.link_id = pd.product_id and chtp.link_type = 'product')";
            $sExtraWhere = "AND pd.language_id='" . (int) $this->config->get('config_language_id') . "'";
        } elseif ($data['link_type'] == 'tip') {
            $sExtraField = ",td.name as name";
            $sExtraJoin = "LEFT JOIN tip_description td ON (chtp.link_id = td.tip_id and chtp.link_type = 'tip')";
            $sExtraWhere = "AND td.language_id='" . (int) $this->config->get('config_language_id') . "'";
        } elseif ($data['link_type'] == 'look') {
            $sExtraField = ",ld.title as name";
            $sExtraJoin = "LEFT JOIN look_description ld ON (chtp.link_id = ld.look_id and chtp.link_type = 'look')";
            $sExtraWhere = "AND ld.language_id='" . (int) $this->config->get('config_language_id') . "'";
        }

        $sql = " 
				SELECT 
					chtp.homepage_id 
					{$sExtraField}
				FROM 
					" . DB_PREFIX . "homepage chtp  
					{$sExtraJoin}
				WHERE 
					chtp.homepage_id='" . (int) $data['homepage_id'] . "'
					{$sExtraWhere}

			";
        // die($sql);
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalHomepageTopProducts() {
        $sql = "
			SELECT 
				COUNT(*) AS total
			FROM 
					" . DB_PREFIX . "cms_homepage_top_products chtp";
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function deleteHomepageTopProducts($cms_homepage_top_products_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "cms_homepage_top_products WHERE cms_homepage_top_products_id = '" . (int) $cms_homepage_top_products_id . "'");
    }

    public function addHomepageTopProducts($data) {
        $sInsertSql = "
						INSERT INTO 
						" . DB_PREFIX . "cms_homepage_top_products (`country_id`, `product_id`, `featured`, `user_id`,`sort_order`,`date_added`, `date_modified`)
						VALUE ( '" . $data['country_id'] . "', '" . (int) $data['product_id'] . "', '" . (isset($data['featured']) ? (int) $data['featured'] : 0) . "', '" . (int) $data['user_id'] . "', '" . (int) $data['sort_order'] . "', NOW(), NOW() ) 
						";
        // die($sInsertSql);
        $this->db->query($sInsertSql);
        $cms_homepage_top_products_id = $this->db->getLastId();
        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('mall', 'image', $cms_homepage_top_products_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('gethomepage') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE homepage SET `image` = '" . $this->db->escape($image) . "' WHERE homepage_id='" . (int) $cms_homepage_top_products_id . "' ");
        }
        return $cms_homepage_slider_images_id;
    }

    public function editHomepageTopProducts($cms_homepage_top_products_id, $data) {
        $recomeneded_size = array(
            '1' => array('width' => '1500', 'height' => '980', 'size' => 'new_big'),
            '2' => array('width' => '720', 'height' => '980', 'size' => 'new_small'),
            '3' => array('width' => '720', 'height' => '980', 'size' => 'new_small'),
            '4' => array('width' => '1500', 'height' => '980', 'size' => 'new_big'),
            '5' => array('width' => '1500', 'height' => '980', 'size' => 'new_big'),
            '6' => array('width' => '720', 'height' => '980', 'size' => 'new_small'),
            '7' => array('width' => '720', 'height' => '980', 'size' => 'new_small'),
            '8' => array('width' => '1500', 'height' => '980', 'size' => 'new_big'),
            '9' => array('width' => '1500', 'height' => '980', 'size' => 'new_big'),
            '10' => array('width' => '720', 'height' => '980', 'size' => 'new_small'),
            '11' => array('width' => '720', 'height' => '980', 'size' => 'new_small'),
            '12' => array('width' => '1500', 'height' => '980', 'size' => 'new_big'),
            '13' => array('width' => '1500', 'height' => '980', 'size' => 'new_big'),
            '14' => array('width' => '720', 'height' => '980', 'size' => 'new_small'),
            '15' => array('width' => '720', 'height' => '980', 'size' => 'new_small'),
            '16' => array('width' => '1500', 'height' => '980', 'size' => 'new_big'),
        );
        
        $sUpdateSql = " UPDATE " . DB_PREFIX . "homepage 
						SET 
							`link_type` = '" . $data['link_type'] . "', 
							`link_id` = '" . $data['link_id'] . "', 
							`user_id` = '" . $data['user_id'] . "',  
							`date_modified` = 'now()'
						WHERE
							homepage_id = '" . $cms_homepage_top_products_id . "'
						";


        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('mall', 'image', $cms_homepage_top_products_id);
            if ($image) {
                $this->load->model('tool/image');
                $this->model_tool_image->resize($image, $this->config_image->get('gethomepage', $recomeneded_size[$cms_homepage_top_products_id]['size'],'width'), $this->config_image->get('gethomepage', $recomeneded_size[$cms_homepage_top_products_id]['size'],'hieght'));
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE homepage SET `image` = '" . $this->db->escape($image) . "' WHERE homepage_id='" . (int) $cms_homepage_top_products_id . "' ");
        }

        // `sort_order` = '" . (int) $data['sort_order'] . "',
        // `country_id` = '". $data['country_id'] ."', 
        // die($sUpdateSql);
        $this->db->query($sUpdateSql);
    }

}
