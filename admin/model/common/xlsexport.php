<?php

class ModelCommonXlsexport extends Model {

    private $error = array();
    
    public function  getCategories($data = array(), $language_id = 0){
        if ($language_id === 0) {
            $language_id = $this->config->get('config_language_id');
        }
        if ($language_id == 1) {
            $language_id2 = 2;
        } else {
            $language_id2 = 1;
        }
        $sql = "SELECT "
                . "cp.category_id AS category_id, "
                . "GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '>') AS name , "
                . "c1.parent_id, c1.sort_order "
                . "FROM " . DB_PREFIX . "category_path cp "
                . "LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) "
                . "LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) "
                . "LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) "
                . "LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) "
                . "WHERE cd1.language_id = '" . (int) $language_id . "' AND cd2.language_id = '" . (int) $language_id . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND CONCAT(cd2.name) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_category_id'])) {
            $sql .= " AND cp.category_id = '" . (int) $data['filter_category_id'] . "'";
        }

        if (!empty($data['filter_category_id_range'])) {
            $str = "";
            foreach ($data['filter_category_id_range'] as $id) {
                $str.= ((int) $id . ",");
            }
            $str = rtrim($str, ",");
            $sql .= " AND cp.category_id IN ({$str})";
        }

        $sql .= " GROUP BY cp.category_id";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY level ";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
//echo "<pre>"; die($sql);
//        $query = $this->db->query($sql);

        /*******EDIT LINES 3-8*******/
        $DB_Server = "localhost"; //MySQL Server    
        $DB_Username = "sayidaty"; //MySQL Username     
        $DB_Password = "sayidaty";             //MySQL Password     
        $DB_DBName = "sayidaty_net";         //MySQL Database Name  
        /*******YOU DO NOT NEED TO EDIT ANYTHING BELOW THIS LINE*******/    
        //create MySQL connection   
        
        $Connect = @mysql_connect($DB_Server, $DB_Username, $DB_Password) or die("Couldn't connect to MySQL:<br>" . mysql_error() . "<br>" . mysql_errno());
        //select database   
        $Db = @mysql_select_db($DB_DBName, $Connect) or die("Couldn't select database:<br>" . mysql_error(). "<br>" . mysql_errno());   
        //execute query 
        $result = @mysql_query($sql,$Connect) or die("Couldn't execute query:<br>" . mysql_error(). "<br>" . mysql_errno());    
        
        return $result;
    }
}