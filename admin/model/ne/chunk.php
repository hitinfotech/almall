<?php

/**
 * Class ModelNeChunk
 * @property DB $db
 */
class ModelNeChunk extends Model
{

    public function save($ready_to_share_id, $toMails)
    {

        $emails = $this->db->escape(implode(",",array_keys($toMails)));
        $this->db->query("insert into " . DB_PREFIX . "emails_chunk 
        set `created_at`= NOW(), `emails_campaign_id` = '" . (int)$ready_to_share_id . "',
            `to_mails` = '" . ($this->db->escape(base64_encode(serialize($toMails)))) . "',
            `to_plain` = '" . $emails . "'
            
            ");
        return $this->db->getLastId();
    }

    public function getReadyList()
    {
        return $this->db->query("SELECT emails_chunk.id,emails_campaign.type FROM `emails_chunk` LEFT JOIN emails_campaign ON (emails_campaign.id = emails_chunk.emails_campaign_id) where emails_chunk.status = 'NEW' order by emails_chunk.id limit 100")->rows;
    }

    public function updateStatus($id, $status)
    {
        $status = $this->db->escape($status);
        $id = (int)$id;
        return $this->db->query("update `" . DB_PREFIX . "emails_chunk` set `status`='$status' WHERE `id`='$id' ");
    }

    public function updateResult($id, $status, $success = 0, $fail = 0)
    {
        $id = (int)$id;
        $status = $this->db->escape($status);
        $success = (int)$success;
        $fail = (int)$fail;
        $this->db->query("update " . DB_PREFIX . "emails_chunk set `status`='$status',`success`='$success',`fail`='$fail' where `id`='$id' ");
    }

    public function getChunk($id)
    {
        $id = (int)$id;
        return $this->db->query("SELECT
  ec.id,
  ec.status,
  nrts.status `cstatus`,
  ec.to_mails `to`,
  nrts.from_email,
  nrts.sender_name,
  nrts.subject,
  nrts.message,
  nrts.id `cid`
FROM
  emails_chunk ec
INNER JOIN
  emails_campaign nrts ON(ec.emails_campaign_id = nrts.id)
WHERE
  ec.id = '$id'")->row;

    }
}
