<?php
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

class ModelNeDraft extends Model
{

    public function getTotal($data = array())
    {
        $sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "ne_draft` WHERE 1=1";

        if (isset($data['filter_date']) && !is_null($data['filter_date'])) {
            $sql .= " AND DATE(datetime) = DATE('" . $this->db->escape($data['filter_date']) . "')";
        }

        if (isset($data['filter_subject']) && !is_null($data['filter_subject'])) {
            $sql .= " AND LCASE(subject) LIKE '" . $this->db->escape(mb_strtolower($data['filter_subject'], 'UTF-8')) . "%'";
        }

        if (isset($data['filter_to']) && !is_null($data['filter_to'])) {
            $sql .= " AND `to` = '" . $this->db->escape($data['filter_to']) . "'";
        }

        if (isset($data['filter_store']) && !is_null($data['filter_store'])) {
            $sql .= " AND `store_id` = '" . (int)$data['filter_store'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getList($data = array())
    {
        if ($data) {
            $sql = "SELECT `draft_id`, `subject`, `datetime`, `to`, `store_id` FROM " . DB_PREFIX . "ne_draft WHERE 1=1";

            if (isset($data['filter_date']) && !is_null($data['filter_date'])) {
                $sql .= " AND DATE(datetime) = DATE('" . $this->db->escape($data['filter_date']) . "')";
            }

            if (isset($data['filter_subject']) && !is_null($data['filter_subject'])) {
                $sql .= " AND LCASE(subject) LIKE '" . $this->db->escape(mb_strtolower($data['filter_subject'], 'UTF-8')) . "%'";
            }

            if (isset($data['filter_to']) && !is_null($data['filter_to'])) {
                $sql .= " AND `to` = '" . $this->db->escape($data['filter_to']) . "'";
            }

            if (isset($data['filter_store']) && !is_null($data['filter_store'])) {
                $sql .= " AND `store_id` = '" . (int)$data['filter_store'] . "'";
            }

            $sort_data = array(
                'draft_id',
                'subject',
                'datetime',
                'to',
                'store_id'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY `" . $data['sort'] . "`";
            } else {
                $sql .= " ORDER BY datetime";
            }

            if (isset($data['order']) && ($data['order'] == 'ASC')) {
                $sql .= " ASC";
            } else {
                $sql .= " DESC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            $results = $query->rows;

            return $results;
        } else {
            return false;
        }
    }

    public function detail($draft_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ne_draft WHERE draft_id = '" . (int)$draft_id . "'");
        $data = $query->row;

        if ($data) {
            $path = dirname(DIR_DOWNLOAD) . DIRECTORY_SEPARATOR . 'attachments' . DIRECTORY_SEPARATOR . 'draft_' . $data['draft_id'];
            if (file_exists($path) && is_dir($path)) {
                $data['attachments'] = $this->attachments($path);
            } else {
                $data['attachments'] = array();
            }
        }

        $data['customer'] = unserialize($data['customer']);
        $data['affiliate'] = unserialize($data['affiliate']);
        $data['product'] = unserialize($data['product']);
        $data['defined_products'] = unserialize($data['defined_products']);
        $data['defined_products_more'] = unserialize($data['defined_products_more']);
        //$data['defined_category'] = unserialize($data['defined_categories']);
        $data['skip_out_of_stock'] = unserialize($data['skip_out_of_stock']);
        $data['marketing_list'] = unserialize($data['marketing_list']);
        $data['visited_category'] = unserialize($data['visited_category']);
        $data['purchased_category'] = unserialize($data['purchased_category']);
        $data['defined_category'] = unserialize($data['defined_category']);

        return $data;
    }

    public function save($data)
    {




        $toSearlize = ['marketing_list', 'defined_products', 'defined_products_more', 'skip_out_of_stock',
            'customer', 'affiliate', 'product','visited_category','defined_category','purchased_category'];
        $col = [
            'to', 'subject', 'message', 'utm_campaign', 'text_message', 'text_message_products', 'store_id', 'attachments_id', 'template_id', 'language_id',
            'currency', 'gender', 'location', 'defined', 'special', 'latest', 'popular', 'categories', 'customer_group_id', 'customer_group_only_subscribers', 'only_selected_language', 'customer', 'affiliate', 'product',
            'defined_product_text','defined_category' ,'defined_products', 'defined_products_more', 'defined_categories', 'skip_out_of_stock', 'defined_category_limit', 'defined_category_sort', 'defined_category_order', 'marketing_list', 'regency', 'frequency', 'life_time_value', 'visited_category', 'purchased_category', 'newsletter_id'
        ];

        $data = array_change_key_case($data, CASE_LOWER);

        $query = "";
        foreach ($data as $key => $value) {
            if(in_array($key,$col) && !empty($value)){
            $value = in_array($key, $toSearlize) ? serialize($value) : $this->db->escape($value);//$this->db->escape($value);
            $query .= " `$key` = '$value', ";
            }
        }

        if (!empty($query)) {
            $query = rtrim(trim( $query),",");
            if (isset($data['id']) && $data['id']) {
                $this->db->query("UPDATE " . DB_PREFIX . "ne_draft SET  $query WHERE  draft_id = '" . (int)$data['id'] . "'");
            } else {
                $this->db->query("INSERT INTO " . DB_PREFIX . "ne_draft SET  $query ");
                $data['id'] = $this->db->getLastId();
            }
        } else {
            $data['id'] = 0;
        }
        return $data['id'];
    }

    public function delete($draft_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "ne_draft WHERE draft_id = '" . (int)$draft_id . "'");

        $path = dirname(DIR_DOWNLOAD) . DIRECTORY_SEPARATOR . 'attachments' . DIRECTORY_SEPARATOR . 'draft_' . $draft_id;
        if (file_exists($path) && is_dir($path)) {
            $this->rrmdir($path);
        }
    }

    private function rrmdir($dir)
    {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file)) {
                $this->rrmdir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dir);
    }

    private function attachments($dir)
    {
        $attachments = array();

        $files = (array)glob($dir);
        if (!empty($files))
            foreach ($files as $file) {
                $attachments[] = array(
                    'filename' => basename($file),
                    'path' => $file
                );
            }

        return $attachments;
    }
}
