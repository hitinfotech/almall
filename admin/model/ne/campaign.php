<?php

/**
 * Class ModelNeReadyToShare
 * @property DB $db;
 */
class ModelNeCampaign extends Model
{

    public $error = '';

    public function save($type, $from_email, $sender_name, $subject, $message, $attachments = null, $draft_id = null, $dateToSend = false, $campId = null)
    {
        if (empty($type) || empty($from_email) || empty($sender_name) || empty($subject) || empty($message)) {
            $this->error = "empty param";
            return false;
        }
        $type = $this->db->escape($type);
        $from_email = $this->db->escape($from_email);
        $sender_name = $this->db->escape($sender_name);
        $subject = $this->db->escape($subject);
        $message = $this->db->escape($message);

        if ($campId != null) {
            $query = "UPDATE `" . DB_PREFIX . "emails_campaign` set ";
        } else {
            $query = "INSERT INTO `" . DB_PREFIX . "emails_campaign` set ";
        }
        if ($attachments != null && !empty($attachments)) {
            $attachments = $this->db->escape(serialize($attachments));
            $query .= " `attachments` = '$attachments', ";
        }
        if ($draft_id != null && !empty($draft_id)) {
            $draft_id = (int)$draft_id;
            $query .= " `draft_id`='$draft_id', ";
        }
        $query .= " `type` = '$type', ";
        $query .= " `from_email` = '$from_email', ";
        $query .= " `sender_name` = '$sender_name', ";
        $query .= " `subject` = '$subject', ";
        $query .= " `message` = '$message', ";
        if ($dateToSend) {
            $query .= " `status`='SCHEDULED', ";
            $query .= " `send_date`='$dateToSend', ";

        }
        $query .= " `created_date` = NOW() ";
        if ($campId != null) {
            $query .= " WHERE id='" . (int)$campId . "' ";
        }
        return $this->db->query($query);

    }

    public function getCampaign($status, $type = null)
    {
        $status = $this->db->escape($status);
        $query = "select * from `" . DB_PREFIX . "emails_campaign` ec left join " . DB_PREFIX . "ne_draft nd on (nd.draft_id =  ec.draft_id) where 1=1 ";
        $query .= " AND ec.status= '$status' ";
        if ($type != null && !empty($type)) {
            $type = $this->db->escape($type);
            $query .= " AND ec.type= '$type' ";
        }
        $query .= " ORDER BY ec.id LIMIT 50 ";
        $output = $this->db->query($query);
        return $output ? $output->rows : false;
    }

    public function updateStatus($id, $status)
    {
        $status = $this->db->escape($status);
        $id = (int)$id;
        return $this->db->query("update `" . DB_PREFIX . "emails_campaign` set `status`='$status' WHERE `id`='$id' ");
    }

    public function getById($id)
    {
        $id = (int)$id;
        return $this->db->query("select * from  `" . DB_PREFIX . "emails_campaign` where `id`='$id' ")->row;
    }

    public function removeById($id)
    {
        $id = (int)$id;
        $this->db->query("delete from `" . DB_PREFIX . "emails_chunk` where `emails_campaign_id`='$id'");
        $this->db->query("delete from `" . DB_PREFIX . "emails_campaign` where `id`='$id' ");
    }

    public function pause($id)
    {
        $id = (int)$id;
        $this->db->query("update `" . DB_PREFIX . "emails_chunk` set `status`='PAUSED' where `emails_campaign_id`='$id' and `status` NOT IN('INPROGRESS','DONE')  ");
        $this->db->query("update `" . DB_PREFIX . "emails_campaign` set `status`='PAUSED' where `id`='$id' and `status` NOT IN ('COMPLETED','FETCHINGMAILS') ");
    }

    public function resume($id)
    {
        $id = (int)$id;
        $this->db->query("update `" . DB_PREFIX . "emails_chunk` set `status`='NEW' where `emails_campaign_id`='$id' and `status`='PAUSED'  ");

        $count = $this->db->query("select count(*) as `c` from  `" . DB_PREFIX . "emails_chunk` where  `emails_campaign_id`='$id'")->row;
        if ($count['c'] > 0) {
            $status = 'READY';
        } else {
            $status = 'NEW';
        }

        $this->db->query("update `" . DB_PREFIX . "emails_campaign` set `status`='$status' where `id`='$id' and `status` = 'PAUSED' ");


    }

    public function getList($start, $page)
    {
        $start = (int)$start;
        $page = (int)$page;
        return $this->db->query("SELECT
  ecm.id,
  ecm.created_date,
  ecm.status,
  (SUM(IFNULL(ech.fail,0)) + SUM(IFNULL(ech.success,0))) 'total',
  COUNT(ech.id) 'chunks',
  SUM(IFNULL(ech.fail,0)) 'fail',
  SUM(IFNULL(ech.success,0)) 'success'
FROM
  emails_campaign ecm
LEFT JOIN
  emails_chunk ech ON(
    ecm.id = ech.emails_campaign_id
  )
  WHERE ecm.status NOT IN ('SCHEDULED')
GROUP BY
  ecm.id 
ORDER BY 
  ecm.id desc limit $start,$page
  ")->rows;
    }

    public function count()
    {
        return $this->db->query("SELECT count(*) `total` from emails_campaign WHERE emails_campaign.status NOT IN ('SCHEDULED')")->row['total'];
    }


    public function getScheduleList($data)
    {
        $start = (int)$data['start'];
        $limit = (int)$data['limit'];
        $sort = " ecm.id";

        $order = "DESC";
        $cond = "";
        if (isset($data['sort']) && in_array($data['sort'], ['name', 'to', 'send_date'])) {
            if ($data['sort'] == 'send_date') {
                $sort = 'ecm.send_date';
            } else {
                $sort = 'nd.' . (($data['sort']) == 'name' ? 'subject' : $data['sort']);
            }
        }
        if (isset($data['order']) && in_array($data['order'], ['DESC', 'ASC'])) {
            $order = $data['order'];
        }
        $orderBy = " " . $sort . ' ' . $order . ' ';
//       echo $orderBy;die;
        if(isset($data['filter_to'])){
            $cond .= " AND nd.to='".$this->db->escape($data['filter_to'])."' ";
        }
        if(isset($data['filter_name'])){
            $cond .= " AND nd.subject like '%".$this->db->escape($data['filter_name'])."%' ";
        }



        return $this->db->query("SELECT
  ecm.id,
  ecm.created_date,
  ecm.status,
  ecm.send_date,
  nd.to,
  nd.subject 'name'
FROM
  emails_campaign ecm
INNER JOIN
  ne_draft nd ON(nd.draft_id = ecm.draft_id)
WHERE
  ecm.status IN('SCHEDULED') ".$cond."
GROUP BY
  ecm.id
ORDER BY
   ".$orderBy." 
LIMIT  $start,$limit
  ")->rows;
    }

    public function getScheduleCount($data)
    {
        return $this->db->query("SELECT count(*) `total` from emails_campaign WHERE emails_campaign.status  IN ('SCHEDULED')")->row['total'];
    }

    public function getDraftId($id)
    {
        $q = $this->db->query("SELECT `draft_id` from `emails_campaign` WHERE id='" . (int)$id . "' ");
        return $q->row['draft_id'];
    }


}