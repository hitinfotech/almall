<?php

class ModelDataimportCategory extends Model {

    public function addFile($data) {
        
        $row = 1;
        
        if (($handle = fopen($data["category"]["tmp_name"], "r")) !== FALSE) {
            while (($arr = fgetcsv($handle, 10000, ",")) !== FALSE) {
                $num = count($arr);
                //echo "<p> $num fields in line $row: <br /></p>\n";

                $last_cat = "";
                if ($row > 1) {
                    for ($c = 0; $c < $num; $c++) {
                        //echo $arr[$c] . "<br />\n";

                        switch ($c) {
                            case 0:
                                $this->addCategory($arr[$c]);
                                break;

                            case 1:
                                $this->addCategory($arr[$c], $last_cat);
                                break;

                            case 2:
                                $this->addCategory($arr[$c], $last_cat);
                                break;

                            case 3:
                                $this->addOption($arr[$c]);
                                break;

                            case 4:
                                $this->addOptionValue($arr[$c], $last_cat);
                                break;

                            default :
                                die("error: no cases ==>row:" . $row . " ==>category:" . $arr[$c] . " ==> last_cat:" . $last_cat);
                        }
                        $last_cat = $arr[$c];
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        //die(var_dump($data));
    }

    public function addCategory($category_info = array()) {
        $data = array(
            'category_description' => array(
                '1' => array(
                    'name' => $category_info['name'],
                    'description' => $category_info['description'],
                    'meta_title' => $category_info['meta_title'],
                    'meta_description' => $category_info['meta_description'],
                    'meta_keyword' => $category_info['meta_keyword']
                ),
                '2' => array(
                    'name' => $category_info['name'],
                    'description' => $category_info['description'],
                    'meta_title' => $category_info['meta_title'],
                    'meta_description' => $category_info['meta_description'],
                    'meta_keyword' => $category_info['meta_keyword']
                )
            ),
            'category_layout' => array(
                0 => 3
            ),
            'category_store' => array(0),
            'column' => 1,
            'sort_order' => 0,
            'status' => 1,
            'parent_id' => 0,
            'top' => (strlen($parent) > 0 ? 1 : 0)
        );

        $result = $this->db->query("SELECT category_id FROM category_description WHERE name='" . $this->db->escape($name) . "' AND language_id='2'");
        if (!$result->num_rows) {

            if ($parent) {
                $result = $this->db->query("SELECT category_id FROM category_description WHERE name='" . $this->db->escape($parent) . "' AND language_id='2'");
                if ($result->num_rows) {
                    $data['parent_id'] = $result->row['category_id'];
                }
            }

            $this->db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int) $data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int) $data['top'] : 0) . "', `column` = '" . (int) $data['column'] . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW(), date_added = NOW()");

            $category_id = $this->db->getLastId();

            if (isset($data['image'])) {
                $this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int) $category_id . "'");
            }

            foreach ($data['category_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int) $category_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
            }

// MySQL Hierarchical Data Closure Table Pattern
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $data['parent_id'] . "' ORDER BY `level` ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int) $category_id . "', `path_id` = '" . (int) $result['path_id'] . "', `level` = '" . (int) $level . "'");

                $level++;
            }

            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int) $category_id . "', `path_id` = '" . (int) $category_id . "', `level` = '" . (int) $level . "'");

            if (isset($data['category_filter'])) {
                foreach ($data['category_filter'] as $filter_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int) $category_id . "', filter_id = '" . (int) $filter_id . "'");
                }
            }

            if (isset($data['category_store'])) {
                foreach ($data['category_store'] as $store_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int) $category_id . "', store_id = '" . (int) $store_id . "'");
                }
            }

            // Set which layout to use with this category
            if (isset($data['category_layout'])) {
                foreach ($data['category_layout'] as $store_id => $layout_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int) $category_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout_id . "'");
                }
            }

            if (isset($data['keyword'])) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int) $category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
            }

            $this->cache->delete('category');
        } else {
            $category_id = $result->row['category_id'];
        }
        return $category_id;
    }

    public function addOption($name) {
        $result = $this->db->query("SELECT option_id FROM option_description WHERE name='" . $this->db->escape($name) . "' AND language_id='2'");
        if (!$result->num_rows) {
            $data = array(
                'type' => 'radio',
                'sort_order' => '0',
                'option_description' => array(
                    '1' => array('name' => $name),
                    '2' => array('name' => $name)
                )
            );

            $this->db->query("INSERT INTO `" . DB_PREFIX . "option` SET type = '" . $this->db->escape($data['type']) . "', sort_order = '" . (int) $data['sort_order'] . "'");

            $option_id = $this->db->getLastId();

            foreach ($data['option_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . (int) $option_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "'");
            }

            if (isset($data['option_value'])) {
                foreach ($data['option_value'] as $option_value) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int) $option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $option_value['sort_order'] . "'");

                    $option_value_id = $this->db->getLastId();

                    foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int) $option_value_id . "', language_id = '" . (int) $language_id . "', option_id = '" . (int) $option_id . "', name = '" . $this->db->escape($option_value_description['name']) . "'");
                    }
                }
            }
        } else {
            $option_id = $result->row['option_id'];
        }
        return $option_id;
    }

    public function addOptionValue($name, $parent = "") {
        $result = $this->db->query("SELECT option_value_id FROM option_value_description WHERE name='" . $this->db->escape($name) . "' AND language_id='2'");
        if (!$result->num_rows) {
            $option_value_id = 0;
            $data = array(
                'type' => 'radio',
                'sort_order' => '0',
                'option_description' => array(
                    '1' => array('name' => $name),
                    '2' => array('name' => $name)
                ),
                'option_value' => array(
                    array(
                        'image' => '',
                        'sort_order' => '0',
                        'option_value_description' => array(
                            '1' => array(
                                'name' => $name
                            ),
                            '2' => array(
                                'name' => $name
                            )
                        )
                    )
                )
            );

            $option_id = $this->addOption($parent);

            if (isset($data['option_value'])) {
                foreach ($data['option_value'] as $option_value) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int) $option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $option_value['sort_order'] . "'");

                    $option_value_id = $this->db->getLastId();

                    foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int) $option_value_id . "', language_id = '" . (int) $language_id . "', option_id = '" . (int) $option_id . "', name = '" . $this->db->escape($option_value_description['name']) . "'");
                    }
                }
            }
        } else {
            $option_value_id = $result->row['option_value_id'];
        }
        return $option__value_id;
    }

}