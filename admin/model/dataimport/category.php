<?php

class ModelDataimportCategory extends Model {

    private $error = array();

    public function addFile($file_data) {

        $max_level = 0;
        $row_no = 1;
        $categories = array();

        if (($handle = fopen($file_data["category"]["tmp_name"], "r")) !== FALSE) {
            while (($arr = fgetcsv($handle, 10000, ",")) !== FALSE) {
                //$num = count($arr);
                //echo "<p> $num fields in line $row: <br /></p>\n";

                $trim_arr = $this->trim_all($arr);

                if ($row_no > 3) {
                    $data = array(
                        'index_excel' => $row_no,
                        'category_description' => array(
                            '2' => array(
                                'name' => $trim_arr[2],
                                'description' => $trim_arr[5],
                                'meta_title' => $trim_arr[7],
                                'meta_description' => $trim_arr[9],
                                'meta_keyword' => $trim_arr[11]
                            ),
                            '1' => array(
                                'name' => $trim_arr[3],
                                'description' => $trim_arr[6],
                                'meta_title' => $trim_arr[8],
                                'meta_description' => $trim_arr[10],
                                'meta_keyword' => $trim_arr[12]
                            )
                        ),
                        'category_layout' => array(
                            0 => 3
                        ),
                        'keyword' => $trim_arr[13],
                        'top' => (strtolower($trim_arr[14]) == "yes" ? 1 : 0),
                        'category_store' => array(0),
                        'column' => 1,
                        'sort_order' => (int) $trim_arr[15],
                        'status' => (strtolower($trim_arr[16]) == "enabled" ? 1 : 0),
                        'parent_id' => 0,
                        'parent_path' => $trim_arr[4],
                    );

                    $data['level'] = count(explode('>', $data['parent_path']));
                    $max_level = ($data['level'] > $max_level?$data['level']:$max_level);
                    //echo "<pre>";die(print_r($data));
                    $categories[] = $data;
                }
                $row_no++;
            }
            fclose($handle);
        }

        //$sort_categories = $this->array_sort($categories, 'level', SORT_ASC);
        
        /*
        $fp = fopen('/var/www/sayidaty_net/file.csv', 'w');
        foreach ($sort_categories as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
        */
        /*
         * $count = count($sort_categories);

        for ($i = 0; $i < $count; $i++) {

            $category = $sort_categories[$i];

            $category['parent_id'] = (int)$this->create_parent($data);

            if ($this->validate_category($category)) {

                $added = $this->addCategory($category);

                if ($added === 0) {
                    echo "error ocurred";
                    print_r($categories);
                    die;
                }
            } else {
                echo "<pre>"; die(print_r($category));
            }
        }
         */
       
        
        for($i = 0; $i <= $max_level; $i++){
            
            $sort_categories = $this->array_manual($categories, 'level', $i);
            
            foreach($sort_categories as $sort_categories_row){
                
                $sort_categories_row['parent_id'] = (int)$this->create_parent($sort_categories_row);
                
                if ($this->validate_category($sort_categories_row)) {
                    
                    $added = $this->addCategory($sort_categories_row);
                    
                    if ($added === 0) {
                        
                        echo "error ocurred";
                        print_r($sort_categories_row);
                        die;
                    }
                } else {
                    echo "<pre>"; die(print_r($sort_categories_row));
                }
            }
        }


        if (!empty($this->error)) {
            return $this->error;
        }

        //die(var_dump($this->error));
    }

    private function validate_category($data = array()) {
        $error = array();

        if (!empty($data['parent_path']) && (int)$data['parent_id'] <= 0) {
            $error['warning'] = "CSV file : Row #{ {$data['index_excel']}: } Error on {{$data['category_description']['1']['name']}} No Parent Name";
        }

        if (!empty($error)) {
            $this->error[] = $error;
            return FALSE;
        }

        return TRUE;
    }

    private function get_category_id($english_name, $parent_id = 0) {

        return $this->category_exists($english_name, $parent_id);
    }

    public function addCategory($data = array()) {

        if ($this->category_exists($data['category_description']['1']['name'], $data['parent_id'])) {

            $category_id = $this->get_category_id($data);

            $this->update_category($category_id, $data);
        } else {

            $this->db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int) $data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int) $data['top'] : 0) . "', `column` = '" . (int) $data['column'] . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW(), date_added = NOW()");

            $category_id = $this->db->getLastId();

            if (isset($data['image'])) {
                $this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int) $category_id . "'");
            }

            foreach ($data['category_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int) $category_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
            }

            // MySQL Hierarchical Data Closure Table Pattern
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $data['parent_id'] . "' ORDER BY `level` ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int) $category_id . "', `path_id` = '" . (int) $result['path_id'] . "', `level` = '" . (int) $level . "'");

                $level++;
            }

            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int) $category_id . "', `path_id` = '" . (int) $category_id . "', `level` = '" . (int) $level . "'");

            if (isset($data['category_filter'])) {
                foreach ($data['category_filter'] as $filter_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int) $category_id . "', filter_id = '" . (int) $filter_id . "'");
                }
            }

            if (isset($data['category_store'])) {
                foreach ($data['category_store'] as $store_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int) $category_id . "', store_id = '" . (int) $store_id . "'");
                }
            }

            // Set which layout to use with this category
            if (isset($data['category_layout'])) {
                foreach ($data['category_layout'] as $store_id => $layout_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int) $category_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout_id . "'");
                }
            }

            if (isset($data['keyword'])) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int) $category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
            }

            $this->cache->delete('category');
        }

        return $category_id;
    }

    private function category_exists($english_name, $parent_id = 0) {

        $category_row = $this->db->query("SELECT c.category_id FROM category_description cd LEFT JOIN category c ON(c.category_id=cd.category_id) WHERE cd.name='" . $this->db->escape($english_name) . "' AND cd.language_id='1' AND c.parent_id='" . (int) $parent_id . "'");
        return $category_row->num_rows ? (int) $category_row->row['category_id'] : FALSE;
    }

    private function update_category($category_id, $data = array()) {

        //$this->event->trigger('pre.admin.category.edit', $data);

        $this->db->query("UPDATE " . DB_PREFIX . "category SET parent_id = '" . (int) $data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int) $data['top'] : 0) . "', `column` = '" . (int) $data['column'] . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW() WHERE category_id = '" . (int) $category_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int) $category_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int) $category_id . "'");

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int) $category_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE path_id = '" . (int) $category_id . "' ORDER BY level ASC");

        if ($query->rows) {
            foreach ($query->rows as $category_path) {
                // Delete the path below the current one
                $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $category_path['category_id'] . "' AND level < '" . (int) $category_path['level'] . "'");

                $path = array();

                // Get the nodes new parents
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $data['parent_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Get whats left of the nodes current path
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $category_path['category_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Combine the paths with a new level
                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int) $category_path['category_id'] . "', `path_id` = '" . (int) $path_id . "', level = '" . (int) $level . "'");

                    $level++;
                }
            }
        } else {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $category_id . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $data['parent_id'] . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int) $category_id . "', `path_id` = '" . (int) $result['path_id'] . "', level = '" . (int) $level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int) $category_id . "', `path_id` = '" . (int) $category_id . "', level = '" . (int) $level . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int) $category_id . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int) $category_id . "', filter_id = '" . (int) $filter_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int) $category_id . "'");

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int) $category_id . "', store_id = '" . (int) $store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int) $category_id . "'");

        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int) $category_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int) $category_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int) $category_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('category');

        $this->event->trigger('post.admin.category.edit', $category_id);
    }

    private function create_parent($data) {

        // parent_path example : Men>Clothing>Beach wear

        $parent_info = explode(">", $data['parent_path']);
        
        $level = count($parent_info); // 3

        $parent_id = $parent_parent_id = 0;

        for ($i = 0; $i < $level; $i++) {

            $parent_name = $parent_info[$i];

            $parent_parent_id = $parent_id;

            $sql = "SELECT c.category_id AS category_id FROM category_description cd LEFT JOIN category c ON(cd.category_id=c.category_id) WHERE name='" . $this->db->escape($parent_name) . "' AND c.parent_id='" . (int) $parent_id . "' AND cd.language_id='1'";

            $query = $this->db->query($sql);

            $parent_id = $query->row['category_id'];
            
            if($data['parent_path']=="Women>Skin Care"){
                
            }
            if ($i == ($level - 2)) {
                if ($query->num_rows == 0) {
                    die("level=> {$level} <br>i=> {$i}<br><br>".$sql);
                    return FALSE;
                } else {
                    return $parent_id;
                }
            }

            
        }
        return FALSE;
    }

    public function addOption($name) {
        $result = $this->db->query("SELECT option_id FROM option_description WHERE name='" . $this->db->escape($name) . "' AND language_id='2'");
        if (!$result->num_rows) {
            $data = array(
                'type' => 'radio',
                'sort_order' => '0',
                'option_description' => array(
                    '1' => array('name' => $name),
                    '2' => array('name' => $name)
                )
            );

            $this->db->query("INSERT INTO `" . DB_PREFIX . "option` SET type = '" . $this->db->escape($data['type']) . "', sort_order = '" . (int) $data['sort_order'] . "'");

            $option_id = $this->db->getLastId();

            foreach ($data['option_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . (int) $option_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "'");
            }

            if (isset($data['option_value'])) {
                foreach ($data['option_value'] as $option_value) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int) $option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $option_value['sort_order'] . "'");

                    $option_value_id = $this->db->getLastId();

                    foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int) $option_value_id . "', language_id = '" . (int) $language_id . "', option_id = '" . (int) $option_id . "', name = '" . $this->db->escape($option_value_description['name']) . "'");
                    }
                }
            }
        } else {
            $option_id = $result->row['option_id'];
        }
        return $option_id;
    }

    public function addOptionValue($name, $parent = "") {
        $result = $this->db->query("SELECT option_value_id FROM option_value_description WHERE name='" . $this->db->escape($name) . "' AND language_id='2'");
        if (!$result->num_rows) {
            $option_value_id = 0;
            $data = array(
                'type' => 'radio',
                'sort_order' => '0',
                'option_description' => array(
                    '1' => array('name' => $name),
                    '2' => array('name' => $name)
                ),
                'option_value' => array(
                    array(
                        'image' => '',
                        'sort_order' => '0',
                        'option_value_description' => array(
                            '1' => array(
                                'name' => $name
                            ),
                            '2' => array(
                                'name' => $name
                            )
                        )
                    )
                )
            );

            $option_id = $this->addOption($parent);

            if (isset($data['option_value'])) {
                foreach ($data['option_value'] as $option_value) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int) $option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $option_value['sort_order'] . "'");

                    $option_value_id = $this->db->getLastId();

                    foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int) $option_value_id . "', language_id = '" . (int) $language_id . "', option_id = '" . (int) $option_id . "', name = '" . $this->db->escape($option_value_description['name']) . "'");
                    }
                }
            }
        } else {
            $option_value_id = $result->row['option_value_id'];
        }
        return $option_value_id;
    }

    private function array_manual($array = array(), $field, $value = 0){
        $new_array = array();
        
        foreach ($array as $subarray){
            if($subarray[$field] == $value){
                $new_array[] = $subarray;
            }
        }
                
        return $new_array;
    }

    private function array_sort($array, $on, $order = SORT_ASC) {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                //$new_array[$k] = $array[$k];
                $new_array[] = $array[$k];
            }
        }

        return $new_array;
    }
    
    private function trim_all($data = array()) {

        $new_array = array();
        if (is_array($data)) {
            foreach ($data as $key => $val) {
                if (is_array($val)) {
                    //$data[$key] = $this->trim_all($val);
                    $new_array[$key] = $this->trim_all($val);
                } else {
                    //$data[$key] = trim($val);
                    $new_array[$key] = trim($val);
                }
            }
        } else {
            //$data = trim($data);
            $new_array = trim($data);
        }

        return $new_array;
    }

}
