<?php

class ModelHomepageHomepage extends Model {

    public function getSetting($code, $page_id) {
        $setting_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homepage_new WHERE page_id = '" . (int) $page_id . "'  AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $setting_data[$result['key']] = $result['value'];
            } else {
                $setting_data[$result['key']] = json_decode($result['value'], true);
            }
        }

        return $setting_data;
    }

    public function editSetting($code, $data, $page_id) {

        $this->db->query("DELETE FROM `" . DB_PREFIX . "page_settings` WHERE page_id = '" . (int) $page_id . "'");
        if (isset($data['page_country'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "page_settings` (page_id, country_id,language_id,code) VALUES ";
            foreach ($data['page_country'] as $key => $value) {
                $str .= ' (' . $page_id . ',' . $key . ',' . $data['coutnry_langauge'] . ',"homepage") ,';
            }
            $this->db->query(" INSERT IGNORE INTO cache   SET `type`='product.getcustlatestproducts2." . (int) $key . "." . (int) $this->config->get('config_language_id') . "',  user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
            $this->db->query(" INSERT IGNORE INTO cache   SET `type`='product.getcustoffersproducts2." . (int) $key . "." . (int) $this->config->get('config_language_id') . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
        }
        $this->db->query(trim($str, ','));

        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_latest` WHERE page_id = '" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "'");
        if (isset($data['product-related'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "product_latest` (page_id,code, product_id,sort_order) VALUES ";
            foreach ($data['product-related'] as $key => $value) {
                $str .= ' (' . $page_id . ',"' . $this->db->escape($code) . '",' . $value . ',0 ),';
            }
            $this->db->query(trim($str, ','));
        }


        $str = '';
        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_offers` WHERE page_id = '" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "'");
        if (isset($data['product-related-offers'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "product_offers` (page_id,code, product_id,sort_order) VALUES ";
            foreach ($data['product-related-offers'] as $key => $value) {
                $str .= ' (' . $page_id . ',"' . $this->db->escape($code) . '",' . $value . ',0 ),';
            }
            $this->db->query(trim($str, ','));
        }


        $this->db->query("DELETE FROM `" . DB_PREFIX . "homepage_new` WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($this->request->files as $key => $file) {
            /* if (isset($this->request->files['homepage_block1_image1']) && $this->request->files['homepage_block1_image1']["error"] == 0) {

              $image = $this->upload_file('Homepage', 'homepage_block1_image1', 1000);
              if ($image) {
              $this->load->model('tool/image');
              $this->model_tool_image->resize($image, $this->config_image->get('gethomepage', 'homepage_block1_image1', 'width'), $this->config_image->get('gethomepage', 'homepage_block1_image1', 'hieght'));
              $data['homepage_block1_image1'] = $image;
              }
              } */
            if (is_array($file) && $file["error"] == 0) {

                $image = $this->upload_file('Homepage', $key, rand(1000, 9000));
                if ($image) {
                    $this->load->model('tool/image');
                    $this->model_tool_image->resize($image, $this->config_image->get('gethomepage', $key, 'width'), $this->config_image->get('gethomepage', $key, 'hieght'));
                    $data[$key] = $image;
                }
            }
        }

        foreach ($data as $key => $value) {
            if (substr($key, 0, strlen($code)) == $code) {
                if (!is_array($value)) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "homepage_new SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "homepage_new SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value)) . "', serialized = '1'");
                }
            }
        }
    }

    public function addSetting($code, $data) {
        $page_id = $this->db->query("SELECT ifnull(max(page_id),0) as max FROM `" . DB_PREFIX . "page_settings` ")->row['max'];
        $page_id++;
        if (isset($data['page_country'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "page_settings` (page_id, country_id,language_id,code) VALUES ";
            foreach ($data['page_country'] as $key => $value) {
                $str .= ' (' . $page_id . ',' . $key . ',' . $data['coutnry_langauge'] . ',"homepage") ,';

                $this->db->query(" INSERT IGNORE INTO cache   SET `type`='product.getcustlatestproducts2." . (int) $key . "." . (int) $this->config->get('config_language_id') . "',  user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
                $this->db->query(" INSERT IGNORE INTO cache   SET `type`='product.getcustoffersproducts2." . (int) $key . "." . (int) $this->config->get('config_language_id') . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
            }
        }
        $this->db->query(trim($str, ','));

        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_latest` WHERE page_id = '" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "'");
        if (isset($data['product-related'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "product_latest` (page_id,code, product_id,sort_order) VALUES ";
            foreach ($data['product-related'] as $key => $value) {
                $str .= ' (' . $page_id . ',"' . $this->db->escape($code) . '",' . $value . ',0 ),';
            }
        }
        $this->db->query(trim($str, ','));

        $str = '';
        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_offers` WHERE page_id = '" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "'");
        if (isset($data['product-related-offers'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "product_offers` (page_id,code, product_id,sort_order) VALUES ";
            foreach ($data['product-related-offers'] as $key => $value) {
                $str .= ' (' . $page_id . ',"' . $this->db->escape($code) . '",' . $value . ',0 ),';
            }
            $this->db->query(trim($str, ','));
        }

        $this->db->query("DELETE FROM `" . DB_PREFIX . "homepage_new` WHERE page_id = '" . (int) $page_id . "'");

        foreach ($this->request->files as $key => $file) {
            /* if (isset($this->request->files['homepage_block1_image1']) && $this->request->files['homepage_block1_image1']["error"] == 0) {

              $image = $this->upload_file('Homepage', 'homepage_block1_image1', 1000);
              if ($image) {
              $this->load->model('tool/image');
              $this->model_tool_image->resize($image, $this->config_image->get('gethomepage', 'homepage_block1_image1', 'width'), $this->config_image->get('gethomepage', 'homepage_block1_image1', 'hieght'));
              $data['homepage_block1_image1'] = $image;
              }
              } */
            if (is_array($file) && $file["error"] == 0) {

                $image = $this->upload_file('Homepage', $key, rand(1000, 9000));
                if ($image) {
                    $this->load->model('tool/image');
                    $this->model_tool_image->resize($image, $this->config_image->get('gethomepage', $key, 'width'), $this->config_image->get('gethomepage', $key, 'hieght'));
                    $data[$key] = $image;
                }
            }
        }

        foreach ($data as $key => $value) {
            if ($key != 'page_country' || $key != 'coutnry_langauge' || $key != 'product-related') {
                if (substr($key, 0, strlen($code)) == $code) {
                    if (!is_array($value)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "homepage_new SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
                    } else {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "homepage_new SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value)) . "', serialized = '1'");
                    }
                }
            }
        }
    }

    public function deleteSetting($code, $page_id) {

        $this->db->query("DELETE FROM `" . DB_PREFIX . "page_settings` WHERE page_id = '" . (int) $page_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "homepage_new WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");
    }

    public function editSettingValue($code = '', $key = '', $value = '', $country_id, $language_id) {
        if (!is_array($value)) {
            $this->db->query("UPDATE " . DB_PREFIX . "homepage_new SET `value` = '" . $this->db->escape($value) . "', serialized = '0'  WHERE `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "' AND country_id = '" . (int) $country_id . "' AND language_id = '" . (int) $language_id . "' ");
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "homepage_new SET `value` = '" . $this->db->escape(json_encode($value)) . "', serialized = '1' WHERE `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "' AND country_id = '" . (int) $country_id . "'  AND language_id = '" . (int) $language_id . "' ");
        }
    }

    public function getPages() {

        $query = $this->db->query("SELECT DISTINCT page_id from `" . DB_PREFIX . "homepage_new`");
        $page_data = array();
        if (!empty($query->rows)) {
            foreach ($query->rows as $page) {
                $page_data[$page['page_id']] = $this->db->query("SELECT country_id,language_id from `" . DB_PREFIX . "page_settings` WHERE page_id='" . (int) $page['page_id'] . "'")->rows;
            }
        }
        return $page_data;
    }

    public function getPageCountries($code, $page_id) {
        $result = $this->db->query("SELECT country_id,language_id from `" . DB_PREFIX . "page_settings` WHERE page_id='" . (int) $page_id . "'")->rows;
        $return_data = array();
        foreach ($result as $key => $value) {
            $return_data[$value['country_id']] = $value['language_id'];
        }
        return $return_data;
    }

    public function getPageProduct($page_id, $code) {
        $query = $this->db->query("SELECT p.product_id, CONCAT(pd1.name,' ', pd2.name) name FROM product_latest p LEFT JOIN product_description pd1 ON(p.product_id=pd1.product_id ) LEFT JOIN product_description pd2 ON(p.product_id = pd2.product_id) WHERE p.page_id = '" . (int) $page_id . "' AND pd1.language_id='1' AND pd2.language_id='2' ");
        return $query->rows;
    }

    public function getPageOffersProduct($page_id, $code) {
        $query = $this->db->query("SELECT p.product_id, CONCAT(pd1.name,' ', pd2.name) name FROM product_offers p LEFT JOIN product_description pd1 ON(p.product_id=pd1.product_id ) LEFT JOIN product_description pd2 ON(p.product_id = pd2.product_id) WHERE p.page_id = '" . (int) $page_id . "' AND pd1.language_id='1' AND pd2.language_id='2' ");
        return $query->rows;
    }

}
