<?php

class ModelToolImage extends Model {

    public function resize($filename, $width, $height, $upload = true) {

        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
        if ($upload) {
            return $this->uploadImage($filename, $width, $height);
        } else if (!is_file(DIR_IMAGE . $new_image)) {
            $this->uploadImage($filename, $width, $height, false);
        }
        return HTTPS_CATALOG . "image/" . $new_image;

        if (defined('HTTPS_IMAGE_S3')) {
            return HTTPS_IMAGE_S3 . $new_image;
        } else {
            if ($this->request->server['HTTPS']) {
                return $this->config->get('config_ssl') . 'image/' . $new_image;
            } else {
                return $this->config->get('config_url') . 'image/' . $new_image;
            }
        }
    }

    public function uploadImage($filename, $width, $height, $upload = true) {
        if (!is_file(DIR_IMAGE . $filename)) {
            $this->log->write("Image File Not Exists: " . DIR_IMAGE . $filename);
            return;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $old_image = $filename;
        $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if ($upload) {
            $this->db->query("INSERT INTO image_s3 SET image='" . $this->db->escape($old_image) . "' , "
                    . " new_image_path='" . $this->db->escape($new_image) . "' , "
                    . " width='" . $this->db->escape($width) . "' , "
                    . " height='" . $this->db->escape($height) . "' , "
                    . " upload_image='1', "
                    . " date_added=NOW(), user_id='" . (int) $this->user->getid() . "' ");

            return HTTPS_CATALOG . "image/" . $new_image;
        } else {
            $this->db->query("INSERT INTO image_s3 SET image='" . $this->db->escape($old_image) . "' , "
                    . " new_image_path='" . $this->db->escape($new_image) . "' , "
                    . " width='" . $this->db->escape($width) . "' , "
                    . " height='" . $this->db->escape($height) . "' , "
                    . " upload_image='0', "
                    . " date_added=NOW(), user_id='" . (int) $this->user->getid() . "' ");
            return HTTPS_IMAGE_S3 . $new_image;
        }
    }

    public function getImageS3($filename, $width, $height) {
        //$this->uploadImage($filename,$width,$height,true);

        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
        if (defined('HTTPS_IMAGE_S3')) {
            return HTTPS_IMAGE_S3 . $new_image;
        } else {
            if ($this->request->server['HTTPS']) {
                return $this->config->get('config_ssl') . 'image/' . $new_image;
            } else {
                return $this->config->get('config_url') . 'image/' . $new_image;
            }
        }
    }

}
