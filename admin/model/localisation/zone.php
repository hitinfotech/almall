<?php

class ModelLocalisationZone extends Model {

    public function addZone($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "zone SET status = '" . (int) $data['status'] . "', name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', country_id = '" . (int) $data['country_id'] . "'");

        $this->cache->delete('zone');
        $this->cache->deletekeys('admin.zone');
        $this->cache->deletekeys('catalog.zone');
    }

    public function editZone($zone_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "zone SET status = '" . (int) $data['status'] . "', name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', country_id = '" . (int) $data['country_id'] . "' WHERE zone_id = '" . (int) $zone_id . "'");

        $this->cache->delete('zone');
        $this->cache->deletekeys('admin.zone');
        $this->cache->deletekeys('catalog.zone');
    }

    public function deleteZone($zone_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int) $zone_id . "'");

        $this->cache->delete('zone');
        $this->cache->deletekeys('admin.zone');
        $this->cache->deletekeys('catalog.zone');
    }

    public function getZone($zone_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone z LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id AND zd.language_id ='".(int) $this->config->get('config_language_id')."') WHERE z.zone_id = " . (int) $zone_id );

        return $query->row;
    }

    public function getZones($data = array()) {
        $sql = "SELECT *, zd.name, cd.name AS country FROM " . DB_PREFIX . "zone z LEFT JOIN " . DB_PREFIX . "zone_description zd ON(z.zone_id=zd.zone_id) LEFT JOIN " . DB_PREFIX . "country c ON (z.country_id = c.country_id) LEFT JOIN " . DB_PREFIX . "country_description cd ON (c.country_id = cd.country_id) WHERE zd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' ";

        $sort_data = array(
            'cd.name',
            'zd.name',
            'z.code'
        );

        if (isset($data['sort']) && $data['sort'] == 'z.name') {
            $data['sort'] = 'zd.name';
        }

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY cd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getZonesByCountryId($country_id) {
        $cache = 'zone.getzonesbycountryid.' . $this->config->get('config_language_id') . '.' . (int) $country_id;

        $zone_data = $this->cache->get($cache);

        if (!$zone_data) {

            $query = $this->db->query("SELECT z.zone_id, z.country_id, z.code, z.status, z.available, CONCAT(zd1.name, ' | ',zd2.name  ) AS name FROM " . DB_PREFIX . "zone z LEFT JOIN zone_description zd1 ON(z.zone_id=zd1.zone_id) LEFT JOIN zone_description zd2 ON(z.zone_id=zd2.zone_id) WHERE zd1.language_id='1' AND  zd2.language_id='2' AND country_id = '" . (int) $country_id . "' AND status = '1' ORDER BY name");

            $zone_data = $query->rows;

            $this->cache->set($cache, $zone_data);
        }

        return $zone_data;
    }

    public function getTotalZones() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone");

        return $query->row['total'];
    }

    public function getTotalZonesByCountryId($country_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int) $country_id . "'");

        return $query->row['total'];
    }

    public function getZonesByName($data) {
        if ($data['filter_name']) {
            $query = $this->db->query("SELECT * "
                    . "FROM " . DB_PREFIX . "zone z "
                    . "INNER JOIN " . DB_PREFIX . "zone_description zd "
                    . "WHERE zd.`name` LIKE '%" . $this->db->escape($data['filter_name']) . "%' "
                    . "AND zd.language_id='" . (int) $this->config->get('config_language_id') . "' "
                    . "AND z.status = 1 "
                    . "AND z.available = 1 ");
            return $query->rows;
        } else {
            return array();
        }
    }

}
