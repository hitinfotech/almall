<?php

class ModelLocalisationCountry extends Model {

    public function addCountry($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "country SET available = '" . (int) $data['available'] . "', curcode = '" . $this->db->escape($data['curcode']) . "', language_code = '" . $this->db->escape($data['language_code']) . "', telecode = '" . $this->db->escape($data['telecode']) . "', telephone = '" . $this->db->escape($data['telephone']) . "' , iso_code_2 = '" . $this->db->escape($data['iso_code_2']) . "', iso_code_3 = '" . $this->db->escape($data['iso_code_3']) . "', address_format = '" . $this->db->escape($data['address_format']) . "', postcode_required = '" . (int) $data['postcode_required'] . "', status = '" . (int) $data['status'] . "'");

        $country_id = $this->db->getLastId();

        foreach ($data['country_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "country_description SET country_id = '" . (int) $country_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "'");
        }

        $this->cache->delete('country');
        $this->cache->deletekeys('catalog.country');
        $this->cache->deletekeys("admin.country");
    }

    public function editCountry($country_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "country SET available = '" . (int) $data['available'] . "', curcode = '" . $this->db->escape($data['curcode']) . "', language_code = '" . $this->db->escape($data['language_code']) . "', telecode = '" . $this->db->escape($data['telecode']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', iso_code_2 = '" . $this->db->escape($data['iso_code_2']) . "', iso_code_3 = '" . $this->db->escape($data['iso_code_3']) . "', address_format = '" . $this->db->escape($data['address_format']) . "', postcode_required = '" . (int) $data['postcode_required'] . "', status = '" . (int) $data['status'] . "' WHERE country_id = '" . (int) $country_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "country_description WHERE country_id = '" . (int) $country_id . "'");

        foreach ($data['country_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "country_description SET country_id = '" . (int) $country_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "'");
        }

        $this->cache->delete('country');
        $this->cache->deletekeys('catalog.country');
        $this->cache->deletekeys("admin.country");
    }

    public function deleteCountry($country_id) {
        //$this->db->query("DELETE FROM " . DB_PREFIX . "country WHERE country_id = '" . (int) $country_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "country_description WHERE country_id = '" . (int) $country_id . "'");

        $this->cache->delete('country');
        $this->cache->deletekeys('catalog.country');
        $this->cache->deletekeys("admin.country");
        return $country_id;
    }

    public function getCountry($country_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE cd.language_id = '" . $this->config->get('config_language_id') . "' AND c.country_id = '" . (int) $country_id . "'");

        return $query->row;
    }

    public function getCountryDescriptions($country_id) {
        $country_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country_description WHERE country_id = '" . (int) $country_id . "'");

        foreach ($query->rows as $result) {
            $country_description_data[$result['language_id']] = array(
                'name' => $result['name']
            );
        }

        return $country_description_data;
    }

    public function getCountries($data = array()) {
        $cache = "country.getCountries." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        //$country_data = $this->cache->get($cache);
        //if (!$country_data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE cd.language_id = '" . $this->config->get('config_language_id') . "'";
            $sort_data = array(
                'name',
                'status',
                'date_added',
                'language_code',
                'curcode',
                'telecode',
                'available',
                'vat_check',
                'vat_percent',
                'p.sort_order',
                'p.algorithm'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY name";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }
            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }
            $query = $this->db->query($sql);

            $country_data = $query->rows;

        //    $this->cache->set($cache, $country_data);
        //}

        return $country_data;
    }

    public function getTotalCountries() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "country");

        return $query->row['total'];
    }

    public function getAvailableCountries() {
        $key = 'country.getavailablecountries';

        $result = $this->cache->get($key);
        if (!$result) {
            $query = $this->db->query(" SELECT c.*, CONCAT(cd1.name,' | ',cd2.name) AS name FROM " . DB_PREFIX . "country c LEFT JOIN country_description cd1 ON(c.country_id = cd1.country_id) LEFT JOIN country_description cd2 ON(c.country_id = cd2.country_id) WHERE cd1.language_id = '1' AND cd2.language_id = '2' AND c.available = '1' ORDER BY cd" . $this->config->get('config_language_id') . ".name ASC");

            $result = $query->rows;
            /* $result[''] = array('country_id' => 0,
              'language' => 'en',
              'curcode' => 'USD',
              'telecode' => '',
              'iso_code_2' => '',
              'iso_code_3' => '',
              'address_format' => '',
              'postcode_required' => 0,
              'status' => 1,
              'available' => 1,
              'flag' => 'flags/un.png',
              'sort_order' => 100,
              'isvirtual' => 1,
              'name' => 'WorldWide'
              ); */

            if (!empty($query->rows)) {
                $this->cache->set($key, $result);
            }

            return $result;
        }
        return $result;
    }

    public function getCountriesByName($data) {
        if ($data['filter_name']) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country c INNER JOIN country_description cd ON(c.country_id=cd.country_id)  WHERE cd.`name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'  AND cd.language_id = '" . $this->config->get('config_language_id') . "'  AND c.status = 1  AND c.available = 1 ");
            return $query->rows;
        } else {
            return array();
        }
    }

}
