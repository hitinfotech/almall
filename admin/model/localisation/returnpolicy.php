<?php

class ModelLocalisationReturnpolicy extends Model {

    public function addPolicy($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "return_policy SET num_days = '" . (int) $data['num_days'] . "', sort_order = '" . (int) $data['sort_order'] . "', date_added=NOW()");

        $return_policy_id = $this->db->getLastId();

        foreach ($data['return_policy_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "return_policy_description SET return_policy_id = '" . (int) $return_policy_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }

        $this->cache->delete('returnpolicy');
        $this->cache->deletekeys('catalog.returnpolicy');
        $this->cache->deletekeys("admin.returnpolicy");
    }

    public function editPolicy($return_policy_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "return_policy SET num_days = '" . (int) $data['num_days'] . "', sort_order = '" . (int) $data['sort_order'] . "' WHERE return_policy_id = '" . (int) $return_policy_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "return_policy_description WHERE return_policy_id = '" . (int) $return_policy_id . "'");

        foreach ($data['return_policy_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "return_policy_description SET return_policy_id = '" . (int) $return_policy_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
        }

        $this->cache->delete('returnpolicy');
        $this->cache->deletekeys('catalog.returnpolicy');
        $this->cache->deletekeys("admin.returnpolicy");
    }

    public function deletePolicy($return_policy_id) {
        //$this->db->query("DELETE FROM " . DB_PREFIX . "return_policy WHERE return_policy_id = '" . (int) $return_policy_id . "'");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "return_policy_description WHERE return_policy_id = '" . (int) $return_policy_id . "'");

        $this->cache->delete('returnpolicy');
        $this->cache->deletekeys('catalog.returnpolicy');
        $this->cache->deletekeys("admin.returnpolicy");
    }

    public function getPolicy($return_policy_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "return_policy rp LEFT JOIN return_policy_description rpd ON(rp.return_policy_id=rpd.return_policy_id) WHERE rpd.language_id = '" . $this->config->get('config_language_id') . "' AND rp.return_policy_id = '" . (int) $return_policy_id . "'");

        return $query->row;
    }

    public function getPolicyDescriptions($return_policy_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "return_policy_description WHERE return_policy_id = '" . (int) $return_policy_id . "'");

        return $query->rows;
    }

    public function getPolicies() {

        $sql = "SELECT * FROM " . DB_PREFIX . "return_policy rp LEFT JOIN return_policy_description rpd ON(rp.return_policy_id=rpd.return_policy_id) WHERE rpd.language_id = '" . (int) $this->config->get('config_language_id') . "' ";
        
        if(!$this->user->getIsCoreGroup()){
            $sql .= " AND rp.isadmin = 0 ";
        }
        
        $sql .= "ORDER BY rp.return_policy_id ASC ";
        
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalPolicies() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "return_policy");

        return $query->row['total'];
    }

}
