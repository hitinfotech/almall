<?php

class ModelLocalisationRmaStatus extends Model {

    public function addRmaStatus($data) {
        foreach ($data['rma_status'] as $language_id => $value) {
            if (isset($rma_status_id)) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "rma_status SET rma_status_id = '" . (int) $rma_status_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "'");
                //$this->db->query(" UPDATE rma_status SET order_status_id='". (int) $data['order_status_id'] ."', color='".$this->db->escape($data['color'])."', quantity='". $this->db->escape($data['quantity']) ."', stock='". $this->db->escape($data['stock']) ."', attention_time='". (int)$data['attention_time'] ."', warning_time='". (int)$data['warning_time'] ."' WHERE rma_status_id = '".(int)$rma_status_id."' ");
                $this->db->query(" UPDATE rma_status SET order_status_id='". (int) $data['order_status_id'] ."', color='".$this->db->escape($data['color'])."', attention_time='". (int)$data['attention_time'] ."', warning_time='". (int)$data['warning_time'] ."' WHERE rma_status_id = '".(int)$rma_status_id."' ");
            } else {
                $this->db->query("INSERT INTO " . DB_PREFIX . "rma_status SET language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "'");

                $rma_status_id = $this->db->getLastId();
                //$this->db->query(" UPDATE rma_status SET order_status_id='". (int) $data['order_status_id'] ."', color='".$this->db->escape($data['color'])."', quantity='". $this->db->escape($data['quantity']) ."', stock='". $this->db->escape($data['stock']) ."', attention_time='". (int)$data['attention_time'] ."', warning_time='". (int)$data['warning_time'] ."' WHERE rma_status_id = '".(int)$rma_status_id."' ");
                $this->db->query(" UPDATE rma_status SET order_status_id='". (int) $data['order_status_id'] ."', color='".$this->db->escape($data['color'])."', attention_time='". (int)$data['attention_time'] ."', warning_time='". (int)$data['warning_time'] ."' WHERE rma_status_id = '".(int)$rma_status_id."' ");
            }
        }

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='rma_status', type_id='0', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function editRmaStatus($rma_status_id, $data) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "rma_status WHERE rma_status_id = '" . (int) $rma_status_id . "'");

        foreach ($data['rma_status'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "rma_status SET rma_status_id = '" . (int) $rma_status_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "'");
        }
        //$this->db->query(" UPDATE rma_status SET order_status_id='". (int) $data['order_status_id'] ."', color='".$this->db->escape($data['color'])."', quantity='". $this->db->escape($data['quantity']) ."', stock='". $this->db->escape($data['stock']) ."', attention_time='". (int)$data['attention_time'] ."', warning_time='". (int)$data['warning_time'] ."' WHERE rma_status_id = '".(int)$rma_status_id."' ");
        $this->db->query(" UPDATE rma_status SET order_status_id='". (int) $data['order_status_id'] ."', color='".$this->db->escape($data['color'])."', attention_time='". (int)$data['attention_time'] ."', warning_time='". (int)$data['warning_time'] ."' WHERE rma_status_id = '".(int)$rma_status_id."' ");

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='rma_status', type_id='0', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function deleteRmaStatus($rma_status_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "rma_status WHERE rma_status_id = '" . (int) $rma_status_id . "'");

        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='rma_status', type_id='0', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function getRmaStatus($rma_status_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "rma_status WHERE rma_status_id = '" . (int) $rma_status_id . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getRmaStatuses($data = array()) {
        if ($data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "rma_status WHERE language_id = '" . (int) $this->config->get('config_language_id') . "'";

            $sql .= " ORDER BY name";

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            
            $cache = "rma_status.getorderstatuses." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));
            
            $rma_status_data = $this->cache->get($cache);
            
            if (!$rma_status_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "rma_status WHERE language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY name");
                $rma_status_data = array();
                foreach($query->rows as $row){
                    $rma_status_data[$row['rma_status_id']] = $row;
                }
                if(!empty($rma_status_data)){
                   $this->cache->set($cache, $rma_status_data);
                }
            }

            return $rma_status_data;
        }
    }

    public function getRmaStatusDescriptions($rma_status_id) {
        $rma_status_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "rma_status WHERE rma_status_id = '" . (int) $rma_status_id . "'");

        foreach ($query->rows as $result) {
            $rma_status_data[$result['language_id']] = array('name' => $result['name']);
        }

        return $rma_status_data;
    }

    public function getTotalRmaStatuses() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "rma_status WHERE language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row['total'];
    }

}
