<?php

class ModelMarketingOfferBanner extends Model {

    public function getSetting($code, $page_id) {
        $setting_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_land WHERE page_id = '" . (int) $page_id . "'  AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $setting_data[$result['key']] = $result['value'];
            } else {
                $setting_data[$result['key']] = json_decode($result['value'], true);
            }
        }

        return $setting_data;
    }

    public function editSetting($code, $data, $page_id) {

        $this->db->query("DELETE FROM `" . DB_PREFIX . "page_settings` WHERE page_id = '" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "'");
        if (isset($data['page_country'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "page_settings` (page_id, country_id,language_id,code) VALUES ";
            foreach ($data['page_country'] as $key => $value) {
                $str .= ' (' . $page_id . ',' . $key . ',' . $data['coutnry_langauge'] . ',"' . $this->db->escape($code) . '") ,';
            }
        }
        $this->db->query(trim($str, ','));


        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_land` WHERE page_id = '" . (int) $page_id . "'  AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($this->request->files as $key => $file) {
            if (is_array($file) && $file["error"] == 0) {

                $image = $this->upload_file('ProductLanding', $key, rand(1000, 9000));
                if ($image) {
                    $this->load->model('tool/image');
                    $this->model_tool_image->resize($image, $this->config_image->get('offer_banner', $key, 'width'), $this->config_image->get('offer_banner', $key, 'hieght'));
                    $data[$key] = $image;
                }
            }
        }

        foreach ($data as $key => $value) {
            if (substr($key, 0, strlen($code)) == $code) {
                if (!is_array($value)) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value)) . "', serialized = '1'");
                }
            }
        }
    }

    public function addSetting($code, $data) {

        $page_id = $this->db->query("SELECT ifnull(max(page_id),0) as max FROM `" . DB_PREFIX . "page_settings` ")->row['max'];
        $page_id++;
        if (isset($data['page_country'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "page_settings` (page_id, country_id,language_id,code) VALUES ";
            foreach ($data['page_country'] as $key => $value) {
                $str .= ' (' . $page_id . ',' . $key . ',' . $data['coutnry_langauge'] . ',"' . $this->db->escape($code) . '" ) ,';
            }
            $this->db->query(trim($str, ','));
        }

        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_land` WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($this->request->files as $key => $file) {
            if (is_array($file) && $file["error"] == 0) {

                $image = $this->upload_file('ProductLanding', $key, rand(1000, 9000));
                if ($image) {
                    $this->load->model('tool/image');
                    $this->model_tool_image->resize($image, $this->config_image->get('offer_banner', $key, 'width'), $this->config_image->get('offer_banner', $key, 'hieght'));
                    $data[$key] = $image;
                }
            }
        }

        foreach ($data as $key => $value) {
            if ($key == 'product-selected' || $key == 'coutnry_langauge' || $key == 'page_country') {
                continue;
            }
            if (substr($key, 0, strlen($code)) == $code) {
                if (!is_array($value)) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value)) . "', serialized = '1'");
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "selected_products WHERE page_id = '" . (int) $page_id . "'");

        if (isset($data['product-selected'])) {
            foreach ($data['product-selected'] as $selected_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "selected_products SET product_id = '" . (int) $selected_id . "', page_id = '" . (int) $page_id . "'");
            }
        }
        return $page_id;
    }

    public function deleteSetting($code, $page_id) {

        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_land` WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "selected_products WHERE page_id = '" . (int) $page_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "offer_banner_blocks WHERE page_id = '" . (int) $page_id . "'");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "page_settings` WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");
    }

    public function addProduct($data, $page_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "offer_banner_blocks WHERE page_id = '" . (int) $page_id . "'");

        if (isset($data['additional_bloacks']) && !empty($data['additional_bloacks'])) {
            foreach ($data['product-related'] as $key => $related_id) {
                foreach ($data['additional_bloacks'][$key] as $k => $v) {
                    if ($k != 'products') {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET page_id = '" . (int) $page_id . "', `code` = 'offer_banner', `key` = '" . $this->db->escape($k) . "', `value` = '" . $this->db->escape($v) . "'");
                    }
                }
            }
        }
        $this->cache->deletekeys('catalog.product.getcustlatestproducts2');
    }

    public function getProducts($page_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT plb.product_id,pd.name,plb.block_id FROM offer_banner_blocks plb LEFT JOIN product_description pd ON (plb.product_id=pd.product_id) where plb.page_id ='" . (int) $page_id . "' AND pd.language_id='" . (int) $this->config->get('config_language_id') . "' order by block_id ASC");

        foreach ($query->rows as $result) {
            $product_related_data[] = array(
                'product_id' => $result['product_id'],
                'name' => $result['name'],
                'block_id' => $result['block_id']
            );
        }

        return $product_related_data;
    }

    public function getSelectedProducts($page_id, $code) {
        $product_related_data = array();

        $query = $this->db->query("SELECT sp.product_id,pd.name FROM selected_products sp LEFT JOIN product_description pd ON (sp.product_id=pd.product_id) where sp.page_id ='" . (int) $page_id . "' AND pd.language_id='" . (int) $this->config->get('config_language_id') . "'");

        foreach ($query->rows as $result) {
            $product_related_data[] = array(
                'product_id' => $result['product_id'],
                'name' => $result['name'],
            );
        }

        return $product_related_data;
    }

    public function getPageCountries($code, $page_id) {
        $result = $this->db->query("SELECT country_id,language_id,gender from `" . DB_PREFIX . "page_settings` WHERE page_id='" . (int) $page_id . "'")->rows;
        $return_data = array();
        foreach ($result as $key => $value) {
            $return_data[$value['country_id']] = array('language_id' => $value['language_id'], 'gender' => $value['gender']);
        }
        return $return_data;
    }

    public function getPages($code) {

        $query = $this->db->query("SELECT DISTINCT page_id from `" . DB_PREFIX . "page_settings` WHERE code='" . $this->db->escape($code) . "'");
        $page_data = array();
        if (!empty($query->rows)) {
            foreach ($query->rows as $page) {
                $page_data[$page['page_id']] = $this->db->query("SELECT country_id,language_id,gender from `" . DB_PREFIX . "page_settings` WHERE page_id='" . (int) $page['page_id'] . "' AND code='" . $this->db->escape($code) . "'")->rows;
            }
        }
        return $page_data;
    }

}
