<?php

class ModelCatalogMotherday extends Model {

    public function getSetting($code, $page_id) {
        $setting_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_land WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $setting_data[$result['key']] = $result['value'];
            } else {
                $setting_data[$result['key']] = json_decode($result['value'], true);
            }
        }

        return $setting_data;
    }

    public function editSetting($code, $data, $page_id) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_land` WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($this->request->files as $key => $file) {
            if (is_array($file) && $file["error"] == 0) {

                $image = $this->upload_file('MotherDay', $key , rand(1000,9000));
                if ($image) {
                    $this->load->model('tool/image');
                    $this->model_tool_image->resize($image, $this->config_image->get('mother_day', $key, 'width'), $this->config_image->get('mother_day', $key, 'hieght'));
                    $data[$key] = $image;
                }
            }
        }

        foreach ($data as $key => $value) {
            if ($key=='product-selected'){
              continue;
            }
            if (substr($key, 0, strlen($code)) == $code) {
                if (!is_array($value)) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value)) . "', serialized = '1'");
                }
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "featured_pages_selected_products WHERE page_id = '".(int) $page_id."' AND code='".$this->db->escape($code)."'");

        if (isset($data['product-selected'])) {
            foreach ($data['product-selected'] as $selected_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "featured_pages_selected_products SET product_id = '" . (int) $selected_id . "', page_id = '".(int) $page_id."', code='".$this->db->escape($code)."'");
            }
        }
    }

    public function getSelectedProducts($code,$page_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT sp.product_id,pd.name FROM featured_pages_selected_products sp LEFT JOIN product_description pd ON (sp.product_id=pd.product_id) where sp.page_id ='".(int) $page_id."' AND code='".$this->db->escape($code)."' AND pd.language_id='".(int) $this->config->get('config_language_id') ."'");

        foreach ($query->rows as $result) {
            $product_related_data[] = array(
              'product_id' => $result['product_id'],
              'name' => $result['name'],
            );
        }

        return $product_related_data;
    }

    public function addProduct($data,$code,$page_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "featured_pages_blocks WHERE code = '".$this->db->escape($code)."' AND page_id='".(int) $page_id."'");

        if(isset( $data['additional_bloacks']) && !empty($data['additional_bloacks'])){
            foreach ($data['product-related'] as $key => $related_id) {
              foreach( $data['additional_bloacks'][$key] as $k => $v){
                if ($k != 'products'){
                  $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET code = '" . $this->db->escape($code) . "', page_id = '" . (int) $page_id . "', `key` = '" . $this->db->escape($k) . "', `value` = '" . $this->db->escape($v) . "'");
                }
              }
              foreach ( $related_id as $id => $prod_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "featured_pages_blocks SET product_id = '" . (int) $prod_id . "', country_id = '".(int) $country_id."',language_id='".(int) $language_id."',block_id='".(int)$key."'");
              }
            }
          }
    }

    public function getProducts($country_id,$language_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT mdb.product_id,pd.name,mdb.block_id FROM featured_pages_blocks mdb LEFT JOIN product_description pd ON (mdb.product_id=pd.product_id) where mdb.language_id ='".(int) $language_id."' AND country_id='".(int) $country_id."' AND pd.language_id='".(int) $language_id ."' order by block_id ASC");

        foreach ($query->rows as $result) {
            $product_related_data[] = array(
              'product_id' => $result['product_id'],
              'name' => $result['name'],
              'block_id' => $result['block_id']
            );
        }

        return $product_related_data;
    }

}
