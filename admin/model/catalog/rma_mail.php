<?php

class ModelCatalogRmaMail extends Model {

    private $data;
    private $message_to_customer = 'message_to_customer';
    private $label_to_customer = 'label_to_customer';

    public function getCustomer($rma) {

        $customer = $this->db->query("SELECT o.*,wrp.product_id,wrp.order_product_id FROM `" . DB_PREFIX . "order` o LEFT JOIN ".  DB_PREFIX."wk_rma_order wro ON (wro.order_id = o.order_id) LEFT JOIN ".DB_PREFIX."wk_rma_product wrp ON (wro.id = wrp.rma_id) WHERE wro.id = '".(int)$rma."'  AND o.deleted='0'  ")->row;

        return $customer;
    }

    public function mail($data, $mail_type = '') {

        $value_index = array();

        $this->load->language('catalog/rma_mail');

        $mail_message = '';

        switch ($mail_type) {

            //admin send message to customer
            case $this->message_to_customer :

                $mail_subject = $this->language->get($this->message_to_customer . '_subject');

                if (!$data['status']) {

								$value_index = array(
													'customer_name' => $customer_info['firstname'].' '.$customer_info['lastname'],
													'rma_id' => $data['rma_id'],
													'link' => $data['link']
													);

								if(file_exists(DIR_IMAGE.$data['label'])){
									$mail_attachment = DIR_IMAGE.$data['label'];
								}
							}
								break;

							default :
								return;
						}


		if($mail_message){

			$this->data['customer_name'] = $customer_info['payment_firstname'] . ' '.$customer_info['payment_lastname'];
			$this->data['order_id'] = $customer_info['order_id'];
			$this->data['store_name'] = $this->config->get('config_name');
			$this->data['store_url'] = HTTP_CATALOG;
			$this->data['logo'] = HTTP_CATALOG.'image/' . $this->config->get('config_logo');

			$find = array(
				'{order_id}',
				'{rma_id}',
				'{customer_name}',
				'{admin_message}',
				'{link}',
				'{config_logo}',
				'{config_icon}',
				'{config_currency}',
				'{config_image}',
				'{config_name}',
				'{config_owner}',
				'{config_address}',
				'{config_geocode}',
				'{config_email}',
				'{config_telephone}',
				);

			$replace = array(
				'order_id' => '',
				'rma_id' => '',
				'customer_name' => '',
				'admin_message' => '',
				'link' => '',
				'config_logo' => '<a href="'.HTTP_CATALOG.'" title="'.$this->data['store_name'].'"><img src="'.HTTP_CATALOG.'image/' . $this->config->get('config_logo').'" alt="'.$this->data['store_name'].'" style="max-width:200px;"/></a>',
				'config_icon' => '<img src="'.HTTP_CATALOG.'image/' . $this->config->get('config_icon').'" style="max-width:200px;">',
				'config_currency' => $this->config->get('config_currency'),
				'config_image' => '<img src="'.HTTP_CATALOG.'image/' . $this->config->get('config_image').'" style="max-width:200px;">',
				'config_name' => $this->config->get('config_name'),
				'config_owner' => $this->config->get('config_owner'),
				'config_address' => $this->config->get('config_address'),
				'config_geocode' => $this->config->get('config_geocode'),
				'config_email' => $this->config->get('config_email'),
				'config_telephone' => $this->config->get('config_telephone'),
			);

			$replace = array_merge($replace,$value_index);

			$mail_message = trim(str_replace($find, $replace, $mail_message));

			$this->data['subject'] = $mail_subject;
			$this->data['message'] = $mail_message;

			$this->data['email_header'] = $this->load->view('mail/header.tpl');
			$this->data['email_footer'] = $this->load->view('mail/footer.tpl');
			if($data['status'] == 'Complete'){
				$product_store_credit = $this->getRmaProductValue($data['rma_id'], $customer_info['order_id'],$customer_info['product_id'],$customer_info['order_product_id']);
				$subtotal = $coupon = $product_price = 0;
				if(count($product_store_credit) >1){
					foreach($product_store_credit as $k => $psc){
						$product_price = $psc['product_price'];
						if($psc['code'] == 'sub_total')
							$subtotal = $psc['value'];
						else
							$coupon = $psc['value'];
					}
				}else{
					$product_price = $product_store_credit[0]['product_price'];
					$subtotal = $product_store_credit[0]['value'];
					$coupon = 0;
				}

				$this->data['store_credit_value'] = $product_price + ($coupon * $product_price/$subtotal);
				$this->data['store_credit_value'] = $this->data['store_credit_value'].' '.$customer_info['currency_code'];
				$this->data['message'] = $this->load->view('mail/rma_complete_mail.tpl', $this->data);

			}
			$html = $this->load->view('catalog/rma_mail.tpl', $this->data);


			$mail_sender = $this->config->get('config_name') ? $this->config->get('config_name') : HTTP_CATALOG;

			if (preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $mail_to) AND preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $mail_from) ) {

				if(VERSION == '2.0.0.0' || VERSION == '2.0.1.0' || VERSION == '2.0.1.1'  ) {
					/*Old mail code*/
					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($mail_to);
					$mail->setFrom($mail_from);
					$mail->setSender($mail_sender);
					$mail->setSubject($mail_subject);
					if(isset($mail_attachment))
						$mail->addAttachment($mail_attachment);
					$mail->setHtml($html);
					$mail->setText(strip_tags($html));
					$mail->send();
				} else {
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
					$mail->setTo($mail_to);
					$mail->setFrom($mail_from);
					$mail->setSender($mail_sender);
					$mail->setSubject($mail_subject);
					if(isset($mail_attachment))
						$mail->addAttachment($mail_attachment);
					$mail->setHtml($html);
					$mail->setText(strip_tags($html));
					$mail->send();


                    $seller_info = $this->getSeller($data['rma_id']);

                    if (isset($seller_info) && !empty($seller_info)) {
                        $data['fbs_email'] = $seller_info['fbs_email'];

                        if (isset($data['fbs_email']) && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $data['fbs_email'])) {

                            $mail->setTo($data['fbs_email']);
                            $mail->send();
                        }
                    }
				}
			}
		}
	}

	public function getRmaProductValue($rma_id,$order_id,$product_id,$order_product_id){
		$query = $this->db->query("SELECT ot.value,ot.code ,IFNULL ((SELECT price from " . DB_PREFIX . "customerpartner_to_order where product_id=".(int) $product_id. " AND order_id =".(int)$order_id." AND deleted='0' AND order_product_id=".(int) $order_product_id." ),0) as product_price FROM `" . DB_PREFIX . "order_total` ot WHERE ot.order_id= ".(int) $order_id." AND code in ('sub_total','coupon') AND ot.deleted='0' ");
		if($query->num_rows){
			return $query->rows;
		}else{
			return 0 ;
		}
	}
    public function getSeller($rma_id) {

        $product_id = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "wk_rma_product wrp WHERE wrp.rma_id = '" . (int) $rma_id . "'")->row;

        $seller_id = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customerpartner_to_product c2p WHERE c2p.product_id = '" . (int) $product_id["product_id"] . "'")->row;

        return $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer c2c WHERE c2c.customer_id = '" . (int) $seller_id["customer_id"] . "'")->row;
    }

}
