<?php

class ModelCatalogSearch extends Model {

    public function approveSearch($keyword_id) {
        $this->event->trigger('pre.admin.search.add', $keyword_id);

        $this->db->query("UPDATE `" . DB_PREFIX . "popular_search` SET approved ='1', approved_by='" . (int) $this->user->getId() . "', approved_on=NOW() WHERE id='" . (int) $keyword_id . "'");

        $this->cache->deletekeys('admin.search');
        $this->cache->deletekeys('catalog.search');
        $this->event->trigger('post.admin.search.add', $keyword_id);

        return $keyword_id;
    }

    public function disapproveSearch($keyword_id) {
        $this->event->trigger('pre.admin.search.add', $keyword_id);

        $this->db->query("UPDATE `" . DB_PREFIX . "popular_search` SET approved ='0', approved_by='" . (int) $this->user->getId() . "', approved_on=NOW() WHERE id='" . (int) $keyword_id . "'");

        $this->cache->deletekeys('admin.search');
        $this->cache->deletekeys('catalog.search');
        $this->event->trigger('post.admin.search.add', $keyword_id);

        return $keyword_id;
    }

    public function add($data) {
        $this->db->query(" INSERT INTO popular_search SET country_id='" . (int) $data['country_id'] . "', language_id='" . (int) $data['language_id'] . "', counter='" . (int) $data['counter'] . "', keytype='" . $this->db->escape($data['keytype']) . "', keyword='" . $this->db->escape($data['name']) . "', url='" . $this->db->escape($data['url']) . "', approved='" . (int) $data['approved'] . "', date_added=NOW(), approved_on=NOW(), approved_by='" . (int) $this->user->getId() . "' ON DUPLICATE KEY UPDATE country_id='" . (int) $data['country_id'] . "', language_id='" . (int) $data['language_id'] . "', counter='" . (int) $data['counter'] . "', keytype='" . $this->db->escape($data['keytype']) . "', keyword='" . $this->db->escape($data['name']) . "', approved='" . (int) $data['approved'] . "', date_added=NOW(), approved_on=NOW(), approved_by='" . (int) $this->user->getId() . "', url='" . $this->db->escape($data['url']) . "'  ");

        return $this->db->getLastId();
    }

    public function edit($data, $id) {
        $this->db->query(" UPDATE popular_search SET country_id='" . (int) $data['country_id'] . "', language_id='" . (int) $data['language_id'] . "', counter='" . (int) $data['counter'] . "', keytype='" . $this->db->escape($data['keytype']) . "', keyword='" . $this->db->escape($data['name']) . "', url='" . $this->db->escape($data['url']) . "', approved='" . (int) $data['approved'] . "', date_added=NOW(), approved_on=NOW(), approved_by='" . (int) $this->user->getId() . "' WHERE id='" . (int) $id . "' ");

        return $this->db->getLastId();
    }

    public function updateSearch($data = array()) {
        $this->db->query(" UPDATE popular_search SET keyword='" . $this->db->escape($data['keyword']) . "' WHERE id='" . (int) $data['id'] . "' ");

        return $this->db->getLastId();
    }

    public function deleteSearch($keyword_id) {
        $this->event->trigger('pre.admin.search.delete', $keyword_id);

        $this->db->query(" DELETE FROM " . DB_PREFIX . "popular_search WHERE id = '" . (int) $keyword_id . "'");

        $this->cache->deletekeys('admin.search');
        $this->cache->deletekeys('catalog.search');

        $this->event->trigger('post.admin.search.delete', $keyword_id);
    }

    public function getKeyword($keyword_id) {
        $query = $this->db->query("SELECT ps.*,cd.name as country,ld.name as language FROM " . DB_PREFIX . "popular_search ps INNER JOIN " . DB_PREFIX . "country_description cd ON (ps.country_id = cd.country_id) INNER JOIN " . DB_PREFIX . "language ld ON (ps.language_id = ld.language_id) WHERE cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND ps.id = '" . (int) $keyword_id . "'");

        return $query->row;
    }

    public function getSearch($data = array()) {
        
        $res = $this->db->query(" SELECT id, COUNT(*) cnt, SUM(counter) counter, TRIM(REPLACE(REPLACE(REPLACE(`keyword`,'\t',' '),'\n',' '),'\r',' ')) keyword FROM popular_search GROUP BY TRIM(REPLACE(REPLACE(REPLACE(`keyword`,'\t',' '),'\n',' '),'\r',' ')) HAVING cnt > 1 ORDER BY cnt ASC ");
        foreach ($res->rows as $row) {
            $this->db->query(" DELETE FROM popular_search WHERE TRIM(REPLACE(REPLACE(REPLACE(`keyword`,'\t',' '),'\n',' '),'\r',' '))= '" . $this->db->escape($row['keyword']) . "' AND id <> '" . (int) ($row['id']) . "' ");
            $this->db->query(" UPDATE popular_search SET counter = '" . (int) $row['cnt'] . "', keyword= '". $this->db->escape(preg_replace('/[ ]{2,}|[\t]/', ' ', trim($row['keyword'])))."' WHERE id='" . (int) $row['id'] . "' ");
        }
        $this->db->query(" UPDATE `popular_search` set `keyword` = TRIM(REPLACE(REPLACE(REPLACE(`keyword`,'\t',' '),'\n',' '),'\r',' ')) ");


        $sql = "SELECT ps.*,cd.name as country,ld.name as language FROM " . DB_PREFIX . "popular_search ps INNER JOIN " . DB_PREFIX . "country_description cd ON (ps.country_id = cd.country_id) INNER JOIN " . DB_PREFIX . "language ld ON (ps.language_id = ld.language_id) WHERE cd.language_id = '" . (int) $this->config->get('config_language_id') . "' ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND ps.keyword LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!is_null($data['filter_approved'])) {
            $sql .= " AND ps.approved = '" . (int) ($data['filter_approved']) . "'";
        }
        if (!empty($data['filter_keytype'])) {
            $sql .= " AND ps.keytype = '" . $this->db->escape($data['filter_keytype']) . "'";
        }
        if (!empty($data['filter_country_id'])) {
            $sql .= " AND ps.country_id = '" . (int) ($data['filter_country_id']) . "'";
        }
        if (!empty($data['filter_language_id'])) {
            $sql .= " AND ps.language_id = '" . (int) ($data['filter_language_id']) . "'";
        }

        if (!empty($data['filter_id'])) {
            $sql .= " AND ps.id = '" . (int) $data['filter_id'] . "'";
        }

        $sort_data = array(
            'keyword',
            'counter',
            'date_added'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY date_added";
        }

        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalSearch($data) {

        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "popular_search ps WHERE 1=1 ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND ps.keyword LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!is_null($data['filter_approved'])) {
            $sql .= " AND ps.approved = '" . (int) ($data['filter_approved']) . "'";
        }
        if (!empty($data['filter_keytype'])) {
            $sql .= " AND ps.keytype = '" . $this->db->escape($data['filter_keytype']) . "'";
        }
        if (!empty($data['filter_country_id'])) {
            $sql .= " AND ps.country_id = '" . (int) ($data['filter_country_id']) . "'";
        }
        if (!empty($data['filter_language_id'])) {
            $sql .= " AND ps.language_id = '" . (int) ($data['filter_language_id']) . "'";
        }

        if (!empty($data['filter_id'])) {
            $sql .= " AND ps.id = '" . (int) $data['filter_id'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

}
