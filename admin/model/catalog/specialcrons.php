<?php

class ModelCatalogSpecialcrons extends Model {

    public function addSpecialCron($data) {
        if (isset($data['product_special']) && !empty($data['product_special'])) {
            foreach ($data['product_special'] as $row) {

                $brand = $this->db->query(" SELECT brand_id  FROM brand WHERE brand_id = '" . (int) $row['brand_id'] . "' ");
                if ($brand->num_rows) {
                    if ((float) $row['percent'] > 0) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "brand_discounts_cronj SET category_id = '" . (int) $row['category_id'] . "', brand_id = '" . (int) $row['brand_id'] . "', percent = '" . (float) $row['percent'] . "', date_start = '" . $this->db->escape($row['date_start']) . "', date_end = '" . $this->db->escape($row['date_end']) . "', date_added = NOW(), user_id = '" . (int) $this->user->getid() . "' ");
                    }
                }
            }
        }
    }

    public function deleteSpecialCron($cron_id) {

        $this->db->query("INSERT IGNORE INTO algolia (`type`, `type_id`)(SELECT 'product',product_id FROM product_special WHERE cron = '" . (int) $cron_id . "') ");
        $this->db->query("UPDATE product_special set deleted='yes', deleted_by=".(int) $this->user->getId().", deleted_date=NOW() WHERE cron = '" . (int) $cron_id . "'");
        $this->db->query("UPDATE brand_discounts_cronj SET deleted = 1, deleted_on=NOW(), deleted_user='" . (int) $this->user->getid() . "' WHERE id = '" . (int) $cron_id . "'");
    }

    public function getSpecialcrons($data = array()) {
        $sql = "SELECT cj.*,(select bd.name from brand_description bd where bd.brand_id=cj.brand_id AND bd.language_id=1) brand,(select cd.name from category_description cd where cd.category_id=cj.category_id AND cd.language_id=1) category,(select ccd.name from country_description ccd where ccd.country_id=cj.country_id and ccd.language_id = 1 ) country, (select concat(u.firstname,' ',u.lastname) from user u where u.user_id=cj.user_id) user FROM brand_discounts_cronj cj WHERE cj.deleted = 0  ";

        if (isset($data['filter_brand']) && !is_null($data['filter_brand'])) {
            $sql .= " AND cj.brand_id = '" . (int) $data['filter_brand'] . "' ";
        }

        if (isset($data['filter_category']) && !is_null($data['filter_category'])) {
            $sql .= " AND cj.category_id = '" . (int) $data['filter_category'] . "' ";
        }

        $sort_data = array(
            'cj.brand_id',
            'cj.category_id',
            'cj.country_id',
            'cj.date_start',
            'cj.date_end',
            'cj.date_added'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY cj.id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalSpecialcrons($data = array()) {
        $sql = "SELECT count(*) total FROM brand_discounts_cronj WHERE deleted = 0 ";

        if (isset($data['filter_brand']) && !is_null($data['filter_brand'])) {
            $sql .= " AND cj.brand_id = '" . (int) $data['filter_brand'] . "' ";
        }

        if (isset($data['filter_category']) && !is_null($data['filter_category'])) {
            $sql .= " AND cj.category_id = '" . (int) $data['filter_category'] . "' ";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

}
