<?php

class ModelCatalogGroup extends Model {

    public function addGroup($data) {
        $this->event->trigger('pre.admin.group.add', $data);

        $this->db->query("INSERT INTO `" . DB_PREFIX . "group` SET parent_id = '" . (int) $data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int) $data['top'] : 0) . "', `column` = '" . (int) $data['column'] . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $group_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE `" . DB_PREFIX . "group` SET image = '" . $this->db->escape($data['image']) . "' WHERE group_id = '" . (int) $group_id . "'");
        }

        foreach ($data['group_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "group_description SET group_id = '" . (int) $group_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int) $data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "group_path` SET `group_id` = '" . (int) $group_id . "', `path_id` = '" . (int) $result['path_id'] . "', `level` = '" . (int) $level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "group_path` SET `group_id` = '" . (int) $group_id . "', `path_id` = '" . (int) $group_id . "', `level` = '" . (int) $level . "'");

        if (isset($data['group_filter'])) {
            foreach ($data['group_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "group_filter SET group_id = '" . (int) $group_id . "', filter_id = '" . (int) $filter_id . "'");
            }
        }

        if (isset($data['group_store'])) {
            foreach ($data['group_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "group_to_store SET group_id = '" . (int) $group_id . "', store_id = '" . (int) $store_id . "'");
            }
        }

        // Set which layout to use with this group
        if (isset($data['group_layout'])) {
            foreach ($data['group_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "group_to_layout SET group_id = '" . (int) $group_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout_id . "'");
            }
        }
        $this->url_custom->create_URL('group', $group_id);


        $this->cache->delete('group');

        $this->event->trigger('post.admin.group.add', $group_id);

        return $group_id;
    }

    public function editGroup($group_id, $data) {
        $this->event->trigger('pre.admin.group.edit', $data);

        $this->db->query("UPDATE `" . DB_PREFIX . "group` SET parent_id = '" . (int) $data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int) $data['top'] : 0) . "', `column` = '" . (int) $data['column'] . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', date_modified = NOW() WHERE group_id = '" . (int) $group_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE `" . DB_PREFIX . "group` SET image = '" . $this->db->escape($data['image']) . "' WHERE group_id = '" . (int) $group_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "group_description WHERE group_id = '" . (int) $group_id . "'");

        foreach ($data['group_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "group_description SET group_id = '" . (int) $group_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE path_id = '" . (int) $group_id . "' ORDER BY level ASC");

        if ($query->rows) {
            foreach ($query->rows as $group_path) {
                // Delete the path below the current one
                $this->db->query("DELETE FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int) $group_path['group_id'] . "' AND level < '" . (int) $group_path['level'] . "'");

                $path = array();

                // Get the nodes new parents
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int) $data['parent_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Get whats left of the nodes current path
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int) $group_path['group_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Combine the paths with a new level
                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO `" . DB_PREFIX . "group_path` SET group_id = '" . (int) $group_path['group_id'] . "', `path_id` = '" . (int) $path_id . "', level = '" . (int) $level . "'");

                    $level++;
                }
            }
        } else {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int) $group_id . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int) $data['parent_id'] . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "group_path` SET group_id = '" . (int) $group_id . "', `path_id` = '" . (int) $result['path_id'] . "', level = '" . (int) $level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "group_path` SET group_id = '" . (int) $group_id . "', `path_id` = '" . (int) $group_id . "', level = '" . (int) $level . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "group_filter WHERE group_id = '" . (int) $group_id . "'");

        if (isset($data['group_filter'])) {
            foreach ($data['group_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "group_filter SET group_id = '" . (int) $group_id . "', filter_id = '" . (int) $filter_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "group_to_store WHERE group_id = '" . (int) $group_id . "'");

        if (isset($data['group_store'])) {
            foreach ($data['group_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "group_to_store SET group_id = '" . (int) $group_id . "', store_id = '" . (int) $store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "group_to_layout WHERE group_id = '" . (int) $group_id . "'");

        if (isset($data['group_layout'])) {
            foreach ($data['group_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "group_to_layout SET group_id = '" . (int) $group_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout_id . "'");
            }
        }

               $this->url_custom->create_URL('group', $group_id);


        $this->cache->delete('group');

        $this->event->trigger('post.admin.group.edit', $group_id);
    }

    public function deleteGroup($group_id) {
        $this->event->trigger('pre.admin.group.delete', $group_id);

        $this->db->query("DELETE FROM " . DB_PREFIX . "group_path WHERE group_id = '" . (int) $group_id . "'");

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group_path WHERE path_id = '" . (int) $group_id . "'");

        foreach ($query->rows as $result) {
            $this->deleteGroup($result['group_id']);
        }

        $this->db->query("DELETE FROM `" . DB_PREFIX . "group` WHERE group_id = '" . (int) $group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "group_description WHERE group_id = '" . (int) $group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "group_filter WHERE group_id = '" . (int) $group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "group_to_store WHERE group_id = '" . (int) $group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "group_to_layout WHERE group_id = '" . (int) $group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_group WHERE group_id = '" . (int) $group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias_ar WHERE query = 'group_id=" . (int) $group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias_en WHERE query = 'group_id=" . (int) $group_id . "'");

        $this->cache->delete('group');

        $this->event->trigger('post.admin.group.delete', $group_id);
    }

    public function repairGroups($parent_id = 0) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group` WHERE parent_id = '" . (int) $parent_id . "'");

        foreach ($query->rows as $group) {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int) $group['group_id'] . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "group_path` WHERE group_id = '" . (int) $parent_id . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "group_path` SET group_id = '" . (int) $group['group_id'] . "', `path_id` = '" . (int) $result['path_id'] . "', level = '" . (int) $level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "group_path` SET group_id = '" . (int) $group['group_id'] . "', `path_id` = '" . (int) $group['group_id'] . "', level = '" . (int) $level . "'");

            $this->repairGroups($group['group_id']);
        }
    }

    public function getGroup($group_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "group_path cp LEFT JOIN " . DB_PREFIX . "group_description cd1 ON (cp.path_id = cd1.group_id AND cp.group_id != cp.path_id) WHERE cp.group_id = c.group_id AND cd1.language_id = '" . (int) $this->config->get('config_language_id') . "' GROUP BY cp.group_id) AS path, (SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias_en WHERE query = 'group_id=" . (int) $group_id . "') AS keyword FROM `" . DB_PREFIX . "group` c LEFT JOIN " . DB_PREFIX . "group_description cd2 ON (c.group_id = cd2.group_id) WHERE c.group_id = '" . (int) $group_id . "' AND cd2.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getGroups($data = array()) {
        $sql = "SELECT cp.group_id AS group_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "group_path cp LEFT JOIN `" . DB_PREFIX . "group` c1 ON (cp.group_id = c1.group_id) LEFT JOIN `" . DB_PREFIX . "group` c2 ON (cp.path_id = c2.group_id) LEFT JOIN " . DB_PREFIX . "group_description cd1 ON (cp.path_id = cd1.group_id) LEFT JOIN " . DB_PREFIX . "group_description cd2 ON (cp.group_id = cd2.group_id) WHERE cd1.language_id = '" . (int) $this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY cp.group_id";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getGroupDescriptions($group_id) {
        $group_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group_description WHERE group_id = '" . (int) $group_id . "'");

        foreach ($query->rows as $result) {
            $group_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
                'description' => $result['description']
            );
        }

        return $group_description_data;
    }

    public function getGroupFilters($group_id) {
        $group_filter_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group_filter WHERE group_id = '" . (int) $group_id . "'");

        foreach ($query->rows as $result) {
            $group_filter_data[] = $result['filter_id'];
        }

        return $group_filter_data;
    }

    public function getGroupStores($group_id) {
        $group_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group_to_store WHERE group_id = '" . (int) $group_id . "'");

        foreach ($query->rows as $result) {
            $group_store_data[] = $result['store_id'];
        }

        return $group_store_data;
    }

    public function getGroupLayouts($group_id) {
        $group_layout_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "group_to_layout WHERE group_id = '" . (int) $group_id . "'");

        foreach ($query->rows as $result) {
            $group_layout_data[$result['store_id']] = $result['layout_id'];
        }

        return $group_layout_data;
    }

    public function getTotalGroups() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "group`");

        return $query->row['total'];
    }

    public function getTotalGroupsByLayoutId($layout_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "group_to_layout WHERE layout_id = '" . (int) $layout_id . "'");

        return $query->row['total'];
    }

}
