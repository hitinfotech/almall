<?php

class ModelCatalogLandingPages extends Model {

    public function addPage($data) {
        $this->db->query("INSERT INTO landing_pages set page_name='" . $this->db->escape($data['landing_page_name']) . "' ,page_name_ar='" . $this->db->escape($data['landing_page_name_ar']) . "', color='" . $this->db->escape($data['color']) . "', background='" . $this->db->escape($data['background']) . "', hcolor='" . $this->db->escape($data['hcolor']) . "', hbackground='" . $this->db->escape($data['hbackground']) . "', show_at_homepage=" . ((isset($data['show_at_homepage']) && $data['show_at_homepage'] == 'on') ? '1' : '0') . ", fontbold=" . ((isset($data['fontbold']) && $data['fontbold'] == 'on') ? '1' : '0') . " , status=" . ((isset($data['status']) && $data['status'] == 'on') ? '1' : '0') . " , deleted=0, user_id=" . $this->user->getId() . " , date_added=NOW()");

        $land_id = $this->db->getLastId();

        if (isset($this->request->files['icon']) && $this->request->files['icon']["error"] == 0) {
            $image = $this->upload_file('LandPage', 'icon', $land_id);
            if ($image) {
                $this->load->model('tool/image');
                $this->model_tool_image->resize($image, 130, 130);
            } else {
                $image = '';
            }
            $this->db->query("UPDATE " . DB_PREFIX . "landing_pages SET icon = '" . $this->db->escape($image) . "' WHERE id = '" . (int) $land_id . "'");
        }
    }

    public function editPage($data) {
        if (isset($data['page_id'])) {
            $this->db->query("UPDATE landing_pages set page_name='" . $this->db->escape($data['landing_page_name']) . "' ,page_name_ar='" . $this->db->escape($data['landing_page_name_ar']) . "', color='" . $this->db->escape($data['color']) . "', background='" . $this->db->escape($data['background']) . "', hcolor='" . $this->db->escape($data['hcolor']) . "', hbackground='" . $this->db->escape($data['hbackground']) . "', show_at_homepage=" . ((isset($data['show_at_homepage']) && $data['show_at_homepage'] == 'on') ? '1' : '0') . " , fontbold=" . ((isset($data['fontbold']) && $data['fontbold'] == 'on') ? '1' : '0') . " , status=" . ((isset($data['status']) && $data['status'] == 'on') ? '1' : '0') . " , user_modified_id=" . $this->user->getId() . " , date_modified=NOW() WHERE id='" . (int) $data['page_id'] . "'");
            if (isset($this->request->files['icon']) && $this->request->files['icon']["error"] == 0) {

                $image = $this->upload_file('LandPage', 'icon', (int) $data['page_id']);
                if ($image) {
                    $this->load->model('tool/image');
                    $this->model_tool_image->resize($image, 130, 130);
                } else {
                    $image = '';
                }
                $this->db->query("UPDATE " . DB_PREFIX . "landing_pages SET icon = '" . $this->db->escape($image) . "' WHERE id = '" . (int) $data['page_id'] . "'");
            }
        }
    }

    public function deletePage($page_id) {
        $this->db->query("UPDATE landing_pages set deleted='1'  WHERE id=" . $page_id);
    }

    public function getPages() {
        $query = $this->db->query("SELECT id,page_name,page_name_ar,lp.date_modified  FROM landing_pages lp LEFT JOIN page_settings ps ON (lp.id=ps.parent_page_id AND ps.deleted='0') LEFT JOIN country_description cd ON (ps.country_id =cd.country_id   AND cd.language_id=1) LEFT JOIN language l ON (ps.language_id = l.language_id)  WHERE lp.deleted=0 group by id ");
        if ($query->num_rows) {
            return $query->rows;
        }
        return array();
    }

    public function getPage($page_id) {
        $query = $this->db->query("SELECT id,page_name,page_name_ar,show_at_homepage,fontbold,status, color, background, hcolor, hbackground, icon FROM landing_pages WHERE deleted=0 AND id=" . $page_id);
        if ($query->num_rows)
            return $query->row;
        return array();
    }

    public function getSubPage($page_id) {
        $query = $this->db->query("SELECT page_id,GROUP_CONCAT(concat('(',cd.name , ' | ' , cd2.name , ')')) as country_name ,l.name as language_name,gender,page_h1,page_title,created_at FROM page_settings ps LEFT JOIN country_description cd ON (ps.country_id = cd.country_id) LEFT JOIN country_description cd2 ON (ps.country_id = cd2.country_id) LEFT JOIN language l ON (ps.language_id=l.language_id) WHERE ps.code='landing_pages' AND deleted='0' AND cd.language_id='" . (int) $this->config->get('config_language_id') . "' AND cd2.language_id='2'  AND parent_page_id=" . $page_id . " group by ps.page_id");
        if ($query->num_rows) {
            return $query->rows;
        }
    }

    public function deleteSubPage($page_id, $code, $parent_id) {

        $this->db->query("UPDATE `" . DB_PREFIX . "page_settings` set deleted='1' WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");

        $this->db->query("UPDATE `" . DB_PREFIX . "product_land` set deleted='1' WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");

        $this->db->query("UPDATE " . DB_PREFIX . "featured_pages_selected_products  set deleted='1' WHERE page_id = '" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "'");

        $this->db->query("UPDATE " . DB_PREFIX . "featured_pages_blocks  set deleted='1' WHERE code = '" . $this->db->escape($code) . "' AND page_id='" . (int) $page_id . "'");

        $this->db->query("UPDATE " . DB_PREFIX . "landing_pages  set date_modified=NOW() WHERE  id='" . (int) $parent_id . "'");
    }

    public function addSetting($parent_id, $code, $data) {

        $page_id = $this->db->query("SELECT ifnull(max(page_id),0) as max FROM `" . DB_PREFIX . "page_settings` ")->row['max'];
        $page_id++;
        $this->db->query("UPDATE `" . DB_PREFIX . "page_settings` set deleted='1' WHERE page_id = '" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "'");
        if (isset($data['page_country'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "page_settings` (parent_page_id,page_id, country_id,language_id,code,page_h1,page_title,seo_url) VALUES ";
            foreach ($data['page_country'] as $key => $value) {
                $str .= ' (' . $parent_id . ',' . $page_id . ',' . $key . ',' . $data['coutnry_langauge'] . ',"' . $this->db->escape($code) . '","' . $this->db->escape($data['landing_pages_h1']) . '","' . $this->db->escape($data['landing_pages_title']) . '","' . $this->db->escape($data['landing_pages_seo']) . '") ,';
            }
            $this->db->query(trim($str, ','));
        }

        $this->db->query("UPDATE `" . DB_PREFIX . "product_land` set deleted='1' WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($this->request->files as $key => $file) {
            if (is_array($file) && $file["error"] == 0) {

                $image = $this->upload_file('FeaturedPages', $key, rand(1000, 9000));
                if ($image) {
                    $this->load->model('tool/image');
                    $this->model_tool_image->resize($image, $this->config_image->get('landing_pages', $key, 'width'), $this->config_image->get('landing_pages', $key, 'hieght'));
                    $data[$key] = $image;
                }
            }
        }

        if (isset($data['page_category'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "landing_page_to_category` (landing_page_id,category_id) VALUES ";
            foreach ($data['page_category'] as $key => $value) {
                $str .= ' (' . $page_id . ' , ' . $value . ') ,';
            }
        }
        $this->db->query(trim($str, ','));

        if (isset($data['page_brands'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "landing_page_to_brand` (landing_page_id,brand_id) VALUES ";
            foreach ($data['page_brands'] as $key => $value) {
                $str .= ' (' . $page_id . ' , ' . $value . ') ,';
            }
        }
        $this->db->query(trim($str, ','));

        foreach ($data as $key => $value) {
            if ($key == 'product-selected') {
                continue;
            }
            if (substr($key, 0, strlen($code)) == $code) {
                if (!is_array($value)) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value)) . "', serialized = '1'");
                }
            }
        }

        $this->db->query("UPDATE " . DB_PREFIX . "featured_pages_selected_products set deleted='1' WHERE page_id = '" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "'");

        if (isset($data['product-selected'])) {
            foreach ($data['product-selected'] as $selected_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "featured_pages_selected_products SET product_id = '" . (int) $selected_id . "', page_id = '" . (int) $page_id . "', code='" . $this->db->escape($code) . "'");
            }
        }

        $this->db->query("UPDATE " . DB_PREFIX . "landing_pages  set date_modified=NOW() WHERE  id='" . (int) $parent_id . "'");

        return $page_id;
    }

    public function editSetting($parent_id, $code, $data, $page_id) {

        $this->db->query("UPDATE `" . DB_PREFIX . "page_settings` set deleted='1' WHERE page_id = '" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "'");
        if (isset($data['page_country'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "page_settings` (parent_page_id,page_id, country_id,language_id,code,page_h1,page_title,seo_url) VALUES ";
            foreach ($data['page_country'] as $key => $value) {
                $str .= ' (' . $parent_id . ' , ' . $page_id . ',' . $key . ',' . $data['coutnry_langauge'] . ',"' . $this->db->escape($code) . '","' . $this->db->escape($data['landing_pages_h1']) . '","' . $this->db->escape($data['landing_pages_title']) . '","' . $this->db->escape($data['landing_pages_seo']) . '") ,';
            }
        }
        $this->db->query(trim($str, ','));

        $this->db->query("DELETE FROM `" . DB_PREFIX . "landing_page_to_category` WHERE landing_page_id = '" . (int) $page_id . "'");
        if (isset($data['page_category'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "landing_page_to_category` (landing_page_id,category_id) VALUES ";
            foreach ($data['page_category'] as $key => $value) {
                $str .= ' (' . $page_id . ' , ' . $value . ') ,';
            }
        }
        $this->db->query(trim($str, ','));

        $this->db->query("DELETE FROM `" . DB_PREFIX . "landing_page_to_brand` WHERE landing_page_id = '" . (int) $page_id . "'");
        if (isset($data['page_brands'])) {
            $str = "INSERT INTO `" . DB_PREFIX . "landing_page_to_brand` (landing_page_id,brand_id) VALUES ";
            foreach ($data['page_brands'] as $key => $value) {
                $str .= ' (' . $page_id . ' , ' . $value . ') ,';
            }
        }
        $this->db->query(trim($str, ','));


        $this->db->query("UPDATE `" . DB_PREFIX . "product_land` set deleted='1' WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($this->request->files as $key => $file) {
            if (is_array($file) && $file["error"] == 0) {

                $image = $this->upload_file('FeaturedPages', $key, rand(1000, 9000));
                if ($image) {
                    $this->load->model('tool/image');
                    $this->model_tool_image->resize($image, $this->config_image->get('landing_pages', $key, 'width'), $this->config_image->get('landing_pages', $key, 'hieght'));
                    $data[$key] = $image;
                }
            }
        }

        foreach ($data as $key => $value) {
            if ($key == 'product-selected') {
                continue;
            }
            if (substr($key, 0, strlen($code)) == $code) {
                if (!is_array($value)) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET page_id = '" . (int) $page_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value)) . "', serialized = '1'");
                }
            }
        }

        $this->db->query("UPDATE  " . DB_PREFIX . "featured_pages_selected_products set deleted='1' WHERE page_id = '" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "'");

        if (isset($data['product-selected'])) {
            foreach ($data['product-selected'] as $selected_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "featured_pages_selected_products SET product_id = '" . (int) $selected_id . "', page_id = '" . (int) $page_id . "', code='" . $this->db->escape($code) . "'");
            }
        }
        $this->db->query("UPDATE " . DB_PREFIX . "landing_pages  set date_modified=NOW() WHERE  id='" . (int) $parent_id . "'");
    }

    public function addProduct($data, $code, $page_id) {
        $this->db->query("UPDATE " . DB_PREFIX . "featured_pages_blocks set deleted='1' WHERE code = '" . $this->db->escape($code) . "' AND page_id='" . (int) $page_id . "'");

        if (isset($data['additional_bloacks']) && !empty($data['additional_bloacks'])) {
            foreach ($data['product-related'] as $key => $related_id) {
                foreach ($data['additional_bloacks'][$key] as $k => $v) {
                    if ($k != 'products') {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_land SET code = '" . $this->db->escape($code) . "', page_id = '" . (int) $page_id . "', `key` = '" . $this->db->escape($k) . "', `value` = '" . $this->db->escape($v) . "' ,position=".$data['position'][$key]." ");
                    }
                }
                foreach ($related_id as $id => $prod_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "featured_pages_blocks SET code = '" . $this->db->escape($code) . "', product_id = '" . (int) $prod_id . "', page_id = '" . (int) $page_id . "',block_id='" . (int) $key . "'");
                }
            }
        }
    }

    public function getPageCountries($code, $page_id) {
        $result = $this->db->query("SELECT country_id,language_id,gender,page_h1,page_title,seo_url from `" . DB_PREFIX . "page_settings` WHERE page_id='" . (int) $page_id . "' AND deleted='0'")->rows;
        $return_data = array();
        foreach ($result as $key => $value) {
            $return_data[$value['country_id']] = array('language_id' => $value['language_id'], 'gender' => $value['gender']);
            $return_data['landing_pages_title'] = $value['page_title'];
            $return_data['landing_pages_h1'] = $value['page_h1'];
            $return_data['landing_pages_seo'] = $value['seo_url'];
        }
        return $return_data;
    }

    public function getSetting($code, $page_id) {
        $setting_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_land WHERE page_id = '" . (int) $page_id . "' AND `code` = '" . $this->db->escape($code) . "'");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $setting_data[$result['key']] = $result['value'];
            } else {
                $setting_data[$result['key']] = json_decode($result['value'], true);
            }
        }

        return $setting_data;
    }

    public function getSelectedProducts($code, $page_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT sp.product_id,pd.name FROM featured_pages_selected_products sp LEFT JOIN product_description pd ON (sp.product_id=pd.product_id) where sp.page_id ='" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "' AND pd.language_id='" . (int) $this->config->get('config_language_id') . "'");

        foreach ($query->rows as $result) {
            $product_related_data[] = array(
                'product_id' => $result['product_id'],
                'name' => $result['name'],
            );
        }

        return $product_related_data;
    }

    public function getProducts($code, $page_id) {
        $product_related_data = array();

        $query = $this->db->query("SELECT mdb.product_id,pd.name,mdb.block_id FROM featured_pages_blocks mdb LEFT JOIN product_description pd ON (mdb.product_id=pd.product_id) where mdb.page_id ='" . (int) $page_id . "' AND code='" . $this->db->escape($code) . "' AND pd.language_id='" . (int) $this->config->get('config_language_id') . "' AND mdb.deleted='0' order by block_id ASC");

        foreach ($query->rows as $result) {
            $product_related_data[] = array(
                'product_id' => $result['product_id'],
                'name' => $result['name'],
                'block_id' => $result['block_id']
            );
        }

        return $product_related_data;
    }

    public function getPageCategories($page_id) {
        $product_category_data = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "landing_page_to_category WHERE landing_page_id = " . (int) $page_id . " group by category_id");
        foreach ($query->rows as $result) {
            $product_category_data[] = $result['category_id'];
        }
        return $product_category_data;
    }

    public function getPageBrands($page_id) {
        $product_brand_data = array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "landing_page_to_brand WHERE landing_page_id = " . (int) $page_id);
        foreach ($query->rows as $result) {
            $product_brand_data[] = $result['brand_id'];
        }
        return $product_brand_data;
    }

}
