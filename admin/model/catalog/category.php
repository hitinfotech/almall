<?php

class ModelCatalogCategory extends Model {

    public function addCategory($data) {
        $this->event->trigger('pre.admin.category.add', $data);

        $this->db->query("INSERT INTO " . DB_PREFIX . "category SET return_policy_id ='" . (int) $data['return_policy_id'] . "', parent_id = '" . (int) $data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int) $data['top'] : 0) . "', `column` = '" . (int) $data['column'] . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', is_algolia = '" . (int) $data['is_algolia'] . "', date_modified = NOW(), date_added = NOW()");

        $category_id = $this->db->getLastId();


        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('category', 'image', $category_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('featuredcategories') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
                ///////
                $im = new imagick(DIR_IMAGE . $image);
                $imageprops = $im->getImageGeometry();
                $old_width = (int) $imageprops['width'];
                $old_height = (int) $imageprops['height'];
                $height = $old_height;
                $width = 810;

                    $newWidth = (int) ($width);
                    $newHeight = (int) (($width / $old_width) * $old_height);

//                $im->resizeimage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1, true);
//                $im->cropImage($newWidth, $newHeight, 0, 0);
//                $im->writeImage(DIR_IMAGE .  $image);
                $this->db->query("INSERT INTO image_s3 SET image='" . $this->db->escape($image) . "' ,
                                                   new_image_path='" . $this->db->escape($image) . "' ,
                                                   width='" . $this->db->escape($newWidth) . "' ,
                                                   height='" . $this->db->escape($newHeight) . "' , 
                                                   date_added=NOW(), user_id='" . (int)$this->user->getid() . "' ");
                //$this->db->query("INSERT INTO image_s3 SET image='" . $this->db->escape($image) . "' ,new_image_path='".$this->db->escape($image)."' ,date_added=NOW(), user_id='" . (int) $this->user->getid() . "' ");

                //////
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE category SET `image` = '" . $this->db->escape($image) . "' WHERE category_id='" . (int) $category_id . "' ");
        }



        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int) $category_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int) $category_id . "', `path_id` = '" . (int) $result['path_id'] . "', `level` = '" . (int) $level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int) $category_id . "', `path_id` = '" . (int) $category_id . "', `level` = '" . (int) $level . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int) $category_id . "', filter_id = '" . (int) $filter_id . "'");
            }
        }

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int) $category_id . "', store_id = '" . (int) $store_id . "'");
            }
        }

        // Set which layout to use with this category
        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int) $category_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout_id . "'");
            }
        }

        $this->url_custom->create_URL('category', $category_id);


        $this->db->query(" INSERT IGNORE INTO algolia SET `type`='category', type_id='" . (int) $category_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        $this->cache->deletekeys('admin.category');
        $this->cache->deletekeys('catalog.category');
        $this->cache->deletekeys('catalog.brand');


        $this->event->trigger('post.admin.category.add', $category_id);

        return $category_id;
    }

    public function editCategory($category_id, $data) {
        $this->event->trigger('pre.admin.category.edit', $data);

        $this->db->query("UPDATE " . DB_PREFIX . "category SET return_policy_id ='" . (int) $data['return_policy_id'] . "', parent_id = '" . (int) $data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int) $data['top'] : 0) . "', `column` = '" . (int) $data['column'] . "', sort_order = '" . (int) $data['sort_order'] . "', status = '" . (int) $data['status'] . "', is_algolia = '" . (int) $data['is_algolia'] . "', date_modified = NOW() WHERE category_id = '" . (int) $category_id . "'");


        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('category', 'image', $category_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('featuredcategories') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                    }
                }
                ///////
                $im = new imagick(DIR_IMAGE .  $image);
                $imageprops = $im->getImageGeometry();
                $old_width = (int) $imageprops['width'];
                $old_height = (int) $imageprops['height'];
                $height = $old_height;
                $width= 810;

                    $newWidth = (int) ($width);
                    $newHeight = (int) (($width / $old_width) * $old_height);

                $this->db->query("INSERT INTO image_s3 SET image='" . $this->db->escape($image) . "' ,
                                                   new_image_path='" . $this->db->escape($image) . "' ,
                                                   width='" . $this->db->escape($newWidth) . "' ,
                                                   height='" . $this->db->escape($newHeight) . "' , 
                                                   date_added=NOW(), user_id='" . (int)$this->user->getid() . "' ");
//                $im->resizeimage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1, true);
//                $im->cropImage($newWidth, $newHeight, 0, 0);
//                $im->writeImage(DIR_IMAGE .  $image);
//                $this->db->query("INSERT INTO image_s3 SET image='" . $this->db->escape($image) . "' ,new_image_path='".$this->db->escape($image)."' ,date_added=NOW(), user_id='" . (int) $this->user->getid() . "' ");

                //////
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE category SET `image` = '" . $this->db->escape($image) . "' WHERE category_id='" . (int) $category_id . "' ");
        }
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int) $category_id . "'");

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int) $category_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE path_id = '" . (int) $category_id . "' ORDER BY level ASC");

        if ($query->rows) {
            foreach ($query->rows as $category_path) {
                // Delete the path below the current one
                $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $category_path['category_id'] . "' AND level < '" . (int) $category_path['level'] . "'");

                $path = array();

                // Get the nodes new parents
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $data['parent_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Get whats left of the nodes current path
                $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $category_path['category_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Combine the paths with a new level
                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int) $category_path['category_id'] . "', `path_id` = '" . (int) $path_id . "', level = '" . (int) $level . "'");

                    $level++;
                }
            }
        } else {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $category_id . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $data['parent_id'] . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int) $category_id . "', `path_id` = '" . (int) $result['path_id'] . "', level = '" . (int) $level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int) $category_id . "', `path_id` = '" . (int) $category_id . "', level = '" . (int) $level . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int) $category_id . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int) $category_id . "', filter_id = '" . (int) $filter_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int) $category_id . "'");

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int) $category_id . "', store_id = '" . (int) $store_id . "'");
            }
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int) $category_id . "'");

        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int) $category_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout_id . "'");
            }
        }

        $this->url_custom->create_URL('category', $category_id);

        $this->db->query(" INSERT IGNORE INTO algolia SET `type`='category', type_id='" . (int) $category_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        $this->cache->deletekeys('catalog.brand');
        $this->cache->delete('category');

        $this->event->trigger('post.admin.category.edit', $category_id);
    }

    public function deleteCategory($category_id) {
        $this->event->trigger('pre.admin.category.delete', $category_id);

        $this->db->query(" DELETE FROM " . DB_PREFIX . "category_path WHERE category_id = '" . (int) $category_id . "'");

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_path WHERE path_id = '" . (int) $category_id . "'");

        foreach ($query->rows as $result) {
            $this->deleteCategory($result['category_id']);
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "category WHERE category_id = '" . (int) $category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int) $category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int) $category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int) $category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int) $category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE category_id = '" . (int) $category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias_ar WHERE query = 'category_id=" . (int) $category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias_en WHERE query = 'category_id=" . (int) $category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "brand_to_category WHERE category_id = '" . (int) $category_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "offer_category WHERE category_id = '" . (int) $category_id . "'");

        $this->cache->delete('category');
        $this->cache->deletekeys('catalog.brand');

        $this->event->trigger('post.admin.category.delete', $category_id);
    }

    public function repairCategories($parent_id = 0) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE parent_id = '" . (int) $parent_id . "'");

        foreach ($query->rows as $category) {
            // Delete the path below the current one
            $this->db->query("DELETE FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $category['category_id'] . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int) $parent_id . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int) $category['category_id'] . "', `path_id` = '" . (int) $result['path_id'] . "', level = '" . (int) $level . "'");

                $level++;
            }

            $this->db->query("REPLACE INTO `" . DB_PREFIX . "category_path` SET category_id = '" . (int) $category['category_id'] . "', `path_id` = '" . (int) $category['category_id'] . "', level = '" . (int) $level . "'");

            $this->repairCategories($category['category_id']);
        }
    }

    public function getCategory($category_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int) $this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path, (SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias_en WHERE query = 'category_id=" . (int) $category_id . "' LIMIT 1) AS keyword FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.category_id = '" . (int) $category_id . "' AND cd2.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }
    
    public function getCategoriesByParent($parent_id = 0) {
        $cats = $this->getCategories(['parent_id' => $parent_id,'sort'=>'name']);
        foreach ($cats as $key=>$val){
            $query= $this->db->query("SELECT COUNT(*) as total FROM category WHERE parent_id = '". (int)$val['category_id'] ."' ");
            $val['is_parent'] = $query->row['total'];
            $cats[$key] = $val;
        }
        return $cats;
    }
    
    public function getOptions($data){
        $sql = "SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) LEFT JOIN " . DB_PREFIX . "option_to_category o2c  ON (o.option_id=o2c.option_id)  WHERE od.language_id = '" . (int) $this->config->get('config_language_id') . "'";
        
        if (isset($data['filter_name']) && !is_null($data['filter_name'])) {
            $sql .= " AND od.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }
        
        if (isset($data['category_id']) && !is_null($data['category_id'])) {
            $sql .= " AND o2c.category_id = '" .(int) $data['category_id'] . "'";
        }
        
        $sort_data = array(
            'od.name',
            'o.type',
            'o.sort_order'
        );
        
        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY od.name";
        }
        
        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }
        
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }
            
            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            
            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
        $query = $this->db->query($sql);
        return $query->rows;
    }
    
    public function getEnabledSize($option_id){
        $sizes = [
            'is_uk'=>1,
            'is_us'=>0,
            'is_eur'=>0,
            'is_xmls'=>0
            ];
        
        $query = $this->db->query("select * from dynamic_values where code='option_option_size_".(int)$option_id."' AND `key` IN ('is_uk','is_us','is_eur','is_xmls');");
        if(isset($query) && isset($query->rows)){
            foreach ($query->rows as $row){
                if(isset($sizes[$row['key']])){
                    $sizes[$row['key']] =  $row['value'];
                }
            }
        }
        return $sizes;
    }
    
    public function getCategoriesAttributes($category_id) {
        $attributes = array();
        $query = $this->db->query(" SELECT agt.attribute_group_id as group_id , name FROM attribute_group_to_category agt  LEFT JOIN attribute_group_description ad ON (agt.attribute_group_id = ad.attribute_group_id) WHERE agt.category_id = '" . (int) $category_id . "' AND ad.language_id = '" . (int) $this->config->get('config_language_id') . "'");
        
        if ($query->num_rows) {
            foreach ($query->rows as $group_attr) {
                $attributes[$group_attr['group_id']]['name'] = $group_attr['name'];
                $attribute_query = $this->db->query('SELECT a.attribute_id,name FROM attribute a LEFT JOIN attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE attribute_group_id=' . (int) $group_attr['group_id'] . ' and language_id= ' . (int) $this->config->get('config_language_id') . ' ');
                if ($attribute_query->num_rows > 0) {
                    $attributes[$group_attr['group_id']]['attributes'] = $attribute_query->rows;
                }
            }
        }
        return $attributes;
    }

    public function getCategories($data = array(), $language_id = FALSE) {
        if (!in_array((int) $language_id, [1, 2])) {
            $language_id = $this->config->get('config_language_id');
        }

        $sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, cd2.name as last_name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int) $language_id . "' AND cd2.language_id = '" . (int) $language_id . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['category_ids'])) {
            $sql .= " AND c1.category_id IN (" . $this->db->escape($data['category_ids']).") ";
        }
        
        if (isset($data['parent_id'])) {
            $sql .= " AND c1.parent_id = '" . (int) $data['parent_id'] . "'";
        }

        $sql .= " GROUP BY cp.category_id";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getLastLevelCategories($data = array()) {

        $sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int) $this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " AND c1.category_id NOT IN(SELECT parent_id FROM category) GROUP BY cp.category_id";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCategoryDescriptions($category_id) {
        $category_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int) $category_id . "'");

        foreach ($query->rows as $result) {
            $category_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
                'description' => $result['description']
            );
        }

        return $category_description_data;
    }

    public function getCategoryFilters($category_id) {
        $category_filter_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int) $category_id . "'");

        foreach ($query->rows as $result) {
            $category_filter_data[] = $result['filter_id'];
        }

        return $category_filter_data;
    }

    public function getCategoryStores($category_id) {
        $category_store_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_store WHERE category_id = '" . (int) $category_id . "'");

        foreach ($query->rows as $result) {
            $category_store_data[] = $result['store_id'];
        }

        return $category_store_data;
    }

      public function getCategoryChildrenById ($category_id) {
    	    $children = array();

    	    $sql = "SELECT cp.category_id as children FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_path cp ON (cp.path_id = c.category_id )  WHERE c.category_id = '".$this->db->escape($category_id)."' ";
    	    $query = $this->db->query($sql);
    	    foreach ($query->rows as $row) {
    		    $children[] = $row['children'];
    	    }

    	    return $children;
    	}

    public function getCategoryLayouts($category_id) {
        $category_layout_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int) $category_id . "'");

        foreach ($query->rows as $result) {
            $category_layout_data[$result['store_id']] = $result['layout_id'];
        }

        return $category_layout_data;
    }

    public function getTotalCategories() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category");

        return $query->row['total'];
    }

    public function getTotalCategoriesByLayoutId($layout_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int) $layout_id . "'");

        return $query->row['total'];
    }

    public function getCategoriesByName($data) {
        if ($data['filter_name']) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_description WHERE language_id = '" . (int) $this->config->get('config_language_id') . "' AND `name` LIKE '%" . $this->db->escape($data['filter_name']) . "%' ");
            return $query->rows;
        } else {
            return array();
        }
    }

    public function autocompleteForSecondLevelCategory($data = array(), $language_id = 0) {

        $sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE c1.parent_id in (1,2,3) AND cd1.language_id = '" . (int) $this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY cp.category_id";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getProductsCount() {

        $sql = "SELECT COUNT(*) AS count, category_id FROM product_to_category GROUP BY category_id ";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getShopableProductsCount() {

        $sql = "SELECT COUNT(p2c.product_id) AS count, category_id FROM product p LEFT JOIN product_to_category p2c ON(p.product_id = p2c.product_id) WHERE p.stock_status_id != 10 GROUP BY p2c.category_id ";

        $query = $this->db->query($sql);

        return $query->rows;
    }

}
