<?php

class ModelCatalogProductRanking extends Model {

  public function update($data){

    $this->db->query("UPDATE product_ranking_algorithm set user_modified= ".(int)$this->user->getId().", date_modified=NOW(), deleted='yes'");
    if(!empty($data['ranking'])){

      if(!empty($data['ranking']['visited_points'])){
        foreach($data['ranking']['visited_points']['from'] as $key => $value){
          $this->db->query("INSERT INTO product_ranking_algorithm (`code`,`from`,`to`,`value`,`user_added`,`deleted`) VALUES ('visited_points',".(float)$data['ranking']['visited_points']['from'][$key].",".(float)$data['ranking']['visited_points']['to'][$key].",".(float)$data['ranking']['visited_points']['value'][$key].", ".(int)$this->user->getId().",'no')");
        }
      }

      if(!empty($data['ranking']['cart_rate_points'])){
        foreach($data['ranking']['cart_rate_points']['from'] as $key => $value){
          $this->db->query("INSERT INTO product_ranking_algorithm (`code`,`from`,`to`,`value`,`user_added`,`deleted`) VALUES ('cart_rate_points',".(float)$data['ranking']['cart_rate_points']['from'][$key].",".(float)$data['ranking']['cart_rate_points']['to'][$key].",".(float)$data['ranking']['cart_rate_points']['value'][$key].", ".(int)$this->user->getId().",'no')");
        }
      }

      if(!empty($data['ranking']['new_product_points'])){
        foreach($data['ranking']['new_product_points']['from'] as $key => $value){
          $this->db->query("INSERT INTO product_ranking_algorithm (`code`,`from`,`to`,`value`,`user_added`,`deleted`) VALUES ('new_product_points',".(float)$data['ranking']['new_product_points']['from'][$key].",".(float)$data['ranking']['new_product_points']['to'][$key].",".(float)$data['ranking']['new_product_points']['value'][$key].", ".(int)$this->user->getId().",'no')");
        }
      }

      if(!empty($data['ranking']['favorite_points'])){
        foreach($data['ranking']['favorite_points']['from'] as $key => $value){
          $this->db->query("INSERT INTO product_ranking_algorithm (`code`,`from`,`to`,`value`,`user_added`,`deleted`) VALUES ('favorite_points',".(float)$data['ranking']['favorite_points']['from'][$key].",".(float)$data['ranking']['favorite_points']['to'][$key].",".(float)$data['ranking']['favorite_points']['value'][$key].", ".(int)$this->user->getId().",'no')");
        }
      }

      if(!empty($data['ranking']['sale_points'])){
        foreach($data['ranking']['sale_points']['from'] as $key => $value){
          $this->db->query("INSERT INTO product_ranking_algorithm (`code`,`from`,`to`,`value`,`user_added`,`deleted`) VALUES ('sale_points',".(float)$data['ranking']['sale_points']['from'][$key].",".(float)$data['ranking']['sale_points']['to'][$key].",".(float)$data['ranking']['sale_points']['value'][$key].", ".(int)$this->user->getId().",'no')");
        }
      }

      $beansTalkd = new Beanstalk(array('server1' => array(
              'host' => BEANSTALKD_SERVER,
              'port' => BEANSTALKD_SERVER_PORT,
              'weight' => 50,
              'timeout' => 10,
              // array of connections/tubes
              'connections' => array(),
      )));
      $client = $beansTalkd->getClient();
      $client->useTube('Products_algolia');
      $products_query = $this->db->query("SELECT product_id from product where status=1");
      if($products_query->num_rows){
        foreach($products_query->rows as $key => $value ){
          $client->put(0, 60, 0, $value['product_id']);
        }
      }
      $client->disconnect();


    }
  }

  public function getSettings(){
    $query = $this->db->query("SELECT `code`,`from`,`to`,`value` FROM product_ranking_algorithm  WHERE deleted='no'");
    if($query->num_rows){
      return $query->rows;
    }
    return array();
  }


}
