<?php

class ModelCatalogwkrmaadmin extends Model {

    public function viewProducts($id) {
        $sql = "SELECT pd.name,wrp.quantity,wrr.reason,pd.product_id,wrp.order_product_id FROM " . DB_PREFIX . "product_description pd LEFT JOIN " . DB_PREFIX . "wk_rma_product wrp ON (wrp.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) WHERE wrp.rma_id = '" . (int) $id . "' AND pd.language_id = '" . $this->config->get('config_language_id') . "' AND wrr.language_id = '" . $this->config->get('config_language_id') . "'";
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function chkAdminPRoduct($product_id) {
        if ($this->config->get('wk_mpaddproduct_status')) {
            $cp2p = count($this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_product WHERE product_id = '" . (int) $product_id . "' ")->rows);

            $pc = count($this->db->query("SELECT * FROM " . DB_PREFIX . "price_comparison WHERE product_id = '" . (int) $product_id . "' ")->rows);
            if ($cp2p == $pc)
                $seller = 'admin';
            else
                $seller = 'seller';
        }else {
            $cp2p = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customerpartner_to_product WHERE product_id = '" . (int) $product_id . "' ")->row;
            if (isset($cp2p['customer_id']) && $cp2p['customer_id']) {
                $seller = 'seller';
            } else {
                $seller = 'admin';
            }
        }

        return $seller;
    }

    public function viewtotal($data = array()) {

        $sql = "SELECT CONCAT(c.firstname,' ', c.lastname) AS name, wro.id, wro.order_id, wro.add_info, wro.rma_auth_no, wro.date, wro.customer_status_id as customer_st, wro.is_confirmed, rs.name rma_status, rs.color FROM " . DB_PREFIX . "wk_rma_order wro LEFT JOIN " . DB_PREFIX . "wk_rma_customer wrc ON (wro.id = wrc.rma_id) LEFT JOIN `" . DB_PREFIX . "order` c ON ((wrc.customer_id = c.customer_id || wrc.email = c.email AND c.deleted='0' ) AND c.order_id = wro.order_id) LEFT JOIN rma_status rs ON(wro.customer_status_id=rs.rma_status_id AND rs.language_id = '". (int)$this->config->get('config_language_id') ."') WHERE 1 ";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(CONCAT(c.firstname, c.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (isset($data['filter_order']) && !is_null($data['filter_order'])) {
            $implode[] = "wro.order_id = '" . (int) $data['filter_order'] . "'";
        }

        if (isset($data['filter_rma_status']) && !is_null($data['filter_rma_status'])) {
            $implode[] = "wro.customer_status_id = '" . (int) $data['filter_rma_status'] . "'";
        }

        if (!empty($data['filter_date'])) {
            $implode[] = "LCASE(wro.date) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'c.firstname',
            'c.order_id',
            'wrr.id',
            'wro.date',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY c.firstname";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function viewtotalentry($data = array()) {

        $sql = "SELECT CONCAT(c.firstname,' ', c.lastname) AS name,wro.id,wro.order_id,wro.add_info,wro.rma_auth_no,wro.date,wro.customer_status_id as customer_st FROM " . DB_PREFIX . "wk_rma_order wro LEFT JOIN " . DB_PREFIX . "wk_rma_customer wrc ON (wro.id = wrc.rma_id) LEFT JOIN `" . DB_PREFIX . "order` c ON ((wrc.customer_id = c.customer_id || wrc.email = c.email ) AND c.order_id = wro.order_id   AND c.deleted='0' ) WHERE 1 ";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(CONCAT(c.firstname, c.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (isset($data['filter_order']) && !is_null($data['filter_order'])) {
            $implode[] = "wro.order_id = '" . (int) $data['filter_order'] . "'";
        }

        if (isset($data['filter_rma_status']) && !is_null($data['filter_rma_status'])) {
            $implode[] = "wro.customer_status_id = '" . (int) $data['filter_rma_status'] . "'";
        }

        if (isset($data['filter_customer_status']) && !is_null($data['filter_customer_status']))
    			$implodeInner = " AND wrs.id = '" . (int)$data['filter_customer_status'] . "'";



        if (!empty($data['filter_date'])) {
            $implode[] = "LCASE(wro.date) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'c.firstname',
            'c.order_id',
            'wrr.id',
            'wro.date',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY c.firstname";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        $result = $this->db->query($sql);

        return count($result->rows);
    }

    public function deleteentry($id) {

        $imagefolder = $this->db->query("SELECT images FROM " . DB_PREFIX . "wk_rma_order WHERE id = '" . (int) $id . "'")->row;

        $this->db->query("DELETE FROM " . DB_PREFIX . "wk_rma_order WHERE id = '" . (int) $id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "wk_rma_order_message WHERE rma_id = '" . (int) $id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "wk_rma_customer WHERE rma_id = '" . (int) $id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "wk_rma_product WHERE rma_id = '" . (int) $id . "'");

        if ($imagefolder) {
            $dirPath = DIR_IMAGE . 'rma/' . $imagefolder['images'];
            if (file_exists($dirPath)) {
                $this->deleteDir($dirPath);
            }
        }
    }

    public function deleteDir($dirPath) {
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        if (file_exists($dirPath))
            rmdir($dirPath);
    }

    public function viewtotalMessageBy($data) {

        $sql = "SELECT * FROM " . DB_PREFIX . "wk_rma_order_message wrm WHERE wrm.rma_id = '" . (int) $data['filter_id'] . "'";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(wrm.writer) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (isset($data['filter_message']) && !empty($data['filter_message'])) {
            $implode[] = "LCASE(wrm.message) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_message'])) . "%'";
        }

        if (isset($data['filter_date']) && !empty($data['filter_date'])) {
            $implode[] = "LCASE(wrm.date) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'wrm.writer',
            'wrm.message',
            'wrm.date',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY wrm.id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function viewtotalNoMessageBy($data) {

        $sql = "SELECT * FROM " . DB_PREFIX . "wk_rma_order_message wrm WHERE wrm.rma_id = '" . (int) $data['filter_id'] . "'";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(wrm.writer) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (isset($data['filter_message']) && !empty($data['filter_message'])) {
            $implode[] = "LCASE(wrm.message) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_message'])) . "%'";
        }

        if (isset($data['filter_date']) && !empty($data['filter_date'])) {
            $implode[] = "LCASE(wrm.date) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $result = $this->db->query($sql);

        return count($result->rows);
    }

    public function updateAdminStatus($msg, $status, $vid, $file_name) {

        $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_order SET customer_status_id='" . (int) $status . "' WHERE id='" . (int) $vid . "' ");
        $this->load->model('localisation/rmaquantity');
        $this->model_localisation_rmaquantity->ChangeProductOrderQuantity($vid);

        if ($msg != '') {
            $this->db->query("INSERT INTO " . DB_PREFIX . "wk_rma_order_message SET rma_id = '" . $this->db->escape($vid) . "', writer = 'admin', message = '" . $this->db->escape(nl2br($msg)) . "', date = NOW(), attachment = '" . $this->db->escape($file_name) . "'");
        }

        $status_info = $this->db->query("SELECT * FROM rma_status WHERE rma_status_id = '".(int)$status."' AND language_id = '".(int)$this->config->get('config_language_id')."' ");
        $status_name = ($status_info->num_rows>0?$status_info->row['name']:'');

        $data = array('status' => $status_name,
            'rma_id' => $vid,
            'message' => $msg,
        );


        $this->load->model('catalog/rma_mail');
        $this->model_catalog_rma_mail->mail($data, 'message_to_customer');
    }

    public function addLabel($id, $file, $folder = '') {
        $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_order SET `shipping_label` = '" . $this->db->escape($file) . "' WHERE id = '" . (int) $id . "'");

        $data = array('rma_id' => $id,
            'link' => HTTP_CATALOG . 'index.php?route=account/rma/viewrma/printlable&vid=' . $id,
            'label' => 'rma/' . $folder . '/files/' . $file
        );

        $this->load->model('catalog/rma_mail');
        $this->model_catalog_rma_mail->mail($data, 'label_to_customer');
    }

    public function getRmaOrderid($id) {
        $result = $this->db->query("SELECT wro.*,wro.customer_status_id as customer_st, rs.name rma_status, rs.color FROM " . DB_PREFIX . "wk_rma_order wro LEFT JOIN " . DB_PREFIX . "wk_rma_customer wrc ON (wrc.rma_id = wro.id) LEFT JOIN rma_status rs ON(wro.customer_status_id=rs.rma_status_id AND rs.language_id='". (int)$this->config->get('config_language_id') ."') WHERE wro.id ='" . (int) $id . "' ");
        return $result->row;
    }

    //for reason
    public function viewreason($data) {
        $sql = "SELECT * FROM " . DB_PREFIX . "wk_rma_reason wrr WHERE language_id ='" . $this->config->get('config_language_id') . "' AND status_info != 'seller' ";

        $implode = array();

        if (!empty($data['filter_reason'])) {
            $implode[] = "LCASE(wrr.reason) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_reason'])) . "%'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "wrr.status = '" . (int) $data['filter_status'] . "'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'wrr.reason',
            'wrr.status',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $result = $this->db->query($sql);

        return $result->rows;
    }


  public function viewreasonbyId($id) {
      $sql = $this->db->query("SELECT * FROM " . DB_PREFIX . "wk_rma_reason WHERE reason_id='" . (int) $id . "'");
      return $sql->rows;
  }

	public function viewCustomerDetails($order_id){
		$result = $this->db->query("SELECT o.firstname,o.lastname FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '$order_id'  AND o.deleted='0'  ");
		return $result->row;
	}

    public function viewtotalreason($data) {
        $sql = "SELECT * FROM " . DB_PREFIX . "wk_rma_reason wrr WHERE language_id ='" . $this->config->get('config_language_id') . "' AND status_info != 'seller' ";

        $implode = array();

        if (!empty($data['filter_reason'])) {
            $implode[] = "LCASE(wrr.reason) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_reason'])) . "%'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "wrr.status = '" . (int) $data['filter_status'] . "'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $result = $this->db->query($sql);
        return count($result->rows);
    }

    public function deleteReason($id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "wk_rma_reason WHERE reason_id = '" . (int) $id . "'");
    }

    public function addReason($data) {
        $reason_id = 1;
        $last_reason_id = $this->db->query("SELECT reason_id FROM " . DB_PREFIX . "wk_rma_reason ORDER BY reason_id DESC LIMIT 1")->row;
        if (isset($last_reason_id['reason_id']))
            $reason_id = $last_reason_id['reason_id'] + 1;
        foreach ($data['reason'] as $key => $value)
            $this->db->query("INSERT INTO " . DB_PREFIX . "wk_rma_reason SET reason_id = '" . (int) $reason_id . "',`customer_id` = NULL,`reason` = '" . $this->db->escape($value) . "',`language_id` ='" . (int) $key . "', `status` = '" . (int) $data['status'] . "'");
    }

    public function UpdateReason($data) {

        $reason_id = $data['id'];
        foreach ($data['reason'] as $key => $value)
            $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_reason SET `reason` = '" . $this->db->escape($value) . "',`status` = '" . (int) $data['status'] . "' WHERE reason_id = '" . (int) $reason_id . "' AND `language_id` ='" . (int) $key . "'");
    }

    public function getCustomerReason() {
        $sql = $this->db->query("SELECT reason ,id FROM " . DB_PREFIX . "wk_rma_reason WHERE language_id ='" . $this->config->get('config_language_id') . "' AND status = 1 ORDER BY id ");
        return $sql->rows;
    }

    public function getOrderProducts($order_id, $id) {

        $sql = $this->db->query("SELECT op.product_id,op.name,op.model,op.quantity as ordered,op.tax tax,op.price,wrp.quantity as returned,op.order_product_id,wrr.reason FROM " . DB_PREFIX . "wk_rma_product wrp LEFT JOIN " . DB_PREFIX . "order_product op ON (wrp.product_id = op.product_id AND wrp.order_product_id = op.order_product_id) LEFT JOIN " . DB_PREFIX . "wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) WHERE op.order_id = '" . (int) $order_id . "' AND wrp.rma_id='" . (int) $id . "' AND wrr.language_id = '" . $this->config->get('config_language_id') . "'")->rows;
        if (!$sql) {
            $sql = $this->db->query("SELECT p.product_id,pd.name,p.model,p.price,wrp.quantity as returned,wrr.reason FROM " . DB_PREFIX . "wk_rma_product wrp LEFT JOIN " . DB_PREFIX . "product p ON (wrp.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) WHERE wrp.rma_id='" . (int) $id . "' AND pd.language_id = '" . $this->config->get('config_language_id') . "' AND wrr.language_id = '" . $this->config->get('config_language_id') . "'")->rows;

            if ($sql)
                foreach ($sql as $key => $value) {
                    $sql[$key]['ordered'] = '0';
                    $sql[$key]['tax'] = '0';
                    $sql[$key]['order_product_id'] = 0;
                }
        }

        return $sql;
    }

    public function returnQty($rma_id) {

        $sql = $this->db->query("SELECT order_id FROM " . DB_PREFIX . "wk_rma_order WHERE id ='" . (int) $rma_id . "'")->row;

        $order_product_query = false;

        if ($sql)
            $order_product_query = $this->getOrderProducts($sql['order_id'], $rma_id);

        if ($order_product_query) {

            //load opencart order model and get order info
            $this->load->model('sale/order');
            $This_order = $this->model_sale_order->getOrder($sql['order_id']);

            //make shipping ,payment, store array for get tax
            $shipping_address = array(
                'country_id' => $This_order['shipping_country_id'],
                'zone_id' => $This_order['shipping_zone_id']
            );

            $payment_address = array(
                'country_id' => $This_order['payment_country_id'],
                'zone_id' => $This_order['payment_zone_id']
            );

            $store_address = array(
                'country_id' => $this->config->get('config_country_id'),
                'zone_id' => $this->config->get('config_zone_id')
            );

            foreach ($order_product_query as $order_product) {

                if ($order_product['returned'] <= $order_product['ordered']) {

                    $product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int) $order_product['product_id'] . "' ")->row;


                    if (isset($product['subtract']) && $product['subtract']) {
                        $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity + " . (int) $order_product['returned'] . ") WHERE product_id = '" . (int) $order_product['product_id'] . "' AND subtract = '1'");
                    }
                		$sql = $this->db->query("SELECT op.product_id,op.name,op.model,op.quantity as ordered,op.tax tax,op.price,wrp.quantity as returned,op.order_product_id,wrr.reason FROM " . DB_PREFIX . "wk_rma_product wrp LEFT JOIN ".DB_PREFIX."order_product op ON (wrp.product_id = op.product_id AND wrp.order_product_id = op.order_product_id AND op.deleted='0') LEFT JOIN ".DB_PREFIX."wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) WHERE op.order_id = '".(int)$order_id."' AND wrp.rma_id='".(int)$id."' AND wrr.language_id = '".$this->config->get('config_language_id')."'")->rows;
                		if(!$sql){
                			$sql = $this->db->query("SELECT p.product_id,pd.name,p.model,p.price,wrp.quantity as returned,wrr.reason FROM " . DB_PREFIX . "wk_rma_product wrp LEFT JOIN ".DB_PREFIX."product p ON (wrp.product_id = p.product_id) LEFT JOIN ".DB_PREFIX."product_description pd ON (p.product_id = pd.product_id) LEFT JOIN ".DB_PREFIX."wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) WHERE wrp.rma_id='".(int)$id."' AND pd.language_id = '".$this->config->get('config_language_id')."' AND wrr.language_id = '".$this->config->get('config_language_id')."'")->rows;

                    $order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int) $sql['order_id'] . "' AND order_product_id = '" . (int) $order_product['order_product_id'] . "'")->rows;

                    if (!empty($order_option_query)) {
                        foreach ($order_option_query as $option) {
                            $chk_subtract = $this->db->query("SELECT subtract FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . (int) $option['product_option_value_id'] . "' ")->row;

                            if (isset($chk_subtract['subtract']) && $chk_subtract['subtract']) {
                                $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int) $order_product['returned'] . ") WHERE product_option_value_id = '" . (int) $option['product_option_value_id'] . "' AND subtract = '1'");
                            }
                        }
                    }

                    $this->db->query("UPDATE " . DB_PREFIX . "order_product SET quantity = (quantity - " . (int) $order_product['returned'] . "), `total` = (`total`-(`total`*" . (int) $order_product['returned'] . ")/" . (int) $order_product['ordered'] . "), reward = (`reward`-(`reward`*" . (int) $order_product['returned'] . ")/" . (int) $order_product['ordered'] . ") WHERE order_product_id = '" . (int) $order_product['order_product_id'] . "'");
                } else { //remove product from order
                    // $this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_product_id = '" . (int)$order_product['order_product_id'] . "'");
                    // $this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_product_id = '" . (int)$order_product['order_product_id'] . "'");
                }
            }
          }



            //get tax from function
            $tax_class_id = $this->db->query("SELECT tax_class_id FROM " . DB_PREFIX . "product WHERE product_id = '" . $order_product['product_id'] . "'")->row;

            $tax_per_product = 0;
            $ItemBasePrice = $this->currency->convert($order_product['price'], $this->config->get('config_currency'), $This_order['currency_code']);
            if ($tax_class_id && isset($This_order['customer_group_id']))
                $tax_per_product = $this->getRates($ItemBasePrice, $tax_class_id['tax_class_id'], $This_order['customer_group_id'], $shipping_address, $payment_address, $store_address);

            if ($tax_per_product) {
                foreach ($tax_per_product as $key => $value) {
                    $this->db->query("UPDATE " . DB_PREFIX . "order_total SET value = (value - " . (float) $value['amount'] * $order_product['returned'] . ") WHERE title = '" . $this->db->escape($value['name']) . "' AND code = 'tax' AND order_id = '" . $sql['order_id'] . "'");
                }
            }

            // code for update total price of order
            $getProducts = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . $sql['order_id'] . "'")->rows;

            if ($getProducts) {
                $sub_total = $total = 0;
                foreach ($getProducts as $key => $value) {
                    $sub_total = $sub_total + $value['total'];
                }
                $getOrderTotal = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . $sql['order_id'] . "'")->rows;

                foreach ($getOrderTotal as $key => $value) {
                    if ($value['code'] != 'sub_total' AND $value['code'] != 'total')
                        $total = $total + $value['value'];
                }
                $total = $total + $sub_total;
                $subtotalWidCurrency = $this->currency->format($sub_total, $This_order['currency_code']);
                $totalWidCurrency = $this->currency->format($total, $This_order['currency_code']);

                $this->db->query("UPDATE " . DB_PREFIX . "order_total SET value = '" . (float) $total . "' WHERE order_id = '" . $sql['order_id'] . "' AND code = 'total'");
                $this->db->query("UPDATE " . DB_PREFIX . "order_total SET value = '" . (float) $sub_total . "' WHERE order_id = '" . $sql['order_id'] . "' AND code = 'sub_total'");
            }else { // delete order from table
                // $this->db->query("DELETE FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM " . DB_PREFIX . "order_fraud WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM `" . DB_PREFIX . "order_history` WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$sql['order_id'] . "'");
            }

            $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_order SET customer_status_id = '4' WHERE id = '" . (int) $rma_id . "'");
            $this->load->model('localisation/rmaquantity');
            $this->model_localisation_rmaquantity->ChangeProductOrderQuantity($rma_id);

            //$this->mailToAdminMessage(false,$rma_id);
        }
    }

    public function getRates($value, $tax_class_id, $customer_group_id, $shipping_address, $payment_address, $store_address) {
        $tax_rates = array();

        if (!$customer_group_id) {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        if ($shipping_address) {
            $tax_query = $this->db->query("SELECT tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.tax_class_id = '" . (int) $tax_class_id . "' AND tr1.based = 'shipping' AND tr2cg.customer_group_id = '" . (int) $customer_group_id . "' AND z2gz.country_id = '" . (int) $shipping_address['country_id'] . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int) $shipping_address['zone_id'] . "') ORDER BY tr1.priority ASC");

            foreach ($tax_query->rows as $result) {
                $tax_rates[$result['tax_rate_id']] = array(
                    'tax_rate_id' => $result['tax_rate_id'],
                    'name' => $result['name'],
                    'rate' => $result['rate'],
                    'type' => $result['type'],
                    'priority' => $result['priority']
                );
            }
        }

        if ($payment_address) {
            $tax_query = $this->db->query("SELECT tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.tax_class_id = '" . (int) $tax_class_id . "' AND tr1.based = 'payment' AND tr2cg.customer_group_id = '" . (int) $customer_group_id . "' AND z2gz.country_id = '" . (int) $payment_address['country_id'] . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int) $payment_address['zone_id'] . "') ORDER BY tr1.priority ASC");

            foreach ($tax_query->rows as $result) {
                $tax_rates[$result['tax_rate_id']] = array(
                    'tax_rate_id' => $result['tax_rate_id'],
                    'name' => $result['name'],
                    'rate' => $result['rate'],
                    'type' => $result['type'],
                    'priority' => $result['priority']
                );
            }
        }

        if ($store_address) {
            $tax_query = $this->db->query("SELECT tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.tax_class_id = '" . (int) $tax_class_id . "' AND tr1.based = 'store' AND tr2cg.customer_group_id = '" . (int) $customer_group_id . "' AND z2gz.country_id = '" . (int) $store_address['country_id'] . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int) $store_address['zone_id'] . "') ORDER BY tr1.priority ASC");

            foreach ($tax_query->rows as $result) {
                $tax_rates[$result['tax_rate_id']] = array(
                    'tax_rate_id' => $result['tax_rate_id'],
                    'name' => $result['name'],
                    'rate' => $result['rate'],
                    'type' => $result['type'],
                    'priority' => $result['priority']
                );
            }
        }

        $tax_rate_data = array();

        foreach ($tax_rates as $tax_rate) {
            if (isset($tax_rate_data[$tax_rate['tax_rate_id']])) {
                $amount = $tax_rate_data[$tax_rate['tax_rate_id']]['amount'];
            } else {
                $amount = 0;
            }

            if ($tax_rate['type'] == 'F') {
                $amount += $tax_rate['rate'];
            } elseif ($tax_rate['type'] == 'P') {
                $amount += ($value / 100 * $tax_rate['rate']);
            }

            $tax_rate_data[$tax_rate['tax_rate_id']] = array(
                'tax_rate_id' => $tax_rate['tax_rate_id'],
                'name' => $tax_rate['name'],
                'rate' => $tax_rate['rate'],
                'type' => $tax_rate['type'],
                'amount' => $amount
            );
        }

        return $tax_rate_data;
    }

    public function mailToAdminMessage($msg = false, $vid) {

        $order_id = $this->db->query("SELECT order_id FROM wk_rma_order WHERE id='" . (int) $vid . "'")->row['order_id'];

        $this->load->model('sale/order');
        $order_info = $this->model_sale_order->getOrder($order_id);
        if ($order_info) {
            $order_id = $order_info['order_id'];
            $order_status = $order_info['order_status_id'];

            $product = $this->db->query("SELECT product_id from wk_rma_product WHERE rma_id='" . (int) $vid . "'")->row;

            $order_product_query = $this->db->query("SELECT op.product_id,op.order_product_id,op.total,op.price, op.name, op.model, op.quantity,p.sku FROM " . DB_PREFIX . "order_product op LEFT JOIN product p ON (op.product_id = p.product_id) WHERE order_id = '" . (int) $order_id . "' AND p.product_id = '" . $product['product_id'] . "'");

            $mailToSellers = array();

            $shipping = 0;

            $data = array();

            if ($order_info['payment_address_format']) {
                $format = $order_info['payment_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['payment_firstname'],
                'lastname' => $order_info['payment_lastname'],
                'company' => $order_info['payment_company'],
                'address_1' => $order_info['payment_address_1'],
                'address_2' => $order_info['payment_address_2'],
                'city' => $order_info['payment_city'],
                'postcode' => $order_info['payment_postcode'],
                'zone' => $order_info['payment_zone'],
                'zone_code' => $order_info['payment_zone_code'],
                'country' => $order_info['payment_country']
            );

            $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $data['payment_method'] = $order_info['payment_method'];

            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['shipping_address_2'],
                'city' => $order_info['shipping_city'],
                'postcode' => $order_info['shipping_postcode'],
                'zone' => $order_info['shipping_zone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country']
            );

            $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $data['shipping_method'] = $order_info['shipping_method'];

            $sub_total = 0;
            foreach ($order_product_query->rows as $product) {

                $prsql = '';

                $mpSellers = $this->db->query("SELECT c2c.email,c2c.customer_id,p.product_id,p.subtract,c2c.commission,c2cd.screenname FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "customerpartner_to_product c2p ON (p.product_id = c2p.product_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer c2c ON (c2c.customer_id = c2p.customer_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer_description c2cd ON (c2c.customer_id = c2cd.customer_id AND c2cd.language_id='1') WHERE p.product_id = '" . $product['product_id'] . "' $prsql ORDER BY c2p.id ASC ")->row;

                if (isset($mpSellers['email']) AND ! empty($mpSellers['email'])) {

                    $option_data = array();

                    $seller_name = $mpSellers['screenname'];

                    $order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . (int) $product['order_product_id'] . "'");

                    foreach ($order_option_query->rows as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['value'];
                        } else {
                            $value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
                        }

                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                        );
                    }

                    $product_total = $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0);
                    $sub_total += $product['total'];
                    $products = array(
                        'product_id' => $product['product_id'],
                        'name' => $product['name'],
                        'model' => $product['model'],
                        'sku' => $product['sku'],
                        'option' => $option_data,
                        'quantity' => $product['quantity'],
                        'product_total' => $product_total, // without symbol
                        'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                        'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
                    );



                    $i = 0;

                    if ($mailToSellers) {

                        foreach ($mailToSellers as $key => $value) {
                            foreach ($value as $key1 => $value1) {
                                $i = 0;
                                if ($key1 == 'email' AND $value1 == $mpSellers['email']) {
                                    $mailToSellers[$key]['products'][] = $products;
                                    $mailToSellers[$key]['total'] = $product_total + $mailToSellers[$key]['total'];
                                    break;
                                } else {
                                    $i++;
                                }
                            }
                        }
                    } else {
                        $mailToSellers[] = array('email' => $mpSellers['email'],
                            'customer_id' => $mpSellers['customer_id'],
                            'seller_email' => $mpSellers['email'],
                            'products' => array(0 => $products),
                            'total' => $product_total
                        );
                    }

                    if ($i > 0) {
                        $mailToSellers[] = array('email' => $mpSellers['email'],
                            'customer_id' => $mpSellers['customer_id'],
                            'seller_email' => $mpSellers['email'],
                            'products' => array(0 => $products),
                            'total' => $product_total
                        );
                    }
                }
            }


            if ($this->config->get('marketplace_mailtoseller')) {

                // Send out order confirmation mail
                $this->load->language('default');
                $this->load->language('mail/rma');

                $subject = sprintf($this->language->get('text_new_subject'), $order_info['store_name'], $order_id);

                $data['title'] = sprintf($this->language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

                $data['text_greeting'] = sprintf($this->language->get('text_greeting'), $seller_name);
                $data['text_shipping'] = $this->language->get('text_shipping');

                $data['text_link'] = $this->language->get('text_new_link');
                $data['text_order_detail'] = $this->language->get('text_new_order_detail');
                $data['text_order_status'] = $this->language->get('text_order_status');

                $data['text_instruction'] = $this->language->get('text_new_instruction');
                $data['text_order_id'] = $this->language->get('text_new_order_id');
                $data['text_date_added'] = $this->language->get('text_new_date_added');
                $data['text_payment_method'] = $this->language->get('text_new_payment_method');
                $data['text_shipping_method'] = $this->language->get('text_new_shipping_method');
                $data['text_email'] = $this->language->get('text_new_email');
                $data['text_telephone'] = $this->language->get('text_new_telephone');
                $data['text_ip'] = $this->language->get('text_new_ip');
                $data['text_payment_address'] = $this->language->get('text_new_payment_address');
                $data['text_shipping_address'] = $this->language->get('text_new_shipping_address');
                $data['text_product'] = $this->language->get('text_new_product');
                $data['text_model'] = $this->language->get('text_new_model');
                $data['text_sku'] = $this->language->get('text_new_sku');
                $data['text_quantity'] = $this->language->get('text_new_quantity');
                $data['text_price'] = $this->language->get('text_new_price');
                $data['text_total'] = $this->language->get('text_new_total');
                //$data['text_footer'] = $this->language->get('text_new_footer');
                $data['text_powered'] = $this->language->get('text_new_powered');
                $data['text_buyer_name'] = $this->language->get('text_buyer_name');
                $data['text_buyer_country'] = $this->language->get('text_buyer_country');
                $data['text_item_number'] = $this->language->get('text_item_number');
                $data['text_below_order_summary'] = $this->language->get('text_below_order_summary');
                $data['text_order_number'] = $this->language->get('text_order_number');
                $data['text_order_date'] = $this->language->get('text_order_date');
                $data['text_rma_date'] = $this->language->get('text_rma_date');

                $data['text_reason_for_return'] = $this->language->get('text_reason_for_return');
                $data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
                $data['store_name'] = $order_info['store_name'];
                $data['store_url'] = $order_info['store_url'];
                $data['customer_id'] = $order_info['customer_id'];
                $data['link'] = $order_info['store_url'] . 'index.php?route=account/customerpartner/orderinfo&order_id=' . $order_id;
                $data['order_id'] = $order_id;
                $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
                $data['payment_method'] = $order_info['payment_method'];
                $data['shipping_method'] = $order_info['shipping_method'];
                $data['email'] = $order_info['email'];
                $data['telephone'] = $order_info['telephone'];
                $data['ip'] = $order_info['ip'];
                $data['order_status'] = $order_status;
                $data['buyer_name'] = $order_info['payment_firstname'] . " " . $order_info['payment_lastname'];
                $data['buyer_country'] = $order_info['payment_country'];
                $data['rma_date'] = date("M d, Y");
                $data['rma_details_page'] = $this->url->link("account/rma/viewrma", 'vid=' . $vid, 'SSL');
                $data['rma_id'] = $vid;
                $data['rma_link'] = $order_info['store_url'] . 'admin/index.php?route=catalog/wk_rma_admin/getForm&id=' . $vid;

                $data['comment'] = false;

                // Products
                $data['products'] = array();

                // Text Mail for seller
                $textBasic = $this->language->get('text_new_order_id') . ' ' . $order_id . "\n";
                $textBasic .= $this->language->get('text_new_date_added') . ' ' . date($this->language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
                $textBasic .= $this->language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

                // Order Totals
                $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int) $order_id . "' ORDER BY sort_order ASC");

                $original_sub_total = 1;
                $total_value = 0;
                $order_countries = $this->db->query("SELECT count(DISTINCT country_id) AS count FROM customerpartner_to_order c2o LEFT JOIN customerpartner_to_customer c2c ON (c2o.customer_id= c2c.customer_id) WHERE c2o.order_id='" . (int) $order_id . "'")->row['count'];

                foreach ($order_total_query->rows as $total) {
                    if ($total['code'] == 'sub_total') {
                        $total_data[] = array(
                            'title' => $total['title'],
                            'text' => $this->currency->format($sub_total, $order_info['currency_code'], $order_info['currency_value']),
                        );
                        $original_sub_total = $total['value'];
                        $total_value += $sub_total;
                    } elseif ($total['code'] == 'shipping') {
                        $total_data[] = array(
                            'title' => $total['title'],
                            'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                        );
                        $total_value += $total['value'] / $order_countries;
                    } elseif ($total['code'] == 'coupon') {
                        $total_data[] = array(
                            'title' => $total['title'],
                            'text' => $this->currency->format($total['value'] * ($sub_total / $original_sub_total), $order_info['currency_code'], $order_info['currency_value']),
                        );
                        $total_value += $total['value'] * ($sub_total / $original_sub_total);
                    } elseif ($total['code'] == 'total') {
                        $total_data[] = array(
                            'title' => $total['title'],
                            'text' => $this->currency->format($total_value, $order_info['currency_code'], $order_info['currency_value']),
                        );
                    } else {
                        $total_data[] = array(
                            'title' => $total['title'],
                            'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                        );
                        $total_value += $total['value'] / $order_countries;
                    }
                }
                $data['totals'] = $total_data;

                foreach ($mailToSellers as $value) {

                    //for template for seller
                    $data['products'] = $value['products'];

                    $data['email_header'] = $this->load->view('mail/header.tpl');
                    $data['email_footer'] = $this->load->view('mail/footer.tpl');


                    $seller_html = $this->load->view('mail/rma.tpl', $data);
                    $shipper_html = $this->load->view('mail/ship_rma.tpl', $data);

                    //for text for seller
                    $products = $value['products'];
                    $text = $textBasic;
                    foreach ($products as $product) {
                        $text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($product['total']) . "\n";

                        foreach ($product['option'] as $option) {
                            $text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($option['value'])) . "\n";
                        }
                    }

                    $text .= $this->language->get('text_new_order_total') . "\n";

                    $text .= $this->currency->format($value['total'], $order_info['currency_code'], 1) . ': ' . html_entity_decode($this->currency->format($value['total'], $order_info['currency_code'], 1), ENT_NOQUOTES, 'UTF-8') . "\n";
                }

                $mailData['title'] = $this->language->get('text_rma_title');

                $values['message'] = $seller_html;
                $values['subject'] = sprintf($this->language->get('seller_subject'), $order_id);
                $mailData['mail_id'] = $this->config->get('wk_rma_customer_mail');
                $mailData['mail_from'] = $this->config->get('marketplace_adminmail');
                $mailData['mail_to'] = $value['seller_email'];
                $mailData['c_id'] = $value['customer_id'];
                $this->mail($mailData, $values);

                $values['message'] = $shipper_html;
                $values['subject'] = sprintf($this->language->get('shipper_subject'), $order_id);
                $mailData['mail_id'] = $this->config->get('wk_rma_admin_mail');
                $mailData['mail_from'] = $this->config->get('marketplace_adminmail');
                $mailData['mail_to'] = $this->config->get('marketplace_adminmail');
                $this->mail($mailData, $values);
            }
        }
    }

    public function mail($data, $values) {

        $value_index = array();

        $mail_id = $data['mail_id'];
        $mail_from = $data['mail_from'];
        $mail_to = $data['mail_to'];

        if (isset($data['c_id']) AND $data['c_id']) {
            $seller_info = $this->getSeller($data['c_id']);
        } else {
            $seller_info['firstname'] = '';
            $seller_info['lastname'] = '';
        }

        if (isset($data['customer_id']) AND $data['customer_id']) {
            $customer_info = $this->getCustomer($data['customer_id']);
        }

        $value_index = $values;

        $data['store_name'] = $this->config->get('config_name');
        $data['store_url'] = HTTP_SERVER;
        $data['logo'] = HTTP_SERVER . 'image/' . $this->config->get('config_logo');

        $mailValues = str_replace('<br />', ',', nl2br($this->config->get('marketplace_mail_keywords')));
        $mailValues = explode(',', $mailValues);
        $find = array();
        $replace = array();
        foreach ($mailValues as $key => $value) {
            $value = str_replace('{', '', $value);
            $value = str_replace('}', '', $value);
            $find[trim($value)] = '{' . trim($value) . '}';
            $replace[trim($value)] = '';
        }

        $html = $this->load->view('mail/mail.tpl', $value_index);

        if (preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $mail_to) AND preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $mail_from)) {

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
            $mail->setTo($mail_to);
            $mail->setFrom($mail_from);
            $mail->setSender($data['store_name']);
            $mail->setSubject($values['subject']);
            $mail->setHtml($html);
            $mail->send();

            if (isset($seller_info['fbs_email']) && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $seller_info['fbs_email'])) {

                $mail->setTo($seller_info['fbs_email']);
                $mail->send();
            }
        }
    }

    public function confirm_rma($id) {
        $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_order set is_confirmed = 1 where id='" . (int) $id . "'");

        $this->mailToAdminMessage(false, $id);
    }

    public function getSeller($id) {
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer c2c WHERE c2c.customer_id = '" . (int) $id . "'")->row;
    }

}
