<?php

class ModelCatalogOption extends Model {

    public function addOption($data) {

        $this->event->trigger('pre.admin.option.add', $data);

        $this->db->query("INSERT INTO `" . DB_PREFIX . "option` SET type = '" . $this->db->escape($data['type']) . "', sort_order = '" . (int) $data['sort_order'] . "', cod = '" . (int) $data['cod'] . "'");

        $option_id = $this->db->getLastId();

        foreach ($data['option_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . (int) $option_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', front_name = '" . $this->db->escape($value['front_name']) . "'");
        }

        if (isset($data['option_value']) && $data['type'] !='size') {
            foreach ($data['option_value'] as $option_value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int) $option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $option_value['sort_order'] . "'");

                $option_value_id = $this->db->getLastId();

                foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int) $option_value_id . "', language_id = '" . (int) $language_id . "', option_id = '" . (int) $option_id . "', name = '" . $this->db->escape($option_value_description['name']) . "'");
                }
            }
        }else if($data['type']=='size' && isset($data['size_op'])){
            $is_us = (isset($data['is_us']) && $data['is_us'] == true);
            $is_eur = (isset($data['is_eur']) && $data['is_eur'] == true);
            $is_xmls = (isset($data['is_xmls']) && $data['is_xmls'] == true);
            $is_it = (isset($data['is_it']) && $data['is_it'] == true);
            $this->setEnabledSize($option_id,'is_uk',1);
            $this->setEnabledSize($option_id,'is_us',$is_us ? 1 : 0);
            $this->setEnabledSize($option_id,'is_eur',$is_eur ? 1 : 0);
            $this->setEnabledSize($option_id,'is_xmls',$is_xmls ? 1 : 0);
            $this->setEnabledSize($option_id,'is_it',$is_it ? 1 : 0);
                foreach ($data['size_op'] as $size){


                    $sort_order = (int) (isset($size['sort_order']) && $size != '') ? $size['sort_order'] : 0;
                    $uk = (isset($size['uk']) && $size['uk']!='') ? $size['uk'] : 0;
                    $us = (isset($size['us']) && $size['us']!='') ? $size['us'] : 0;
                    $eur = (isset($size['eur']) && $size['eur']!='') ? $size['eur'] : 0;
                    $xmls = (isset($size['xmls']) && $size['xmls']!='') ? $size['xmls'] : 0;
                    $it = (isset($size['it']) && $size['it']!='') ? $size['it'] : 0;


                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int) $option_id . "', sort_order = '" . $sort_order . "'");
                    $option_value_id = $this->db->getLastId();
                    foreach ([1,2] as $language_id) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET 
                        option_value_id = '" . (int) $option_value_id . "', 
                        language_id = '" . (int) $language_id . "',
                        option_id = '" . (int) $option_id . "', 
                        name = '" . $uk . "'");
                    }


                    $q  = "INSERT INTO  `option_to_size` set `option_value_id`='$option_value_id' , `uk`='$uk' , `us`='$us',`eur`='$eur',`xmls`='$xmls',`it`='$it' ";

                    $this->db->query($q);

                }


        }

        if (isset($data['option_category'])) {
            foreach ($data['option_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "option_to_category SET option_id = '" . (int) $option_id . "', category_id = '" . (int) $category_id . "'");
            }
        }

        $this->event->trigger('post.admin.option.add', $option_id);

        return $option_id;
    }

    public function editOption($option_id, $data) {
        $this->event->trigger('pre.admin.option.edit', $data);

        $this->db->query("UPDATE `" . DB_PREFIX . "option` SET type = '" . $this->db->escape($data['type']) . "', sort_order = '" . (int) $data['sort_order'] . "', cod = '" . (int) $data['cod'] . "' WHERE option_id = '" . (int) $option_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int) $option_id . "'");

        foreach ($data['option_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . (int) $option_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', front_name = '" . $this->db->escape($value['front_name']) . "'");
        }
        $this->db->query("DELETE ots FROM option_to_size ots,option_value ov,`option` o WHERE o.option_id = ov.option_id AND ots.option_value_id = ov.option_value_id AND o.option_id = '" . (int) $option_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_value WHERE option_id = '" . (int) $option_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_value_description WHERE option_id = '" . (int) $option_id . "'");

        if (isset($data['option_value'])  && $data['type'] !='size') {
            foreach ($data['option_value'] as $option_value) {
                if ($option_value['option_value_id']) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_value_id = '" . (int) $option_value['option_value_id'] . "', option_id = '" . (int) $option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $option_value['sort_order'] . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int) $option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int) $option_value['sort_order'] . "'");
                }

                $option_value_id = $this->db->getLastId();

                foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int) $option_value_id . "', language_id = '" . (int) $language_id . "', option_id = '" . (int) $option_id . "', name = '" . $this->db->escape($option_value_description['name']) . "'");
                }
            }
        }else if($data['type']=='size' && isset($data['size_op'])){
            $is_us = (isset($data['is_us']) && $data['is_us'] == true);
            $is_eur = (isset($data['is_eur']) && $data['is_eur'] == true);
            $is_xmls = (isset($data['is_xmls']) && $data['is_xmls'] == true);
            $is_it = (isset($data['is_it']) && $data['is_it'] == true);
            $this->setEnabledSize($option_id,'is_uk',1);
            $this->setEnabledSize($option_id,'is_us',$is_us ? 1 : 0);
            $this->setEnabledSize($option_id,'is_eur',$is_eur ? 1 : 0);
            $this->setEnabledSize($option_id,'is_xmls',$is_xmls ? 1 : 0);
            $this->setEnabledSize($option_id,'is_it',$is_it ? 1 : 0);

            foreach ($data['size_op'] as $id=>$size){
                //echo '<pre>';


                $sort_order = (int) (isset($size['sort_order']) && $size != '') ? $size['sort_order'] : 0;
                $uk = (isset($size['uk']) && $size['uk']!='') ? $size['uk'] : 0;
                $us = (isset($size['us']) && $size['us']!='') ? $size['us'] : 0;
                $eur = (isset($size['eur']) && $size['eur']!='') ? $size['eur'] : 0;
                $xmls = (isset($size['xmls']) && $size['xmls']!='') ? $size['xmls'] : 0;
                $it = (isset($size['it']) && $size['it']!='') ? $size['it'] : 0;

                if(isset($size['is_new']) && $size['is_new'] == 1){
                $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int) $option_id . "', sort_order = '" . $sort_order . "'");
                $option_value_id = $this->db->getLastId();
                }else{
                    $option_value_id = (int)$id;
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_value_id='$option_value_id',option_id = '" . (int) $option_id . "', sort_order = '" . $sort_order . "'");
                }
                foreach ([1,2] as $language_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET 
                        option_value_id = '" . (int) $option_value_id . "', 
                        language_id = '" . (int) $language_id . "',
                        option_id = '" . (int) $option_id . "', 
                        name = '" . $uk . "'");
                }


                $q  = "INSERT INTO  `option_to_size` set `option_value_id`='$option_value_id' , `uk`='$uk' , `us`='$us',`eur`='$eur',`xmls`='$xmls',`it`='$it' ";

                $this->db->query($q);

            }


        }


        $this->db->query("DELETE FROM " . DB_PREFIX . "option_to_category WHERE option_id = '" . (int) $option_id . "'");
        if (isset($data['option_category'])) {
            foreach ($data['option_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "option_to_category SET option_id = '" . (int) $option_id . "', category_id = '" . (int) $category_id . "'");
            }
        }

        $this->event->trigger('post.admin.option.edit', $option_id);
    }

    public function deleteOption($option_id) {
        $this->event->trigger('pre.admin.option.delete', $option_id);

        $this->db->query("DELETE FROM `" . DB_PREFIX . "option` WHERE option_id = '" . (int) $option_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int) $option_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_value WHERE option_id = '" . (int) $option_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "option_value_description WHERE option_id = '" . (int) $option_id . "'");

        $this->event->trigger('post.admin.option.delete', $option_id);
    }

    public function getOption($option_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE o.option_id = '" . (int) $option_id . "' AND od.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getOptions($data = array()) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE od.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND od.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sort_data = array(
            'od.name',
            'o.type',
            'o.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY od.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getOptionDescriptions($option_id) {
        $option_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int) $option_id . "'");

        foreach ($query->rows as $result) {
            $option_data[$result['language_id']] = array('name' => $result['name'],'front_name' => $result['front_name']);
        }

        return $option_data;
    }

    public function getOptionValue($option_value_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) LEFT JOIN option_to_size o2s ON (ov.option_value_id = o2s.option_value_id) WHERE ov.option_value_id = '" . (int) $option_value_id . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getOptionValues($option_id) {
        $option_value_data = array();

        $option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov INNER JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ov.option_id = '" . (int) $option_id . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY ov.sort_order, ovd.name");

        foreach ($option_value_query->rows as $option_value) {
            $option_value_data[] = array(
                'option_value_id' => $option_value['option_value_id'],
                'name' => $option_value['name'],
                'image' => $option_value['image'],
                'sort_order' => $option_value['sort_order']
            );
        }

        return $option_value_data;
    }

    public function getOptionValueDescriptions($option_id) {
        $option_value_data = array();

        $option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value WHERE option_id = '" . (int) $option_id . "' ORDER BY sort_order ");

        foreach ($option_value_query->rows as $option_value) {
            $option_value_description_data = array();

            $option_value_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value_description WHERE option_value_id = '" . (int) $option_value['option_value_id'] . "'  ");

            foreach ($option_value_description_query->rows as $option_value_description) {
                $option_value_description_data[$option_value_description['language_id']] = array('name' => $option_value_description['name']);
            }

            $option_value_data[] = array(
                'option_value_id' => $option_value['option_value_id'],
                'option_value_description' => $option_value_description_data,
                'image' => $option_value['image'],
                'sort_order' => $option_value['sort_order']
            );
        }

        return $option_value_data;
    }

    public function getTotalOptions() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "option`");

        return $query->row['total'];
    }

    public function getOptionCategories($option_id){
      $option_category_data = array();

      $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_to_category WHERE option_id = '" . (int) $option_id . "'");

      foreach ($query->rows as $result) {
          $option_category_data[] = $result['category_id'];
      }

      return $option_category_data;
    }
    public function getSizes($option_id){
        $option_id = (int)$option_id;
        $query = $this->db->query("SELECT o.option_id, o.type, ov.option_value_id,ov.sort_order ,ots.uk, ots.us, ots.eur, ots.xmls, ots.it FROM `option` o LEFT JOIN option_value ov ON (o.option_id = ov.option_id) LEFT JOIN option_to_size ots ON ( ots.option_value_id = ov.option_value_id ) WHERE o.type = 'size' && o.option_id = ".$option_id);
        if(isset($query->rows) && count($query->rows)>0){
            $outout = [];
            foreach ($query->rows as $row){
                $outout[$row['option_value_id']] = [
                    'sort_order'=>$row['sort_order'],
                    'uk'=>$row['uk'],
                    'us'=>$row['us'],
                    'eur'=>$row['eur'],
                    'xmls'=>$row['xmls'],
                    'it'=>$row['it'],
                ];
            }
            return $outout;
        }
        return array();
    }


    public function setEnabledSize($option_id,$key,$value){
        $query = $this->db->query("select * from dynamic_values where code='option_option_size_".(int)$option_id."' AND `key` = '".$this->db->escape($key)."' ");
        if(isset($query) && !empty($query->rows)){
            $this->db->query("UPDATE dynamic_values set `value`='".$this->db->escape($value)."' where code='option_option_size_".(int)$option_id."' AND `key` = '".$this->db->escape($key)."'  ");
        }else{
         $this->db->query("INSERT INTO dynamic_values set
          `code`='option_option_size_".(int)$option_id."',
          `key` = '".$this->db->escape($key)."',
          `value`='".$this->db->escape($value)."' ");
        }


    }
    public function getEnabledSize($option_id){
        $sizes = [
            'is_uk'=>1,
            'is_us'=>0,
            'is_eur'=>0,
            'is_xmls'=>0,
            'is_it'=>0,
        ];

        $query = $this->db->query("select * from dynamic_values where code='option_option_size_".(int)$option_id."' AND `key` IN ('is_uk','is_us','is_eur','is_xmls','is_it');");
        if(isset($query) && isset($query->rows)){
            foreach ($query->rows as $row){
                if(isset($sizes[$row['key']])){
                    $sizes[$row['key']] =  $row['value'];
                }
            }
        }
        return $sizes;
    }

    public function getSizesForOptionId($id){
        $id = (int)$id;
        $query = $this->db->query("SELECT * FROM option_to_size where option_value_id = '$id'");
        return $query->row;
    }
}
