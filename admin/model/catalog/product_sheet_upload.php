<?php

class ModelCatalogProductSheetUpload extends Model {

  public function getSheets(){
    $query = $this->db->query("SELECT sheet_id, companyname,username,file_name , pus.date_added FROM product_upload_sheet pus LEFT JOIN customerpartner_to_customer_description cp2cd ON (pus.seller_id = cp2cd.customer_id AND cp2cd.language_id=1) LEFT JOIN user u ON (pus.user_id = u.user_id)");
    if($query->num_rows)
      return $query->rows;
    return array();
  }

  public function getSheet($sheet_id){
    $query = $this->db->query("SELECT seller_id, companyname as seller_name , currency_id FROM product_upload_sheet pus LEFT JOIN customerpartner_to_customer_description cp2cd ON (pus.seller_id = cp2cd.customer_id AND cp2cd.language_id=1) WHERE sheet_id=".(int)$sheet_id);
    if($query->num_rows)
      return $query->row;
    return array();
  }

  public function add($post,$files){

    $file_name = $this->upload_xls_file('product_sheets', 'input_xls_file', $post['seller_id']);

    $this->db->query("INSERT INTO product_upload_sheet set seller_id=".(int) $post['seller_id'] .", currency_id=".(int)$post['products_currency']." ,file_name='".$this->db->escape($file_name)."' , sheet_status='ADDED' , date_added=NOW() , user_id=".$this->user->getId());

  }

  public function enable($sheet_id){
    $this->db->query("UPDATE product set status=1 , is_algolia=1 where product_id in (select product_id FROM sheet_to_product WHERE sheet_id=".(int)$sheet_id.")");
    $this->db->query("INSERT INTO sheet_enabling_track set sheet_id=".$sheet_id. " , status='enabled' , user_id=".$this->user->getId() . " , date_added= NOW()");
    $this->db->query("INSERT IGNORE INTO algolia  (type,type_id) (select 'product',product_id FROM sheet_to_product WHERE sheet_id=".(int)$sheet_id.")");
  }

  public function disable($sheet_id){
    $this->db->query("UPDATE product set status=0, is_algolia=0 where product_id in (select product_id FROM sheet_to_product WHERE sheet_id=".(int)$sheet_id.")");
    $this->db->query("INSERT INTO sheet_enabling_track set sheet_id=".$sheet_id. " , status='disabled' , user_id=".$this->user->getId() . " , date_added= NOW()");
    $this->db->query("INSERT IGNORE INTO algolia  (type,type_id) (select 'product',product_id FROM sheet_to_product WHERE sheet_id=".(int)$sheet_id.")");
  }

  public function archiveOrder($product_id,$status){
        if( SAVE_PRODUCT_DATA == 'YES'){
          try{
            $m = new MongoClient(MONGO_DEFUALT_HOST);
            $db = $m->selectDB('product_old_data');
            $collection = new MongoCollection($db, 'product_old_data');
            $serialized_data = array('code' => 'product_'.$product_id."__".date("Y-m-d_h:i:sa"),'status'=> $status,'date_of_modification' => date("Y-m-d h:i:sa"),'user_id'=> $this->user->getId(),'user_ip' => $this->user->getIp(),'user_type'=>'xls_product_edit','product_id'=>$product_id);

            $product_tables = ['product', 'product_option','product_option_value'];
            foreach($product_tables as $key => $value){
                $serialized_data['table_name'] = $value;
                $product_data = $this->db->query("SELECT * FROM `" . DB_PREFIX . $value."`  WHERE product_id = '" . (int) $product_id . "'");
                $serialized_data['table_data'] =  '';
                if(!empty($product_data->rows))
                    $serialized_data['table_data'] = $product_data->rows;
                $collection->insert($serialized_data);
                unset($serialized_data['_id']);

            }
        }catch(Exception $e){
          return false;
        }
      }
      return false;
  }

}
