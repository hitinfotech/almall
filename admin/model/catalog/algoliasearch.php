<?php

class ModelCatalogAlgoliasearch extends Model {

    public function editProduct($product_id, $sort_order) {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET  sort_order = '" . (int) $sort_order . "'WHERE product_id = '" . (int) $product_id . "'");
        $this->db->query(" INSERT IGNORE INTO algolia SET `type`='product', type_id='" . (int) $product_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function getProducts($data = array()) {
        $sql = "SELECT p.product_id, pd.name,bds.name AS bname, p.model, p.sku, p.image, p.quantity, p.status, p.sort_order AS sort_order, b.sort_order AS bsort_order, p.date_added, p.date_modified, u.username user_add, mu.username user_modify FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "user u ON (p.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (p.last_mod_id = mu.user_id) LEFT JOIN " . DB_PREFIX . "otp_data otp ON (p.product_id = otp.product_id) LEFT JOIN brand b ON (p.brand_id = b.brand_id)  LEFT JOIN brand_description bds ON (p.brand_id = bds.brand_id) LEFT JOIN customerpartner_to_product cp2p ON(p.product_id=cp2p.product_id) LEFT JOIN customerpartner_to_customer ON(cp2p.customer_id=cp2c.customer_id) WHERE p.status =1 AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND bds.language_id = '" . (int) $this->config->get('config_language_id') . "'";
        $sql.=" AND p.stock_status_id != 10 ";
        
        if (!empty($data['filter_product_id'])) {
            $sql .= " AND p.product_id = '" . (int) $data['filter_product_id'] . "' ";
        }
        
        if (isset($data['filter_country'])) {
            $sql .= " AND cp2c.available_country_id = '" . (int) $data['filter_country'] . "' ";
        }
        
        $sql .= " GROUP BY p.product_id ORDER BY b.sort_order desc, p.sort_order DESC ";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalProducts($data = array()) {
        $sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)  LEFT JOIN customerpartner_to_product cp2p ON(p.product_id=cp2p.product_id) LEFT JOIN customerpartner_to_customer ON(cp2p.customer_id=cp2c.customer_id) LEFT JOIN " . DB_PREFIX . "otp_data otp ON (p.product_id = otp.product_id) ";

        $sql .= " WHERE p.status= 1 AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";
        $sql.=" AND p.stock_status_id != 10 ";

        if (!empty($data['filter_product_id'])) {
            $sql .= " AND p.product_id = '" . (int) $data['filter_product_id'] . "' ";
        }

        if (isset($data['filter_country'])) {
            $sql .= " AND cp2c.available_country_id = '" . (int) $data['filter_country'] . "' ";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

}
