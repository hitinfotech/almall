<?php

class ModelSettingStore extends Model {

    public function addStore($data) {

        $this->event->trigger('pre.admin.store.add', $data);

        $this->db->query("INSERT INTO " . DB_PREFIX . "store SET name = '" . $this->db->escape($data['config_name']) . "', `url` = '" . $this->db->escape($data['config_url']) . "', `ssl` = '" . $this->db->escape($data['config_ssl']) . "'");

        $store_id = $this->db->getLastId();

        // Layout Route
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout_route WHERE store_id = '0'");

        foreach ($query->rows as $layout_route) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "layout_route SET layout_id = '" . (int) $layout_route['layout_id'] . "', route = '" . $this->db->escape($layout_route['route']) . "', store_id = '" . (int) $store_id . "'");
        }

        $this->cache->delete('store');

        $this->event->trigger('post.admin.store.add', $store_id);

        return $store_id;
    }

    public function editStore($store_id, $data) {

        $this->event->trigger('pre.admin.store.edit', $data);

        $this->db->query("UPDATE " . DB_PREFIX . "store SET name = '" . $this->db->escape($data['config_name']) . "', `url` = '" . $this->db->escape($data['config_url']) . "', `ssl` = '" . $this->db->escape($data['config_ssl']) . "' WHERE store_id = '" . (int) $store_id . "'");

        $this->cache->delete('store');

        $this->event->trigger('post.admin.store.edit', $store_id);
    }

    public function deleteStore($store_id) {
        $this->event->trigger('pre.admin.store.delete', $store_id);

        $this->db->query("DELETE FROM " . DB_PREFIX . "store WHERE store_id = '" . (int) $store_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "layout_route WHERE store_id = '" . (int) $store_id . "'");

        $this->cache->delete('store');

        $this->event->trigger('post.admin.store.delete', $store_id);
    }

    public function getStore($store_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "store WHERE store_id = '" . (int) $store_id . "'");

        return $query->row;
    }

    public function getStores() {
        //$query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "store ORDER BY url ");

        $store_data = array(); //$query->rows;

        return $store_data;
    }

    public function getMainStores() {
        $query = $this->db->query(" SELECT ms.main_store_id,msd.name FROM " . DB_PREFIX . "main_store ms LEFT JOIN main_store_description msd ON(ms.main_store_id=msd.main_store_id) WHERE msd.language_id='" . (int) $this->config->get('config_language_id') . "' ORDER BY msd.name ");

        $store_data = $query->rows;

        return $store_data;
    }

    public function getMainStoreBranches($main_store_id = 0) {
        $query = $this->db->query(" SELECT s.store_id FROM " . DB_PREFIX . "store s WHERE s.main_store_id='" . (int) $main_store_id . "' ");

        $store_data = array();

        foreach ($query->rows as $row) {
            $store_data[$row['store_id']] = $row['store_id'];
        }

        return $store_data;
    }

    public function getTotalStores() {
        //$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "store");

        return 0; //$query->row['total'];
    }

    public function getTotalStoresByLayoutId($layout_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_layout_id' AND `value` = '" . (int) $layout_id . "' AND store_id != '0'");

        return $query->row['total'];
    }

    public function getTotalStoresByLanguage($language) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_language' AND `value` = '" . $this->db->escape($language) . "' AND store_id != '0'");

        return $query->row['total'];
    }

    public function getTotalStoresByCurrency($currency) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_currency' AND `value` = '" . $this->db->escape($currency) . "' AND store_id != '0'");

        return $query->row['total'];
    }

    public function getTotalStoresByCountryId($country_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_country_id' AND `value` = '" . (int) $country_id . "' AND store_id != '0'");

        return $query->row['total'];
    }

    public function getTotalStoresByZoneId($zone_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_zone_id' AND `value` = '" . (int) $zone_id . "' AND store_id != '0'");

        return $query->row['total'];
    }

    public function getTotalStoresByCustomerGroupId($customer_group_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_customer_group_id' AND `value` = '" . (int) $customer_group_id . "' AND store_id != '0'");

        return $query->row['total'];
    }

    public function getTotalStoresByInformationId($information_id) {
        $account_query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_account_id' AND `value` = '" . (int) $information_id . "' AND store_id != '0'");

        $checkout_query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_checkout_id' AND `value` = '" . (int) $information_id . "' AND store_id != '0'");

        return ($account_query->row['total'] + $checkout_query->row['total']);
    }

    public function getTotalStoresByOrderStatusId($order_status_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_order_status_id' AND `value` = '" . (int) $order_status_id . "' AND store_id != '0'");

        return $query->row['total'];
    }
    
    public function getTotalStoresByRmaStatusId($rma_status_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "setting WHERE `key` = 'config_rma_status_id' AND `value` = '" . (int) $rma_status_id . "' AND store_id != '0'");

        return $query->row['total'];
    }

    public function getStoresByName($data) {
        if ($data['filter_name']) {
            $query = $this->db->query(" SELECT s.store_id , sd.name, md.name
                                        FROM " . DB_PREFIX . "store s
                                        LEFT JOIN " . DB_PREFIX . "store_description sd ON (s.store_id = sd.store_id)
                                        LEFT JOIN " . DB_PREFIX . "mall_description md ON (s.mall_id = md.mall_id)
                                        WHERE
                                            sd.`name` LIKE '%" . $this->db->escape($data['filter_name']) . "%'
                                        AND sd.language_id = '" . (int) $this->config->get('config_language_id') . "'
                                        AND md.language_id = '" . (int) $this->config->get('config_language_id') . "'
                                        ");
            return $query->rows;
        } else {
            return array();
        }
    }

    public function getStoresByMainStore($main_store_id) {
        $query = $this->db->query("SELECT s.store_id, GROUP_CONCAT(name, ' - ') AS name FROM " . DB_PREFIX . "store s LEFT JOIN " . DB_PREFIX . "store_description sd ON(s.store_id=sd.store_id) WHERE s.main_store_id = '" . (int) $main_store_id . "' GROUP BY s.store_id ");

        return $query->rows;
    }

}
