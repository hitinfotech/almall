<?php

################################################################################################
#  wk_rma for Opencart 1.5.1.x From webkul http://webkul.com  	  	       #
################################################################################################

class ModelModulewkrma extends Model {

    public function createTable() {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "wk_rma_order` (
                        `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,                 
                        `order_id` varchar(100) NOT NULL ,
                        `images` varchar(8000) NOT NULL ,
                      	`add_info` varchar(4000) NOT NULL ,
                        `customer_status_id` INT ,
                        `rma_auth_no` varchar(100) NOT NULL ,
                        `customer_id` int(100) NOT NULL,                       
                        `date` varchar(100) NOT NULL ,
                        `shipping_label` varchar(100) NOT NULL ,
                        PRIMARY KEY (`id`) ) DEFAULT CHARSET=utf8 ;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "wk_rma_product` (
                        `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,			                        
                        `rma_id` int(100) NOT NULL ,
                        `reason` varchar(4000) NOT NULL ,
                        `product_id` int(100) NOT NULL ,
                        `customer_id` int(100) NOT NULL ,
                        `quantity` int(100) NOT NULL ,   
                        `order_product_id` int(100) NOT NULL ,                          
                        PRIMARY KEY (`id`) ) DEFAULT CHARSET=utf8 ;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "wk_rma_customer` (
                        `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,                                          
                        `rma_id` int(100) NOT NULL ,
                        `customer_id` int(100) NOT NULL ,
                        `email` varchar(1000) NOT NULL ,                            
                        PRIMARY KEY (`id`) ) DEFAULT CHARSET=utf8 ;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "wk_rma_order_message` (
                        `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                        `rma_id` varchar(50) NOT NULL ,
                        `writer` varchar(500) NOT NULL ,
                        `msg_to` int(100) NOT NULL ,
                        `message` varchar(5000) NOT NULL ,
                        `date` varchar(500) NOT NULL ,
                        `attachment` varchar(500) NOT NULL ,  
                        PRIMARY KEY (`id`) ) DEFAULT CHARSET=utf8 ;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "wk_rma_reason` (
			       `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                         `reason_id` int(10) NOT NULL,
                         `customer_id` int(10),
                         `reason` varchar(18000) NOT NULL,  
                         `language_id` int(10) NOT NULL,
                         `status` int(10) NOT NULL ,
                         `status_info` varchar(100) NOT NULL,                                  
                         PRIMARY KEY (`id`) ) DEFAULT CHARSET=utf8 ;");
    }

}