<?php 
class ModelModuleOrderFollowUp extends Model {
  	public function install() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "orderfollowup_log` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`date` DATETIME NULL DEFAULT NULL,  
			`store_id` INT(11) NOT NULL DEFAULT 0,  
			`order_id` INT(11) NOT NULL,
			`followup_id` INT(11) NOT NULL,
			`message` TEXT NOT NULL,
			 PRIMARY KEY (`id`))");
			 
		//$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=1 WHERE `name` LIKE'%OrderFollowUp by iSenseLabs%'");
		//$modifications = $this->load->controller('extension/modification/refresh');
  	} 
    
    public function init() {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "orderfollowup_unsubscribe_list` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`email` VARCHAR(255) NULL DEFAULT NULL,
            `date_created` DATETIME NULL DEFAULT NULL, 
			 PRIMARY KEY (`id`))");   
    }
  
  	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "orderfollowup_log`");
		
		//$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=0 WHERE `name` LIKE'%OrderFollowUp by iSenseLabs%'");
		//$modifications = $this->load->controller('extension/modification/refresh');
  	}
	
	public function viewLogs($page=1, $limit=8, $store=0,$sort="id", $order="DESC") {	
		if ($page) {
			$start = ($page - 1) * $limit;
		}
		$query =  $this->db->query("SELECT * FROM `" . DB_PREFIX . "orderfollowup_log`
			WHERE `store_id`='".$store."'
			ORDER BY `id` DESC
			LIMIT ".$start.", ".$limit);
			
		return $query->rows; 
	}
	
	public function getTotalLog($store=0){
		$query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "orderfollowup_log` WHERE `store_id`=".$store);
		
		return $query->row['count']; 
	}
	public function deleteLogEntry($data, $store_id)
    {	
    	$sql="DELETE FROM `".DB_PREFIX."orderfollowup_log` WHERE id in (".implode(",",$data).") AND store_id = '" . (int)$store_id . "'";  
    	$this->db->query($sql);  	
    }	
}
?>