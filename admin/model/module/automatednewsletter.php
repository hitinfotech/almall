<?php
class ModelModuleAutomatedNewsletter extends Model {

  public function getSelectedProducts($products = array(), $store_id = 0) {
    $implode  = array();
    $whereClause = '';
    if(isset($products)) {
	        foreach ($products as $product_id) {
            $implode[] = "ps.product_id = '" . $product_id . "'";
        }
      $whereClause = "AND (" . implode(" OR ", $implode) . ")";
    }

    $products = $this->db->query("SELECT name, ps.product_id AS product_id
                                  FROM " . DB_PREFIX . "product_description AS pd
                                  JOIN " . DB_PREFIX . "product_to_store AS ps ON pd.product_id=ps.product_id
                                  WHERE language_id='" . $this->config->get('config_language_id') . "'
                                  AND ps.store_id='".(int)$store_id ."'
                                  ". $whereClause )->rows;
    return $products;
  }

 // ENS
	public function viewENSsubscribers($store_id) {
		$res = $this->db->query("SHOW TABLES LIKE '".DB_PREFIX."easynewslettersubscription'");
			if ($res->num_rows) {
				$query =  $this->db->query("SELECT customer_email as email, customer_name as firstname, subscribe_id as ens_user_id, language_id FROM `" . DB_PREFIX . "easynewslettersubscription`
					WHERE store_id='" . $store_id . "'
					ORDER BY `date_created` DESC ");
				return $query->rows;
			} else {
				return array();
			}
	}

  public function getCustomers($customers=array(), $store_id = 0) {

    $implode     = array();
    $whereClause = '';
    if (!empty($customers)) {
      foreach ($customers as $customer_id) {
        $implode[] = "c.customer_id = '" . $customer_id . "'";
      }
      $whereClause = "  AND (" . implode(" OR ", $implode) . ")";
    }
    $customers = $this->db->query("SELECT c.customer_id, c.email, c.firstname, c.lastname, cl.language_id, c.customer_group_id
                                    FROM " . DB_PREFIX . "customer as c
                                    LEFT JOIN " . DB_PREFIX . "customer_last_used_language as cl ON c.customer_id=cl.customer_id
                                    WHERE c.store_id='" . $store_id . "' ". $whereClause);
	return $customers->rows;
  }

  public function getAffiliates($affiliates = NULL)  {

    $implode     = array();
    $whereClause = '';
    if (isset($affiliates)) {
        foreach ($affiliates as $affiliate_id) {
            $implode[] = "a.affiliate_id = '" . $affiliate_id . "'";
        }
        $whereClause = "WHERE (" . implode(" OR ", $implode) . ")";
    }
    $query = $this->db->query("SELECT email, firstname, lastname, affiliate_id FROM " . DB_PREFIX . "affiliate AS a " . $whereClause);
    return $query->rows;
  }

  public function getCustomersByProductsOrdered($products) {

    $implode = array();
    foreach ($products as $product_id) {
        $implode[] = "op.product_id = '" . $product_id . "'";
    }
    $query = $this->db->query("SELECT DISTINCT email, firstname, lastname FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0'  AND o.deleted='0' ");
    return $query->rows;
  }

  public function getSetting($group, $store_id = 0) {

    $data = array();

    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->db->escape($group) . "'");

    foreach ($query->rows as $result) {
      if (!$result['serialized']) {
        $data[$result['key']] = $result['value'];
      } else {
        $data[$result['key']] = unserialize($result['value']);
      }
    }

    return $data;
  }

  public function editSetting($group, $data, $store_id = 0) {

    $this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->db->escape($group) . "'");

    foreach ($data as $key => $value) {
      if (!is_array($value)) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `group` = '" . $this->db->escape($group) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
      } else {
        $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `group` = '" . $this->db->escape($group) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(serialize($value)) . "', serialized = '1'");
      }
    }
  }

  public function getAutocompleteCustomers($data = array()) {

    $sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cgd.name AS customer_group
            FROM " . DB_PREFIX . "customer c
            LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (c.customer_group_id = cgd.customer_group_id)
            WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
    $implode = array();
    if (isset($data['store_id'])) {
      $implode[] = "c.store_id='" . (int)$data['store_id'] . "'";
    }
    if (!empty($data['filter_name'])) {
      $implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
    }

    if ($implode) {
      $sql .= " AND " . implode(" AND ", $implode);
    }
    $sql .= " ORDER BY `name` ASC LIMIT 0, 20";
    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getAutocompleteAffiliates($data = array()) {

    $sql = "SELECT *, CONCAT(a.firstname, ' ', a.lastname) AS name, (SELECT SUM(at.amount) FROM " . DB_PREFIX . "affiliate_transaction at WHERE at.affiliate_id = a.affiliate_id GROUP BY at.affiliate_id) AS balance FROM " . DB_PREFIX . "affiliate a WHERE CONCAT(a.firstname, ' ', a.lastname) LIKE '" . $this->db->escape($data['filter_name']) . "%' ORDER BY `name` ASC LIMIT 0, 20";
    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getProducts($data = array(), $store_id) {

    $sql = "SELECT name, ps.product_id AS product_id  FROM " . DB_PREFIX . "product_description AS pd
            JOIN " . DB_PREFIX . "product_to_store AS ps ON pd.product_id=ps.product_id
            WHERE language_id='" . $this->config->get('config_language_id') . "' AND ps.store_id='".(int)$store_id ."'";
    if (!empty($data['filter_name'])) {
      $sql .= " AND pd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
    }
    $sql .= " GROUP BY pd.product_id ORDER BY pd.name ASC LIMIT 0, 20";
    $query = $this->db->query($sql);
    return $query->rows;
  }

  public function getSentNewsletters($data, $store_id) {
    return $this->db->query("SELECT log.*, l.*,
	(SELECT COUNT(*) FROM " . DB_PREFIX . "automatednewsletter_sent_log WHERE newsletter_id=log.newsletter_id AND success=1) AS success_sent,
	(SELECT COUNT(*) FROM " . DB_PREFIX . "automatednewsletter_sent_log WHERE newsletter_id=log.newsletter_id) AS customers_count
	FROM " . DB_PREFIX . "automatednewsletter_log AS log
	LEFT JOIN " . DB_PREFIX . "automatednewsletter_sent_log AS asl ON log.newsletter_id=asl.newsletter_id
	LEFT JOIN " . DB_PREFIX . "language AS l ON l.language_id=log.language_id
	WHERE log.store_id='".(int)$store_id."'
	GROUP BY log.newsletter_id
	ORDER BY ". $data['sort'] . " ". $data['order'] .  "
	LIMIT ". $data['start'] . ", " .$data['limit'])->rows;
  }

  public function getSentNewslettersCustomers($store_id) {

    $customers = $this->db->query("SELECT * FROM " . DB_PREFIX . "automatednewsletter_sent_log AS asl LEFT JOIN " . DB_PREFIX . "customer AS c ON c.customer_id=asl.user_id WHERE c.store_id='".(int)$store_id."' AND asl.user_type='c'")->rows;

    if( $this->db->query("SHOW TABLES LIKE '". DB_PREFIX . "easynewslettersubscription'")->num_rows >0 ) {
        $ens = $this->db->query("SELECT *,ens.customer_email as email, ens.customer_name as firstname FROM " . DB_PREFIX . "automatednewsletter_sent_log AS asl LEFT JOIN " . DB_PREFIX . "easynewslettersubscription AS ens ON ens.subscribe_id=asl.user_id WHERE ens.store_id='".(int)$store_id."' AND asl.user_type='n' ")->rows;
    } else {
        $ens = array();
    }

	$affiliates = $this->db->query("SELECT * FROM " . DB_PREFIX . "automatednewsletter_sent_log AS asl LEFT JOIN " . DB_PREFIX . "affiliate AS a ON a.affiliate_id=asl.user_id WHERE asl.user_type='a' ")->rows;

	if(!empty($ens)) {
		$all_customers = array_merge($customers, $ens);
	} else {
		$all_customers = $customers;
	}

	if(!empty($affiliates)) {
		return array_merge($all_customers, $affiliates);
	} else {
		return $all_customers;
	}

  }

  public function getTotalSentNewsletters($store_id) {
    return $this->db->query("SELECT * FROM " . DB_PREFIX . "automatednewsletter_log WHERE store_id='".(int)$store_id."'")->num_rows;
  }

  public function getSentNewsletter($newsletter_id){
    return  $this->db->query("SELECT log.* FROM " . DB_PREFIX . "automatednewsletter_log AS log WHERE log.newsletter_id=".(int)$newsletter_id)->row;
  }

  public function deleteNewsletter($newsletters) {

    if(is_array($newsletters)) {

      foreach ($newsletters as $newsletter) {
        $implode[] = "newsletter_id=".(int)$newsletter;
      }
      $implode = implode(" OR ", $implode);
      if(isset($implode)) {
        $whereClause= "WHERE ". $implode;
      }
      $this->db->query("DELETE FROM " . DB_PREFIX . "automatednewsletter_log ". $whereClause);
	  $this->db->query("DELETE FROM " . DB_PREFIX . "automatednewsletter_sent_log ". $whereClause);

    }
  }

  public function deleteTemplate($templates){
    if(is_array($templates)) {
      foreach ($templates as $template) {
        $implode[] = "template_id=".(int)$template;
      }
      $implode = implode(" OR ", $implode);
      if(isset($implode)) {
        $whereClause= "WHERE ". $implode;
      }
      $this->db->query("DELETE FROM " . DB_PREFIX . "automatednewsletter_template ". $whereClause);
    }
  }

  public function getStockStatuses() {
    return $this->db->query("SELECT * FROM " . DB_PREFIX . "stock_status WHERE language_id=" . $this->config->get('config_language_id'))->rows;
  }

  public function getTemplates($data, $store_id) {
    return  $this->db->query("SELECT * FROM " . DB_PREFIX . "automatednewsletter_template
                              WHERE store_id=" . (int)$store_id . "
                              AND language_id='".$this->config->get('config_language_id') . "'
                              ORDER BY ". $data['sort'] . " ". $data['order'] .  " LIMIT ". $data['start']. ", " .$data['limit'] )->rows;
  }

  public function addTemplate($templates = array(), $store_id) {

    $current_id = (int)$this->getMaxTemplateId() + 1;
    foreach ($templates as $key => $template) {
      $this->db->query("INSERT INTO " . DB_PREFIX . "automatednewsletter_template
                        SET template_id=" . $current_id . ",
                        language_id=" . (int)$this->db->escape($key) . ",
                        subject='" . $this->db->escape($template['subject']) . "',
                        content='" . $this->db->escape($template['content']) . "',
                        time_added=" . time() . ",
                        store_id=" . (int)$store_id);
    }
  }

  public function getMaxTemplateId() {
    $query =  $this->db->query("SELECT MAX(template_id) as max_id FROM " .  DB_PREFIX ."automatednewsletter_template")->row;
    return  $query['max_id'];
  }

  public function updateTemplate($template_id, $templates, $store_id) {
        foreach ($templates as $key => $template) {
          $this->db->query("UPDATE " . DB_PREFIX . "automatednewsletter_template
                        SET subject='" . $this->db->escape($template['subject']) . "',
                        content='" . $this->db->escape($template['content']) . "',
                        time_added=" . time() . ",
                        store_id=" . (int)$store_id . "
                        WHERE template_id=" . (int)$template_id . " AND language_id=" . (int)$this->db->escape($key));
        }
  }

  public function getTemplate($template_id) {
    $query = $this->db->query("SELECT * FROM " . DB_PREFIX ."automatednewsletter_template
                             WHERE template_id=". (int)$template_id)->rows;
    $templates = array();
    foreach ($query as $key => $tmp) {
      $templates[$tmp['language_id']] = $tmp;
    }
    return $templates;
  }

  public function existTemplate($template_id){
      return $this->db->query("SELECT * FROM " . DB_PREFIX . "automatednewsletter_template WHERE template_id=". (int)$template_id)->num_rows > 0 ? TRUE : FALSE;
  }

  public function getTotalTemplates() {
    return $this->db->query("SELECT * FROM " . DB_PREFIX . "automatednewsletter_template GROUP BY template_id")->num_rows;
  }

  public function install() {
    $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX ."customer_last_used_language (
                      customer_language_id INT(11) NOT NULL AUTO_INCREMENT,
                      customer_id INT(11) NOT NULL UNIQUE,
                      language_id INT(11),
                      PRIMARY KEY (`customer_language_id`))");

    $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX ."automatednewsletter_log (
                      newsletter_id INT(11) NOT NULL AUTO_INCREMENT,
                      subject VARCHAR(128),
                      content TEXT NOT NULL,
                      time_added INT(11) NOT NULL,
                      language_id INT(11),
                      customers_count INT(11),
                      options VARCHAR(200),
                      store_id INT(11) NOT NULL,
                      template_id INT(11) NOT NULL,
                      PRIMARY KEY (`newsletter_id`)) CHARACTER SET utf8 COLLATE utf8_unicode_ci");

    $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX ."automatednewsletter_sent_log (
                      sent_log_id INT(11) NOT NULL AUTO_INCREMENT,
                      PRIMARY KEY (`sent_log_id`),
                      user_id INT(11) NOT NULL,
                      user_type VARCHAR(1) NOT NULL,
                      newsletter_id INT(11) NOT NULL,
                      success INT(1) NOT NULL)");

    $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX ."automatednewsletter_template (
                      automatednewsletter_template_id INT(11) NOT NULL AUTO_INCREMENT,
                      template_id INT(11) NOT NULL,
                      language_id INT(11),
                      subject VARCHAR(128),
                      content MEDIUMTEXT NOT NULL,
                      time_added INT(11) NOT NULL,
                      store_id INT(11) NOT NULL,
                      PRIMARY KEY(automatednewsletter_template_id)) CHARACTER SET utf8 COLLATE utf8_unicode_ci");

	$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=1 WHERE `name` LIKE'%AutomatedNewsletter by iSenseLabs%'");
 	$modifications = $this->load->controller('extension/modification/refresh');
  }

  public function uninstall() {
    $this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX ."customer_last_used_language");
    $this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX ."automatednewsletter_log");
    $this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX ."automatednewsletter_template");
    $this->load->model('setting/setting');
    $this->model_setting_setting->deleteSetting('automatednewsletter_settings');

	$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=0 WHERE `name` LIKE'%AutomatedNewsletter by iSenseLabs%'");
 	$modifications = $this->load->controller('extension/modification/refresh');
  }

    public function upgrade() {
        $res = $this->db->query("SHOW TABLES LIKE '".DB_PREFIX."automatednewsletter_log'");
        if ($res->num_rows) {
            $cols = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "automatednewsletter_log LIKE '%content%'");
            if (!empty($cols->row['Type']) && $cols->row['Type'] != 'mediumtext') {
                $this->db->query("ALTER TABLE " . DB_PREFIX ."automatednewsletter_log MODIFY content MEDIUMTEXT");
            }
        }

        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX ."automatednewsletter_template (
                      automatednewsletter_template_id INT(11) NOT NULL AUTO_INCREMENT,
                      template_id INT(11) NOT NULL,
                      language_id INT(11),
                      subject VARCHAR(128),
                      content MEDIUMTEXT NOT NULL,
                      time_added INT(11) NOT NULL,
                      store_id INT(11) NOT NULL,
                      PRIMARY KEY(automatednewsletter_template_id))");

        $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX ."automatednewsletter_sent_log (
                      sent_log_id INT(11) NOT NULL AUTO_INCREMENT,
                      PRIMARY KEY (`sent_log_id`),
                      user_id INT(11) NOT NULL,
                      user_type VARCHAR(1) NOT NULL,
                      newsletter_id INT(11) NOT NULL,
                      success INT(1) NOT NULL)");

        $res = $this->db->query("SHOW TABLES LIKE '".DB_PREFIX."automatednewsletter_template'");
        if ($res->num_rows) {
            $cols = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "automatednewsletter_template LIKE '%language%'");
            if ($cols->num_rows) {
                $language_code_exists = false;
                foreach ($cols->rows as $col) {
                    if ($col['Field'] == 'language_code') {
                        $language_code_exists = true;
                        break;
                    }
                }

                if ($language_code_exists) {//Version <= 1.3
                    $languages = $this->db->query("SELECT * FROM " . DB_PREFIX . "language")->rows;

                    //upgrade templates
                    $templates = $this->db->query("SELECT * FROM " . DB_PREFIX . "automatednewsletter_template");
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "automatednewsletter_template CHANGE COLUMN language_code language_id INT(11)");
                    foreach ($templates->rows as $template) {
                        $lang_id = 0;
                        foreach ($languages as $language) {
                            if ($language['code'] == $template['language_code']) {
                                $lang_id = $language['language_id'];
                            }
                        }

                        if ($lang_id) {
                            $this->db->query("UPDATE " . DB_PREFIX . "automatednewsletter_template SET language_id=".$lang_id." WHERE automatednewsletter_template_id=".$template['automatednewsletter_template_id']);
                        }
                    }

                    //upgrade logs
                    $logs = $this->db->query("SELECT * FROM " . DB_PREFIX . "automatednewsletter_log");
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "automatednewsletter_log ADD COLUMN template_id INT(11) NOT NULL");
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "automatednewsletter_log CHANGE COLUMN language_code language_id INT(11) NOT NULL");
                    foreach ($logs->rows as $log) {
                        $lang_id = 0;
                        foreach ($languages as $language) {
                            if ($language['code'] == $log['language_code']) {
                                $lang_id = $language['language_id'];
                            }
                        }

                        if ($lang_id) {
                            $this->db->query("UPDATE " . DB_PREFIX . "automatednewsletter_log SET language_id=".$lang_id." WHERE newsletter_id=".$log['newsletter_id']);
                        }
                    }

                    //upgrade last used languages
                    $last_languages = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_last_used_language");
                    $this->db->query("ALTER TABLE " . DB_PREFIX . "customer_last_used_language CHANGE COLUMN language_code language_id INT(11)");
                    foreach ($last_languages->rows as $ll) {
                        $lang_id = 0;
                        foreach ($languages as $language) {
                            if ($language['code'] == $ll['language_code']) {
                                $lang_id = $language['language_id'];
                            }
                        }

                        if ($lang_id) {
                            $this->db->query("UPDATE " . DB_PREFIX . "customer_last_used_language SET language_id=".$lang_id." WHERE customer_language_id=".$ll['customer_language_id']);
                        }
                    }

                    $this->db->query("ALTER TABLE ".DB_PREFIX."automatednewsletter_template CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci");
                    $this->db->query("ALTER TABLE ".DB_PREFIX."automatednewsletter_log CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci");
                }
            }
        }
        return true;
    }
}
?>
