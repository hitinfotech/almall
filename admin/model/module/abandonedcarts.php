<?php

class ModelModuleAbandonedcarts extends Model
{

    public function install()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "abandonedcarts` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`date_created` DATETIME NULL DEFAULT NULL,
			`date_modified` DATETIME NULL DEFAULT NULL,
			`cart` TEXT NULL DEFAULT NULL,
			`customer_info` TEXT NULL DEFAULT NULL,
			`last_page` VARCHAR(255) NULL DEFAULT NULL,
			`restore_id` VARCHAR(255) NULL DEFAULT NULL,
			`ip` VARCHAR(100) NULL DEFAULT NULL,
			`notified` SMALLINT NOT NULL DEFAULT 0,
			`ordered` TINYINT NOT NULL DEFAULT 0,
			`store_id` TINYINT NOT NULL DEFAULT 0,
			 PRIMARY KEY (`id`))
        ");

        //$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=1 WHERE `name` LIKE'%AbandonedCarts by iSenseLabs%'");
        //$modifications = $this->load->controller('extension/modification/refresh');
    }

    public function uninstall()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "abandonedcarts`");

        //$this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=0 WHERE `name` LIKE'%AbandonedCarts by iSenseLabs%'");
        //$modifications = $this->load->controller('extension/modification/refresh');
    }

    public function viewAbandonedCarts($page = 1, $limit = 8, $store = 0, $notified = false, $ordered = false, $sort = "id", $order = "DESC")
    {

        if (VERSION >= '2.1.0.1') {
            $this->load->model('customer/customer');
        } else {
            $this->load->model('sale/customer');
        }

        if ($page) {
            $start = ($page - 1) * $limit;
        }

        $query = "SELECT abandonedcarts.*,country.curcode FROM `" . DB_PREFIX . "abandonedcarts`  LEFT JOIN country ON (country.country_id = abandonedcarts.country_id)  WHERE `store_id`='" . $store . "' ";
        if ($notified) {
            $query .= "AND `notified` > 0 AND `ordered` = 0 ";
        } else if ($ordered) {
            $query .= "AND `ordered` = 1 ";
        } else {
            $query .= "AND `notified` = 0 ";
        }

        $query .= "ORDER BY `date_modified` DESC LIMIT " . $start . ", " . $limit;
        $query = $this->db->query($query);

        $abandonedCarts = array();
        foreach ($query->rows as $row) {
            $row['customer_info'] = json_decode($row['customer_info'], true);

            if (isset($row['customer_info']['language'])) {
                $lang_image = $this->getLanguageImage($row['customer_info']['language']);
                if (version_compare(VERSION, '2.2.0.0', "<")) {
                    if (isset($lang_image)) {
                        $row['customer_info']['lang_image'] = 'view/image/flags/' . $lang_image;
                    }
                } else {
                    $row['customer_info']['lang_image'] = 'language/' . $row['customer_info']['language'] . '/' . $row['customer_info']['language'] . '.png';
                }
            }


            if (isset($row['cart'])) {
                $row['cart'] = (json_decode($row['cart'], true));
                if (!is_array($row['cart'])) {
                    //remove this one
                    $this->db->query("DELETE FROM `abandonedcarts` WHERE `id`=" . (int)$row['id']);

                    continue;
                } else {
                    $cart = [];
                    foreach ($row['cart'] as $product) {
                        $p = $this->getProduct($product['product_id']);
                        if ($p) {
                            $p['quantity'] = $product['quantity'];
                            $cart[] = $p;
                        }
                    }
                    if (count($cart) == 0) {
                        $this->db->query("DELETE FROM `abandonedcarts` WHERE `id`=" . (int)$row['id']);
                        continue;
                    }
                    $row['cart'] = $cart;
                }
            }

            $row['customer_info'] = json_encode($row['customer_info']);
            $row['cart'] = json_encode($row['cart']);
            $abandonedCarts[] = $row;
        }
        return $abandonedCarts;
    }

    public function getTotalAbandonedCarts($store = 0, $notified = false, $ordered = false)
    {
        $query = "SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "abandonedcarts` WHERE `store_id`=" . $store . " ";

        if ($notified) {
            $query .= "AND `notified` > 0 AND `ordered` = 0";
        } else if ($ordered) {
            $query .= "AND `ordered` = 1 ";
        } else {
            $query .= "AND `notified` = 0 ";
        }

        $query = $this->db->query($query);

        return $query->row['count'];
    }

    public function getCartInfo($id,$lang = false)
    {
        $query = $this->db->query("SELECT abandonedcarts.*,country.curcode,country.iso_code_2  FROM `" . DB_PREFIX . "abandonedcarts` LEFT JOIN country ON (country.country_id = abandonedcarts.country_id)
			WHERE `id`=" . $id);
        if ($query->row) {

            $cart = [];

            $data = json_decode($query->row['cart'],true);

            foreach ($data as $product) {

                $p = $this->getProduct($product['product_id'],$lang);
                if ($p) {
                    $p['quantity'] = $product['quantity'];
                    $cart[] = $p;
                }
            }
            if (count($cart) == 0) {
                return false;
            }
            $query->row['cart'] = json_encode($cart);
        }
        return $query->row;
    }

    public function isUniqueCode($randomCode)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code='" . $this->db->escape($randomCode) . "'");

        if ($query->num_rows == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function generateuniquerandomcouponcode()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $couponCode = '';

        for ($i = 0; $i < 10; $i++) {
            $couponCode .= $characters[rand(0, strlen($characters) - 1)];
        }

        if ($this->isUniqueCode($couponCode)) {
            return $couponCode;
        } else {
            return $this->generateuniquerandomcouponcode();
        }
    }

    public function sendMail($data = array())
    {
        $this->load->model('setting/store');
        $this->load->model('setting/setting');

        $store_info = $this->model_setting_store->getStore($data['store_id']);

        if ($store_info) {
            $store_name = $store_info['name'];
        } else {
            $store_name = $this->config->get('config_name');
        }

        if (VERSION >= '2.0.0.0' && VERSION < '2.0.2.0') {
            $mailToUser = new Mail($this->config->get('config_mail'));
        } else {
            $mailToUser = new Mail();
            $mailToUser->protocol = $this->config->get('config_mail_protocol');
            $mailToUser->parameter = $this->config->get('config_mail_parameter');
            if (VERSION >= '2.0.2.0') {
                $mailToUser->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                $mailToUser->smtp_username = $this->config->get('config_mail_smtp_username');
                $mailToUser->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                $mailToUser->smtp_port = $this->config->get('config_mail_smtp_port');
                $mailToUser->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
            } else {
                $mailToUser->hostname = $this->config->get('config_mail_smtp_hostname');
                $mailToUser->username = $this->config->get('config_mail_smtp_username');
                $mailToUser->password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                $mailToUser->port = $this->config->get('config_mail_smtp_port');
                $mailToUser->timeout = $this->config->get('config_mail_smtp_timeout');
            }
        }

        $mailToUser->setTo($data['email']);
        $mailToUser->setFrom($data['store_email']);
        $mailToUser->setSender($store_name);
        $mailToUser->setSubject(html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8'));
        $mailToUser->setHtml($data['message']);

        $abandonedCartsSettings = $this->model_setting_setting->getSetting('AbandonedCarts', $data['store_id']);
        if (isset($abandonedCartsSettings['AbandonedCarts']['BCC']) && $abandonedCartsSettings['AbandonedCarts']['BCC'] == 'yes') {
            $mailToUser->setAbandonedCartsBcc($data['store_email']);
        }

        $mailToUser->send();

        if ($mailToUser)
            return true;
        else
            return false;
    }

    public function getGivenCoupons($data = array())
    {
        $givenCoupons = $this->db->query("	SELECT *
											FROM " . DB_PREFIX . "coupon
											WHERE name LIKE  '%AbCart [%'
											ORDER BY " . $data['sort'] . " " . $data['order'] . "
											LIMIT " . $data['start'] . ", " . $data['limit']);
        return $givenCoupons->rows;
    }

    public function getTotalGivenCoupons()
    {
        $givenCoupons = $this->db->query("SELECT COUNT(*) as count FROM " . DB_PREFIX . "coupon WHERE name LIKE '%AbCart [%'");

        return $givenCoupons->row['count'];
    }

    public function getUsedCoupons($data = array())
    {
        $usedCoupons = $this->db->query("SELECT *
		 								  FROM `" . DB_PREFIX . "coupon` AS c
										  JOIN `" . DB_PREFIX . "coupon_history` AS ch ON c.coupon_id=ch.coupon_id
										  WHERE name LIKE  '%AbCart [%'
										  ORDER BY " . $data['sort'] . " " . $data['order'] . "
										  LIMIT " . $data['start'] . ", " . $data['limit']);

        return $usedCoupons->rows;
    }

    public function getTotalUsedCoupons()
    {
        $givenCoupons = $this->db->query("SELECT COUNT(*) as count FROM `" . DB_PREFIX . "coupon` as c
											JOIN `" . DB_PREFIX . "coupon_history` AS ch ON c.coupon_id=ch.coupon_id
											WHERE name LIKE  '%AbCart [%'");

        return $givenCoupons->row['count'];
    }

    public function getLanguageImage($language_code)
    {
        $query = $this->db->query("SELECT image FROM " . DB_PREFIX . "language WHERE code = '" . $language_code . "'");

        return $query->row['image'];
    }

    public function getLanguageId($language_code)
    {
        $query = $this->db->query("SELECT language_id FROM " . DB_PREFIX . "language WHERE code = '" . $language_code . "'");

        return $query->row['language_id'];
    }

    public function getTotalRegisteredCustomers()
    {
        $TotalRegisteredCustomers = $this->db->query("SELECT count(*) as `registered` FROM `" . DB_PREFIX . "abandonedcarts` WHERE `customer_info` LIKE '%email%'");

        return $TotalRegisteredCustomers->row['registered'];
    }

    public function getTotalCustomers()
    {
        $TotalCustomers = $this->db->query("SELECT count(*) as `count` FROM `" . DB_PREFIX . "abandonedcarts`");

        return $TotalCustomers->row['count'];
    }

    public function getMostVisitedPages()
    {
        $Pages = $this->db->query("SELECT `last_page`, count(`last_page`) as count FROM `" . DB_PREFIX . "abandonedcarts` GROUP BY `last_page` ORDER BY count DESC LIMIT 15");

        return $Pages->rows;
    }


    public function getProduct($product_id, $custom_language = false)
    {
        $language_id = $this->config->get('config_language_id');
        if ($custom_language) {
            $language_id = $custom_language;
        }
        $query = $this->db->query("SELECT DISTINCT *,p.date_added as added_on, b.image AS brand_logo,
        (SELECT name FROM brand_description bd WHERE p.brand_id = bd.brand_id AND
        bd.language_id='" . (int)$language_id . "')AS brand_name ,
         pd.name AS name, p.image, m.name AS manufacturer,
         (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE
         pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'
         AND pd2.quantity = '1' AND
         ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount,
          (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id
           AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "'
           AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW() AND ps.deleted='no' )
           AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,
           (SELECT points FROM " . DB_PREFIX . "product_reward pr
           WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "') AS reward,
           (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss
           WHERE ss.stock_status_id = p.stock_status_id AND
           ss.language_id = '" . (int)$language_id . "') AS stock_status,
            (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$language_id . "') AS weight_class,
             (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$language_id . "') AS length_class,
              (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating,
               (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews,
               p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) LEFT JOIN brand b ON(p.brand_id=b.brand_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$language_id . "' AND p.status = '1' AND b.status = '1' AND p.date_available <= NOW() ");


        if ($query->num_rows) {
            //$seller_info = $this->db->query(" SELECT available_country FROM " . DB_PREFIX . "customerpartner_to_customer WHERE customer_id IN (SELECT customer_id FROM customerpartner_to_product WHERE product_id = '" . (int)$product_id . "') ");

            $available = 1;

//            if ($seller_info->num_rows) {
//                if ($seller_info->row['available_country'] == 0 || $seller_info->row['available_country'] == $this->country->getId()) {
//                    $available = 1;
//                }
//            }

            $stock_status_id = $query->row['stock_status_id'];

            return array(
                'product_id' => $query->row['product_id'],
                'name' => $query->row['name'],
                'description' => $query->row['description'],
                'meta_title' => $query->row['meta_title'],
                'meta_description' => $query->row['meta_description'],
                'meta_keyword' => $query->row['meta_keyword'],
                'tag' => $query->row['tag'],
                'model' => $query->row['model'],
                'sku' => $query->row['sku'],
                'upc' => $query->row['upc'],
                'ean' => $query->row['ean'],
                'jan' => $query->row['jan'],
                'isbn' => $query->row['isbn'],
                'mpn' => $query->row['mpn'],
                'location' => $query->row['location'],
                'quantity' => $query->row['quantity'],
                'stock_status' => $query->row['stock_status'],
                'brand_id' => $query->row['brand_id'],
                'brand_logo' => $query->row['brand_logo'],
                'brand_name' => $query->row['brand_name'],
                'stock_status_id' => $stock_status_id,
                'product_available' => $available,
                'image' => $query->row['image'],
                'manufacturer_id' => $query->row['manufacturer_id'],
                'manufacturer' => $query->row['manufacturer'],
                'price' => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
                'starting_from' => $this->getStartingFrom($product_id),
                'special' => $query->row['special'],
                //'shop_logo' => $store_logo,
                //'store_id' => $store_id,
                'reward' => $query->row['reward'],
                'points' => $query->row['points'],
                'tax_class_id' => $query->row['tax_class_id'],
                'date_available' => $query->row['date_available'],
                'weight' => $query->row['weight'],
                'weight_class_id' => $query->row['weight_class_id'],
                'length' => $query->row['length'],
                'width' => $query->row['width'],
                'height' => $query->row['height'],
                'length_class_id' => $query->row['length_class_id'],
                'subtract' => $query->row['subtract'],
                'rating' => round($query->row['rating']),
                'reviews' => $query->row['reviews'] ? $query->row['reviews'] : 0,
                'minimum' => $query->row['minimum'],
                'sort_order' => $query->row['sort_order'],
                'status' => $query->row['status'],
                'date_added' => $query->row['added_on'],
                'date_modified' => $query->row['date_modified'],
                'viewed' => $query->row['viewed']
            );
        } else {
            return false;
        }
    }

    public function getStartingFrom($product_id)
    {
        $price_list = array();
        $query_otp = $this->db->query("SELECT `price_prefix`, `price`, `special` FROM `" . DB_PREFIX . "otp_data` WHERE `product_id` = '" . (int)$product_id . "' AND (`price` > 0 OR `special` > 0)");
        if ($query_otp->num_rows) {

            $price = $this->db->query("SELECT `price` FROM `" . DB_PREFIX . "product` WHERE `product_id` = '" . (int)$product_id . "'")->row['price'];
            if ($price > 0) {
                $price_list[] = $price;
            }

            $query = $this->db->query("SELECT `price` FROM " . DB_PREFIX . "product_special WHERE `product_id` = '" . (int)$product_id . "' AND deleted='no' AND `customer_group_id` = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((`date_start` = '0000-00-00' OR `date_start` < NOW()) AND (`date_end` = '0000-00-00' OR `date_end` > NOW())) AND `price` > 0 ORDER BY `priority` ASC, `price` ASC LIMIT 1");
            if ($query->num_rows) {
                $price_list[] = $price;
                $price = $query->row['price'];
            }

            foreach ($query_otp->rows as $combination) {

                if ($combination['price'] > 0) {
                    if ($combination['price_prefix'] == '=') {
                        $price_list[] = $combination['price'];
                    } else {
                        if ($combination['price_prefix'] == '+') {
                            $price_list[] = $price + $combination['price'];
                        } else {
                            $price_list[] = $price - $combination['price'];
                        }
                    }
                }

                if ($combination['special'] > 0) {
                    $price_list[] = $combination['special'];
                }
            }
        }
        if (count($price_list)) {
            return min($price_list);
        } else {
            return false;
        }
    }
}
