<?php

class ModelNewsletterNewsletter extends Model {

    public function deleteNewsletter($id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "popup_email WHERE id = '" . (int) $id . "'");

        $this->cache->deletekeys('catalog.newsletter');
        $this->cache->deletekeys("admin.newsletter");
    }

    public function getNewsletterList($data = array()) {
        $sql = " SELECT DISTINCT n.id,n.email, n.status, n.confirm_sent, n.created_date, n.updated_date, n.status, n.gender,n.language_id ,n.country_id FROM " . DB_PREFIX . "popup_email n where n.id>0 ";
        if (isset($data['filter_id']) && !empty($data['filter_id'])) {
            $sql .= " AND n.id = '" . $data['filter_id'] . "' ";
        } else {
            if (isset($data['filter_country_id'])) {
                $sql .= " AND n.country_id = '" . (int) $data['filter_country_id'] . "'";
            }

            if (isset($data['filter_status'])) {
                $sql .= " AND n.status ='" . (int) $data['filter_status'] . "'";
            }

            if (isset($data['filter_gender'])) {
                $sql .= " AND n.gender ='" . (int) $data['filter_gender'] . "'";
            }

            if (!empty($data['filter_email'])) {
                $sql .= " AND email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
            }
        }
        $sort = array(
            'n.id',
            'n.email',
            'n.status',
            'n.created_date',
            'n.updated_date',
            'n.gender',
            'n.language_id',
            'n.country_id'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY LCASE(n.email)";
        }

        if (isset($data['order']) && $data['order'] == "DESC") {
            $sql .= " DESC ";
        } else {
            $sql .= " ASC ";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] <= 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
//die($sql);
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getNewsletter($id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "popup_email n  WHERE n.id = '" . (int) $id . "'");

        return $query->row;
    }

    public function getTotalNewsletter($data = array()) {
//        $query = $this->db->query(" SELECT COUNT(*) AS total FROM " . DB_PREFIX . "newsletter");
        $sql = " SELECT COUNT(*) AS total FROM " . DB_PREFIX . "popup_email n where n.id>0 ";
        if (isset($data['filter_id']) && !empty($data['filter_id'])) {
            $sql .= " AND n.id = '" . $data['filter_id'] . "' ";
        } else {
            if (isset($data['filter_country_id'])) {
                $sql .= " AND n.country_id = '" . (int) $data['filter_country_id'] . "'";
            }

            if (isset($data['filter_status'])) {
                $sql .= " AND n.status ='" . (int) $data['filter_status'] . "'";
            }

            if (isset($data['filter_gender'])) {
                $sql .= " AND n.gender ='" . (int) $data['filter_gender'] . "'";
            }

            if (!empty($data['filter_email'])) {
                $sql .= " AND n.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
            }
        }
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

}
