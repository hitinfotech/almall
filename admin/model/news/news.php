<?php

class ModelNewsNews extends Model {

    public function addNews($data) {
        $this->db->query(" INSERT INTO " . DB_PREFIX . "news SET `start_date`='" . date('Y-m-d H:i:s', strtotime($data['start_date'])) . "', `end_date` = '" . date('Y-m-d H:i:s', strtotime($data['end_date'])) . "', `sort_order` = '" . (int) $data['sort_order'] . "', `date_added`=NOW(), `date_modified`=NOW(),user_id = '" . (int)$this->user->getid() . "' ");

        $news_id = $this->db->getLastId();

        foreach ($data['news_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "news_description SET news_id = '" . (int) $news_id . "', language_id = '" . (int) $language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', body_text = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['body_text']))) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "news_image WHERE news_id = '" . (int) $news_id . "'");

        if (isset($data['news_image'])) {
            $images = $this->upload_images('news', 'news_image', $news_id);
            if (is_array($images)) {
                $this->load->model('tool/image');
                foreach ($data['news_image'] as $key => $news_image) {
                    $image = ((strlen($news_image['image_text']) > 0) ? $news_image['image_text'] : $images[$key]);

                    $this->db->query("INSERT INTO " . DB_PREFIX . "news_image SET news_id = '" . (int) $news_id . "', image = '" . $this->db->escape($image) . "', sort_order = '" . (int) $news_image['sort_order'] . "'");

                    foreach ($this->config_image->get('news_offers_malls') as $row) {
                        if ($row['width'] > 10) {
                            $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                        }
                    }
                }
            }
        }



        $this->url_custom->create_URL('news', $news_id);

        $this->db->query("DELETE FROM news_to_shop WHERE news_id='" . (int) $news_id . "'");
        foreach ($data['shops'] as $shop_id) {
            $this->db->query("INSERT INTO news_to_shop SET shop_id='" . (int) $shop_id . "', news_id='" . (int) $news_id . "' ");
        }
        $this->db->query("DELETE FROM news_to_brand WHERE news_id='" . (int) $news_id . "'");
        foreach ($data['brands'] as $brand_id) {
            $this->db->query("INSERT INTO news_to_brand SET brand_id='" . (int) $brand_id . "', news_id='" . (int) $news_id . "' ");
        }
        
        //$this->cache->deletekeys('catalog.news');
        //$this->cache->deletekeys("admin.news");
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='news', type_id='" . (int) $news_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");

        return $news_id;
    }

    public function editNews($news_id, $data) {
        $this->db->query(" UPDATE " . DB_PREFIX . "news SET `start_date`='" . date('Y-m-d H:i:s', strtotime($data['start_date'])) . "', `end_date` = '" . date('Y-m-d H:i:s', strtotime($data['end_date'])) . "', `home` = '" . $data['home'] . "',`status` = '" . $data['status'] . "', `sort_order` = '" . (int) $data['sort_order'] . "', `date_modified`=NOW(),last_mod_id = '" . (int)$this->user->getid() . "'  WHERE news_id ='" . (int) $news_id . "'");

        $this->db->query("DELETE FROM news_description WHERE news_id='" . (int) $news_id . "'");
        foreach ($data['news_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "news_description SET news_id = '" . (int) $news_id . "', language_id = '" . (int) $language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "', body_text = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['body_text']))) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "news_image WHERE news_id = '" . (int) $news_id . "'");

        if (isset($data['news_image'])) {
            $images = $this->upload_images('news', 'news_image', $news_id);
                $this->load->model('tool/image');
                foreach ($data['news_image'] as $key => $news_image) {
                    $image = ((strlen($news_image['image_text']) > 0) ? $news_image['image_text'] : $images[$key]);

                    $this->db->query("INSERT INTO " . DB_PREFIX . "news_image SET news_id = '" . (int) $news_id . "', image = '" . $this->db->escape($image) . "', sort_order = '" . (int) $news_image['sort_order'] . "'");

                    foreach ($this->config_image->get('news_offers_malls') as $row) {
                        if ($row['width'] > 10) {
                            $this->model_tool_image->resize($image, $row['width'], $row['hieght']);
                        }
                    }
                }
        }

        $this->url_custom->create_URL('news', $news_id);

        $this->db->query("DELETE FROM news_to_shop WHERE news_id='" . (int) $news_id . "'");
        foreach ($data['shops'] as $shop_id) {
            $this->db->query("INSERT INTO news_to_shop SET shop_id='" . (int) $shop_id . "', news_id='" . (int) $news_id . "' ");
        }
        $this->db->query("DELETE FROM news_to_brand WHERE news_id='" . (int) $news_id . "'");
        foreach ($data['brands'] as $brand_id) {
            $this->db->query("INSERT INTO news_to_brand SET brand_id='" . (int) $brand_id . "', news_id='" . (int) $news_id . "' ");
        }
        
        //$this->cache->deletekeys('catalog.news');
        //$this->cache->deletekeys("admin.news");
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='news', type_id='" . (int) $news_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function deleteNews($news_id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "news_link WHERE news_id = '" . (int) $news_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "news_image WHERE news_id = '" . (int) $news_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int) $news_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "news WHERE news_id = '" . (int) $news_id . "'");

        //$this->cache->deletekeys('catalog.news');
        //$this->cache->deletekeys("admin.news");
        
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='news', type_id='" . (int) $news_id . "', user_id='" . (int) $this->user->getid() . "', date_added=NOW() ");
    }

    public function getNewsList($data = array()) {
        $sql = " SELECT DISTINCT n.news_id, CONCAT(nd1.title, ' ',nd2.title ) title, (SELECT image FROM news_image ni WHERE n.news_id=ni.news_id LIMIT 1) image, n.status, n.sort_order, n.start_date, n.end_date, n.date_added, n.date_modified,u.username user_add, mu.username user_modify   FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd1 ON (n.news_id = nd1.news_id) LEFT JOIN " . DB_PREFIX . "news_description nd2 ON (n.news_id = nd2.news_id) LEFT JOIN " . DB_PREFIX . "news_to_shop n2s ON (n.news_id = n2s.news_id) LEFT JOIN " . DB_PREFIX . "store s ON (n2s.shop_id = s.store_id)LEFT JOIN zone z ON(s.zone_id=z.zone_id) LEFT JOIN " . DB_PREFIX . "user u ON (n.user_id = u.user_id)  LEFT JOIN " . DB_PREFIX . "user mu ON (n.last_mod_id = mu.user_id) WHERE nd1.language_id='1' AND nd2.language_id='2' ";

        if (isset($data['filter_news_id']) && !empty($data['filter_news_id'])) {
            $sql .= " AND n.news_id = '" . $data['filter_news_id'] . "' ";
        } else {
            if (isset($data['filter_country_id'])) {
                $sql .= " AND z.country_id = '" . (int) $data['filter_country_id'] . "'";
            }

            if (isset($data['filter_status'])) {
                $sql .= " AND n.status ='" . (int) $data['filter_status'] . "'";
            }

            if (isset($data['filter_group'])) {
                $sql .= " AND n2g.news_group_id ='" . (int) $data['filter_group'] . "'";
            }

            if (!empty($data['filter_description'])) {
                $sql .= " AND CONCAT(nd1.title,' ',nd1.body_text, ' ',nd2.title,' ',nd2.body_text) LIKE '%" . $this->db->escape($data['filter_description']) . "%'";
            }
        }

        $sort = array(
            'nd' . (int) $this->config->get('config_language_id') . '.title',
            'n.status',
            'n.date_added',
            'n.date_modified',
            'n.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY LCASE(nd" . (int) $this->config->get('config_language_id') . ".title)";
        }

        if (isset($data['order']) && $data['order'] == "DESC") {
            $sql .= " DESC ";
        } else {
            $sql .= " ASC ";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] <= 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }
//die($sql);
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getNews($news_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "news n LEFT JOIN " . DB_PREFIX . "news_description nd ON (n.news_id = nd.news_id)  WHERE n.news_id = '" . (int) $news_id . "' AND nd.language_id = '" . $this->config->get('config_language_id') . "' ");

        return $query->row;
    }

    public function getNewsDescriptions($news_id) {
        $news_description_data = array();
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "news_description WHERE news_id = '" . (int) $news_id . "' ");

        foreach ($query->rows as $result) {
            $news_description_data[$result['language_id']] = array(
                'title' => $result['title'],
                'description' => $result['description'],
                'body_text' => $result['body_text']
            );
        }

        return $news_description_data;
    }

    public function getNewsBrands($news_id) {
        $query = $this->db->query(" SELECT n2b.brand_id, CONCAT(bd1.name, ' - ',bd2.name) name FROM " . DB_PREFIX . "news_to_brand n2b LEFT JOIN brand b ON(n2b.brand_id=b.brand_id) LEFT JOIN brand_description bd1 ON(b.brand_id=bd1.brand_id) LEFT JOIN brand_description bd2 ON(b.brand_id=bd2.brand_id) WHERE bd1.language_id='1' AND bd2.language_id='2' AND news_id='" . (int) $news_id . "' ");
        return $query->rows;
    }

    public function getNewsShops($news_id) {
        $query = $this->db->query(" SELECT n2s.shop_id, CONCAT(sd1.name, ' - ',sd2.name,' | ',md.name, ' | ',zd.name) name FROM " . DB_PREFIX . "news_to_shop n2s LEFT JOIN store s ON(n2s.shop_id=s.store_id) LEFT JOIN store_description sd1 ON(s.store_id=sd1.store_id) LEFT JOIN store_description sd2 ON(s.store_id=sd2.store_id) LEFT JOIN mall m ON(s.mall_id=m.mall_id) LEFT JOIN mall_description md ON(m.mall_id=md.mall_id) LEFT JOIN zone z ON(s.zone_id=z.zone_id) LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE sd1.language_id='1' AND sd2.language_id='2' AND md.language_id='" . (int) $this->config->get('config_language_id') . "' AND zd.language_id='" . (int) $this->config->get('config_language_id') . "' AND news_id='" . (int) $news_id . "' ");
        return $query->rows;
    }

    public function getNewsImages($news_id) {
        $query = $this->db->query(" SELECT * FROM " . DB_PREFIX . "news_image WHERE news_id = '" . (int) $news_id . "'  ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getTotalNews($data = array()) {
//        $query = $this->db->query(" SELECT COUNT(*) AS total FROM " . DB_PREFIX . "news");
        $sql = " SELECT COUNT(*) AS total "
                . "FROM " . DB_PREFIX . "news n "
                . "LEFT JOIN " . DB_PREFIX . "news_description nd1 ON (n.news_id = nd1.news_id) "
                . "LEFT JOIN " . DB_PREFIX . "news_description nd2 ON (n.news_id = nd2.news_id) "
                . "LEFT JOIN " . DB_PREFIX . "news_to_shop n2s ON (n.news_id = n2s.news_id) "
                . "LEFT JOIN " . DB_PREFIX . "store s ON (n2s.shop_id = s.store_id) "
                . "LEFT JOIN zone z ON(s.zone_id=z.zone_id) "
                . "WHERE nd1.language_id='1' AND nd2.language_id='2' ";

        if (isset($data['filter_news_id']) && !empty($data['filter_news_id'])) {
            $sql .= " AND n.news_id = '" . $data['filter_news_id'] . "' ";
        } else {
            if (isset($data['filter_country_id'])) {
                $sql .= " AND z.country_id = '" . (int) $data['filter_country_id'] . "'";
            }

            if (isset($data['filter_status'])) {
                $sql .= " AND n.status ='" . (int) $data['filter_status'] . "'";
            }

            if (isset($data['filter_group'])) {
                $sql .= " AND n2g.news_group_id ='" . (int) $data['filter_group'] . "'";
            }

            if (!empty($data['filter_description'])) {
                $sql .= " AND CONCAT(nd1.title,' ',nd1.body_text, ' ',nd2.title,' ',nd2.body_text) LIKE '%" . $this->db->escape($data['filter_description']) . "%'";
            }
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

}
