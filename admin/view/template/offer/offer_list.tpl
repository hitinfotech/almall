<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </a> 
                <!--
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="(checkForDelete()) ? (confirm('<?php echo $text_confirm; ?>') ? $('#form-mall').submit() : false) : false;">
                    <i class="fa fa-trash-o"></i>
                </button>
                -->
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container-fluid">

        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">

                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_description; ?></label>
                                <input type="text" name="offer_description" value="<?php echo $filter_discription; ?>" placeholder="<?php echo $entry_description; ?>" id="input-offer_description" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-country"><?php echo $entry_country_id; ?></label>
                                <select name="filter_country_id" id="input-country_id" class="form-control">
                                    <option value=""><?php echo $select_country; ?></option>
                                    <?php foreach ($countries as $row) { ?>
                                        <?php if ($filter_country_id == $row['country_id']) { ?>
                                            <option value="<?php echo $row['country_id'] ?>" selected="selected"><?php echo $row['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $row['country_id'] ?>"><?php echo $row['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">

                            </div>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                        </div>
                    </div>
                </div>

                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-mall">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center">
                                        <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'sort_offer') { ?>
                                            <a href="<?php echo $sort_offer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_title; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_offer; ?>"><?php echo $column_title; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'date_added') { ?>
                                            <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'date_modified') { ?>
                                            <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'sort_order') { ?>
                                            <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_order; ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php echo $column_user_id; ?></td>
                                    <td class="text-center"><?php echo $column_last_mod_id; ?></td>

                                    <td class="text-left">
                                        <?php echo $column_action; ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($offer) { ?>
                                    <?php foreach ($offer as $value) { ?>
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" name="selected[]" value="<?php echo $value['offer_id']; ?>" 
                                                <?php echo ((isset($value['offer_id']) && !empty($value['offer_id']) && in_array($value['offer_id'], $selected)) ? "checked=\"checked\"" : ""); ?> 
                                                       />
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['title']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['date_added']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['date_modified']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['sort_order']; ?>
                                            </td>
                                            <td class="text-center"><b><?php echo $value['user_add']; ?></b></td>
                                            <td class="text-center"><b><?php echo $value['user_modify']; ?></b></td>

                                            <td class="text-left">
                                                <a href="<?php echo $value['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>



    </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
    /*
     function checkForDelete() {
     return ($('input[name="selected[]"]:checked').length > 0);
     }
     */
</script>
<script type="text/javascript"><!--
$('#button-filter').on('click', function () {
        var url = 'index.php?route=offer/offer&token=<?php echo $token; ?>';

        var filter_description = $('input[name=\'offer_description\']').val();
        // alert(filter_description);
        if (filter_description) {
            url += '&filter_description=' + encodeURIComponent(filter_description);
        }

        var filter_country_id = $('select[name=\'filter_country_id\']').val();
        // alert(filter_country_id);
        if (filter_country_id != '') {
            url += '&filter_country_id=' + encodeURIComponent(filter_country_id);
        }
        // alert(url);
        location = url;
    });
//--></script>