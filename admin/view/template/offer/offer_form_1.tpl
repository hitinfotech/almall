<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-offer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-offer" class="form-horizontal">
          <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab_attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <li><a href="#tab_links" data-toggle="tab"><?php echo $tab_links; ?></a></li>
            <li><a href="#tab_image" data-toggle="tab"><?php echo $tab_image; ?></a></li>
          </ul>
          <div class="tab-content">

            <input type="hidden" id="input-user_id" name="user_id" value="<?php echo $user_id;?>"></input>
            <div class="tab-pane active" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="offer_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($offer_description[$language['language_id']]) ? $offer_description[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_title[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_title[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="offer_description[<?php echo $language['language_id']; ?>][description]" rows="5" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($offer_description[$language['language_id']]) ? $offer_description[$language['language_id']]['description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-body_text<?php echo $language['language_id']; ?>"><?php echo $entry_body_text; ?></label>
                    <div class="col-sm-10">
                      <textarea name="offer_description[<?php echo $language['language_id']; ?>][body_text]" placeholder="<?php echo $entry_body_text; ?>" id="input-body_text<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($offer_description[$language['language_id']]) ? $offer_description[$language['language_id']]['body_text'] : ''; ?></textarea>
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>

            <div class="tab-pane" id="tab_attribute">
                <div class="form-group">
                <label class="col-sm-2 control-label" for="input-start_date"><?php echo $entry_start_date; ?></span></label>
                <div class="col-sm-10">
                  <div class="input-group start-date">
                    <input type="text" name="phone"  value="<?php echo $start_date; ?>" placeholder="<?php echo $entry_start_date; ?>" id="input-start_date" class="form-control" data-date-format="YYYY-MM-DD" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-end_date"><?php echo $entry_end_date; ?></label>
                <div class="col-sm-10">
                  <div class="input-group end-date">
                    <input type="text" name="end_date" value="<?php echo $end_date; ?>" placeholder="<?php echo $entry_end_date; ?>" id="input-end_date" class="form-control" data-date-format="YYYY-MM-DD" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                </div>
              </div>
              
            </div>
            
            <div class="tab-pane" id="tab_links">
             
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-link-country"><?php echo $entry_country_id; ?></label>
                <div class="col-sm-10">
                 <select class="form-control" name="link_country[]" id="input-link-country" data-input-type="country"  multiple  placeholder="<?php echo $entry_country_id; ?>"></select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-link-city"><?php echo $entry_city_id; ?></label>
                <div class="col-sm-10">
                 <select class="form-control" name="link_city[]" id="input-link-city" data-input-type="city"  multiple  placeholder="<?php echo $entry_city_id; ?>"></select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-link-mall"><?php echo $entry_mall_id; ?></label>
                <div class="col-sm-10">
                 <select class="form-control" name="link_mall[]" id="input-link-mall" data-input-type="mall"  multiple  placeholder="<?php echo $entry_mall_id; ?>"></select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-link-shop"><?php echo $entry_shop_id; ?></label>
                <div class="col-sm-10">
                 <select class="form-control" name="link_shop[]" id="input-link-shop" data-input-type="shop"  multiple  placeholder="<?php echo $entry_shop_id; ?>"></select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-link-group"><?php echo $entry_group_id; ?></label>
                <div class="col-sm-10">
                 <select class="form-control" name="link_group[]" id="input-link-group" data-input-type="group"  multiple  placeholder="<?php echo $entry_group_id; ?>"></select>
                </div>
              </div>

            </div>

            <div class="tab-pane" id="tab_image">
                <div class="table-responsive">
                    <table id="images" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <td class="text-left"><?php echo $entry_image; ?></td>
                                <td class="text-right"><?php echo $entry_sort_order; ?></td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $image_row = 0; ?>
                            <?php foreach ($offer_images as $offer_image) { ?>
                            <tr id="image-row<?php echo $image_row; ?>">
                                <td class="text-left">
                                  <a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail">
                                    <img src="<?php echo $offer_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                  </a>
                                  <input type="hidden" name="offer_image[<?php echo $image_row; ?>][image]" value="<?php echo $offer_image['image']; ?>" id="input-image<?php echo $image_row; ?>" />
                                </td>
                                <td class="text-right">
                                  <input type="text" name="offer_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $offer_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" />
                                </td>
                                <td class="text-left">
                                  <button type="button" onclick="$('#image-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger">
                                    <i class="fa fa-minus-circle"></i>
                                  </button>
                                </td>
                            </tr>
                            <?php $image_row++; ?>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2"></td>
                                <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

          </div>
        </form>
      </div>
    </div>
  </div>

  <script type="text/javascript">
  <!--

  var stores = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      remote: {
        url: 'index.php?route=offer/offer/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
        replace: function(url) {
          return url.replace('%TYPE', encodeURIComponent($('.tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.tt-input').val()));
        },
        wildcard: '%QUERY',
        filter: function(list){
          return $.map(list, function (v, i) { 
            return { store_id:v.store_id, name:v.name }; 
          });
        }
      }
  });

  stores.initialize();

  $('#input-link-country').tagsinput({
      itemValue: 'store_id',
      itemText: 'name',
      typeaheadjs: {
          name: 'stores',
          displayKey: 'name',
          source: stores.ttAdapter()
      }
  });

  $('#input-link-city').tagsinput({
      itemValue: 'store_id',
      itemText: 'name',
      typeaheadjs: {
          name: 'stores',
          displayKey: 'name',
          source: stores.ttAdapter()
      }
  });

  $('#input-link-mall').tagsinput({
      itemValue: 'store_id',
      itemText: 'name',
      typeaheadjs: {
          name: 'stores',
          displayKey: 'name',
          source: stores.ttAdapter()
      }
  });

  $('#input-link-shop').tagsinput({
      itemValue: 'store_id',
      itemText: 'name',
      typeaheadjs: {
          name: 'stores',
          displayKey: 'name',
          source: stores.ttAdapter()
      }
  });

  $('#input-link-group').tagsinput({
      itemValue: 'store_id',
      itemText: 'name',
      typeaheadjs: {
          name: 'stores',
          displayKey: 'name',
          source: stores.ttAdapter()
      }
  });

  // $('#input-link-country,#input-link-city,#input-link-mall,#input-link-shop,#input-link-group').tagsinput('add', { "value": 1 , "text": "Amsterdam" });

  //-->
  </script> 
  <script type="text/javascript">
    <!--
    $('.end-date, #input-start_date').datetimepicker({
        pickTime: false
    });
    $('.start-date, #input-end_date').datetimepicker({
        pickTime: false
    });
    -->
  </script>
  <script type="text/javascript">
  <!--
    var image_row = <?php echo $image_row; ?>;
    function addImage() {
        html = '<tr id="image-row' + image_row + '">';
        html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /><input type="hidden" name="product_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
        html += '  <td class="text-right"><input type="text" name="product_image[' + image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#images tbody').append(html);

        image_row++;
    }
  //-->
  </script>
  <script type="text/javascript">
  <!--
  <?php foreach ($languages as $language) { ?>
    $('#input-body_text<?php echo $language['language_id']; ?>').summernote({
    	height: 300
    });
  <?php } ?>
  //-->
  </script>
  <script type="text/javascript">
  <!--
    $('#language a:first').tab('show');
  //-->
  </script>
</div>
<?php echo $footer; ?>