<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
        <script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
        <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
        <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
    </head>
    <style>
        .content-padding {
            padding-bottom: 110px;
            padding-top: 20px;
        }
        .top-buffer {
            margin-top:20px;
        }
    </style>
    <body>
        <div class="container">
            <div class="content-padding">
                <div style="page-break-after: always;">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <img src="<?php echo $logo ?>" alt="logo" />
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <br><br><br>
                                    <div class='col-md-6 col-sm-6 col-xs-6'>
                                        <p style="font-size:12px;"><strong>From : <?php echo $from_date ?></strong></p>
                                        <p style="font-size:12px;"><strong>To   : <?php echo $to_date ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p style="font-size:20px; margin-top: 50px;margin-bottom: 50px;"><?php echo 'Sayidaty Orders Report' ?></p>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td class="text-left"><b><?php echo 'Order Id'; ?></b></td>
                                    <td class="text-left <?php echo ($seller_name) ? '' : 'hide'?>" width='25%' ><b><?php echo $column_seller_name ?></b></td>
                                    <td class="text-left <?php echo ($buyer_name) ? '' : 'hide'?>"><b><?php echo $column_buyer_name ?></b></td>
                                    <td class="text-left <?php echo ($buyer_country) ? '' : 'hide'?>"><b><?php echo $column_buyer_country ; ?></b></td>
                                    <td class="text-left <?php echo ($oder_subtotal) ? '' : 'hide'?>"><b><?php echo $column_order_subtotal; ?></b></td>
                                    <td class="text-left <?php echo ($discount) ? '' : 'hide'?>"><b><?php echo $column_discount_voucher; ?></b></td>
                                    <td class="text-left <?php echo ($shipping) ? '' : 'hide'?> "><b><?php echo $column_delivery_charges ; ?></b></td>
                                    <td class="text-left <?php echo ($codfees) ? '' : 'hide'?> "><b><?php echo $column_order_cod ; ?></b></td>
                                    <td class="text-left <?php echo ($order_total) ? '' : 'hide'?>"><b><?php echo $column_order_total; ?></b></td>
                                    <td class="text-left <?php echo ($payment_method) ? '' : 'hide'?>"><b><?php echo $column_payment_method; ?></b></td>
                                    <td class="text-left <?php echo ($currency) ? '' : 'hide'?>"><b><?php echo $column_order_currency; ?></b></td>
                                    <td class="text-left <?php echo ($vat_amount) ? '' : 'hide'?>"><b><?php echo $column_vat_amount; ?></b></td>
                                    <td class="text-left"><b><?php echo $column_ship_company; ?></b></td>
                                    <td class="text-left"><b><?php echo "Store Credit"; ?></b></td>
                                </tr>
                            </thead>
                            <?php
                            $sum_order_total = 0;
                            $sum_sub_totals = 0;
                            $sum_discount = 0;
                            $sum_shipping = 0;
                            $sum_codfees = 0;
                            $sum_vat = 0;
                            $sum_credit=0;
                            ?>
                    <?php foreach ($orders as $order_id => $order) { ?>
                            <tbody>
                            <?php
                            $sum_order_total+=$order['total'];
                            $sum_sub_totals+=$order['sub_total'];
                            $sum_discount+=isset($order['coupon']) ? $order['coupon'] : 0;
                            $sum_shipping+=isset($order['shipping']) ? $order['shipping'] : 0;
                            $sum_codfees+=isset($order['codfees']) ? $order['codfees'] : 0;
                            $sum_vat+=$order['vat_amount'];
                            $sum_credit+=$order['credit'];
                            ?>
                                    <tr>
                                        <td class="text-left"><?php echo $order['order_id']; ?></td>
                                        <td class="text-left <?php echo ($seller_name) ? '' : 'hide'?>" width='25%'><?php echo $order['seller_name'] ?></td>
                                        <td class="text-left <?php echo ($buyer_name) ? '' : 'hide'?>"><?php echo $order['buyer_name']; ?></td>
                                        <td class="text-left <?php echo ($buyer_country) ? '' : 'hide'?>"><?php echo $order['payment_country'] ?></td>
                                        <td class="text-left <?php echo ($oder_subtotal) ? '' : 'hide'?>"><?php echo isset($order['sub_total']) ? $order['sub_total'] : 0 ?></td>
                                        <td class="text-left <?php echo ($discount) ? '' : 'hide'?>"><?php echo isset($order['coupon']) ? $order['coupon'] : 0; ?></td>
                                        <td class="text-left <?php echo ($shipping) ? '' : 'hide'?>"><?php echo isset($order['shipping']) ? $order['shipping'] : 0; ?></td>
                                        <td class="text-left <?php echo ($codfees) ? '' : 'hide'?>"><?php echo isset($order['codfees']) ? $order['codfees'] : 0; ?></td>
                                        <td class="text-left <?php echo ($order_total) ? '' : 'hide'?>"><?php echo isset($order['total']) ? $order['total'] : 0; ?></td>
                                        <td class="text-left <?php echo ($payment_method) ? '' : 'hide'?>"><?php echo $order['payment_code'] ?></td>
                                        <td class="text-left <?php echo ($currency) ? '' : 'hide'?>"><?php echo $order['currency'] ?></td>
                                        <td class="text-left <?php echo ($vat_amount) ? '' : 'hide'?>"><?php echo $order['vat_amount'] ?></td>
                                        <td class="text-left"><?php echo $order['shipping_company'] ?></td>
                                        <td class="text-left"><?php echo $order['credit'] ?></td>
                                    </tr>
                    <?php } ?>
                                <tr>
                                    <td class="text-left"  class=""><b>Totals :</b></td>
                                    <td class="text-left <?php echo ($seller_name) ? '' : 'hide'?>"><b></b></td>
                                    <td class="text-left <?php echo ($buyer_name) ? '' : 'hide'?>"><b></b></td>
                                    <td class="text-left <?php echo ($buyer_country) ? '' : 'hide'?>"><b></b></td>
                                    <td class="text-left <?php echo ($oder_subtotal) ? '' : 'hide'?>"> <b><?php echo round($sum_sub_totals,2); ?> </b></td>
                                    <td class="text-left <?php echo ($discount) ? '' : 'hide'?>"><b><?php echo round($sum_discount,2); ?></b></td>
                                    <td class="text-left <?php echo ($shipping) ? '' : 'hide'?>"><b><?php echo round($sum_shipping,2); ?></b></td>
                                    <td class="text-left <?php echo ($codfees) ? '' : 'hide'?>"><b><?php echo round($sum_codfees,2); ?></b></td>
                                    <td class="text-left <?php echo ($order_total) ? '' : 'hide'?>" ><b><?php echo round($sum_order_total,2); ?></b></td>
                                    <td class="text-left <?php echo ($payment_method) ? '' : 'hide'?>"></td>
                                    <td class="text-left <?php echo ($currency) ? '' : 'hide'?>"></td>
                                    <td class="text-left <?php echo ($vat_amount) ? '' : 'hide'?>"><b><?php echo round($sum_vat,2);?></b></td>
                                    <td class="text-left "></td>
                                    <td class="text-left "><b><?php echo round($sum_credit,2);?></b></td>
                                </tr>
                            </tbody>
                  </table>
                    <div class='col-md-12 col-sm-12 col-xs-12'>
                        <br/><br/>
                        <p style="font-size:15px;"><?php echo $text_more_details ?></p>
                    </div>

                </div>
            </div>
    </body>
</html>
