<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class='row'>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-date-start-1"><?php echo $entry_date_start; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_start_1" data-date-format="YYYY-MM-DD" id="input-date-start-1" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-date-end-1"><?php echo $entry_date_end; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_end_1" data-date-format="YYYY-MM-DD" id="input-date-end-1" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-partner"><?php echo $filter_partner; ?></label>
                                <input type="text" name="filter_partner" placeholder="<?php echo $filter_partner; ?>" id="input-partner" class="form-control" />
                                <input type="hidden" name="filter_partner_id" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-sm-3 col-sm-offset-6'>
                          <div class="form-group">
                              <button type="button" id="button-filter" class="btn btn-primary pull-right" onclick="monthly_report()"><?php echo $button_generate_monthly; ?></button>
                          </div>
                        </div>
                        <div class='col-sm-3'>
                          <div class="form-group">
                              <button type="button" id="generate_invoice" class="btn btn-primary pull-right" onclick="generate_invoice()"><?php echo $button_generate_monthly_invoice; ?></button>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class='row'>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-date-start-2"><?php echo $entry_date_start; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_start_2" data-date-format="YYYY-MM-DD" id="input-date-start-2" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-date-end-2"><?php echo $entry_date_end; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_end_2" data-date-format="YYYY-MM-DD" id="input-date-end-2" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                          <div class="form-group">
                              <label class="control-label" for="input-account-manager"><?php echo $entry_account_manager; ?></label>
                              <div >
                                <select name="account_manager" id="input-account-manager" class="form-control">
                                    <option value="*"><?php echo $select_account; ?></option>
                                      <?php foreach ($account_managers as $user) { ?>
                                        <?php if ($user_id == $user['key']) { ?>
                                            <option value="<?php echo $user['key'] ?>" selected="selected"><?php echo $user['value'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $user['key'] ?>"><?php echo $user['value'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                          </div>

                        </div>
                        <div class='col-sm-3'>
                          <div class="form-group"><br>
                              <button type="button" id="button-filter" class="btn btn-primary" onclick="partners_detailed_report()"><?php echo $button_generate; ?></button>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="well">
                  <div class='row'>
                      <div class='col-sm-12'>
                        <div class="form-group">
                          <label class="col-sm-2"><input type="checkbox" name='hidden_columns[]' value='seller_name'>Seller Name</label>
                          <label class="col-sm-2"><input type="checkbox" name='hidden_columns[]' value='buyer_name'>buyer Name</label>
                          <label class="col-sm-2"><input type="checkbox" name='hidden_columns[]' value='buyer_country'>buyer's country</label>
                          <label class="col-sm-2"><input type="checkbox" name='hidden_columns[]' value='oder_subtotal'>Order Subtotal</label>
                          <label class="col-sm-2"><input type="checkbox" name='hidden_columns[]' value='discount'>Discount</label>
                          <label class="col-sm-2"><input type="checkbox" name='hidden_columns[]' value='cod'>COD</label>
                          <label class="col-sm-2"><input type="checkbox" name='hidden_columns[]' value='currency'>Currency</label>
                          <label class="col-sm-2"><input type="checkbox" name='hidden_columns[]' value='shipping'>Delivery charges</label>
                          <label class="col-sm-2"><input type="checkbox" name='hidden_columns[]' value='order_total'>Order Total</label>
                          <label class="col-sm-2"><input type="checkbox" name='hidden_columns[]' value='payment_method'>Payment method</label>
                          <label class="col-sm-2"><input type="checkbox" name='hidden_columns[]' value='vat_amount'>Vat Amount</label>
                        </div>
                      </div>
                  </div>
                    <div class='row'>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-date-start-3"><?php echo $entry_date_start; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_start_3" data-date-format="YYYY-MM-DD" id="input-date-start-3" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-date-end-3"><?php echo $entry_date_end; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_end_3" data-date-format="YYYY-MM-DD" id="input-date-end-3" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                                <select name="filter_order_status" id="input-order-status" class="form-control">
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                          <option value="<?php echo $order_status['order_status_id']; ?>" <?php echo ($order_status['order_status_id']==5) ?"selected" : ''; ?>><?php echo $order_status['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class='col-sm-3'>
                          <div class="form-group">
                              <button type="button" id="button-filter" class="btn btn-primary pull-left" onclick="order_detailed_report()"><?php echo $button_order_report; ?></button>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class='row'>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-date-start-4"><?php echo $entry_date_start; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_start_4" data-date-format="YYYY-MM-DD" id="input-date-start-4" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-date-end-4"><?php echo $entry_date_end; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_end_4" data-date-format="YYYY-MM-DD" id="input-date-end-4" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-partner"><?php echo $filter_partner; ?></label>
                                <input type="text" name="filter_partner" placeholder="<?php echo $filter_partner; ?>" id="input-partner" class="form-control" />
                                <input type="hidden" name="filter_partner_id" />
                            </div>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-sm-3'>
                            <div class="form-group">
                                <button type="button" id="generate_vat_invoice" class="btn btn-primary pull-right" onclick="generate_vat_invoice()">Generate VAT Invoice</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $('input[name=\'filter_partner\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=customerpartner/partner/autocompleteForProducts&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    json.unshift({
                        seller_id: 0,
                        company: '<?php echo "none"; ?>'
                    });

                    response($.map(json, function (item) {
                        return {
                            label: item['company'],
                            value: item['seller_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'filter_partner\']').val(item['label']);
            $('input[name=\'filter_partner_id\']').val(item['value']);
        }
    });

    function monthly_report(){
      var date_start = $('input[name=\'filter_date_start_1\']').val();
      var date_end = $('input[name=\'filter_date_end_1\']').val();
      var partner_id = $('input[name=\'filter_partner_id\']').val();

      if (date_start != '*' && date_start != '' && date_end != '' && partner_id!=''){
        window.open( 'index.php?route=customerpartner/invoice&customer_id='+partner_id+'&date_start=' + date_start + '&date_end=' + date_end + '&token=<?php echo $token ?>');
      }else{
        alert('Please Select Month, Year and Seller to generate this report')
      }

    }

    function partners_detailed_report(){
      var date_start = $('input[name=\'filter_date_start_2\']').val();
      var date_end = $('input[name=\'filter_date_end_2\']').val();
      var account_manager = $('select[name=\'account_manager\']').val();


      if (date_start != '' && date_start!='undefined'  && date_end!='undefined' && date_end != ''){
        window.open( 'index.php?route=customerpartner/monthly_invoice&date_start=' + date_start + '&date_end=' + date_end + '&account_manager=' + account_manager + '&token=<?php echo $token ?>');
      }else{
        alert('Please Select Month and Year to generate this report')
      }

    }

    function order_detailed_report(){
      var date_start = $('input[name=\'filter_date_start_3\']').val();
      var date_end = $('input[name=\'filter_date_end_3\']').val();
      var order_status = $('select[name=\'filter_order_status\']').val();

      var viewed_columns = [];
      $('input[name="hidden_columns[]"]:checked').each(function() {
      viewed_columns.push(this.value);
      });

      if (date_start != '' && date_start!='undefined'  && date_end!='undefined' && date_end != ''){
        window.open( 'index.php?route=customerpartner/monthly_invoice_by_order&date_start=' + date_start + '&date_end=' + date_end + '&order_status='+ order_status+'&viewed_columns='+JSON.stringify(viewed_columns)+ '&token=<?php echo $token ?>');
      }else{
        alert('Please Select Month and Year to generate this report')
      }
    }

    function generate_invoice(){
      var date_start = $('input[name=\'filter_date_start_1\']').val();
      var date_end = $('input[name=\'filter_date_end_1\']').val();
      var partner_id = $('input[name=\'filter_partner_id\']').val();

      if (date_start != '*' && date_start != '' && date_end != '' && partner_id!=''){
        var strconfirm = confirm("Are you sure you want to Create Monthly invoice?");
         if (strconfirm == true) {
             window.open( 'index.php?route=customerpartner/generate_seller_monthly_invoice&customer_id='+partner_id+'&date_start=' + date_start + '&date_end=' + date_end);
         }
      }else{
        alert('Please Select Month, Year and Seller to generate this report')
      }
    }

    function generate_vat_invoice() {
        var date_start = $('input[name=\'filter_date_start_4\']').val();
        var date_end = $('input[name=\'filter_date_end_4\']').val();
        var partner_id = $('input[name=\'filter_partner_id\']').val();
        if (date_start != '*' && date_start != '' && date_end != '' && partner_id!=''){
            var strconfirm = confirm("Are you sure you want to Create Monthly invoice?");
            if (strconfirm == true) {
                window.open( 'index.php?route=customerpartner/vat_invoice&customer_id='+partner_id+'&date_start=' + date_start + '&date_end=' + date_end);
            }
        }else{
            alert('Please Select Month, Year and Seller to generate this report')
        }
    }

    $('.date').datetimepicker({
            pickTime: false
        });

    </script>
<?php echo $footer; ?>
