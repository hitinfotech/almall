<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
        <script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
        <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
        <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
    </head>
    <style>
        .content-padding {
            padding-bottom: 110px;
            padding-top: 20px;
        }
        .top-buffer {
            margin-top:20px;
        }
    </style>
    <body>
        <div class="container">
            <div class="content-padding">
                <div style="page-break-after: always;">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <img src="<?php echo $logo ?>" alt="logo" />
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <p style="font-size:16px;"><strong><?php echo $company ?></strong></p>
                                        <p style="font-size:16px;"><strong>Country</strong> : <?php echo $data['country']; ?></p>
                                        <p style="font-size:14px;"><strong>Contact</strong> : <?php echo $seller_name; ?></p>
                                        <p style="font-size:14px;"><strong>Email</strong> : <?php echo $email ?></p>
                                        <p style="font-size:14px;"><strong>Phone</strong> : <?php echo $phone; ?></p>
                                        <p style="font-size:14px;"><strong>Total Orders</strong> : <?php echo $total_orders; ?></p>
                                        <p style="font-size:14px;"><strong>Successful Orders</strong> : <?php echo $grand_total_completed_order_AED + $grand_total_completed_order_SAR; ?></p>
                                    </div>
                                    <div class='col-md-6 col-sm-6 col-xs-6'>
                                        <p style="font-size:12px;"><strong>From : <?php echo $from_date ?></strong></p>
                                        <p style="font-size:12px;"><strong>To   : <?php echo $to_date ?></strong></p>
                                        <?php if($invoice_filename != '') {?>
                                          <p style="font-size:14px;"><strong>Invoice No.</strong> : <?php echo $invoice_filename; ?></p>
                                        <?php } ?>
                                        <p style="font-size:14px;"><strong>Commission</strong> : <?php echo $commission; ?></p>
                                        <p style="font-size:14px;"><strong>Transaction Fees</strong> : <?php echo $flat_rate; ?></p>
                                        <p style="font-size:14px;"><strong>Storage Fees</strong> : <?php echo round($fbs_storage_charges * ($days_per_month - $fbs_free_storage_days )/$days_per_month,2); ?></p>
                                        <p style="font-size:14px;"><strong>FullFillment Fees</strong> : <?php echo $fbs_fullfillment_charges ; ?></p>
                                        <p style="font-size:12px;"><strong>Bank Info</strong> : <br/> <small><?php echo $bank; ?></small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php foreach($report_countries as $key => $country){ ?>
                      <?php  if (!empty(${'partner_orders_'.$country['curcode']})) { ?>
                          <p style="font-size:20px;"><?php echo $country['name_old']. " " . $text_order_detail ?></p>

                          <table class="table table-bordered">
                              <thead>
                                  <tr>
                                      <td class="text-left"><b><?php echo 'Order-NO'; ?></b></td>
                                      <td><b><?php echo $column_product . ' / ' . $column_sku. ' / ' . $column_model; ?></b></td>
                                      <td class="text-left"><b><?php echo $column_quantity; ?></b></td>
                                      <td class="text-left"><b><?php echo 'Date'; ?></b></td>
                                      <td class="text-left"><b><?php echo 'Status'; ?></b></td>
                                      <td class="text-left"><b><?php echo 'Total Sales  '.$country['curcode']; ?></b></td>
                                      <td class="text-left"><b><?php echo 'Total Sales ('.$converting_currency.')'; ?></b></td>
                                </tr>
                              </thead>
                              <tbody>
                                  <?php if (${'partner_orders_'.$country['curcode']}) { ?>
                                      <?php foreach (${'partner_orders_'.$country['curcode']} as $order) { ?>
                                          <tr>
                                              <td class="text-right"><?php echo $order['order_id']; ?></td>
                                              <td class="text-left">
                                                <?php echo $order['name'] . ' - ' . $order['sku'] . ' - ' . $order['model'] ?>
                                                <?=  ( $order['is_fbs']) ?"<p><b>Bin: ".$order['fbs_bin'].", Stack: ".$order['fbs_stack']."</b></p>" :"" ?>
                                              </td>
                                              <td class="text-left"><?php echo $order['quantity']; ?></td>
                                              <td class="text-left"><?php echo date('Y-m-d', strtotime($order['date_added'])); ?></td>
                                              <td class="text-left"><?php echo $order['status'] ?></td>
                                              <td class="text-left"><?php echo round($order['total'],3); ?></td>
                                              <td class="text-left"><?php echo round($order['converted_total'],3); ?></td>
                                          </tr>
                                      <?php } ?>
                                      <tr>
                                          <td class="text-right" colspan="2"><strong>Total No Of Items</strong></td>
                                          <td><strong><?php echo ${'grand_total_quantity_'.$country['curcode']} ?></strong></td>
                                          <td class='text-right' colspan="2"><strong><?php echo "Total Sales" ?></strong></td>
                                          <td><strong><?php echo round(${'grand_total_total_local_currency_'.$country['curcode']},3) ." ".$country['curcode']?> </strong></td>
                                          <td><strong><?php echo round(${'grand_total_total_'.$country['curcode']},3) ." ".$converting_currency?> </strong></td>
                                      </tr>
                                      <tr>
                                          <td class="text-right" colspan="2"><strong>
                                              <?php echo "Total No. of successfully orders (completed)"  ?></strong>
                                          </td>
                                          <td class="text-left" ><strong>
                                              <?php echo ${'grand_total_completed_order_'.$country['curcode']}  ?></strong>
                                          </td>

                                          <td class='text-right' colspan="3"><strong><?php echo "Total Transaction Fees" ?></strong></td>
                                          <td><strong><?php echo ${'grand_total_fees_'.$country['curcode']} ." ".$converting_currency ?> </strong></td>
                                      </tr>
                                      <tr>
                                          <td class='text-right' colspan="6"><strong><?php echo "Total Commission" ?></strong></td>
                                          <td><strong><?php echo round($commission * ${'grand_total_total_'.$country['curcode']} / 100,3) ." ".$converting_currency ?> </strong></td>
                                      </tr>
                                      <tr>
                                          <td class='text-right' colspan="6"><strong><?php echo $text_total_transaction_fees ?></strong></td>
                                          <td><strong><?php echo round(${'grand_total_total_'.$country['curcode']} - ($commission * ${'grand_total_total_'.$country['curcode']} / 100 + ${'grand_total_fees_'.$country['curcode']} ),3 )  ." ".$converting_currency ?> </strong></td>
                                      </tr>

                                  <?php } else { ?>
                                      <tr><td class="text-center" colspan="9"><?php echo $text_no_results; ?></td></tr>
                                  <?php } ?>

                              </tbody>
                          </table>
                      <?php } ?>
                    <?php } ?>

                    <?php if (!empty($partner_orders_FBS)) { ?>
                        <p style="font-size:20px;"><?php echo 'FBS  ' . $text_order_detail ?></p>

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td class="text-left"><b><?php echo 'Order-NO'; ?></b></td>
                                    <td><b><?php echo $column_product . ' / ' . $column_sku. ' / ' . $column_model; ?></b></td>
                                    <td class="text-left"><b><?php echo $column_quantity; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Date'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Status'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Total Sales (Local currency)'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Total Sales'; ?></b></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($partner_orders_FBS) {  ?>
                                    <?php foreach ($partner_orders_FBS as $order) { ?>
                                        <tr>
                                            <td class="text-right"><?php echo $order['order_id']; ?></td>
                                            <td class="text-left">
                                              <?php echo $order['name'] . ' - ' . $order['sku']. ' - ' . $order['model'] ?>
                                              <?= ( $order['is_fbs']) ?"<p><b>Bin: ".$order['fbs_bin'].", Stack: ".$order['fbs_stack']."</b></p>" :""?>
                                            </td>
                                            <td class="text-left"><?php echo $order['quantity']; ?></td>
                                            <td class="text-left"><?php echo date('Y-m-d', strtotime($order['date_added'])); ?></td>
                                            <td class="text-left"><?php echo $order['status'] ?></td>
                                            <td class="text-left"><?php echo round($order['total'],3) . ' '.$order['original_currency_code']; ?></td>
                                            <td class="text-left"><?php echo round($order['converted_total'],3); ?></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td class="text-right" colspan="2"><strong>Total No Of Items</strong></td>
                                        <td><strong><?php echo $grand_total_quantity_FBS ?></strong></td>
                                        <td class='text-right' colspan="3"><strong><?php echo "Total Sales" ?></strong></td>
                                        <td><strong><?php echo round($grand_total_total_FBS,3) ?> SAR</strong></td>
                                    </tr>
                                    <tr>
                                        <td class='text-right' colspan="2"><strong><?php echo "Total No. of successfully orders (completed)"  ?></strong></td>
                                        <td class="text-left"><strong>
                                            <?php echo $grand_total_completed_order_FBS  ?></strong>
                                        </td>
                                        <td class='text-right' colspan="3"><strong><?php echo "Total transaction Fees" ?></strong></td>
                                        <td><strong><?php echo $grand_total_fees_FBS ?> SAR</strong></td>
                                    </tr>
                                    <tr>
                                        <td class='text-right' colspan="6"><strong><?php echo "Total Commission" ?></strong></td>
                                        <td><strong><?php echo round(($commission * $grand_total_total_FBS / 100),2) ?> SAR</strong></td>
                                    </tr>
                                      <tr>
                                          <td class='text-right' colspan="6"><strong><?php echo "Fulfillment Charges" ?></strong></td>
                                          <td><strong><?php echo $fbs_fullfillment_charges * $grand_total_completed_order_FBS  ?> SAR</strong></td>
                                      </tr>

                                      <tr>
                                          <td class='text-right' colspan="6"><strong><?php echo "Storage Charges" ?></strong></td>
                                          <td><strong><?php echo round($fbs_storage_charges ,2) ?> SAR</strong></td>
                                      </tr>

                                    <tr>
                                        <td class='text-right' colspan="6"><strong><?php echo $text_total_transaction_fees ?></strong></td>
                                        <td><strong><?php echo round($grand_total_total_FBS - ($commission * $grand_total_total_FBS / 100 + $grand_total_fees_FBS ) - ($fbs_fullfillment_charges * $grand_total_completed_order_FBS ) - $fbs_storage_charges ,2) ?> SAR</strong></td>
                                    </tr>
                                <?php } else { ?>
                                    <tr><td class="text-center" colspan="9"><?php echo $text_no_results; ?></td></tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    <?php } ?>

                    <h3><b><?php echo  "Final amount payable : ".$total_seller_amount_for_all . " ". $seller_currency?></b></h3>
                        <br>
                         <p>- <?php echo "Total transaction  = Fees (No. of successful order * transaction fee)" ?></p>
                        <p>- <?php echo "Total Commission  = (comission * total Sales)" ?></p>
                        <p>- <?php echo "Storage Charges  = fbs_storage_charges * (days_per_month - fbs_free_storage_days )/days_per_month" ?></p>
                        <p>- <?php echo "Fulfillment Charges  = (fbs_fullfillment_charges * FBS_total_completed_order)" ?></p>
                        <p>- Seller Amount  = (Total Sales - Commission - Transaction Fees)</p>
                    <?php if ($partner_returns) { ?>
                        <p style="font-size:20px;"><?php echo $text_rma_details ?></p>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-left">
                                            <?php echo $column_order_rma_id; ?>
                                        </th>
                                        <th class="text-left">
                                            <?php echo $column_product; ?>
                                        </th>
                                        <th class="text-left">
                                            <?php echo $column_reason; ?>
                                        </th>
                                        <th class="text-left">
                                            <!-- <a > -->
                                            <?php echo $column_quantity; ?>
                                            <!-- </a> -->
                                        </th>
                                    </tr>
                                </thead>
                                <?php
                                if ($partner_returns) {
                                    foreach ($partner_returns as $key => $partner_return)

                                        ?>
                                    <tr>
                                        <td>
                                            <?php echo $partner_return['order_id'] . ' / ' . $partner_return['id']; ?>
                                        </td>
                                        <td>
                                            <?php echo $partner_return['product_name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $partner_return['reason']; ?>
                                        </td>
                                        <td>
                                            <?php echo $partner_return['quantity']; ?>
                                        </td>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="8" class="text-center"><?php echo $no_records; ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } ?>
                    <div class='col-md-12 col-sm-12 col-xs-12'>
                        <br/><br/>
                        <p style="font-size:15px;"><?php echo $text_more_details ?></p>
                    </div>

                </div>
            </div>
    </body>
</html>
