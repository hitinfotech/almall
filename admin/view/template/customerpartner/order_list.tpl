<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class='row'>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-seller-country"><?php echo $entry_seller_country; ?></label>
                                <select name="filter_seller_country" class="form-control">
                                    <option value='0' >choose</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value='<?php echo $country["country_id"] ?>' <?php echo ($country["country_id"] == $filter_seller_country ) ? 'selected' : '' ?> ><?php echo $country['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-payment-country"><?php echo $entry_payment_country; ?></label>
                                <select name="filter_payment_country" class="form-control">
                                    <option value='0' >choose</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value='<?php echo $country["country_id"] ?>' <?php echo ($country["country_id"] == $filter_payment_country ) ? 'selected' : '' ?>><?php echo $country['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-shipping-country"><?php echo $entry_shipping_country; ?></label>
                                <select name="filter_shipping_country" class="form-control">
                                    <option value='0' >choose</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value='<?php echo $country["country_id"] ?>' <?php echo ($country["country_id"] == $filter_shipping_country ) ? 'selected' : '' ?> ><?php echo $country['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        <div class="form-group">
                                <label class="control-label" for="input-customer-number"><?php echo $entry_customer_number; ?></label>
                                <input type="text" name="filter_customer_number" value="<?php echo $filter_customer_number; ?>" placeholder="<?php echo $entry_customer_number; ?>" id="input-customer-number" class="form-control" />
                            </div>
                        </div>
                        <!-- <div class="col-sm-3">
                             <div class="form-group">
                                 <label class="control-label" for="filter_seller_email"><?php echo $entry_seller_email; ?></label>
                                 <input type="text" name="filter_seller_email" value="<?php echo $filter_seller_email; ?>" placeholder="<?php echo $entry_seller_email; ?>" id="input-seller-email" class="form-control" />
                             </div>
                         </div>-->
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-order-id"><?php echo $entry_order_id; ?></label>
                                <input type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-customer"><?php echo $entry_customer; ?></label>
                                <input type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" placeholder="<?php echo $entry_customer; ?>" id="input-customer" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-customer-email"><?php echo $entry_customer_email; ?></label>
                                <input type="text" name="filter_customer_email" value="<?php echo $filter_customer_email; ?>" placeholder="<?php echo $entry_customer_email; ?>" id="input-customer-email" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-seller"><?php echo $entry_seller; ?></label>
                                <input type="text" name="filter_seller" value="<?php echo $filter_seller; ?>" placeholder="<?php echo $entry_seller; ?>" id="input-seller" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-seller-number"><?php echo $entry_seller_number; ?></label>
                                <input type="text" name="filter_seller_number" value="<?php echo $filter_seller_number; ?>" placeholder="<?php echo $entry_seller_number; ?>" id="input-seller-number" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                                <select name="filter_order_status" id="input-order-status" class="form-control">
                                    <option value="*"></option>
                                    <?php if ($filter_order_status == '0') { ?>
                                        <option value="0" selected="selected"><?php echo $text_missing; ?></option>
                                    <?php } else { ?>
                                        <option value="0"><?php echo $text_missing; ?></option>
                                    <?php } ?>
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $filter_order_status) { ?>
                                            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-total"><?php echo $entry_total; ?></label>
                                <input type="text" name="filter_total" value="<?php echo $filter_total; ?>" placeholder="<?php echo $entry_total; ?>" id="input-total" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-date-added"><?php echo $entry_date_added; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" placeholder="<?php echo $entry_date_added; ?>" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-date-modified"><?php echo $entry_date_modified; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_modified" value="<?php echo $filter_date_modified; ?>" placeholder="<?php echo $entry_date_modified; ?>" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>

                    </div>
                </div>
                <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td class="text-right"><?php if ($sort == 'o.order_id') { ?>
                                            <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_id; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_order; ?>"><?php echo $column_order_id; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'customer') { ?>
                                            <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'status') { ?>
                                            <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php echo $column_shipping_coutry; ?></td>
                                    <td class="text-right"><?php if ($sort == 'o.total') { ?>
                                            <a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_total; ?>"><?php echo $column_total; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'o.date_added') { ?>
                                            <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'o.date_modified') { ?>
                                            <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } ?></td>
                                    <td class="text-right"><?php echo $column_action; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($orders) { ?>
                                    <?php foreach ($orders as $order) { ?>
                                        <tr>
                                            <td class="text-right"><?php echo $order['order_id']; ?></td>
                                            <td class="text-left"><?php echo $order['customer']; ?></td>
                                            <td class="text-left"><?php echo $order['status']; ?></td>
                                            <td class="text-left"><?php echo $order['shipping_country']; ?></td>
                                            <td class="text-right"><?php echo $order['total']; ?></td>
                                            <td class="text-left"><?php echo $order['date_added']; ?></td>
                                            <td class="text-left"><?php echo $order['date_modified']; ?></td>
                                            <td class="text-right"><a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a><a href="<?php echo $order['edit_order']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-info"><i class="fa fa-pencil"></i></a></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
  $('#button-filter').on('click', function () {
            url = 'index.php?route=customerpartner/order&token=<?php echo $token; ?>';

            var filter_order_id = $('input[name=\'filter_order_id\']').val();

            if (filter_order_id) {
                url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
            }

            var filter_customer = $('input[name=\'filter_customer\']').val();

            if (filter_customer) {
                url += '&filter_customer=' + encodeURIComponent(filter_customer);
            }

            var filter_seller = $('input[name=\'filter_seller\']').val();

            if (filter_seller) {
                url += '&filter_seller=' + encodeURIComponent(filter_seller);
            }

            var filter_seller_email = $('input[name=\'filter_seller_email\']').val();

            if (filter_seller_email) {
                url += '&filter_seller_email=' + encodeURIComponent(filter_seller_email);
            }

            var filter_seller_number = $('input[name=\'filter_seller_number\']').val();

            if (filter_seller_number) {
                url += '&filter_seller_number=' + encodeURIComponent(filter_seller_number);
            }

            var filter_customer_number = $('input[name=\'filter_customer_number\']').val();

            if (filter_customer_number) {
                url += '&filter_customer_number=' + encodeURIComponent(filter_customer_number);
            }

            var filter_customer_email = $('input[name=\'filter_customer_email\']').val();

            if (filter_customer_email) {
                url += '&filter_customer_email=' + encodeURIComponent(filter_customer_email);
            }

            var filter_order_status = $('select[name=\'filter_order_status\']').val();

            if (filter_order_status != '*') {
                url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
            }

            var filter_total = $('input[name=\'filter_total\']').val();

            if (filter_total) {
                url += '&filter_total=' + encodeURIComponent(filter_total);
            }

            var filter_date_added = $('input[name=\'filter_date_added\']').val();

            if (filter_date_added) {
                url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
            }

            var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

            if (filter_date_modified) {
                url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
            }

            var filter_seller_country = $('select[name=\'filter_seller_country\']').val();

            if (filter_seller_country && filter_seller_country != 0) {
                url += '&filter_seller_country=' + encodeURIComponent(filter_seller_country);
            }

            var filter_payment_country = $('select[name=\'filter_payment_country\']').val();

            if (filter_payment_country && filter_payment_country != 0) {
                url += '&filter_payment_country=' + encodeURIComponent(filter_payment_country);
            }

            var filter_shipping_country = $('select[name=\'filter_shipping_country\']').val();

            if (filter_shipping_country && filter_shipping_country != 0) {
                url += '&filter_shipping_country=' + encodeURIComponent(filter_shipping_country);
            }

            location = url;
        });
        </script>
    <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
    <script type="text/javascript">
    $('.date').datetimepicker({
            pickTime: false
        });

        // Seller
        $('input[name=\'filter_seller\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=customerpartner/partner/autocompleteForProducts&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        json.unshift({
                            seller_id: 0,
                            company: '<?php echo "none"; ?>'
                        });

                        response($.map(json, function (item) {
                            return {
                                label: item['company'],
                                value: item['seller_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'filter_seller\']').val(item['label']);
            }
        });

        </script></div>
<?php echo $footer; ?>
