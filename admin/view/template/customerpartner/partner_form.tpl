<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>

            <div class="panel-body">

                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
                    <ul class="nav nav-tabs">
                      <!-- <li><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li> -->
                        <li class="active"><a href="#tab-dashboard" data-toggle="tab"><?php echo "Dashboard"; ?>  <i class="fa fa-spinner fa-spin remove-me"></i></a></li>
                        <li><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab-profile" data-toggle="tab"><?php echo $tab_info; ?></a></li>
                        <li><a href="#tab-product" data-toggle="tab"><?php echo $tab_product; ?></a></li>
                        <li><a href="#tab-legal-info" data-toggle="tab"><?php echo $tab_legal_info; ?></a></li>
                        <li><a href="#tab-financial-info" data-toggle="tab"><?php echo $tab_financial_info; ?></a></li>
                        <li><a href="#tab-order" data-toggle="tab"><?php echo $tab_order; ?></a></li>
                        <!-- <li><a href="#tab-transaction" data-toggle="tab"><?php echo $tab_transaction; ?></a></li> -->
                        <li><a href="#tab-location" data-toggle="tab"><?php echo $tab_location; ?></a></li>
                    </ul>

                    <div class="tab-content">

                        <!-- <div class="tab-pane active" id="tab-general"></div> -->

                        <div id="tab-dashboard" class="tab-pane active">

                        </div>

                        <div class="tab-pane " id="tab-profile">

                            <div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> <?php echo $tab_profile_info; ?>
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-image"><span data-toggle="tooltip" title="<?php echo $entry_avatar_info; ?>"><?php echo $entry_avatar; ?></span></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo (isset($avatar_placeholder_admin_listing)?$avatar_placeholder_admin_listing : ''); ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <?php if(isset($avatar_original_admin_listing)){ ?><a target="_blank" href="<?php echo $avatar_original_admin_listing ?>">Original</a><?php }?>
                                    <input type="file" name="avatar" value="" id="input-image" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-account-manager"><?php echo $entry_account_manager; ?></label>
                                <div class="col-sm-10">
                                    <?php /* the user_id is not valid because we use it in many table to inform about user added the record */?>
                                  <select name="customer[user_id]" id="input-account-manager" class="form-control">
                                      <option value="*"><?php echo $select_account; ?></option>
                                        <?php foreach ($account_managers as $user) { ?>
                                          <?php if ($user_id == $user['key']) { ?>
                                              <option value="<?php echo $user['key'] ?>" selected="selected"><?php echo $user['value'] ?></option>
                                          <?php } else { ?>
                                              <option value="<?php echo $user['key'] ?>"><?php echo $user['value'] ?></option>
                                          <?php } ?>
                                      <?php } ?>
                                  </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[firstname]" value="<?php echo $firstname; ?>"  placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[lastname]" value="<?php echo $lastname; ?>"  placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-entry-trackcode"><?php echo $entry_trackcode; ?></label>
                                <div class="col-sm-10">
                                    <select name="customer[trackcode]" id="input-entry-trackcode" class="form-control">
                                        <?php if($trackcode == 1) { ?>
                                        <option value="1" selected="selected"><?php echo $entry_yes; ?></option>
                                        <option value="0"><?php echo $entry_no; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $entry_yes; ?></option>
                                        <option value="0" selected="selected"><?php echo $entry_no; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-trackcodeadd"><?php echo $entry_trackcodeadd; ?></label>
                                <div class="col-sm-10">
                                    <textarea name="customer[trackcodeadd]" id="input-trackcodeadd" class="form-control"><?php echo $trackcodeadd; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-trackcodeview"><?php echo $entry_trackcodeview; ?></label>
                                <div class="col-sm-10">
                                    <textarea name="customer[trackcodeview]" id="input-trackcodeview" class="form-control"><?php echo $trackcodeview; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[email]" value="<?php echo $email; ?>"  placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-fbs_email"><?php echo $entry_email; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[fbs_email]" value="<?php echo isset($fbs_email) ? $fbs_email : ''; ?>"  placeholder="<?php echo $entry_email; ?>" id="input-fbs_email" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-phone"><?php echo $entry_phone; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[telephone]" value="<?php echo $telecode.' ' .$phone; ?>"  placeholder="<?php echo $entry_phone; ?>" id="input-phone" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-num_of_offices"><?php echo $entry_num_of_offices; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[num_of_branches]" value="<?php echo $num_of_offices; ?>"  placeholder="<?php echo $entry_num_of_offices; ?>" id="input-num_of_offices" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-is-return"><?php echo $entry_return_policy; ?></label>
                                <div class="col-sm-10">
                                    <select name="customer[return_policy_id]" id="input-is-return" class="form-control">
                                        <option value="0" selected="selected"><?php echo 'Default Return Prolicy'; ?></option>
                                        <?php foreach ($returnpolicies as $policy) { ?>
                                            <?php if ($policy['return_policy_id'] == $return_policy_id) { ?>
                                                <option value="<?php echo $policy['return_policy_id'] ?>" selected="selected"><?php echo $policy['name']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $policy['return_policy_id'] ?>"><?php echo $policy['name']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-available_countries"><?php echo $entry_available_countries; ?></label>
                                <div class="col-sm-10">
                                  <select name="customer[available_country]" id="input-available_countries" class="form-control">
                                      <option value="0">Sell to All Countries</option>
                                      <?php if ($available_countries) { ?>
                                          <?php foreach ($available_countries as $available) { ?>
                                              <?php if ($available['country_id'] == $available_country) { ?>
                                                  <option value="<?php echo $available['country_id']; ?>" selected><?php echo $available['name']; ?></option>
                                              <?php } else { ?>
                                                  <option value="<?php echo $available['country_id']; ?>"><?php echo $available['name']; ?></option>
                                              <?php } ?>
                                          <?php } ?>
                                      <?php } ?>
                                  </select>
                                </div>
                            </div>

                            <div class="form-group" <?= ($available_country != 0) ? "style='display: none'" : '' ?> id='excluded-countries'>
                                <label class="col-sm-2 control-label" for="input-excluded-countries"><?php echo $entry_excluded_countries; ?></label>
                                <div class="col-sm-10">
                                  <?php if ($available_countries) { ?>
                                      <?php foreach ($available_countries as $available) { ?>
                                            <input type='checkbox' value="<?php echo $available['country_id']; ?>" name='customer[execluded_countries][]' <?= in_array($available['country_id'],$execluded_countries) ?'checked' : '' ?> ><?php echo $available['name']; ?></option>
                                      <?php } ?>
                                  <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-admin_name"><?php echo $entry_admin_name; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[admin_name]" value="<?php echo $admin_name; ?>"  placeholder="<?php echo $entry_admin_name; ?>" id="input-admin_name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-admin_phone"><?php echo $entry_admin_phone; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[admin_phone]" value="<?php echo $admin_telecode.' '. $admin_phone; ?>"  placeholder="<?php echo $entry_admin_phone; ?>" id="input-admin_phone" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-admin_email"><?php echo $entry_admin_email; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[admin_email]" value="<?php echo $admin_email; ?>"  placeholder="<?php echo $entry_admin_email; ?>" id="input-admin_email" class="form-control" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-gender"><?php echo $entry_gender; ?></label>
                                <div class="col-sm-10">
                                    <select name="customer[gender]" class="form-control">
                                        <option> </option>
                                        <option value="M" <?php if ($gender == 'M') echo 'selected'; ?> > Male </option>
                                        <option value="F" <?php if ($gender == 'F') echo 'selected'; ?>> Female </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-clienturl"><?php echo $entry_SEO_url; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[clienturl]"  value="<?php echo $seo_url ?>" placeholder="client's url" id="input-clienturl" class="form-control" />
                                    <?php if (isset($error_seo_required) && $error_seo_required != '') { ?>
                                        <div class="text-danger"><?php echo $error_seo_required; ?></div>
                                    <?php } else if (isset($error_unique) && $error_unique != '') { ?>
                                        <div class="text-danger"><?php echo $error_unique; ?></div>
                                    <?php } else if (isset($error_wrong_syntax) && $error_wrong_syntax != '') { ?>
                                        <div class="text-danger"><?php echo $error_wrong_syntax; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-website"><?php echo $entry_website; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[website]" value="<?php echo $website; ?>" placeholder="" id="input-website" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-twitterid"><?php echo $entry_twitter; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[twitterid]" value="<?php echo $twitterid; ?>" placeholder="" id="input-twitterid" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-facebookid"><?php echo $entry_facebook; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[facebookid]" value="<?php echo $facebookid; ?>" placeholder="" id="input-facebookid" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-instagramid"><?php echo $entry_instagramid; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[instagramid]" value="<?php echo $instagramid; ?>" placeholder="" id="input-instagramid" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-companylocality"><?php echo $entry_locality; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[companylocality]" value="<?php echo $companylocality; ?>" placeholder="" id="input-companylocality" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i>
                                                <?php foreach ($countries as $value_country) { ?>
                                                    <?php if ($value_country['country_id'] == $country) { ?>
                                                        <img src="./view/image/flags/<?php echo strtolower($value_country['iso_code_2']); ?>.png" id="country">
                                                    <?php } ?>
                                                <?php } ?>
                                            </i></span>

                                        <select name="customer[country_id]" id="input-country" class="form-control">
                                            <option value=""></option>
                                            <?php if ($countries) { ?>
                                                <?php foreach ($countries as $value_country) { ?>
                                                    <?php if ($value_country['country_id'] == $country) { ?>
                                                        <option value="<?php echo $value_country['country_id']; ?>" data-val="<?php echo $value_country['iso_code_2']; ?>" selected><?php echo $value_country['name']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $value_country['country_id']; ?>" data-val="<?php echo $value_country['iso_code_2']; ?>"><?php echo $value_country['name']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php if (isset($error_country_required) && $error_country_required != '') { ?>
                                        <div class="text-danger"><?php echo $error_country_required; ?></div>
                                    <?php } ?>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-zone-id"><?php echo $entry_zone; ?></label>
                                <div class="col-sm-10">
                                    <select name="customer[zone_id]" id="input-zone-id" class="form-control">
                                        <option value=""></option>
                                        <?php if ($zones) { ?>
                                            <?php foreach ($zones as $value_zone) { ?>
                                                <?php if ($value_zone['zone_id'] == $zone_id) { ?>
                                                    <option value="<?php echo $value_zone['zone_id']; ?>" selected><?php echo $value_zone['name']; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $value_zone['zone_id']; ?>" ><?php echo $value_zone['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[city]" value="<?php echo $city; ?>" placeholder="" id="input-city" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="is_fbs"><?php echo $entry_is_fbs; ?></label>
                                <div class="col-sm-10">
                                    <input type="checkbox" name="customer[is_fbs]"  placeholder="<?php echo $entry_is_fbs; ?>" id="is_fbs" class="form-control" <?php echo (isset($is_fbs) && ($is_fbs == 1 || $is_fbs == 'on'))? 'checked' : ''; ?> />
                                </div>
                            </div>

                            <div class="form-group fbs <?php echo ($is_fbs == 0 && $is_fbs != 'on')? 'hide' : ''; ?>">
                                <label class="col-sm-2 control-label" for="input-fbs_store_location"><?php echo $entry_fbs_store_location; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[fbs_store_location]" value="<?php echo isset($fbs_store_location) ? $fbs_store_location : ''; ?>" placeholder="<?php echo $entry_fbs_store_location; ?>" id="input-fbs_store_location" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group fbs <?php echo ($is_fbs == 0 && $is_fbs != 'on')? 'hide' : ''; ?>">
                                <label class="col-sm-2 control-label" for="input-fbs_inventory_volume"><?php echo $entry_fbs_inventory_volume; ?></label>
                                <div class="col-sm-5">
                                    <input type="text" name="customer[fbs_inventory_volume]" value="<?php echo isset($fbs_inventory_volume) ? $fbs_inventory_volume : 0; ?>" placeholder="<?php echo $entry_fbs_inventory_volume; ?>" id="input-fbs_inventory_volume" class="form-control" />
                                </div>
                                <div class="col-sm-5">
                                    <select class="form-control" name="customer[fbs_inventory_volume_unit]" >
                                      <?php foreach($fbs_volume_units as $key => $unit) {?>
                                        <option value="<?php echo $unit['key'] ?>" <?php echo (isset($fbs_inventory_volume_unit) && ($fbs_inventory_volume_unit == $unit['key'])) ? "selected" : ""?>> <?php echo $unit['value']  ?></option>
                                      <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group fbs <?php echo ($is_fbs == 0 && $is_fbs != 'on')? 'hide' : ''; ?>">
                                <label class="col-sm-2 control-label" for="input-date-range"><?php echo $entry_date_range; ?></label>
                                <div class="col-sm-10">
                                    <div class="row">
                                      <label class="col-sm-1 control-label" for="input-date-in"><?php echo $entry_fbs_date_in; ?></label>
                                      <div class="col-sm-5">
                                        <div class="input-group date">
                                          <input type="text" name="customer[fbs_date_in]" value="<?php echo isset($fbs_date_in) ? $fbs_date_in : ""; ?>" placeholder="<?php echo $entry_fbs_date_in; ?>" id="input-fbs-date-in" data-date-format="YYYY-MM-DD"  class="form-control" />
                                          <span class="input-group-btn">
                                              <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                          </span>
                                          </div>
                                      </div>
                                      <label class="col-sm-1 control-label" for="input-date-out"><?php echo $entry_fbs_date_out; ?></label>
                                      <div class="col-sm-5">
                                        <div class="input-group date">
                                          <input type="text" name="customer[fbs_date_out]" value="<?php echo isset($fbs_date_out) ? $fbs_date_out : ""; ?>" placeholder="<?php echo $entry_fbs_date_out; ?>" id="input-fbs-date-out" data-date-format="YYYY-MM-DD"  class="form-control" />
                                          <span class="input-group-btn">
                                              <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group fbs <?php echo ($is_fbs == 0 && $is_fbs != 'on')? 'hide' : ''; ?>">
                                <label class="col-sm-2 control-label" for="input-fbs_free_storage_days"><?php echo $entry_fbs_free_storage_days; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[fbs_free_storage_days]" value="<?php echo isset($fbs_free_storage_days) ? $fbs_free_storage_days : 0; ?>" placeholder="<?php echo $entry_fbs_free_storage_days; ?>" id="input-fbs_free_storage_days" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group not-fbs <?php echo ($is_fbs == 1 || $is_fbs == 'on')? 'hide' : ''; ?>">
                                <label class="col-sm-2 control-label" for="input-warehouse_address"><?php echo $entry_warehouse_address; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[warehouse_address]" value="<?php echo $warehouse_address; ?>" placeholder="" id="input-warehouse_address" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-open"><span data-toggle="tooltip" ><?php echo $entry_open; ?></span></label>
                                <div class="col-sm-10">
                                    <textarea name="customer[opening_time]" rows="5" placeholder="<?php echo $entry_open; ?>" id="input-open" class="form-control"><?php echo $opening_time; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-backgroundcolor"><span data-toggle="tooltip" title="<?php echo $entry_theme; ?>"><?php echo $entry_theme; ?></span></label>
                                <div class="col-sm-10">
                                    <div class="input-group colorpicker">
                                        <span class="input-group-addon"><i></i></span>
                                        <input type="text" name="customer[backgroundcolor]" value="<?php echo $backgroundcolor; ?>" placeholder="" id="input-backgroundcolor" class="form-control" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-image"><span data-toggle="tooltip" title="<?php echo $entry_banner_info; ?>"><?php echo $entry_banner; ?></span></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo isset($companybanner_placeholder_admin) ? $companybanner_placeholder_admin : ""; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <?php if(isset($companybanner_original_admin)){ ?><a target="_blank" href="<?php echo $companybanner_original_admin ?>">Original</a><?php }?>
                                    <input type="file" name="companybanner" value="" id="input-image-companybanner" />
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane " id="tab-general">
                            <!-- { nav-tabs } !-->
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                    <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img
                                                src="./view/image/flags/<?php echo $language['image']; ?>"
                                                title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?>
                                        </a></li>
                                <?php } ?>
                            </ul>
                            <!-- { / nav-tabs } !-->

                            <!-- { tab-content } !-->
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">


                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-screenname[<?php echo $language['language_id']; ?>]"><span data-toggle="tooltip" title="<?php echo $entry_screenname_info; ?>"><?php echo $entry_screenname; ?></span></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="description[<?php echo $language['language_id']; ?>][screenname]" value="<?php echo isset($description[$language['language_id']]['screenname']) ? $description[$language['language_id']]['screenname'] : ''; ?>" placeholder="" id="input-screenname[<?php echo $language['language_id']; ?>]" class="form-control" />
                                                <?php if (isset($error_screenname_required) && $error_screenname_required != '') { ?>
                                                    <div class="text-danger"><?php echo $error_screenname_required; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-shortprofile[<?php echo $language['language_id']; ?>]"><?php echo $entry_profile; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="description[<?php echo $language['language_id']; ?>][shortprofile]" id="input-shortprofile[<?php echo $language['language_id']; ?>]" class="form-control"><?php echo isset($description[$language['language_id']]['shortprofile']) ? $description[$language['language_id']]['shortprofile'] : ''; ?></textarea>
                                            </div>
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>
                            <!-- { / tab-content } !-->



                        </div>
                        <!-- transaction -->
                        <!-- <div class="tab-pane" id="tab-transaction">

                          <div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> <?php echo $tab_transaction_info; ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                          </div>

                          <div id="transaction"></div>
                          <br/>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-description"><?php echo $entry_description; ?></label>
                            <div class="col-sm-10">
                              <input type="text" name="description" placeholder="<?php echo $entry_description; ?>" id="input-description" class="form-control" />
                            </div>
                          </div>

                          <div class="form-group">
                           <label class="col-sm-2 control-label" for="input-amount"><?php echo $entry_amount; ?></label>
                            <div class="col-sm-10">
                              <input type="text" name="amount" placeholder="<?php echo $entry_amount; ?>" id="input-amount" class="form-control" />
                            </div>
                          </div>

                          <div class="text-right">
                            <button type="button" id="button-transaction" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_add_transaction; ?></button>
                          </div>
                        </div> -->

                        <!-- order -->
                        <div class="tab-pane" id="tab-order">

                            <div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> <?php echo $tab_order_info; ?>
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> <?php echo $tab_order; ?></h3>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <td class="text-right"><?php echo $entry_orderid; ?></td>
                                                <td class="text-left"><?php echo $entry_name; ?></td>
                                                <td class="text-left"><?php echo $entry_products; ?></td>
                                                <td class="text-left"><?php echo $column_date_added; ?></td>
                                                <td class="text-left"><?php echo str_replace(':', '', $entry_status); ?></td>
                                                <td class="text-left"><?php echo $entry_total; ?></td>
                                                <td class="text-left"><?php echo $column_action; ?></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (isset($partner_orders) AND $partner_orders) { ?>
                                                <?php foreach ($partner_orders as $partner_order) { ?>
                                                    <tr>
                                                        <td class="text-right"><?php echo $partner_order['order_id']; ?></td>
                                                        <td class="text-left"><?php echo $partner_order['name']; ?></td>
                                                        <td class="text-left"><?php
                                                            if (isset($partner_order['productname'])) {
                                                                echo substr($partner_order['productname'], 0, -2);
                                                            }
                                                            ?></td>
                                                        <td class="text-left"><?php echo $partner_order['date_added']; ?></td>
                                                        <td class="text-left"><?php echo $partner_order['orderstatus']; ?></td>
                                                        <td class="text-left"><?php
                                                            if (isset($partner_order['total'])) {
                                                                echo $partner_order['total'];
                                                            }
                                                            ?></td>
                                                        <td class="text-left">
                                                            <a href="<?php echo $partner_order['view']; ?>" data-toggle="tooltip" title="<?php echo $text_view; ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                                            <a href="<?php echo $partner_order['edit']; ?>" data-toggle="tooltip" title="<?php echo $text_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <tr><td class="text-center" colspan="7"><?php echo $text_no_results; ?></td></tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <!-- product -->
                        <div class="tab-pane" id="tab-product">

                            <div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> <?php echo $tab_product_info; ?>
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-commission"><?php echo $entry_commission; ?></label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <input type="text" name="customer[commission]" value="<?php echo $commission; ?>" placeholder="<?php echo $entry_commission; ?>" id="input-commission" class="form-control" />
                                        <span class="input-group-addon"> <b>%</b> </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-flat-rate"><?php echo $entry_flat_rate; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[flat_rate]" value="<?php echo $flat_rate; ?>" placeholder="<?php echo $entry_flat_rate; ?>" id="input-flat-rate" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-contract-date"><?php echo $entry_contract_date; ?></label>
                                <div class="col-sm-10 input-group date">
                                    <input type="text" value='<?= $contract_date ?>' name="customer[contract_date]" data-date-format="YYYY-MM-DD" id="input-date-start-1" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group fbs <?php echo ($is_fbs == 0 && $is_fbs != 'on')? 'hide' : ''; ?>">
                                <label class="col-sm-2 control-label" for="fullfillment_charges"><?php echo $entry_fbs_fullfillment_charges; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[fbs_fullfillment_charges]" value="<?php echo isset($fbs_fullfillment_charges) ? $fbs_fullfillment_charges : 0; ?>" placeholder="<?php echo $entry_fbs_fullfillment_charges; ?>" id="fullfillment_charges" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group fbs <?php echo ($is_fbs == 0 && $is_fbs != 'on')? 'hide' : ''; ?>">
                                <label class="col-sm-2 control-label" for="storage_charges"><?php echo $entry_fbs_storage_charges; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[fbs_storage_charges]" value="<?php echo isset($fbs_storage_charges) ? $fbs_storage_charges : 0; ?>" placeholder="<?php echo $entry_fbs_storage_charges; ?>" id="storage_charges" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-paypalid"><?php echo $entry_paypalid; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[paypalid]" value="<?php echo $paypalid; ?>"  placeholder="<?php echo $entry_paypalid; ?>" id="input-paypalid" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-otherpayment"><?php echo $entry_otherinfo; ?></label>
                                <div class="col-sm-10">
                                    <textarea name="customer[otherpayment]" placeholder="<?php echo $entry_otherinfo; ?>" id="input-otherpayment" class="form-control"><?php echo $otherpayment; ?></textarea>
                                </div>
                            </div>

                            <?php /*
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-product"><span data-toggle="tooltip" title="<?php echo $entry_product_id_info; ?>"><?php echo $entry_product_id; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="product" value="" placeholder="<?php echo $entry_product_id; ?>" id="input-product" class="form-control" />
                                    <div id="product-product" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach ($partner_products as $partner_product) { ?>
                                            <div id="product-id<?php echo $partner_product['product_id']; ?>">
                                                <i class="fa fa-minus-circle"></i><?php echo $partner_product['name']; ?>
                                                <input type="hidden" name="product_ids[]" value="<?php echo $partner_product['product_id']; ?>" />
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php if (isset($error_duplicated_product) && $error_duplicated_product != '') { ?>
                                        <div class="text-danger"><?php echo $error_duplicated_product; ?></div>
                                    <?php } ?>
                                </div>
                            </div> */ ?>
                        </div>

                        <div class="tab-pane" id="tab-legal-info">

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-company_name"><?php echo $entry_company_name; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[legal_info][company_name]" value="<?php echo $company_name; ?>" placeholder="" id="input-company_name" class="form-control" />
                                    <?php if (isset($error_companyname_required) && $error_companyname_required != '') { ?>
                                    <div class="text-danger"><?php echo $error_companyname_required; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-trade_license_number"><?php echo $entry_trade_license_number; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[legal_info][trade_license_number]" value="<?php echo $trade_license_number; ?>" placeholder="" id="input-trade_license_number" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-license"><span data-toggle="tooltip" title="<?php echo $entry_license_info; ?>"><?php echo $entry_license; ?></span></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $license_placeholder_admin; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <?php if(isset($license_original_admin)){ ?><a target="_blank" href="<?php echo $license_original_admin ?>">Original</a><?php } ?>
                                    <input type="file" name="license"  value="" id="input-image-license" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-address_license_number"><?php echo $entry_address_license_number; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[legal_info][address_license_number]" value="<?php echo $address_license_number; ?>" placeholder="" id="input-address_license_number" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-legal_name"><?php echo $entry_legal_name; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[legal_info][legal_name]" value="<?php echo $legal_name; ?>" placeholder="" id="input-legal_name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-nationality"><?php echo $entry_nationality; ?></label>
                                <div class="col-sm-10">
                                  <div class="input-group">
                                      <span class="input-group-addon"><i>
                                              <?php foreach ($countries as $value_country) { ?>
                                                  <?php if ($value_country['country_id'] == $nationality) { ?>
                                                      <img src="./view/image/flags/<?php echo strtolower($value_country['iso_code_2']); ?>.png" id="country">
                                                  <?php } ?>
                                              <?php } ?>
                                          </i>
                                      </span>
                                      <select name="customer[legal_info][nationality]" id="input-legal-country" class="form-control">
                                          <option value=""></option>
                                          <?php if ($countries) { ?>
                                              <?php foreach ($countries as $value_country) { ?>
                                                  <?php if ($value_country['country_id'] == $nationality) { ?>
                                                      <option value="<?php echo $value_country['country_id']; ?>" data-val="<?php echo $value_country['iso_code_2']; ?>" selected><?php echo $value_country['name']; ?></option>
                                                  <?php } else { ?>
                                                      <option value="<?php echo $value_country['country_id']; ?>" data-val="<?php echo $value_country['iso_code_2']; ?>"><?php echo $value_country['name']; ?></option>
                                                  <?php } ?>
                                              <?php } ?>
                                          <?php } ?>
                                      </select>
                                  </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-passport_num"><?php echo $entry_passport_num; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[legal_info][passport_num]" value="<?php echo $passport_num; ?>" placeholder="" id="input-passport_num" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-passport"><span data-toggle="tooltip" title="<?php echo $entry_passport_info; ?>"><?php echo $entry_passport; ?></span></label>
                                <div class="col-sm-10">
                                    <a target="_blank" href="<?php echo isset($passport_placeholder_avatar) ? $passport_placeholder_avatar : 'javascript:void(0)'; ?>">
                                        <img src="<?php echo isset($passport_placeholder_admin) ? $passport_placeholder_admin : '#'; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                        <?php if(isset($passport_original_avatar)){ ?><a target="_blank" href="<?php echo $passport_original_avatar ?>">Original</a><?php } ?>
                                        <input type="file" name="passport"  value="" id="input-image-passport" >
                                    </a>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-legal_email"><?php echo $entry_legal_email; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[legal_info][email]" value="<?php echo $legal_email; ?>" placeholder="" id="input-legal_email" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-direct_officer_number"><?php echo $entry_direct_officer_number; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[legal_info][direct_officer_number]" value="<?php echo $direct_officer_number_telecode .' '.$direct_officer_number; ?>" placeholder="" id="input-direct_officer_number" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-legal_phone"><?php echo $entry_legal_phone; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[legal_info][telephone]" value="<?php echo $telephone_telecode.' '.$legal_phone; ?>" placeholder="" id="input-legal_phone" class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab-financial-info">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-company_officer_name"><?php echo $entry_company_officer_name; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[financial_info][company_officer_name]" value="<?php echo $company_officer_name; ?>" placeholder="" id="input-company_officer_name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-financial_email"><?php echo $entry_financial_email; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[financial_info][email]" value="<?php echo $financial_email; ?>" placeholder="" id="input-financial_email" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-financial_phone"><?php echo $entry_financial_phone; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[financial_info][telephone]" value="<?php echo $fin_telephone_telecode.' '.$financial_phone; ?>" placeholder="" id="input-financial_phone" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-beneficiary_name"><?php echo $entry_beneficiary_name; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[financial_info][beneficiary_name]" value="<?php echo $beneficiary_name; ?>" placeholder="" id="input-beneficiary_name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-bank_name"><?php echo $entry_bank_name; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[financial_info][bank_name]" value="<?php echo $bank_name; ?>" placeholder="" id="input-bank_name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-bank_account"><?php echo $entry_bank_account; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[financial_info][bank_account]" value="<?php echo $bank_account; ?>" placeholder="" id="input-bank_account" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-iban_code"><?php echo $entry_iban_code; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[financial_info][iban_code]" value="<?php echo $iban_code; ?>" placeholder="" id="input-iban_code" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-swift_code"><?php echo $entry_swift_code; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[financial_info][swift_code]" value="<?php echo $swift_code; ?>" placeholder="" id="input-swift_code" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-currency"><?php echo $entry_currency; ?></label>
                                <div class="col-sm-10">
                                    <select name="customer[financial_info][currency]" id="input-fin-country" class="form-control">
                                        <option value=""></option>
                                        <?php if ($currencies) { ?>
                                            <?php foreach ($currencies as $seller_currency) { ?>
                                                <?php if ($seller_currency['currency_id'] == $currency) { ?>
                                                    <option value="<?php echo $seller_currency['currency_id']; ?>"  selected><?php echo $seller_currency['title']."(".$seller_currency['code'].")"; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $seller_currency['currency_id']; ?>"><?php echo $seller_currency['title']."(".$seller_currency['code'].")"; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">

                                <label class="col-sm-2 control-label" for="input-currency"><?php echo $entry_eligible_vat; ?></label>
                                <div class="col-sm-10">
                                    <input type="checkbox" name="eligible_check" id="eligible_check" class="form-control" onclick="toggle('#vat_form',this)" <?php echo isset($eligible_check)&&$eligible_check==1?'checked':'' ?>/><br>
                                    <div class="col-sm-4"><label class="control-label" for="input-document"><?php echo $entry_eligible_upload; ?></label></div>
                                    <div class="col-sm-6">
                                        <img src="<?php echo (isset($document_path_placeholder_profile)?$document_path_placeholder_profile : ''); ?>" alt="" width="100" height="100" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                        <?php if(isset($document_path_original_profile)){ ?><a target="_blank" href="<?php echo $document_path_original_profile ?>">Original</a><?php }?>
                                        <input type="file" name="document" value="" placeholder="" id="input-document" class="form-control" />
                                    </div>
                                    <div id="vat_form" style="display: none;">

                                        <div class="col-sm-4 required"><label class="control-label" for="input-doc-company-name"><?php echo $entry_company_name; ?></label></div>
                                        <div class="col-sm-6">
                                            <input type="text" name="doc_company_name" value="<?php echo isset($doc_company_name)?$doc_company_name:'' ?>" placeholder="" id="input-company-name" class="form-control" />
                                            <?php if (isset($error_companyname_required) && $error_companyname_required != '') { ?>
                                            <div class="text-danger"><?php echo $error_companyname_required; ?></div>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-4 required"><label class="control-label" for="input-business-address"><?php echo $entry_eligible_business_address; ?></label></div>
                                        <div class="col-sm-6">
                                            <input type="text" name="business_address" value="<?php echo isset($business_address)?$business_address:'' ?>" placeholder="" id="input-business-address" class="form-control" />
                                            <?php if (isset($error_business_address_required) && $error_business_address_required != '') { ?>
                                            <div class="text-danger"><?php echo $error_business_address_required; ?></div>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-4 required"><label class="control-label" for="input-tax-reg-num"><?php echo $entry_eligible_tax_reg_num; ?></label></div>
                                        <div class="col-sm-6">
                                            <input type="text" name="tax_reg_num" value="<?php echo isset($tax_reg_num)?$tax_reg_num:'' ?>" placeholder="" id="input-tax-reg-num" class="form-control" />
                                            <?php if (isset($error_tax_reg_num_required) && $error_tax_reg_num_required != '') { ?>
                                            <div class="text-danger"><?php echo $error_tax_reg_num_required; ?></div>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-4 required"><label class="control-label" for="input-issue-date"><?php echo $entry_eligible_issue_date; ?></label></div>
                                        <div class="col-sm-6">
                                            <!--<input type="text" name="issue_date" value="<?php echo isset($issue_date)?$issue_date:'' ?>" placeholder="" id="input-issue-date" class="form-control" />-->
                                            <div class="input-group date">
                                                <input type="text" name="issue_date" value="<?php echo isset($issue_date)&&$issue_date!='0000-00-00'?$issue_date:'' ?>" data-date-format="YYYY-MM-DD" id="input-issue-date" class="form-control" />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>

                                            <?php if (isset($error_issue_date_required) && $error_issue_date_required != '') { ?>
                                            <div class="text-danger"><?php echo $error_issue_date_required; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- tab location -->
                        <div class="tab-pane" id="tab-location">

                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>

</div>
    <script>
<?php foreach ($languages as $language) { ?>
            // $("#input-description<?php echo $language['language_id']; ?>").summernote();
            CKEDITOR.replace("input-shortprofile[<?php echo $language['language_id']; ?>]", {
                filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
                filebrowserWindowWidth: 800,
                filebrowserWindowHeight: 500
            });
<?php } ?>
        $('#language a:first').tab('show');
    </script>

    <script type="text/javascript"><!--

        $(document).ready(function(){

          $('.date').datetimepicker({
              pickTime: false
          });

          $("#is_fbs").click(function(){
            if($(this).is(":checked")){
              $(".fbs").each(function(){
                $(this).removeClass("hide");
              });
              $(".not-fbs").each(function(){
                $(this).addClass("hide");
              });
            }else{
              $(".fbs").each(function(){
                $(this).addClass("hide");
              });
              $(".not-fbs").each(function(){
                $(this).removeClass("hide");
              });
            }
          });

          $("#input-available_countries").change(function(){
            if($(this).val() == '0'){
              $("#excluded-countries").show();
            }else{
              $("#excluded-countries").hide();
            }
          });

        if ($("#eligible_check").is(":checked")) {
            $("#vat_form").show();
        }

        });

        function toggle(eid, obj) {
            var $input = $(obj);
            if ($input.prop('checked')) $(eid).show();
            else $(eid).hide();
        }

        $('#tab-dashboard').load("index.php?route=customerpartner/dashboard&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>", function () {
            $('.remove-me').remove();
        });

        $("#input-show-in-ksa").change(function () {
            $("#input-show-in-ksa-hidden").val("0");
            if ($(this).is(":checked")) {
                $("#input-show-in-ksa-hidden").val("1");
            }
        });

        $("#input-show-in-uae").change(function () {
            $("#input-show-in-uae-hidden").val("0");
            if ($(this).is(":checked")) {
                $("#input-show-in-uae-hidden").val("1");
            }
        });

        // Product
        $('input[name=\'product\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request) + '&filter_for_seller=1',
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'product\']').val('');

                $('#product-product' + item['value']).remove();

                $('#product-product').append('<div id="product-id' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_ids[]" value="' + item['value'] + '" /></div>');
            }
        });

        $('#product-product').delegate('.fa-minus-circle', 'click', function () {
            $(this).parent().remove();
        });


        $('#transaction').delegate('.pagination a', 'click', function (e) {
            $('#transaction').load(this.href);

            return false;
        });

        $('#transaction').load('index.php?route=customerpartner/partner/transaction&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

        $('#button-transaction').on('click', function () {
            $.ajax({
                url: 'index.php?route=customerpartner/partner/transaction&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>',
                type: 'post',
                dataType: 'html',
                data: 'description=' + encodeURIComponent($('#tab-transaction input[name=\'description\']').val()) + '&amount=' + encodeURIComponent($('#tab-transaction input[name=\'amount\']').val()),
                beforeSend: function () {
                    $('#button-transaction').button('loading');
                },
                complete: function () {
                    $('#button-transaction').button('reset');
                },
                success: function (html) {
                    $('#tab-transaction .alert').remove();
                    $('#transaction').html(html);
                    $('#tab-transaction input[name=\'amount\']').val('');
                    $('#tab-transaction input[name=\'description\']').val('');
                }
            });
        });

        $("#input-country").change(function () {
            getZones($("#input-country option:selected").val());
            var newSrc = "./view/image/flags/" + $("#input-country option:selected").attr("data-val") + ".png";
            $('#country').attr('src', newSrc.toLowerCase());

        });

        localocation = false;
        $('a[href=\'#tab-location\']').on('click', function () {
            if (!localocation) {
                $(this).append(' <i class="fa fa-spinner fa-spin remove-me"></i>');

                $.ajax({
                    url: '<?php echo $loadLocation; ?>',
                    dataType: 'html',
                    success: function (response) {
                        $('#tab-location').html(response);
                        $('.remove-me').remove();
                    }
                });
                localocation = true;
            }
        })

        //--></script>
    <link href="view/javascript/color-picker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <script src="view/javascript/color-picker/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.colorpicker').colorpicker();
        });
    </script>

    <script>
        function getZones(country_id) {
            country_id = typeof country_id !== 'undefined' ? country_id : '';
            if (country_id != '') {
                $.ajax({
                    url: 'index.php?route=customerpartner/partner/getZone&token=<?php echo $token; ?>&country_id=' + country_id,
                    dataType: 'json'
                })
                        .success(function (data) {
                            $('#input-zone-id').find('option').remove().end();
                            $('#input-zone-id').append($("<option></option>").attr("value", "").text("<?php echo $text_select; ?>"));
                            $.each(data, function (key, value) {
                                $('#input-zone-id').append($("<option></option>").attr("value", value.zone_id).text(value.name));
                            });
                        })
            }
        }


    </script>


    <?php echo $footer; ?>
