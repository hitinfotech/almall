<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
            <div class="pull-right">
                <button id="export-btn" data-toggle="tooltip" title="<?php echo $button_export; ?>" class="btn btn-success"><i class="fa fa-file-excel-o"></i></button>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id='resultsTable'>
                            <thead>
                                <tr>
                                  <td class="text-left"><?php echo $column_rma_id; ?></td>
                                    <td class="text-left"><?php if ($sort == 'o.order_id') { ?>
                                            <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_id; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_order; ?>"><?php echo $column_order_id; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'customer') { ?>
                                            <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                                        <?php } ?></td>
                                        <td class="text-left"><?php echo 'Country'; ?></td>
                                        <td class="text-left"><?php echo 'Seller'; ?></td>
                                        <td class="text-left"><?php echo 'Country'; ?></td>

                                    <td class="text-left"><?php echo $column_book_awbno; ?></td>
                                    <?php if ($return_case!= 1 ) {?>
                                      <td class="text-right"><?php echo "Dilevery AWBNo"; ?></td>
                                    <?php } ?>
                                    <td class="text-left"><?php if ($sort == 'status') { ?>
                                        <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                    <?php } else { ?>
                                        <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                    <?php } ?></td>
                                    <td class="text-right"><?php if ($sort == 'o.total') { ?>
                                        <a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
                                    <?php } else { ?>
                                        <a href="<?php echo $sort_total; ?>"><?php echo $column_total; ?></a>
                                    <?php } ?></td>
                                    <td class="text-left"><?php echo $column_currency; ?></td>
                                    <td class="text-left"><?php if ($sort == 'o.date_added') { ?>
                                            <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                                        <?php } ?></td>
                                        <?php if ($return_case ==2 ) {?>
                                        <td class="text-left noExl"><?php echo $column_action; ?></td>
                                        <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($orders) {  ?>
                                    <?php foreach ($orders as $order) { ?>
                                        <tr>
                                            <td class="text-right"><?php echo $order['return_id']; ?></td>
                                            <td class="text-right"><?php echo $order['order_id']; ?></td>
                                            <td class="text-left"><?php echo $order['customer']; ?></td>
                                            <td class="text-left"><?php echo $order['shipping_country']; ?></td>
                                            <td class="text-left"><?php echo $order['seller_name']; ?></td>
                                            <td class="text-left"><?php echo $order['seller_country']; ?></td>
                                            <td class="text-right"><?php echo $order['return_awbno']; ?></td>
                                            <?php if ($return_case != 1 ) {?>
                                              <td class="text-right"><?php echo $order['ship_awbno']; ?></td>
                                            <?php } ?>
                                            <td class="text-left"><?php echo $order['status']; ?></td>
                                            <td class="text-right"><?php echo $order['total']; ?></td>
                                            <td class="text-left"><?php echo $order['currency_code']; ?></td>
                                            <td class="text-left"><?php echo $order['date_added']; ?></td>
                                            <?php if ($return_case ==2 ) {?>
                                            <td class="text-left noExl">
                                                <a href="<?php echo $order['action']; ?>" data-toggle="tooltip" class="btn btn-primary noExl"><i class="fa fa-eye"></i></a>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="12"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"><!--
  $('#button-filter').on('click', function () {
            url = 'index.php?route=customerpartner/order&token=<?php echo $token; ?>';

            var filter_order_id = $('input[name=\'filter_order_id\']').val();

            if (filter_order_id) {
                url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
            }

            var filter_customer = $('input[name=\'filter_customer\']').val();

            if (filter_customer) {
                url += '&filter_customer=' + encodeURIComponent(filter_customer);
            }

            var filter_seller = $('input[name=\'filter_seller\']').val();

            if (filter_seller) {
                url += '&filter_seller=' + encodeURIComponent(filter_seller);
            }

            var filter_seller_email = $('input[name=\'filter_seller_email\']').val();

            if (filter_seller_email) {
                url += '&filter_seller_email=' + encodeURIComponent(filter_seller_email);
            }

            var filter_seller_number = $('input[name=\'filter_seller_number\']').val();

            if (filter_seller_number) {
                url += '&filter_seller_number=' + encodeURIComponent(filter_seller_number);
            }

            var filter_order_status = $('select[name=\'filter_order_status\']').val();

            if (filter_order_status != '*') {
                url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
            }

            var filter_total = $('input[name=\'filter_total\']').val();

            if (filter_total) {
                url += '&filter_total=' + encodeURIComponent(filter_total);
            }

            var filter_date_added = $('input[name=\'filter_date_added\']').val();

            if (filter_date_added) {
                url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
            }

            var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

            if (filter_date_modified) {
                url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
            }

            var filter_seller_country = $('select[name=\'filter_seller_country\']').val();

            if (filter_seller_country && filter_seller_country != 0) {
                url += '&filter_seller_country=' + encodeURIComponent(filter_seller_country);
            }

            var filter_payment_country = $('select[name=\'filter_payment_country\']').val();

            if (filter_payment_country && filter_payment_country != 0) {
                url += '&filter_payment_country=' + encodeURIComponent(filter_payment_country);
            }

            var filter_shipping_country = $('select[name=\'filter_shipping_country\']').val();

            if (filter_shipping_country && filter_shipping_country != 0) {
                url += '&filter_shipping_country=' + encodeURIComponent(filter_shipping_country);
            }

            location = url;
        });
        //--></script>
    <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
    <script type="text/javascript"><!--
    $('.date').datetimepicker({
            pickTime: false
        });

        // Seller
        $('input[name=\'filter_seller\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=customerpartner/partner/autocompleteForProducts&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        json.unshift({
                            seller_id: 0,
                            company: '<?php echo "none"; ?>'
                        });

                        response($.map(json, function (item) {
                            return {
                                label: item['company'],
                                value: item['seller_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'filter_seller\']').val(item['label']);
            }
        });

        $('#export-btn').on('click', function(e){
            e.preventDefault();
            ResultsToTable();
        });

        function ResultsToTable(){

            var x = $("#resultsTable").clone();
                  $(x).find("tr td a").replaceWith(function(){
                  return $.text([this]);
              });
              $(x).table2excel({
                    exclude: ".noExl",
                    filename: "Shipping Orders",
                    name: "Results",
                    exclude_img: true,
                    exclude_links: true,
                    exclude_inputs: true,
                    columns : [0,1,2,3,4,5,6,7,8]
                });
        }

        //--></script></div>
<?php echo $footer; ?>
