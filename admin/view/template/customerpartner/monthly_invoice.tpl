<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
        <script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
        <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
        <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
    </head>
    <style>
        .content-padding {
            padding-bottom: 110px;
            padding-top: 20px;
        }
        .top-buffer {
            margin-top:20px;
        }
    </style>
    <body>
        <div class="container">
            <div class="content-padding">
                <div style="page-break-after: always;">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <img src="<?php echo $logo ?>" alt="logo" />
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <br><br><br>
                                    <div class='col-md-6 col-sm-6 col-xs-6'>
                                        <p style="font-size:12px;"><strong>From : <?php echo $from_date ?></strong></p>
                                        <p style="font-size:12px;"><strong>To   : <?php echo $to_date ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p style="font-size:20px; margin-top: 50px;"><?php echo 'Sayidaty Monthly Report' ?></p>

                    <?php foreach ($orders as $country_sellers_key => $country_sellers) { ?>
                        <p style="font-size:20px; margin-top: 50px;"><?php echo 'Sayidaty Monthly Report FOR ' . $country_sellers_key ?></p>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td class="text-left"><b><?php echo 'Seller #'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Name' ?></b></td>
                                    <td class="text-left"><b><?php echo 'Country' ?></b></td>
                                    <td class="text-left"><b><?php echo 'Order Id'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Completed Orders'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Order total'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'SPRC %'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Total SPRC %'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Transaction Fees'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Storage Fees'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Fullfillment Fees'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Total SPRC fees By Order'; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Total Seller '.$country_sellers_key; ?></b></td>
                                    <td class="text-left"><b><?php echo 'Total Seller VAT' ?></b></td>
                                    <td class="text-left"><b><?php echo 'Total Seller (SAR)' ?></b></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                $completed_order=0;
                                $total_sales=0;
                                $commission =0;
                                $total_commission =0;
                                $transaction_fees =0;
                                $total_transaction_fees =0;
                                $total_fbs_storage_charges = 0;
                                $total_fbs_fullfillment_charges = 0;
                                $total_seller =0;
                                $converted_total_sales = 0;
                                $total_vat_value = 0;

                                foreach ($country_sellers as $seller) {
                                $completed_order+=$seller['completed_order'];
                                $total_sales+=$seller['total_sales'];
                                $commission +=$seller['commission'];
                                $total_commission +=$seller['total_commission'];
                                $transaction_fees +=$seller['transaction_fees'];
                                $total_fbs_storage_charges +=$seller['fbs_storage_charges'];
                                $total_fbs_fullfillment_charges +=$seller['fbs_fullfillment_charges'] * $seller['completed_order'];
                                $total_transaction_fees +=round($seller['fbs_fullfillment_charges'],2)  * $seller['completed_order'] + round($seller['fbs_storage_charges'],2) + $seller['transaction_fees'] +$seller['total_commission'];
                                $total_seller +=round($seller['total_seller'],2) - (round($seller['fbs_fullfillment_charges'],2)  * $seller['completed_order'] + round($seller['fbs_storage_charges'],2));
                                $converted_total_sales +=round($seller['converted_total_sales'],2) - (round($seller['fbs_fullfillment_charges'],2)  * $seller['completed_order'] + round($seller['fbs_storage_charges'],2));
                                $total_vat_value += round($seller['total_vat_value'],2);
                                $seller['orders_ids'] = str_replace(',','<br>',$seller['orders_ids']);

?>

                                    <tr>
                                        <td class="text-left"><?php echo $seller['seller_id']; ?></td>
                                        <td class="text-left"><?php echo $seller['name'] ?></td>
                                        <td class="text-left"><?php echo $seller['country']; ?></td>
                                        <td class="text-left"><?php echo $seller['orders_ids'] ?></td>
                                        <td class="text-left"><?php echo $seller['completed_order'] ?></td>
                                        <td class="text-left"><?php echo round($seller['total_sales'],2); ?></td>
                                        <td class="text-left"><?php echo $seller['commission']; ?></td>
                                        <td class="text-left"><?php echo round($seller['total_commission'],2) ?></td>
                                        <td class="text-left"><?php echo round($seller['transaction_fees'],2); ?></td>
                                        <td class="text-left"><?php echo round($seller['fbs_storage_charges'],2); ?></td>
                                        <td class="text-left"><?php echo round($seller['fbs_fullfillment_charges'],2)  * $seller['completed_order']; ?></td>
                                        <td class="text-left"><?php echo round($seller['fbs_fullfillment_charges']  * $seller['completed_order'] + $seller['fbs_storage_charges'] + $seller['transaction_fees'] +$seller['total_commission'],2) ; ?></td>
                                        <td class="text-left"><?php echo round($seller['total_seller'],2) - (round($seller['fbs_fullfillment_charges'],2)  * $seller['completed_order'] + round($seller['fbs_storage_charges'],2)); ?></td>
                                        <td class="text-left"><?php echo round($seller['total_vat_value'],2) ?></td>
                                        <td class="text-left"><?php echo round($seller['converted_total_sales'],2) - (round($seller['fbs_fullfillment_charges'],2)  * $seller['completed_order'] + round($seller['fbs_storage_charges'],2)); ?></td>

                                    </tr>
                                    <?php } ?>
                                <tr>
                                    <td class="text-left" colspan="4" class=""><b>Totals :</b></td>
                                    <td class="text-left"><b><?php echo $completed_order; ?></b></td>
                                    <td class="text-left"><b><?php echo round($total_sales,2); ?></b></td>
                                    <td class="text-left"><b><?php echo $commission; ?></b></td>
                                    <td class="text-left"><b><?php echo round($total_commission,2) ?></b></td>
                                    <td class="text-left"><b><?php echo round($transaction_fees,2); ?></b></td>
                                    <td class="text-left"><b><?php echo round($total_fbs_storage_charges,2); ?></b></td>
                                    <td class="text-left"><b><?php echo round($total_fbs_fullfillment_charges,2); ?></b></td>
                                    <td class="text-left"><b><?php echo round($total_transaction_fees,2); ?></b></td>
                                    <td class="text-left"><b><?php echo round($total_seller,2); ?></b></td>
                                    <td class="text-left"><b><?php echo round($total_vat_value,2); ?></b></td>
                                    <td class="text-left"><b><?php echo round($converted_total_sales,2); ?></b></td>
                                </tr>



                            </tbody>
                        </table>
                    <?php } ?>
                    <div class='col-md-12 col-sm-12 col-xs-12'>
                        <br/><br/>
                        <p style="font-size:15px;"><?php echo $text_more_details ?></p>
                    </div>

                </div>
            </div>
    </body>
</html>
