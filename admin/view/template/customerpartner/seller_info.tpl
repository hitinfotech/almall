<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>

                                    <td class="text-left">
                                       <?php echo $column_order_id; ?>
                                    </td>

                                    <td class="text-right">
                                       <?php echo $column_customer; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $column_status; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $column_total; ?>
                                    </td>
                                    <td class="text-left">
                                       <?php echo $column_date_added; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $column_date_modified; ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($orders)) {
                                    ?>
                                    <?php foreach ($orders as $order) { ?>
                                        <tr>
                                            <td class="text-left"><?php echo $order['order_id']; ?></td>
                                            <td class="text-left"><?php echo $order['customer']; ?></td>
                                            <td class="text-right"><?php echo $order['status']; ?></td>
                                            <td class="text-right"><?php echo $order['total']; ?></td>
                                            <td class="text-right"><?php echo $order['date_added']; ?></td>
                                            <td class="text-right"><?php echo $order['date_modified']; ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
    <script type="text/javascript"><!--
  $('#button-filter').on('click', function () {
            url = 'index.php?route=customerpartner/order&token=<?php echo $token; ?>';

            var filter_order_id = $('input[name=\'filter_order_id\']').val();

            if (filter_order_id) {
                url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
            }

            var filter_customer = $('input[name=\'filter_customer\']').val();

            if (filter_customer) {
                url += '&filter_customer=' + encodeURIComponent(filter_customer);
            }

            var filter_seller = $('input[name=\'filter_seller\']').val();

            if (filter_seller) {
                url += '&filter_seller=' + encodeURIComponent(filter_seller);
            }

            var filter_order_status = $('select[name=\'filter_order_status\']').val();

            if (filter_order_status != '*') {
                url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
            }

            var filter_total = $('input[name=\'filter_total\']').val();

            if (filter_total) {
                url += '&filter_total=' + encodeURIComponent(filter_total);
            }

            var filter_date_added = $('input[name=\'filter_date_added\']').val();

            if (filter_date_added) {
                url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
            }

            var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

            if (filter_date_modified) {
                url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
            }

            location = url;
        });
        //--></script>
    <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
    <script type="text/javascript"><!--
    $('.date').datetimepicker({
            pickTime: false
        });
        //--></script></div>
<?php echo $footer; ?>
