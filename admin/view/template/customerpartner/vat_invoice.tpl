<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
    <meta charset="UTF-8" />
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
    <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
    <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>
<style>
    .content-padding {
        padding-bottom: 110px;
        padding-top: 20px;
    }
    .top-buffer {
        margin-top:20px;
    }
</style>
<body>
    <div class="container">
        <div class="content-padding">
            <div style="page-break-after: always;">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <img src="<?php echo $logo ?>" alt="logo" />
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="row">
                            <p style="font-size:16px;"><strong>Invoice Number</strong> : <?php echo $invoice_name; ?></p>
                            <p style="font-size:16px;"><strong>Invoice Period</strong> : <?php echo $invoice_period; ?></p>
                            <p style="font-size:14px;"><strong>Invoice Date</strong> : <?php echo $invoice_date; ?></p>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p style="font-size:16px;"><strong>Seller</strong></p>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <p style="font-size:14px;"><strong>Company Name:</strong> <?php echo $company; ?></p>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <p style="font-size:14px;"><?php echo "ID# ".$seller_id; ?></p>
                                        </div>
                                    </div>
                                    <p style="font-size:14px;"><strong>Address:</strong> <?php echo $sellect_reg_address; ?></p>
                                    <p style="font-size:14px;"><strong>Country:</strong> <?php echo $country; ?></p>
                                    <p style="font-size:14px;"><strong>Email:</strong> <?php echo $email; ?></p>
                                    <p style="font-size:14px;"><strong>TRN#:</strong> <?php echo $trn; ?></p>
                                    <p style="font-size:14px;"><?php echo $bank; ?></p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p style="font-size:16px;"><strong>Buyer</strong></p>
                                    <p style="font-size:14px;"><strong>Company Name:</strong> <?php echo $buyer_name; ?></p>
                                    <p style="font-size:14px;"><strong>Address:</strong> <?php echo $buyer_address; ?></p>
                                    <p style="font-size:14px;"><strong>Country:</strong> <?php echo $country; ?></p>
                                    <p style="font-size:14px;"><strong>TRN#:</strong> <?php echo $buyer_trn; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td class="text-left"><b>PO Number</b></td>
                                    <td><b>Description</b></td>
                                    <td class="text-left"><b>Supply Date</b></td>
                                    <td class="text-left"><b>Processed Date </b></td>
                                    <td class="text-left"><b>Quantity</b></td>
                                    <td class="text-left"><b>Unit Retail Price</b></td>
                                    <td class="text-left"><b>Amount</b></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($orders_info as $order) { ?>
                                    <tr>
                                        <td class="text-right"><?php echo $order['order_id']; ?></td>
                                        <td class="text-left"><?php echo $order['desc']; ?></td>
                                        <td class="text-left"><?php echo $order['date_pending']; ?></td>
                                        <td class="text-left"><?php echo $order['date_completed']; ?></td>
                                        <td class="text-left"><?php echo $order['quantity']; ?></td>
                                        <td class="text-left"><?php echo $seller_currency.' '.$order['unit_price']; ?></td>
                                        <td class="text-left"><?php echo $seller_currency.' '.$order['amount']; ?></td>
                                    </tr>

                                <?php } ?>
                                <tr class="border_bottom">
                                    <td class="text-left noborder" style="width: 20%">Total PO</td>
                                    <td class="text-left noborder" style="width: 20%"><?php echo $total_po; ?></td>
                                    <td class="text-left noborder"></td>
                                    <td class="text-left noborder"><strong>Subtotal</strong></td>
                                    <td class="noborder"></td>
                                    <td class="noborder"></td>
                                    <td class="text-right noborder"><?php echo $seller_currency.' '.$sub_total; ?></td>
                                </tr>
                                <tr class="border_bottom">
                                    <td class="text-left noborder">Total PO with VAT</td>
                                    <td class="text-left noborder"><?php echo $total_po_with_vat; ?></td>
                                    <td class="text-left noborder"></td>
                                    <td class="text-left noborder"><strong>Discount%</strong></td>
                                    <td class="noborder"><?php echo $discount_percent; ?></td>
                                    <td class="noborder"></td>
                                    <td class="text-right noborder">- <?php echo $discount; ?></td>
                                </tr>
                                <tr class="border_bottom">
                                    <td class="text-left noborder">Warehousing Units</td>
                                    <td class="text-left noborder"><?php echo $warehousing_units; ?></td>
                                    <td class="text-left noborder"></td>
                                    <td class="text-left noborder"><strong>Processing Fees</strong></td>
                                    <td class="noborder"><?php echo $processing_fees_amount; ?></td>
                                    <td class="noborder"></td>
                                    <td class="text-right noborder"><?php if ($processing_fees>0) echo '-'; ?><?php echo $processing_fees; ?></td>
                                </tr>
                                <tr class="border_bottom">
                                    <td class="text-left noborder"></td>
                                    <td class="text-left noborder"></td>
                                    <td class="text-left noborder"></td>
                                    <td class="text-left noborder"><strong>Warehousing Fees</strong></td>
                                    <td class="noborder"><?php echo $warehousing_amount; ?></td>
                                    <td class="noborder"></td>
                                    <td class="text-right noborder"><?php if ($warehousing_fees>0) echo '-'; ?><?php echo $warehousing_fees; ?></td>
                                </tr>
                                <tr class="border_bottom">
                                    <td class="text-left noborder" colspan="3">Deduction on Fullfilment/order and Storage by Sayidaty Mall</td>
                                    <td class="text-left noborder"><strong>Handling fee / Order</strong></td>
                                    <td class="noborder"><?php echo $handling_fee_amount; ?></td>
                                    <td class="noborder"></td>
                                    <td class="text-right noborder"><?php if ($warehousing_fees>0) echo '-'; ?><?php echo $handling_fee; ?></td>
                                </tr>
                                <tr class="border_bottom">
                                    <td class="text-center noborder" rowspan="5" colspan="3">
                                        <strong>Terms and Conditions:</strong><br>
                                        (1) Payment to be made in the name of company. (2) Any claims after 2 days of delivery shall not be acceptable.<br>
                                    </td>

                                </tr>
                                <tr class="border_bottom">
                                    <td class="text-left noborder"><strong>Total Deductions</strong></td>
                                    <td class="noborder"></td>
                                    <td class="noborder"></td>
                                    <td class="text-right noborder"><?php echo $total_deductions; ?></td>
                                </tr>
                                <tr class="border_bottom">
                                    <td class="text-left noborder"><strong>Total Amount (Gross)</strong></td>
                                    <td class="noborder"></td>
                                    <td class="noborder"></td>
                                    <td class="text-right noborder"><?php echo $seller_currency.' '.$gross; ?></td>
                                </tr>
                                <tr class="border_bottom">
                                    <td class="text-left noborder"><strong>VAT</strong></td>
                                    <td class="noborder">5%</td>
                                    <td class="noborder"></td>
                                    <td class="text-right noborder"><?php echo $seller_currency.' '.$total_vat_value; ?></td>
                                </tr>
                                <tr class="border_bottom">
                                    <td class="text-left noborder"><strong>Total Amount (Net)</strong></td>
                                    <td class="noborder"></td>
                                    <td class="noborder"></td>
                                    <td class="text-right noborder"><?php echo $seller_currency.' '.$total; ?></td>
                                </tr>
                                <tr>
                                    <td style="border: none!important;"  colspan="6"></td>
                                </tr>
                                <tr>
                                    <td style="border: none!important;" colspan="6"></td>
                                </tr>
                                <tr>
                                    <td style="border: none!important;" colspan="6"><b>Provided by Sayidaty Mall</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</body>
<style>
    .noborder {
        border-right: none!important;
        border-left: none!important;
    }
</style>