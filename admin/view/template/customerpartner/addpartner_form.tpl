<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>

            <div class="panel-body">

                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
                    <ul class="nav nav-tabs" id='first-tabs'>
                        <li><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab-profile" data-toggle="tab"><?php echo $tab_info; ?></a></li>
                        <li><a href="#tab-product" data-toggle="tab"><?php echo $tab_product; ?></a></li>
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane " id="tab-general">
                            <!-- { nav-tabs } !-->
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                    <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img
                                                src="./view/image/flags/<?php echo $language['image']; ?>"
                                                title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?>
                                        </a></li>
                                <?php } ?>
                            </ul>
                            <!-- { / nav-tabs } !-->

                            <!-- { tab-content } !-->
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">


                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-screenname[<?php echo $language['language_id']; ?>]"><span data-toggle="tooltip" title="<?php echo $entry_screenname_info; ?>"><?php echo $entry_screenname; ?></span></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="description[<?php echo $language['language_id']; ?>][screenname]" value="<?php echo isset($description[$language['language_id']]['screenname']) ? $description[$language['language_id']]['screenname'] : ''; ?>" placeholder="" id="input-screenname[<?php echo $language['language_id']; ?>]" class="form-control" />
                                                <?php if (isset($error_screenname_required) && $error_screenname_required != '') { ?>
                                                    <div class="text-danger"><?php echo $error_screenname_required; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-shortprofile[<?php echo $language['language_id']; ?>]"><?php echo $entry_profile; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="description[<?php echo $language['language_id']; ?>][shortprofile]" id="input-shortprofile[<?php echo $language['language_id']; ?>]" class="form-control"><?php echo isset($description[$language['language_id']]['shortprofile']) ? $description[$language['language_id']]['shortprofile'] : ''; ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-companyname[<?php echo $language['language_id']; ?>]"><?php echo $entry_company; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="description[<?php echo $language['language_id']; ?>][companyname]" value="<?php echo isset($description[$language['language_id']]['companyname']) ? $description[$language['language_id']]['companyname'] : ''; ?>" placeholder="" id="input-companyname[<?php echo $language['language_id']; ?>]" class="form-control" />
                                                <?php if (isset($error_companyname_required) && $error_companyname_required != '') { ?>
                                                    <div class="text-danger"><?php echo $error_companyname_required; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>
                            <!-- { / tab-content } !-->
                        </div>

                        <div class="tab-pane " id="tab-profile">

                            <div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> <?php echo $tab_profile_info; ?>
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-image"><span data-toggle="tooltip" title="<?php echo $entry_avatar_info; ?>"><?php echo $entry_avatar; ?></span></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $avatar_placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="avatar" value="" id="input-image" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[firstname]" value="<?php echo $firstname; ?>"  placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[lastname]" value="<?php echo $lastname; ?>"  placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[email]" value="<?php echo $email; ?>"  placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-phone"><?php echo $entry_phone; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[telephone]" value="<?php echo $telephone; ?>"  placeholder="<?php echo $entry_phone; ?>" id="input-phone" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-is-return"><?php echo $entry_return_policy; ?></label>
                                <div class="col-sm-10">
                                    <select name="customer[return_policy_id]" id="input-is-return" class="form-control">
                                        <option value="0" selected="selected"><?php echo 'Default Return Prolicy'; ?></option>
                                        <?php foreach ($returnpolicies as $policy) { ?>
                                            <?php if ($policy['return_policy_id'] == $return_policy_id) { ?>
                                                <option value="<?php echo $policy['return_policy_id'] ?>" selected="selected"><?php echo $policy['name']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $policy['return_policy_id'] ?>"><?php echo $policy['name']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-gender"><?php echo $entry_gender; ?></label>
                                <div class="col-sm-10">
                                    <select name="customer[gender]" class="form-control">
                                        <option> </option>
                                        <option value="M" <?php if ($gender == 'M') echo 'selected'; ?> > Male </option>
                                        <option value="F" <?php if ($gender == 'F') echo 'selected'; ?>> Female </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-clienturl"><?php echo $entry_SEO_url; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[clienturl]"  value="<?php echo $seo_url ?>" placeholder="client's url" id="input-clienturl" class="form-control" />
                                    <?php if (isset($error_seo_required) && $error_seo_required != '') { ?>
                                        <div class="text-danger"><?php echo $error_seo_required; ?></div>
                                    <?php } else if (isset($error_unique) && $error_unique != '') { ?>
                                        <div class="text-danger"><?php echo $error_unique; ?></div>
                                    <?php } else if (isset($error_wrong_syntax) && $error_wrong_syntax != '') { ?>
                                        <div class="text-danger"><?php echo $error_wrong_syntax; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-twitterid"><?php echo $entry_twitter; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[twitterid]" value="<?php echo $twitterid; ?>" placeholder="" id="input-twitterid" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-facebookid"><?php echo $entry_facebook; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[facebookid]" value="<?php echo $facebookid; ?>" placeholder="" id="input-facebookid" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-companylocality"><?php echo $entry_locality; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[companylocality]" value="<?php echo $companylocality; ?>" placeholder="" id="input-companylocality" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i>
                                                <?php foreach ($countries as $value_country) { ?>
                                                    <?php if ($value_country['country_id'] == $country) { ?>
                                                        <img src="./view/image/flags/<?php echo strtolower($value_country['iso_code_2']); ?>.png" id="country">
                                                    <?php } ?>
                                                <?php } ?>
                                            </i></span>
                                        <select name="customer[country_id]" id="input-country" class="form-control">
                                            <option value=""></option>
                                            <?php if ($countries) { ?>
                                                <?php foreach ($countries as $value_country) { ?>
                                                    <?php if ($value_country['country_id'] == $country) { ?>
                                                        <option value="<?php echo $value_country['country_id']; ?>" data-val="<?php echo $value_country['iso_code_2']; ?>" selected><?php echo $value_country['name']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $value_country['country_id']; ?>" data-val="<?php echo $value_country['iso_code_2']; ?>"><?php echo $value_country['name']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php if (isset($error_country_required) && $error_country_required != '') { ?>
                                        <div class="text-danger"><?php echo $error_country_required; ?></div>
                                    <?php } ?>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-zone-id"><?php echo $entry_zone; ?></label>
                                <div class="col-sm-10">
                                    <select name="customer[zone_id]" id="input-zone-id" class="form-control">
                                        <option value=""></option>
                                        <?php if (isset($zones) && $zones != '[]') { ?>
                                            <?php foreach ($zones as $value_zone) { ?>
                                                <?php if ($value_zone['zone_id'] == $zone) { ?>
                                                    <option value="<?php echo $value_zone['zone_id']; ?>" selected><?php echo $value_zone['name']; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $value_zone['zone_id']; ?>" ><?php echo $value_zone['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[city]" value="<?php echo $city; ?>" placeholder="" id="input-city" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-address"><?php echo $entry_address; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[address]" value="<?php echo $address; ?>" placeholder="" id="input-address" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-open"><span data-toggle="tooltip" ><?php echo $entry_open; ?></span></label>
                                <div class="col-sm-10">
                                    <textarea name="customer[opening_time]" rows="5" placeholder="<?php echo $entry_open; ?>" id="input-open" class="form-control"><?php echo $opening_time; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-backgroundcolor"><span data-toggle="tooltip" title="<?php echo $entry_theme; ?>"><?php echo $entry_theme; ?></span></label>
                                <div class="col-sm-10">
                                    <div class="input-group colorpicker">
                                        <span class="input-group-addon"><i></i></span>
                                        <input type="text" name="customer[backgroundcolor]" value="<?php echo $backgroundcolor; ?>" placeholder="" id="input-backgroundcolor" class="form-control" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-image"><span data-toggle="tooltip" title="<?php echo $entry_banner_info; ?>"><?php echo $entry_banner; ?></span></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $companybanner_placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="companybanner" value="" id="input-image-companybanner" />
                                </div>
                            </div>
                        </div>

                        <!-- product -->
                        <div class="tab-pane" id="tab-product">

                            <div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> <?php echo $tab_product_info; ?>
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-commission"><?php echo $entry_commission; ?></label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <input type="text" name="customer[commission]" value="<?php echo $commission; ?>" placeholder="<?php echo $entry_commission; ?>" id="input-commission" class="form-control" />
                                        <span class="input-group-addon"> <b>%</b> </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-flat-rate"><?php echo $entry_flat_rate; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[flat_rate]" value="<?php echo $flat_rate; ?>" placeholder="<?php echo $entry_flat_rate; ?>" id="input-flat-rate" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-paypalid"><?php echo $entry_paypalid; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="customer[paypalid]" value="<?php echo $paypalid; ?>"  placeholder="<?php echo $entry_paypalid; ?>" id="input-paypalid" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-otherpayment"><?php echo $entry_otherinfo; ?></label>
                                <div class="col-sm-10">
                                    <textarea name="customer[otherpayment]" placeholder="<?php echo $entry_otherinfo; ?>" id="input-otherpayment" class="form-control" ><?php echo $otherpayment; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-product"><span data-toggle="tooltip" title="<?php echo $entry_product_id_info; ?>"><?php echo $entry_product_id; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="product" value="" placeholder="<?php echo $entry_product_id; ?>" id="input-product" class="form-control" />
                                    <div id="product-product" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php if (isset($partner_products)) { ?>
                                            <?php foreach ($partner_products as $partner_product) { ?>
                                                <div id="product-id<?php echo $partner_product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $partner_product['name']; ?>
                                                    <input type="hidden" name="product_ids[]" value="<?php echo $partner_product['product_id']; ?>" />
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <?php if (isset($error_duplicated_product) && $error_duplicated_product != '') { ?>
                                        <div class="text-danger"><?php echo $error_duplicated_product; ?></div>
                                    <?php } ?>
                                </div>
                            </div>


                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script>
<?php foreach ($languages as $language) { ?>
            // $("#input-description<?php echo $language['language_id']; ?>").summernote();
            CKEDITOR.replace("input-shortprofile[<?php echo $language['language_id']; ?>]", {
                filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
                filebrowserWindowWidth: 800,
                filebrowserWindowHeight: 500
            });
<?php } ?>
        $('#language a:first').tab('show');
        $('#first-tabs a:first').tab('show');

    </script>

    <script type="text/javascript"><!--

        $("#input-show-in-ksa").change(function () {
            $("#input-show-in-ksa-hidden").val("0");
            if ($(this).is(":checked")) {
                $("#input-show-in-ksa-hidden").val("1");
            }
        });

        $("#input-show-in-uae").change(function () {
            $("#input-show-in-uae-hidden").val("0");
            if ($(this).is(":checked")) {
                $("#input-show-in-uae-hidden").val("1");
            }
        });

        // Product
        $('input[name=\'product\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request) + '&filter_for_seller=1',
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'product\']').val('');

                $('#product-product' + item['value']).remove();

                $('#product-product').append('<div id="product-id' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_ids[]" value="' + item['value'] + '" /></div>');
            }
        });

        $('#product-product').delegate('.fa-minus-circle', 'click', function () {
            $(this).parent().remove();
        });


        $('#input-country').on('change', function () {
            newSrc = "./view/image/flags/" + $("#input-country option:selected").attr("data-val") + ".png";
            $('#country').attr('src', newSrc.toLowerCase());
        })

        $("#input-country").change(function () {
            getZones($("#input-country option:selected").val());
        });

        localocation = false;
        $('a[href=\'#tab-location\']').on('click', function () {
            if (!localocation) {
                $(this).append(' <i class="fa fa-spinner fa-spin remove-me"></i>');

                $.ajax({
                    url: '<?php echo $loadLocation; ?>',
                    dataType: 'html',
                    success: function (response) {
                        $('#tab-location').html(response);
                        $('.remove-me').remove();
                    }
                });
                localocation = true;
            }
        })

        //--></script>
    <link href="view/javascript/color-picker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <script src="view/javascript/color-picker/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.colorpicker').colorpicker();
        });
    </script>

    <script>
        function getZones(country_id) {
            country_id = typeof country_id !== 'undefined' ? country_id : '';
            if (country_id != '') {
                $.ajax({
                    url: 'index.php?route=customerpartner/partner/getZone&token=<?php echo $token; ?>&country_id=' + country_id,
                    dataType: 'json'
                })
                        .success(function (data) {
                            $('#input-zone-id').find('option').remove().end();
                            $('#input-zone-id').append($("<option></option>").attr("value", "").text("<?php echo $text_select; ?>"));
                            $.each(data, function (key, value) {
                                $('#input-zone-id').append($("<option></option>").attr("value", value.zone_id).text(value.name));
                            });
                        })
            }
        }
    </script>


    <?php echo $footer; ?>
