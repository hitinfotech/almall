<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-order-id"><?php echo $entry_order_id; ?></label>
                                <input type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-seller"><?php echo $entry_seller; ?></label>
                                <input type="text" name="filter_seller" value="<?php echo $filter_seller; ?>" placeholder="<?php echo $entry_seller; ?>" id="input-seller" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-awbno"><?php echo $entry_awbno; ?></label>
                                <input type="text" name="filter_awbno" value="<?php echo $filter_awbno; ?>" placeholder="<?php echo $entry_awbno; ?>" id="input-awbno" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-country"><?php echo $entry_country; ?></label>
                                <select class="form-control" name='filter_country' id='input-country'>
                                  <option value='0' ></option>
                                  <?php foreach($countries as $key => $country){ ?>
                                      <option value="<?php echo $country['country_id']?>" <?php echo ($country['country_id']== $filter_seller_country)?'selected':'' ?>><?php echo $country['name']?></option>
                                  <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                                <select name="filter_order_status" id="input-order-status" class="form-control">
                                    <option value="*"></option>
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $filter_order_status) { ?>
                                            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                    </div>
                </div>
                <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td class="text-right"><?php if ($sort == 'o.order_id') { ?>
                                            <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_id; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_order; ?>"><?php echo $column_order_id; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'awbno') { ?>
                                            <a href="<?php echo $sort_awbno; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_awbno; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_awbno; ?>"><?php echo $column_awbno; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'seller_name') { ?>
                                            <a href="<?php echo $sort_seller_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_seller_name; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_seller_name; ?>"><?php echo $column_seller_name; ?></a>
                                        <?php } ?></td>

                                    <td class="text-left"><?php if ($sort == 'customer_name') { ?>
                                            <a href="<?php echo $sort_customer_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer_name; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_customer_name; ?>"><?php echo $column_customer_name; ?></a>
                                        <?php } ?></td>

                                    <td class="text-right"><?php if ($sort == 'status') { ?>
                                            <a href="<?php echo $sort_order_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_status; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_order_status; ?>"><?php echo $column_order_status; ?></a>
                                        <?php } ?>
                                      </td>
                                    <td class="text-left"><?php if ($sort == 'o.date_added') { ?>
                                            <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                                        <?php } ?></td>
                                    <td class="text-right"><?php echo $column_action; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($orders) { ?>
                                    <?php foreach ($orders as $order) { ?>
                                        <tr>
                                            <td class="text-right"><?php echo $order['order_id']; ?></td>
                                            <td class="text-left"><?php echo $order['AWBNo']; ?></td>
                                            <td class="text-left"><?php echo $order['seller_name']; ?></td>
                                            <td class="text-left"><?php echo $order['customer_name']; ?></td>
                                            <td class="text-right"><?php echo $order['order_status']; ?></td>
                                            <td class="text-left"><?php echo $order['date_added']; ?></td>
                                            <td class="text-right"><a href="<?php echo $order['view']; ?>" data-toggle="tooltip" class="btn btn-info"><i class="fa fa-eye"></i></a></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"><!--
  $('#button-filter').on('click', function () {
            url = 'index.php?route=customerpartner/order_tracking&token=<?php echo $token; ?>';

            var filter_order_id = $('input[name=\'filter_order_id\']').val();

            if (filter_order_id) {
                url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
            }

            var filter_seller = $('input[name=\'filter_seller\']').val();

            if (filter_seller) {
                url += '&filter_seller=' + encodeURIComponent(filter_seller);
            }

            var filter_order_status = $('select[name=\'filter_order_status\']').val();

            if (filter_order_status != '*') {
                url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
            }

            var filter_awbno = $('input[name=\'filter_awbno\']').val();

            if (filter_awbno) {
                url += '&filter_awbno=' + encodeURIComponent(filter_awbno);
            }

            var filter_country = $('select[name=\'filter_country\']').val();

            if (filter_country != '0') {
                url += '&filter_seller_country=' + encodeURIComponent(filter_country);
            }

            location = url;
        });
        //--></script>
    <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
    <script type="text/javascript"><!--
    $('.date').datetimepicker({
            pickTime: false
        });

    // Seller
    $('input[name=\'filter_seller\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=customerpartner/partner/autocompleteForProducts&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    json.unshift({
                        seller_id: 0,
                        company: '<?php echo "none"; ?>'
                    });

                    response($.map(json, function (item) {
                        return {
                            label: item['company'],
                            value: item['seller_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'filter_seller\']').val(item['label']);
        }
    });
        //--></script></div>
<?php echo $footer; ?>
