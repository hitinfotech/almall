<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-store" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-store" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-block7" data-toggle="tab"><?php echo $tab_block7; ?></a></li>
                        <li><a href="#tab-block1" data-toggle="tab"><?php echo $tab_block1; ?></a></li>
                        <li><a href="#tab-block2" data-toggle="tab"><?php echo $tab_block2; ?></a></li>
                        <li><a href="#tab-block3" data-toggle="tab"><?php echo $tab_block3; ?></a></li>
                        <li><a href="#tab-block4" data-toggle="tab"><?php echo $tab_block4; ?></a></li>
                        <li><a href="#tab-block5" data-toggle="tab"><?php echo $tab_block5; ?></a></li>
                        <li><a href="#tab-block6" data-toggle="tab"><?php echo $tab_block6; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab-block1">
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_title1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_title1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_title1" value="<?php echo $homepage_block1_title1; ?>" placeholder="<?php echo $text_homepage_block1_title1; ?>" id="input-homepage_block1_title1" class="form-control" />
                                    <?php if ($error_homepage_block1_title1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_title1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_description1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_description1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_description1" value="<?php echo $homepage_block1_description1; ?>" placeholder="<?php echo $text_homepage_block1_description1; ?>" id="input-homepage_block1_description1" class="form-control" />
                                    <?php if ($error_homepage_block1_description1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_description1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_url1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_url1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_url1" value="<?php echo $homepage_block1_url1; ?>" placeholder="<?php echo $text_homepage_block1_url1; ?>" id="input-homepage_block1_url1" class="form-control" />
                                    <?php if ($error_homepage_block1_url1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_url1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_button1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_button1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_button1" value="<?php echo $homepage_block1_button1; ?>" placeholder="<?php echo $text_homepage_block1_button1; ?>" id="input-homepage_block1_button1" class="form-control" />
                                    <?php if ($error_homepage_block1_button1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_button1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b1_t1"><?php echo $text_homepage_block1_image1; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_11; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block1_image1" value="<?php echo $homepage_block1_image1; ?>" id="input-image_b1_t1" />
                                    <input type="hidden" name="homepage_block1_image1" value="<?php echo $homepage_block1_image1; ?>" />
                                    <?php if ($error_homepage_block1_image1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_image1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- block 1 title 2-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_title2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_title2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_title2" value="<?php echo $homepage_block1_title2; ?>" placeholder="<?php echo $text_homepage_block1_title2; ?>" id="input-homepage_block1_title2" class="form-control" />
                                    <?php if ($error_homepage_block1_title2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_title2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_description2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_description2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_description2" value="<?php echo $homepage_block1_description2; ?>" placeholder="<?php echo $text_homepage_block1_description2; ?>" id="input-homepage_block1_description2" class="form-control" />
                                    <?php if ($error_homepage_block1_description2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_description2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_url2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_url2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_url2" value="<?php echo $homepage_block1_url2; ?>" placeholder="<?php echo $text_homepage_block1_url2; ?>" id="input-homepage_block1_url2" class="form-control" />
                                    <?php if ($error_homepage_block1_url2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_url2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_button2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_button2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_button2" value="<?php echo $homepage_block1_button2; ?>" placeholder="<?php echo $text_homepage_block1_button2; ?>" id="input-homepage_block1_button2" class="form-control" />
                                    <?php if ($error_homepage_block1_button2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_button2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b1_t2"><?php echo $text_homepage_block1_image2; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_12; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block1_image2" value="<?php echo $homepage_block1_image2; ?>" id="input-image_b1_t2" />
                                    <input type="hidden" name="homepage_block1_image2" value="<?php echo $homepage_block1_image2; ?>" />
                                    <?php if ($error_homepage_block1_image2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_image2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- // block 3-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_title3"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_title3; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_title3" value="<?php echo $homepage_block1_title3; ?>" placeholder="<?php echo $text_homepage_block1_title3; ?>" id="input-homepage_block1_title3" class="form-control" />
                                    <?php if ($error_homepage_block1_title3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_title3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_description3"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_description3; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_description3" value="<?php echo $homepage_block1_description3; ?>" placeholder="<?php echo $text_homepage_block1_description3; ?>" id="input-homepage_block1_description3" class="form-control" />
                                    <?php if ($error_homepage_block1_description3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_description3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_url3"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_url3; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_url3" value="<?php echo $homepage_block1_url3; ?>" placeholder="<?php echo $text_homepage_block1_url3; ?>" id="input-homepage_block1_url3" class="form-control" />
                                    <?php if ($error_homepage_block1_url3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_url3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_button3"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_button3; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_button3" value="<?php echo $homepage_block1_button3; ?>" placeholder="<?php echo $text_homepage_block1_button3; ?>" id="input-homepage_block1_button3" class="form-control" />
                                    <?php if ($error_homepage_block1_button3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_button3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b1_t3"><?php echo $text_homepage_block1_image3; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_13; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block1_image3" value="<?php echo $homepage_block1_image3; ?>" id="input-image_b1_t3" />
                                    <input type="hidden" name="homepage_block1_image3" value="<?php echo $homepage_block1_image3; ?>" />
                                    <?php if ($error_homepage_block1_image3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_image3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- // block 4-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_title4"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_title4; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_title4" value="<?php echo $homepage_block1_title4; ?>" placeholder="<?php echo $text_homepage_block1_title4; ?>" id="input-homepage_block1_title4" class="form-control" />
                                    <?php if ($error_homepage_block1_title4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_title4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_description4"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_description4; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_description4" value="<?php echo $homepage_block1_description4; ?>" placeholder="<?php echo $text_homepage_block1_description4; ?>" id="input-homepage_block1_description4" class="form-control" />
                                    <?php if ($error_homepage_block1_description4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_description4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_url4"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_url4; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_url4" value="<?php echo $homepage_block1_url4; ?>" placeholder="<?php echo $text_homepage_block1_url4; ?>" id="input-homepage_block1_url4" class="form-control" />
                                    <?php if ($error_homepage_block1_url4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_url4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_button4"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_button4; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_button4" value="<?php echo $homepage_block1_button4; ?>" placeholder="<?php echo $text_homepage_block1_button4; ?>" id="input-homepage_block1_button4" class="form-control" />
                                    <?php if ($error_homepage_block1_button4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_button4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b1_t4"><?php echo $text_homepage_block1_image4; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_14; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block1_image4" value="<?php echo $homepage_block1_image4; ?>" id="input-image_b1_t4" />
                                    <input type="hidden" name="homepage_block1_image4" value="<?php echo $homepage_block1_image4; ?>" />
                                    <?php if ($error_homepage_block1_image4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_image4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- // block 5-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_title5"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_title5; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_title5" value="<?php echo $homepage_block1_title5; ?>" placeholder="<?php echo $text_homepage_block1_title5; ?>" id="input-homepage_block1_title5" class="form-control" />
                                    <?php if ($error_homepage_block1_title5) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_title5; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_description5"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_description5; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_description5" value="<?php echo $homepage_block1_description5; ?>" placeholder="<?php echo $text_homepage_block1_description5; ?>" id="input-homepage_block1_description5" class="form-control" />
                                    <?php if ($error_homepage_block1_description5) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_description5; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_url5"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_url5; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_url5" value="<?php echo $homepage_block1_url5; ?>" placeholder="<?php echo $text_homepage_block1_url5; ?>" id="input-homepage_block1_url5" class="form-control" />
                                    <?php if ($error_homepage_block1_url5) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_url5; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block1_button5"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block1_button5; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block1_button5" value="<?php echo $homepage_block1_button5; ?>" placeholder="<?php echo $text_homepage_block1_button5; ?>" id="input-homepage_block1_button5" class="form-control" />
                                    <?php if ($error_homepage_block1_button5) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_button5; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b1_t5"><?php echo $text_homepage_block1_image5; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_15; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block1_image5" value="<?php echo $homepage_block1_image5; ?>" id="input-image_b1_t5" />
                                    <input type="hidden" name="homepage_block1_image5" value="<?php echo $homepage_block1_image5; ?>" />
                                    <?php if ($error_homepage_block1_image5) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block1_image5; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-block2">

                          <div class="form-group">
                              <label class="col-sm-2 control-label" for="input-related_sa"><span data-toggle="tooltip" title="<?php //echo $help_related; ?>"><?php echo $entry_related; ?></span></label>
                              <div class="col-sm-10">
                                  <input type="text" name="related" value="" placeholder="<?php echo $entry_related; ?>" id="input-related" class="form-control" />
                                  <div id="product-related" class="well well-sm" style="height: 150px; overflow: auto;">
                                      <?php foreach ($product_relateds as $product) { ?>
                                          <div id="product-related<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle" onclick="$(this).parent().remove();"></i> <?php echo $product['name']; ?>
                                              <input type="hidden" name="product-related[]" value="<?php echo $product['product_id']; ?>" />
                                          </div>
                                      <?php } ?>
                                  </div>
                              </div>
                          </div>

                        </div>
                        <div class="tab-pane" id="tab-block3">

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_title"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_title; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_title" value="<?php echo $homepage_block3_title; ?>" placeholder="<?php echo $text_homepage_block3_title; ?>" id="input-homepage_block3_title" class="form-control" />
                                    <?php if ($error_homepage_block3_title) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_title; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_description"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_description; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_description" value="<?php echo $homepage_block3_description; ?>" placeholder="<?php echo $text_homepage_block3_description; ?>" id="input-homepage_block3_description" class="form-control" />
                                    <?php if ($error_homepage_block3_description) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_description; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- // block 1-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_title1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_title1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_title1" value="<?php echo $homepage_block3_title1; ?>" placeholder="<?php echo $text_homepage_block3_title1; ?>" id="input-homepage_block3_title1" class="form-control" />
                                    <?php if ($error_homepage_block3_title1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_title1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_description1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_description1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_description1" value="<?php echo $homepage_block3_description1; ?>" placeholder="<?php echo $text_homepage_block3_description1; ?>" id="input-homepage_block3_description1" class="form-control" />
                                    <?php if ($error_homepage_block3_description1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_description1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_url1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_url1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_url1" value="<?php echo $homepage_block3_url1; ?>" placeholder="<?php echo $text_homepage_block3_url1; ?>" id="input-homepage_block3_url1" class="form-control" />
                                    <?php if ($error_homepage_block3_url1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_url1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b3_t1"><?php echo $text_homepage_block3_image1; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_31; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block3_image1" value="<?php echo $homepage_block3_image1; ?>" id="input-image_b3_t1" />
                                    <input type="hidden" name="homepage_block3_image1" value="<?php echo $homepage_block3_image1; ?>" />
                                    <?php if ($error_homepage_block3_image1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_image1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- // block 2-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_title2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_title2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_title2" value="<?php echo $homepage_block3_title2; ?>" placeholder="<?php echo $text_homepage_block3_title2; ?>" id="input-homepage_block3_title2" class="form-control" />
                                    <?php if ($error_homepage_block3_title2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_title2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_description2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_description2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_description2" value="<?php echo $homepage_block3_description2; ?>" placeholder="<?php echo $text_homepage_block3_description2; ?>" id="input-homepage_block3_description2" class="form-control" />
                                    <?php if ($error_homepage_block3_description2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_description2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_url2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_url2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_url2" value="<?php echo $homepage_block3_url2; ?>" placeholder="<?php echo $text_homepage_block3_url2; ?>" id="input-homepage_block3_url2" class="form-control" />
                                    <?php if ($error_homepage_block3_url2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_url2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b3_t2"><?php echo $text_homepage_block3_image2; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_32; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block3_image2" value="<?php echo $homepage_block3_image2; ?>" id="input-image_b3_t2" />
                                    <input type="hidden" name="homepage_block3_image2" value="<?php echo $homepage_block3_image2; ?>" />
                                    <?php if ($error_homepage_block3_image2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_image2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- // block 3-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_title3"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_title3; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_title3" value="<?php echo $homepage_block3_title3; ?>" placeholder="<?php echo $text_homepage_block3_title3; ?>" id="input-homepage_block3_title3" class="form-control" />
                                    <?php if ($error_homepage_block3_title3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_title3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_description3"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_description3; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_description3" value="<?php echo $homepage_block3_description3; ?>" placeholder="<?php echo $text_homepage_block3_description3; ?>" id="input-homepage_block3_description3" class="form-control" />
                                    <?php if ($error_homepage_block3_description3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_description3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_url3"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_url3; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_url3" value="<?php echo $homepage_block3_url3; ?>" placeholder="<?php echo $text_homepage_block3_url3; ?>" id="input-homepage_block3_url3" class="form-control" />
                                    <?php if ($error_homepage_block3_url3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_url3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b3_t3"><?php echo $text_homepage_block3_image3; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_33; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block3_image3" value="<?php echo $homepage_block3_image3; ?>" id="input-image_b3_t3" />
                                    <input type="hidden" name="homepage_block3_image3" value="<?php echo $homepage_block3_image3; ?>" />
                                    <?php if ($error_homepage_block3_image3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_image3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- // block 4-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_title4"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_title4; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_title4" value="<?php echo $homepage_block3_title4; ?>" placeholder="<?php echo $text_homepage_block3_title4; ?>" id="input-homepage_block3_title4" class="form-control" />
                                    <?php if ($error_homepage_block3_title4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_title4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_description4"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_description4; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_description4" value="<?php echo $homepage_block3_description4; ?>" placeholder="<?php echo $text_homepage_block3_description4; ?>" id="input-homepage_block3_description4" class="form-control" />
                                    <?php if ($error_homepage_block3_description4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_description4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block3_url4"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block3_url4; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block3_url4" value="<?php echo $homepage_block3_url4; ?>" placeholder="<?php echo $text_homepage_block3_url4; ?>" id="input-homepage_block3_url4" class="form-control" />
                                    <?php if ($error_homepage_block3_url4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_url4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b3_t4"><?php echo $text_homepage_block3_image4; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_34; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block3_image4" value="<?php echo $homepage_block3_image4; ?>" id="input-image_b3_t4" />
                                    <input type="hidden" name="homepage_block3_image4" value="<?php echo $homepage_block3_image4; ?>" />
                                    <?php if ($error_homepage_block3_image4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block3_image4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-block4">
                            <!-- // block 1-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_title"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_title; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_title" value="<?php echo $homepage_block4_title; ?>" placeholder="<?php echo $text_homepage_block4_title; ?>" id="input-homepage_block4_title" class="form-control" />
                                    <?php if ($error_homepage_block4_title) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_title; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_description"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_description; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_description" value="<?php echo $homepage_block4_description; ?>" placeholder="<?php echo $text_homepage_block4_description; ?>" id="input-homepage_block4_description" class="form-control" />
                                    <?php if ($error_homepage_block4_description) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_description; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_title1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_title1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_title1" value="<?php echo $homepage_block4_title1; ?>" placeholder="<?php echo $text_homepage_block4_title1; ?>" id="input-homepage_block4_title1" class="form-control" />
                                    <?php if ($error_homepage_block4_title1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_title1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_description1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_description1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_description1" value="<?php echo $homepage_block4_description1; ?>" placeholder="<?php echo $text_homepage_block4_description1; ?>" id="input-homepage_block4_description1" class="form-control" />
                                    <?php if ($error_homepage_block4_description1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_description1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_url1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_url1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_url1" value="<?php echo $homepage_block4_url1; ?>" placeholder="<?php echo $text_homepage_block4_url1; ?>" id="input-homepage_block4_url1" class="form-control" />
                                    <?php if ($error_homepage_block4_url1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_url1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b4_t1"><?php echo $text_homepage_block4_image1; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_41; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block4_image1" value="<?php echo $homepage_block4_image1; ?>" id="input-image_b4_t1" />
                                    <input type="hidden" name="homepage_block4_image1" value="<?php echo $homepage_block4_image1; ?>" />
                                    <?php if ($error_homepage_block4_image1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_image1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- // block 2-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_title2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_title2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_title2" value="<?php echo $homepage_block4_title2; ?>" placeholder="<?php echo $text_homepage_block4_title2; ?>" id="input-homepage_block4_title2" class="form-control" />
                                    <?php if ($error_homepage_block4_title2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_title2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_description2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_description2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_description2" value="<?php echo $homepage_block4_description2; ?>" placeholder="<?php echo $text_homepage_block4_description2; ?>" id="input-homepage_block4_description2" class="form-control" />
                                    <?php if ($error_homepage_block4_description2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_description2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_url2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_url2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_url2" value="<?php echo $homepage_block4_url2; ?>" placeholder="<?php echo $text_homepage_block4_url2; ?>" id="input-homepage_block4_url2" class="form-control" />
                                    <?php if ($error_homepage_block4_url2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_url2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b3_t2"><?php echo $text_homepage_block4_image2; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_42; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block4_image2" value="<?php echo $homepage_block4_image2; ?>" id="input-image_b4_t2" />
                                    <input type="hidden" name="homepage_block4_image2" value="<?php echo $homepage_block4_image2; ?>" />
                                    <?php if ($error_homepage_block4_image2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_image2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- // block 3-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_title3"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_title3; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_title3" value="<?php echo $homepage_block4_title3; ?>" placeholder="<?php echo $text_homepage_block4_title3; ?>" id="input-homepage_block4_title3" class="form-control" />
                                    <?php if ($error_homepage_block4_title3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_title3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_description3"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_description3; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_description3" value="<?php echo $homepage_block4_description3; ?>" placeholder="<?php echo $text_homepage_block4_description3; ?>" id="input-homepage_block4_description3" class="form-control" />
                                    <?php if ($error_homepage_block4_description3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_description3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_url3"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_url3; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_url3" value="<?php echo $homepage_block4_url3; ?>" placeholder="<?php echo $text_homepage_block4_url3; ?>" id="input-homepage_block4_url3" class="form-control" />
                                    <?php if ($error_homepage_block4_url3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_url3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b3_t3"><?php echo $text_homepage_block4_image3; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_43; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block4_image3" value="<?php echo $homepage_block4_image3; ?>" id="input-image_b4_t3" />
                                    <input type="hidden" name="homepage_block4_image3" value="<?php echo $homepage_block4_image3; ?>" />
                                    <?php if ($error_homepage_block4_image3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_image3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- // block 4-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_title4"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_title4; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_title4" value="<?php echo $homepage_block4_title4; ?>" placeholder="<?php echo $text_homepage_block4_title4; ?>" id="input-homepage_block4_title4" class="form-control" />
                                    <?php if ($error_homepage_block4_title4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_title4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_description4"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_description4; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_description4" value="<?php echo $homepage_block4_description4; ?>" placeholder="<?php echo $text_homepage_block4_description4; ?>" id="input-homepage_block4_description4" class="form-control" />
                                    <?php if ($error_homepage_block4_description4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_description4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_block4_url4"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_homepage_block4_url4; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_block4_url4" value="<?php echo $homepage_block4_url4; ?>" placeholder="<?php echo $text_homepage_block4_url4; ?>" id="input-homepage_block4_url4" class="form-control" />
                                    <?php if ($error_homepage_block4_url4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_url4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b3_t4"><?php echo $text_homepage_block4_image4; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_44; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block4_image4" value="<?php echo $homepage_block4_image4; ?>" id="input-image_b4_t4" />
                                    <input type="hidden" name="homepage_block4_image4" value="<?php echo $homepage_block4_image4; ?>" />
                                    <?php if ($error_homepage_block4_image4) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block4_image4; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-block5">
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand1"><span data-toggle="tooltip" title="<?php echo $text_brand; ?>"><?php echo $text_brand; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand1" value="<?php echo $homepage_brand1 ?>" placeholder="<?php echo $text_brand; ?>" id="input-homepage_brand1" class="form-control" />
                                    <input type="hidden" name="homepage_brand1_id" value="<?php echo $homepage_brand1_id; ?>" />
                                    <?php if ($error_homepage_brand1_id) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand1_id; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand1_name"><span data-toggle="tooltip" title="<?php echo $text_brand_name; ?>"><?php echo $text_brand_name; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand1_name" value="<?php echo $homepage_brand1_name ?>" placeholder="<?php echo $text_brand_name; ?>" id="input-homepage_brand1_name" class="form-control" />
                                    <?php if ($error_homepage_brand1_name) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand1_name; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand1_desc"><span data-toggle="tooltip" title="<?php echo $text_brand_desc; ?>"><?php echo $text_brand_name; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand1_desc" value="<?php echo $homepage_brand1_desc ?>" placeholder="<?php echo $text_brand_name; ?>" id="input-homepage_brand1_desc" class="form-control" />
                                    <?php if ($error_homepage_brand1_desc) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand1_desc; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand1_button"><span data-toggle="tooltip" title="<?php echo $text_brand_button; ?>"><?php echo $text_brand_button; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand1_button" value="<?php echo $homepage_brand1_button ?>" placeholder="<?php echo $text_brand_button; ?>" id="input-homepage_brand1_button" class="form-control" />
                                    <?php if ($error_homepage_brand1_button) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand1_button; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b5_t1"><?php echo $text_homepage_block5_image1; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_51; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block5_image1" value="<?php echo $homepage_block5_image1; ?>" id="input-image_b5_t1" />
                                    <input type="hidden" name="homepage_block5_image1" value="<?php echo $homepage_block5_image1; ?>" />
                                    <?php if ($error_homepage_block5_image1) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block5_image1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand2"><span data-toggle="tooltip" title="<?php echo $text_brand; ?>"><?php echo $text_brand; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand2" value="<?php echo $homepage_brand2 ?>" placeholder="<?php echo $text_brand; ?>" id="input-homepage_brand2" class="form-control" />
                                    <input type="hidden" name="homepage_brand2_id" value="<?php echo $homepage_brand2_id; ?>" />
                                    <?php if ($error_homepage_brand2_id) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand2_id; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand2_name"><span data-toggle="tooltip" title="<?php echo $text_brand_name; ?>"><?php echo $text_brand_name; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand2_name" value="<?php echo $homepage_brand2_name ?>" placeholder="<?php echo $text_brand_name; ?>" id="input-homepage_brand2_name" class="form-control" />
                                    <?php if ($error_homepage_brand2_name) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand2_name; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand2_desc"><span data-toggle="tooltip" title="<?php echo $text_brand_desc; ?>"><?php echo $text_brand_name; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand2_desc" value="<?php echo $homepage_brand2_desc ?>" placeholder="<?php echo $text_brand_name; ?>" id="input-homepage_brand2_desc" class="form-control" />
                                    <?php if ($error_homepage_brand2_desc) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand2_desc; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand2_button"><span data-toggle="tooltip" title="<?php echo $text_brand_button; ?>"><?php echo $text_brand_button; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand2_button" value="<?php echo $homepage_brand2_button ?>" placeholder="<?php echo $text_brand_button; ?>" id="input-homepage_brand2_button" class="form-control" />
                                    <?php if ($error_homepage_brand2_button) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand2_button; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b5_t2"><?php echo $text_homepage_block5_image2; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_52; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block5_image2" value="<?php echo $homepage_block5_image2; ?>" id="input-image_b5_t2" />
                                    <input type="hidden" name="homepage_block5_image2" value="<?php echo $homepage_block5_image2; ?>" />
                                    <?php if ($error_homepage_block5_image2) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block5_image2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand3"><span data-toggle="tooltip" title="<?php echo $text_brand; ?>"><?php echo $text_brand; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand3" value="<?php echo $homepage_brand3 ?>" placeholder="<?php echo $text_brand; ?>" id="input-homepage_brand3" class="form-control" />
                                    <input type="hidden" name="homepage_brand3_id" value="<?php echo $homepage_brand3_id; ?>" />
                                    <?php if ($error_homepage_brand3_id) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand3_id; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand3_name"><span data-toggle="tooltip" title="<?php echo $text_brand_name; ?>"><?php echo $text_brand_name; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand3_name" value="<?php echo $homepage_brand3_name ?>" placeholder="<?php echo $text_brand_name; ?>" id="input-homepage_brand3_name" class="form-control" />
                                    <?php if ($error_homepage_brand3_name) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand3_name; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand3_desc"><span data-toggle="tooltip" title="<?php echo $text_brand_desc; ?>"><?php echo $text_brand_name; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand3_desc" value="<?php echo $homepage_brand3_desc ?>" placeholder="<?php echo $text_brand_name; ?>" id="input-homepage_brand3_desc" class="form-control" />
                                    <?php if ($error_homepage_brand3_desc) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand3_desc; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-homepage_brand3_button"><span data-toggle="tooltip" title="<?php echo $text_brand_button; ?>"><?php echo $text_brand_button; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="homepage_brand3_button" value="<?php echo $homepage_brand3_button ?>" placeholder="<?php echo $text_brand_button; ?>" id="input-homepage_brand3_button" class="form-control" />
                                    <?php if ($error_homepage_brand3_button) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_brand3_button; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b5_t3"><?php echo $text_homepage_block5_image3; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_53; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="homepage_block5_image3" value="<?php echo $homepage_block5_image3; ?>" id="input-image_b5_t3" />
                                    <input type="hidden" name="homepage_block5_image3" value="<?php echo $homepage_block5_image3; ?>" />
                                    <?php if ($error_homepage_block5_image3) { ?>
                                        <div class="text-danger"><?php echo $error_homepage_block5_image3; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="tab-block6">
                          <div class="form-group">
                              <label class="col-sm-2 control-label" for="input-offer"><span data-toggle="tooltip" title="<?php //echo $help_related; ?>"><?php echo $entry_offers; ?></span></label>
                              <div class="col-sm-10">
                                  <input type="text" name="related-offers" value="" placeholder="<?php echo $entry_related; ?>" id="input-offers" class="form-control" />
                                  <div id="product-related-offers" class="well well-sm" style="height: 150px; overflow: auto;">
                                      <?php foreach ($product_offers as $product) { ?>
                                          <div id="product-related-offers<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle" onclick="$(this).parent().remove();"></i> <?php echo $product['name']; ?>
                                              <input type="hidden" name="product-related-offers[]" value="<?php echo $product['product_id']; ?>" />
                                          </div>
                                      <?php } ?>
                                  </div>
                              </div>
                          </div>
                        </div>
                        <div class="tab-pane active" id="tab-block7">
                          <div class="form-group required">
                            <div class='row' style="margin-left: 20px" >
                              <label class="col-sm-3 ">Language</label>
                              <div class='col-sm-3 '>
                                <select class="form-control" name='coutnry_langauge'>
                                  <option value='1' <?php echo ($page_language ==1 ? "selected" : "")?> > english</option>
                                  <option value='2' <?php echo ($page_language ==2 ? "selected" : "")?> > Arabic</option>
                                </select>
                              </div>
                            </div>
                            <br>
                              <div class='row' style="margin-left: 20px" >
                                <?php foreach ($available_coutnries as $key => $country){?>
                                  <label class="col-sm-3 "><input class='in_country' data-country-id='<?php echo $country["country_id"] ?>' type='checkbox' name='page_country[<?php echo $country["country_id"] ?>]' <?php echo (isset($page_countries[$country["country_id"]]) ? "checked" : "")?> ><?php echo $country["name"] ?>  </label>
                                <?php } ?>
                              </div>
                          </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script><!--
// Brand1
    $('input[name=\'homepage_brand1\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=mall/brand/autocomplete&token=<?php echo $token; ?>&filter_status=1&filter_keyword=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    json.unshift({
                        id: 0,
                        name: '<?php echo $text_none; ?>'
                    });

                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['id']
                        };
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'homepage_brand1\']').val(item['label']);
            $('input[name=\'homepage_brand1_id\']').val(item['value']);
        }
    });

    // Brand2
    $('input[name=\'homepage_brand2\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=mall/brand/autocomplete&token=<?php echo $token; ?>&filter_keyword=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    json.unshift({
                        id: 0,
                        name: '<?php echo $text_none; ?>'
                    });

                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['id']
                        };
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'homepage_brand2\']').val(item['label']);
            $('input[name=\'homepage_brand2_id\']').val(item['value']);
        }
    });

    // Brand3
    $('input[name=\'homepage_brand3\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=mall/brand/autocomplete&token=<?php echo $token; ?>&filter_keyword=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    json.unshift({
                        id: 0,
                        name: '<?php echo $text_none; ?>'
                    });

                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['id']
                        };
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'homepage_brand3\']').val(item['label']);
            $('input[name=\'homepage_brand3_id\']').val(item['value']);
        }
    });
//--></script>
<script>
$(document).ready(function() {
    $(".in_country").each(function(){
      if( $(this).is(':checked') ){
        $('input[name=\'coutnry_langauge['+$(this).attr("data-country-id")+']\']').removeAttr("disabled");
      }else{
        $('input[name=\'coutnry_langauge['+$(this).attr("data-country-id")+']\']').attr("disabled",true);
        $('input[name=\'coutnry_langauge['+$(this).attr("data-country-id")+']\']').prop('checked', false);
      }
    })

    $(".in_country").click( function(){
       if( $(this).is(':checked') ){
         $('input[name=\'coutnry_langauge['+$(this).attr("data-country-id")+']\']').removeAttr("disabled");
       }else{
         $('input[name=\'coutnry_langauge['+$(this).attr("data-country-id")+']\']').attr("disabled",true);
       }
    });
});

$('input[name=\'related\']').autocomplete({

      'source': function (request, response) {
          $.ajax({
            url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent($(this).val()),
            dataType: 'json',
            success: function (json) {
                response($.map(json, function (item) {
                    return {
                        label: item['name'],
                        value: item['product_id']
                    }
                }));
            }
          });
      },
      'select': function (item) {
        $('input[name=\'related\']').val('');

        $('#product-related' + item['value']).remove();

        $('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle" onclick="$(this).parent().remove();"></i> ' + item['label'] + '<input type="hidden" name="product-related[]" value="' + item['value'] + '" /></div>');
    }
});

$('input[name=\'related-offers\']').autocomplete({
  'source': function (request, response) {
      $.ajax({
        url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent($(this).val()),
        dataType: 'json',
        success: function (json) {
            response($.map(json, function (item) {
                return {
                    label: item['name'],
                    value: item['product_id']
                }
            }));
        }
      });
  },
  'select': function (item) {
        $('input[name=\'related-offers\']').val('');

        $('#product-related-offers' + item['value']).remove();

        $('#product-related-offers').append('<div id="product-related-offers' + item['value'] + '"><i class="fa fa-minus-circle" onclick="$(this).parent().remove();"></i> ' + item['label'] + '<input type="hidden" name="product-related-offers[]" value="' + item['value'] + '" /></div>');
    }
});

</script>
<?php echo $footer; ?>
