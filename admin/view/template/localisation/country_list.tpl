<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-country').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-country">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'language_code') { ?>
                    <a href="<?php echo $sort_language; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_language; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_language; ?>"><?php echo $column_language; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'curcode') { ?>
                    <a href="<?php echo $sort_currency; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_currency; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_currency; ?>"><?php echo $column_currency; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'telecode') { ?>
                    <a href="<?php echo $sort_telecode; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_telecode; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_telecode; ?>"><?php echo $column_telecode; ?></a>
                    <?php } ?></td>
                    <td class="text-left"><?php if ($sort == 'telephone') { ?>
                      <a href="<?php echo $sort_telephone; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_telephone; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $sort_telephone; ?>"><?php echo $column_telephone; ?></a>
                      <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'available') { ?>
                    <a href="<?php echo $sort_availability; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_availability; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_availability; ?>"><?php echo $column_availability; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'iso_code_2') { ?>
                    <a href="<?php echo $sort_iso_code_2; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_iso_code_2; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_iso_code_2; ?>"><?php echo $column_iso_code_2; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'iso_code_3') { ?>
                    <a href="<?php echo $sort_iso_code_3; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_iso_code_3; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_iso_code_3; ?>"><?php echo $column_iso_code_3; ?></a>
                    <?php } ?></td>
                  <td class="text-left">
                    <?php echo $column_vat; ?>
                  </td>
                  <td class="text-left">
                    <?php echo $column_vat_percent; ?>
                  </td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($countries) { ?>
                <?php foreach ($countries as $country) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($country['country_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $country['country_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $country['country_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $country['name']; ?></td>
                  <td class="text-left"><?php echo $country['langauge']; ?></td>
                  <td class="text-left"><?php echo $country['curcode']; ?></td>
                  <td class="text-left"><?php echo $country['telecode']; ?></td>
                  <td class="text-left"><?php echo $country['telephone']; ?></td>
                  <td class="text-left"><?php echo $country['available']; ?></td>
                  <td class="text-left"><?php echo $country['iso_code_2']; ?></td>
                  <td class="text-left"><?php echo $country['iso_code_3']; ?></td>
                  <td class="text-left">
                    <?php if($country['vat_check']){ ?>
                      <input type="checkbox" checked value="<?php echo $country['iso_code_2']; ?>" id="<?php echo $country['iso_code_2']; ?>" onclick="ajaxAddVAT(this);">
                    <?php } else { ?>
                      <input type="checkbox" value="<?php echo $country['iso_code_2']; ?>" id="<?php echo $country['iso_code_2']; ?>" onclick="ajaxAddVAT(this);">
                    <?php } ?>
                  </td>
                  <td class="text-left">
                    <?php if(!is_null($country['vat_percent'])) { ?>
                      <div class="col-sm-6">
                        <input class="col-sm-10" type="number" step="any" min="0" max="100" id="<?php echo $country['iso_code_2']; ?>" value="<?php echo $country['vat_percent']?>">%</input>
                      </div>
                    <?php } ?>
                    <div class="col-sm-6">
                      <button class="btn btn-primary"  id="<?php echo $country['iso_code_2']; ?>">Update</button>
                    </div>
                  </td>
                  <td class="text-right"><a href="<?php echo $country['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script>
  function ajaxAddVAT(e) {
      var id = $(e).attr('id');
      if ($(e).is(':checked')) {
          var check_value = true;
      } else {
          var check_value = false;
      }
      $.ajax({
          url: 'index.php?route=localisation/country/ChangeVATvalue&country_iso=' + id + '&check_value='+check_value,
          type: 'get',
          dataType: 'json',
          success: function(res) {
          }

      });
  }

  $('button').click(function(e) {
      var id = e.target.id;
      var txt = $("input[type='number'][id="+id+"]").val();
      $.ajax({
          url: 'index.php?route=localisation/country/ChangeVATvalue&country_iso=' + id + '&percent='+txt,
          type: 'get',
          dataType: 'json',
          success: function(res) {

          }

      });

  });
</script>
