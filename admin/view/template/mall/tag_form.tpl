<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-<?php echo $controller_name?>" data-toggle="tooltip" title="<?php echo $button_save; ?>"
                        class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
                   class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <!-- { breadcrumb } !-->
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
            <!-- { / breadcrumb } !-->
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-<?php echo $controller_name?>"
                      class="form-horizontal">


                    <!-- { nav-tabs } !-->
                    <ul class="nav nav-tabs">

                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>

                        <li><a href="#tab_attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                        <li><a href="#tab_links" data-toggle="tab"><?php echo $tab_links; ?></a></li>

                    </ul>
                    <!-- { / nav-tabs } !-->

                    <div class="tab-content">

                        <!-- { tab-pane } !-->
                        <div class="tab-pane active" id="tab-general">

                            <!-- { nav-tabs } !-->
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img
                                                src="view/image/flags/<?php echo $language['image']; ?>"
                                                title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?>
                                    </a></li>
                                <?php } ?>
                            </ul>
                            <!-- { / nav-tabs } !-->

                            <!-- { tab-content } !-->
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="item_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($item_description[$language['language_id']]) ? $item_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                            <?php if (isset($error_name[$language['language_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="item_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($item_description[$language['language_id']]) ? $item_description[$language['language_id']]['description'] : ''; ?></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="item_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($item_description[$language['language_id']]) ? $item_description[$language['language_id']]['meta_title'] : ''; ?>"
                                                   placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"
                                               for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="item_description[<?php echo $language['language_id']; ?>][meta_description]" value="<?php echo isset($item_description[$language['language_id']]) ? $item_description[$language['language_id']]['meta_description'] : ''; ?>"
                                                   placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"
                                               for="input-meta-keywords<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keywords; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="item_description[<?php echo $language['language_id']; ?>][meta_keywords]" value="<?php echo isset($item_description[$language['language_id']]) ? $item_description[$language['language_id']]['meta_keywords'] : ''; ?>"
                                                   placeholder="<?php echo $entry_meta_keywords; ?>" id="input-meta-keywords<?php echo $language['language_id']; ?>"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <!-- { / tab-content } !-->

                        </div>
                        <!-- { / tab-pane } !-->


                        <div class="tab-pane" id="tab_attribute">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $entry_image; ?>" />
                                    <input type="file" name="image" value="<?php echo $image; ?>" id="input-image" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="status" id="input-status" class="form-control">
                                        <?php if ($status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-home"><?php echo $entry_home; ?></label>
                                <div class="col-sm-10">
                                    <select name="home" id="input-home" class="form-control">
                                        <?php if ($home) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label"
                                       for="input-entry_author"><?php echo $entry_author; ?></span></label>
                                <div class="col-sm-10">

                                    <input type="text" name="author" value="<?php echo $author; ?>"
                                           placeholder="<?php echo $entry_author; ?>" id="input-author"
                                           class="form-control"/>
                                    <?php if (isset($error_name['author'])) { ?>
                                    <div class="text-danger"><?php echo $error_name['author']; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sort_order" value="<?php echo $sort_order; ?>"
                                           placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order"
                                           class="form-control"/>
                                </div>
                            </div>


                        </div>

                        <div class="tab-pane" id="tab_links">

                            <div class="form-group country-field">
                                <label class="col-sm-2 control-label"
                                       for="input-link-country"><?php echo $entry_country_id; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="item_link[country][]" id="input-link-country"
                                            data-input-type="country" multiple
                                            placeholder="<?php echo $entry_country_id; ?>"></select>
                                </div>
                            </div>

                            <?php foreach(array('product'=>array('route'=>'catalog/product/autocomplete'), 'tip'=>array('route'=>'mall/tip/autocomplete'), 'look'=>array('route'=>'mall/look/autocomplete')) as $for_relation_key=>$for_relation_value) { ?>
                            <?php $for_entry_related_label = "entry_related_{$for_relation_key}"?>
                            <?php $for_entry_related_help = "entry_help_{$for_relation_key}"?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-related-<?php echo $for_relation?>"><span data-toggle="tooltip" title="<?php echo $$for_entry_related_help; ?>"><?php echo $$for_entry_related_label; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" value="" placeholder="<?php echo $$for_entry_related_label; ?>"
                                        <?php foreach($for_relation_value as $for_relation_value_key=>$for_relation_value_value) { ?>
                                            data-<?php echo $for_relation_value_key?>="<?php echo $for_relation_value_value?>"
                                        <?php } ?>
                                        id="input-related-<?php echo $for_relation_key?>" class="form-control" />
                                    <div id="<?php echo $for_relation_key?>-related" class="well well-sm" style="height: 150px; overflow: auto;"></div>
                                </div>
                            </div>
                            <?php }?>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<script>

    var dataSource1 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/<?php echo $controller_name;?>/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function (url) {
                return url.replace('%TYPE', encodeURIComponent($('.country-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.country-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {id: v.id, name: v.name};
                });
            }
        }
    });
    dataSource1.initialize();

    $('.country-field > > #input-link-country').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource1',
            displayKey: 'name',
            source: dataSource1.ttAdapter()
        }
    });
</script>


<?php if(isset($item_link) && !empty($item_link)) { ?>
<script type="text/javascript">
    var linksArray = (<?php echo json_encode($item_link) ?>);
    if (!jQuery.isEmptyObject(linksArray)) {

        $.each(linksArray, function (index, value) {
            switch (index) {
                case 'country':
                    $.each(value, function (index1, value1) {
                        $('#input-link-country').tagsinput('add', {
                            "id": value1.id,
                            "name": "" + value1.name.replace(/'/g, "\\'") + ""
                        });
                    });
                    break;
                case 'category':
                    $.each(value, function (index6, value6) {
                        $('#input-link-category').tagsinput('add', {
                            "id": value6.id,
                            "name": "" + value6.name.replace(/'/g, "\\'") + ""
                        });
                    });
                    break;
                case 'shop':
                    $.each(value, function (index4, value4) {
                        $('#input-link-shop').tagsinput('add', {
                            "id": value4.id ,
                            "name": ""+value4.name.replace(/'/g, "\\'")+""
                        });
                    });
                    break;
                default:
                    break;
            }
        });
    }
</script>
<?php } ?>

<script>
    var input_related_prefix = 'input-related-';

    $('input[id^='+ input_related_prefix +']').each(function() {
        var $this = $(this);
        var data_route = $this.attr('data-route');
        var related_id = $this.attr('id').substr(input_related_prefix.length);
        var append_item = function (item) {
            $('#input-related-' + related_id).val('');

            $('#product-related' + item['value']).remove();

            var html_related_div = $('<div></div>')
                    .attr('id', related_id +'-related' + item['value']);
            var html_related_close = $('<a href="javascript:void(0)"><i class="fa fa-minus-circle"></i></a>');
            var html_related_content = $('<a></a>').text(item['label']);
            var html_related_hidden = $('<input type="hidden" />')
                    .attr('name', 'item_link['+ related_id +'][]')
                    .attr('value', item['value']);

            html_related_div
                    .append(html_related_close)
                    .append(html_related_content)
                    .append(html_related_hidden);
            $('#'+ related_id +'-related')
                    .append(html_related_div);

            html_related_close.click(function() {
                html_related_div.remove();
            });
        };

        $this.autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route='+ data_route +'&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item[related_id + '_id']
                            }
                        }));
                    }
                });
            },
            'select': append_item
        });
        if(typeof linksArray !== "undefined" && linksArray[related_id]) {
            $.each(linksArray[related_id], function(index, value) {
                var item = { 'value': value.id, 'label': value.name };
                append_item(item);
            });
        }
    });
</script>



<script>
<?php foreach ($languages as $language) { ?>
        // $("#input-description<?php echo $language['language_id']; ?>").summernote();
        CKEDITOR.replace("input-description<?php echo $language['language_id']; ?>", {
            filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
            filebrowserWindowWidth: 800,
            filebrowserWindowHeight: 500
        });
<?php } ?>
    $('#language a:first').tab('show');
</script>

<?php echo $footer; ?>