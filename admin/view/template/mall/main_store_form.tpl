<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-main-group" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-main-group" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                    <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="main_store_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($main_store_description[$language['language_id']]) ? $main_store_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                            </div>
                                            <?php if (isset($error_name[$language['language_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="main_store_description[<?php echo $language['language_id']; ?>][description]" rows="5" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($main_store_description[$language['language_id']]) ? $main_store_description[$language['language_id']]['description'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-location<?php echo $language['language_id']; ?>"><?php echo $entry_location; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="main_store_description[<?php echo $language['language_id']; ?>][location]" value="<?php echo isset($main_store_description[$language['language_id']]) ? $main_store_description[$language['language_id']]['location'] : ''; ?>" placeholder="<?php echo $entry_location; ?>" id="input-location<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_location[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_location[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-data">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $entry_image; ?>" />
                                    <input type="file" name="image" value="<?php echo $image; ?>" id="input-image" />
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-main_group"><?php echo $entry_main_group; ?></label>
                                <div class="col-sm-10">
                                    <select name="main_group_id" id="input-main_group" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($main_groups as $row) { ?>
                                            <option <?php echo $row['main_group_id'] == $main_group_id ? "selected" : "" ?> value="<?php echo $row['main_group_id'] ?>"><?php echo $row['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php if (isset($error_main_group_id) and $error_main_group_id !='' ) { ?>
                                        <div class="text-danger"><?php echo $error_main_group_id ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php /* <div class="form-group">
                              <label class="col-sm-2 control-label" for="input-main_group"><?php echo $entry_category; ?></label>
                              <div class="col-sm-10">
                              <select name="category_id" id="input-category_id" class="form-control">
                              <option value=""><?php echo $text_select;?></option>
                              <?php foreach ($categories as $row) { ?>
                              <option <?php echo $row['category_id']==$category_id?"selected":"" ?> value="<?php echo $row['category_id'] ?>"><?php echo $row['name']; ?></option>
                              <?php } ?>
                              </select>
                              </div>
                              </div> */ ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-phone"><?php echo $entry_phone; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="phone" value="<?php echo $phone; ?>" placeholder="<?php echo $entry_phone; ?>" id="input-website" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-website"><?php echo $entry_website; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="website" value="<?php echo $website; ?>" placeholder="<?php echo $entry_website; ?>" id="input-website" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-social_fb"><?php echo $entry_social_fb; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="social_fb" value="<?php echo $social_fb; ?>" placeholder="<?php echo $entry_social_fb; ?>" id="input-social_fb" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-social_tw"><?php echo $entry_social_tw; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="social_tw" value="<?php echo $social_tw; ?>" placeholder="<?php echo $entry_social_tw; ?>" id="input-social_tw" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-social_jeeran"><?php echo $entry_social_jeeran; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="social_jeeran" value="<?php echo $social_jeeran; ?>" placeholder="<?php echo $entry_social_jeeran; ?>" id="input-social_jeeran" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-latitude"><?php echo $entry_latitude; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="latitude" value="<?php echo $latitude; ?>" placeholder="<?php echo $entry_latitude; ?>" id="input-latitude" class="form-control" />
                                    <?php if ($error_google) { ?>
                                        <div class="text-danger"><?php echo $error_google ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-longitude"><?php echo $entry_longitude; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="longitude" value="<?php echo $longitude; ?>" placeholder="<?php echo $entry_longitude; ?>" id="input-longitude" class="form-control" />
                                    <?php if ($error_google) { ?>
                                        <div class="text-danger"><?php echo $error_google ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-zoom"><?php echo $entry_zoom; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="zoom" value="<?php echo $zoom; ?>" placeholder="<?php echo $entry_zoom; ?>" id="input-zoom" class="form-control" />
                                    <?php if ($error_google) { ?>
                                        <div class="text-danger"><?php echo $error_google ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-parent"><?php echo $entry_country; ?></label>
                                <div class="col-sm-10">
                                    <select name="country_id" id="input-country-id" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($countries as $key => $country) { ?>
                                            <option value="<?php echo $key; ?>" <?php if (isset($country_id) && $key == $country_id) { ?>selected<?php } ?>><?php echo $country; ?></option>
                                        <?php } ?>
                                    </select>
                                        <?php if (isset($error_country_id) and $error_country_id !='' ) { ?>
                                        <div class="text-danger"><?php echo $error_country_id ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-parent"><?php echo $entry_city; ?></label>
                                <div class="col-sm-10">
                                    <select name="city_id" id="input-city-id" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                    </select>
                                   <?php if (isset($error_city_id) and $error_city_id !=''  ) { ?>
                                        <div class="text-danger"><?php echo $error_city_id ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="status" id="input-status" class="form-control">
                                        <?php if (isset($status) && $status == 1) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sub-stores"><?php echo $entry_sub_stores; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="sub_stores[]" id="input-sub-stores"  multiple  placeholder="<?php echo $entry_sub_stores; ?>"></select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-store_category"><?php echo $entry_store_category; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="store_category[]" id="input-store_category"  multiple  placeholder="<?php echo $entry_store_category; ?>"></select>
                                </div>
                            </div>

                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="input-enable_brand"><?php echo $entry_enable_brand; ?></label>
                              <input type="checkbox" name="show_related_brand" id="input-enable_brand" class=" col-sm-2 form-control" <?php echo ($show_related_brand==1) ? "checked" : ""?> />
                                <label class="col-sm-2 control-label" for="input-brands"><?php echo $entry_brand; ?></label>
                                <div class="col-sm-6" id="brand-select">
                                    <select class="form-control" name="brands[]" id="input-brands"  multiple placeholder="<?php echo $entry_brand; ?>"></select>
                                </div>
                                <div class="col-sm-6 hidden" id="brand-text">
                                    <input type="text" name="brand" placeholder="<?php echo $entry_brand; ?>" id="input-brand" class="form-control"  readonly/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-enable_category"><?php echo $entry_enable_category; ?></label>
                                <input type="checkbox" name="show_related_category" id="input-enable_category" class=" col-sm-2 form-control" <?php echo ($show_related_category==1) ? "checked" : ""?> />


                                <label class="col-sm-2 control-label" for="input-category"><?php echo $entry_category; ?></label>
                                <div class="col-sm-6" id="category-select">
                                    <select class="form-control" name="related_categories[]" id="input-categories" multiple placeholder="<?php echo $entry_category; ?>"></select>
                                </div>
                                <div class="col-sm-6 hidden" id="category-text">
                                    <input type="text" name="category"  placeholder="<?php echo $entry_category; ?>" id="input-category" class="form-control"  readonly/>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript"><!--
// -- on page open check for country and city pre select values
        getCities('<?php echo $country_id ?>', '<?php echo $city_id ?>');

        function getCities(country_id, city_id) {
            country_id = typeof country_id !== 'undefined' ? country_id : '';
            city_id = typeof city_id !== 'undefined' ? city_id : '';
            if (country_id != '') {
                //-- show loader
                $.ajax({
                    url: 'index.php?route=mall/mall/getZone&token=<?php echo $token; ?>&country_id=' + $('#input-country-id').val(),
                    dataType: 'json'/*,
                     data: {param1: 'value1'},*/
                })
                        .success(function (data) {
                            // console.log("success");
                            // console.log(data);
                            $('#input-city-id')
                                    .find('option')
                                    .remove()
                                    .end();
                            $('#input-city-id')
                                    .append($("<option></option>")
                                            .attr("value", "")
                                            .text("<?php echo $text_select; ?>"));
                            $.each(data, function (key, value) {
                                $('#input-city-id')
                                        .append($("<option></option>")
                                                .attr("value", value.zone_id)
                                                .text(value.name));
                            });
                            if (city_id != '') {
                                $("#input-city-id option[value='" + city_id + "']").prop('selected', true);
                            }
                            ;
                        })
                        .done(function () {
                            // console.log("success");
                            // -- Hide loader
                        })
                        .fail(function (jqXHR, textStatus, errorThrown) {
                            console.log("error");
                            console.log(textStatus, errorThrown);
                        })
                        .always(function () {
                            // console.log("complete");
                        });
            }
        }

        $('#input-country-id').change(function (event) {
            getCities($('#input-country-id').val());
        });
//--></script>

    <script type="text/javascript"><!--

        var stores = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'index.php?route=mall/main_store/autocomplete&token=<?php echo $token; ?>&rand=' + Math.random() + '&filter_name=%QUERY',
                wildcard: '%QUERY',
                filter: function (list) {
                    return $.map(list, function (v, i) {
                        return {store_id: v.store_id, name: v.name};
                    });
                }
            }
        });
        stores.initialize();
        $('#input-sub-stores').tagsinput({
            itemValue: 'store_id',
            itemText: 'name',
            cache: false,
            typeaheadjs: {
                name: 'stores',
                displayKey: 'name',
                cache: false,
                source: stores.ttAdapter()
            }
        });




        var store_category = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'index.php?route=mall/store_category/autocomplete&token=<?php echo $token; ?>&rand=' + Math.random() + '&filter_name=%QUERY',
                wildcard: '%QUERY',
                filter: function (list) {
                    return $.map(list, function (v, i) {
                        return {store_category_id: v.store_category_id, name: v.name};
                    });
                }
            }
        });
        store_category.initialize();
        $('#input-store_category').tagsinput({
            itemValue: 'store_category_id',
            itemText: 'name',
            cache: false,
            typeaheadjs: {
                name: 'store_category',
                displayKey: 'name',
                cache: false,
                source: store_category.ttAdapter()
            }
        });

        var brands = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'index.php?route=mall/brand/autocomplete&token=<?php echo $token; ?>&filter_keyword=%QUERY',
                wildcard: '%QUERY',
                filter: function (list) {
                    return $.map(list, function (v, i) {
                        return {brand_id: v.id, name: v.name};
                    });
                }
            }
        });
        brands.initialize();
        $('#input-brands').tagsinput({
            itemValue: 'brand_id',
            itemText: 'name',
            cache: false,
            interactive:false,
            typeaheadjs: {
                name: 'brands',
                displayKey: 'name',
                cache: false,
                source: brands.ttAdapter()
            }
        });


        var categories = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'index.php?route=catalog/category/autocompletemixedlevel&token=<?php echo $token; ?>&filter_name=%QUERY',
                wildcard: '%QUERY',
                filter: function (list) {
                    return $.map(list, function (v, i) {
                        return {category_id: v.category_id, name: v.name};
                    });
                }
            }
        });
        categories.initialize();
        $('#input-categories').tagsinput({
            itemValue: 'category_id',
            itemText: 'name',
            cache: false,
            typeaheadjs: {
                name: 'related_categories',
                displayKey: 'name',
                cache: false,
                source: categories.ttAdapter()
            }
        });

        //-->
    </script>

<?php if (isset($sub_stores) && !empty($sub_stores)) { ?>
        <script type="text/javascript">
            var subStoresArray = <?= json_encode($sub_stores) ?>;
            if (subStoresArray.length > 0) {
                $.each(subStoresArray, function (index, value) {
                    $('#input-sub-stores').tagsinput('add', {"store_id": value.store_id, "name": "" + value.name.replace(/'/g, "\\'") + ""});
                });
            }
        </script>
<?php } ?>


<?php if (isset($store_category) && !empty($store_category)) { ?>
        <script type="text/javascript">
            var storeCategoriesArray = <?= json_encode($store_category) ?>;
            if (storeCategoriesArray.length > 0) {
                $.each(storeCategoriesArray, function (index, value) {
                    $('#input-store_category').tagsinput('add', {"store_category_id": value.store_category_id, "name": "" + value.name.replace(/'/g, "\\'") + ""});
                });
            }
        </script>
<?php } ?>

<?php if (isset($brands) && !empty($brands)) { ?>
<script type="text/javascript">
    var brandsArray = <?= json_encode($brands) ?>;
    if (brandsArray.length > 0) {
        $.each(brandsArray, function (index, value) {
            $('#input-brands').tagsinput('add', {"brand_id": value.brand_id, "name": "" + value.name.replace(/'/g, "\\'") + ""});
                });
            }
        </script>
<?php } ?>

<?php if (isset($related_categories) && !empty($related_categories)) { ?>
<script type="text/javascript">
    var categoriesArray = <?= json_encode($related_categories) ?>;
    if (categoriesArray.length > 0) {
        $.each(categoriesArray, function (index, value) {
            $('#input-categories').tagsinput('add', {"category_id": value.category_id, "name": "" + value.name.replace(/'/g, "\\'") + ""});
        });
    }
</script>
<?php } ?>


    <script type="text/javascript"><!--
        $('input[name=\'path\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&rand=' + Math.random() + '&rand=' + Math.random() + '&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    cache: false,
                    success: function (json) {
                        json.unshift({
                            category_id: 0,
                            name: '<?php echo $text_none; ?>'
                        });
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['category_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'path\']').val(item['label']);
                $('input[name=\'parent_id\']').val(item['value']);
            }
        });
        //--></script>
    <script type="text/javascript"><!--
        $('input[name=\'filter\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&rand=' + Math.random() + '&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    cache: false,
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                cache: false,
                                label: item['name'],
                                value: item['filter_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'filter\']').val('');
                $('#category-filter' + item['value']).remove();
                $('#category-filter').append('<div id="category-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="category_filter[]" value="' + item['value'] + '" /></div>');
            }
        });
        $('#category-filter').delegate('.fa-minus-circle', 'click', function () {
            $(this).parent().remove();
        });
        //--></script>
    <script type="text/javascript"><!--

        <?php foreach ($languages as $language) { ?>
        // $("#input-description<?php echo $language['language_id']; ?>").summernote();
        CKEDITOR.replace("input-description<?php echo $language['language_id']; ?>", {
            filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
            filebrowserWindowWidth: 800,
            filebrowserWindowHeight: 500
        });
<?php } ?>
        $('#language a:first').tab('show');

        // Brand
        /*$('input[name=\'brand\']').autocomplete({
            'source': function (request, response) {

                $.ajax({
                    url: 'index.php?route=mall/brand/autocomplete&token=<?php echo $token; ?>&filter_keyword=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        json.unshift({
                            id: 0,
                            name: '<?php echo $text_none; ?>'
                        });

                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'brand\']').val(item['label']);
                $('input[name=\'brand_id\']').val(item['value']);
            }
        });

        // Category
        $('input[name=\'category\']').autocomplete({
            'source': function (request, response) {
              if($('input[name=\'show_related_category\']').is(":checked")){
                $.ajax({
                    url: 'index.php?route=catalog/category/autocompleteforproductform&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['category_id']
                            }
                        }));
                    }
                });
              }
            },
            'select': function (item) {
                $('input[name=\'category\']').val(item['label']);
                $('input[name=\'related_category\']').val(item['value']);

            }
        });*/

        $('input[name=\'show_related_category\']').on('click',function(){
          if(! $(this).is(':checked')){
              $('#category-text').removeClass('hidden');
              $('#category-select').addClass('hidden');
          }else{
              $('#category-text').addClass('hidden');
              $('#category-select').removeClass('hidden');
          }
        });

        $('input[name=\'show_related_brand\']').on('click',function(){
          if(! $(this).is(':checked')){
              $('#brand-text').removeClass('hidden');
              $('#brand-select').addClass('hidden');
          }else{
              $('#brand-text').addClass('hidden');
              $('#brand-select').removeClass('hidden');
          }
        });


        $(document).ready(function(){
          if(! $('input[name=\'show_related_brand\']').is(":checked")){
            $('#brand-text').removeClass('hidden');
            $('#brand-select').addClass('hidden');
          }

          if(! $('input[name=\'show_related_category\']').is(":checked")){
            $('#category-text').removeClass('hidden');
            $('#category-select').addClass('hidden');

          }

        });


        //--></script></div>
<?php echo $footer; ?>
