<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </a>

                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="(checkForDelete()) ? (confirm('<?php echo $text_confirm; ?>') ? $('#form-tip').submit() : false) : false;">
                    <i class="fa fa-trash-o"></i>
                </button>

            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
              <?php ##filers## ?>
              <div class="well">
                    <div class="row">
                        <form action="index.php" method="get">
                            <input type="hidden" name="route" value="mall/keyword">
                            <input type="hidden" name="token" value="<?php echo $token; ?>">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                    <input type="text" name="filter_filter" value="<?php echo $filter_filter; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4 pull-right">
                                <div class="form-group">
                                    <button type="submit" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                  <?php ## End Of filters # ?>


                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-tip">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center">
                                        <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                    </td>
                                    <td class="text-left">
                                          <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                    </td>
                                    <td class="text-left">
                                          <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                                    </td>
                                    <td class="text-left">
                                          <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                                    </td>
                                    <td class="text-left">
                                          <?php echo $column_user_id; ?>
                                    </td>
                                    <td class="text-left">
                                          <?php echo $column_last_mod_id; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $column_action; ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($items)) { ?>
                                    <?php foreach ($items as $item) { ?>
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" name="selected[]" value="<?php echo $item['keyword_id']; ?>"
                                                <?php echo ((isset($item["keyword_id"]) && !empty($item["keyword_id"]) && in_array($item["keyword_id"], $selected)) ? "checked=\"checked\"" : ""); ?>
                                                       />
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['name']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['date_added'] ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['date_modified']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['user_add'] ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['user_modify']; ?>
                                            </td>
                                            <td class="text-left">
                                                <a href="<?php echo $item['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php echo $footer; ?>

<script type="text/javascript">
	function checkForDelete(){
		return ($('input[name="selected[]"]:checked').length > 0);
	}
</script>
