<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </a>
                <!--
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="(checkForDelete()) ? (confirm('<?php echo $text_confirm; ?>') ? $('#form-mall').submit() : false) : false;">
                    <i class="fa fa-trash-o"></i>
                </button>
                -->
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>



        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                <input type="text" name="filter_keyword" value="<?php echo $filter_keyword; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-country"><?php echo $entry_country; ?></label>
                                <select name="filter_country_id" id="input-country" class="form-control">
                                    <option value="*"><?php echo $text_select_all; ?></option>
                                    <?php foreach ($countries as $row) { ?>
                                        <?php if ($filter_country_id == $row['country_id']) { ?>
                                            <option value="<?php echo $row['country_id'] ?>" selected="selected"><?php echo $row['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $row['country_id'] ?>"><?php echo $row['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-city"><?php echo $entry_city; ?></label>
                                <select name="filter_city_id" id="input-city" class="form-control">
                                    <option value="*"><?php echo $text_select_all; ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-mall"><?php echo $entry_mall; ?></label>
                                <select name="filter_mall_id" id="input-mall" class="form-control">
                                    <option value="*"><?php echo $text_select_all; ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-city"><?php echo $entry_category; ?></label>
                                <select name="filter_category" id="input-filter_category" class="form-control">
                                    <option value="*"><?php echo $text_select_all; ?></option>
                                    <?php foreach ($categories as $row) { ?>
                                        <option <?php echo $row['category_id'] == $filter_category ? "selected" : "" ?> value="<?php echo $row['category_id'] ?>"><?php echo $row['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="form-group">

                            </div>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-mall">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center">
                                        <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'name') { ?>
                                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'sort_order') { ?>
                                            <a href="<?php echo $sort_sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_sort_order; ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $column_products_count; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $column_creat_date; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $column_update_date; ?>
                                    </td>
                                    <td class="text-center"><?php echo $column_user_id; ?></td>
                                    <td class="text-center"><?php echo $column_last_mod_id; ?></td>

                                    <td class="text-left">
                                        <?php echo $column_action; ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($stores) { ?>
                                    <?php foreach ($stores as $value) { ?>
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" name="selected[]" value="<?php echo $value['main_store_id']; ?>" <?php echo ((isset($value['main_store_id']) && !empty($value['main_store_id']) && in_array($value['main_store_id'], $selected)) ? "checked='checked'" : ""); ?> />
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['name']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['sort_order']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['products_count']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['date_added']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['date_modified']; ?>
                                            </td>
                                            <td class="text-center"><b><?php echo $value['user_add']; ?></b></td>
                                            <td class="text-center"><b><?php echo $value['user_modify']; ?></b></td>

                                            <td class="text-left">
                                                <a href="<?php echo $value['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>



    </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
    /*
     function checkForDelete() {
     return ($('input[name="selected[]"]:checked').length > 0);
     }
     */
</script>
<script type="text/javascript">
<!--
    $('#button-filter').on('click', function () {
        var url = 'index.php?route=mall/main_store&token=<?php echo $token; ?>';
        var filter_keyword = $('input[name=\'filter_keyword\']').val();
        if (filter_keyword) {
            url += '&filter_keyword=' + encodeURIComponent(filter_keyword);
        }
        var filter_category = $('select[name=\'filter_category\']').val();
        if (filter_category != '*') {
            url += '&filter_category=' + encodeURIComponent(filter_category);
        }
        var filter_mall_id = $('select[name=\'filter_mall_id\']').val();
        if (filter_mall_id != '*') {
            url += '&filter_mall_id=' + encodeURIComponent(filter_mall_id);
        }
        var filter_city_id = $('select[name=\'filter_city_id\']').val();
        if (filter_city_id != '*') {
            url += '&filter_city_id=' + encodeURIComponent(filter_city_id);
        }
        var filter_country_id = $('select[name=\'filter_country_id\']').val();
        if (filter_country_id != '*') {
            url += '&filter_country_id=' + encodeURIComponent(filter_country_id);
        }

        location = url;
    });
//-->
</script>
<script type="text/javascript">
<!--
// -- on page open check for country and city pre select values
    getCities('<?php echo $filter_country_id ?>', '<?php echo $filter_city_id ?>');

    function getMalls(city_id, mall_id) {
        city_id = typeof city_id !== 'undefined' ? city_id : '';
        mall_id = typeof mall_id !== 'undefined' ? mall_id : '';
        if (city_id != '') {
            //-- show loader
            $.ajax({
                url: 'index.php?route=mall/mall/getMalls&token=<?php echo $token; ?>&filter_city_id=' + $('#input-city').val(),
                dataType: 'json'
            })
                    .success(function (data) {
                        // console.log("success");
                        // console.log(data);
                        $('#input-mall')
                                .find('option')
                                .remove()
                                .end();
                        $('#input-mall')
                                .append($("<option></option>")
                                        .attr("value", "*")
                                        .text("<?php echo $text_select_all; ?>"));
                        $.each(data, function (key, value) {
                            $('#input-mall')
                                    .append($("<option></option>")
                                            .attr("value", value.mall_id)
                                            .text(value.name));
                        });
                        if (mall_id != '') {
                            $("#input-mall option[value='" + mall_id + "']").prop('selected', true);
                        }
                        ;
                    })
                    .done(function () {
                        // console.log("success");
                        // -- Hide loader
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log("error");
                        console.log(textStatus, errorThrown);
                    })
                    .always(function () {
                        // console.log("complete");
                    });
        }
    }
    function getCities(country_id, city_id) {
        country_id = typeof country_id !== 'undefined' ? country_id : '';
        city_id = typeof city_id !== 'undefined' ? city_id : '';
        if (country_id != '') {
            //-- show loader
            $.ajax({
                url: 'index.php?route=mall/mall/getZone&token=<?php echo $token; ?>&country_id=' + $('#input-country').val(),
                dataType: 'json'/*,
                 data: {param1: 'value1'},*/
            })
                    .success(function (data) {
                        // console.log("success");
                        // console.log(data);
                        $('#input-city')
                                .find('option')
                                .remove()
                                .end();
                        $('#input-city')
                                .append($("<option></option>")
                                        .attr("value", "*")
                                        .text("<?php echo $text_select_all; ?>"));
                        $.each(data, function (key, value) {
                            $('#input-city')
                                    .append($("<option></option>")
                                            .attr("value", value.zone_id)
                                            .text(value.name));
                        });
                        if (city_id != '') {
                            $("#input-city option[value='" + city_id + "']").prop('selected', true);
                        }
                        ;

                        getMalls('<?php echo $filter_city_id ?>', '<?php echo $filter_mall_id ?>');    //Call to preselect Malls upone cities

                    })
                    .done(function () {
                        // console.log("success");
                        // -- Hide loader
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log("error");
                        console.log(textStatus, errorThrown);
                    })
                    .always(function () {
                        // console.log("complete");
                    });
        }
    }

    $('#input-country').change(function (event) {
        getCities($('#input-country').val());
    });
    $('#input-city').change(function (event) {
        getMalls($('#input-city').val());
    });
//-->
</script> 