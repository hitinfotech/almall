<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-shop" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-shop" class="form-horizontal">
                    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab_attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane active" id="tab-general">
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                    <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="shop_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($shop_description[$language['language_id']]) ? $shop_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_name[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="shop_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($shop_description[$language['language_id']]) ? $shop_description[$language['language_id']]['description'] : ''; ?></textarea>
                                                <?php if (isset($error_description[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_description[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-open<?php echo $language['language_id']; ?>"><?php echo $entry_open; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="shop_description[<?php echo $language['language_id']; ?>][open]" placeholder="<?php echo $entry_open; ?>" id="input-open<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($shop_description[$language['language_id']]) ? $shop_description[$language['language_id']]['open'] : ''; ?></textarea>
                                                <?php if (isset($error_description[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_description[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-location<?php echo $language['language_id']; ?>"><?php echo $entry_location; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="shop_description[<?php echo $language['language_id']; ?>][location]" value="<?php echo isset($shop_description[$language['language_id']]) ? $shop_description[$language['language_id']]['location'] : ''; ?>" placeholder="<?php echo $entry_location; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_location[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_location[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <?php /* <!-- <div class="form-group">
                                          <label class="col-sm-2 control-label" for="input-meta_tag"><?php echo $entry_meta_tag; ?></label>
                                          <div class="col-sm-10">
                                          <input type="text" name="meta_tag" value="<?php echo $meta_tag; ?>" placeholder="<?php echo $entry_meta_tag; ?>" id="input-meta_tag" class="form-control" />
                                          </div>
                                          </div> --> */ ?>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-meta_title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="shop_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($shop_description[$language['language_id']]['meta_title']) ? $shop_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta_title<?php echo $language['language_id']; ?>" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-meta_keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="shop_description[<?php echo $language['language_id']; ?>][meta_keyword]" value="<?php echo isset($shop_description[$language['language_id']]['meta_keyword']) ? $shop_description[$language['language_id']]['meta_keyword'] : ''; ?>" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta_keyword<?php echo $language['language_id']; ?>" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-meta_description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="shop_description[<?php echo $language['language_id']; ?>][meta_description]" value="<?php echo isset($shop_description[$language['language_id']]['meta_description']) ? $shop_description[$language['language_id']]['meta_description'] : ''; ?>" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta_description<?php echo $language['language_id']; ?>" class="form-control" />
                                            </div>
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_attribute">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-country_id"><?php echo $entry_country_id; ?></label>
                                <div class="col-sm-10">
                                    <select name="country_id" id="input-country_id" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($countries as $key => $country) { ?>
                                            <option value="<?php echo $key; ?>" <?php if (isset($country_id) && $key == $country_id) { ?>selected<?php } ?>><?php echo $country; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-city_id"><?php echo $entry_city_id; ?></label>
                                <div class="col-sm-10">
                                    <select name="city_id" id="input-city_id" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-mall_id"><?php echo $entry_mall_id; ?></label>
                                <div class="col-sm-10">
                                    <select name="mall_id" id="input-mall_id" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($malls as $row) { ?>
                                            <?php if ($row['mall_id'] == $mall_id) { ?>
                                                <option value="<?php echo $row['mall_id'] ?>" selected=""><?php echo $row['name'] ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $row['mall_id'] ?>"><?php echo $row['name'] ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <?php if (isset($error_mall_id[$language['language_id']])) { ?>
                                        <div class="text-danger"><?php echo $error_mall_id[$language['language_id']]; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-main_store_id"><?php echo $entry_main_store_id; ?></label>
                                <div class="col-sm-10">
                                    <select name="main_store_id" id="input-main_store_id" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($main_stores as $main_store) { ?>
                                            <?php if ($main_store_id == $main_store['main_store_id']) { ?>
                                                <option value="<?php echo $main_store['main_store_id'] ?>" selected ><?php echo $main_store['name']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $main_store['main_store_id'] ?>"><?php echo $main_store['name']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-store_category"><?php echo $entry_form_store_category; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="store_category[]" id="input-store_category"  multiple  placeholder="<?php echo $entry_form_store_category; ?>"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-store_brands"><?php echo $entry_store_brands; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="store_brands[]" id="input-store_brands"  multiple  placeholder="<?php echo $entry_store_brands; ?>"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-landing"><?php echo $entry_landing; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="landing" value="<?php echo $landing; ?>" placeholder="<?php echo $entry_landing; ?>" id="input-landing" class="form-control" />
                                    <?php if (isset($error_landing)) { ?>
                                        <div class="text-danger"><?php echo $error_landing; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-phone"><?php echo $entry_phone; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="phone" value="<?php echo $phone; ?>" placeholder="<?php echo $entry_phone; ?>" id="input-website" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-website"><?php echo $entry_website; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="website" value="<?php echo $website; ?>" placeholder="<?php echo $entry_website; ?>" id="input-website" class="form-control" />
                                    <?php if (isset($error_website)) { ?>
                                        <div class="text-danger"><?php echo $error_website; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-social_jeeran"><?php echo $entry_social_jeeran; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="social_jeeran" value="<?php echo $social_jeeran; ?>" placeholder="<?php echo $entry_social_jeeran; ?>" id="input-social_jeeran" class="form-control" />
                                    <?php if (isset($error_social_jeeran)) { ?>
                                        <div class="text-danger"><?php echo $error_social_jeeran; ?></div>
                                    <?php } ?>              
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-social_fb"><?php echo $entry_social_fb; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="social_fb" value="<?php echo $social_fb; ?>" placeholder="<?php echo $entry_social_fb; ?>" id="input-social_fb" class="form-control" />
                                    <?php if (isset($error_social_fb)) { ?>
                                        <div class="text-danger"><?php echo $error_social_fb; ?></div>
                                    <?php } ?>              
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-social_tw"><?php echo $entry_social_tw; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="social_tw" value="<?php echo $social_tw; ?>" placeholder="<?php echo $entry_social_tw; ?>" id="input-social_tw" class="form-control" />
                                    <?php if (isset($error_social_tw)) { ?>
                                        <div class="text-danger"><?php echo $error_social_tw; ?></div>
                                    <?php } ?>                
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-latitude"><?php echo $entry_latitude; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="latitude" value="<?php echo $latitude; ?>" placeholder="<?php echo $entry_latitude; ?>" id="input-latitude" class="form-control" />
                                    <small><a target="_blank" href="https://www.google.jo/?gfe_rd=cr&ei=_YCxV7WBNuna8Ae3gJn4Cg&gws_rd=ssl#q=how+to+fill+right+values+for+google+maps">View google map documentation</a></small>
                                    <?php if ($error_google_latitude) { ?>
                                        <div class="text-danger"><?php echo $error_google_latitude; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-longitude"><?php echo $entry_longitude; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="longitude" value="<?php echo $longitude; ?>" placeholder="<?php echo $entry_longitude; ?>" id="input-longitude" class="form-control" />
                                    <small><a target="_blank" href="https://www.google.jo/?gfe_rd=cr&ei=_YCxV7WBNuna8Ae3gJn4Cg&gws_rd=ssl#q=how+to+fill+right+values+for+google+maps">View google map documentation</a></small>
                                    <?php if ($error_google_longitude) { ?>
                                        <div class="text-danger"><?php echo $error_google_longitude; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-zoom"><?php echo $entry_zoom; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="zoom" value="<?php echo $zoom; ?>" placeholder="<?php echo $entry_zoom; ?>" id="input-zoom" class="form-control" />
                                    <?php if ($error_google_zoom) { ?>
                                        <div class="text-danger"><?php echo $error_google_zoom; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-url"><?php echo $entry_url; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="url" value="<?php echo $url; ?>" placeholder="<?php echo $entry_url; ?>" id="input-url" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-ssl"><?php echo $entry_ssl; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="ssl" value="<?php echo $ssl; ?>" placeholder="<?php echo $entry_ssl; ?>" id="input-ssl" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-display"><?php echo $entry_display; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="display" value="<?php echo $display; ?>" placeholder="<?php echo $entry_display; ?>" id="input-display" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="status" id="input-status" class="form-control">
                                        <option value="1" <?php
                                        if ($status == '1') {
                                            echo "selected=\"selected\"";
                                        }
                                        ?>><?php echo $text_enabled; ?></option>
                                        <option value="0" <?php
                                        if ($status == '0') {
                                            echo "selected=\"selected\"";
                                        }
                                        ?>><?php echo $text_disabled; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                                </div>
                            </div>

                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var store_category = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/store_category/autocomplete&token=<?php echo $token; ?>&rand=' + Math.random() + '&filter_name=%QUERY',
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {store_category_id: v.store_category_id, name: v.name};
                });
            }
        }
    });
    store_category.initialize();
    $('#input-store_category').tagsinput({
        itemValue: 'store_category_id',
        itemText: 'name',
        cache: false,
        typeaheadjs: {
            name: 'store_category',
            displayKey: 'name',
            cache: false,
            source: store_category.ttAdapter()
        }
    });

    var store_brands = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/brand/autocomplete&token=<?php echo $token; ?>&rand=' + Math.random() + '&filter_keyword=%QUERY',
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {brand_id: v.id, name: v.name};
                });
            }
        }
    });
    store_brands.initialize();
    $('#input-store_brands').tagsinput({
        itemValue: 'brand_id',
        itemText: 'name',
        cache: false,
        typeaheadjs: {
            name: 'store_brands',
            displayKey: 'name',
            cache: false,
            source: store_brands.ttAdapter()
        }
    });

</script>

<?php if (isset($store_category) && !empty($store_category)) { ?>
    <script type="text/javascript">
        var storeCategoriesArray = <?= json_encode($store_category) ?>;
        if (storeCategoriesArray.length > 0) {
            $.each(storeCategoriesArray, function (index, value) {
                $('#input-store_category').tagsinput('add', {"store_category_id": value.store_category_id, "name": "" + value.name.replace(/'/g, "\\'") + ""});
            });
        }
    </script>
<?php } ?>

<?php if (isset($store_brands) && !empty($store_brands)) { ?>
    <script type="text/javascript">
        var storeBrandsArray = <?= json_encode($store_brands) ?>;
        if (storeBrandsArray.length > 0) {
            $.each(storeBrandsArray, function (index, value) {
                $('#input-store_brands').tagsinput('add', {"brand_id": value.brand_id, "name": "" + value.name.replace(/'/g, "\\'") + ""});
            });
        }
    </script>
<?php } ?>

<script type="text/javascript"><!--
    $('input[name=\'path\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    json.unshift({
                        category_id: 0,
                        name: '<?php echo $text_none; ?>'
                    });

                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['category_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'path\']').val(item['label']);
            $('input[name=\'parent_id\']').val(item['value']);
        }
    });
    //--></script> 

<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
        // $("#input-description<?php echo $language['language_id']; ?>").summernote();
        CKEDITOR.replace("input-description<?php echo $language['language_id']; ?>", {
            filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
            filebrowserWindowWidth: 800,
            filebrowserWindowHeight: 500
        });
<?php } ?>
    $('#language a:first').tab('show');
    //--></script>

<script type="text/javascript"><!--
    // -- on page open check for country and city pre select values
    getCities('<?php echo $country_id ?>', '<?php echo $city_id ?>');

    function getMainStores(country_id, main_store_id) {
        main_store_id = typeof main_store_id !== 'undefined' ? main_store_id : '';
        country_id = typeof country_id !== 'undefined' ? country_id : '';
        if (country_id != '') {
            //-- show loader
            $.ajax({
                url: 'index.php?route=mall/main_store/getMainStores&token=<?php echo $token; ?>&filter_country_id=' + country_id,
                dataType: 'json'/*,
                 data: {param1: 'value1'},*/
            })
                    .success(function (data) {
                        // console.log("success");
                        // console.log(data);
                        $('#input-main_store_id').find('option').remove().end();
                        $('#input-main_store_id').append($("<option></option>").attr("value", "").text("<?php echo $text_select; ?>"));
                        $.each(data, function (key, value) {
                            $('#input-main_store_id').append($("<option></option>").attr("value", value.main_store_id).text(value.name));
                        });
                        if (main_store_id != '') {
                            $("#input-main_store_id option[value='" + main_store_id + "']").prop('selected', true);
                        }
                        ;
                    })
                    .done(function () {
                        // console.log("success");
                        // -- Hide loader
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log("error");
                        console.log(textStatus, errorThrown);
                    })
                    .always(function () {
                        // console.log("complete");
                    });
        }
    }

    function getMalls(city_id, mall_id) {
        mall_id = typeof mall_id !== 'undefined' ? mall_id : '';
        city_id = typeof city_id !== 'undefined' ? city_id : '';
        if (city_id != '') {
            //-- show loader
            $.ajax({
                url: 'index.php?route=mall/mall/getMalls&token=<?php echo $token; ?>&filter_city_id=' + city_id,
                dataType: 'json'/*,
                 data: {param1: 'value1'},*/
            })
                    .success(function (data) {
                        // console.log("success");
                        // console.log(data);
                        $('#input-mall_id')
                                .find('option')
                                .remove()
                                .end();
                        $('#input-mall_id')
                                .append($("<option></option>")
                                        .attr("value", "")
                                        .text("<?php echo $text_select; ?>"));
                        $.each(data, function (key, value) {
                            $('#input-mall_id')
                                    .append($("<option></option>")
                                            .attr("value", value.mall_id)
                                            .text(value.name));
                        });
                        if (mall_id != '') {
                            $("#input-mall_id option[value='" + mall_id + "']").prop('selected', true);
                        }
                        ;
                    })
                    .done(function () {
                        // console.log("success");
                        // -- Hide loader
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log("error");
                        console.log(textStatus, errorThrown);
                    })
                    .always(function () {
                        // console.log("complete");
                    });
        }
    }

    function getCities(country_id, city_id) {
        country_id = typeof country_id !== 'undefined' ? country_id : '';
        city_id = typeof city_id !== 'undefined' ? city_id : '';
        if (country_id != '') {
            //-- show loader
            $.ajax({
                url: 'index.php?route=mall/mall/getZone&token=<?php echo $token; ?>&country_id=' + country_id,
                dataType: 'json'/*,
                 data: {param1: 'value1'},*/
            })
                    .success(function (data) {
                        // console.log("success");
                        // console.log(data);
                        $('#input-city_id')
                                .find('option')
                                .remove()
                                .end();
                        $('#input-city_id')
                                .append($("<option></option>")
                                        .attr("value", "")
                                        .text("<?php echo $text_select; ?>"));
                        $.each(data, function (key, value) {
                            $('#input-city_id')
                                    .append($("<option></option>")
                                            .attr("value", value.zone_id)
                                            .text(value.name));
                        });
                        if (city_id != '') {
                            $("#input-city_id option[value='" + city_id + "']").prop('selected', true);
                            getMalls('<?php echo $city_id ?>', '<?php echo $mall_id ?>');
                        }

                    })
                    .done(function () {
                        // console.log("success");
                        // -- Hide loader
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log("error");
                        console.log(textStatus, errorThrown);
                    })
                    .always(function () {
                        // console.log("complete");
                    });
        }
    }

    $('#input-country_id').change(function (event) {
        getCities($('#input-country_id').val());
        getMainStores($('#input-country_id').val());
    });
    $('#input-city_id').change(function (event) {
        getMalls($('#input-city_id').val());
    });
    //--></script> 
<script type="text/javascript"><!--
    $('input[name=\'filter\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['filter_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'filter\']').val('');

            $('#category-filter' + item['value']).remove();

            $('#category-filter').append('<div id="category-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="category_filter[]" value="' + item['value'] + '" /></div>');
        }
    });

    $('#category-filter').delegate('.fa-minus-circle', 'click', function () {
        $(this).parent().remove();
    });
    //--></script> 
<script type="text/javascript"><!--
    $('#language a:first').tab('show');
    //--></script>
<?php echo $footer; ?>