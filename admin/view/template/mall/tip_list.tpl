<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </a> 
                <!--
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="(checkForDelete()) ? (confirm('<?php echo $text_confirm; ?>') ? $('#form-tip').submit() : false) : false;">
                    <i class="fa fa-trash-o"></i>
                </button>
                -->
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">

                        <input type="hidden" name="route" value="mall/tip">
                        <input type="hidden" name="token" value="<?php echo $token; ?>">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                <input type="text" name="filter_filter" value="<?php echo $filter_filter; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <select name="filter_status" id="input-status" class="form-control">
                                    <option value="*"><?php echo $select_status; ?></option>
                                    <?php if ($filter_status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                    <?php } ?>
                                    <?php if (!$filter_status && !is_null($filter_status)) { ?>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-country"><?php echo $entry_country_id; ?></label>
                                <select name="filter_country_id" id="input-country_id" class="form-control">
                                    <option value="*"><?php echo $select_country; ?></option>
                                    <?php foreach ($countries as $row) { ?>
                                        <?php if ($filter_country_id == $row['country_id']) { ?>
                                            <option value="<?php echo $row['country_id'] ?>" selected="selected"><?php echo $row['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $row['country_id'] ?>"><?php echo $row['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <button id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                            </div>
                        </div>
                        <!--                        <div class="col-sm-3">
                                                    
                                                </div>-->

                    </div>
                </div>
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-tip">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center">
                                        <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                    </td>
                                    <td class="text-center"><?php echo $column_image; ?></td>
                                    <td class="text-left">
                                        <?php if ($sort == 'td.name') { ?>
                                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 't.status') { ?>
                                            <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 't.author') { ?>
                                            <a href="<?php echo $sort_author; ?>" class="<?php echo strtolower($order); ?>"><?php echo $entry_author; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_author; ?>"><?php echo $entry_author; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 't.date_added') { ?>
                                            <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 't.date_modified') { ?>
                                            <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 't.sort_order') { ?>
                                            <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_order; ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php echo $column_user_id; ?></td>
                                    <td class="text-center"><?php echo $column_last_mod_id; ?></td>

                                    <td class="text-left">
                                        <?php echo $column_action; ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($items)) { ?>
                                    <?php foreach ($items as $item) { ?>
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" name="selected[]" value="<?php echo $item['tip_id']; ?>"
                                                <?php echo ((isset($item['tip_id']) && !empty($item['tip_id']) && in_array($item['tip_id'], $selected)) ? "checked=\"checked\"" : ""); ?>
                                                       />
                                            </td>
                                            <td class="text-center"><?php if ($item['image']) { ?>
                                                    <img src="<?php echo $item['image']; ?>" alt="<?php echo $item['name']; ?>" class="img-thumbnail" />
                                                <?php } else { ?>
                                                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                                                <?php } ?></td>
                                            <td class="text-left">
                                                <?php echo $item['name']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php if ($item['status']) { ?> Enabled <?php } else { ?> Disabled <?php } ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['author']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['date_added']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['date_modified']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['sort_order']; ?>
                                            </td>
                                            <td class="text-center"><b><?php echo $item['user_add']; ?></b></td>
                                            <td class="text-center"><b><?php echo $item['user_modify']; ?></b></td>

                                            <td class="text-left">
                                                <a href="<?php echo $item['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
<!--
    /*
     function checkForDelete() {
     return ($('input[name="selected[]"]:checked').length > 0);
     }
     */
//-->
</script>
<script type="text/javascript">
<!--
// -- on page open check for country and city pre select values

    function getMainStores(city_id, main_store_id) {
        main_store_id = typeof main_store_id !== 'undefined' ? main_store_id : '';
        city_id = typeof city_id !== 'undefined' ? city_id : '';
        if (city_id != '') {
            //-- show loader
            $.ajax({
                url: 'index.php?route=mall/mall/getMainStores&token=<?php echo $token; ?>&filter_city_id=' + city_id,
                dataType: 'json'/*,
                 data: {param1: 'value1'},*/
            })
                    .success(function (data) {
                        // console.log("success");
                        // console.log(data);
                        $('#input-main_store')
                                .find('option')
                                .remove()
                                .end();
                        $('#input-main_store')
                                .append($("<option></option>")
                                        .attr("value", "")
                                        .text("<?php echo $text_select; ?>"));
                        $.each(data, function (key, value) {
                            $('#input-main_store')
                                    .append($("<option></option>")
                                            .attr("value", value.main_store_id)
                                            .text(value.name));
                        });
                        if (main_store_id != '') {
                            $("#input-main_store option[value='" + main_store_id + "']").prop('selected', true);
                        }
                        ;
                    })
                    .done(function () {
                        // console.log("success");
                        // -- Hide loader
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log("error");
                        console.log(textStatus, errorThrown);
                    })
                    .always(function () {
                        // console.log("complete");
                    });
        }
    }

    function getMalls(city_id, mall_id) {
        mall_id = typeof mall_id !== 'undefined' ? mall_id : '';
        city_id = typeof city_id !== 'undefined' ? city_id : '';
        if (city_id != '') {
            //-- show loader
            $.ajax({
                url: 'index.php?route=mall/mall/getMalls&token=<?php echo $token; ?>&filter_city_id=' + city_id,
                dataType: 'json'/*,
                 data: {param1: 'value1'},*/
            })
                    .success(function (data) {
                        // console.log("success");
                        // console.log(data);
                        $('#input-mall')
                                .find('option')
                                .remove()
                                .end();
                        $('#input-mall')
                                .append($("<option></option>")
                                        .attr("value", "")
                                        .text("<?php echo $text_select; ?>"));
                        $.each(data, function (key, value) {
                            $('#input-mall')
                                    .append($("<option></option>")
                                            .attr("value", value.mall_id)
                                            .text(value.name));
                        });
                        if (mall_id != '') {
                            $("#input-mall option[value='" + mall_id + "']").prop('selected', true);
                        }
                        ;
                    })
                    .done(function () {
                        // console.log("success");
                        // -- Hide loader
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log("error");
                        console.log(textStatus, errorThrown);
                    })
                    .always(function () {
                        // console.log("complete");
                    });
        }
    }

    $('#input-country').change(function (event) {
        getCities($('#input-country').val());
    });

//-->
</script> 
<script type="text/javascript"><!--
    $('#button-filter').on('click', function () {
        var url = 'index.php?route=mall/tip&token=<?php echo $token; ?>';

        var filter_filter = $('input[name=\'filter_filter\']').val();
        if (filter_filter) {
            url += '&filter_filter=' + encodeURIComponent(filter_filter);
        }
        var filter_status = $('select[name=\'filter_status\']').val();
        if (filter_status != '*') {
            url += '&filter_status=' + encodeURIComponent(filter_status);
        }
        var filter_country_id = $('select[name=\'filter_country_id\']').val();
        if (filter_country_id != '*') {
            url += '&filter_country_id=' + encodeURIComponent(filter_country_id);
        }

        location = url;
    });
//--></script>
<?php echo $footer; ?>
