<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </a> 
                <!--
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="(checkForDelete()) ? (confirm('<?php echo $text_confirm; ?>') ? $('#form-shop').submit() : false) : false;">
                    <i class="fa fa-trash-o"></i>
                </button>
                -->
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                <input type="text" name="filter_filter" value="<?php echo $filter_filter; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-country"><?php echo $entry_country; ?></label>
                                <select name="filter_country_id" id="input-country" class="form-control">
                                    <option value=""><?php echo $select_country; ?></option>
                                    <?php foreach ($countries as $row) { ?>
                                        <?php if ($filter_country_id == $row['country_id']) { ?>
                                            <option value="<?php echo $row['country_id'] ?>" selected="selected"><?php echo $row['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $row['country_id'] ?>"><?php echo $row['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-city"><?php echo $entry_city; ?></label>
                                <select name="filter_city_id" id="input-city" class="form-control">
                                    <option value=""><?php echo $select_city ?></option>
                                    <?php if(is_array($cities) && !empty($cities)) {?>
                                    <?php foreach ($cities as $row) { ?>
                                        <?php if ($filter_city_id == $row['zone_id']) { ?>
                                            <option value="<?php echo $row['zone_id'] ?>" selected="selected"><?php echo $row['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $row['zone_id'] ?>"><?php echo $row['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-main_store"><?php echo $entry_main_store; ?></label>
                                <select name="filter_main_store_id" id="input-main_store" class="form-control">
                                    <option value=""><?php echo $text_select ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-store_category"><?php echo $entry_store_category; ?></label>
                                <select name="filter_store_category_id" id="input-main_store" class="form-control">
                                    <option value=""><?php echo $text_select ?></option>
                                    <?php foreach ($store_categories as $row) { ?>
                                        <?php if ($filter_store_category_id == $row['store_category_id']) { ?>
                                            <option value="<?php echo $row['store_category_id'] ?>" selected="selected"><?php echo $row['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $row['store_category_id'] ?>"><?php echo $row['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-shop">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center">
                                        <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'md.main_store') { ?>
                                            <a href="<?php echo $sort_main_store; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_main_store; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_main_store; ?>"><?php echo $column_main_store; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'md.name') { ?>
                                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                        <?php } ?>
                                    </td>
                                    
                                    <td class="text-left">
                                        <?php if ($sort == 'zd.name') { ?>
                                            <a href="<?php echo $sort_city; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_city; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_city; ?>"><?php echo $column_city; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'm.products_count') { ?>
                                            <a href="<?php echo $sort_shop; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_products_count; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_shop; ?>"><?php echo $column_products_count; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'm.sort_order') { ?>
                                            <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_order; ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php echo $column_user_id; ?></td>
                                    <td class="text-center"><?php echo $column_last_mod_id; ?></td>

                                    <td class="text-left">
                                        <?php echo $column_action; ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($shops)) { ?>
                                    <?php foreach ($shops as $value) { ?>
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" name="selected[]" value="<?php echo $value['shop_id']; ?>" 
                                                <?php echo ((isset($value['shop_id']) && !empty($value['shop_id']) && in_array($value['shop_id'], $selected)) ? "checked=\"checked\"" : ""); ?> 
                                                       />
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['main_store']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['name']; ?><b> ( <?php echo ($value['status'] == '1') ? 'Published' : 'Not Published'; ?> )</b>
                                            </td>
                                            
                                            <td class="text-left">
                                                <a href="<?php echo $value['filter_by_city']; ?>">
                                                    <?php echo $value['city']; ?>
                                                </a>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['products_count']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $value['sort_order']; ?>
                                            </td>
                                            <td class="text-center"><b><?php echo $value['user_add']; ?></b></td>
                                            <td class="text-center"><b><?php echo $value['user_modify']; ?></b></td>

                                            <td class="text-left">
                                                <a href="<?php echo $value['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
<!--
    $('#button-filter').on('click', function () {
        var url = 'index.php?route=mall/shopdesigner&token=<?php echo $token; ?>';
        var filter_filter = $('input[name=\'filter_filter\']').val();
        if (filter_filter) {
            url += '&filter_filter=' + encodeURIComponent(filter_filter);
        }
        var filter_city_id = $('select[name=\'filter_city_id\']').val();
        if (filter_city_id != '') {
            url += '&filter_city_id=' + encodeURIComponent(filter_city_id);
        }
        var filter_country_id = $('select[name=\'filter_country_id\']').val();
        if (filter_country_id != '') {
            url += '&filter_country_id=' + encodeURIComponent(filter_country_id);
        }
        
        var filter_main_store_id = $('select[name=\'filter_main_store_id\']').val();
        if (filter_main_store_id != '') {
            url += '&filter_main_store_id=' + encodeURIComponent(filter_main_store_id);
        }
        var filter_store_category_id = $('select[name=\'filter_store_category_id\']').val();
        if (filter_store_category_id != '') {
            url += '&filter_store_category_id=' + encodeURIComponent(filter_store_category_id);
        }

        location = url;
    });
//-->
</script>
<script type="text/javascript">
<!--
    /*
     function checkForDelete() {
     return ($('input[name="selected[]"]:checked').length > 0);
     }
     */
//-->
</script>
<script type="text/javascript">
<!--
// -- on page open check for country and city pre select values
    getCities('<?php echo $filter_country_id ?>', '<?php echo $filter_city_id ?>');

    function getMainStores(country_id, city_id, main_store_id) {
        main_store_id = typeof main_store_id !== 'undefined' ? main_store_id : '';
        city_id = typeof city_id !== 'undefined' ? city_id : '';
        country_id = typeof country_id !== 'undefined' ? country_id : '';
        if (city_id != '' || country_id != '') {
            //-- show loader
            $.ajax({
                url: 'index.php?route=mall/mall/getMainStores&token=<?php echo $token; ?>&filter_city_id=' + city_id + '&filter_country_id=' + country_id,
                dataType: 'json'/*,
                 data: {param1: 'value1'},*/
            })
                    .success(function (data) {
                        // console.log("success");
                        // console.log(data);
                        $('#input-main_store')
                                .find('option')
                                .remove()
                                .end();
                        $('#input-main_store')
                                .append($("<option></option>")
                                        .attr("value", "")
                                        .text("<?php echo $text_select; ?>"));
                        $.each(data, function (key, value) {
                            $('#input-main_store')
                                    .append($("<option></option>")
                                            .attr("value", value.main_store_id)
                                            .text(value.name));
                        });
                        if (main_store_id != '') {
                            $("#input-main_store option[value='" + main_store_id + "']").prop('selected', true);
                        }
                        ;
                    })
                    .done(function () {
                        // console.log("success");
                        // -- Hide loader
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log("error");
                        console.log(textStatus, errorThrown);
                    })
                    .always(function () {
                        // console.log("complete");
                    });
        }
    }

    function getMalls(country_id, city_id, mall_id) {
        mall_id = typeof mall_id !== 'undefined' ? mall_id : '';
        city_id = typeof city_id !== 'undefined' ? city_id : '';
        country_id = typeof country_id !== 'undefined' ? country_id : '';
        if (city_id != '' || country_id != '') {
            //-- show loader
            $.ajax({
                url: 'index.php?route=mall/mall/getMalls&token=<?php echo $token; ?>&filter_city_id=' + city_id + '&filter_country_id=' + country_id,
                dataType: 'json'/*,
                 data: {param1: 'value1'},*/
            })
                    .success(function (data) {
                        // console.log("success");
                        // console.log(data);
                        $('#input-mall')
                                .find('option')
                                .remove()
                                .end();
                        $('#input-mall')
                                .append($("<option></option>")
                                        .attr("value", "")
                                        .text("<?php echo $text_select; ?>"));
                        $.each(data, function (key, value) {
                            $('#input-mall')
                                    .append($("<option></option>")
                                            .attr("value", value.mall_id)
                                            .text(value.name));
                        });
                        if (mall_id != '') {
                            $("#input-mall option[value='" + mall_id + "']").prop('selected', true);
                        }
                        ;
                    })
                    .done(function () {
                        // console.log("success");
                        // -- Hide loader
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log("error");
                        console.log(textStatus, errorThrown);
                    })
                    .always(function () {
                        // console.log("complete");
                    });
        }
    }

    function getCities(country_id, city_id) {
        country_id = typeof country_id !== 'undefined' ? country_id : '';
        city_id = typeof city_id !== 'undefined' ? city_id : '';
        if (country_id != '') {
            //-- show loader
            $.ajax({
                url: 'index.php?route=mall/mall/getZone&token=<?php echo $token; ?>&country_id=' + $('#input-country').val(),
                dataType: 'json'/*,
                 data: {param1: 'value1'},*/
            })
                    .success(function (data) {
                        // console.log("success");
                        // console.log(data);
                        $('#input-city')
                                .find('option')
                                .remove()
                                .end();
                        $('#input-city')
                                .append($("<option></option>")
                                        .attr("value", "")
                                        .text("<?php echo $text_select; ?>"));
                        $.each(data, function (key, value) {
                            $('#input-city')
                                    .append($("<option></option>")
                                            .attr("value", value.zone_id)
                                            .text(value.name));
                        });
                        if (city_id != '') {
                            $("#input-city option[value='" + city_id + "']").prop('selected', true);
                            getMalls('<?php echo $filter_country_id ?>', '<?php echo $filter_city_id ?>', '<?php echo $filter_mall_id ?>');
                            getMainStores('<?php echo $filter_country_id ?>', '<?php echo $filter_city_id ?>', '<?php echo $filter_main_store_id ?>');
                        }
                        ;
                    })
                    .done(function () {
                        // console.log("success");
                        // -- Hide loader
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log("error");
                        console.log(textStatus, errorThrown);
                    })
                    .always(function () {
                        // console.log("complete");
                    });
        }
    }

    $('#input-country').change(function (event) {
        getCities($('#input-country').val());
        getMalls($('#input-country').val());
        getMainStores($('#input-country').val());
    });
    $('#input-city').change(function (event) {
        getMalls($('#input-country').val(), $('#input-city').val());
        getMainStores($('#input-country').val(), $('#input-city').val());
    });
//-->
</script> 
<?php echo $footer; ?>