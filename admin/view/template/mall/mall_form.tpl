<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-mall" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-mall" class="form-horizontal">
                    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab_attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane active" id="tab-general">
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                    <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="mall_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($mall_description[$language['language_id']]) ? $mall_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_name[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="mall_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($mall_description[$language['language_id']]) ? $mall_description[$language['language_id']]['description'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-location<?php echo $language['language_id']; ?>"><?php echo $entry_location; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="mall_description[<?php echo $language['language_id']; ?>][location]" value="<?php echo isset($mall_description[$language['language_id']]) ? $mall_description[$language['language_id']]['location'] : ''; ?>" placeholder="<?php echo $entry_location; ?>" id="input-location<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_location[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_location[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-open<?php echo $language['language_id']; ?>"><?php echo $entry_open; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="mall_description[<?php echo $language['language_id']; ?>][open]" placeholder="<?php echo $entry_open; ?>" id="input-open<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($mall_description[$language['language_id']]) ? $mall_description[$language['language_id']]['open'] : ''; ?></textarea>
                                            </div>
                                            <?php if (isset($error_open[$language['language_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_open[$language['language_id']]; ?></div>
                                            <?php } ?>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="mall_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($mall_description[$language['language_id']]) ? $mall_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_meta_title[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_meta_title[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="mall_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($mall_description[$language['language_id']]) ? $mall_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="mall_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($mall_description[$language['language_id']]) ? $mall_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_attribute">

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-parent"><?php echo $entry_country_id; ?></label>
                                <div class="col-sm-10">
                                    <select name="country_id" id="input-country-id" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                        <?php foreach ($countries as $key => $country) { ?>
                                            <option value="<?php echo $key; ?>" <?php if (isset($country_id) && $key == $country_id) { ?>selected<?php } ?>><?php echo $country; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-parent"><?php echo $entry_city_id; ?></label>
                                <div class="col-sm-10">
                                    <select name="city_id" id="input-city-id" class="form-control">
                                        <option value=""><?php echo $text_select; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-phone"><?php echo $entry_phone; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="phone" value="<?php echo $phone; ?>" placeholder="<?php echo $entry_phone; ?>" id="input-phone" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-fax"><?php echo $entry_fax; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-website"><?php echo $entry_website; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="website" value="<?php echo $website; ?>" placeholder="<?php echo $entry_website; ?>" id="input-website" class="form-control" />
                                    <?php if (isset($error_website)) { ?>
                                        <div class="text-danger"><?php echo $error_website; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-social_jeeran"><?php echo $entry_social_jeeran; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="social_jeeran" value="<?php echo $social_jeeran; ?>" placeholder="<?php echo $entry_social_jeeran; ?>" id="input-social_jeeran" class="form-control" />
                                    <?php if (isset($error_social_jeeran)) { ?>
                                        <div class="text-danger"><?php echo $error_social_jeeran; ?></div>
                                    <?php } ?>              
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-social_fb"><?php echo $entry_social_fb; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="social_fb" value="<?php echo $social_fb; ?>" placeholder="<?php echo $entry_social_fb; ?>" id="input-social_fb" class="form-control" />
                                    <?php if (isset($error_social_fb)) { ?>
                                        <div class="text-danger"><?php echo $error_social_fb; ?></div>
                                    <?php } ?>              
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-social_tw"><?php echo $entry_social_tw; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="social_tw" value="<?php echo $social_tw; ?>" placeholder="<?php echo $entry_social_tw; ?>" id="input-social_tw" class="form-control" />
                                    <?php if (isset($error_social_tw)) { ?>
                                        <div class="text-danger"><?php echo $error_social_tw; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php /* <div class="form-group">
                              <label class="col-sm-2 control-label" for="input-template"><?php echo $entry_template; ?></label>
                              <div class="col-sm-10">
                              <input type="text" name="template" value="<?php echo $template; ?>" placeholder="<?php echo $entry_template; ?>" id="input-template" class="form-control" />
                              <?php if (isset($error_template)) { ?>
                              <div class="text-danger"><?php echo $error_template; ?></div>
                              <?php } ?>
                              </div>
                              </div> */ ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $entry_image; ?>" />
                                    <input type="file" name="image" value="<?php echo $image; ?>" id="input-image" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-latitude"><?php echo $entry_latitude; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="latitude" value="<?php echo $latitude; ?>" placeholder="<?php echo $entry_latitude; ?>" id="input-latitude" class="form-control" />
                                    <small><a target="_blank" href="https://www.google.jo/?gfe_rd=cr&ei=_YCxV7WBNuna8Ae3gJn4Cg&gws_rd=ssl#q=how+to+fill+right+values+for+google+maps">View google map documentation</a></small>
                                    <?php if ($error_google_latitude) { ?>
                                        <div class="text-danger"><?php echo $error_google_latitude ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-longitude"><?php echo $entry_longitude; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="longitude" value="<?php echo $longitude; ?>" placeholder="<?php echo $entry_longitude; ?>" id="input-longitude" class="form-control" />
                                    <small><a target="_blank" href="https://www.google.jo/?gfe_rd=cr&ei=_YCxV7WBNuna8Ae3gJn4Cg&gws_rd=ssl#q=how+to+fill+right+values+for+google+maps">View google map documentation</a></small>
                                    <?php if ($error_google_longitude) { ?>
                                        <div class="text-danger"><?php echo $error_google_longitude ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-zoom"><?php echo $entry_zoom; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="zoom" value="<?php echo $zoom; ?>" placeholder="<?php echo $entry_zoom; ?>" id="input-zoom" class="form-control" />
                                    <?php if ($error_google_zoom) { ?>
                                        <div class="text-danger"><?php echo $error_google_zoom ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="status" id="input-status" class="form-control">
                                        <option value="1" <?php if ($status == '1') {
                                        echo "selected=\"selected\"";
                                    } ?>><?php echo $text_enabled; ?></option>
                                        <option value="0" <?php if ($status == '0') {
                                        echo "selected=\"selected\"";
                                    } ?>><?php echo $text_disabled; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                                </div>
                            </div>

                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
        // $("#input-description<?php echo $language['language_id']; ?>").summernote();
        CKEDITOR.replace("input-description<?php echo $language['language_id']; ?>", {
            filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
            filebrowserWindowWidth: 800,
            filebrowserWindowHeight: 500
        });
<?php } ?>
//--></script> 
    <script type="text/javascript"><!--
$('input[name=\'path\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        json.unshift({
                            category_id: 0,
                            name: '<?php echo $text_none; ?>'
                        });

                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['category_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'path\']').val(item['label']);
                $('input[name=\'parent_id\']').val(item['value']);
            }
        });
        //--></script> 
    <script type="text/javascript"><!--
        // -- on page open check for country and city pre select values
        getCities('<?php echo $country_id ?>', '<?php echo $city_id ?>');

        function getCities(country_id, city_id) {
            country_id = typeof country_id !== 'undefined' ? country_id : '';
            city_id = typeof city_id !== 'undefined' ? city_id : '';
            if (country_id != '') {
                //-- show loader
                $.ajax({
                    url: 'index.php?route=mall/mall/getZone&token=<?php echo $token; ?>&country_id=' + $('#input-country-id').val(),
                    dataType: 'json'/*,
                     data: {param1: 'value1'},*/
                })
                        .success(function (data) {
                            // console.log("success");
                            // console.log(data);
                            $('#input-city-id')
                                    .find('option')
                                    .remove()
                                    .end();
                            $('#input-city-id')
                                    .append($("<option></option>")
                                            .attr("value", "")
                                            .text("<?php echo $text_select; ?>"));
                            $.each(data, function (key, value) {
                                $('#input-city-id')
                                        .append($("<option></option>")
                                                .attr("value", value.zone_id)
                                                .text(value.name));
                            });
                            if (city_id != '') {
                                $("#input-city-id option[value='" + city_id + "']").prop('selected', true);
                            }
                            ;
                        })
                        .done(function () {
                            // console.log("success");
                            // -- Hide loader
                        })
                        .fail(function (jqXHR, textStatus, errorThrown) {
                            console.log("error");
                            console.log(textStatus, errorThrown);
                        })
                        .always(function () {
                            // console.log("complete");
                        });
            }
        }

        $('#input-country-id').change(function (event) {
            getCities($('#input-country-id').val());
        });
        //--></script> 
    <script type="text/javascript"><!--
        $('input[name=\'filter\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['filter_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'filter\']').val('');

                $('#category-filter' + item['value']).remove();

                $('#category-filter').append('<div id="category-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="category_filter[]" value="' + item['value'] + '" /></div>');
            }
        });

        $('#category-filter').delegate('.fa-minus-circle', 'click', function () {
            $(this).parent().remove();
        });
        //--></script> 
    <script type="text/javascript"><!--
        $('#language a:first').tab('show');
        //--></script></div>
<?php echo $footer; ?>