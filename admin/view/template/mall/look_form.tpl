<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-look" data-toggle="toollook" title="<?php echo $button_save; ?>"
                        class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
                   class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-look" class="form-horizontal">
                    <!-- { nav-tabs } !-->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab_attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                        <li><a href="#tab_links" data-toggle="tab"><?php echo $tab_links; ?></a></li>
                        <li><a href="#tab-image" data-toggle="tab"><?php echo $tab_image; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                    <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="look_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($look_description[$language['language_id']]) ? $look_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_name[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="look_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($look_description[$language['language_id']]) ? $look_description[$language['language_id']]['description'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="look_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($look_description[$language['language_id']]) ? $look_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="look_description[<?php echo $language['language_id']; ?>][meta_description]" value="<?php echo isset($look_description[$language['language_id']]) ? $look_description[$language['language_id']]['meta_description'] : ''; ?>" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-meta-keywords<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keywords; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="look_description[<?php echo $language['language_id']; ?>][meta_keywords]" value="<?php echo isset($look_description[$language['language_id']]) ? $look_description[$language['language_id']]['meta_keywords'] : ''; ?>" placeholder="<?php echo $entry_meta_keywords; ?>" id="input-meta-keywords<?php echo $language['language_id']; ?>" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- { / tab-content } !-->
                        </div>
                        <!-- { / tab-pane } !-->
                        <div class="tab-pane" id="tab_attribute">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="status" id="input-status" class="form-control">
                                        <?php if ($status) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-home"><?php echo $entry_home; ?></label>
                                <div class="col-sm-10">
                                    <select name="home" id="input-home" class="form-control">
                                        <?php if ($home) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <!-- <?php /* <div class="form-group required">
                                          <label class="col-sm-2 control-label"
                                          for="input-entry_author"><?php echo $entry_author; ?></span></label>
                                          <div class="col-sm-10">

                                          <input type="text" name="author" value="<?php echo $author; ?>"
                                          placeholder="<?php echo $entry_author; ?>" id="input-author"
                                          class="form-control"/>
                                          <?php if (isset($error_name['author'])) { ?>
                                          <div class="text-danger"><?php echo $error_name['author']; ?></div>
                                          <?php } ?>
                                          </div>
                                          </div> */ ?>-->
                            <?php /*
                              <div class="form-group">
                              <label class="col-sm-2 control-label"
                              for="input-group_id"><?php echo $entry_group; ?></label>
                              <div class="col-sm-10">
                              <select id="input-group_id" name="group_id" class="form-control">
                              <option value=""><?php echo $select_group; ?></option>
                              <option value="0" <?php if(isset($group_id) && $group_id == 0) { ?>selected<?php }?>>Famous</option>
                              <option value="1" <?php if(isset($group_id) && $group_id == 1) { ?>selected<?php }?>>Women Look</option>
                              <option value="2" <?php if(isset($group_id) && $group_id == 2) { ?>selected<?php }?>>Men Look</option>
                              <option value="3" <?php if(isset($group_id) && $group_id == 3) { ?>selected<?php }?>>Baby Look</option>
                              </select>
                              </div>
                              </div>
                             */ ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input-language"><?php echo $entry_language; ?></label>
                                <div class="col-sm-10">
                                    <?php echo $arabic_lang; ?>&nbsp; <input type="checkbox" name="is_arabic" id="input-arlanguage" value="1" <?php if ($is_arabic > 0) { ?> checked <?php } ?>> &nbsp;&nbsp;&nbsp;
                                    <?php echo $english_lang; ?>&nbsp <input type="checkbox" name="is_english" id="input-enlanguage" value="1" <?php if ($is_english > 0) { ?> checked <?php } ?>> 
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_links">

                            <div class="form-group country-field">
                                <label class="col-sm-2 control-label" for="input-link-country"><?php echo $entry_country_id; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="look_link[country][]" id="input-link-country" data-input-type="country" multiple placeholder="<?php echo $entry_country_id; ?>"></select>
                                </div>
                            </div>

                            <div class="form-group group-field">
                                <label class="col-sm-2 control-label" for="input-link-group"><?php echo $entry_group; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="look_link[group][]" id="input-link-group" data-input-type="group" multiple placeholder="<?php echo $entry_group; ?>"></select>
                                </div>
                            </div>
                            <div class="form-group keyword-field">
                                <label class="col-sm-2 control-label"
                                       for="input-link-keyword"><?php echo $entry_keywords; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="look_link[keyword][]" id="input-link-keyword"
                                            data-input-type="keyword" multiple
                                            placeholder="<?php echo $entry_keywords; ?>"></select>
                                </div>
                            </div>
                            <?php /*<div class="form-group">
                                <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="<?php echo $help_related; ?>"><?php echo $entry_related; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="related" value="" placeholder="<?php echo $entry_related; ?>" id="input-related" class="form-control" />
                                    <div id="product-related" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach ($product_relateds as $product_related) { ?>
                                            <div id="product-related<?php echo $product_related['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_related['name']; ?>
                                                <input type="hidden" name="product_related[]" value="<?php echo $product_related['product_id']; ?>" />
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div> */ ?>

                            <?php ////////// ******** ///////// ?>
                            <?php ////////// ******** ///////// ?>
                            <?php ////////// ******** ///////// ?>

                            <table id="products" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="text-left"><?php echo $entry_related_product; ?></td>
                                        <td class="text-right"><?php echo $entry_sort_order; ?></td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $product_row = 0; ?>
                                    <?php foreach ($product_relateds as $product_related) { ?>
                                        <tr id="product_row<?php echo $product_row; ?>">
                                            <td class="text-left">
                                                <input type="text" class="form-control" name="product" onfocus="setconVars(<?php echo $product_row; ?>)" value="<?php echo $product_related['name']; ?>" />
                                                <input type="hidden" name="product_related[<?php echo $product_row; ?>][product_id]" value="<?php echo $product_related['product_id']; ?>" id="input-product<?php echo $product_row; ?>" />
                                            </td>
                                            <td class="text-right"><input type="text" name="product_related[<?php echo $product_row; ?>][sort_order]" value="<?php echo $product_related['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                                            <td class="text-left"><button type="button" onclick="$('#product_row<?php echo $product_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                        </tr>
                                        <?php $product_row++; ?>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="text-left"><button type="button" onclick="addProduct();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab-image">
                            <div class="table-responsive">
                                <table id="images" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left"><?php echo $entry_image; ?></td>
                                            <td class="text-right"><?php echo $entry_sort_order; ?></td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $image_row = 0; ?>
                                        <?php foreach ($look_images as $look_image) { ?>
                                            <tr id="image-row<?php echo $image_row; ?>">
                                                <td class="text-left">
                                                    <img src="<?php echo $look_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                                    <input type="hidden" name="look_image[<?php echo $image_row; ?>][image_text]" value="<?php echo $look_image['image']; ?>" id="input-image<?php echo $image_row; ?>" />
                                                </td>
                                                <td class="text-right"><input type="text" name="look_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $look_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                                                <td class="text-left"><button type="button" onclick="$('#image-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                            </tr>
                                            <?php $image_row++; ?>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    var dataSource1 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/look/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function (url) {
                return url.replace('%TYPE', encodeURIComponent($('.country-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.country-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {id: v.id, name: v.name};
                });
            }
        }
    });
    dataSource1.initialize();

    $('.country-field > > #input-link-country').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource1',
            displayKey: 'name',
            source: dataSource1.ttAdapter()
        }
    });
</script>

<script>
    var dataSource2 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/look/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function (url) {
                return url.replace('%TYPE', encodeURIComponent($('.group-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.group-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {id: v.id, name: v.name};
                });
            }
        }
    });
    dataSource2.initialize();

    $('.group-field > > #input-link-group').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource2',
            displayKey: 'name',
            source: dataSource2.ttAdapter()
        }
    });
</script>
<script>
    var dataSource2 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/look/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function (url) {
                return url.replace('%TYPE', encodeURIComponent($('.keyword-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.keyword-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {id: v.id, name: v.name};
                });
            }
        }
    });
    dataSource2.initialize();

    $('.keyword-field > > #input-link-keyword').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource2',
            displayKey: 'name',
            source: dataSource2.ttAdapter()
        }
    });
</script>
<?php if (isset($item_link) && !empty($item_link)) { ?>
    <script type="text/javascript">
        var linksArray = eval('(<?php echo json_encode($item_link) ?>)');
        if (!jQuery.isEmptyObject(linksArray)) {

            $.each(linksArray, function (index, value) {
                switch (index) {
                    case 'country':
                        $.each(value, function (index1, value1) {
                            $('#input-link-country').tagsinput('add', {
                                "id": value1.id,
                                "name": "" + value1.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'group':
                        $.each(value, function (index5, value5) {
                            $('#input-link-group').tagsinput('add', {
                                "id": value5.id,
                                "name": "" + value5.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'category':
                        $.each(value, function (index6, value6) {
                            $('#input-link-category').tagsinput('add', {
                                "id": value6.id,
                                "name": "" + value6.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'shop':
                        $.each(value, function (index4, value4) {
                            $('#input-link-shop').tagsinput('add', {
                                "id": value4.id,
                                "name": "" + value4.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'keyword':
                        $.each(value, function (index7, value7) {
                            $('#input-link-keyword').tagsinput('add', {
                                "id": value7.id,
                                "name": "" + value7.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    default:
                        break;
                }
            });
        }
    </script>
<?php } ?>


<?php if (isset($look_link) && !empty($look_link)) { ?>
    <script type="text/javascript">
        var linksArray = eval('(<?php echo json_encode($look_link) ?>)');
        if (!jQuery.isEmptyObject(linksArray)) {

            $.each(linksArray, function (index, value) {
                switch (index) {
                    case 'country':
                        $.each(value, function (index1, value1) {
                            $('#input-link-country').tagsinput('add', {
                                "id": value1.id,
                                "name": "" + value1.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'group':
                        $.each(value, function (index5, value5) {
                            $('#input-link-group').tagsinput('add', {
                                "id": value5.id,
                                "name": "" + value5.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'category':
                        $.each(value, function (index6, value6) {
                            $('#input-link-category').tagsinput('add', {
                                "id": value6.id,
                                "name": "" + value6.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'shop':
                        $.each(value, function (index4, value4) {
                            $('#input-link-shop').tagsinput('add', {
                                "id": value4.id,
                                "name": "" + value4.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'keyword':
                        $.each(value, function (index7, value7) {
                            $('#input-link-keyword').tagsinput('add', {
                                "id": value7.id,
                                "name": "" + value7.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    default:
                        break;
                }
            });
        }
    </script>
<?php } ?>
<script><!--
    // Related
    $('input[name=\'related\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'related\']').val('');

            $('#product-related' + item['value']).remove();

            $('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_related[]" value="' + item['value'] + '" /></div>');
        }
    });
    $('#product-related').delegate('.fa-minus-circle', 'click', function () {
        $(this).parent().remove();
    });

    //--></script>
<script type="text/javascript"><!--
    var Xrow_id = 0;
    function setconVars(row_id) {
        Xrow_id = row_id;
    }
//--></script>
<script type="text/javascript"><!--
    // Product
    $('input[name=\'product\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    json.unshift({
                        product_id: 0,
                        name: '<?php echo $text_none; ?>'
                    });
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        };
                    }));
                }
            });
        },
        'select': function (item) {
            $(this).val(item['label']);
            $('input[name=\'product_related[' + Xrow_id + '][product_id]').val(item['value']);
        }
    });
    //--></script>
<script type="text/javascript"><!--
    var product_row = <?php echo $product_row; ?>;

    function addProduct() {
        html = '<tr id="product-row' + product_row + '">';
        html += '  <td class="text-left">';
        html += '     <input type="text" onfocus="setconVars('+product_row+')" class="form-control" value="" name="product" id="product'+product_row+'"/>';
        html += '     <input type="hidden" name="product_related[' + product_row + '][product_id]" id="input-product' + product_row + '" />';
        html += '  </td>';
        html += '  <td class="text-right"><input type="text" name="product_related[' + product_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#product_row' + product_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';
        $('#products tbody').append(html);

        $('#product' + product_row).autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        json.unshift({
                            product_id: 0,
                            name: '<?php echo $text_none; ?>'
                        });
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            };
                        }));
                    }
                });
            },
            'select': function (item) {
                $(this).val(item['label']);
                $('#input-product' + Xrow_id).val(item['value']);
            }
        });

        product_row++;
    }
    //--></script>
<script type = "text/javascript"><!--
    var image_row = <?php echo $image_row; ?>;

    function addImage() {
        html = '<tr id="image-row' + image_row + '">';
        html += '  <td class="text-left"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="image/cache/no_image-100x100.png" /><input type="file" name="look_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
        html += '  <td class="text-right"><input type="text" name="look_image[' + image_row + '][sort_order]" value="" placeholder="Sort Order" class="form-control" /></td>';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#images tbody').append(html);

        image_row++;
    }
    //--></script>
<script>
<?php foreach ($languages as $language) { ?>
        // $("#input-description<?php echo $language['language_id']; ?>").summernote();
        CKEDITOR.replace("input-description<?php echo $language['language_id']; ?>", {
            filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
            filebrowserWindowWidth: 800,
            filebrowserWindowHeight: 500
        });
<?php } ?>
    $('#language a:first').tab('show');
</script>

<?php echo $footer; ?>
