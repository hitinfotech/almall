<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </a> 
                <!--
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="(checkForDelete()) ? (confirm('<?php echo $text_confirm; ?>') ? $('#form-tip').submit() : false) : false;">
                    <i class="fa fa-trash-o"></i>
                </button>
                -->
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                <input type="text" name="filter_filter" value="<?php echo $filter; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-group"><?php echo $entry_group; ?></label>
                                <select name="filter_group" id="input-group_id" class="form-control">
                                    <option value=""><?php echo $select_group; ?></option>
                                    <?php foreach ($aLookGroups as $v) { ?>
                                        <option value="<?php echo $v['look_group_id']; ?>" <?php if ($filter_group == $v['look_group_id']) { ?>selected<?php } ?>><?php echo $v['name']; ?></option>
                                    <?php } ?>
                                    <?php /*
                                      <option value="1" <?php if ($filter['group'] == 1) { ?>selected<?php } ?>>Women Look</option>
                                      <option value="2" <?php if ($filter['group'] == 2) { ?>selected<?php } ?>>Men Look</option>
                                      <option value="3" <?php if ($filter['group'] == 3) { ?>selected<?php } ?>>Baby Look</option>
                                     */ ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <select name="filter_status" id="input-status" class="form-control">
                                    <option value="*"><?php echo $select_status; ?></option>
                                    <?php if ($filter_status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                    <?php } ?>
                                    <?php if (!$filter_status && !is_null($filter_status)) { ?>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-country"><?php echo $entry_country; ?></label>
                                <select name="filter_country_id" id="input-country_id" class="form-control">
                                    <option value="*"><?php echo $select_country; ?></option>
                                    <?php foreach ($countries as $row) { ?>
                                        <?php if ($filter_country_id == $row['country_id']) { ?>
                                            <option value="<?php echo $row['country_id'] ?>" selected="selected"><?php echo $row['name'] ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $row['country_id'] ?>"><?php echo $row['name'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 pull-right">
                            <div class="form-group">
                                <button id="button-filter" class="btn btn-primary pull-right" type="button"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-tip">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center">
                                        <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'ld1.name') { ?>
                                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'l.status') { ?>
                                            <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left"  width="97px">
                                        <?php if ($sort == 'l.sort_order') { ?>
                                            <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_order; ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'l.date_added') { ?>
                                            <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'l.date_modified') { ?>
                                            <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_modified ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php echo $column_user_id; ?></td>
                                    <td class="text-center"><?php echo $column_last_mod_id; ?></td>

                                    <td class="text-left"  width="99px">
                                        <?php echo $column_action; ?>
                                    </td>

                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($items)) { ?>
                                    <?php foreach ($items as $item) { ?>
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" name="selected[]" value="<?php echo $item["look_id"]; ?>"
                                                <?php echo ((isset($item["look_id"]) && !empty($item["look_id"]) && in_array($item["look_id"], $selected)) ? "checked=\"checked\"" : ""); ?>
                                                       />
                                            </td>
                                            <td class="text-left">
                                                <a href="<?php echo $item['edit']; ?>" data-toggle="tooltip" title="<?php echo $item['name']; ?>">
                                                    <?php echo $item['name']; ?>
                                                </a>
                                            </td>
                                            <td class="text-left">
                                                <?php if ($item['status']) { ?> Enabled <?php } else { ?> Disabled <?php } ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['sort_order']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['date_added']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $item['date_modified']; ?>
                                            </td>
                                            <td class="text-center"><b><?php echo $item['user_add']; ?></b></td>
                                            <td class="text-center"><b><?php echo $item['user_modify']; ?></b></td>

                                            <td class="text-left">
                                                <a href="<?php echo $item['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="<?php echo $item['edit_status']; ?>" data-toggle="tooltip" title="<?php echo $item['edit_status_text']; ?>" class="btn btn-primary">
                                                    <?php if ($item['status'] == 0) { ?>
                                                        <i class="fa fa-star"></i>
                                                    <?php } else { ?>
                                                        <i class="fa fa-star-o"></i>
                                                    <?php } ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
<!--
    $('#button-filter').on('click', function () {
        var url = 'index.php?route=mall/look&token=<?php echo $token; ?>';
        var filter_filter = $('input[name=\'filter_filter\']').val();
        if (filter_filter) {
            url += '&filter_filter=' + encodeURIComponent(filter_filter);
        }
        var filter_status = $('select[name=\'filter_status\']').val();
        if (filter_status != '*') {
            url += '&filter_status=' + encodeURIComponent(filter_status);
        }
        var filter_group = $('select[name=\'filter_group\']').val();
        if (filter_group != '*') {
            url += '&filter_group=' + encodeURIComponent(filter_group);
        }
        var filter_country_id = $('select[name=\'filter_country_id\']').val();
        if (filter_country_id != '*') {
            url += '&filter_country_id=' + encodeURIComponent(filter_country_id);
        }

        location = url;
    });
//-->
</script>
<script type="text/javascript">
<!--
    /*
     function checkForDelete() {
     return ($('input[name="selected[]"]:checked').length > 0);
     }
     */
//-->
</script>
<?php echo $footer; ?>
