<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-tip" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
                   class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <!-- { breadcrumb } !-->
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
            <!-- { / breadcrumb } !-->
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-tip"
                      class="form-horizontal">


                    <!-- { nav-tabs } !-->
                    <ul class="nav nav-tabs">

                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>

                        <li><a href="#tab_attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                        <li><a href="#tab_links" data-toggle="tab"><?php echo $tab_links; ?></a></li>

                    </ul>
                    <!-- { / nav-tabs } !-->

                    <div class="tab-content">

                        <!-- { tab-pane } !-->
                        <div class="tab-pane active" id="tab-general">

                            <!-- { nav-tabs } !-->
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                    <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img
                                                src="view/image/flags/<?php echo $language['image']; ?>"
                                                title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?>
                                        </a></li>
                                <?php } ?>
                            </ul>

                            <!-- { / nav-tabs } !-->

                            <!-- { tab-content } !-->
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="tip_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($tip_description[$language['language_id']]) ? $tip_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_name[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_author; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="tip_description[<?php echo $language['language_id']; ?>][author]" value="<?php echo isset($tip_description[$language['language_id']]) ? $tip_description[$language['language_id']]['author'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_name[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="tip_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($tip_description[$language['language_id']]) ? $tip_description[$language['language_id']]['description'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Tip Sections</label>
                                        </div>
                                        <table id="images<?php echo $language['language_id'] ?>" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <td class="text-left"><?php echo $entry_name; ?></td>
                                                    <td class="text-left"><?php echo $entry_description; ?></td>
                                                    <td class="text-left"><?php echo $entry_image; ?></td>
                                                    <td class="text-left"><?php echo $entry_product; ?></td>
                                                    <td class="text-left"><?php echo $entry_sort_order; ?></td>
                                                    <td>Action</td>
                                                </tr>
                                            </thead>
                                            <tbody class="tbody">
                                                <?php $section_row = 0; ?>
                                                <?php if (isset($tip_sections[$language['language_id']])) { ?>
                                                    <?php foreach ($tip_sections[$language['language_id']] as $tip_section) { ?>
                                                        <tr class="imagerow" id="image-row<?php echo $section_row . $language['language_id']; ?>">
                                                            <td class="text-right"><input type="text" name="tip_section[<?php echo $section_row; ?>][<?php echo $language['language_id']; ?>][title]" value="<?php echo $tip_section['title'] ?>" placeholder="Title" class="form-control" /></td>
                                                            <td class="text-right"> <textarea  name="tip_section[<?php echo $section_row; ?>][<?php echo $language['language_id']; ?>][description]" id="tip_section[<?php echo $section_row; ?>][<?php echo $language['language_id']; ?>][description]" value='<?php echo $tip_section['description'] ?>' placeholder="Description" class="form-control" /><?php echo $tip_section['description'] ?></textarea>
                                                            <td class="text-left">
                                                                <img src="<?php echo $tip_section['thumb']; ?>" alt="" title="" data-placeholder="" />
                                                                <input type="hidden" name="tip_section[<?php echo $section_row; ?>][<?php echo $language['language_id']; ?>][image_text]" value="<?php echo $tip_section['image']; ?>" id="input-image<?php echo $section_row; ?>" />
                                                                <input type="file" name="tip_section[<?php echo $section_row; ?>][<?php echo $language['language_id']; ?>][image_text]" id="input-image<?php echo $section_row; ?>" />

                                                                <div class="checkbox">
                                                                  <label>
                                                                    <input type="checkbox" name="tip_section[<?php echo $section_row; ?>][<?php echo $language['language_id']; ?>][no_image]" id="input-no-image<?php echo $section_row; ?>" />
                                                                    Delete Image
                                                                  </label>
                                                                </div>


                                                            </td>
                                                            <td class="text-left">
                                                                <input type="hidden" name="tip_section[<?php echo $section_row; ?>][<?php echo $language['language_id']; ?>][product_id]" value="<?php echo $tip_section['product_id']; ?>" />
                                                                <input type="text" name="product" value="<?php echo $tip_section['product_name'] ?>" onclick="setconVars(<?php echo $section_row ?>, <?php echo $language['language_id'] ?>)" placeholder="<?php echo $entry_product; ?>" id="input-product_<?php echo $section_row; ?>_<?php echo $language['language_id']; ?>" class="form-control" />
                                                            </td>
                                                            <td class="text-right"><input type="text" name="tip_section[<?php echo $section_row; ?>][<?php echo $language['language_id']; ?>][sort_order]" value="<?php echo $tip_section['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                                                            <td class="text-left"><button type="button" onclick="$('#image-row<?php echo $section_row . $language['language_id']; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                                        </tr>
                                                        <?php $section_row++; ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="5"></td>
                                                    <td class="text-left"><button id="blusbtn" type="button" onclick="addImage(<?php echo $language['language_id']; ?>);" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- { / tab-content } !-->

                        </div>
                        <!-- { / tab-pane } !-->


                        <div class="tab-pane" id="tab_attribute">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $entry_image; ?>" />
                                    <input type="file" name="image" value="<?php echo $image; ?>" id="input-image" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="status" id="input-status" class="form-control">
                                        <?php if ($status) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-home"><?php echo $entry_home; ?></label>
                                <div class="col-sm-10">
                                    <select name="home" id="input-home" class="form-control">
                                        <?php if ($home) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sort_order" value="<?php echo $sort_order; ?>"
                                           placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input-language"><?php echo $entry_language; ?></label>
                                <div class="col-sm-10">
                                    <?php echo $arabic_lang; ?>&nbsp; <input type="checkbox" name="is_arabic" id="input-arlanguage" value="1" <?php if($is_arabic > 0) { ?> checked <?php }?>> &nbsp;&nbsp;&nbsp;
                                    <?php echo $english_lang; ?>&nbsp <input type="checkbox" name="is_english" id="input-enlanguage" value="1" <?php if($is_english > 0) { ?> checked <?php }?>>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_links">
                            <div class="form-group country-field">
                                <label class="col-sm-2 control-label" for="input-link-country"><?php echo $entry_country_id; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="tip_link[country][]" id="input-link-country"
                                            data-input-type="country" multiple
                                            placeholder="<?php echo $entry_country_id; ?>"></select>
                                </div>
                            </div>

                            <div class="form-group group-field">
                                <label class="col-sm-2 control-label"
                                       for="input-link-group"><?php echo $entry_group_id; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="tip_link[group][]" id="input-link-group"
                                            data-input-type="group" multiple
                                            placeholder="<?php echo $entry_group_id; ?>"></select>
                                </div>
                            </div>

                            <div class="form-group keyword-field">
                                <label class="col-sm-2 control-label"
                                       for="input-link-keyword"><?php echo $entry_keywords; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="tip_link[keyword][]" id="input-link-keyword"
                                            data-input-type="keyword" multiple
                                            placeholder="<?php echo $entry_keywords; ?>"></select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="<?php echo $help_related; ?>"><?php echo $entry_related; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="related" value="" placeholder="<?php echo $entry_related; ?>" id="input-related" class="form-control" />
                                    <div id="product-related" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach ($product_relateds as $product_related) { ?>
                                            <div id="product-related<?php echo $product_related['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_related['name']; ?>
                                                <input type="hidden" name="product_related[]" value="<?php echo $product_related['product_id']; ?>" />
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    var Xlanguage_id = 0;
    var Xsection_id = 0;
    function setconVars(section_id, language_id) {
        Xlanguage_id = language_id;
        Xsection_id = section_id;
    }
//--></script>
<script type="text/javascript"><!--
    // Product
    $('input[name=\'product\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    json.unshift({
                        product_id: 0,
                        name: '<?php echo $text_none; ?>'
                    });
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        };
                    }));
                }
            });
        },
        'select': function (item) {
            $(this).val(item['label']);
            $('input[name=\'tip_section[' + Xsection_id + '][' + Xlanguage_id + '][product_id]\']').val(item['value']);
        }
    });
    //--></script>
<script type="text/javascript"><!--
    function addImage(x) {
        var section_row = 0;
        $(".tbody").children("tr").each(function () {
            section_row++;
        });

        html = '<tr id="image-row' + section_row + x + '">';
        html += '  <td class="text-right"><input type="text" name="tip_section[' + section_row + '][' + x + '][title]" value="" placeholder="Title" class="form-control" /></td>';
        html += '  <td class="text-right"> <textarea name="tip_section[' + section_row + '][' + x + '][description]"value="" placeholder="Description" class="form-control" /></textarea></td>';
        html += '  <td class="text-left"><img src="image/cache/no_image-100x100.png" alt="" title="" data-placeholder="image/cache/no_image-100x100.png" /><input type="file" value="" name="tip_section[' + section_row + '][' + x + '][image_text]" id="input-image' + section_row + '" /></td>';
        html += '  <td class="text-right">';
        html += '      <input type="hidden" name="tip_section[' + section_row + '][' + x + '][product_id]" value="" /> ';
        html += '      <input type="text" onclick="setconVars(' + section_row + ',' + x + ')" name="product" value="" placeholder="<?php echo $entry_product; ?>" id="input-product_' + section_row + '_' + x + '" class="form-control" /> ';
        html += '   </td> ';
        html += '  <td class="text-right"><input type="text" name="tip_section[' + section_row + '][' + x + '][sort_order]" value="" placeholder="Sort Order" class="form-control" /></td>';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + section_row + x + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';
        $('#images' + x + ' tbody').append(html);
        $('#input-product_' + section_row + '_' + x).autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        json.unshift({
                            product_id: 0,
                            name: '<?php echo $text_none; ?>'
                        });
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            };
                        }));
                    }
                });
            },
            'select': function (item) {
                $(this).val(item['label']);
                $('input[name=\'tip_section[' + Xsection_id + '][' + Xlanguage_id + '][product_id]\']').val(item['value']);
            }
        });

        CKEDITOR.replace('tip_section[' + section_row + '][' + x + '][description]', {
            filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
            filebrowserWindowWidth: 100,
            filebrowserWindowHeight: 10
        });
        section_row++;

    }

    //--></script>
<script type="text/javascript"><!--
// Related
    $('input[name=\'related\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'related\']').val('');
            $('#product-related' + item['value']).remove();
            $('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_related[]" value="' + item['value'] + '" /></div>');
        }

    });
    $('#product-related').delegate('.fa-minus-circle', 'click', function () {
        $(this).parent().remove();
    });

//--></script>
<script type="text/javascript"><!--
    var dataSource1 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/tip/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function (url) {
                return url.replace('%TYPE', encodeURIComponent($('.country-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.country-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {id: v.id, name: v.name};
                });
            }
        }
    });
    dataSource1.initialize();
    $('.country-field > > #input-link-country').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource1',
            displayKey: 'name',
            source: dataSource1.ttAdapter()
        }
    });
//--></script>
<script type="text/javascript"><!--
    var dataSource2 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/tip/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function (url) {
                return url.replace('%TYPE', encodeURIComponent($('.group-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.group-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {id: v.id, name: v.name};
                });
            }
        }
    });
    dataSource2.initialize();
    $('.group-field > > #input-link-group').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource2',
            displayKey: 'name',
            source: dataSource2.ttAdapter()
        }
    });
//--></script>
<script type="text/javascript"><!--
    var dataSource1 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/tip/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function (url) {
                return url.replace('%TYPE', encodeURIComponent($('.keyword-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.keyword-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {id: v.id, name: v.name};
                });
            }
        }
    });
    dataSource1.initialize();
    $('.keyword-field > > #input-link-keyword').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource1',
            displayKey: 'name',
            source: dataSource1.ttAdapter()
        }
    });</script>

<?php if (isset($tip_link) && !empty($tip_link)) { ?>
    <script type="text/javascript">
        var linksArray = eval('(<?php echo json_encode($tip_link) ?>)');
        if (!jQuery.isEmptyObject(linksArray)) {

            $.each(linksArray, function (index, value) {
                switch (index) {
                    case 'country':
                        $.each(value, function (index1, value1) {
                            $('#input-link-country').tagsinput('add', {
                                "id": value1.id,
                                "name": "" + value1.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'group':
                        $.each(value, function (index5, value5) {
                            $('#input-link-group').tagsinput('add', {
                                "id": value5.id,
                                "name": "" + value5.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'category':
                        $.each(value, function (index6, value6) {
                            $('#input-link-category').tagsinput('add', {
                                "id": value6.id,
                                "name": "" + value6.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'shop':
                        $.each(value, function (index4, value4) {
                            $('#input-link-shop').tagsinput('add', {
                                "id": value4.id,
                                "name": "" + value4.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'keyword':
                        $.each(value, function (index7, value7) {
                            $('#input-link-keyword').tagsinput('add', {
                                "id": value7.id,
                                "name": "" + value7.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    default:
                        break;
                }
            });
        }
    </script>
<?php } ?>
<script type="text/javascript"><!--
    var Xlanguage_id = 0;
    var Xsection_id = 0;
    function setdesc(section_id, language_id) {
        Xlanguage_id = language_id;
        Xsection_id = section_id;
    }
//--></script>

<script type="text/javascript"><!--

<?php foreach ($languages as $language) { ?>
        CKEDITOR.replace("input-description<?php echo $language['language_id']; ?>", {
            filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
            filebrowserWindowWidth: 800,
            filebrowserWindowHeight: 500
        });

    <?php $section_row = 0; ?>
    <?php if (isset($tip_sections[$language['language_id']])) { ?>
        <?php foreach ($tip_sections[$language['language_id']] as $tip_section) { ?>
                CKEDITOR.replace("tip_section[<?php echo $section_row ?>][<?php echo $language['language_id']; ?>][description]", {
                    filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
                    filebrowserWindowWidth: 100,
                    filebrowserWindowHeight: 10
                });
            <?php $section_row++; ?>
        <?php } ?>
    <?php } ?>
<?php } ?>
    $('#language a:first').tab('show');
//--></script>

<?php echo $footer; ?>
