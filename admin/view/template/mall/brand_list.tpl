<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                </a> 
                <!--
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="(checkForDelete()) ? (confirm('<?php echo $text_confirm; ?>') ? $('#form-brand').submit() : false) : false;">
                    <i class="fa fa-trash-o"></i>
                </button>
                -->
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                <input type="text" name="filter_filter" value="<?php echo $filter_filter; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <select name="filter_status" id="input-status" class="form-control">
                                    <option value="*"><?php echo $select_status; ?></option>
                                    <?php if ($filter_status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                    <?php } ?>
                                    <?php if (!$filter_status && !is_null($filter_status)) { ?>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-shopable"><?php echo $entry_shopable; ?></label>
                                <select name="filter_shopable" id="input-shopable" class="form-control">
                                    <option value="*"><?php echo $select_shopable; ?></option>
                                    <?php foreach ($stock_statuses as $status) { ?>
                                        <option value="<?php echo $status['stock_status_id'] ?>"
                                        <?php if ($filter_shopable == $status['stock_status_id']) { ?>
                                                    selected="selected"
                                                <?php } ?>
                                                ><?php echo $status['name'] ?></option>
                                            <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-country"><?php echo $entry_country; ?></label>
                                <select name="filter_country" id="input-country" class="form-control">
                                    <option value="*"><?php echo $select_country; ?></option>
                                    <?php foreach ($countries as $country_id => $country_name) { ?>
                                        <?php if ($filter_country == $country_id) { ?>
                                            <option value="<?php echo $country_id ?>" selected="selected"><?php echo $country_name ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $country_id ?>"><?php echo $country_name ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-category"><?php echo $entry_category; ?></label>
                                <select name="filter_category" id="input-category" class="form-control">
                                    <option value="*"><?php echo $select_category; ?></option>
                                    <?php foreach ($data['categories'] as $foreach_category) { ?>
                                        <option value="<?php echo $foreach_category['category_id'] ?>"
                                                <?php if ($foreach_category['category_id'] == $filter_category) { ?>selected="selected" <?php } ?>>
                                                    <?php echo $foreach_category['name']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-brand">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center">
                                        <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                    </td>
                                    <td class="text-center"><?php echo $column_image; ?></td>
                                    <td class="text-left">
                                        <?php if ($sort == 'bd.name') { ?>
                                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'b.status') { ?>
                                            <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'b.date_added') { ?>
                                            <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'b.date_modified') { ?>
                                            <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'b.sort_order') { ?>
                                            <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_order; ?>"><?php echo $column_sort_order; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-center"><?php echo $column_user_id; ?></td>
                                    <td class="text-center"><?php echo $column_last_mod_id; ?></td>

                                    <td class="text-left">
                                        <?php echo $column_action; ?>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($brands)) { ?>
                                    <?php foreach ($brands as $brand) { ?>
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" name="selected[]" value="<?php echo $brand['brand_id']; ?>"
                                                <?php echo ((isset($brand['brand_id']) && !empty($brand['brand_id']) && in_array($brand['brand_id'], $selected)) ? "checked=\"checked\"" : ""); ?>
                                                       />
                                            </td>
                                            <td class="text-center"><?php if ($brand['image']) { ?>
                                                    <img src="<?php echo $brand['image']; ?>" alt="<?php echo $brand['name']; ?>" class="img-thumbnail" />
                                                <?php } else { ?>
                                                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                                                <?php } ?></td>
                                            <td class="text-left">
                                                <?php echo $brand['name']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php if ($brand['status']) { ?> Enabled <?php } else { ?> Disabled <?php } ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $brand['date_added']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $brand['date_modified']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $brand['sort_order']; ?>
                                            </td>
                                            <td class="text-center"><b><?php echo $brand['user_add']; ?></b></td>
                                            <td class="text-center"><b><?php echo $brand['user_modify']; ?></b></td>

                                            <td class="text-left">
                                                <a href="<?php echo $brand['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-8 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-4 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
<!--
    $('#button-filter').on('click', function () {
        var url = 'index.php?route=mall/brand&token=<?php echo $token; ?>';
        var filter_filter = $('input[name=\'filter_filter\']').val();
        if (filter_filter) {
            url += '&filter_filter=' + encodeURIComponent(filter_filter);
        }
        var filter_status = $('select[name=\'filter_status\']').val();
        if (filter_status != '*') {
            url += '&filter_status=' + encodeURIComponent(filter_status);
        }
        var filter_shopable = $('select[name=\'filter_shopable\']').val();
        if (filter_shopable != '*') {
            url += '&filter_shopable=' + encodeURIComponent(filter_shopable);
        }
        var filter_country = $('select[name=\'filter_country\']').val();
        if (filter_country != '*') {
            url += '&filter_country=' + encodeURIComponent(filter_country);
        }
        var filter_category = $('select[name=\'filter_category\']').val();
        if (filter_category != '*') {
            url += '&filter_category=' + encodeURIComponent(filter_category);
        }

        location = url;
    });
//-->
</script>
<script type="text/javascript">
<!--
    /*
     function checkForDelete() {
     return ($('input[name="selected[]"]:checked').length > 0);
     }
     */
//-->
</script>
<?php echo $footer; ?>
