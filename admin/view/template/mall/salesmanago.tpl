<?php
/**
 * @var String $header
 * @var String $column_left
 * @var String $footer
 * @var String[] $english_popup
 * @var String[] $arabic_popup
 */
?>
<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content" style="padding: 30px">
    <form method="post" id="en-form" enctype="multipart/form-data">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Sales Manago English</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <img src="<?=$english_popup['popup_image']?>" class="img-responsive img-thumbnail" />
                        <div class="form-group"><input type="file" id="popup_image" name="popup_image"></div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="popup_title">Popup Title</label>
                            <input type="text" class="form-control" name="EnglishPopup[popup_title]" id="popup_title"  value="<?=$english_popup['popup_title']?>" placeholder="Popup Title">
                        </div>
                        <div class="form-group">
                            <label for="popup_description">Popup Description</label>
                            <textarea  class="form-control" name="EnglishPopup[popup_description]" id="popup_description"  placeholder="Popup Description"><?=$english_popup['popup_description']?></textarea>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="popup_yes">Popup Yes</label>
                                <input type="text" class="form-control" name="EnglishPopup[popup_yes]" id="popup_yes" value="<?=$english_popup['popup_yes']?>" placeholder="Popup Yes">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="popup_no">Popup No</label>
                                <input type="text" class="form-control" name="EnglishPopup[popup_no]" id="popup_no" value="<?=$english_popup['popup_no']?>" placeholder="Popup No">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </form>



    <form method="post" id="ar-form" enctype="multipart/form-data">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Sales Manago Arabic</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <img src="<?=$arabic_popup['popup_image']?>" class="img-responsive img-thumbnail" />
                        <div class="form-group"><input type="file" id="popup_image" name="popup_image"></div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="popup_title">Popup Title</label>
                            <input type="text" class="form-control" name="ArabicPopup[popup_title]" id="popup_title"  value="<?=$arabic_popup['popup_title']?>" placeholder="Popup Title">
                        </div>
                        <div class="form-group">
                            <label for="popup_description">Popup Description</label>
                            <textarea  class="form-control" name="ArabicPopup[popup_description]" id="popup_description"  placeholder="Popup Description"><?=$arabic_popup['popup_description']?></textarea>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="popup_yes">Popup Yes</label>
                                <input type="text" class="form-control" name="ArabicPopup[popup_yes]" id="popup_yes" value="<?=$arabic_popup['popup_yes']?>" placeholder="Popup Yes">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="popup_no">Popup No</label>
                                <input type="text" class="form-control" name="ArabicPopup[popup_no]" id="popup_no" value="<?=$arabic_popup['popup_no']?>" placeholder="Popup No">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </form>


</div>

<?php echo $footer; ?>
