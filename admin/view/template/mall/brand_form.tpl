<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-brand" data-toggle="tooltip" title="<?php echo $button_save; ?>"
                        class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
                   class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <!-- { breadcrumb } !-->
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
            <!-- { / breadcrumb } !-->
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-brand"
                      class="form-horizontal">


                    <!-- { nav-tabs } !-->
                    <ul class="nav nav-tabs">

                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>

                        <li><a href="#tab_attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                        <li><a href="#tab_links" data-toggle="tab"><?php echo $tab_links; ?></a></li>

                    </ul>
                    <!-- { / nav-tabs } !-->

                    <div class="tab-content">

                        <!-- { tab-pane } !-->
                        <div class="tab-pane active" id="tab-general">

                            <!-- { nav-tabs } !-->
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img
                                                src="view/image/flags/<?php echo $language['image']; ?>"
                                                title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?>
                                    </a></li>
                                <?php } ?>
                            </ul>
                            <!-- { / nav-tabs } !-->

                            <!-- { tab-content } !-->
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="brand_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($brand_description[$language['language_id']]) ? $brand_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                            <?php if (isset($error_name[$language['language_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="brand_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($brand_description[$language['language_id']]) ? $brand_description[$language['language_id']]['description'] : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="brand_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($brand_description[$language['language_id']]) ? $brand_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                                            <?php if (isset($error_meta_title[$language['language_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_meta_title[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="brand_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($brand_description[$language['language_id']]) ? $brand_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="brand_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($brand_description[$language['language_id']]) ? $brand_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <!-- { / tab-content } !-->

                        </div>
                        <!-- { / tab-pane } !-->


                        <div class="tab-pane" id="tab_attribute">

                             <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                                <div class="col-sm-10">
                                        <img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $entry_image; ?>" />
                                    <input type="file" name="image" value="<?php echo $image; ?>" id="input-image" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="status" id="input-status" class="form-control">
                                        <?php if ($status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-fbfeeds"><?php echo $entry_fbfeeds; ?></label>
                                <div class="col-sm-10">
                                    <select name="fbfeeds" id="input-fbfeeds" class="form-control">
                                        <?php if ($fbfeeds) { ?>
                                        <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                        <option value="0"><?php echo $text_no; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $text_yes; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_algolia; ?></label>
                                <div class="col-sm-10">
                                    <select name="is_algolia" id="input-status" class="form-control">
                                        <?php if ($is_algolia) { ?>
                                        <option value="1" selected="selected"><?php echo 'yes'; ?></option>
                                        <option value="0"><?php echo 'no'; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo 'yes'; ?></option>
                                        <option value="0" selected="selected"><?php echo 'no'; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-api"><?php echo $entry_show_api; ?></label>
                                <div class="col-sm-10">
                                    <select name="show_api" id="input-api" class="form-control">
                                        <?php if ($show_api == 'yes') { ?>
                                        <option value="yes" selected="selected"><?php echo 'yes'; ?></option>
                                        <option value="no"><?php echo 'no'; ?></option>
                                        <?php } else { ?>
                                        <option value="yes"><?php echo 'yes'; ?></option>
                                        <option value="no" selected="selected"><?php echo 'no'; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input-entry_website"><?php echo $entry_website; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="website" value="<?php echo $website; ?>"
                                           placeholder="<?php echo $entry_website; ?>" id="input-website"
                                           class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input-entry_social_fb"><?php echo $entry_social_fb; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="social_fb" value="<?php echo $social_fb; ?>"
                                           placeholder="<?php echo $entry_social_fb; ?>" id="input-social_fb"
                                           class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input-entry_social_tw"><?php echo $entry_social_tw; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="social_tw" value="<?php echo $social_tw; ?>"
                                           placeholder="<?php echo $entry_social_tw; ?>" id="input-social_tw"
                                           class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sort_order" value="<?php echo $sort_order; ?>"
                                           placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order"
                                           class="form-control"/>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="tab_links">

                            <div class="form-group country-field">
                                <label class="col-sm-2 control-label"
                                       for="input-link-country"><?php echo $entry_country_id; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="brand_link[country][]" id="input-link-country"
                                            data-input-type="country" multiple
                                            placeholder="<?php echo $entry_country_id; ?>"></select>
                                </div>
                            </div>

                            <div class="form-group category-field">
                                <label class="col-sm-2 control-label"
                                       for="input-link-category"><?php echo $entry_category_id; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="brand_link[category][]" id="input-link-category"
                                            data-input-type="category" multiple
                                            placeholder="<?php echo $entry_category_id; ?>"></select>
                                </div>
                            </div>

                            <div class="form-group shop-field">
                                <label class="col-sm-2 control-label" for="input-link-shop"><?php echo $entry_shop_id; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="brand_link[shop][]" id="input-link-shop" data-input-type="shop"  multiple  placeholder="<?php echo $entry_shop_id; ?>"></select>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<script>

    var dataSource1 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/brand/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function (url) {
                return url.replace('%TYPE', encodeURIComponent($('.country-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.country-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {id: v.id, name: v.name};
                });
            }
        }
    });
    dataSource1.initialize();

    $('.country-field > > #input-link-country').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource1',
            displayKey: 'name',
            source: dataSource1.ttAdapter()
        }
    });
</script>

<script>

    var dataSource2 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/brand/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function (url) {
                return url.replace('%TYPE', encodeURIComponent($('.category-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.category-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {id: v.id, name: v.name};
                });
            }
        }
    });
    dataSource2.initialize();

    $('.category-field > > #input-link-category').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource2',
            displayKey: 'name',
            source: dataSource2.ttAdapter()
        }
    });
</script>

<script>

    var dataSource4 = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=mall/brand/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function(url) {
                return url.replace('%TYPE', encodeURIComponent($('.shop-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.shop-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function(list){
                return $.map(list, function (v, i) {
                    return { id:v.id, name:v.name };
                });
            }
        }
    });
    dataSource4.initialize();
    $('.shop-field > > #input-link-shop').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource4',
            displayKey: 'name',
            source: dataSource4.ttAdapter()
        }
    });
</script>

<?php if(isset($brand_link) && !empty($brand_link)) { ?>
<script type="text/javascript">
    var brandLinksArray = <?php echo json_encode($brand_link) ?>;
    if (!jQuery.isEmptyObject(brandLinksArray)) {

        $.each(brandLinksArray, function (index, value) {
            switch (index) {
                case 'country':
                    $.each(value, function (index1, value1) {
                        console.log(value1);
                        $('#input-link-country').tagsinput('add', {
                            "id": value1.id,
                            "name": "" + value1.name.replace(/'/g, "\\'") + ""
                        });
                    });
                    break;
                case 'category':
                    $.each(value, function (index6, value6) {
                        $('#input-link-category').tagsinput('add', {
                            "id": value6.id,
                            "name": "" + value6.name.replace(/'/g, "\\'") + ""
                        });
                    });
                    break;
                case 'shop':
                    $.each(value, function (index4, value4) {
                        // console.log(value4);
                        $('#input-link-shop').tagsinput('add', {
                            "id": value4.id ,
                            "name": ""+value4.name.replace(/'/g, "\\'")+""
                        });
                    });
                    break;
                default:
                    break;
            }
        });
    }
</script>
<?php } ?>

<script>
<?php foreach ($languages as $language) { ?>
        // $("#input-description<?php echo $language['language_id']; ?>").summernote();
        CKEDITOR.replace("input-description<?php echo $language['language_id']; ?>", {
            filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
            filebrowserWindowWidth: 800,
            filebrowserWindowHeight: 500
        });
<?php } ?>
    $('#language a:first').tab('show');
</script>

<?php echo $footer; ?>
