<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-news" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-news" class="form-horizontal">
                    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab_attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                        <li><a href="#tab_image" data-toggle="tab"><?php echo $tab_image; ?></a></li>
                        <li><a href="#tab_links" data-toggle="tab"><?php echo $tab_links; ?></a></li>
                    </ul>
                    <div class="tab-content">

                        <input type="hidden" id="input-user_id" name="user_id" value="<?php echo $user_id; ?>"></input>
                        <div class="tab-pane active" id="tab-general">
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                    <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="news_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($news_description[$language['language_id']]) ? $news_description[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_title[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_title[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="news_description[<?php echo $language['language_id']; ?>][description]" rows="5" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($news_description[$language['language_id']]) ? $news_description[$language['language_id']]['description'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-body_text<?php echo $language['language_id']; ?>"><?php echo $entry_body_text; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="news_description[<?php echo $language['language_id']; ?>][body_text]" placeholder="<?php echo $entry_body_text; ?>" id="input-body_text<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($news_description[$language['language_id']]) ? $news_description[$language['language_id']]['body_text'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_attribute">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-start_date"><?php echo $entry_start_date; ?></span></label>
                                <div class="col-sm-10">
                                    <div class="input-group start-date">
                                        <input type="text" name="start_date"  value="<?php echo $start_date; ?>" placeholder="<?php echo $entry_start_date; ?>" id="input-start_date" class="form-control" data-date-format="YYYY-MM-DD" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span>
                                    </div>
                                    <?php if ($error_date_start) { ?>
                                        <div class="text-danger"><?php echo $error_date_start; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-end_date"><?php echo $entry_end_date; ?></label>
                                <div class="col-sm-10">
                                    <div class="input-group end-date">
                                        <input type="text" name="end_date" value="<?php echo $end_date; ?>" placeholder="<?php echo $entry_end_date; ?>" id="input-end_date" class="form-control" data-date-format="YYYY-MM-DD" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span>
                                    </div>
                                    <?php if ($error_date_end) { ?>
                                        <div class="text-danger"><?php echo $error_date_end; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="status" id="input-status" class="form-control">
                                        <?php if ($status) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-home"><?php echo $entry_home; ?></label>
                                <div class="col-sm-10">
                                    <select name="home" id="input-home" class="form-control">
                                        <?php if ($home) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_image">
                            <div class="table-responsive">
                                <table id="images" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left"><?php echo $entry_image; ?></td>
                                            <td class="text-left"><?php echo $entry_sort_order; ?></td>
                                            <td class="text-left"><?php echo $entry_featured; ?></td>
                                            <td class="text-left"><?php echo $column_action; ?></td>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <?php $image_row = 0; ?>
                                        <?php foreach ($news_images as $news_image) { ?>
                                            <tr id="image-row<?php echo $image_row; ?>">
                                                <td class="text-left">
                                                    <img src="<?php echo $news_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                                    <input type="hidden" name="news_image[<?php echo $image_row; ?>][image_text]" value="<?php echo $news_image['image']; ?>" id="input-image<?php echo $image_row; ?>" />
                                                </td>
                                                <td class="text-right"><input type="text" name="news_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $news_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                                                <td class="text-right"><input type="text" name="news_image[<?php echo $image_row; ?>][featured]" value="<?php echo $news_image['featured']; ?>" placeholder="<?php echo $entry_featured; ?>" class="form-control" /> </td>

                                                <td class="text-left"><button type="button" onclick="$('#image-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                            </tr>
                                            <?php $image_row++; ?>
                                        <?php } ?>
                                    </tbody>
                                    
                                    <tfoot>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_links">          
                            <div class="form-group shop-field">
                                <label class="col-sm-2 control-label" for="input-shops"><?php echo $entry_shop_id; ?></label>   
                                <div class="col-sm-10">
                                    <select class="form-control" name="shops[]" id="input-shops" data-input-type="shop"  multiple  placeholder="<?php echo $entry_shop_id; ?>"></select>
                                </div>
                            </div>

                            <div class="form-group brand-field">
                                <label class="col-sm-2 control-label" for="input-brands"><?php echo $entry_brand_id; ?></label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="brands[]" id="input-brands" data-input-type="brand"  multiple  placeholder="<?php echo $entry_brand_id; ?>"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    var dataSource_shop = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=news/news/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function (url) {
                return url.replace('%TYPE', encodeURIComponent($('.shop-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.shop-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {id: v.id, name: v.name};
                });
            }
        }
    });
    dataSource_shop.initialize();
    $('.shop-field > > #input-shops').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource_shop',
            limit: 1000,
            displayKey: 'name',
            source: dataSource_shop.ttAdapter()
        }
    });


    var dataSource_brand = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'index.php?route=news/news/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
            replace: function (url) {
                return url.replace('%TYPE', encodeURIComponent($('.brand-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.brand-field .tt-input').val()));
            },
            wildcard: '%QUERY',
            filter: function (list) {
                return $.map(list, function (v, i) {
                    return {id: v.id, name: v.name};
                });
            }
        }
    });
    dataSource_brand.initialize();
    $('.brand-field > > #input-brands').tagsinput({
        itemValue: 'id',
        itemText: 'name',
        typeaheadjs: {
            name: 'dataSource_brand',
            displayKey: 'name',
            source: dataSource_brand.ttAdapter()
        }
    });
//--></script> 

<?php if (isset($shops) && !empty($shops)) { ?>
    <script type="text/javascript">
        var subStoresArray = <?php echo json_encode($shops) ?>;
        if (subStoresArray.length > 0) {
            $.each(subStoresArray, function (index, value) {
                $('#input-shops').tagsinput('add', {"id": value.shop_id, "name": "" + value.name.replace(/'/g, "\\'") + ""});
            });
        }
    </script>
<?php } ?>
    
<?php if (isset($brands) && !empty($brands)) { ?>
    <script type="text/javascript">
        var subStoresArray = <?php echo json_encode($brands) ?>;
        if (subStoresArray.length > 0) {
            $.each(subStoresArray, function (index, value) {
                $('#input-brands').tagsinput('add', {"id": value.brand_id, "name": "" + value.name.replace(/'/g, "\\'") + ""});
            });
        }
    </script>
<?php } ?>


<script type="text/javascript">
    <!--
  $('.end-date, #input-start_date').datetimepicker({
        pickTime: false
    });
    $('.start-date, #input-end_date').datetimepicker({
        pickTime: false
    });
-->
</script>
<script type="text/javascript">
    <!--
  var image_row = <?php echo $image_row; ?>;
    function addImage() {
        html = '<tr id="image-row' + image_row + '">';
        html += '  <td class="text-left"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /><input type="file" name="news_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
        html += '  <td class="text-right"><input type="text" name="news_image[' + image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
        html += '  <td class="text-right"><input type="text" name="news_image[' + image_row + '][featured]" value="" placeholder="<?php echo $entry_featured; ?>" class="form-control" /></td>';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#images tbody').append(html);

        image_row++;
    }
    //-->
</script>
<script type="text/javascript">
<!--
<?php foreach ($languages as $language) { ?>
        // $("#input-description<?php echo $language['language_id']; ?>").summernote();
        CKEDITOR.replace("input-body_text<?php echo $language['language_id']; ?>", {
            filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
            filebrowserWindowWidth: 800,
            filebrowserWindowHeight: 500
        });
<?php } ?>
    //-->
</script>
<script type="text/javascript">
    <!--
  $('#language a:first').tab('show');
    //-->
</script>

<?php echo $footer; ?>