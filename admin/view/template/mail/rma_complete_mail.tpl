<div>
<p >
  Hello <?php echo $customer_name ?> <br>
  Thank you for choosing Sayidaty Mall as your online shopping destination.
</p>
</div>

<div>
<p>We would like to inform you that your return request from order #<?php echo $order_id ?> has been completed & we have credited <?php echo $store_credit_value ?> into your account.</p>


<p>In case you didn’t receive your credit balance, please contact us on 8004330033 (UAE/KSA-Toll Free) or drop an email mall@sayidaty.net and we will be happy to assist you.</p>
</div>

 
