<!doctype html>
<!--Quite a few clients strip your Doctype out, and some even apply their own. Many clients do honor your doctype and it can make things much easier if you can validate constantly against a Doctype.-->
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>New Return</title>

<!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
<!-- When use in Email please remove all comments as it is removed by Email clients-->
<!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->

<style type="text/css">
body {
	margin: 0;
}
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, 'Times New Roman', serif;
	font-style: normal;
	font-weight: 400;
}
button{
	width:90%;
}
@media screen and (max-width:600px) {
/*styling for objects with screen size less than 600px; */
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, 'Times New Roman', serif;
}
table {
	/* All tables are 100% width */
	width: 100%;
}
.footer {
	/* Footer has 2 columns each of 48% width */
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
table.responsiveImage {
	/* Container for images in catalog */
	height: auto !important;
	max-width: 30% !important;
	width: 30% !important;
}
table.responsiveContent {
	/* Content that accompanies the content in the catalog */
	height: auto !important;
	max-width: 66% !important;
	width: 66% !important;
}
.top {
	/* Each Columnar table in the header */
	height: auto !important;
	max-width: 48% !important;
	width: 48% !important;
}
.catalog {
	margin-left: 0%!important;
}

}
@media screen and (max-width:480px) {
/*styling for objects with screen size less than 480px; */
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, 'Times New Roman', serif;
}
table {
	/* All tables are 100% width */
	width: 100% !important;
	border-style: none !important;
}
.footer {
	/* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.table.responsiveImage {
	/* Container for each image now specifying full width */
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.table.responsiveContent {
	/* Content in catalog  occupying full width of cell */
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.top {
	/* Header columns occupying full width */
	height: auto !important;
	max-width: 100% !important;
	width: 100% !important;
}
.catalog {
	margin-left: 0%!important;
}
button{
	width:90%!important;
}
}
</style>
</head>
<body yahoo="yahoo">
	<?php echo $email_header ?>
<table width="100%"  cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td><table width="600"  align="center" cellpadding="0" cellspacing="0">
          <!-- Main Wrapper Table with initial width set to 60opx -->
          <tbody>
            <tr>
              <!-- HTML Spacer row -->
              <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
            </tr>
            <tr>
              <!-- HTML IMAGE SPACER -->
              <td style="font-size: 0; line-height: 1.5;" >
                <table align="center"  cellpadding="0" width="100%" cellspacing="0" >
                  <tr>
                    <td style="padding: 0 20px;">
                      <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 18px; padding: 10px 0;"><b>Order Return Request!</b></p>
											<p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 18px; padding: 10px 0;"><?php echo $text_greeting ?></p>
											<p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 18px; padding: 10px 0;">
                        A customer has placed a return request for the order number
                        <span style="color: #d91a5d;"><?php echo $order_id; ?></span>.
                        <br><br>
  											<b> Return request number </b><span style="color: #d91a5d;"><?php echo $rma_id ?></span>.<br><br>
  						          Please review the order summary below and contact the customer to validate the request.<br><br>
                        To view your order click <a href="<?php echo $rma_link ?>">this link</a><br>
                      </p>
                    </td>
                  </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="font-size: 0; line-height: 0;" height="10">
              <table width="96%" align="left"  cellpadding="0" cellspacing="0" ></table>
              <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                  <thead>
                    <tr>
                      <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222" colspan="2"><?php echo $text_order_detail; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px; line-height: 15px;"><b><?php echo $text_order_id; ?></b> <?php echo $order_id; ?><br>
                        <b><?php echo $text_date_added; ?></b> <span class="aBn" data-term="goog_39039013" tabindex="0"><span class="aQJ"><?php echo $date_added; ?></span></span><br>
                        <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?> <img src="https://ci3.googleusercontent.com/proxy/Qw7VcreceLXafv7TM0UtqO75gTO1rQtobLHc0lS8FcANFt5xkXsmIZjI1qvuCJnvtOTEdzwshqyxbIRSscDsCCdzlan55Gzfn7pq=s0-d-e1-ft#https://dnc2qm9v6i95t.cloudfront.net/icon/paytabs.png" class="CToWUd"> <br>
                        <?php if ($shipping_method) { ?>
                        <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
                        <?php } ?>
                      </td>
                      <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px; line-height: 15px;"><b><?php echo $text_email ?></b> <a href="mailto:<?php echo $email ?>" target="_blank"><?php echo $email ?></a><br>
                        <b><?php echo $text_telephone ?></b> <?php echo $telephone ?><br>
                        <b><br>
                          <b><?php echo $text_order_status ?></b> <?php echo $order_status ?><br>
                        </b></td>
                    </tr>
                  </tbody>
                </table>
                <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                  <thead>
                    <tr>
                      <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222"><?php echo $text_payment_address ?></td>
                      <?php if ($shipping_address) { ?>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222"><?php echo  $text_shipping_address ?></td>
                      <?php } ?>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px; line-height: 15px;"> <?php echo $payment_address; ?> </td>
                      <?php if ($shipping_address) { ?>
                        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;line-height: 15px;"><?php echo $shipping_address; ?></td>
                      <?php } ?>
                    </tr>
                  </tbody>
                </table>
                <table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
                  <thead>
                    <tr>
                      <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222"><?php echo $text_product; ?></td>
                      <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222"><?php echo $text_sku; ?></td>
                      <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222"><?php echo $text_model; ?></td>
                      <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222"><?php echo $text_quantity; ?></td>
                      <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222"><?php echo $text_price; ?></td>
                      <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222"><?php echo $text_total; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($products as $product) { ?>
                      <tr>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px; line-height: 15px;"><?php echo $product['name']; ?>
                          <?php foreach ($product['option'] as $option) { ?>
                          <br />
                          &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                          <?php } ?></td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px; line-height: 15px;"><?php echo $product['sku']; ?></td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px; line-height: 15px;"><?php echo $product['model']; ?></td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px; line-height: 15px;"><?php echo $product['quantity']; ?></td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px; line-height: 15px;"><?php echo $product['price']; ?></td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px; line-height: 15px;"><?php echo $product['total']; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <?php foreach ($totals as $total) { ?>
                      <tr>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px" colspan="5"><b><?php echo $total['title']; ?>:</b></td>
                        <td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px; line-height: 15px;"><?php echo $total['text']; ?></td>
                      </tr>
                    <?php } ?>
                  </tfoot>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding: 0 20px 10px;">
                  <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; padding: 0px 0; font-weight: bold;">
										Sincerely,<br>
										Sayidaty Mall Support Team.</p>
              </td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
<?php echo $email_footer ?>
</body>
</html>
