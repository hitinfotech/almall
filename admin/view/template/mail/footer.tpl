<table align="left" cellpadding="0" cellspacing="0" style="height: 117px;" width="700">
<tbody>
<tr>
<td align="center" style="text-align: center; padding: px;"><img data-cke-saved-src="<?php echo HTTPS_IMAGE_S3 ?>emails/follow.png" src="<?php echo HTTPS_IMAGE_S3 ?>emails/follow.png"></td>
</tr>
<tr>
<td align="center" style="padding: 0 20px 10px;"><a data-cke-saved-href="https://www.facebook.com/Sayidatymall/?fref=ts&amp;utm_source=mall&amp;utm_campaign=campaign100&amp;utm_medium=email" href="https://www.facebook.com/Sayidatymall/?fref=ts&amp;utm_source=mall&amp;utm_campaign=campaign100&amp;utm_medium=email"><img alt="" data-cke-saved-src="https://mall.sayidaty.net/image/catalog/fb.png" src="https://mall.sayidaty.net/image/catalog/fb.png" width="40"></a> <a data-cke-saved-href="https://twitter.com/sayidaty_mall&amp;utm_source=mall&amp;utm_campaign=campaign100&amp;utm_medium=email" href="https://twitter.com/sayidaty_mall&amp;utm_source=mall&amp;utm_campaign=campaign100&amp;utm_medium=email"><img alt="" data-cke-saved-src="https://mall.sayidaty.net/image/catalog/tw.png" src="https://mall.sayidaty.net/image/catalog/tw.png" width="40"></a> <a data-cke-saved-href="https://www.youtube.com/channel/UCw0FpmUg229kGcDqKKVj-Aw" href="https://www.youtube.com/channel/UCw0FpmUg229kGcDqKKVj-Aw"><img alt="" data-cke-saved-src="https://mall.sayidaty.net/image/catalog/yt.png" src="https://mall.sayidaty.net/image/catalog/yt.png" width="40"></a> <a data-cke-saved-href="https://www.instagram.com/sayidatymall" href="https://www.instagram.com/sayidatymall"><img alt="" data-cke-saved-src="https://mall.sayidaty.net/image/catalog/ins.png" src="https://mall.sayidaty.net/image/catalog/ins.png" width="40"></a></td>
</tr>
<tr>
<td height="20" style="font-size: 0; line-height: 0;"><table align="left" cellpadding="0" cellspacing="0" width="96%">
<tbody>
<tr>
<td height="20" style="font-size: 0; line-height: 0;"><br></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="leftt" cellpadding="0" cellspacing="0" class="footer" style="height: 216px;" width="700">
<tbody>
<tr>
<td><table style="height: 42px;" width="676">
<tbody>
<tr>
<td width="50%"><table width="100%">
<tbody>
<tr>
<td align="left" valign="middle"><img alt="" data-cke-saved-src="https://mall.sayidaty.net/image/catalog/mail.jpg" src="https://mall.sayidaty.net/image/catalog/mail.jpg"></td>
<td align="left" style="font-size: 12px; font-family: tahoma, 'Helvetica Neue', Helvetica, Arial, sans-serif;" valign="middle">Do you have questions?<br><a data-cke-saved-href="mailto:mall@sayidaty.net" href="mailto:mall@sayidaty.net" style="color: #d91a5d; text-decoration: none; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">mall@sayidaty.net</a></td>
</tr>
</tbody>
</table>
</td>
<td width="50%"><table style="height: 39px;" width="340">
<tbody>
<tr>
<td align="right" valign="middle"><img data-cke-saved-src="https://mall.sayidaty.net/image/catalog/phone.jpg" src="https://mall.sayidaty.net/image/catalog/phone.jpg"></td>
<td align="left" style="font-size: 12px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" valign="middle"><p style="direction: ltr; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align: left; margin: 0;">800 433 0033</p>
<p style="text-align: left; font-family: tahoma, Arial, Helvetica, sans-serif; margin: 0;">Sunday - Thursday 9:00 Am - 5:00 PM</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td height="20" style="font-size: 0; line-height: 0;"><br></td>
</tr>
<tr>
<td><table align="right" cellpadding="0" cellspacing="0" class="footer" style="height: 131px;" width="693">
<tbody>
<tr>
<td style="border-top: 1px solid #ddd;"><p style="font-size: 12px; font-style: normal; font-weight: normal; color: #555; line-height: 1.8; text-align: center; padding-top: 10px; margin-left: 20px; margin-right: 20px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">This email was sent to you by Sayidaty Mall. To ensure delivery to your inbox (not bulk or junk folders), please add our e-mail address, <a data-cke-saved-href="mailto:mall@sayidaty.net" href="mailto:mall@sayidaty.net" style="color: #d91a5d; text-decoration: none; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">mall@sayidaty.net</a>, to your address book.</p>
<p style="font-size: 12px; font-family: tahoma, Arial, Helvetica, sans-serif; text-align: center;">To view our Privacy Policy click here.To unsubscribe click here.Copyright © 2017 SRPC. All rights reserved.</p>
<p align="center" style="font-size: 11px; color: #333;"><a data-cke-saved-href="https://mall.sayidaty.net/index.php?route=ne/subscribe&amp;uid={uid}&amp;key={key}" href="https://mall.sayidaty.net/index.php?route=ne/subscribe&amp;uid={uid}&amp;key={key}" style="text-decoration: none; color: #7c7c7c;">Subscribe</a>&nbsp;|&nbsp;<a data-cke-saved-href="https://mall.sayidaty.net/index.php?route=ne/unsubscribe&amp;uid={uid}&amp;key={key}" href="https://mall.sayidaty.net/index.php?route=ne/unsubscribe&amp;uid={uid}&amp;key={key}" style="text-decoration: none; color: #7c7c7c;">Unsubscribe</a>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
