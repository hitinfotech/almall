<!doctype html>
<!--Quite a few clients strip your Doctype out, and some even apply their own. Many clients do honor your doctype and it can make things much easier if you can validate constantly against a Doctype.-->
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>New Return</title>

<!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
<!-- When use in Email please remove all comments as it is removed by Email clients-->
<!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->

<style type="text/css">
body {
	margin: 0;
}
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, 'Times New Roman', serif;
	font-style: normal;
	font-weight: 400;
}
button{
	width:90%;
}
@media screen and (max-width:600px) {
/*styling for objects with screen size less than 600px; */
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, 'Times New Roman', serif;
}
table {
	/* All tables are 100% width */
	width: 100%;
}
.footer {
	/* Footer has 2 columns each of 48% width */
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
table.responsiveImage {
	/* Container for images in catalog */
	height: auto !important;
	max-width: 30% !important;
	width: 30% !important;
}
table.responsiveContent {
	/* Content that accompanies the content in the catalog */
	height: auto !important;
	max-width: 66% !important;
	width: 66% !important;
}
.top {
	/* Each Columnar table in the header */
	height: auto !important;
	max-width: 48% !important;
	width: 48% !important;
}
.catalog {
	margin-left: 0%!important;
}

}
@media screen and (max-width:480px) {
/*styling for objects with screen size less than 480px; */
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, 'Times New Roman', serif;
}
table {
	/* All tables are 100% width */
	width: 100% !important;
	border-style: none !important;
}
.footer {
	/* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.table.responsiveImage {
	/* Container for each image now specifying full width */
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.table.responsiveContent {
	/* Content in catalog  occupying full width of cell */
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.top {
	/* Header columns occupying full width */
	height: auto !important;
	max-width: 100% !important;
	width: 100% !important;
}
.catalog {
	margin-left: 0%!important;
}
button{
	width:90%!important;
}
}
</style>
</head>
<body yahoo="yahoo">
<table width="100%"  cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td><table width="600"  align="center" cellpadding="0" cellspacing="0">
          <!-- Main Wrapper Table with initial width set to 60opx -->
          <tbody>
            <tr>
              <td align="left" style="padding:10px;">
                <a href="https://mall.sayidaty.net/" title="Sayidaty Mall" target="_blank" data-saferedirecturl="https://dnc2qm9v6i95t.cloudfront.net/catalog/logo/mall-en.png" alt="Sayidaty Mall" style="margin-bottom:20px;border:none" class="CToWUd">
                  <img src="https://dnc2qm9v6i95t.cloudfront.net/catalog/logo/mall-en.png" alt="Sayidaty Mall" style="margin-bottom:20px;border:none" class="CToWUd">
                </a>
            </td>
            </tr>
            <tr>
              <!-- HTML Spacer row -->
              <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
            </tr>
            <tr>
              <!-- HTML IMAGE SPACER -->
              <td style="font-size: 0; line-height: 1.5;" >
                <table align="center"  cellpadding="0" width="100%" cellspacing="0" >
                  <tr>
                    <td style="padding: 0 20px;">
                      <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 18px; padding: 10px 0;"><b>Order Return Request!</b></p>
                      <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 18px; padding: 10px 0;">
                        A customer has placed a return request for the order number
                        <span style="color: #d91a5d;"><?php echo $order_id; ?></span>.
                        <br><br>
  											<b> Return request number </b><span style="color: #d91a5d;"><?php echo $rma_id ?></span>.<br><br>
  						          Please login to your Mall Dashboard > RMA Requests to review the request and proceed to delivery.<br><br>
                        To view your request click <a href="<?php echo $rma_link ?>">this link</a><br>
                      </p>
                    </td>
                  </tr>
              </table>
            </td>
          </tr>
            <tr>
              <td style="padding: 40px 20px 20px;" align="center">
                  <img src="<?php echo HTTPS_IMAGE_S3 ?>emails/follow.png">
              </td>
            </tr>
            <tr>
              <td align="center" style="padding: 0 20px 10px;">
                  <a  href="https://www.facebook.com/Sayidatymall/?fref=ts" target="_blank"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/fb.png" width="40"></a>
                  <a href="https://twitter.com/sayidaty_mall" target="_blank"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/tw.png" width="40"></a>
                  <a href="https://www.youtube.com/channel/UCw0FpmUg229kGcDqKKVj-Aw" target="_blank"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/yt.png" width="40"></a>
                  <a href="https://www.instagram.com/sayidatymall" target="_blank"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/ins.png" width="40"></a>
              </td>
            </tr>
            <tr>
              <td>
                <table class="footer" width="100%"  align="left" cellpadding="0" cellspacing="0">
                  <!-- Second column of footer content -->
                  <tr>
                    <td style="border-top: 1px solid #ddd;"><p style="font-size: 12px; font-style: normal; font-weight:normal; color: #555; line-height: 1.8; text-align:justify;padding-top:10px; margin-left:20px; margin-right:20px; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; text-align: center;">To view our Privacy Policy   <a href="https://mall.sayidaty.net/sa/privacypolicy" target="_blank">click here.</a>
                      Copyright © 2016 SRPC. All rights reserved. </p>
                      </td>
                  </tr>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
</body>
</html>
