<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title><?php echo $title; ?></title>
    </head>
    <body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
        <div style="width: 680px;"><a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>"><img src="<?php echo $logo; ?>" alt="<?php echo $store_name; ?>" style="margin-bottom: 20px; border: none;" /></a>
            <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_greeting; ?></p>
            <?php if ($customer_id) { ?>
                <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_link; ?></p>
                <p style="margin-top: 0px; margin-bottom: 20px;"><a href="<?php echo $link; ?>"><?php echo $link; ?></a></p>
            <?php } ?>
            <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
                <thead>
                    <tr>
                        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;" colspan="2"><?php echo $text_order_detail; ?></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><b><?php echo $text_order_id; ?></b> <a href="<?php echo $link; ?>"><?php echo $order_id; ?></a><br />
                            <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?><br />
                            <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
                            <?php if ($shipping_method) { ?>
                                <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
                            <?php } ?></td>
                        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><b><?php echo $text_buyer_name; ?></b> <?php echo $buyer_name; ?><br />
                            <b><?php echo $text_buyer_country; ?></b> <?php echo $buyer_country; ?><br />
                            <?php /* echo $text_ip; ?><b></b> <?php echo $ip; */ ?><br />
                            <b><?php echo $text_order_status; ?></b> <span style="color: red"> <?php echo $order_status; ?></span><br /></td>
                    </tr>
                </tbody>
            </table>

            <table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
                <thead>
                    <tr>
                        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;"><?php echo $text_SKU; ?></td>
                        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;"><?php echo $text_product; ?></td>
                        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;"><?php echo $text_model; ?></td>
                        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;"><?php echo $text_quantity; ?></td>
                        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;"><?php echo $text_price; ?></td>
                        <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;"><?php echo $text_total; ?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products as $product) { ?>
                        <tr>
                            <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $product['SKU']; ?>
                            <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $product['name']; ?>
                                <?php foreach ($product['option'] as $option) { ?>
                                    <br />
                                    &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                <?php } ?></td>
                            <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><?php echo $product['model']; ?></td>
                            <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo $product['quantity']; ?></td>
                            <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo $product['price']; ?></td>
                            <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo $product['total']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <?php foreach ($totals as $total) { ?>
                        <tr>
                            <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="5"><b><?php echo $total['title']; ?>:</b></td>
                            <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;"><?php echo $total['text']; ?></td>
                        </tr>
                    <?php } ?>
                </tfoot>
            </table>
            <table>
                <tr>
                    <td style="padding: 0 20px 10px;">
                        <p style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 0px 0; font-weight: bold;">
                            Sincerely,<br>
                            Sayidaty Mall Support Team.</p>
                    </td>
                </tr>

                <tr>
                    <td style="padding: 40px 20px 20px;" align="center">
                        <img src="<?php echo HTTPS_IMAGE_S3 ?>emails/follow.png">
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding: 0 20px 10px;">
                        <a  href="https://www.facebook.com/Sayidatymall/?fref=ts" target="_blank"><img src="' . HTTPS_IMAGE_S3 . 'emails/fb.png" width="40"></a>
                        <a href="https://twitter.com/sayidaty_mall" target="_blank"><img src="' . HTTPS_IMAGE_S3 . 'emails/tw.png" width="40"></a>
                        <a href="https://www.youtube.com/channel/UCw0FpmUg229kGcDqKKVj-Aw" target="_blank"><img src="' . HTTPS_IMAGE_S3 . 'emails/yt.png" width="40"></a>
                        <a href="https://www.instagram.com/sayidatymall" target="_blank"><img src="' . HTTPS_IMAGE_S3 . 'emails/ins.png" width="40"></a>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table class="footer" width="100%"  align="left" cellpadding="0" cellspacing="0">
                            <!-- Second column of footer content -->
                            <tr>
                                <td style="border-top: 1px solid #ddd;"><p style="font-size: 12px; font-style: normal; font-weight:normal; color: #555; line-height: 1.8; text-align:justify;padding-top:10px; margin-left:20px; margin-right:20px; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center;">To view our Privacy Policy <a href="https://mall.sayidaty.net/sa/privacypolicy" target="_blank">click here.</a>
                                        Copyright © 2016 SRPC. All rights reserved. </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
