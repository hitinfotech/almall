<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-coupon').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_code; ?></label>
                                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_code; ?>" id="input-name" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-date-start"><?php echo $entry_date_start; ?></label>
                                <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="<?php echo $entry_date_start . ' date format 2017-01-31'; ?>" id="input-date-start" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <select name="filter_status" id="input-status" class="form-control">
                                    <option value="*"><?php echo $entry_status ?></option>
                                    <?php if ($filter_status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                    <?php } ?>
                                    <?php if (!$filter_status && !is_null($filter_status)) { ?>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
                                <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="<?php echo $entry_date_end . ' date format 2017-12-31'; ?>" id="input-date-end" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-used"><?php echo $entry_used; ?></label>
                                <select name="filter_used" id="input-used" class="form-control">
                                    <option value="*"><?php echo $entry_used ?></option>
                                    <?php if ($filter_used) { ?>
                                        <option value="1" selected="selected"><?php echo $text_used; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_used; ?></option>
                                    <?php } ?>
                                    <?php if (!$filter_used && !is_null($filter_used)) { ?>
                                        <option value="0" selected="selected"><?php echo $text_not_used; ?></option>
                                    <?php } else { ?>
                                        <option value="0"><?php echo $text_not_used; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-date-end2"><?php echo $entry_date_expired_on; ?></label>
                                <input type="text" name="filter_date_end2" value="<?php echo $filter_date_end2; ?>" placeholder="<?php echo $entry_date_expired_on . ' date format 2017-12-31'; ?>" id="input-date-end" class="form-control" />
                            </div>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-coupon">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                    <td class="text-left"><?php if ($sort == 'cd.name') { ?>
                                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'cd.notification') { ?>
                                            <a href="<?php echo $sort_notification; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Notification'; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_notification; ?>"><?php echo 'Notification'; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'c.code') { ?>
                                            <a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_code; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_code; ?>"><?php echo $column_code; ?></a>
                                        <?php } ?></td>
                                    <td class="text-right"><?php if ($sort == 'c.discount') { ?>
                                            <a href="<?php echo $sort_discount; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_discount; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_discount; ?>"><?php echo $column_discount; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'c.date_start') { ?>
                                            <a href="<?php echo $sort_date_start; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_start; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_start; ?>"><?php echo $column_date_start; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'c.date_end') { ?>
                                            <a href="<?php echo $sort_date_end; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_end; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_end; ?>"><?php echo $column_date_end; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'c.status') { ?>
                                            <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                        <?php } ?></td>
                                    <td class="text-right"><?php echo $column_action; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($coupons) { ?>
                                    <?php foreach ($coupons as $coupon) { ?>
                                        <tr>
                                            <td class="text-center"><?php if (in_array($coupon['coupon_id'], $selected)) { ?>
                                                    <input type="checkbox" name="selected[]" value="<?php echo $coupon['coupon_id']; ?>" checked="checked" />
                                                <?php } else { ?>
                                                    <input type="checkbox" name="selected[]" value="<?php echo $coupon['coupon_id']; ?>" />
                                                <?php } ?></td>
                                            <td class="text-left"><?php echo $coupon['name']; ?></td>
                                            <td class="text-left"><?php echo $coupon['notification'] == 1 ?'Yes' :'No'; ?></td>
                                            <td class="text-left"><?php echo $coupon['code']; ?></td>
                                            <td class="text-right"><?php echo $coupon['discount']; ?></td>
                                            <td class="text-left"><?php echo $coupon['date_start']; ?></td>
                                            <td class="text-left"><?php echo $coupon['date_end']; ?></td>
                                            <td class="text-left"><?php echo $coupon['status']; ?></td>
                                            <td class="text-right"><a href="<?php echo $coupon['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#button-filter').on('click', function () {
        var url = 'index.php?route=marketing/coupon&token=<?php echo $token; ?>';

        var filter_name = $('input[name=\'filter_name\']').val();

        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }

        var filter_date_start = $('input[name=\'filter_date_start\']').val();

        if (filter_date_start) {
            url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
        }

        var filter_date_end = $('input[name=\'filter_date_end\']').val();

        if (filter_date_end) {
            url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
        }

        var filter_date_end2 = $('input[name=\'filter_date_end2\']').val();

        if (filter_date_end2) {
            url += '&filter_date_end2=' + encodeURIComponent(filter_date_end2);
        }

        var filter_status = $('select[name=\'filter_status\']').val();

        if (filter_status != '*') {
            url += '&filter_status=' + encodeURIComponent(filter_status);
        }

        var filter_used = $('select[name=\'filter_used\']').val();

        if (filter_used != '*') {
            url += '&filter_used=' + encodeURIComponent(filter_used);
        }

        location = url;
    });
    //--></script>
<?php echo $footer; ?>