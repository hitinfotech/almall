<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-store" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-store" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-block6" data-toggle="tab"><?php echo $tab_block6; ?></a></li>
                        <li><a href="#tab-block5" data-toggle="tab"><?php echo $tab_block5; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab-block5">
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b5_t1"><?php echo $text_offer_banner_block5_image1; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_51; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="offer_banner_block5_image1" value="<?php echo $offer_banner_block5_image1; ?>" id="input-image_b5_t1" />
                                    <input type="hidden" name="offer_banner_block5_image1" value="<?php echo $offer_banner_block5_image1; ?>" />
                                    <?php if ($error_offer_banner_block5_image1) { ?>
                                        <div class="text-danger"><?php echo $error_offer_banner_block1_image1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane active" id="tab-block6">
                            <div class="form-group required">
                                <div class='row' style="margin-left: 20px" >
                                    <label class="col-sm-2 ">Language</label>
                                    <div class='col-sm-3 '>
                                        <select class="form-control" name='coutnry_langauge'>
                                            <option value='1' <?php echo ($page_language == 1 ? "selected" : "") ?> > english</option>
                                            <option value='2' <?php echo ($page_language == 2 ? "selected" : "") ?> > Arabic</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class='row' style="margin-left: 20px" >
                                    <?php foreach ($available_coutnries as $key => $country) { ?>
                                        <label class="col-sm-3 "><input class='in_country' data-country-id='<?php echo $country["country_id"] ?>' type='checkbox' name='page_country[<?php echo $country["country_id"] ?>]' <?php echo (isset($page_countries[$country["country_id"]]) ? "checked" : "") ?> ><?php echo $country["name"] ?>  </label>
                                        <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
