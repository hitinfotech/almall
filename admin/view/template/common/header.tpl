<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <?php if ($description) { ?>
            <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>
        <?php if ($keywords) { ?>
            <meta name="keywords" content="<?php echo $keywords; ?>" />
        <?php } ?>
        <script>window.APP = eval('(<?php echo json_encode($js_vars) ?>)');</script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
        <script type="text/javascript" src="view/javascript/jquery/jquery-2.2.0.min.js"></script>
        <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="view/javascript/typeahead/typeahead.bundle.min.js"></script>
        <script type="text/javascript" src="view/javascript/typeahead/bootstrap-tagsinput.min.js"></script>
        <link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />
        <?php if ($direction == "rtl"): ?>
            <link href="view/stylesheet/bootstrap-ar.css" type="text/css" rel="stylesheet" />
        <?php endif; ?>
        <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
        <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
        <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
        <?php $editor_routes = array('module/abandonedcarts','module/abandonmentcarts','catalog/information/edit','catalog/product/edit', 'ne/template/update', 'ne/newsletter','ne/schedule/insert'); ?>
        <?php if(isset($route) && (!in_array($route, $editor_routes))){ ?>
        <script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
        <?php }  ?>
        <script src="view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
        <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
        <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />
        <link href="view/javascript/typeahead/bootstrap-tagsinput.css" type="text/css" rel="stylesheet" />
        <?php if ($direction == "rtl"): ?>
            <link type="text/css" href="view/stylesheet/stylesheet-ar.css" rel="stylesheet" media="screen" />
        <?php endif; ?>
        <?php foreach ($styles as $style) { ?>
            <link type="text/css" href="<?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>
        <?php foreach ($links as $link) { ?>
            <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>
            
        <script src="view/javascript/common.js" type="text/javascript"></script>
        
        <?php foreach ($scripts as $script) { ?>
            <script type="text/javascript" src="<?php echo $script; ?>"></script>
        <?php } ?>
        <link rel="icon" href="<?php echo HTTP_CATALOG; ?>/image/catalog/opencart.ico">
        <?php if (!empty($_GET)): ?>
    <?php if ($_GET['route'] === 'catalog/product/edit' || $_GET['route'] === 'catalog/product/add'): ?>

        <link rel="stylesheet" href="view/javascript/instaloader2/css/style.css">
        <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
        <link rel="stylesheet" href="view/javascript/instaloader2/css/jquery.fileupload.css">
        <link rel="stylesheet" href="view/javascript/instaloader2/css/jquery.fileupload-ui.css">
        <!--link rel="stylesheet" href="view/javascript/instaloader2/jquery-ui-1.11.4/jquery-ui.min.css"-->

        <style>
            .alert {
                display: none;
            }
        </style>

    <?php elseif($_GET['route'] === 'catalog/product'): ?>
        <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
        <link rel="stylesheet" href="view/javascript/instaloader2/css/gallery.css">
    <?php endif; ?>
    <?php endif; ?>
    </head>
    <body>
        <div id="container">
            <header id="header" class="navbar navbar-static-top">
                <div class="navbar-header">
                    <?php if ($logged) { ?>
                        <a type="button" id="button-menu" class="pull-left"><i class="fa fa-indent fa-lg"></i></a>
                    <?php } ?>
                    <a href="<?php echo $home; ?>" class="navbar-brand">
                        <img src="https://dnc2qm9v6i95t.cloudfront.net/catalog/logo/mall-en.png" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" style="width: 57px"/>
                    </a>
                </div>
                <span style="display: inline-block; font-weight: bold; margin: 12px;"><?php echo isset($username) ? "Welcome: " . $username : "" ?></span>
                <?php if ($logged) { ?>
                    <?php if (ENVIROMENT == "development") { // OUR Menu ?>
                        <ul class="nav pull-right">
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><span class="label label-danger pull-left"><?php echo $alerts; ?></span> <i class="fa fa-bell fa-lg"></i></a>
                                <ul class="dropdown-menu dropdown-menu-right alerts-dropdown">
                                    <li class="dropdown-header"><?php echo $text_order; ?></li>
                                    <li><a href="<?php echo $processing_status; ?>" style="display: block; overflow: auto;"><span class="label label-warning pull-right"><?php echo $processing_status_total; ?></span><?php echo $text_processing_status; ?></a></li>
                                    <li><a href="<?php echo $complete_status; ?>"><span class="label label-success pull-right"><?php echo $complete_status_total; ?></span><?php echo $text_complete_status; ?></a></li>
                                    <li><a href="<?php echo $return; ?>"><span class="label label-danger pull-right"><?php echo $return_total; ?></span><?php echo $text_return; ?></a></li>
                                    <li class="divider"></li>
                                    <li class="dropdown-header"><?php echo $text_customer; ?></li>
                                    <li><a href="<?php echo $online; ?>"><span class="label label-success pull-right"><?php echo $online_total; ?></span><?php echo $text_online; ?></a></li>
                                    <li><a href="<?php echo $customer_approval; ?>"><span class="label label-danger pull-right"><?php echo $customer_total; ?></span><?php echo $text_approval; ?></a></li>
                                    <li class="divider"></li>
                                    <li class="dropdown-header"><?php echo $text_product; ?></li>
                                    <li><a href="<?php echo $product; ?>"><span class="label label-danger pull-right"><?php echo $product_total; ?></span><?php echo $text_stock; ?></a></li>
                                    <li><a href="<?php echo $review; ?>"><span class="label label-danger pull-right"><?php echo $review_total; ?></span><?php echo $text_review; ?></a></li>
                                    <li class="divider"></li>
                                    <li class="dropdown-header"><?php echo $text_affiliate; ?></li>
                                    <li><a href="<?php echo $affiliate_approval; ?>"><span class="label label-danger pull-right"><?php echo $affiliate_total; ?></span><?php echo $text_approval; ?></a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-life-ring fa-lg"></i></a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                  <?php foreach ($stores as $store) { ?>
                                        <li><a href="<?php echo $store['href']; ?>" target="_blank"><?php echo $store['name']; ?></a></li>
                                    <?php } ?>
                                    <?php /*<li class="divider"></li>
                                    <li class="dropdown-header"><?php echo $text_help; ?> <i class="fa fa-life-ring"></i></li>
                                    <li><a href="http://www.opencart.com" target="_blank"><?php echo $text_homepage; ?></a></li>
                                    <li><a href="http://docs.opencart.com" target="_blank"><?php echo $text_documentation; ?></a></li>
                                    <li><a href="http://forum.opencart.com" target="_blank"><?php echo $text_support; ?></a></li>*/ ?>
                                </ul>
                            </li>
                            <li><a href="<?php echo $logout; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_logout; ?></span> <i class="fa fa-sign-out fa-lg"></i></a></li>
                        </ul>
                    <?php } else { ?>
                        <ul class="nav pull-right">
                            <?php foreach ($stores as $store) { ?>
                                <li><a href="<?php echo $store['href']; ?>" target="_blank"><?php echo $store['name']; ?></a></li>
                            <?php } ?>
                            <li class="divider"></li>
                            <li><a href="<?php echo $logout; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_logout; ?></span> <i class="fa fa-sign-out fa-lg"></i></a></li>
                        </ul>
                    <?php } ?>
                <?php } ?>
            </header>
