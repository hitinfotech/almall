<ul id="menu">
    <?php if($userac->hasPermission('access','common/dashboard')) {  ?>
    <li id="dashboard"><a href="<?php echo $home; ?>"><i class="fa fa-dashboard fa-fw"></i> <span><?php echo $text_dashboard; ?></span></a></li>
    <?php } ?>

    <li id="mall"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span><?php echo $text_new_features; ?></span></a>
        <ul>
            <?php if($userac->hasPermission('access','mall/main_group')) {  ?>
            <li><a href="<?php echo $main_group; ?>"><?php echo $text_main_group; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','mall/main_store')) {  ?>
            <li><a href="<?php echo $main_store; ?>"><?php echo $text_main_store; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','mall/salesmanago')) {  ?>
                <li><a href="<?php echo $salesmanago; ?>"><?php echo $text_salesmanago; ?></a></li>
            <?php } ?>

            <?php if($userac->hasPermission('access','mall/shop')) {  ?>
            <li><a href="<?php echo $shop; ?>"><?php echo $text_shop; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','mall/shopdesigner')) {  ?>
            <li><a href="<?php echo $shopdesigner; ?>"><?php echo $text_shopdesigner; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','mall/store_category')) {  ?>
            <li><a href="<?php echo $store_category; ?>"><?php echo $text_store_category; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','mall/mall')) {  ?>
            <li><a href="<?php echo $malls; ?>"><?php echo $text_malls; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','offer/offer')) {  ?>
            <li><a href="<?php echo $offers; ?>"><?php echo $text_offers; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','news/news')) {  ?>
            <li><a href="<?php echo $news; ?>"><?php echo $text_news; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','mall/brand')) {  ?>
            <li><a href="<?php echo $brand; ?>"><?php echo $text_brand; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','mall/tip')) {  ?>
            <li><a href="<?php echo $tip; ?>"><?php echo $text_tip; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','mall/tag')) {  ?>
            <li><a href="<?php echo $tag; ?>"><?php echo $text_tag; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','mall/look')) {  ?>
            <li><a href="<?php echo $look; ?>"><?php echo $text_look; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','toppages/toppages')) {  ?>
            <li><a href="<?php echo $toppages; ?>"><?php echo $text_toppages; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','mall/keyword')) {  ?>
            <li><a href="<?php echo $keyword; ?>"><?php echo $text_keyword; ?></a></li>
            <?php } ?>
            <?php /* <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li> */ ?>
        </ul>
    </li>
    <li id="slider"><a class="parent"><i class="fa fa-html5 fa-fw"></i> <span><?php echo $text_homepage; ?></span></a>
        <ul>
            <?php if($userac->hasPermission('access','homepage/homepage')) {  ?>
            <li><a href="<?php echo $homepage; ?>"><?php echo $text_homepage; ?></a></li>
            <?php } ?>
        </ul>
    </li>
    <?php if (isset($marketplace_status) && $marketplace_status) { ?>
        <li id="customerpartner"><a class="parent"><i class="fa fa-users"></i> <span><?php echo "Market Place"; ?></span></a>
            <ul>
                <li id="3pl"><a class="parent"><span><?php echo $text_marketplace_shipping; ?></span></a>
                    <ul>
                        <?php if($userac->hasPermission('access','customerpartner/shipping_order')) {  ?>
                        <?php foreach ($shipping_companies as $key => $company) { ?>
                            <li><a class="parent"><span><?php echo $company['name']; ?></span></a>
                                <ul>
                                    <li><a href="<?php echo $orders_in_pickup . '&shipping_company=' . $company['ship_company_id'] . '&order_case=3' ?>"><?php echo $text_order_in_pickup; ?></a></li>
                                    <li><a href="<?php echo $orders_in_fullfillment . '&shipping_company=' . $company['ship_company_id'] . '&order_case=1' ?>"><?php echo $text_order_in_fullfillment; ?></a></li>
                                    <li><a href="<?php echo $orders_in_dilevery . '&shipping_company=' . $company['ship_company_id'] . '&order_case=2' ?>"><?php echo $text_orders_in_delivery; ?></a></li>
                                    <li><a href="<?php echo $orders_in_dilevery . '&shipping_company=' . $company['ship_company_id'] . '&order_case=4' ?>"><?php echo $text_delivered_orders; ?></a></li>
                                    <li><a href="<?php echo $company_returns . '&shipping_company=' . $company['ship_company_id'] . '&return_case=1' ?>"><?php echo $text_returned_orders_in_pickup; ?></a></li>
                                    <li><a href="<?php echo $company_returns . '&shipping_company=' . $company['ship_company_id'] . '&return_case=2' ?>"><?php echo $text_returned_orders_in_fullfillment; ?></a></li>
                                    <li><a href="<?php echo $company_returns . '&shipping_company=' . $company['ship_company_id'] . '&return_case=3' ?>"><?php echo $text_returned_orders_in_delivery; ?></a></li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php } ?>
                    </ul>
                </li>
                <?php if($userac->hasPermission('access','customerpartner/partner')) {  ?>
                <li><a href="<?php echo $cp_partnerlist; ?>"><?php echo "Partners"; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','customerpartner/commission')) {  ?>
                <li><a href="<?php echo $cp_commission; ?>"><?php echo "Commission"; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','customerpartner/product')) {  ?>
                <li><a href="<?php echo $cp_productlist ?>"><?php echo "Products"; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','customerpartner/order')) {  ?>
                <li><a href="<?php echo $cp_order ?>"><?php echo "Orders"; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','customerpartner/order_tracking')) {  ?>
                <li><a href="<?php echo $cp_order_tracking ?>"><?php echo "Orders tracking"; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','customerpartner/order_finance_report')) {  ?>
                <li><a href="<?php echo $cp_order_report ?>"><?php echo "Orders Finance Report"; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','customerpartner/seller')) {  ?>
                <li><a href="<?php echo $cp_seller ?>"><?php echo "Seller"; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','customerpartner/income')) {  ?>
                <li><a href="<?php echo $cp_income ?>"><?php echo "Income"; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','customerpartner/transaction')) {  ?>
                <li><a href="<?php echo $cp_transaction ?>"><?php echo "Transaction"; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','customerpartner/shipping')) {  ?>
                <li><a href="<?php echo $cp_shipping ?>"><?php echo "Shipping"; ?></a></li>
                <?php } ?>
                <?php if (isset($wktwilio_status) && $wktwilio_status) { ?>
                    <li><a href="<?php echo $cp_sms ?>"><?php echo "SMS"; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','customerpartner/mails')) {  ?>
                <li><a href="<?php echo $cp_mail ?>"><?php echo "Mail"; ?></a></li>
                <?php } ?>
                <?php if ($wk_rma_status) { ?>
                    <li id="rma"><a class="parent"><span><?php echo "RMA"; ?></span></a>
                        <ul>
                            <?php if($userac->hasPermission('access','catalog/wk_rma_admin')) {  ?>
                            <li><a href="<?php echo $rma_rma ?>"><?php echo "Manage RMA(s)"; ?></a></li>
                            <?php } ?>
                            <?php if($userac->hasPermission('access','catalog/wk_rma_reason')) {  ?>
                            <li><a href="<?php echo $rma_reason ?>"><?php echo "Manage RMA Reasons"; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
                <?php if($userac->hasPermission('access','wkcustomfield/wkcustomfield')) {  ?>
                <li><a href="<?php echo $wk_customfield; ?>"><?php echo "Custom fields"; ?></a></li>
                <?php } ?>
            </ul>
        </li>
    <?php } ?>
    <li id="category"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span><?php echo $text_catalog; ?></span></a>
        <ul>
            <?php if($userac->hasPermission('access','catalog/category')) {  ?>
            <li><a href="<?php echo $category; ?>"><?php echo $text_category; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','catalog/search')) {  ?>
            <li><a href="<?php echo $search; ?>"><?php echo $text_search; ?></a></li>
            <?php } ?>
            <?php /* <li><a href="<?php echo $group; ?>"><?php echo $text_group; ?></a></li> */ ?>
            <li id="products"><a class="parent"><span><?php echo $text_product; ?></span></a>
                <ul>
                    <?php if($userac->hasPermission('access','catalog/product')) {  ?>
                    <li><a href="<?php echo $product; ?>"><?php echo $text_product; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','catalog/product_landing')) {  ?>
                    <li><a href="<?php echo $product_landing; ?>"><?php echo $text_product_landing; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','catalog/product_sheet_upload')) {  ?>
                    <li><a href="<?php echo $product_sheet_upload; ?>"><?php echo $text_product_mass_upload; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','module/productbundles')) {  ?>
                    <li><a href="index.php?route=module/productbundles">Product Bundles</a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','module/iproductvideo')) {  ?>
                    <li><a href="<?php echo $add_video_to_product; ?>"><?php echo $text_add_video_to_product; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','catalog/specialcrons')) {  ?>
                    <li><a href="<?php echo $specialcrons; ?>"><?php echo $text_specialcrons; ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php if($userac->hasPermission('access','catalog/landing_pages')) {  ?>
              <li><a href="<?php echo $landing_pages; ?>"><?php echo $text_landing_pages; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','catalog/mother_day')) {  ?>
            <li><a href="<?php echo $mother_day; ?>"><?php echo $text_mother_day; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','catalog/newin')) {  ?>
            <li><a href="<?php echo $ramadan; ?>"><?php echo $text_ramadan; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','catalog/recurring')) {  ?>
            <li><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','catalog/filter')) {  ?>
            <li><a href="<?php echo $filter; ?>"><?php echo $text_filter; ?></a></li>
            <?php } ?>
            <li id="attribute"><a class="parent"><?php echo $text_attribute; ?></a>
                <ul>
                    <?php if($userac->hasPermission('access','catalog/attribute')) {  ?>
                    <li><a href="<?php echo $attribute; ?>"><?php echo $text_attribute; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','catalog/attribute_group')) {  ?>
                    <li><a href="<?php echo $attribute_group; ?>"><?php echo $text_attribute_group; ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php if($userac->hasPermission('access','catalog/option')) {  ?>
            <li><a href="<?php echo $option; ?>"><?php echo $text_option; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','catalog/manufacturer')) {  ?>
            <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','catalog/download')) {  ?>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','catalog/review')) {  ?>
            <li><a href="<?php echo $review; ?>"><?php echo $text_review; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','catalog/information')) {  ?>
            <li><a href="<?php echo $information; ?>"><?php echo $text_information; ?></a></li>
            <?php } ?>
        </ul>
    </li>
    <li id="algolia"><a class="parent"><i class="fa fa-tags fa-fw"></i> <span><?php echo 'Algolia Search' ?></span></a>
        <ul>
            <?php if($userac->hasPermission('access','catalog/algoliasearch')) {  ?>
            <li><a href="<?php echo $algolialist; ?>"><?php echo 'Algolia Search'; ?></a></li>
            <?php } ?>
        </ul>
    </li>

    <li id="extension"><a class="parent"><i class="fa fa-puzzle-piece fa-fw"></i><span><?php echo $text_extension; ?></span></a>
        <ul>
            <?php if($userac->hasPermission('access','extension/installer')) {  ?>
            <li><a href="<?php echo $installer; ?>"><?php echo $text_installer; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','extension/modification')) {  ?>
            <li><a href="<?php echo $modification; ?>"><?php echo $text_modification; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','extension/analytics')) {  ?>
            <li><a href="<?php echo $analytics; ?>"><?php echo $text_analytics; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','extension/captcha')) {  ?>
            <li><a href="<?php echo $captcha; ?>"><?php echo $text_captcha; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','extension/feed')) {  ?>
            <li><a href="<?php echo $feed; ?>"><?php echo $text_feed; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','tool/excel_export_orders')) {  ?>
            <li><a href="<?php echo $excel_export_orders; ?>"><?php echo $text_excel_export_orders; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','extension/fraud')) {  ?>
            <li><a href="<?php echo $fraud; ?>"><?php echo $text_fraud; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','extension/module')) {  ?>
            <li><a href="<?php echo $module; ?>"><?php echo $text_module; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','extension/payment')) {  ?>
            <li><a href="<?php echo $payment; ?>"><?php echo $text_payment; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','extension/shipping')) {  ?>
            <li><a href="<?php echo $shipping; ?>"><?php echo $text_shipping; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','extension/total')) {  ?>
            <li><a href="<?php echo $total; ?>"><?php echo $text_total; ?></a></li>
            <?php } ?>
            <?php if ($openbay_show_menu == 1) { ?>
                <li id="openbay"><a class="parent"><?php echo $text_openbay_extension; ?></a>
                    <ul>
                        <?php if($userac->hasPermission('access','extension/openbay')) {  ?>
                        <li><a href="<?php echo $openbay_link_extension; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
                        <?php } ?>
                        <?php if($userac->hasPermission('access','extension/openbay/orderlist')) {  ?>
                        <li><a href="<?php echo $openbay_link_orders; ?>"><?php echo $text_openbay_orders; ?></a></li>
                        <?php } ?>
                        <?php if($userac->hasPermission('access','extension/openbay/items')) {  ?>
                        <li><a href="<?php echo $openbay_link_items; ?>"><?php echo $text_openbay_items; ?></a></li>
                        <?php } ?>
                        <?php if ($openbay_markets['ebay'] == 1) { ?>
                            <li id="ebay"><a class="parent"><?php echo $text_openbay_ebay; ?></a>
                                <ul>
                                    <?php if($userac->hasPermission('access','openbay/ebay')) {  ?>
                                    <li><a href="<?php echo $openbay_link_ebay; ?>"><?php //echo $text_openbay_dashboard; ?></a></li>
                                    <?php } ?>
                                    <?php if($userac->hasPermission('access','openbay/ebay')) {  ?>
                                    <li><a href="<?php echo $openbay_link_ebay_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
                                    <?php } ?>
                                    <?php if($userac->hasPermission('access','openbay/ebay')) {  ?>
                                    <li><a href="<?php echo $openbay_link_ebay_links; ?>"><?php echo $text_openbay_links; ?></a></li>
                                    <?php } ?>
                                    <?php if($userac->hasPermission('access','openbay/ebay')) {  ?>
                                    <li><a href="<?php echo $openbay_link_ebay_orderimport; ?>"><?php echo $text_openbay_order_import; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } else { ?>
                            <li id=""><a href="<?php echo $link['href']; ?>"><?php echo $link['text']; ?></a></li>
                        <?php } ?>
                        <?php if ($openbay_markets['amazon'] == 1) { ?>
                            <li id="amazon"><a class="parent"><?php echo $text_openbay_amazon; ?></a>
                                <ul>
                                    <?php if($userac->hasPermission('access','openbay/amazon')) {  ?>
                                    <li><a href="<?php echo $openbay_link_amazon; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
                                    <?php } ?>
                                    <?php if($userac->hasPermission('access','openbay/amazon')) {  ?>
                                    <li><a href="<?php echo $openbay_link_amazon_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
                                    <?php } ?>
                                    <?php if($userac->hasPermission('access','openbay/amazon')) {  ?>
                                    <li><a href="<?php echo $openbay_link_amazon_links; ?>"><?php echo $text_openbay_links; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($openbay_markets['amazonus'] == 1) { ?>
                            <li id="amazonus"><a class="parent"><?php echo $text_openbay_amazonus; ?></a>
                                <ul>
                                    <?php if($userac->hasPermission('access','openbay/amazonus')) {  ?>
                                    <li><a href="<?php echo $openbay_link_amazonus; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
                                    <?php } ?>
                                    <?php if($userac->hasPermission('access','openbay/amazonus')) {  ?>
                                    <li><a href="<?php echo $openbay_link_amazonus_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
                                    <?php } ?>
                                    <?php if($userac->hasPermission('access','openbay/amazonus')) {  ?>
                                    <li><a href="<?php echo $openbay_link_amazonus_links; ?>"><?php echo $text_openbay_links; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ($openbay_markets['etsy'] == 1) { ?>
                            <li id="etsy"><a class="parent"><?php echo $text_openbay_etsy; ?></a>
                                <ul>
                                    <?php if($userac->hasPermission('access','openbay/etsy')) {  ?>
                                    <li><a href="<?php echo $openbay_link_etsy; ?>"><?php echo $text_openbay_dashboard; ?></a></li>
                                    <?php } ?>
                                    <?php if($userac->hasPermission('access','openbay/etsy')) {  ?>
                                    <li><a href="<?php echo $openbay_link_etsy_settings; ?>"><?php echo $text_openbay_settings; ?></a></li>
                                    <?php } ?>
                                    <?php if($userac->hasPermission('access','openbay/etsy')) {  ?>
                                    <li><a href="<?php echo $openbay_link_etsy_links; ?>"><?php echo $text_openbay_links; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </li>
    <li id="design"><a class="parent"><i class="fa fa-television fa-fw"></i> <span><?php echo $text_design; ?></span></a>
        <ul>
            <?php if($userac->hasPermission('access','design/layout')) {  ?>
            <li><a href="<?php echo $layout; ?>"><?php echo $text_layout; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','design/banner')) {  ?>
            <li><a href="<?php echo $banner; ?>"><?php echo $text_banner; ?></a></li>
            <?php } ?>
        </ul>
    </li>
    <li id="sale"><a class="parent"><i class="fa fa-shopping-cart fa-fw"></i> <span><?php echo $text_sale; ?></span></a>
        <ul>
            <?php if($userac->hasPermission('access','sale/order')) {  ?>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','sale/shipping_process')) {  ?>
            <li><a href="<?php echo $link_shipping_process; ?>"><?php echo $text_shipping_process; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','sale/recurring')) {  ?>
            <li><a href="<?php echo $order_recurring; ?>"><?php echo $text_order_recurring; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','sale/return')) {  ?>
            <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
            <?php } ?>
            <li id="voucher"><a class="parent"><?php echo $text_voucher; ?></a>
                <ul>
                    <?php if($userac->hasPermission('access','sale/voucher')) {  ?>
                    <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','sale/voucher_theme')) {  ?>
                    <li><a href="<?php echo $voucher_theme; ?>"><?php echo $text_voucher_theme; ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php if($userac->hasPermission('access','module/automatednewsletter')) {  ?>
            <li><a href="<?php echo $AutomatedNewsletterURL; ?>"><?php echo "Automated Newsletters"; ?></a></li>
            <?php } ?>
            <li id="paypal_search"><a class="parent"><?php echo $text_paypal ?></a>
                <ul>
                    <?php if($userac->hasPermission('access','payment/pp_express')) {  ?>
                    <li><a href="<?php echo $paypal_search ?>"><?php echo $text_paypal_search ?></a></li>
                    <?php } ?>
                </ul>
            </li>
        </ul>
    </li>
    <li id="customer"><a class="parent"><i class="fa fa-user fa-fw"></i> <span><?php echo $text_customer; ?></span></a>
        <ul>
            <?php if($userac->hasPermission('access','customer/customer')) {  ?>
            <li><a href="<?php echo $customer; ?>"><?php echo $text_customer; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','customer/customer_group')) {  ?>
            <li><a href="<?php echo $customer_group; ?>"><?php echo $text_customer_group; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','customer/custom_field')) {  ?>
            <li><a href="<?php echo $custom_field; ?>"><?php echo $text_custom_field; ?></a></li>
            <?php } ?>
        </ul>
    </li>
    <li id="marketing"><a class="parent"><i class="fa fa-share-alt fa-fw"></i> <span><?php echo $text_marketing; ?></span></a>
        <ul>
            <?php if($userac->hasPermission('access','marketing/popup')) {  ?>
            <li><a href="<?php echo $popup; ?>"><?php echo $text_popup; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','marketing/offer_banner')) {  ?>
            <li><a href="<?php echo $offer_banner; ?>"><?php echo $text_offer_banner; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','marketing/upper_banner')) {  ?>
            <li><a href="<?php echo $upper_banner; ?>"><?php echo $text_upper_banner; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','marketing/marketing')) {  ?>
            <li><a href="<?php echo $marketing; ?>"><?php echo $text_marketing; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','marketing/affiliate')) {  ?>
            <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','marketing/coupon')) {  ?>
            <li><a href="<?php echo $coupon; ?>"><?php echo $text_coupon; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','marketing/contact')) {  ?>
            <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','module/smartnotifications')) {  ?>
            <li><a href="<?php echo $smartnotifications; ?>"><?php echo $entry_smartnotifications; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','catalog/product_ranking')) {  ?>
              <li><a href="<?php echo $product_ranking; ?>"><?php echo $text_product_ranking  ; ?></a></li>
            <?php } ?>

        </ul>
    </li>
    <?php if (isset($text_ne)) { ?>
        <li id="ne"><a class="parent"><i class="fa fa-envelope fa-fw"></i> <span><?php echo $text_ne; ?></span></a>
            <ul>
                <?php if($userac->hasPermission('access','ne/status')) {  ?>
                <li><a href="<?php echo $ne_status; ?>"><?php echo $text_ne_status; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','ne/newsletter')) {  ?>
                <li><a href="<?php echo $ne_email; ?>"><?php echo $text_ne_email; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','ne/draft')) {  ?>
                <li><a href="<?php echo $ne_draft; ?>"><?php echo $text_ne_draft; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','ne/marketing')) {  ?>
                <li><a href="<?php echo $ne_marketing; ?>"><?php echo $text_ne_marketing; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','ne/subscribers')) {  ?>
                <li><a href="<?php echo $ne_subscribers; ?>"><?php echo $text_ne_subscribers; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','ne/stats')) {  ?>
                <li><a href="<?php echo $ne_stats; ?>"><?php echo $text_ne_stats; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','ne/schedule')) {  ?>
                <li><a href="<?php echo $ne_robot; ?>"><?php echo $text_ne_robot; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','ne/template')) {  ?>
                <li><a href="<?php echo $ne_template; ?>"><?php echo $text_ne_template; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','ne/subscribe_box')) {  ?>
                <li><a href="<?php echo $ne_subscribe_box; ?>"><?php echo $text_ne_subscribe_box; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','ne/blacklist')) {  ?>
                <li><a href="<?php echo $ne_blacklist; ?>"><?php echo $text_ne_blacklist; ?></a></li>
                <?php } ?>
                <?php if($userac->hasPermission('access','ne/check_update')) {  ?>
                <li><a href="<?php echo $ne_update_check; ?>"><?php echo $text_ne_update_check; ?></a></li>
                <?php } ?>
                <li id="ne_support"><a class="parent"><?php echo $text_ne_support; ?></a>
                    <ul>
                        <li><a href="https://www.codersroom.com/support/register.php" target="_blank"><?php echo $text_ne_support_register; ?></a></li>
                        <li><a href="https://www.codersroom.com/support/clientarea.php" target="_blank"><?php echo $text_ne_support_login; ?></a></li>
                        <li><a href="https://www.codersroom.com/support/" target="_blank"><?php echo $text_ne_support_dashboard; ?></a></li>
                    </ul>
                </li>
            </ul>
        </li>
    <?php } ?>
    <li id="system"><a class="parent"><i class="fa fa-cog fa-fw"></i> <span><?php echo $text_system; ?></span></a>
        <ul>
            <?php if($userac->hasPermission('access','setting/store')) {  ?>
            <li><a href="<?php echo $setting; ?>"><?php echo $text_setting; ?></a></li>
            <?php } ?>
            <li id="user"><a class="parent"><?php echo $text_users; ?></a>
                <ul>
                    <?php if($userac->hasPermission('access','user/user')) {  ?>
                    <li><a href="<?php echo $user; ?>"><?php echo $text_user; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','user/user_permission')) {  ?>
                    <li><a href="<?php echo $user_group; ?>"><?php echo $text_user_group; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','user/api')) {  ?>
                    <li><a href="<?php echo $api; ?>"><?php echo $text_api; ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <li id="localisation"><a class="parent"><?php echo $text_localisation; ?></a>
                <ul>
                    <?php if($userac->hasPermission('access','localisation/location')) {  ?>
                    <li><a href="<?php echo $location; ?>"><?php echo $text_location; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','localisation/language')) {  ?>
                    <li><a href="<?php echo $language; ?>"><?php echo $text_language; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','localisation/currency')) {  ?>
                    <li><a href="<?php echo $currency; ?>"><?php echo $text_currency; ?></a></li>
                    <?php } ?>
                    <?php  if($userac->hasPermission('access','localisation/stock_status')) {  ?>
                    <li><a href="<?php echo $stock_status; ?>"><?php echo $text_stock_status; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','localisation/order_status')) {  ?>
                    <li><a href="<?php echo $order_status; ?>"><?php echo $text_order_status; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','localisation/rma_status')) {  ?>
                    <li><a href="<?php echo $rma_status; ?>"><?php echo $text_rma_status; ?></a></li>
                    <?php } ?>
                    <li id="returns"><a class="parent"><?php echo $text_return; ?></a>
                        <ul>
                            <?php if($userac->hasPermission('access','localisation/return_status')) {  ?>
                            <li><a href="<?php echo $return_status; ?>"><?php echo $text_return_status; ?></a></li>
                            <?php } ?>
                            <?php if($userac->hasPermission('access','localisation/return_action')) {  ?>
                            <li><a href="<?php echo $return_action; ?>"><?php echo $text_return_action; ?></a></li>
                            <?php } ?>
                            <?php if($userac->hasPermission('access','localisation/return_reason')) {  ?>
                            <li><a href="<?php echo $return_reason; ?>"><?php echo $text_return_reason; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php if($userac->hasPermission('access','localisation/country')) {  ?>
                    <li><a href="<?php echo $country; ?>"><?php echo $text_country; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','localisation/zone')) {  ?>
                    <li><a href="<?php echo $zone; ?>"><?php echo $text_zone; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','localisation/geo_zone')) {  ?>
                    <li><a href="<?php echo $geo_zone; ?>"><?php echo $text_geo_zone; ?></a></li>
                    <?php } ?>
                    <li id="tax"><a class="parent"><?php echo $text_tax; ?></a>
                        <ul>
                            <?php if($userac->hasPermission('access','localisation/tax_class')) {  ?>
                            <li><a href="<?php echo $tax_class; ?>"><?php echo $text_tax_class; ?></a></li>
                            <?php } ?>
                            <?php if($userac->hasPermission('access','localisation/tax_rate')) {  ?>
                            <li><a href="<?php echo $tax_rate; ?>"><?php echo $text_tax_rate; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php if($userac->hasPermission('access','localisation/length_class')) {  ?>
                    <li><a href="<?php echo $length_class; ?>"><?php echo $text_length_class; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','localisation/weight_class')) {  ?>
                    <li><a href="<?php echo $weight_class; ?>"><?php echo $text_weight_class; ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <li id="tools"><a class="parent"><?php echo $text_tools; ?></a>
                <ul>
                    <?php if($userac->hasPermission('access','tool/upload')) {  ?>
                    <li><a href="<?php echo $upload; ?>"><?php echo $text_upload; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','tool/backup')) {  ?>
                    <li><a href="<?php echo $backup; ?>"><?php echo $text_backup; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','tool/error_log')) {  ?>
                    <li><a href="<?php echo $error_log; ?>"><?php echo $text_error_log; ?></a></li>
                    <?php } ?>
                </ul>
            </li>
        </ul>
    </li>
    <?php if ($ticketSystem) { ?>
        <li id="tickets"><a class="parent"><label style="font-size: 20px;font-weight: normal;">H</label> <span><?php echo $text_ticket_system; ?></span></a>
            <ul>
                <?php foreach ($ticketSystem as $key => $link) { ?>
                    <?php if (is_array($link) AND isset($link['realArray'])) { ?>
                        <li><a class="parent"><?php
                                echo $link['realArray'];
                                unset($link['realArray']);
                                ?></a>
                            <ul>
                                <?php foreach ($link as $key2 => $link2) { ?>
                                    <li><a href="<?php echo $link2['href']; ?>"><?php echo $link2['text']; ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } else { ?>
                        <li><a href="<?php echo $link['href']; ?>"><?php echo $link['text']; ?></a></li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </li>
    <?php } ?>
    <?php if (isset($text_abandonedCarts)) { ?>
        <?php if($userac->hasPermission('access','module/abandonedcarts')) {  ?>
        <li id="abandoned_carts"><a href="<?php echo $link_abandonedCarts; ?>"><i class="fa fa-shopping-cart fa-fw"></i> <span><?php echo $text_abandonedCarts; ?></span></a></li>
        <?php } ?>
    <?php } ?>

    <?php if (isset($text_abandonmentCarts)) { ?>
        <?php if($userac->hasPermission('access','module/abandonmentcarts')) {  ?>
        <li id="abandonment_carts"><a href="<?php echo $link_abandonmentCarts; ?>"><i class="fa fa-shopping-cart fa-fw"></i> <span><?php echo $text_abandonmentCarts; ?></span></a></li>
        <?php } ?>
    <?php } ?>
    <?php if (isset($text_barcode)) { ?>
        <?php if($userac->hasPermission('access','sale/shipping_process')) {  ?>
        <li id="barcode"><a href="<?php echo $link_barcode; ?>"><i class="glyphicon glyphicon-barcode"></i> <span><?php echo $text_barcode; ?></span></a></li>
        <?php } ?>
    <?php } ?>
    <li id="reports"><a class="parent"><i class="fa fa-bar-chart-o fa-fw"></i> <span><?php echo $text_reports; ?></span></a>
        <ul>
            <li id="report-sale"><a class="parent"><?php echo $text_sale; ?></a>
                <ul>
                    <?php if($userac->hasPermission('access','report/sale_order')) {  ?>
                    <li><a href="<?php echo $report_sale_order; ?>"><?php echo $text_report_sale_order; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/sale_tax')) {  ?>
                    <li><a href="<?php echo $report_sale_tax; ?>"><?php echo $text_report_sale_tax; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/sale_shipping')) {  ?>
                    <li><a href="<?php echo $report_sale_shipping; ?>"><?php echo $text_report_sale_shipping; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/sale_return')) {  ?>
                    <li><a href="<?php echo $report_sale_return; ?>"><?php echo $text_report_sale_return; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/sale_coupon')) {  ?>
                    <li><a href="<?php echo $report_sale_coupon; ?>"><?php echo $text_report_sale_coupon; ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <li id="report-product"><a class="parent"><?php echo $text_product; ?></a>
                <ul>
                    <?php if($userac->hasPermission('access','report/product_viewed')) {  ?>
                    <li><a href="<?php echo $report_product_viewed; ?>"><?php echo $text_report_product_viewed; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/product_purchased')) {  ?>
                    <li><a href="<?php echo $report_product_purchased; ?>"><?php echo $text_report_product_purchased; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/price_category')) {  ?>
                    <li><a href="<?php echo $report_price_category; ?>"><?php echo $text_report_price_category; ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <li id="report-customer"><a class="parent"><?php echo $text_customer; ?></a>
                <ul>
                    <?php if($userac->hasPermission('access','report/customer_online')) {  ?>
                    <li><a href="<?php echo $report_customer_online; ?>"><?php echo $text_report_customer_online; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/customer_activity')) {  ?>
                    <li><a href="<?php echo $report_customer_activity; ?>"><?php echo $text_report_customer_activity; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/customer_order')) {  ?>
                    <li><a href="<?php echo $report_customer_order; ?>"><?php echo $text_report_customer_order; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/customer_reward')) {  ?>
                    <li><a href="<?php echo $report_customer_reward; ?>"><?php echo $text_report_customer_reward; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/customer_credit')) {  ?>
                    <li><a href="<?php echo $report_customer_credit; ?>"><?php echo $text_report_customer_credit; ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <li id="report-aff"><a class="parent"><?php echo $text_marketing; ?></a>
                <ul>
                    <?php if($userac->hasPermission('access','report/marketing')) {  ?>
                    <li><a href="<?php echo $report_marketing; ?>"><?php echo $text_marketing; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/affiliate')) {  ?>
                    <li><a href="<?php echo $report_affiliate; ?>"><?php echo $text_report_affiliate; ?></a></li>
                    <?php } ?>
                    <?php if($userac->hasPermission('access','report/affiliate_activity')) {  ?>
                    <li><a href="<?php echo $report_affiliate_activity; ?>"><?php echo $text_report_affiliate_activity; ?></a></li>
                    <?php } ?>
                </ul>
            </li>
        </ul>

    </li>

    <?php if($userac->hasPermission('access','operations/operations')) {  ?>
    <li id="abandoned_carts"><a href="<?php echo $operations; ?>"><i class="fa fa-tasks"></i> <span><?php echo $text_operations; ?></span></a></li>
    <?php } ?>
    <li id="dataimport"><a class="parent"><i class="fa fa-database"></i> <span><?php echo $text_dataimport; ?></span></a>
        <ul>
            <?php if($userac->hasPermission('access','dataimport/category')) {  ?>
            <li><a href="<?php echo $dataimport_category; ?>"><?php echo $text_dataimport_category; ?></a></li>
            <?php } ?>
            <?php if($userac->hasPermission('access','dataimport/product')) {  ?>
            <li><a href="<?php echo $dataimport_product; ?>"><?php echo $text_dataimport_product; ?></a></li>
            <?php } ?>
            <?php /* <li><a class="parent"><?php echo $text_users; ?></a>
              <ul>
              <li><a href="<?php echo $user; ?>"><?php echo $text_user; ?></a></li>
              <li><a href="<?php echo $user_group; ?>"><?php echo $text_user_group; ?></a></li>
              <li><a href="<?php echo $api; ?>"><?php echo $text_api; ?></a></li>
              </ul>
              </li> */ ?>
        </ul>
    </li>
</ul>

<script>
    $(document).ready(
        $($('ul > li').get().reverse()).each(function(i,val) {
            var id = val.id;
            if (id !== 'undefined' && id !== '') {
                var count = $('li#' + id + ' ul').children('li').length;
                var hasParentClass = $('li#' + id + ' a').hasClass('parent');
                if (id === 'ne' && $('li#ne ul li').attr('id') === 'ne_support'){
                    console.log(count );
                    count = 0;
                }
                if (count <= 0 && hasParentClass) {
                    $('#' + id).remove();
                }
            }
        })
    )
</script>
