<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
               
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="(checkForDelete()) ? (confirm('<?php echo $text_confirm; ?>') ? $('#form-mall').submit() : false) : false;">
                    <i class="fa fa-trash-o"></i>
                </button>
               
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_email; ?></label>
                                <input type="text" name="newsletter_email" value="<?php echo $filter_email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-newsletter_email" class="form-control" />
                            </div>
                        </div>
                        <!--<div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <input type="text" name="newsletter_status" value="<?php echo $filter_status; ?>" placeholder="<?php echo $entry_status; ?>" id="input-newsletter_status" class="form-control" />
                            </div>
                        </div>-->
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-gender"><?php echo $entry_gender; ?></label>
                                <select name="newsletter_gender" id="input-newsletter_gender" class="form-control">
                                        <option value=""><?php echo $select_gender; ?></option>
                                        <?php if ($newsletter_gender) { ?>
                                            <option value="1" selected="selected"><?php echo 'Male'; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo 'Male'; ?></option>
                                        <?php } ?>
                                        <?php if (!$newsletter_gender && !is_null($newsletter_gender)) { ?>
                                            <option value="0" selected="selected"><?php echo 'Female'; ?></option>
                                        <?php } else { ?>
                                            <option value="0"><?php echo 'Female'; ?></option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-country"><?php echo $entry_country_id; ?></label>
                                <select name="filter_country_id" id="input-country_id" class="form-control">
                                    <option value=""><?php echo $select_country; ?></option>
                                    <?php foreach($countries as $row){ ?>
                                    <?php if ($filter_country_id == $row['country_id']) { ?>
                                    <option value="<?php echo $row['country_id'] ?>" selected="selected"><?php echo $row['name'] ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $row['country_id'] ?>"><?php echo $row['name'] ?></option>
                                    <?php } ?>
                                   <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                
                            </div>
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                        </div>
                    </div>
                </div>

                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-mall">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center">
                                        <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" />
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'n.id') { ?>
                                            <a href="<?php echo $sort_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_id; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_id; ?>"><?php echo $column_id; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'n.email') { ?>
                                            <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'n.status') { ?>
                                            <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $entry_status; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_status; ?>"><?php echo $entry_status; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'n.gender') { ?>
                                            <a href="<?php echo $sort_gender; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_gender; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_gender; ?>"><?php echo $column_gender; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'n.created_date') { ?>
                                            <a href="<?php echo $sort_created_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $entry_created_date; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_created_date; ?>"><?php echo $entry_created_date; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'n.updated_date') { ?>
                                            <a href="<?php echo $sort_updated_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $entry_updated_date; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_updated_date; ?>"><?php echo $entry_updated_date; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'n.language_id') { ?>
                                            <a href="<?php echo $sort_language_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $entry_language_id; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_language_id; ?>"><?php echo $entry_language_id; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td class="text-left">
                                        <?php if ($sort == 'n.country_id') { ?>
                                            <a href="<?php echo $sort_country_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $entry_country_id; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_country_id; ?>"><?php echo $entry_country_id; ?></a>
                                        <?php } ?>
                                    </td>
                                    
                                    
                                   <!-- <td class="text-left">
                                        <?php //echo $column_action; ?>
                                    </td> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($newsletter) { ?>
                                    <?php foreach ($newsletter as $row) { ?>
                                        <tr>
                                            <td class="text-center">
                                                <?php if(in_array($row['id'], $selected)){ ?>
                                                <input type="checkbox" name="selected[]" value="<?php echo $row['id']; ?>" checked="checked" />
                                                <?php } else { ?>
                                                <input type="checkbox" name="selected[]" value="<?php echo $row['id']; ?>" />
                                                <?php } ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $row['id']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $row['email']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $row['status']; ?>
                                            </td>
                                            <td class="text-left">
                                    <?php  if( $row['gender']==1){ echo 'Male'; } else {echo "Female" ;} ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $row['created_date']; ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo $row['updated_date']; ?>
                                            </td>
                                            <td class="text-left">
                                    <?php  if( $row['language_id']==2){ echo 'ARABIC'; } else {echo "ENGLISH" ;} ?>
                                            </td>
                                            <td class="text-left">
                                    <?php  if( $row['country_id']==184){ echo 'KSA'; } else {echo "AE" ;} ?>
                                            </td>
                                           <!-- <td class="text-left">
                                                <a href="<?php echo $row['delete']; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-primary">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td> -->
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
    
    function checkForDelete() {
        return ($('input[name="selected[]"]:checked').length > 0);
    }
    
</script>
<script type="text/javascript"><!--
$('#button-filter').on('click', function () {
        var url = 'index.php?route=newsletter/newsletter&token=<?php echo $token; ?>';

        var filter_email = $('input[name=\'newsletter_email\']').val();
        // alert(filter_email);
        if (filter_email) {
            url += '&filter_email=' + encodeURIComponent(filter_email);
        }
        
         var filter_status = $('input[name=\'newsletter_status\']').val();
        // alert(filter_status);
        if (filter_status) {
            url += '&filter_status=' + encodeURIComponent(filter_status);
        }
         var filter_gender = $('select[name=\'newsletter_gender\']').val();
        // alert(filter_gender);
        if (filter_gender) {
            url += '&filter_gender=' + encodeURIComponent(filter_gender);
        }
        

        var filter_country_id = $('select[name=\'filter_country_id\']').val();
        // alert(filter_country_id);
        if (filter_country_id != '') {
            url += '&filter_country_id=' + encodeURIComponent(filter_country_id);
        }
        // alert(url);
        location = url;
    });
//--></script>