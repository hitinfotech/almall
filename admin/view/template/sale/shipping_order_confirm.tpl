<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
        <script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
        <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
        <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
        <script type="text/javascript" src="https://cdn.jsdelivr.net/jsbarcode/3.3.7/JsBarcode.all.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/core.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/md5.min.js"></script>


    </head>
    <style>
        .button {
            background-color: #d91a5c;
            display: inline-block;
            color: white;
            width: 20%;
            margin: 0 auto;
            padding: 15px 32px;
            text-align: center;
            font-size: 16px;
        }
        .content-padding {
            padding-bottom: 110px;
            padding-top: 20px;
        }
        .top-buffer {
            margin-top:20px;
        }
    </style>
    <body>
        <div class="container">
            <div class="content-padding">

                <?php foreach ($orders as $order) { ?>
                    <?php $order_countries = count($order['product']); ?>
                    <?php $order_id = $order['order_id'] ?>
                    <div style="page-break-after: always;">
                        <div class="row">
                            <div class="col-md-9 col-sm-9 col-xs-9">

                                <p><br/><br/>
                                <table>
                                    <tr>
                                        <td  style="font-size:20px;"><?php echo 'Seller Order Invoice Of Order No.  '; ?></td>
                                        <td><svg id="code39"></svg></td>
                                    </tr>
                                </table>
                                <span class="barcode">
                                    <?php $barcode = $order['payment_iso_code_2'] . '-' . $order['order_id'] . '-' . $order['count_order']; ?>
                                </span>
                                </p>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <a class='btn btn-info pull-right' href='<?php echo $back_button ?>'><i class='fa fa-reply'>Back </i></a>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <img src="<?php echo $logo ?>" alt="logo" />
                        </div>
                        <div class="row top-buffer">
                            <div class='col-md-6 col-sm-6 col-xs-6'>
                                <p style="font-size:20px;"><?php echo $text_email; ?>: <?php echo $order['store_email']; ?></p>
                                <p style="font-size:20px;"><?php echo $text_telephone; ?>: <?php echo $order['store_telephone']; ?></p>

                            </div>
                            <div class='col-md-6 col-sm-6 col-xs-6'>
                                <p style="font-size:20px;"><?php echo $text_date_added; ?>: <?php echo $order['date_added']; ?></p>
                                <p style="font-size:20px;"><?php echo 'Order ID'; ?>: <?php echo $order['order_id']; ?></p>
                            </div>


                        </div>

                        <div>
                            <?php foreach ($order['product'] as $country => $products) { ?>
                                <p style="font-size:20px;"><?php echo ($country == 221) ? "UAE " : "KSA " ?><?php echo $text_order_detail ?></p>
                                <table class="table table-bordered" id='order_details_<?php echo $country ?>'>
                                    <thead>
                                        <tr>
                                            <td><b><?php echo $column_product; ?></b></td>

                                            <td><b><?php echo 'SKU'; ?></b></td>
                                            <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
                                            <td class="text-right"><b><?php echo $column_price; ?></b></td>
                                            <td class="text-right"><b><?php echo $column_total; ?></b></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count=0;
                                        foreach ($products as $product) { ?>
                                            <tr>
                                                <td><?php echo $product['name']; ?>
                                                    <?php foreach ($product['option'] as $option) { ?>
                                                        <br />
                                                        &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                                    <?php } ?></td>
                                                <td class="text-right"><?php echo $product['sku']; ?></td>
                                                <td class="text-right"><?php echo $product['quantity']; ?></td>
                                                <td class="text-right"><?php echo $product['price']; ?></td>
                                                <td class="text-right"><?php echo $product['total']; ?></td>
                                        <input type='hidden' id="prod_id" data-id="<?php echo $product['product_id'] ?>" />
                                        </tr>
                                    <?php $count++; } ?>
                                    <?php foreach ($order['voucher'] as $voucher) { ?>
                                        <tr>
                                            <td><?php echo $voucher['description']; ?></td>
                                            <td></td>
                                            <td class="text-right">1</td>
                                            <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                            <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                <?php if (!in_array($country, $closed_countries)) { ?>
                                    <div class='row' id='confirm_products_<?php echo $country ?>'>
                                        <div class='col-md-5'>
                                            <label class='control-label' for='product_number_<?php echo $country ?>'>Product SKU</label>
                                            <div class='row'>
                                                <input class="form-control" id='product_number_<?php echo $country ?>' />
                                            </div>
                                        </div>
                                        <div class='col-md-2'>
                                            <button class='btn btn-info' onclick="move_number(<?php echo $country ?>)">Add >></button>
                                        </div>
                                        <div class='col-md-5' >
                                            <div class='well' style='padding: 10px; overflow:hidden ;' >
                                                <h3 class="panel-title"><i class="fa fa-list"></i> Added Products: </h3>
                                                <br>
                                                <div id='product_list_<?php echo $country ?>'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class='row' >
                                        <p style='color:red; font-size: 20px; text-align:center'>
                                            This Order is Closed

                                        </p>
                                    </div>

                                <?php } ?>
                            </div>
                        <?php } ?>


                    <?php } ?>

                </div>

                <div class="modal fade in" id="invoice_popup" tabindex="-1" role="dialog" aria-labelledby="size" style="display: none; padding-right: 15px;">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="size-chart">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <iframe id='invoice' src="" width="100%" frameborder="0" scrolling="no">
                                    <p>Your browser does not support iframes.</p>
                                    </iframe>
                                </div>
                            </div>
                            <button class='button' id='print_button' > Print </button>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </body>
</html>

<script>
    JsBarcode("#code39", "<?php echo $barcode ?>", {format: "code39", width: 1.7, height: 70});
    var closed_countries = <?php echo count($closed_countries) ?>;
    var order_countries = <?php echo $order_countries ?>;
    var product_skus_ksa = new Array();
    var product_skus_uae = new Array();
    $("#invoice").hide();

    function move_number(country_id) {
        var prod_number = $("#product_number_" + country_id).val().trim();
        if (prod_number == '') {
            alert("Please Insert a value")
        } else {
            if (country_id == 221) {
                product_skus_uae.push(prod_number +"*");
            } else {
                product_skus_ksa.push(prod_number +"*");
            }
            $("#product_number_" + country_id).val('');
            $("#product_list_" + country_id).append('<span class="prod_sku" id=' + 'd' + CryptoJS.MD5($.trim(prod_number)) + '_' + country_id + ' data-sku = "'+prod_number+'"><h5>' + prod_number + ' <i class="fa fa-times" aria-hidden="true" onclick="remove_sku(\'' +  prod_number + '\',' + country_id + ')"></i></h5></span>');

            if (!$("#product_list_" + country_id).parent().find('button').length) {
                $("#product_list_" + country_id).parent().append("<button class='btn btn-info pull-right' onclick='check_products(" + country_id + ")'>Confirm</button>");
            }
        }

    }

    function check_products(country_id) {

        $("#order_details_" + country_id + " tbody tr ").each(function () {
            $(this).removeClass("alert-danger");
        });
        $("#product_list_" + country_id + " span ").each(function () {
            $(this).removeClass("alert-danger");
            $(this).removeClass("alert-success");
            $(this).find('h5 span ').remove();
            $(this).find('h5 i').addClass('fa-times');
            $(this).find('h5 i').removeClass('fa-check');
            $(this).find('h5 i').attr('onclick','remove_sku(\'' + $(this).attr("data-sku") + '\',' + country_id + ')');
        });
        var product_skus = new Array();
        if (country_id == 221) {
            product_skus = product_skus_uae;
        } else {
            product_skus = product_skus_ksa;
        }

        $.ajax({
            url: "index.php?route=sale/shipping_process/check_order_products&country_id=" + country_id + "&token=<?php echo $token; ?>",
            data: '&order_id=<?php echo $order_id ?>&product_skus=' + encodeURIComponent(product_skus),
            type: 'get',
            dataType: 'html',
            success: function (data) {
                result = JSON.parse(data);
                if (Array.isArray(result)) {
                    result.forEach(function (errors) {
                        if (errors['Error'] == "Missing products") {
                            errors['ids'].forEach(function (element) {
                              $( "input[data-id='"+element+"']" ).parent().addClass("alert-danger");
                              console.log($( "input[data-id='"+element+"']" ).parent())

                            });
                        }

                        if (errors['Error2'] == 'Extra products') {
                            errors['skus'].forEach(function (element) {
                              console.log($.trim(element) + '_' + country_id);
                                $("#" + 'd' + CryptoJS.MD5($.trim(element)) + '_' + country_id).addClass("alert-danger");
                                $("#" + 'd' + CryptoJS.MD5($.trim(element)) + '_' + country_id).find('h5 span').remove();
                                $("#" + 'd' + CryptoJS.MD5($.trim(element)) + '_' + country_id).find('h5 i').before('<span> (This product is not in the Order items) </span>');
                            });
                        }

                        if (errors['Error3'] == 'quantity mismatch') {
                            errors['skus'].forEach(function (element) {
                                $("#" + 'd' + CryptoJS.MD5($.trim(element)) + '_' + country_id).addClass("alert-danger");
                                $("#" + 'd' + CryptoJS.MD5($.trim(element)) + '_' + country_id).find('h5 span').remove();
                                $("#" +'d' + CryptoJS.MD5($.trim(element)) + '_' + country_id).find('h5 i').before('<span> ('+errors['msg']+') </span>');
                            });
                        }
                    });
                } else {
                  $('#product_list_' + country_id).parent().find('button').attr("disabled",'true');
                    $('#product_list_' + country_id).parent().find('button').before("<button class='btn btn-info pull-right' onclick='close_shipment(" + country_id + ',' + <?php echo $order_id ?> + ")'>Close Shipment</button><button class='btn btn-info pull-right' onclick='print_invioce(" + country_id + ',' + <?php echo $order_id ?> + ")'>Print Invioce</button>")
                    $('#product_number_' + country_id).attr('readonly', 'readonly');
                }
                $(".prod_sku").each(function (ele) {
                    if (!$(this).hasClass('alert-danger')) {
                        $(this).addClass('alert-success');
                        $(this).find('h5 i').removeClass('fa-times');
                        $(this).find('h5 i').removeAttr('onclick');
                        $(this).find('h5 i').addClass('fa-check');
                    }
                });
            }
        });
    }

    function remove_sku(prod_sku, country_id) {
        $("#" +'d' + CryptoJS.MD5($.trim(prod_sku)) + '_' + country_id).remove();
        if (country_id == 221) {
            for (var i in product_skus_uae) {
                if ($.trim(product_skus_uae[i]) == $.trim(prod_sku+"*")) {
                    product_skus_uae.splice(i, 1);
                    break;
                }
            }
        } else {
            for (var i in product_skus_ksa) {
                if ($.trim(product_skus_ksa[i]) == $.trim(prod_sku+"*")) {
                    product_skus_ksa.splice(i, 1);
                    break;
                }
            }
        }

    }

    function print_invioce(country_id, order_id) {
        $('#invoice').attr('src', '<?php echo $invoice_url ?>' + '&order_id=' + order_id + '&country_id=' + country_id + "&token=<?php echo $token; ?>")
        $("#invoice").show();
        $('#invoice_popup').modal('show')
    }

    $('iframe').load(function () {
        $(this).height($(this).contents().outerHeight());
    });

    $('#print_button').on('click', function () {
        $("#invoice").get(0).contentWindow.print();
        return false;
    });

    function close_shipment(country_id, order_id) {
        var close_order = confirm("Are you sure you want to Colse this Order");
        if (close_order == true) {
            $.ajax({
                url: "index.php?route=sale/shipping_process/close_order_country&country_id=" + country_id + "&shipping_company=" + <?php echo $shipping_company ?> + "&token=<?php echo $token; ?>",
                data: '&order_id=<?php echo $order_id ?>',
                type: 'get',
                dataType: 'html',
                success: function (data) {
                  var json = JSON.parse(data);
                  change_order_status(country_id,json['awbno']);
                }
            });
        }
    }

    function change_order_status(country_id,awbno) {
        var product_ids = [];
        $('#order_details_' + country_id + ' tbody tr input#prod_id').each(function () {
            product_ids.push(encodeURIComponent($(this).attr("data-id").trim()))
        });

        $.ajax({
            url: 'index.php?route=customerpartner/order/history&order_id=<?php echo $order_id ?>&token=<?php echo $token; ?>&comment='+awbno,
            type: 'post',
            dataType: 'json',
            data: 'order_status_id=3&product_ids=' + encodeURIComponent(product_ids),
            success: function (json) {

            }
        });
        var back_link = '<?php echo $back_button ?>';
        var find = 'amp;';
        var re = new RegExp(find, 'g');

        back_link = back_link.replace(re, '');
        window.location.href  = back_link;


    }


</script>
