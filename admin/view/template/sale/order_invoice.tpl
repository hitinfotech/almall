<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>
<style>
.content-padding {
  padding-bottom: 110px;
  padding-top: 20px;
}
.top-buffer {
  margin-top:20px;
}
</style>
<body>
<div class="container">
  <div class="content-padding">
  <?php foreach ($orders as $order) { ?>
  <div style="page-break-after: always;">
    <div class="row">
    <div class="col-md-9 col-sm-9 col-xs-9">
      <p style="font-size:20px;"><?php echo $text_invoice_title ?></p>
    </div>
    <div>
      <p style="font-size:20px;"><?php echo $text_invoice; ?> #<?php echo $order['order_id']; ?></p>
    </div>
  </div>
    <div class="row top-buffer">
      <img src="<?php echo $logo  ?>" alt="logo" />
    </div>
    <div class="row top-buffer">
      <div class='col-md-6 col-sm-6 col-xs-6'>
        <p style="font-size:20px;"><?php echo $text_email; ?>: <?php echo $order['store_email']; ?></p>
      </div>
      <div class='col-md-6 col-sm-6 col-xs-6'>
          <p style="font-size:20px;"><?php echo $text_date_added; ?>: <?php echo $order['date_added']; ?></p>
      </div>
    </div>
    <div class="row" >
      <div class='col-md-9 col-sm-9 col-xs-9'>
        <p style="font-size:20px;"><?php echo $text_telephone; ?>: <?php echo $order['store_telephone']; ?></p>
      </div>
    </div>
      <div>
        <table class="table table-bordered top-buffer">
          <thead>
            <tr>
              <td style="width: 50%;"><b><?php echo $text_payment_address; ?></b></td>
              <td style="width: 50%;"><b><?php echo $text_shipping_address; ?></b></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><address>
                <?php echo $order['payment_address']; ?>
                </address></td>
              <td><address>
                <?php echo $order['shipping_address']; ?>
                </address></td>
            </tr>
          </tbody>
        </table>


        <p style="font-size:20px;"><?php echo $text_order_detail ?></p>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $column_sku; ?></b></td>
          <td><b><?php echo $column_product; ?></b></td>
          <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
          <td class="text-right"><b><?php echo $column_price; ?></b></td>
          <td class="text-right"><b><?php echo $column_total; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr>
          <td><?php echo $product['sku']; ?></td>
          <td><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          <td class="text-right"><?php echo $product['quantity']; ?></td>
          <td class="text-right"><?php echo $product['price']; ?></td>
          <td class="text-right"><?php echo $product['total']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['voucher'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>

      </tbody>
    </table>
  <?php if(isset($order['total']) && !empty($order['total'])){?>
    <table class="table table-bordered" style="width: 25%; float: right">
      <tbody>
        <?php foreach ($order['total'] as $total) { ?>
        <tr>
          <td class="text-left"><?php echo $total['title']; ?></td>
          <td class="text-left"><?php echo $total['text']; ?></td>
        </tr>
        <?php } ?>
    </tbody>
  </table
  <?php } ?>

    <?php if ($order['comment']) { ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $text_comment; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $order['comment']; ?></td>
        </tr>
      </tbody>
    </table>
    <?php } ?>
  </div>
  <?php } ?>

<div class='col-md-12 col-sm-12 col-xs-12'>
  <br/><br/>
  <p style="font-size:15px;"><?php echo $text_more_details ?></p>
</div>

</div>
</div>
</body>
</html>
