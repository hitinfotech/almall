<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
        <script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
        <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
        <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />

    </head>
    <style>
      .button {
          background-color: #d91a5c;
          display: inline-block;
          color: white;
          width: 20%;
          margin: 0 auto;
          padding: 15px 32px;
          text-align: center;
          font-size: 16px;
      }
        .content-padding {
            padding-bottom: 110px;
            padding-top: 20px;
        }
        .top-buffer {
            margin-top:20px;
        }
        @font-face {
            font-family: 'Code39r';
            src: url('<?php echo $base; ?>view/stylesheet/free3of9/Code39r.ttf'); /* IE6+ */
            src: local('Code39r'),
                local('Code39r'),
                url('/admin/view/stylesheet/free3of9/Code39r.ttf') format('ttf'), /* FF3.6 */
                url('/admin/view/stylesheet/free3of9/Code39r.ttf') format('truetype') /* Saf3+,Chrome,Opera10+ */
                ;
        }

        .barcode { font-family: Code39r, Arial, sans-serif; font-size: 80px; }
    </style>
    <body>
        <div class="container">
            <div class="content-padding">
                <?php foreach ($orders as $order) { ?>
                  <?php $order_countries = count($order['product']);?>
                  <?php $order_id = $order['order_id'] ?>
                    <div style="page-break-after: always;">
                        <div class="row">
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <p style="font-size:20px;">
                                    <?php echo 'Seller Order Invoice Of Order No.  '; ?>  *<?php echo 'RMA-'. $order['payment_iso_code_2'] . '-' . $order['order_id'] . '-' . $order['count_order']; ?>*
                                     &nbsp; &nbsp;<span class="barcode"><?php echo 'RMA-'. $order['payment_iso_code_2'] . '-' . $order['order_id'] . '-' . $order['count_order']; ?></span>
                                </p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                              <a class='btn btn-info pull-right' href='<?php echo $back_button ?>'><i class='fa fa-reply'>Back </i></a>
                              <button class='btn btn-info pull-right' onclick='close_shipment(<?php echo $rma_id   ?>)'>Close Shipment</button>
                              <button class='btn btn-info pull-right' onclick='print_invioce(<?php echo $rma_id ?>)'>Print Invioce</button>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <img src="<?php echo $logo ?>" alt="logo" />
                        </div>
                        <div class="row top-buffer">
                            <div class='col-md-6 col-sm-6 col-xs-6'>
                                <p style="font-size:20px;"><?php echo $text_email; ?>: <?php echo $order['store_email']; ?></p>
                                <p style="font-size:20px;"><?php echo $text_telephone; ?>: <?php echo $order['store_telephone']; ?></p>

                            </div>
                            <div class='col-md-6 col-sm-6 col-xs-6'>
                                <p style="font-size:20px;"><?php echo $text_date_added; ?>: <?php echo $order['date_added']; ?></p>
                                <p style="font-size:20px;"><?php echo 'Order ID'; ?>: <?php echo $order['order_id']; ?></p>
                            </div>


                        </div>

                        <div>
                            <table class="table table-bordered top-buffer">
                                <thead>
                                    <tr>
                                        <td style="width: 50%;"><b><?php echo "FROM"; ?></b></td>
                                        <td style="width: 50%;"><b><?php echo "RECIEVER"; ?></b></td>
                                      </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><address>
                                                <?php echo $order['shipping_address']; ?>
                                            </address></td>
                                        <td><address>
                                                <?php echo $order['store_address']; ?>
                                            </address></td>
                                    </tr>
                                </tbody>
                            </table>

                            <?php foreach($order['product'] as  $product) { ?>
                            <p style="font-size:20px;"><?php echo $text_order_detail ?></p>
                            <table class="table table-bordered" id='order_details'>
                                <thead>
                                    <tr>
                                        <td><b><?php echo $column_product; ?></b></td>

                                        <td><b><?php echo 'SKU'; ?></b></td>
                                        <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
                                        <td class="text-right"><b><?php echo $column_price; ?></b></td>
                                        <td class="text-right"><b><?php echo $column_total; ?></b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td><?php echo $product['name']; ?>
                                                <?php foreach ($product['option'] as $option) { ?>
                                                    <br />
                                                    &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                                <?php } ?></td>
                                            <td><span class="barcode"><?php echo $product['sku']; ?></span></td>

                                            <td class="text-right"><?php echo $product['quantity']; ?></td>
                                            <td class="text-right"><?php echo $product['price']; ?></td>
                                            <td class="text-right"><?php echo $product['total']; ?></td>
                                            <input type='hidden' id="prod_id" data-id="<?php echo $product['product_id'] ?>" />
                                        </tr>
                                    <?php foreach ($order['voucher'] as $voucher) { ?>
                                        <tr>
                                            <td><?php echo $voucher['description']; ?></td>
                                            <td></td>
                                            <td class="text-right">1</td>
                                            <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                            <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                <?php } ?>

                    <div class='col-md-12 col-sm-12 col-xs-12'>
                        <br/><br/>
                        <div class='col-md-6 col-sm-6 col-xs-6'>
                          <p style="font-size:15px;"><?php echo $text_more_details ?></p>
                        </div>
                        <div class='col-md-6 col-sm-6 col-xs-6'>
                          <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/fb.png" width="20"> Sayidatymall</span>
                          <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/tw.png" width="20"> sayidaty_mall</span>
                          <br>
                          <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/ins.png" width="20"> sayidatymall</span>
                          <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/yt.png" width="20"> sayidaty mall</span>
                        </div>
                    </div>

                </div>

                <div class="modal fade in" id="invoice_popup" tabindex="-1" role="dialog" aria-labelledby="size" style="display: none; padding-right: 15px;">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="size-chart">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                		              <iframe id='invoice' src="" width="100%" frameborder="0" scrolling="no">
                                      <p>Your browser does not support iframes.</p>
                                  </iframe>
                                  </div>
                            </div>
                            <button class='button' id='print_button' > Print </button>
                        </div>
                    </div>
                </div>


            </div>
          </div>
    </body>
</html>

<script>
var product_sku = new Array();
$("#invoice").hide();

function print_invioce(rma_id){
  $('#invoice').attr('src','<?php echo $invoice_url ?>'+'&rma_id='+rma_id+"&token=<?php echo $token; ?>")
   $("#invoice").show();
   $('#invoice_popup').modal('show')
}

$('iframe').load(function(){$(this).height($(this).contents().outerHeight());});

$('#print_button').on('click',function (){
  $("#invoice").get(0).contentWindow.print();
  return false;
});

  function close_shipment(order_id){
    var close_order = confirm("Are you sure you want to Close this Returns");
    if(close_order==true){
      $.ajax({
          url: "index.php?route=sale/shipping_process/close_return&token=<?php echo $token; ?>",
          data: '&rma_id=<?php echo $rma_id ?>&order_id=<?php echo $order_id ?>',
          type: 'get',
          dataType: 'html',
          success: function(data) {
          }
      });
    }
  }
</script>
