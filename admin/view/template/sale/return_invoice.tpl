<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>
<style>
.content-padding {
  padding-bottom: 110px;
  padding-top: 20px;
}
.top-buffer {
  margin-top:20px;
}

@font-face {
    font-family: 'code128';
    src: url('<?php echo $base; ?>view/stylesheet/free3of9/code128.ttf'); /* IE6+ */
    src: local('code128'),
        local('code128'),
        url('/admin/view/stylesheet/free3of9/code128.ttf') format('ttf'), /* FF3.6 */
        url('/admin/view/stylesheet/free3of9/code128.ttf') format('truetype') /* Saf3+,Chrome,Opera10+ */
        ;
}

.barcode { font-family: code128, Arial, sans-serif; font-size: 80px; }
</style>
<body>
<div class="container">
  <div class="content-padding">
    <div class='row' >
      <div class='col-md-6 col-sm-6 col-xs-6'>
        <img src="<?php echo $logo  ?>" alt="logo"  width="70%" />
      </div>
    </div>
    <br><br>

  <?php foreach ($orders as $order) { ?>
  <div style="page-break-after: always;">
    <div class="row">
    <div class="col-md-9 col-sm-9 col-xs-9">
      <h1 style="color: red;"><?php echo $text_invoice_title ?></h1>
    </div>
    <div>
      <p style="font-size:20px;"><?php echo date("m/d/Y - H:i") ?></p>
    </div>
      <div>
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <td class="text-left" colspan="2"><?php echo $text_return_details; ?></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-left" style="width: 50%;">
                        <b><?php echo $text_return_no; ?></b> <?php echo $rma_details['return_id']; ?><br />
                        <b><?php echo $text_order_id; ?></b> # <?php echo $rma_details['order_id']; ?><br />
                      </td>
                    <td class="text-left" style="width: 50%;">
                      <b><?php echo $text_return_status; ?></b> <?php echo $rma_details['return_status']; ?><br />
                      <b><?php echo $text_customer; ?></b> # <?php echo $rma_details['customer_name']; ?><br />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered top-buffer">
          <thead>
            <tr>
              <td style="width: 50%;"><b><?php echo 'Sender'; ?></b></td>
              <td style="width: 50%;"><b><?php echo 'Reciever'; ?></b></td>
            </tr>
          </thead>
          <tbody>
              <tr>
                  <td><address>
                          <?php echo $order['shipping_address']; ?>
                      </address></td>
                  <td><address>
                          <?php echo $order['store_address']; ?>
                      </address></td>
              </tr>
          </tbody>
        </table>


        <p style="font-size:20px;"><?php echo $text_order_detail ?></p>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $column_product; ?></b></td>
          <td ><b><?php echo $column_sku; ?></b></td>
          <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
          <td class="text-right"><b><?php echo $column_price; ?></b></td>
          <td class="text-right"><b><?php echo $column_total; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr>
          <td><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          <td ><span class='barcode'><?php echo $product['sku']; ?></span><br><?php echo $product['sku']; ?></td>
          <td class="text-right"><?php echo $product['quantity']; ?></td>
          <td class="text-right"><?php echo $product['price']; ?></td>
          <td class="text-right"><?php echo $product['total']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['voucher'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>

      </tbody>
      <?php if(isset($order['total']) && !empty($order['total'])){?>
        <tfoot>
            <?php foreach ($order['total'] as $total) { ?>
              <tr>
                  <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
                  <td class="text-right"><?php echo $total['text']; ?></td>
              </tr>
            <?php } ?>
        </tfoot>
      <?php } ?>
    </table>


    <?php if ($order['comment']) { ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $text_comment; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $order['comment']; ?></td>
        </tr>
      </tbody>
    </table>
    <?php } ?>
  </div>
  <?php } ?>

<div class='col-md-12 col-sm-12 col-xs-12'>
  <br/><br/>
  <hr  style="border: solid 2px dimgrey ;" />
  <div class='col-md-5 col-sm-5 col-xs-5'>
    <p style="font-size:10px;"><?php echo $text_more_details ?></p>
  </div>
  <div class='col-md-7 col-sm-7 col-xs-7'>
    <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/fb.png" width="20"> Sayidatymall</span>
    <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/tw.png" width="20"> sayidaty_mall</span>
    <br>
    <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/ins.png" width="20"> sayidatymall</span>
    <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/yt.png" width="20"> sayidaty mall</span>
  </div>
</div>

</div>
</div>
</body>
</html>
