<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <head>
        <meta charset="UTF-8" />
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
        <script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
        <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
        <link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
        <script type="text/javascript" src="https://cdn.jsdelivr.net/jsbarcode/3.3.7/JsBarcode.all.min.js"></script>

    </head>
    <style>
        .content-padding {
            padding-bottom: 110px;
            padding-top: 20px;
        }
        .top-buffer {
            margin-top:20px;
        }

    </style>
    <body>
        <div class="container">
            <div class="content-padding">
                <?php foreach ($orders as $order) { ?>
                    <?php $order_id = $order['order_id'] ?>
                    <div style="page-break-after: always;">
                        <div class="row">
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <table>
                                    <tr>
                                        <td  style="font-size:20px;"><?php echo 'Seller Order Invoice Of Order No.  '; ?></td>
                                        <td><svg id="code39"></svg></td>
                                    </tr>
                                </table>
                                <span class="barcode">
                                    <?php $barcode = $order['payment_iso_code_2'] . '-' . $order['order_id'] . '-' . $order['count_order']; ?>
                                </span>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <img src="<?php echo $logo ?>" alt="logo" />
                        </div>
                        <div class="row top-buffer">
                            <div class='col-md-6 col-sm-6 col-xs-6'>
                                <p style="font-size:20px;"><?php echo $text_email; ?>: <?php echo $order['store_email']; ?></p>
                                <p style="font-size:20px;"><?php echo $text_telephone; ?>: <?php echo $order['store_telephone']; ?></p>

                            </div>
                            <div class='col-md-6 col-sm-6 col-xs-6'>
                                <p style="font-size:20px;"><?php echo $text_date_added; ?>: <?php echo $order['date_added']; ?></p>
                                <p style="font-size:20px;"><?php echo 'Order ID'; ?>: <?php echo $order['order_id']; ?></p>
                            </div>


                        </div>

                        <div>
                            <table class="table table-bordered top-buffer">
                                <thead>
                                    <tr>
                                        <td style="width: 50%;"><b><?php echo $text_seller_info; ?></b></td>
                                        <td style="width: 50%;"><b><?php echo $text_shipping_address; ?></b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><address>
                                                <?php echo $order['payment_address']; ?>
                                            </address></td>
                                        <td><address>
                                                <?php echo $order['shipping_address']; ?>
                                            </address></td>
                                    </tr>
                                </tbody>
                            </table>


                            <p style="font-size:20px;"><?php echo $text_order_detail ?></p>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td><b><?php echo $column_product; ?></b></td>

                                        <td><b><?php echo 'SKU'; ?></b></td>
                                        <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
                                        <td class="text-right"><b><?php echo $column_price; ?></b></td>
                                        <td class="text-right"><b><?php echo $column_total; ?></b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $count=0;
                                    foreach ($order['product'] as $product) { ?>
                                        <tr>
                                            <td><?php echo $product['name']; ?>
                                                <?php foreach ($product['option'] as $option) { ?>
                                                    <br />
                                                    &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                                <?php } ?></td>
                                            <td>
                                                <span class="barcode">
                                                    <svg id="sku<?php echo $count; ?>"></svg>
                                                </span>
                                                <script>
                                                    JsBarcode("#sku<?php echo $count; ?>", "<?php echo $product['sku'] ?>", {format: "code39", width: 1.5, height: 70});
                                                </script>
                                            </td>

                                            <td class="text-right"><?php echo $product['quantity']; ?></td>
                                            <td class="text-right"><?php echo $product['price']; ?></td>
                                            <td class="text-right"><?php echo $product['total']; ?></td>
                                        </tr>
                                    <?php $count++; } ?>
                                    <?php foreach ($order['voucher'] as $voucher) { ?>
                                        <tr>
                                            <td><?php echo $voucher['description']; ?></td>
                                            <td></td>
                                            <td class="text-right">1</td>
                                            <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                            <td class="text-right"><?php echo $voucher['amount']; ?></td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>

                        </div>
                    <?php } ?>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12 col-sm-12 col-xs-6">
                           <table class="table">
                             <tbody>
                               <tr>
                                 <td width="500">  <p style="font-size:15px;">For further information, please contact our customer service center. <br>
                                 Tel. 800 433 0033 | email. <a href="mailto:finance@sayidatymall.net"> mall@sayidaty.net</a> </p>
                                 </td>
                                 <td width="450">
                                   <p style="font-size:15px;">
                                     <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/fb.png" width="20"> Sayidatymall</span>
                                     <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/tw.png" width="20"> sayidaty_mall</span> <br>
                                     <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/ins.png" width="20"> sayidatymall</span>
                                     <span style="padding: 0 20px 10px;"><img src="https://dnc2qm9v6i95t.cloudfront.net/emails/yt.png" width="20"> sayidaty mall</span>
                                   </p>
                                 </td>
                               </tr>
                             </tbody>
                           </table>
                         </div>
                      </div>
                </div>
            </div>
        </div>
        <script>    JsBarcode("#code39", "<?php echo $barcode ?>", {format: "code39", width: 1.7, height: 70});

        </script>
    </body>
</html>
