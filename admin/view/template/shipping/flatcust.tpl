<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-flat" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-flat" class="form-horizontal">
                  <!--  <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-cost"><?php echo $entry_cost; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="flatcust_cost" value="<?php echo $flatcust_cost; ?>" placeholder="<?php echo $entry_cost; ?>" id="input-cost" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-cost-mix"><?php echo $entry_cost_mix; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="flatcust_cost_mix" value="<?php echo $flatcust_cost_mix; ?>" placeholder="<?php echo $entry_cost_mix; ?>" id="input-cost-mix" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-zero-cost"><?php echo $entry_zero_cost; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="flatcust_zero_cost" value="<?php echo $flatcust_zero_cost; ?>" placeholder="<?php echo $entry_zero_cost; ?>" id="input-zero-cost" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-zero-cost-mix"><?php echo $entry_zero_cost_mix; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="flatcust_zero_cost_mix" value="<?php echo $flatcust_zero_cost_mix; ?>" placeholder="<?php echo $entry_zero_cost_mix; ?>" id="input-zero-cost-mix" class="form-control" />
                        </div>
                    </div>-->
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input_between_0-100"><?php echo $between_0_100; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="flatcust_between_0_100" value="<?php echo $flatcust_between_0_100; ?>" placeholder="<?php echo $between_0_100; ?>" id="input_between_0-100" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input_between_101-200"><?php echo $between_101_200; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="flatcust_between_101_200" value="<?php echo $flatcust_between_101_200; ?>" placeholder="<?php echo $between_101_200; ?>" id="input_between_101-200" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input_between_201-300"><?php echo $between_201_300; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="flatcust_between_201_300" value="<?php echo $flatcust_between_201_300; ?>" placeholder="<?php echo $between_201_300; ?>" id="input_between_201-300" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
                        <div class="col-sm-10">
                            <select name="flatcust_tax_class_id" id="input-tax-class" class="form-control">
                                <option value="0"><?php echo $text_none; ?></option>
                <?php foreach ($tax_classes as $tax_class) { ?>
                <?php if ($tax_class['tax_class_id'] == $flatcust_tax_class_id) { ?>
                                <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                <?php } else { ?>
                                <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                <?php } ?>
                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-geo-zone"><?php echo $entry_geo_zone; ?></label>
                        <div class="col-sm-10">
                            <select name="flatcust_geo_zone_id" id="input-geo-zone" class="form-control">
                                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $flatcust_geo_zone_id) { ?>
                                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                            <select name="flatcust_status" id="input-status" class="form-control">
                <?php if ($flatcust_status) { ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="flatcust_sort_order" value="<?php echo $flatcust_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?> 