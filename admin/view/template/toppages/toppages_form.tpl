<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-toppages" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-toppages" class="form-horizontal">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                    <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?>
                                        </a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="top_pages_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($toppages_description[$language['language_id']]) ? $toppages_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_name[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="top_pages_description[<?php echo $language['language_id']; ?>][body]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($toppages_description[$language['language_id']]) ? $toppages_description[$language['language_id']]['description'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- { / tab-content } !-->

                        </div>
                        <!-- { / tab-pane } !-->


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-image"><?php echo $column_image; ?></label>
                            <div class="col-sm-10">
                                <img src="<?php
                                if (isset($toppages)) {
                                    echo $toppages[0]['image'];
                                }
                                ?>" alt="" title="" data-placeholder="" />
                                <input type="file" name="image" value="<?php echo $image; ?>" id="input-image" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-date"><?php echo $entry_date; ?></label>
                            <div class="col-sm-3">
                                <div class="input-group date">
                                    <input type="text" name="date" value="<?php
                                    if (isset($toppages[0]['day'])) {
                                        echo $toppages[0]['day'];
                                    }
                                    ?>" placeholder="<?php echo $entry_date; ?>" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span></div>
                            </div>
                        </div>
                        <div class="form-group country-field">
                            <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country_id; ?></label>
                            <div class="col-sm-10"> 
                                <select class="form-control" name="top_pages_link[country][]" id="input-country" data-input-type="country" multiple placeholder="<?php echo $entry_country_id; ?>"></select>
                            </div>
                        </div>
                        <div class="form-group group-field">
                            <label class="col-sm-2 control-label" for="input-group"><?php echo $entry_group; ?></label>
                            <div class="col-sm-10">
                                <select class="form-control" name="top_pages_link[group][]" id="input-group" data-input-type="group" multiple placeholder="<?php echo $entry_group; ?>"></select>
                            </div>
                        </div>

                        <div class="form-group keyword-field">
                            <label class="col-sm-2 control-label" for="input-keyword"><?php echo $entry_keywords; ?></label>
                            <div class="col-sm-10">
                                <select class="form-control" name="top_pages_link[keyword][]" id="input-keyword" data-input-type="keyword" multiple placeholder="<?php echo $entry_keywords; ?>"></select>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                            <div class="col-sm-3">
                                <select name="status" id="input-status" class="form-control">
                                    <option value="1" <?php if (isset($toppages[0]['status']) && $toppages[0]['status'] == '1') { ?> selected="selected" <?php } ?> ><?php echo 'Enable'; ?></option>
                                    <option value="0"  <?php if (isset($toppages[0]['status']) && $toppages[0]['status'] == '0') { ?> selected="selected" <?php } ?> ><?php echo 'Disable'; ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-language"><?php echo $entry_language; ?></label>
                            <div class="col-sm-10">
                                <?php echo $arabic_lang; ?>&nbsp; <input type="checkbox" name="is_arabic" id="input-arlanguage" value="1" <?php if ($toppages[0]['is_arabic'] > 0) { ?> checked <?php } ?>> &nbsp;&nbsp;&nbsp;
                                <?php echo $english_lang; ?>&nbsp <input type="checkbox" name="is_english" id="input-enlanguage" value="1" <?php if ($toppages[0]['is_english'] > 0) { ?> checked <?php } ?>> 
                            </div>
                        </div>

                        <table id="top_pages" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td class="text-left"><?php echo $entry_name; ?></td>
                                    <td class="text-left"><?php echo $entry_sort_order; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $top_pages_row = 0; ?>
                                <?php foreach ($toppages_details as $top_pages) { ?>

                                    <?php if ($top_pages['type']) { ?>

                                        <tr id="top_pages-row<?php echo $top_pages_row; ?>">
                                            <td class="text-left">
                                                <input type="hidden" name="top_pages[<?php echo $top_pages_row; ?>]" value="<?php echo $_GET['top_pages_id']; ?>" />
                                                <input  type="text"  name="filter_name[<?php echo $top_pages_row; ?>]" value="<?php echo $top_pages['type_id']; ?>" id="input-type_id<?php echo $top_pages_row; ?>" class="form-control" />
                                                <input type="hidden" name="type_id[<?php echo $top_pages_row; ?>]" value="<?php echo $top_pages['id']; ?>" id="input-link-id<?php echo $top_pages_row; ?>" />

                                            </td>
                                            <td class="text-right"><input type="text" name="sort_order[<?php echo $top_pages_row; ?>]" value="<?php echo $top_pages['sort_order']; ?>" class="form-control" /></td>
                                        </tr>

                                        <?php $top_pages_row++; ?>
                                        <?php
                                    }
                                }
                                if (!isset($top_pages['type'])) {
                                    $top_pages_row2 = 0;
                                    $i = 5;
                                } else {
                                    $top_pages_row2 = count($top_pages);
                                    $i = 4;
                                }
                                for ($i; $top_pages_row2 < $i; $i --) {
                                    ?>
                                    <tr id="top_pages-row<?php echo $top_pages_row2; ?>">
                                        <td class="text-left">
                                            <input type="hidden" name="top_pages[<?php echo $top_pages_row2; ?>]" value="<?php
                                            if (isset($_GET['top_pages_id'])) {
                                                echo $_GET['top_pages_id'];
                                            }
                                            ?>" />
                                            <input type="text"   name="filter_name[<?php echo $top_pages_row2; ?>]" value="" placeholder="<?php echo $entry_name; ?>" id="input-type_id<?php echo $top_pages_row2; ?>" class="form-control" />                                           
                                            <input type="hidden" name="type_id[<?php echo $top_pages_row2; ?>]" value="" id="input-link-id<?php echo $top_pages_row2; ?>" />
                                        </td>
                                        <td class="text-right"><input type="text" name="sort_order[<?php echo $top_pages_row2; ?>]" value="<?php echo $top_pages_row2; ?>" class="form-control" /></td>
                                    </tr>
                                    <?php
                                    $top_pages_row2++;
                                    if (!isset($top_pages['type'])) {
                                        $i ++;
                                    }
                                }
                                ?>
                            </tbody>

                        </table>


                </form>

            </div>
        </div>
    </div>
    <script>
        var dataSource1 = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'index.php?route=mall/tip/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
                replace: function (url) {
                    return url.replace('%TYPE', encodeURIComponent($('.country-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.country-field .tt-input').val()));
                },
                wildcard: '%QUERY',
                filter: function (list) {
                    return $.map(list, function (v, i) {
                        return {id: v.id, name: v.name};
                    });
                }
            }
        });
        dataSource1.initialize();
        $('.country-field > > #input-country').tagsinput({
            itemValue: 'id',
            itemText: 'name',
            typeaheadjs: {
                name: 'dataSource1',
                displayKey: 'name',
                source: dataSource1.ttAdapter()
            }
        });
    </script>
    <script>
        var dataSource2 = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'index.php?route=mall/look/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
                replace: function (url) {
                    return url.replace('%TYPE', encodeURIComponent($('.group-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.group-field .tt-input').val()));
                },
                wildcard: '%QUERY',
                filter: function (list) {
                    return $.map(list, function (v, i) {
                        return {id: v.id, name: v.name};
                    });
                }
            }
        });
        dataSource2.initialize();
        $('.group-field > > #input-group').tagsinput({
            itemValue: 'id',
            itemText: 'name',
            typeaheadjs: {
                name: 'dataSource2',
                displayKey: 'name',
                source: dataSource2.ttAdapter()
            }
        });
    </script>
    <script type="text/javascript">
        var dataSource3 = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'index.php?route=mall/tip/autocomplete&token=<?php echo $token; ?>&query=%QUERY&type=%TYPE',
                replace: function (url) {
                    return url.replace('%TYPE', encodeURIComponent($('.keyword-field .tt-input').parents(".bootstrap-tagsinput").next().data('input-type'))).replace('%QUERY', encodeURIComponent($('.keyword-field .tt-input').val()));
                },
                wildcard: '%QUERY',
                filter: function (list) {
                    return $.map(list, function (v, i) {
                        return {id: v.id, name: v.name};
                    });
                }
            }
        });
        dataSource3.initialize();
        $('.keyword-field > > #input-keyword').tagsinput({
            itemValue: 'id',
            itemText: 'name',
            typeaheadjs: {
                name: 'dataSource3',
                displayKey: 'name',
                source: dataSource3.ttAdapter()
            }
        });
    </script>
    <script type="text/javascript"><!--
        $('input[name=\'filter_name[0]\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=toppages/toppages/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'filter_name[0]\']').val(item['label']);
                $('input[name=\'type_id[0]\']').val(item['value']);

            }
        });
        $('input[name=\'filter_name[1]\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=toppages/toppages/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'filter_name[1]\']').val(item['label']);
                $('input[name=\'type_id[1]\']').val(item['value']);
            }
        });
        $('input[name=\'filter_name[2]\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=toppages/toppages/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'filter_name[2]\']').val(item['label']);
                $('input[name=\'type_id[2]\']').val(item['value']);

            }
        });
        $('input[name=\'filter_name[3]\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=toppages/toppages/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'filter_name[3]\']').val(item['label']);
                $('input[name=\'type_id[3]\']').val(item['value']);

            }
        });
        $('input[name=\'filter_name[4]\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=toppages/toppages/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'filter_name[4]\']').val(item['label']);
                $('input[name=\'type_id[4]\']').val(item['value']);

            }
        });
        $('input[name=\'filter_name[5]\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=toppages/toppages/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'filter_name[5]\']').val(item['label']);
                $('input[name=\'type_id[5]\']').val(item['value']);

            }
        });
        $('.date').datetimepicker({
            pickTime: false
        });
        //--></script>

    <script type="text/javascript">
        var linksArray = eval('(<?php echo json_encode($top_pages_link) ?>)');
        if (!jQuery.isEmptyObject(linksArray)) {

            $.each(linksArray, function (index, value) {
                switch (index) {
                    case 'country':
                        $.each(value, function (index1, value1) {
                            $('#input-country').tagsinput('add', {
                                "id": value1.id,
                                "name": "" + value1.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'group':
                        $.each(value, function (index5, value5) {
                            $('#input-group').tagsinput('add', {
                                "id": value5.id,
                                "name": "" + value5.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    case 'keyword':
                        $.each(value, function (index6, value6) {
                            $('#input-keyword').tagsinput('add', {
                                "id": value6.id,
                                "name": "" + value6.name.replace(/'/g, "\\'") + ""
                            });
                        });
                        break;
                    default:
                        break;
                }
            });
        }
    </script>

    <script>
<?php foreach ($languages as $language) { ?>
            // $("#input-description<?php echo $language['language_id']; ?>").summernote();
            CKEDITOR.replace("input-description<?php echo $language['language_id']; ?>", {
                filebrowserUploadUrl: "index.php?route=common/fileuploader/upload&token=<?php echo $token ?>",
                filebrowserWindowWidth: 800,
                filebrowserWindowHeight: 500
            });
<?php } ?>
        $('#language a:first').tab('show');
    </script>
    <style type="text/css">
        .dropdown-menu{
            max-height: 200px;
            overflow-y: scroll; 
        }
    </style>
</div>
<?php echo $footer; ?>