<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-customer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-customer" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <?php if ($customer_id) { ?>
                            <li><a href="#tab-history" data-toggle="tab"><?php echo $tab_history; ?></a></li>
                            <li><a href="#tab-transaction" data-toggle="tab"><?php echo $tab_transaction; ?></a></li>
                            <li><a href="#tab-reward" data-toggle="tab"><?php echo $tab_reward; ?></a></li>
                            <li><a href="#tab-ip" data-toggle="tab"><?php echo $tab_ip; ?></a></li>
                           <!-- <li><a href="#tab-popup" data-toggle="tab"><?php echo $tab_popup; ?></a></li> -->
                        <?php } ?>
                        <li><a href="#tab-newsletter" data-toggle="tab"><?php echo $tab_newsletter; ?></a></li>
                        <li><a href="#tab-notification" data-toggle="tab"><?php echo $tab_notification; ?></a></li>
                        <li><a href="#tab-view" data-toggle="tab"><?php echo $tab_view; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <div class="row">
                                <div class="col-sm-2">
                                    <ul class="nav nav-pills nav-stacked" id="address">
                                        <li class="active"><a href="#tab-customer" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                                        <?php $address_row = 1; ?>
                                        <?php foreach ($addresses as $address) { ?>
                                            <li><a href="#tab-address<?php echo $address_row; ?>" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$('#address a:first').tab('show');
                                                    $('#address a[href=\'#tab-address<?php echo $address_row; ?>\']').parent().remove();
                                                    $('#tab-address<?php echo $address_row; ?>').remove();"></i> <?php echo $tab_address . ' ' . $address_row; ?></a></li>
                                                <?php $address_row++; ?>
                                            <?php } ?>
                                        <li id="address-add"><a onclick="addAddress();"><i class="fa fa-plus-circle"></i> <?php echo $button_address_add; ?></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-10">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab-customer">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-customer-group"><?php echo $entry_customer_group; ?></label>
                                                <div class="col-sm-10">
                                                    <select name="customer_group_id" id="input-customer-group" class="form-control">
                                                        <?php foreach ($customer_groups as $customer_group) { ?>
                                                            <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
                                                                <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                                                    <?php if ($error_firstname) { ?>
                                                        <div class="text-danger"><?php echo $error_firstname; ?></div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                                                    <?php if ($error_lastname) { ?>
                                                        <div class="text-danger"><?php echo $error_lastname; ?></div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                                                    <?php if ($error_email) { ?>
                                                        <div class="text-danger"><?php echo $error_email; ?></div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="form-group required">
                                                <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
                                                    <?php if ($error_telephone) { ?>
                                                        <div class="text-danger"><?php echo $error_telephone; ?></div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-fax"><?php echo $entry_fax; ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control" />
                                                </div>
                                            </div>
                                            <?php foreach ($custom_fields as $custom_field) { ?>
                                                <?php if ($custom_field['location'] == 'account') { ?>
                                                    <?php if ($custom_field['type'] == 'select') { ?>
                                                        <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-10">
                                                                <select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
                                                                    <option value=""><?php echo $text_select; ?></option>
                                                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                                        <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $account_custom_field[$custom_field['custom_field_id']]) { ?>
                                                                            <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>" selected="selected"><?php echo $custom_field_value['name']; ?></option>
                                                                        <?php } else { ?>
                                                                            <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </select>
                                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($custom_field['type'] == 'radio' || $custom_field['type'] == 'size') { ?>
                                                        <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-10">
                                                                <div>
                                                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                                        <div class="radio">
                                                                            <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $account_custom_field[$custom_field['custom_field_id']]) { ?>
                                                                                <label>
                                                                                    <input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
                                                                                    <?php echo $custom_field_value['name']; ?></label>
                                                                            <?php } else { ?>
                                                                                <label>
                                                                                    <input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
                                                                                    <?php echo $custom_field_value['name']; ?></label>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($custom_field['type'] == 'checkbox') { ?>
                                                        <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-10">
                                                                <div>
                                                                    <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                                        <div class="checkbox">
                                                                            <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $account_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                                <label>
                                                                                    <input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
                                                                                    <?php echo $custom_field_value['name']; ?></label>
                                                                            <?php } else { ?>
                                                                                <label>
                                                                                    <input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
                                                                                    <?php echo $custom_field_value['name']; ?></label>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($custom_field['type'] == 'text') { ?>
                                                        <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-10">
                                                                <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($custom_field['type'] == 'textarea') { ?>
                                                        <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-10">
                                                                <textarea name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
                                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($custom_field['type'] == 'file') { ?>
                                                        <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-10">
                                                                <button type="button" id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                                                <input type="hidden" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : ''); ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" />
                                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($custom_field['type'] == 'date') { ?>
                                                        <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group date">
                                                                    <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                    <span class="input-group-btn">
                                                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                    </span></div>
                                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($custom_field['type'] == 'time') { ?>
                                                        <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group time">
                                                                    <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                    <span class="input-group-btn">
                                                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                    </span></div>
                                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if ($custom_field['type'] == 'datetime') { ?>
                                                        <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">
                                                            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group datetime">
                                                                    <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                    <span class="input-group-btn">
                                                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                    </span></div>
                                                                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                                                                    <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php /* ?>  <div class="form-group required">
                                              <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
                                              <div class="col-sm-10">
                                              <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" autocomplete="off" />
                                              <?php if ($error_password) { ?>
                                              <div class="text-danger"><?php echo $error_password; ?></div>
                                              <?php } ?>
                                              </div>
                                              </div>
                                              <div class="form-group required">
                                              <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                                              <div class="col-sm-10">
                                              <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" autocomplete="off" id="input-confirm" class="form-control" />
                                              <?php if ($error_confirm) { ?>
                                              <div class="text-danger"><?php echo $error_confirm; ?></div>
                                              <?php } ?>
                                              </div>
                                              </div> <?php */ ?>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                                <div class="col-sm-10">
                                                    <select name="status" id="input-status" class="form-control">
                                                        <?php if ($status) { ?>
                                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                                            <option value="0"><?php echo $text_disabled; ?></option>
                                                        <?php } else { ?>
                                                            <option value="1"><?php echo $text_enabled; ?></option>
                                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-approved"><?php echo $entry_approved; ?></label>
                                                <div class="col-sm-10">
                                                    <select name="approved" id="input-approved" class="form-control">
                                                        <?php if ($approved) { ?>
                                                            <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                                            <option value="0"><?php echo $text_no; ?></option>
                                                        <?php } else { ?>
                                                            <option value="1"><?php echo $text_yes; ?></option>
                                                            <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-safe"><?php echo $entry_safe; ?></label>
                                                <div class="col-sm-10">
                                                    <select name="safe" id="input-safe" class="form-control">
                                                        <?php if ($safe) { ?>
                                                            <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                                            <option value="0"><?php echo $text_no; ?></option>
                                                        <?php } else { ?>
                                                            <option value="1"><?php echo $text_yes; ?></option>
                                                            <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-gender"><?php echo $entry_gender; ?></label>
                                                <div class="col-sm-10">
                                                    <select name="gender" id="input-gender" class="form-control">
                                                        <?php if ($gender == 'male') { ?>
                                                            <option value=""><?php echo $text_select; ?></option>
                                                            <option value="male" selected="selected"><?php echo $male; ?></option>
                                                            <option value="female"><?php echo $female; ?></option>
                                                        <?php } elseif ($gender == 'female') { ?>
                                                            <option value="" ><?php echo $text_select; ?></option>
                                                            <option value="male"><?php echo $male; ?></option>
                                                            <option value="female" selected="selected"><?php echo $female; ?></option>
                                                        <?php } else { ?>
                                                            <option value="" selected="selected"><?php echo $text_select; ?></option>
                                                            <option value="male"><?php echo $male; ?></option>
                                                            <option value="female" ><?php echo $female; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="col-sm-2 control-label" for="input-country"><?php echo 'Interface'; ?></label>
                                                <div class="col-sm-10">
                                                    <select name="country_interface" id="input-country-interface" class="form-control">
                                                        <option value="<?php echo $interface_country_id; ?>" selected="selected"><?php echo $interface_country_name; ?></option>
                                                        <?php foreach ($countries as $country) { ?>
                                                            <?php if ($interface_country_id != $country['country_id']) { ?>
                                                                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="col-sm-2 control-label" for="input-country<?php echo $address_row; ?>"><?php echo $entry_country; ?></label>
                                                <div class="col-sm-10">
                                                    <select name="country" id="input-country" class="form-control">
                                                        <option value="<?php echo $country_id; ?>" selected="selected"><?php echo $country_name; ?></option>
                                                        <?php foreach ($jarwan as $country) { ?>
                                                            <option value="<?php echo $country['country_id']; ?>" ><?php echo $country['name']; ?></option>
                                                        <?php } ?>

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="col-sm-2 control-label" for="input-language<?php echo $address_row; ?>"><?php echo $entry_language_id; ?></label>
                                                <div class="col-sm-10">
                                                    <select name="language" id="input-language" class="form-control">
                                                        <?php if ($language_id == 2) { ?>
                                                            <option value="2" selected="selected"><?php echo "ARABIC"; ?></option>
                                                            <option value="1"><?php echo 'English'; ?></option>
                                                        <?php } else { ?>
                                                            <option value="2"><?php echo 'ARABIC'; ?></option>
                                                            <option value="1" selected="selected"><?php echo "English"; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>


                                        </div>
                                        <?php $address_row = 1; ?>
                                        <?php foreach ($addresses as $address) { ?>
                                            <div class="tab-pane" id="tab-address<?php echo $address_row; ?>">
                                                <input type="hidden" name="address[<?php echo $address_row; ?>][address_id]" value="<?php echo $address['address_id']; ?>" />
                                                <div class="form-group required">
                                                    <label class="col-sm-2 control-label" for="input-firstname<?php echo $address_row; ?>"><?php echo $entry_firstname; ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="address[<?php echo $address_row; ?>][firstname]" value="<?php echo $address['firstname']; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname<?php echo $address_row; ?>" class="form-control" />
                                                        <?php if (isset($error_address[$address_row]['firstname'])) { ?>
                                                            <div class="text-danger"><?php echo $error_address[$address_row]['firstname']; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-2 control-label" for="input-lastname<?php echo $address_row; ?>"><?php echo $entry_lastname; ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="address[<?php echo $address_row; ?>][lastname]" value="<?php echo $address['lastname']; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname<?php echo $address_row; ?>" class="form-control" />
                                                        <?php if (isset($error_address[$address_row]['lastname'])) { ?>
                                                            <div class="text-danger"><?php echo $error_address[$address_row]['lastname']; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-company<?php echo $address_row; ?>"><?php echo $entry_company; ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="address[<?php echo $address_row; ?>][company]" value="<?php echo $address['company']; ?>" placeholder="<?php echo $entry_company; ?>" id="input-company<?php echo $address_row; ?>" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-2 control-label" for="input-address-1<?php echo $address_row; ?>"><?php echo $entry_address_1; ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="address[<?php echo $address_row; ?>][address_1]" value="<?php echo $address['address_1']; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1<?php echo $address_row; ?>" class="form-control" />
                                                        <?php if (isset($error_address[$address_row]['address_1'])) { ?>
                                                            <div class="text-danger"><?php echo $error_address[$address_row]['address_1']; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="input-address-2<?php echo $address_row; ?>"><?php echo $entry_address_2; ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="address[<?php echo $address_row; ?>][address_2]" value="<?php echo $address['address_2']; ?>" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2<?php echo $address_row; ?>" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-2 control-label" for="input-city<?php echo $address_row; ?>"><?php echo $entry_city; ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="address[<?php echo $address_row; ?>][city]" value="<?php echo $address['city']; ?>" placeholder="<?php echo $entry_city; ?>" id="input-city<?php echo $address_row; ?>" class="form-control" />
                                                        <?php if (isset($error_address[$address_row]['city'])) { ?>
                                                            <div class="text-danger"><?php echo $error_address[$address_row]['city']; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-2 control-label" for="input-postcode<?php echo $address_row; ?>"><?php echo $entry_postcode; ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="address[<?php echo $address_row; ?>][postcode]" value="<?php echo $address['postcode']; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode<?php echo $address_row; ?>" class="form-control" />
                                                        <?php if (isset($error_address[$address_row]['postcode'])) { ?>
                                                            <div class="text-danger"><?php echo $error_address[$address_row]['postcode']; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-2 control-label" for="input-country<?php echo $address_row; ?>"><?php echo $entry_country; ?></label>
                                                    <div class="col-sm-10">
                                                        <select name="address[<?php echo $address_row; ?>][country_id]" id="input-country<?php echo $address_row; ?>" onchange="change_country(this, '<?php echo $address_row; ?>', '<?php echo $address['zone_id']; ?>');" class="form-control">
                                                            <option value=""><?php echo $text_select; ?></option>
                                                            <?php foreach ($countries as $country) { ?>
                                                                <?php if ($country['country_id'] == $address['country_id']) { ?>
                                                                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                                                <?php } else { ?>
                                                                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </select>
                                                        <?php if (isset($error_address[$address_row]['country'])) { ?>
                                                            <div class="text-danger"><?php echo $error_address[$address_row]['country']; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-2 control-label" for="input-zone<?php echo $address_row; ?>"><?php echo $entry_zone; ?></label>
                                                    <div class="col-sm-10">
                                                        <select name="address[<?php echo $address_row; ?>][zone_id]" id="input-zone<?php echo $address_row; ?>" class="form-control">
                                                        </select>
                                                        <?php if (isset($error_address[$address_row]['zone'])) { ?>
                                                            <div class="text-danger"><?php echo $error_address[$address_row]['zone']; ?></div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <?php foreach ($custom_fields as $custom_field) { ?>
                                                    <?php if ($custom_field['location'] == 'address') { ?>
                                                        <?php if ($custom_field['type'] == 'select') { ?>
                                                            <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">
                                                                <label class="col-sm-2 control-label" for="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                                <div class="col-sm-10">
                                                                    <select name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" id="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
                                                                        <option value=""><?php echo $text_select; ?></option>
                                                                        <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                                            <?php if (isset($address['custom_field'][$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $address['custom_field'][$custom_field['custom_field_id']]) { ?>
                                                                                <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>" selected="selected"><?php echo $custom_field_value['name']; ?></option>
                                                                            <?php } else { ?>
                                                                                <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </select>
                                                                    <?php if (isset($error_address[$address_row]['custom_field'][$custom_field['custom_field_id']])) { ?>
                                                                        <div class="text-danger"><?php echo $error_address[$address_row]['custom_field'][$custom_field['custom_field_id']]; ?></div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'radio') { ?>
                                                            <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">
                                                                <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                                                <div class="col-sm-10">
                                                                    <div>
                                                                        <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                                            <div class="radio">
                                                                                <?php if (isset($address['custom_field'][$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $address['custom_field'][$custom_field['custom_field_id']]) { ?>
                                                                                    <label>
                                                                                        <input type="radio" name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
                                                                                        <?php echo $custom_field_value['name']; ?></label>
                                                                                <?php } else { ?>
                                                                                    <label>
                                                                                        <input type="radio" name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
                                                                                        <?php echo $custom_field_value['name']; ?></label>
                                                                                <?php } ?>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <?php if (isset($error_address[$address_row]['custom_field'][$custom_field['custom_field_id']])) { ?>
                                                                        <div class="text-danger"><?php echo $error_address[$address_row]['custom_field'][$custom_field['custom_field_id']]; ?></div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'checkbox') { ?>
                                                            <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">
                                                                <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                                                <div class="col-sm-10">
                                                                    <div>
                                                                        <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                                                                            <div class="checkbox">
                                                                                <?php if (isset($address['custom_field'][$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $address['custom_field'][$custom_field['custom_field_id']])) { ?>
                                                                                    <label>
                                                                                        <input type="checkbox" name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
                                                                                        <?php echo $custom_field_value['name']; ?></label>
                                                                                <?php } else { ?>
                                                                                    <label>
                                                                                        <input type="checkbox" name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
                                                                                        <?php echo $custom_field_value['name']; ?></label>
                                                                                <?php } ?>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <?php if (isset($error_address[$address_row]['custom_field'][$custom_field['custom_field_id']])) { ?>
                                                                        <div class="text-danger"><?php echo $error_address[$address_row]['custom_field'][$custom_field['custom_field_id']]; ?></div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'text') { ?>
                                                            <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">
                                                                <label class="col-sm-2 control-label" for="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address['custom_field'][$custom_field['custom_field_id']]) ? $address['custom_field'][$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                    <?php if (isset($error_address[$address_row]['custom_field'][$custom_field['custom_field_id']])) { ?>
                                                                        <div class="text-danger"><?php echo $error_address[$address_row]['custom_field'][$custom_field['custom_field_id']]; ?></div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'textarea') { ?>
                                                            <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">
                                                                <label class="col-sm-2 control-label" for="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                                <div class="col-sm-10">
                                                                    <textarea name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo (isset($address['custom_field'][$custom_field['custom_field_id']]) ? $address['custom_field'][$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
                                                                    <?php if (isset($error_address[$address_row]['custom_field'][$custom_field['custom_field_id']])) { ?>
                                                                        <div class="text-danger"><?php echo $error_address[$address_row]['custom_field'][$custom_field['custom_field_id']]; ?></div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'file') { ?>
                                                            <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">
                                                                <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
                                                                <div class="col-sm-10">
                                                                    <button type="button" id="button-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                                                    <input type="hidden" name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address['custom_field'][$custom_field['custom_field_id']]) ? $address['custom_field'][$custom_field['custom_field_id']] : ''); ?>" />
                                                                    <?php if (isset($error_address[$address_row]['custom_field'][$custom_field['custom_field_id']])) { ?>
                                                                        <div class="text-danger"><?php echo $error_address[$address_row]['custom_field'][$custom_field['custom_field_id']]; ?></div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'date') { ?>
                                                            <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">
                                                                <label class="col-sm-2 control-label" for="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                                <div class="col-sm-10">
                                                                    <div class="input-group date">
                                                                        <input type="text" name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address['custom_field'][$custom_field['custom_field_id']]) ? $address['custom_field'][$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                        <span class="input-group-btn">
                                                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                        </span></div>
                                                                    <?php if (isset($error_address[$address_row]['custom_field'][$custom_field['custom_field_id']])) { ?>
                                                                        <div class="text-danger"><?php echo $error_address[$address_row]['custom_field'][$custom_field['custom_field_id']]; ?></div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'time') { ?>
                                                            <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">
                                                                <label class="col-sm-2 control-label" for="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                                <div class="col-sm-10">
                                                                    <div class="input-group time">
                                                                        <input type="text" name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address['custom_field'][$custom_field['custom_field_id']]) ? $address['custom_field'][$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                        <span class="input-group-btn">
                                                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                        </span></div>
                                                                    <?php if (isset($error_address[$address_row]['custom_field'][$custom_field['custom_field_id']])) { ?>
                                                                        <div class="text-danger"><?php echo $error_address[$address_row]['custom_field'][$custom_field['custom_field_id']]; ?></div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if ($custom_field['type'] == 'datetime') { ?>
                                                            <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">
                                                                <label class="col-sm-2 control-label" for="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
                                                                <div class="col-sm-10">
                                                                    <div class="input-group datetime">
                                                                        <input type="text" name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address['custom_field'][$custom_field['custom_field_id']]) ? $address['custom_field'][$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-address<?php echo $address_row; ?>-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
                                                                        <span class="input-group-btn">
                                                                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                        </span></div>
                                                                    <?php if (isset($error_address[$address_row]['custom_field'][$custom_field['custom_field_id']])) { ?>
                                                                        <div class="text-danger"><?php echo $error_address[$address_row]['custom_field'][$custom_field['custom_field_id']]; ?></div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"><?php echo $entry_default; ?></label>
                                                    <div class="col-sm-10">
                                                        <label class="radio">
                                                            <?php if (($address['address_id'] == $address_id) || !$addresses) { ?>
                                                                <input type="radio" name="address[<?php echo $address_row; ?>][default]" value="<?php echo $address_row; ?>" checked="checked" />
                                                            <?php } else { ?>
                                                                <input type="radio" name="address[<?php echo $address_row; ?>][default]" value="<?php echo $address_row; ?>" />
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php $address_row++; ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($customer_id) { ?>
                            <div class="tab-pane" id="tab-history">
                                <div id="history"></div>
                                <br />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-comment"><?php echo $entry_comment; ?></label>
                                    <div class="col-sm-10">
                                        <textarea name="comment" rows="8" placeholder="<?php echo $entry_comment; ?>" id="input-comment" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button id="button-history" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_history_add; ?></button>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-transaction">
                                <div id="transaction"></div>
                                <br />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-transaction-description"><?php echo $entry_description; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="description" value="" placeholder="<?php echo $entry_description; ?>" id="input-transaction-description" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-pricecode"><?php echo $entry_pricecode; ?></label>
                                    <div class="col-sm-10">
                                        <select name="pricecode" id="input-pricecode" onchange="changecur()" class="form-control">
                                            <?php foreach ($currencies as $row) { ?>
                                                <?php if ($row['code'] == $pricecode) { ?>
                                                    <option value="<?php echo $row['code']; ?>" selected="selected"><?php echo $row['title']; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $row['code']; ?>"><?php echo $row['title']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-amount"><?php echo $entry_amount; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="amount" value="" placeholder="<?php echo $entry_amount; ?>" id="input-amount" class="form-control" />
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="button" id="button-transaction" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_transaction_add; ?></button>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-reward">
                                <div id="reward"></div>
                                <br />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-reward-description"><?php echo $entry_description; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="description" value="" placeholder="<?php echo $entry_description; ?>" id="input-reward-description" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-points"><span data-toggle="tooltip" title="<?php echo $help_points; ?>"><?php echo $entry_points; ?></span></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="points" value="" placeholder="<?php echo $entry_points; ?>" id="input-points" class="form-control" />
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="button" id="button-reward" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_reward_add; ?></button>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="tab-pane" id="tab-ip">
                            <div id="ip"></div>
                        </div>
                        <div class="tab-pane" id="tab-newsletter">
                            <div id="newsletter">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-newsletter"><?php echo $entry_newsletter; ?></label>
                                    <div class="col-sm-3">
                                        <select name="newsletter" id="input-newsletter" class="form-control">
                                            <?php if ($newsletter2) { ?>
                                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                                <option value="0"><?php echo $text_disabled; ?></option>
                                            <?php } else { ?>
                                                <option value="1"><?php echo $text_enabled; ?></option>
                                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <hr>

                                <?php if (isset($newsletter_category) && !empty($newsletter_category)) { ?>
                                    <?php $counter = count($newsletter_category); ?>
                                    <?php foreach ($newsletter_category as $key => $value) { ?>
                                        <?php if ($key % 2 == 0) { ?><div class="row"><?php } ?>
                                            <div class="col-md-6 col-sm-6">
                                                <p>
                                                    <label>
                                                        <input type="checkbox" <?php echo $value['selected'] == 1 ? "checked" : "" ?> class="exclude" value="<?php echo $value['newsletter_category_id']; ?>" name="newsletter_check[]">
                                                        <?php echo $value['name']; ?>
                                                    </label>
                                                </p>
                                            </div>
                                            <?php $counter--; ?>
                                            <?php if ($key % 2 != 0) { ?></div><?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="tab-pane" id='tab-notification'>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-product"><span data-toggle="tooltip" title="<?php echo $entry_product_id_info; ?>"><?php echo $entry_product_id; ?></span></label>
                            <div class="col-sm-10">
                                <input type="text" name="product" value="" placeholder="<?php echo $entry_product_id; ?>" id="input-product" class="form-control notification-autocomplete" />
                                <div id="product-product" class="well well-sm notification-notification" style="height: 150px; overflow: auto;">
                                    <?php foreach ($favorite_products as $product) { ?>
                                        <div id="product-id<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                                            <input type="hidden" name="product_ids[]" value="<?php echo $product['product_id']; ?>" />
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-brand"><span data-toggle="tooltip" title="<?php echo $entry_brand_id_info; ?>"><?php echo $entry_brand_id; ?></span></label>
                            <div class="col-sm-10">
                                <input type="text" name="brand" value="" placeholder="<?php echo $entry_brand_id; ?>" id="input-brand" class="form-control notification-autocomplete" />
                                <div id="brand-brand" class="well well-sm notification-notification" style="height: 150px; overflow: auto;">
                                    <?php foreach ($favorite_brands as $brand) { ?>
                                        <div id="brand-id<?php echo $brand['brand_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $brand['name']; ?>
                                            <input type="hidden" name="brand_ids[]" value="<?php echo $brand['brand_id']; ?>" />
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-seller"><span data-toggle="tooltip" title="<?php echo $entry_seller_id_info; ?>"><?php echo $entry_seller_id; ?></span></label>
                            <div class="col-sm-10">
                                <input type="text" name="seller" value="" placeholder="<?php echo $entry_seller_id; ?>" id="input-seller" class="form-control notification-autocomplete" />
                                <div id="seller-seller" class="well well-sm notification-notification" style="height: 150px; overflow: auto;">
                                    <?php foreach ($favorite_sellers as $seller) { ?>
                                        <div id="seller-id<?php echo $seller['seller_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $seller['screenname']; ?>
                                            <input type="hidden" name="seller_ids[]" value="<?php echo $seller['seller_id']; ?>" />
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-category"><span data-toggle="tooltip" title="<?php echo $entry_category_id_info; ?>"><?php echo $entry_category_id; ?></span></label>
                            <div class="col-sm-10">
                                <input type="text" name="category"  value="" placeholder="<?php echo $entry_category_id; ?>" id="input-category" class="form-control notification-autocomplete" />
                                <div id="category-category" class="well well-sm notification-notification" style="height: 150px; overflow: auto;">
                                    <?php foreach ($favorite_categories as $category) { ?>
                                        <div id="category-id<?php echo $category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $category['name']; ?>
                                            <input type="hidden" name="category_ids[]" value="<?php echo $category['category_id']; ?>" />
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-look"><span data-toggle="tooltip" title="<?php echo $entry_look_id_info; ?>"><?php echo $entry_look_id; ?></span></label>
                            <div class="col-sm-10">
                                <input type="text" name="look" value="" placeholder="<?php echo $entry_look_id; ?>" id="input-look" class="form-control notification-autocomplete" />
                                <div id="look-look" class="well well-sm notification-notification" style="height: 150px; overflow: auto;">
                                    <?php foreach ($favorite_looks as $look) { ?>
                                        <div id="look-id<?php echo $look['look_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $look['name']; ?>
                                            <input type="hidden" name="look_ids[]" value="<?php echo $look['look_id']; ?>" />
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-tip"><span data-toggle="tooltip" title="<?php echo $entry_tip_id_info; ?>"><?php echo $entry_tip_id; ?></span></label>
                            <div class="col-sm-10">
                                <input type="text" name="tip" value="" placeholder="<?php echo $entry_tip_id; ?>" id="input-tip" class="form-control notification-autocomplete" />
                                <div id="tip-tip" class="well well-sm notification-notification" style="height: 150px; overflow: auto;">
                                    <?php foreach ($favorite_tips as $tip) { ?>
                                        <div id="tip-id<?php echo $tip['tip_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $tip['name']; ?>
                                            <input type="hidden" name="tip_ids[]" value="<?php echo $tip['tip_id']; ?>" />
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id='tab-view'>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td class="text-right">Age </td>
                                    <td class="text-right">Gender</td>
                                    <td class="text-right">Location</td>
                                    <td class="text-right">Language</td>
                                    <td class="text-right">Regency</td>
                                    <td class="text-right">Frequency </td>
                                    <td class="text-right">Life Time Value</td>
                                    <td class="text-right">Order value</td>
                                    <td class="text-right">Utm Meduim</td>
                                    <td class="text-right">Utm source</td>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td class="text-right"><?php
                                        if ($birthdate > 0) {
                                            $str = strtotime(date("Y-m-d ")) - (strtotime($birthdate));
                                            $birthdate = floor($str / (12 * 30 * 60 * 60 * 24));
                                            if ($birthdate >= 15 && $birthdate <= 24) {
                                                echo '15 to 24 years old';
                                            } elseif ($birthdate >= 25 && $birthdate <= 34) {
                                                echo '25 to 34 years old';
                                            } elseif ($birthdate > 35 && $birthdate <= 45) {
                                                echo "35 to 45 years old";
                                            } elseif ($birthdate > 45 && $birthdate <= 54) {
                                                echo "45 to 54 years old";
                                            } elseif ($birthdate >= 55) {
                                                echo "+55 years old";
                                            }
                                        } else {
                                            echo "null";
                                        }
                                        ?></td>
                                    <td class="text-right"><?php echo $gender; ?></td>
                                    <td class="text-right"> <?php
                                        if (isset($country_name)) {
                                            echo $country_name;
                                        }
                                        ?></td>
                                    <td class="text-right">
                                        <?php
                                        if ($language_id == 2) {
                                            echo "ARABIC";
                                        } else {
                                            echo "English";
                                        }
                                        ?>
                                    </td>
                                    <td class="text-right">
                                        <?php
                                        if (isset($order_email['date_added'])) {
                                            $str = strtotime(date("Y-m-d ")) - (strtotime($order_email['date_added']));
                                            $timenew = ceil($str / (30 * 60 * 60 * 24));

                                            if ($timenew <= 1) {
                                                echo 'Active';
                                            } elseif ($timenew > 1 && $timenew < 3) {
                                                echo "Hesitant";
                                            } elseif ($timenew >= 3 && $timenew < 6) {
                                                echo "At risk";
                                            } else {
                                                echo "Lost";
                                            }
                                        }
                                        ?>
                                    </td>

                                    <td class="text-right">
                                        <?php
                                        if (isset($order_email['num_order'])) {
                                            if ($order_email['num_order'] > 4) {
                                                echo 'Loyal';
                                            } elseif ($order_email['num_order'] > 2 && $order_email['num_order'] < 4) {
                                                echo "Repeat";
                                            } elseif ($order_email['num_order'] == 1) {
                                                echo "OneTime";
                                            } else {
                                                echo "Lead";
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td class="text-right">

                                        <?php
                                        if (isset($order_email['sumtotal'])) {
                                            if ($order_email['sumtotal'] >= 10000) {
                                                echo 'VIP';
                                            } elseif ($order_email['sumtotal'] >= 5000 && $order_email['sumtotal'] < 10000) {
                                                echo "Top";
                                            } elseif ($order_email['sumtotal'] >= 1000) {
                                                echo "Medium";
                                            } else {
                                                echo "Low";
                                            }
                                        }
                                        ?>

                                    </td>
                                    <td class="text-right">
                                        <?php
                                        if (isset($order_email['counttotal'])) {
                                            if ($order_email['counttotal'] >= 2500) {
                                                echo 'Spender';
                                            } elseif ($order_email['counttotal'] >= 500 && $order_email['counttotal'] < 2500) {
                                                echo "Average";
                                            } elseif ($order_email['counttotal'] < 500) {
                                                echo "Tight";
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td class="text-right">
                                        <?= $registration_utm_source ?>
                                    </td>
                                    <td class="text-right">
                                        <?= $registration_utm_meduim?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td class="text-left">Viewed </td>
                                    <td class="text-left">Purchased</td>
                                </tr>
                            </thead>

                            <tr>
                                <td class="text-left" style="vertical-align: top">
                                    <table>
                                        <tr>
                                            <th class="text-center" style="min-width: 100px">Product ID</th>
                                            <th class="text-center" style="min-width: 100px">Category ID</th>
                                            <th class="text-center" style="min-width: 100px">Product Name</th>
                                            <th class="text-center" style="min-width: 100px">Product View </th>
                                            <th class="text-center" style="min-width: 100px">Last View</th>
                                            <th class="text-center" style="min-width: 100px">UTM Source</th>
                                            <th class="text-center" style="min-width: 100px">UTM Meduim</th>

                                        </tr> <tr><td> &nbsp;</td></tr>

                                        <?php
                                        if ($data['cutomer_data'] != '') {
                                            foreach ($data['cutomer_data'] as $customer_value) {
                                                ?>
                                                <tr>
                                                    <?php echo '<td  class="text-left"> ' . $customer_value['product_id'] . ' </td><td class="text-left"> ' . $customer_value['view_category_id'] . ' </td><td class="text-left"> ' . $customer_value['product_name'] . '</td> <td class="text-center">' . $customer_value['product_count'] . ' </td><td class="text-center">' . $customer_value['date_added'] . ' </td><td class="text-center">' . $customer_value['utm_source'] . ' </td><td class="text-center">' . $customer_value['utm_meduim'] . ' </td>'; ?>
                                                    <?php
                                                }
                                            }
                                            ?>

                                    </table>
                                </td>
                                <td class="text-left" style="vertical-align: top">
                                    <table>
                                        <tr>
                                            <th class="text-center" style="min-width: 100px">Product ID</th>
                                            <th class="text-center" style="min-width: 100px">Category ID</th>
                                            <th class="text-center" style="min-width: 100px">Product Name</th>
                                        </tr><tr><td> &nbsp;</td></tr>
                                        <?php
                                        if ($data['cutomer_dataaaa'] != '') {
                                            foreach ($data['cutomer_dataaaa'] as $customer_prod) {
                                                ?>
                                                <tr>
                                                    <?php echo '<td  class="text-left">' . $customer_prod['customer_prod_id'] . ' </td><td class="text-left">  ' . $customer_prod['customer_cat_name'] . ' </td><td class="text-center"> ' . $customer_prod['customer_prod_name'] . ' </td>'; ?>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
$('select[name=\'customer_group_id\']').on('change', function () {
        $.ajax({
            url: 'index.php?route=customer/customer/customfield&token=<?php echo $token; ?>&customer_group_id=' + this.value,
            dataType: 'json',
            success: function (json) {
                $('.custom-field').hide();
                $('.custom-field').removeClass('required');

                for (i = 0; i < json.length; i++) {
                    custom_field = json[i];

                    $('.custom-field' + custom_field['custom_field_id']).show();

                    if (custom_field['required']) {
                        $('.custom-field' + custom_field['custom_field_id']).addClass('required');
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'customer_group_id\']').trigger('change');
    //--></script>
<script type="text/javascript"><!--
    var address_row = <?php echo $address_row; ?>;

    function addAddress() {
        html = '<div class="tab-pane" id="tab-address' + address_row + '">';
        html += '  <input type="hidden" name="address[' + address_row + '][address_id]" value="" />';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-firstname' + address_row + '"><?php echo $entry_firstname; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][firstname]" value="" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname' + address_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-lastname' + address_row + '"><?php echo $entry_lastname; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][lastname]" value="" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname' + address_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group">';
        html += '    <label class="col-sm-2 control-label" for="input-company' + address_row + '"><?php echo $entry_company; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][company]" value="" placeholder="<?php echo $entry_company; ?>" id="input-company' + address_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-address-1' + address_row + '"><?php echo $entry_address_1; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][address_1]" value="" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1' + address_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group">';
        html += '    <label class="col-sm-2 control-label" for="input-address-2' + address_row + '"><?php echo $entry_address_2; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][address_2]" value="" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2' + address_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-city' + address_row + '"><?php echo $entry_city; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][city]" value="" placeholder="<?php echo $entry_city; ?>" id="input-city' + address_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-postcode' + address_row + '"><?php echo $entry_postcode; ?></label>';
        html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][postcode]" value="" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode' + address_row + '" class="form-control" /></div>';
        html += '  </div>';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-country' + address_row + '"><?php echo $entry_country; ?></label>';
        html += '    <div class="col-sm-10"><select name="address[' + address_row + '][country_id]" id="input-country' + address_row + '" onchange="change_country(this, \'' + address_row + '\', \'0\');" class="form-control">';
        html += '         <option value=""><?php echo $text_select; ?></option>';
<?php foreach ($countries as $country) { ?>
            html += '         <option value="<?php echo $country['country_id']; ?>"><?php echo addslashes($country['name']); ?></option>';
<?php } ?>
        html += '      </select></div>';
        html += '  </div>';

        html += '  <div class="form-group required">';
        html += '    <label class="col-sm-2 control-label" for="input-zone' + address_row + '"><?php echo $entry_zone; ?></label>';
        html += '    <div class="col-sm-10"><select name="address[' + address_row + '][zone_id]" id="input-zone' + address_row + '" class="form-control"><option value=""><?php echo $text_none; ?></option></select></div>';
        html += '  </div>';

        // Custom Fields
<?php foreach ($custom_fields as $custom_field) { ?>
    <?php if ($custom_field['location'] == 'address') { ?>
        <?php if ($custom_field['type'] == 'select') { ?>

                    html += '  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
                    html += '  		<label class="col-sm-2 control-label" for="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
                    html += '  		<div class="col-sm-10">';
                    html += '  		  <select name="address[' + address_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" id="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">';
                    html += '  			<option value=""><?php echo $text_select; ?></option>';

            <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                        html += '  			<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo addslashes($custom_field_value['name']); ?></option>';
            <?php } ?>

                    html += '  		  </select>';
                    html += '  		</div>';
                    html += '  	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'radio') { ?>
                    html += '  	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>">';
                    html += '  		<label class="col-sm-2 control-label"><?php echo addslashes($custom_field['name']); ?></label>';
                    html += '  		<div class="col-sm-10">';
                    html += '  		  <div>';

            <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                        html += '  			<div class="radio"><label><input type="radio" name="address[' + address_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" /> <?php echo addslashes($custom_field_value['name']); ?></label></div>';
            <?php } ?>

                    html += '		  </div>';
                    html += '		</div>';
                    html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'checkbox') { ?>
                    html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
                    html += '		<label class="col-sm-2 control-label"><?php echo addslashes($custom_field['name']); ?></label>';
                    html += '		<div class="col-sm-10">';
                    html += '		  <div>';

            <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
                        html += '			<div class="checkbox"><label><input type="checkbox" name="address[<?php echo $address_row; ?>][custom_field][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" /> <?php echo addslashes($custom_field_value['name']); ?></label></div>';
            <?php } ?>

                    html += '		  </div>';
                    html += '		</div>';
                    html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'text') { ?>
                    html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
                    html += '		<label class="col-sm-2 control-label" for="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
                    html += '		<div class="col-sm-10">';
                    html += '		  <input type="text" name="address[' + address_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo addslashes($custom_field['value']); ?>" placeholder="<?php echo addslashes($custom_field['name']); ?>" id="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />';
                    html += '		</div>';
                    html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'textarea') { ?>
                    html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
                    html += '		<label class="col-sm-2 control-label" for="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
                    html += '		<div class="col-sm-10">';
                    html += '		  <textarea name="address[' + address_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo addslashes($custom_field['name']); ?>" id="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo addslashes($custom_field['value']); ?></textarea>';
                    html += '		</div>';
                    html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'file') { ?>
                    html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
                    html += '		<label class="col-sm-2 control-label"><?php echo addslashes($custom_field['name']); ?></label>';
                    html += '		<div class="col-sm-10">';
                    html += '		  <button type="button" id="button-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>';
                    html += '		  <input type="hidden" name="address[' + address_row + '][<?php echo $custom_field['custom_field_id']; ?>]" value="" id="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" />';
                    html += '		</div>';
                    html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'date') { ?>
                    html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
                    html += '		<label class="col-sm-2 control-label" for="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
                    html += '		<div class="col-sm-10">';
                    html += '		  <div class="input-group date"><input type="text" name="address[' + address_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo addslashes($custom_field['value']); ?>" placeholder="<?php echo addslashes($custom_field['name']); ?>" data-date-format="YYYY-MM-DD" id="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div>';
                    html += '		</div>';
                    html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'time') { ?>
                    html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
                    html += '		<label class="col-sm-2 control-label" for="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
                    html += '		<div class="col-sm-10">';
                    html += '		  <div class="input-group time"><input type="text" name="address[' + address_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo addslashes($custom_field['name']); ?>" data-date-format="HH:mm" id="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div>';
                    html += '		</div>';
                    html += '	  </div>';
        <?php } ?>

        <?php if ($custom_field['type'] == 'datetime') { ?>
                    html += '	  <div class="form-group custom-field custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">';
                    html += '		<label class="col-sm-2 control-label" for="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo addslashes($custom_field['name']); ?></label>';
                    html += '		<div class="col-sm-10">';
                    html += '		  <div class="input-group datetime"><input type="text" name="address[' + address_row + '][custom_field][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo addslashes($custom_field['value']); ?>" placeholder="<?php echo addslashes($custom_field['name']); ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-address' + address_row + '-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div>';
                    html += '		</div>';
                    html += '	  </div>';
        <?php } ?>

    <?php } ?>
<?php } ?>

        html += '  <div class="form-group">';
        html += '    <label class="col-sm-2 control-label"><?php echo $entry_default; ?></label>';
        html += '    <div class="col-sm-10"><label class="radio"><input type="radio" name="address[' + address_row + '][default]" value="1" /></label></div>';
        html += '  </div>';

        html += '</div>';

        $('#tab-general .tab-content').append(html);

        $('select[name=\'customer_group_id\']').trigger('change');

        $('select[name=\'address[' + address_row + '][country_id]\']').trigger('change');

        $('#address-add').before('<li><a href="#tab-address' + address_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'#address a:first\').tab(\'show\'); $(\'a[href=\\\'#tab-address' + address_row + '\\\']\').parent().remove(); $(\'#tab-address' + address_row + '\').remove();"></i> <?php echo $tab_address; ?> ' + address_row + '</a></li>');

        $('#address a[href=\'#tab-address' + address_row + '\']').tab('show');

        $('.date').datetimepicker({
            pickTime: false
        });

        $('.datetime').datetimepicker({
            pickDate: true,
            pickTime: true
        });

        $('.time').datetimepicker({
            pickDate: false
        });

        $('#tab-address' + address_row + ' .form-group[data-sort]').detach().each(function () {
            if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#tab-address' + address_row + ' .form-group').length) {
                $('#tab-address' + address_row + ' .form-group').eq($(this).attr('data-sort')).before(this);
            }

            if ($(this).attr('data-sort') > $('#tab-address' + address_row + ' .form-group').length) {
                $('#tab-address' + address_row + ' .form-group:last').after(this);
            }

            if ($(this).attr('data-sort') < -$('#tab-address' + address_row + ' .form-group').length) {
                $('#tab-address' + address_row + ' .form-group:first').before(this);
            }
        });

        address_row++;
    }
    //--></script>
<script type="text/javascript"><!--
    function change_country(element, index, zone_id) {
        $.ajax({
            url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + element.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'address[' + index + '][country_id]\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function () {
                $('.fa-spin').remove();
            },
            success: function (json) {
                if (json['postcode_required'] == '1') {
                    $('input[name=\'address[' + index + '][postcode]\']').parent().parent().addClass('required');
                } else {
                    $('input[name=\'address[' + index + '][postcode]\']').parent().parent().removeClass('required');
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] && json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == zone_id) {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0"><?php echo $text_none; ?></option>';
                }

                $('select[name=\'address[' + index + '][zone_id]\']').html(html);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

    $('select[name$=\'[country_id]\']').trigger('change');
    //--></script>
<script type="text/javascript"><!--
    $('#history').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();

        $('#history').load(this.href);
    });

    $('#history').load('index.php?route=customer/customer/history&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

    $('#button-history').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: 'index.php?route=customer/customer/addhistory&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>',
            type: 'post',
            dataType: 'json',
            data: 'comment=' + encodeURIComponent($('#tab-history textarea[name=\'comment\']').val()),
            beforeSend: function () {
                $('#button-history').button('loading');
            },
            complete: function () {
                $('#button-history').button('reset');
            },
            success: function (json) {
                $('.alert').remove();

                if (json['error']) {
                    $('#tab-history').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');
                }

                if (json['success']) {
                    $('#tab-history').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');

                    $('#history').load('index.php?route=customer/customer/history&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

                    $('#tab-history textarea[name=\'comment\']').val('');
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#transaction').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();

        $('#transaction').load(this.href);
    });

    $('#transaction').load('index.php?route=customer/customer/transaction&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

    $('#button-transaction').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: 'index.php?route=customer/customer/addtransaction&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>',
            type: 'post',
            dataType: 'json',
            data: 'description=' + encodeURIComponent($('#tab-transaction input[name=\'description\']').val()) + '&amount=' + encodeURIComponent($('#tab-transaction input[name=\'amount\']').val()) + '&pricecode=' + encodeURIComponent($('#tab-transaction select[name=\'pricecode\']').val()),
            beforeSend: function () {
                $('#button-transaction').button('loading');
            },
            complete: function () {
                $('#button-transaction').button('reset');
            },
            success: function (json) {
                $('.alert').remove();

                if (json['error']) {
                    $('#tab-transaction').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');
                }

                if (json['success']) {
                    $('#tab-transaction').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');

                    $('#transaction').load('index.php?route=customer/customer/transaction&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

                    $('#tab-transaction input[name=\'amount\']').val('');
                    $('#tab-transaction input[name=\'description\']').val('');
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#reward').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();

        $('#reward').load(this.href);
    });

    $('#reward').load('index.php?route=customer/customer/reward&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

    $('#button-reward').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: 'index.php?route=customer/customer/addreward&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>',
            type: 'post',
            dataType: 'json',
            data: 'description=' + encodeURIComponent($('#tab-reward input[name=\'description\']').val()) + '&points=' + encodeURIComponent($('#tab-reward input[name=\'points\']').val()),
            beforeSend: function () {
                $('#button-reward').button('loading');
            },
            complete: function () {
                $('#button-reward').button('reset');
            },
            success: function (json) {
                $('.alert').remove();

                if (json['error']) {
                    $('#tab-reward').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');
                }

                if (json['success']) {
                    $('#tab-reward').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');

                    $('#reward').load('index.php?route=customer/customer/reward&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

                    $('#tab-reward input[name=\'points\']').val('');
                    $('#tab-reward input[name=\'description\']').val('');
                }
            }
        });
    });

    $('#ip').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();

        $('#ip').load(this.href);
    });

    $('#ip').load('index.php?route=customer/customer/ip&token=<?php echo $token; ?>&customer_id=<?php echo $customer_id; ?>');

    $('#content').delegate('button[id^=\'button-custom-field\'], button[id^=\'button-address\']', 'click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload/upload&token=<?php echo $token; ?>',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $(node).parent().find('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input[type=\'hidden\']').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);
                        }

                        if (json['code']) {
                            $(node).parent().find('input[type=\'hidden\']').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });

    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.notification-notification').delegate('.fa-minus-circle', 'click', function () {
        $(this).parent().remove();
    });
    // Sort the custom fields
<?php $address_row = 1; ?>
<?php foreach ($addresses as $address) { ?>
        $('#tab-address<?php echo $address_row ?> .form-group[data-sort]').detach().each(function () {
            if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#tab-address<?php echo $address_row ?> .form-group').length) {
                $('#tab-address<?php echo $address_row ?> .form-group').eq($(this).attr('data-sort')).before(this);
            }

            if ($(this).attr('data-sort') > $('#tab-address<?php echo $address_row ?> .form-group').length) {
                $('#tab-address<?php echo $address_row ?> .form-group:last').after(this);
            }

            if ($(this).attr('data-sort') < -$('#tab-address<?php echo $address_row ?> .form-group').length) {
                $('#tab-address<?php echo $address_row ?> .form-group:first').before(this);
            }
        });
    <?php $address_row++; ?>
<?php } ?>


<?php foreach ($addresses as $address) { ?>
        $('#tab-customer .form-group[data-sort]').detach().each(function () {
            if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#tab-customer .form-group').length) {
                $('#tab-customer .form-group').eq($(this).attr('data-sort')).before(this);
            }

            if ($(this).attr('data-sort') > $('#tab-customer .form-group').length) {
                $('#tab-customer .form-group:last').after(this);
            }

            if ($(this).attr('data-sort') < -$('#tab-customer .form-group').length) {
                $('#tab-customer .form-group:first').before(this);
            }
        });
<?php } ?>

    var urls = {
        'category': 'index.php?route=catalog/category/autocompletesecondlevel&token=<?php echo $token; ?>&filter_name=',
        'brand': 'index.php?route=mall/brand/autocomplete&token=<?php echo $token; ?>&filter_keyword=',
        'seller': 'index.php?route=customerpartner/partner/autocomplete&token=<?php echo $token; ?>&filter_name=',
        'product': 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=',
        'look': 'index.php?route=mall/look/autocomplete&token=<?php echo $token; ?>&filter_name=',
        'tip': 'index.php?route=mall/tip/autocomplete&token=<?php echo $token; ?>&filter_name='
    };

    $(".notification-autocomplete").autocomplete({
        'source': function (request, response) {

            var input_name = $(this).attr('name');
            var path_url = urls[input_name] + encodeURIComponent(request);
            var value_item = 'id';
            if (input_name != "brand") {
                value_item = input_name + "_id";
            }
            $.ajax({
                url: path_url,
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item[value_item]
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            var input_name = $(this).attr('name');
            $('input[name=' + input_name + ']').val('');

            $('#' + input_name + '-' + input_name + item['value']).remove();

            $('#' + input_name + '-' + input_name).append('<div id="' + input_name + '-id' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name=' + input_name + '_ids[]" value="' + item['value'] + '" /></div>');
        }
    });
    //--></script>
<script type="text/javascript"><!--
    var old_cur_code = '<?php echo $pricecode; ?>';
    function convertinto(price, from, to) {
        var f = 1;
        var t = 1;
        for (var i = 0; i < window.APP.currencies.length; i++) {
            if (window.APP.currencies[i].code == from) {
                f = window.APP.currencies[i].value;
            }
            if (window.APP.currencies[i].code == to) {
                t = window.APP.currencies[i].value;
            }
        }
        var value = parseFloat(price * t / f).toFixed(2);
        return value;
    }
    function changecur() {
        // parseFloat("1.5551").toFixed(2); 
        var new_price_code = document.getElementById('input-pricecode').value;
        if (new_price_code != old_cur_code) {
            if (document.getElementById('input-amount').value) {
                var price = parseFloat(document.getElementById('input-amount').value).toFixed(2);
                document.getElementById('input-amount').value = convertinto(price, old_cur_code, new_price_code);
            } else {
                document.getElementById('input-amount').value = convertinto(0, old_cur_code, new_price_code);
            }
            old_cur_code = new_price_code;

        }
    }
    //--></script>
<?php echo $footer; ?>
