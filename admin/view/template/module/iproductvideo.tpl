<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content" class="iProductVideo">
	<script type="text/javascript">
		NProgress.configure({
			showSpinner: false,
			ease: 'ease',
			speed: 500,
			trickleRate: 0.2,
			trickleSpeed: 200 
		});
	</script>
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($success) { ?>
			<div class="alert alert-success slidesUp"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<script type="text/javascript">
				$('.slidesUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);
			</script>
		<?php } ?>

		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> Edit <?php echo $heading_title; ?></h3>
			</div>
			<div class="panel-body">
				<?php
					$dirname = DIR_APPLICATION . 'view/template/module/iproductvideo/';
					
					$tab_files = scandir($dirname);
					$tabs = array();
					foreach ($tab_files as $key => $file) {
						if (strpos($file,'tab_') !== false) {
							$tabs[] = array(
								'file' => $dirname.$file,
								'name' => ucwords(str_replace('.php','',str_replace('_',' ',str_replace('tab_','',$file))))
							);
						} 
					}
					foreach ($tabs as $key => $tab) {
						if ($tab['name'] == 'Support' && $key < count($tabs) - 1) {
							$temp = $tabs[count($tabs) - 1];
							$tabs[count($tabs) - 1] = $tab;
							$tabs[$key] = $temp;
							break;
						}
					}
				?>
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
					<div class="tabbable">
						<div class="tab-navigation">
							<ul class="nav nav-tabs mainMenuTabs">
								<?php $i=0; foreach($tabs as $tab): ?>
								<li <?php echo ($i == 0) ? 'class="active"' : ''; ?>><a href="#tab_<?php echo str_replace(' ', '_', $tab['name']); ?>" data-toggle="tab"><?php echo $tab['name']; ?></a></li>
								<?php $i++; endforeach; ?>
							</ul>
							<div class="tab-buttons">
								<a onclick="$('#form').submit();" class="btn btn-primary save-changes" title="<?php echo $button_save; ?>"><i class="fa fa-check"></i>&nbsp;&nbsp;<?php echo $button_save; ?></a>
								<a onclick="<?php echo $cancel; ?>" class="btn btn-default" title="<?php echo $button_cancel; ?>"><?php echo $button_cancel; ?></a>
							</div>
						</div>
						<div class="tab-content">
							<?php $i=0; foreach($tabs as $tab): ?>
							<div id="tab_<?php echo str_replace(' ', '_', $tab['name']); ?>" class="tab-pane <?php echo ($i == 0) ? 'active' : ''; ?>">
								<?php require_once($tab['file']); ?>
							</div>
							<?php $i++; endforeach; ?>
						</div>
						<!-- /.tab-content --> 
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$(document).ready(function(e) {
		if (window.localStorage && window.localStorage['currentTab']) {
			$('.mainMenuTabs a[href=' + window.localStorage['currentTab'] + ']').trigger('click');  
		}
		
		if (window.localStorage && window.localStorage['currentStoreTab']) {
			$('.storeTabs a[href=' + window.localStorage['currentStoreTab'] + ']').trigger('click');  
		}
		
		if (window.localStorage && window.localStorage['currentSubTab']) {
			if ($('a[href=' + window.localStorage['currentSubTab'] + ']').length != 0) {
				$('a[href=' + window.localStorage['currentSubTab'] + ']').trigger('click');
			}
		}
		
		$('.mainMenuTabs a[data-toggle="tab"]').click(function() {
			if (window.localStorage) {
				window.localStorage['currentTab'] = $(this).attr('href');
			}
		});
		
		$('.storeTabs a[data-toggle="pill"]').click(function() {
			if (window.localStorage) {
				window.localStorage['currentStoreTab'] = $(this).attr('href');
			}
		});
		
		$('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"])').click(function() {
			if (window.localStorage) {
				window.localStorage['currentSubTab'] = $(this).attr('href');
			}
		});
		
		$(document).on('click', '.iproductvideo-tab-menu > .btn-group > label', function(e) {
	        e.preventDefault();
			
	        var index = $(this).index();
			var parent = $(this).parents('.iproductvideo-tab-container');
			
	        $(".iproductvideo-tab > .iproductvideo-tab-content", parent).removeClass("active");
	        $(".iproductvideo-tab > .iproductvideo-tab-content", parent).eq(index).addClass("active");
	    });
	    
	    $(".iproductvideo-tab-menu > .btn-group > label.active").click();
	});
	</script> 

	<!-- Modal -->
	<div id="help-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="HelpModal" aria-hidden="true">
		<div class="modal-dialog modal-lg">
	    <div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">How to increase my file upload limit?</h4>
			</div>
			<div class="modal-body">
				<p>The maximum upload size is predefined for your server and is different for each hosting provider. If the limit is too low, you can either contact your hosting provider to increase it, or you can try to do it yourself by following one of the methods below.</p>
				<h4>Method 1: Modify your php.ini file with the following entries:</h4>
				<pre>
memory_limit = 256M
upload_max_filesize = 200M
post_max_size = 201M
</pre><br />
			<h4>Method 2: In your /admin/ folder of OpenCart, create an .htaccess file with the following entries:</h4>
			<pre>
php_value memory_limit 256M
php_value upload_max_filesize 200M
php_value post_max_size 201M
</pre>
				<p>You can find additional information in <a href="http://php.net/manual/en/ini.core.php" target="_blank">php.net</a></p>
			</div>
	    </div>
	  </div>
	</div>
	<?php if ($warning_modal) { ?>
	<script type="text/javascript">
	$(document).ready(function(e) {
		$('#warning-modal').modal('show')
	});
	</script>
	<div id="warning-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Warning!" aria-hidden="true" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Warning! iProductVideo is not able to run on your system.</h4>
				</div>
				<div class="modal-body">
					<?php foreach ($warning_modal['errors'] as $error) { ?>
						<?php echo $error; ?><br />
					<?php } ?>
					<hr />
					The above errors are caused by improper file/directory permissions. You can try contacting your hosting provider to set the permissions for you or open a support ticket at <a href="http://isenselabs.com" target="_blank">iSenseLabs</a>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<?php echo $footer; ?>