<div class="tab-pane fade in active">
  <div class="row">
    <div class="col-md-3">
      <h5><strong>Type of discount:</strong></h5>
      <span class="help"><i class="fa fa-info-circle"></i>&nbsp;If you choose the option 'No discount', you will have to remove the following codes from the mail template: {discount_code}, {discount_value}, {total_amount} and {date_end}.</span>
    </div>
    <div class="col-md-3">
      <select name="<?php echo $follow_name; ?>[DiscountType]" class="discountTypeSelect form-control">
        <option value="P" <?php if(!empty($follow_data['DiscountType']) && $follow_data['DiscountType'] == "P") echo "selected"; ?>>Percentage</option>
        <option value="F" <?php if(!empty($follow_data['DiscountType']) && $follow_data['DiscountType'] == "F") echo "selected"; ?>>Fixed amount</option>
        <option value="N" <?php if(empty($follow_data['DiscountType']) || $follow_data['DiscountType'] == "N") echo "selected"; ?>>No discount</option>
      </select>
    </div>
  </div>
  <br />
  <div class="discountSettings">
    <div class="row">
      <div class="col-md-3">
        <h5><strong><span class="required">* </span>Discount:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Enter the discount percent or value.</span>
      </div>
      <div class="col-md-3">
        <div class="input-group">
          <input type="text" class="form-control" name="<?php echo $follow_name; ?>[Discount]" value="<?php if(!empty($follow_data['Discount'])) echo $follow_data['Discount']; else echo '10'; ?>">
          <span class="input-group-addon">
            <span style="display:none;" id="currencyAddon"><?php echo $currency; ?></span><span style="display:none;" id="percentageAddon">%</span>
          </span>
        </div>
      </div>
    </div>
    <br />
    <div class="row">
      <div class="col-md-3">
        <h5><strong><span class="required">* </span>Total amount:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;The total amount that must reached before the coupon is valid.</span>
      </div>
      <div class="col-md-3">
        <div class="input-group">
          <input type="text" class="form-control" name="<?php echo $follow_name; ?>[TotalAmount]" value="<?php if(!empty($follow_data['TotalAmount'])) echo $follow_data['TotalAmount']; else echo '20'; ?>">
          <span class="input-group-addon"><?php echo $currency ?></span>
        </div>
      </div>
    </div>
    <br />
    <div class="row">
      <div class="col-md-3">
        <h5><strong><span class="required">* </span>Discount validity:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Define how many days the discount code will be active after sending the reminder.</span>
      </div>
      <div class="col-md-3">
        <div class="input-group">
          <input type="text" class="form-control" value="<?php if(!empty($follow_data['DiscountValidity'])) echo (int)$follow_data['DiscountValidity']; else echo 7; ?>" name="<?php echo $follow_name; ?>[DiscountValidity]">
          <span class="input-group-addon">days</span>
        </div>
      </div>
    </div>
  </div>
</div>