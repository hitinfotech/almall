<div class="tab-pane tab-pane fade in active">
    <div class="row">
        <div class="col-md-3">
            <h5><strong>Order status:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Define the order status for the selected follow up.</span>
        </div>
        <div class="col-md-3">
            <select required class="form-control" name="<?php echo $follow_name; ?>[OrderStatusID]">
                <option disabled selected value="">Select order status</option>
                <?php if (!empty($orderStatuses)) { foreach ($orderStatuses as $orderStatus) {  ?>
                <option value="<?php echo $orderStatus['order_status_id']; ?>"
                    <?php if(!empty($follow_data['OrderStatusID']) && ($orderStatus['order_status_id'] == $follow_data['OrderStatusID'])) {
                    echo 'selected="selected"';
                    } else if(empty($follow_data['OrderStatusID']) && $orderStatus['name'] == 'Complete') {
                    echo 'selected="selected"';
                    } ?>>
                    <?php echo $orderStatus['name']; ?>
                </option>
                <?php } }?>
            </select>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-3">
            <h5><strong>Order total:</strong></h5>
            <span class="help">
                <i class="fa fa-info-circle"></i>&nbsp;The total amount that must be reached for the selected follow up.
            </div>
            <div class="col-md-3">
                <div class="input-group">
                    <input type="text" class="form-control" name="<?php echo $follow_name; ?>[OrderTotal]" value="<?php if(!empty($follow_data['OrderTotal'])) echo $follow_data['OrderTotal']; else echo '0'; ?>">
                    <span class="input-group-addon"><?php echo $currency ?></span>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-3">
                <h5><strong>Message delay:</strong></h5>
                <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Define after how many days to send the email.<br /><br />
                <strong>NOTE: </strong>If you set the delay to 0, the message will be sent immediately after you change the order status.</span></span>
            </div>
            <div class="col-md-3">
                <div class="input-group">
                    <input type="text" class="form-control" name="<?php echo $follow_name; ?>[Delay]" value="<?php if(isset($follow_data['Delay'])) echo $follow_data['Delay']; else echo '15'; ?>" />
                    <span class="input-group-addon">days</span>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-3">
                <h5><strong>Products:</strong></h5>
                <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Send only to customers who have ordered products in the list. (Autocomplete)<br /><br /></span>
            </div>
            <div class="col-md-3">
                <input type="text" name="productsInput" value="" class="form-control" style="margin-bottom:12px;" />
                <div id="followup-<?php echo $followup['id']; ?>-prbox" class="scrollbox">
                    <?php $class1 = 'odd'; ?>
                    <?php if (!empty($follow_data['products'])) { ?>
                    <?php foreach ($follow_data['products'] as $pr) {
                    $class1 = ($class1 == 'even' ? 'odd' : 'even');
                    $product = $catalog_product->getProduct($pr); ?>
                    <div id="followup-<?php echo $followup['id']; ?>-prbox_<?php echo $pr; ?>" class="<?php echo $class1; ?>"><?php echo $product['name']; ?><i class="fa fa-minus-circle removeIcon"></i>
                        <input type="hidden" name="<?php echo $follow_name; ?>[products][]" value="<?php echo $pr; ?>" />
                    </div>
                    <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="row">
            <div class="col-md-4">
                <h5><span class="required">* </span><strong>Customer Groups</strong></h5>
                <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Choose the customer group you want to apply the notification for. At least one customer group should be selected!</span>
            </div>
            <div class="col-md-3">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="<?php echo $follow_name ?>[customerGroups][0]" <?php echo isset($follow_data['customerGroups'][0]) && ($follow_data['customerGroups'][0] == 'on') ? 'checked="checked"' : ''; ?>/> Guest
                    </label>
                </div>
                <?php foreach($customerGroups as $customerGroup) { ?>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="<?php echo $follow_name ?>[customerGroups][<?php echo $customerGroup['customer_group_id']?>]" <?php echo isset($follow_data['customerGroups'][$customerGroup['customer_group_id']]) && ($follow_data['customerGroups'][$customerGroup['customer_group_id']] == 'on') ? 'checked="checked"' : ''; ?>/> <?php echo $customerGroup['name'] ?>
                    </label>
                </div>
                <?php } ?>
            </div>
        </div>
        <br />
        
    </div>