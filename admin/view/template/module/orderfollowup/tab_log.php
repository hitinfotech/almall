<div id="LogWrapper<?php echo $store['store_id']; ?>"> </div>
<script>
    $(document).ready(function(){
         $.ajax({
            url: "index.php?route=<?php echo $module_path; ?>/getlog&token=<?php echo $token; ?>&page=1&store_id=<?php echo $store['store_id']; ?>",
            type: 'get',
            dataType: 'html',
            success: function(data) {		
                $("#LogWrapper<?php echo $store['store_id']; ?>").html(data);
            }
    
         });
    });
	$('#log').delegate('button[data-event-id]','click', function(e){
		e.preventDefault();
		e.stopPropagation();
		$('#logModalBody iframe').attr('src', $(this).attr('href'));
		$('#logModal').modal({backdrop: true});
	});
</script>