<table id="LogWrapper<?php echo $store_id; ?>" class="table table-bordered table-hover" width="100%" >
      <thead>
        <tr class="table-header">
          <td width="2%"><input type="checkbox" onclick="$('input[class*=\'select-to-remove\']').attr('checked', this.checked);" /></td>
          <td class="left" width="14%"><strong>Order ID</strong></td>
          <td class="left" width="19%"><strong>Customer</strong></td>
		  <td class="left" width="18%"><strong>Email</strong></td>
          <td class="left" width="14%"><strong>FollowUp ID</strong></td>
          <td class="left" width="14%"><strong>Date</strong></td>
          <td class="left" width="14%"><strong>Message</strong></td>
        </tr>
      </thead>
	<?php if (!empty($sources)) { ?>
		<?php $i=0; foreach ($sources as $src) { ?>
              <tbody>
				<tr>
        <td><input class="select-to-remove" type="checkbox" data-followup-logid="<?php echo $src['id'] ?>"></td>
                  <td class="left">
                        <button type="button" class="btn btn-default btn-sm disabled" tabindex="-1"><?php echo $src['order_id']; ?></button>
                        <a href="index.php?route=sale/order/info&token=<?php echo $token; ?>&order_id=<?php echo $src['order_id']; ?>" target="_blank" class="btn btn-default btn-sm btn-info">Order details</a>
                  </td>
                  <td class="left">
                  	<?php $order_data = $sale_order->getOrder($src['order_id']); ?>
                    <?php echo (isset($order_data['firstname'])) ? $order_data['firstname'] : '(not provided)'; ?> <?php echo (isset($order_data['lastname'])) ? $order_data['lastname'] : ''; ?>
                  </td>
				<td class="left">
                  	<?php echo (isset($order_data['email'])) ? $order_data['email'] : '(not provided)'; ?>
                  </td>
                  <td class="left">
                        <button type="button" class="btn btn-default btn-sm disabled" tabindex="-1"><?php echo $src['followup_id']; ?></button>
                        <button type="button" onClick="showFollowUp(<?php echo $src['followup_id']; ?>);" class="btn btn-default btn-sm btn-info" tabindex="-1">View more</button>
                  </td>
                  <td class="left">
                  	<?php echo $src['date']; ?>
                  </td>
                  <td class="left">
                  	<button data-event-id="<?php echo $src['id']; ?>" href="<?php echo $url_link->link($module_path.'/getlogmessage', 'token=' . $token."&id=".$src['id'], 'SSL'); ?>" class="btn btn-info btn-sm">View message</button>
                  </td>
                </tr>
              </tbody>
        <?php } ?>
	<?php } ?>
    <tfoot>
    	<tr>
        	<td colspan="10">
                <div class="row">
                  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
        	</td>
        </tr>
    </tfoot>
</table>
<div class="row">
  <div class="col-md-12">
      <a id="btn-remove" class="btn btn-danger pull-right">Remove</a>
  </div>
</div>
<script>
function showFollowUp(id) {
	$('.mainMenuTabs a[href="#controlpanel"]').click();	
	$('.tabs-left a[href="#followup_'+ id +'"]').click();
}
$(document).ready(function(){
	$('#LogWrapper<?php echo $store_id; ?> .pagination a').click(function(e){
		e.preventDefault();
		$.ajax({
			url: this.href,
			type: 'get',
			dataType: 'html',
			success: function(data) {				
				$("#LogWrapper<?php echo $store_id; ?>").html(data);
			}
		});
	});		 
});

//Remove log entries
$('#btn-remove').on('click', function(){
    var selected_log_entries = [];
    var store_id = <?php echo $store_id ?>;
    $("input:checkbox[class=select-to-remove]:checked").each(function(){
        selected_log_entries.push($(this).attr('data-followup-logid') );
    });
    $.ajax({
      url: 'index.php?route=<?php echo $module_path; ?>/deleteLogEntry&token=<?php echo $token; ?>',
      type:'post',
      data: {selected_log_entries: selected_log_entries, store_id:store_id},
      success:function(){
        location.reload();
      }
    });    
});
</script>