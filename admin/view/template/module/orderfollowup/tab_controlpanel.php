<div class="container-fluid">
    <div class="row">
      <div class="col-md-2">
        <h5><strong><span class="required">* </span>Module status:</strong></h5>
        <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Enable or disable the module.</span>
      </div>
      <div class="col-md-2">
        <select id="Checker" name="<?php echo $moduleName; ?>[Enabled]" class="form-control">
              <option value="yes" <?php echo (!empty($moduleData['Enabled']) && $moduleData['Enabled'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
              <option value="no"  <?php echo (empty($moduleData['Enabled']) || $moduleData['Enabled']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
        </select>
      </div>
    </div>
    <br />
</div>
<div class="tabbable tabs-left" id="followup_tabs">
	<div class="container-fluid">
        <div class="row">
          <div class="col-md-2">
            <h5><strong>Keep log:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Keep log of the sent emails.</span>
          </div>
          <div class="col-md-2">
            <select id="LogChecker" name="<?php echo $moduleName; ?>[Log]" class="form-control">
                  <option value="yes" <?php echo (!empty($moduleData['Log']) && $moduleData['Log'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="no"  <?php echo (empty($moduleData['Log']) || $moduleData['Log']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
          </div>
          <div class="col-md-2">
				<button class="btn btn-primary cronModal">How to set up the cron job?</button>
          </div>
        </div>
    </div>
    <br />
    <br />
    <ul class="nav nav-tabs followup-list" id="followUpTabs">
        <li class="static"><a class="addNewFollowUp"><i class="fa fa-plus"></i> Add New FollowUp</a></li>
        <?php if (isset($moduleData['FollowUp'])) { ?>
            <?php foreach ($moduleData['FollowUp'] as $followup) { ?>
            <li><a href="#followup_<?php echo $followup['id']; ?>" data-toggle="tab" data-followup-id="<?php echo $followup['id']; ?>"><i class="fa fa-pencil-square-o"></i> <?php echo (isset($followup['Name']) && !empty($followup['Name'])) ? $followup['Name'] : 'FollowUp '.$followup['id']; ?> <i class="fa fa-minus-circle removeFollowUp"></i>
                <input type="hidden" name="<?php echo $moduleName; ?>[FollowUp][<?php echo $followup['id']; ?>][id]" value="<?php echo $followup['id']; ?>" />
                </a> </li>
            <?php } ?>
        <?php } ?>
    </ul>
    <div class="tab-content followup-settings">
        <?php if (isset($moduleData['FollowUp'])) { ?>
            <?php foreach ($moduleData['FollowUp'] as $followup) { 
                require(DIR_APPLICATION.'view/template/module/'.$moduleNameSmall.'/tab_followuptab.tpl');
            } ?>
        <?php } ?>
    </div>
</div>
        
<script type="text/javascript"><!--
// Add FollowUp
function addNewFollowUp() {
	count = $('.followup-list li:last-child > a').data('followup-id') + 1 || 1;
	var ajax_data = {};
	ajax_data.token = '<?php echo $token; ?>';
	ajax_data.store_id = '<?php echo $store['store_id']; ?>';
	ajax_data.followup_id = count;

	$.ajax({
		url: 'index.php?route=<?php echo $module_path; ?>/get_followup_settings',
		data: ajax_data,
		dataType: 'html',
		beforeSend: function() {
			NProgress.start();
		},
		success: function(settings_html) {
			$('.followup-settings').append(settings_html);
			
			if (count == 1) { $('a[href="#followup_'+ count +'"]').tab('show'); }
			tpl 	= '<li>';
			tpl 	+= '<a href="#followup_'+ count +'" data-toggle="tab" data-followup-id="'+ count +'">';
			tpl 	+= '<i class="fa fa-pencil-square-o"></i> FollowUp '+ count;
			tpl 	+= '<i class="fa fa-minus-circle removeFollowUp"></i>';
			tpl 	+= '<input type="hidden" name="<?php echo $moduleName; ?>[FollowUp]['+ count +'][id]" value="'+ count +'"/>';
			tpl 	+= '</a>';
			tpl	+= '</li>';
			
			$('.followup-list').append(tpl);
			
			NProgress.done();
			$('.followup-list').children().last().children('a').trigger('click');
			window.localStorage['currentSubTab'] = $('.followup-list').children().last().children('a').attr('href');
		}
	});
}

// Remove FollowUp
function removeFollowUp() {
	tab_link = $(event.target).parent();
	tab_pane_id = tab_link.attr('href');
	
	var confirmRemove = confirm('Are you sure you want to remove ' + tab_link.text().trim() + '?');
	
	if (confirmRemove == true) {
		tab_link.parent().remove();
		$(tab_pane_id).remove();
		
		if ($('.followup-list').children().length > 1) {
			$('.followup-list > li:nth-child(2) a').tab('show');
			window.localStorage['currentSubTab'] = $('.followup-list > li:nth-child(2) a').attr('href');
		}
	}
}

// Events for the Add and Remove buttons
$(document).ready(function() {
	// Add New Label
	$('.addNewFollowUp').click(function(e) { addNewFollowUp(); });
	// Remove Label
	$('.followup-list').delegate('.removeFollowUp', 'click', function(e) { removeFollowUp(); });
});

// Show the EDITOR
<?php 
if (isset($moduleData['FollowUp'])) { 
	foreach ($moduleData['FollowUp'] as $followup) {
		foreach ($languages as $language) { ?>
			$('#message_<?php echo $followup['id']; ?>_<?php echo $language['language_id']; ?>').summernote({
				height: 320
			});
<?php	}
	}
} ?>

$('.cronModal').on('click', function(e){
	e.preventDefault();
	e.stopPropagation();
	$('.cronjobs').modal({backdrop: true});
});

// Selectors for discount
function selectorsForDiscount() {
	$('.discountTypeSelect').each(function() {
		//debugger;
		if ($(this).val() == 'P'){
			$(this).parents('.followups').find('#percentageAddon').show();
		} else {
			$(this).parents('.followups').find('#currencyAddon').show();
		}
		if ($(this).val() == 'N'){
			$(this).parents('.followups').find('.discountSettings').hide();
		} else {
			$(this).parents('.followups').find('.discountSettings').show();
		}
	});
	
	
	$('.discountTypeSelect').on('change', function(e){ 
		if($(this).val() == 'P') {
			$(this).parents('.followups').find('#percentageAddon').show();
			$(this).parents('.followups').find('#currencyAddon').hide();
		} else {
			$(this).parents('.followups').find('#currencyAddon').show();
			$(this).parents('.followups').find('#percentageAddon').hide();
		}	
		if($(this).val() == 'N') {
			$(this).parents('.followups').find('.discountSettings').hide(300);
		} else {
			$(this).parents('.followups').find('.discountSettings').show(300);
		}
	});
}

// Add Products
var initializeAutocomplete = function () {
	$('input[name=\'productsInput\']').autocomplete({
		delay: 500,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {		
					response($.map(json, function(item) {
						return {
							label: item['name'],
							value: item['product_id'],
							price: item['price'],
							special: item['special']
						}
					}));
				}
			});
		}, 
		select: function(item) {
			var bundle_id = $(this).parents('.followups.active').find('.followup_id').val();
			
			$(this).parent().find('#followup-' + bundle_id + '-prbox_' + item['value']).remove();
			$(this).parent().find('.scrollbox').append('<div id="followup-' + bundle_id + '-prbox_' + item['value'] + '">' + item['label'] + '<i class="fa fa-minus-circle removeIcon"></i><input type="hidden" name="OrderFollowUp[FollowUp][' + bundle_id + '][products][]" value="' + item['value'] +'" /></div>');
			$(this).parent().find('#followup-' + bundle_id + '-prbox div:odd').attr('class', 'odd');
			$(this).parent().find('#followup-' + bundle_id + '-prbox div:even').attr('class', 'even');		
			return false;
		},
		focus: function(event, ui) {
		  return false;
	   }
	});
				
	$('.followup-settings').delegate('.scrollbox div .removeIcon', 'click', function() {
		var bundle_id = $(this).parents('.followups.active').find('.followup_id').val();
		$(this).parent().remove();
		$(this).parent().find('#followup-' + bundle_id + '-prbox div:odd').attr('class', 'odd');
		$(this).parent().find('#followup-' + bundle_id + '-prbox div:even').attr('class', 'even');	
	});
}

// Initialize selector for discount
$(function() {
	initializeAutocomplete();
	selectorsForDiscount();
});
</script>
<script>
	$('#followUpTabs a:first').tab('show'); // Select first tab
if (window.localStorage && window.localStorage['currentTab']) {
	$('.followup-list a[href="'+window.localStorage['currentTab']+'"]').tab('show');
}
if (window.localStorage && window.localStorage['currentSubTab']) {
	$('a[href="'+window.localStorage['currentSubTab']+'"]').tab('show');
}
$('.fadeInOnLoad').css('visibility','visible');
$('.followup-list a[data-toggle="tab"]').click(function() {
	if (window.localStorage) {
		window.localStorage['currentTab'] = $(this).attr('href');
	}
});
$('a[data-toggle="tab"]:not(.followup-list a[data-toggle="tab"], .followup_tabs a[data-toggle="tab"])').click(function() {
	if (window.localStorage) {
		window.localStorage['currentSubTab'] = $(this).attr('href');
	}
});
</script>