<div class="tab-pane fade in active">
    <ul class="nav nav-tabs followup_tabs">
        <?php $i=0; foreach ($languages as $language) { ?>
        <li <?php if ($i==0) echo 'class="active"'; ?>><a href="#tab-<?php echo $followup['id']; ?>-<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="<?php echo $language['flag_url'] ?>"/> <?php echo $language['name']; ?></a></li>
        <?php $i++; }?>
    </ul>
    <div class="tab-content">
        <?php $i=0; foreach ($languages as $language) { ?>
        <div id="tab-<?php echo $followup['id']; ?>-<?php echo $language['language_id']; ?>" language-id="<?php echo $language['language_id']; ?>" class="row-fluid tab-pane language <?php if ($i==0) echo 'active'; ?>">
            <br />
            <div class="row">
                <div class="col-md-3">
                    <h5><strong>Subject:</strong></h5>
                </div>
                <div class="col-md-9">
                    <input placeholder="Mail subject" type="text" class="form-control" name="<?php echo $follow_name; ?>[Subject][<?php echo $language['language_id']; ?>]" value="<?php if(!empty($follow_data['Subject'][$language['language_id']])) echo $follow_data['Subject'][$language['language_id']]; else echo "Follow Up Subject"; ?>" />
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-3">
                    <h5><strong>Message:</strong></h5>
                    <span class="help">
                        <i class="fa fa-info-circle"></i>&nbsp;Use can use the following short-codes:
                        <br />
                        <br />{first_name} - First name
                        <br />{last_name} - Last name
                        <br />{discount_code} - Discount code
                        <br />{discount_value} - Discount code
                        <br />{total_amount} - Total amount
                        <br />{date_end} - End date of coupon validity
                        <br />{order_id} - Order ID
                        <!-- version 2.3 helper -->
                        <br />{order_products} - Show the ordered products
                        <br />{unsubscribe_url} - Link for the customer to be unsubscribed from OrderFollowUp emails
                        <br /><br />
                        <strong>NOTE:</strong> You can also use {first_name}, {last_name} and {order_id} in the subject field.
                        <!-- version 2.3 helper -->
                    </span>
                </div>
                <div class="col-md-9">
                    <textarea id="message_<?php echo $followup['id']; ?>_<?php echo $language['language_id']; ?>" name="<?php echo $follow_name; ?>[Message][<?php echo $language['language_id']; ?>]">
                    <?php if(!empty($follow_data['Message'][$language['language_id']])) echo $follow_data['Message'][$language['language_id']]; else echo '<table style="width:100%">
                        <tbody>
                            <tr>
                                <td align="center">
                                    <table style="width:650px;margin:0 auto;border:1px solid #f0f0f0;padding:10px;line-height:1.8">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p>Hello <strong>{first_name} {last_name}</strong>,</p>
                                                    
                                                    <p>This is a sample follow up&nbsp;message!</p>
                                                    
                                                    <p>We would like to give you a special discount code - <strong>{discount_code}</strong> - which gives you <strong>{discount_value} OFF</strong>.&nbsp;The code applies after you spent <strong>{total_amount}</strong>. This promotion is just for you and expires on <strong>{date_end}</strong>.</p>
                                                    
                                                    <p>Kind Regards,<br />
                                                        <span style="line-height: 1.8;">YourStore</span></p>
                                                        
                                                        <p><a href="http://www.example.com" target="_blank">http://www.example.com</a></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        '; ?>
                        </textarea>
                    </div>
                </div>
            </div>
            <?php $i++; } ?>
        </div>
    </div>