<div class="tab-pane fade in active">
    <div class="row">
        <div class="col-md-3">
            <h5><strong><span class="required">* </span>FollowUp <?php echo $followup['id']; ?> status:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Enable or disable the selected follow up configuration.</span>
        </div>
        <div class="col-md-3">
            <select id="Checker" name="<?php echo $follow_name; ?>[Enabled]" class="form-control">
                <option value="yes" <?php echo (!empty($follow_data['Enabled']) && $follow_data['Enabled'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
                <option value="no"  <?php echo (empty($follow_data['Enabled']) || $follow_data['Enabled']== 'no') ? 'selected=selected' : '' ?>>Disabled</option>
            </select>
        </div>
    </div>
    <br />
    <div class="row">
        <br />
        <div class="col-md-3">
            <h5><strong>Template <?php echo $followup['id']; ?> name:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;Set the name of the template which will show up on the left column.</span>
        </div>
        <div class="col-md-3">
            <input type="text" class="form-control" name="<?php echo $follow_name; ?>[Name]" value="<?php if (isset($follow_data['Name'])) echo $follow_data['Name']; else echo 'FollowUp '.$followup['id'] ; ?>" />
        </div>
    </div>
</div>