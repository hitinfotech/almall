<?php
$follow_name = $moduleName.'[FollowUp]['.$followup['id'].']';
$follow_data = isset($moduleData['FollowUp'][$followup['id']]) ? $moduleData['FollowUp'][$followup['id']] : array();
?>
<div id="followup_<?php echo $followup['id']; ?>" class="tab-pane followups" style="width:99%">
    <input type="hidden" name="<?php echo $follow_name; ?>[id]" class="followup_id" value="<?php echo $followup['id']; ?>">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#general_settings_<?php echo $followup['id'] ?>">General</a></li>
        <li><a data-toggle="tab" href="#conditions_<?php echo $followup['id'] ?>">FollowUp Conditions</a></li>
        <li><a data-toggle="tab" href="#discount_<?php echo $followup['id'] ?>">Discount Settings</a></li>
        <li><a data-toggle="tab" href="#email_template_<?php echo $followup['id'] ?>">Email template</a></li>
    </ul>
    <div class="tab-content">
        <div id="general_settings_<?php echo $followup['id'] ?>" class="tab-pane fade in active">
            <?php require(DIR_APPLICATION.'view/template/'.$module_path.'/tab_general.php'); ?>
        </div>
        <div id="conditions_<?php echo $followup['id'] ?>" class="tab-pane fade in">
            <?php require(DIR_APPLICATION.'view/template/'.$module_path.'/tab_conditions.php'); ?>
        </div>
        <div id="discount_<?php echo $followup['id'] ?>" class="tab-pane fade in">
            <?php require(DIR_APPLICATION.'view/template/'.$module_path.'/tab_discount.php'); ?>
        </div>
        <div id="email_template_<?php echo $followup['id'] ?>" class="tab-pane fade in">
            <?php require(DIR_APPLICATION.'view/template/'.$module_path.'/tab_emailtemplate.php'); ?>
        </div>
    </div>
    <?php if (isset($newAddition) && $newAddition==true) { ?>
    <script type="text/javascript">
        initializeAutocomplete();
    <?php foreach ($languages as $language) { ?>
        $('#message_<?php echo $followup['id']; ?>_<?php echo $language['language_id']; ?>').summernote({
        height: 320
        });
    <?php } ?>
    selectorsForDiscount();
    </script>
    <?php } ?>
</div>