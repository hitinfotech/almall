<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right" style="position:relative;">

        <button type="submit" form="form-account" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>

        <button class="btn btn-default dropdown-toggle" type="button" id="rma-option" data-toggle="dropdown" aria-expanded="true">
          <span><i class="fa fa-cogs"></i></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="rma-option">
          <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $manage_rma; ?>"><?php echo $text_rma_manage; ?></a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $reason_rma; ?>"><?php echo $text_rma_add_reasons; ?></a></li>
        </ul>

        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">

        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
          <li><a href="#tab-labels" data-toggle="tab"><?php echo $tab_labels; ?></a></li>
        </ul>

        <div class="tab-content">

          <div id="tab-general" class="tab-pane active">

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-account" class="form-horizontal">

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $text_rma_enable; ?></label>
                <div class="col-sm-10">
                  <select name="wk_rma_status" id="input-status" class="form-control">
                    <?php if ($wk_rma_status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-time"><span data-toggle="tooltip" title="<?php echo $text_rma_time_info; ?>"><?php echo $text_rma_time; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="wk_rma_system_time" id="input-time" value="<?php echo $wk_rma_system_time; ?>" class="form-control"/>              
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-order-status"><span data-toggle="tooltip" title="<?php echo $text_order_status_info; ?>"><?php echo $text_order_status; ?></span></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php if($order_status){  ?>
                      <?php foreach($order_status as $orders){ ?>
                        <div class="checkbox">
                          <label>
                            <?php if (is_array($wk_rma_system_orders) AND in_array($orders['order_status_id'],$wk_rma_system_orders)) { ?>
                            <input type="checkbox" name="wk_rma_system_orders[]" value="<?php echo $orders['order_status_id']; ?>" checked="checked" />
                            <?php echo $orders['name']; ?>
                            <?php } else { ?>
                            <input type="checkbox" name="wk_rma_system_orders[]" value="<?php echo $orders['order_status_id']; ?>" />
                            <?php echo $orders['name']; ?>
                            <?php } ?>
                          </label>
                        </div>
                      <?php } ?>
                    <?php } ?> 
                  </div>
                  <a onclick="$(this).parent().find(':checkbox').prop('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').prop('checked', false);"><?php echo $text_unselect_all; ?></a> 
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-ex-type"><?php echo $text_image_extenstion; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="wk_rma_system_image" id="input-ex-type" value="<?php echo $wk_rma_system_image; ?>" placeholder="<?php echo $text_extenstion_holder; ?>" class="form-control"/>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-ex-type"><?php echo $text_file_extenstion; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="wk_rma_system_file" id="input-ex-type" value="<?php echo $wk_rma_system_file; ?>" placeholder="<?php echo $text_extenstion_holder; ?>" class="form-control"/>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-ex-size"><span data-toggle="tooltip" title="<?php echo $text_size_info; ?>"><?php echo $text_extenstion_size; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="wk_rma_system_size" id="input-ex-size" value="<?php echo $wk_rma_system_size; ?>" placeholder="200" class="form-control"/>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-information"><span data-toggle="tooltip" title="<?php echo $text_rma_info_policy_info; ?>"><?php echo $text_rma_info_policy; ?></span></label>
                <div class="col-sm-10">
                  <select name="wk_rma_system_information" class="form-control" id="input-information">
                      <option value=""></option>
                      <?php if($information){  ?>
                        <?php foreach($information as $info){ ?>
                          <option value="<?php echo $info['information_id']; ?>" <?php if($wk_rma_system_information == $info['information_id']) echo 'selected'; ?> > <?php echo $info['title']; ?> </option>
                        <?php } ?>
                      <?php } ?> 
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-policy"><span data-toggle="tooltip" title="<?php echo $text_rma_return_add_info; ?>"><?php echo $text_rma_return_add; ?></span></label>
                <div class="col-sm-10">
                  <textarea name="wk_rma_address" class="form-control" id="input-policy" rows="5"><?php echo $wk_rma_address;?></textarea>      
                </div>
              </div>

              <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_info_mail; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
              </div>              

              <div class="form-group">
                <label class="col-sm-2 control-label">
                  <span data-toggle="tooltip" data-original-title="<?php echo $mail_keywords_info; ?>" >
                    <?php echo $entry_mail_keywords; ?>
                  </span>
                </label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="marketplace_mail_keywords" style="height:150px"><?php if(isset($marketplace_mail_keywords)) { echo $marketplace_mail_keywords; } else { ?>
{product_name}
{customer_name}
{seller_name}
{config_logo}
{config_icon}
{config_currency}
{config_image}
{config_name}
{config_owner}
{config_address}
{config_geocode}
{config_email}
{config_telephone}
{rma_id}
<?php } ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-mail-seller"><span data-toggle="tooltip" title="<?php echo $entry_mail_seller_info; ?>"><?php echo $entry_mail_seller; ?></span></label>
                <div class="col-sm-10">
                  <select name="wk_rma_seller_mail" id="input-mail-seller" class="form-control">
                    <option value=""></option>
                    <?php if($mails){ ?>
                    <?php foreach($mails as $mail){ ?>
                      <option value="<?php echo $mail['id']; ?>" <?php if($wk_rma_seller_mail==$mail['id']) echo 'selected';?>>  <?php echo $mail['name']; ?> </option>
                    <?php } ?>
                    <?php } ?>                    
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-mail-customer"><span data-toggle="tooltip" title="<?php echo $entry_mail_customer_info; ?>"><?php echo $entry_mail_customer; ?></span></label>
                <div class="col-sm-10">
                  <select name="wk_rma_customer_mail" id="input-mail-customer" class="form-control">
                    <option value=""></option>
                    <?php if($mails){ ?>
                    <?php foreach($mails as $mail){ ?>
                      <option value="<?php echo $mail['id']; ?>" <?php if($wk_rma_customer_mail==$mail['id']) echo 'selected';?>>  <?php echo $mail['name']; ?> </option>
                    <?php } ?>
                    <?php } ?>                    
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-mail-admin"><span data-toggle="tooltip" title="<?php echo $entry_mail_admin_info; ?>"><?php echo $entry_mail_admin; ?></span></label>
                <div class="col-sm-10">
                  <select name="wk_rma_admin_mail" id="input-mail-admin" class="form-control">
                    <option value=""></option>
                    <?php if($mails){ ?>
                    <?php foreach($mails as $mail){ ?>
                      <option value="<?php echo $mail['id']; ?>" <?php if($wk_rma_admin_mail==$mail['id']) echo 'selected';?>>  <?php echo $mail['name']; ?> </option>
                    <?php } ?>
                    <?php } ?>                    
                  </select>
                </div>
              </div>
            </form>
          </div>

          <div id="tab-labels" class="tab-pane">
            <form action="<?php echo $action_image; ?>" method="post" enctype="multipart/form-data" id="form-account" class="form-horizontal">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $text_labels; ?></label>
                <div class="col-sm-10">
                  <button data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary pull-right col-sm-1" onclick="$('#form-account').submit()"><i class="fa fa-save"></i></button>
                  <div class="input-group col-sm-10">
                    <span class="input-group-btn">
                      <label type="button" class="btn btn-primary" for="file-upload"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></label>
                    </span>
                    <input type="file" id="file-upload" multiple name="up_file[]" class="form-control hide">
                    <input type="text" id="input-file-name" class="form-control" disabled/>
                  </div>
                </div>
              </div>
            </form>

            <?php if($shipping_label_folder){ ?>
              <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-image" class="form-horizontal">
                <table class="table table-bordered table-hover">    
                  <thead>
                    <tr>
                        <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>                     
                        <td class="text-center"><?php echo $text_lable_image; ?></td> 
                        <td class="text-left">
                          <?php echo $text_lable_name; ?>
                          <a type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger pull-right" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-image').submit() : '';"><i class="fa fa-trash-o"></i></a>
                        </td>                   
                    </tr>
                  </thead>      
                  
                  <?php foreach($shipping_label_folder as $image){ ?>
                  <tr>
                    <td><input type="checkbox" name="selected[]" value="<?php echo $image['name']; ?>" /></td>       
                    <td class="text-center"><img src="<?php echo $image['image']; ?>" alt="<?php echo $image['name']; ?>"/></td>
                    <td class="text-left"><?php echo $image['name']; ?></td>
                  </tr>
                  <?php } ?> 
                  
                </table>
              </form>
            <?php } ?>

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
jQuery('#file-upload').change(function(){
  name = '';
  if($(this)[0].files!=undefined){
    $($(this)[0].files).each(function(index,value){
      name = value.name  + ', ' + name;
    });
  }
  $('#input-file-name').val(name);
  // $('#input-file-name').val(jQuery(this).val().replace(/C:\\fakepath\\/i, ''));
});
</script>
