<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_bundle_description; ?></strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundle_description_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select id="LinkChecker" name="<?php echo $moduleName; ?>[DescriptionViewEnabled]" class="form-control">
                <option value="yes" <?php echo (!empty($moduleData['DescriptionViewEnabled']) && $moduleData['DescriptionViewEnabled'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                <option value="no"  <?php echo (empty($moduleData['DescriptionViewEnabled']) || $moduleData['DescriptionViewEnabled']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_picture_width_height; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_in_pixels_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="input-group">
              <span class="input-group-addon"><?php echo $text_width; ?>&nbsp;</span>
                <input type="text" name="<?php echo $moduleName; ?>[ViewWidth]" class="form-control" value="<?php echo (isset($moduleData['ViewWidth'])) ? $moduleData['ViewWidth'] : '80' ?>" />
              <span class="input-group-addon"><?php echo $text_px; ?></span>
            </div>
            <br />
            <div class="input-group">
              <span class="input-group-addon"><?php echo $text_height; ?></span>
                <input type="text" name="<?php echo $moduleName; ?>[ViewHeight]" class="form-control" value="<?php echo (isset($moduleData['ViewHeight'])) ? $moduleData['ViewHeight'] : '80' ?>" />
              <span class="input-group-addon"><?php echo $text_px; ?></span>
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_custom_css; ?></strong></h5>
       		<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_custom_css_helper; ?></span> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <textarea rows="10" name="<?php echo $moduleName; ?>[ViewCustomCSS]" placeholder="CSS Style" class="form-control"><?php echo (isset($moduleData['ViewCustomCSS'])) ? $moduleData['ViewCustomCSS'] : '' ?></textarea>
        </div>
    </div>
    <hr />
</div>