<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><span class="required">*</span> <strong><?php echo $text_module_status; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_module_status_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select id="Checker" name="<?php echo $moduleName; ?>[Enabled]" class="form-control">
                  <option value="yes" <?php echo (!empty($moduleData['Enabled']) && $moduleData['Enabled'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                  <option value="no"  <?php echo (empty($moduleData['Enabled']) || $moduleData['Enabled']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_multiple_bundles; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_multiple_bundles_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select name="<?php echo $moduleName; ?>[MultipleBundles]" class="form-control">
                  <option value="no"  <?php echo (empty($moduleData['MultipleBundles']) || $moduleData['MultipleBundles']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_add_the_discount_once; ?></option>
                  <option value="yes" <?php echo (!empty($moduleData['MultipleBundles']) && $moduleData['MultipleBundles'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_add_discount_every_time; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_regard_taxes_on_discount; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_regard_taxes_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select name="<?php echo $moduleName; ?>[DiscountTaxation]" class="form-control">
                  <option value="no"  <?php echo (empty($moduleData['DiscountTaxation']) || $moduleData['DiscountTaxation']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
                  <option value="yes" <?php echo (!empty($moduleData['DiscountTaxation']) && $moduleData['DiscountTaxation'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
            </select>
        </div>
    </div>
    <hr />
</div>