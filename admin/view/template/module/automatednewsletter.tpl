<?php echo $header;?><?php echo $column_left; ?>
<div id="content">
<div class="modal fade" id="progressModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo $text_sending; ?></h4>
            </div>
            <div class="modal-body">
                <div class="progress progress-striped active">
                    <div id="percentProgress" class="bar progress-bar" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">0%</div>
                </div>
                <div id="progressMessage"><span><?php echo $text_preparing; ?></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger disabled" id="btnProgressAbort"><?php echo $text_abort; ?></button>
                <button class="btn btn-success disabled" id="btnOkModalClose">OK</button>
            </div>
        </div>
    </div>
</div>
 <div class="page-header">
    <div class="container-fluid">
      <h1>&nbsp;<?php echo $heading_title; ?></h1>
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
     </div>
</div>
<div class="container-fluid">         
    <?php echo (empty($data['AutomatedNewsletter']['LicensedOn'])) ? base64_decode('ICAgIDxkaXYgY2xhc3M9ImFsZXJ0IGFsZXJ0LWRhbmdlciBmYWRlIGluIj4NCiAgICAgICAgPGJ1dHRvbiB0eXBlPSJidXR0b24iIGNsYXNzPSJjbG9zZSIgZGF0YS1kaXNtaXNzPSJhbGVydCIgYXJpYS1oaWRkZW49InRydWUiPsOXPC9idXR0b24+DQogICAgICAgIDxoND5XYXJuaW5nISBVbmxpY2Vuc2VkIHZlcnNpb24gb2YgdGhlIG1vZHVsZSE8L2g0Pg0KICAgICAgICA8cD5Zb3UgYXJlIHJ1bm5pbmcgYW4gdW5saWNlbnNlZCB2ZXJzaW9uIG9mIHRoaXMgbW9kdWxlISBZb3UgbmVlZCB0byBlbnRlciB5b3VyIGxpY2Vuc2UgY29kZSB0byBlbnN1cmUgcHJvcGVyIGZ1bmN0aW9uaW5nLCBhY2Nlc3MgdG8gc3VwcG9ydCBhbmQgdXBkYXRlcy48L3A+PGRpdiBzdHlsZT0iaGVpZ2h0OjVweDsiPjwvZGl2Pg0KICAgICAgICA8YSBjbGFzcz0iYnRuIGJ0bi1kYW5nZXIiIGhyZWY9ImphdmFzY3JpcHQ6dm9pZCgwKSIgb25jbGljaz0iJCgnYVtocmVmPSNpc2Vuc2Vfc3VwcG9ydF0nKS50cmlnZ2VyKCdjbGljaycpIj5FbnRlciB5b3VyIGxpY2Vuc2UgY29kZTwvYT4NCiAgICA8L2Rpdj4=') : '' ?>
    <?php if ($error_warning) { ?>
  		<div class="alert alert-danger autoSlideUp"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
 		 	<button type="button" class="close" data-dismiss="alert">&times;</button>
 		 </div>
 	<?php } ?>
   	<?php if ($success) { ?>
        <div class="alert alert-success autoSlideUp"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <script> $('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
    <?php $success = null; } ?>
    <div class="alert alert-success autoCronJobSlideUp" style="display:none"> <i class="icon-ok-sign"></i><?php echo $cron_successfully_changed; ?> </div>
    <div class="panel panel-default">
        <div class="panel-heading">
             <h3 class="panel-title"><i class="fa fa-list"></i>&nbsp;<span style="vertical-align:middle;font-weight:bold;"><?php echo $text_settings; ?></span></h3>
                <div class="storeSwitcherWidget">
                    <div class="form-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><?php echo $store['name']; if($store['store_id'] == 0) echo " <strong>(".$text_default.")</strong>"; ?>&nbsp;<span class="caret"></span><span class="sr-only"><?php echo $text_toggle; ?></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <?php foreach ($stores  as $st) { ?>
                                <li><a href="index.php?route=<?php echo $modulePath; ?>&store_id=<?php echo $st['store_id'];?>&token=<?php echo $token; ?>"><?php echo $st['name']; ?></a></li>
                            <?php } ?> 
                        </ul>
                    </div>
                </div>
        </div>
        <div class="clearfix"></div>
        <div class="panel-body fadeInOnLoad">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form"> 
                <input type="hidden" name="store_id" value="<?php echo $store['store_id']; ?>">
                <input type="hidden" name="AutomatedNewsletter_status" value="1" />
                    <div class="tabbable">
                        <div class="tab-navigation">
                            <ul class="nav nav-tabs mainMenuTabs">
                                <li class="active"><a href="#controlpanel" data-toggle="tab"><i class="icon-wrench"></i>&nbsp;<?php echo $text_settings; ?></a></li>
                                <li><a href="#template" data-toggle="tab"><i class="icon-envelope"></i>&nbsp;<?php echo $entry_newsletter; ?></a></li>
                                <li><a href="#prooduct" data-toggle="tab"><i class="icon-th-large"></i>&nbsp;<?php echo $text_list; ?></a></li>
                                <li><a href="#sentNewslettersContent" data-toggle="tab"><i class="icon-eye-open"></i>&nbsp;<?php echo $text_sent_newsletter; ?></a></li>
                                <li><a href="#schedule" data-toggle="tab"><i class="icon-time"></i>&nbsp;<?php echo $text_tasks; ?></a></li>
                                <li><a href="#isense_support" data-toggle="tab"><i class="icon-share"></i>&nbsp;<?php echo $text_support; ?></a></li>
                            </ul>
                            <div class="tab-buttons">
                                <button type="submit" class="btn btn-primary save-changes"><i class="icon-ok"></i>&nbsp;<?php echo $text_save; ?></button>
                                <a onclick="location = '<?php echo $cancel; ?>'" class="btn"><?php echo $text_cancel; ?></a>
                            </div>
                        </div>
                        <div class="tab-content">
                    	<?php
                            if (!function_exists('modification_vqmod')) {
                                function modification_vqmod($file) {
                                    if (class_exists('VQMod')) {
                                        return VQMod::modCheck(modification($file), $file);
                                    } else {
                                        return modification($file);
                                    }
                                }
                            }
                         ?>
                            <div id="controlpanel" class="tab-pane active"><?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_settings.php'); ?></div>
                            <div id="template" class="tab-pane"></div>
                            <div id="prooduct" class="tab-pane"><?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_product.php'); ?></div>
                            <div id="sentNewslettersContent" class="tab-pane"></div>
                            <div id="schedule" class="tab-pane"><?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_schedule.php'); ?></div>
                            <div id="isense_support" class="tab-pane"><?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_support.php'); ?></div>
                        </div> <!-- /.tab-content --> 
                    </div><!-- /.tabbable -->
            </form>
        </div>
    </div>
<script type="text/javascript">
    token = "<?php echo $token; ?>";
    store_id = "<?php echo $store['store_id']; ?>";
    ckeditors = {};
	
$('.tab-content').delegate('.sentNewsletters .pagination a', 'click',  function(e){ 
  e.preventDefault(); 
  $.ajax({
      url: this.href,
      type:'post',
      dataType:'html',
      success: function(data) { 
        $('.sentNewsletters').html(data);
      }
    });
});

$('.tab-content').delegate( '.btn.deleteNewsletter','click',function(e){
  e.preventDefault(); 
  if(confirm("Are you sure you want to delete selected newsletters?")) {
    var deleteList=[];
    $('input[type="checkbox"][name="deleteSentNewsletter"]:checked').each(function(){ deleteList.push($(this).val());});
    $.ajax({
      url: "index.php?route=<?php echo $modulePath; ?>/deleteNewsletter&token=<?php echo $token; ?>",
      type:'post',
      dataType:'html',
      data: {'newsletters': deleteList, 'store_id': <?php echo $store['store_id']; ?> },
      success: function(data) { 
        $('.sentNewsletters').html(data);
      }
    });
  }
});	
</script>
<?php echo $footer; ?>