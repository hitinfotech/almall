<a class="btn btn-link backToSent"><i class="icon-chevron-left" style="font-size:10px"></i> <?php echo $text_back; ?></a>
<?php echo $newsletter['content']?>

<script type="text/javascript">
  $('.backToSent').on('click', function(e){
    e.preventDefault();
    $.ajax({
      url: "index.php?route=<?php echo $modulePath; ?>/sentNewsletters&token=<?php echo $token; ?>",
      type:'post',
      dataType:'html',
      success: function(data){
        $('#sentNewslettersContent').html(data);
      }
    });
  })
</script>
