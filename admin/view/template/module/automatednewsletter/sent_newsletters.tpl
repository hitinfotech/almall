<div class="sentNewsletters">
  <a class="btn btn-danger deleteNewsletter pull-right"><?php echo $text_delete; ?></a>
  <table class="table sentnewsletters">
    <thead>
      <tr>
        <td width="15px"><input type="checkbox" name="selectAll"></td>
        <td class="left">
          <?php if ($sort == 'subject') { ?><a href="<?php echo $sort_subject ?>" class="<?php echo strtolower($order); ?>"><?php echo $newsletter_subject; ?></a>
              <?php } else { ?><a href="<?php echo $sort_subject; ?>"><?php echo $newsletter_subject; ?></a><?php } ?>
          </td>
        <td class="left">
          <?php if ($sort == 'time_added') { ?><a href="<?php echo $sort_time_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $date_sent; ?></a>
              <?php } else { ?><a href="<?php echo $sort_time_added; ?>"><?php echo $date_sent; ?></a><?php } ?>
          </td>
          <td class="left">
          <?php if ($sort == 'language_id') { ?><a href="<?php echo $sort_language; ?>" class="<?php echo strtolower($order); ?>"><?php echo $language; ?></a>
              <?php } else { ?><a href="<?php echo $sort_language; ?>"><?php echo $language; ?></a><?php } ?>
          </td>
          <td class="left" colspan="2">
          <?php if ($sort == 'customers_count') { ?><a href="<?php echo $sort_customers_count; ?>" class="<?php echo strtolower($order); ?>"><?php echo $recepients; ?></a>
              <?php } else { ?><a href="<?php echo $sort_customers_count; ?>"><?php echo $recepients; ?></a><?php } ?>
          </td>
      </tr>
    </thead>
    <?php if(!empty($newsletters)){
       foreach ($newsletters as $newsletter) { if(isset($newsletter['newsletter_id'])) {  ?>
    <tbody style="border-top: 1px solid #dddddd !important;">
        <tr>
          <td>
            <input type="checkbox" name="deleteSentNewsletter"  value="<?php echo $newsletter['newsletter_id'];?>">
          </td>
          <td class="left">
            <a href="index.php?route=<?php echo $modulePath; ?>/newsletterView&newsletter_id=<?php echo $newsletter['newsletter_id']; ?>&token=<?php echo $token ;?>" id= "<?php echo $newsletter['newsletter_id'];?>" class="newsletterSubject">
              <?php echo $newsletter['subject']; ?>
            </a>
          </td>
          <td class="left"><?php echo date("d.m.Y/H:i:s",$newsletter['time_added']); ?></td>
          <td class="left"><?php echo $newsletter['name']; ?></td>
          <td class="left" colspan="3" style="width: 35%";><a id="<?php echo $newsletter['newsletter_id']; ?>" class="toggle" href="javascript:void(0)" ><?php echo $newsletter['success_sent'].' / '.$newsletter['customers_count']; ?></a><?php if ($newsletter['success_sent'] < $newsletter['customers_count']) { ?>&nbsp;<a class="btn btn-success btn-small btn-continue" data-store_id="<?php echo $newsletter['store_id']; ?>" data-template_id="<?php echo $newsletter['template_id']; ?>" data-language_id="<?php echo $newsletter['language_id']; ?>" data-newsletter_id="<?php echo $newsletter['newsletter_id']; ?>">Continue process...</a><?php } ?></a></td>
        </tr>
    </tbody>
    
    <thead class="customers_table_head" id="customers_tablehead<?php echo $newsletter['newsletter_id']; ?>" style="display:none;">
    <tr>
          <td><?php echo $entry_name; ?></td>
            <td><?php echo $entry_email; ?></td>
            <td> </td>
        </td>
    </thead>
    <tbody class="customers_table_body" id="customers_tablebody<?php echo $newsletter['newsletter_id']; ?>" style="display:none">
          <?php if (isset($newsletters_customers)) { foreach($newsletters_customers AS $newsletter_customer) {
           if($newsletter_customer['newsletter_id'] == $newsletter['newsletter_id']) { ?> 
      <tr>
            <td>
                <?php echo $newsletter_customer['firstname'].' '.(isset($newsletter_customer['lastname']) ? $newsletter_customer['lastname'] : ''); ?>
            </td>
            <td>
                <?php echo $newsletter_customer['email']; ?>
            </td>
            <td>
             <?php if($newsletter_customer['success'] == 1) { ?>
              <i class="fa fa-check"></i>
             <?php } else { ?>
              <i class="fa fa-times"></i> 
             <?php } ?>
            </td>
           </tr>        
           <?php  } else {} } } else { ?>
            <tr>
              <td colspan="3"><?php echo $text_no_results; ?></td>
            </tr> 
           <?php } ?>         
  </tbody>  
           <?php } else {} }  ?>
    <?php } else { ?>
      <tr><td class="center" colspan="5"><?php echo $text_no_results; ?></td></tr>
    <?php } ?>
  </table>
  <div class="pagination">
    <?php echo $pagination; ?>
  </div>
</div>
<script> 
$('.sentnewsletters').on('click', '.toggle' ,function () {
var id = this.id; 
$("#customers_tablehead".concat(id)).slideToggle(150);
$("#customers_tablehead".concat(id)).css( "display", "inline-block" );

$("#customers_tablebody".concat(id)).slideToggle(150);
$("#customers_tablebody".concat(id)).css( "display", "inline-block" );
});
</script>

<script type="text/javascript">
$('.newsletterSubject').on('click', function(e) {
  e.preventDefault();
  $.ajax({
    url: this.href,
    type:'post',
    dataType:'html',
    success: function(data){
      $('#sentNewslettersContent').html(data);
    }
  });
});


$('input[type="checkbox"][name="selectAll"]').on('change', function(){
  $('input[name="deleteSentNewsletter"]').attr('checked', this.checked);
});
</script>