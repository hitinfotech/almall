<table id="mainSettings" class="table">
  
		<tr>
			<td class="col-xs-3">
                <h5><span class="required">* </span><strong><?php echo $entry_code; ?></strong></h5>    
                <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $entry_code_help; ?></span></td>
            <td class="col-xs-9">
                <div class="col-xs-4">
                    <select name="AutomatedNewsletter[Enabled]" class="AutomatedNewsletterEnabled form-control">
                        <option value="true" <?php echo (!empty($data['AutomatedNewsletter']['Enabled']) && $data['AutomatedNewsletter']['Enabled'] == 'true') ? 'selected=selected' : ''?>><?php echo $text_enabled; ?></option>
                        <option value="false" <?php echo (empty($data['AutomatedNewsletter']['Enabled']) || $data['AutomatedNewsletter']['Enabled'] == 'false') ? 'selected=selected' : ''?>><?php echo $text_disabled; ?></option>
                    </select>
                </div>
            </td>
		</tr>
       <tr>
    	<td class="col-xs-3">
            <h5><strong><?php echo $entry_store?></strong></h5></td>
        <td class="col-xs-9">
        	<div class="col-xs-4">
                 <input type="text" class="form-control"  value="<?php if(!empty($data['AutomatedNewsletter']['from'])) echo $data['AutomatedNewsletter']['from']; else echo $store_name; ?>" name="AutomatedNewsletter[from]"/>
         	</div>
         </td>
        </tr>
       <tr>
    	<td class="col-xs-3">
            <h5><strong><?php echo $entry_store_email?></strong></h5></td>
        <td class="col-xs-9">
        	<div class="col-xs-4">
                 <input type="text" class="form-control"  value="<?php if(!empty($data['AutomatedNewsletter']['from_email'])) echo $data['AutomatedNewsletter']['from_email']; else echo $e_mail; ?>" name="AutomatedNewsletter[from_email]"/>
         	</div>
         </td>
        </tr>
        <tr>
    		<td class="col-xs-3">
            	<h5><span class="required">*</span><strong><?php echo  $admin_notification; ?></strong></h5>
                <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo str_replace('{email}', $e_mail , $admin_notification_help); ?></span>
            </td>
  			<td class="col-xs-9">
        		<div class="col-xs-4">            
                  <select name="AutomatedNewsletter[admin_notification]" class="form-control">
                     <option value="yes" <?php echo (!empty($data['AutomatedNewsletter']['admin_notification']) && $data['AutomatedNewsletter']['admin_notification'] == 'yes') ? 'selected=selected' : ''?>><?php echo $text_enabled; ?></option>
                     <option value="no" <?php echo (empty($data['AutomatedNewsletter']['admin_notification']) || $data['AutomatedNewsletter']['admin_notification'] == 'no') ? 'selected=selected' : ''?>><?php echo $text_disabled; ?></option>
                  </select>
                </div>
             </td>
         </tr>  
        <tr>
            <td class="col-xs-3">
            	<h5><strong><?php echo $entry_to; ?></strong></h5></td>
         	<td class="col-xs-9">
        	<div class="col-xs-4">
                <select name="AutomatedNewsletter[newsletter_receiver]" class="form-control">
                    <option value="newsletter" <?php if(!empty($data['AutomatedNewsletter']['newsletter_receiver']) && $data['AutomatedNewsletter']['newsletter_receiver'] == 'newsletter') echo 'selected="selected"'?>><?php echo $text_newsletter; ?></option>
                    <option value="customer_all" <?php if(!empty($data['AutomatedNewsletter']['newsletter_receiver']) && $data['AutomatedNewsletter']['newsletter_receiver'] == 'customer_all') echo 'selected="selected"'?>><?php echo $text_customer_all; ?></option>
                    <option value="customer_group" <?php if(!empty($data['AutomatedNewsletter']['newsletter_receiver']) && $data['AutomatedNewsletter']['newsletter_receiver'] == 'customer_group') echo 'selected="selected"'?>><?php echo $text_customer_group; ?></option>
                    <option value="customer" <?php if(!empty($data['AutomatedNewsletter']['newsletter_receiver']) && $data['AutomatedNewsletter']['newsletter_receiver'] == 'customer') echo 'selected="selected"'?>><?php echo $text_customer; ?></option>
                    <option value="affiliate_all" <?php if(!empty($data['AutomatedNewsletter']['newsletter_receiver']) && $data['AutomatedNewsletter']['newsletter_receiver'] == 'affiliate_all') echo 'selected="selected"'?>><?php echo $text_affiliate_all; ?></option>
                    <option value="affiliate" <?php if(!empty($data['AutomatedNewsletter']['newsletter_receiver']) && $data['AutomatedNewsletter']['newsletter_receiver'] == 'affiliate') echo 'selected="selected"'?>><?php echo $text_affiliate; ?></option>
                    <option value="product" <?php if(!empty($data['AutomatedNewsletter']['newsletter_receiver']) && $data['AutomatedNewsletter']['newsletter_receiver'] == 'product') echo 'selected="selected"'?>><?php echo $text_product; ?></option>
                </select>
             </div>
            </td>
        </tr>
  
    <tbody id="to-customer-group" class="to">
      <tr>
        <td class="col-xs-3">
            <h5><strong><?php echo $entry_customer_group ?></strong></h5></td>
         <td class="col-xs-9">
        	<div class="col-xs-4">
                <select name="AutomatedNewsletter[to][customer_group_id]" class="form-control">
                <?php foreach ($customer_groups as $customer_group) { ?>
                    <option  value="<?php echo $customer_group['customer_group_id']; ?>" <?php if(!empty($data['AutomatedNewsletter']['to']['customer_group_id']) && $data['AutomatedNewsletter']['to']['customer_group_id'] == $customer_group['customer_group_id']) echo 'selected="selected"';?>> <?php echo $customer_group['name']; ?> </option>
                <?php } ?>
                </select>
              </div>
            </td>
        </tr>
    </tbody>
    <tbody id="to-customer" class="to">
		<tr>
            <td class="col-xs-3">
                <h5><strong><?php echo $entry_customer; ?></strong></h5>
            </td>
            <td class="col-xs-9">
                <div class="col-xs-4">
                    <div clas="input-group">
                        <input type="text" name="customers" class="form-control" value="" placeholder="Specific customer name..." />
                        <div id="customer_container" class="well well-sm">
                            <?php foreach($customers as $customer) { ?>
                                <div id="customer<?php echo $customer['customer_id']; ?>'">
                                    <i class="fa fa-minus-circle"></i> <?php echo $customer['firstname'] . ' ' . $customer['lastname'];?> 
                                    <input type="hidden" name="AutomatedNewsletter[to][customer][]" value="<?php echo $customer['customer_id']; ?>" />
                                </div>
                            <?php } ?>
                        </div>
                    </div>
				</div>
            </td>
        </tr>
    </tbody>
    <tbody id="to-affiliate" class="to">
        <tr>
        <td class="col-xs-3">
            <h5><strong><?php echo $entry_affiliate; ?></strong></h5>
        </td>
         <td class="col-xs-9">
        	<div class="col-xs-4">
				<div clas="input-group">
                	<input type="text" name="affiliates" class="form-control" value="" placeholder="Specific affiliate name..."  />
                    <div id="affiliate" class="well well-sm">
                        <?php if(isset($affiliates)) {
                            foreach($affiliates as $affiliate) { ?>                                
                                <div id="customer<?php echo $affiliate['affiliate_id']; ?>'">
                                <i class="fa fa-minus-circle"></i><?php echo $affiliate['firstname'] . ' ' . $affiliate['lastname'];?>  
                                <input type="hidden" name="AutomatedNewsletter[to][affiliate][]" value="<?php echo $affiliate['affiliate_id']; ?>" />
                            	</div>
                        <?php } } ?>
                    </div>
                </div>
              </div> 
            </td>
        </tr>
    </tbody>
    <tbody id="to-product" class="to">
        <tr>
            <td class="col-xs-3">
                <h5><strong><?php echo  $entry_spec_product; ?></strong></h5>
                <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $entry_spec_product_help; ?></span></td>
             <td class="col-xs-9">
                <div class="col-xs-4">
                    <div clas="input-group">
						<input type="text" name="products" value="" class="form-control" placeholder="Product name..."  />
                        <div id="product" class="well well-sm">
                             <?php if(isset($products)) { 
                            	foreach($products as $product)  {?>                                
                                    <div id="product<?php echo $product['product_id']; ?>'">
                                        <i class="fa fa-minus-circle"></i><?php echo $product['name'];?>  
                                        <input type="hidden" name="AutomatedNewsletter[to][product][]" value="<?php echo $product['product_id']; ?>" />
                                    </div>
                            <?php } } ?>
                        </div>
                    </div>
                 </div>
              </td>
        </tr>
  </tbody>
    <tr>
        <td class="col-xs-3">
            <h5><strong><?php echo $entry_num_emails_per_request; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $entry_num_emails_per_request_help; ?></span></td>
        <td class="col-xs-9">
            <div class="col-xs-4">
                 <input type="number" class="form-control"  value="<?php if(!empty($data['AutomatedNewsletter']['num_emails'])) echo $data['AutomatedNewsletter']['num_emails']; else echo '5'; ?>" name="AutomatedNewsletter[num_emails]"/>
            </div>
         </td>
     </tr>
 </table> 
