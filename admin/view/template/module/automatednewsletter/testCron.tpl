<table class="table testCron">
  <tr>
    <td colspan="2"><h5>System information</h5></td>
  </tr>
  <tr>
        <td colspan="2">Server time: <?php echo date('H:i:s'); ?></td>
  </tr>
  <tr>
    <td>shell_exec() function status:</td>
    <td><b><p class="<?php echo $shell_exec_status ?>"><?php echo $shell_exec_status ?></td>
  </tr>
    <tr><td><?php  echo $cron_folder;?></td>
      <td><b><p class="<?php echo $folder_permission ?>"><?php echo $folder_permission ?></td>
    </tr>
  <tr>
    <td>Cron job status:</td>
    <td><b><p class="<?php echo $cronjob_status ?>"><?php echo $cronjob_status ?></td>
  </tr>
  <tr>
    <td colspan="2"><h5>Current cron jobs</h5></td>
  </tr>
  <?php if(isset($current_cron_jobs)) { 
              foreach($current_cron_jobs as $cron_job) { 
                  if(!empty($cron_job)) {?>
                <tr><td colspan="2"><p class="cronJobEntry"><?php  echo $cron_job; ?></td></tr>
              <?php }}} ?>
</table>