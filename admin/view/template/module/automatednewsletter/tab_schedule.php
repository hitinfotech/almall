<div class="schedule_left_column">
<table class="table cronForm" id="mainSettings">
	<tr>
       <td class="col-xs-4">
            <h5><span class="required">* </span><strong> <?php echo $schedule_tasks_status; ?></strong></h5>    
       </td> 
       <td class="col-xs-8">
            <div class="col-xs-6">
                <select name="AutomatedNewsletter[scheduleEnabled]" class="form-control">
                    <option value="yes" <?php echo (!empty($data['AutomatedNewsletter']['scheduleEnabled']) && $data['AutomatedNewsletter']['scheduleEnabled'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                    <option value="no"  <?php echo (empty($data['AutomatedNewsletter']['scheduleEnabled']) || $data['AutomatedNewsletter']['scheduleEnabled']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
                 </select>
             </div>
         </td>
	</tr>
	<tr class="hideable" style="display:none">
     	<td class="col-xs-4">
            <h5><span class="required">* </span><strong><?php echo $schedule_type; ?></strong></h5> 
        </td>      
        <td class="col-xs-8">
            <div class="col-xs-6">
                <select name="AutomatedNewsletter[scheduleType]" class="form-control">
                <option value="F" <?php if(!empty($data['AutomatedNewsletter']['scheduleType']) && $data['AutomatedNewsletter']['scheduleType'] == 'F') echo "selected" ?>><?php echo $fixed_dates; ?></option>
                <option value="P" <?php if(!empty($data['AutomatedNewsletter']['scheduleType']) && $data['AutomatedNewsletter']['scheduleType'] == 'P') echo "selected" ?>><?php echo $periodic; ?></option>
              </select>
           </div>
       </td>
  </tr>
	<tr class="hideable" style="display:none">
		<td class="col-xs-3">&nbsp;  </td>
		<td class="col-xs-9">
            <div id="fixedDateOptions"  class="col-xs-12"><!-- <td style="display:none;"></td> -->
                <div class="col-xs-3"  style="padding-left: 0px">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                       <input type="text" id="fixedDate" class="form-control" data-format="DD-MM-YYYY" value="" placeholder="Date..." readonly />
                    </div>
                </div>
                <div class="col-xs-3"  style="padding-left: 0px">
                    <div class="input-group">
                       <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                    <input type="text" id="fixedDateTime" class="timepicker form-control" data-format="HH:mm" value="" placeholder="Time..." readonly />
                    </div>
                </div>
                <select name="scheduled_template" class="form-control" style="float:left; width:30%; margin-right:10px;">
                  <?php foreach ($templates as $key => $template) { ?>
                    <option value="<?php echo $template['template_id']?>"><?php echo $template['subject'];?></option>
                  <?php }?>
                </select>
                <button type="button" class="btn btn-primary addDate" data-original-title="Add Date" ><i class="fa fa-plus-circle"></i></button>
                <div class="well well-sm scrollbox dateList">     
                  <?php if(isset($data['AutomatedNewsletter']['fixedDates'])) { 
                  foreach($data['AutomatedNewsletter']['fixedDates'] as $val) { 
                    $explode = explode('~~', $val); 
                    $date = $explode[0]; 
					 $id = explode( '.', $date); 
                     $id = explode( '/', $id[0]); 
                     ?>
                    <div id="date<?php echo $id[0].$id[1].$id[2]; ?>"><?php echo $date ?>&nbsp;&nbsp;&nbsp;<?php echo $explode[2]?><i class="icon-minus-sign removeIcon"></i><input type="hidden" name="AutomatedNewsletter[fixedDates][]" value="<?php echo $val ?>" /></div> 
                  <?php }} ?> 
                 </div>
      		</div>
			<div id="periodicOptions" class="col-xs-12">
                <div id="cronSelector" style="display: inline-block;margin-right:30px"></div>
                <div style="display: inline-block;">
                  <select name="AutomatedNewsletter[scheduled_template_periodic]" class="form-control" style="width:220px;" id="scheduled_template_periodic">
                    <?php foreach ($templates as $key => $template) { ?>
                      <option <?php if(isset($data['AutomatedNewsletter']['scheduled_template_periodic']) && $data['AutomatedNewsletter']['scheduled_template_periodic'] == $template['template_id']) echo "selected"?> value="<?php echo $template['template_id']?>"><?php echo $template['subject'];?></option>
                    <?php }?>
                  </select>
                </div>
        	<input type="hidden" name="AutomatedNewsletter[periodicCronValue]" value="">
      		</div>
		</td>
	</tr>
  <tr class="hideable" style="display:none">
    <td colspan="2">
    	<button id="testCronAvailablity" class="btn btn-warning"><?php echo $test_cron_button; ?></button>
    	<span class="help" style="display: inline-block; font-style: italic; margin-top:10px;"><i class="icon-info-sign"></i>&nbsp;<?php echo $cron_requirements; ?></span>
    </td>
  </tr>
</table>
</div>
<div class="schedule_right_column hideable">
    <h3><?php echo $text_cron; ?></h3>
    <div><?php echo $cron_services; ?>
       	<span class="help" style="display: inline-block; font-style: italic; margin-top:10px;""><i class="icon-info-sign"></i>&nbsp;<?php echo $cron_services_help; ?></span>
    </div>
    
    <input type="hidden" id="cron_url" value="<?php echo $cron_url; ?>">
    <table class="table other_cron_services">
        <thead>
            <td style="width: 90px; padding-right:20px"><b><?php echo $name; ?></b></td>
            <td><b><?php echo $link; ?></b></td>
        </thead>
        <tbody>
			<?php if(!empty($data['AutomatedNewsletter']['scheduleEnabled']) && $data['AutomatedNewsletter']['scheduleEnabled'] == 'yes' && isset($template_info) ) {  ?>
            <?php foreach($template_info as $template) {
                if($template['name'] == null) { 
                    $template['name']= '';
                }
            ?>
            <tr> 
                <td id="template_name_periodic"><?php echo $template['name']; ?></td>
                <td style="color:#666; overflow-wrap: break-word;"><?php echo  $template['link']; ?></td>
            <tr>
            
            <?php } ?> 
            <?php } ?>
        </tbody>
    </table>
</div>
<!-- CronModal -->
<div class="modal fade" id="cronModal" tabindex="-1" role="dialog" aria-labelledby="cronModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $text_close; ?></span></button>
        <h4 class="modal-title" id="cronModalLabel"><?php echo $text_schedule_cron; ?></h4>
      </div>
      <div class="modal-body" id="cronModalBody">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $text_close; ?></button>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() { 
$('#fixedDate').datetimepicker({ pickTime: false, format: 'DD/MM/YYYY' });
$('.timepicker').datetimepicker({ pickDate: false, format: 'HH:mm' });
	 
  $('#cronSelector').cron({
      initial: "<?php if(!empty($data['AutomatedNewsletter']['periodicCronValue'])) echo $data['AutomatedNewsletter']['periodicCronValue']; else echo "* * * * *";  ?>",
      onChange: function() {
        $('input[name="AutomatedNewsletter[periodicCronValue]"]').val($(this).cron("value"));    
    },
  });
 template_name_periodic = $("#scheduled_template_periodic :selected").text();
  $('#template_name_periodic').text(template_name_periodic);  
});
</script>