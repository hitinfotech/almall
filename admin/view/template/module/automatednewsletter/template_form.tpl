<a class="btn btn-link backToList"><i class="icon-chevron-left" style="font-size:10px"></i> <?php echo $text_back; ?></a>
<div class="tabbable"> 
  <div class="tab-navigation">
    <ul class="nav nav-tabs mainMenuTabs">
      <?php $class="active";  foreach ($languages as $language) { ?>
          <li class="<?php echo $class; ?>"><a href="#tab-<?php echo $language['code']; ?>" data-toggle="tab"><img src="<?php echo $language['flag_url']; ?>"/></a></li>
      <?php  $class="";}?>
    </ul>
	<div class="tab-buttons">
		<span data-toggle="tooltip" data-placement="bottom" class="infoTooltip" title="<?php echo $send_now_info; ?>"><i class='icon-info-sign'></i>&nbsp;</span>
	   	<button id="sendMailToSpecifiedRecipients" class="btn btn-success" href="javascript:void(0)"><?php echo $send_now_button; ?></button> 
	</div>
  </div>
  <input type="hidden" value="<?php echo $template_id;?>" name="template_id">
  <div class="tab-content">
    <?php $class=" active"; foreach ($languages as $language) { ?>
      <div id="tab-<?php echo $language['code']; ?>" language-id="<?php echo $language['language_id']; ?>" class="row-fluid tab-pane<?php echo $class; ?> language">
        <div class="col-xs-3">
        	<h5><strong><?php echo $newsletter_information_text; ?></strong></h5>
            <span class="help"><?php echo $newsletter_information_text_help; ?><span>
        </div>
        <div class="col-xs-9">    
            <div class="emailLanguageWrapper">
                <label for="subject"><span class="required">*</span><?php echo $subject_text;?> :</label>
                <input type="text" class="subject form-control" style="width:20%; display:inline-block;" language-id="<?php echo $language['language_id']; ?>" name="template[<?php echo $language['language_id']?>][subject]" value="<?php if(!empty($template[$language['language_id']]['subject'])) { echo $template[$language['language_id']]['subject']; } ?>">
            <div class="input-append sendSpecificMail" style="margin-bottom:-11px !important; display:inline-block;" >
              <input type="email" class="form-control" style="width:60%; display:inline-block;" placeholder="Email" name="specificEmail" id="specificEmail" value="<?php echo $e_mail; ?>" />
              <button id="sendMessageSpecificMail" class="btn" type="button">Send test email</button>
            </div>
            	<div>
                	<textarea language-id="<?php echo $language['language_id']; ?>" id="message_<?php echo $language['language_id']; ?>" name="template[<?php echo $language['language_id'];?>][content]"><?php if(!empty($template[$language['language_id']]['content'])) { echo $template[$language['language_id']]['content']; } else { echo $default_template; } ?></textarea>
            	</div>
            </div>
        </div>
      </div>
    <?php $class="";} ?>
  </div>
</div>
<script type="text/javascript">
current_template_id = "<?php echo isset($current_template_id)?$current_template_id:0; ?>";
ckeditors={};
<?php foreach ($languages as $language) { ?>
	ckeditors["<?php echo $language['code'];?>"] = $("#message_<?php echo $language['language_id']; ?>");
	$("#message_<?php echo $language['language_id']; ?>").summernote({height: 300});
<?php } ?>
  $('button[data-event=\'showImageDialog\']').attr('data-toggle', 'image').removeAttr('data-event');
</script>