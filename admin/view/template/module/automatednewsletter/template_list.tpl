<div class="templateList">
		<button id="createNewTemplate" class="btn btn-warning pull-right" href="#" style="margin:0px 0 10px 5px"><?php echo $create_new_template; ?></button>
		<a class="btn btn-danger deleteTemplate pull-right">Delete</a>
  	<table class="table">
	    <thead>
	      <tr>
	      	<td width="15px"><input type="checkbox" name="selectAll"></td>
	        <td class="left">
	          <?php if ($sort == 'subject') { ?><a href="<?php echo $sort_subject ?>" class="<?php echo strtolower($order); ?>"><?php echo $newsletter_subject; ?></a>
	              <?php } else { ?><a href="<?php echo $sort_subject; ?>"><?php echo $newsletter_subject; ?></a><?php } ?>
	          </td>
	        <td class="left">
	          <?php if ($sort == 'time_added') { ?><a href="<?php echo $sort_time_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $last_modified; ?></a>
	              <?php } else { ?><a href="<?php echo $sort_time_added; ?>"><?php echo $last_modified; ?></a><?php } ?>
	        </td>
	      </tr>
	    </thead> 
	    <?php if(!empty($templates)){ ?>
	    <?php foreach ($templates as $template) { ?>
	    <?php if($template['language_id'] != $language_id) continue; ?>
	    <tr data-link="index.php?route=<?php echo $modulePath; ?>/templateForm&template_id=<?php echo $template['template_id']; ?>&token=<?php echo $token; ?>"> 
		  <td>
	        <input type="checkbox" name="deleteTemplate" value="<?php echo $template['template_id'];?>">
	      </td>
	      <td class="left templateLoader"><?php echo $template['subject']; ?></td>
	      <td class="left templateLoader"><?php echo date("d.m.Y/H:i:s", $template['time_added']); ?></td>
	      <tr>
	    <?php }} else {?>
	    	<tr><td class="center" colspan="3"><?php echo $text_no_results; ?></td></tr>
	    <?php } ?>
	</table>
  	<div class="pagination">
    <?php echo $pagination; ?>
  </div>
</div>
