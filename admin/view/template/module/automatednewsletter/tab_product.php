<table id="mainSettings" class="table mailForm">
  <tbody>
  	<tr>
    	<td class="col-xs-3">
       	 	<h5><span class="required">* </span><strong><?php echo $count_of_products_per_row; ?></strong></h5>   
    	</td> 
        <td class="col-xs-9">
            <div class="col-xs-4">
                <div class="input-group">
                    <input type="text" value="<?php if(!empty($data['AutomatedNewsletter']['countOfProductsPerRow'])) echo (int)$data['AutomatedNewsletter']['countOfProductsPerRow']; else echo 5; ?>" class="form-control brSmallField" name="AutomatedNewsletter[countOfProductsPerRow]">
                    <span class="input-group-addon"><?php echo $text_products;?></span>
                 </div>
             </div>
        </td>
    </tr>
    <tr>
		<td class="col-xs-3">
        	<h5><span class="required">* </span><strong><?php echo $select_currency; ?></strong></h5>    
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $select_currency_help; ?></span>
		</td>
    	<td class="col-xs-9">
        	<div class="col-xs-4">
                <select name="AutomatedNewsletter[config_currency]"  class="form-control">
                    <?php foreach ($currencies as $key => $currency) { ?>
                    <option value="<?php echo $currency['code']?>" <?php echo isset($data['AutomatedNewsletter']['config_currency']) && $data['AutomatedNewsletter']['config_currency'] == $currency['code'] ?'selected':'' ?>><?php echo $currency['title']; ?></option>
                    <?php }?>
                 </select>
        	</div>
      	</td>
    </tr>
    <tr>
		<td class="col-xs-3">
        	<h5><span class="required">* </span><strong><?php echo $product_image_size; ?></strong></h5>    
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $product_image_size_help; ?></span>
        </td>
		<td class="col-xs-9">
        	<div class="col-xs-4">
        		<div class="input-group">
         			<input type="text" value="<?php if(!empty($data['AutomatedNewsletter']['productImageWidth'])) echo (int)$data['AutomatedNewsletter']['productImageWidth']; else echo 100; ?>" class="form-control brSmallField" name="AutomatedNewsletter[productImageWidth]">
          			<span class="input-group-addon">X</span>
          			<input type="text" value="<?php if(!empty($data['AutomatedNewsletter']['productImageHeight'])) echo (int)$data['AutomatedNewsletter']['productImageHeight']; else echo 100; ?>" class="form-control brSmallField" name="AutomatedNewsletter[productImageHeight]">
        		</div>
        	</div>
        </td>
    </tr>
    <tr>
        <td class="col-xs-3">
            <h5><span class="required">* </span><strong><?php echo $new_products_for_last; ?></strong></h5> 
        </td>        
        <td class="col-xs-9">
      	<div class="col-xs-12" style="padding:0px">
             <div class="col-xs-2">
                <div class="input-group">
                    <input type="text" value="<?php if(!empty($data['AutomatedNewsletter']['newProductsLastDays'])) echo (int)$data['AutomatedNewsletter']['newProductsLastDays']; else echo 7; ?>" class="form-control brSmallField" name="AutomatedNewsletter[newProductsLastDays]">
                    <span class="input-group-addon"><?php echo $text_days; ?></span> 
                 </div>
             </div>
            <span class="product-limit-label"><?php echo $text_limit; ?> </span>
            <div class="col-xs-2">
                <div class="input-group">
                    <input type="text" value="<?php if(!empty($data['AutomatedNewsletter']['newProductsLastDaysLimit'])) echo $data['AutomatedNewsletter']['newProductsLastDaysLimit']; else echo 5; ?>" class="form-control brSmallField" name="AutomatedNewsletter[newProductsLastDaysLimit]">
                    <span class="input-group-addon"><?php echo $text_products; ?></span>
                </div>
            </div>
       	</div>
        <div class="col-xs-5" style="margin:10px 0;">
            	<span style="display:inline-block; width:70px; padding:2px;"><?php echo $text_from; ?></span>
                 <select name="AutomatedNewsletter[manufacturer]" id="manufac" class="form-control">
                        <option value="all_manufacturers" <?php if(!empty($data['AutomatedNewsletter']['manufacturer']) && $data['AutomatedNewsletter']['manufacturer'] == 'all_manufacturers') echo 'selected="selected"'?>><?php echo $all_manufacturers; ?></option>
                        <option value="selected_manufacturers" <?php if(!empty($data['AutomatedNewsletter']['manufacturer']) && $data['AutomatedNewsletter']['manufacturer'] == 'selected_manufacturers') echo 'selected="selected"'?>><?php echo $selected_manufacturers; ?></option>
                 </select>
         </div>
         <div class="specific-manufacturers col-xs-4" >  
			<div clas="input-group">
				<input type="text" name="specific-manufacturers" value="" placeholder="Specific manufacturer..." class="form-control" />
                 <div id="manufacturer" class="well well-sm">
                     <?php foreach($data['AutomatedNewsletter']['specific_manufacturer'] as $specific_manufacturer) {  ?>
                                 <div id="manufacturer-<?php echo $specific_manufacturer['manufacturer_id']; ?>">
                                    <i class="fa fa-minus-circle"></i><?php echo $specific_manufacturer['name']; ?>
                                    <input type="hidden" name="AutomatedNewsletter[specific_manufacturer][]" value="<?php echo $specific_manufacturer['manufacturer_id']; ?>" />
                                </div>
                            <?php } ?>
                        </div>
                     </div>
                </div> 
           </div>       
        </td>
    </tr>
    <tr>
        <td class="col-xs-3">
        	<h5><span class="required">* </span><strong><?php echo $best_deals; ?></strong></h5> 
        </td>   
        <td class="col-xs-9">
        	<div class="col-xs-2">
        		<div class="input-group">
        			<input type="text" value="<?php if(!empty($data['AutomatedNewsletter']['bestsellerLastDays'])) echo (int)$data['AutomatedNewsletter']['bestsellerLastDays']; else echo 7; ?>" class="form-control brSmallField" name="AutomatedNewsletter[bestsellerLastDays]">
       				 <span class="input-group-addon"><?php echo $text_days; ?></span> 
             	</div>
			</div>        
       		<span class="product-limit-label"><?php echo $text_limit; ?></span>
        	<div class="col-xs-2">
        		<div class="input-group">
            		<input type="text" value="<?php if(!empty($data['AutomatedNewsletter']['bestsellerLastDaysLimit'])) echo $data['AutomatedNewsletter']['bestsellerLastDaysLimit']; else echo 5; ?>" class="form-control brSmallField" name="AutomatedNewsletter[bestsellerLastDaysLimit]">
            		<span class="input-group-addon"><?php echo $text_products; ?></span> 
                </div>
        	</div>
        </td>
    </tr>
    <tr>
        <td class="col-xs-3">
        	<h5><span class="required">* </span><strong><?php echo $specials_for_next; ?></strong></h5>    
        <td class="col-xs-9">
        	<div class="col-xs-2">
            </div>
        	<span class="product-limit-label"><?php echo $text_limit; ?></span>
        	<div class="col-xs-2">
        		<div class="input-group">
                    <input type="text" value="<?php if(!empty($data['AutomatedNewsletter']['specialsNextDaysLimit'])) echo $data['AutomatedNewsletter']['specialsNextDaysLimit']; else echo 5; ?>" class="form-control brSmallField" name="AutomatedNewsletter[specialsNextDaysLimit]">
                    <span class="input-group-addon"><?php echo $text_products; ?></span> 
                 </div>
            </div>
        </td>
    </tr>
	<tr>
		<td class="col-xs-3">
			<h5><strong><?php echo $entry_product; ?></strong></h5>    
			<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $entry_product_help; ?></span></td>
        <td class="col-xs-9">
      		<div class="col-xs-4">
        		<div clas="input-group">
					<input type="text" name="product" value="" class="form-control" placeholder="Type a product name here.." />
     				<div id="custom-product" class="well well-sm">
            			<?php foreach ($data['AutomatedNewsletter']['custom_product'] as $custom_product) { ?>
            				<div id="custom-product<?php echo $custom_product['product_id']; ?>">
             					<i class="fa fa-minus-circle"></i><?php echo $custom_product['name']; ?>
              					<input type="hidden" name="AutomatedNewsletter[custom_product][]" value="<?php echo $custom_product['product_id']; ?>" />
            				</div>
            			<?php } ?>
          			</div>
        		</div>
        	</div>
		</td>
    </tr>
  </tbody>
</table>
<script>
$(document).ready(function() {
if($( "#manufac" ).val() == 'selected_manufacturers') {
	$( ".specific-manufacturers" ).show();
} else {
	$( ".specific-manufacturers" ).hide();
}
});

$( "#manufac" ).change(function() {
if($( "#manufac" ).val() == 'selected_manufacturers') {
	$( ".specific-manufacturers" ).show();
} else {
	$( ".specific-manufacturers" ).hide();
}
});
</script>                 
                 
