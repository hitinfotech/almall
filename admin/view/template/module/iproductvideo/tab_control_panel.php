<ul class="nav nav-pills storeTabs">
	<li class="disabled"><a>Your OpenCart Stores:</a></li>
<?php $i=0; foreach ($stores as $store) : ?>
	<li <?php echo ($i == 0) ? 'class="active"' : ''; ?>> <a href="#tab-store_<?php echo $store['store_id']; ?>" data-toggle="pill"><?php echo $store['name'] ?></a> </li>
<?php $i++; endforeach; ?>
</ul>
<div class="tab-content">
	<?php $i=0; foreach ($stores as $store) : ?>
	<div id="tab-store_<?php echo $store['store_id']; ?>" class="tab-pane store-pane <?php echo ($i == 0) ? 'active' : ''; ?>" data-store-id="<?php echo $store['store_id']; ?>" data-store-name="<?php echo $store['name']; ?>">
		<div class="general-content">
			<table class="form">
				<tr>
					<td><label><span class="required">*</span> <?php echo $entry_code; ?></label></td>
					<td>
						<select class="form-control store_settings_toggle" data-rel="store_<?php echo $store['store_id']; ?>_settings" name="iProductVideo[<?php echo $store['store_id']; ?>][Enabled]">
							<option value="true" <?php echo (!empty($data['iProductVideo'][$store['store_id']]['Enabled']) && $data['iProductVideo'][$store['store_id']]['Enabled'] == 'true') ? 'selected=selected' : ''?>><?php echo $text_enabled; ?></option>
							<option value="false" <?php echo (empty($data['iProductVideo'][$store['store_id']]['Enabled']) || $data['iProductVideo'][$store['store_id']]['Enabled'] == 'false') ? 'selected=selected' : ''?>><?php echo $text_disabled; ?></option>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="tabbable tabs-left" id="store_<?php echo $store['store_id']; ?>_settings" style="display:none;">
			<ul class="nav nav-tabs video-list">
				<li><a class="addNewVideo"><i class="fa fa-plus"></i> Add New Video</a></li>
				<?php if (!empty($data['iProductVideo']) && !empty($data['iProductVideo'][$store['store_id']]['Videos'])) { ?>
					<?php foreach ($data['iProductVideo'][$store['store_id']]['Videos'] as $video_id => $video) { ?>
					<li> <a href="#store_<?php echo $store['store_id']; ?>_video_<?php echo $video_id; ?>" data-toggle="tab" data-video-id="<?php echo $video_id; ?>"> Video <?php echo $video_id; ?> <i class="fa fa-minus removeVideo"></i>
					<input type="hidden" name="iProductVideo[<?php echo $store['store_id']; ?>][Videos][<?php echo $video_id; ?>]" value="<?php echo $video_id; ?>" />
					</a> </li>
					<?php } ?>
				<?php } ?>
			</ul>
			<div class="tab-content video-settings">
				<?php
					if (!empty($data['iProductVideo']) && !empty($data['iProductVideo'][$store['store_id']]['Videos'])) {
						foreach ($data['iProductVideo'][$store['store_id']]['Videos'] as $video_id => $video) {
							require(DIR_APPLICATION.'view/template/module/iproductvideo/iproductvideo_settings.tpl');
						}
					}
				?>
			</div>
		</div>
	</div>
	<?php $i++; endforeach; ?>
	<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $maxSize; ?>" /> 
</div>
<script type="text/javascript">
function switchStoreSettings() {
	$('.store_settings_toggle').each(function(){
		if ($(this).val() == 'true') {
			$('#' + $(this).attr('data-rel')).fadeIn();
		} else {
			$('#' + $(this).attr('data-rel')).fadeOut();	
		}
		
		$('.store-pane.active .video-list > li:nth-child(2) a').tab('show');
	});
}

function addNewVideo() {
	video_count 		= $('.store-pane.active .video-list li:last-child > a').data('video-id') + 1 || 1;
	active_store_id 	= $('.store-pane.active').data('store-id');
	active_store_name 	= $('.store-pane.active').data('store-name');
	
	var ajax_data = {};
	
	ajax_data.token = '<?php echo $token; ?>';
	ajax_data.active_store_id = active_store_id;
	ajax_data.video_count = video_count;

	$.ajax({
		url: 'index.php?route=module/iproductvideo/get_iproductvideo_settings',
		data: ajax_data,
		dataType: 'html',
		beforeSend: function() {
			NProgress.start();
		},
		success: function(settings_html) {

			$('.store-pane.active .video-settings').append(settings_html);

			if (video_count == 1) { $('a[href="#store_' + active_store_id + '_video_' + video_count + '"]').tab('show'); }
			
			$('.bfh-selectbox').each(function() {
				var $selectbox;
				$selectbox = $(this);
				$selectbox.bfhselectbox($selectbox.data());
			});

			NProgress.done();

			video_tpl 	= '<li>';
			video_tpl 	+= '<a href="#store_' + active_store_id + '_video_' + video_count + '" data-toggle="tab" data-store-name="' + active_store_name + '" data-video-id="' + video_count + '">';
			video_tpl 	+= 'Video ' + video_count;
			video_tpl 	+= '<i class="fa fa-minus removeVideo"></i>';
			video_tpl 	+= '<input type="hidden" name="iProductVideo[' + active_store_id + '][Videos][' + video_count + ']" value="' + video_count + '"/>';
			video_tpl 	+= '</a>';
			video_tpl	+= '</li>';

			$('.store-pane.active .video-list').append(video_tpl);
			$('.store-pane.active .video-list').children().last().children('a').trigger('click');
		}
	});
}

function removeVideo() {
	active_store_name = $('.store-pane.active').data('store-name');
	video_tab_link = $(event.target).parent();
	video_tab_pane_id = video_tab_link.attr('href');
	
	var confirmVideoRemove = confirm('Are you sure you want to remove ' + video_tab_link.text().trim() + ' from ' + active_store_name);
	
	if (confirmVideoRemove == true) {
		video_tab_link.parent().remove();
		$(video_tab_pane_id).remove();
		
		if ($('.store-pane.active .video-list').children().length > 1) {
			$('.store-pane.active .video-list > li:nth-child(2) a').tab('show');
		}
	}
}

function refreshUploadedVideosLists() {
	$('.uploaded_videos_list').each(function(index, element) {
		var store_id 	= $(this).attr('data-store-id');
		var video_id 	= $(this).attr('data-video-id');
		var language_id = $(this).attr('data-language-id');

		$(this).load('index.php?route=module/iproductvideo/load_uploaded_videos&store_id=' + store_id + '&video_id=' + video_id + '&language_id=' + language_id + '&token=<?php echo $token; ?>', function(){
			setTimeout(function(){
				$('.video_editor .btn-add-text .fa.fa-cog').removeClass('fa-spin');
			}, 1000);	
		});
	});

	NProgress.done();
}

$(document).ready(function() {
	switchStoreSettings();
	
	$('.store_settings_toggle').change(function(){ switchStoreSettings(); });
	
	// Add New Video
	$('.addNewVideo').click(function(e) { addNewVideo(); });
	
	// Remove Video
	$(document).on('click', '.removeVideo', function(e) { removeVideo(); });
	
	$(document).on('click', '.colorSelector', function(){
		$('#' + $(this).attr('data-rel')).show();
	});

	// Settings Fade
	$(document).on('change', '.toggle-fade-next', function(){
		if ($(this).val() == 'all') {
			$(this).next().fadeOut();
		} else {
			$(this).next().fadeIn();	
		}
	});
	
	// Products
	$(document).on('focus', '.products-autocomplete', function() {
		$(this).autocomplete({
			delay: 500,
			source: function(request, response) {
				$.ajax({
					url: 'index.php?route=module/iproductvideo/autocomplete_product&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
					dataType: 'json',
					success: function(json) {		
						response($.map(json, function(item) {
							return {
								label: item.name,
								value: item.product_id
							}
						}));
					}
				});
			}, 
			select: function(event, ui) {
				store_id 	= $(this).attr('data-store-id');
				video_id 	= $(this).attr('data-video-id');
				language_id = $(this).attr('data-language-id');
				$_video_nb		= 'iProductVideo[' + store_id + '][Videos][' + video_id + '][' + language_id + ']';

				$('#product-store_' + store_id + '_video_' + video_id + '_language_' + language_id + '_' + ui.item.value).remove();
				
				$('#product-store_' + store_id + '_video_' + video_id + '_language_' + language_id + '_' + ui.item.value).remove();
				
				$('#product-store_' + store_id + '_video_' + video_id + '_language_' + language_id).append(
					'<div id="product-store_' + store_id + '_video_' + video_id + '_language_' + language_id + '_' + ui.item.value + '">' + ui.item.label + '<i class="fa fa-minus removeIcon"></i>' +
						'<input type="hidden" name="' + $_video_nb + '[LimitProductsList][]" value="' + ui.item.value + '" />' +
					'</div>');
						
				return false;
			},
			focus: function(event, ui) {
				return false;
			}
		});
	});
	
	// Scrollbox
	$(document).on('click', '.scrollbox .removeIcon', function() {
		$(this).parent().remove();
	});
	
	// File Upload Handler
	$(document).on('click', '.video-upload input[type="file"]', function(e) {
		upload_param = $(this).attr('name');
		upload_progress_wrapper = $(this).attr('data-rel');
		
		$(this).fileupload({
			dropZone: $(this),
			url:'index.php?route=module/iproductvideo/upload_video' +
				'&token=<?php echo $token; ?>' + 
				'&upload_param=' + upload_param,
			dataType: 'json',
			submit: function (e, data) {
				$(upload_progress_wrapper).addClass('file-added');
			},
			done: function (e, data) {
				if (data.response().result.error) {
					$(upload_progress_wrapper).removeClass('file-added');
					$(upload_progress_wrapper).removeClass('file-uploaded');
					$('.progress-bar', upload_progress_wrapper).css('width','0');
					alert(data.response().result.error);
				} else {
					$(upload_progress_wrapper).attr('data-status', data.textStatus);
					$(upload_progress_wrapper).addClass('file-uploaded');
					
					setTimeout(function(){
						$(upload_progress_wrapper).removeClass('file-added');
						$(upload_progress_wrapper).removeClass('file-uploaded');
						$('.progress-bar', upload_progress_wrapper).css('width','0');
					}, 3000);
				}
				
				refreshUploadedVideosLists();
			},
			progress: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				$('.progress-bar', upload_progress_wrapper).css('width', progress + '%');
			}
		});
	});
	
	// Delete Video File
	$(document).on('click', '.video_editor .btn-delete-video', function(){
		var ajax_data = {};
	
		ajax_data.token = '<?php echo $token; ?>';
		ajax_data.video = $(this).attr('data-video');
		
		var confirm_delete = confirm('Are you sure that you want to permanently delete the video: ' + ajax_data.video);
		
		if (confirm_delete == true) {
			$.ajax({
				url: 'index.php?route=module/iproductvideo/delete_video',
				data: ajax_data,
				dataType: 'json',
				success: function(json) {
					if (json != null && json.error) { alert(json.error); }
					refreshUploadedVideosLists();
				}
			});
		}
	});
});
</script>