<?php 
	// ID Base For Video Navigation
	$_id	= 'store_' . $store['store_id'] . '_video_' . $video_id;
?>
<div id="<?php echo $_id; ?>" class="tab-pane">
	<ul class="nav nav-pills languageTabs">
		<li class="disabled"><a>Video Settings For Language:</a></li>
		<?php $i=0; foreach ($languages as $language) : ?>
			<?php 
				// ID Base Per Language
				$_id	= 'store_' . $store['store_id'] . '_video_' . $video_id . '_language_' . $language['language_id'];
			?>
			<li <?php echo ($i == 0) ? 'class="active"' : ''; ?>> <a href="#<?php echo $_id; ?>" data-toggle="pill">
					<?php echo $language['name'] ?>
					<?php if ($language['flag']) { ?>
						<img src="<?php echo $language['flag']; ?>" title="<?php echo $language['name']; ?>"  alt="<?php echo $language['name']; ?>" />
					<?php } ?>
			</a></li>
		<?php $i++; endforeach; ?>
	</ul>

	<hr />

	<div class="tab-content">
		<!-- Videos Settings Per Language -->
		<?php $i=0; foreach ($languages as $language) : ?>
			<?php 
				// ID Base Per Language
				$_id	= 'store_' . $store['store_id'] . '_video_' . $video_id . '_language_' . $language['language_id'];
				
				// Video Name Base Per Language
				$_vnb	= 'iProductVideo[' . $store['store_id'] . '][Videos][' . $video_id . '][' . $language['language_id']. ']';
				
				// Video
				if (!empty($data['iProductVideo'][$store['store_id']]['Videos'][$video_id][$language['language_id']])) {
					$_video	= $data['iProductVideo'][$store['store_id']]['Videos'][$video_id][$language['language_id']];
				} else {
					$_video	= NULL;
				}
			?>

			<div id="<?php echo $_id; ?>" class="tab-pane <?php echo ($i == 0) ? 'active' : ''; ?>">
				<div class="video_editor container-fluid"><div class="row">
					<div class="col-xs-6">
						<label><h3>1. Choose video</h3></label>
						<div class="video_editor_wrap">
							<div class="iproductvideo-tab-container">
								<div class="iproductvideo-tab-menu">
									<div class="btn-group" data-toggle="buttons">
										<label class="btn btn-default <?php echo ((!empty($_video['VideoType']) && $_video['VideoType'] == 'internet') || empty($_video['VideoType'])) ? 'active' : '' ?>">
											<input type="radio" name="<?php echo $_vnb; ?>[VideoType]" value="internet" <?php echo !empty($_video['VideoType']) && $_video['VideoType'] == 'internet' ? 'checked="checked"' : '' ?>>Internet Video
										</label>
										<label class="btn btn-default <?php echo !empty($_video['VideoType']) && $_video['VideoType'] == 'uploaded' ? 'active' : '' ?>">
											<input type="radio" name="<?php echo $_vnb; ?>[VideoType]" value="uploaded" <?php echo !empty($_video['VideoType']) && $_video['VideoType'] == 'uploaded' ? 'checked="checked"' : '' ?>>Uploaded Video
										</label>
									</div>
								</div>
								<div class="iproductvideo-tab">
									<div class="iproductvideo-tab-content active">
										<div class="row">
											<div class="col-xs-12 form-group">
												<label>Video URL:</label>
												<input class="form-control" placeholder="http://" type="text"  name="<?php echo $_vnb; ?>[VideoURL]" value="<?php echo (!empty($_video['VideoURL'])) ? $_video['VideoURL']  : ''; ?>"/><br />
												<span class="text-info">YouTube and Vimeo links are currently supported</span>
											</div>
										</div>
									</div>
									<div class="iproductvideo-tab-content">
										<div class="row">
											<div class="col-xs-12 form-group">
												<label>Uploaded Videos:</label>
												<div class="video_list uploaded_videos_list well" data-store-id="<?php echo $store['store_id']; ?>" data-video-id="<?php echo $video_id; ?>" data-language-id="<?php echo $language['language_id']; ?>">
													<?php if (!empty($uploaded_videos)) { ?>
														<?php require(DIR_APPLICATION.'view/template/module/iproductvideo/videos_loop.tpl'); ?>
													<?php } else { ?>
														<span> No uploaded videos.<br /> Use the upload form below<br /> or put your videos in this server directory<br /> <b>/vendors/iproductvideo/uploaded_videos</b> </span>
													<?php } ?>
												</div>
											</div>
										</div>

										<!-- HR --><hr />
							
										<div class="video-upload">
											<div id="<?php echo $_id; ?>_videouploadprogress" class="progress progress-striped active"><div class="progress-bar"></div></div>
											<a class="btn btn-primary">Upload Video
												<input class="form-control" id="<?php echo $_id; ?>_videoupload" type="file" name="<?php echo $_id; ?>_videoupload" data-rel="#<?php echo $_id; ?>_videouploadprogress" accept="video/*">
											</a>
											<span class="fileSizeInfo">&nbsp;&nbsp;<?php echo $text_max_size; ?> <?php echo $maxSizeReadable; ?></span>
											<a data-toggle="modal" class="needMoreSize" data-target="#help-modal"><?php echo $text_max_size_learn; ?></a>
										</div>
									</div>
									<hr />
									<div class="row">
										<div class="col-xs-6">
											<label class="checkbox">
												<input type="checkbox" name="<?php echo $_vnb; ?>[MainImage]" value="1" <?php echo (!empty($_video['MainImage']) && $_video['MainImage'] == 1) ? 'checked="checked"' : ''; ?> />
												Main product image<br />
												<span class="help">Check this if you want to replace the main product image with the video.</span>
											</label>
										</div>
										<div class="col-xs-6">
											<div class="row">
												<div class="col-xs-6"><label for="<?php echo $_id; ?>_videosortorder">Video Sort Order</label></div>
												<div class="col-xs-6"><input id="<?php echo $_id; ?>_videosortorder" type="number" min="0" class="form-control form-control-xs" name="<?php echo $_vnb; ?>[SortOrder]" value="<?php echo (!empty($_video['SortOrder'])) ? $_video['SortOrder']  : 0; ?>" /></div>
											</div>
											<span class="help">This is the position in which the video will be placed between the product's additional images. If the <b>Main product image</b> is checked, this feature is disabled.</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<label><h3>2. Assign video to products</h3></label>
						<div class="row">
							<div class="col-xs-12">
								<label><?php echo $entry_video_limit_products; ?></label>
								<select class="form-control toggle-fade-next" name="<?php echo $_vnb; ?>[LimitProducts]">
									<option value="all"<?php echo !empty($_video['LimitProducts']) && $_video['LimitProducts'] == 'all' ? ' selected="selected"' : ''; ?>><?php echo $entry_all_products; ?></option>
									<option value="specific"<?php echo !empty($_video['LimitProducts']) && $_video['LimitProducts'] == 'specific' ? ' selected="selected"' : ''; ?>><?php echo $entry_following_products; ?></option>
								</select>
								<div <?php echo (empty($_video['LimitProducts']) || (!empty($_video['LimitProducts']) && $_video['LimitProducts'] == 'all')) ? ' style="display:none;"' : ''; ?>>
									<input class="form-control products-autocomplete" placeholder="Autocomplete.." data-store-id="<?php echo $store['store_id']; ?>" data-video-id="<?php echo $video_id; ?>" data-language-id="<?php echo $language['language_id']; ?>" type="text" /><br />
									<div id="product-<?php echo $_id; ?>" class="scrollbox">
										<?php if (!empty($products)) { ?>
											<?php foreach ($products[$store['store_id']]['Videos'][$video_id][$language['language_id']] as $product) { ?>
											<div id="product-<?php echo $_id; ?>_<?php echo $product['product_id']; ?>"> <?php echo $product['name']; ?><i class="fa fa-minus removeIcon"></i>
												<input type="hidden" name="<?php echo $_vnb; ?>[LimitProductsList][]" value="<?php echo $product['product_id']; ?>" />
											</div>
											<?php } ?>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div></div>
			</div>
		<?php $i++; endforeach; ?>
	</div>
</div>