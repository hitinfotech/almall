<?php 
	// ID Base = $_id;
	
	// Video Name Base = $_vnb;
	
	// Video = $_video;
?>

<?php foreach ($uploaded_videos as $video_index => $video) { ?>
	<label class="video_wrap" for="<?php echo $_id; ?>_localvideo_<?php echo $video_index; ?>" title="<?php echo $video['name']; ?>">
		<?php if (strpos($video['src'], 'uploaded_videos') != false) { ?>
			<div class="btn-delete-video" data-video="<?php echo $video['name']; ?>">Delete this video&nbsp;&nbsp;<i class="fa fa-remove"></i></div>
		<?php } ?>
		<input type="radio" name="<?php echo $_vnb; ?>[LocalVideo]" id="<?php echo $_id; ?>_localvideo_<?php echo $video_index; ?>" value="<?php echo $video['path']; ?>" <?php echo !empty($_video['LocalVideo']) && $_video['LocalVideo'] == $video['path'] ? 'checked="checked"' : '' ?>/>
		<div class="video" data-video-path="<?php echo $video['path']; ?>" data-store-id="<?php echo $store['store_id']; ?>" data-label-id="<?php echo $video_index; ?>"  data-language-id="<?php echo $language['language_id']; ?>" data-rel="<?php echo $_id; ?>_videodata">
			<video>
				<source src="<?php echo $video['src']; ?>" type="<?php echo $video['type']; ?>">
				Your browser does not support the video tag.
			</video>
		</div>
	</label>
<?php } ?>