<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
          <div class="pull-right">
              <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
          </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
                <h3 class="panel-title pull-right"><a href='<?php echo $template_sheet; ?>' > Template Sheet for filling</a></h3>
            </div>
            <div class="panel-body">
                <form action="" method="post" enctype="multipart/form-data" id="form-store">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                    <td class="text-left"><?php echo $column_seller_name; ?></td>
                                    <td class="text-left"><?php echo $column_sheet_link; ?></td>
                                    <td class="text-left"><?php echo $column_username; ?></td>
                                    <td class="text-left"><?php echo $column_date_added; ?></td>
                                    <td class="text-right"><?php echo $column_action; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($sheets_added) { ?>
                                    <?php foreach ($sheets_added as $sheet) { ?>
                                        <tr>
                                            <td class="text-center"><?php if (in_array($sheet['sheet_id'], $selected)) { ?>
                                                    <input type="checkbox" name="selected[]" value="<?php echo $sheet['sheet_id']; ?>" checked="checked" />
                                                <?php } else { ?>
                                                    <input type="checkbox" name="selected[]" value="<?php echo $sheet['sheet_id']; ?>" />
                                                <?php } ?></td>
                                                <td class="text-left"><?php echo $sheet['companyname']; ?></td>
                                                <td class="text-left"><a href="<?php echo $sheet['sheet_link']; ?>" target="_blank" > click here</a></td>
                                                <td class="text-left"><?php echo $sheet['username']; ?></td>
                                                <td class="text-left"><?php echo $sheet['date_added']; ?></td>
                                                <td class="text-right">
                                                  <a href="<?php echo $sheet['enable']; ?>" data-toggle="tooltip" title="<?php echo $button_enable; ?>" class="btn btn-primary"><i class="fa fa-thumbs-up"></i></a>
                                                  <a href="<?php echo $sheet['disable']; ?>" data-toggle="tooltip" title="<?php echo $button_disable; ?>" class="btn btn-primary"><i class="fa fa-thumbs-down"></i></a>
                                                </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
