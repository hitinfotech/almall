<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-special" data-toggle="tab"><?php echo $tab_special; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-special">
                            <div class="table-responsive">
                                <table id="special" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td width="5%" class="text-left"><?php echo $column_id; ?></td>
                                            <td width="15%" class="text-left"><?php echo $column_brand; ?></td>
                                            <td width="20%" class="text-left"><?php echo $column_category; ?></td>
                                            <?php /* <td width="15%" class="text-left"><?php echo $column_country; ?></td> */ ?>
                                            <td width="5%" class="text-left"><?php echo $column_percent; ?></td>

                                            <td width="12%" class="text-left"><?php echo $column_date_start; ?></td>
                                            <td width="12%" class="text-left"><?php echo $column_date_end; ?></td>
                                            <td width="8%" class="text-left"><?php echo $column_done; ?></td>
                                            <td width="10%" class="text-left"><?php echo $column_user; ?></td>
                                            <td width="12%" class="text-left"><?php echo $column_date_added; ?></td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $special_row = 0; ?>
                                        <?php foreach ($product_specials as $product_special) { ?>
                                            <tr id="special-row<?php echo $special_row; ?>">
                                                <td class="text-left"><input disabled type="text" name="product_special[<?php echo $special_row; ?>][id]" value="<?php echo $product_special['id']; ?>" placeholder="<?php echo $column_id; ?>" class="form-control" /></td>
                                                <td class="text-left">
                                                    <a href="<?php echo $product_special['brand_url'] ?>"><?php echo $product_special['brand'] . " (" . $product_special['brand_id'] . ")"; ?></a>
                                                </td>
                                                <td class="text-left"><input disabled type="text" name="product_special[<?php echo $special_row; ?>][category_id]" value="<?php echo $product_special['category'] . " (" . $product_special['category_id'] . ")"; ?>" placeholder="<?php echo $column_category; ?>" class="form-control" /></td>
                                                <?php /*<td class="text-left"><input disabled type="text" name="product_special[<?php echo $special_row; ?>][country_id]" value="<?php echo $product_special['country'] . " (" . $product_special['country_id'] . ")"; ?>" placeholder="<?php echo $column_country; ?>" class="form-control" /></td> */ ?>
                                                <td class="text-left"><input disabled type="text" name="product_special[<?php echo $special_row; ?>][percent]" value="<?php echo $product_special['percent']; ?>" placeholder="<?php echo $column_percent; ?>" class="form-control" /></td>


                                                <td class="text-left"><input disabled type="text" name="product_special[<?php echo $special_row; ?>][date_start]" value="<?php echo $product_special['date_start']; ?>" placeholder="<?php echo $column_date_start; ?>" class="form-control" /></td>
                                                <td class="text-left"><input disabled type="text" name="product_special[<?php echo $special_row; ?>][date_end]"   value="<?php echo $product_special['date_end']; ?>"   placeholder="<?php echo $column_date_end; ?>"   class="form-control" /></td>

                                                <td class="text-left"><input disabled type="text" name="product_special[<?php echo $special_row; ?>][done]"   value="<?php echo $product_special['done']; ?>"   placeholder="<?php echo $column_done; ?>"   class="form-control" /></td>
                                                <td class="text-left"><input disabled type="text" name="product_special[<?php echo $special_row; ?>][user]" value="<?php echo $product_special['user']; ?>" placeholder="<?php echo $column_user; ?>" class="form-control" /></td>
                                                <td class="text-left"><input disabled type="text" name="product_special[<?php echo $special_row; ?>][date_added]" value="<?php echo $product_special['date_added']; ?>" placeholder="<?php echo $column_date_added; ?>" class="form-control" /></td>

                                                <td class="text-left"><button type="button" onclick="doremove(<?php echo $product_special['id']; ?>);" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                            </tr>
                                            <?php $special_row++; ?>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="9"></td>
                                            <td class="text-left"><button type="button" onclick="addSpecial();" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    function doremove(cron_number) {
        window.location = '<?php echo str_replace('&amp;', '&', $delete) ?>&cron_id=' + cron_number;
    }
//--></script>
<script type="text/javascript"><!--
    var special_row = <?php echo $special_row; ?>;
    function addSpecial() {
        html = '<tr id="special-row' + special_row + '">';
        html += '<td class="text-left"></td>';
        html += '  <td class="text-left"><select name="product_special[' + special_row + '][brand_id]" class="form-control">';
<?php foreach ($brands as $row) { ?>
            html += '<option value="<?php echo $row['brand_id']; ?>"><?php echo addslashes($row['name']); ?></option>';
<?php } ?>
        html += '  </select></td>';
        html += '  <td class="text-left"><select name="product_special[' + special_row + '][category_id]" class="form-control">';
<?php foreach ($categories as $row) { ?>
            html += '<option value="<?php echo $row['category_id']; ?>"><?php echo addslashes($row['name']); ?></option>';
<?php } ?>
        html += '  </select></td>';
        /*html += '<td class="text-left"><select name="product_special[' + special_row + '][country_id]" class="form-control">';
        html += '<option value="0">All Countries</option>';
<?php foreach ($countries as $country) { ?>
            html += '<option value="<?php echo $country['country_id']; ?>"><?php echo addslashes($country['name']); ?></option>';
<?php } ?>
        html += '  </select></td>';*/
        html += '<td class="text-left"><input  type="text" name="product_special[' + special_row + '][percent]" value="" placeholder="<?php echo $column_percent; ?>" class="form-control" /></td>';

        html += '<td class="text-left"><div class="input-group date"><input  type="text" name="product_special[' + special_row + '][date_start]" value="" data-date-format="YYYY-MM-DD" placeholder="<?php echo $column_date_start; ?>" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
        html += '<td class="text-left"><div class="input-group date"><input  type="text" name="product_special[' + special_row + '][date_end]"   value="" data-date-format="YYYY-MM-DD" placeholder="<?php echo $column_date_end; ?>"   class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
        html += '<td></td><td></td><td></td>';
        /* html+= '<td class="text-left"><input  type="text" name="product_special[' + special_row + '][done]"   value=""   placeholder="<?php echo $column_done; ?>"   class="form-control" /></td>';
         html+= '<td class="text-left"><input  type="text" name="product_special[' + special_row + '][user]" value="" placeholder="<?php echo $column_user; ?>" class="form-control" /></td>';
         html+= '<td class="text-left"><input  type="text" name="product_special[' + special_row + '][date_added]" value="" placeholder="<?php echo $column_date_added; ?>" class="form-control" /></td>';*/
        html += '<td class="text-left"><button type="button" onclick="$(\'#special-row' + special_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';
        $('#special tbody').append(html);

        $('.date').datetimepicker({
            pickTime: false
        });

        special_row++;
    }
    //--></script>

<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
    //--></script>
<?php echo $footer; ?>
