<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> 
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-search').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-search">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                    <td class="text-left" width="12%"><?php if ($sort == 'name') { ?>
                                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                        <?php } ?></td>
                                    <td class="text-center"><?php echo $column_keytype ?></td>
                                    <td class="text-center"><?php echo $column_url ?></td>
                                    <td class="text-center"><?php echo $column_country ?></td>
                                    <td class="text-center"><?php echo $column_language ?></td>
                                    <td class="text-center"><?php echo $column_date_added ?></td>
                                    <td class="text-center"  width="9%"><?php if ($sort == 'sort_order') { ?>
                                        <a href="<?php echo $sort_counter; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_counter; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_counter; ?>"><?php echo $column_counter; ?></a>
                                        <?php } ?></td>
                                    <td class="text-center"><?php echo $column_approved ?></td>
                                    <td class="text-right"><?php echo $column_action; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                        <td class="text-center"></td>
                                        <td class="text-left">
                                            <input type="text" name="filter_name" value="<?php echo $filter_name ?>" placeholder="<?php echo $filter_name; ?>"  id="filter-name" class="form-control" />
                                        </td>
                                        <td class="text-right"><select name="filter_keytype" id="filter-keytype" class="form-control">
                                                <option value="*" selected="selected">-- All --</option>
                                                <?php foreach ($keytypes as $row) { ?>
                                                    <?php if ($filter_keytype == $row) { ?>
                                                        <option value="<?php echo $row ?>" selected="selected"><?php echo $row; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $row ?>"><?php echo $row; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select></td>
                                        <td class="text-left">
                                            <input type="text" name="filter_url" value="<?php echo $filter_url ?>" placeholder="<?php echo $filter_url; ?>"  id="filter-url" class="form-control" disabled=""/>
                                        </td>
                                        <td class="text-left">
                                            <select name="filter_country_id" id="filter-country" class="form-control">
                                                <option value="*" selected="selected">-- All --</option>
                                                <?php foreach ($countries as $country) { ?>
                                                    <?php if ($country['country_id'] == $filter_country_id) { ?>
                                                        <option value="<?php echo $country['country_id'] ?>" selected="selected"><?php echo $country['name']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $country['country_id'] ?>"><?php echo $country['name']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td class="text-left"><select name="filter_language_id" id="filter-language" class="form-control">
                                                <option value="*" selected="selected">-- All --</option>
                                                <?php foreach ($languages as $language) { ?>
                                                    <?php if ($language['language_id'] == $filter_language_id) { ?>
                                                        <option value="<?php echo $language['language_id'] ?>" selected="selected"><?php echo $language['name']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $language['language_id'] ?>"><?php echo $language['name']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"><select name="filter_approved" id="filter-language" class="form-control">
                                                <option value="*"></option>
                                                <?php if ($filter_approved) { ?>
                                                    <option value="1" selected="selected"><?php echo $button_approve; ?></option>
                                                <?php } else { ?>
                                                    <option value="1"><?php echo $button_approve; ?></option>
                                                <?php } ?>
                                                <?php if (!$filter_approved && !is_null($filter_approved)) { ?>
                                                    <option value="0" selected="selected"><?php echo $button_disapprove; ?></option>
                                                <?php } else { ?>
                                                    <option value="0"><?php echo $button_disapprove; ?></option>
                                                <?php } ?>
                                            </select></td>
                                        <td class="text-right"><button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button></td>
                                    </tr>
                                    <?php if(!empty($searchlist)) { ?>
                                    <?php foreach ($searchlist as $search) { ?>
                                        <tr>
                                            <td class="text-center"><?php if (in_array($search['id'], $selected)) { ?>
                                                    <input type="checkbox" name="selected[]" value="<?php echo $search['id']; ?>" checked="checked" />
                                                <?php } else { ?>
                                                    <input type="checkbox" name="selected[]" value="<?php echo $search['id']; ?>" />
                                                <?php } ?></td>
                                            <td class="text-left">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a onclick="update(<?php echo $search['id'] ?>)" href="javascript:void(0);" data-toggle="tooltip" title="<?php echo $button_approve; ?>" class="btn btn-primary"><i class="fa fa-save"></i></a>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input type="text" value="<?php echo $search['keyword']; ?>" id="keyword<?php echo $search['id'] ?>" class="form-control">
                                                    </div>      
                                                </div>                                     
                                            </td>
                                            <td class="text-right"><?php echo $search['keytype'] ?></td>
                                            <td class="text-right"><?php echo $search['url'] ?></td>
                                            <td class="text-left"><?php echo $search['country'] ?></td>
                                            <td class="text-left"><?php echo $search['language'] ?></td>
                                            <td class="text-right"><?php echo $search['date_added']; ?></td>
                                            <td class="text-right"><?php echo $search['counter'] ?></td>
                                            <td class="text-right"><?php echo $search['status']; ?></td>
                                            <td class="text-right">
                                                <?php if (!$search['status']) { ?>
                                                    <a href="<?php echo $search['approve']; ?>" data-toggle="tooltip" title="<?php echo $button_approve; ?>" class="btn btn-primary"><i class="fa fa-check"></i></a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $search['disapprove']; ?>" data-toggle="tooltip" title="<?php echo $button_disapprove; ?>" class="btn btn-primary"><i class="fa fa-remove"></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="10"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    function update(rowid) {
        var keyword = $('#keyword' + rowid).val();
        $.ajax({
            type: 'post',
            url: 'index.php?route=catalog/search/update/',
            data: 'id='+ rowid+'&keyword='+keyword,
            success: function (data) {
                alert(data);
            }
        });
    }
    //--></script>
<script type="text/javascript"><!--
    $('#button-filter').on('click', function () {
        var url = 'index.php?route=catalog/search';

        var filter_name = $('input[name=\'filter_name\']').val();
        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }

        /*var filter_quantity = $('input[name=\'filter_quantity\']').val();
         if (filter_quantity) {
         url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
         }*/

        var filter_keytype = $('select[name=\'filter_keytype\']').val();
        if (filter_keytype != '*') {
            url += '&filter_keytype=' + encodeURIComponent(filter_keytype);
        }

        var filter_approved = $('select[name=\'filter_approved\']').val();
        if (filter_approved != '*') {
            url += '&filter_approved=' + encodeURIComponent(filter_approved);
        }

        var filter_language_id = $('select[name=\'filter_language_id\']').val();
        if (filter_language_id != '*') {
            url += '&filter_language_id=' + encodeURIComponent(filter_language_id);
        }

        var filter_country_id = $('select[name=\'filter_country_id\']').val();
        if (filter_country_id != '*') {
            url += '&filter_country_id=' + encodeURIComponent(filter_country_id);
        }

        location = url;
    });
    //--></script>
<?php echo $footer; ?>