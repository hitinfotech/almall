<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-store" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-store" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-block1" data-toggle="tab"><?php echo $tab_block1; ?></a></li>
                        <li><a href="#tab-block2" data-toggle="tab"><?php echo $tab_block2; ?></a></li>
                        <li><a href="#tab-block3" data-toggle="tab"><?php echo $tab_block3; ?></a></li>
                        <li><a href="#tab-block4" data-toggle="tab"><?php echo $tab_block4; ?></a></li>
                        <li><a href="#tab-block5" data-toggle="tab"><?php echo $tab_block5; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-block1">
                            <div id='visited_points'>
                              <?php  $visited_row = 0 ?>
                              <?php if(!empty($settings['visited_points'])){ ?>
                                <?php foreach ($settings['visited_points'] as $key => $value) {  ?>
                                  <div class="form-group">
                                    <label class="col-sm-1 control-label pull-left" for="input_visited_from_<?= $visited_row ?>"><?php echo $text_from; ?></label>
                                    <div class="col-sm-2">
                                        <input type="text" name="ranking[visited_points][from][]" value="<?= $value['from']?>" placeholder="<?php echo $text_from; ?>" id="input_visited_from_<?= $visited_row ?>" class="form-control" />
                                    </div>

                                    <label class="col-sm-1 control-label pull-left" for="input_visited_to_<?= $visited_row ?>"><?php echo $text_to; ?></label>
                                    <div class="col-sm-2">
                                        <input type="text" name="ranking[visited_points][to][]" value="<?= $value['to']?>" placeholder="<?php echo $text_to; ?>" id="input_visited_to_<?= $visited_row ?>" class="form-control" />
                                    </div>

                                    <label class="col-sm-1 control-label pull-left" for="input_visited_value_<?= $visited_row ?>"><?php echo $text_value; ?></label>
                                    <div class="col-sm-2">
                                        <input type="text" name="ranking[visited_points][value][]" value="<?= $value['value']?>" placeholder="<?php echo $text_value; ?>" id="input_visited_value_<?= $visited_row ?>" class="form-control" />
                                    </div>
                                    <button type="button" onclick="$(this).parent().remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                  </div>
                                  <?php $visited_row++ ?>
                                <?php } ?>
                              <?php } ?>
                            </div>
                            <button type="button" onclick="addVisitedInput();" data-toggle="tooltip" title=" <?= $button_add_new_range ?>" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i></button>
                        </div>


                        <div class="tab-pane" id="tab-block2">
                          <div id='cart_rate_points'>
                            <?php  $cart_rate_row = 0 ?>
                            <?php if(!empty($settings['cart_rate_points'])){ ?>
                              <?php foreach ($settings['cart_rate_points'] as $key => $value) {  ?>
                                <div class="form-group">
                                  <label class="col-sm-1 control-label pull-left" for="input_cart_rate_from_<?= $cart_rate_row ?>"><?php echo $text_from; ?></label>
                                  <div class="col-sm-2">
                                      <input type="text" name="ranking[cart_rate_points][from][]" value="<?= $value['from']?>" placeholder="<?php echo $text_from; ?>" id="input_cart_rate_from_<?= $cart_rate_row ?>" class="form-control" />
                                  </div>

                                  <label class="col-sm-1 control-label pull-left" for="input_cart_rate_to_<?= $cart_rate_row ?>"><?php echo $text_to; ?></label>
                                  <div class="col-sm-2">
                                      <input type="text" name="ranking[cart_rate_points][to][]" value="<?= $value['to']?>" placeholder="<?php echo $text_to; ?>" id="input_cart_rate_to_<?= $cart_rate_row ?>" class="form-control" />
                                  </div>

                                  <label class="col-sm-1 control-label pull-left" for="input_cart_rate_value_<?= $cart_rate_row ?>"><?php echo $text_value; ?></label>
                                  <div class="col-sm-2">
                                      <input type="text" name="ranking[cart_rate_points][value][]" value="<?= $value['value']?>" placeholder="<?php echo $text_value; ?>" id="input_cart_rate_value_<?= $cart_rate_row ?>" class="form-control" />
                                  </div>
                                  <button type="button" onclick="$(this).parent().remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                </div>
                                <?php $cart_rate_row++ ?>
                              <?php } ?>
                            <?php } ?>
                          </div>
                          <button type="button" onclick="addCartRateInput();" data-toggle="tooltip" title=" <?= $button_add_new_range ?>" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i></button>
                        </div>


                        <div class="tab-pane" id="tab-block3">
                          <div id='new_product_points'>
                            <?php  $new_product_row = 0 ?>
                            <?php if(!empty($settings['new_product_points'])){ ?>
                              <?php foreach ($settings['new_product_points'] as $key => $value) {  ?>
                                <div class="form-group">
                                  <label class="col-sm-1 control-label pull-left" for="input_new_product_from_<?= $new_product_row ?>"><?php echo $text_from; ?></label>
                                  <div class="col-sm-2">
                                      <input type="text" name="ranking[new_product_points][from][]" value="<?= $value['from']?>" placeholder="<?php echo $text_from; ?>" id="input_new_product_from_<?= $new_product_row ?>" class="form-control" />
                                  </div>

                                  <label class="col-sm-1 control-label pull-left" for="input_new_product_to_<?= $new_product_row ?>"><?php echo $text_to; ?></label>
                                  <div class="col-sm-2">
                                      <input type="text" name="ranking[new_product_points][to][]" value="<?= $value['to']?>" placeholder="<?php echo $text_to; ?>" id="input_new_product_to_<?= $new_product_row ?>" class="form-control" />
                                  </div>

                                  <label class="col-sm-1 control-label pull-left" for="input_new_product_value_<?= $new_product_row ?>"><?php echo $text_value; ?></label>
                                  <div class="col-sm-2">
                                      <input type="text" name="ranking[new_product_points][value][]" value="<?= $value['value']?>" placeholder="<?php echo $text_value; ?>" id="input_new_product_value_<?= $new_product_row ?>" class="form-control" />
                                  </div>
                                  <button type="button" onclick="$(this).parent().remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                </div>
                              <?php $new_product_row++ ?>
                            <?php } ?>
                          <?php } ?>
                        </div>

                          <button type="button" onclick="addNewProductInput();" data-toggle="tooltip" title=" <?= $button_add_new_range ?>" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i></button>
                        </div>

                        <div class="tab-pane" id="tab-block4">

                          <div id='favorite_points'>
                            <?php  $favorite_row = 0 ?>
                            <?php if(!empty($settings['favorite_points'])){ ?>
                              <?php foreach ($settings['favorite_points'] as $key => $value) {  ?>
                                <div class="form-group">
                                  <label class="col-sm-1 control-label pull-left" for="input_favorite_from_<?= $favorite_row ?>"><?php echo $text_from; ?></label>
                                  <div class="col-sm-2">
                                      <input type="text" name="ranking[favorite_points][from][]" value="<?= $value['from']?>" placeholder="<?php echo $text_from; ?>" id="input_favorite_from_<?= $favorite_row ?>" class="form-control" />
                                  </div>

                                  <label class="col-sm-1 control-label pull-left" for="input_favorite_to_<?= $favorite_row ?>"><?php echo $text_to; ?></label>
                                  <div class="col-sm-2">
                                      <input type="text" name="ranking[favorite_points][to][]" value="<?= $value['to']?>" placeholder="<?php echo $text_to; ?>" id="input_favorite_to_<?= $favorite_row ?>" class="form-control" />
                                  </div>

                                  <label class="col-sm-1 control-label pull-left" for="input_favorite_value_<?= $favorite_row ?>"><?php echo $text_value; ?></label>
                                  <div class="col-sm-2">
                                      <input type="text" name="ranking[favorite_points][value][]" value="<?= $value['value']?>" placeholder="<?php echo $text_value; ?>" id="input_favorite_value_<?= $favorite_row ?>" class="form-control" />
                                  </div>
                                  <button type="button" onclick="$(this).parent().remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                </div>
                                <?php $favorite_row++ ?>
                              <?php } ?>
                            <?php } ?>
                          </div>
                          <button type="button" onclick="addFavoriteInput();" data-toggle="tooltip" title=" <?= $button_add_new_range ?>" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i></button>


                        </div>
                    <div class="tab-pane" id="tab-block5">
                      <div id='sale_points'>
                        <?php  $sale_row = 0 ?>
                        <?php if(!empty($settings['sale_points'])){ ?>
                          <?php foreach ($settings['sale_points'] as $key => $value) {  ?>

                            <div class="form-group">
                              <label class="col-sm-1 control-label pull-left" for="input_sale_from_<?= $sale_row ?>"><?php echo $text_from; ?></label>
                              <div class="col-sm-2">
                                  <input type="text" name="ranking[sale_points][from][]" value="<?= $value['from']?>" placeholder="<?php echo $text_from; ?>" id="input_sale_from_<?= $sale_row ?>" class="form-control" />
                              </div>

                              <label class="col-sm-1 control-label pull-left" for="input_sale_to_<?= $sale_row ?>"><?php echo $text_to; ?></label>
                              <div class="col-sm-2">
                                  <input type="text" name="ranking[sale_points][to][]" value="<?= $value['to']?>" placeholder="<?php echo $text_to; ?>" id="input_sale_to_<?= $sale_row ?>" class="form-control" />
                              </div>

                              <label class="col-sm-1 control-label pull-left" for="input_sale_value_<?= $sale_row ?>"><?php echo $text_value; ?></label>
                              <div class="col-sm-2">
                                  <input type="text" name="ranking[sale_points][value][]" value="<?= $value['value']?>" placeholder="<?php echo $text_value; ?>" id="input_sale_value_<?= $sale_row ?>" class="form-control" />
                              </div>
                              <button type="button" onclick="$(this).parent().remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                            </div>
                        <?php $sale_row++ ?>
                      <?php } ?>
                    <?php } ?>

                      </div>
                      <button type="button" onclick="addSaleInput();" data-toggle="tooltip" title=" <?= $button_add_new_range ?>" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i></button>

                    </div>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<script>
var visited_row = '<?= $visited_row ?>';
var cart_rate_row = '<?= $cart_rate_row ?>';
var new_product_row = '<?= $new_product_row ?>';
var favorite_row = '<?= $favorite_row ?>';
var sale_row = '<?= $sale_row ?>';


function addVisitedInput(){
  var html = '<div class="form-group">';
      html +='<label class="col-sm-1 control-label pull-left" for="input_visited_from_'+visited_row+'"><?php echo $text_from; ?></label>';
      html +='  <div class="col-sm-2">  <input type="text" name="ranking[visited_points][from][]" value="" placeholder="<?php echo $text_from; ?>" id="input_visited_from_'+visited_row+'" class="form-control" /></div>';
      html +='<label class="col-sm-1 control-label pull-left" for="input_visited_to_'+visited_row+'"><?php echo $text_to; ?></label>';
      html +='<div class="col-sm-2"><input type="text" name="ranking[visited_points][to][]" value="" placeholder="<?php echo $text_to; ?>" id="input_visited_to_'+visited_row+'" class="form-control" /></div>';
      html +='<label class="col-sm-1 control-label pull-left" for="input_visited_value_'+visited_row+'"><?php echo $text_value; ?></label>';
      html +='<div class="col-sm-2"><input type="text" name="ranking[visited_points][value][]" value="" placeholder="<?php echo $text_value; ?>" id="input_visited_value_'+visited_row+'" class="form-control" /></div>';
      html +='<button type="button" onclick="$(this).parent().remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>';
      html +='</div>';
      visited_row++ ;

      $("#visited_points").append(html);
}

function addCartRateInput(){
  var html = '<div class="form-group">';
      html +='<label class="col-sm-1 control-label pull-left" for="input_cart_rate_from_'+cart_rate_row+'"><?php echo $text_from; ?></label>';
      html +='  <div class="col-sm-2">  <input type="text" name="ranking[cart_rate_points][from][]" value="" placeholder="<?php echo $text_from; ?>" id="input_cart_rate_from_'+cart_rate_row+'" class="form-control" /></div>';
      html +='<label class="col-sm-1 control-label pull-left" for="input_cart_rate_to_'+cart_rate_row+'"><?php echo $text_to; ?></label>';
      html +='<div class="col-sm-2"><input type="text" name="ranking[cart_rate_points][to][]" value="" placeholder="<?php echo $text_to; ?>" id="input_cart_rate_to_'+cart_rate_row+'" class="form-control" /></div>';
      html +='<label class="col-sm-1 control-label pull-left" for="input_cart_rate_value_'+cart_rate_row+'"><?php echo $text_value; ?></label>';
      html +='<div class="col-sm-2"><input type="text" name="ranking[cart_rate_points][value][]" value="" placeholder="<?php echo $text_value; ?>" id="input_cart_rate_value_'+cart_rate_row+'" class="form-control" /></div>';
      html +='<button type="button" onclick="$(this).parent().remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>';
      html +='</div>';
      cart_rate_row++ ;

      $("#cart_rate_points").append(html);
}

function addNewProductInput(){
  var html = '<div class="form-group">';
      html +='<label class="col-sm-1 control-label pull-left" for="input_new_product_from_'+new_product_row+'"><?php echo $text_from; ?></label>';
      html +='  <div class="col-sm-2">  <input type="text" name="ranking[new_product_points][from][]" value="" placeholder="<?php echo $text_from; ?>" id="input_new_product_from_'+new_product_row+'" class="form-control" /></div>';
      html +='<label class="col-sm-1 control-label pull-left" for="input_new_product_to_'+new_product_row+'"><?php echo $text_to; ?></label>';
      html +='<div class="col-sm-2"><input type="text" name="ranking[new_product_points][to][]" value="" placeholder="<?php echo $text_to; ?>" id="input_new_product_to_'+new_product_row+'" class="form-control" /></div>';
      html +='<label class="col-sm-1 control-label pull-left" for="input_new_product_value_'+new_product_row+'"><?php echo $text_value; ?></label>';
      html +='<div class="col-sm-2"><input type="text" name="ranking[new_product_points][value][]" value="" placeholder="<?php echo $text_value; ?>" id="input_new_product_value_'+new_product_row+'" class="form-control" /></div>';
      html +='<button type="button" onclick="$(this).parent().remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>';
      html +='</div>';
      new_product_row++ ;

      $("#new_product_points").append(html);
}

function addFavoriteInput(){
  var html = '<div class="form-group">';
      html +='<label class="col-sm-1 control-label pull-left" for="input_favorite_from_'+favorite_row+'"><?php echo $text_from; ?></label>';
      html +='  <div class="col-sm-2">  <input type="text" name="ranking[favorite_points][from][]" value="" placeholder="<?php echo $text_from; ?>" id="input_favorite_from_'+favorite_row+'" class="form-control" /></div>';
      html +='<label class="col-sm-1 control-label pull-left" for="input_favorite_to_'+favorite_row+'"><?php echo $text_to; ?></label>';
      html +='<div class="col-sm-2"><input type="text" name="ranking[favorite_points][to][]" value="" placeholder="<?php echo $text_to; ?>" id="input_favorite_to_'+favorite_row+'" class="form-control" /></div>';
      html +='<label class="col-sm-1 control-label pull-left" for="input_favorite_value_'+favorite_row+'"><?php echo $text_value; ?></label>';
      html +='<div class="col-sm-2"><input type="text" name="ranking[favorite_points][value][]" value="" placeholder="<?php echo $text_value; ?>" id="input_favorite_value_'+favorite_row+'" class="form-control" /></div>';
      html +='<button type="button" onclick="$(this).parent().remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>';
      html +='</div>';
      favorite_row++ ;

      $("#favorite_points").append(html);
}

function addSaleInput(){
  var html = '<div class="form-group">';
      html +='<label class="col-sm-1 control-label pull-left" for="input_favorite_from_'+sale_row+'"><?php echo $text_from; ?></label>';
      html +='  <div class="col-sm-2">  <input type="text" name="ranking[sale_points][from][]" value="" placeholder="<?php echo $text_from; ?>" id="input_sale_from_'+sale_row+'" class="form-control" /></div>';
      html +='<label class="col-sm-1 control-label pull-left" for="input_sale_to_'+sale_row+'"><?php echo $text_to; ?></label>';
      html +='<div class="col-sm-2"><input type="text" name="ranking[sale_points][to][]" value="" placeholder="<?php echo $text_to; ?>" id="input_sale_to_'+sale_row+'" class="form-control" /></div>';
      html +='<label class="col-sm-1 control-label pull-left" for="input_sale_value_'+sale_row+'"><?php echo $text_value; ?></label>';
      html +='<div class="col-sm-2"><input type="text" name="ranking[sale_points][value][]" value="" placeholder="<?php echo $text_value; ?>" id="input_sale_value_'+sale_row+'" class="form-control" /></div>';
      html +='<button type="button" onclick="$(this).parent().remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>';
      html +='</div>';
      sale_row++ ;

      $("#sale_points").append(html);
}

</script>
