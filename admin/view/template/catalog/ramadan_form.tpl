<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-store" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-store" class="form-horizontal">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab-block4" data-toggle="tab"><?php echo $tab_block4; ?></a></li>
                      <li><a href="#tab-block1" data-toggle="tab"><?php echo $tab_block1; ?></a></li>
                      <?php /* <li><a href="#tab-block2" data-toggle="tab"><?php echo $tab_block3; ?></a></li> */ ?>
                        <li><a href="#tab-block3" data-toggle="tab"><?php echo $tab_block2; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab-block1">
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-ramadan_block1_title1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_block1_title1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="ramadan_block1_title1" value="<?php echo $ramadan_block1_title1; ?>" placeholder="<?php echo $text_ramadan_block1_title1; ?>" id="input-ramadan_block1_title1" class="form-control" />
                                    <?php if ($error_ramadan_block1_title1) { ?>
                                        <div class="text-danger"><?php echo $error_ramadan_block1_title1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-ramadan_block1_description1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_block1_description1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="ramadan_block1_description1" value="<?php echo $ramadan_block1_description1; ?>" placeholder="<?php echo $text_ramadan_block1_description1; ?>" id="input-ramadan_block1_description1" class="form-control" />
                                    <?php if ($error_ramadan_block1_description1) { ?>
                                        <div class="text-danger"><?php echo $error_ramadan_block1_description1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-ramadan_block1_url1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_block1_url1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="ramadan_block1_url1" value="<?php echo $ramadan_block1_url1; ?>" placeholder="<?php echo $text_ramadan_block1_url1; ?>" id="input-ramadan_block1_url1" class="form-control" />
                                    <?php if ($error_ramadan_block1_url1) { ?>
                                        <div class="text-danger"><?php echo $error_ramadan_block1_url1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-ramadan_block1_button1"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_block1_button1; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="ramadan_block1_button1" value="<?php echo $ramadan_block1_button1; ?>" placeholder="<?php echo $text_ramadan_block1_button1; ?>" id="input-ramadan_block1_button1" class="form-control" />
                                    <?php if ($error_ramadan_block1_button1) { ?>
                                        <div class="text-danger"><?php echo $error_ramadan_block1_button1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b1_t1"><?php echo $text_ramadan_block1_image1; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_11; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="ramadan_block1_image1" value="<?php echo $ramadan_block1_image1; ?>" id="input-image_b1_t1" />
                                    <input type="hidden" name="ramadan_block1_image1" value="<?php echo $ramadan_block1_image1; ?>" />
                                    <?php if ($error_ramadan_block1_image1) { ?>
                                        <div class="text-danger"><?php echo $error_ramadan_block1_image1; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!-- block 1 title 2-->
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-ramadan_block1_title2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_block1_title2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="ramadan_block1_title2" value="<?php echo $ramadan_block1_title2; ?>" placeholder="<?php echo $text_ramadan_block1_title2; ?>" id="input-ramadan_block1_title2" class="form-control" />
                                    <?php if ($error_ramadan_block1_title2) { ?>
                                        <div class="text-danger"><?php echo $error_ramadan_block1_title2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-ramadan_block1_description2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_block1_description2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="ramadan_block1_description2" value="<?php echo $ramadan_block1_description2; ?>" placeholder="<?php echo $text_ramadan_block1_description2; ?>" id="input-ramadan_block1_description2" class="form-control" />
                                    <?php if ($error_ramadan_block1_description2) { ?>
                                        <div class="text-danger"><?php echo $error_ramadan_block1_description2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-ramadan_block1_url2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_block1_url2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="ramadan_block1_url2" value="<?php echo $ramadan_block1_url2; ?>" placeholder="<?php echo $text_ramadan_block1_url2; ?>" id="input-ramadan_block1_url2" class="form-control" />
                                    <?php if ($error_ramadan_block1_url2) { ?>
                                        <div class="text-danger"><?php echo $error_ramadan_block1_url2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-ramadan_block1_button2"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_block1_button2; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="ramadan_block1_button2" value="<?php echo $ramadan_block1_button2; ?>" placeholder="<?php echo $text_ramadan_block1_button2; ?>" id="input-ramadan_block1_button2" class="form-control" />
                                    <?php if ($error_ramadan_block1_button2) { ?>
                                        <div class="text-danger"><?php echo $error_ramadan_block1_button2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-image_b1_t2"><?php echo $text_ramadan_block1_image2; ?></label>
                                <div class="col-sm-10">
                                    <img src="<?php echo $thumb_12; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                    <input type="file" name="ramadan_block1_image2" value="<?php echo $ramadan_block1_image2; ?>" id="input-image_b1_t2" />
                                    <input type="hidden" name="ramadan_block1_image2" value="<?php echo $ramadan_block1_image2; ?>" />
                                    <?php if ($error_ramadan_block1_image2) { ?>
                                        <div class="text-danger"><?php echo $error_ramadan_block1_image2; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-block2">
                          <div class="form-group">
                              <label class="col-sm-2 control-label" for="input-selected-products"><span data-toggle="tooltip"><?php echo $entry_products; ?></span></label>
                              <div class="col-sm-10">
                                <input type="text" name="selected_products" placeholder="<?php echo $entry_products; ?>" id="input-selected-products" class="form-control"/>
                                <div id="selected-products" class="well well-sm product_icon" style="height: 150px; overflow: auto;">
                                  <?php foreach($selected_products as $p => $selected_product) {?>
                                    <div id="product-selected-<?php echo $selected_product['product_id']?>">
                                      <i class="fa fa-minus-circle"></i> <?php echo $selected_product['product_name']?>
                                      <input type="hidden" name="product-selected[]" value="<?php echo $selected_product['product_id']?>">
                                    </div>
                                  <?php } ?>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-block3">
                          <div class='row'>
                            <div class="col-sm-2">
                                <ul class="nav nav-pills nav-stacked" id="blocks">
                                    <?php $address_row = 0; ?>
                                    <?php foreach ($additional_products as $block) { ?>
                                        <li><a href="#tab-additional-block-<?php echo $address_row; ?>" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$('#blocks a:first').tab('show');
                                                    $('#blocks a[href=\'#tab-additional-block-<?php echo $address_row; ?>\']').parent().remove();
                                                    $('#tab-additional-block-<?php echo $address_row; ?>').remove();"></i> <?php echo $block['title'];//$tab_block . ' ' . $address_row; ?></a></li>
                                            <?php $address_row++; ?>
                                        <?php } ?>
                                    <li id="block-add"><a onclick="addBlock();"><i class="fa fa-plus-circle"></i> <?php echo $button_block_add; ?></a></li>
                                </ul>
                            </div>
                            <div class="col-sm-10">
                                <div  class="tab-content" id="tab-blocks">
                                  <?php $address_row = 0; ?>
                                  <?php foreach ($additional_products as $block) { ?>
                                    <div class="tab-pane" id="tab-additional-block-<?php echo $address_row ?>">
                                      <div class="form-group required">
                                      <label class="col-sm-2 control-label" for="input-additional_block_title"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_additional_block_title; ?></span></label>
                                      <div class="col-sm-10">
                                      <input type="text" name="additional_bloacks[<?php echo $address_row ?>][ramadan_additional_block<?php echo $address_row ?>_title<?php echo $address_row ?>]" placeholder="<?php echo $text_ramadan_additional_block_title; ?>" id="input-additional_block_title" class="form-control" value='<?php echo $block["title"] ?>' />
                                      </div></div>
                                      <div class="form-group required">
                                      <label class="col-sm-2 control-label" for="input-additional_block_action"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_additional_block_action; ?></span></label>'
                                      <div class="col-sm-10">
                                      <input type="text" name="additional_bloacks[<?php echo $address_row ?>][ramadan_additional_block<?php echo $address_row ?>_action<?php echo $address_row ?>]" placeholder="<?php echo $text_ramadan_additional_block_action; ?>" id="input-additional_block_action" class="form-control" value='<?php echo $block["action"] ?>' />
                                      </div></div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-additional_block_position"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_additional_block_position; ?></span></label>
                                            <div class="col-sm-10">
                                                <?php if($block['position']) { ?>
                                                <input type="number" class="form-control" placeholder="Position" name="position[<?php echo $address_row ?>]" value="<?php echo $block['position'] ?>" >
                                                <?php } else { ?>
                                                <input type="number" class="form-control" placeholder="Position" name="position[<?php echo $address_row ?>]" value="<?php echo $address_row ?>" >
                                                <?php } ?>
                                            </div>
                                        </div>

                                      <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-products"><span data-toggle="tooltip"><?php echo $entry_products; ?></span></label>
                                        <div class="col-sm-10">
                                          <input type="text" name="additional_bloacks[][products]" placeholder="<?php echo $entry_products; ?>" id="input-products" class="form-control product_autocomplete" data-row-num="<?php echo $address_row ?>" />
                                          <div id="product-related-<?php echo $address_row ?>" class="well well-sm product_icon" style="height: 150px; overflow: auto;">
                                            <?php foreach($block['products'] as $k => $added_product) {?>
                                              <div id="product-related_<?php echo $address_row ?>_<?php echo $added_product['product_id']?>">
                                                <i class="fa fa-minus-circle"></i> <?php echo $added_product['product_name']?>
                                                <input type="hidden" name="product-related[<?php echo $address_row?>][]" value="<?php echo $added_product['product_id']?>">
                                              </div>
                                            <?php } ?>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                      <?php $address_row++; ?>
                                  <?php } ?>
                                </div>
                            </div>
                          </row>
                        </div>
                    </div>
                    <div class="tab-pane active" id="tab-block4">
                      <div class="form-group required">
                        <div class='row' style="margin-left: 20px" >
                          <label class="col-sm-2 ">Language</label>
                          <div class='col-sm-3 '>
                            <select class="form-control" name='coutnry_langauge'>
                              <option value='1' <?php echo ($page_language ==1 ? "selected" : "")?> > english</option>
                              <option value='2' <?php echo ($page_language ==2 ? "selected" : "")?> > Arabic</option>
                            </select>
                          </div>
                        </div>
                        <br>
                          <div class='row' style="margin-left: 20px" >
                            <?php foreach ($available_coutnries as $key => $country){?>
                              <label class="col-sm-3 "><input class='in_country' data-country-id='<?php echo $country["country_id"] ?>' type='checkbox' name='page_country[<?php echo $country["country_id"] ?>]' <?php echo (isset($page_countries[$country["country_id"]]) ? "checked" : "")?> ><?php echo $country["name"] ?>  </label>
                            <?php } ?>
                          </div>
                      </div>
                    </div>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript"><!--
    // Related
    $('#input-selected-products').autocomplete({
        'source': function (request, response) {
            $.ajax({
              url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent($(this).val()),
              dataType: 'json',
              success: function (json) {
                  response($.map(json, function (item) {
                      return {
                          label: item['name'],
                          value: item['product_id']
                      }
                  }));
              }
            });
        },
        'select': function (item) {
            $('input[name=\'selected_products\']').val('');

            $('#product-selected-'+ item['value']).remove();

            $('#selected-products').append('<div id="product-selected-'+ item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product-selected[]" value="' + item['value'] + '" /></div>');
        }
    });
    $('.product_icon').delegate('.fa-minus-circle', 'click', function () {
        $(this).parent().remove();
    });

    $('#blocks a:first').tab('show');

    //-->
    </script>

    <script type="text/javascript"><!--
        var address_row = <?php echo $address_row; ?>;

        function addBlock() {
          var counter = 0;
            html = '';
            html = '<div class="tab-pane" id="tab-additional-block-' + address_row + '">';
            html += '<div class="form-group required">';
            html += '<label class="col-sm-2 control-label" for="input-additional_block_title"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_additional_block_title; ?></span></label>'
            html += '<div class="col-sm-10">';
            html += '<input type="text" name="additional_bloacks['+address_row+'][ramadan_additional_block' + address_row + '_title' + address_row + ']" placeholder="<?php echo $text_ramadan_additional_block_title; ?>" id="input-additional_block_title" class="form-control" />';
            html += '</div></div>';
            html += '<div class="form-group required">';
            html += '<label class="col-sm-2 control-label" for="input-additional_block_action"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_additional_block_action; ?></span></label>'
            html += '<div class="col-sm-10">';
            html += '<input type="text" name="additional_bloacks['+address_row+'][ramadan_additional_block' + address_row + '_action' + address_row + ']" placeholder="<?php echo $text_ramadan_additional_block_action; ?>" id="input-additional_block_action" class="form-control" />';
            html += '</div></div>';
            html += '<div class="form-group">';
            html += '<label class="col-sm-2 control-label" for="input-additional_block_position"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_ramadan_additional_block_position; ?></span></label>';
            html += '<div class="col-sm-10">';
            html += '<input type="number" class="form-control" placeholder="Position" name="position[' + address_row + ']" value="' + address_row + '" >';
            html += '</div></div>';

            html +='<div class="form-group">';
            html += '<label class="col-sm-2 control-label" for="input-products"><span data-toggle="tooltip"><?php echo $entry_products; ?></span></label>';
            html += '<div class="col-sm-10">';
            html += '<input type="text" name="additional_bloacks[][products]" placeholder="<?php echo $entry_products; ?>" id="input-products" class="form-control product_autocomplete" data-row-num="'+address_row+'" />';
            html += '<div id="product-related-'+address_row+'" class="well well-sm product_icon" style="height: 150px; overflow: auto;">';
            html += '</div></div></div>';
            html += '</div>';


            $('#tab-blocks').append(html);

            $('#block-add').before('<li><a href="#tab-additional-block-' + address_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'#blocks a:first\').tab(\'show\'); $(\'a[href=\\\'#tab-additional-block-' + address_row + '\\\']\').parent().remove(); $(\'#tab-additional-block-' + address_row + '\').remove();"></i> <?php echo $tab_block; ?> ' + address_row + '</a></li>');

            $('#blocks a[href=\'#tab-additional-block-' + address_row + '\']').tab('show');

            // Related
            $('.product_autocomplete').autocomplete({
              'source': function (request, response) {
                  $.ajax({
                      url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent($(this).val()),
                      dataType: 'json',
                      success: function (json) {
                          response($.map(json, function (item) {
                              return {
                                  label: item['name'],
                                  value: item['product_id']
                              }
                          }));
                      }
                  });
              },
              'select': function (item) {
                    $('input[name=\'additional_bloacks[][products]\']').val('');

                    $('#product-related_' + $(this).attr('data-row-num')+ '_'+ item['value']).remove();

                    $('#product-related-'+$(this).attr('data-row-num')).append('<div id="product-related_'+$(this).attr('data-row-num')+'_' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product-related['+$(this).attr('data-row-num')+'][]" value="' + item['value'] + '" /></div>');
                }
            });
            $('.product_icon').delegate('.fa-minus-circle', 'click', function () {
                $(this).parent().remove();
            });

            address_row++;
        }

        // Related
        $('.product_autocomplete').autocomplete({
            'source': function (request, response) {
                $.ajax({
                  url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent($(this).val()),
                  dataType: 'json',
                  success: function (json) {
                      response($.map(json, function (item) {
                          return {
                              label: item['name'],
                              value: item['product_id']
                          }
                      }));
                  }
                });
            },
            'select': function (item) {
                $('input[name=\'additional_bloacks[][products]\']').val('');

                $('#product-related_'+ $(this).attr('data-row-num')+ '_'+ item['value']).remove();

                $('#product-related-'+$(this).attr('data-row-num')).append('<div id="product-related_'+$(this).attr('data-row-num')+'_' +  + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product-related['+$(this).attr('data-row-num')+'][]" value="' + item['value'] + '" /></div>');
            }
        });
        $('.product_icon').delegate('.fa-minus-circle', 'click', function () {
            $(this).parent().remove();
        });

        // Related
        $('#input-selected-products').autocomplete({
            'source': function (request, response) {
                $.ajax({
                  url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent($(this).val()),
                  dataType: 'json',
                  success: function (json) {
                      response($.map(json, function (item) {
                          return {
                              label: item['name'],
                              value: item['product_id']
                          }
                      }));
                  }
                });
            },
            'select': function (item) {
                $('input[name=\'selected_products\']').val('');

                $('#product-selected-'+ item['value']).remove();

                $('#selected-products').append('<div id="product-selected-'+ item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product-selected[]" value="' + item['value'] + '" /></div>');
            }
        });
        $('.product_icon').delegate('.fa-minus-circle', 'click', function () {
            $(this).parent().remove();
        });

        $('#blocks a:first').tab('show');

        //-->
        </script>
