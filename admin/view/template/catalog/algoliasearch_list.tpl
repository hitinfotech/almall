<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="well">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="input-name"><?php echo 'Product ID'; ?></label>
                            <input type="text" name="filter_product_id" value="<?php echo $filter_product_id; ?>" placeholder="<?php echo 'Product ID'; ?>" id="input-product_id" class="form-control" />
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for="input-country"><?php echo $entry_country; ?></label>
                            <select name="filter_country" id="input-country" class="form-control">
                                <option value="*"><?php echo $select_country; ?></option>
                                <?php foreach ($countries as $country_id => $country_name) { ?>
                                    <?php if ($filter_country == $country_id) { ?>
                                        <option value="<?php echo $country_id ?>" selected="selected"><?php echo $country_name ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $country_id ?>"><?php echo $country_name ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td class="text-center"><?php echo $column_image; ?></td>
                                <td class="text-right">ID</td>
                                <td class="text-left"><?php echo $column_name; ?></td>
                                <td class="text-left"><?php echo $column_model; ?></td>
                                <td class="text-left"><?php echo $column_sku; ?></td>
                                <td class="text-right"><?php echo $column_quantity; ?></td>
                                <td class="text-left"><?php echo $brand_name; ?></td>
                                <td class="text-left"><?php echo $entry_status; ?></td>
                                <td class="text-left"><?php echo $column_bsort_order; ?></td>
                                <td class="text-left"><?php echo $column_sort_order; ?></td>
                                <td class="text-right"><?php echo $column_action; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($products) { ?>
                                <?php foreach ($products as $product) { ?>
                                    <tr>
                                        <td class="text-center"><?php if ($product['image']) { ?>
                                                <img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" class="img-thumbnail" />
                                            <?php } else { ?>
                                                <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                                            <?php } ?>
                                        </td>
                                        <td class="text-right"><?php echo $product['product_id']; ?></td>
                                        <td class="text-left"><?php echo $product['name']; ?></td>
                                        <td class="text-left"><?php echo $product['model']; ?></td>
                                        <td class="text-left"><?php echo $product['sku']; ?></td>
                                        <td class="text-right"><?php if ($product['quantity'] <= 0) { ?>
                                                <span class="label label-warning"><?php echo $product['quantity']; ?></span>
                                            <?php } elseif ($product['quantity'] <= 5) { ?>
                                                <span class="label label-danger"><?php echo $product['quantity']; ?></span>
                                            <?php } else { ?>
                                                <span class="label label-success"><?php echo $product['quantity']; ?></span>
                                            <?php } ?>
                                        </td>
                                        <td class="text-left"><?php echo $product['bname']; ?></td>
                                        <td class="text-left"><?php echo $product['status']; ?></td>
                                        <td class="text-left"><?php echo $product['bsort_order']; ?></td>
                                        <td class="text-left">
                                            <input id="sort_order<?php echo $product['product_id'] ?>" class="form-control" type="text" placeholder="Sort Order" value="<?php echo $product['sort_order']; ?>" name="sort_order">
                                        </td>
                                        <td class="text-right">
                                            <button type="button" onclick="dosort(<?php echo $product['product_id'] ?>)" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <tr>
                                    <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="col-sm-8 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-4 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript"><!--
    function dosort(product_id) {
        var sort_order = $('#sort_order' + product_id).val();
        $.ajax({
            url: 'index.php?route=catalog/algoliasearch/edit&token=<?php echo $token; ?>',
            type: 'post',
            dataType: 'json',
            cache: false,
            data: 'product_id=' + product_id + '&sort_order=' + sort_order + '&rand=' + Math.random(),
            success: function (json) {
                if (json['error']) {
                    alert(json['error']);
                }
                if (json['success']) {
                    $('#sort_order' + product_id).parent().append(json['success']);
                }
            }
        });
    }
//--></script>

<script type="text/javascript">
<!--
    $('#button-filter').on('click', function () {
        var url = 'index.php?route=catalog/algoliasearch&token=<?php echo $token; ?>';
        var filter_product_id = $('input[name=\'filter_product_id\']').val();
        if (filter_product_id) {
            url += '&filter_product_id=' + encodeURIComponent(filter_product_id);
        }
        var filter_country = $('select[name=\'filter_country\']').val();
        if (filter_country != '*') {
            url += '&filter_country=' + encodeURIComponent(filter_country);
        }


        location = url;
    });
//-->
</script>

<?php echo $footer; ?>