<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-store" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-store" class="form-horizontal">
                    <div class="tab-content">

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input_seller_name"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_seller_name; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="seller_name" value="<?= isset($seller_name) ? $seller_name : ''; ?>" placeholder="<?php echo $text_seller_name; ?>" id="input_seller_name" class="form-control" />
                                    <input type="hidden" name="seller_id" value="<?= isset($seller_id) ? $seller_id : ''; ?>" id="input_seller_id" />
                                    <?php if ($error_seller_id) { ?>
                                        <div class="text-danger"><?php echo $error_seller_id; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input_products_currency"><span data-toggle="tooltip" data-html="true" title=""><?php echo $text_products_currency; ?></span></label>
                                <div class="col-sm-10">
                                    <select type="text" name="products_currency"  id="input_products_currency" class="form-control" />
                                      <?php if(!empty($currencies)){ ?>
                                        <?php foreach($currencies as $key =>$value){ ?>
                                          <?php if($value['status'] == 1) {?>
                                            <option value="<?= $value['currency_id']?>" <?= ($products_currency == $value['currency_id']) ? "selected" : ""?>><?= $value['title']?></option>
                                          <?php } ?>
                                        <?php } ?>
                                      <?php } ?>
                                    </select>
                                    <?php if ($error_products_currency) { ?>
                                        <div class="text-danger"><?php echo $error_products_currency; ?></div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input_xls_file"><?php echo $text_upload_file; ?></label>
                                <div class="col-sm-10">
                                    <input type="file" name="input_xls_file"  id="input_xls_file" accept=".xls" />
                                    <?php if ($error_xls_file) { ?>
                                        <div class="text-danger"><?php echo $error_xls_file; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                      </form>
              </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<script>
  $('input[name=\'seller_name\']').autocomplete({
      'source': function (request, response) {
          $.ajax({
              url: 'index.php?route=customerpartner/partner/autocompleteForProducts&filter_name=' + encodeURIComponent(request),
              dataType: 'json',
              success: function (json) {
                  json.unshift({
                      seller_id: 0,
                      company: '<?php echo "none"; ?>'
                  });

                  response($.map(json, function (item) {
                      return {
                          label: item['company'],
                          value: item['seller_id']
                      }
                  }));
              }
          });
      },
      'select': function (item) {
          $('input[name=\'seller_name\']').val(item['label']);
          $('input[name=\'seller_id\']').val(item['value']);
      }
  });
</script>
