<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="name" value="<?php echo $name ?>" placeholder="<?php echo $entry_name; ?>"  id="input-name" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-url"><?php echo $entry_url; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="url" value="<?php echo $url ?>" placeholder="<?php echo $entry_url; ?>"  id="input-url" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
                        <div class="col-sm-10">
                            <select name="country_id" id="input-country" class="form-control">
                                <option value="0" selected="selected"><?php echo '--select--'; ?></option>
                                <?php foreach ($countries as $country) { ?>
                                    <?php if ($country['country_id'] == $country_id) { ?>
                                        <option value="<?php echo $country['country_id'] ?>" selected="selected"><?php echo $country['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $country['country_id'] ?>"><?php echo $country['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-language"><?php echo $entry_language; ?></label>
                        <div class="col-sm-10">
                            <select name="language_id" id="input-language" class="form-control">
                                <option value="0" selected="selected"><?php echo '--select--'; ?></option>
                                <?php foreach ($languages as $language) { ?>
                                    <?php if ($language['language_id'] == $language_id) { ?>
                                        <option value="<?php echo $language['language_id'] ?>" selected="selected"><?php echo $language['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $language['language_id'] ?>"><?php echo $language['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-keytype"><?php echo $entry_keytype; ?></label>
                        <div class="col-sm-10">
                            <select name="keytype" id="input-keytype" class="form-control">
                                <?php foreach ($keytypes as $row) { ?>
                                    <?php if ($keytype == $row) { ?>
                                        <option value="<?php echo $row ?>" selected="selected"><?php echo $row; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $row ?>"><?php echo $row; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-counter"><?php echo $entry_counter; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="counter" value="<?php echo $counter; ?>" placeholder="<?php echo $entry_counter; ?>" id="input-counter" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-approved"><?php echo $entry_approved; ?></label>
                        <div class="col-sm-10">
                            <select name="approved" id="input-approved" class="form-control">
                                <?php if ($status) { ?>
                                    <option value="1" selected="selected"><?php echo $text_approved; ?></option>
                                    <option value="0"><?php echo $text_disapproved; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_approved; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disapproved; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
    $('#input-description<?php echo $language['language_id']; ?>').summernote({
        height: 300
    });
<?php } ?>
//--></script>
<script type="text/javascript"><!--
$('input[name=\'path\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    json.unshift({
                        category_id: 0,
                        name: '<?php echo $text_none; ?>'
                    });

                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['category_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'path\']').val(item['label']);
            $('input[name=\'parent_id\']').val(item['value']);
        }
    });
    //--></script>
<script type="text/javascript"><!--
    $('input[name=\'filter\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['filter_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'filter\']').val('');

            $('#category-filter' + item['value']).remove();

            $('#category-filter').append('<div id="category-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="category_filter[]" value="' + item['value'] + '" /></div>');
        }
    });

    $('#category-filter').delegate('.fa-minus-circle', 'click', function () {
        $(this).parent().remove();
    });
    //--></script>
<script type="text/javascript"><!--
    $('#language a:first').tab('show');
    //--></script>
<?php echo $footer; ?>
