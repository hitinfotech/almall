<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button id="btn-translate" data-toggle="tooltip" title="<?php echo $button_translate; ?>" class="btn btn-info"><i class="fa fa-language"></i></button>
                <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>&nbsp;<a target="_blank" href="<?php echo HTTP_CATALOG . $external_link ?>"> Catalog View </a>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" name="form-product" class="form-horizontal">
                    <input type="hidden" id="global_product_id" value="<?php echo $global_product_id ?>">
                    <ul class="nav nav-tabs">
                        <li class="active"><a id="a-tab-general" href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li ><a id="a-tab-attribute" href="#tab-attribute" data-toggle="tab"><?php echo $tab_attribute . ' & ' . $tab_option; ?></a></li>
                        <li ><a id="a-tab-image" onclick="showimages()" href="#tab-image" data-toggle="tab"><?php echo $tab_image; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                    <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="product_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                                <?php if (isset($error_name[$language['language_id']])) { ?>
                                                    <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                            <div class="col-sm-10">
                                                <textarea name="product_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['description'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <input type="hidden" name="product_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                                        <input type="hidden" name="product_description[<?php echo $language['language_id']; ?>][meta_description]" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_description'] : ''; ?>" />
                                        <input type="hidden" name="product_description[<?php echo $language['language_id']; ?>][meta_keyword]" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control" vlaue="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_keyword'] : ''; ?>" />
                                        <input type="hidden" name="product_description[<?php echo $language['language_id']; ?>][tag]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['tag'] : ''; ?>" placeholder="<?php echo $entry_tag; ?>" id="input-tag<?php echo $language['language_id']; ?>" class="form-control" />
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_model; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="model" value="<?php echo $model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
                                    <?php if ($error_model) { ?>
                                        <div class="text-danger"><?php echo $error_model; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sku"><span data-toggle="tooltip" title="<?php echo $help_sku; ?>"><?php echo $entry_sku; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sku" value="<?php echo $sku; ?>" placeholder="<?php echo $entry_sku; ?>" id="input-sku" class="form-control" />
                                </div>
                            </div>
                            <input type="hidden" name="upc" value="" id="input-upc" class="form-control" />
                            <input type="hidden" name="ean" value="" id="input-ean" class="form-control" />
                            <input type="hidden" name="jan" value="" id="input-jan" class="form-control" />
                            <input type="hidden" name="isbn" value="" id="input-isbn" class="form-control" />
                            <input type="hidden" name="mpn" value="" id="input-mpn" class="form-control" />
                            <input type="hidden" name="tax_class_id" id="input-tax-class" class="form-control">
                            <input type="hidden" name="location" value="" id="input-location" class="form-control" />

                            <div class="form-group fbs <?php echo ($is_fbs == 0 && $is_fbs != 'on') ? 'hide' : ''; ?>">
                                <label class="col-sm-2 control-label" for="input-damaged-quantity"><?php echo $entry_damaged_quantity; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" id="input-damaged-quantity" name="damaged_quantity" value="<?php echo isset($damaged_quantity) ? $damaged_quantity : 0; ?>" placeholder="<?php echo $entry_damaged_quantity; ?>" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group fbs <?php echo ($is_fbs == 0 && $is_fbs != 'on') ? 'hide' : ''; ?>">
                                <label class="col-sm-2 control-label" for="input-lost-quantity"><?php echo $entry_lost_quantity; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" id="input-lost-quantity" name="lost_quantity" value="<?php echo isset($lost_quantity) ? $lost_quantity : 0; ?>" placeholder="<?php echo $entry_lost_quantity; ?>" class="form-control" />
                                </div>
                            </div>

                            <input type="hidden" name="minimum" value="<?php echo $minimum; ?>" placeholder="<?php echo $entry_minimum; ?>" id="input-minimum" class="form-control" />
                            <input type="hidden" name="subtract" value="1" />
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-stock-status"><span data-toggle="tooltip" title="<?php echo $help_stock_status; ?>"><?php echo $entry_stock_status; ?></span></label>
                                <div class="col-sm-10">
                                    <select name="stock_status_id" id="input-stock-status" class="form-control">
                                        <?php foreach ($stock_statuses as $stock_status) { ?>
                                            <?php if ($stock_status['stock_status_id'] == $stock_status_id) { ?>
                                                <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-is-return"><?php echo $entry_return_policy; ?></label>
                                <div class="col-sm-10">
                                    <select name="return_policy_id" id="input-is-return" class="form-control">
                                        <option value="0" selected="selected"><?php echo 'Default Return Prolicy'; ?></option>
                                        <?php foreach ($returnpolicies as $policy) { ?>
                                            <?php if ($policy['return_policy_id'] == $return_policy_id) { ?>
                                                <option value="<?php echo $policy['return_policy_id'] ?>" selected="selected"><?php echo $policy['name']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $policy['return_policy_id'] ?>"><?php echo $policy['name']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <input type="hidden" name="shipping" value="1" />
                            <input type="hidden" name="date_available" value="<?php echo $date_available; ?>" placeholder="<?php echo $entry_date_available; ?>" data-date-format="YYYY-MM-DD" id="input-date-available" class="form-control" />
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-is-fbs"><?php echo $entry_is_fbs; ?></label>
                                <div class="col-sm-10">
                                    <input type="checkbox" name="is_fbs" id="is_fbs"  placeholder="<?php echo $entry_is_fbs; ?>" id="input-is-fbs" class="form-control" <?php echo ($is_fbs == 1 || $is_fbs == 'on') ? 'checked' : ''; ?> />
                                </div>
                            </div>

                            <div class="form-group fbs <?php echo ($is_fbs == 0 && $is_fbs != 'on') ? 'hide' : ''; ?>">
                                <label class="col-sm-2 control-label" for="input-fbs_bin"><?php echo $entry_physcal_location; ?></label>
                                <div class="col-sm-10">
                                    <div class="row">
                                        <label class="col-sm-1 control-label" for="input-fbs_bin"><?php echo $entry_fbs_bin; ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="fbs_bin" value="<?php echo isset($fbs_bin) ? $fbs_bin : 0; ?>" placeholder="<?php echo $entry_fbs_bin; ?>" id="input-fbs_bin" class="form-control" />
                                        </div>
                                        <label class="col-sm-1 control-label" for="input-fbs_stack"><?php echo $entry_fbs_stack; ?></label>
                                        <div class="col-sm-5">
                                            <input type="text" name="fbs_stack" value="<?php echo isset($fbs_stack) ? $fbs_stack : 0; ?>" placeholder="<?php echo $entry_fbs_stack; ?>" id="input-fbs_stack" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group fbs <?php echo ($is_fbs == 0 && $is_fbs != 'on') ? 'hide' : ''; ?>">
                                <label class="col-sm-2 control-label" for="input-date-range"><?php echo $entry_date_range; ?></label>
                                <div class="col-sm-10">
                                    <div class="row">
                                        <label class="col-sm-1 control-label" for="input-date-in"><?php echo $entry_fbs_date_in; ?></label>
                                        <div class="col-sm-5">
                                            <div class="input-group date">
                                                <input type="text" name="date_in" value="<?php echo (isset($fbs_date_in) && $fbs_date_in != '0000-00-00') ? $fbs_date_in : ""; ?>" placeholder="<?php echo $entry_fbs_date_in; ?>" id="input-fbs-date-in" data-date-format="YYYY-MM-DD"  class="form-control" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <label class="col-sm-1 control-label" for="input-date-out"><?php echo $entry_fbs_date_out; ?></label>
                                        <div class="col-sm-5">
                                            <div class="input-group date">
                                                <input type="text" name="date_out" value="<?php echo (isset($fbs_date_out) && $fbs_date_out != '0000-00-00') ? $fbs_date_out : ""; ?>" placeholder="<?php echo $entry_fbs_date_out; ?>" id="input-fbs-date-out" data-date-format="YYYY-MM-DD"  class="form-control" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="length" value="<?php echo $length; ?>" placeholder="<?php echo $entry_length; ?>" id="input-length" class="form-control" />
                            <input type="hidden" name="width" value="<?php echo $width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-width" class="form-control" />
                            <input type="hidden" name="height" value="<?php echo $height; ?>" placeholder="<?php echo $entry_height; ?>" id="input-height" class="form-control" />
                            <input type="hidden" name="length_class_id" id="input-length-class" class="form-control" vlaue="0"/>
                            <input type="hidden" name="weight" value="<?php echo $weight; ?>" placeholder="<?php echo $entry_weight; ?>" id="input-weight" class="form-control" />
                            <input type="hidden" name="weight_class_id" id="input-weight-class" class="form-control" value="0"/>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="status" id="input-status" class="form-control">
                                        <?php if ($status) { ?>
                                            <option value="1" selected><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-algolia"><?php echo $entry_algolia; ?></label>
                                <div class="col-sm-10">
                                    <select name="is_algolia" id="input-algolia" class="form-control">
                                        <?php if ($is_algolia) { ?>
                                            <option value="1" selected="selected"><?php echo 'yes'; ?></option>
                                            <option value="0"><?php echo 'no'; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo 'yes'; ?></option>
                                            <option value="0" selected="selected"><?php echo 'no'; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                                </div>
                            </div>
                            <input type="hidden" name="manufacturer_id" value="<?php echo $manufacturer_id; ?>" />
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-brand"><span data-toggle="tooltip" title="<?php echo $help_brand; ?>"><?php echo $entry_brand; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="brand" value="<?php echo $brand ?>" placeholder="<?php echo $entry_brand; ?>" id="input-brand" class="form-control" />
                                    <input type="hidden" name="brand_id" value="<?php echo $brand_id; ?>" />
                                    <?php if ($error_brand) { ?>
                                        <div class="text-danger"><?php echo $error_brand; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-seller"><span data-toggle="tooltip" title="<?php echo $help_seller; ?>"><?php echo $entry_seller; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="seller" value="<?php echo $seller ?>" placeholder="<?php echo $entry_seller; ?>" id="input-seller" class="form-control" />
                                    <input type="hidden" name="seller_id" value="<?php echo $seller_id; ?>" />
                                    <?php if ($error_seller) { ?>
                                        <div class="text-danger"><?php echo $error_seller; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-price"><?php echo $entry_price; ?></label>
                                <div class="col-sm-2">
                                    <select name="pricecode" id="input-pricecode" onchange="changecur()" form="form-product" class="form-control">
                                        <?php foreach ($currencies as $row) { ?>
                                            <?php if ($row['code'] == $pricecode) { ?>
                                                <option value="<?php echo $row['code']; ?>" selected="selected"><?php echo $row['title']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $row['code']; ?>"><?php echo $row['title']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" name="price" value="<?php echo $price; ?>" placeholder="<?php echo $entry_price; ?>" onchange="myDiscount('original', 0); changecur();" id="input-price" class="form-control" />
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label" id="converter"><?php echo $converter_sar_price; ?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-discount"><?php echo $tab_special ?></label>
                                <div class="col-sm-10">
                                    <div class="table-responsive">
                                        <table id="special" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <td class="text-right"><?php echo $entry_disc_per; ?></td>
                                                    <td class="text-right"><?php echo $entry_discount; ?></td>
                                                    <td class="text-right"><?php echo $entry_net_price; ?></td>
                                                    <td class="text-left"><?php echo $entry_date_start; ?></td>
                                                    <td class="text-left"><?php echo $entry_date_end; ?></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody id="specialbody">
                                                <?php if (isset($error_special) && $error_special != '') { ?>
                                                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_special; ?>
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                </div>
                                            <?php } ?>
                                            <?php $special_row = 0; ?>
                                            <?php foreach ($product_specials as $product_special) { ?>
                                                <tr id="special-row<?php echo $special_row; ?>">
                                                    <td class="text-right">
                                                        <input type="text" id="perce<?php echo $special_row; ?>" onchange="myDiscount('perce', <?php echo $special_row; ?>)"  name="product_special[<?php echo $special_row; ?>][discount_per]" value ="<?php echo isset($product_special['percentage']) ? $product_special['percentage'] : ($price > 0 ? round(($price - $product_special['price']) / $price, 2) : 0); ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" />
                                                    </td>
                                                    <td class="text-right">
                                                        <input type="text" id="disco<?php echo $special_row; ?>" onchange="myDiscount('disco', <?php echo $special_row; ?>)" name="product_special[<?php echo $special_row; ?>][discount]" value ="<?php echo $price - $product_special['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" />
                                                    </td>
                                                    <td class="text-right">
                                                        <input type="text" id="price<?php echo $special_row; ?>" onchange="myDiscount('price', <?php echo $special_row; ?>)" name="product_special[<?php echo $special_row; ?>][price]" value="<?php echo $product_special['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" readonly=""/>
                                                    </td>
                                                    <td class="text-left" style="width: 20%;"><div class="input-group date">
                                                            <input type="text" name="product_special[<?php echo $special_row; ?>][date_start]" value="<?php echo $product_special['date_start']; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span></div>
                                                    </td>
                                                    <td class="text-left" style="width: 20%;"><div class="input-group date">
                                                            <input type="text" name="product_special[<?php echo $special_row; ?>][date_end]" value="<?php echo $product_special['date_end']; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span></div>
                                                    </td>
                                                    <td class="text-left"><button type="button" onclick="$('#special-row<?php echo $special_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                                </tr>
                                                <?php $special_row++; ?>
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="5"></td>
                                                    <td class="text-left"><button type="button" onclick="addSpecial();" data-toggle="tooltip" title="<?php echo $button_special_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php foreach ($product_filters as $product_filter) { ?>
                                <input type="hidden" name="product_filter[]" value="<?php echo $product_filter['filter_id']; ?>" />
                            <?php } ?>
                            <?php foreach ($product_store as $row) { ?>
                                <input type="hidden" name="product_store[]" value="<?php echo $row['store_id']; ?>" />
                            <?php } ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="<?php echo $help_related; ?>"><?php echo $entry_related; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="related" value="" placeholder="<?php echo $entry_related; ?>" id="input-related" class="form-control" />
                                    <div id="product-related" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach ($product_relateds as $product_related) { ?>
                                            <div id="product-related<?php echo $product_related['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_related['name']; ?>
                                                <input type="hidden" name="product_related[]" value="<?php echo $product_related['product_id']; ?>" />
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-color"><span data-toggle="tooltip" title="<?php echo $help_color; ?>"><?php echo $entry_color; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="color" value="" placeholder="<?php echo $entry_color; ?>" id="input-color" class="form-control" />
                                    <div id="product-color" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach ($product_colors as $product_color) { ?>
                                            <div id="product-color<?php echo $product_color['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_color['name']; ?>
                                                <input type="hidden" name="product_color[]" value="<?php echo $product_color['product_id']; ?>" />
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-color"><span data-toggle="tooltip" title="<?php echo $help_color; ?>"><?php echo $tab_how_to_ware; ?></span></label>
                                <div class="col-sm-10">
                                    <div class="table-responsive">
                                        <table id="how-to-ware-products" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <td class="text-left"><?php echo $entry_product; ?></td>
                                                    <td class="text-left"><?php echo $entry_sort_order; ?></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $how_to_ware_row = 0; ?>
                                                <?php if (isset($how_to_ware_product) && !empty($how_to_ware_product)) { ?>
                                                    <?php foreach ($how_to_ware_product as $ware_product) { ?>
                                                        <tr id="how-to-wear-row<?php echo $how_to_ware_row; ?>">
                                                            <td class="text-left"><input type="text" name="how_to_ware_product[<?php echo $how_to_ware_row; ?>][ware_id]" value="<?php echo $ware_product['ware_id']; ?> - <?php echo $ware_product['name']; ?>" placeholder="<?php echo $entry_wear; ?>" id="input-ware_id<?php echo $how_to_ware_row; ?>" class="how_to_wear form-control" /></td>
                                                            <td class="text-right"><input type="text" name="how_to_ware_product[<?php echo $how_to_ware_row; ?>][sort_order]" value="<?php echo $ware_product['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                                                            <td class="text-left"><button type="button" onclick="$('#how-to-wear-row<?php echo $how_to_ware_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                                        </tr>
                                                        <?php $how_to_ware_row++; ?>
                                                    <?php } ?>
                                                <?php } ?>
                                                <?php $how_to_ware_row++; ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td class="text-left"><button type="button" onclick="addItemHowToWear();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="table-responsive">
                                    <label class="col-sm-2 control-label" for="input-category">
                                        <span data-toggle="tooltip" title="<?php echo $help_category; ?>"><?php echo $entry_category; ?></span>
                                    </label>
                                    <div class="col-sm-10">
                                        <div id="product-category" class="well well-sm" style="height: 150px; overflow: auto;">
                                            <?php foreach ($product_categories as $product_category) { ?>
                                                <div id="product-category<?php echo $product_category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_category['name']; ?>
                                                    <input type="hidden" name="product_category[]" value="<?php echo $product_category['category_id']; ?>" />
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <?php if ($error_category) { ?>
                                            <div class="text-danger"><?php echo $error_category; ?></div>
                                        <?php } ?>
                                        <style>
                                            .add-prod-list li a{display: block;padding: 10px 5px;color: black;}ul{list-style: none;padding: 0;margin: 0;}
                                        </style>
                                        <script type="text/javascript">
                                            <?php if((int)$global_product_id == 0){ ?>
                                            var warning_message = 0;
                                            var clicked = 1;
                                            <?php } else { ?>
                                            var warning_message = 1;
                                            var clicked = 0;
                                            <?php } ?>
                                            function confirm_change(ele, mySel) {
                                                var res = false;
                                                if (warning_message == 1) {
                                                    res = confirm("By changing the category, you will lose all your options & Attributes selected before.");
                                                }
                                                if (warning_message == 1 && res == true) {
                                                    clicked = 1;
                                                    warning_message = 0;
                                                }
                                                if (clicked == 1) {
                                                    select_this(ele, mySel);
                                                    getSubCategories(ele);
                                                    ele.parent().find('li').attr('style', '');
                                                    ele.attr('style', 'border: 2px solid #000000');
                                                }
                                            }
                                            function select_this(element, mySel) {
                                                if (parseInt(mySel) <= 0) {
                                                    if(document.getElementById("product-category" + element.attr("data-category-id"))!=null){
                                                        alert("already exists");
                                                    } else {
                                                        $('#product-category' + element.attr("data-category-id")).remove();
                                                        $('#product-category').append('<div id="product-category' + element.attr("data-category-id") + '"><i class="fa fa-minus-circle"></i> ' + element.attr('data-category-addname') + '<input type="hidden" name="product_category[]" value="' + element.attr("data-category-id") + '" /></div>');
                                                        doCategory_ajax();
                                                    }
                                                    }
                                                    }
                                            function doCategory_ajax(){
                                                    $("#attribute").hide();
                                                    $("#tab-option").hide();
                                                    $("#attribute > tbody").each(function () {
                                                        $(this).remove();
                                                    });
                                                    var categories_string = "";
                                                    $("input[name^='product_category']").each(function () {
                                                        var category_id = $(this).val();
                                                        categories_string += category_id + ",";
                                                    });
                                                    $.ajax({
                                                        url: 'index.php?route=catalog/category/getCategoryAttributes&category_ids=' + categories_string,
                                                        type: 'get',
                                                        dataType: 'json',
                                                        success: function (json) {
                                                            $("#attribute > tbody").each(function(){
                                                                $(this).remove();
                                                            });
                                                            for (var prop in json) {
                                                                $("#attribute").show();
                                                                $("#add_attribute").trigger("click");
                                                                var attr_row_id = attr_input_name = '';
                                                                $("#attribute > tbody").each(function () {
                                                                    attr_row_id = $(this).attr("id");
                                                                    attr_input_name = $(this).find("tr > td > input:first").attr("name");
                                                                });
                                                                $("#" + attr_row_id + " > tr > td:first ").html("");
                                                                $("#" + attr_row_id + " > tr > td:first ").append("<label class='col-sm-3 control-label'>" + json[prop]['name'] + "</label>");
                                                                $("#" + attr_row_id + " > tr > td:first ").append("<div class='col-sm-6' ><select class='pull-left form-control' id='" + "row" + attr_row_id + "' name='" + attr_input_name + "'>" + json[prop]['name'] + "<option value='*'> Select " + json[prop]['name'] + "</option></select></div>");
                                                                json[prop]['attributes'].forEach(function (ele) {
                                                                    $("#" + "row" + attr_row_id).append("<option value='" + ele.attribute_id + "'>" + ele.name + "</option>");
                                                                });
                                                            }
                                                        }
                                                    });
                                                    $("#option > li").each(function () {
                                                        $(this).find("a > i").click();
                                                    });
                                                    $.ajax({
                                                        url: 'index.php?route=catalog/category/getCategoryOptions&category_ids=' + categories_string,
                                                        type: 'get',
                                                        dataType: 'json',
                                                        success: function (json) {
                                                            json.forEach(function (ele) {
                                                                addOption(ele);
                                                            });
                                                        }
                                                    });
                                                    }
                                            function getSubCategories(element) {
                                                element.parent().find('li').attr('style', '');
                                                element.parent().find('li i').remove();
                                                element.find("a").append('<i class="fa fa-caret-right pull-left" aria-hidden="true"></i>');
                                                var category_id = element.attr("data-category-id");
                                                var column_num = element.parent().parent().attr("data-column");
                                                $.ajax({
                                                    url: 'index.php?route=catalog/category/getCategoriesByParent&category_id=' + category_id,
                                                    type: 'get',
                                                    dataType: 'json',
                                                    success: function (json) {
                                                        var next_div = $("div[data-column=" + (parseInt(column_num) + 1) + "]");
                                                        next_div.find('ul li').each(function () {
                                                            $(this).remove();
                                                        });
                                                        var next_next_div = $("div[data-column=" + (parseInt(column_num) + 2) + "]");
                                                        next_next_div.find('ul li').each(function () {
                                                            $(this).remove();
                                                        });
                                                        var next_next_next_div = $("div[data-column=" + (parseInt(column_num) + 3) + "]");
                                                        next_next_next_div.find('ul li').each(function () {
                                                            $(this).remove();
                                                        });
                                                        if (!json[0]) {
                                                            element.parent().find('li i').remove();
                                                            element.parent().find('li').attr('style', '');
                                                            element.attr('style', 'border: 2px solid #000000');
                                                        }
                                                        $.each(json, function () {
                                                            var parent = parseInt(this.is_parent);
                                                            var listners = '';
                                                            if (column_num < 3) {
                                                                listners = "confirm_change($(this)," + parent + ");";
                                                            } else {
                                                                listners = "confirm_change($(this)," + parent + ");";
                                                            }
                                                            next_div.find("ul").append('<li data-category-id="' + this.category_id + '" data-category-addname="' + this.name + '" data-category-description="' + this.last_name + '" onclick=" ' + listners + '" ><a href="javascript:void(0)">' + this.last_name + '</a></li>')
                                                        });
                                                    }
                                                });
                                            }
                                            function re_calculate_quantity() {
                                                var total_quantity = 0;
                                                $('#options_tab > div.tab-pane').each(function () {
                                                    $(this).find("div.options_table table tbody tr").each(function (row, tr) {
                                                        $(this).find("td").each(function (column, td) {
                                                            if (typeof $(this).find("input").attr("name") != 'undefined' && $(this).find("input").hasClass('quantity-input')) {
                                                                if (!isNaN(parseInt($(this).find("input.form-control").val()))) {
                                                                    total_quantity += parseInt($(this).find("input.form-control").val());
                                                                }
                                                            }
                                                        });
                                                    });
                                                });
                                                $("#input-quantity").val(total_quantity);
                                            }
                                        </script>
                                        <div class="col-md-3 col-sm-3 col-xs-12" data-column="1">
                                            <ul class="add-prod-list">
                                                <?php foreach ($main_categories as $row) { ?>
                                                    <li data-category-id="<?php echo $row['category_id'] ?>" data-category-description="" data-category-addname="<?php echo $row['name'] ?>" onclick="confirm_change($(this), 1)" style="">
                                                        <a href="javascript:void(0)"><?php echo $row['last_name'] ?><i class="fa fa-caret-right pull-left" aria-hidden="true"></i></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12" data-column="2"><ul class="add-prod-list"></ul></div>
                                        <div class="col-md-3 col-sm-3 col-xs-12" data-column="3"><ul class="add-prod-list"></ul></div>
                                        <div class="col-md-3 col-sm-3 col-xs-12" data-column="4"><ul class="add-prod-list"></ul></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-attribute">
                            <div class="table-responsive">
                                <select id='select_attributes' class='hidden'>
                                    <option></option>
                                </select>
                                <table id="attribute" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-right"><?php echo $entry_attribute; ?></td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <?php $attribute_row = 0; ?>
                                    <?php foreach ($product_attributes as $product_attribute) { ?>
                                        <tbody id="attribute-row<?php echo $attribute_row; ?>">
                                            <tr>
                                                <td class="text-left">
                                                    <input type="hidden" name="product_attribute[<?php echo $attribute_row; ?>][attribute_id]" value="<?php echo $product_attribute['attribute_id']; ?>" class='attr_id' />
                                                    <input type="text" name="product_attribute[<?php echo $attribute_row; ?>][name]" value="<?php echo $product_attribute['name']; ?>" placeholder="<?php echo $entry_attribute; ?>" class="form-control" readonly='readonly' />
                                                </td>
                                                <td class="text-left">
                                                    <button type="button" onclick="$(this).parent().parent().remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <?php $attribute_row++; ?>
                                    <?php } ?>
                                    <tfoot>
                                        <tr >
                                            <td ></td>
                                            <td class="text-left">
                                                <button type="button" onclick="addAttribute();" data-toggle="tooltip" title="<?php echo $button_add_attribute; ?>" class="btn btn-primary hide" id="add_attribute"><i class="fa fa-plus-circle"></i></button>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div id='hidden-attribute' class='hide'> </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_quantity ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="quantity" value="<?php echo $quantity ?>" placeholder="<?php echo $entry_quantity ?>" id="input-quantity" class="form-control">
                                </div>
                            </div>
                            <legend><?php echo $entry_option ?></legend>
                            <div class="row" id="tab-option">
                                <div class="col-sm-3">
                                    <ul class="nav nav-pills nav-stacked" id="option">
                                        <?php $option_row = 0; ?>
                                        <?php foreach ($product_options as $product_option) { ?>
                                            <li><a href="#tab-option<?php echo $option_row; ?>" data-toggle="tab" id="option<?php echo $option_row; ?>"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-option<?php echo $option_row; ?>\']').parent().remove();
                                                        $('#tab-option<?php echo $option_row; ?>').remove();
                                                        $('#option a:first').tab('show');
                                                        re_calculate_quantity()"></i> <?php echo $product_option['name']; ?></a></li>
                                                <?php $option_row++; ?>
                                            <?php } ?>
                                        <li>
                                            <input type="text" name="option" value="" placeholder="<?php echo $button_add_option; ?>" id="input-option" class="form-control hide" />
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-9">
                                    <div class="tab-content" id='options_tab'>
                                        <?php $option_row = 0; ?>
                                        <?php $option_value_row = 0; ?>
                                        <?php foreach ($product_options as $product_option) { ?>

                                            <div class="tab-pane" id="tab-option<?php echo $option_row; ?>">
                                                <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_id]" value="<?php echo $product_option['product_option_id']; ?>" class="product_option_id" />
                                                <input type="hidden" name="product_option[<?php echo $option_row; ?>][name]" value="<?php echo $product_option['name']; ?>" />
                                                <input type="hidden" name="product_option[<?php echo $option_row; ?>][option_id]" value="<?php echo $product_option['option_id']; ?>" class="option_id"  />
                                                <input type="hidden" name="product_option[<?php echo $option_row; ?>][type]" value="<?php echo $product_option['type']; ?>" class="option_type" />

                                                <?php if ($product_option['type'] == 'text') { ?>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($product_option['type'] == 'textarea') { ?>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                        <div class="col-sm-9">
                                                            <textarea name="product_option[<?php echo $option_row; ?>][option_value]" rows="5" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control"><?php echo $product_option['option_value']; ?></textarea>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($product_option['type'] == 'file') { ?>
                                                    <div class="form-group" style="display: none;">
                                                        <label class="col-sm-3 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control" />
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($product_option['type'] == 'date') { ?>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                        <div class="col-sm-3">
                                                            <div class="input-group date">
                                                                <input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD" id="input-value<?php echo $option_row; ?>" class="form-control" />
                                                                <span class="input-group-btn">
                                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                                </span></div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($product_option['type'] == 'time') { ?>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group time">
                                                                <input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="HH:mm" id="input-value<?php echo $option_row; ?>" class="form-control" />
                                                                <span class="input-group-btn">
                                                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                </span></div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($product_option['type'] == 'datetime') { ?>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                        <div class="col-sm-9">
                                                            <div class="input-group datetime">
                                                                <input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-value<?php echo $option_row; ?>" class="form-control" />
                                                                <span class="input-group-btn">
                                                                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                </span></div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image' || $product_option['type'] == 'size') { ?>
                                                    <div class="options_table">
                                                        <table id="option-value<?php echo $option_row; ?>" class="table table-striped table-bordered table-hover option-table">
                                                            <thead>
                                                                <tr>
                                                                    <td class="text-left"><?php echo $entry_option_value; ?>
                                                                        <?php if ($product_option['type'] == 'size') { ?>
                                                                            <select class='change-sizes' id="option-size-type<?php echo $product_option['option_id'] ?>" name="product_option[<?php echo $option_row; ?>][product_option_size_default]" >
                                                                                <option value="uk" <?php if ($product_option['size']['is_uk'] == '0') echo 'disabled'; ?>  <?php echo ($product_option['size_default'] == 'uk') ? 'selected' : '' ?>>UK</option>
                                                                                <option value="us" <?php if ($product_option['size']['is_us'] == '0') echo 'disabled'; ?>  <?php echo ($product_option['size_default'] == 'us') ? 'selected' : '' ?>>US</option>
                                                                                <option value="eur" <?php if ($product_option['size']['is_eur'] == '0') echo 'disabled'; ?>  <?php echo ($product_option['size_default'] == 'eur') ? 'selected' : '' ?>>EUR</option>
                                                                                <option value="xmls" <?php if ($product_option['size']['is_xmls'] == '0') echo 'disabled'; ?>  <?php echo ($product_option['size_default'] == 'xmls') ? 'selected' : '' ?>>Intl</option>
                                                                            </select>
                                                                        <?php } ?>
                                                                    </td>
                                                                    <td class="text-right"><?php echo $entry_quantity; ?></td>
                                                                    <td class="text-right"><?php echo $entry_sku_size; ?></td>
                                                                    <td></td>
                                                                </tr>
                                                            </thead>
                                                            <?php foreach ($product_option['product_option_value'] as $product_option_value) { ?>
                                                                <tbody id="option-value-row<?php echo $option_value_row; ?>">
                                                                    <tr>
                                                                        <td class="text-left">
                                                                            <select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][option_value_id]" class="form-control">
                                                                                <?php if (isset($option_values[$product_option['option_id']])) { ?>
                                                                                    <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                                                                                        <?php
                                                                                        $size_data = '';
                                                                                        if ($product_option['type'] == 'size') {
                                                                                            $opValues = ($product_option['option_size_values'][$option_value['option_value_id']]);
                                                                                            $size_data = " data-opsize='option-size-type" . $product_option['option_id'] . "' data-uk='" . $opValues['uk'] . "' data-us='" . $opValues['us'] . "' data-eur='" . $opValues['eur'] . "' data-xmls='" . $opValues['xmls'] . "' ";
                                                                                        }
                                                                                        ?>
                                                                                        <?php if ($option_value['option_value_id'] == $product_option_value['option_value_id']) { ?>
                                                                                            <option value="<?php echo $option_value['option_value_id']; ?>" selected="selected" <?php echo $size_data ?> ><?php echo $option_value['name']; ?></option>
                                                                                        <?php } else { ?>
                                                                                            <option value="<?php echo $option_value['option_value_id']; ?>" <?php echo $size_data ?> ><?php echo $option_value['name']; ?></option>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                            </select>
                                                                            <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][product_option_value_id]" value="<?php echo $product_option_value['product_option_value_id']; ?>" /></td>
                                                                        <td class="text-right"><input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][quantity]" value="<?php echo $product_option_value['quantity']; ?>" class="form-control quantity-input" onchange="re_calculate_quantity()" /></td>
                                                                        <td class="text-right"><input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][sku_size]" value="<?php echo $product_option_value['sku_size']; ?>" class="form-control" /></td>
                                                                        <td class="text-left">
                                                                            <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][product_option_value_id]" value="<?php echo $product_option_value['product_option_value_id']; ?>" />
                                                                            <button type="button" onclick="$('#option-value-row<?php echo $option_value_row; ?>').remove(); re_calculate_quantity();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                                <?php $option_value_row++; ?>
                                                            <?php } ?>
                                                            <tfoot>
                                                                <tr>
                                                                    <td colspan="3"></td>
                                                                    <td class="text-left">
                                                                        <button type="button" onclick="addOptionValue('<?php echo $option_row; ?>');" data-toggle="tooltip" title="<?php echo $button_add_option_value; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                    <select id="option-values<?php echo $option_row; ?>" style="display: none;">
                                                        <?php if (isset($option_values[$product_option['option_id']])) { ?>
                                                            <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                                                                <?php
                                                                $size_data = '';

                                                                if ($product_option['type'] == 'size') {
                                                                    $opValues = ($product_option['option_size_values'][$option_value['option_value_id']]);
                                                                    $size_data = " data-opsize='option-size-type" . $product_option['option_id'] . "' data-uk='" . $opValues['uk'] . "' data-us='" . $opValues['us'] . "' data-eur='" . $opValues['eur'] . "' data-xmls='" . $opValues['xmls'] . "' ";
                                                                }
                                                                ?>
                                                                <option value="<?php echo $option_value['option_value_id']; ?>" <?php echo $size_data ?>><?php echo $option_value['name']; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>

                                                <?php } ?>
                                            </div><!--tab-pane-->
                                            <?php $option_row++; ?>
                                        <?php } ?>
                                    </div><!--tab-content-->
                                </div><!--col-sm-9-->
                            </div><!--Row-->
                        </div>

                        <div class="tab-pane" id="tab-image">
                            <div class="table-responsive">
                                <table id="images" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="text-left"><?php echo $entry_image; ?></td>
                                            <td class="text-right"><?php echo $entry_sort_order; ?></td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $image_row = 0; ?>
                                        <?php foreach ($product_images as $product_image) { ?>
                                            <tr id="image-row<?php echo $image_row; ?>">
                                                <td class="text-left"><a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $product_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_image[<?php echo $image_row; ?>][image]" value="<?php echo $product_image['image']; ?>" id="input-image<?php echo $image_row; ?>" /></td>
                                                <td class="text-right"><input type="text" name="product_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $product_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                                                <td class="text-left"><button type="button" onclick="$('#image-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                            </tr>
                                            <?php $image_row++; ?>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2"></td>
                                            <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
                <div id='fileupload_div' style='display: none'>
                    <div class="row fileupload">
                        <div class="col-md-6">
                            <div class="table-responsive">
                                <!-- The file upload form used as target for the file upload widget -->
                                <form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
                                    <!-- Redirect browsers with JavaScript disabled to the origin page -->
                                    <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
                                    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                    <div class="row fileupload-buttonbar">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 vcenter">
                                                <span class="help-message">Drag images here or </span>
                                                <!-- The fileinput-button span is used to style the file input field as button -->
                                                <span class="btn btn-success fileinput-button">
                                                    <i class="fa fa-plus"></i>
                                                    <span>Add images</span>
                                                    <input type="file" name="files[]" multiple>
                                                </span>
                                                <!-- The global file processing state -->
                                                <span class="fileupload-process"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- The global progress state -->
                                            <div class="col-md-12 col-lg-12 col-sm-12 fileupload-progress fade">
                                                <!-- The global progress bar -->
                                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                </div>
                                                <!-- The extended global progress state -->
                                                <div class="progress-extended">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- The table listing the files available for upload/download -->
                                    <table role="presentation" class="table table-striped"><tbody class="files" id="instaloaderFilesTable"></tbody></table>
                                </form>
                            </div>
                        </div>
                        <!--div class="col-md-6">
                            <div class="search-boxes">
                              <div class="tao-search external-search-box">
                                <h2>Import from TaoBao, AliExpress or Alibaba</h2>
                                <div class="row">
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="taobao-url" placeholder="Insert Url Here" />
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-success" id="tao-search" style="width:100%;"><i class="icon-download icon-white"></i> Import </button>
                                    </div>
                                </div>
                                <div class="alert alert-warning" role="alert">...</div>
                              </div>
                            </div>
                        </div-->
                    </div>
                </div>
                <?php $discount_row = 0; ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function addOption(item){
        $("#tab-option").show();
        html  = '<div class="tab-pane" id="tab-option' + option_row + '">';
        html += ' <input type="hidden" name="product_option[' + option_row + '][product_option_id]" value=""  class="product_option_id" />';
        html += ' <input type="hidden" name="product_option[' + option_row + '][name]" value="' + item['name'] + '" />';
        html += ' <input type="hidden" name="product_option[' + option_row + '][option_id]" class="option_id" value="' + item['option_id'] + '" />';
        html += ' <input type="hidden" name="product_option[' + option_row + '][type]" value="' + item['type'] + '" class="option_type" />';
        if (item['type'] == 'text') {
            html += ' <div class="form-group">';
            html += '   <label class="col-sm-3 control-label" for="input-value' + option_row + '">OptionValue</label>';
            html += '   <div class="col-sm-9"><input type="text" name="product_option[' + option_row + '][option_value]" value="" placeholder="OptionValue" id="input-value' + option_row + '" class="form-control" /></div>';
            html += ' </div>';
        }
        if (item['type'] == 'textarea') {
            html += ' <div class="form-group">';
            html += '   <label class="col-sm-3 control-label" for="input-value' + option_row + '">OptionValue</label>';
            html += '   <div class="col-sm-9"><textarea name="product_option[' + option_row + '][option_value]" rows="5" placeholder="OptionValue" id="input-value' + option_row + '" class="form-control"></textarea></div>';
            html += ' </div>';
        }
        if (item['type'] == 'file') {
            html += ' <div class="form-group" style="display: none;">';
            html += '   <label class="col-sm-3 control-label" for="input-value' + option_row + '">OptionValue</label>';
            html += '   <div class="col-sm-9"><input type="text" name="product_option[' + option_row + '][option_value]" value="" placeholder="OptionValue" id="input-value' + option_row + '" class="form-control" /></div>';
            html += ' </div>';
        }
        if (item['type'] == 'date') {
            html += ' <div class="form-group">';
            html += '   <label class="col-sm-3 control-label" for="input-value' + option_row + '">OptionValue</label>';
            html += '   <div class="col-sm-3"><div class="input-group date"><input type="text" name="product_option[' + option_row + '][option_value]" value="" placeholder="OptionValue" data-date-format="YYYY-MM-DD" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
            html += ' </div>';
        }
        if (item['type'] == 'time') {
            html += ' <div class="form-group">';
            html += '   <label class="col-sm-3 control-label" for="input-value' + option_row + '">OptionValue</label>';
            html += '   <div class="col-sm-9"><div class="input-group time"><input type="text" name="product_option[' + option_row + '][option_value]" value="" placeholder="OptionValue" data-date-format="HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
            html += ' </div>';
        }
        if (item['type'] == 'datetime') {
            html += ' <div class="form-group">';
            html += '   <label class="col-sm-3 control-label" for="input-value' + option_row + '">OptionValue</label>';
            html += '   <div class="col-sm-9"><div class="input-group datetime"><input type="text" name="product_option[' + option_row + '][option_value]" value="" placeholder="OptionValue" data-date-format="YYYY-MM-DD HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
            html += ' </div>';
        }
        if (item['type'] == 'select' || item['type'] == 'radio' || item['type'] == 'checkbox' || item['type'] == 'image'  || item['type'] == 'size') {
            var tmpSize = "";
            if(item['type'] == 'size'){
                tmpSize += '<select class="change-sizes" id="option-size-type' + item['value'] + '"  name="product_option[' + option_row + '][product_option_size_default]">';
                if(item['enabled_size']['is_uk'] != '0')
                    tmpSize += '<option value="uk">UK</option>';
                else
                    tmpSize += '<option value="uk" disabled>UK</option>';
                if(item['enabled_size']['is_us'] != '0')
                    tmpSize += '<option value="us">US</option>';
                else
                    tmpSize += '<option value="us" disabled>US</option>';
                if(item['enabled_size']['is_eur'] != '0')
                    tmpSize += '<option value="eur">EUR</option>';
                else
                    tmpSize += '<option value="eur" disabled>EUR</option>';
                if(item['enabled_size']['is_xmls'] != '0')
                    tmpSize += '<option value="xmls">Intl</option>';
                else
                    tmpSize += '<option value="xmls" disabled>Intl</option>';
                if(item['enabled_size']['is_it'] != '0')
                    tmpSize += '<option value="it">IT</option>';
                else
                    tmpSize += '<option value="it" disabled>IT</option>';
                tmpSize += '</select>';
            }
            html += '<div class="table-responsive options_table">';
            html += '  <table id="option-value' + option_row + '" class="table table-striped table-bordered table-hover">';
            html += '  	 <thead>';
            html += '      <tr>';
            html += '        <td class="text-left">OptionValue'+tmpSize+'</td>';
            html += '        <td class="text-right">Quantity</td>';
            html += '        <td class="text-left hidden">SubtractStock</td>';
            html += '        <td class="text-right hidden">Price</td>';
            html += '        <td class="text-right hidden">Points</td>';
            html += '        <td class="text-right hidden">Weight</td>';
            html += '        <td></td>';
            html += '      </tr>';
            html += '  	 </thead>';
            html += '  	 <tbody>';
            html += '    </tbody>';
            html += '    <tfoot>';
            html += '      <tr>';
            html += '        <td colspan="3"></td>';
            html += '        <td class="text-left"><button type="button" onclick="addOptionValue(' + option_row + ');" data-toggle="tooltip" title="Add Option Value" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>';
            html += '      </tr>';
            html += '    </tfoot>';
            html += '  </table>';
            html += '</div>';
            html += '  <select id="option-values' + option_row + '" style="display: none;">';
            for (i = 0; i < item['option_value'].length; i++) {
                var dataattr = '';
                if(item['type'] == 'size'){
                    dataattr = " data-opsize='option-size-type"+ item['value'] +"' data-uk='"+item['option_value'][i]['size']['uk']+"' data-us='"+item['option_value'][i]['size']['us']+"' data-eur='"+item['option_value'][i]['size']['eur']+"' data-xmls='"+item['option_value'][i]['size']['xmls']+"' data-it='"+item['option_value'][i]['size']['it']+"' ";
                }
                html += '  <option value="' + item['option_value'][i]['option_value_id'] + '" '+dataattr+'>' + item['option_value'][i]['name'] + '</option>';
            }
            $('#tab-option .tab-content').append(html);
            $('#option > li:last-child').before('<li><a href="#tab-option' + option_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'a[href=\\\'#tab-option' + option_row + '\\\']\').parent().remove(); $(\'#tab-option' + option_row + '\').remove(); $(\'#option a:first\').tab(\'show\');re_calculate_quantity()"></i> ' + item['name'] + '</li>');
            $('#option a[href=\'#tab-option' + option_row + '\']').tab('show');
            $('.date').datetimepicker({
                pickTime: false
            });
            $('.time').datetimepicker({
                pickDate: false
            });
            $('.datetime').datetimepicker({
                pickDate: true,
                pickTime: true
            });
            option_row++;
        }
        }
</script>
<script type="text/javascript">
<?php foreach ($languages as $language) { ?>
        $('#input-description<?php echo $language['language_id']; ?>').summernote({height: 300, toolbar: [['para', ['ul', 'ol', 'paragraph']], ['height', ['height']]]});
<?php } ?>
</script>
<script type="text/javascript">
// Color
    $('input[name=\'color\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'color\']').val('');
            $('#product-color' + item['value']).remove();
            $('#product-color').append('<div id="product-color' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_color[]" value="' + item['value'] + '" /></div>');
        }
    });
    $('#product-color').delegate('.fa-minus-circle', 'click', function () {
        $(this).parent().remove();
    });
    // Related
    $('input[name=\'related\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'related\']').val('');
            $('#product-related' + item['value']).remove();
            $('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_related[]" value="' + item['value'] + '" /></div>');
        }
    });
    $('#product-related').delegate('.fa-minus-circle', 'click', function () {
        $(this).parent().remove();
    });
    // Brand
    $('input[name=\'brand\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=mall/brand/autocomplete&token=<?php echo $token; ?>&filter_keyword=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    json.unshift({
                        id: 0,
                        name: '<?php echo $text_none; ?>'
                    });
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'brand\']').val(item['label']);
            $('input[name=\'brand_id\']').val(item['value']);
        }
    });
    // Seller
    $('input[name=\'seller\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=customerpartner/partner/autocompleteForProducts&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    json.unshift({
                        seller_id: 0,
                        company: '<?php echo $text_none; ?>'
                    });
                    response($.map(json, function (item) {
                        return {
                            label: item['company'],
                            value: item['seller_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'seller\']').val(item['label']);
            $('input[name=\'seller_id\']').val(item['value']);
        }
    });</script>
<script type="text/javascript">
    var x, y;
    $('#btn-translate').click(function (event) {
        $.ajax({
            url: 'index.php?route=helper/translate&token=<?php echo $token; ?>&q=' + encodeURIComponent($('#input-name2').val().replace(/(<([^>]+)>)/ig, "")) + '&source=ar&target=en&output=json',
            dataType: 'json',
            success: function (json) {
                x = json;
                $('#input-name1').val(json.data.translations[0].translatedText);
            }
        });
        $.ajax({
            url: 'index.php?route=helper/translate&token=<?php echo $token; ?>&q=' + encodeURIComponent($('#input-description2').val().replace(/(<([^>]+)>)/ig, "")) + '&source=ar&target=en&output=json',
            dataType: 'json',
            success: function (json) {
                y = json;
                $('#input-description1').text(json.data.translations[0].translatedText);
                $('#input-description1').summernote('code', json.data.translations[0].translatedText);
                $('#language1 .note-editable').html(json.data.translations[0].translatedText);
            }
        });
    });</script>
<script type="text/javascript">
    var attribute_row = <?php echo $attribute_row; ?>;
    function addAttribute() {
        html  = '<tbody id="attribute-row' + attribute_row + '">';
        html += '  <tr>';
        html += '    <td class="text-left" id="product_attribute-' + attribute_row + '"><input type="hidden" name="product_attribute[' + attribute_row + '][attribute_id]" value="" />';
        html +='   </td>';
        html += '    <td class="text-left"><button type="button" onclick="$(\'#attribute-row' + attribute_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '  </tr>';
        html += '</tbody>';
        $('#attribute tfoot').before(html);
        $("#select_attributes").clone().appendTo('#product_attribute-' + attribute_row);
        $('#product_attribute-' + attribute_row + ' select').removeClass("hidden");
        $('#product_attribute-' + attribute_row + ' select').addClass("pull-left form-control");
        $('input[name=\'product_attribute[' + attribute_row + '][name]\']').click(function(){
            $(this).autocomplete("search");
        });
        attribute_row++;
    }
</script>
<script type="text/javascript">
    var option_row = <?php echo $option_row; ?>;
    $('input[name=\'option\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/option/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            category: item['category'],
                            label: item['name'],
                            value: item['option_id'],
                            type: item['type'],
                            option_value: item['option_value'],
                            enabled_size: item['enabled_size']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            html = '<div class="tab-pane" id="tab-option' + option_row + '">';
            html += '	<input type="hidden" name="product_option[' + option_row + '][product_option_id]" value="" />';
            html += '	<input type="hidden" name="product_option[' + option_row + '][name]" value="' + item['label'] + '" />';
            html += '	<input type="hidden" name="product_option[' + option_row + '][option_id]" value="' + item['value'] + '" />';
            html += '	<input type="hidden" name="product_option[' + option_row + '][type]" value="' + item['type'] + '" />';
            html += '	<div class="form-group">';
            html += '	  <label class="col-sm-2 control-label" for="input-required' + option_row + '"><?php echo $entry_required; ?></label>';
            html += '	  <div class="col-sm-10"><select name="product_option[' + option_row + '][required]" id="input-required' + option_row + '" class="form-control">';
            html += '	      <option value="1"><?php echo $text_yes; ?></option>';
            html += '	      <option value="0"><?php echo $text_no; ?></option>';
            html += '	  </select></div>';
            html += '	</div>';
            if (item['type'] == 'text') {
                html += '	<div class="form-group">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control" /></div>';
                html += '	</div>';
            }

            if (item['type'] == 'textarea') {
                html += '	<div class="form-group">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-10"><textarea name="product_option[' + option_row + '][value]" rows="5" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control"></textarea></div>';
                html += '	</div>';
            }

            if (item['type'] == 'file') {
                html += '	<div class="form-group" style="display: none;">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control" /></div>';
                html += '	</div>';
            }

            if (item['type'] == 'date') {
                html += '	<div class="form-group">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-3"><div class="input-group date"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
                html += '	</div>';
            }

            if (item['type'] == 'time') {
                html += '	<div class="form-group">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-10"><div class="input-group time"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
                html += '	</div>';
            }

            if (item['type'] == 'datetime') {
                html += '	<div class="form-group">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-10"><div class="input-group datetime"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
                html += '	</div>';
            }

            if (item['type'] == 'select' || item['type'] == 'radio' || item['type'] == 'checkbox' || item['type'] == 'image' || item['type'] == 'size') {
                var tmpSize = "";
                if (item['type'] == 'size') {


                    tmpSize += '<select class="change-sizes" id="option-size-type' + item['value'] + '"  name="product_option[' + option_row + '][product_option_size_default]">';
                    if (item['enabled_size']['is_uk'] != '0')
                        tmpSize += '<option value="uk">UK</option>';
                    else
                        tmpSize += '<option value="uk" disabled>UK</option>';
                    if (item['enabled_size']['is_us'] != '0')
                        tmpSize += '<option value="us">US</option>';
                    else
                        tmpSize += '<option value="us" disabled>US</option>';
                    if (item['enabled_size']['is_eur'] != '0')
                        tmpSize += '<option value="eur">EUR</option>';
                    else
                        tmpSize += '<option value="eur" disabled>EUR</option>';
                    if (item['enabled_size']['is_xmls'] != '0')
                        tmpSize += '<option value="xmls">Intl</option>';
                    else
                        tmpSize += '<option value="xmls" disabled>Intl</option>';
                    tmpSize += '</select>';
                }

                html += '<div class="table-responsive">';
                html += '  <table id="option-value' + option_row + '" class="table table-striped table-bordered table-hover">';
                html += '  	 <thead>';
                html += '      <tr>';
                html += '        <td class="text-left"><?php echo $entry_option_value; ?>' + tmpSize + '</td>';
                html += '        <td class="text-right"><?php echo $entry_quantity; ?></td>';
                html += '        <td class="text-left hidden"><?php echo $entry_subtract; ?></td>';
                html += '        <td class="text-right hidden"><?php echo $entry_price; ?></td>';
                html += '        <td class="text-right hidden"><?php echo $entry_option_points; ?></td>';
                html += '        <td class="text-right hidden"><?php echo $entry_weight; ?></td>';
                html += '        <td></td>';
                html += '      </tr>';
                html += '  	 </thead>';
                html += '  	 <tbody>';
                html += '    </tbody>';
                html += '    <tfoot>';
                html += '      <tr>';
                html += '        <td colspan="2"></td>';
                html += '        <td class="text-left"><button type="button" onclick="addOptionValue(' + option_row + ');" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>';
                html += '      </tr>';
                html += '    </tfoot>';
                html += '  </table>';
                html += '</div>';
                html += '  <select id="option-values' + option_row + '" style="display: none;">';
                for (i = 0; i < item['option_value'].length; i++) {
                    var dataattr = '';
                    if (item['type'] == 'size') {
                        dataattr = " data-opsize='option-size-type" + item['value'] + "' data-uk='" + item['option_value'][i]['size']['uk'] + "' data-us='" + item['option_value'][i]['size']['us'] + "' data-eur='" + item['option_value'][i]['size']['eur'] + "' data-xmls='" + item['option_value'][i]['size']['xmls'] + "' ";
                    }
                    html += '  <option value="' + item['option_value'][i]['option_value_id'] + '" ' + dataattr + '>' + item['option_value'][i]['name'] + '</option>';
                }

                html += '  </select>';
                html += '</div>';
            }

            $('#tab-option .tab-content').append(html);
            $('#option > li:last-child').before('<li><a href="#tab-option' + option_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'a[href=\\\'#tab-option' + option_row + '\\\']\').parent().remove(); $(\'#tab-option' + option_row + '\').remove(); $(\'#option a:first\').tab(\'show\')"></i> ' + item['label'] + '</li>');
            $('#option a[href=\'#tab-option' + option_row + '\']').tab('show');
            $('.date').datetimepicker({
                pickTime: false
            });
            $('.time').datetimepicker({
                pickDate: false
            });
            $('.datetime').datetimepicker({
                pickDate: true,
                pickTime: true
            });
            option_row++;
        }
    });</script>
<script type="text/javascript">
    var option_value_row = <?php echo $option_value_row; ?>;
    function addOptionValue(option_row) {
        html = '<tr id="option-value-row' + option_value_row + '">';
        html += '<td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_value_id]" class="form-control">';
        html += $('#option-values' + option_row).html();
        html += '  </select><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][product_option_value_id]" value="" /></td>';
        html += '  <td class="text-right"><input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][quantity]" value="" onchange="re_calculate_quantity()" placeholder="<?php echo $entry_quantity; ?>" class="form-control quantity-input" /></td>';
        html += '  <td class="text-right"><input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][sku_size]" value="" placeholder="<?php echo $entry_sku_size; ?>" class="form-control" /></td>';
        html += '  <td class="text-left"><button type="button" onclick="$(this).tooltip(\'destroy\');$(\'#option-value-row' + option_value_row + '\').remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#option-value' + option_row + ' tbody').parent().append(html);
        $('[rel=tooltip]').tooltip();
        option_value_row++;
        $('.change-sizes').change();
    }
</script>
<script type="text/javascript">
    var discount_row = <?php echo $discount_row; ?>;</script>
<script type="text/javascript">
    var special_row = <?php echo $special_row; ?>;
    function addSpecial() {
        html = '<tr id="special-row' + special_row + '">';
        html += '<td class="text-right"><input type="text" id="perce' + special_row + '" onchange="myDiscount(\'perce\', ' + special_row + ')" name="product_special[' + special_row + '][discount_per]" value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
        html += '<td class="text-right"><input type="text" id="disco' + special_row + '" onchange="myDiscount(\'disco\', ' + special_row + ')" name="product_special[' + special_row + '][discount]"     value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
        html += '<td class="text-right"><input type="text" id="price' + special_row + '" onchange="myDiscount(\'price\', ' + special_row + ')" name="product_special[' + special_row + '][price]"        value="0" placeholder="<?php echo $entry_price; ?>" class="form-control" readonly/></td>';
        html += '<td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_special[' + special_row + '][date_start]" value="" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
        html += '<td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_special[' + special_row + '][date_end]" value="" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
        html += '<td class="text-left"><button type="button" onclick="$(\'#special-row' + special_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';
        $('#special tbody').append(html);
        $('.date').datetimepicker({
            pickTime: false
        });
        special_row++;
    }
</script>
<script type="text/javascript">
    var old_cur_code = '<?php echo $pricecode; ?>';
    function convertinto(price, from, to) {
        var f = 1;
        var t = 1;
        for (var i = 0; i < window.APP.currencies.length; i++) {
            if (window.APP.currencies[i].code == from) {
                f = window.APP.currencies[i].value;
            }
            if (window.APP.currencies[i].code == to) {
                t = window.APP.currencies[i].value;
            }
        }
        var value = parseFloat(price * t / f).toFixed(3);
        return value;
    }
    function changecur() {
        // parseFloat("1.5551").toFixed(3);
        var new_price_code = document.getElementById('input-pricecode').value;
        if (new_price_code != old_cur_code) {
            var price = parseFloat(document.getElementById('input-price').value).toFixed(3);
            document.getElementById('input-price').value = convertinto(price, old_cur_code, new_price_code);
            var table = document.getElementById("specialbody");
            for (var i = 0, row; row = table.rows[i]; i++) {
                document.getElementById('price' + i).value = convertinto(document.getElementById('price' + i).value, old_cur_code, new_price_code);
                document.getElementById('disco' + i).value = convertinto(document.getElementById('disco' + i).value, old_cur_code, new_price_code);
            }
            old_cur_code = new_price_code;
            document.getElementById('converter').innerHTML = "Value in SAR " + (convertinto(parseFloat(document.getElementById('input-price').value), new_price_code, 'SAR'));
        }
    }
    function myDiscount(type, row_no) {
        var sp_price = 0;
        var sp_disco = 0;
        var sp_perce = 0;
        var sar_value = 0;
        var x = $('#input-price').val();
        var price = parseFloat(x);
        if (type == 'original') {
            var table = document.getElementById("specialbody");
            for (var i = 0, row; row = table.rows[i]; i++) {
                if (price > 0) {
                    sp_perce = parseFloat(document.getElementById('perce' + i).value).toFixed(3);
                    sp_disco = parseFloat(sp_perce * price).toFixed(3);
                    sp_price = parseFloat(price - sp_disco).toFixed(3);
                } else {
                    sp_price = 0;
                    sp_disco = 0;
                    sp_perce = 0;
                }
                document.getElementById('price' + i).value = sp_price;
                document.getElementById('disco' + i).value = sp_disco;
                document.getElementById('perce' + i).value = sp_perce;
            }
        } else {
            if (type == 'price') {
                x = $('#price' + row_no).val();
                sp_price = parseFloat(x).toFixed(3);
                sp_disco = parseFloat(price - sp_price).toFixed(3);
                if (price > 0) {
                    sp_perce = parseFloat(sp_disco / price).toFixed(3);
                } else {
                    sp_perce = 0;
                }
            }
            if (type == 'disco') {
                x = $('#disco' + row_no).val();
                sp_disco = parseFloat(x).toFixed(3);
                sp_price = parseFloat(price - sp_disco).toFixed(3);
                if (price > 0) {
                    sp_perce = parseFloat(sp_disco / price).toFixed(3);
                } else {
                    sp_perce = 0;
                }
            }
            if (type == 'perce') {
                x = $('#perce' + row_no).val();
                if (x >= 1) {
                    sp_perce = parseFloat(x / 100).toFixed(3);
                } else {
                    sp_perce = parseFloat(x).toFixed(3);
                }
                sp_disco = parseFloat(price * sp_perce).toFixed(3);
                sp_price = parseFloat(price - sp_disco).toFixed(3);
            }
            document.getElementById('price' + row_no).value = parseFloat(sp_price).toFixed(3);
            document.getElementById('disco' + row_no).value = parseFloat(sp_disco).toFixed(3);
            document.getElementById('perce' + row_no).value = parseFloat(sp_perce).toFixed(3);
        }
        var new_price_code = document.getElementById('input-pricecode').value;
        document.getElementById('converter').innerHTML = "Value in SAR " + (convertinto(parseFloat(document.getElementById('input-price').value), new_price_code, 'SAR'));
    }
</script>
<script type="text/javascript">
    var image_row = <?php echo $image_row; ?>;

    function addImage() {
        html = '<tbody id="image-row' + image_row + '">';
        html += '  <tr>';
        html += '  <td class="text-left"><input type="file" class="hide" name="product_image[' + image_row + '][image]"><img src="<?php echo $placeholder; ?>" class="img-thumbnail click-file" data-placeholder="<?php echo $placeholder; ?>" /><input type="hidden" name="product_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
        html += '  <td class="text-right"><input type="text" name="product_image[' + image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';
        html += '</tbody>';

        $('#images tfoot').before(html);

        image_row++;

    }
</script>
<script type="text/javascript">
    var how_to_ware_row = <?php echo $how_to_ware_row; ?>;
    function addItemHowToWear() {
        html = '<tr id="how-to-wear-row' + how_to_ware_row + '">';
        html += '  <td class="text-left"><input type="text" name="how_to_ware_product[' + how_to_ware_row + '][ware_id]" value="" placeholder="<?php echo $entry_wear; ?>" id="input-ware_id' + how_to_ware_row + '" class="how_to_wear form-control" /></td>';
        html += '  <td class="text-right"><input type="text" name="how_to_ware_product[' + how_to_ware_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#how-to-wear-row' + how_to_ware_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';
        $('#how-to-ware-products tbody').append(html);
        $('#input-ware_id' + how_to_ware_row).autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $(this).val('');
                $(this).val('' + item['value'] + ' - ' + item['label'] + '');
            }
        });
        how_to_ware_row++;
    }

</script>
<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });
    $('.time').datetimepicker({
        pickDate: false
    });
    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });</script>
<script type="text/javascript">
    $('#language a:first').tab('show');
    $('#option a:first').tab('show');
</script>
<style>.product_stores ul.dropdown-menu{max-height: 200px;overflow-y: scroll;overflow-x: hidden;}</style>

<script type="text/javascript">
    $(function () {
        $(document).on('change', '.change-sizes', function () {
            var value = this.value;
            var id = $(this).attr('id');
            $('option[data-opsize="' + id + '"]').each(function (index) {
                $(this).html($(this).attr('data-' + value));
            });
        });
        if ($('.change-sizes'))
            $('.change-sizes').change();
    })
</script>
<div id="55"></div>
<script type="text/javascript">
    $('#product-category').delegate('.fa-minus-circle', 'click', function () {
        $(this).parent().remove();
        doCategory_ajax();
    });
</script>
<script type="text/javascript">
    var showimagesdone = 0;
    function showimages(){
        if(showimagesdone == 0){
        $("#fileupload_div").appendTo("#divfileupload");
        $("#fileupload_div").show();
        $(".fileupload").removeClass('hidden');
        if( (location.search.split(name + '=')[1] || '').split('&')[1] == null) {
            performTaoSearch();
            initilizeFileUploader();
            bindUploadAndDestroyEvents();
            loadScriptsForNeededPage();
        }
        showimagesdone = 1;
    }
    }
</script>
<script type="text/javascript">
    $( document ).ready(function(event) {
        $("#is_fbs").click(function () {
            if ($(this).is(":checked")) {
                $(".fbs").each(function () {
                    $(this).removeClass("hide");
                });
            } else {
                $(".fbs").each(function () {
                    $(this).addClass("hide");
                });
            }
        });
    });
</script>
<style>
    .preview a img{width: 314px;height: 460px;}
</style>
<?php echo $footer; ?>
