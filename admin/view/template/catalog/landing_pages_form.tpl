<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-store" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
              </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="panel panel-default">
          <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
          </div>
          <div class="panel-body">
              <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-store" class="form-horizontal">
                  <div class="form-group ">
                      <label class="col-sm-2 control-label" for="input_landing_page_name"><?php echo $text_input_landing_page_name; ?></label>
                      <div class="col-sm-10">
                          <input type="text" name="landing_page_name" value="<?php echo  (isset($page_data['page_name']) ? $page_data['page_name'] : '') ; ?>" placeholder="<?php echo $text_input_landing_page_name; ?>" id="input_landing_page_name" class="form-control" />
                          <input type="hidden" name="page_id" value="<?php echo  (isset($page_id) ? $page_id : '0') ; ?>" />
                      </div>
                  </div>
                  <div class="form-group ">
                      <label class="col-sm-2 control-label" for="input_landing_page_name_ar"><?php echo $text_input_landing_page_name_ar; ?></label>
                      <div class="col-sm-10">
                          <input type="text" name="landing_page_name_ar" value="<?php echo  (isset($page_data['page_name_ar']) ? $page_data['page_name_ar'] : '') ; ?>" placeholder="<?php echo $text_input_landing_page_name_ar; ?>" id="input_landing_page_name_ar" class="form-control" />
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-2 control-label" for="input-icon"><?php echo $entry_icon; ?></label>
                      <div class="col-sm-10">
                          <img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                          <input type="file" name="icon" value="<?php echo $icon; ?>" id="input-icon" />
                      </div>
                  </div>
                  <div class="form-group ">
                      <label class="col-sm-2 control-label" for="input_landing_page_color"><?php echo $text_input_landing_color; ?></label>
                      <div class="col-sm-10">
                          <input type="text" name="color" value="<?php echo (isset($page_data['color']) ? $page_data['color'] : '') ; ?>" placeholder="<?php echo $text_input_landing_color; ?>" id="input_landing_page_color" class="form-control" />
                      </div>
                  </div>
                  <div class="form-group ">
                      <label class="col-sm-2 control-label" for="input_landing_page_background"><?php echo $text_input_landing_background; ?></label>
                      <div class="col-sm-10">
                          <input type="text" name="background" value="<?php echo (isset($page_data['background']) ? $page_data['background'] : '') ; ?>" placeholder="<?php echo $text_input_landing_background; ?>" id="input_landing_page_background" class="form-control" />
                      </div>
                  </div>
                  
                  <div class="form-group ">
                      <label class="col-sm-2 control-label" for="input_landing_page_hcolor"><?php echo $text_input_landing_hcolor; ?></label>
                      <div class="col-sm-10">
                          <input type="text" name="hcolor" value="<?php echo (isset($page_data['hcolor']) ? $page_data['hcolor'] : '') ; ?>" placeholder="<?php echo $text_input_landing_hcolor; ?>" id="input_landing_page_hcolor" class="form-control" />
                      </div>
                  </div>
                  <div class="form-group ">
                      <label class="col-sm-2 control-label" for="input_landing_page_hbackground"><?php echo $text_input_landing_hbackground; ?></label>
                      <div class="col-sm-10">
                          <input type="text" name="hbackground" value="<?php echo (isset($page_data['hbackground']) ? $page_data['hbackground'] : '') ; ?>" placeholder="<?php echo $text_input_landing_hbackground; ?>" id="input_landing_page_hbackground" class="form-control" />
                      </div>
                  </div>
                  
                  <div class="form-group ">
                      <label class="col-sm-2 control-label" for="input_show_at_homepage"><?php echo $text_show_at_homepage; ?></label>
                      <div class="col-sm-10">
                          <input type="checkbox" name="show_at_homepage" <?php echo  ((isset($page_data['show_at_homepage']) && $page_data['show_at_homepage'] ==1) ? 'checked' : '') ; ?> id="input_show_at_homepage" class="form-control" />
                      </div>
                  </div>
                  <div class="form-group ">
                      <label class="col-sm-2 control-label" for="input_fontbold"><?php echo $text_fontbold; ?></label>
                      <div class="col-sm-10">
                          <input type="checkbox" name="fontbold" <?php echo  ((isset($page_data['fontbold']) && $page_data['fontbold'] ==1) ? 'checked' : '') ; ?> id="input_fontbold" class="form-control" />
                      </div>
                  </div>
                  <div class="form-group ">
                      <label class="col-sm-2 control-label" for="input_status"><?php echo $text_status; ?></label>
                      <div class="col-sm-10">
                          <input type="checkbox" name="status" id="input_status" class="form-control" <?php echo  ((isset($page_data['status']) && $page_data['status'] ==1) ? 'checked' : '') ; ?> />
                      </div>
                  </div>
              </form>
            </div>
          </div>
      </div>
  </div>
<?php echo $footer; ?>
