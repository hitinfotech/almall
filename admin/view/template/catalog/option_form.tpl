<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-option" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-option" class="form-horizontal">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo $entry_name; ?></label>
                        <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                                <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                    <input type="text" name="option_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($option_description[$language['language_id']]) ? $option_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" class="form-control" />
                                </div>
                                <?php if (isset($error_name[$language['language_id']])) { ?>
                                    <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo $entry_front_name; ?></label>
                        <div class="col-sm-10">
                            <?php foreach ($languages as $language) { ?>
                                <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" /></span>
                                    <input type="text" name="option_description[<?php echo $language['language_id']; ?>][front_name]" value="<?php echo isset($option_description[$language['language_id']]) ? $option_description[$language['language_id']]['front_name'] : ''; ?>" placeholder="<?php echo $entry_front_name; ?>" class="form-control" />
                                </div>
                                <?php if (isset($error_name[$language['language_id']])) { ?>
                                    <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    
                   <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-cod"><?php echo 'IS COD'; ?></label>
                        <div class="col-sm-10">
                            <select name="cod" id="input-cod" class="form-control">
                                <option value="0" <?php if($cod != 1){ ?>selected="selected"<?php }?>><?php echo 'Yes'; ?></option>
                                <option value="1" <?php if($cod == 1){ ?>selected="selected"<?php }?>><?php echo 'No'; ?></option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-type"><?php echo $entry_type; ?></label>
                        <div class="col-sm-10">
                            <select name="type" id="input-type" class="form-control">
                                <optgroup label="<?php echo $text_choose; ?>">

                                    <?php if ($type == 'select') { ?>
                                        <option value="select" selected="selected"><?php echo $text_select; ?></option>
                                    <?php } else { ?>
                                        <option value="select"><?php echo $text_select; ?></option>
                                    <?php } ?>
                                    <?php if ($type == 'radio') { ?>
                                        <option value="radio" selected="selected"><?php echo $text_radio; ?></option>
                                    <?php } else { ?>
                                        <option value="radio"><?php echo $text_radio; ?></option>
                                    <?php } ?>
                                    <?php if ($type == 'checkbox') { ?>
                                        <option value="checkbox" selected="selected"><?php echo $text_checkbox; ?></option>
                                    <?php } else { ?>
                                        <option value="checkbox"><?php echo $text_checkbox; ?></option>
                                    <?php } ?>
                                    <?php if ($type == 'image') { ?>
                                        <option value="image" selected="selected"><?php echo $text_image; ?></option>
                                    <?php } else { ?>
                                        <option value="image"><?php echo $text_image; ?></option>
                                    <?php } ?>
                                    <?php if ($type == 'list') { ?>
                                        <option value="list" selected="selected">List</option>
                                    <?php } else { ?>
                                        <option value="list">List</option>
                                    <?php } ?>
                                    <?php if ($type == 'size') { ?>
                                    <option value="size" selected="selected">Size</option>
                                    <?php } else { ?>
                                    <option value="size">Size</option>
                                    <?php } ?>

                                </optgroup>
                                <optgroup label="<?php echo $text_input; ?>">
                                    <?php if ($type == 'text') { ?>
                                        <option value="text" selected="selected"><?php echo $text_text; ?></option>
                                    <?php } else { ?>
                                        <option value="text"><?php echo $text_text; ?></option>
                                    <?php } ?>
                                    <?php if ($type == 'textarea') { ?>
                                        <option value="textarea" selected="selected"><?php echo $text_textarea; ?></option>
                                    <?php } else { ?>
                                        <option value="textarea"><?php echo $text_textarea; ?></option>
                                    <?php } ?>
                                </optgroup>
                                <optgroup label="<?php echo $text_file; ?>">
                                    <?php if ($type == 'file') { ?>
                                        <option value="file" selected="selected"><?php echo $text_file; ?></option>
                                    <?php } else { ?>
                                        <option value="file"><?php echo $text_file; ?></option>
                                    <?php } ?>
                                </optgroup>
                                <optgroup label="<?php echo $text_date; ?>">
                                    <?php if ($type == 'date') { ?>
                                        <option value="date" selected="selected"><?php echo $text_date; ?></option>
                                    <?php } else { ?>
                                        <option value="date"><?php echo $text_date; ?></option>
                                    <?php } ?>
                                    <?php if ($type == 'time') { ?>
                                        <option value="time" selected="selected"><?php echo $text_time; ?></option>
                                    <?php } else { ?>
                                        <option value="time"><?php echo $text_time; ?></option>
                                    <?php } ?>
                                    <?php if ($type == 'datetime') { ?>
                                        <option value="datetime" selected="selected"><?php echo $text_datetime; ?></option>
                                    <?php } else { ?>
                                        <option value="datetime"><?php echo $text_datetime; ?></option>
                                    <?php } ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-category"><span data-toggle="tooltip" title="<?php echo $help_category; ?>"><?php echo $entry_category; ?></span></label>
                        <div class="col-sm-10">
                            <?php if ($error_category) { ?>
                                <div class="text-danger"><?php echo $error_category; ?></div>
                            <?php } ?>
                            <input type="text" name="category" value="" placeholder="<?php echo $entry_category; ?>" id="input-category" class="form-control" />
                            <div id="product-category" class="well well-sm" style="height: 150px; overflow: auto;">
                                <?php foreach ($option_categories as $option_category) { ?>
                                    <div id="product-category<?php echo $option_category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $option_category['name']; ?>
                                        <input type="hidden" name="option_category[]" value="<?php echo $option_category['category_id']; ?>" />
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                        </div>
                    </div>
                    <table id="option-value" class="table table-striped table-bordered table-hover" style="display: none">
                        <thead>
                            <tr>
                                <td class="text-right">ID</td>
                                <td class="text-left required"><?php echo $entry_option_value; ?></td>
                                <td class="text-left"><?php echo $entry_image; ?></td>
                                <td class="text-right"><?php echo $entry_sort_order; ?></td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $option_value_row = 0; ?>
                            <?php foreach ($option_values as $option_value) { ?>
                                <tr id="option-value-row<?php echo $option_value_row; ?>">
                                    <td class="text-right"><?php echo $option_value['option_value_id']; ?></td>
                                    <td class="text-left"><input type="hidden" name="option_value[<?php echo $option_value_row; ?>][option_value_id]" value="<?php echo $option_value['option_value_id']; ?>" />
                                        <?php foreach ($languages as $language) { ?>
                                            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                                <input type="text" name="option_value[<?php echo $option_value_row; ?>][option_value_description][<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($option_value['option_value_description'][$language['language_id']]) ? $option_value['option_value_description'][$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_option_value; ?>" class="form-control" />
                                            </div>
                                            <?php if (isset($error_option_value[$option_value_row][$language['language_id']])) { ?>
                                                <div class="text-danger"><?php echo $error_option_value[$option_value_row][$language['language_id']]; ?></div>
                                            <?php } ?>
                                        <?php } ?></td>
                                    <td class="text-left"><a href="" id="thumb-image<?php echo $option_value_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $option_value['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                        <input type="hidden" name="option_value[<?php echo $option_value_row; ?>][image]" value="<?php echo $option_value['image']; ?>" id="input-image<?php echo $option_value_row; ?>" /></td>
                                    <td class="text-right"><input type="text" name="option_value[<?php echo $option_value_row; ?>][sort_order]" value="<?php echo $option_value['sort_order']; ?>" class="form-control" /></td>
                                    <td class="text-left"><button type="button" onclick="$('#option-value-row<?php echo $option_value_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                </tr>
                                <?php $option_value_row++; ?>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4"></td>
                                <td class="text-left"><button type="button" onclick="addOptionValue();" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                            </tr>
                        </tfoot>
                    </table>



                    <table  class="table table-striped table-bordered table-hover tbSizeOption">
                        <thead>
                        <tr>
                            <td class="text-center" style="width:1%">ID</td>
                            <td class="text-center"><label><input name="is_uk" value="true" type="checkbox" checked disabled/> UK</label></td>
                            <td class="text-center"><label><input name="is_us" value="true" type="checkbox" <? if($is_us) echo 'checked' ?>/> US</label></td>
                            <td class="text-center"><label><input name="is_eur" value="true" type="checkbox" <? if($is_eur) echo 'checked' ?>/>  EUR</label></td>
                            <td class="text-center"><label><input name="is_xmls" value="true" type="checkbox" <? if($is_xmls) echo 'checked' ?>/>  Intl</label></td>
                            <td class="text-center"><label><input name="is_it" value="true" type="checkbox" <? if($is_it) echo 'checked' ?>/>  IT</label></td>
                            <td class="text-center"><?php echo $entry_sort_order; ?></td>
                            <td style="width:1%"></td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $max_size_id = 0;
                        if(isset($size_op)){

                                foreach($size_op as $id=>$size){ $max_size_id = $id; ?>

                            <tr>
                                <td class="text-center"  style="width:1%"><?=$id?></td>
                                <td class="text-center">
                                    <input name="size_op[<?=$id?>][uk]" value="<?=$size['uk']?>"   type="text"  class="form-control">
                                </td>
                                <td class="text-center">
                                    <input name="size_op[<?=$id?>][us]" value="<?=$size['us']?>"   type="text"  class="form-control"/>
                                </td>
                                <td class="text-center">
                                    <input name="size_op[<?=$id?>][eur]" value="<?=$size['eur']?>"  type="text"  class="form-control"/>
                                </td>
                                <td class="text-center">
                                    <input name="size_op[<?=$id?>][xmls]" value="<?=$size['xmls']?>" type="text"  class="form-control"/>
                                </td>
                                <td class="text-center">
                                    <input name="size_op[<?=$id?>][it]" value="<?=$size['it']?>" type="text"  class="form-control"/>
                                </td>
                                <td class="text-center">
                                    <input type="text" name="size_op[<?=$id?>][sort_order]" value="<?=$size['sort_order']?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" />
                                </td>
                                <td class="text-center"  style="width:1%">
                                    <button type="button" onclick="$(this).closest('tr').fadeOut('normal', function(){ $(this).remove();});" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                </td>
                            </tr>




                        <?php  }

                        }
                        ?>
                       </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="6"></td>
                            <td class="text-center" style="width:1%"><button type="button" onclick="addSizeOption();" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                        </tr>
                        </tfoot>
                    </table>

                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript"><!--
  $('select[name=\'type\']').on('change', function () {
            if (this.value == 'select' || this.value == 'radio' || this.value == 'checkbox' || this.value == 'image' || this.value == 'list') {
                $('.tbSizeOption').hide();
                $("#option-value").show();
            } else if(this.value == 'size') {
                $('.tbSizeOption').show();
                $("#option-value").hide();

            }else{
                $('.tbSizeOption').hide();
                $("#option-value").hide();

            }
        });

        $('select[name=\'type\']').trigger('change');

        var option_value_row = <?php echo $option_value_row; ?>;

        function addOptionValue() {
            html = '<tr id="option-value-row' + option_value_row + '"><td></td>';
            html += '  <td class="text-left"><input type="hidden" name="option_value[' + option_value_row + '][option_value_id]" value="" />';
<?php foreach ($languages as $language) { ?>
                html += '    <div class="input-group">';
                html += '      <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span><input type="text" name="option_value[' + option_value_row + '][option_value_description][<?php echo $language['language_id']; ?>][name]" value="" placeholder="<?php echo $entry_option_value; ?>" class="form-control" />';
                html += '    </div>';
<?php } ?>
            html += '  </td>';
            html += '  <td class="text-left"><a href="" id="thumb-image' + option_value_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="option_value[' + option_value_row + '][image]" value="" id="input-image' + option_value_row + '" /></td>';
            html += '  <td class="text-right"><input type="text" name="option_value[' + option_value_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
            html += '  <td class="text-left"><button type="button" onclick="$(\'#option-value-row' + option_value_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#option-value tbody').append(html);

            option_value_row++;
        }
        //--></script></div>
<script>

        $(document).ready(function () {
            // Category
            $('input[name=\'category\']').autocomplete({
                'source': function (request, response) {
                    $.ajax({
                        url: 'index.php?route=catalog/category/autocompleteforproductform&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                        dataType: 'json',
                        success: function (json) {
                            response($.map(json, function (item) {
                                return {
                                    label: item['name'],
                                    value: item['category_id']
                                }
                            }));
                        }
                    });
                },
                'select': function (item) {
                    $('input[name=\'category\']').val('');

                    $('#product-category' + item['value']).remove();

                    $('#product-category').append('<div id="product-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="option_category[]" value="' + item['value'] + '" /></div>');
                }
            });

            $('#product-category').delegate('.fa-minus-circle', 'click', function () {
                $(this).parent().remove();
            });
        });



        var rowid = <?=$max_size_id?>;
        function addSizeOption(){

            var html = "";
            rowid++;

        html +='<tr>';
            html+='<td class="text-center"  style="width:1%">'+rowid+' <input type="hidden" name="size_op['+rowid+'][is_new]" value="1"/> </td>';
            html+='<td class="text-center"><input name="size_op['+rowid+'][uk]" value=""   type="text"  class="form-control"> </td>';
            html+='<td class="text-center"><input name="size_op['+rowid+'][us]" value=""   type="text"  class="form-control"/></td>';
            html+='<td class="text-center"><input name="size_op['+rowid+'][eur]" value=""  type="text"  class="form-control"/></td>';
            html+='<td class="text-center"><input name="size_op['+rowid+'][xmls]" value="" type="text"  class="form-control"/></td>';
            html+='<td class="text-center"><input name="size_op['+rowid+'][it]" value="" type="text"  class="form-control"/></td>';
            html+='<td class="text-center"><input type="text" name="size_op['+rowid+'][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
            html+='<td class="text-center"  style="width:1%"><button type="button" onclick="$(this).closest(\'tr\').fadeOut(\'normal\', function(){ $(this).remove();});" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html+='</tr>';


            var new_item = $(html).hide();
            $(".tbSizeOption tbody").append(new_item);
            new_item.show('normal');
         //  $(".tbSizeOption tbody").append(html);
        }
</script>

<?php echo $footer; ?>
