<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <?php if ($config_otp_tool) { ?>
                    <button type="button" data-toggle="tooltip" title="Toggle OTP Form" class="btn btn-info" onclick="otptoggle();"><i class="fa fa-eye"></i></button>
                <?php } ?>
                <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_copy; ?>" class="btn btn-default" onclick="$('#form-product').attr('action', '<?php echo $copy; ?>').submit()"><i class="fa fa-copy"></i></button>
              <!--<button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-product').submit() : false;"><i class="fa fa-trash-o"></i></button>
                -->
                <button type="button" data-toggle="tooltip" title="<?php echo $button_disable; ?>" class="btn btn-warning" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-product').submit() : false;"><i class="fa fa-ban"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <?php if ($config_otp_tool) { ?>
                    <div id="otp-form" class="table-responsive" style="display:none;">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td class="text-left">Import / Update Option Combinations</td>
                                    <td class="text-left">Export Option Combinations</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-left">
                                        <input type="text" id="filename" class="form-control form-group" value="" name="filename" readonly></input>
                                        <button id="upload-otp" class="btn btn-primary" type="button">Upload CSV</button>
                                        <button class="btn btn-primary" type="button" onclick="updateotp();">Import / Update</button>
                                    </td>
                                    <td class="text-left">
                                        <select id="otp-category" class="form-control form-group">
                                            <option value="0">All Categories</option>
                                            <?php if (!empty($otp_categories)) { ?>
                                                <?php foreach ($otp_categories as $category) { ?>
                                                    <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <button class="btn btn-primary" type="button" onclick="exportotp();">Export</button>
                                    </td>
                                </tr>
                            </tbody>
                            <thead>
                                <tr>
                                    <td class="text-left">Update Option Combinations By Model</td>
                                    <td class="text-left">Export Option Combinations By Model</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-left">
                                        <input type="text" id="filename-model" class="form-control form-group" value="" name="filename-model" readonly></input>
                                        <button id="upload-otp-model" class="btn btn-primary" type="button">Upload CSV</button>
                                        <button class="btn btn-primary" type="button" onclick="updateotpmodel();">Update</button>
                                    </td>
                                    <td class="text-left">
                                        <select id="otp-category-model" class="form-control form-group">
                                            <option value="0">All Categories</option>
                                            <?php if (!empty($otp_categories)) { ?>
                                                <?php foreach ($otp_categories as $category) { ?>
                                                    <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <button class="btn btn-primary" type="button" onclick="exportotpmodel();">Export</button>
                                    </td>
                                </tr>
                            </tbody>
                            <thead>
                                <tr>
                                    <td class="text-left">Import / Update Option Image Data</td>
                                    <td class="text-left">Export Option Image Data</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-left">
                                        <input type="text" id="filename-img" class="form-control form-group" value="" name="filename-img" readonly></input>
                                        <button id="upload-otp-img" class="btn btn-primary" type="button">Upload CSV</button>
                                        <button class="btn btn-primary" type="button" onclick="updateotpimg();">Import / Update</button>
                                    </td>
                                    <td class="text-left">
                                        <select id="otp-category-img" class="form-control form-group">
                                            <option value="0">All Categories</option>
                                            <?php if (!empty($otp_categories)) { ?>
                                                <?php foreach ($otp_categories as $category) { ?>
                                                    <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <button class="btn btn-primary" type="button" onclick="exportotpimg();">Export</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
                <div class="well">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-model"><?php echo $entry_model; ?></label>
                                <input type="text" name="filter_model" value="<?php echo $filter_model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-category"><?php echo $entry_category; ?></label>
                                <select name="filter_category" id="input-category" class="form-control">
                                    <option selected="selected" value="*">select Category</option>
                                    <?php foreach ($categories as $category) { ?>
                                        <?php if ($filter_category_id == $category['category_id']) { ?>
                                            <option value="<?php echo $category['category_id'] ?>" selected="selected"><?php echo $category['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $category['category_id'] ?>" ><?php echo $category['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <select name="filter_status" id="input-status" class="form-control">
                                    <option value="*"></option>
                                    <?php if ($filter_status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                    <?php } ?>
                                    <?php if (!$filter_status && !is_null($filter_status)) { ?>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="input-sku"><?php echo $entry_sku; ?></label>
                                <input type="text" name="filter_sku" value="<?php echo $filter_sku; ?>" placeholder="<?php echo $entry_sku; ?>" id="input-model" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-seller"><?php echo $entry_seller; ?></label>
                                <select name="filter_seller" id="input-seller" class="form-control">
                                    <option selected="selected" value="*">select Seller</option>
                                    <?php foreach ($sellers as $seller) { ?>
                                        <?php if ($filter_seller_id == $seller['customer_id']) { ?>
                                            <option value="<?php echo $seller['customer_id'] ?>" selected="selected"><?php echo $seller['company']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $seller['customer_id'] ?>" ><?php echo $seller['company']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-brand"><?php echo $entry_brand; ?></label>
                                <select name="filter_brand" id="input-brand" class="form-control">
                                    <option selected="selected" value="*">select Brand</option>
                                    <?php foreach ($brands as $brand) { ?>
                                        <?php if ($filter_brand == $brand['brand_id']) { ?>
                                            <option value="<?php echo $brand['brand_id'] ?>" selected="selected"><?php echo $brand['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $brand['brand_id'] ?>" ><?php echo $brand['name']; ?></option>
                                        <?php } ?>

                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-product_id"><?php echo $entry_product_id; ?></label>
                                <input type="text" name="filter_product_id" value="<?php echo $filter_product_id; ?>" placeholder="<?php echo $entry_product_id; ?>" id="input-model" class="form-control " />
                            </div>
                            
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label" for="input-date-added-start"><?php echo $entry_date_added_start; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_added_start" value="<?php echo $filter_date_added_start; ?>" placeholder="<?php echo $entry_date_added_start; ?>" data-date-format="YYYY-MM-DD" id="input-date-added-start" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-date-added-end"><?php echo $entry_date_added_end; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date_added_end" value="<?php echo $filter_date_added_end; ?>" placeholder="<?php echo $entry_date_added_end; ?>" data-date-format="YYYY-MM-DD" id="input-date-added-end" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $disable; ?>" method="post" enctype="multipart/form-data" id="form-product">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                    <td class="text-center"><?php echo $column_image; ?></td>
                                    <td class="text-right">ID</td>
                                    <td class="text-left"><?php if ($sort == 'pd.name') { ?>
                                            <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'p.model') { ?>
                                            <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                                        <?php } ?></td>

                                    <td class="text-left"><?php if ($sort == 'p.sku') { ?>
                                            <a href="<?php echo $sort_sku; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sku; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_sku; ?>"><?php echo $column_sku; ?></a>
                                        <?php } ?></td>


                                    <?php /* <td class="text-right">
                                      <?php if ($sort == 'p.price') { ?>
                                      <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                                      <?php } else { ?>
                                      <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                                      <?php } ?>
                                      </td> */ ?>
                                    <td class="text-left"><?php if ($sort == 'bd.name') { ?>
                                            <a href="<?php echo $sort_brand; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_brand; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_brand; ?>"><?php echo $column_brand; ?></a>
                                        <?php } ?></td>
                                    <td class="text-right"><?php if ($sort == 'p.quantity') { ?>
                                            <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                                        <?php } ?></td>
                                    <td class="text-right"><?php if ($sort == 'p.algorithm') { ?>
                                            <a href="<?php echo $sort_algorithm; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_algorithm; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_algorithm; ?>"><?php echo $column_algorithm; ?></a>
                                        <?php } ?></td>
                                    <td class="text-right"><?php if ($sort == 'p.sort_order') { ?>
                                            <a href="<?php echo $sort_sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_push; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_sort_order; ?>"><?php echo $column_push; ?></a>
                                        <?php } ?></td>
                                    <td class="text-left"><?php if ($sort == 'p.status') { ?>
                                            <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                        <?php } ?></td>
                                    <td class="text-right"><?php if ($sort == 'p.date_added') { ?>
                                            <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                                        <?php } ?></td>
                                    <td class="text-right"><?php if ($sort == 'p.date_modified') { ?>
                                            <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } else { ?>
                                            <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                                        <?php } ?></td>

                                    <td class="text-center"><?php echo $column_user_id; ?></td>
                                    <td class="text-center"><?php echo $column_last_mod_id; ?></td>

                                    <?php /* <td class="text-left"><?php echo $column_date_added; ?></td> */ ?>
                                    <td class="text-right"><?php echo $column_action; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($products) { ?>
                                    <?php foreach ($products as $product) { ?>
                                        <tr>
                                            <td class="text-center"><?php if (in_array($product['product_id'], $selected)) { ?>
                                                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                                                <?php } else { ?>
                                                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                                                <?php } ?></td>
                                            <td class="text-center"><?php if ($product['image']) { ?>
                                                <img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" class="img-thumbnail" width="75px" />
                                                <?php } else { ?>
                                                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                                                <?php } ?></td>
                                            <td class="text-right"><?php echo $product['product_id']; ?></td>
                                            <td class="text-left"><?php echo $product['name']; ?></td>
                                            <td class="text-left"><?php echo $product['model']; ?></td>
                                            <td class="text-left"><?php echo $product['sku']; ?></td>
                                            <td class="text-left"><?php echo $product['brand']; ?></td>
                                            <?php /* <td class="text-right"><?php if ($product['special']) { ?>
                                              <span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
                                              <div class="text-danger"><?php echo $product['special']; ?></div>
                                              <?php } else { ?>
                                              <?php echo $product['price']; ?>
                                              <?php } ?></td> */ ?>
                                            <td class="text-right"><?php if ($product['quantity'] <= 0) { ?>
                                                    <span class="label label-warning"><?php echo $product['quantity']; ?></span>
                                                <?php } elseif ($product['quantity'] <= 5) { ?>
                                                    <span class="label label-danger"><?php echo $product['quantity']; ?></span>
                                                <?php } else { ?>
                                                    <span class="label label-success"><?php echo $product['quantity']; ?></span>
                                                <?php } ?></td>
                                            <td class="text-right"><?php echo $product['algorithm']; ?></td>
                                            <td class="text-right"><?php echo $product['sort_order']; ?></td>
                                            <td class="text-left"><?php echo $product['status']; ?></td>
                                            <td class="text-left"><?php echo $product['date_added']; ?></td>
                                            <td class="text-left"><?php echo $product['date_modified']; ?></td>
                                            <td class="text-center"><b><?php echo $product['user_add']; ?></b></td>
                                            <td class="text-center"><b><?php echo $product['user_modify']; ?></b></td>
                                            <td class="text-right"><a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#button-filter').on('click', function () {
        var url = 'index.php?route=catalog/product&token=<?php echo $token; ?>';

        var filter_name = $('input[name=\'filter_name\']').val();

        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }

        var filter_model = $('input[name=\'filter_model\']').val();

        if (filter_model) {
            url += '&filter_model=' + encodeURIComponent(filter_model);
        }

        var filter_sku = $('input[name=\'filter_sku\']').val();

        if (filter_sku) {
            url += '&filter_sku=' + encodeURIComponent(filter_sku);
        }

        var filter_product_id = $('input[name=\'filter_product_id\']').val();

        if (filter_product_id) {
            url += '&filter_product_id=' + encodeURIComponent(filter_product_id);
        }

        var filter_price = $('input[name=\'filter_price\']').val();

        if (filter_price) {
            url += '&filter_price=' + encodeURIComponent(filter_price);
        }

        var filter_quantity = $('input[name=\'filter_quantity\']').val();

        if (filter_quantity) {
            url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
        }

        var filter_status = $('select[name=\'filter_status\']').val();

        if (filter_status != '*') {
            url += '&filter_status=' + encodeURIComponent(filter_status);
        }
        
        var filter_category = $('select[name=\'filter_category\']').val();

        if (filter_category != '*') {
            url += '&filter_category_id=' + encodeURIComponent(filter_category);
        }
        
        var filter_seller = $('select[name=\'filter_seller\']').val();
        if (filter_seller != '*') {
            url += '&filter_seller_id=' + encodeURIComponent(filter_seller);
        }

        var filter_brand = $('select[name=\'filter_brand\']').val();

        if (filter_brand != '*') {
            url += '&filter_brand=' + encodeURIComponent(filter_brand);
        }
        
        var filter_date_added_start = $('input[name=\'filter_date_added_start\']').val();
        if (filter_date_added_start) {
            url += '&filter_date_added_start=' + encodeURIComponent(filter_date_added_start);
        }
        
        var filter_date_added_end = $('input[name=\'filter_date_added_end\']').val();
        if (filter_date_added_end) {
            url += '&filter_date_added_end=' + encodeURIComponent(filter_date_added_end);
        }

        location = url;
    });
    </script>
<script type="text/javascript">
    $('input[name=\'filter_name\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'filter_name\']').val(item['label']);
        }
    });

    $('input[name=\'filter_model\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['model'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'filter_model\']').val(item['label']);
        }
    });
    </script>
<script type="text/javascript">
    function otptoggle() {
        $('#otp-form').toggle();
        $('#otp-form').toggleClass('active');
        if ($('#otp-form').hasClass('active')) {
            $(this).html('Hide OTP Form');
        }
        else {
            $(this).html('Show OTP Form');
        }
    }
    ;
    $('#upload-otp').on('click', function () {
        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
        $('#form-upload input[name=\'file\']').trigger('click');
        $('#form-upload input[name=\'file\']').on('change', function () {
            $.ajax({
                url: 'index.php?route=catalog/product/uploadotp&token=<?php echo $token; ?>',
                type: 'post',
                dataType: 'json',
                data: new FormData($(this).parent()[0]),
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#upload-otp').button('loading');
                },
                complete: function () {
                    $('#upload-otp').button('reset');
                },
                success: function (json) {
                    if (json['error']) {
                        alert(json['error']);
                    }
                    if (json['success']) {
                        alert(json['success']);
                        $('input[name=\'filename\']').attr('value', json['filename']);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    });
    function updateotp() {
        if ($('#filename').val() != '') {
            $('#update-otp').after('<img src="view/image/loading.gif" class="loading" style="padding-left: 5px;" />');
            filename = $('#filename').val();
            $.ajax({
                type: "POST",
                url: 'index.php?route=catalog/product/updateotp&token=<?php echo $token; ?>',
                data: 'filename=' + filename,
                success: function (data) {
                    $('#filename').val("");
                    $('.loading').remove();
                    alert(data);
                }
            });
        }
        else {
            alert('No file was uploaded!');
        }
    }
    ;
    function exportotp() {
        document.location.href = 'index.php?route=catalog/product/exportotp&category=' + $('#otp-category').val() + '&token=<?php echo $token; ?>';
    }
    ;
    $('#upload-otp-img').on('click', function () {
        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
        $('#form-upload input[name=\'file\']').trigger('click');
        $('#form-upload input[name=\'file\']').on('change', function () {
            $.ajax({
                url: 'index.php?route=catalog/product/uploadotp&token=<?php echo $token; ?>',
                type: 'post',
                dataType: 'json',
                data: new FormData($(this).parent()[0]),
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#upload-otp-img').button('loading');
                },
                complete: function () {
                    $('#upload-otp-img').button('reset');
                },
                success: function (json) {
                    if (json['error']) {
                        alert(json['error']);
                    }
                    if (json['success']) {
                        alert(json['success']);
                        $('input[name=\'filename-img\']').attr('value', json['filename']);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    });
    function updateotpimg() {
        if ($('#filename-img').val() != '') {
            $('#update-otp-img').after('<img src="view/image/loading.gif" class="loading" style="padding-left: 5px;" />');
            filename = $('#filename-img').val();
            $.ajax({
                type: "POST",
                url: 'index.php?route=catalog/product/updateotpimg&token=<?php echo $token; ?>',
                data: 'filename=' + filename,
                success: function (data) {
                    $('#filename-img').val("");
                    $('.loading').remove();
                    alert(data);
                }
            });
        }
        else {
            alert('No file was uploaded!');
        }
    }
    ;
    function exportotpimg() {
        document.location.href = 'index.php?route=catalog/product/exportotpimg&category=' + $('#otp-category-img').val() + '&token=<?php echo $token; ?>';
    }
    ;
    $('#upload-otp-model').on('click', function () {
        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
        $('#form-upload input[name=\'file\']').trigger('click');
        $('#form-upload input[name=\'file\']').on('change', function () {
            $.ajax({
                url: 'index.php?route=catalog/product/uploadotp&token=<?php echo $token; ?>',
                type: 'post',
                dataType: 'json',
                data: new FormData($(this).parent()[0]),
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#upload-otp-model').button('loading');
                },
                complete: function () {
                    $('#upload-otp-model').button('reset');
                },
                success: function (json) {
                    if (json['error']) {
                        alert(json['error']);
                    }
                    if (json['success']) {
                        alert(json['success']);
                        $('input[name=\'filename-model\']').attr('value', json['filename']);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    });
    function updateotpmodel() {
        if ($('#filename-model').val() != '') {
            $('#update-otp-model').after('<img src="view/image/loading.gif" class="loading" style="padding-left: 5px;" />');
            filename = $('#filename-model').val();
            $.ajax({
                type: "POST",
                url: 'index.php?route=catalog/product/updateotpmodel&token=<?php echo $token; ?>',
                data: 'filename=' + filename,
                success: function (data) {
                    $('#filename-model').val("");
                    $('.loading').remove();
                    alert(data);
                }
            });
        }
        else {
            alert('No file was uploaded!');
        }
    }
    function exportotpmodel() {
        document.location.href = 'index.php?route=catalog/product/exportotpmodel&category=' + $('#otp-category-model').val() + '&token=<?php echo $token; ?>';
    }
</script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
<script type="text/javascript">
    $('.date').datetimepicker({
        pickTime: false
    });
    </script>
<?php echo $footer; ?>
