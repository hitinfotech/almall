<div class="container-fluid createBundleForm">
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
            <?php if ($bundle['id'] == '0') { ?>
                <h3 class="step"><?php echo $text_create_bundle; ?></h3>
            <?php } else { ?>
                <h3 class="step"><?php echo $text_edit_bundle; ?></h3>
            <?php } ?>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
            <div class="pull-right">
                <a onClick="saveBundle(this);" class="btn btn-primary"><?php echo $text_btn_save_bundle; ?></a>&nbsp;&nbsp;<a onClick="cancelBundle(this);" class="btn btn-danger"><?php echo $text_btn_cancel; ?></a>
            </div>
        </div>
    </div>        
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
            <h5><strong><?php echo $text_bundle_status; ?>:</strong></h5>
        </div>    
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select name="bundle[status]" class="form-control">
                <option value="1" <?php echo ($bundle['status']=='1') ? 'seleccted="selected"' : ''; ?>><?php echo $text_enabled; ?></option>
                <option value="0" <?php echo ($bundle['status']=='0') ? 'seleccted="selected"' : ''; ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <br><hr><br />
    <div class="row">
        <div class="col-xs-12">
            <h3 class="step">1. Set Bundle Name and Description</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_bundle_name; ?>:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundle_name_helper; ?></span>
            <input type="hidden" name="bundle[id]" value="<?php echo $bundle['id']; ?>" />    
        </div>    
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <?php foreach ($languages as $language) { ?>
                <div class="input-group">
                  <span class="input-group-addon"><img src="<?php echo $language['flag_url']; ?>"/></span>
                  <input type="text" class="form-control" name="bundle[name][<?php echo $language['language_id']; ?>]" value="<?php echo isset($bundle['name'][$language['language_id']]) ? $bundle['name'][$language['language_id']] : ''; ?>" />
                </div>
                <br />
            <?php } ?>
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_bundle_seo_keyword; ?>:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundle_seo_keyword_helper; ?></span>
        </div>    
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <input type="text" class="form-control" name="bundle[slug]" value="<?php echo isset($bundle['slug']) ? $bundle['slug'] : ''; ?>" />
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
            <h5><strong><?php echo $text_bundle_description; ?>:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundle_description_helper; ?></span>
        </div>    
        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-6">
            <ul class="nav nav-tabs" id="langtabs-bundle" role="tablist">
              <?php foreach ($languages as $language) { ?>
                <li><a href="#lang-bundle-<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language['flag_url']; ?>" title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?></a></li>
              <?php } ?>
            </ul>
            <div class="tab-content">
              <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="lang-bundle-<?php echo $language['language_id']; ?>">
                    <textarea class="form-control bundle-description" name="bundle[description][<?php echo $language['language_id']; ?>]"><?php echo isset($bundle['description'][$language['language_id']]) ? $bundle['description'][$language['language_id']] : ''; ?></textarea>
                </div>
              <?php } ?>
            </div>
            <?php foreach ($languages as $language) { ?>
                
            <?php } ?>
        </div>
    </div>
    <hr><br />
    <div class="row">
        <div class="col-xs-12">
            <h3 class="step">2. Add Products to the bundle and where the bundle will be displayed</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2">
            <h5><strong><?php echo $text_bundled_products; ?>:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundled_products_helper; ?></span>
        </div>    
        <div class="col-xs-12 col-sm-7 col-md-9 col-lg-10">
            <div class="row">
                <div class="col-xs-12 col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"><?php echo $text_product_add_label; ?></span>
                        <input type="text" name="productInput" class="form-control" value="" placeholder="Start typing the product name" />
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon"><?php echo $text_quantity_label; ?></span>
                        <input type="text" name="quantityInput" class="form-control" value="1" />
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <a href="javascript:void(0)" onClick="addProductToBundle(this);" class="btn btn-primary addProductToBundle">Add Product</a>
                </div>
            </div>
            <br />
            <div class="row bundle-products-listbox">
                <?php foreach ($bundle['products'] as $bundle_product) { ?>
                <div class="col-sm-6 col-md-3 col-lg-2">
                    <input type="hidden" name="bundle[products][]" value="<?php echo $bundle_product['product_id']; ?>" />    
                    <a onClick="removeProductFromBundle(this, '<?php echo $bundle_product['price']; ?>');" class="btn btn-xs btn-default removeProductFromBundle" role="button"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                    <div class="thumbnail">
                        <img src="<?php echo $bundle_product['image']; ?>" alt="<?php echo $bundle_product['name']; ?>">
                        <div class="caption text-center">
                            <h3><?php echo ($bundle_product['options']) ? '<i class="fa fa-tags" style="color:#ab9a87;font-size:13px;"></i>' : ''; ?>&nbsp;<?php echo $bundle_product['name']; ?></h3>
                            <p><?php echo $text_price; ?>: <?php echo $bundle_product['price']; ?> <?php echo $config_currency; ?></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <br /><br />
    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2">
            <h5><strong><?php echo $text_bundle_show_on; ?>:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundle_show_on_helper; ?></span>
        </div>    
        <div class="col-xs-12 col-sm-7 col-md-9 col-lg-10">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group input-group">
                       <span class="input-group-addon" style="border: 0; background-color: transparent;"><?php echo $text_products; ?></span>
                       <input type="text" name="productShowInput" class="form-control" value="" />
                    </div>
                    <div class="bundle-products-show-listbox well well-sm">
                        <?php foreach ($bundle['products_show'] as $bundle_product_show) { ?>
                        <span class="label label-lg label-primary label-product-<?php echo $bundle_product_show['product_id']; ?>"><?php echo $bundle_product_show['name']; ?> <i class="fa fa-times-circle removeIcon text-right"></i><input type="hidden" name="bundle[products_show][]" value="<?php echo $bundle_product_show['product_id']; ?>" /></span>
                        <?php } ?>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6">
                    <div class="form-group input-group">
                       <span class="input-group-addon" style="border: 0; background-color: transparent;"><?php echo $text_categories; ?></span>
                       <input type="text" name="categoryShowInput" class="form-control" value="" />
                    </div>
                    <div class="bundle-categories-show-listbox well well-sm">
                        <?php foreach ($bundle['categories_show'] as $bundle_category_show) { ?>
                        <span class="label label-lg label-primary label-category-<?php echo $bundle_category_show['category_id']; ?>"><?php echo $bundle_category_show['name']; ?> <i class="fa fa-times-circle removeIcon text-right"></i><input type="hidden" name="bundle[categories_show][]" value="<?php echo $bundle_category_show['category_id']; ?>" /></span>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr><br />
    <div class="row">
        <div class="col-xs-12">
            <h3 class="step">3. Set Bundle Discount</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2">
            <h5><strong><?php echo $text_bundle_discount_options; ?>:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundle_discount_options_helper; ?></span>
        </div>    
        <div class="col-xs-12 col-sm-7 col-md-9 col-lg-10">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="input-group">
                    <span class="input-group-addon"><?php echo $text_discount_type; ?></span>
                    <select name="bundle[discount_type]" class="form-control discountType">
                        <option value="1" <?php echo ($bundle['discount_type']=='1') ? 'selected="selected"' : ''; ?>><?php echo $text_fixed_price; ?></option>
                        <option value="2" <?php echo ($bundle['discount_type']=='2') ? 'selected="selected"' : ''; ?>><?php echo $text_percentage; ?></option>
                    </select>
                </div>
                <br /><br />
                <div class="input-group">
                    <span class="input-group-addon"><?php echo $text_discount_value; ?></span>
                    <input type="text" class="form-control bundlePriceInput" name="bundle[discount_value]" value="<?php echo isset($bundle['discount_value']) ? $bundle['discount_value'] : ''; ?>" />
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h4 style="line-height: 33px;"><?php echo $text_total_product_price; ?> <span class="label label-default"><span class='total-products-price'><?php echo $bundle['price']; ?></span> <?php echo $config_currency; ?></span></h4>
                <br />
                <br />
                <h4 style="line-height: 19px;"><?php echo $text_price_with_discount; ?> <span class="label label-default"><span class='total-bundle-price'><?php echo $bundle['bundled_price']; ?></span> <?php echo $config_currency; ?></span></h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <br />
                <div class="alert alert-warning" role="alert"><?php echo $text_prices_no_taxes; ?></div>
            </div>
        </div>
    </div>
    <hr><br />
    <div class="row">
        <div class="col-xs-12">
            <h3 class="step">4. Optional</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2">
            <h5><strong><?php echo $text_bundle_sort_order; ?>:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundle_sort_order_helper; ?></span>
        </div>    
        <div class="col-xs-12 col-sm-7 col-md-9 col-lg-10">
            <input type="text" class="form-control" name="bundle[sort_order]" value="<?php echo isset($bundle['sort_order']) ? $bundle['sort_order'] : ''; ?>" />
        </div>
    </div>
    <br /><br />
    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2">
            <h5><strong><?php echo $text_date_available; ?>:</strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_date_available_helper; ?></span>
        </div>    
        <div class="col-xs-12 col-sm-7 col-md-9 col-lg-10">
            <input type="text" class="form-control date" data-date-format="YYYY-MM-DD" name="bundle[date_available]" value="<?php echo isset($bundle['date_available']) ? $bundle['date_available'] : date('Y-m-d'); ?>" />
        </div>
    </div>
    <br /><br />

    <div class="row">
        <div class="col-xs-12 col-sm-5 col-md-7 col-lg-7">

        </div>
        <div class="col-xs-12 col-sm-7 col-md-5 col-lg-5">
            <div class="pull-right">
                <a onClick="saveBundle(this);" class="btn btn-primary"><?php echo $text_btn_save_bundle; ?></a>&nbsp;&nbsp;<a onClick="cancelBundle(this);" class="btn btn-danger"><?php echo $text_btn_cancel; ?></a>
            </div>
        </div>
    </div>
</div>

