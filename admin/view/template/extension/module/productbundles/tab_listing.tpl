<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_listing_url; ?></strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_listing_url_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <a href="<?php echo $store['url']."index.php?route=" . $modulePath . "/listing"; ?>" target="_blank"><?php echo $store['url']."index.php?route=" . $modulePath . "/listing"; ?></a>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_add_listing_to_main_menu; ?></strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_add_listing_to_main_menu_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select id="LinkChecker" name="<?php echo $moduleName; ?>[MainLinkEnabled]" class="form-control">
                <option value="yes" <?php echo (!empty($moduleData['MainLinkEnabled']) && $moduleData['MainLinkEnabled'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                <option value="no"  <?php echo (empty($moduleData['MainLinkEnabled']) || $moduleData['MainLinkEnabled']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div id="MainLinkOptions">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <h5><strong><?php echo $text_menu_link_title; ?></strong></h5>
        	    <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_menu_link_title_helper; ?></span>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <?php foreach ($languages as $language) { ?>
                    <div class="input-group">
                        <span class="input-group-addon"><?php echo $language['name']; ?>:</span>
                        <input type="text" class="form-control" name="<?php echo $moduleName; ?>[LinkTitle][<?php echo $language['language_id']; ?>]" value="<?php if(isset($moduleData['LinkTitle'][$language['language_id']])) { echo $moduleData['LinkTitle'][$language['language_id']]; } else { echo "Bundles"; }?>" />
                    </div>
                    <br />
                <?php } ?>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <h5><strong><?php echo $text_menu_link_sort_order; ?></strong></h5>
        	    <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_menu_link_sort_order_helper; ?></span>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <input type="number" class="form-control" name="<?php echo $moduleName; ?>[LinkSortOrder]" value="<?php if(isset($moduleData['LinkSortOrder'])) { echo $moduleData['LinkSortOrder']; } else { echo "7"; }?>" />
            </div>
        </div>
        <hr />
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_bundles_per_page; ?></strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundles_per_page_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <input type="number" name="<?php echo $moduleName; ?>[ListingLimit]" class="form-control" value="<?php echo (isset($moduleData['ListingLimit'])) ? $moduleData['ListingLimit'] : '10' ?>" /> 
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_seo_options; ?></strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_seo_options_helper1; ?><br /><br /><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_seo_options_helper2; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <ul class="nav nav-tabs" id="langtabs" role="tablist">
              <?php foreach ($languages as $language) { ?>
                <li><a href="#lang-<?php echo $language['language_id']; ?>" role="tab" data-toggle="tab"><img src="<?php echo $language['flag_url']; ?>" title="<?php echo $language['name']; ?>"/></a></li>
              <?php } ?>
            </ul>
            <div class="tab-content">
              <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="lang-<?php echo $language['language_id']; ?>">
                    <?php echo $text_page_title; ?><br />
                    <input name="<?php echo $moduleName; ?>[PageTitle][<?php echo $language['language_id']; ?>]" class="form-control" type="text" value="<?php echo (isset($moduleData['PageTitle'][$language['language_id']])) ? $moduleData['PageTitle'][$language['language_id']] : 'Product Bundles'; ?>" />
                    <br />
                    <?php echo $text_meta_description; ?><br />
                    <textarea name="<?php echo $moduleName; ?>[MetaDescription][<?php echo $language['language_id']; ?>]" class="form-control" rows="4"><?php echo (isset($moduleData['MetaDescription'][$language['language_id']])) ? $moduleData['MetaDescription'][$language['language_id']] : 'Bundles with great discount! Only in example.com.'; ?></textarea>
                    <br />
                    <?php echo $text_meta_keywords; ?><br />
                    <input name="<?php echo $moduleName; ?>[MetaKeywords][<?php echo $language['language_id']; ?>]" class="form-control" type="text" value="<?php echo (isset($moduleData['MetaKeywords'][$language['language_id']])) ? $moduleData['MetaKeywords'][$language['language_id']] : 'product bundles, discount, products, get discount'; ?>" />
                    <br/>
                    <?php echo $text_seo_slug; ?><br />
                    <input name="<?php echo $moduleName; ?>[SeoURL][<?php echo $language['language_id']; ?>]" class="form-control" type="text" value="<?php echo (isset($moduleData['SeoURL'][$language['language_id']])) ? $moduleData['SeoURL'][$language['language_id']] : 'bundles'; ?>" />
                    <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo str_replace("%site%", $store['url'], $text_seo_slug_helper); ?></span>
                </div>
              <?php } ?>
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_bundle_description; ?></strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundle_description_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select id="LinkChecker" name="<?php echo $moduleName; ?>[DescriptionListingEnabled]" class="form-control">
                <option value="yes" <?php echo (!empty($moduleData['DescriptionListingEnabled']) && $moduleData['DescriptionListingEnabled'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                <option value="no"  <?php echo (empty($moduleData['DescriptionListingEnabled']) || $moduleData['DescriptionListingEnabled']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_picture_width_height; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_in_pixels_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="input-group">
              <span class="input-group-addon"><?php echo $text_width; ?>&nbsp;</span>
                <input type="text" name="<?php echo $moduleName; ?>[ListingPictureWidth]" class="form-control" value="<?php echo (isset($moduleData['ListingPictureWidth'])) ? $moduleData['ListingPictureWidth'] : '120' ?>" />
              <span class="input-group-addon"><?php echo $text_px; ?></span>
            </div>
            <br />
            <div class="input-group">
              <span class="input-group-addon"><?php echo $text_height; ?></span>
                <input type="text" name="<?php echo $moduleName; ?>[ListingPictureHeight]" class="form-control" value="<?php echo (isset($moduleData['ListingPictureHeight'])) ? $moduleData['ListingPictureHeight'] : '120' ?>" />
              <span class="input-group-addon"><?php echo $text_px; ?></span>
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_custom_css; ?></strong></h5>
       		<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_custom_css_helper; ?></span> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <textarea rows="10" name="<?php echo $moduleName; ?>[ListingCustomCSS]" placeholder="CSS Style" class="form-control"><?php echo (isset($moduleData['ListingCustomCSS'])) ? $moduleData['ListingCustomCSS'] : '' ?></textarea>
        </div>
    </div>
    <hr />
</div>