<?php echo $header; ?><?php echo $column_left; ?>
<?php
if (!function_exists('modification_vqmod')) {
    function modification_vqmod($file) {
        if (class_exists('VQMod')) {
            return VQMod::modCheck(modification($file), $file);
        } else {
            return modification($file);
        }
    }
}
?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
            <div class="pull-right">
                <div class="storeSwitcherWidget">
                    <div class="form-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">&nbsp;&nbsp;<?php echo $store['name']; if($store['store_id'] == 0) echo " <strong>".$text_default."</strong>"; ?>&nbsp;<span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                        <ul class="dropdown-menu" role="menu">
                            <?php foreach ($stores as $st) { ?>
                                <li><a href="index.php?route=<?php echo $modulePath; ?>&store_id=<?php echo $st['store_id'];?>&<?php echo $token_addon; ?>"><?php echo $st['name']; ?></a></li>
                            <?php } ?> 
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container-fluid">
        <?php echo (empty($moduleData['LicensedOn'])) ? base64_decode('ICAgIDxkaXYgY2xhc3M9ImFsZXJ0IGFsZXJ0LWRhbmdlciBmYWRlIGluIj4NCiAgICAgICAgPGJ1dHRvbiB0eXBlPSJidXR0b24iIGNsYXNzPSJjbG9zZSIgZGF0YS1kaXNtaXNzPSJhbGVydCIgYXJpYS1oaWRkZW49InRydWUiPsOXPC9idXR0b24+DQogICAgICAgIDxoND5XYXJuaW5nISBVbmxpY2Vuc2VkIHZlcnNpb24gb2YgdGhlIG1vZHVsZSE8L2g0Pg0KICAgICAgICA8cD5Zb3UgYXJlIHJ1bm5pbmcgYW4gdW5saWNlbnNlZCB2ZXJzaW9uIG9mIHRoaXMgbW9kdWxlISBZb3UgbmVlZCB0byBlbnRlciB5b3VyIGxpY2Vuc2UgY29kZSB0byBlbnN1cmUgcHJvcGVyIGZ1bmN0aW9uaW5nLCBhY2Nlc3MgdG8gc3VwcG9ydCBhbmQgdXBkYXRlcy48L3A+PGRpdiBzdHlsZT0iaGVpZ2h0OjVweDsiPjwvZGl2Pg0KICAgICAgICA8YSBjbGFzcz0iYnRuIGJ0bi1kYW5nZXIiIGhyZWY9ImphdmFzY3JpcHQ6dm9pZCgwKSIgb25jbGljaz0iJCgnYVtocmVmPSNpc2Vuc2Vfc3VwcG9ydF0nKS50cmlnZ2VyKCdjbGljaycpIj5FbnRlciB5b3VyIGxpY2Vuc2UgY29kZTwvYT4NCiAgICA8L2Rpdj4=') : '' ?>
        <?php if (!$total_status) { ?>
        <div class="alert alert-danger fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><?php echo $text_total_text; ?></h4>
            <p><?php echo $text_total_text_helper; ?></p><div style="height:5px;"></div>
            <a class="btn btn-danger" href="<?php echo $total_url; ?>"><?php echo $text_total_button; ?></a>
        </div>
        <?php } ?>
        <?php if (!empty($old_bundles)) { ?>
        <div class="alert alert-warning fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><?php echo $text_older_version; ?></h4>
            <p><?php echo $text_older_version_helper; ?></p><div style="height:5px;"></div>
            <a class="btn btn-warning" href="<?php echo $migrate_bundles_link; ?>"><?php echo $text_older_version_button; ?></a>
        </div>
        <?php } ?>
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger autoSlideUp"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success autoSlideUp"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <script>$('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                    <input type="hidden" name="store_id" value="<?php echo $store['store_id']; ?>" />
                    <input type="hidden" name="<?php echo $moduleName; ?>_status" value="1" />
                    <div class="tabbable">
                        <div class="tab-navigation form-inline">
                            <ul class="nav nav-tabs mainMenuTabs" id="mainTabs" role="tablist">
                                <li class="active"><a href="#controlpanel" data-toggle="tab"><?php echo $text_control_panel; ?></a></li>
                                <li id="bundlesTab"><a href="#bundles" data-toggle="tab"><?php echo $text_bundles; ?></a></li>
                                <li id="settingsTab" class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $text_settings; ?>&nbsp;<span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#widget" role="tab" data-toggle="tab"><?php echo $text_bundle_widget; ?></a></li>
                                        <li><a href="#view" role="tab" data-toggle="tab"><?php echo $text_bundle_view; ?></a></li>
                                        <li><a href="#listing" role="tab" data-toggle="tab"><?php echo $text_bundle_listing; ?></a></li>
                                    </ul>
                                </li>
                                <li><a href="#isense_support" data-toggle="tab"><?php echo $text_support; ?></a></li>
                            </ul>
                            <div class="tab-buttons">
                                <button type="submit" class="btn btn-success save-changes"><?php echo $text_save_changes; ?></button>
                                <a onclick="location = '<?php echo $cancel; ?>'" class="btn btn-warning"><?php echo $button_cancel; ?></a>
                            </div> 
                        </div><!-- /.tab-navigation --> 
                        <div class="tab-content">
                            <div id="controlpanel" class="tab-pane active">
                              <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_controlpanel.tpl'); ?>
                            </div> 
                            <div id="bundles" class="tab-pane">
                              <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_bundles.tpl'); ?>
                            </div>    
                            <div id="listing" class="tab-pane">
                              <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_listing.tpl'); ?>
                            </div>
                            <div id="widget" class="tab-pane">
                              <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_widget.tpl'); ?>
                            </div>     
                            <div id="view" class="tab-pane">
                              <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_view.tpl'); ?>
                            </div>         
                            <div id="isense_support" class="tab-pane">
                              <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/'.$modulePath.'/tab_support.tpl'); ?>
                            </div>
                        </div> <!-- /.tab-content --> 
                    </div><!-- /.tabbable -->
                </form>
            </div> 
        </div>
    </div>
</div>
<?php 
$hostname = (!empty($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : '' ;
$hostname = (strstr($hostname,'http://') === false) ? 'http://'.$hostname: $hostname;
?>
<script type="text/javascript">
    var MID                     = 'VLS0W1DLZ0';
    var domain                  = '<?php echo base64_encode($hostname); ?>';
    var domainraw               = '<?php echo $hostname; ?>';
    var timenow                 = '<?php echo time(); ?>';
    var loadingPhase            = '<br /><div class="loader"></div>';
    var modulePath              = '<?php echo $modulePath; ?>';
    var tokenAddon              = '<?php echo $token_addon; ?>';
    var storeId                 = '<?php echo $store["store_id"]; ?>';
    var storeAddon              = 'store_id=<?php echo $store["store_id"]; ?>';
    var currency                = '<?php echo $config_currency; ?>';
    var text_price              = '<?php echo $text_price; ?>';
    var error_missing_data      = '<?php echo $error_missing_data; ?>';
    var text_error_missing_data = '<?php echo $text_error_missing_data; ?>'; 
    var text_delete             = '<?php echo $text_delete; ?>';
    var text_delete_a_confirmation = '<?php echo $text_delete_a_confirmation; ?>';
    var text_cancel             = '<?php echo $text_cancel; ?>';
    var text_confirm            = '<?php echo $text_confirm; ?>';
</script>
<?php echo $footer;