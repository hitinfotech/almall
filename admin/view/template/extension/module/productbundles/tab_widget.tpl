<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_widget_title; ?></strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_widget_title_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <?php foreach ($languages as $language) { ?>
                <div class="input-group">
                  <span class="input-group-addon"><img src="<?php echo $language['flag_url']; ?>" style="margin-top:-3px;" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></span>
                  <input name="<?php echo $moduleName; ?>[WidgetTitle][<?php echo $language['language_id']; ?>]" class="form-control" type="text" value="<?php echo (isset($moduleData['WidgetTitle'][$language['language_id']])) ? $moduleData['WidgetTitle'][$language['language_id']] : $text_check_this_bundle ?>" />
                </div>
                <br />
            <?php } ?>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_wrap_in_widget; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_wrap_in_widget_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select name="<?php echo $moduleName; ?>[WrapInWidget]" class="form-control">
                <option value="yes" <?php echo (isset($moduleData['WrapInWidget']) && ($moduleData['WrapInWidget'] == 'yes')) ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                <option value="no" <?php echo (isset($moduleData['WrapInWidget']) && ($moduleData['WrapInWidget'] == 'no')) ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_bundles_in_widget; ?></strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundles_in_widget_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <input type="number" name="<?php echo $moduleName; ?>[WidgetLimit]" class="form-control" value="<?php echo (isset($moduleData['WidgetLimit'])) ? $moduleData['WidgetLimit'] : '2' ?>" /> 
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_display_type; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_display_type_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select name="<?php echo $moduleName; ?>[DisplayType]" class="form-control">
                <option value="default" <?php echo (isset($moduleData['DisplayType']) && ($moduleData['DisplayType'] == 'default')) ? 'selected=selected' : '' ?>><?php echo $text_default; ?></option>
                <option value="random" <?php echo (isset($moduleData['DisplayType']) && ($moduleData['DisplayType'] == 'random')) ? 'selected=selected' : '' ?>><?php echo $text_random; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_show_random_bundles; ?></strong>:</h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_show_random_bundles_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select name="<?php echo $moduleName; ?>[ShowRandomBundles]" class="form-control">
                <option value="yes" <?php echo (isset($moduleData['ShowRandomBundles']) && $moduleData['ShowRandomBundles'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
               <option value="no" <?php echo (isset($moduleData['ShowRandomBundles']) && $moduleData['ShowRandomBundles'] == 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_bundle_description; ?></strong></h5>
        	<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_bundle_description_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select id="LinkChecker" name="<?php echo $moduleName; ?>[DescriptionWidgetEnabled]" class="form-control">
                <option value="yes" <?php echo (!empty($moduleData['DescriptionWidgetEnabled']) && $moduleData['DescriptionWidgetEnabled'] == 'yes') ? 'selected=selected' : '' ?>><?php echo $text_enabled; ?></option>
                <option value="no"  <?php echo (empty($moduleData['DescriptionWidgetEnabled']) || $moduleData['DescriptionWidgetEnabled']== 'no') ? 'selected=selected' : '' ?>><?php echo $text_disabled; ?></option>
            </select>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_picture_width_height; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_in_pixels_helper; ?></span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="input-group">
              <span class="input-group-addon"><?php echo $text_width; ?>&nbsp;</span>
                <input type="text" name="<?php echo $moduleName; ?>[WidgetWidth]" class="form-control" value="<?php echo (isset($moduleData['WidgetWidth'])) ? $moduleData['WidgetWidth'] : '80' ?>" />
              <span class="input-group-addon"><?php echo $text_px; ?></span>
            </div>
            <br />
            <div class="input-group">
              <span class="input-group-addon"><?php echo $text_height; ?></span>
                <input type="text" name="<?php echo $moduleName; ?>[WidgetHeight]" class="form-control" value="<?php echo (isset($moduleData['WidgetHeight'])) ? $moduleData['WidgetHeight'] : '80' ?>" />
              <span class="input-group-addon"><?php echo $text_px; ?></span>
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <h5><strong><?php echo $text_custom_css; ?></strong></h5>
       		<span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $text_custom_css_helper; ?></span> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <textarea rows="10" name="<?php echo $moduleName; ?>[CustomCSS]" placeholder="CSS Style" class="form-control"><?php echo (isset($moduleData['CustomCSS'])) ? $moduleData['CustomCSS'] : '' ?></textarea>
        </div>
    </div>
    <hr />
</div>