<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-9 col-lg-10">
            <h3><?php echo $text_bundle_list; ?></h3>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2">
            <div class="pull-right">
                <a onClick="addNewBundle();" class="btn btn-primary"><?php echo $text_create_bundle; ?></a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <br />
    <table class="table table-striped table-hover table-bundle-list"> 
        <thead>
            <tr> 
                <th>#</th> 
                <th><?php echo $text_name; ?></th>
                <th><?php echo $text_products; ?></th> 
                <th><?php echo $text_discount_value; ?></th> 
                <th><?php echo $text_status; ?></th> 
                <th><?php echo $text_date_added; ?></th>
                <th><?php echo $text_date_modified; ?></th>
                <th class="text-right"><?php echo $text_action; ?></th> 
            </tr> 
            <tr class='filter-bundles'> 
                <th></th> 
                <th><input type="text" class="form-control" name="filter_name" value="<?php echo $filter_name; ?>" /></th>
                <th><input type="hidden" name="filter_product_id" value="<?php echo $filter_product_id; ?>" /><input type="text" class="form-control" name="filter_product" value="<?php echo $filter_product; ?>" /></th> 
                <th><input type="text" class="form-control" name="filter_discount" value="<?php echo $filter_discount; ?>" /></th> 
                <th><select class="form-control" name="filter_status">
                    <option value="*" <?php echo ($filter_status == '*') ? 'selected="selected"' : ''; ?>><?php echo $text_all; ?></option>
                    <option value="1" <?php echo ($filter_status == '1') ? 'selected="selected"' : ''; ?>><?php echo $text_enabled; ?></option>
                    <option value="0" <?php echo ($filter_status == '0') ? 'selected="selected"' : ''; ?>><?php echo $text_disabled; ?></option>
                </select></th> 
                <th></th>
                <th></th>
                <th class="text-right"><input type="hidden" name="store_id" value="<?php echo $store_id; ?>" /><a onClick="filterData('.filter-bundles');" class="btn btn-primary"><?php echo $text_filter; ?></a></th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php if (count($sources)>0) { ?>   
                <?php foreach ($sources as $index => $result) { ?>
                <tr> 
                    <th scope="row"><?php echo ($index+1)*$page; ?></th> 
                    <td><a onClick="editBundle(<?php echo $result['id']; ?>);"><?php echo $result['name']; ?></a></td>
                    <td><?php echo $result['products']; ?></td>
                    <td>
                        <?php echo $result['discount']; ?> 
                        <?php if ($result['discount_type']=='1') { ?>
                            <?php echo $config_currency; ?>
                        <?php } else { ?>
                            %
                        <?php } ?>

                    </td>
                    <td>
                        <?php if ($result['enabled']) { ?>
                        <span class="label label-bundle label-success"><?php echo $result['status']; ?></span>  
                        <?php } else { ?>
                        <span class="label label-bundle label-danger"><?php echo $result['status']; ?></span>  
                        <?php } ?>
                    </td> 
                    <td><?php echo $result['date_added']; ?></td>
                    <td><?php echo $result['date_modified']; ?></td>
                    <td class="text-right">
                        <a onClick="editBundle(<?php echo $result['id']; ?>);" class="btn btn-primary btn-md"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;<a onClick="deleteBundle(<?php echo $result['id']; ?>);" class="btn btn-danger btn-md"><i class="fa fa-times" aria-hidden="true"></i></a>
                    </td> 
                </tr> 
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="8" class="text-center"><?php echo $text_no_bundles; ?></td>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="8">
                    <div class="row">
                        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>