<?php
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------
?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-files-o"></i> <?php echo $text_status_newsletters; ?></h3>
            </div>
            <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td class="text-left"><?php echo $head_ready_to_share_id; ?></td>
                                    <td class="text-left">DATE</td>
                                    <td class="text-left"><?php echo $head_is_done; ?> </td>
                                    <td class="text-left"><?php echo $head_chunks; ?> </td>
                                    <td class="text-left"><?php echo $head_total; ?> </td>

                                    <td class="text-left"><?php echo $head_fail; ?> </td>
                                    <td class="text-left"><?php echo $head_success; ?> </td>

                                    <td class="text-right"><?php echo $column_actions; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($list) { ?>
                                    <?php foreach ($list as $entry) {  ?>
                                        <tr>
                                            <td class="text-left"><?php echo $entry['id']; ?></td>
                                            <td class="text-left"><?php echo $entry['created_date']; ?></td>
                                            <td class="text-left"><?php echo $entry['status']; ?></td>
                                            <td class="text-left"><?php echo $entry['chunks']; ?></td>
                                            <td class="text-left"><?php echo $entry['total']; ?></td>
                                            <td class="text-left"><?php echo round($entry['fail'] / ($entry['total']>0 ? $entry['total'] : 1) * 100,2); ?>% - <?php echo $entry['fail']; ?></td>
                                            <td class="text-left"><?php echo round($entry['success'] / ($entry['total']>0 ? $entry['total'] : 1) * 100,2); ?>% - <?php echo $entry['success']; ?></td>
                                            <td class="text-right">
                                                <a href="<?php echo $delete ?>&id=<?php echo $entry['id']; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>

                                                <?php if($entry['status'] != 'PAUSED' && $entry['status'] != 'COMPLETED') : ?>
                                                <a href="<?php echo $pause ?>&id=<?php echo $entry['id']; ?>" data-toggle="tooltip" title="<?php echo $button_pause; ?>" class="btn btn-info"><i class="fa fa-pause"></i></a>
                                                <?php endif; ?>
                                                <?php if($entry['status'] == 'PAUSED' && $entry['status'] != 'COMPLETED') : ?>
                                                <a href="<?php echo $continue ?>&id=<?php echo $entry['id']; ?>" data-toggle="tooltip" title="<?php echo $button_continue; ?>" class="btn btn-primary"><i class="fa fa-play"></i></a>
                                                <?php endif; ?>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>

            </div>
        </div>
        <p class="text-center small">Newsletter Enhancements OpenCart Module v3.9.1</p>
    </div>
</div>
<?php echo $footer; ?>