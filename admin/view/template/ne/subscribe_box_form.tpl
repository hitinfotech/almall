<?php
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------
?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-check-square-o"></i> <?php echo $text_subscribe_box; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
                    <fieldset>
                        <legend><?php echo $text_settings; ?></legend>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                                <?php if ($error_name) { ?>
                                    <div class="text-danger"><?php echo $error_name; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_status; ?></label>
                            <div class="col-sm-10">
                                <label class="radio-inline">
                                    <?php if ($status) { ?>
                                        <input type="radio" name="status" value="1" checked="checked" />
                                        <?php echo $text_enabled; ?>
                                    <?php } else { ?>
                                        <input type="radio" name="status" value="1" />
                                        <?php echo $text_enabled; ?>
                                    <?php } ?>
                                </label>
                                <label class="radio-inline">
                                    <?php if (!$status) { ?>
                                        <input type="radio" name="status" value="0" checked="checked" />
                                        <?php echo $text_disabled; ?>
                                    <?php } else { ?>
                                        <input type="radio" name="status" value="0" />
                                        <?php echo $text_disabled; ?>
                                    <?php } ?>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_show_for; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <select name="show_for" class="form-control">
                                    <?php if (!$show_for) { ?>
                                        <option value="1"><?php echo $text_everyone; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_guests; ?></option>
                                    <?php } else { ?>
                                        <option value="1" selected="selected"><?php echo $text_everyone; ?></option>
                                        <option value="0"><?php echo $text_guests; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_type; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <select name="type" class="form-control">
                                    <?php if ($type == '1') { ?>
                                        <option value="1" selected="selected"><?php echo $text_content_box; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_content_box; ?></option>
                                    <?php } ?>
                                    <?php if ($type == '2') { ?>
                                        <option value="2" selected="selected"><?php echo $text_content_box_to_modal; ?></option>
                                    <?php } else { ?>
                                        <option value="2"><?php echo $text_content_box_to_modal; ?></option>
                                    <?php } ?>
                                    <?php if ($type == '3') { ?>
                                        <option value="3" selected="selected"><?php echo $text_modal_popup; ?></option>
                                    <?php } else { ?>
                                        <option value="3"><?php echo $text_modal_popup; ?></option>
                                    <?php } ?>
                                    <?php if ($type == '4') { ?>
                                        <option value="4" selected="selected"><?php echo $text_flyin_popup; ?></option>
                                    <?php } else { ?>
                                        <option value="4"><?php echo $text_flyin_popup; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="ne-popup">
                        <legend><?php echo $text_popup_settings; ?></legend>
                        <div class="form-group ne-fly-in">
                            <label class="col-sm-2 control-label"><?php echo $entry_position; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <select name="flyin_position" class="form-control">
                                    <?php if ($flyin_position == '1') { ?>
                                        <option value="1" selected="selected"><?php echo $text_top_left; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_top_left; ?></option>
                                    <?php } ?>
                                    <?php if ($flyin_position == '2') { ?>
                                        <option value="2" selected="selected"><?php echo $text_top_right; ?></option>
                                    <?php } else { ?>
                                        <option value="2"><?php echo $text_top_right; ?></option>
                                    <?php } ?>
                                    <?php if ($flyin_position == '3') { ?>
                                        <option value="3" selected="selected"><?php echo $text_bottom_left; ?></option>
                                    <?php } else { ?>
                                        <option value="3"><?php echo $text_bottom_left; ?></option>
                                    <?php } ?>
                                    <?php if ($flyin_position == '4') { ?>
                                        <option value="4" selected="selected"><?php echo $text_bottom_right; ?></option>
                                    <?php } else { ?>
                                        <option value="4"><?php echo $text_bottom_right; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-popup-delay"><span data-toggle="tooltip" title="<?php echo $help_popup_delay; ?>"><?php echo $entry_popup_delay; ?></span></label>
                            <div class="col-sm-10 col-md-2">
                                <input type="text" name="popup_delay" value="<?php echo $popup_delay; ?>" id="input-popup-delay" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-popup-timeout"><span data-toggle="tooltip" title="<?php echo $help_popup_timeout; ?>"><?php echo $entry_popup_timeout; ?></span></label>
                            <div class="col-sm-10 col-md-2">
                                <input type="text" name="popup_timeout" value="<?php echo $popup_timeout; ?>" id="input-popup-timeout" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-repeat-time"><span data-toggle="tooltip" title="<?php echo $help_popup_repeat_time; ?>"><?php echo $entry_popup_repeat_time; ?></span></label>
                            <div class="col-sm-10 col-md-2">
                                <input type="text" name="repeat_time" value="<?php echo $repeat_time; ?>" id="input-repeat-time" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-popup-width"><?php echo $entry_popup_width; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <input type="text" name="popup_width" value="<?php echo $popup_width; ?>" id="input-popup-width" class="form-control" />
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend><?php echo $text_box_settings; ?></legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_border; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <select name="border" class="form-control">
                                    <?php if ($border == '0') { ?>
                                        <option value="0" selected="selected"><?php echo $text_none; ?></option>
                                    <?php } else { ?>
                                        <option value="0"><?php echo $text_none; ?></option>
                                    <?php } ?>
                                    <?php if ($border == '1') { ?>
                                        <option value="1" selected="selected"><?php echo $text_all; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_all; ?></option>
                                    <?php } ?>
                                    <?php if ($border == '2') { ?>
                                        <option value="2" selected="selected"><?php echo $text_top; ?></option>
                                    <?php } else { ?>
                                        <option value="2"><?php echo $text_top; ?></option>
                                    <?php } ?>
                                    <?php if ($border == '3') { ?>
                                        <option value="3" selected="selected"><?php echo $text_bottom; ?></option>
                                    <?php } else { ?>
                                        <option value="3"><?php echo $text_bottom; ?></option>
                                    <?php } ?>
                                    <?php if ($border == '4') { ?>
                                        <option value="4" selected="selected"><?php echo $text_left; ?></option>
                                    <?php } else { ?>
                                        <option value="4"><?php echo $text_left; ?></option>
                                    <?php } ?>
                                    <?php if ($border == '5') { ?>
                                        <option value="5" selected="selected"><?php echo $text_right; ?></option>
                                    <?php } else { ?>
                                        <option value="5"><?php echo $text_right; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-border-radius"><?php echo $entry_border_radius; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <input type="text" name="border_radius" value="<?php echo $border_radius; ?>" id="input-border-radius" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-border-width"><?php echo $entry_border_width; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <input type="text" name="border_width" value="<?php echo $border_width; ?>" id="input-border-width" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-border-color"><?php echo $entry_border_color; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <div class="input-group color">
                                    <input type="text" name="border_color" value="<?php echo $border_color; ?>" id="input-border-color" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_shadow; ?></label>
                            <div class="col-sm-10">
                                <label class="radio-inline">
                                    <?php if ($shadow) { ?>
                                        <input type="radio" name="shadow" value="1" checked="checked" />
                                        <?php echo $text_yes; ?>
                                    <?php } else { ?>
                                        <input type="radio" name="shadow" value="1" />
                                        <?php echo $text_yes; ?>
                                    <?php } ?>
                                </label>
                                <label class="radio-inline">
                                    <?php if (!$shadow) { ?>
                                        <input type="radio" name="shadow" value="0" checked="checked" />
                                        <?php echo $text_no; ?>
                                    <?php } else { ?>
                                        <input type="radio" name="shadow" value="0" />
                                        <?php echo $text_no; ?>
                                    <?php } ?>
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend><?php echo $text_form_settings; ?></legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_fields; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <select name="fields" class="form-control">
                                    <?php if ($fields == '1') { ?>
                                        <option value="1" selected="selected"><?php echo $text_only_email; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_only_email; ?></option>
                                    <?php } ?>
                                    <?php if ($fields == '2') { ?>
                                        <option value="2" selected="selected"><?php echo $text_email_name; ?></option>
                                    <?php } else { ?>
                                        <option value="2"><?php echo $text_email_name; ?></option>
                                    <?php } ?>
                                    <?php if ($fields == '3') { ?>
                                        <option value="3" selected="selected"><?php echo $text_email_full; ?></option>
                                    <?php } else { ?>
                                        <option value="3"><?php echo $text_email_full; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_field_orientation; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <select name="field_orientation" class="form-control">
                                    <?php if ($field_orientation == '0') { ?>
                                        <option value="0" selected="selected"><?php echo $text_stacked; ?></option>
                                    <?php } else { ?>
                                        <option value="0"><?php echo $text_stacked; ?></option>
                                    <?php } ?>
                                    <?php if ($field_orientation == '1') { ?>
                                        <option value="1" selected="selected"><?php echo $text_stacked_compact; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_stacked_compact; ?></option>
                                    <?php } ?>
                                    <?php if ($field_orientation == '2') { ?>
                                        <option value="2" selected="selected"><?php echo $text_inline; ?></option>
                                    <?php } else { ?>
                                        <option value="2"><?php echo $text_inline; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-field-border-radius"><?php echo $entry_field_border_radius; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <input type="text" name="field_border_radius" value="<?php echo $field_border_radius; ?>" id="input-field-border-radius" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-field-border-color"><?php echo $entry_field_border_color; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <div class="input-group color">
                                    <input type="text" name="field_border_color" value="<?php echo $field_border_color; ?>" id="input-field-border-color" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-field-active-border-color"><?php echo $entry_field_active_border_color; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <div class="input-group color">
                                    <input type="text" name="field_active_border_color" value="<?php echo $field_active_border_color; ?>" id="input-field-active-border-color" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-button-border-radius"><?php echo $entry_button_border_radius; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <input type="text" name="button_border_radius" value="<?php echo $button_border_radius; ?>" id="input-button-border-radius" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-button-background-color"><?php echo $entry_button_background_color; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <div class="input-group color">
                                    <input type="text" name="button_background_color" value="<?php echo $button_background_color; ?>" id="input-button-background-color" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-button-text-color"><?php echo $entry_button_text_color; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <div class="input-group color">
                                    <input type="text" name="button_text_color" value="<?php echo $button_text_color; ?>" id="input-button-text-color" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-button-text"><?php echo $entry_button_text; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <?php if (version_compare(VERSION, '2.2.0.0', '>=')) { ?>
                                            <img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" />
                                            <?php } else { ?>
                                            <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" />
                                            <?php } ?>
                                        </span>
                                        <input type="text" name="button_text[<?php echo $language['language_id']; ?>]" value="<?php echo empty($button_text[$language['language_id']]) ? $text_subscribe : $button_text[$language['language_id']]; ?>" class="form-control" />
                                    </div>
                                    <br/>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-form-background-color"><?php echo $entry_background_color; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <div class="input-group color">
                                    <input type="text" name="form_background_color" value="<?php echo $form_background_color; ?>" id="input-form-background-color" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-success-text-color"><?php echo $entry_success_text_color; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <div class="input-group color">
                                    <input type="text" name="form_success_text_color" value="<?php echo $form_success_text_color; ?>" id="input-success-text-color" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-error-text-color"><?php echo $entry_error_text_color; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <div class="input-group color">
                                    <input type="text" name="form_error_text_color" value="<?php echo $form_error_text_color; ?>" id="input-error-text-color" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_list_type; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <select name="list_type" class="form-control">
                                    <?php if ($list_type == '1') { ?>
                                        <option value="0"><?php echo $text_checkboxes; ?></option>
                                        <option value="1" selected="selected"><?php echo $text_radio_buttons; ?></option>
                                    <?php } else { ?>
                                        <option value="0" selected="selected"><?php echo $text_checkboxes; ?></option>
                                        <option value="1"><?php echo $text_radio_buttons; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_accept; ?></label>
                            <div class="col-sm-10 col-md-10">
                                <div class="checkbox">
                                  <label><input type="checkbox" name="accept" value="1"<?php if ($accept) { ?> checked="checked"<?php } ?> /> <?php echo $text_accept; ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group accept-checkbox">
                            <label class="col-sm-2 control-label" for="input-accept-text"><span data-toggle="tooltip" title="<?php echo $text_accept_text; ?>"><?php echo $entry_accept_text; ?></span></label>
                            <div class="col-sm-10 col-md-10">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <?php if (version_compare(VERSION, '2.2.0.0', '>=')) { ?>
                                            <img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" />
                                            <?php } else { ?>
                                            <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" />
                                            <?php } ?>
                                        </span>
                                        <textarea name="accept_text[<?php echo $language['language_id']; ?>]" class="form-control"><?php echo empty($accept_text[$language['language_id']]) ? '' : $accept_text[$language['language_id']]; ?></textarea>
                                    </div>
                                    <br/>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group accept-checkbox">
                            <label class="col-sm-2 control-label" for="input-accept-error"><?php echo $entry_accept_error; ?></label>
                            <div class="col-sm-10 col-md-10">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <?php if (version_compare(VERSION, '2.2.0.0', '>=')) { ?>
                                            <img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" />
                                            <?php } else { ?>
                                            <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" />
                                            <?php } ?>
                                        </span>
                                        <input type="text" name="accept_error[<?php echo $language['language_id']; ?>]" value="<?php echo empty($accept_error[$language['language_id']]) ? '' : $accept_error[$language['language_id']]; ?>" class="form-control" />
                                    </div>
                                    <br/>
                                <?php } ?>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend><?php echo $text_content_settings; ?></legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-heading-text-color"><?php echo $entry_heading_text_color; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <div class="input-group color">
                                    <input type="text" name="heading_text_color" value="<?php echo $heading_text_color; ?>" id="input-heading-text-color" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_heading_text; ?></label>
                            <div class="col-sm-10 col-md-6">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <?php if (version_compare(VERSION, '2.2.0.0', '>=')) { ?>
                                            <img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" />
                                            <?php } else { ?>
                                            <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" />
                                            <?php } ?>
                                        </span>
                                        <input type="text" name="heading[<?php echo $language['language_id']; ?>]" value="<?php echo isset($heading[$language['language_id']]) ? $heading[$language['language_id']] : $text_content_heading; ?>" class="form-control" />
                                    </div>
                                    <br/>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-content-text-color"><?php echo $entry_text_color; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <div class="input-group color">
                                    <input type="text" name="content_text_color" value="<?php echo $content_text_color; ?>" id="input-content-text-color" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_text; ?></label>
                            <div class="col-sm-10 col-md-10">
                                <?php foreach ($languages as $language) { ?>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <?php if (version_compare(VERSION, '2.2.0.0', '>=')) { ?>
                                            <img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" />
                                            <?php } else { ?>
                                            <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" alt="<?php echo $language['name']; ?>" />
                                            <?php } ?>
                                        </span>
                                        <textarea name="text[<?php echo $language['language_id']; ?>]" class="form-control"><?php echo isset($text[$language['language_id']]) ? $text[$language['language_id']] : $text_content_text; ?></textarea>
                                    </div>
                                    <br/>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_content_divider; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <select name="content_divider" class="form-control">
                                    <?php if ($content_divider == '0') { ?>
                                        <option value="0" selected="selected"><?php echo $text_none; ?></option>
                                    <?php } else { ?>
                                        <option value="0"><?php echo $text_none; ?></option>
                                    <?php } ?>
                                    <?php if ($content_divider == '1') { ?>
                                        <option value="1" selected="selected"><?php echo $text_triangle; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_triangle; ?></option>
                                    <?php } ?>
                                    <?php if ($content_divider == '2') { ?>
                                        <option value="2" selected="selected"><?php echo $text_wide_triangle; ?></option>
                                    <?php } else { ?>
                                        <option value="2"><?php echo $text_wide_triangle; ?></option>
                                    <?php } ?>
                                    <?php if ($content_divider == '3') { ?>
                                        <option value="3" selected="selected"><?php echo $text_zigzag; ?></option>
                                    <?php } else { ?>
                                        <option value="3"><?php echo $text_zigzag; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-content-background-color"><?php echo $entry_background_color; ?></label>
                            <div class="col-sm-10 col-md-4">
                                <div class="input-group color">
                                    <input type="text" name="content_background_color" value="<?php echo $content_background_color; ?>" id="content-background-color" class="form-control" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend><?php echo $text_content_image_settings; ?></legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_position; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <select name="image_position" class="form-control">
                                    <?php if ($image_position == '1') { ?>
                                        <option value="1" selected="selected"><?php echo $text_above_text; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_above_text; ?></option>
                                    <?php } ?>
                                    <?php if ($image_position == '2') { ?>
                                        <option value="2" selected="selected"><?php echo $text_below_text; ?></option>
                                    <?php } else { ?>
                                        <option value="2"><?php echo $text_below_text; ?></option>
                                    <?php } ?>
                                    <?php if ($image_position == '3') { ?>
                                        <option value="3" selected="selected"><?php echo $text_left_from_text; ?></option>
                                    <?php } else { ?>
                                        <option value="3"><?php echo $text_left_from_text; ?></option>
                                    <?php } ?>
                                    <?php if ($image_position == '4') { ?>
                                        <option value="4" selected="selected"><?php echo $text_right_from_text; ?></option>
                                    <?php } else { ?>
                                        <option value="4"><?php echo $text_right_from_text; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_image; ?></label>
                            <div class="col-sm-10">
                                <label class="radio-inline text-center" style="padding-left: 0;padding-right: 20px">
                                    <img src="<?php echo HTTPS_CATALOG; ?>image/ne/no_image.png" alt="" class="img-thumbnail" style="width:100px"><br>
                                    <?php if ($image == 0) { ?>
                                        <input type="radio" name="image" value="0" checked="checked" style="margin: 10px auto 20px" />
                                    <?php } else { ?>
                                        <input type="radio" name="image" value="0" style="margin: 10px auto 20px" />
                                    <?php } ?>
                                </label>
                                <label class="radio-inline text-center" style="padding-left: 0;padding-right: 20px">
                                    <img src="<?php echo HTTPS_CATALOG; ?>image/ne/1.png" alt="" class="img-thumbnail" style="width:100px"><br>
                                    <?php if ($image == 1) { ?>
                                        <input type="radio" name="image" value="1" checked="checked" style="margin: 10px auto 20px" />
                                    <?php } else { ?>
                                        <input type="radio" name="image" value="1" style="margin: 10px auto 20px" />
                                    <?php } ?>
                                </label>
                                <label class="radio-inline text-center" style="padding-left: 0;padding-right: 20px">
                                    <img src="<?php echo HTTPS_CATALOG; ?>image/ne/2.png" alt="" class="img-thumbnail" style="width:100px"><br>
                                    <?php if ($image == 2) { ?>
                                        <input type="radio" name="image" value="2" checked="checked" style="margin: 10px auto 20px" />
                                    <?php } else { ?>
                                        <input type="radio" name="image" value="2" style="margin: 10px auto 20px" />
                                    <?php } ?>
                                </label>
                                <label class="radio-inline text-center" style="padding-left: 0;padding-right: 20px">
                                    <img src="<?php echo HTTPS_CATALOG; ?>image/ne/3.png" alt="" class="img-thumbnail" style="width:100px"><br>
                                    <?php if ($image == 3) { ?>
                                        <input type="radio" name="image" value="3" checked="checked" style="margin: 10px auto 20px" />
                                    <?php } else { ?>
                                        <input type="radio" name="image" value="3" style="margin: 10px auto 20px" />
                                    <?php } ?>
                                </label>
                                <label class="radio-inline text-center" style="padding-left: 0;padding-right: 20px">
                                    <img src="<?php echo HTTPS_CATALOG; ?>image/ne/4.png" alt="" class="img-thumbnail" style="width:100px"><br>
                                    <?php if ($image == 4) { ?>
                                        <input type="radio" name="image" value="4" checked="checked" style="margin: 10px auto 20px" />
                                    <?php } else { ?>
                                        <input type="radio" name="image" value="4" style="margin: 10px auto 20px" />
                                    <?php } ?>
                                </label>
                                <label class="radio-inline text-center" style="padding-left: 0;padding-right: 20px">
                                    <img src="<?php echo HTTPS_CATALOG; ?>image/ne/5.png" alt="" class="img-thumbnail" style="width:100px"><br>
                                    <?php if ($image == 5) { ?>
                                        <input type="radio" name="image" value="5" checked="checked" style="margin: 10px auto 20px" />
                                    <?php } else { ?>
                                        <input type="radio" name="image" value="5" style="margin: 10px auto 20px" />
                                    <?php } ?>
                                </label>
                                <label class="radio-inline text-center" style="padding-left: 0;padding-right: 20px">
                                    <img src="<?php echo HTTPS_CATALOG; ?>image/ne/6.png" alt="" class="img-thumbnail" style="width:100px"><br>
                                    <?php if ($image == 6) { ?>
                                        <input type="radio" name="image" value="6" checked="checked" style="margin: 10px auto 20px" />
                                    <?php } else { ?>
                                        <input type="radio" name="image" value="6" style="margin: 10px auto 20px" />
                                    <?php } ?>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_custom_image; ?></label>
                            <div class="col-sm-10">
                                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                <input type="hidden" name="image_custom" value="<?php echo $image_custom; ?>" id="input-image" />
                                <p class="help-block"><?php echo $text_custom_image_text; ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-image-width"><?php echo $entry_image_width; ?></label>
                            <div class="col-sm-10 col-md-2">
                                <input type="text" name="image_width" value="<?php echo $image_width; ?>" id="input-image-width" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_image_circled; ?></label>
                            <div class="col-sm-10">
                                <label class="radio-inline">
                                    <?php if ($image_circled) { ?>
                                        <input type="radio" name="image_circled" value="1" checked="checked" />
                                        <?php echo $text_yes; ?>
                                    <?php } else { ?>
                                        <input type="radio" name="image_circled" value="1" />
                                        <?php echo $text_yes; ?>
                                    <?php } ?>
                                </label>
                                <label class="radio-inline">
                                    <?php if (!$image_circled) { ?>
                                        <input type="radio" name="image_circled" value="0" checked="checked" />
                                        <?php echo $text_no; ?>
                                    <?php } else { ?>
                                        <input type="radio" name="image_circled" value="0" />
                                        <?php echo $text_no; ?>
                                    <?php } ?>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_hide_on_mobile; ?></label>
                            <div class="col-sm-10">
                                <label class="radio-inline">
                                    <?php if ($hide_image_on_mobile) { ?>
                                        <input type="radio" name="hide_image_on_mobile" value="1" checked="checked" />
                                        <?php echo $text_yes; ?>
                                    <?php } else { ?>
                                        <input type="radio" name="hide_image_on_mobile" value="1" />
                                        <?php echo $text_yes; ?>
                                    <?php } ?>
                                </label>
                                <label class="radio-inline">
                                    <?php if (!$hide_image_on_mobile) { ?>
                                        <input type="radio" name="hide_image_on_mobile" value="0" checked="checked" />
                                        <?php echo $text_no; ?>
                                    <?php } else { ?>
                                        <input type="radio" name="hide_image_on_mobile" value="0" />
                                        <?php echo $text_no; ?>
                                    <?php } ?>
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend><?php echo $text_custom_styles; ?></legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_css_code; ?></label>
                            <div class="col-sm-10 col-md-10">
                                <textarea name="css_code" class="form-control" rows="10"><?php echo $css_code; ?></textarea>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
        <p class="text-center small">Newsletter Enhancements OpenCart Module v3.9.1</p>
    </div>
    <script type="text/javascript"><!--
        $(function(){
            $('.color').colorpicker({
                'format': 'hex'
            });

            $('select[name=\'type\']').bind('change', function(){
                if ($(this).val() == '1') {
                    $('.ne-popup').hide();
                } else {
                    $('.ne-popup').show();
                    if ($(this).val() == '4') {
                        $('.ne-fly-in').show();
                    } else {
                        $('.ne-fly-in').hide();
                    }
                }
            }).trigger('change');

            $('input[name=\'accept\']').bind('change', function(){
                if ($(this).prop('checked')) {
                    $('.accept-checkbox').show();
                } else {
                    $('.accept-checkbox').hide();
                }
            }).trigger('change');

        });
    //--></script>
</div>
<?php echo $footer; ?>