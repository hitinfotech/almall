<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
      <fieldset>
        <legend><h2><?php echo $text_order.":" ?></h2></legend>
          <div class="row">
            <?php if(!empty($orders_count)) { ?>
                <?php foreach ($orders_count as $key => $value){ ?>
                <div class="col-lg-2 col-md-2 col-sm-6" style='margin: 10px'>
                  <div class="tile">
                    <div class="tile-heading"><a href='<?= $value["link"] ?>' ><?php echo $value['name']." Orders"; ?></a></div>
                    <div class="tile-body"><i class="fa fa-shopping-cart"></i>
                      <h2 class="pull-right"><?php echo $value['order_count']; ?></h2>
                    </div>
                    <div class="tile-footer"></div>
                  </div>
                </div>
              <?php } ?>
            <?php } ?>
          </div>
      </fieldset>
      <br><br>

      <fieldset>
        <legend><h2><?php echo $text_delayed_order.":" ?></h2></legend>
          <div class="row">
            <?php if(!empty($orders_count_by_period)) { ?>
                <?php foreach ($orders_count_by_period as $key => $value){ ?>
                <div class="col-lg-2 col-md-2 col-sm-6" style='margin: 10px'>
                  <div class="tile">
                    <div class="tile-heading"><?php echo $value['name']." Orders"; ?></div>
                    <div class="tile-body"><i class="fa fa-shopping-cart"></i>
                      <h2 class="pull-right"><?php echo $value['order_count']; ?></h2>
                    </div>
                    <div class="tile-footer"></div>
                  </div>
                </div>
              <?php } ?>
            <?php } ?>
          </div>
      </fieldset>
      <br><br>
      <fieldset>
        <legend><h2><?php echo $text_average_order.":" ?></h2></legend>
          <div class="row">
            <?php if(!empty($latest_average)) { ?>
                <?php foreach ($latest_average as $key => $value){ ?>
                <div class="col-lg-2 col-md-2 col-sm-6" style='margin: 10px'>
                  <div class="tile">
                    <div class="tile-heading"><?php echo $value['name']." Orders"; ?></div>
                    <div class="tile-body"><i class="fa fa-shopping-cart"></i>
                      <h3 class="pull-right"><?php echo $value['hours_avg']."h".$value['minutes_avg']."min"; ?></h3>
                    </div>
                    <div class="tile-footer"></div>
                  </div>
                </div>
              <?php } ?>
            <?php } ?>
          </div>
      </fieldset>


    </div>
</div>
<?php echo $footer; ?>
