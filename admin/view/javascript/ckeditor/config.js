/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
      config.removeButtons = 'Source,Save,NewPage,DocProps,Preview,Print,Templates,document' + 
                ',Scayt'+
                ',Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField'+
                ',Bold,Italic,Underline,Strike,Subscript,Superscript'+
                ',NumberedList,BulletedList,Outdent,Indent,Blockquote,CreateDiv'+
                ',Language'+
                ',Anchor'+
                ',CreatePlaceholder,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,InsertPre'+
                ',Styles,Format,Font,FontSize'+
                ',TextColor,BGColor'+
                ',UIColor,Maximize,ShowBlocks'+
                ',button1,button2,button3,oembed,MediaEmbed'+
                ',About'; 
};
