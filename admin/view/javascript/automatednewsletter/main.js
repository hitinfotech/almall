$(document).ready( function() {
  recipientSwitcherOptions()
  affiliateAutocomplete();
  loadTemplateList();
  templateListOption();
  productAutocomplete();
  productboughtAutocomplete();
  manufacturerAutocomplete(); 
  refreshStatistics();
  customerAutocomplete(); 
  tabPersistance();
  cronTabOptions();
  sendMailButtons();
  validatation(); 
  $('.infoTooltip').tooltip();
  $('#template').on('click', '.backToList', function() {
    if(saveTemplate(ckeditors)) {
      loadTemplateList();
    } 
  });
}); 

var progress = (function(){
    var taskName = 'isense';
    var securityToken = getURLVar('token');
    var moduleBase = getURLVar('route').match(/^(.*?\/.*?)(\/|$)/)[1];

    var clearProgress = function (callback) {
        $.ajax({
            url: 'index.php',
            type: 'GET',
            data: {
                route: moduleBase + '/clearProgress',
                task_name: taskName,
                token: securityToken
            },
            success: callback
        });
    }

    var getProgressMessage = function (callback) {
        $.ajax({
            url: 'index.php',
            type: 'GET',
            data: {
                route: moduleBase + '/getProgressMessage',
                task_name: taskName,
                token: securityToken
            },
            success: callback
        });
    }

    var getProgress = function (callback) {
        $.ajax({
            url: 'index.php',
            type: 'GET',
            data: {
                route: moduleBase + '/getProgress',
                task_name: taskName,
                token: securityToken
            },
            success: callback
        });
    }

    var abort = function() {
        $.ajax({
            url: 'index.php',
            type: 'GET',
            data: {
                route: moduleBase + '/abortProcess',
                task_name: taskName,
                token: securityToken
            }
        });
    }

    return {
        init: function (t, callback) {
            taskName = t;
            clearProgress(callback);
        },
        getMessage: function(callback) {
            getProgressMessage(callback);
        },
        getCurrent: function(callback) {
            getProgress(callback);
        },
        abortProcess: function() {
            abort();
        }
    }
})();

var progressTimeout = null;
function getProgress() {
    progress.getMessage(function progressGetMessage(response) {
        $('#progressMessage span').text(response);
    });
    progress.getCurrent(function progressGetCurrent(response) {
        $('#percentProgress').width(response+'%').text(response+'%');
    });
}

$(document).on('click', '#btnOkModalClose:not(.disabled)', function(){
    $('#progressModal').modal('hide');
});

function watchProgress() {
    getProgress();
    progressTimeout = setTimeout(watchProgress, 500);
}

$(document).on('click', '#btnProgressAbort:not(.disabled)', function(){
    progress.abortProcess();
});

function validatation() {
  $('.tab-buttons').on('click','.btn.btn-primary.save-changes' ,function(e) {
    if(!$('input[name="AutomatedNewsletter[from]"]').val()){
      e.preventDefault();
      $('#formError').show(200);
      $('input[name="AutomatedNewsletter[from]"]').css({'background':'#f2dede'});
    }
	
	var email_format = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^ ()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	if(!$('input[name="AutomatedNewsletter[from_email]"]').val() || !email_format.test($('input[name="AutomatedNewsletter[from_email]"]').val()) ){
      e.preventDefault();
      $('#formError').show(200);
      $('input[name="AutomatedNewsletter[from_email]"]').css({'background':'#f2dede'});
    }
	
	
    validateNumber($('input[name="AutomatedNewsletter[newProductsLastDays]"]'), e);
    validateNumber($('input[name="AutomatedNewsletter[bestsellerLastDays]"]'), e);
    validateNumber($('input[name="AutomatedNewsletter[specialsNextDays]"]'), e);
    validateNumber($('input[name="AutomatedNewsletter[bestsellerCount]"]'), e);
    validateNumber($('input[name="AutomatedNewsletter[countOfProductsPerRow]"]'), e);
    validateNumber($('input[name="AutomatedNewsletter[productImageWidth]"]'), e);
    validateNumber($('input[name="AutomatedNewsletter[productImageHeight]"]'), e);
  });
}

function storeSwitcherOptions() {
  $('select[name="selectStore"]').on('change', function(e) {
    $('.btn.btn-primary.save-changes').trigger('click');
    window.location="index.php?route=module/automatednewsletter&store_id=" + $('select[name="selectStore"]').val() + "&token=" + token;
  })
  
  $('.dropdown-menu .disabled'). on('click', function(e){
    e.preventDefault();
  })
}

function recipientSwitcherOptions(){
  $('select[name=\'AutomatedNewsletter[newsletter_receiver]\']').bind('change', function() {
    $('#mainSettings .to').hide();
	var variable = $(this).prop('value').replace('_', '-');
    $('#mainSettings #to-' + variable).show();
  });

  $('select[name=\'AutomatedNewsletter[newsletter_receiver]\']').trigger('change');

}

function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if( !emailReg.test( $email ) || !$email ) {
    return false;
  } else {
    return true;
  }
}     

function validateNumber(input) {  
  var regex = /[0-9]|\./; 
  if(!regex.test(input.val())) { 
    input.css({'background':'#f2dede'}); 
    return false; 
  } else { 
    input.css({'background':'#fff'}); 
    return true; 
  } 
} 

function select_text(){
  var content=eval(this);
  content.focus();
  content.select();
}

function refreshStatistics() {
  $.ajax({
    url: "index.php?route=module/automatednewsletter/sentNewsletters&token=" + getURLVar('token') + "&store_id=" + store_id,
    type:'post',
    dataType:'html',
    success: function(data) {
      $('#sentNewslettersContent').html(data);
    }
  });
}

function loadTemplateForm(template_id) { 
  $.ajax({ 
      url: 'index.php?route=module/automatednewsletter/templateForm&token=' + getURLVar('token'),
      type:'POST',
      dataType:'html',
      data:{'template_id': template_id, 'store_id': store_id},
      success: function(data) {
        $('#template').html(data);

      }
  });
}

function loadTemplateList() {
  $.ajax({ 
      url: 'index.php?route=module/automatednewsletter/templateList&token=' + token + "&store_id=" + store_id,
      type:'POST',
      dataType:'html',
      data:{},
      success: function(data) {
        $('#template').html(data);
      }
  });
}

function templateListOption() {
  $('body').on('click','.templateLoader', function(e) {
    e.preventDefault();
    $.ajax({
      url: $(this).parents('tr').attr('data-link'),
      type:'post',
      dataType:'html',
      success: function(data) {
        $('#template').html(data);
      }
    });
  });

  $('body').on('click','#createNewTemplate', function(e){
    e.preventDefault();
    $.ajax({
        url: "index.php?route=module/automatednewsletter/templateForm&token=" + token,
        type:'post',
        dataType:'html',
        success: function(data) { 
          $('#template').html(data);
        }
      });
  });

  $('body'). on('click','.templateList .pagination .links a', function(e){
    e.preventDefault(); 
    $.ajax({ 
        url: this.href,
        type:'post',
        dataType:'html',
        success: function(data) { 
          $('#template').html(data);
        }
      });
  });

  $('body'). on('click', '.templateList table thead td a', function(e){
    e.preventDefault(); 
    $.ajax({
        url: this.href,
        type:'post',
        dataType:'html',
        success: function(data) { 
          $('#template').html(data);
        }
      });
  });

  $('body').on('click', '.btn.deleteTemplate', function(e){
    e.preventDefault(); 
    if(confirm("Are you sure you want to delete selected templates?")) {
      var deleteList=[];
      $('input[type="checkbox"][name="deleteTemplate"]:checked').each(function(){ deleteList.push($(this).val());});
      $.ajax({
        url: "index.php?route=module/automatednewsletter/deleteTemplate&token=" + token,
        type:'post',
        dataType:'html',
        data: {'templates': deleteList, 'store_id': store_id },
        success: function(data) { 
          loadTemplateList();
        }
      });
    }
  });
  $('input[type="checkbox"][name="selectAll"]').on('change', function(){
    $('input[name="deleteTemplate"]').attr('checked', this.checked);
  });
}

function tabPersistance(){ 
  if (window.localStorage && window.localStorage['currentTab']) { 
    $('.mainMenuTabs a[href='+window.localStorage['currentTab']+']').trigger('click');  
  }
  if (window.localStorage && window.localStorage['currentSubTab']) { 
    $('a[href='+window.localStorage['currentSubTab']+']').trigger('click');  
  }
  $('.fadeInOnLoad').css('visibility','visible');
  $('.mainMenuTabs a[data-toggle="tab"]').click(function() {
    if (window.localStorage) {
      window.localStorage['currentTab'] = $(this).attr('href');
    }
  });

  $('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"])').click(function() {
    if (window.localStorage) {
      window.localStorage['currentSubTab'] = $(this).attr('href');
    }
  });
}

function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if( !emailReg.test( $email ) || !$email ) {
    return false;
  } else {
    return true;
  }
}     

function select_text(){
  var content=eval(this);
  content.focus();
  content.select();
}

function saveTemplate(ckeditors, callback) { 
  var templates = {};
  template_id = $('input[type="hidden"][name="template_id"]').val();
  $.each(ckeditors, function(i, ed){
	 
    templates[$('#' + ed[0].id).attr('language-id')] = {content:ed[0].innerText, subject: $('.subject.form-control[name^="template[' + $('#' + ed[0].id).attr('language-id') + ']"]').val()}
  });
 
  $.ajax({
    url:'index.php?route=module/automatednewsletter/saveTemplate&token=' + token,
    dataType: 'json',
    type:'post',
    data: {'template': templates, 'store_id': store_id, 'template_id': template_id},
    complete: callback
  });
  return true;
}


function sendMailButtons() {
	
  $('body').on('click','button#sendMessageSpecificMail', function(e) {
	  
    if(validateEmail($('#specificEmail').val())) {
       e.preventDefault();

       saveTemplate(ckeditors, function() { 
        $.ajax({
           url: "../index.php?route=module/automatednewsletter/sendNewsletter",
           type: "POST",
           data: {'specificEmail': $('#specificEmail').val(), 'store_id': store_id, secretWord:"Su3ov2JJpgT3QGhysaUN", languageId: $(".row-fluid.tab-pane.active.language").attr("language-id"), template_id: $('input[type="hidden"][name="template_id"]').val(), task_name: 'sendNewsletter'},
           dataType: "html",
           beforeSend: function() {
             $('#sendMessageSpecificMail').attr('disabled', true); 
             $('.input-append.sendSpecificMail').after('<span class="wait" style="margin-left:10px;"><i class="fa fa-circle-o-notch fa-spin"></i>');
           },
           success: function(data) {  
             $('.wait').remove();
             $('#sendMessageSpecificMail').attr('disabled', false);
             alert('The message has been sent!');
             refreshStatistics(); 
           } 
         });
       });

    } else {
      $('#specificEmail').addClass("error");
    }
	
  }); 

  $('body').on('click','button#sendMailToSpecifiedRecipients', function(e) {
    
	e.preventDefault();
	saveTemplate(ckeditors);
	
	var template_id = $('input[type="hidden"][name="template_id"]').val();
	var newsletter_id='';
	
	if(confirm('This action will send newsletter to all selected recipients! Click OK to proceed')) {
		$('#btnProgressAbort').addClass('disabled');
		$('#btnOkModalClose').removeClass('disabled');
		$('#percentProgress').width('0%').text('0%').parent().addClass('progress-striped');
		$('#progressModal').modal({
			backdrop: false,
			keyboard: false,
			show:  true
		});
	
		progress.init('sendNewsletter',function() {
		$('#progressMessage span').text('Building receivers list...');
	
			$.ajax({
				url: "../index.php?route=module/automatednewsletter/getReceivers",
				type: "POST",
				data: {'store_id': store_id, template_id: template_id, newsletter_id:newsletter_id},
				dataType: "json",
				beforeSend: function() {
					$('#sendMailToSpecifiedRecipients').attr('disabled', true);
					$('#sendMailToSpecifiedRecipients').after('<span class="wait" style="margin-left:5px;"><i class="icon-spinner icon-spin icon-large"></i>');
					$('#btnProgressAbort').removeClass('disabled');
					$('#btnOkModalClose').addClass('disabled');
				},
				success: function(json) { 
					var total_receivers = json.receivers.length;
					var newsletter_log = json.newsletter_log;
					
					if (json.error) {
						$('#progressMessage span').text(json.error);
						$('#btnProgressAbort').addClass('disabled');
						$('#btnOkModalClose').removeClass('disabled');
		
						$('#percentProgress').parent().removeClass('progress-striped');
						$('.wait').remove();
						$('#sendMailToSpecifiedRecipients').attr('disabled', false);
						refreshStatistics();	
					} else {
						function sendChunk(i) {
							
							if (i > total_receivers) {
								$('#btnProgressAbort').addClass('disabled');
								$('#btnOkModalClose').removeClass('disabled');
								$('#percentProgress').parent().removeClass('progress-striped');
								$('.wait').remove();
								$('#sendMailToSpecifiedRecipients').attr('disabled', false);
			
								refreshStatistics();
			
							} else {
								var temp_receivers,chunk = json.chunk;
								temp_receivers = json.receivers.slice(i,i+chunk);
			
								sendNewsletter(temp_receivers, total_receivers, template_id, newsletter_log, newsletter_id, function () {
									// Here we send the next email portion
									getProgress();  
									sendChunk(i+chunk); 
								});
							}
						}
			
						if (total_receivers > 0) {
							$('#progressMessage span').text('Sending newsletters...');
							sendChunk(0);
						} else {
							alert('Please, check carefully your list of receivers! The current one is empty.');
							$('#btnProgressAbort').addClass('disabled');
							$('#btnOkModalClose').removeClass('disabled');
			
							$('#percentProgress').parent().removeClass('progress-striped');
							$('.wait').remove();
							$('#sendMailToSpecifiedRecipients').attr('disabled', false);
							refreshStatistics();
						}
					}
				},
		
			});
		});
	}
});
  
//Send non-sent emails

	$(document).on('click','.sentNewsletters a.btn.btn-continue:not(.disabled)', function(e) { 
		e.preventDefault();
		var template_id = $(this).attr('data-template_id');
		var newsletter_id = $(this).attr('data-newsletter_id');
		
		if(confirm('This action will send newsletter to all remaining recipients! Click OK to proceed.')) {
			$('#btnProgressAbort').addClass('disabled');
			$('#btnOkModalClose').removeClass('disabled');
			$('#percentProgress').width('0%').text('0%').parent().addClass('progress-striped');
			$('#progressModal').modal({
				backdrop: false,
				keyboard: false,
				show:  true
			});
			
			progress.init('sendNewsletter',function() {
			
			$('#progressMessage span').text('Building receivers list...');
			
			$.ajax({
				url: "../index.php?route=module/automatednewsletter/getReceivers",
				type: "POST",
				data: {'store_id': store_id, template_id: template_id,newsletter_id:newsletter_id},
				dataType: "json",
				
				beforeSend: function() {
					$('.sentNewsletters a.btn-continue').addClass('disabled');
					$('#btnProgressAbort').removeClass('disabled');
					$('#btnOkModalClose').addClass('disabled');
				},
				
				success: function(json) {
					var total_receivers = json.receivers.length;
					var newsletter_log = [];
			
			 		function sendChunk(i) {
						if (i > total_receivers) {
							$('#btnProgressAbort').addClass('disabled');
							$('#btnOkModalClose').removeClass('disabled');
							$('#percentProgress').parent().removeClass('progress-striped');
							$('.wait').remove();
							$('#sendMailToSpecifiedRecipients').attr('disabled', false);
							refreshStatistics();
						} else { 
							var temp_receivers, chunk= json.chunk;
							temp_receivers = json.receivers.slice(i,i+chunk);
			
							sendNewsletter(temp_receivers, total_receivers, template_id,  newsletter_log, newsletter_id, function () {
								// Here we send the next email portion
								getProgress();  
								sendChunk(i+chunk); 
							});
						}
					}
			
					if (total_receivers > 0) {
						$('#progressMessage span').text('Sending newsletters...');
						sendChunk(0);
					} else {
						alert('Please, check carefully your list of receivers!');
					}

				},
			
			});
			
		});
		}
	});
}

function sendNewsletter(temp_receivers,all_receivers, template_id, newsletter_log, newsletter_id, callback) {
     $.ajax({
        url: "../index.php?route=module/automatednewsletter/sendNewsletter",
        type: "POST",
        data: {'store_id': store_id, 'receivers': temp_receivers, 'all_receivers': all_receivers, 'secretWord':"Su3ov2JJpgT3QGhysaUN", 'template_id': template_id, 'newsletter_log': newsletter_log, 'newsletter_id': newsletter_id, 'task_name': 'sendNewsletter'},
        dataType: "html",
        success: function(data) { 
          progress.getMessage(callback);
        },
    })
} 

function cronTabOptions(){
	$('#testCronAvailablity').on('click', function(e){
	var modal = $('#cronModal'), modalBody = $('#cronModal .modal-body');
    modal
        .on('show.bs.modal', function () { 
            modalBody.load('index.php?route=module/automatednewsletter/testCron&token=' + token)
        })
        .modal();
    e.preventDefault();
});

  if($('select[name="AutomatedNewsletter[scheduleType]"]').val() == 'P') {
    $('#fixedDateOptions').hide();
    $('#periodicOptions').show(200);
  } else {
    $('#periodicOptions').hide();
    $('#fixedDateOptions').show(200); 
  }

  $('select[name="AutomatedNewsletter[scheduleType]"]').on('change', function(e){ 
    if($(this).val() == 'P') {
      $('#fixedDateOptions').hide();
      $('#periodicOptions').show(200);  
    } else {
      $('#periodicOptions').hide();
      $('#fixedDateOptions').show(200); 
      } 
  });
 
  url = $('#cron_url').val();

  $(document).on('click','.btn.addDate', function(e){
      e.preventDefault();	  	  
	  if($('#fixedDate').val() && $('#fixedDateTime').val() ){
        $('#date' + $('#fixedDate').val().replace(/\//g, '')).remove();
        
		template_subject = $('#fixedDateOptions [name="scheduled_template"]').find(':selected').text();
        
		$('.scrollbox.dateList').append('<div id="date' + $('#fixedDate').val().replace(/\//g, '') + '">' + $('#fixedDate').val() + '/' + $('#fixedDateTime').val() + ' ' + template_subject + '<i class="icon-minus-sign removeIcon"></i><input type="hidden" name="AutomatedNewsletter[fixedDates][]" value="' + $('#fixedDate').val() + ' . ' + $('#fixedDateTime').val() + '~~' +   $('#fixedDateOptions [name="scheduled_template"]').val() + '~~' + template_subject +'" /></div>');
		
		
        $('.other_cron_services > tbody > .periodic').remove();
        
        $('.other_cron_services > tbody').append('<tr id="date' + $('#fixedDate').val().replace(/\./g,'') + '"><td>'+ template_subject +'</td><td style="color:#666; overflow-wrap: break-word;">'+url +'&store_id='+store_id+'&template_id=' + $('#fixedDateOptions [name="scheduled_template"]').val() + '</td></tr>');        
        
        $('#fixedDate').val('');		
		$('#fixedDateTime').val('');      
		} else {
          alert('Please fill date and time!');
      }
  });

  $('body').on('click','.scrollbox.dateList div  .removeIcon', function() {
    $(this).parent().remove();
  });

  $(document).ready(function(){
    if($('select[name="AutomatedNewsletter[scheduleEnabled]"]').val()=='yes'){
      $('.hideable').show();
    }
  });

  $('select[name="AutomatedNewsletter[scheduleEnabled]"]').on('change', function(e){
    if($(this).val() == 'yes'){
      $('.hideable').show();
    } else {
      $('.hideable').hide();
    }
  }); 
  
  $('select[name="AutomatedNewsletter[scheduled_template_periodic]"]').on('change', function(e){
    $('.other_cron_services > tbody > tr').remove();
        
    template_subject=$("#scheduled_template_periodic :selected").text();
        
    $('.other_cron_services > tbody').append('<tr class="periodic"><td>'+ template_subject +'</td><td style="color:#666;">'+url +'&store_id='+store_id+'&template_id=' +  $('select[name="AutomatedNewsletter[scheduled_template_periodic]"]').val() + '</td></tr>');        
        
   }); 
  
}

//Autocomplete functions start

//Customer Autocomplete
function customerAutocomplete() { 
  $('input[name=\'customers\']').autocomplete({

    delay: 500,
    source: function(request, response) { 
      $.ajax({
		  url: 'index.php?route=module/automatednewsletter/autocompleteCustomer&token=' + getURLVar('token') + '&store_id=' + store_id + '&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function (json) {
          response($.map(json, function (item) {
            return {
              category: item.customer_group,
              label: item.name,
              value: item.customer_id
            }
          }));
        }
      });
    },	
		'select': function(item) {
		 
		  $('#customer' + item['value']).remove();
		  $('#to-customer #customer_container').append('<div id="customer_container' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="AutomatedNewsletter[to][customer][]" value="' + item['value'] + '" /></div>');			
		}
	});
	$('#customer_container').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});	  
}
   
//Product Autocomplete                     
function productAutocomplete() { 
  $('input[name=\'product\']').autocomplete({
    delay: 500,
    source: function (request, response) {
      $.ajax({		
        url: 'index.php?route=catalog/product/autocomplete&token=' + getURLVar('token') + '&store_id=' + store_id + '&filter_name=' + encodeURIComponent(request),
        dataType: 'json',
        success: function (json) {
          response($.map(json, function (item) {
            return {
              label: item.name,
              value: item.product_id
            }
          }));
        }
      });
    },	
		'select': function(item) {
		  $('input[name=\'product\']').val('');
		  $('#custom-product' + item['value']).remove();
		  
		  $('#custom-product').append('<div id="custom-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="AutomatedNewsletter[custom_product][]" value="' + item['value'] + '" /></div>');			
		}
	});
	$('#custom-product').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});	  
}


function productboughtAutocomplete() {
  $('input[name=\'products\']').autocomplete({

    delay: 500,
    source: function(request, response) { 
      $.ajax({
        url: 'index.php?route=catalog/product/autocomplete&token=' + getURLVar('token') + '&store_id=' + store_id + '&filter_name=' + encodeURIComponent(request),
        dataType: 'json',
        success: function (json) {
          response($.map(json, function (item) {
            return {
              label: item.name,
              value: item.product_id
            }
          }));
        }
      });
    },	
		'select': function(item) {
		 
		  $('#product' + item['value']).remove();
		  
		  $('#product').append('<div id="product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="AutomatedNewsletter[to][product][]" value="' + item['value'] + '" /></div>');			
		}
	});
	$('#product').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});	  
}
 
// Affiliate Autocomplete                       
function affiliateAutocomplete() {
  $('input[name=\'affiliates\']').autocomplete({
    delay: 500,
    source: function (request, response) {
      $.ajax({
        url: 'index.php?route=module/automatednewsletter/autocompleteAffiliate&token=' + getURLVar('token') + '&store_id=' + store_id + '&filter_name=' +  encodeURIComponent(request),        
		dataType: 'json',
        success: function (json) {
          response($.map(json, function (item) {
            return {
              label: item.name,
              value: item.affiliate_id
            }
          }));
        }
      });
    },	
		'select': function(item) {
		  $('#affiliate' + item['value']).remove();
		  
		  $('#affiliate').append('<div id="affiliate' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="AutomatedNewsletter[to][affiliate]][]" value="' + item['value'] + '" /></div>');			
		}
	});
	$('#affiliate').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});	  
}


// Manufacturer Autocomplete
function manufacturerAutocomplete() {
   $('input[name=\'specific-manufacturers\']').autocomplete({ 
    delay: 500,
    source: function (request, response) {
      $.ajax({
        url: 'index.php?route=catalog/manufacturer/autocomplete&token=' + getURLVar('token') + '&store_id=' + store_id + '&filter_name=' +  encodeURIComponent(request),        
		dataType: 'json',
        success: function (json) {
          response($.map(json, function (item) {
            return {
              label: item.name,
              value: item.manufacturer_id
            }
          }));
        }
      });
    },	
		'select': function(item) {

		  $('#manufacturer' + item['value']).remove();

		  $('#manufacturer').append('<div id="manufacturer' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="AutomatedNewsletter[specific_manufacturer][]" value="' + item['value'] + '" /></div>');	
		  $('input[name=\'specific-manufacturers\']').val('');
      	  return false;		
		}
	});
	$('#manufacturer').delegate('.fa-minus-circle', 'click', function() {
		$(this).parent().remove();
	});	  
}
//Autocomplete functions end
