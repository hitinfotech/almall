/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

 /* global $, window */

 $(function () {
    'use strict';

    // INSTALOADER 2 ADDITIONS
    // Show the table only when the image tab is activated
    $('.nav-tabs li').on('click', function(){
        if ($(this).children('a[href="#tab-image"]')[0]) {
            $('#images').hide();
            $('.fileupload').removeClass('hidden');
        } else {
            $('.fileupload').addClass('hidden');
        }
    });

    performTaoSearch();

    initilizeFileUploader();

    bindUploadAndDestroyEvents();

    loadScriptsForNeededPage();

});


function loadScriptsForNeededPage()
{
    // Product Insert Page
    if (document.URL.indexOf('catalog/product/add') !== -1) {
        fillInProductTitleAndModel();
        setProductStatus(0);
        autosubmitProductInsertForm();
    }

    // Product Update Page
    if (document.URL.indexOf('catalog/product/edit') !== -1) {
        emptyPregeneratedProductName();
        emptyPregeneratedProductModel();
        emptyPregeneratedProductKeyword();
        setProductStatus(1);
    }
}

function fillInProductTitleAndModel()
{
    var placeHolder = 'product-' + new Date().getTime();

    // For different translations we genereate here 10 possible product titles
    for (var i = 1; i<=10; i++) {
        //$('input[name="product_description[' + i +'][name]"]').val(placeHolder);
        //$('input[name="product_description[' + i +'][meta_title]"]').val(placeHolder);
    }

    // Model
    //$('input[name="model"]').val(placeHolder);

    // SEO KeyWord Placeholder
    //$('input[name="keyword"]').val(placeHolder);
}

function setProductStatus(status)
{
    //$('select[name="status"]').val(status);
}

function autosubmitProductInsertForm()
{
    //$('#form-product').submit();
}


function emptyPregeneratedProductName()
{
    // Empty product pregenerated Name - for all the languages
    for (i=0;i<=10;i++) {
        var text = $('input[name="product_description[' + i  + '][name]"]').val();
        var pregeneratedPattern = /product-\d+/;
        var match = pregeneratedPattern.exec(text);
        if (match != null) {
            $('input[name="product_description[' + i + '][name]"]').val('');
        }

        var text = $('input[name="product_description[' + i  + '][meta_title]"]').val();
        var pregeneratedPattern = /product-\d+/;
        var match = pregeneratedPattern.exec(text);
        if (match != null) {
            $('input[name="product_description[' + i + '][meta_title]"]').val('');
        }

        // And focus on the product name field
        $('input[name="product_description[1][name]"]').focus();
    }
}

function emptyPregeneratedProductModel()
{

    var text = $('input[name="model"]').val();
    var pregeneratedPattern = /product-\d+/;
    var match = pregeneratedPattern.exec(text);
    if (match != null) {
        $('input[name="model"]').val('');
    }

}

function emptyPregeneratedProductKeyword()
{

    var text = $('input[name="keyword"]').val();
    var pregeneratedPattern = /product-\d+/;
    var match = pregeneratedPattern.exec(text);
    if (match != null) {
        $('input[name="keyword"]').val('');
    }

}



/**
* Set the order of the photos
*/
function setOrderOfProductPhotos() {

    $('tbody.files tr').each(function(){

        var sort_order = $(this).index();
        var file_name = $(this).find('.name')[0].textContent;
        // Trim the leading whitespaces
        file_name = file_name.replace(/^\s+/g, '');
        // And now trim the following whitespaces
        file_name = file_name.replace(/\s+$/g, '');

        $.ajax({
            url: 'view/javascript/instaloader2/server/php/order.php',
            type: 'POST',
            data: {'sort_order': sort_order, 'file_name': file_name, 'product_id': $("#global_product_id").val()},
            success: function(data) {
            }
        });
    });

}

function processExternalPhotosResult(result) {
    if (result.status == 'success') {

                $('.alert').removeClass('alert-warning').addClass('alert-success').html('<strong>Done:</strong> photos were successfully imported!').show();

                $('.files').html('');

                $.ajax({
                          // Uncomment the following to send cross-domain cookies:
                          //xhrFields: {withCredentials: true},
                          url: $('#fileupload').fileupload('option', 'url'),
                          dataType: 'json',
                          context: $('#fileupload')[0]
                      }).done(function (result) {
                          $(this).fileupload('option', 'done')
                          .call(this, $.Event('done'), {result: result});
                      });
                  } else if (result.status == 'error') {
                      $('.alert').removeClass('alert-warning').addClass('alert-danger').html('<strong>Error:</strong> ' + result.message).show();
                  }
}

function getParameterByName(name)
{
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return "";
else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

/**
DATE:
WHY:
*/
function makeTableSortable() {

    var el = document.getElementById('instaloaderFilesTable');

    instaSortable = new Sortable(el, {

        // dragging started
        onStart: function (/**Event*/evt) {

            evt.oldIndex;  // element index within parent
        },

        // dragging ended
        onEnd: function (/**Event*/evt) {

            evt.oldIndex;  // element's old index within parent
            evt.newIndex;  // element's new index within parent

            setOrderOfProductPhotos();
        }

    });

}

/**
    * Tao-Bao Importer Part
    */
    function performTaoSearch() {
        $('input[name="taobao-url"]').on('focus', function(e) {
          $(this).val('');
        });

        $('#tao-search').on('click', function(e) {
          e.preventDefault();
          $('.alert').removeClass('alert-success').removeClass('alert-danger').addClass('alert-warning').html('<strong>Working:</strong> receiving data...').show();
          var url = $('input[name="taobao-url"]').val();
          var product_id = $("#global_product_id").val();

          var store = '';

          if (url.indexOf('taobao.com') !== -1) {
            store = 'taobao';
          } else if (url.indexOf('tmall.com') !== -1) {
            store = 'tmall';
          } else if (url.indexOf('alibaba.com') !== -1) {
            store = 'alibaba';
          } else if (url.indexOf('aliexpress.com') !== -1) {
            store = 'aliexpress';
          }

          if (store != '') {

            $.ajax({
                data: {url: url, product_id: product_id},
                url: 'http://188.166.21.56:8080/' + store,
                //url: 'http://localhost:8080/' + store,
                dataType: 'json',
                method: 'POST'
            }).success(function (result) {

                $.ajax({
                    data: {json: result, product_id: product_id},
                    url: 'view/javascript/search-tao/',
                    dataType: 'json',
                    method: 'POST'
                }).success(function(result){
                    processExternalPhotosResult(result);
                });
            });

          } else {

            $.ajax({
                data: {url: url, product_id: product_id},
                url: 'view/javascript/search-tao/',
                dataType: 'json',
            }).success(function (result) {
                processExternalPhotosResult(result);

            });

          }
        });
    }

function initilizeFileUploader() {
        // Initialize the jQuery File Upload widget:
        $('#fileupload').fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            autoUpload: true,
            url: 'view/javascript/instaloader2/server/php/?product_id=' + $("#global_product_id").val()
        });

        // Enable iframe cross-domain access via redirect option:
        $('#fileupload').fileupload(
            'option',
            'redirect',
            window.location.href.replace(
                /\/[^\/]*$/,
                '/cors/result.html?%s'
                )
            );

        if (window.location.hostname === 'blueimp.github.io') {
            // Demo settings:
            $('#fileupload').fileupload('option', {
                url: '//jquery-file-upload.appspot.com/',
                // Enable image resizing, except for Android and Opera,
                // which actually support image resizing, but fail to
                // send Blob objects via XHR requests:
                disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
                maxFileSize: 5000000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
            });
            // Upload server status check for browsers with CORS support:
            if ($.support.cors) {
                $.ajax({
                    url: '//jquery-file-upload.appspot.com/',
                    type: 'HEAD'
                }).fail(function () {
                    $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                        new Date())
                    .appendTo('#fileupload');
                });
            }
        } else {
            // Load existing files:
            $('#fileupload').addClass('fileupload-processing');
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $('#fileupload').fileupload('option', 'url'),
                dataType: 'json',
                context: $('#fileupload')[0]
            }).always(function () {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {


                makeTableSortable();

                $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});


            });
        }
}

function bindUploadAndDestroyEvents() {

    // Get the product id form the URL
    var product_id = $("#global_product_id").val();
    var fileListIndex = $('input[name^=product_image]').filter('[type=hidden]').filter('[id^=image]').size();
    var sortOrder = parseInt(fileListIndex, 10)+1;

    $('#fileupload').bind('fileuploadcompleted', function (e, data) {

        fileListIndex++;
        sortOrder++;
        setOrderOfProductPhotos();
       // makeTableSortable();
    });

    $('#fileupload').bind('fileuploaddestroy', function (e, data) {

        var row_index = $(data.context[0]).index();

        var file = data.context[0].children[1].innerText;
        var delete_url = data.url + '&product_id=' + product_id;
        delete_url = delete_url.replace("https","http");
        
        $.ajax({
            url: delete_url,
            type: data.type,
            success: function() {

                $(data.context).remove();
                $('#image-row' + row_index).remove();
                setOrderOfProductPhotos();
            }
        });
    });

}
