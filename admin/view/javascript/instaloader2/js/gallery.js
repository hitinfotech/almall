$(function () {

	$('.img-thumbnail').on('click', function(){

		// Get the id of the product
		var productLink = $(this).parents('tr').children('td').last().children('a').attr('href');

		var regexS = "[\\?&]" + 'product_id' + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(productLink);
		
		var product_id = decodeURIComponent(results[1].replace(/\+/g, " "));

		var fetchUrl = 'view/javascript/instaloader2/server/php/?product_id=' + product_id;

		var galleryUrls = [];

		$.ajax({
            url: fetchUrl,
            success: function(data) {

            	_.each($.parseJSON(data)['files'], function(element, index, list) {
            		galleryUrls.push({
            			href: element.url,
            			thumbnail: element.thumbnailUrl
            		});
            	});
             
             	var gallery = blueimp.Gallery(galleryUrls);
            }
        });

		

	})

});