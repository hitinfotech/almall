<?php

error_reporting(E_ALL | E_STRICT);
if (!defined(__DIR__)){
    define(__DIR__, dirname(__FILE__));
}
if (!defined('ENVIROMENT')){
    define('ENVIROMENT', getenv('ENVIROMENT'));
}

$config = __DIR__ . '/../../../../../../config/config.php';
require $config;

$session_expire = 3600;
if (defined('HTTP_CATALOG')) {
    $session_expire = ( 3600 * 24); // 1 day for admin.mall.
} else {
    $session_expire = ( 3600 * 24 * 365); // 1 year for customers
}

if (!session_id()) {
    ini_set('session.gc_maxlifetime', $session_expire);
    ini_set('session.save_handler', 'redis');
    ini_set('session.save_path', 'tcp://' . REDIS_IP . ':' . REDIS_PORT . '?database=' . REDIS_SESSION_DB . '&avg_ttl=' . $session_expire . '&ttl=' . $session_expire);
    session_set_cookie_params(0, '/');
    session_start();
}

$product_id = $_POST['product_id'];
$sort_order = $_POST['sort_order'] + 1;

$prod_main_image =  query("SELECT image FROM `product` WHERE product_id = ".$product_id." AND image like '%".$_POST['file_name']."'", 2);

if(!count($prod_main_image) ){
    $prod_main_image =  query("SELECT image FROM `product_image` WHERE product_id = ".$product_id." AND image like '%".$_POST['file_name']."'", 2);
}
//$image = 'catalog/product-' . $product_id . '/' . str_replace(' ', '', trim($_POST['file_name'], ' '));
$image = $prod_main_image[0]['image'];

if((int)$product_id > 0){
    # Don't forget to update product table with the leading image
    if (file_exists(DIR_IMAGE . $image) && trim($_POST['file_name']) != '') {
        if ($sort_order == 1) {
            $add_to_db = query("UPDATE `" . DB_PREFIX . "product` SET image = '$image' WHERE product_id = $product_id");
            query("DELETE FROM `" . DB_PREFIX . "product_image` WHERE image = '$image' AND product_id = $product_id");
        } else {
            $add_to_db = query("DELETE FROM `" . DB_PREFIX . "product_image` WHERE product_id = $product_id AND image = '$image'");
            $add_to_db = query("INSERT INTO  `" . DB_PREFIX . "product_image` SET sort_order = $sort_order, product_id = $product_id, image = '$image'");
        }
    } else {
        $add_to_db = query("DELETE FROM `" . DB_PREFIX . "product_image` WHERE product_id = $product_id AND image = '$image'");
    }
} else {
    if (file_exists(DIR_IMAGE . $image) && trim($_POST['file_name']) != '') {
        $add_to_db = query("DELETE FROM `" . DB_PREFIX . "product_image_add` WHERE session_id='". session_id()."' AND product_id = '". (int) $product_id."' AND image = '$image'");
        $add_to_db = query("INSERT INTO `" . DB_PREFIX . "product_image_add` SET   session_id='". session_id()."', product_id = '". (int) $product_id."', sort_order = '".(int)$sort_order."', image = '$image'");
        echo ("INSERT INTO `" . DB_PREFIX . "product_image_add` SET   session_id='". session_id()."', product_id = '". (int) $product_id."', sort_order = '".(int)$sort_order."', image = '$image'");
    } else {
        $add_to_db = query("DELETE FROM `" . DB_PREFIX . "product_image_add` WHERE session_id='". session_id()."' AND product_id = '". (int) $product_id."' AND image = '$image'");
    }
}

return $add_to_db;

function query($query, $need_result = false)
{
    $database = DB_DATABASE;
    $host = DB_HOSTNAME;
    $username = DB_USERNAME;
    $password = DB_PASSWORD;
    $link = mysqli_connect($host,$username,$password);
    if (!$link) {
        die(mysqli_error($link));
    }
    $db_selected = mysqli_select_db($link, $database);
    if (!$db_selected) {
        die(mysqli_error($link));
    }
    $result = mysqli_query($link, $query);
    if($need_result === 2){
      $returned_data = array();
      while ($data = $result->fetch_assoc()){
        $returned_data[] = $data;
      }
      return $returned_data;
    }
    if ($need_result) {
        if (mysqli_num_rows($result) <= 0) {
            return 0;
        }
        return mysqli_result($result, 0);
    }
    mysqli_close($link);
    return $result;
}
