<?php
/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
error_reporting(E_ALL | E_STRICT);
if( !defined( __DIR__ ) )define( __DIR__, dirname(__FILE__) );
if( !defined( 'ENVIROMENT' ) )define('ENVIROMENT', getenv('ENVIROMENT'));

$config = __DIR__ . '/../../../../../../config/config.php';
require $config;

$session_expire = 3600;
if (defined('HTTP_CATALOG')) {
    $session_expire = ( 3600 * 24); // 1 day for admin.mall.
} else {
    $session_expire = ( 3600 * 24 * 365); // 1 year for customers
}

if (!session_id()) {
    ini_set('session.gc_maxlifetime', $session_expire);
    ini_set('session.save_handler', 'redis');
    ini_set('session.save_path', 'tcp://' . REDIS_IP . ':' . REDIS_PORT . '?database=' . REDIS_SESSION_DB . '&avg_ttl=' . $session_expire . '&ttl=' . $session_expire);
    session_set_cookie_params(0, '/');
    session_start();
}


$product_id = (isset($_GET['product_id'])?$_GET['product_id']:0);

require('PUploadHandler.php');


//$upload_handler = new UploadHandler(null, true, null, $product_id);
$upload_handler = new PUploadHandler(null, true, null, $product_id);
