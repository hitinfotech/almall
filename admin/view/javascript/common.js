function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

$(document).ready(function () {


        $("img").on("error", function(){
            $(this).attr('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAKQWlDQ1BJQ0MgUHJvZmlsZQAASA2dlndUU9kWh8+9N73QEiIgJfQaegkg0jtIFQRRiUmAUAKGhCZ2RAVGFBEpVmRUwAFHhyJjRRQLg4Ji1wnyEFDGwVFEReXdjGsJ7601896a/cdZ39nnt9fZZ+9917oAUPyCBMJ0WAGANKFYFO7rwVwSE8vE9wIYEAEOWAHA4WZmBEf4RALU/L09mZmoSMaz9u4ugGS72yy/UCZz1v9/kSI3QyQGAApF1TY8fiYX5QKUU7PFGTL/BMr0lSkyhjEyFqEJoqwi48SvbPan5iu7yZiXJuShGlnOGbw0noy7UN6aJeGjjAShXJgl4GejfAdlvVRJmgDl9yjT0/icTAAwFJlfzOcmoWyJMkUUGe6J8gIACJTEObxyDov5OWieAHimZ+SKBIlJYqYR15hp5ejIZvrxs1P5YjErlMNN4Yh4TM/0tAyOMBeAr2+WRQElWW2ZaJHtrRzt7VnW5mj5v9nfHn5T/T3IevtV8Sbsz55BjJ5Z32zsrC+9FgD2JFqbHbO+lVUAtG0GQOXhrE/vIADyBQC03pzzHoZsXpLE4gwnC4vs7GxzAZ9rLivoN/ufgm/Kv4Y595nL7vtWO6YXP4EjSRUzZUXlpqemS0TMzAwOl89k/fcQ/+PAOWnNycMsnJ/AF/GF6FVR6JQJhIlou4U8gViQLmQKhH/V4X8YNicHGX6daxRodV8AfYU5ULhJB8hvPQBDIwMkbj96An3rWxAxCsi+vGitka9zjzJ6/uf6Hwtcim7hTEEiU+b2DI9kciWiLBmj34RswQISkAd0oAo0gS4wAixgDRyAM3AD3iAAhIBIEAOWAy5IAmlABLJBPtgACkEx2AF2g2pwANSBetAEToI2cAZcBFfADXALDIBHQAqGwUswAd6BaQiC8BAVokGqkBakD5lC1hAbWgh5Q0FQOBQDxUOJkBCSQPnQJqgYKoOqoUNQPfQjdBq6CF2D+qAH0CA0Bv0BfYQRmALTYQ3YALaA2bA7HAhHwsvgRHgVnAcXwNvhSrgWPg63whfhG/AALIVfwpMIQMgIA9FGWAgb8URCkFgkAREha5EipAKpRZqQDqQbuY1IkXHkAwaHoWGYGBbGGeOHWYzhYlZh1mJKMNWYY5hWTBfmNmYQM4H5gqVi1bGmWCesP3YJNhGbjS3EVmCPYFuwl7ED2GHsOxwOx8AZ4hxwfrgYXDJuNa4Etw/XjLuA68MN4SbxeLwq3hTvgg/Bc/BifCG+Cn8cfx7fjx/GvyeQCVoEa4IPIZYgJGwkVBAaCOcI/YQRwjRRgahPdCKGEHnEXGIpsY7YQbxJHCZOkxRJhiQXUiQpmbSBVElqIl0mPSa9IZPJOmRHchhZQF5PriSfIF8lD5I/UJQoJhRPShxFQtlOOUq5QHlAeUOlUg2obtRYqpi6nVpPvUR9Sn0vR5Mzl/OX48mtk6uRa5Xrl3slT5TXl3eXXy6fJ18hf0r+pvy4AlHBQMFTgaOwVqFG4bTCPYVJRZqilWKIYppiiWKD4jXFUSW8koGStxJPqUDpsNIlpSEaQtOledK4tE20Otpl2jAdRzek+9OT6cX0H+i99AllJWVb5SjlHOUa5bPKUgbCMGD4M1IZpYyTjLuMj/M05rnP48/bNq9pXv+8KZX5Km4qfJUilWaVAZWPqkxVb9UU1Z2qbapP1DBqJmphatlq+9Uuq43Pp893ns+dXzT/5PyH6rC6iXq4+mr1w+o96pMamhq+GhkaVRqXNMY1GZpumsma5ZrnNMe0aFoLtQRa5VrntV4wlZnuzFRmJbOLOaGtru2nLdE+pN2rPa1jqLNYZ6NOs84TXZIuWzdBt1y3U3dCT0svWC9fr1HvoT5Rn62fpL9Hv1t/ysDQINpgi0GbwaihiqG/YZ5ho+FjI6qRq9Eqo1qjO8Y4Y7ZxivE+41smsImdSZJJjclNU9jU3lRgus+0zwxr5mgmNKs1u8eisNxZWaxG1qA5wzzIfKN5m/krCz2LWIudFt0WXyztLFMt6ywfWSlZBVhttOqw+sPaxJprXWN9x4Zq42Ozzqbd5rWtqS3fdr/tfTuaXbDdFrtOu8/2DvYi+yb7MQc9h3iHvQ732HR2KLuEfdUR6+jhuM7xjOMHJ3snsdNJp9+dWc4pzg3OowsMF/AX1C0YctFx4bgccpEuZC6MX3hwodRV25XjWuv6zE3Xjed2xG3E3dg92f24+ysPSw+RR4vHlKeT5xrPC16Il69XkVevt5L3Yu9q76c+Oj6JPo0+E752vqt9L/hh/QL9dvrd89fw5/rX+08EOASsCegKpARGBFYHPgsyCRIFdQTDwQHBu4IfL9JfJFzUFgJC/EN2hTwJNQxdFfpzGC4sNKwm7Hm4VXh+eHcELWJFREPEu0iPyNLIR4uNFksWd0bJR8VF1UdNRXtFl0VLl1gsWbPkRoxajCCmPRYfGxV7JHZyqffS3UuH4+ziCuPuLjNclrPs2nK15anLz66QX8FZcSoeGx8d3xD/iRPCqeVMrvRfuXflBNeTu4f7kufGK+eN8V34ZfyRBJeEsoTRRJfEXYljSa5JFUnjAk9BteB1sl/ygeSplJCUoykzqdGpzWmEtPi000IlYYqwK10zPSe9L8M0ozBDuspp1e5VE6JA0ZFMKHNZZruYjv5M9UiMJJslg1kLs2qy3mdHZZ/KUcwR5vTkmuRuyx3J88n7fjVmNXd1Z752/ob8wTXuaw6thdauXNu5Tnddwbrh9b7rj20gbUjZ8MtGy41lG99uit7UUaBRsL5gaLPv5sZCuUJR4b0tzlsObMVsFWzt3WazrWrblyJe0fViy+KK4k8l3JLr31l9V/ndzPaE7b2l9qX7d+B2CHfc3em681iZYlle2dCu4F2t5czyovK3u1fsvlZhW3FgD2mPZI+0MqiyvUqvakfVp+qk6oEaj5rmvep7t+2d2sfb17/fbX/TAY0DxQc+HhQcvH/I91BrrUFtxWHc4azDz+ui6rq/Z39ff0TtSPGRz0eFR6XHwo911TvU1zeoN5Q2wo2SxrHjccdv/eD1Q3sTq+lQM6O5+AQ4ITnx4sf4H++eDDzZeYp9qukn/Z/2ttBailqh1tzWibakNml7THvf6YDTnR3OHS0/m/989Iz2mZqzymdLz5HOFZybOZ93fvJCxoXxi4kXhzpXdD66tOTSna6wrt7LgZevXvG5cqnbvfv8VZerZ645XTt9nX297Yb9jdYeu56WX+x+aem172296XCz/ZbjrY6+BX3n+l37L972un3ljv+dGwOLBvruLr57/17cPel93v3RB6kPXj/Mejj9aP1j7OOiJwpPKp6qP6391fjXZqm99Oyg12DPs4hnj4a4Qy//lfmvT8MFz6nPK0a0RupHrUfPjPmM3Xqx9MXwy4yX0+OFvyn+tveV0auffnf7vWdiycTwa9HrmT9K3qi+OfrW9m3nZOjk03dp76anit6rvj/2gf2h+2P0x5Hp7E/4T5WfjT93fAn88ngmbWbm3/eE8/syOll+AAAQPUlEQVR4Ae2daZMURRCGaxdEBS9E8cBj0QgQ9YsR/v9fsF8IDe9jVUC5lEMEAVl9x6iNnNzqnD5nZreeilj6qOrqyqfzraurh43t7e3dRIAABIoENotnOQkBCMwIIBAcAQIBAQQSwCEKAggEH4BAQACBBHCIggACwQcgEBBAIAEcoiCAQPABCAQEEEgAhygIIBB8AAIBAQQSwCEKAggEH4BAQACBBHCIggACwQcgEBBAIAEcoiCAQPABCAQEEEgAhygIIBB8AAIBAQQSwCEKAggEH4BAQACBBHCIggACwQcgEBBAIAEcoiCAQPABCAQEEEgAhygIIBB8AAIBAQQSwCEKAggEH4BAQACBBHCIggACwQcgEBBAIAEcoiCAQPABCAQEEEgAhygIIBB8AAIBAQQSwCEKAggEH4BAQACBBHCIggACwQcgEBBAIAEcoiCAQPABCAQEEEgAhygIIBB8AAIBAQQSwCEKAggEH4BAQACBBHCIggACwQcgEBBAIAEcoiCAQPABCAQEEEgAhygIIBB8AAIBAQQSwCEKAggEH4BAQACBBHCIgsDRMRE8fPgwPXjwID1+/DgdPXo0Pf300+nYsWNpY2NjzNuQFwSWRmAUgdy6dStduXIl/fnnn/sKvrm5md544430+uuvpyNHjuyL5wQE1pnAYIH8/PPP6bfffmu08cmTJ+ny5cvp2rVr6cKFC+mZZ55pTEvE+hO4e/du+vvvv9Pzzz8/6yGsf4mHlXCQQH799ddQHLZojx49St9++2366KOPkloVwsEisLu7m77//vv0+++/zwqubvN7772XTp06dbAM6Vja3p6q8calS5eKt1Ptcvz48X1x9+/fT7/88su+85xYfwJ//PHHnjhUWglmZ2dntl3/0vcvYe8WRF0mQbLh5MmTaWtrKz311FOz02qO1Wpo0J7D9evX01tvvcV4JANZ4VbPTxWdDU2TKpp88eGff/6ZXa/JmMMaegvkxo0bc0zUbXr33Xf3xKFItSRnzpxJP/30015ajUnUTL/66qt759hZDQGJ4+LFi3M3/+STT+aeYY48ceJE3t3bqiKUoA5z6NXFElhf85w+fboIS0LwYw51tQgHi8CLL76YXnvttb1Caxr//fffP/RT+L1akNJ0rlqLUpA4XnjhhdkU8CuvvJIkJGaySqTW/5x6CJquV+WoMWYN0/a9BHLv3r19T7M0KM+J3nnnnVnr4luSHM/24BDQeOMwjzn8k+glEN+9UqZRX5QWw2Pn+KAQ6D0GsQZqsDblchLNlowdNIMzRr5j5OFt00RGqRIqpVPagxiG8tf1y7C9Vwuil342TNEX1XTw7du3k6aKdT+1QhrLaBzz3HPP2dsX97X0Rdfn8PLLL88GmRo/aYpaccr32WefTS+99NJs6rmtyKcq2507d9LVq1dnZdPyHM0AloLeSagMSq8yq/waG2im6Ycffpi96c7Xvfnmm0kDbBs0i6j7yMl80LS88ixd55lqWl/3LQWfdiz+sl0zqPnZyidUBm1//PHH2VrAXB6dVxmHhLUUSGn5iubh9Sc458+fn00hR4YrrcSVg8ZIEsdXX301V/NoRk1/qrH1ZniRSKYqm9azyTlLTptt0FY2fffdd3Ppbt68OXOYDz/8cGajfWfhKzPlIVstG53LIU/AlK7zTFW5NAWfdgz+JdvFTWL54IMPZrbbGdIx3vL36mL5pm3MFkRv5xet7fr6669TaaKg6WHpvMB98803c+Kw6eVkgh2FqcomZ1TNv0gcStckIr2MjeyL7FpG3FD+ke3iptZjEb8+dvYSiC/Iolq3bcH0kLW+y4bcfbCTABJo1yUr6o5ovKD81E3Lb/vtvXKzbc/l/SnLpi6PXW2Q7+m36l6U0qlrpWegWrvN2EW1ud5PlWpYsVHc2BMrQ/mr5+Btl835BaZs19/YoXMXy4tDBRpLIHIAm78eoF5GKegl1WeffbbXAgi4apWSo88uKPyjvNQXVpAjff7553PQtUq1KUxdNt1XNqrPLOcstcpewJo21+JPdXXEQvZouyiov64/2auW04a33367E1N77aL9ofxt/rL9448/nrFSt/CLL76w0aPtdxaI716VSqICq8sQBT1Y7wT+YemlYg6ae1ftn1eT6ryc1qbJaUtbCSmLQ/FqkZSfXTITzUhNWTaVRy/h7JtqnbNBFYcqBRs0CM3jANmngbVd1mPTrnp/CH/Z/tdff82ZYF84a9JGrZ4mLsYOvbpYiwohEeUmr2nr85BzWgdQDeFnq1Tr2WDFYs+X9nNTbON8N6JJIFOXTcs29ICjII6+fH71gucT5bfsuCH81dL5itnbOnS2qolHZ4GM1Z3y+fiugWp4n8a/we3S55QT+uDP2e6dTTt12TQmWLTKwPe/VT5dZ4MEvygfm36Z+5617u3PNfEvjau87SUBjmFfZ4GUHkCTYVEBvfOXnNBfbwfqiis5jb8mH/vunM77MuS0fjt12dqMo0q2+utkj2fkbVnV8RD+JYF4O8XCC24MW/dXqy1y1YOworD7ulxN/6effrqXk7pOmoLMQdd75/QOoGZ1578PcmzwzayO9VcSrb1u6P7UZWvzYH33qsnmpvNDGazy+ra2S4T+WQ0tdy+B6CHYQtt9FagkAFtQLw7FecMkEL3xXhR0na9NFl3TNX4dyubL0CSEUk3d1d51S+8r4Cbbm84PsadzF0s38027F4gvkK/5Sw/RO4DPo+nYd3+a0pXOl4RaSreKsvlyeIZNztB03ue3Dsdt+be1veRXQ+3s1YKoxrYD5EUC8fFtuhQahJVeZHmDvVh9/BTHqyibdyZfq2Y7vTPl8wd5621fpo29BWKBqxZXoZtqLy+QklP7ayVCLdhbh7AOZfNl8Ewzp2U6T77n1FtfoTbZ2HR+SPl6dbH8dKsKEHV1fBeljUCaHGCIsX2vbeucffNvc50vQ5MzrBO3Nna1SVMSSKkFbWLS5h5NaXq1IP4FnjLX4sGScBSXV4hqX8G/oNM5f23Tg9bMVhajmt5lfBe9DmUrTUSIke93+8pIbA968DbKHvmAZ9LkM0Ps7yWQ0ksZrROySzlyoaRqv7Tav+RRWu+EWv2pa33NqSUfGYQA+f5pvu+Y23UoW6nV1fIL+zZd7wsOo0B8C6JnK9utQOQTueIc89n36mKpwF4kWgfj18uooFrr5JtDf63SyVhrsK7xLY8cIItD1+R1SNqfMqxD2dTq+srA8+76CcCUzMbMWxWUt93b6lmMdf9eAtHNS2uHtCZfrYWcW7W/1kr5xXMShxVCNkQA/KyVvkqzwS9GW5ZA1qFsakm9vfo0IFcYYq7fQD6MQV0s21LKRn0RaVuM6BuiIUx6dbF0QzmzPiCyTbpU/eWXX87ek+i8bznydU0FVp72exC9gdfXc+q6KW8bpzxKIm3Ke+j5dSibFuTZmlItqpZ5a1WyKiYbN9TedbteNtrFrPIv2S7fkN3+U4Cxyt9bIFL12bNnZ1+4+cJYZds4dROi5ekam2gCwHat1AqVVu3KYX2Nau819v46lE0fM6mSsLM1GqvpTyG3zKW1S2PzWHZ+Eog+krOVrlZb5EpTYzS19GPb3ruLJUCq0fSbV22Cxi2acfKDbn/tuXPn9o1vfBoJQx/2LDusumzqi6tSagp6Fov4Nl277udVuUa26/cESrNdQ+0aJBDdXB/t6P/98Ovzc8GkajWD+vqrNDjP6fJWQtIH+LrGz14oL3Wr9OMEubbM1y1juw5lU8upH/+2bFR7ShxidpiDWlBvu/xAtvtfbhmLw8b29vb+337pmbuaNzX32sqZVXh1TezD7Jq1lrQoT9UgpZmcrvmNmX6VZVNXQ+MycRZjbWsJsl3jDm1V6Wbb9Ul27m6KxdZ//9NA1KVvw6v3GKSUuQQxds2ehVG636rPrbJscorSC9tVM1nG/WV7m97IGN3NUQWyDDjcoz4CmvSx/1mTBLL1X+vgQ57yzufHGJMgkEyT7VoT8O/ANEnjBeAFMqRrn2EMHqTnjNhCYCoCmoTwzu7fe2hM4gUyRncfgUz1VMl3VAL+nZdWaOjFod4JabLEr9jQlLhfQ9enQHSx+lDjmqUT0A9563eVc9C4xB7n83mr6fAxAi3IGBTJY3ICes/WdspW78qafhm/a0ERSFdipF8ZAf36pN6m+8F5LpCmdfXiWjNc+d1Ijuu7pYvVlxzXLZ2AnF6tQ160qZeC6mppfKKXpVO8SEYgS3/M3HAoAc1oqcvVtLxpaP72erpYlgb7EHAEEIgDwiEELAEEYmmwDwFHAIE4IBxCwBJAIJYG+xBwBBCIA8IhBCwBBGJpsA8BRwCBOCAcQsASQCCWBvsQcAQQiAPCIQQsAQRiabAPAUcAgTggHELAEkAglgb7EHAEEIgDwiEELAEEYmmwDwFHAIE4IBxCwBJAIJYG+xBwBBCIA8IhBCwBBGJpsA8BRwCBOCAcQsASQCCWBvsQcAQQiAPCIQQsAQRiabAPAUcAgTggHELAEkAglgb7EHAEEIgDwiEELAEEYmmwDwFHAIE4IBxCwBJAIJYG+xBwBBCIA8IhBCwBBGJpsA8BRwCBOCAcQsASQCCWBvsQcAQQiAPCIQQsAQRiabAPAUcAgTggHELAEkAglgb7EHAEEIgDwiEELAEEYmks2H/y5Ena3d1dkOr/6C5pdUWX9FOlbWVYZYn4f9JbPPCHDx+my5cvp5s3b6bNzc109uzZ2X9mX7q0S1pd3yX9VGlLdnDufwK0IC084cqVK+n69euzWv7x48dpZ2ensSXpkla37pJ+qrQtEFSbBIEsePTqUt24cWMu1aNHj9KdO3fmzumgS9qu6bvk3SXtPiM4MUcAgczh2H+wsbGRjh07NhehbtaJEyfmzumgS9qu6bvk3SXtPiM4MUcAgczhKB9sbW2lI0eOzCK1PXPmTDp6tDx865JWGXZJP1XastWcFYGN7e3tdtMylfPS2OP+/fvp+PHje2JpQtIlrfLokn6qtE221H4egdTuAdgfEqCLFeIhsnYCCKR2D8D+kAACCfEQWTsBBFK7B2B/SACBhHiIrJ0AAqndA7A/JIBAQjxE1k4AgdTuAdgfEkAgIR4iayeAQGr3AOwPCSCQEA+RtRNAILV7APaHBBBIiIfI2gkgkNo9APtDAggkxENk7QQQSO0egP0hAQQS4iGydgIIpHYPwP6QAAIJ8RBZOwEEUrsHYH9IAIGEeIisnQACqd0DsD8kgEBCPETWTgCB1O4B2B8SQCAhHiJrJ4BAavcA7A8JIJAQD5G1E0AgtXsA9ocEEEiIh8jaCSCQ2j0A+0MCCCTEQ2TtBBBI7R6A/SEBBBLiIbJ2Agikdg/A/pAAAgnxEFk7AQRSuwdgf0gAgYR4iKydAAKp3QOwPySAQEI8RNZOAIHU7gHYHxJAICEeImsngEBq9wDsDwkgkBAPkbUTQCC1ewD2hwQQSIiHyNoJIJDaPQD7QwIIJMRDZO0EEEjtHoD9IQEEEuIhsnYCCKR2D8D+kAACCfEQWTsBBFK7B2B/SACBhHiIrJ0AAqndA7A/JPAvhaEBmvVrwysAAAAASUVORK5CYII=');
        });



    //Form Submit for IE Browser
    $('button[type=\'submit\']').on('click', function () {
        $("form[id*='form-']").submit();
    });

    // Highlight any found errors
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();

        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });

    // Set last page opened on the menu
    $('#menu a[href]').on('click', function () {
        sessionStorage.setItem('menu', $(this).attr('href'));
    });

    if (!sessionStorage.getItem('menu')) {
        $('#menu #dashboard').addClass('active');
    } else {
        // Sets active and open to selected page in the left column menu.
        $('#menu a[href=\'' + sessionStorage.getItem('menu') + '\']').parents('li').addClass('active open');
    }

    if (localStorage.getItem('column-left') == 'active') {
        $('#button-menu i').replaceWith('<i class="fa fa-dedent fa-lg"></i>');

        $('#column-left').addClass('active');

        // Slide Down Menu
        $('#menu li.active').has('ul').children('ul').addClass('collapse in');
        $('#menu li').not('.active').has('ul').children('ul').addClass('collapse');
    } else {
        $('#button-menu i').replaceWith('<i class="fa fa-indent fa-lg"></i>');

        $('#menu li li.active').has('ul').children('ul').addClass('collapse in');
        $('#menu li li').not('.active').has('ul').children('ul').addClass('collapse');
    }

    // Menu button
    $('#button-menu').on('click', function () {
        // Checks if the left column is active or not.
        if ($('#column-left').hasClass('active')) {
            localStorage.setItem('column-left', '');

            $('#button-menu i').replaceWith('<i class="fa fa-indent fa-lg"></i>');

            $('#column-left').removeClass('active');

            $('#menu > li > ul').removeClass('in collapse');
            $('#menu > li > ul').removeAttr('style');
        } else {
            localStorage.setItem('column-left', 'active');

            $('#button-menu i').replaceWith('<i class="fa fa-dedent fa-lg"></i>');

            $('#column-left').addClass('active');

            // Add the slide down to open menu items
            $('#menu li.open').has('ul').children('ul').addClass('collapse in');
            $('#menu li').not('.open').has('ul').children('ul').addClass('collapse');
        }
    });

    // Menu
    $('#menu').find('li').has('ul').children('a').on('click', function () {
        if ($('#column-left').hasClass('active')) {
            $(this).parent('li').toggleClass('open').children('ul').collapse('toggle');
            $(this).parent('li').siblings().removeClass('open').children('ul.in').collapse('hide');
        } else if (!$(this).parent().parent().is('#menu')) {
            $(this).parent('li').toggleClass('open').children('ul').collapse('toggle');
            $(this).parent('li').siblings().removeClass('open').children('ul.in').collapse('hide');
        }
    });

    // Override summernotes image manager
    $('button[data-event=\'showImageDialog\']').attr('data-toggle', 'image').removeAttr('data-event');

    $(document).delegate('button[data-toggle=\'image\']', 'click', function () {
        $('#modal-image').remove();

        $(this).parents('.note-editor').find('.note-editable').focus();

        $.ajax({
            url: 'index.php?route=common/filemanager&token=' + getURLVar('token'),
            dataType: 'html',
            beforeSend: function () {
                $('#button-image i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                $('#button-image').prop('disabled', true);
            },
            complete: function () {
                $('#button-image i').replaceWith('<i class="fa fa-upload"></i>');
                $('#button-image').prop('disabled', false);
            },
            success: function (html) {
                $('body').append('<div id="modal-image" class="modal">' + html + '</div>');

                $('#modal-image').modal('show');
            }
        });
    });

    // Image Manager
    $(document).delegate('a[data-toggle=\'image\']', 'click', function (e) {
        e.preventDefault();

        $('.popover').popover('hide', function () {
            $('.popover').remove();
        });

        var element = this;

        $(element).popover({
            html: true,
            placement: 'right',
            trigger: 'manual',
            content: function () {
                return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
            }
        });

        $(element).popover('show');

        $('#button-image').on('click', function () {
            $('#modal-image').remove();

            $.ajax({
                url: 'index.php?route=common/filemanager&token=' + getURLVar('token') + '&target=' + $(element).parent().find('input').attr('id') + '&thumb=' + $(element).attr('id'),
                dataType: 'html',
                beforeSend: function () {
                    $('#button-image i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                    $('#button-image').prop('disabled', true);
                },
                complete: function () {
                    $('#button-image i').replaceWith('<i class="fa fa-pencil"></i>');
                    $('#button-image').prop('disabled', false);
                },
                success: function (html) {
                    $('body').append('<div id="modal-image" class="modal">' + html + '</div>');

                    $('#modal-image').modal('show');
                }
            });

            $(element).popover('hide', function () {
                $('.popover').remove();
            });
        });

        $('#button-clear').on('click', function () {
            $(element).find('img').attr('src', $(element).find('img').attr('data-placeholder'));

            $(element).parent().find('input').attr('value', '');

            $(element).popover('hide', function () {
                $('.popover').remove();
            });
        });
    });

    // tooltips on hover
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body', html: true});

    // Makes tooltips work on ajax generated content
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
    });

    // https://github.com/opencart/opencart/issues/2595
    $.event.special.remove = {
        remove: function (o) {
            if (o.handler) {
                o.handler.apply(this, arguments);
            }
        }
    }

    $('[data-toggle=\'tooltip\']').on('remove', function () {
        $(this).tooltip('destroy');
    });
});

// Autocomplete */
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('autocomplete', 'off');

            // Focus
            $(this).on('focus', function () {
                this.request();
            });

            // Blur
            $(this).on('blur', function () {
                setTimeout(function (object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function (event) {
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }

            // Show
            this.show = function () {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });

                $(this).siblings('ul.dropdown-menu').show();
            }

            // Hide
            this.hide = function () {
                $(this).siblings('ul.dropdown-menu').hide();
            }

            // Request
            this.request = function () {
                clearTimeout(this.timer);

                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function (json) {
                html = '';

                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu').html(html);
            }

            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

        });
    }
})(window.jQuery);
