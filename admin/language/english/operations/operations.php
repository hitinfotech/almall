<?php
// Heading
$_['heading_title']                = 'Operations';

// Text
$_['text_order']                   = 'Orders';
$_['text_delayed_order']           = 'DELAYED ORDERS';
$_['text_average_order']           = 'AVERAGE (based on last 100 orders)';
