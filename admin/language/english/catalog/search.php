<?php
// Heading
$_['heading_title']          = 'Popular Search';

// Text
$_['text_success']           = 'Success: You have modified search!';
$_['text_list']              = 'Search List';
$_['text_add']               = 'Add Search';
$_['text_edit']              = 'Edit Search';
$_['text_default']           = 'Default';
$_['text_approved']          = 'Approved';
$_['text_disapproved']       = 'Dis-approved';

// Column
$_['column_name']            = 'Search Keyword';
$_['column_counter']         = 'Search Counter';
$_['column_keytype']         = 'Search ON';
$_['column_url']             = 'Url';
$_['column_country']         = 'Country';
$_['column_language']        = 'Language';
$_['column_date_added']      = 'Last Search On';
$_['column_action']          = 'Action';
$_['column_approved']        = 'Approved';
$_['column_date_added']      = 'Last Search';

$_['button_approve']         = 'Approve';
$_['button_disapprove']      = 'Dis-approve';

// Entry
$_['entry_name']             = 'Search Keyword';
$_['entry_approved']         = 'Approval';
$_['entry_description']      = 'Description';

$_['entry_keyword']          = 'SEO Keyword';
$_['entry_url']              = 'Url';

$_['entry_country']          = 'Country';
$_['entry_language']         = 'Language';
$_['entry_counter']          = 'Counter';
$_['entry_keytype']          = 'Type';

// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the keyword is globally unique.';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent categories.';
$_['help_column']            = 'Number of columns to use for the bottom 3 categories. Only works for the top parent categories.';
$_['help_return_days']       = 'Return Days';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify search!';
$_['error_name']             = 'Category Name must be between 2 and 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO keyword already in use!';
