<?php


$_['heading_title'] = 'Upload Products';

$_['text_success'] = 'Sheets Added Successfully';
$_['text_success_enabled'] = 'Products Enabled successfully';
$_['text_success_disabled'] = 'Products Disabled successfully';
$_['text_list'] = 'Sheets List';
$_['text_form'] = 'Add Products';
$_['text_seller_name'] = 'Seller Name';
$_['text_products_currency'] = 'Products Price Currency';
$_['text_upload_file'] = 'file to upload';
$_['text_no_results'] = 'No Uploads Yet';

$_['column_seller_name'] = 'Seller Name';
$_['column_sheet_link'] = 'Products Sheet';
$_['column_username'] = 'Added By';
$_['column_date_added'] = 'Date Added';
$_['column_action'] = 'Action';

$_['button_add'] = 'Add New Sheet';
$_['button_save'] = 'Save';
$_['button_enable'] = 'Enable Products';
$_['button_disable'] = 'Disable Prodicts';
$_['button_cancel'] = 'Back';
