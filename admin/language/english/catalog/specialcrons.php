<?php
// Heading
$_['heading_title']          = 'Product Special Prices ';

// Text
$_['text_success']           = 'Success: You have modified products!';
$_['text_list']              = 'Product Special List';
$_['text_add']               = 'Add Special';
$_['text_edit']              = 'Edit Product';

$_['text_default']           = 'Default';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';
$_['text_no_results']        = 'No Record Found';
$_['text_form']              = 'Special Crons List';

// Column
$_['column_id']              = 'Cron ID';
$_['column_brand']           = 'Brand';
$_['column_category']        = 'Category';
$_['column_country']         = 'Country';
$_['column_percent']         = 'Percent';
$_['column_date_added']      = 'Added ON';
$_['column_date_start']      = 'Date Start';
$_['column_date_end']        = 'Date End';
$_['column_done']            = 'is Done';
$_['column_user']            = 'User';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 1 and less than 64 characters!';
$_['error_brand']            = 'Product Brand is mandetory field!';
$_['error_keyword']          = 'SEO keyword already in use!';
$_['error_category']        = 'Specify al lest one category';
