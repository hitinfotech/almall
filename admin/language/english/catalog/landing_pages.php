<?php
// Heading
$_['heading_title']                    = 'Landing Pages';
$_['text_list']                        = 'Landing Pages';

// Text
$_['text_settings']                    = 'Landing Pages';
$_['text_success']                     = 'Success: You have modified settings!';
$_['text_edit']                        = 'Edit Setting ';

$_['text_input_landing_page_name']      = 'English Page Name';
$_['text_input_landing_page_name_ar']   = 'Arabic Page Name';
$_['text_input_landing_page_color']     = 'Text Color';
$_['text_input_landing_page_background']= 'Background Color';
$_['entry_icon']                        = 'Icon File';
$_['text_input_landing_page_hcolor']     = 'Hover Text Color';
$_['text_input_landing_page_hbackground']= 'Hover Background Color';

$_['text_show_at_homepage']            = 'Show on Navigation YES/ NO';
$_['text_fontbold']                    = 'Font Bold YES/ NO';
$_['text_status']                      = 'Status Enable/ Disable';
$_['text_sub_pages']                   = 'Go To Pages';
$_['button_sub_pages']                   = 'Inner Pages';

$_['button_save']                      ='Save Page';
$_['column_id']                       ='Page Id';
$_['column_name']                      ='Page Name';
$_['column_name_ar']                   ='Arabic Page Name';
$_['column_date_modified']             ='Date Modified';
$_['column_innserpages']               ='Inner Pages';
$_['column_action']                    ='Action';


$_['entry_brand']                      = 'Show at brand ';
$_['entry_category']                   = 'Show at Category ';

$_['heading_title_subs']              = 'Pages of %s';
$_['text_list_subs']              = 'Pages of %s';

$_['tab_block1']                       = 'Block 1- Main Upper';
$_['tab_block2']                       = 'Block 2- Products';
$_['tab_block3']                       = 'Block 2- Selected Products';
$_['tab_block4']                        = 'Block 0- Countreis and Langauges';
$_['tab_block5']                        = 'Block 3- Page Headers';
$_['tab_block']                         = 'Block ';

$_['text_landing_pages_title']       = 'Page title';
$_['text_landing_pages_h1']       = 'Page H1';
$_['text_landing_pages_seo']       = 'Page url';

$_['column_name']                      ='English Page Name';
$_['column_language']                  ='Language';
$_['column_country']                   ='Country';
$_['column_page_h1']                   ='Page H1';
$_['column_page_title']                ='Page Title';
$_['column_action']                    ='Action';

$_['text_landing_pages_block1_title1']      = 'Title 1';
$_['text_landing_pages_block1_button1']      = 'Shop Button';
$_['text_landing_pages_block1_description1']= 'Description 1';
$_['text_landing_pages_block1_url1']= 'Url 1';
$_['text_landing_pages_block1_image1']= 'Image 1';

$_['text_landing_pages_block1_title2']      = 'Title 2';
$_['text_landing_pages_block1_button2']      = 'Shop Button';
$_['text_landing_pages_block1_description2']= 'Description 2';
$_['text_landing_pages_block1_url2']= 'Url 2';
$_['text_landing_pages_block1_image2']= 'Image 2';

$_['text_brand'] = 'Select Brand';
$_['text_brand_name'] = 'Brand title';
$_['text_brand_desc'] = 'Brand title';
$_['text_brand_button'] = 'Button text';
$_['text_no_results'] = 'No Result';
$_['text_confirm'] = 'Are you sure you want to delete these Pages ?';

$_['button_block_add']            = 'Add Blocks';
$_['text_landing_pages_additional_block_title'] = 'Title ';
$_['text_landing_pages_additional_block_action'] = 'Action ';
$_['text_landing_pages_additional_block_position'] = 'Position ';
$_['text_landing_pages_additional_block_url'] = 'URL';
$_['text_landing_pages_additional_block_image'] = 'Image';
$_['entry_products']                    = 'Products';


// Help
$_['help_comment']                     = 'This field is for any special notes you would like to tell the customer i.e. Store does not accept cheques.';

// Error
$_['error_warning']                    = 'Warning: Please check the form carefully for errors!';
$_['error_permission']                 = 'Warning: You do not have permission to modify settings!';
$_['error_field_reuired']              = ' %s field required';
$_['error_image_thumb']                = 'Product Image Thumb Size dimensions required!';
