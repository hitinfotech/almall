<?php
// Heading
$_['heading_title']          = 'Algolia Products';
$_['heading_title2']         = 'Algolia Search';


// Column
$_['column_name']            = 'Product Name';
$_['select_country']         = 'Select Country';
$_['column_model']           = 'Model';
$_['column_sku']             = 'SKU';
$_['column_image']           = 'Image';
$_['column_quantity']        = 'Quantity';
$_['entry_status']            = 'Status';
$_['entry_status']            = 'Status';
$_['brand_name']             = 'Brand Name';
$_['column_sort_order']      = 'Sort order';
$_['column_bsort_order']     = 'Brand Priority';
$_['column_date_added']      = 'Date Added';

 $_['text_list']             = 'Product List';

$_['entry_filter']           = 'Filters';
$_['entry_country']          = 'Country Name';


$_['column_action']          = 'Save';
