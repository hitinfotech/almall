<?php
// Heading
$_['heading_title']                    = 'Product Landing';
$_['text_list']                        = 'Product Landing Pages';

// Text
$_['text_settings']                    = 'Update Product Landing';
$_['text_success']                     = 'Success: You have modified settings!';
$_['text_edit']                        = 'Edit Setting';

$_['tab_block1']                       = 'Block 1- Main Upper';
$_['tab_block2']                       = 'Block 2- Recently arrived';
$_['tab_block3']                       = 'Block 3- Selected Products';
$_['tab_block4']                       = 'Block 4- Products';
$_['tab_block5']                       = 'Block 5- Offers';
$_['tab_block6']                         = 'Block 0- Countreis and Langauges';
$_['tab_block']                         = 'Block ';

$_['column_countries']                      ='Countries';
$_['column_language']                   = 'Language';
$_['column_gender']                     = 'Gender';
$_['column_page_id']                       ='Page Id';
$_['column_action']                    ='Action';

$_['text_product_landing_block1_title1']      = 'Title 1';
$_['text_product_landing_block1_button1']      = 'Shop Button';
$_['text_product_landing_block1_description1']= 'Description 1';
$_['text_product_landing_block1_url1']= 'Url 1';
$_['text_product_landing_block1_image1']= 'Image 1';

$_['text_product_landing_block1_title2']      = 'Title 2';
$_['text_product_landing_block1_button2']      = 'Shop Button';
$_['text_product_landing_block1_description2']= 'Description 2';
$_['text_product_landing_block1_url2']= 'Url 2';
$_['text_product_landing_block1_image2']= 'Image 2';

$_['text_product_landing_block3_title1']      = 'Title 1';
$_['text_product_landing_block3_description1']= 'Description 1';
$_['text_product_landing_block3_url1']= 'Url 1';
$_['text_product_landing_block3_image1']= 'Image 1';

$_['text_product_landing_block3_title2']      = 'Title 2';
$_['text_product_landing_block3_description2']= 'Description 2';
$_['text_product_landing_block3_url2']= 'Url 2';
$_['text_product_landing_block3_image2']= 'Image 2';

$_['text_product_landing_block3_title3']      = 'Title 3';
$_['text_product_landing_block3_description3']= 'Description 3';
$_['text_product_landing_block3_url3']= 'Url 3';
$_['text_product_landing_block3_image3']= 'Image 3';

$_['text_product_landing_block3_title4']      = 'Title 4';
$_['text_product_landing_block3_description4']= 'Description 4';
$_['text_product_landing_block3_url4']= 'Url 4';
$_['text_product_landing_block3_image4']= 'Image 4';


$_['text_product_landing_block2_title1']      = 'Title 1';
$_['text_product_landing_block2_description1']= 'Description 1';
$_['text_product_landing_block2_url1']= 'Url 1';
$_['text_product_landing_block2_image1']= 'Image 1';

$_['text_product_landing_block2_title2']      = 'Title 2';
$_['text_product_landing_block2_description2']= 'Description 2';
$_['text_product_landing_block2_url2']= 'Url 2';
$_['text_product_landing_block2_image2']= 'Image 2';

$_['text_product_landing_block2_title3']      = 'Title 3';
$_['text_product_landing_block2_description3']= 'Description 3';
$_['text_product_landing_block2_url3']= 'Url 3';
$_['text_product_landing_block2_image3']= 'Image 3';

$_['text_product_landing_block2_title4']      = 'Title 4';
$_['text_product_landing_block2_description4']= 'Description 4';
$_['text_product_landing_block2_url4']= 'Url 4';
$_['text_product_landing_block2_image4']= 'Image 4';

$_['text_brand'] = 'Select Brand';
$_['text_brand_name'] = 'Brand title';
$_['text_brand_desc'] = 'Brand title';
$_['text_brand_button'] = 'Button text';
$_['text_product_landing_block5_image1']= 'Offer Image ';

$_['button_block_add']            = 'Add Blocks';
$_['text_product_landing_additional_block_title'] = 'Title ';
$_['text_product_landing_additional_block_action'] = 'Action ';
$_['text_product_landing_additional_block_position'] = 'Position ';
$_['text_product_landing_additional_block_url'] = 'URL';
$_['text_product_landing_additional_block_image'] = 'Image';
$_['entry_products']                    = 'Products';


// Help
$_['help_comment']                     = 'This field is for any special notes you would like to tell the customer i.e. Store does not accept cheques.';

// Error
$_['error_warning']                    = 'Warning: Please check the form carefully for errors!';
$_['error_permission']                 = 'Warning: You do not have permission to modify settings!';
$_['error_field_reuired']              = ' %s field required';
$_['error_image_thumb']                = 'Product Image Thumb Size dimensions required!';
