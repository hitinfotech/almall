<?php
// Heading
$_['heading_title']                    = 'NEW IN';
$_['text_list']                        = 'NEW IN Pages';

// Text
$_['text_settings']                    = 'NEW IN';
$_['text_success']                     = 'Success: You have modified settings!';
$_['text_edit']                        = 'Edit Setting ';

$_['tab_block1']                       = 'Block 1- Main Upper';
$_['tab_block2']                       = 'Block 3- Products';
$_['tab_block3']                       = 'Block 2- Selected Products';
$_['tab_block4']                        = 'Block 0- Countreis and Langauges';
$_['tab_block']                         = 'Block ';

$_['column_name']                      ='Country';
$_['column_url']                       ='Language';
$_['column_action']                    ='Action';

$_['text_ramadan_block1_title1']      = 'Title 1';
$_['text_ramadan_block1_button1']      = 'Shop Button';
$_['text_ramadan_block1_description1']= 'Description 1';
$_['text_ramadan_block1_url1']= 'Url 1';
$_['text_ramadan_block1_image1']= 'Image 1';

$_['text_ramadan_block1_title2']      = 'Title 2';
$_['text_ramadan_block1_button2']      = 'Shop Button';
$_['text_ramadan_block1_description2']= 'Description 2';
$_['text_ramadan_block1_url2']= 'Url 2';
$_['text_ramadan_block1_image2']= 'Image 2';

$_['text_brand'] = 'Select Brand';
$_['text_brand_name'] = 'Brand title';
$_['text_brand_desc'] = 'Brand title';
$_['text_brand_button'] = 'Button text';

$_['button_block_add']            = 'Add Blocks';
$_['text_ramadan_additional_block_title'] = 'Title ';
$_['text_ramadan_additional_block_action'] = 'Action ';
$_['text_ramadan_additional_block_position'] = 'Position ';
$_['text_ramadan_additional_block_url'] = 'URL';
$_['text_ramadan_additional_block_image'] = 'Image';
$_['entry_products']                    = 'Products';


// Help
$_['help_comment']                     = 'This field is for any special notes you would like to tell the customer i.e. Store does not accept cheques.';

// Error
$_['error_warning']                    = 'Warning: Please check the form carefully for errors!';
$_['error_permission']                 = 'Warning: You do not have permission to modify settings!';
$_['error_field_reuired']              = ' %s field required';
$_['error_image_thumb']                = 'Product Image Thumb Size dimensions required!';
