<?php


$_['heading_title'] = 'Product Ranking Algorithm';
$_['text_form'] = 'Product Ranking Algorithm';

$_['tab_block1'] = 'Number of visits of product page';
$_['tab_block2'] = 'Add to cart rate (product add to cart/product page visit)';
$_['tab_block3'] = 'New product';
$_['tab_block4'] = 'Add to favorite';
$_['tab_block5'] = 'Product on Sale';


$_['button_add_new_range'] = 'Add New Range';
$_['button_remove'] = 'Remove';
$_['button_save'] = 'Save';
$_['button_cancel'] = 'Cancel';
$_['text_from'] = 'From:';
$_['text_to'] = 'To:';
$_['text_value'] = 'Value:';

$_['text_success'] = 'Product Algorithm Updated successfully';
