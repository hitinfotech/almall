<?php
// Heading
$_['heading_title']         = 'Market Place Partners';

// Text
$_['text_success']          = 'Success: You have modified Marketplace Partners !';
$_['text_success_seller']   = 'Success: You have successfully updated Product(s) Partner !';

$_['text_default']          = 'Default';
$_['text_isnotpartner']     = 'Cancel Partnership';
$_['text_ispartner']        = 'Make Partner';
$_['text_approved']         = 'You have update %s partners status!';
$_['text_wait']             = 'Please Wait!';
$_['text_balance']          = 'Balance:';
$_['text_view_requested']   = 'Requested But not Partner';
$_['text_view_partners']    = 'Partners';
$_['text_view_all'] 	    = 'All';
$_['text_login']            = 'Login Front';
$_['text_confirm']          = 'Delete/Uninstall cannot be undone! Are you sure you want to do this ?';
$_['text_form']             = 'Edit Partner';
$_['text_loading']          = 'Loading...';
$_['text_update']           = 'Update';

$_['button_delete']         = 'Delete (Only MP Data)';

// Column
$_['column_sellerId']       = 'ID';
$_['column_name']           = 'Customer';
$_['column_company']        = 'Company';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Customer Group';
$_['column_status']         = 'Status';
$_['column_login']          = 'Type';
$_['column_approved']       = 'Approved';
$_['column_date_added']     = 'Added';
$_['column_description']    = 'Description';
$_['column_amount']         = 'Amount';
$_['column_action']         = 'Action';
$_['column_ip']         	= 'Ip';

// Entry
$_['entry_customer_type']  		= 'Customer Type ';
$_['entry_customer_type_info']  = 'Who has requested for Seller ship or not.';
$_['entry_excluded_countries']  = 'Execluded countries';
$_['entry_contract_date']  = 'contract Date';

$_['entry_firstname']           = 'Fisrt Name';
$_['entry_lastname']            = 'Last Name';

$_['entry_partner_commission']  = 'Commission ';
$_['entry_quantity_sold']       = 'Total Quantity sold ';
$_['entry_income']           	= 'Total Income ';
$_['entry_partner_income']      = 'Partner Income ';
$_['entry_admin_income']        = 'Admin Income ';
$_['entry_total_paid']      	= 'Total Paid Amount ';
$_['entry_left_paid']  			= 'Paid Amount (Left) ';
$_['entry_password']        = 'Password';
$_['entry_confirm']         = 'Confirm';
$_['entry_approved']         = 'Approevd';
$_['entry_not_approved']     = 'Not Approved';
$_['entry_return_policy']     = 'Return Policy';
$_['entry_available_countries'] = 'Available Countries';
$_['entry_yes']              = 'Yes';
$_['entry_no']               = 'No';

$_['entry_trackcode']        = 'Pixel Code Status';
$_['entry_trackcodeadd']     = 'Pixel Code - Add to Cart';
$_['entry_trackcodeview']    = 'Pixel Code - View Product';


$_['entry_is_fbs'] = 'Is FBS';
$_['entry_date_range'] = 'Date Range';
$_['entry_fbs_date_in'] = 'Date In';
$_['entry_fbs_date_out'] = 'Date Out';
$_['entry_fbs_store_location'] = 'FBS Store Location';
$_['entry_fbs_inventory_volume'] = 'FBS store Volume';
$_['entry_fbs_storage_charges'] ='Storage Chanrges';
$_['entry_fbs_fullfillment_charges'] ='Fullfillment Chanrges';
$_['entry_fbs_free_storage_days'] = ' Free storage days';


//product tab
$_['add_product']          = 'Seller Products ';
$_['entry_product_id_info']     = 'Only Store Products can add to Seller not other Seller\'s.';

$_['entry_commission']        	= 'Commission ';
$_['entry_paypalid']          	= 'Paypal ID ';
$_['entry_otherinfo']         	= 'Other Payment Info (Bank) ';

//order tab
$_['entry_name']             	= 'Customer Name';
$_['entry_products']            = 'Products';
$_['entry_total']             	= 'Total';
$_['entry_orderid']          	= 'Order Id';
$_['text_edit']              	= 'Edit';
$_['text_view']              	= 'View';

//profile tab
$_['entry_screenname']          = 'Screen Name ';
$_['entry_screenname_info']     = 'SEF keyword for Profile';
$_['entry_gender']        		= 'Gender ';
$_['entry_profile']         	= 'Seller Profile Info ';
$_['entry_store']         		= 'Company Info ';
$_['entry_SEO_url']      		   = "Client's URL";
$_['entry_twitter']      		   = 'Twitter Id ';
$_['entry_website']      		   = 'Website';
$_['entry_instagramid']      	 = 'Instagram Id ';

$_['entry_facebook']          	= 'Facebook Id ';
$_['entry_theme']       		= 'Theme ';
$_['entry_banner']       		= 'Company Banner';
$_['entry_banner_info']       	= '1130px X 150px';
$_['entry_logo']            	= 'Company Logo';
$_['entry_logo_info']       	= '200px X 200px';
$_['entry_avatar']       		= 'Avatar';
$_['entry_avatar_info']       	= 'Seller Image 200px X 200px';
$_['entry_locality']         	= 'Fill your Company City ';
$_['entry_country']         	= 'Country ';
$_['entry_zone']            	= 'Zone ';
$_['entry_city']            	= 'City';
$_['entry_address']         	= 'Office address ';
$_['entry_warehouse_address'] = 'Warehouse address ';
$_['entry_num_of_offices']    = 'Number Of Branches';
$_['entry_open']            	= 'Opening Times ';
$_['entry_flat_rate']         = 'Flat rate';
$_['entry_email']             = 'Email';
$_['entry_phone']             = 'Telephone';
$_['select_country']         	= 'Select Country ';
$_['show_contract']           ='Show Contract';
$_['entry_passport_num']      = 'Passport Number';

// Legal tab
$_['entry_legal_name'] = 'Legal Name';
$_['entry_company_name'] = 'Company Name';
$_['entry_administrator_contact'] = 'Administrator Contact';
$_['entry_nationality'] = 'Nationality';
$_['entry_legal_email'] = 'Email Address';
$_['entry_direct_officer_number'] = 'Direct Office Phone Number';
$_['entry_legal_phone'] = 'Mobile Number';
$_['entry_hd_logo']     = 'HD Logo';
$_['entry_license']     = 'License';
$_['entry_passport']     = 'Passport';
$_['entry_hd_logo_info']     = 'HD Logo';
$_['entry_license_info']     = 'License';
$_['entry_passport_info']     = 'Passport';
$_['entry_trade_license_number']  = 'trade Licensa number';
$_['entry_address_license_number'] = 'License number Adress';
$_['entry_admin_name'] = 'Admin Contact';
$_['entry_admin_phone'] = 'Admin Phone';
$_['entry_admin_email'] = 'Admin Email';

// finance tab
$_['entry_company_officer_name'] = 'Company Financial officer contact Name';
$_['entry_financial_email'] = 'email address';
$_['entry_financial_phone'] = 'Phone Number';
$_['entry_beneficiary_name'] = 'Beneficiary Name';
$_['entry_bank_name'] = 'Bank Name';
$_['entry_bank_account'] = 'Bank Account';
$_['entry_iban_code'] = 'IBAN code';
$_['entry_swift_code'] = 'Swift code';
$_['entry_currency'] = 'Currency';
$_['entry_eligible_vat'] = 'Eligible for VAT';
$_['entry_eligible_upload'] = 'Upload VAT registration document';
$_['entry_eligible_business_address'] = 'Registered Business Address';
$_['entry_eligible_tax_reg_num'] = 'Tax Registration Number';
$_['entry_eligible_issue_date'] = 'Issue Date';
$_['entry_account_manager'] = 'Account Manager';
$_['select_account'] = ' Select account Manager';
$_['no_account_manager'] = '';

//transaction tab
$_['entry_description']     	= 'Description';
$_['entry_amount']          	= 'Amount';

//tab
$_['tab_location']      	   	= 'Location';
$_['tab_info']      	   		  = 'Partner Details';
$_['tab_product']          		= 'Payment & Commission';
$_['tab_legal_info']          = 'Legal Information';
$_['tab_financial_info']      = 'Financial Information';
$_['tab_order']           		= 'Orders';
$_['tab_transaction_info']      = 'You can see all transactions related to Seller here and can Add transactions for this Seller.';
$_['tab_profile_info']      	= 'Under this tab You can edit all Profile options for Seller.';
$_['tab_order_info']      		= 'All Order(s) which have Seller products.';
$_['tab_product_info']      	= 'Seller Commission, Payment, Bank Details. You can add more products to this Seller from here.';

// Error
$_['error_permission']      = 'Warning: You do not have permission to modify Marketplace Partners !';
$_['error_seller']          = 'Warning: No Data exists related to this Seller Id !';

$_['error_warning']         = 'Warning: Please check the form carefully for errors!';
$_['error_required']          = 'Error: this Field is required!';
$_['error_unique']          = 'Error: This seo is added for other partner !';
$_['error_wrong_syntax']    = "Error: The seo can contian only letters, numbers and - sign ";
$_['error_duplicated_product'] = 'Error: at least one of the seller products exists in %s products';
$_['error_companyname_required'] = 'Error: Company Name is required';
$_['error_business_address_required'] = 'Error: Business Address is required';
$_['error_tax_reg_num_required'] = 'Error: Tax Number is required';
$_['error_issue_date_required'] = 'Error: Issue Date is required';

$_['button_insert']         = 'Add Partner';
$_['total_transaction_fees'] = 'Seller Amount ';
