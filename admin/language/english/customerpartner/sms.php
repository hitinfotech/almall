<?php
################################################################################################
# Seller / Admin mail for Opencart 1.5.x.x From webkul http://webkul.com  	  	       #
################################################################################################

// Heading
$_['heading_title']    	     = 'Market Place SMS';

// Text
$_['text_module']            = 'Modules';
$_['text_success']       	 = 'Success - SMS Saved Successfully.';
$_['text_update']        	 = 'Success - SMS Updated Successfully.';
$_['text_delete']        	 = 'Success - SMS Deleted Successfully.';
$_['text_edit']            	 = 'Edit';

// Button
$_['button_save']			 = 'Save';
$_['button_back']			 = 'Back';
$_['button_cancel']			 = 'Cancel';
$_['button_insert']			 = 'Add';
$_['button_delete']			 = 'Delete';
$_['button_filter']			 = 'Filter';
$_['button_clear']			 = 'Clear Filter';

// Entry
$_['entry_id']        		 = 'Id';
$_['entry_name']      		 = 'Name';
$_['entry_name_info']      	 = 'Name which will display at time of selection.';
$_['entry_message']          = 'Message';
$_['entry_message_info']     = 'Add SMS';
$_['entry_no_records']       = 'No Records Found !';
$_['entry_action']     		 = 'Action';

// Error
$_['error_name']             = 'Warning - Please add Name for this SMS !!';
$_['error_message']    		 = 'Warning - Please add Message for SMS, character must between 25 to 5000 !!';

// Info
$_['info_sms']			    = ' You can read Info for more details.';

$_['info_sms_add']			= ' You can save SMS messages for default conditions for Marketplace. Create here and add these Under Module Marketplace > SMS tab to conditions. Marketplace module send SMS to Admin / Seller in these conditions -
<br/><br/> Customer request to Partnership (SMS to Admin + Thanks SMS to Customer). 
<br/> Customer request Approval (SMS to Customer). 
<br/> Customer Product add request (SMS to Admin + Thanks SMS to Seller). 
<br/> Customer Product Approval (SMS to Seller). 
<br/> After Transaction added by Admin (SMS to Seller). 
<br/> After Customer will buy Seller\'s Products (SMS to Seller). 
<br/> Seller contact to Admin (SMS to Admin). 
<br/> Customer contact to Seller (SMS to Seller). 
<br/><br/> So You can add SMS Messages here. Some SMS will work normally but some with Message + Marketplace data combination like Transaction and Order to Seller. So to do this you can add sort codes with your message, using this sort codes you can add these details also in SMS.
<br/>
.';




$_['tab_info']      	 	 = 'Info';
$_['tab_general']         	 = 'Add Message';
$_['entry_for']         	 = 'For';
$_['entry_code']      		 = 'Keyword';


//user
$_['text_hello']    		  = 'Hello ';
$_['text_to_seller']    	  = 'Your request has been approved. Please login and manage your store .';
$_['text_auto']    	          = 'Your request has been approved. Please login and manage your store .';
$_['text_ur_pre']    	      = 'Admin commission will be ';
$_['text_sellersubject']      = 'Your Request has been approved. ';
$_['text_thanksadmin']    	  = 'Thank you, ';

//for product mail
$_['entry_pname']      	      = 'Product Name - ';
$_['ptext_auto']    	      = 'Your product has been approved. Please login and manage your store .';
$_['ptext_sellersubject']     = 'Your Product has been approved';

?>
