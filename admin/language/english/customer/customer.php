<?php
// Heading
$_['heading_title']         = 'Customers';

// Text
$_['text_success']          = 'Success: You have modified customers!';
$_['text_list']             = 'Customer List';
$_['text_add']              = 'Add Customer';
$_['text_edit']             = 'Edit Customer';
$_['text_default']          = 'Default';
$_['text_balance']          = 'Balance';
$_['text_ksa']              = 'KSA';
$_['text_ae']               = 'UAE';

// Column
$_['column_name']           = 'Customer Name';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Customer Group';
$_['column_status']         = 'Status';
$_['column_date_added']     = 'Date Added';
$_['column_user_added']     = 'User Added';
$_['column_comment']        = 'Comment';
$_['column_description']    = 'Description';
$_['column_amount']         = 'Amount';
$_['column_points']         = 'Points';
$_['column_ip']             = 'IP';
$_['column_total']          = 'Total Accounts';
$_['column_action']         = 'Action';

// Entry
$_['entry_customer_group']  = 'Customer Group';
$_['entry_firstname']       = 'First Name';
$_['entry_lastname']        = 'Last Name';
$_['entry_email']           = 'E-Mail';
$_['entry_telephone']       = 'Telephone';
$_['entry_fax']             = 'Fax';
$_['entry_newsletter']      = 'Newsletter';
$_['entry_status']          = 'Status';
$_['entry_approved']        = 'Approved';
$_['entry_safe']            = 'Safe';
$_['entry_password']        = 'Password';
$_['entry_confirm']         = 'Confirm';
$_['entry_company']         = 'Company';
$_['entry_address_1']       = 'Address 1';
$_['entry_address_2']       = 'Address 2';
$_['entry_city']            = 'City';
$_['entry_postcode']        = 'Postcode';
$_['entry_country']         = 'Country';
$_['entry_zone']            = 'Region / State';
$_['entry_default']         = 'Default Address';
$_['entry_comment']         = 'Comment';
$_['entry_description']     = 'Description';
$_['entry_amount']          = 'Amount';
$_['entry_points']          = 'Points';
$_['entry_name']            = 'Customer Name';
$_['entry_ip']              = 'IP';
$_['entry_date_added']      = 'Date Added';
$_['entry_product_id']      = 'Favorite products';
$_['entry_brand_id']        = 'Favorite brands';
$_['entry_seller_id']       = 'Favorite sellers';
$_['entry_category_id']     = 'Favorite categories';
$_['entry_look_id']       = 'Favorite Looks';
$_['entry_tip_id']       = 'Favorite Tips';
$_['entry_product_id_info']      = 'Favorite products info ';
$_['entry_brand_id_info']        = 'Favorite brands info ';
$_['entry_seller_id_info']       = 'Favorite sellers info ';
$_['entry_category_id_info']       = 'Favorite categories info ';
$_['entry_look_id_info']       = 'Favorite Looks info ';
$_['entry_tip_id_info']       = 'Favorite Tips info ';
$_['entry_pricecode']     = 'Currency Code';

// Help
$_['help_safe']             = 'Set to true to avoid this customer from being caught by the anti-fraud system';
$_['help_points']           = 'Use minus to remove points';

// Error
$_['error_warning']         = 'Warning: Please check the form carefully for errors!';
$_['error_permission']      = 'Warning: You do not have permission to modify customers!';
$_['error_amount']          = 'Warning: Amount must be more than 0 !';
$_['error_description']     = 'Warning: Description field required !';

$_['error_exists']          = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']       = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']        = 'Last Name must be between 1 and 32 characters!';
$_['error_email']           = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']       = 'Telephone must be between 3 and 32 characters!';
$_['error_password']        = 'Password must be between 4 and 20 characters!';
$_['error_confirm']         = 'Password and password confirmation do not match!';
$_['error_address_1']       = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']            = 'City must be between 2 and 128 characters!';
$_['error_postcode']        = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_country']         = 'Please select a country!';
$_['error_zone']            = 'Please select a region / state!';
$_['error_custom_field']    = '%s required!';




// Entry
$_['entry_start_date'] = 'Start Date';
$_['entry_end_date'] = 'End Date';
$_['entry_date_added'] = 'Creat Date';
$_['entry_sort_order'] = 'Sort Order';
$_['entry_featured'] = 'Is Featured';

$_['entry_title'] = 'News Title';
$_['entry_email'] = 'E-mail ';
$_['entry_gender'] = 'Gender ';
$_['entry_status'] = 'Status ';
$_['select_gender'] = 'Select Gender ';
$_['column_id'] = 'ID ';
$_['column_email'] = 'Email ';
$_['column_gender'] = 'Gender ';
$_['entry_created_date'] = 'Date Added ';
$_['entry_updated_date'] = 'Modify Date ';
$_['entry_language_id'] = 'language ';
