<?php
// Heading
$_['heading_title']      			= 'ProductBundles';

// Text
$_['text_module']         			= 'Modules';
$_['text_success']        			= 'Success: You have modified module ProductBundles!';
$_['text_success_activation']       = 'ACTIVATED: You have successfully activated ProductBundles!';
$_['entry_yes']						= 'Yes';
$_['entry_no']						= 'No';
$_['error_permission']    			= 'Warning: You do not have permission to modify module ProductBundles!';

// Total messages
$_['text_total_text']               = 'Warning! The Total Extension is Not Activated!';
$_['text_total_text_helper']        = 'The module is installed correctly, however, the total extension is not enabled. This means that although the bundles will be added to the shopping cart, the discount will not be applied. You need to install &amp; enable the total extension and <strong>set it with a sort order after the Sub-Total</strong>, so the discount can be deducted from the total price of the shopping carts of your customers.';
$_['text_total_button']             = 'Install &amp; Enable the ProductBundles Total Extension';

$_['text_older_version']            = 'Bundles From an Older Version Are Detected!';
$_['text_older_version_helper']     = 'Since 4.0, the bundles are saved in a new, better way. Click on the button below if you want to migrate the bundles from your old version of the module. If you wish to start over, just click on the "Save Changes" button.';
$_['text_older_version_button']     = 'Click Here to Migrate The Old Bundles';
    
// Licensing
$_['text_your_license']             = 'Your License';
$_['text_please_enter_the_code']    = 'Please enter your product purchase license code:';
$_['text_activate_license']         = 'Activate License';
$_['text_not_having_a_license']     = 'Not having a code? Get it from here.';
$_['text_license_holder']           = 'License Holder';
$_['text_registered_domains']       = 'Registered domains';
$_['text_expires_on']               = 'License Expires on';
$_['text_valid_license']            = 'VALID LICENSE';
$_['text_manage']                   = 'manage';
$_['text_get_support']              = 'Get Support';
$_['text_community']                = 'Community';
$_['text_ask_our_community']        = 'We have a big community. You are free to ask it about your issue on the forum.';
$_['text_browse_forums']            = 'Browse forums';
$_['text_tickets']                  = 'Tickets';
$_['text_open_a_ticket']            = 'Want to comminicate one-to-one with our tech people? Then open a support ticket.';
$_['text_open_ticket_for_real']     = 'Open a ticket';
$_['text_pre_sale']                 = 'Pre-sale';
$_['text_pre_sale_text']            = 'Have a brilliant idea for your webstore? Our team of developers can make it real.';
$_['text_bump_the_sales']           = 'Bump the sales';

$_['text_control_panel']            = 'Control Panel';
$_['text_bundles']                  = 'Bundles';
$_['text_settings']                 = 'Settings';
$_['text_bundle_widget']            = 'Widget';
$_['text_bundle_view']              = 'View Page';
$_['text_bundle_listing']           = 'Listing Page';
$_['text_support']                  = 'Support';

$_['text_bundle_list']              = 'Bundle List';
$_['text_module_status']            = 'Module Status:';
$_['text_module_status_helper']     = 'Enable or disable the module.';
$_['text_multiple_bundles']         = 'Behavior when a bundle is added more than once in the cart';
$_['text_multiple_bundles_helper']  = 'If the same bundle is added to the cart more than once, you can choose whether to apply the bundle discount only once or as many times you have the bundle in your cart';
$_['text_add_the_discount_once']    = 'Add the Discount Once';
$_['text_add_discount_every_time']  = 'Add the Discount Every Time';
$_['text_regard_taxes_on_discount'] = 'Regard Taxes While Calculating the Bundle Price';
$_['text_regard_taxes_helper']      = 'If this option is disabled, the bundle discount in the front-end part of your store will be calculated <strong>after</strong> the taxes (if any) are applied in the shopping cart. If the option is set to enabled, the bundle discount will be calculated <strong>before</strong> the taxes are applied in the cart.';

$_['text_picture_width_height']     = 'Picture Width &amp; Height:';
$_['text_in_pixels_helper']         = 'The numbers here are in pixels.';
$_['text_width']                    = 'Width:';
$_['text_height']                   = 'Height:';
$_['text_px']                       = 'px';
$_['text_custom_css']               = 'Custom CSS:';
$_['text_custom_css_helper']        = 'Place your custom CSS here.';

$_['text_widget_title']             = 'Title:';
$_['text_widget_title_helper']      = 'This will be the title of the widget.';
$_['text_check_this_bundle']        = 'Check out this bundle:';
$_['text_wrap_in_widget']           = 'Wrap in Widget:';
$_['text_wrap_in_widget_helper']    = 'Wraps the bundle info in a container. The style may vary depending on the design on the template.';
$_['text_bundles_in_widget']        = 'Bundles in Widget:';
$_['text_bundles_in_widget_helper'] = 'Define how many bundles to be shown in the widget.';
$_['text_display_type']             = 'Display Type:';
$_['text_display_type_helper']      = 'Choose how the bundles should be displayed.<br /><br />- Default: Shows the bundles chronologically.<br />- Random: Shows the bundles randomly.';
$_['text_default']                  = 'Default';
$_['text_random']                   = 'Random';
$_['text_show_random_bundles']      = 'Show Random Bundles:';
$_['text_show_random_bundles_helper'] = 'If enabled, ProductBundles will display existing bundles randomly on the <strong>layouts</strong> where the module is enabled. Applies only on the pages with no associated bundles.';

$_['text_listing_url']              = 'Listing URL:';
$_['text_listing_url_helper']       = 'This is the URL of the listing page, which shows all bundles.';
$_['text_add_listing_to_main_menu'] = 'Add Link to The Listing in The Main Menu:';
$_['text_add_listing_to_main_menu_helper'] = 'It may require some additional work to make it work on a highly customized themes.';
$_['text_menu_link_title']          = 'Menu Link Title:';
$_['text_menu_link_title_helper']   = 'Specify the title, which will be displayed in the main menu of your store. The option supports multi-lingual websites.';
$_['text_menu_link_sort_order']     = 'Menu Link Sort Order:';
$_['text_menu_link_sort_order_helper'] = 'Specify the sort order for the link in the main menu.';
$_['text_bundles_per_page']         = 'Bundles Per Page:';
$_['text_bundles_per_page_helper']  = 'Define how many bundles to be shown on first page.';
$_['text_seo_options']              = 'SEO Options:';
$_['text_seo_options_helper1']      = 'Here you will find SEO options which (if used correctly) will boost your SEO rankings.';
$_['text_seo_options_helper2']      = '<strong>Important</strong>: The SEO title will also act as a heading title to the listing page.';
$_['text_page_title']               = 'Page Title:';
$_['text_meta_description']         = 'Meta Description:';
$_['text_meta_keywords']            = 'Meta Keywords:';
$_['text_seo_slug']                 = 'SEO Slug:';
$_['text_seo_slug_helper']          = 'Type only the slug that you want to use for SEO URL. For example, you want the bundle listing to be accessible from <em>%site%<strong>bundles</strong></em>, type only <strong>bundles</strong> in the field above.';

$_['text_save_changes']             = 'Save Changes';
$_['text_cancel']                   = 'Cancel';
$_['text_confirm']                  = 'Confirm';
$_['text_delete']                   = 'Delete Bundle?';
$_['text_delete_a_confirmation']    = 'Are you sure that you wish to delete the bundle? The action is irreversible and you have to recreate it again if you decide to bring it back?';
$_['text_success_delete']           = 'The bundle was deleted successfully!';

$_['error_missing_data']            = 'Missing data!';
$_['text_error_missing_data']       = 'It seems that your bundle is missing some of the required fields in order to be saved. <br /><br />Please make sure that the bundle has a name and the bundle has at least two products in it!';
$_['error_unexpected']              = 'There was an unexpected error. Please, try again later or contact the developers of the product.';
$_['text_no_bundles']               = 'Currently there are no bundles here. Why don\'t you create some?';
$_['text_prices_no_taxes']          = '<strong>Important:</strong> Keep in mind that due to the fact that you are in the admin panel, the prices displayed here are without applied taxes/VAT (if used) and we are using the default currency. The prices in the front-end part of your store will be adjusted accordingly to the selected currency by the customer and to the taxes/VAT (if used).';
$_['text_edit_bundle']              = 'Edit Bundle';

$_['text_enabled']                  = 'Enabled';
$_['text_disabled']                 = 'Disabled';
$_['text_price']                    = 'Price';
$_['text_products']                 = 'Products';
$_['text_categories']               = 'Categories';
$_['text_action']                   = 'Action';
$_['text_name']                     = 'Name';
$_['text_status']                   = 'Status';
$_['text_discount']                 = 'Discount';
$_['text_date_added']               = 'Date Added';
$_['text_date_modified']            = 'Date Modified';
$_['text_date_available']           = 'Date Available';

$_['text_discount_value']           = 'Discount Value';
$_['text_create_bundle']            = 'Create a New Bundle';
$_['text_bundle_name']              = 'Name';
$_['text_bundle_description']       = 'Description';
$_['text_bundle_status']            = 'Status';
$_['text_bundle_sort_order']        = 'Sort Order';
$_['text_bundle_seo_keyword']       = 'SEO Keyword';
$_['text_bundled_products']         = 'Add Product';
$_['text_bundle_show_on']           = 'Show the Bundle on';
$_['text_bundle_discount_options']  = 'Discount Options';

$_['text_bundle_name_helper']		= 'This will display as a name of your bundle. The field is multi-lingual';
$_['text_bundle_seo_keyword_helper'] = 'If set this will be your bundle URL';
$_['text_bundle_description_helper'] = 'Add bundle description';

$_['text_bundled_products_helper'] 	= 'Add products to your bundle';
$_['text_bundle_show_on_helper'] 	= 'Choose where which products and categories you would like to display this bundle to.';
$_['text_bundle_discount_options_helper'] = 'Set fixed type or percentage discount. Select a discount value.';
$_['text_bundle_sort_order_helper']	= 'Set bundle sort order. Smaller numbers account to bundle showing first.';
$_['text_date_available_helper']	= 'Date Available helps you to add a bundle for a future date. Format is YYYY-MM-DATE';

$_['text_product_add_label']        = 'Choose Product';
$_['text_quantity_label']           = 'Quantity';
$_['text_category_add_label']       = 'Choose Category';

$_['text_btn_save_bundle']          = 'Save Bundle';
$_['text_btn_cancel']               = 'Discard Changes';

$_['text_total_product_price']      = 'Total Price of the Products in the Bundle:';
$_['text_price_with_discount']      = 'Price with Discount:';
$_['text_discount_type']            = 'Discount Type';
$_['text_discount_value']           = 'Discount Value';
$_['text_fixed_price']              = 'Fixed Price';
$_['text_percentage']               = 'Percentage';

$_['text_bundle_description']       = 'Show the Description of the Bundle:';
$_['text_bundle_description_helper'] = 'Select "Enabled" if you want to see the description of the bundle (if it has descripton) in the front-end part of your store.';

$_['text_all']                      = '  ';
$_['text_filter']                   = 'Filter';