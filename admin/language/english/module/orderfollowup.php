<?php
$_['heading_title']               = 'OrderFollowUp';
$_['error_permission']            = 'Warning: You do not have permission to modify module OrderFollowUp!';
$_['text_success']                = 'Success: You have modified module OrderFollowUp!';
$_['text_enabled']                = 'Enabled';
$_['text_disabled']               = 'Disabled';
$_['button_cancel']				  = 'Cancel';
$_['save_changes']				  = 'Save changes';
$_['text_default']				  = 'Default';
$_['text_module']				  = 'Module';
?>