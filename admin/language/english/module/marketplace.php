<?php
// Heading
$_['heading_title1']    	  			= 'MarketPlace';
$_['heading_title']    	  				= '<b>MarketPlace</b>';

// Text
$_['text_module']      	  				= 'Modules';
$_['text_success']        				= 'Success You have modified module Marketplace !';
$_['text_form']   		  				= 'Edit Marketplace';
$_['text_divide_shipping']   		  	= ' Warning: As you have checked option to divide shipping cost to among seller then shipping amount will be equally divided to all sellers whom products are in order';

//Tabs
$_['tab_general']    					= 'General';
$_['tab_seo']   	 					= 'SEF Urls Settings';
$_['tab_commission']  					= 'Commission';
$_['tab_product']   					= 'Product';
$_['tab_order']   					    = 'Order';
$_['tab_seo']  							= 'SEO Settings ';
$_['tab_sell']  						= 'Sell';
$_['tab_mpseo']    						= 'Marketplace Links';
$_['tab_defaultseo']    				= 'Create SEF Links';
$_['tab_productseo']    				= 'Product SEF Settings';
$_['tab_profile']    					= 'Profile Settings';
$_['tab_tab']    						= 'Sell Tabs';
$_['tab_mod_config']    				= 'Catalog Module configuration';
$_['tab_mod_config_account']    		= 'Account';
$_['tab_mod_config_product']    		= 'Product';
$_['tab_mail']    						= 'Mail';
$_['tab_sms']               = 'SMS';
 //SMS tab
              $_['text_info_sms']          = 'Under this tab you can set SMS for different conditions which will occur in Marketplace.';
              $_['entry_sms_keywords']     = 'SMS Keywords';
              $_['entry_sms_partner_request']      = 'Request for Partnership';
              $_['entry_sms_partner_request_info']   = 'Select SMS, which will send to customer after customer apply for Partnership.';
              $_['entry_sms_partner_admin']      = 'SMS to Admin after Customer Request';
              $_['entry_sms_product_request']      = 'Product add';
              $_['entry_sms_product_request_info']   = 'Select SMS, which will send to seller';
              $_['entry_sms_product_admin']      = 'SMS to Admin after Product Add';
              $_['entry_sms_transaction']        = 'Transaction Add';
              $_['entry_sms_transaction_info']     = 'Select SMS, which will send to seller after transaction will add.';
              $_['entry_sms_order']          = 'Order SMS';
              $_['entry_sms_order_info']       = 'Select SMS, which will send to Seller after customer will buy his products with order details.';
              $_['entry_sms_cutomer_to_seller']    = 'Customer Contact Seller SMS';
              $_['entry_sms_seller_to_admin']      = 'Seller Contact Admin SMS';
              $_['entry_sms_edit_product_seller']    = 'Edit product SMS to seller';
              $_['entry_sms_edit_product_sellerinfo']    = 'SMS will be sent to seller when seller will edit any product and \'disapprove on edit\' is enabled';
              $_['entry_sms_edit_product_admin']   = 'Edit product SMS to admin';
              $_['entry_sms_edit_product_admininfo']   = 'SMS will be sent to admin when seller will edit any product and \'disapprove on edit\' is enabled';
              $_['entry_sms_partner_approve']      = 'SMS After Approve Partner (Manually)';
              $_['entry_sms_product_approve']      = 'SMS After Approve Product (Manually)';
//Entry
$_['entry_becomepartner']   			= 'Partner/Vendor at Registration page ';
$_['entry_becomepartnerinfo']   		= 'Using this option, you can visible Become Partner / Vendor option for Customer at registration page .';

$_['entry_sellerProductDelete']   	    = 'Product Delete after Seller Delete';
$_['entry_sellerProductDeleteInfo']     = 'If enabled then all the product\'s will be deleted after seller is deleted otherwise all the products will be the admin product\'s of that seller';

$_['entry_sellerProductVisible']   	    = 'Product Shows after Seller Disable';
$_['entry_sellerProductVisibleInfo']    = 'If this field is enabled, all the product\'s of the disabled seller will show at the front end otherwise products will not show at the front end';

$_['entry_sellerBuyProduct']   	        = 'Seller Can Buy their Product';
$_['entry_sellerBuyProductInfo']        = 'If enabled seller can buy his own products';

$_['entry_productapprov']   			= 'Approve Product Automatic  ';
$_['entry_productapprovinfo']   		= 'Using this option, you don\'t have to approve Products manually, products will be approved automatically whenever seller adds a new product.';

$_['entry_partnerapprov']   			= 'Approve Partner Automatic  ';
$_['entry_partnerapprovinfo']   		= 'Using this option, you don\'t have to approve partners manually, customers will become partners automatically when customers apply for become a partner.';

$_['wkentry_mailtoseller']  			= 'Mail to Seller/Partner After Order ';
$_['entry_mailtosellerinfo']  			= 'Using this, mail will send to Partners as any customer will buy their Products.';

$_['entry_customer_delete_product']  	= 'Partner Delete Product From Store ';
$_['entry_customer_delete_productinfo'] = 'After enabling this when any seller delete product from his/her store then Product will automatically remove from your store too.';

$_['wkentry_seller_order_status']  		= 'Partner can change Order Status ';
$_['wkentry_seller_order_statusinfo']  	= 'If you enable this then Partner can change status of Order related to his/her products.';

$_['wkentry_seller_order_status_notify_admin'] = 'Notify Admin when Order Status Is Changed By Seller';
$_['wkentry_seller_order_status_notify_admin_info'] = 'If you enable then admin will be notified when seller changes order status';

$_['wkentry_seller_order_status_sequence']  		= 'Order status sequence';
$_['wkentry_seller_order_status_sequenceinfo']  	= 'Set order status, only in which sequence seller can change order status.';

$_['wkentry_seller_available_order_status']  		= 'Order status for sellers';
$_['wkentry_seller_available_order_statusinfo']  	= 'Select order status for sellers that seller can change for their orders.';

$_['entry_admin_mail']  				= 'Admin/Default Mail ';
$_['entry_admin_mailinfo']  			= 'Admin/Default mail will use to send mails if you want Privacy for your store.';

$_['entry_customer_contact_seller']  	= 'Customer can contact Sellers ';
$_['entry_customer_contact_sellerinfo'] = 'This option will display contact option to customer.';


$_['entry_hide_seller_email']  			= 'Hide Seller/Customer Email ';
$_['entry_hide_seller_emailinfo']  		= 'After enable this option Mail will not send by customer/seller email, yours default will use.This is useful to manage your store privacy.';

$_['entry_mail_admin_customer_contact_seller']  	= 'Mail to Admin if Customer Contact Seller ';
$_['entry_mail_admin_customer_contact_sellerinfo']  = 'If customer mail to seller/partner then same mail will send to Admin.';

$_['entry_commission_worked']  			= 'Commission Worked on All Category ';
$_['entry_commission_workedinfo']  		= 'If this checkbox is checked then commission will be sum of all catagory commission related to particular product.';

$_['entry_commission']    				= 'Partner Fixed Commission (Default) ';
$_['entry_cat_commission']  			= 'Category Commission ';

$_['entry_priority_commission']  		= 'Select Commission Priority ';
$_['entry_priority_commissioninfo']  	= 'You can select commission priority. This will determine in what priority each commission will work.';

$_['entry_product_add_email']  			= 'Admin Recieve Email After Product Add ';
$_['entry_product_add_emailinfo']  		= 'If partner add any product then Admin will get notification email.';

$_['wkentry_pimagesize']      			= 'Max Image size for seller to upload ';
$_['wkentry_pimagesizeinfo']      		= 'Using this you can limit image size for seller. (In kbs)';

$_['entry_download_size']  	   			= 'Max Downloadable File Size ';
$_['entry_download_sizeinfo']      		= 'Using this you can limit download size for seller. (In kbs)';

$_['entry_image_ex']  		   			= 'Allowed Image Extensions  ';
$_['entry_image_exinfo']      			= 'Enter your Allowed extensions for Image. ';

$_['entry_download_ex']  	   			= 'Allowed Download Extensions ';
$_['entry_download_exinfo']      		= 'Enter your Allowed extensions for Download. ';

$_['entry_no_of_images']  	   			= 'Allowed No. of Images ';
$_['entry_no_of_download']     			= 'Allowed No. of Downloads ';

$_['entry_alowed_product_columns']  	= 'Allowed Product Fields ';
$_['entry_alowed_product_columnsinfo']  = 'Select Product fields for partner to add/edit any product. We suggest you to enable at least Model, Price and Quantity. ';

$_['entry_alowed_product_tabs']	    	= 'Allowed Product Tabs ';
$_['entry_alowed_product_tabsinfo']  	= 'Select tabs which ones you want to enable for partner.';

$_['entry_product_reapprove']	    	= 'Disapprove on edit';
$_['entry_product_reapproveinfo']  	= 'If enable and then seller edit his/her product and product auto approve is not enabled then product will be disabled from store';

$_['entry_commission_add']  			= 'Category Commission Based on ';
$_['entry_commission_addinfo']  		= 'Select Categories for Category Commission.';

$_['entry_alowed_profile_columns']  	= 'Allowed Profile Fields ';
$_['entry_alowed_profile_columnsinfo']  = 'Select Profile fields for partner to add/edit any his/her profile and only allowed fields will display to customer. ';
$_['entry_seller_email']				= 'Customer can see seller\'s Email ID';
$_['entry_seller_telephone']			= 'Customer can see seller\'s Telephone number';

$_['entry_seller_shipping_method']  	= 'Shipping Method For Seller';
$_['entry_seller_shipping_methodinfo']  = 'Select shipping method for which shipping amount will be transfered to seller\'s account';

$_['entry_complete_order_status']  	= 'Complete Order Status';
$_['entry_complete_order_statusinfo']  = 'Set order status that will be considered as completion of product and it will be used to do transaction';

$_['entry_divide_shipping_cost']  	= 'Divide Shipping Cost';
$_['entry_divide_shipping_costinfo']  = 'if checked then shipping cost will be divided among sellers.';

$_['entry_seo_seller_details']  	= 'Seller\'s Details';
$_['entry_seo_seller_detailsinfo']  = 'Select which you want to display with product name';

$_['entry_seo_display_format']  	= 'Display Format';
$_['entry_seo_display_formatinfo']  = 'Select how sef should be displayed';

$_['entry_seo_default_name']  	= 'Default Name';
$_['entry_seo_default_nameinfo']  = 'This will be used as seller\'s info and for admin products if seller\'s info does not exist as per the above selection and also for admin products if seller\'s info is required';

$_['entry_seo_default_name_product']  	= 'Make Product Name as SEF';
$_['entry_seo_default_name_productinfo']  = 'Product name will be used if seo keyword is not found for product';

$_['entry_seo_add_page_extension']  	= 'Add Extension';
$_['entry_seo_add_page_extensioninfo']  = 'Enter extension if want to show any extension with seo url initial with period(.) like .html and don\'t use .tpl';

$_['entry_default_image']  			= 'Upload default image';
$_['entry_default_imageinfo']  		= 'It will be used as default image if the images are not uploaded by the seller for profile or other values.';
$_['entry_remove']  				= 'Remove';
$_['entry_profile_tab']  				= 'Profile Tab';
$_['entry_store_tab']  				= 'About Store Tab';
$_['entry_collection_tab']  				= 'Collection Tab';
$_['entry_review_tab']  				= 'Review Tab';
$_['entry_product_review_tab']  				= 'Product Review Tab';
$_['entry_customer_seller_profile']  				= 'Customer\'s Seller Profile';
$_['entry_location_tab']  				= 'Location Tab';

$_['entry_selectall']  					= 'Select All';
$_['entry_deselectall']  				= 'Deselect All';
$_['entry_route']  						= 'Select Route';
$_['entry_fixed']  						= 'Fixed';
$_['entry_category']  					= 'Category';
$_['entry_category_child']  			= 'Category Child';

$_['entry_both']  						= 'All';
$_['entry_addmore']    					= 'Add SEF URL';
$_['entry_seo_seller_name']  			= 'Seller Name';
$_['entry_seo_company_name']    		= 'Company Name';
$_['entry_seo_screen_name']    			= 'Screen Name';
$_['entry_only_product']  				= 'Only product';
$_['entry_seller_and_product']    		= 'Seller detail-Product';
$_['entry_product_and_seller']    		= 'Product-Seller detail';

// Module configuration entry

$_['entry_mod_profile']  				= 'My Profile';
$_['entry_mod_dashboard']  				= 'Dashboard';
$_['entry_mod_order']  					= 'Order History';
$_['entry_mod_transaction']  			= 'Transaction';
$_['entry_mod_productlist']  			= 'Product List';
$_['entry_mod_addproduct']  			= 'Add Product';
$_['entry_mod_downloads']  				= 'Downloads';
$_['entry_mod_manageshipping']  		= 'Manage Shipping';
$_['entry_mod_asktoadmin']  			= 'Ask to Admin';

// membership
$_['entry_mod_membership']  			= 'Add MemberShip'; 				
 			

$_['entry_allowed_account_menu']  	= 'Allowed Account Menu';
$_['entry_allowed_account_menuinfo']  = 'Select those options which you want to show at front end Account menu';

$_['entry_account_menu_sequence']  	= 'Account Menu Sequence';
$_['entry_account_menu_sequenceinfo']  = 'Toggle Account Menu Sequence according to the need';

$_['entry_product_name_display']  	= 'Name Display';
$_['entry_product_name_displayinfo']  = 'Set which one should be displayed on module when it is set to product page';

$_['entry_product_show_seller_product']  	= 'Show Seller\'s Products';
$_['entry_product_show_seller_productinfo']  = 'Enable to show seller\'s product on product page with module else disable';

$_['entry_product_image_display']  	= 'Display Image';
$_['entry_product_image_displayinfo']  = 'Set which image should be displayed with module';


//mail tab
$_['entry_mail_keywords']  		= 'Mail Keywords';
$_['entry_mail_product_approve']  		= 'Please enter keyword that you want to enter in mail ending with comma(,)';
$_['entry_mail_product_approve_info']   = 'Select mail, which will send to seller after admin approve the seller product.';
$_['entry_mail_partner_request']  		= 'Request for Partnership';
$_['entry_mail_partner_request_info']  	= 'Select mail, which will send to customer after customer apply for Partnership.';
$_['entry_mail_partner_admin']  		= 'Mail to Admin after Customer Request';
$_['entry_mail_partner_admin_info']     = 'Select mail, which will send to admin after customer apply for Partnership.';
$_['entry_mail_product_request']  		= 'Product add';
$_['entry_mail_product_request_info']  	= 'Select Mail, which will send to seller';
$_['entry_mail_product_admin']  		= 'Mail to Admin after Product Add';
$_['entry_mail_product_admin_info']     = 'Select mail, which will send to admin after seller add product.';
$_['entry_mail_transaction']  			= 'Transaction Add';
$_['entry_mail_transaction_info']  		= 'Select Mail, which will send to seller after transaction will add.';
$_['entry_mail_order']  				= 'Order Mail';
$_['entry_mail_order_info']  			= 'Select Mail, which will send to Seller after customer will buy his products with order details.';
$_['entry_mail_cutomer_to_seller']  	= 'Customer Contact Seller Mail';
$_['entry_mail_cutomer_to_seller_info'] = 'Select Mail, which will send to seller after customer contact seller.';
$_['entry_mail_seller_to_admin']  		= 'Seller Contact Admin Mail';
$_['entry_mail_seller_to_admin_info']   = 'Select Mail, which will send to admin after customer contact seller.';
$_['entry_mail_edit_product_seller']  	= 'Edit product mail to seller';
$_['entry_mail_edit_product_sellerinfo']  	= 'Mail will be sent to seller when seller will edit any product and \'disapprove on edit\' is enabled';
$_['entry_mail_edit_product_admin']  	= 'Edit product mail to admin';
$_['entry_mail_edit_product_admininfo']  	= 'Mail will be sent to admin when seller will edit any product and \'disapprove on edit\' is enabled';
$_['entry_mail_partner_approve']  		= 'Mail After Approve Partner (Manually)';
$_['entry_mail_partner_approve_info']   = 'Select mail, which will send to seller after admin approve his partnership manually.';
$_['entry_mail_product_approve']  		= 'Mail After Approve Product (Manually)';

$_['entry_seller_product_store']  		= 'Seller Product Store';
$_['entry_seller_product_store_info']   = 'Select store option for seller to add product';
$_['entry_mulistore']    			    = 'Multi Store';
$_['entry_choosestore']    			    = 'Choose Store';
$_['entry_ownstore']    			    = 'Own store';


$_['entry_mpinfo']    					= 'Please Use SEO keywords, without spaces or other special characters and unique .';
$_['entry_useseo']    					= 'Use SEO keywords for Market-Place  ';
$_['entry_wksell']    					= 'SEO Keyword For Sell Page ';
$_['entry_productlist']   				= 'SEO Keyword For Product List Page ';
$_['entry_profile']    					= 'SEO Keyword For Account Profile Page ';
$_['entry_addproduct']    				= 'SEO Keyword For Add Product Page ';
$_['entry_add_shipping_mod']			= 'SEO Keyword For Shipping Page ';
$_['entry_dashboard']    				= 'SEO Keyword For Dashboard Page ';
$_['entry_orderlist']    				= 'SEO Keyword For Order List Page ';
$_['entry_wkorder_info']    			= 'SEO Keyword For Order Info Page ';
$_['entry_soldlist']    				= 'SEO Keyword For Sold List Page ';
$_['entry_soldinvoice']     			= 'SEO Keyword For Soldinvoice Page ';
$_['entry_editproduct']    				= 'SEO Keyword For Edit Product Page ';
$_['entry_storeprofile']    			= 'SEO Keyword For Store Profile Page ';
$_['entry_collection']    				= 'SEO Keyword For Collection Page ';
$_['entry_feedback']    				= 'SEO Keyword For Feedback Page ';
$_['entry_store']    					= 'SEO Keyword For Store Page ';
$_['entry_store']   					= 'SEO Keyword For Store Page ';
$_['entry_downloads']  					= 'SEO keyword for Download Page ';
$_['entry_transactions']  				= 'SEO keyword for Transaction Page ';
$_['entry_addseomore']  				= 'Add SEO keyword ';
$_['entry_addseomoreinfo'] 				= 'Using this Option you can manually Add SEF URLs for Site URLs. Select URL link and then type it\'s SEF keyword. This Select Option is Displaying all files under your Controller Folder which can or can\'t be direct URL. You can check  URL at Frontend in Address Bar.';

$_['entry_sellinfo']    				= 'Using these options you can manage your Sell Page. You can display information to Customers/Sellers in the form of tabs.';

$_['entry_cancel_order_statusinfo']  = 'Set order status that will be considered as cancel of product and it will be used to do cancel the order product';
$_['entry_cancel_order_status'] = 'Cancel Order Status';
$_['entry_seller_name_cart'] = 'Seller Name In The Cart';
$_['entry_seller_name_cart_info'] = 'If enable then customer can see seller name as the suffix of every seller product';
$_['entry_partner_list_limit'] = 'Seller List Limit';
$_['entry_partner_product_list_limit'] = 'Seller Product List Limit';
$_['entry_partner_list_limit_info'] = 'Set the limit of seller list on the sell page';
$_['entry_partner_product_list_limit_info'] ='Set the limit of seller products on the sell page';
$_['entry_seller_name_cart_info'] = 'If enable then customer can see seller name as the suffix of every seller product';

$_['wkentry_yes']         	 			= 'Yes';
$_['wkentry_no']         	 			= 'No';
$_['wkentry_sellh']      	 			= 'Sell Header:';
$_['wkentry_mailtoseller']   			= 'Mail to Seller/Partner :';
$_['wkentry_sellb']     	 			= 'Sell Button Title:';
$_['wkentry_selld']    		 			= 'Sell Description';
$_['wkentry_show_partner']   			= 'Show Partner\'s';
$_['wkentry_show_products']  			= 'Show Product\'s';
$_['wkentry_add_tab']     	 			= 'Add tab';
$_['entry_text']     	  	 			= 'Add Text';
$_['text_tab_title']	  	 			= 'Enter Tab Title:';
$_['text_status']		  	 			= 'Marketplace Module Status';
$_['text_in_kbs']		  	 			= 'in kb(s)';
$_['text_confirm']       	 			= 'Confirm - Remove Tables and Install New Ones (This is irreversible ) !!';
$_['text_info']     	  	 			= 'If you have Previous version of Marketplace then please <a class="reinstall"> Click Me </a> to Remove Previous Tables and Install New ones.';
$_['text_info_profile'] 	 			= 'Under this tab you can globally enable/disable Sellers Profile details, which will visible to customer .';
$_['text_info_mail']	 	 			= 'Under this tab you can set Mail for different conditions which will occur in Marketplace.';

// Error
$_['error_permission'] 		 			= 'Warning You do not have permission to modify module Marketplace Module !';

// image file upload check error
$_['error_filename']   = 'Warning: Filename must be a between 3 and 255!';
$_['error_folder']     = 'Warning: Folder name must be a between 3 and 255!';
$_['error_exists']     = 'Warning: A file or directory with the same name already exists!';
$_['error_directory']  = 'Warning: Directory does not exist!';
$_['error_filetype']   = 'Warning: Incorrect file type!';
$_['error_upload']     = 'Warning: File could not be uploaded for an unknown reason!';
?>