<?php
// Heading
$_['heading_title']						= 'iProductVideo 3.2.1';

// Text
$_['text_module']         				= 'Modules';
$_['text_success']						= 'Success: You have modified module iProductVideo!';
$_['text_activate']						= 'Activate';
$_['text_not_activated']				= 'iProductVideo is not activated.';
$_['text_click_activate']				= 'Activate iProductVideo';
$_['text_success_activation']			= 'ACTIVATED: You have successfully activated iProductVideo!';
$_['text_content_top']					= 'Content Top';
$_['text_content_bottom']				= 'Content Bottom';
$_['text_column_left']					= 'Column Left';
$_['text_column_right']					= 'Column Right';
$_['text_type_image']					= 'Image';
$_['text_type_text']					= 'Text';
$_['text_max_size']						= 'Max size:';
$_['text_max_size_learn']				= 'Learn how to increase it';
$_['text_top_left']						= 'Top left';
$_['text_top_right']					= 'Top right';
$_['text_center']						= 'Center';
$_['text_bottom_left']					= 'Bottom left';
$_['text_bottom_right']					= 'Bottom right';
$_['text_default']						= 'Default';

// Entry
$_['entry_code']						= 'iProductVideo status:<br /><span class="help">Enable or disable iProductVideo for the selected store.</span>';
$_['entry_video_limit_products']		= 'Make the video available for these products:';
$_['entry_all_products']				= 'All products';
$_['entry_following_products']			= 'The following products';

// Error
$_['error_permission']					= 'Warning: You do not have permission to modify module iProductVideo!';
$_['error_type_empty']					= 'Warning: You need to choose a label image type!';
$_['error_upload_error']				= 'Upload error: ';
$_['error_unable_upload']				= 'Warning: Failed to copy uploaded file from temp folder to server!';
$_['error_invalid_file']				= 'Warning: The uploaded file is either too large or is of an unsupported file format!';
$_['error_invalid_position']			= 'Warning: Please choose a valid position!';
$_['error_invalid_opacity']				= 'Warning: Opacity must be a number between 0 and 100!';
$_['error_text_empty']					= 'Warning: Label text value must be set!';
$_['error_text_color']					= 'Warning: Provided color is invalid!';
$_['error_image_empty']					= 'Warning: You have to upload an image!';
$_['error_invalid_limit_size_type']		= 'Warning: Please choose a valid limit size type!';
$_['error_invalid_limit_size_width']	= 'Warning: Please set a numeric and positive size width!';
$_['error_invalid_limit_size_height']	= 'Warning: Please set a numeric and positive size height!';
$_['error_invalid_font_size']			= 'Warning: Invalid font size! Must be between 8 and 100.'

?>