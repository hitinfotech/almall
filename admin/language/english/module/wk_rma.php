<?php
################################################################################################
#  Marketplace RMA for Opencart 2.x.x.x From webkul http://webkul.com  	  	       #
################################################################################################

// Heading Goes here:
$_['heading_title']      	  = 'Marketplace RMA System ';

$_['entry_status']      	  = 'Status ';

// Text
$_['tab_general']     	 	  = 'RMA Settings';
$_['tab_labels']     	 	  = 'Add Shipping Label';

$_['text_labels']     	 	  = 'Upload Label(s)';

$_['text_module']     	 	  = 'Modules';
$_['text_edit']     	 	  = 'Edit RMA System';
$_['text_success']    	 	  = 'Success: You have modified module RMA System !';
$_['text_rma_return_add']	  = 'Return Address ';
$_['text_rma_return_add_info']= 'After send Shipping lable to customer this will be your return address for product.';

$_['text_rma_time']	  		  = 'Add RMA Time ';
$_['text_rma_time_info']	  = 'You can add Time limit for customer, only less than these days customer can generate RMA for any order. Use 0 or blank this field for unlimited time.';
$_['text_rma_enable']	  	  = 'RMA Status ';
$_['text_rma_info_policy']	  = 'RMA Policy ';
$_['text_rma_info_policy_info'] = 'Using this you can add policy from Informations which will display to customer at time of Add RMA.';
$_['text_order_status']	 	  = 'Order Status for RMA';
$_['text_order_status_info']  = 'Customer can place RMA only for those status of order which is selected here.';
$_['text_image_extenstion']   = 'Allowed Image Extention';
$_['text_file_extenstion']    = 'Allowed File Extention';
$_['text_extenstion_size']    = 'Allowed File/Image Size';
$_['text_size_info']    	  = 'In Kb';
$_['text_extenstion_holder']  = 'jpg,JPG,png,PNG or Use * for All';
$_['text_to']                  = 'To';
$_['text_ship_to']             = 'Ship To (if different address)';


$_['text_rma_add_status']  	  = 'Manage RMA Status';
$_['text_rma_add_reasons'] 	  = 'Manage RMA Reasons';
$_['text_rma_manage']	  	  = 'Manage RMA(s)';

$_['text_lable_image']	  	  = 'Image';
$_['text_lable_name']	  	  = 'Name';
$_['entry_mail_seller']	  	  = 'RMA Mail to seller';
$_['entry_mail_customer']	  = 'RMA Mail to customer';
$_['entry_mail_admin']	  	  = 'RMA Mail to admin';
$_['entry_mail_keywords']	  = 'RMA Mail Keywords';
$_['mail_keywords_info']	  = 'This is list of the keywords which you can use in your RMA mail template';
$_['entry_mail_seller_info']  = 'Select the RMA mail template which you want to send to seller on product return';
$_['entry_mail_customer_info']= 'Select the RMA mail template which you want to send to customer on product return';
$_['entry_mail_admin_info']	  = 'Select the RMA mail template which you want to send to admin on product return';
$_['text_info_mail']	  	  = 'Here, you can set Mail for different conditions which will occur in RMA. For this, the mail template must be created in Marketplace>>Mail';

$_['entry_mod_mprma'] = 'RMA';

$_['text_image_success']  	  = 'Success: %d Label(s) %s has been uploaded successfully.';
$_['text_success_delete']	  = 'Success: Shipping label(s) has been deleted successfully.';

// Error
$_['error_permission']  	  = 'Warning: You do not have permission to modify module RMA System ';
?>
