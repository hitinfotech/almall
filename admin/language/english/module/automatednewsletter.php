<?php
$_['heading_title']               		= 'AutomatedNewsletter';
// Text
$_['text_module']                 		= 'Modules';
$_['text_success']                		= 'Success: You have modified module AutomatedNewsletter!';
$_['subject_text']                		= 'Subject';
$_['text_yes']                   		= 'Yes';
$_['text_no']                     		= 'No'; 
$_['text_enabled']                		= 'Enabled';
$_['text_disabled']              	 	= 'Disabled';
$_['newsletter_information_text'] 		= 'Newsletter ';    
$_['newsletter_information_text_help'] 	= 'Here, you can configure the content of the newsletter and promote the products by adding the templates in the newsletter email.<p>Use the following codes:<br/><br/><b>{firstname}</b> - first name of the customer<br/><b>{lastname}</b> - last name of the customer<br/><b>{special_list}</b> - list of specials for next days<br/><b>{bestseller_list}</b> - list of bestseller products<br/><b>{new_product_list}</b> - list with the new products<br/><b>{featured_list}</b> - list with the featured products</br><b>{selected_products}</b> - list with the selected products</br>';       
$_['new_products_text'] 				= 'New products';
$_['text_newsletter']     				= 'All newsletter subscribers';
$_['text_customer_all']    				= 'All customers';
$_['text_customer_group']  				= 'Customer group';
$_['text_customer']        				= 'Specific customers...';
$_['text_affiliate_all']   				= 'All affiliates';
$_['text_affiliate']       				= 'Specific affiliates...';
$_['text_product']         				= 'Customers bought specific products';
$_['text_products']						= 'products';
$_['text_days']							= 'days';
$_['text_no_results']					= 'No results';
$_['text_limit']		    			= 'Limit to:';
$_['text_default'] 						= 'Default';
$_['text_close'] 						= 'Close';


$_['text_sending']                		= 'Sending newsletter. Please wait..';
$_['text_preparing']              	 	= 'Preparing to send newsletter...';
$_['text_abort']				 		= 'Abort';    
$_['text_settings'] 					= 'Settings';
$_['text_toggle']     					= 'Toggle Dropdown';
$_['text_list']    						= 'Product lists';
$_['text_sent_newsletter']  			= 'Sent newsletters';
$_['text_tasks']        				= 'Schedule tasks';
$_['text_support']   					= 'Support';
$_['text_save']       					= 'Save';
$_['text_cancel']         				= 'Cancel';
$_['text_from']							= 'From: ';
$_['text_cron']							= 'Other Cron Services';
$_['text_schedule_cron']				= 'Schedule options & cron jobs';
$_['text_back']		    				= 'Back';
$_['text_delete'] 						= 'Delete';
$_['text_send_test']         			= 'Send test email';


//Entry
$_['entry_code']                  		= 'AutomatedNewsletter status:';
$_['entry_code_help']             		= 'Enable or disable the module';
$_['entry_store']          				= 'From Name:';
$_['entry_store_email']     			= 'From E-mail:';
$_['entry_to']             				= 'Recipients:';
$_['entry_customer_group'] 				= 'Customer group:';
$_['entry_customer']      				= 'Customers:';
$_['entry_customer_help']  				= 'Select specific customers. Just start writing in input field';
$_['entry_affiliate']      				= 'Affiliates:';
$_['entry_affiliate_help'] 				= 'Select specific affiliates. Just start writing in input field';
$_['entry_product']        				= 'Insert selected products:';
$_['entry_product_help']   				= 'Here, you can select products that will be send upon your next campaign. Start typing the product\'s name and select it. You can send the created list by clicking on the Newsletter tab and using <b>{selected_products}</b>';
$_['entry_spec_product']   				= 'Choose specific products:';
$_['entry_spec_product_help']   		= 'The customers who have bought the selected products will receive the newsletter';
$_['entry_category']       				= 'Insert from categeory:';
$_['entry_category_help_one'] 			= 'short code: {category_one}:';
$_['entry_category_help_two'] 			= 'short code: {category_two}:';
$_['entry_subject']        				= 'Subject:';
$_['entry_message']       		 		= 'Message:';
$_['entry_num_emails_per_request']		= 'Emails per AJAX Request:';
$_['entry_num_emails_per_request_help']	= 'Enter number of emails to be sent per AJAX request. The default value is 5.';

$_['entry_newsletter']                  = 'Newsletters';
$_['entry_name']                  		= 'Name';
$_['entry_email']                  		= 'Email';


//Error 
$_['error_permission']            		= 'Warning: You do not have permission to modify module AutomatedNewsletter!';
$_['error_input_form']           		= ' Please check the form for errors!';

//Others
$_['schedule_type']         			= 'Type of schedule';
$_['fixed_dates']         				= 'Fixed dates';
$_['periodic']             				= 'Periodic';
$_['default_subject'] 					=  'Newsletter subject';
$_['cron_select_admin_period']  		= 'Email store owner';
$_['cron_select_admin_period_help']  	= 'Use this option to configure how often to send a list of current birthdays to the store owner';
$_['cron_select_customer_period'] 		= 'Email customer(s)';
$_['cron_select_customer_period_help'] 	= 'Use this option to configure when to send emails to customer(s)';
$_['cron_time'] 						= 'Time: ';
$_['schedule_tasks_status'] 			= "Schedule tasks status:";
$_['best_deals'] 						= "Get bestsellers for last:";
$_['new_products_for_last'] 			= "Get new products for last:";
$_['best_deals_top']   					= 'For the bestsellers list get the first:';
$_['specials_for_next'] 				= 'Get specials';
$_['custom_css'] 						= ' Custom CSS:';
$_['custom_css_help'] 					= 'You can modify the style of the email by adding your CSS in this field';
$_['product_image_size'] 				= 'Product image size:';
$_['product_image_size_help'] 			= 'Configure the size of the product images in the newsletter';

$_['count_of_products_per_row'] 		= 'Count of products per row:';
$_['newsletter_subject'] 				= "Newsletter Subject";
$_['date_sent'] 						= "Date Sent";
$_['language'] 							= "Language";
$_['recepients'] 						= "Recepients";
$_['selected_products_list'] 			= 'Selected products list';
$_['send_now_info']						= ' This will send emails to all selected customers. Each customer will receive email in the language that was last used.';
$_['send_now_button']					= 'Send now';
$_['send_test_mail_button'] 			= 'Send test mail';
$_['admin_notification']				= 'Send BCC to store owner:'; 
$_['admin_notification_help']			= 'Enabling this option will add {email} as BCC recepient.';     

$_['test_cron_button']					= 'Test cron status';
$_['cron_requirements']					= 'Please mind that in order to use scheduled tasks, you must have cron jobs enabled on your server';

$_['select_currency']					= 'Newsletter currency';
$_['select_currency_help']				= 'The currency that will be use for the prices in the newsletter';
$_['text_stock_statuses'] 				= 'Send products with status';
$_['create_new_template']				= 'Create new template';
$_['last_modified']						= 'Last modified';
$_['cron_services']						= '<b>If you wish to use other cron services like <i>easycron.com</i> and <i>setcronjob.com</i>, you can copy and paste the generated link for each template</b>';
$_['cron_services_help']				= 'Note that you have to add a cronjob in order to generate a link';
$_['name'] 								= 'Template Name';
$_['link'] 								= 'Link';
$_['all_manufacturers']    				= 'All manufacturers';
$_['selected_manufacturers'] 			= 'Specific manufacturers';
?>