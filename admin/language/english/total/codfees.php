<?php
// Heading
$_['heading_title']    = 'COD Fees';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Success: You have modified low order fee total!';
$_['text_edit']        = 'Edit COD Fee Total';

// Entry
$_['entry_total']      = 'Order Total';
$_['entry_fee']        = 'Fee';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Help
$_['help_total']       = 'The checkout total the order must reach before this order total is deactivated.';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify COD fee total!';
