<?php
// Heading
$_['heading_title']    = 'RMA Statuses';

// Text
$_['text_success']     = 'Success: You have modified order statuses!';
$_['text_list']        = 'RMA Status List';
$_['text_add']         = 'Add RMA Status';
$_['text_edit']        = 'Edit RMA Status';
$_['text_add']         = 'Added';
$_['text_subtract']    = 'Subtracted';
$_['text_in']          = 'In-Stock';
$_['text_out']         = 'Out-Stock';


// Column
$_['column_name']      = 'RMA Status Name';
$_['column_action']    = 'Action';

// Entry
$_['entry_name']           = 'RMA Status Name';
$_['entry_attention_time'] = 'Attention Time(hours)';
$_['entry_warning_time']   = 'Warning Time(hours)';
$_['entry_quantity']       = 'Order Quantity';
$_['entry_order_status']   = 'New Order Status';
$_['entry_stock']          = 'Order Products';
$_['entry_color']          = 'RMA Color';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify order statues!';
$_['error_name']       = 'RMA Status Name must be between 3 and 32 characters!';
$_['error_default']    = 'Warning: This order status cannot be deleted as it is currently assigned as the default store order status!';
$_['error_download']   = 'Warning: This order status cannot be deleted as it is currently assigned as the default download status!';
$_['error_store']      = 'Warning: This order status cannot be deleted as it is currently assigned to %s stores!';
$_['error_order']      = 'Warning: This order status cannot be deleted as it is currently assigned to %s orders!';