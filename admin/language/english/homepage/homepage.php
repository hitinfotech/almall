<?php
// Heading
$_['heading_title']                      = 'Homepage Setup';

// Text
$_['text_settings']                      = 'Homepage';
$_['text_success']                       = 'Success: You have modified settings!';
$_['text_list']                          = 'Available Countries for Custome Homepage';
$_['text_edit']                          = 'Homepage Settings';

// Columns
$_['column_name']                        = 'Country';
$_['column_language']                    = 'language';
$_['column_page_id']                     = 'Page id';
$_['column_action']                      = 'Edit';

// Tabs
$_['tab_block0']                         = 'Block 1- Homepage Banner';
$_['tab_block1']                         = 'Block 2- Main Upper';
$_['tab_block2']                         = 'Block 3- Selected Products';
$_['tab_block3']                         = 'Block 4- Exclusive';
$_['tab_block4']                         = 'Block 5- Categories';
$_['tab_block5']                         = 'Block 6- Designers';
$_['tab_block6']                         = 'Block 7- Offers';
$_['tab_block7']                         = 'Block 0- Countreis and Langauges';

$_['text_homepage_block0_image1']        = 'Desktop Banner Image (5649 x 855)';
$_['text_homepage_block0_image2']        = 'Mobile Banner Image(2403 x 987)';
$_['text_homepage_block0_url1']          = 'Banner Url';

$_['text_homepage_block1_title1']        = 'Title 1';
$_['text_homepage_block1_button1']       = 'Shop Button';
$_['text_homepage_block1_description1']  = 'Description 1';
$_['text_homepage_block1_url1']          = 'Url 1';
$_['text_homepage_block1_image1']        = 'Image 1';

$_['text_homepage_block1_title2']      = 'Title 2';
$_['text_homepage_block1_button2']      = 'Shop Button';
$_['text_homepage_block1_description2']= 'Description 2';
$_['text_homepage_block1_url2']= 'Url 2';
$_['text_homepage_block1_image2']= 'Image 2';

$_['text_homepage_block1_title3']      = 'Title 3';
$_['text_homepage_block1_button3']      = 'Shop Button';
$_['text_homepage_block1_description3']= 'Description 3';
$_['text_homepage_block1_url3']= 'Url 3';
$_['text_homepage_block1_image3']= 'Image 3';

$_['text_homepage_block1_title4']      = 'Title 4';
$_['text_homepage_block1_button4']      = 'Shop Button';
$_['text_homepage_block1_description4']= 'Description 4';
$_['text_homepage_block1_url4']= 'Url 4';
$_['text_homepage_block1_image4']= 'Image 4';

$_['text_homepage_block1_title5']      = 'Title 5';
$_['text_homepage_block1_button5']      = 'Shop Button';
$_['text_homepage_block1_description5']= 'Description 5';
$_['text_homepage_block1_url5']= 'Url 5';
$_['text_homepage_block1_image5']= 'Image 5';

$_['text_homepage_block3_title']      = 'Title';
$_['text_homepage_block3_description']= 'Description';

$_['text_homepage_block3_title1']      = 'Title 1';
$_['text_homepage_block3_description1']= 'Description 1';
$_['text_homepage_block3_url1']= 'Url 1';
$_['text_homepage_block3_image1']= 'Image 1';

$_['text_homepage_block3_title2']      = 'Title 2';
$_['text_homepage_block3_description2']= 'Description 2';
$_['text_homepage_block3_url2']= 'Url 2';
$_['text_homepage_block3_image2']= 'Image 2';

$_['text_homepage_block3_title3']      = 'Title 3';
$_['text_homepage_block3_description3']= 'Description 3';
$_['text_homepage_block3_url3']= 'Url 3';
$_['text_homepage_block3_image3']= 'Image 3';

$_['text_homepage_block3_title4']      = 'Title 4';
$_['text_homepage_block3_description4']= 'Description 4';
$_['text_homepage_block3_url4']= 'Url 4';
$_['text_homepage_block3_image4']= 'Image 4';

$_['text_homepage_block4_title']      = 'Title';
$_['text_homepage_block4_description']= 'Description';

$_['text_homepage_block4_title1']      = 'Title 1';
$_['text_homepage_block4_description1']= 'Description 1';
$_['text_homepage_block4_url1']= 'Url 1';
$_['text_homepage_block4_image1']= 'Image 1';

$_['text_homepage_block4_title2']      = 'Title 2';
$_['text_homepage_block4_description2']= 'Description 2';
$_['text_homepage_block4_url2']= 'Url 2';
$_['text_homepage_block4_image2']= 'Image 2';

$_['text_homepage_block4_title3']      = 'Title 3';
$_['text_homepage_block4_description3']= 'Description 3';
$_['text_homepage_block4_url3']= 'Url 3';
$_['text_homepage_block4_image3']= 'Image 3';

$_['text_homepage_block4_title4']      = 'Title 4';
$_['text_homepage_block4_description4']= 'Description 4';
$_['text_homepage_block4_url4']= 'Url 4';
$_['text_homepage_block4_image4']= 'Image 4';

$_['text_brand'] = 'Select Brand';
$_['text_brand_name'] = 'Brand title';
$_['text_brand_desc'] = 'Brand title';
$_['text_brand_button'] = 'Button text';
$_['text_homepage_block5_image1']= 'Image 1';
$_['text_homepage_block5_image2']= 'Image 2';
$_['text_homepage_block5_image3']= 'Image 3';

// Help
$_['help_comment']                     = 'This field is for any special notes you would like to tell the customer i.e. Store does not accept cheques.';

// Error
$_['error_warning']                    = 'Warning: Please check the form carefully for errors!';
$_['error_permission']                 = 'Warning: You do not have permission to modify settings!';
$_['error_field_reuired']              = ' %s field required';
$_['error_image_thumb']                = 'Product Image Thumb Size dimensions required!';

$_['entry_related']                   = 'Related products';
$_['entry_offers']                    = 'Offers Product ';
