<?php
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

// Heading
$_['heading_title'] = 'Newsletter Status';

// Text
$_['text_default'] = 'Default';
$_['text_view'] = 'View';
$_['text_newsletter'] = 'All Newsletter Subscribers';
$_['text_customer_all'] = 'All Customers';
$_['text_customer_group'] = 'Customer Group';
$_['text_customer'] = 'Customers';
$_['text_affiliate_all'] = 'All Affiliates';
$_['text_affiliate'] = 'Affiliates';
$_['text_product'] = 'Products';
$_['text_marketing'] = 'All Marketing Subscribers';
$_['text_marketing_all'] = 'All Marketing';
$_['text_subscriber_all'] = 'All Newsletter Subscribers & Marketing Subscribers';
$_['text_all'] = 'All Customers & Marketing';
$_['text_success'] = 'Success: You have deleted selected items!';
$_['text_status_newsletters'] = 'Status Newsletters';
$_['text_pause_success'] = " Success Campaign Paused";
$_['text_continue_success'] = " Success Campaign Resumed";
$_['please_wait_to_finish_fetch'] = "Please Wait To Finish Fetch";

$_['text_pause_fail'] = "Cant Pause Finished Campaign Or In Fetching";
// Column 
$_['column_actions'] = 'Action';
$_['column_subject'] = 'Subject';
$_['column_to'] = 'To';
$_['column_date'] = 'Date';
$_['column_store'] = 'Store';

// Button
$_['button_filter'] = 'Filter';
$_['button_delete'] = 'Delete';
$_['button_pause'] = 'Pause';
$_['button_continue'] = 'Continue';
$_['button_edit'] = 'Edit';

$_['ready_to_share_id'] = 'ID';
$_['is_done'] = 'Status';
$_['chunks'] = 'Chunks';
$_['total'] = 'Total';
$_['fail'] = 'Fail';
$_['success'] = 'Success';
$_['subject'] = 'Subject';
$_['is_done_1'] = "Done";
$_['is_done_0'] = "In Progress";