<?php
// Heading
$_['heading_title']    = 'Flat Rate - SayMall';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified flat rate shipping!';
$_['text_edit']        = 'Edit Flat Rate Shipping';

// Entry
$_['entry_cost']            = 'Single Country Cost';
$_['entry_cost_mix']        = 'Mix Country Cost';
$_['entry_zero_cost']       = 'Limit 0 Cost Single';
$_['entry_zero_cost_mix']   = 'Limit 0 Cost Mix';
$_['entry_tax_class']       = 'Tax Class';
$_['entry_geo_zone']        = 'Geo Zone';
$_['entry_status']          = 'Status';
$_['entry_sort_order']      = 'Sort Order';
$_['between_0_100']         = 'shipping fees between  00 - 100 ';
$_['between_101_200']       = 'shipping fees between 100 - 200 ';
$_['between_201_300']       = 'shipping fees between 200 - 300 ';



// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Say-Mall flat rate shipping!';