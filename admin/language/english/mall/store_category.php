<?php
// Heading
$_['heading_title']          = 'Stores Categories';

// Text
$_['text_success']           = 'Success: You have modified Main Store!';
$_['text_list']              = 'Main Store List';
$_['text_add']               = 'Add Main Store';
$_['text_edit']              = 'Edit Main Store';
$_['text_default']           = 'Default';

// Column
$_['column_name']            = 'Store Category Name';
$_['column_parent_category'] = 'Parent Category';
$_['column_sort_order']      = 'Sort Order';
$_['column_creat_date']      = 'Create Date';
$_['column_update_date']     = 'Update Date';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Store Category Name';
$_['entry_description']      = 'Description';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keywords']    = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_meta_tags']        = 'Meta Tag';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_status']           = 'Status';
$_['entry_parent_id']        = 'Parent';
$_['entry_group_id']       	 = 'Group';


// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the keyword is globally unique.';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent categories.';
$_['help_column']            = 'Number of columns to use for the bottom 3 categories. Only works for the top parent categories.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Main Store Name must be between 2 and 255 characters!';
$_['error_location']         = 'Location must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO keyword already in use!';
$_['error_meta_title']         = 'meta_title must be Filled!';
$_['error_meta_keywords']         = 'meta_keywords must be Filled!';
$_['error_meta_tag']         = 'meta_tags must be Filled!';
$_['error_parent_id']        = 'Parent must be choosed!';
$_['error_group_id']       	 = 'Group must be choosed!';