<?php
// Heading
$_['heading_title']          = 'Main Groups';

// Text
$_['text_success']           = 'Success: You have modified Main Group!';
$_['text_list']              = 'Main Group List';
$_['text_add']               = 'Add Main Group';
$_['text_edit']              = 'Edit Main Group';
$_['text_default']           = 'Default';

// Column
$_['column_name']            = 'Main Group Name';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Action';
$_['column_user_id']         = 'User Added Name';
$_['column_last_mod_id']     = 'Last Update Name';

// Entry
$_['entry_name']             = 'Main Group Name';
$_['entry_description']      = 'Description';
$_['entry_location']      	 = 'Location';
$_['entry_open'] 	     	 = 'Open';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_city_id']          = 'City';
$_['entry_country_id']       = 'Country';
$_['entry_image']            = 'Image';
$_['entry_phone']            = 'Phone';
$_['entry_fax'] 	         = 'Fax';
$_['entry_website']          = 'Website';
$_['entry_social_jeeran']    = 'Social Jeeran';
$_['entry_social_fb'] 	     = 'Social Facebook';
$_['entry_social_tw']   	 = 'Social Twitter';
$_['entry_template']   	 	 = 'Template';
$_['entry_active']   	 	 = 'Active';
$_['entry_column']           = 'Columns';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_status']           = 'Status';

// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the keyword is globally unique.';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent categories.';
$_['help_column']            = 'Number of columns to use for the bottom 3 categories. Only works for the top parent categories.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Main Group Name must be between 2 and 255 characters!';
$_['error_location']         = 'Location must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO keyword already in use!';