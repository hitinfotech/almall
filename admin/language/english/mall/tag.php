<?php
// Heading
$_['heading_title']          = 'Tags';

// Text
$_['text_success']           = 'Success: You have modified Mall!';
$_['text_tag']               = 'Tags';
$_['text_list']              = 'Tag List';
$_['text_add']               = 'Add Tag';
$_['text_edit']              = 'Edit Tag';
$_['text_default']           = 'Default';

// Column
$_['column_name']            = 'Tag Title';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Action';
$_['column_products_count']  = 'Products Count';
$_['column_city']            = 'City';
$_['column_image']           = 'Image';
$_['column_status']          = 'Status';
$_['column_date_added']      = 'Date added';
$_['column_date_modified']   = 'Date modified';

// Entry
$_['entry_name']             = 'Tag Title';
$_['entry_description']      = 'Tag description';
$_['entry_location']         = 'Tag location';
$_['entry_home']             = 'Home';
$_['entry_author']           = 'Author';

$_['entry_landing']          = 'Landing';
$_['entry_approved']         = 'Approved';
$_['entry_meta_tag'] 	     = 'Meta Tags';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword']     = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_city_id']          = 'City';
$_['entry_country_id']       = 'Country';
$_['entry_mall_id']	 	     = 'Mall';
$_['entry_main_store_id']    = 'Main Store';
$_['entry_url']         	 = 'URL';
$_['entry_ssl']              = 'SSL';
$_['entry_display']          = 'Display';
$_['entry_website']          = 'Website';
$_['entry_social_jeeran']    = 'Social Jeeran';
$_['entry_social_fb'] 	     = 'Social Facebook';
$_['entry_social_tw']        = 'Social Twitter';
$_['entry_column']           = 'Columns';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_status']           = 'Status';

$_['entry_related_product']  = 'Product';
$_['entry_help_product']     = 'Related products';

$_['entry_related_tip']     = 'Tip';
$_['entry_help_tip']        = 'Related tips';

$_['entry_related_look']  = 'Look';
$_['entry_help_look']     = 'Related looks';

$_['entry_meta_title']           = 'Meta Title';
$_['entry_meta_description']     = 'Meta Description';
$_['entry_meta_keywords']        = 'Meta Keywords';

$_['entry_latitude']         = 'Latitude';
$_['entry_longitude']        = 'Longitude';
$_['entry_zoom']             = 'Zoom';
$_['entry_form_store_category']   = 'Tag Category';


//filter 
$_['entry_city']             = 'Filter city';
$_['entry_country']          = 'Filter country';
$_['entry_mall']             = 'Filter mall';
$_['entry_main_store']       = 'Filter main store';
$_['entry_store_category']   = 'Filter store category';
$_['entry_image']            = 'Image';
$_['entry_category_id']      = 'Category';
$_['entry_shop_id']          = 'Shop';

$_['select_status']         = '--select status --';
$_['select_country']         = '--select country --';
$_['select_mall']            = '--select mall --';
$_['select_main_store']      = '--select main store --';


// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the keyword is globally unique.';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent categories.';
$_['help_column']            = 'Number of columns to use for the bottom 3 categories. Only works for the top parent categories.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Tag Name must be between 2 and 255 characters!';
$_['error_title']             = 'Tag Title must be between 2 and 255 characters!';
$_['error_landing']          = 'Landing value 0 and 9999 !';
$_['error_approved']         = 'approved value numbers only!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO keyword already in use!';
$_['error_google_latitude']  = 'Need a valid latitude value!';
$_['error_google_longitude'] = 'Need a valid longitude value!';
$_['error_google_zoom']  	 = 'Need a valid zoom value!';