<?php
// Heading
$_['heading_title']                    = 'Offer Banner\'s';
$_['text_list']                        = 'Offer Banner Pages';

// Text
$_['text_settings']                    = 'Update Product Landing';
$_['text_success']                     = 'Success: You have modified settings!';
$_['text_edit']                        = 'Edit Setting';

$_['tab_block5']                       = 'Block 5- Offers';
$_['tab_block6']                         = 'Block 0- Countreis and Langauges';
$_['tab_block']                         = 'Block ';

$_['column_countries']                      ='Countries';
$_['column_language']                   = 'Language';
$_['column_gender']                     = 'Gender';
$_['column_page_id']                       ='Page Id';
$_['column_action']                    ='Action';

$_['text_brand'] = 'Select Brand';
$_['text_brand_name'] = 'Brand title';
$_['text_brand_desc'] = 'Brand title';
$_['text_brand_button'] = 'Button text';
$_['text_offer_banner_block5_image1']= 'Offer Image ';

$_['button_block_add']            = 'Add Blocks';
$_['text_product_landing_additional_block_title'] = 'Title ';
$_['text_product_landing_additional_block_action'] = 'Action ';
$_['text_product_landing_additional_block_url'] = 'URL';
$_['text_product_landing_additional_block_image'] = 'Image';
$_['entry_products']                    = 'Products';


// Help
$_['help_comment']                     = 'This field is for any special notes you would like to tell the customer i.e. Store does not accept cheques.';

// Error
$_['error_warning']                    = 'Warning: Please check the form carefully for errors!';
$_['error_permission']                 = 'Warning: You do not have permission to modify settings!';
$_['error_field_reuired']              = ' %s field required';
$_['error_image_thumb']                = 'Product Image Thumb Size dimensions required!';
