<?php

// Heading
$_['heading_title'] = 'Popup Setup';
// Text
$_['text_settings'] = 'Update (%s) language (%s) Homepage';
$_['text_success'] = 'Success: You have modified settings!';
$_['text_list'] = 'Available Countries for Custome Homepage';
$_['text_edit'] = 'Edit Setting';

// Columns
$_['column_name'] = 'Country';
$_['column_url'] = 'Language';
$_['column_action'] = 'Edit';

// Tabs
$_['tab_block0'] = 'Newsletter Popup (step1)';
$_['tab_block1'] = 'Registration (step2)';
$_['tab_block2'] = 'Success (step3)';

$_['text_popup_block0_image1'] = 'Desktop Image ';
$_['text_popup_block0_image2'] = 'Mobile Image';
$_['text_popup_block0_url1'] = 'Link';

$_['text_popup_block0_title1'] = 'Title ';
$_['text_popup_block0_color1'] = 'Background Color ';
$_['text_popup_block0_description1'] = 'Description';
$_['text_popup_block1_url1'] = 'Url';
$_['text_popup_block1_image1'] = 'Image';
$_['text_popup_block0_status1'] = 'Status';

$_['text_popup_block1_title1'] = 'Register Title ';
$_['text_popup_block1_description1'] = 'Register Description';

$_['text_popup_block2_title1'] = 'Success Title ';
$_['text_popup_block2_description1'] = 'Success Description';
$_['text_popup_block2_coupon1'] = 'Coupon Code';
$_['text_country'] = 'Country';



// Help
$_['help_comment'] = 'This field is for any special notes you would like to tell the customer i.e. Store does not accept cheques.';

// Error
$_['error_warning'] = 'Warning: Please check the form carefully for errors!';
$_['error_permission'] = 'Warning: You do not have permission to modify settings!';
$_['error_field_reuired'] = ' %s field required';
$_['error_image_thumb'] = 'Product Image Thumb Size dimensions required!';
