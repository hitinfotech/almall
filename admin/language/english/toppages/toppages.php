<?php
// Heading
$_['heading_title']          = 'Top Pages';

// Text
$_['text_success']           = 'Success: You have modified Mall!';
$_['text_list']              = 'Top Pages List';
$_['text_add']               = 'Add Top Pages';
$_['text_edit']              = 'Edit Top Pages';
$_['text_default']           = 'Default';

// Column
$_['column_title']           = 'Top Pages Title';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Action';
$_['column_country_id']      = 'Country';
$_['column_user_id']         = 'User Added Name';
$_['column_last_mod_id']     = 'Last Update Name';
$_['column_day']             = 'Day';


// Entry
$_['entry_start_date']       = 'Start Date';
$_['entry_end_date'] 	     = 'End Date';
$_['entry_sort_order']       = 'Sort Order';

$_['entry_title']            = 'Top Pages Title';
$_['entry_description']      = 'Description';
$_['entry_type']      	     = 'Type';
$_['entry_name']      	     = 'Name';
$_['entry_group']      	     = 'Group';
$_['entry_keywords']         = 'Keywords';

$_['select_country']         = '--select country --';

$_['entry_country_id']       = 'Country';
$_['entry_city_id']          = 'City';
$_['entry_mall_id']    		 = 'Mall';
$_['entry_shop_id']    		 = 'Shop';
$_['entry_brand_id']       	 = 'Brands';
$_['entry_category_id']      = 'Category';

$_['entry_image']            = 'Image';

$_['entry_active']   	 	 = 'Active';
$_['entry_column']           = 'Columns';
$_['entry_status']           = 'Status';
$_['column_country']         = 'Country';
$_['column_status']          = 'Status';
$_['column_desc']            = 'Description';
$_['column_image']           = 'Image';
 
$_['entry_home']             = 'Home';
$_['column_date_added']      = 'Date added';
$_['column_date_modified']   = 'Date modified';
$_['entry_language']         = 'Language';
$_['arabic_lang']            = 'Is Arabic';
$_['english_lang']           = 'Is English';

// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the keyword is globally unique.';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent categories.';
$_['help_column']            = 'Number of columns to use for the bottom 3 categories. Only works for the top parent categories.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_title']            = 'Top Pages Title must be between 3 and 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO keyword already in use!';
$_['error_date_start']       = 'Start date must be before the end Date!';
$_['error_date_end']         = 'Start date must be before the end Date!';