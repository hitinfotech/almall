<?php
// Heading
$_['heading_title']    = 'Import Products via CSV File';

// Text
$_['text_uploaded']    = 'Select file to upload';
$_['text_add']         = 'Add file to database';
$_['text_none']        = 'No File Selected';
$_['text_success']     = 'Data Import Successfull';

// Entry
$_['entry_select_file']= 'Choose CSV file';

// Confirm message
$_['confirm_message']  = "Are you sure, save this file?";

// Error
$_['error_permission'] = 'Warning: Permission Denied!';
$_['error_filename']   = 'Warning: Filename must be a between 3 and 255!';
$_['error_folder']     = 'Warning: Folder name must be a between 3 and 255!';
$_['error_exists']     = 'Warning: A file or directory with the same name already exists!';
$_['error_directory']  = 'Warning: Directory does not exist!';
$_['error_filetype']   = 'Warning: It is not CSV file !';
$_['error_upload']     = 'Warning: File could not be uploaded for an unknown reason!';
$_['error_delete']     = 'Warning: You can not delete this directory!';