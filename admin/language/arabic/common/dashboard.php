<?php
// Heading
$_['heading_title']                = 'لوحة التحكم';

// Text
$_['text_order_total']             = 'مجموع الطلبات';
$_['text_customer_total']          = 'مجموع العملاء';
$_['text_sale_total']              = 'مجموع المبيعات';
$_['text_online_total']            = 'المتواجدون في الموقع';
$_['text_map']                     = 'الخريطة';
$_['text_sale']                    = 'تحليل المبيعات';
$_['text_activity']                = 'اخر الفعاليات';
$_['text_recent']                  = 'اخر الطلبات';
$_['text_order']                   = 'الطلبات';
$_['text_customer']                = 'العملاء';
$_['text_day']                     = 'اليوم';
$_['text_week']                    = 'الاسبوع';
$_['text_month']                   = 'الشهر';
$_['text_year']                    = 'السنة';
$_['text_view']                    = 'إظهار المزيد ...';

// Error
$_['error_install']                = 'Warning: Install folder still exists and should be deleted for security reasons!';