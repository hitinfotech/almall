<?php
// header
$_['heading_title']  = 'الإدارة';

// Text
$_['text_heading']   = 'إدارة المحل';
$_['text_login']     = 'الرجاء إدخال بيانات الدخول.';
$_['text_forgotten'] = 'نسيت كلمة المرور ؟';

// Entry
$_['entry_username'] = 'إسم المستخدم';
$_['entry_password'] = 'كلمة المرور';

// Button
$_['button_login']   = 'تسجيل دخول';

// Error
$_['error_login']    = 'لا يوجد تطابق في إسم المستخدم و كلمة المرور';
$_['error_token']    = 'تشفير الجلسة غير صحيح . يرجى إعادة تسجيل الدخول';