<?php
// Heading
$_['heading_title']          = 'سيدتي مول';

// Text
$_['text_order']             = 'الطلبات';
$_['text_processing_status'] = 'قيد المعاجة';
$_['text_complete_status']   = 'مكتمل';
$_['text_customer']          = 'العملاء';
$_['text_online']            = 'العملاء المتواجدون';
$_['text_approval']          = 'بانتظار التأكيد';
$_['text_product']           = 'المنتجات';
$_['text_stock']             = 'غير متوفر';
$_['text_review']            = 'التعليقات';
$_['text_return']            = 'المعاد';
$_['text_affiliate']         = 'أفلييت';
$_['text_store']             = 'محلات';
$_['text_front']             = 'واجهة المحل';
$_['text_help']              = 'مساعدة';
$_['text_homepage']          = 'الصفحة الرئيسية';
$_['text_support']           = 'الدعم الفني';
$_['text_documentation']     = 'التوثيق';
$_['text_logout']            = 'تسجيل خروج';