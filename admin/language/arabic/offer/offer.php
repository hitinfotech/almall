<?php
// Heading
$_['heading_title']          = 'Offers';

// Text
$_['text_success']           = 'Success: You have modified Mall!';
$_['text_list']              = 'Offer List';
$_['text_add']               = 'Add Offer';
$_['text_edit']              = 'Edit Offer';
$_['text_default']           = 'Default';

// Column
$_['column_title']           = 'Offer Title';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Action';

// Entry
$_['entry_start_date']       = 'Start Date';
$_['entry_end_date'] 	     = 'End Date';
$_['entry_sort_order']       = 'Sort Order';

$_['entry_title']            = 'Offer Title';
$_['entry_description']      = 'Description';
$_['entry_body_text']      	 = 'Body Text';

$_['entry_country_id']       = 'Country';
$_['entry_city_id']          = 'City';
$_['entry_mall_id']    		 = 'Mall';
$_['entry_shop_id']    		 = 'Shop';
$_['entry_group_id']       	 = 'Group';
$_['entry_category_id']      = 'Category';

$_['entry_image']            = 'Image';

$_['entry_active']   	 	 = 'Active';
$_['entry_column']           = 'Columns';
$_['entry_status']           = 'Status';
$_['entry_home']             = 'Home';

// Help
$_['help_filter']            = '(Autocomplete)';
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the keyword is globally unique.';
$_['help_top']               = 'Display in the top menu bar. Only works for the top parent categories.';
$_['help_column']            = 'Number of columns to use for the bottom 3 categories. Only works for the top parent categories.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Offer Name must be between 2 and 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO keyword already in use!';
$_['error_date_start']        = 'Start date must be before the end Date!';
$_['error_date_end']          = 'Start date must be before the end Date!';