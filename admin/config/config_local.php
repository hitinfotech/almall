<?php

// HTTP
define('HTTP_SERVER', "http://commerce.sayidaty.local/admin/");
define('HTTP_CATALOG', "http://commerce.sayidaty.local/");

// HTTPS
define('HTTPS_SERVER', "http://commerce.sayidaty.local/admin/");
define('HTTPS_CATALOG', "http://commerce.sayidaty.local/");
define("MONGO_DEFUALT_HOST","localhost");

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'sayidaty');
define('DB_PASSWORD', 'sayidaty');
define('DB_DATABASE', 'sayidaty_net');
define('DB_PORT', '3306');
define('DB_PREFIX', '');


define("VAT_PERCENT","0.05");
define("KSA_VAT_NUMBER","300056517310003");
define("UAE_VAT_NUMBER","100343881700003");
//require_once DIR_SYSTEM . '/library/beans/Beanstalk.php';
