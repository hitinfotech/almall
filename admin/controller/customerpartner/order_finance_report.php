<?php

class ControllerCustomerpartnerorderfinancereport extends Controller {

  public function index(){

    $this->load->language('customerpartner/finance_report');


    $data['heading_title'] = $this->language->get('heading_title');
    $data['button_generate'] = $this->language->get('button_generate');
    $data['button_generate_monthly'] = $this->language->get('button_generate_monthly');
    $data['button_generate_monthly_invoice'] = $this->language->get('button_generate_monthly_invoice');
    $data['button_order_report'] = $this->language->get('button_order_report');
    $data['entry_select_month'] = $this->language->get('entry_select_month');
    $data['entry_select_year'] = $this->language->get('entry_select_year');
    $data['filter_partner'] = $this->language->get('filter_partner');
    $data['entry_date_start'] = $this->language->get('entry_date_start');
    $data['entry_date_end'] = $this->language->get('entry_date_end');
    $data['entry_order_status'] = $this->language->get('entry_order_status');

    $data['entry_account_manager'] = $this->language->get('entry_account_manager');
    $data['select_account'] = $this->language->get('select_account');
    $data['account_managers'] = $this->dynamic_values->getValues("account_manager");



    $this->load->model('localisation/order_status');
    $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();


    $data['token'] = $this->session->data['token'];


    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('text_home'),
        'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('customerpartner/order_finance_report', 'token=' . $this->session->data['token'], 'SSL')
    );

    $data['header'] = $this->load->controller('common/header', array('breadcrumbs' => $data['breadcrumbs']));
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');


    $this->response->setOutput($this->load->view('customerpartner/finance_report.tpl', $data));
  }

}
