<?php

class ControllerCustomerpartnerSeller extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('customerpartner/seller');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customerpartner/seller');

        $this->getList();
    }

    protected function getList() {

        if (isset($this->request->get['filter_seller'])) {
            $filter_seller = $this->request->get['filter_seller'];
        } else {
            $filter_seller = null;
        }


        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'customer_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';


        if (isset($this->request->get['filter_seller'])) {
            $url .= '&filter_seller=' . urlencode(html_entity_decode($this->request->get['filter_seller'], ENT_QUOTES, 'UTF-8'));
        }



        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['orders'] = array();

        $filter_data = array(
            'filter_seller' => $filter_seller,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $order_total = $this->model_customerpartner_seller->getTotalSeller($filter_data);

        $results = $this->model_customerpartner_seller->getSeller($filter_data);

        foreach ($results as $result) {
            $data['orders'][] = array(
                'customer_id' => $result['customer_id'],
                'screenname' => $result['screenname'],
                'companyname' => $result['companyname'],
                'email' => $result['email'],
                'view' => $this->url->link('customerpartner/seller/info', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, 'SSL'),
                'edit' => $this->url->link('customerpartner/seller/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, 'SSL'),
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_missing'] = $this->language->get('text_missing');
        $data['text_loading'] = $this->language->get('text_loading');

        $data['column_order_id'] = $this->language->get('column_order_id');
        $data['column_customer'] = $this->language->get('column_customer');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_action'] = $this->language->get('column_action');

        $data['entry_return_id'] = $this->language->get('entry_return_id');
        $data['entry_order_id'] = $this->language->get('entry_order_id');
        $data['entry_customer'] = $this->language->get('entry_customer');
        $data['entry_company'] = $this->language->get('entry_company');
        $data['entry_screen'] = $this->language->get('entry_screen');
        $data['entry_seller'] = $this->language->get('entry_seller');
        $data['entry_order_status'] = $this->language->get('entry_order_status');
        $data['entry_total'] = $this->language->get('entry_total');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_date_added'] = $this->language->get('entry_date_added');
        $data['entry_date_modified'] = $this->language->get('entry_date_modified');

        $data['button_invoice_print'] = $this->language->get('button_invoice_print');
        $data['button_shipping_print'] = $this->language->get('button_shipping_print');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['button_view'] = $this->language->get('button_view');
        $data['button_ip_add'] = $this->language->get('button_ip_add');

        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if (isset($this->request->get['filter_seller'])) {
            $url .= '&filter_seller=' . urlencode(html_entity_decode($this->request->get['filter_seller'], ENT_QUOTES, 'UTF-8'));
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_customer_id'] = $this->url->link('customerpartner/seller', 'token=' . $this->session->data['token'] . '&sort=customer_id' . $url, 'SSL');

        $url = '';


        if (isset($this->request->get['filter_seller'])) {
            $url .= '&filter_seller=' . urlencode(html_entity_decode($this->request->get['filter_seller'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

        $data['filter_seller'] = $filter_seller;

        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['store'] = HTTPS_CATALOG;

        $data['header'] = $this->load->controller('common/header', array('breadcrumbs' => $data['breadcrumbs']));
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customerpartner/seller_list.tpl', $data));
    }

    public function info() {

        $data = array();
        $data = array_merge($data, $this->load->language('customerpartner/seller'));
        $this->load->model('customerpartner/seller');

        if (isset($this->request->get['customer_id'])) {
            $customer_id = $this->request->get['customer_id'];
        } else {
            $customer_id = 0;
        }
        $data['heading_title'] = $this->language->get('heading_title');

        $data['column_order_id'] = $this->language->get('column_order_id');
        $data['column_customer'] = $this->language->get('column_customer');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_action'] = $this->language->get('column_action');

        $data['entry_return_id'] = $this->language->get('entry_return_id');
        $data['entry_order_id'] = $this->language->get('entry_order_id');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');

        $order_info = $this->model_customerpartner_seller->getSellerOrder($customer_id);
        foreach ($order_info as $result) {
            $data['orders'][] = array(
                'order_id' => $result['order_id'],
                'customer' => $result['customer'],
                'status' => $result['status'],
                'total' => $result['total'],
                'date_added' => $result['date_added'],
                'date_modified' => $result['date_modified'],
            );
        }


            $this->document->setTitle($this->language->get('heading_title'));

            $data['heading_title'] = $this->language->get('heading_title');

            $url = '';

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $data['text_home'],
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['breadcrumbs'][] = array(
                'text' => $data['heading_title'],
                'href' => $this->url->link('customerpartner/seller', 'token=' . $this->session->data['token'] . $url, 'SSL')
            );


            $data['token'] = $this->session->data['token'];

            $data['action'] = $this->url->link('customerpartner/seller/info&order_id=' . $customer_id, 'token=' . $this->session->data['token'], 'SSL');

            $data['header'] = $this->load->controller('common/header', array('breadcrumbs' => $data['breadcrumbs']));
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');
            $this->response->setOutput($this->load->view('customerpartner/seller_info.tpl', $data));

    }

    public function history() {

        $this->language->load('customerpartner/order');
        $this->load->model('customerpartner/order');
        $this->load->model('catalog/product');

        $product_names = array();
        $json = array();

        if ($this->config->get('marketplace_cancel_order_status')) {
            $marketplace_cancel_order_status = $this->config->get('marketplace_cancel_order_status');
        } else {
            $marketplace_cancel_order_status = '';
        }

        $this->load->model('localisation/order_status');
        $order_statuses = $this->model_localisation_order_status->getOrderStatuses();
        if (isset($this->request->post['comment']) && !empty($this->request->post['comment']) && empty($this->request->post['product_ids'])) {

            $getOrderStatusId = $this->model_customerpartner_order->getOrderStatusId($this->request->get['order_id']);

            $this->request->post['order_status_id'] = $getOrderStatusId['order_status_id'];

            $this->model_customerpartner_order->addOrderHistory($this->request->get['order_id'], $this->request->post);

            $json['success'] = $this->language->get('text_success_history');
        } elseif (isset($this->request->post['product_ids']) && !empty($this->request->post['product_ids'])) {

            $products = explode(",", $this->request->post['product_ids']);

            foreach ($products as $value) {
                $product_details = $this->model_catalog_product->getProduct($value);
                $product_names[] = $product_details['name'];
            }

            $product_name = implode(",", $product_names);

            foreach ($order_statuses as $value) {

                if (in_array($this->request->post['order_status_id'], $value)) {

                    $seller_change_order_status_name = $value['name'];
                }
            }

            if ($this->request->post['order_status_id'] == $marketplace_cancel_order_status) {

                $this->changeOrderStatus($this->request->get, $this->request->post, $products, $marketplace_cancel_order_status, $seller_change_order_status_name);
            } else {

                $this->changeOrderStatus($this->request->get, $this->request->post, $products, $marketplace_cancel_order_status, $seller_change_order_status_name);
            }


            $json['success'] = $this->language->get('text_success_history');
        } else {

            $json['error'] = $this->language->get('error_product_select');
        }

        $this->response->setOutput(json_encode($json));
    }

    private function changeOrderStatus($get, $post, $products, $marketplace_cancel_order_status, $seller_change_order_status_name) {


        /**
         * First step - Add seller changing status for selected products
         */
        $this->model_customerpartner_order->addsellerorderproductstatus($get['order_id'], $post['order_status_id'], $products);


        // Second Step - add comment for each selected products
        $this->model_customerpartner_order->addSellerOrderStatus($get['order_id'], $post['order_status_id'], $post, $products, $seller_change_order_status_name);

        // Thired Step - Get status Id that will be the whole order status id after changed the order product status by seller
        $getWholeOrderStatus = $this->model_customerpartner_order->getWholeOrderStatus($get['order_id'], $marketplace_cancel_order_status);


        // Fourth Step - add comment in order_history table and send mails to admin(If admin notify is enable) and customer
        $this->model_customerpartner_order->addOrderHistory($get['order_id'], $post, $seller_change_order_status_name);


        // Fifth Step - Update whole order status in order table
        if ($getWholeOrderStatus) {
            $this->model_customerpartner_order->changeWholeOrderStatus($get['order_id'], $getWholeOrderStatus);
        }
    }

}
