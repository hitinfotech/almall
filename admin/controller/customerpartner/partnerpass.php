<?php

class ControllerCustomerpartnerPartnerpass extends Controller {

    private $error = array();

    public function index() {

      if (isset($this->request->get['customer_id'])) {
          $customer_id = $this->request->get['customer_id'];
      } else {
          $customer_id = NULL;
      }

      $this->response->redirect($this->url->link('customerpartner/partnerpass/editpass', 'token=' . $this->session->data['token'].'&customer_id=' . $customer_id, 'SSL'));
    }

    public function editpass() {
        $this->load->language('customerpartner/partner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customerpartner/partner');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validatePassForm()) {
            $this->model_customerpartner_partner->editCustomerPass($this->request->get['customer_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('customerpartner/partner', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getPassForm();
    }

    protected function getPassForm() {

        $this->load->language('customerpartner/partner');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('customerpartner/partner');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['customer_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        $data['entry_password'] = $this->language->get('entry_password');
        $data['entry_confirm'] = $this->language->get('entry_confirm');

        $data['help_safe'] = $this->language->get('help_safe');
        $data['help_points'] = $this->language->get('help_points');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_remove'] = $this->language->get('button_remove');


        $data['token'] = $this->session->data['token'];
        $url = '';

        if (isset($this->request->get['customer_id'])) {
            $customer_id = $this->request->get['customer_id'];
        } else {
            $customer_id = NULL;
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['password'])) {
            $data['error_password'] = $this->error['password'];
        } else {
            $data['error_password'] = '';
        }

        if (isset($this->error['confirm'])) {
            $data['error_confirm'] = $this->error['confirm'];
        } else {
            $data['error_confirm'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('customerpartner/partner', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );


        $data['action'] = $this->url->link('customerpartner/partnerpass/editpass', 'token=' . $this->session->data['token'] . '&customer_id=' . $this->request->get['customer_id'] . $url, 'SSL');

        $data['cancel'] = $this->url->link('customerpartner/partner', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $customer_info = $this->model_customerpartner_partner->getCustomer($this->request->get['customer_id']);
        }


        if (isset($this->request->post['password'])) {
            $data['password'] = $this->request->post['password'];
        } else {
            $data['password'] = '';
        }

        if (isset($this->request->post['confirm'])) {
            $data['confirm'] = $this->request->post['confirm'];
        } else {
            $data['confirm'] = '';
        }

        $data['token'] = $this->session->data['token'];
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('customer/customer_pass.tpl', $data));
    }

    protected function validatePassForm() {
        if (!$this->user->hasPermission('modify', 'customerpartner/partnerpass')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ($this->request->post['password'] || (!isset($this->request->get['customer_id']))) {
            if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
                $this->error['password'] = $this->language->get('error_password');
            }

            if ($this->request->post['password'] != $this->request->post['confirm']) {
                $this->error['confirm'] = $this->language->get('error_confirm');
            }
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

}
