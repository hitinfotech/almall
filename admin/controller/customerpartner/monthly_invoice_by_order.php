<?php

class ControllerCustomerpartnermonthlyinvoicebyorder extends Controller {

    public function index() {

        $this->load->language('sale/order');
        $this->load->language('customerpartner/partner');
        $this->load->language('customerpartner/finance_report');

        $data['base'] = HTTPS_SERVER;

        $data['text_more_details'] = $this->language->get('text_more_details');

        $data['column_seller_name'] = $this->language->get('column_seller_name');
        $data['column_buyer_name'] = $this->language->get('column_buyer_name');
        $data['column_buyer_country'] = $this->language->get('column_buyer_country');
        $data['column_order_subtotal'] = $this->language->get('column_order_subtotal');
        $data['column_discount_voucher'] = $this->language->get('column_discount_voucher');
        $data['column_delivery_charges'] = $this->language->get('column_delivery_charges');
        $data['column_order_total'] = $this->language->get('column_order_total');
        $data['column_payment_method'] = $this->language->get('column_payment_method');
        $data['column_order_cod'] = $this->language->get('column_order_cod');
        $data['column_order_currency'] = $this->language->get('column_order_currency');
        $data['column_vat_amount'] = $this->language->get('column_vat_amount');
        $data['column_ship_company'] = $this->language->get('column_ship_company');

        if (isset($this->request->get['date_end']) && $this->validateDate($this->request->get['date_start'])) {
            $date_start = $this->request->get['date_start'];
        } else {
            $date_start = date('01-m-Y');
        }

        if (isset($this->request->get['date_end']) && $this->validateDate($this->request->get['date_end'])) {
            $date_end = $this->request->get['date_end'];
        } else {
            $date_end =  date("Y-m-t", strtotime($date_start));
        }

        if (isset($this->request->get['order_status']) && $this->request->get['date_end']!= 0) {
            $order_status = $this->request->get['order_status'];
        } else {
            $order_status = 5;
        }

        if(isset($this->request->get['viewed_columns'])){
          $data['seller_name'] = (strpos($this->request->get['viewed_columns'],'seller_name' )) ? true : false;
          $data['buyer_name'] = (strpos($this->request->get['viewed_columns'],'buyer_name' )) ? true : false;
          $data['buyer_country'] = (strpos($this->request->get['viewed_columns'],'buyer_country' )) ? true : false;
          $data['oder_subtotal'] = (strpos($this->request->get['viewed_columns'],'oder_subtotal' )) ? true : false;
          $data['discount'] = (strpos($this->request->get['viewed_columns'],'discount' )) ? true : false;
          $data['shipping'] = (strpos($this->request->get['viewed_columns'],'shipping' )) ? true : false;
          $data['order_total'] = (strpos($this->request->get['viewed_columns'],'order_total' )) ? true : false;
          $data['payment_method'] = (strpos($this->request->get['viewed_columns'],'payment_method' )) ? true : false;
          $data['codfees'] = (strpos($this->request->get['viewed_columns'],'cod' )) ? true : false;
          $data['currency'] = (strpos($this->request->get['viewed_columns'],'currency' )) ? true : false;;
          $data['vat_amount'] = (strpos($this->request->get['viewed_columns'],'vat_amount' )) ? true : false;
        }

        $this->load->model('customerpartner/partner');
        $this->load->model('shipping/shipping');

        $orders = $this->model_customerpartner_partner->getCompletedOrdersByPeriod($date_start,$date_end,$order_status);

//        echo '<pre>';
//        print_r($orders);
//        die;
        $result = array();
        $return_statuses_arr = [9,11,19,20,23];
        $seller_ids = [];

        foreach ($orders as $order) {
            if($order['order_product_status'] ==  5 && $this->model_customerpartner_partner->statusChangedBeforeRange($order['order_id'],$order['product_id'],5,$date_start, $date_end)){
                continue;
            }

            if (!isset($result[$order['order_id']])) {
                $seller_ids[$order['order_id']][] = $order['customer_id'];
                $result[$order['order_id']] = array(
                    'order_id' => $this->commonfunctions->convertOrderNumber($order['order_id']),
                    'seller_id' => $order['customer_id'],
                    'seller_name' => $order['companyname'],
                    'buyer_name' => $order['buyer_name'],
                    'payment_country' => $order['payment_country'],
                    'payment_code' => $order['payment_code'],
                    'codfees'=>round($this->currency->convert($order['codfees'], $this->config->get('config_currency'), $order['currency']),2),
                    'currency'=>$order['currency'],
                    'currency_value'=>$order['currency_value'],
                  );

                $order_totals = $this->model_customerpartner_partner->getOrderTotals($order['order_id']);
                // TOTAL UNCOMPLETED
                $uncom =  $this->model_customerpartner_partner->getUnCompltedCount($order['order_id']);

                $total_price_not_in_range =  $this->model_customerpartner_partner->getTotalNotInRange($order['order_id'],$date_start, $date_end);

                $result[$order['order_id']]['credit'] = '0';
                // TOTAL UNCOMPLTED
                foreach($order_totals as $k => $total){
                    if($total['code']=='sub_total'){
                        if (in_array($order_status,$return_statuses_arr)) {
                            $result[$order['order_id']]['sub_total'] = 0;
                        } else {
                            $result[$order['order_id']]['sub_total'] = round($this->currency->convert($total['value']-$uncom-$total_price_not_in_range, $this->config->get('config_currency'), $order['currency']),2);
                        }
                    }elseif($total['code']=='shipping'){
                        $result[$order['order_id']]['shipping'] = round($this->currency->convert($total['value'], $this->config->get('config_currency'), $order['currency']),2);
                    }elseif($total['code']=='coupon'){
                        $result[$order['order_id']]['coupon'] = round($this->currency->convert($total['value'], $this->config->get('config_currency'), $order['currency']),2);
                    }elseif($total['code']=='total'){
                        if (in_array($order_status,$return_statuses_arr)) {
                            $result[$order['order_id']]['total'] = round($this->currency->convert($total['value'], $this->config->get('config_currency'), $order['currency']),2);
                        } else {
                            $result[$order['order_id']]['total'] = round($this->currency->convert($total['value']-$uncom, $this->config->get('config_currency'), $order['currency']),2);
                        }
                    } elseif ($total['code']=='credit') {
                        $result[$order['order_id']]['credit'] = round($this->currency->convert($total['value'], $this->config->get('config_currency'), $order['currency']),2);
                    }

                }

                $result[$order['order_id']]['vat_amount'] = '0';

                $vat_info = unserialize($order['vat_info']);
                if ($vat_info) {
                    if ($result[$order['order_id']]['sub_total'] != 0) {
                        $result[$order['order_id']]['vat_amount'] = round($this->currency->convert($vat_info['vat_amount'], $this->config->get('config_currency'), $order['currency']),2);

                    } else {
                        $result[$order['order_id']]['vat_amount'] = round($this->currency->convert($vat_info['vat_amount'], $this->config->get('config_currency'), $order['currency']),2);
                    }
                }

                $result[$order['order_id']]['shipping_company'] = '';
                $shipping_companies =  $this->model_shipping_shipping->getShippingCompaniesForOrder($order['order_id']);
                if (!empty($shipping_companies)) {
                    $lastElement = end($shipping_companies);
                    foreach ($shipping_companies as $company) {
                        $result[$order['order_id']]['shipping_company'] .= $company['name'];
                        if ($company == $lastElement) break;
                        $result[$order['order_id']]['shipping_company'] .= '<br>';
                    }
                }
            }else{
              if(!in_array($order['customer_id'],$seller_ids[$order['order_id']]) && $order['order_product_status'] == $order_status ){
                $result[$order['order_id']]['seller_name'] .= ' - ' .$order['companyname'];
                  $seller_ids[$order['order_id']][] = $order['customer_id'];

              }
            }
        }
        $data['orders'] = $result;


        $data['from_date'] = $date_start;
        $data['to_date'] = $date_end;
        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');
        $this->load->model('tool/image');
        $data['logo'] = HTTPS_IMAGE_S3 . $this->config->get('config_logo_ltr'); //$this->model_tool_image->resize($partner['avatar'], $this->config_image->get('partner_avatar','avatar','width'), $this->config_image->get('partner_avatar','avatar','hieght'), false);

        $data['title'] = $this->language->get('text_invoice');




        $this->response->setOutput($this->load->view('customerpartner/monthly_invoice_by_order.tpl', $data));
    }

    function validateDate($myDateString){
        return (bool)strtotime($myDateString);
      }

}
