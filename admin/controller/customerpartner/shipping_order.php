<?php

class ControllerCustomerpartnerShippingOrder extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('customerpartner/shipping_order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customerpartner/shipping_order');

        $this->getList();
    }

    protected function getList() {
        $isDownload = false;
        if (isset($this->request->get['download'])) {
            $isDownload = true;
        }




        if (isset($this->request->get['shipping_company'])) {
            $filter_shipping_company = $this->request->get['shipping_company'];
        } else {
            $filter_shipping_company = null;
        }

        if($this->user->getShippingCompany() != $filter_shipping_company && $this->user->getShippingCompany() != 0){
          $this->response->redirect($this->url->link('user/user_permission', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        if (isset($this->request->get['order_case'])) {
            $order_case = $this->request->get['order_case'];
        } else {
            $order_case = null;
        }
        $data['order_case'] = $order_case;
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'o.order_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['shipping_company'])) {
            $url .= '&shipping_company=' . $this->request->get['shipping_company'];
        }

        if (isset($this->request->get['order_case'])) {
            $url .= '&order_case=' . $this->request->get['order_case'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('customerpartner/shipping_order', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $this->document->addScript('view/javascript/jquery/jquery.table2excel.js');


        $data['orders'] = array();

        $filter_data = array(
            'filter_shipping_company' => $filter_shipping_company,
            'filter_order_case' => $order_case,
            'sort' => $sort,
            'order' => $order,
         //   'start' => ($page - 1) * $this->config->get('config_limit_admin'),
        //    'limit' => $this->config->get('config_limit_admin')
        );

        if(!$isDownload){
            $filter_data['start'] =  ($page - 1) * $this->config->get('config_limit_admin');
            $filter_data['limit'] =  $this->config->get('config_limit_admin');

        }
        $order_total = $this->model_customerpartner_shipping_order->getTotalOrders($filter_data);

        $results = $this->model_customerpartner_shipping_order->getOrders($filter_data);


        foreach ($results as $result) {

          $action = $this->url->link('sale/shipping_process/info', 'token=' . $this->session->data['token'] . '&order_id='.$result['order_id'] . $url, 'SSL');
          if($order_case == 1){
              $action = $this->url->link('sale/shipping_process/order_confirm', 'token=' . $this->session->data['token'] . '&order_id='.$result['order_id'] . $url, 'SSL');
          }
          if($order_case == 1 || $order_case == 3){
              $book_awbno =  $this->db->query("select GROUP_CONCAT(book_response) book_awbno from order_booking where order_id = '".$result['order_id']."' AND country_id = '".$result['country_id']."'");
              $result['book_awbno']=$book_awbno->row['book_awbno'];
          }
            $data['orders'][] = array(
                'order_id' => $this->commonfunctions->convertOrderNumber($result['order_id'],$result['date_added']),
                'customer' => $result['customer'],
                'seller_name' => $result['seller_name'],
                'seller_country' => $result['seller_country'],
                'payment_code' => $result['payment_code'],
                'status' => $result['status'],
                'ship_awbno' => $result['ship_awbno'],
                'book_awbno' => $result['book_awbno'],
                'currency_code' => $result['currency_code'],
                'number_of_items' => $result['number_of_items'],
                'shipping_country' => $result['shipping_country'],
                'total' => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
                'date_added' => $result['date_added'],
                'date_modified' => date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
                'shipping_code' => $result['shipping_code'],
                'action' =>$action
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_missing'] = $this->language->get('text_missing');
        $data['text_loading'] = $this->language->get('text_loading');

        $data['column_order_id'] = $this->language->get('column_order_id');
        $data['column_customer'] = $this->language->get('column_customer');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_currency'] = $this->language->get('column_currency');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_num_of_items'] = $this->language->get('column_num_of_items');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_ship_awbno'] = $this->language->get('column_ship_awbno');
        $data['column_book_awbno'] = $this->language->get('column_book_awbno');

        $data['button_export'] = $this->language->get('button_export');

        $data['token'] = $this->session->data['token'];

        $url = '';

        if (isset($this->request->get['shipping_company'])) {
            $url .= '&shipping_company=' . $this->request->get['shipping_company'];
        }

        if (isset($this->request->get['order_case'])) {
            $url .= '&order_case=' . $this->request->get['order_case'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_order'] = $this->url->link('customerpartner/shipping_order', 'token=' . $this->session->data['token'] . '&sort=o.order_id' . $url, 'SSL');
        $data['sort_customer'] = $this->url->link('customerpartner/shipping_order', 'token=' . $this->session->data['token'] . '&sort=customer' . $url, 'SSL');
        $data['sort_status'] = $this->url->link('customerpartner/shipping_order', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
        $data['sort_total'] = $this->url->link('customerpartner/shipping_order', 'token=' . $this->session->data['token'] . '&sort=o.total' . $url, 'SSL');
        $data['sort_date_added'] = $this->url->link('customerpartner/shipping_order', 'token=' . $this->session->data['token'] . '&sort=o.date_added' . $url, 'SSL');
        $data['sort_date_modified'] = $this->url->link('customerpartner/shipping_order', 'token=' . $this->session->data['token'] . '&sort=o.date_modified' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['order_case'])) {
            $url .= '&order_case=' . $this->request->get['order_case'];
        }

        if (isset($this->request->get['shipping_company'])) {
            $url .= '&shipping_company=' . $this->request->get['shipping_company'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        $pagination = new Pagination();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('customerpartner/shipping_order', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

        $data['filter_shipping_company'] = $filter_shipping_company;

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['store'] = HTTPS_CATALOG;

        $data['header'] = $this->load->controller('common/header', array('breadcrumbs' => $data['breadcrumbs']));
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customerpartner/shipping_order_list.tpl', $data));
    }

}
