<?php

class ControllerCustomerpartnerInvoice extends Controller {

    public function index() {

        $this->load->language('sale/order');
        $this->load->language('customerpartner/partner');

        $data['base'] = HTTPS_SERVER;

        $data['text_invoice'] = $this->language->get('text_invoice');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_rma_details'] = $this->language->get('text_rma_details');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_website'] = $this->language->get('text_website');
        $data['text_payment_address'] = $this->language->get('text_payment_address');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_comment'] = $this->language->get('text_comment');
        $data['text_invoice_title'] = $this->language->get('text_invoice_title');
        $data['text_more_details'] = $this->language->get('text_more_details');

        $data['column_product'] = $this->language->get('column_product');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_sku'] = $this->language->get('column_sku');
        $data['column_order'] = $this->language->get('column_order');
        $data['column_admin'] = $this->language->get('column_admin');
        $data['column_customer'] = $this->language->get('column_customer');
        $data['column_discount'] = $this->language->get('column_discount');
        $data['column_customer_after_discount'] = $this->language->get('column_customer_after_discount');
        $data['column_order_rma_id'] = $this->language->get('column_order_rma_id');
        $data['column_reason'] = $this->language->get('column_reason');
        $data['text_seller_name'] = $this->language->get('text_seller_name');
        $data['text_dashboard'] = $this->language->get('text_dashboard');
        $data['text_commission'] = $this->language->get('text_commission');
        $data['text_total_amount'] = $this->language->get('text_total_amount');
        $data['text_seller_amount'] = $this->language->get('text_seller_amount');
        $data['text_paid_to_seller'] = $this->language->get('text_paid_to_seller');
        $data['text_rem_amount'] = $this->language->get('text_rem_amount');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_admin_amount'] = $this->language->get('text_admin_amount');
        $data['text_grand_total'] = $this->language->get('text_grand_total');
        $data['text_grand_paid'] = $this->language->get('text_grand_paid');
        $data['text_grand_rem'] = $this->language->get('text_grand_rem');
        $data['text_action'] = $this->language->get('text_action');
        $data['text_from'] = $this->language->get('text_from');
        $data['text_to'] = $this->language->get('text_to');
        $data['no_records'] = $this->language->get('no_records');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['button_reset_filter'] = $this->language->get('button_reset_filter');
        $data['text_total_transaction_fees'] = $this->language->get('total_transaction_fees');

        if (isset($this->request->get['customer_id'])) {
            $partner_id = $this->request->get['customer_id'];
        } else {
            $partner_id = 0;
        }

        if (isset($this->request->get['date_start']) && $this->validateDate($this->request->get['date_start'])) {
            $date_start = $this->request->get['date_start'];
        } else {
            $date_start = date('01-m-Y');
        }

        if (isset($this->request->get['date_end']) && $this->validateDate($this->request->get['date_end'])) {
            $date_end = $this->request->get['date_end'];
        } else {
            $date_end =  date("Y-m-t", strtotime($date_start));
        }

        $this->load->model('sale/order');
        $this->load->model('customerpartner/partner');
        $this->load->model('setting/setting');
        $this->load->model('localisation/country');


        $partner = $this->model_customerpartner_partner->getPartnerCustomerInfo($partner_id);

        if (empty($partner)) {
            die('seller Not Found');
        }
        $data['report_countries'] = $this->model_localisation_country->getAvailableCountries();

        $partner_desc = $this->model_customerpartner_partner->getPartnerDescription($partner_id);

        $data['company'] = $partner_desc[1]['companyname'];

        $days_per_month =  cal_days_in_month(CAL_GREGORIAN, date("m",strtotime($date_start)), date("y",strtotime($date_start)));


        $diff_for_date_range = round(abs(strtotime($partner['fbs_date_in']) - strtotime($date_start))/ 86400);

        $storage_fees = $partner['fbs_storage_charges'];

        if($partner['fbs_free_storage_days'] > $diff_for_date_range )
          $storage_fees = $partner['fbs_storage_charges'] - round(($partner['fbs_free_storage_days']-$diff_for_date_range) * ($partner['fbs_storage_charges'] / $days_per_month),2);

        if($storage_fees < 0)
          $storage_fees = 0;

        $conversion_volume = ($partner['fbs_inventory_volume_unit'] == 2 ) ? 0.093  : 1;
        $storage_fees = $storage_fees * $partner['fbs_inventory_volume'] * $conversion_volume;




        //$storage_fees_per_day = round($partner['fbs_storage_charges'] / ($days_per_month - $partner['fbs_free_storage_days']),2);
        $data['from_date'] = $date_start;
        $data['to_date'] = $date_end;
        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');
        $this->load->model('tool/image');
        $data['logo'] = HTTPS_IMAGE_S3 . $this->config->get('config_logo_ltr'); //$this->model_tool_image->resize($partner['avatar'], $this->config_image->get('partner_avatar','avatar','width'), $this->config_image->get('partner_avatar','avatar','hieght'), false);

        $financial_data = '-Beneficiary Name - '.$partner['beneficiary_name'] .'-Bank Name-' .$partner['bank_name'].'<br>-Bank Account  - ' .$partner['bank_account'] .'<br>-IBan code- ' .$partner['iban_code'] .'-Swift code-' .$partner['swift_code'].'-Currency-' .$partner['curcode'];
        $data['bank'] = (isset($partner['otherpayment']) && $partner['otherpayment'] != '') ? $partner['otherpayment'] : $financial_data;
        $data['seller_name'] = $partner['firstname'] . " " . $partner['lastname'];
        $data['flat_rate'] = round($this->currency->convert($partner['flat_rate'],$partner['curcode'],$this->config->get('config_currency')),2);
        $data['seller_currency'] = $partner['curcode'];
        $data['is_seller_fbs'] = $partner['is_fbs'];
        $data['fbs_storage_charges'] = $storage_fees;//$partner['fbs_storage_charges'];
        //$data['fbs_storage_charges_per_day'] = $storage_fees_per_day;
        $data['fbs_free_storage_days'] = $partner['fbs_free_storage_days'];
        $data['days_per_month'] = $days_per_month;
        $data['fbs_fullfillment_charges'] = $partner['fbs_fullfillment_charges'];
        $data['email'] = $partner['email'];
        $data['phone'] = $partner['telephone'];
        $data['commission'] = $partner['commission'] . " %";
        $data['country'] = ($partner['country_id'] == 184 ? 'Saudi Arabia' : 'United Arab Emarites');

        $data['partner_returns'] = $this->model_customerpartner_partner->getReturns($this->request->get['customer_id'], $date_start, $date_end);

        $invoice_name = $result = substr($data['company'], 0, 4) . $partner_id . date('F Y',strtotime($date_start)) .'-'.date('F Y',strtotime($date_end))  ;

        $data['title'] = $this->language->get('text_invoice') . " - " . $invoice_name;
        $data['converting_currency'] = $this->config->get('config_currency');

        $data['grand_total_quantity'] = 0;
        $data['grand_total_quantity'] = 0;
        $data['grand_total_total'] = 0;
        $data['grand_total_fees'] = 0;
        $data['grand_total_seller'] = 0;
        $data['grand_total_total_SAR'] = 0;
        $data['grand_total_total_AED'] = 0;
        $data['grand_total_total_USD'] = 0;
        $data['grand_total_total_BHD'] = 0;
        $data['grand_total_total_FBS'] = 0;
        $data['grand_total_completed_order'] = 0;

        $data['grand_total_total_FBS'] = 0;
        $data['grand_total_quantity_FBS'] = 0;
        $data['grand_total_fees_FBS'] = 0;
        $data['grand_total_fees_SAR'] = 0;
        $data['grand_total_fees_USD'] = 0;
        $data['grand_total_fees_BHD'] = 0;
        $data['grand_total_fees_AED'] = 0;

        $data['grand_total_seller_SAR'] = 0;
        $data['grand_total_seller_AED'] = 0;
        $data['grand_total_seller_BHD'] = 0;
        $data['grand_total_seller_USD'] = 0;
        $data['grand_total_seller_FBS'] = 0;

        $data['grand_total_order_SAR'] = 0;
        $data['grand_total_order_AED'] = 0;
        $data['grand_total_order_USD'] = 0;
        $data['grand_total_order_BHD'] = 0;
        $data['grand_total_order_FBS'] = 0;

        $data['grand_total_completed_order'] = 0;
        $data['grand_total_completed_order_SAR'] = 0;
        $data['grand_total_completed_order_AED'] = 0;
        $data['grand_total_completed_order_BHD'] = 0;
        $data['grand_total_completed_order_USD'] = 0;
        $data['grand_total_completed_order_FBS'] = 0;


        $data['grand_total_sorder_SAR'] = 0;
        $data['grand_total_sorder_AED'] = 0;
        $data['grand_total_sorder_USD'] = 0;
        $data['grand_total_sorder_BHD'] = 0;
        $data['grand_total_sorder_FBS'] = 0;

        //$data['fbs_fullfillment_charges'] = ($partner['is_fbs']==1) ? $partner['fbs_fullfillment_charges'] : 0;
        //$data['fbs_storage_charges'] = ($partner['is_fbs']==1) ? $partner['fbs_storage_charges'] : 0;

        $partner_orders = $this->model_customerpartner_partner->getPartnerOrders($this->request->get['customer_id'], $date_start, $date_end);
        foreach($partner_orders as $init_key => $init_value){
          $data['grand_total_completed_order_'.$init_value['currency_code']] = 0;
          $data['grand_total_sorder_'.$init_value['currency_code']] = 0;
          $data['grand_total_order_'.$init_value['currency_code']] = 0;
          $data['grand_total_seller_'.$init_value['currency_code']] = 0;
          $data['grand_total_fees_'.$init_value['currency_code']] = 0;
          $data['grand_total_total_'.$init_value['currency_code']] = 0;
          $data['grand_total_total_local_currency_'.$init_value['currency_code']] = 0;
          $data['grand_total_quantity_'.$init_value['currency_code']] = 0;
        }

        $successfull_orders = array();
        $total_orders = array ();
        foreach ($partner_orders as $row) {


            if($row['order_product_status'] ==  5 && $this->model_customerpartner_partner->statusChangedBeforeRange($row['order_id'],$row['product_id'],5,$date_start, $date_end)){
                    continue;
            }

            $invoice_filename = $this->model_customerpartner_partner->getSellerInvoicename($row['customer_id'],date("m",strtotime($date_start)),date("Y",strtotime($date_start)));
            $invoice_filename = str_replace('seller_monthly_invoices/' , '' ,$invoice_filename);
            $data['invoice_filename'] = str_replace('.pdf' , '' ,$invoice_filename);
            $row['original_currency_code'] = $row['currency_code'];
            $row['currency_code'] = ($row['is_fbs'] == 1 ) ? "FBS": $row['currency_code'];
            $row['order_id'] = $this->commonfunctions->convertOrderNumber($row['order_id'],$row['date_added']);

            $fees = 0;
            $successfull = 0;
            $total_orders[$row['order_id']] = 1;
            if (!in_array($row['order_id'] . '_' . $row['customer_id'] . '_' . $row['currency_code'], $successfull_orders)) {
                $successfull_orders[] = $row['order_id'] . '_' . $row['customer_id'] . '_' . $row['currency_code'];

                $successfull = 1;
                $info = $this->model_customerpartner_partner->getPartner($row['customer_id']);
                $fees =round( $this->currency->convert($info['flat_rate'],$partner['curcode'],$this->config->get('config_currency')),2);
            }

            $row['total'] = ($row['price']) . ' ' . $row['currency_code'];
            $row['converted_total'] = 0;
            $row['customer_total'] = ($row['customer'] - $data['flat_rate']) . ' ' . $row['currency_code'];
            $row['flat_rate'] = $data['flat_rate'] . ' ' . $row['currency_code'];
            $row['commission_applied'] = $row['commission_applied'] . ' %';

            if ($row['order_product_status'] == 5 || $row['order_product_status'] == 22) {
                $row['converted_total'] = $this->currency->convert($row['price'],$row['original_currency_code'],$this->config->get('config_currency'));

                $data['currency_code'] = $row['original_currency_code'];
                $data['grand_total_sorder_' . $row['currency_code']] +=1;
                $data['grand_total_completed_order_' . $row['currency_code']] += $successfull;
                $data['grand_total_quantity_' . $row['currency_code']] += $row['quantity'];
                $data['grand_total_total_' . $row['currency_code']] =$data['grand_total_total_' . $row['currency_code']] +  $row['converted_total'] ;
                $data['grand_total_total_local_currency_' . $row['currency_code']] =$data['grand_total_total_local_currency_' . $row['currency_code']] +  $row['price'] ;

                $data['grand_total_fees_' . $row['currency_code']] += $fees;
                $data['grand_total_seller_' . $row['currency_code']] += ($row['price']) - ($row['price'] * $row['commission_applied'] / 100) - $data['flat_rate'];

                $data['grand_total_completed_order'] += $successfull;
                $data['grand_total_quantity'] += $row['quantity'];
                $data['grand_total_total'] +=($row['price'] * $row['quantity']);

                $data['grand_total_fees'] += $fees;
                $data['grand_total_seller'] += ($row['quantity'] * $row['price']) - ($row['quantity'] * $row['price'] * $row['commission_applied'] / 100) - $data['flat_rate'];
            } else {
                $row['total'] = $row['flat_rate'] = $row['customer_total'] = $row['commission_applied'] = ' -- ';
            }
            $data['partner_orders_' . $row['currency_code']][] = $row;
            if(isset( $data['grand_total_order_' . $row['currency_code']]))
             $data['grand_total_order_' . $row['currency_code']] +=1;
            else
             $data['grand_total_order_' . $row['currency_code']] = 1;
        }
         $data['total_seller_amount_for_all'] = 0;
         foreach($data['report_countries'] as $key => $country){
            if(isset($data['grand_total_total_'.$country['curcode']]) && $data['grand_total_total_'.$country['curcode']] != 0){
              $data['total_seller_amount_for_all'] += round($data['grand_total_total_'.$country['curcode']] - ($data['commission'] * $data['grand_total_total_'.$country['curcode']] / 100 + $data['grand_total_fees_'.$country['curcode']] ),2);
            }
        }
        $data['total_seller_amount_for_all'] += round($data['grand_total_total_FBS'] - ($data['commission'] * $data['grand_total_total_FBS'] / 100 + $data['grand_total_fees_FBS'] ),2);
        $data['total_seller_amount_for_all'] = round($this->currency->convert($data['total_seller_amount_for_all'],$this->config->get('config_currency'),$partner['curcode']),2);
        $data['total_orders'] = count($total_orders);

        $this->response->setOutput($this->load->view('customerpartner/invoice.tpl', $data));
    }

    function validateDate($myDateString){
        return (bool)strtotime($myDateString);
      }


}
