<?php

class ControllerCustomerpartnermonthlyinvoice extends Controller
{

    public function index()
    {

        $this->load->language('sale/order');
        $this->load->language('customerpartner/partner');

        $data['base'] = HTTPS_SERVER;

        $data['text_invoice'] = $this->language->get('text_invoice');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_rma_details'] = $this->language->get('text_rma_details');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_website'] = $this->language->get('text_website');
        $data['text_payment_address'] = $this->language->get('text_payment_address');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_comment'] = $this->language->get('text_comment');
        $data['text_invoice_title'] = $this->language->get('text_invoice_title');
        $data['text_more_details'] = $this->language->get('text_more_details');

        $data['column_product'] = $this->language->get('column_product');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_sku'] = $this->language->get('column_sku');
        $data['column_order'] = $this->language->get('column_order');
        $data['column_admin'] = $this->language->get('column_admin');
        $data['column_customer'] = $this->language->get('column_customer');
        $data['column_discount'] = $this->language->get('column_discount');
        $data['column_customer_after_discount'] = $this->language->get('column_customer_after_discount');
        $data['column_order_rma_id'] = $this->language->get('column_order_rma_id');
        $data['column_reason'] = $this->language->get('column_reason');
        $data['text_seller_name'] = $this->language->get('text_seller_name');
        $data['text_dashboard'] = $this->language->get('text_dashboard');
        $data['text_commission'] = $this->language->get('text_commission');
        $data['text_total_amount'] = $this->language->get('text_total_amount');
        $data['text_seller_amount'] = $this->language->get('text_seller_amount');
        $data['text_paid_to_seller'] = $this->language->get('text_paid_to_seller');
        $data['text_rem_amount'] = $this->language->get('text_rem_amount');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_admin_amount'] = $this->language->get('text_admin_amount');
        $data['text_grand_total'] = $this->language->get('text_grand_total');
        $data['text_grand_paid'] = $this->language->get('text_grand_paid');
        $data['text_grand_rem'] = $this->language->get('text_grand_rem');
        $data['text_action'] = $this->language->get('text_action');
        $data['text_from'] = $this->language->get('text_from');
        $data['text_to'] = $this->language->get('text_to');
        $data['no_records'] = $this->language->get('no_records');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['button_reset_filter'] = $this->language->get('button_reset_filter');
        $data['total_transaction_fees'] = $this->language->get('total_transaction_fees');

        if (isset($this->request->get['date_end']) && $this->validateDate($this->request->get['date_start'])) {
            $date_start = $this->request->get['date_start'];
        } else {
            $date_start = date('01-m-Y');
        }

        if (isset($this->request->get['date_end']) && $this->validateDate($this->request->get['date_end'])) {
            $date_end = $this->request->get['date_end'];
        } else {
            $date_end = date("Y-m-t", strtotime($date_start));
        }

        if (isset($this->request->get['account_manager']) && $this->request->get['account_manager'] != '*') {
            $account_manager = $this->request->get['account_manager'];
        } else {
            $account_manager = '*' ;
        }

        $this->load->model('customerpartner/partner');

        $orders = $this->model_customerpartner_partner->getCompletedMonthlyOrders($date_start, $date_end,$account_manager);

        $result = array();
        $fees_orders = array();
        $success_fees_orders = array();
        $total_orders_count = array();
        $complete_orders_count = array();

        $days_per_month =  cal_days_in_month(CAL_GREGORIAN, date("m",strtotime($date_start)), date("y",strtotime($date_start)));
        $gross=0;
        $selected_sellers = [];
        $exist_orders = [];

        foreach ($orders as $order) {

            if($order['order_product_status'] ==  5 && $this->model_customerpartner_partner->statusChangedBeforeRange($order['order_id'],$order['product_id'],5,$date_start, $date_end)){
                continue;
            }

            $country_code = $order['order_country'];
            $customer_id = $order['customer_id'];

            if (!isset($result[$country_code][$customer_id]['vat'])) {
                $result[$country_code][$customer_id]['vat'] = [];
                $result[$country_code][$customer_id]['vat']['gross'] = 0;
                $result[$country_code][$customer_id]['vat']['cnt_vat_orders'] = 0;
            }

            $diff_for_date_range = round(abs(strtotime($order['fbs_date_in']) - strtotime($date_start))/ 86400);

            $storage_fees = $order['fbs_storage_charges'];

            if($order['fbs_free_storage_days'] > $diff_for_date_range )
              $storage_fees = $order['fbs_storage_charges'] - round(($order['fbs_free_storage_days']-$diff_for_date_range) * ($order['fbs_storage_charges'] / $days_per_month),2);

            if($storage_fees < 0)
              $storage_fees = 0;

            $conversion_volume = ($order['fbs_inventory_volume_unit'] == 2 ) ? 0.093  : 1;
            $storage_fees = $storage_fees * $order['fbs_inventory_volume'] * $conversion_volume;

            $total_orders_count[$country_code][$customer_id][$order['order_id']] = 1;

            /*if (!in_array($country_code, array('AE', 'SA'))) {
                $country_code = 'NOT';
            }*/
            if ((int)$customer_id <= 0) {
                $customer_id = 0;
            }

            $fees = 0;
            $commission = 0;
            $total_sales = 0;
            $total_srpc = 0;
            $total_seller = 0;

            if ($order['order_product_status'] == 5 || $order['order_product_status'] == 22) {
                $key = $order['order_id'] . '_' . $order['customer_id'] . '_' . $order['product_id'] . '_' . $order['order_country'];
                $success_key = $order['order_id'] . '_' . $order['customer_id'] . '_' . $order['order_country'];
                if (!in_array($key, $fees_orders)) {

                    $fees_orders[] = $key;

                    $fees = 0;
                    if (!in_array($success_key, $success_fees_orders)) {
                        $success_fees_orders[] = $success_key;
                        $fees = $order['flat_rate'];

                    }

                    $commission = $order['commission'] / 100 * $order['total_price'];
                    $complete_orders_count[$country_code][$customer_id][$order['order_id']] = 1;
                    $total_sales = $order['total_price'];
                    $total_srpc = $commission + $fees;
                    $total_seller = $total_sales - $total_srpc;
                }
            }

            if (!isset($result[$country_code])) {
                $result[$country_code] = array();
            }

            if ($order['date_added'] > date('Y').'-01-01 00:00:00' && ($order['order_product_status'] == 5 || $order['order_product_status'] == 22)) {
                if (in_array($order['customer_id'], $selected_sellers)) {
                    $result[$country_code][$customer_id]['vat']['gross'] += $this->currency->convert($order['total_price'],$order['currency_code'],$order['seller_currency']);
                    if (!in_array($order['order_id'],$exist_orders[$country_code][$customer_id])) {
                        $exist_orders[$country_code][$customer_id][] = $order['order_id'];
                        $result[$country_code][$customer_id]['vat']['cnt_vat_orders']++;
                    }
                    $result[$country_code][$customer_id]['vat']['commission']=$order['commission'];
                    $result[$country_code][$customer_id]['vat']['flat_rate']=$order['flat_rate'];
                } else {
                    $eligible_vat_info = $this->db->query("SELECT * FROM eligible_vat WHERE customer_id = '" . (int)$order['customer_id'] . "' AND checked=1");
                    if ($eligible_vat_info->num_rows) {
                        $selected_sellers[] = $order['customer_id'];

                        if (!in_array($order['order_id'],$exist_orders[$country_code][$customer_id])) {
                            $exist_orders[$country_code][$customer_id][] = $order['order_id'];
                            $result[$country_code][$customer_id]['vat']['cnt_vat_orders']++;
                        }
                        $result[$country_code][$customer_id]['vat']['gross'] += $this->currency->convert($order['total_price'],$order['currency_code'],$order['seller_currency']);
                        $result[$country_code][$customer_id]['vat']['commission']=$order['commission'];
                        $result[$country_code][$customer_id]['vat']['flat_rate']=$order['flat_rate'];
                    }
                }
            }
            if (!isset($result[$country_code][$customer_id][$order['order_id']])&& ($order['is_product_fbs'] ==0)) {


                //$orders = $this->model_customerpartner_partner->getMonthlyOrders($date_start,$date_end,$customer_id,$order['payment_country_id']);
                //$total_vat_value = ($order['total_price'] - (($order['commission'] * ($order['total_price'] / 100)) + $order['flat_rate']))*VAT_PERCENT;
                $result[$country_code][$customer_id][$order['order_id']] = array(
                    'order_status'=>$order['order_product_status'],
                    'seller_id' => $order['customer_id'],
                    'name' => $order['companyname'],
                    'country' => $order['seller_country'],
                    'commission' => $order['commission'],
                    'total_sales' => $order['total_price'],
                    'converted_total_sales' => $this->currency->convert($order['total_price'] - (($order['commission'] * $order['total_price'] / 100) + $this->currency->convert($order['flat_rate'],$order['seller_currency'],$order['currency_code'])),$order['currency_code'],$this->config->get('config_currency')),
                    'total_commission' => ($order['commission']/100) * $order['total_price'],
                    'fbs_storage_charges' => 0 ,
                    'fbs_fullfillment_charges' => 0,
                    'transaction_fees' => $this->currency->convert($order['flat_rate'],$order['seller_currency'],$order['currency_code']),
                    'total_transaction_fees' => ($order['commission'] * $order['total_price'] / 100) + $order['flat_rate'],
                    'total_seller' => $order['total_price'] - (($order['commission'] * $order['total_price'] / 100) + $this->currency->convert($order['flat_rate'],$order['seller_currency'],$order['currency_code'])),

                );
            } elseif ($order['is_product_fbs'] ==0) {
                $result[$country_code][$customer_id][$order['order_id']]['total_sales'] += $order['total_price'];
                $result[$country_code][$customer_id][$order['order_id']]['total_commission'] = (($order['commission']/100) * $order['total_price']) + $result[$country_code][$customer_id][$order['order_id']]['total_commission'];
                $result[$country_code][$customer_id][$order['order_id']]['transaction_fees'] = $this->currency->convert($order['flat_rate'],$order['seller_currency'],$order['currency_code']);
                $result[$country_code][$customer_id][$order['order_id']]['fbs_storage_charges'] =  0;
                $result[$country_code][$customer_id][$order['order_id']]['fbs_fullfillment_charges'] = 0;
                $result[$country_code][$customer_id][$order['order_id']]['total_transaction_fees'] = ($order['flat_rate'] + $result[$country_code][$customer_id][$order['order_id']]['total_commission']);
                $result[$country_code][$customer_id][$order['order_id']]['total_seller'] = $result[$country_code][$customer_id][$order['order_id']]['total_sales'] - ($result[$country_code][$customer_id][$order['order_id']]['transaction_fees']+$result[$country_code][$customer_id][$order['order_id']]['total_commission']);
                $result[$country_code][$customer_id][$order['order_id']]['converted_total_sales'] = $this->currency->convert($result[$country_code][$customer_id][$order['order_id']]['total_sales'] - ($result[$country_code][$customer_id][$order['order_id']]['transaction_fees']+$result[$country_code][$customer_id][$order['order_id']]['total_commission']),$order['currency_code'],$this->config->get('config_currency'));

            }elseif (!isset($result[$country_code][$customer_id."-2"][$order['order_id']]) && ( $order['is_product_fbs'] !=0)) {
                $result[$country_code][$customer_id."-2"][$order['order_id']] = array(
                    'order_status'=>$order['order_product_status'],
                    'seller_id' => $order['customer_id'],
                    'name' => $order['companyname']."<p><b>FBS - Products</b></p>",
                    'country' => $order['seller_country'],
                    'commission' => $order['commission'],
                    'total_sales' => $order['total_price'],
                    'converted_total_sales' => $this->currency->convert($order['total_price'] - (($order['commission'] * $order['total_price'] / 100) + $order['flat_rate']),$order['currency_code'],$this->config->get('config_currency')),
                    'total_commission' => ($order['commission']/100) * $order['total_price'],
                    'fbs_storage_charges' => $this->currency->convert($storage_fees,$order['seller_currency'],$order['currency_code']) ,
                    'fbs_fullfillment_charges' => $this->currency->convert(($order['fbs_fullfillment_charges'] != null) ?$order['fbs_fullfillment_charges'] :0,$order['seller_currency'],$order['currency_code']),
                    'transaction_fees' => $this->currency->convert($order['flat_rate'],$order['seller_currency'],$order['currency_code']),
                    'total_transaction_fees' => ($order['commission'] * $order['total_price'] / 100) + $order['flat_rate'],
                    'total_seller' => $order['total_price'] - (($order['commission'] * $order['total_price'] / 100) + $order['flat_rate']),
                    //'total_vat_value' => $total_vat_value
                );
            } else {
                $result[$country_code][$customer_id."-2"][$order['order_id']]['total_sales'] += $order['total_price'];
                $result[$country_code][$customer_id."-2"][$order['order_id']]['total_commission'] = (($order['commission'] / 100) * $order['total_price']) + $result[$country_code][$customer_id."-2"][$order['order_id']]['total_commission'];
                $result[$country_code][$customer_id."-2"][$order['order_id']]['transaction_fees'] = $this->currency->convert($order['flat_rate'],$order['seller_currency'],$order['currency_code']);
                $result[$country_code][$customer_id."-2"][$order['order_id']]['fbs_storage_charges'] = $this->currency->convert($storage_fees,$order['seller_currency'],$order['currency_code']);
                $result[$country_code][$customer_id."-2"][$order['order_id']]['fbs_fullfillment_charges'] = $this->currency->convert(($order['fbs_fullfillment_charges'] != null) ?$order['fbs_fullfillment_charges'] :0,$order['seller_currency'],$order['currency_code']);
                $result[$country_code][$customer_id."-2"][$order['order_id']]['total_transaction_fees'] = ($order['flat_rate'] + $result[$country_code][$customer_id."-2"][$order['order_id']]['total_commission']);
                $total_vat_value = ($result[$country_code][$customer_id."-2"][$order['order_id']]['total_sales'] - $result[$country_code][$customer_id."-2"][$order['order_id']]['total_transaction_fees'])*VAT_PERCENT;
                $result[$country_code][$customer_id."-2"][$order['order_id']]['total_seller'] = $result[$country_code][$customer_id."-2"][$order['order_id']]['total_sales'] - $result[$country_code][$customer_id."-2"][$order['order_id']]['total_transaction_fees'];
                $result[$country_code][$customer_id."-2"][$order['order_id']]['converted_total_sales'] = $this->currency->convert($result[$country_code][$customer_id."-2"][$order['order_id']]['total_sales'] - $result[$country_code][$customer_id."-2"][$order['order_id']]['total_transaction_fees'],$order['currency_code'],$this->config->get('config_currency'));
                //$result[$country_code][$customer_id."-2"][$order['order_id']]['total_vat_value'] = $total_vat_value;

            }

            //$result[$country_code][$customer_id][$order['order_id']]['total_orders_count'] = count($total_orders_count[$country_code][$customer_id]);
            //$result[$country_code][$customer_id][$order['order_id']]['complete_orders_count'] = count($complete_orders_count[$country_code][$customer_id]);
            //$result[$country_code][$customer_id][$order['order_id']]['total_sales'] += $total_sales;
            //$result[$country_code][$customer_id][$order['order_id']]['total_commission'] +=$commission;
            //$result[$country_code][$customer_id][$order['order_id']]['total_transaction_fees'] += $fees;
            //$result[$country_code][$customer_id][$order['order_id']]['total_srpc'] += $total_srpc;
            //$result[$country_code][$customer_id][$order['order_id']]['total_seller'] += $total_seller;
        }

        foreach ($result as $country => $sellers) {

            $output[$country] = [];
            foreach ($sellers as $sllerId => $orders) {

                $output[$country][$sllerId] = [];
                $output[$country][$sllerId]['total_vat_value'] = 0;
                if ($orders['vat']['gross'] > 0) {
                    $output[$country][$sllerId]['total_vat_value'] = round(($orders['vat']['gross'] - (($orders['vat']['commission'] / 100) * $orders['vat']['gross']) - ($orders['vat']['flat_rate'] * $orders['vat']['cnt_vat_orders'])) * VAT_PERCENT, 2);
                }
                unset($orders['vat']);
                foreach ($orders as $orderId=> $oneOrder) {
                    if(!isset( $output[$country][$sllerId]['completed_order'])){
                        $output[$country][$sllerId]['completed_order'] = 0;
                    }
                    if(isset($output[$country][$sllerId]['orders_ids'])){
                        $output[$country][$sllerId]['orders_ids'] .=','.$this->commonfunctions->convertOrderNumber($orderId);
                    }else{
                        $output[$country][$sllerId]['orders_ids'] = $this->commonfunctions->convertOrderNumber($orderId);
                    }

                    if($oneOrder['order_status'] == 5 || $oneOrder['order_status'] == 22){
                            $output[$country][$sllerId]['completed_order']++;
                    }




                    $output[$country][$sllerId]['seller_id'] = $oneOrder['seller_id'];
                    $output[$country][$sllerId]['name'] = $oneOrder['name'];
                    $output[$country][$sllerId]['country'] = isset($oneOrder['country']) ? $oneOrder['country'] : '';





                        $output[$country][$sllerId]['commission'] = isset($oneOrder['commission']) ? $oneOrder['commission'] : 0;
                        $output[$country][$sllerId]['fbs_fullfillment_charges'] = isset($oneOrder['fbs_fullfillment_charges']) ? $oneOrder['fbs_fullfillment_charges'] : 0;
                        $output[$country][$sllerId]['fbs_storage_charges'] = isset($oneOrder['fbs_storage_charges']) ? $oneOrder['fbs_storage_charges'] : 0;

                    if (!isset($output[$country][$sllerId]['total_commission']))
                        $output[$country][$sllerId]['total_commission'] = isset($oneOrder['total_commission']) ? $oneOrder['total_commission'] : 0;
                    else
                        $output[$country][$sllerId]['total_commission'] += isset($oneOrder['total_commission']) ? $oneOrder['total_commission'] : 0;

                    if (!isset($output[$country][$sllerId]['total_sales']))
                        $output[$country][$sllerId]['total_sales'] = isset($oneOrder['total_sales']) ? $oneOrder['total_sales'] : 0;
                    else
                        $output[$country][$sllerId]['total_sales'] += isset($oneOrder['total_sales']) ? $oneOrder['total_sales'] : 0;

                    if (!isset($output[$country][$sllerId]['transaction_fees']))
                        $output[$country][$sllerId]['transaction_fees'] = isset($oneOrder['transaction_fees']) ? $oneOrder['transaction_fees'] : 0;
                    else
                        $output[$country][$sllerId]['transaction_fees'] += isset($oneOrder['transaction_fees']) ? $oneOrder['transaction_fees'] : 0;

                    if (!isset($output[$country][$sllerId]['total_transaction_fees']))
                        $output[$country][$sllerId]['total_transaction_fees'] = (isset($oneOrder['total_transaction_fees']) ? $oneOrder['total_transaction_fees'] : 0) + $output[$country][$sllerId]['fbs_fullfillment_charges'] + $output[$country][$sllerId]['fbs_storage_charges'];
                    else
                        $output[$country][$sllerId]['total_transaction_fees'] += (isset($oneOrder['total_transaction_fees']) ? $oneOrder['total_transaction_fees'] : 0) ;// Depends if they want the fullfillment & storage fees to be by order r by month $output[$country][$sllerId]['fbs_fullfillment_charges'] + $output[$country][$sllerId]['fbs_storage_charges'];

                    if (!isset($output[$country][$sllerId]['total_seller']))
                        $output[$country][$sllerId]['total_seller'] = isset($oneOrder['total_seller']) ? $oneOrder['total_seller'] : 0;
                    else
                        $output[$country][$sllerId]['total_seller'] += isset($oneOrder['total_seller']) ? $oneOrder['total_seller'] : 0;

                    if (!isset($output[$country][$sllerId]['converted_total_sales']))
                        $output[$country][$sllerId]['converted_total_sales'] = isset($oneOrder['converted_total_sales']) ? $oneOrder['converted_total_sales'] : 0;
                    else
                        $output[$country][$sllerId]['converted_total_sales'] += isset($oneOrder['converted_total_sales']) ? $oneOrder['converted_total_sales'] : 0;

                    /*if (!isset($output[$country][$sllerId]['total_vat_value']))
                        $output[$country][$sllerId]['total_vat_value'] = isset($oneOrder['total_vat_value']) ? $oneOrder['total_vat_value'] : 0;
                    else
                        $output[$country][$sllerId]['total_vat_value'] += isset($oneOrder['total_vat_value']) ? $oneOrder['total_vat_value'] : 0;*/



                }
            }

        }
        $data['orders'] = !empty($output) ? $output : array();


        $data['from_date'] = $date_start;
        $data['to_date'] = $date_end;
        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');
        $this->load->model('tool/image');
        $data['logo'] = HTTPS_IMAGE_S3 . $this->config->get('config_logo_ltr'); //$this->model_tool_image->resize($partner['avatar'], $this->config_image->get('partner_avatar','avatar','width'), $this->config_image->get('partner_avatar','avatar','hieght'), false);

        $data['title'] = $this->language->get('text_invoice');


        $this->response->setOutput($this->load->view('customerpartner/monthly_invoice.tpl', $data));
    }

    function validateDate($myDateString)
    {
        return (bool)strtotime($myDateString);
    }

}
