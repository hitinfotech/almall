<?php
require_once(DIR_ROOT.'vendors/TCPDF/tcpdf.php');

class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES.'mall-en.png';
        $this->Image($image_file, 15, 7, '', 22, 'PNG', '', 'T', false, 250, '', false, false, 0, false, false, false);

    }

    // Page footer
  public function Footer() {
      // Position at 15 mm from bottom
      $this->SetY(-20);
      // Set font
      $this->SetFont('helvetica', 'I', 8);

      $this->Cell(0, 10, '', 0, 1, 'C', 0, '', 0, false, 'T', 'M');
  }
}

class ControllerCustomerpartnerGenerateSellerMonthlyInvoice extends Controller {

    public function index() {

        $this->load->language('sale/order');
        $this->load->language('customerpartner/partner');

        $data['base'] = HTTPS_SERVER;

        $text_order_detail = $this->language->get('text_order_detail');

        if (isset($this->request->get['customer_id'])) {
            $partner_id = $this->request->get['customer_id'];
        } else {
            $partner_id = 0;
        }

        if (isset($this->request->get['date_start']) && $this->validateDate($this->request->get['date_start'])) {
            $date_start = $this->request->get['date_start'];
        } else {
            $date_start = date('01-m-Y');
        }

        if (isset($this->request->get['date_end']) && $this->validateDate($this->request->get['date_end'])) {
            $date_end = $this->request->get['date_end'];
        } else {
            $date_end =  date("Y-m-t", strtotime($date_start));
        }

        $invoice_file_name = $this->db->query("SELECT invoice_name FROM customerpartner_to_invoice WHERE customer_id=".(int)$partner_id.' AND month='.(int)date("m", strtotime($date_start)).' AND year= '.date("Y", strtotime($date_start)));
        if(!empty($invoice_file_name->row)){
          $this->response->redirect(HTTP_SERVER.'../image/'.$invoice_file_name->row['invoice_name']);
        }
        $file_name = $partner_id.'-'.date("m", strtotime($date_start))."-". date("Y", strtotime($date_start))."-".rand(1000, 10000);

        $this->load->model('sale/order');
        $this->load->model('customerpartner/partner');
        $this->load->model('setting/setting');
        $this->load->model('localisation/country');
        $this->load->model('customerpartner/order');


        $partner = $this->model_customerpartner_partner->getPartnerCustomerInfo($partner_id);

        if (empty($partner)) {
            die('seller Not Found');
        }
        $report_countries = $this->model_localisation_country->getAvailableCountries();

        $partner_desc = $this->model_customerpartner_partner->getPartnerDescription($partner_id);

        $company = $partner_desc[1]['companyname'];

        $days_per_month =  cal_days_in_month(CAL_GREGORIAN, date("m",strtotime($date_start)), date("y",strtotime($date_start)));


        $diff_for_date_range = round(abs(strtotime($partner['fbs_date_in']) - strtotime($date_start))/ 86400);

        $storage_fees = $partner['fbs_storage_charges'];

        if($partner['fbs_free_storage_days'] > $diff_for_date_range )
          $storage_fees = $partner['fbs_storage_charges'] - round(($partner['fbs_free_storage_days']-$diff_for_date_range) * ($partner['fbs_storage_charges'] / $days_per_month),2);

        if($storage_fees < 0)
          $storage_fees = 0;

        $conversion_volume = ($partner['fbs_inventory_volume_unit'] == 2 ) ? 0.093  : 1;
        $storage_fees = $storage_fees * $partner['fbs_inventory_volume'] * $conversion_volume;




        //$storage_fees_per_day = round($partner['fbs_storage_charges'] / ($days_per_month - $partner['fbs_free_storage_days']),2);
        $from_date = $date_start;
        $to_date = $date_end;
        $bank = $partner['otherpayment'];
        $seller_name = $partner['firstname'] . " " . $partner['lastname'];
        $flat_rate = $partner['flat_rate'];
        $data['is_seller_fbs'] = $partner['is_fbs'];
        $fbs_storage_charges = $storage_fees;//$partner['fbs_storage_charges'];
        //$data['fbs_storage_charges_per_day'] = $storage_fees_per_day;
        $fbs_free_storage_days = $partner['fbs_free_storage_days'];
        $data['days_per_month'] = $days_per_month;
        $fbs_fullfillment_charges = $partner['fbs_fullfillment_charges'];
        $email = $partner['email'];
        $phone = $partner['telephone'];
        $commission = $partner['commission'] . " %";
        $country = ($partner['country_id'] == 184 ? 'Saudi Arabia' : 'United Arab Emarites');
        $seller_currency = $partner['curcode'];

        $data['partner_returns'] = $this->model_customerpartner_partner->getReturns($this->request->get['customer_id'], $date_start, $date_end);

        $invoice_name = $result = substr($company, 0, 4) . $partner_id . date('F Y',strtotime($date_start)) .'-'.date('F Y',strtotime($date_end))  ;

        $data['title'] = $this->language->get('text_invoice') . " - " . $invoice_name;
        $converting_currency = $this->config->get('config_currency');



        $data['grand_total_quantity'] = 0;
        $data['grand_total_quantity'] = 0;
        $data['grand_total_total'] = 0;
        $data['grand_total_fees'] = 0;
        $data['grand_total_seller'] = 0;
        $data['grand_total_completed_order'] = 0;

        $data['grand_total_total_FBS'] = 0;
        $data['grand_total_quantity_FBS'] = 0;
        $data['grand_total_fees_FBS'] = 0;
        $data['grand_total_seller_FBS'] = 0;
        $data['grand_total_order_FBS'] = 0;

        $data['grand_total_completed_order'] = 0;
        $data['grand_total_completed_order_FBS'] = 0;
        $data['grand_total_sorder_FBS'] = 0;


        $data['grand_total_sorder_FBS'] = 0;

        $partner_orders = $this->model_customerpartner_partner->getPartnerOrders($this->request->get['customer_id'], $date_start, $date_end);
        foreach($partner_orders as $init_key => $init_value){
          $data['grand_total_completed_order_'.$init_value['currency_code']] = 0;
          $data['grand_total_sorder_'.$init_value['currency_code']] = 0;
          $data['grand_total_order_'.$init_value['currency_code']] = 0;
          $data['grand_total_seller_'.$init_value['currency_code']] = 0;
          $data['grand_total_fees_'.$init_value['currency_code']] = 0;
          $data['grand_total_total_'.$init_value['currency_code']] = 0;
          $data['grand_total_quantity_'.$init_value['currency_code']] = 0;
        }
        $grand_total_completed_order_overall = 0;
        $successfull_orders = array();
        $total_orders = array ();
        foreach ($partner_orders as $row) {


            if($row['order_product_status'] ==  5 && $this->model_customerpartner_partner->statusChangedBeforeRange($row['order_id'],$row['product_id'],5,$date_start, $date_end)){
                    continue;
            }
            $this->model_customerpartner_partner->update_order_product_filename($row['order_id'] ,$row['order_product_id'] ,$row['customer_id'] ,$file_name);

            if ($this->config->get('marketplace_complete_order_status')) {
                $marketplace_complete_order_status = $this->config->get('marketplace_complete_order_status');
            } else {
                $marketplace_complete_order_statuss = '5';
            }

            if ($this->config->get('marketplace_cancel_order_status')) {
                $marketplace_cancel_order_status = $this->config->get('marketplace_cancel_order_status');
            } else {
                $marketplace_cancel_order_status = '10';
            }

            $getWholeOrderStatus = $this->model_customerpartner_order->getWholeOrderStatus($row['order_id'],$marketplace_complete_order_status, $marketplace_cancel_order_status);
            $this->model_customerpartner_order->addOrderHistory($row['order_id'], array('order_status_id' => 22, 'notify'=>0,'comment' => 'Updated By seller Invoice'), '');
            if ($getWholeOrderStatus) {
                $this->model_customerpartner_order->changeWholeOrderStatus($row['order_id'], $getWholeOrderStatus);
            }

            $row['original_currency_code'] = $row['currency_code'];
            $row['currency_code'] = ($row['is_fbs'] == 1 ) ? "FBS": $row['currency_code'];

            $fees = 0;
            $successfull = 0;
            $total_orders[$row['order_id']] = 1;
            if (!in_array($row['order_id'] . '_' . $row['customer_id'] . '_' . $row['currency_code'], $successfull_orders)) {
                $successfull_orders[] = $row['order_id'] . '_' . $row['customer_id'] . '_' . $row['currency_code'];

                $successfull = 1;
                $info = $this->model_customerpartner_partner->getPartner($row['customer_id']);
                $fees =round( $this->currency->convert($info['flat_rate'],$partner['curcode'],$this->config->get('config_currency')),2);
            }

            $row['total'] = ($row['price']) . ' ' . $row['currency_code'];
            $row['customer_total'] = ($row['customer'] - $flat_rate) . ' ' . $row['currency_code'];
            $row['flat_rate'] = $flat_rate . ' ' . $row['currency_code'];
            $row['commission_applied'] = $row['commission_applied'] . ' %';
            $row['converted_total'] = 0;
            if ($row['order_product_status'] == 5) {
                $row['converted_total'] = $this->currency->convert($row['price'],$row['original_currency_code'],$this->config->get('config_currency'));

                $data['currency_code'] = $row['original_currency_code'];
                $data['grand_total_sorder_' . $row['currency_code']] +=1;
                $data['grand_total_completed_order_' . $row['currency_code']] += $successfull;
                $grand_total_completed_order_overall += $successfull;
                $data['grand_total_quantity_' . $row['currency_code']] += $row['quantity'];
                $data['grand_total_total_' . $row['currency_code']] +=($row['converted_total']);

                $data['grand_total_fees_' . $row['currency_code']] += $fees;
                $data['grand_total_seller_' . $row['currency_code']] += ($row['price']) - ($row['price'] * $row['commission_applied'] / 100) - $flat_rate;

                $data['grand_total_completed_order'] += $successfull;
                $data['grand_total_quantity'] += $row['quantity'];
                $data['grand_total_total'] +=($row['price'] * $row['quantity']);

                $data['grand_total_fees'] += $fees;
                $data['grand_total_seller'] += ($row['quantity'] * $row['price']) - ($row['quantity'] * $row['price'] * $row['commission_applied'] / 100) - $flat_rate;
            } else {
                $row['total'] = $row['flat_rate'] = $row['customer_total'] = $row['commission_applied'] = ' -- ';
            }
            $data['partner_orders_' . $row['currency_code']][] = $row;
            if(isset( $data['grand_total_order_' . $row['currency_code']]))
             $data['grand_total_order_' . $row['currency_code']] +=1;
            else
             $data['grand_total_order_' . $row['currency_code']] = 1;
        }
         $data['total_seller_amount_for_all'] = 0;
         foreach($report_countries as $key => $report_country){
            if(isset($data['grand_total_total_'.$report_country['curcode']]) && $data['grand_total_total_'.$report_country['curcode']] != 0){
              $data['total_seller_amount_for_all'] += round($data['grand_total_total_'.$report_country['curcode']] - ($commission * $data['grand_total_total_'.$report_country['curcode']] / 100 + $data['grand_total_fees_'.$report_country['curcode']] ),2);
            }
        }
        $data['total_seller_amount_for_all'] += round($data['grand_total_total_FBS'] - ($commission * $data['grand_total_total_FBS'] / 100 + $data['grand_total_fees_FBS'] ),2);
        $data['total_seller_amount_for_all'] = round($this->currency->convert($data['total_seller_amount_for_all'],$this->config->get('config_currency'),$partner['curcode']),2);
        $data['total_orders'] = count($total_orders);

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, [300,500], true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Sayidaty Mall');
        $pdf->SetTitle('Seller Agreeement');
        $pdf->SetSubject('Seller Agreeement');
        $pdf->SetKeywords('Sayidaty Mall, Seller, Agreement');

        // set default header data

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set margins
        $pdf->SetMargins(10, 32, 10);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set font
        $pdf->SetFont('dejavusans', '', 10);
        $pdf->AddPage();
        $seller_data = '
        <table border="0" cellspacing="2" cellpadding="1" align="right">
            <tr >
                <th><strong>'.$company .'</strong></th>
                <th><strong>From :</strong>'. $from_date .'</th>
            </tr>
            <tr>
                <th><strong>Country</strong> : '. $country.'</th>
                <th><strong>To   : </strong>'. $to_date .'</th>
            </tr>
            <tr>
                <th><strong>Contact</strong> : '. $seller_name .'</th>
                <th><strong>Invoice No.</strong> : '.$file_name  .'</th>
            </tr>
            <tr>
                <th><strong>Email</strong> : '. $email .'</th>
                <th><strong>Commission</strong> : '. $commission .'</th>
            </tr>
            <tr>
                <th><strong>Phone</strong> : '. $phone.'</th>
                <th><strong>Transaction Fees: </strong>'. $flat_rate .'</th>
            </tr>
            <tr>
                <th><strong>Total Orders</strong> : '. count($total_orders) .'</th>
                <th><strong>Storage Fees  : </strong>'. round($fbs_storage_charges * ($days_per_month - $fbs_free_storage_days )/$days_per_month,2).'</th>
            </tr>
            <tr>
                <th><strong>Successful Orders</strong> : '. ($grand_total_completed_order_overall) .'</th>
                <th><strong>FullFillment Fees</strong> : '. $fbs_fullfillment_charges .'</th>
            </tr>
            <tr>
              <th></th>
              <th><strong>Bank Info </strong><small>'.  $bank .'</small> </th>
            </tr>

        </table>';

        $pdf->writeHTML($seller_data, true, false, true, false, '');


        /*

        if (!empty($data['partner_orders_AED'])) {
            $partner_orders_AED =  $data['partner_orders_AED'];
            $aed_data = '<p style="font-size:20px;">UAE ' . $text_order_detail .'</p>

            <table  border="0.3" cellpadding="5" >
                <thead>
                    <tr>
                        <td class="text-left" style="width:70px"><b> Order-NO</b></td>
                        <td style="width:250px" ><b>'. $this->language->get('column_product') . ' / ' . $this->language->get('column_sku') .'</b></td>
                        <td class="text-left" style="width:70px"><b>'.$this->language->get('column_quantity').'</b></td>
                        <td class="text-left" style="width:120px"><b> Date </b></td>
                        <td class="text-left" style="width:120px"><b> Status </b></td>
                        <td class="text-left" style="width:120px"><b> Total Sales </b></td>
                    </tr>
                </thead>
                <tbody>';
                 if ($partner_orders_AED) {
                     foreach ($partner_orders_AED as $order) {
                        $aed_data .='<tr>
                                        <td style="width:70px"> '.$order['order_id'].'</td>
                                        <td style="width:250px" > '. $order['name'] . ' - ' . $order['sku'];
                        $aed_data .=( $order['is_fbs']) ?"<p><b>Bin: ".$order['fbs_bin'].", Stack: ".$order['fbs_stack']."</b></p>" :"" ;
                        $aed_data .= '</td>
                                        <td class="text-left" style="width:70px"> '.$order['quantity'].'</td>
                                        <td class="text-left" style="width:120px">'. date('Y-m-d', strtotime($order['date_added'])).'</td>
                                        <td class="text-left" style="width:120px">'. $order['status'] .'</td>
                                        <td class="text-left" style="width:120px">'. round($order['total'],3) .'</td>
                                    </tr>';
                       }
                        $aed_data .='
                        <tr>
                            <td class="text-right" colspan="2"><strong>Total No Of Items</strong></td>
                            <td><strong>'. $data['grand_total_quantity_AED'] .'</strong></td>
                            <td class="text-right" colspan="2"><strong>Total Sales </strong></td>
                            <td><strong>'. round($data['grand_total_total_AED'],3) .' AED</strong></td>
                        </tr>
                        <tr>
                            <td class="text-right" colspan="2"><strong>
                                Total No. of successfully orders (completed)</strong>
                            </td>
                            <td class="text-left"><strong>'. $data['grand_total_completed_order_AED']  .'</strong>
                            </td>

                            <td class="text-right" colspan="2"><strong>Total Transaction Fees</strong></td>
                            <td><strong>'. $data['grand_total_fees_AED'] .' AED</strong></td>
                        </tr>
                        <tr>
                            <td class="text-right" colspan="5" align="right"><strong>Total Commission </strong></td>
                            <td><strong>'. round($commission * $data['grand_total_total_AED'] / 100,3) .'AED</strong></td>
                        </tr>
                        <tr>
                            <td class="text-right" colspan="5" align="right"><strong>'.$this->language->get('total_transaction_fees').'</strong></td>
                            <td><strong>'. round($data['grand_total_total_AED'] - ($commission * $data['grand_total_total_AED'] / 100 + $data['grand_total_fees_AED'] ),3 ).' AED</strong></td>
                        </tr>';

                     } else {
                        $aed_data .= '<tr><td class="text-center" colspan="9"><?php echo $text_no_results; ?></td></tr>';

                      }

                $aed_data .= '</tbody></table>';
            $pdf->writeHTML($aed_data, true, false, true, false, '');
          }

          if (!empty($data['partner_orders_SAR'])) {
              $partner_orders_SAR =  $data['partner_orders_SAR'];
              $SAR_data = '<p style="font-size:20px;">KSA ' . $text_order_detail .'</p>

              <table border="0.3" cellpadding="5" >
                  <thead>
                      <tr>
                          <td class="text-left" style="width:70px"><b> Order-NO</b></td>
                          <td style="width:250px" ><b>'. $this->language->get('column_product') . ' / ' . $this->language->get('column_sku') .'</b></td>
                          <td class="text-left" style="width:70px"><b>'.$this->language->get('column_quantity').'</b></td>
                          <td class="text-left" style="width:120px"><b> Date </b></td>
                          <td class="text-left" style="width:120px"><b> Status </b></td>
                          <td class="text-left" style="width:120px"><b> Total Sales </b></td>
                      </tr>
                  </thead>
                  <tbody>';
                   if ($partner_orders_SAR) {
                       foreach ($partner_orders_SAR as $order) {
                          $SAR_data .='<tr>
                                          <td class="text-right" style="width:70px"> '.$order['order_id'].'</td>
                                          <td class="text-left" style="width:250px"> '. $order['name'] . ' - ' . $order['sku'];
                          $SAR_data .=( $order['is_fbs']) ?"<p><b>Bin: ".$order['fbs_bin'].", Stack: ".$order['fbs_stack']."</b></p>" :"" ;
                          $SAR_data .= '</td>
                                          <td class="text-left" style="width:70px"> '.$order['quantity'].'</td>
                                          <td class="text-left" style="width:120px">'. date('Y-m-d', strtotime($order['date_added'])).'</td>
                                          <td class="text-left" style="width:120px">'. $order['status'] .'</td>
                                          <td class="text-left" style="width:120px">'. round($order['total'],3) .'</td>
                                      </tr>';
                         }
                          $SAR_data .='
                          <tr>
                              <td class="text-right" colspan="2"><strong>Total No Of Items</strong></td>
                              <td><strong>'. $data['grand_total_quantity_SAR'] .'</strong></td>
                              <td class="text-right" colspan="2"><strong>Total Sales</strong></td>
                              <td><strong>'. round($data['grand_total_total_SAR'],3) .' SAR</strong></td>
                          </tr>
                          <tr>
                              <td class="text-right" colspan="2"><strong>
                                  Total No. of successfully orders (completed)</strong>
                              </td>
                              <td class="text-left"><strong>
                                  '.$data['grand_total_completed_order_SAR'].'</strong>
                              </td>

                              <td class="text-right" colspan="2"><strong>Total Transaction Fees</strong></td>
                              <td><strong>'. $data['grand_total_fees_SAR'] .' SAR</strong></td>
                          </tr>
                          <tr>
                              <td class="text-right" colspan="5" align="right"><strong> Total Commission</strong></td>
                              <td><strong>'. round($commission * $data['grand_total_total_SAR'] / 100,3) .' SAR</strong></td>
                          </tr>
                          <tr>
                              <td class="text-right" colspan="5" align="right"><strong>'.$this->language->get('total_transaction_fees').'</strong></td>
                              <td><strong>'. round($data['grand_total_total_SAR'] - ($commission * $data['grand_total_total_SAR'] / 100 + $data['grand_total_fees_SAR'] ),3 ) .' SAR</strong></td>
                          </tr>';

                       } else {
                          $SAR_data .= '<tr><td class="text-center" colspan="9"><?php echo $text_no_results; ?></td></tr>';

                        }

                  $SAR_data .= '</tbody></table>';
              $pdf->writeHTML($SAR_data, true, false, true, false, '');
            }*/

           foreach($report_countries as $key => $country){
             $country_data = '';
               if (!empty($data['partner_orders_'.$country['curcode']])) {
                  $country_data = "<p style=\"font-size:20px;\">". $country['name_old']. " " . $text_order_detail." </p>";

                  $country_data .= '<table border="0.3" cellpadding="5">
                      <thead>
                          <tr>
                              <td class="text-left" style="width:80px"><b> Order-NO </b></td>
                              <td style="width:250px"><b>'. $this->language->get('column_product') . ' / ' . $this->language->get('column_sku'). ' / ' . $this->language->get('column_model') . '</b></td>
                              <td class="text-left" style="width:70px"><b>'. $this->language->get('column_quantity') .'</b></td>
                              <td class="text-left" style="width:100px"><b>Date</b></td>
                              <td class="text-left" style="width:100px"><b>Status</b></td>
                              <td class="text-left" style="width:100px"><b>Total Sales  '.$country['curcode'] .'</b></td>
                              <td class="text-left" style="width:100px"><b>Total Sales ('.$converting_currency.')</b></td>
                        </tr>
                      </thead>
                      <tbody>';
                      if ($data['partner_orders_'.$country['curcode']]) {
                           foreach ($data['partner_orders_'.$country['curcode']] as $order) {
                                $country_data .=' <tr>
                                      <td class="text-left" style="width:80px">.'.$this->commonfunctions->convertOrderNumber($order['order_id']) .'</td>
                                      <td class="text-left" style="width:250px">'. $order['name'] . ' - ' . $order['sku'] . ' - ' . $order['model'].'</td>
                                      <td class="text-left" style="width:70px">'. $order['quantity']. '</td>
                                      <td class="text-left" style="width:100px">'. date('Y-m-d', strtotime($order['date_added'])) .'</td>
                                      <td class="text-left" style="width:100px">'. $order['status'] .'</td>
                                      <td class="text-left" style="width:100px">'. round($order['total'],3).'</td>
                                      <td class="text-left" style="width:100px">'. round($order['converted_total'],3).'</td>
                                  </tr>';
                              }
                              $country_data .='
                              <tr>
                                  <td class="text-right" colspan="2" align="right"><strong>Total No Of Items</strong></td>
                                  <td><strong>'. $data['grand_total_quantity_'.$country['curcode']] .'</strong></td>
                                  <td class=\'text-right\' colspan="3" align="right"  ><strong>Total Sales</strong></td>
                                  <td><strong>'. round($data['grand_total_total_'.$country['curcode']],3) ." ".$converting_currency .'</strong></td>
                              </tr>
                              <tr>
                                  <td class="text-right" colspan="2" align="right" ><strong>
                                      Total No. of successfully orders (completed)</strong>
                                  </td>
                                  <td class="text-left"><strong>
                                      '. $data['grand_total_completed_order_'.$country['curcode']]  .'</strong>
                                  </td>

                                  <td class=\'text-right\' colspan="3" align="right"><strong>Total Transaction Fees</strong></td>
                                  <td><strong> '. $data['grand_total_fees_'.$country['curcode']] ." ".$converting_currency .'</strong></td>
                              </tr>
                              <tr>
                                  <td class=\'text-right\' colspan="6" align="right"><strong> Total Commission </strong></td>
                                  <td><strong>'. round($commission * $data['grand_total_total_'.$country['curcode']] / 100,3) ." ".$converting_currency .' </strong></td>
                              </tr>
                              <tr>
                                  <td class=\'text-right\' colspan="6" align="right"><strong>'. $this->language->get('total_transaction_fees') .'</strong></td>
                                  <td><strong>'. round($data['grand_total_total_'.$country['curcode']] - ($commission * $data['grand_total_total_'.$country['curcode']] / 100 + $data['grand_total_fees_'.$country['curcode']] ),3 )  ." ".$converting_currency .'</strong></td>
                              </tr>';

                           } else {
                              $country_data .='<tr><td class="text-center" colspan="9">'; $text_no_results .'</td></tr>';
                          }
                          $country_data .='</tbody>
                                  </table>';



              $pdf->writeHTML($country_data, true, false, true, false, '');
            }
           }

            if (!empty($data['partner_orders_FBS'])) {
                $partner_orders_FBS =  $data['partner_orders_FBS'];
                $FBS_data = '<p style="font-size:20px;">FBS ' . $text_order_detail .'</p>

                <table border="0.3" cellpadding="5" >
                    <thead>
                        <tr>
                            <td class="text-left" style="width:80px"><b> Order-NO</b></td>
                            <td style="width:250px" ><b>'. $this->language->get('column_product') . ' / ' . $this->language->get('column_sku') .'</b></td>
                            <td class="text-left" style="width:70px"><b>'.$this->language->get('column_quantity').'</b></td>
                            <td class="text-left" style="width:100px"><b> Date </b></td>
                            <td class="text-left" style="width:100px"><b> Status </b></td>
                            <td class="text-left" style="width:100px"><b> Total Sales  (Local currency) </b></td>
                            <td class="text-left" style="width:100px"><b> Total Sales</b></td>
                        </tr>
                    </thead>
                    <tbody>';
                     if ($partner_orders_FBS) {
                         foreach ($partner_orders_FBS as $order) {
                            $FBS_data .='<tr>
                                            <td class="text-right" style="width:80px"> '.$this->commonfunctions->convertOrderNumber($order['order_id']).'</td>
                                            <td class="text-left" style="width:250px"> '. $order['name'] . ' - ' . $order['sku'];
                            $FBS_data .=( $order['is_fbs']) ?"<p><b>Bin: ".$order['fbs_bin'].", Stack: ".$order['fbs_stack']."</b></p>" :"" ;
                            $FBS_data .= '</td>
                                            <td class="text-left" style="width:70px"> '.$order['quantity'].'</td>
                                            <td class="text-left" style="width:100px">'. date('Y-m-d', strtotime($order['date_added'])).'</td>
                                            <td class="text-left" style="width:100px">'. $order['status'] .'</td>
                                            <td class="text-left" style="width:100px">'. round($order['total'],3) . ' '.$order['original_currency_code'] .'</td>
                                            <td class="text-left" style="width:100px">'. round($order['converted_total'],3) .'</td>
                                        </tr>';
                           }
                            $FBS_data .='
                            <tr>
                                <td class="text-right" colspan="2" align="right"><strong>Total No Of Items</strong></td>
                                <td><strong>'. $data['grand_total_quantity_FBS'] .'</strong></td>
                                <td class="text-right" colspan="3" align="right"><strong>Total Sales</strong></td>
                                <td><strong>'. round($data['grand_total_total_FBS'],3) .' SAR</strong></td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan="2" align="right"><strong>
                                    Total No. of successfully orders (completed)</strong>
                                </td>
                                <td class="text-right" align="right"><strong>
                                    '.$data['grand_total_completed_order_FBS'].'</strong>
                                </td>

                                <td class="text-right" colspan="3" align="right" ><strong>Total Transaction Fees</strong></td>
                                <td><strong>'. $data['grand_total_fees_FBS'] .' SAR</strong></td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan="6" align="right"><strong> Total Commission</strong></td>
                                <td><strong>'. round($commission * $data['grand_total_total_FBS'] / 100,3) .' SAR</strong></td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan="6" align="right"><strong>Fulfillment Charges </strong></td>
                                <td><strong>'. $fbs_fullfillment_charges * $data['grand_total_completed_order_FBS']  .' SAR</strong></td>
                            </tr>

                            <tr>
                                <td class="text-right" colspan="6" align="right"><strong>Storage Charges </strong></td>
                                <td><strong>'. round($fbs_storage_charges ,2) .' SAR</strong></td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan="6" align="right"><strong>'.$this->language->get('total_transaction_fees').'</strong></td>
                                <td><strong>'. round($data['grand_total_total_FBS'] - ($commission * $data['grand_total_total_FBS'] / 100 + $data['grand_total_fees_FBS'] ) - ($fbs_fullfillment_charges * $data['grand_total_completed_order_FBS'] ) - $fbs_storage_charges ,2)  .' SAR</strong></td>
                            </tr>';

                         } else {
                            $FBS_data .= '<tr><td class="text-center" colspan="9"><?php echo $text_no_results; ?></td></tr>';

                          }

                    $FBS_data .= '</tbody></table>';
                $pdf->writeHTML($FBS_data, true, false, true, false, '');
              }

              $text_more_data = '
              <h3><b> Final amount payable : '.$data['total_seller_amount_for_all'] . " ". $seller_currency. '</b></h3>
              <br><p>- Total transaction  = Fees (No. of successful order * transaction fee)</p>
              <p>- Total Commission  = (comission * total Sales)</p>
              <p>- Storage Charges  = fbs_storage_charges * (days_per_month - fbs_free_storage_days )/days_per_month</p>
              <p>- Fulfillment Charges  = (fbs_fullfillment_charges * FBS_total_completed_order)</p>
              <p>- Seller Amount  = (Total Sales - Commission - Transaction Fees)</p>';

              $pdf->writeHTML($text_more_data, true, false, true, false, '');



              $text_footer = '<div class="col-md-12 col-sm-12 col-xs-12"><br/><br/>
                  <p style="font-size:15px;">'. $this->language->get('text_more_details') .'</p></div>';

              $pdf->writeHTML($text_footer, true, false, true, false, '');



          $this->db->query("INSERT INTO customerpartner_to_invoice SET customer_id=".(int)$partner_id.', month='.(int)date("m", strtotime($date_start)).', year= '.date("Y", strtotime($date_start)).', invoice_name="'. $this->db->escape('seller_monthly_invoices/'.$file_name.'.pdf').'", user_id='.(int) $this->user->getId());

        $pdf->Output(DIR_IMAGE.'seller_monthly_invoices/'.$file_name.'.pdf', 'F');
        $pdf->Output(DIR_IMAGE.'seller_monthly_invoices/'.$file_name.'.pdf', 'I');



    }

    function validateDate($myDateString){
        return (bool)strtotime($myDateString);
      }


}
