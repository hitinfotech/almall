<?php

class ControllerCustomerpartnerLogin extends Controller {

    public function index() {
        $json = array();

        if (isset($this->request->get['partner_id'])) {
            $partner_id = $this->request->get['partner_id'];
        } else {
            $partner_id = 0;
        }

        $this->load->model('customerpartner/partner');

        $partner_info = $this->model_customerpartner_partner->getPartner($partner_id);

        if ($partner_info) {
            // Create token to login with
            $token = token(64);

            $this->model_customerpartner_partner->editToken($partner_id, $token);

            if (isset($this->request->get['store_id'])) {
                $store_id = $this->request->get['store_id'];
            } else {
                $store_id = 0;
            }

            $this->load->model('setting/store');

            $store_info = $this->model_setting_store->getStore($store_id);

            if ($store_info) {
                $this->response->redirect($store_info['url'] . 'index.php?route=account/login&token=' . $token);
            } else {
                $this->response->redirect(HTTP_CATALOG . 'partner/index.php?route=account/login&token=' . $token);
            }
        } else {
            $this->load->language('error/not_found');

            $this->document->setTitle($this->language->get('heading_title'));

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_not_found'] = $this->language->get('text_not_found');

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('error/not_found.tpl', $data));
        }
    }

}
