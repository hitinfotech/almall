<?php
class ControllerCustomerpartnerVatinvoice extends Controller
{
    public function index(){

        $this->load->language('sale/order');

        $this->load->model('setting/setting');
        $this->load->model('localisation/country');

        $data['base'] = HTTPS_SERVER;
        if (isset($this->request->get['customer_id'])) {
            $partner_id = (int)$this->request->get['customer_id'];
        } else {
            $partner_id = 0;
        }

        if (isset($this->request->get['date_start']) && $this->validateDate($this->request->get['date_start'])) {
            $date_start = $this->request->get['date_start'];
        } else {
            $date_start = date('01-m-Y');
        }

        if (isset($this->request->get['date_end']) && $this->validateDate($this->request->get['date_end'])) {
            $date_end = $this->request->get['date_end'];
        } else {
            $date_end =  date("Y-m-t", strtotime($date_start));
        }

        $this->load->model('customerpartner/partner');

        $partner = $this->model_customerpartner_partner->getPartnerCustomerInfo($partner_id);
        $file_name = $partner_id.'-'.date("m", strtotime($date_start))."-". date("Y", strtotime($date_start))."-".rand(1000, 10000);
        $this->db->query("INSERT INTO customerpartner_to_invoice SET customer_id=".(int)$partner_id.', month='.(int)date("m", strtotime($date_start)).', year= '.date("Y", strtotime($date_start)).', invoice_name="'.$file_name.'", user_id='.(int) $this->user->getId());

        if (empty($partner)) {
            die('seller Not Found');
        }
        $data['report_countries'] = $this->model_localisation_country->getAvailableCountries();

        $data['from_date'] = $date_start;
        $data['to_date'] = $date_end;
        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');
        $this->load->model('tool/image');
        $data['logo'] = $this->model_tool_image->resize($partner['avatar'], $this->config_image->get('partner_avatar','avatar','width'), $this->config_image->get('partner_avatar','avatar','hieght'), false);
        
        $financial_data = '';
        $financial_data .= trim($partner['beneficiary_name'])!=''?'Beneficiary Name : '.$partner['beneficiary_name'] . ', ':'';
        $financial_data .= trim($partner['bank_name'])!=''?'Bank Name : ' .$partner['bank_name']:'';
        $financial_data .= trim($partner['bank_account'])!=''?'<br>Bank Account : ' .$partner['bank_account']:'';
        $financial_data .= trim($partner['iban_code'])!=''?'<br>IBan code : ' .$partner['iban_code'] . ', ':'';
        $financial_data .= trim($partner['swift_code'])!=''?'Swift code : ' .$partner['swift_code']:'';

        $data['bank'] = $financial_data;
        $data['seller_name'] = $partner['firstname'] . " " . $partner['lastname'];
        $data['seller_id'] = $partner['customer_id'];
        $data['flat_rate'] = $partner['flat_rate'];
        $data['seller_currency'] = $partner['curcode'];
        $data['is_seller_fbs'] = $partner['is_fbs'];

        $data['fbs_free_storage_days'] = $partner['fbs_free_storage_days'];
        $data['fbs_fullfillment_charges'] = $partner['fbs_fullfillment_charges'];
        $data['email'] = $partner['fi_email'];
        $data['phone'] = $partner['telephone'];
        $data['commission'] = $partner['commission'] . " %";

        $data['invoice_name'] = $file_name;
        $data['invoice_date'] = date("Y-m-d");
        $data['invoice_period'] = $date_start . ' to ' .$date_end;

        $data['company'] = $partner['company_name'];
        $data['sellect_reg_address'] = !is_null($partner['address_license_number'])?$partner['address_license_number']:'';
        $data['country'] = 'United Arab Emarites';
        $data['buyer_name'] = 'Saudi Research & Publishing Company.';

        if (!empty($partner['country_id'])) {
            $country = $this->db->query("SELECT name FROM country_description WHERE country_id = ". $partner['country_id'] ." AND language_id = ". (int) $this->config->get('config_language_id') ." ");
            if ($country->num_rows) {
                $data['country'] = $country->row['name'];
                if ($partner['country_id'] == 184) {
                    $data['buyer_address'] = 'Motamarat Area, Makkah Road, Riyadh, P.O. Box 478, 11411';
                    $data['buyer_trn'] = KSA_VAT_NUMBER;
                } elseif ($partner['country_id'] == 221) {
                    $data['buyer_address'] = 'Dubai media city, CNN Building, Office # 506, P.O Box 502038';
                    $data['buyer_trn'] = UAE_VAT_NUMBER;
                }
            }
        }

        $partner_orders = $this->model_customerpartner_partner->getPartnerOrders($partner_id, $date_start, $date_end);

        $order_info = [];
        $data['orders_info'] = [];

        $sub_total = 0;
        $vat_percent = 0.05;
        $arr_total_po = [];
        $arr_total_po['gross'] = 0;
        $arr_total_po['order_ids'] = [];
        $arr_total_po['order_vat_ids'] = [];
        $total_po = 0;
        $total_po_with_vat = 0;
        foreach ($partner_orders as $order) {
            if($order['order_product_status'] ==  5 && $this->model_customerpartner_partner->statusChangedBeforeRange($order['order_id'],$order['product_id'],5,$date_start, $date_end)){
                continue;
            }

            if ($order['order_product_status'] == 5 || $order['order_product_status'] == 22) {

                if (!in_array($order['order_id'],$arr_total_po['order_ids'])) {
                    $arr_total_po['order_ids'][] = $order['order_id'];
                    $total_po++;
                }
                if ($order['date_added'] > date('Y').'-01-01 00:00:00') {
                    $arr_total_po['gross'] += round($this->currency->convert($order['price'], $order['currency_code'], $partner['curcode']), 2);
                    if (!in_array($order['order_id'],$arr_total_po['order_vat_ids'])) {
                        $arr_total_po['order_vat_ids'][] = $order['order_id'];
                        $total_po_with_vat++;
                    }
                }


                $order_info['date_completed'] = 'Not Completed';
                $date_completed = $this->model_customerpartner_partner->getCompletedOrderDate($order['order_id'],$order['product_id']);
                if ($date_completed)
                    $order_info['date_completed'] = date_format(new DateTime($date_completed),'Y-m-d');
                else
                    $order_info['date_completed'] = $order['status'];

                $order_info['date_pending'] = '';
                $date_pending = $this->model_customerpartner_partner->getPendingOrderDate($order['order_id']);
                if ($date_pending)
                    $order_info['date_pending'] = date_format(new DateTime($date_pending),'Y-m-d');

                $order_info['order_id'] = $this->commonfunctions->convertOrderNumber($order['order_id']);
                $order_info['desc'] = $order['name'] . ' - ' . $order['sku'] . ' - ' . $order['model'];
                $order_info['quantity'] = $order['quantity'];
                $order_info['unit_price'] = round($this->currency->convert($order['price']/$order['quantity'],$order['currency_code'],$partner['curcode']),2);
                $order_info['amount'] = round($this->currency->convert($order['price'],$order['currency_code'],$partner['curcode']), 2);
                $sub_total += $order_info['amount'];

                $data['orders_info'][] = $order_info;
            }

        }
        $discount = $sub_total * ($partner['commission']/100);
        $data['processing_fees_amount'] = $partner['flat_rate'];
        $data['processing_fees'] = $total_po * $partner['flat_rate'];
        $data['warehousing_units']=$data['warehousing_fees']=$data['handling_fee_amount']=$data['handling_fee']=$data['total_vat_value']=$data['warehousing_amount']=0;
        if ($partner['is_fbs']) {
            $data['warehousing_units'] = $partner['fbs_inventory_volume'];
            $data['warehousing_amount'] = $partner['fbs_storage_charges'];
            $data['warehousing_fees'] = round($partner['fbs_storage_charges'] * $partner['fbs_inventory_volume'],2);

            $data['handling_fee_amount'] = $partner['fbs_fullfillment_charges'];
            $data['handling_fee'] = round($partner['fbs_fullfillment_charges'] * $total_po_with_vat,2);
        }
        $total_deductions = $discount + $data['processing_fees'] + $data['warehousing_fees'] + $data['handling_fee'];
        $gross = $sub_total - $total_deductions;
        $data['trn']='';
        $eligible_vat_info = $this->db->query("SELECT * FROM eligible_vat WHERE customer_id = '" . (int) $partner_id . "' AND checked=1");
        if ($eligible_vat_info->num_rows) {
            $data['trn'] = $eligible_vat_info->row['tax_registration_number'];
            $data['total_vat_value'] = round(($arr_total_po['gross']-(($partner['commission']/100)*$arr_total_po['gross'])-($data['processing_fees_amount']*$total_po_with_vat)) * $vat_percent,2);
            //$data['total_vat_value'] = round($arr_total_po['gross']* $vat_percent,2);

        }
        $data['sub_total'] = round($sub_total,2);
        $data['discount'] = round($discount,2);
        $data['discount_percent'] = $partner['commission'] . "%";
        $data['total_deductions'] = round($total_deductions,2);
        $data['gross'] = round($gross,2);
        $data['total'] = round($data['total_vat_value'] + $gross,2);
        $data['total_po_with_vat'] = $total_po_with_vat;
        $data['total_po'] = $total_po;
        $data['total_in_words'] = 'One Thousand Two Hundred Ninety One Dirhams and Fifty Fils Only';

        $invoice_name = substr($data['company'], 0, 4) . $partner_id . date('F Y',strtotime($date_start)) .'-'.date('F Y',strtotime($date_end))  ;

        $data['title'] = $this->language->get('text_invoice') . " - " . $invoice_name;

        $this->response->setOutput($this->load->view('customerpartner/vat_invoice.tpl', $data));
    }

    public function validateDate($myDateString){
        return (bool)strtotime($myDateString);
    }

    public function convertNumberToWord($num = false)
    {
        $num = str_replace(array(',', ' '), '' , trim($num));
        if(! $num) {
            return false;
        }
        $num = (int) $num;
        $words = array();
        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
            'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
        );
        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
        $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
            'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
            'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
        );
        $num_length = strlen($num);
        $levels = (int) (($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);
        for ($i = 0; $i < count($num_levels); $i++) {
            $levels--;
            $hundreds = (int) ($num_levels[$i] / 100);
            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
            $tens = (int) ($num_levels[$i] % 100);
            $singles = '';
            if ( $tens < 20 ) {
                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
            } else {
                $tens = (int)($tens / 10);
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int) ($num_levels[$i] % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
        } //end for loop
        $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }
        return implode(' ', $words);
    }
}


