<?php
require_once(DIR_ROOT.'vendors/TCPDF/tcpdf.php');

  class MYPDF extends TCPDF {
      //Page header
      public function Header() {
          // Logo
          $image_file = K_PATH_IMAGES.'mall-en.png';
          $image_file2 = K_PATH_IMAGES.'mall-ar.png';
          $this->Image($image_file, 45, 7, '', 12, 'PNG', '', 'T', false, 250, '', false, false, 0, false, false, false);
          $this->Image($image_file2, 200, 7, '', 12, 'PNG', '', 'T', false, 250, '', false, false, 0, false, false, false);
      }

      // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-20);
        // Set font
        $this->SetFont('helvetica', 'I', 8);

        $this->Cell(0, 10, 'FIRST PARTY’S INITIALS: _____                  SECOND PARTY’S INITIALS: _____', 0, 1, 'C', 0, '', 0, false, 'T', 'M');

        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
  }


  class ControllerCustomerpartnerShowcontract extends Controller{
    public function index(){

      $legal_name = $commercial_name = $business_license_num = $location ='';

      if(isset($this->request->get['customer_id'])){
        $this->load->model('customerpartner/partner');
        $partner_info = $this->model_customerpartner_partner->getPartner($this->request->get['customer_id']);
        $legal_name = (isset($partner_info['legal_name']) ? $partner_info['legal_name'] : '');
        $commercial_name = (isset($partner_info['company_name']) ? $partner_info['company_name'] : '');
        $location = (isset($partner_info['address_license_number']) ? $partner_info['address_license_number'] : '');
        $business_license_num = (isset($partner_info['trade_license_number']) ? $partner_info['trade_license_number'] : '');
        //$date_approved = (isset($partner_info['date_approved']) ? date('d/m/Y', strtotime($partner_info['date_approved'])) : '');
        $date_approved = (isset($partner_info['contract_date']) ? date('d/m/Y', strtotime($partner_info['contract_date'])) : '');

      }
      $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Sayidaty Mall');
      $pdf->SetTitle('Seller Agreeement');
      $pdf->SetSubject('Seller Agreeement');
      $pdf->SetKeywords('Sayidaty Mall, Seller, Agreement');

      // set default header data

      // set header and footer fonts
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

      // set margins
      $pdf->SetMargins(10, 24, 10);

      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      // set some language dependent data:
      $lg = Array();
      $lg['a_meta_charset'] = 'UTF-8';
      $lg['a_meta_dir'] = 'rtl';
      $lg['a_meta_language'] = 'fa';
      $lg['w_page'] = 'page';

      // set some language-dependent strings (optional)
      $pdf->setLanguageArray($lg);

      // ---------------------------------------------------------

      // set font
      $pdf->SetFont('dejavusans', '', 10);

      // add a page
      $pdf->AddPage();


      // create some HTML content
      $html = '
    <table style="width: 100%;"cellpadding="10 ;border-color:#ddd">
      <tr style="width: 100%; background-color: #EEEEEE ;"><td style="padding: 15px;" align="right"><h1>الشروط والاحكام</h1></td><td align="left"><h1>Terms and Conditions</h1></td></tr>
      <tr style="font-size: medium; color: #064998"><td align="right">هذه الشروط والاحكام تحكم العلاقة بين: </td><td align="left">These terms and conditions governs the relationship between you, </td></tr>

      <tr style="font-size: small;"><td align="right">الاسم القانوني للتاجر (وفقا للسجل التجاري/ الرخصةالتجارية):</td><td align="left">Merchant Legal Name (as per business license): </td></tr>
      <tr style="font-size: small;" style="background-color: #EEEEEE ;padding: 15px;"><td align="left" colspan="2"> <input value="" /> Sayidaty Mall</td></tr>

      <tr style="font-size: small;"><td align="right">يملك سجل تجاري رقم:</td><td align="left">Having Business License Number: </td></tr>
      <tr style="font-size: small;" style="background-color: #EEEEEE ;padding: 15px;"><td align="left" colspan="2"> <input value="" /> 30041 </td></tr>

      <tr style="font-size: small;"><td align="right">مقره في:</td><td align="left">Located at: </td></tr>
      <tr style="font-size: small;" style="background-color: #EEEEEE ;padding: 15px;"><td align="left" colspan="2"> <input value="" /> CNN Building No.2, 5th Floor, SRPC Dubai Media City,, P.O Box 502038, Dubai,, UAE</td></tr>

      <tr style="font-size: large; color: #064998"><td align="right"> و</td><td align="left">And </td></tr>

      <tr style="font-size: small;"><td align="right">الاسم القانوني (وفقا لسجل التجاري/ الرخصة التجارية):</td><td align="left">Legal Name:(as per business license): </td></tr>
      <tr style="font-size: small;" style="background-color: #EEEEEE ;padding: 15px;"><td align="left" colspan="2"> <input value="" /> '.$legal_name.'</td></tr>

      <tr style="font-size: small;"><td align="right">الاسم التجاري:</td><td align="left">Commercial Name: </td></tr>
      <tr style="font-size: small;" style="background-color: #EEEEEE ;padding: 15px;"><td align="left" colspan="2"> <input value="" /> '.$commercial_name.' </td></tr>

      <tr style="font-size: small;"><td align="right">يملك سجل تجاري رقم:</td><td align="left">Having Business License Number: </td></tr>
      <tr style="font-size: small;" style="background-color: #EEEEEE ;padding: 15px;"><td align="left" colspan="2"> <input value="" /> '.$business_license_num.' </td></tr>

      <tr style="font-size: small;"><td align="right">مقره في:</td><td align="left">Located at: </td></tr>
      <tr style="font-size: small;" style="background-color: #EEEEEE ;padding: 15px;"><td align="left" colspan="2"> <input value="" /> '.$location.' </td></tr>

      <br>
      <tr style="font-size: medium;"><td align="left" colspan="2"> First Party and Second Party are collectively referred to as the ‘Parties’ </td></tr>
      </table>';
      $pdf->writeHTML($html, true, false, true, false, '');

      $pdf->AddPage();
      $html = '<style>
            </style><table style="width: 100%; border-collapse: collapse; ;border-color:#ddd" cellpadding="10" cellspacing="0">

      <tr style="font-size: small; border:none;"><td align="right"></td>
      	<td align="left">
        and is legally and binding on you by applying for the Services (as defined below) through the online application on the the Website (as defined below). You have to click the ‘Agree to the Terms and Conditions’ button to confirm that you have read and understood these Terms and Conditions and agreed to be bound by their terms.<br>
You agree to use our Services to sell your Items in the permitted market through the Site
      	</td>
      	</tr>
      </table>';

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      $html = '<table style="width: 100%; border-collapse: collapse;" cellpadding="10" cellspacing="0">
      <tr style="width: 100%; border:none; background-color: #EEEEEE ;padding: 15px;"><td align="right"><h1>الشروط والاحكام</h1></td><td align="left"><h1>Terms and Conditions</h1></td></tr>

      <table style=\'width: 100%\'>
          <tr>
            <td dir="rtl">
              <p><b>التفسیرات والتعریفات</b></p>
            </td>
            <td align="left">
              <p><b>1. Definitions And Interpretation</b></p>
            </td>
          </tr>
          <tr>
            <td dir="rtl">
              <p><b>التعریفات :</b></p>
            </td>
            <td align="left">
            <p><b>Definitions:</b></p>
            </td>
          </tr>
          <tr>
            <td dir="rtl">
              <p>ما لم يقتضي السياق خلاف ذلك، تحمل الكلمات والتعبيرات  التالية المعنى المحدد قرين كل منها فيما يلي :</p>
            </td>
            <td align="left">
              <p>In this Agreement, except where the context otherwise requires the following words and expressions shall have the following meanings:</p>
            </td>

          </tr>
          <tr><br>
            <td dir="rtl" width="45%">
              <table dir="rtl">
                <tr>
                  <td  width="25%">
                    <b>الاتفاقیة</b>
                  </td>
                  <td width="70%">
                    يقصد بها هذه الاتفاقية وما تتضمنه من جداول وملحقات ومستندات  وتعديلات وفقا للشروط والأحكام المنصوص عليها بهذه الاتفاقية  والتي تشمل التعديلات على الجداول والملحقات والمستندات.
                  </td>
                </tr>
                <tr>
                  <td  width="25%">
                    <b>التابع</b>
                  </td>
                  <td width="70%">
                    يقصد به  (1) أي شخص يدير أو يخضع لإدارة أو يخضع للإدارة المشتركة مع شخص آخر  أو(2) أي شخص يمتلك أو يتولى إدارة 10 % أو أكثر من الأسهم المتداولة التي تخول الحق في التصويت لهذا الشخص الآخر(3) أي مسئول أو مدير  أو  شريك آخر لهذا الشخص (4)  إذا كان هذا الشخص مسئول أو مدير  أو شريك  أو أي كيان أو شركة والتي يعمل بها هذا الشخص بأي صفة.
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>المشتري</b>
                  </td>
                  <td width="70%">
                    يقصد به أي  طرف /  شخص يشتري بضائع  البائع عن طريق الموقع الالكتروني.
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>تاریخ البدء</b>
                  </td>
                  <td width="70%">
                       يقصد به تاريخ '.$date_approved.'
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>العملة</b>
                  </td>
                  <td width="70%">
                    یقصد بھا العملة الرسمیة للمملكة العربیة السعودیة و/ أو الإمارات العربیة المتحدة
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>المعلومات السریة</b>
                  </td>
                  <td width="70%">
                    يقصد بها المعلومات التي لها طبيعة سرية (مثل  الأسرار التجارية ومعلومات القيمة التجارية المعروفة لكل طرف والمتعلقة بكل طرف أو شركائهم أو أي مشروع والتي تم تبادلها بين الطرفين  وكما تتضمن المعلومات السرية  المعلومات المتعلقة بالمشغلين / العملاء المشتركين بين الطرفين  وشروط وأحكام هذه الاتفاقية .
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>القوة القاھرة</b>
                  </td>
                  <td width="70%">
                    یقصد بھا الحرب والغزو وأفعال العدو الخارجي والعداءات ( سواء تم إعلان الحرب أو لا) والحروب المدنیة والتمرد وأفعال القدر والثورات والعصیان المسلح والقوة العسكریة والاستیلاء على الحكم والأحكام العرفیة والمصادرة بواسطة الأمر الصادر من الھیئات الحكومیة وأي ظروف خارجة عن السیطرة المعقولة للطرفین (وأي من الأحداث الغیر متوقعة والتي تعیق أي طرف من الوفاء بالتزاماتھ بموجب ھذه الاتفاقیة)
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>البضائع</b>
                  </td>
                  <td width="70%">
                    يقصد بها  البضائع / المنتجات التي يبعها البائع عن طريق الموقع الالكتروني ، حسب ما هو منصوص عليه بالملحق أ من هذه الاتفاقية .
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>الغرامة</b>
                  </td>
                  <td width="70%">
                      يقصد بها الغرامة التي تفرض على البائع نتيجة  بيعه للمشتري منتجات معيبة أو ناقصة أو متدنية الجودة
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>السیاسات</b>
                  </td>
                  <td width="70%">
                    يقصد بها سياسات  الطرف الأول  القائمة حاليا والسارية
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>“م.ع.س”</b>
                  </td>
                  <td width="70%">
                    یقصد بھا المملكة العربیة السعودیة
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>الھیئة (الھیئات )المختصة</b>
                  </td>
                  <td width="70%">
                    حسب ما یقتضي السیاق ،<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;یقصد بھا :<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 أ) حكومة الإمارات العربية المتحدة  و/ أو المملكة العربية السعودية  حسب الحالة.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 ب) حكومة أي إمارة بالإمارات العربية المتحدة.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ج) أي وزارة أو دائرة أو  هيئة محلية أخرى مختصة  بالبيع في المملكة العربية السعودية \ أو الإمارات العربية المتحدة. <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;د) أي هيئات حكومية مختصة بإدارة  معاملات البيع الالكتروني في المملكة العربية السعودية \ أو الإمارات العربية المتحدة.
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>البضائع المرتجعة</b>
                  </td>
                  <td width="70%">
                    يقصد بها البضائع التي يعيدها المشتري للبائع ف غضون 15 يوم
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>عائد البیع</b>
                  </td>
                  <td width="70%">
                    يقصد به إجمالي عائد البيع  من أي معاملات بيع للبائع والتي تشمل كافة نفقات الشحن والمناولة ولف الهدايا والنفقات الأخرى ويستثنى من ذلك  الضرائب المحددة والتي يتم حسابها بشكل منفصل.
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>رسوم الخدمة</b>
                  </td>
                  <td width="70%">
                    يقصد بها رسوم الخدمة التي يدفعها البائع  للطرف الأول لتوفير الخدمات بموجب هذه الاتفاقية .
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>الموقع</b>
                  </td>
                  <td width="70%">
                    يقصد به  المواقع  www.sayidaty.net, sayidatymall.net,sayidatymall.com,  Mall.sayidaty.net;  التي يمتلكها ويشغلها الطرف الأول
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>لنطاق الجغرافي</b>
                  </td>
                  <td width="70%">
                    يقصد به منطقة المملكة العربية السعودية والإمارات العربية المتحدة
                  </td>
                </tr>
                <tr>
                  <td width="25%">
                    <b>”أ.ع.م“</b>
                  </td>
                  <td width="70%">
                    يقصد بها دولة الإمارات العربية المتحدة .
                  </td>
                </tr>
              </table>
            </td>
            <td  width="5%"></td>
            <td align="left" width="50%">
              <table align="left">
                <tr align="left">
                  <td width="70%">
                    shall mean this Agreement including its schedules, appendices, exhibits and amendments, if any, pursuant to the terms and conditions hereof including any amendments made to schedules, appendices and exhibits;
                  </td>
                  <td width="25%">
                    <b style="display: inline-block; direction: ltr" align="left">‘Agreement’</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    shall refer to (i) any person directly or indirectly controlling, controlled by or under common control with another person, (ii) any person owning or controlling 10% or more of the outstanding voting securities of such other person, (iii) any officer, director or other partner of such person and (iv) if such other person is an officer, director or partner, any business or entity for which such person acts in any such capacity;
                  </td>
                  <td width="25%">
                    <b>“Affiliate”</b>
                  </td>
                </tr>
                <tr>
                  <td width="70%">
                    means any party / person who purchases the Seller’s item through the Site;
                  </td>
                  <td width="25%">
                    <b>“Buyer”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means the date of '.$date_approved.';
                  </td >
                  <td width="25%">
                    <b>“Commencement Date”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means the legitimate currency of KSA and/or the UAE;
                  </td>
                  <td width="25%">
                    <b>“Currency”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means information of a confidential nature (including trade secrets and information of commercial value) known to each Party and concerning each Party or its partners or any project and communicated between the Parties.<br>  Confidential Information also includes information relating to Operators / customers common between the Party, and terms and conditions of this Agreement;
                  </td>
                  <td width="25%">
                    <b>“Confidential Information”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%" >
                    means war, invasion, act of foreign enemies, hostilities (whether war be declared or not), civil war, rebellion, act of God, revolution, insurrection or military or usurped power, martial law or confiscation by order of any government and any circumstances beyond the reasonable control of the Parties (as unforeseeable course of events excusing the Parties from the fulfillment of this Agreement);
                  </td>
                  <td width="25%">
                    <b>“Force Majeure”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means the item(s)/products to be sold by the Seller through the Site, as specified in Annexure – A of this Agreement;
                  </td>
                  <td width="25%">
                    <b>“Item(s)”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means the penalty imposed on the Seller due to provision of defective or missing or bad quality item to the Buyer;
                  </td>
                  <td width="25%">
                    <b>“Penalty”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means currently existing and effective policies of the First Party;
                  </td>
                  <td width="25%">
                    <b>“Policies”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means the Kingdom of Saudi Arabia;
                  </td>
                  <td width="25%">
                    <b>“KSA”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    shall as the context requires mean: <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. The government of the UAE and/or KSA, as the case may be; <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. The government of any of the emirate of UAE; <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Any other ministry, department or local authority having jurisdiction over the sale in KSA and/or UAE; and <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d. Any governmental authority controlling the subject online sale transaction in KSA and/or UAE.
                  </td>
                  <td width="25%">
                    <b>“Relevant Authority(ies)”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                      means the items returned by the Buyer to the Seller within 15 days;                  </td>
                  <td width="25%">
                    <b>“Return Items”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means the gross proceeds from any of the Seller’s sale transaction, including all shipping and handling, gift wrap and other charges, but excluding any taxes separately stated and charged;
                  </td>
                  <td width="25%">
                    <b>“Sale Proceeds”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means the service charges payable by the Seller to the First Party for the provision of the Services under this Agreement.
                  </td>
                  <td width="25%" >
                    <b>“Service Charges”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means www.sayidaty.net, sayidatymall.net,sayidatymall.com,  Mall.sayidaty.net; owned and operated by the First Party;
                  </td>
                  <td width="25%">
                    <b>“Site”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means the territories of KSA and UAE;
                  </td>
                  <td width="25%" >
                    <b>“Territory”</b>
                  </td>
                </tr>
                <tr>

                  <td width="70%">
                    means the United Arab Emirates.
                  </td>
                  <td width="25%">
                    <b>“UAE”</b>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td dir="rtl">
              <p>(أ) شرط العناوين والتعبيرات التي تحتها خط استرشادية فقط ولا يعتد بها في تفسر الاتفاقية .</p>
              <p>(ب) الكلمات المفردة  تشمل الجمع والعكس صحيح والكلمات التي تشير للجنس تشمل أي جنس .</p>
              <p>(ج)  كافة التعهدات والضمانات والوعود  والتعويضات والعهود  والاتفاقيات المقدمة من الطرفين تعد ملزمة للطرفين  بالتضامن والتكافل..</p>
              <p>(د)  البيانات والجداول التي تشكل جزء من هذه الاتفاقية  والتي تكون نافذة كما لو كان منصوص عليها في الاتفاقية ذاتها وأي إشارة لهذه الاتفاقية تشمل الإشارة لهذه الملحقات والبيانات.</p>
              <p>(ه)   تُحدد كافة التواريخ والفترات وفقا للتقويم الميلادي</p>
              <p>(و) أي من الأحكام المنصوص عليها بتعريفات الاتفاقية والذي يشكل حكم  أساسي  بحيث يكفل حقوق ويفرض التزامات  وعلى الرغم من أنه حكم بتفسيرات الاتفاقية فيكون له نفس النفاذ  كما لو كان حكم أساسي في الاتفاقية نفسها .</p>
            </td>
            <td></td>
            <td align="left">
              <p>a) The clause headings and the underlinings are for convenience only and shall not affect the interpretation of this Agreement.</p>
              <p>b) Words importing the singular include the plural and vice-versa and words importing a gender include any gender;</p>
              <p>c) All representations, warranties, undertakings, indemnity, covenants or agreements on the part of the Parties bind them jointly and severally.</p>
              <p>d) Particulars and schedules form part of this Agreement and shall have effect as if set out in full in the body of this Agreement and any reference to this Agreement shall include the ‘Schedule’ and ‘Particulars’.</p>
              <p>e) All dates and periods shall be determined by reference to the Gregorian calendar.</p>
              <p>f) If any provision in a definition in this Agreement is a substantive provision conferring rights or imposing obligations, then, notwithstanding that it is only in the interpretation clause of this Agreement, effect shall be given to it as if it were a substantive provision in the body of this Agreement.</p>
            </td>
          </tr>
          <tr>
            <td dir="rtl">
              <p><b>2 مھام " سیدتي مول" -</b></p>
              <p>2-1  يتفق الطرفان على أن الطرف الأول  يوفر منصة  لأي طرف ثالث من البائعين والمشترين  للتفاوض وإنهاء الصفقات  حيث أن الطرف الأول لا يكون طرف في التعاملات بين البائع والمشتري باستثناء الحدود المنصوص عليها بهذه الاتفاقية، كما يجوز للبائع إدراج أي قائمة من البضائع بالموقع  ما لم تكون بضائع محظورة  وفقا للإجراءات والإرشادات الخاصة بالطرف الأول  أو كانت محظورة بموجب القانون  وعلى سبيل المثال لا الحصر  لا يجو للبائع إدراج أي من البضائع  أو الروابط أو  الملصقات الخاصة بالمواد ذات الصلة التي :
              </p>
              <p>أ-  تنتهك حقوق الملكية الفكرية للغير (حقوق الطبع – العلامات التجارية – براءة الاختراع – والأسرار التجارية)  أو أي حقوق ملكية أخرى  والتي تتضمن ( حقوق الإشهار والخصوصية ) .</p>
              <p>ب-  السب والقذف وما شابه ذلك من تشويه السمعة </p>
              <p>ج – الأمور الملفقة والغير قانونية والمسروقة والتدليس</p>
              <p>2-2  يتحمل البائع المسئولية الكاملة دون غيره للوصف الدقيق للبضائع وبصفته البائع  فإن الطرف الثاني  الموقع والخدمات مع تحمله لكافة المخاطر. </p>
            </td>
            <td></td>
            <td align="left">
              <p><b>2. SAYIDATY MALL’S ROLE</b></p>
              <p>2.1	The Parties agree that the First Party provides a platform for third-party Sellers and Buyers to negotiate and complete transactions. The First Party is not involved in the actual transaction between Seller and Buyer, except to the extent as mentioned in this Agreement. The Second Party, being a Seller, may list any item on the Site unless it is a prohibited item as defined in the procedures and guidelines provided by the First Party, or otherwise prohibited by law. Without limitation, the Seller may not list any item or link or post any related material that:</p>
              <p>(a) 	infringes any third-party intellectual property rights (including copyright, trademark, patent, and trade secrets) or other proprietary rights (including rights of publicity or privacy); </p>
              <p>(b) 	constitutes libel or slander or is otherwise defamatory; or </p>
              <p>(c) 	is counterfeited, illegal, stolen, or fraudulent. </p>
              <p>2.2	It shall be the Seller’s sole responsibility to accurately describe the item for sale. As a Seller, the Second Party shall use the Site and the Services at its own risk.</p>

            </td>

          </tr>
          <tr>
          <td dir="rtl">
            <p><b>3 – خدمة معالجة المعاملات</b></p>
            <p>3-1 يوافق الطرفان أنه فور التسجيل  أو استخدام الخدمات فإن البائع  يخول سيدتي مول للتصرف نيابة عنه  بغرض التعامل مع الدفعات والبضائع المرتجعة  واسترداد الأموال والتسويات المتعلقة بالمعاملات  وقبض والاحتفاظ بعائد البيع  وتحويل عائد البيع للحساب المصرفي للبائع  والرسوم الخاصة بالبطاقة الائتمانية  ودفع المبالغ الخاصة بسيدتي مول والشركات التابعة  وكافة المبالغ المستحقة للبائع بموجب هذه الاتفاقية أو أي اتفاقيات أخرى للبائع مع سيدتي مول وأي من الشركات التابعة له.</p>
            <p>3-2  يوافق الأطراف أن سيدتي مول يتولى تسهيل  شراء بضائع البائع المدرجة بالموقع ومن ثم يفرض سيدتي مول على البائع رسوم '.$partner_info['flat_rate'].'  درهم إماراتي بالإضافة إلى '.$partner_info['commission'].' % من السعر بصفتها  حصة في الأرباح /  عمولة لكل صفقة  بيع للبضائع عن طريق الموقع  ويصدر سيدتي مول  للبائع فاتورة عائدات البيع  بصفة شهرية  ويحول سيدتي مول  المبالغ المستحقة للبائع على الحساب المصرفي المخصص للبائع  في غضون 15 يوم من تاريخ إصدار الفاتورة  بعد خصم رسوم التحويل المصرفي ، إن وجد .</p>
            <p>3-3  بالإضافة إلى ذلك يوافق الطرفان على أنه عند إصدار تعليمات من المشتري بالدفع للبائع  فيخول سيدتي مول بتحويل المبالغ التي دفعها المشتري للبائع  بعد خصم الرسوم أو أي مبالغ أخرى مستحقة لسيدتي مول بموجب هذه الاتفاقية،  كما يوافق البائع كذلك اعتبار أن المشتري استوفى بالتزاماته تجاه البائع فيما يتعلق بصفقة البيع  عند استلام سيدتي مول لعائد البيع  ويوافق الطرفان على أن  سيدتي مول مسئول عن تحويل النقود بعد خصم رسوم الخدمة  ما لم يتم احتجازها بسبب المطالبات المحتملة .</p>
            <p>3-4  يجوز أن يطلب <b>" سيدتي مول "</b>   من البائع تقديم أي معلومات مالية أو تجارية أو شخصية  في أي وقت للتحقق من هوية البائع وكما يتعهد البائع بعدم انتحاله صفة أي شخص أو استخدام أي اسم غير مصرح له قانونيا باستخدامه.</p>
            <p>3-5  يوافق البائع على أنه يجوز تقييد عائد البيع فقط  على الحسابات المصرفية  بالمملكة العربية السعودية أو الإمارات العربية المتحدة المدعومة بالأداء الوظيفي القياسي الخاص ب" سيدتي مول "    والمتاحة لحساب البائع.</p>
            <p>3-6   يوافق الطرفان  على أن خدمة المبيعات تكون متاحة سبعة  (7) أيام  في الأسبوع و 24 ساعة يوميا  باستثناء وقف التعطل  المقرر نتيجة لمنظومة الصيانة  وفي حالة الأعطال الفنية ومن المتفق عليه أن التحويلات  على حساب البائع يتم تقييدها بشكل عام على حساب البائع خلال  1-2 يوم عمل من تاريخ شروط" سيدتي مول "   في التحويل ولا يستلم البائع أي فائدة أو أرباح من عائد البيع.            </p>
            <p>3-7  يوافق الطرفان أنه نظرا  للإجراءات أمنية  فيخول    " سيدتي مول "   والشركات التابعة له، ولكن لا يطلب منه، فرض حدود على المعاملات  على بعض أو كل  من البائعين والمشترين  فيما يتعلق بقيمة أي معاملة أو استرداد  أو تسوية خلال فترة زمنية أو عدد من المعاملات  / يوم أو لأي فترة من الوقت  ولا يتحمل " سيدتي مول "    أو الشركات التابعة له أي مسئولية تجاه  البائع.</p>
            <p>(1)  في حالة عدم شروع " سيدتي مول "    في صفقة البيع أو الدفع أو التسوية  بعد أي مدة محددة  بواسطة " سيدتي مول "    أو الشركات التابعة له  لدواعي أمني.</p>
            <p>(2)  في حالة سماح " سيدتي مول "    أو الشركات التابعة له للمشتري  بالانسحاب  من الصفقة  بسبب عدم توفر خدمات معالجة الصفقات(التعاملات)  بعد الفعل في الصفقة</p>
            <p>3-8  يتفق الطرفان أنه في حالة إذا رأت " سيدتي مول "    أو الشركات التابعة لها أو اعتبرت استنادا على المعلومات المتاحة أن تصرفات البائع وأدائه فيما يتعلق بالخدمات يؤد إلى منازعات  مع المشتري أو  استرداد القيمة أو أي مطالبات أخرى  فيجوز أن تقوم  " سيدتي مول "    حسب تقديرها الشخصي بتأخير تحويل أو احتجاز الدفعات أو المبالغ المستحقة للبائع فيما يتعلق بالخدمات أو هذه الاتفاقية  حتى الانتهاء من أي تحقيق متعلق بتصرفات أو أداء البائع بموجب هذه الاتفاقية .</p>
            <p>3-9 يوافق الطرفان  على تحمل البائع لكافة المخاطر المتعلقة بالتدليس أو الخسائر لأي من بضائع البائع التي لم يتم الوفاء بها مطلقا طبقا للمعلومات المنصوص عليها بأمر الشراء  أو معلومات الشحن المقدمة.</p>
          </td>
          <td></td>
            <td align="left">
              <p><b>3. TRANSACTION PROCESS SERVICE</b></p>
              <p>3.1	The Parties agree that by registering for or using the Services, the Seller authorizes Sayidaty Mall to act on its behalf for purposes of processing payments, returns, refunds and adjustments for sale transactions, receiving and holding Sales Proceeds, remitting sales proceeds to the Seller’s bank account, charging its credit card, and paying Sayidaty Mall and its Affiliates all/any amounts the Seller owes in accordance with this Agreement or other agreements the Seller may have with Sayidaty Mall or its Affiliates.</p>
              <p>3.2	The Parties agree that Sayidaty Mall shall facilitate the purchase of Seller’s Items listed on the Site. Sayidaty Mall shall charge the Seller AED '.$partner_info['flat_rate'].' plus '.$partner_info['commission'].'% of the price as profit share/commission for each transaction of Item through the Site, and Sayidaty Mall shall issue invoice to the Seller relating to Sales Proceeds on monthly basis. The funds shall be transferred by Sayidaty Mall to the Sellers’s designated bank account within fifteen (15) days from the billing date, after deduction of the bank transfer fees, if applicable.</p>
              <p>3.3	Further, the Parties agree that when a Buyer shall instruct Sayidaty Mall to pay the Seller, Sayidaty Mall shall be authorized to remit the Buyer’s payment to the Seller, after deduction of any applicable fees or other amounts Sayidaty Mall may collect under this Agreement. The Seller further agrees that Buyer shall be considered to have satisfied its obligations towards the Seller for the sale transaction when Sayidaty Mall receives the Sales Proceeds. The Parties agree that Sayidaty Mall shall be responsible to transfer the funds after deductions of its Service Charges, unless withheld due to potential claims.</p>
              <p>3.4	Sayidaty Mall may require the Seller to provide any financial, business or personal information at any time to verify the Seller’s identity. The Seller undertakes that it shall not impersonate any person or use a name it is not legally authorized to use.</p>
              <p>3.5	The Parties agree that the Sales Proceeds can be credited only to bank accounts in the KSA or UAE as supported by Sayidaty Mall’s standard functionality and enabled for the Seller’s account.</p>
              <p>3.6	The Parties agree that sale transaction Service shall be available seven (7) days per week, twenty four (24) hours per day, except for the scheduled downtime due to system maintenance or in case of any technical default. It is agreed that the transfers to the Seller’s Account shall generally be credited within 1-2 business days of the date Sayidaty Mall initiates the transfer. The Seller shall not receive interest or any other earnings on any Sales Proceeds.</p>
              <p>3.7	The Parties agree that for security measures, Sayidaty Mall and/or its Affiliates, shall be authorized to, but not required to, impose transaction limits on some or all Buyers and Seller relating to the value of any transaction, disbursement, or adjustment, the cumulative value of all transactions, disbursements, or adjustments during a period of time, or the number of transactions per day or other period of time. Neither Sayidaty Mall nor its Affiliates shall be liable to Seller: </p>
              <p>(i) 	if Sayidaty Mall does not proceed with a sale transaction, disbursement, or adjustment that would exceed any limit established by Sayidaty Mall or its Affiliates for a security reason, or</p>
              <p>(ii) 	if Sayidaty Mall or its affiliates permits a Buyer to withdraw from a transaction because the transaction processing Service is unavailable following the commencement of a transaction.</p>
              <p>3.8	The Parties agree that if Sayidaty Mall or its affiliates reasonably concludes or considers, based on available information, that Seller’s actions and/or performance in connection with the Services may result in Buyer disputes, chargebacks or other claims, then Sayidaty Mall may, in its sole discretion, delay initiating any remittances and withhold any payments to be made or that are otherwise due to Seller in connection with the Services or this Agreement until the completion of any investigation(s) regarding any of the Seller’s actions and/or performance in connection with this Agreement. </p>
              <p>3.9	The Parties agree that the Seller shall bear all risk of fraud or loss in connection with any of Seller’s Items that are not fulfilled strictly in accordance with the order information and provided shipping information.</p>

            </td>

          </tr>
          <tr>
            <td dir="rtl">
              <p><b>4 – الحقوق المحفوظة ل " سيدتي مول "</b></p>
              <p>4-1  يتفق الطرفان أن " سيدتي مول "    يحتفظ بالحق  في تقرير المحتوى والمظهر والتصميم والأداء الوظيفي وكافة المناحي الأخرى بالموقع  والخدمات ( والتي تشمل الحق في إعادة التصميم وتعديل وإزالة وتبديل المحتوى  والمظهر والتصميم والأداء الوظيفي والخدمات وأي من العناصر والجوانب والأجزاء والسمات الخاصة بذلك  حسب ما يراه  مناسبا في أي وقت)  وكذلك تأخير أو  تعليق إدراج أو رفض  أو إلغاء إدراج القائمة أو مطالبة البائع بعدم إدراج  أي أو كافة البضائع حسب تقدير " سيدتي مول "    وكذلك  يجوز ل" سيدتي مول "    حسب تقديرها الإحجام عن الفحص أو رفض التعامل أو  تقييد جهات وصول الشحن ووقف  و / أو إلغاء أي من الصفقات التجارية للبائع  وفور استلام تعليمات من " سيدتي مول "    بهذا الصدد فيبذل البائع الجهود  التجارية المعقولة لوقف الصفقات على نفقته الخاصة.</p>
              <p>4-2 يسمح لموظفي " سيدتي مول "    والشركات التابعة للمشاركة  بصفتهم الشخصية .</p>

            </td>
            <td></td>
            <td align="left">
              <p><b>4. SAYIDATY MALL’S RESERVED RIGHTS</b></p>
              <p>4.1	The Parties agree that Sayidaty Mall retains the right to determine the content, appearance, design, functionality and all other aspects of the Site and the Services (including the right to re-design, modify, remove and alter the content, appearance, design, functionality, and other aspects of the Site and the Service, and any element, aspect, portion or feature thereof, as Sayidaty Mall may deem it suitable at any time), and to delay or suspend listing of, or to refuse to list, or to de-list, or to require Seller not to list, any or all Items in Sayidaty Mall’s sole discretion. Furthermore, Sayidaty Mall may in its sole discretion withhold for investigation, refuse to process, restrict shipping destinations, stop and/or cancel any of the Seller’s transactions, and on receiving instructions from Sayidaty Mall, the Seller shall exert commercially reasonable efforts to stop any transaction, at its own costs. </p>
              <p>4.2	Sayidaty Mall’s employees and its Affiliates are permitted to participate in their personal capacity.</p>

            </td>

          </tr>

          <tr>
            <td dir=\'rtl\'>
              <p><b>5- التزامات البائع</b></p>
              <p>5-1  يوافق الطرفان  يدرج البضائع بالموقع بسعر ثابت  ويلتزم البائع ببيع البضائع للمشتري بالأسعار المدرجة الذي يستوفي شروط البائع  ومن المتفق عليه أنه بإدراج البضائع بسعر ثابت فيتعهد ويضمن البائع  للمشترين المحتملين أن البائع  يتمتع بكافة الحقوق والقدرة الكاملة للبيع  وكما يتعهد بأن إدراج البضائع  هو إدراج دقيق وساري وكامل  وأنه لا ينطوي على أي تضليل أو ما شابه ذلك من سبل التدليس والغش.</p>
              <p>5-2 يتعهد ويقر البائع أنه في حالة عدم استيفائه  لهذه الالتزامات  أو  تصرف البائع أو الامتناع عن التصرف  فيخول " سيدتي مول "    بالحق في  اتخاذ الإجراءات القانونية المناسبة .</p>
              <p>5-3  يتفق الطرفان على أن البائع مسئول عن تحديد  أي من الضرائب التي تطبق على صفقات  البيع وكذلك تحصيل وإصدار التقارير وتحويل الضرائب لهيئات  الضرائب المعنية  ولا يتحمل " سيدتي مول "    تحويل أي ضرائب</p>
              <p>5-4 يقر البائع ويتعهد بأن  كافة البنود التي لم يتم توفيرها / الغير مقبولة للمشتري باستخدام خدمات " سيدتي مول "    فيقبل البائع  ويتولى عملية الإرجاع أو يسمح  ل" سيدتي مول "    لتولي عملية الإرجاع واسترداد الأموال  والتسويات اللازمة طبقا لهذه الاتفاقية وكما يحتفظ " سيدتي مول "    بحقوقها في  تعديل أو تقديم سياساتها  المتعلقة بعملية استرداد الأموال  من وقت لآخر بالموقع.</p>
              <p>5-5  يتعهد البائع كذلك بالتزامه بعرض بضائع جديدة  بالموقع الالكتروني ل " سيدتي مول "    عندما يجب بيع هذه البضائع مباشرة في   بلد معين.</p>
              <p>5-6  يمكن أن ينتفع البائع من ميزة  الطلب المسبق والتي تسمح للمشتري طلب بضائع قبل توفيرها  كما يتعهد البائع بالتزامه بالوفاء بتوفير البضائع المطلوبة وفقا للطلب المسبق خلال الإطار الزمني المحدد.</p>
              <p>5-7  يتعهد البائع بتوفير  مسئول اتصال ليكون  نقطة اتصال   مع " سيدتي مول "    فيما يتعلق بالأمور التشغيلية واستيفاء الالتزامات .</p>
              <p>5-8 يتعهد البائع كذلك بالتزامه بالرد على استفسارات المشتري  أو أي منازعات أو أمور تنشأ إبان معالجة صفقة البيع المذكورة أعلاه  خلال ساعات العمل المقبولة.</p>
              <p>5-9  يوافق البائع على الالتزام بالإجراءات والتوجيهات المنصوص عليها بهذه الاتفاقية  أو بقسم المساعدة أو بأي مكان آخر  بالموقع  لعملية البيع بسعر ثابت  وكما يوافق الطرفان على أن الطرف الأول  يتمتع بكامل الحق في  تغير أي من هذه الإجراءات أو الإرشادات في المستقبل  وهذه التغيرات تكون نافذة فور نشرها دون إشعار مسبق للبائع ، كما يجب أن يرجع البائع بانتظام للموقع لفهم الإجراءات  والإرشادات الحالية للمشاركة في الأعمال التجارية وللتأكيد على أن البضائع المعروضة بواسطة البائع للبيع يمكن بيعها من خلال  الموقع .</p>
              <p>5-10   يجب أن يقدم البائع ل" سيدتي مول "    بيانات المناطق   المتعلقة بالبضائع المدرجة بالموقع  بواسطة البائع ، ويتضمن ذلك على سبيل المثال لا الحصر،  اسم الإمارة أو البلد المسموح بها في مزاولة الأعمال التجارية .</p>
              <p>5-11  يوافق البائع أن " سيدتي مول "   توفر خدمات الشحن والخدمات اللوجيستية الأخرى مقابل رسوم الخدمة  بموجب هذه الاتفاقية،  كما يقدم البائع ل" سيدتي مول "    و / أو أي طرف مشترك  بالخدمات اللوجيستية  بكافة المعلومات والتسهيلات  والتعاون المطلوب للتعبئة والشحن.</p>
              <p>5-12 يتعهد ويضمن البائع  عدم تدني أو سوء  جودة البضائع   أو تكون معيبة  ويجب أن تكون مقبولة للمشتري و يتحمل البائع بمفرده مسئولية أي عيوب أو تدني في مستوى الجودة أو نقص في البضائع أو الشكاوى  من أي نوع آخر متعلقة  بقبول المواد، كما يعوض البائع " سيدتي مول "    ويبرئ ذمتها ضد أي ضرر أو خسائر  أيا  كانت طبيعتها المتعلقة بهذا الشأن.</p>
              <p>5-13 في حالة تولي البائع توفير  تعبئة البضائع وترتيبات الشحن  للمشتري دون تدخل من " سيدتي مول "  ، كما يجب أن يوفر البائع لسيدتي مول "    أي معلومات مطلوبة متعلقة بالتعبئة الشحن  ومتابعة  حالات إضافة الطلبات (إن وجد)  خلال الوقت المحدد، وكذلك يخول  " سيدتي مول "    بالحق في إعلان هذه المعلومات  المتاحة وفي حالة إخفاق البائع  في إمداد " سيدتي مول "    بتأكيد الشحن خلال الإطار الزمني الذي حدده " سيدتي مول "    ( على سبيل المثال يومين  من تاريخ تقديم أمر الشراء)  كما يجوز " لسيدتي مول "   حسب تقديرها الشخصي  إلغاء أو توجيه البائع لوقف أو إلغاء أي من صفقات البيع هذه.</p>
            </td>
            <td></td>
            <td align="left">
              <p><b>5. SELLER’S OBLIGATIONS</b></p>
              <p>5.1	The Parties agree that the Seller shall list goods on Site for sale at a fixed price and the Seller shall be obligated to sell the goods at the listed price to Buyers who meet the Seller\'s terms. Furthermore, it is agreed that by listing an item in a fixed price sale, the Seller shall represent and warrant to prospective Buyers that the Seller has all the right and full ability to sell, and also undertake that the listing is accurate, current, and complete and is not misleading or otherwise deceptive.</p>
              <p>5.2	The Seller undertakes and acknowledges that by not fulfilling these obligations, the Seller’s act or omission shall entitle to Sayidaty Mall to take appropriate legal action.</p>
              <p>5.3	The Parties agree that the Seller shall be responsible to determine whether any taxes apply to the sale transactions and to collect, report, and remit the correct taxes to the appropriate tax authority, and that Sayidaty Mall shall not be liable for remittance of any tax.</p>
              <p>5.4	The Seller acknowledges and undertakes that any or all of its Items that are not fulfilled/accepted by the Buyer using Sayidaty Mall’s Services, the Seller shall accept and process or allow Sayidaty Mall to process the returns, refunds and adjustments in accordance with this Agreement. Sayidaty Mall reserves its rights to amend or introduce its policies regarding the process of refund from time to time, on its Site.</p>
              <p>5.5	The Seller further undertakes that the Seller is committed to reflect new Items in Sayidaty Mall Website when such Items are to be directly sold in the set country.</p>
              <p>5.6	The Seller can benefit from the feature of pre-order, which allows the Buyer to pre-order an Item before being available. The Seller undertakes that the Seller is obliged to fulfill all Items that are set under pre-order feature within the agreed timeframe.</p>
              <p>5.7  The Seller undertakes to provide an assigned contact to be the point of contact with Sayidaty Mall for any operational and obligations fulfillment.</p>
              <p>5.8 	The Seller further undertakes that the Seller is obliged to respond to Buyer’s queries, conflict or issues that might occur during the transaction process mentioned above within acceptable business hours.</p>
              <p>5.9	The Seller agrees to abide by the procedures and guidelines, contained in this Agreement, the Help section or mentioned at other place in the Site, for conducting fixed price sales. The Parties agree that the First Party shall have full rights to change any of the procedures and guidelines in the future and such changes will be effective immediately upon posting without any notification to the Seller. The Seller shall regularly refer to the Site to understand the current procedures and guidelines for participating in the business and to be sure that the Item(s) offered by the Seller for sale can be sold on the Site.</p>
              <p>5.10 	In relation to each Item listed by the Seller on the Site, the Seller shall provide Sayidaty Mall with the complete details of regions, including but not limited to name of emirate or country, permitted for trade.</p>
              <p>5.11	The Parties agree that Sayidaty Mall shall provide the shipment and other logistic services in consideration of the Service Charges under this Agreement. The Seller shall provide Sayidaty Mall and/or any party engaged in logistics all information, facilities and cooperation required for packaging and shipment.</p>
              <p>5.12	The Seller warrants and undertakes that the quality of the Item shall not be low/bad or defective, and must be acceptable to the Buyer. Any defect, bad quality, missing Item, or complain of any other type relating to Item’s acceptability shall be the sole responsibility of the Seller, and the Seller shall keep Sayidaty Mall indemnified from any harm or loss, of whatsoever nature, in this regard.</p>
              <p>5.13	In case the Seller renders the packaging of the Item and further arranges the shipment of the Item to the Buyer without Sayidaty Mall’s involvement, the Seller shall provide to Sayidaty Mall any requested information regarding the packaging, shipment, tracking (if available) ad order status, within the provided time period, and Sayidaty Mall shall be authorized to make any of such information publicly available. If the Seller fails to provide Sayidaty Mall with the confirmation of shipment within the time frame specified by Sayidaty Mall (e.g., 2 days from the date an order was placed), Sayidaty Mall may in its sole discretion cancel, and/or may direct the Seller to stop and/or cancel, any such sale transaction.</p>
            </td>

          </tr>
          <tr>
            <td dir="rtl">
              <p><b>6- الأنشطة المحظورة والغير مشروعة</b></p>
              <p>6-1 يوافق الطرفان على استخدام البائع للموقع  والخدمات فقط للأغراض المشروعة  وبطريقة قانونية، كما يوافق ويتعهد البائع بالامتثال لكافة القوانين  والتشريعات واللوائح  المعمول بها  وعدم التسجيل باسم مزيف أو استخدام بطاقات ائتمان غير سارية أو غير مصرح  بها  كما يعد  ارتكاب أي تدليس أو غش  انتهاكا  للقوانين الفيدرالية والحكومية والإماراتية  وتُحال للهيئات القضائية وهيئات إنفاذ القانون  .</p>
              <p>6-2  يوافق الطرفان كذلك على أن " سيدتي مول "    يتمتع بالحق في  مراقبة أي أنشطة أو المحتوى المتعلق بالموقع والتحقيق حسب  المطلوب، كما أن " سيدتي مول "    يحتفظ بالحق  والاختيار المطلق حسب تقديره في إزالة ومراقبة وكتابة أي محتوى من الموقع  والذي يشكل انتهاك لهذه الأحكام  أو يكون مرفوض  وما شابه ذلك.</p>
              <p>6-3  يوافق البائع ويقر كذلك بأن " سيدتي مول "    يجوز له الإطلاع وإعلان  المعلومات المتعلقة  بأي صفقات بيع لأي  من مسئولي تنفيذ القانون  أو الهيئات التنظيمية  أو الغير في حالة اعتبار ذلك ضروريا  أو مناسبا والذي يتضمن على سبيل المثال لا الحصر  بيانات الاتصال للمستخدم و عنوان برتوكول الانترنت (IP) ومعلومات عن معدل استخدام الموقع  وسجل الاستخدام والمحتوى الذي يُنشر بالموقع.</p>
              <p>6-4  يتفق الطرفان على أنه لا يجوز للبائع  وإلزام الشركات التابعة له بعدم القيام بالإفصاح بشكل مباشر أو غير مباشر  أو نقل أو استخدام أي من  المعلومات والبيانات المتعلقة بأمر الشراء  أو المعلومات التي حصل عليها البائع والشركات التابعة له من " سيدتي مول "   أو الشركات التابعة لها ( أو خلاف ذلك  نتيجة للتعاملات المتضمنة  بهذه الاتفاقية.</p>
            </td>
            <td></td>
            <td align="left">
              <p><b>6. PROHIBITED & ILLEGAL ACTIVITIES</b></p>
              <p>6.1	The Parties agree that the Seller shall use the Site and Services only for lawful purposes and in a lawful manner. The Seller agrees and undertakes to comply with all applicable laws, statutes, and regulations and it shall not register under a false name or use an invalid or unauthorized credit card. Any fraudulent conduct shall be a violation of federal, state and emirate(s) laws, and shall be referred to judicial and law enforcement authorities.</p>
              <p>6.2	The Parties further agree that Sayidaty Mall shall have the right to monitor any activity and content associated with its Site and investigate as it may deem appropriate. Further, Sayidaty Mall reserves the right and shall have absolute discretion to remove, screen, or edit any content from the Site that violates these provisions or is otherwise objectionable.</p>
              <p>6.3	The Seller agrees and acknowledges that Sayidaty Mall may access and disclose any information in relation to any sale transaction to law enforcement officials, regulators, or other third parties, it considers necessary or appropriate, including but not limited to user contact details, IP addressing and traffic information, usage history, and content posted on the Site.</p>
              <p>6.4	The Parties agree that the Seller shall not, and shall cause its Affiliates not to, directly or indirectly disclose, convey or use any order information or other data or information acquired by the Seller or its Affiliates from Sayidaty Mall or its Affiliates (or otherwise) as a result of the transaction contemplated under this Agreement.</p>
            </td>

          </tr>
          <tr>
            <td dir="rtl">
              <p><b>7 - التعهدات والضمانات</b></p>
              <p>7-1 يتعهد ويضمن كلا الطرفان بأن كل منهما يتمتعا بالحق  القانوني والصلاحية والسلطة الكاملة لإبرام الاتفاقية وإتمام الصفقات المتضمنة بهذه الاتفاقية  وكذلك أي من تنفيذ أو أداء  هذه الاتفاقية أو الامتثال للشروط والأحكام المنصوص عليها بها لا يتعارض أو يكون غير متسق مع  أي من شروط أو تعهدات أو أحكام   أو تشكل تقصير بموجب  أو ناجمة من انتهاك أي (1)  قانون أو أحكام أو لوائح (2)  حكم قضائي أو  طلب أو أمر قضائي أو  مرسوم أو أي حكم صادر من هيئة تحكيم  أو محكمة أو هيئات حكومية  أو هيئات محلية أو خارجية  والتي يخضع أو يجوز أن يخضع لها الطرفين  أو تمنع وتحيل دون تنفيذ أو أداء  هذه الاتفاقية أو الصفقات المتضمنة بها</p>
              <p>7-2  يوافق الطرفان على أنه لا يقدم سيدتي مول أو الشركات التابعة له أي تعهدات أو ضمانات أيا كان نوعها ، سواء كان ذلك صراحة أو ضمنا،  والتي تشمل على سبيل المثال لا الحصر :
                <br>أ - أن الموقع والخدمات يجب أن تفي بمتطلبات البائع  وأنها تكون دائما متاحة ومسموح بدخولها  وغير منقطعة ومنضبطة  ومضمونة وتعمل دون أي خطأ.
                <br>ب -  أن المعلومات والمحتوى والمواد والبضائع  المعروضة بالموقع  يتم عرضها وفقا  للبائع  وإتاحتها للبيع في وقت تحديد الأسعار  أو شرعية في بيعها أو أداء  البائع  أو المشتري حسب التعهدات.
                <br>ج -  أي تعهدات ضمنية ناجمة من تنفيذ الصفقات أو استخدام  التجارة
                <br>د -  أي مسئوليات أو التزامات أو  حقوق أو مطالبات أو تعويض  عن أي ضرر  سواء كان ذلك ناجم من أي إهمال من " سيدتي مول "    أو الشركات التابعة  له أو لا .
                <br>بالإضافة إلى أنه  في أقصى حد تسمح به القوانين السارية فإن " سيدتي مول "    والشركات التابعة له تخلي مسئوليتها من جميع هذه التعهدات
              </p>
              <p>7-3  يوافق البائع ويتعهد أن " سيدتي مول "    والشركات التابعة له غير  طرف في  صفقات البيع  بين البائع والمشتري أو أي  تعاملات للمشاركين  وعليه ففي حالة إذا نشبت منازعات بين  واحد أو أكثر من المشاركين  فإن البائع يعفي سيدتي مول والشركات التابعة له (  ومن يتبعه من وكلاء وموظفين معنيين ) من كافة  الطلبات والمطالبات والأضرار سواء كانت الفعلية أو التبعية  أيا كان نوعها أو طبيعتها وسواء كانت معروفة أو غير معروفة ، مشتبه بها أو غير مشتبه  بها،  تم الإفصاح عنها أو لم يتم الإفصاح عنها  والناجمة من أو فيما يتعلق بهذه المنازعات بأي حال.</p>
              <p>7-4  يوافق الطرفان على أن البائع  يدافع عن ويعوض ويدفع الضرر عن " سيدتي مول "    والشركات التابعة له ( ومن يتبعهم من موظفين ومديرين ووكلاء ومندوبين معنيين)  من وضد كافة المطالبات والتكاليف والخسائر والأضرار  والأحكام والغرامات والفوائد  والنفقات (والتي تشمل أتعاب المحاماة المعقولة والنفقات القانونية الأخرى)  والناجمة من أو فيما يتعلق بأي إدعاءات / خلافات  والتي يمكن أن تنشأ من أو فيما يتعلق (1) أي انتهاك فعلي أو مدعى به لأي من تعهدات وضمانات والتزامات البائع  المنصوص عليها بهذه الاتفاقية (2)  الموقع الالكتروني الخاص بالبائع أو أي قنوات بيع  أو البنود التي يبيعها البائع أو المحتوى الذي  يقدمه البائع والإعلانات  والبيع  والعرض وإرجاع أي من البضائع التي باعها البائع  وأي انتهاك فعلي أو مدعى به لأي  من حقوق الملكية الفكرية  أي من البنود التي  يبيعها البائع  أو المحتوى الذي يقدمه البائع  أو الضرائب المعمول بها والتحصيل والدفع  والإخفاق في دفع أو تحصيل الضرائب، إن وجد.</p>
              <p>7-5 يتعهد البائع أن البضائع التي يبيعها في سيدتي مول غير معروضة للبيع في أي موقع الكتروني آخر  بأسعار أقل من الأسعار المعروضة في موقع " سيدتي مول "    الالكتروني.</p>
            </td>

            <td></td>
            <td align="left">
              <p><b>7. WARRANTIES AND REPRESENTATIONS</b></p>
              <p>7.1	The Parties represent and warrant that they have the legal right and full power and authority to enter into and consummate the transaction contemplated by this Agreement.Neither the execution nor the performance of this Agreement nor compliance with the terms and provisions hereof, will conflict or be inconsistent with any of the terms, covenants, conditions or provisions of, or constitute a default under or result in a breach of: (i) any law, rule or regulation; (ii) any judgment, order, injunction, decree or ruling of any arbitral tribunal or any court or governmental authority, domestic or foreign, which is or may be applicable to the Parties, or which would prevent or restrict the execution or performance of this Agreement and the transaction contemplated thereunder by the Parties.</p>
              <p>7.2	The Parties agree that neither Sayidaty Mall nor its Affiliates make any other representations or warranties of any kind, express or implied, including without limitation:</p>
              <p>a) that the Site or the Services shall meet the Seller’s requirements, shall always be available, accessible, uninterrupted, timely, secure, or operate without error.</p>
              <p>b) that the information, content, materials, or Items appeared on the Site shall be as represented by Sellers, available for sale at the time of fixed price sale, lawful to sell, or that Seller or Buyer shall perform as promised.</p>
              <p>c) any implied warranty arising from course of dealing or usage of trade.</p>
              <p>d) d) any obligation, liability, right, claim, or remedy in tort, whether or not arising from the negligence of Sayidaty Mall or its Affiliates.</p>
              <p>Moreover, to the full extent permissible under applicable law, Sayidaty Mall and its Affiliates disclaim any and all such warranties.</p>
              <p>7.3	The Seller agrees and undertakes that Sayidaty Mall and its Affiliates are not involved in sale transactions between the Seller and the Buyer or other participant dealings, therefore in case of any dispute arising between one or more participants, the Seller shall release Sayidaty Mall or its Affiliates (and their respective agents and employees) from claims, demands, and damages (actual and consequential) of any kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way connected with such dispute.</p>
              <p>7.4	The Parties agree that the Seller shall defend, indemnify and hold harmless Sayidaty Mall and its Affiliates (and their respective employees, directors, agents and representatives) from and against any and all claims, costs, losses, damages, judgments, penalties, interest and expenses (including reasonable attorneys’ fees and other legal costs) arising out of or in connections with any claim/difference that may arise out of or relates to: (i) any actual or alleged breach of the Seller’s representations, warranties, or obligations set forth in this Agreement; or (ii) the Seller’s own website or other sales channels, the Items the Seller sells, any content the Seller provides, the advertisement, offer, sale or return of any Item the Seller sells, any actual or alleged infringement of any intellectual property or proprietary rights by any Items the Seller sells or content the Seller provides, or applicable tax or the collection, payment or failure to collect or pay tax, if any.</p>
              <p>7.5	The Seller undertakes that the Items that it sells on Sayidaty Mall are not displayed on any other websites with a price lower than offered on Sayidaty Mall website.</p>

            </td>

          </tr>
          <tr>
            <td dir="rtl">
              <p><b>8–الملكیة الفكریة</b></p>
              <p>8-1 أي حقوق ملكية فكرية متعلقة بالموقع  أو ناتجة من استخدامه فتكون من حق " سيدتي مول "    ويجوز للبائع استخدامها وفقا لما هو متفق عليها  بالاتفاقية  ويستمر سريان هذا الحكم بعد انتهاء الاتفاقية .</p>
              <p>8-2  يمنح البائع " سيدتي مول "    رخصة محدودة، بدون رسوم، عالمية وغير حصرية  لاستخدام  الملكية الفكرية  الخاصة به  بغرض تعريف وتشغيل  وترويج الخدمات  فيما يتعلق بمحتويات الموضوع / الملكية / البضائع  والمنتجات.</p>
              <p>8-3  في حالة أي مطالبات أو خسائر أو منازعات ناتجة من  سيدتي مول والغير  فيما يتعلق بملكية وحقوق محتوى  الموقع فإن البائع  يعوض " سيدتي مول "    عن  أي من هذه المطالبات والخسائر والالتزامات.</p>
              <p>8-4  يوافق الطرفان  أن البائع ملتزم بدفع  أي بضائع أو رسوم  وما شابه ذلك من الأمور المعمول بها بالنطاق الجغرافي  فيما يتعلق  بتنفيذ  الاتفاقية.</p>
            </td>
            <td></td>
            <td align="left">
              <p><b>8. INTELLECTUAL PROPERTY</b></p>
              <p>8.1	Any Intellectual Property rights relating to the Site or arising out of its use, shall belong to Sayidaty Mall and the Seller may utilize the same in a manner as agreed in this Agreement. This Article shall survive the termination of this Agreement.</p>
              <p>8.2	The Seller shall grant Sayidaty Mall a limited, royal free, worldwide, non-exclusive license to use its Intellectual Property for the purpose of identification, operation or promotion of the Services in association with the subject contents / property / Item/ product.</p>
              <p>8.3	In case of any claim, loss, or dispute arising between Sayidaty Mall and any third party in relation to ownership or rights of the contents of the Site, the Seller shall indemnify Sayidaty Mall from such claim, loss or any liability.</p>
              <p>8.4	Further, the Parties agree that the Seller shall be liable to pay any tax or fees etc. applicable in the Territory in relation to execution of this Agreement.</p>
            </td>
          </tr>
          <tr>

            <td dir="rtl">
              <p><b>9 التقصير</b></p>
              <p>9-1  في حالة  :
                <br>(أ) تعيين وصي على أي طرف
                <br>(ب) في حالة إصدار حكم بإفلاس أو إعسار أي طرف أو إصدار قرار صحيح بتصفية  أي طرف
                <br>(ج) أو إصدار أي حكم بالتصفية من المحكمة  القانونية  وعليه فيجب أن يخطر هذا الطرف (الطرف المعسر) ومن يتبعه من أعضاء  الطرف الآخر
              </p>
              <p>9-2  فور وقوع أي من الأحداث المنصوص عليها بالمادة 9-1  فيكون من  حق الطرف الغير معسر كافة الدفعات المقدمة من المشتري  مقابل صفقات البيع من خلال الموقع الالكتروني  ويجب تكوين وتعيين بما لا رجعة فيه محامي عن الطرف المعسر  والذي يخول  ويفوض بالتصديق باسم الطرف المعسر  على كافة الشيكات والكمبيالات والدفعات  والدفع بالطرق الأخرى  واستلام الدفعات بالحسابات  التي يكون فيها اسم الطرف المعسر  " المستفيد"</p>
              <p>9-3  يتمتع أي من الوصي أو  مأمور قضائي  أو مصفي أو أي شخص  موكل قانونيا بمباشرة شئون المعسر  بالحق في  استلام  حصته في المكسب ورسوم الخدمات بعد خصم العائد والتكاليف والنفقات  المدفوعة بموجب هذه الاتفاقية  والتي يتم حسابها بداية من تاريخ الإفلاس أو الإعسار أو  الحراسة القضائية  أو التصفية .</p>
              <p>9-4  تطبق هذه الأحكام  المنصوص عليها في هذه المادة  9، مع تغيير ما يلزم، عند انتهاك أي طرف  لالتزاماته بموجب الاتفاقية وإخفاقه في تدارك  هذا الانتهاك أو عدم الأداء، إن كان قابل للإصلاح،  في غضون 15 يوم بعد استلام إشعار من الطرف الآخر مطالبا بالإصلاح.</p>

            </td>
            <td></td>
            <td align="left">
              <p><b>9. DEFAULT</b></p>
              <p>9.1	In the event that:</p>
              <p>a) a custodian be appointed to a Party; or</p>
              <p>b) a Party be adjudged bankrupt or insolvent or has any resolution for its winding up duly passed; or</p>
              <p>c) any order for the winding up of any Party is made by any court of law;</p>
              <p>then such Party (“Insolvent Party”) and its member shall immediately notify the other Party.</p>
              <p>9.2	Upon the occurrence of one of the events described in Article 9.1 the Party, other than the Insolvent Party, shall be entitled to all payments thereafter made by the Buyer(s) towards the sale transaction through the Site and shall hereby irrevocably constituted and appointed the attorney of such Insolvent Party and be authorized and empowered to endorse the name of such Insolvent Party on all cheques or drafts or payment through other manner, received on account of such payments on which the name of such Insolvent Party appears as payee.</p>
              <p>9.3	The trustees, receivers, liquidator or other person lawfully entrusted to handle the affairs of an Insolvent Party shall be entitled to receive its share of the earnings / Service Charges, after deduction of proceeds, costs, expenses etc. payable under this Agreement computed as of the date on which the bankruptcy, insolvency, receivership or winding up occurred.</p>
              <p>9.4	The provisions of this Article 10 shall also apply mutatis mutandis where a Party breaches any of its obligations hereunder and fails to remedy such breach or non-performance, if capable of remedy, within fifteen (15) days, after receipt of notice from other Party requiring such remedy.</p>

            </td>
          </tr>
          <tr>

            <td dir="rtl">
              <p><b>10-  المدة والفسخ</b></p>
              <p>10-1  تستمر هذه الاتفاقية بداية من تاريخ تحرير الاتفاقية ولمدة 12 شهر  وتُجدد تلقائيا ما لم يشعر أحد الطرفين الطرف الآخر تحريريا قبل موعد انتهاء الاتفاقية ب 60 يوم بخلاف ذلك.</p>
              <p>10-2 يتمتع كل طرف  بالحق في فسخ الاتفاقية في حالة إذا قام الطرف الآخر بأي من الأمور التالية:
              <br>أ)  ارتكاب أي انتهاك أو الإخفاق في أداء أو الالتزام  بأي من الأحكام والشروط والتعهدات المنصوص عليها بهذه الاتفاقية .
              <br>ب)  إشهار إفلاسه أو  اتخاذ أي إجراءات للإفلاس أو  أبرم تسويات أو اتفاقيات مع الدائنين  أو قام بالتصفية الإجبارية أو الاختيارية  ويجوز لسيدتي مول وفقا للأحكام المنصوص عليها بهذه الاتفاقية إلغاء  صفقات بيع بضائع البائع وفسخ هذه الاتفاقية.</p>
              <p>10-3 يوافق الطرفان أنه يجوز لسيدتي مول  حسب تقديرها الشخصي  فسخ الاتفاقية والدخول للموقع والخدمات المنصوص عليها بهذه الاتفاقية  أو أي مبيعات بالأسعار الثابتة الحالية على الفور بدون إشعار لأي سبب ، وبالإضافة إلى أنه يجوز لسيدتي مول حسب تقديرها منع البائع من إدراج أي بضائع  للبيع.</p>

            </td>
            <td></td>
            <td align="left">
              <p><b>10. DURATION AND TERMINATION</b></p>
              <p>10.1	The duration of this Agreement shall run continuously from the date of the Agreement for a period of 12 months and shall be automatically renewed unless specified in writing by either Party within 60 days with prior written notice to the other Party.</p>
              <p>10.2 	Each Party has the right to terminate the Agreement, if the other Party:</p>
              <p>a) Commits any breach of or fails to perform or observe any terms or conditions or covenants enumerated under this Agreement; or</p>
              <p>b) Is declared a bankrupt, commits an act of bankruptcy or enters into any composition or arrangement with its creditors or enters into liquidation whether voluntary or compulsory; Sayidaty Mall may subject to the terms of this Agreement annul the sale transaction of the Seller’s Item(s) and shall forthwith terminate this Agreement.</p>
              <p>10.3	The Parties agree that Sayidaty Mall, in its sole discretion, may terminate this Agreement, access to the Site or other services under this Agreement, or any current fixed price sales immediately without notice for any reason. Furthermore, Sayidaty Mall, in its sole discretion, also may prohibit the Seller from listing any items for sale.</p>
            </td>
          </tr>
          <tr>
            <td dir="rtl">
              <p><b>11-  القوانين الحاكمة والاختصاص القضائي</b></p>
              <p>11-1  تخضع هذه الاتفاقية في تفسيرها  وسريان أحكامها للقوانين المعمول بها في إمارة دبي و والقوانين الفيدرالية للإمارات العربية المتحدة.</p>
              <p>11-2  في حالة إذا نشبت أي منازعات أو خلافات بين الطرفين والناجمة من أو فيما يتعلق  أو ذات صلة بهذه الاتفاقية فيعقد  المندوبين المخولين نيابة عن الأطراف  في غضون  عشرين(20) يوم  من إرسال أحد الطرفين إشعار تحريري للآخر ( إشعار المنازعات)  اجتماعا (اجتماع المنازعات) مع بذل الجهود لمحاولة فض هذه المنازعات أو الخلافات  ويبذل كلا الطرفين  المساعي المعقولة  بإرسال المندوب الذي يتمتع بصلاحية   تسوية المنازعات والخلافات في اجتماع المنازعات.</p>
              <p>11-3  أي منازعات أو خلافات لم يتم تسويتها في غضون 40 يوم من إرسال إشعار المنازعات  وسواء تم انعقاد اجتماع تسوية المنازعات أو لا   وعليه حسب طلب أي من الطرفين بعد انتهاء مدة 40  يوم  تحال المنازعات للتحكيم بموجب  قواعد مركز دبي للتحكيم الدولي (  القواعد)  والقانون الإماراتي، أمام محكم واحد يتم تعيينه وفقا للقواعد  ويوافق الطرفان كذلك أن  الحكم الصادر من التحكيم يكون حكما نهائيا وملزما للطرفين.</p>
              <p>11-4  يكون مقر التحكيم بدبي ولغة التحكيم هي اللغة الإنجليزية  كما يتنازل الطرفين عن الحق في تقديم طلب لأي محكمة بأي اختصاص قضائي في حالة  إذا أمكن القيام بهذا التنازل صحيحا</p>
            </td>
            <td></td>
            <td align="left">
              <p><b>11. GOVERNING LAW AND RESOLUTION OF DISPUTES</b></p>
              <p>11.1	This Agreement shall be governed by and construed in accordance with the laws of the Emirate of Dubai and the Federal Laws of the United Arab Emirates.</p>
              <p>11.2	In the event of any dispute or difference between the Parties arising out of or in connection with or relating to this Agreement, representatives of the Parties shall, within twenty (20) days of a written notice from one Party to the other Party (a Dispute Notice), hold a meeting (a Dispute Meeting) in an effort to resolve the dispute or difference. Each Party shall use all reasonable endeavours to send a representative who has authority to settle the dispute or difference at the Dispute Meeting.</p>
              <p>11.3	Any dispute or difference which is not resolved within forty (40) days after the service of a Dispute Notice, whether or not a Dispute Meeting has been held, shall, at the request of either Party, following the expiry of the forty (40) day period, be referred to arbitration under the rules of the Dubai International Arbitration Centre (the Rules) and the UAE laws, before a single arbitrator who shall be appointed in accordance with the Rules and the Parties further agree that the award of the arbitrator shall be final and binding upon the Parties.</p>
              <p>11.4	The place of arbitration shall be Dubai and the language of the arbitration shall be English. The Parties waive any right of appeal to any court in any jurisdiction, insofar as such waiver can validly be made.</p>

            </td>
          </tr>
          <tr>

        <td dir="rtl">
          <p><b>12- أحكام عامة</b></p>
          <p>12-1 في حالة إذا كان أو  أصبح أي جزء من أي من الأحكام المنصوص عليها بهذه الاتفاقية باطلا أو غير نافذا فإن  باقي هذا الحكم أو الأحكام الأخرى بهذه الاتفاقية تظل سارية ونافذة .</p>
          <p>12-2  يتفق الطرفان بالتبادل أن هذه الاتفاقية ملزمة للطرفين</p>
          <p>12-3  لغرض هذه الاتفاقية يقصد بالقوة القاهرة  الأحداث الخارجة عن السيطرة المعقولة للبائع أو سيدتي مول  أو كلاهما  والتي تتضمن على سبيل المثال لا الحصر الأقدار والحرب  والإرهاب  والاحتجاجات و الحوادث والشغب والسيول  والقوانين الصناعية والزلازل وأي أمور تتعلق بقوى الطبيعة والتي تكون خارجة عن التوقع المعقول أو القدرة على تجنبها  من الطرف المتضرر.</p>
          <p>12-4 تشكل هذه الاتفاقية مجمل الاتفاق بين الطرفين  ولا تكون أي تعديلات ملزمة ما لم يتم الموافقة عليها تحريريا من كلا الطرفين  وتحل هذه الاتفاقية محل كافة  التفاهمات والاتفاقيات السابقة بين الطرفين .</p>
          <p>12-5  كافة المكاتبات المتعلقة بهذه الاتفاقية يجب أن تكون تحريرية  وتسلم بواسطة البريد الالكتروني أو البريد السريع أو  الفاكس للطرف المعني  على العنوان ذا الصلة المنصوص عليه في صدر هذه الاتفاقية ( و أي عنوان آخر يخطر به  من وقت لآخر أحد الطرفين للطرف الآخر حسب ما هو منصوص عليه بهذا البند)</p>
          <p>يؤكد الطرفان أنهم قد  قرأوا هذه الشروط والأحكام ووافقواعلى الالتزام بها.</p>
        </td>
        <td></td>
        <td align="left">
          <p><b>12. GENERAL</b></p>
          <p>12.1	If any part of any provision of this Agreement shall be or become invalid or unenforceable, then the remainder of such provision and all other provisions of this Agreement shall remain valid and enforceable.</p>
          <p>12.2	The Parties mutually agree that this Agreement is binding on the Parties.</p>
          <p>12.3	For purposes of this Agreement ‘Force Majeure’ shall mean an event beyond the reasonable control of the Seller and/ or Sayidaty Mall including but not limited to acts of God, war, terrorism, strikes, accidents, riots, floods, industrial action, earthquakes or any operation of the forces of nature that reasonable foresight and ability on the part of the affected Party could not provide against.</p>
          <p>12.4	This Agreement constitutes the entire agreement between the Parties hereto and no amendment shall be binding unless agreed upon by the Parties in writing. This Agreement supersedes any previous understanding or agreement between the Parties.</p>
          <p>12.5	All communications relating to this Agreement shall be in writing and delivered by email, courier or fax to the Party concerned at the relevant address shown at the start of this Agreement (or such other address as may be notified from time to time in accordance with this clause by the relevant party to the other Party).</p>
          <p>The Parties confirm that they have read and accepted these terms and conditions and agree to abide by the same.</p>
        </td>
      </tr>
        </table>
      </table>';

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

/*
      // output some RTL HTML content
      $html = '<div style="text-align:center">The words &#8220;<span dir="rtl">&#1502;&#1494;&#1500; [mazel] &#1496;&#1493;&#1489; [tov]</span>&#8221; mean &#8220;Congratulations!&#8221;</div>';
      $pdf->writeHTML($html, true, false, true, false, '');

      // test some inline CSS
      $html = '<p>This is just an example of html code to demonstrate some supported CSS inline styles.
      <span style="font-weight: bold;">bold text</span>
      <span style="text-decoration: line-through;">line-trough</span>
      <span style="text-decoration: underline line-through;">underline and line-trough</span>
      <span style="color: rgb(0, 128, 64);">color</span>
      <span style="background-color: rgb(255, 0, 0); color: rgb(255, 255, 255);">background color</span>
      <span style="font-weight: bold;">bold</span>
      <span style="font-size: xx-small;">xx-small</span>
      <span style="font-size: x-small;">x-small</span>
      <span style="font-size: small;">small</span>
      <span style="font-size: medium;">medium</span>
      <span style="font-size: large;">large</span>
      <span style="font-size: x-large;">x-large</span>
      <span style="font-size: xx-large;">xx-large</span>
      </p>';

      $pdf->writeHTML($html, true, false, true, false, '');

      // reset pointer to the last page
      $pdf->lastPage();

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      // Print a table

      // add a page
      $pdf->AddPage();

      // create some HTML content
      $subtable = '<table border="1" cellspacing="6" cellpadding="4"><tr><td>a</td><td>b</td></tr><tr><td>c</td><td>d</td></tr></table>';

      $html = '<h2>HTML TABLE:</h2>
      <table border="1" cellspacing="3" cellpadding="4">
          <tr>
              <th>#</th>
              <th align="right">RIGHT align</th>
              <th align="left">LEFT align</th>
              <th>4A</th>
          </tr>
          <tr>
              <td>1</td>
              <td bgcolor="#cccccc" align="center" colspan="2">A1 ex<i>amp</i>le <a href="http://www.tcpdf.org">link</a> column span. One two tree four five six seven eight nine ten.<br />line after br<br /><small>small text</small> normal <sub>subscript</sub> normal <sup>superscript</sup> normal  bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla<ol><li>first<ol><li>sublist</li><li>sublist</li></ol></li><li>second</li></ol><small color="#FF0000" bgcolor="#FFFF00">small small small small small small small small small small small small small small small small small small small small</small></td>
              <td>4B</td>
          </tr>
          <tr>
              <td>'.$subtable.'</td>
              <td bgcolor="#0000FF" color="yellow" align="center">A2 € &euro; &#8364; &amp; è &egrave;<br/>A2 € &euro; &#8364; &amp; è &egrave;</td>
              <td bgcolor="#FFFF00" align="left"><font color="#FF0000">Red</font> Yellow BG</td>
              <td>4C</td>
          </tr>
          <tr>
              <td>1A</td>
              <td rowspan="2" colspan="2" bgcolor="#FFFFCC">2AA<br />2AB<br />2AC</td>
              <td bgcolor="#FF0000">4D</td>
          </tr>
          <tr>
              <td>1B</td>
              <td>4E</td>
          </tr>
          <tr>
              <td>1C</td>
              <td>2C</td>
              <td>3C</td>
              <td>4F</td>
          </tr>
      </table>';

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      // Print some HTML Cells

      $html = '<span color="red">red</span> <span color="green">green</span> <span color="blue">blue</span><br /><span color="red">red</span> <span color="green">green</span> <span color="blue">blue</span>';

      $pdf->SetFillColor(255,255,0);

      $pdf->writeHTMLCell(0, 0, '', '', $html, 'LRTB', 1, 0, true, 'L', true);
      $pdf->writeHTMLCell(0, 0, '', '', $html, 'LRTB', 1, 1, true, 'C', true);
      $pdf->writeHTMLCell(0, 0, '', '', $html, 'LRTB', 1, 0, true, 'R', true);

      // reset pointer to the last page
      $pdf->lastPage();

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      // Print a table

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      // reset pointer to the last page
      $pdf->lastPage();

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      // Print all HTML colors

      // add a page
      $pdf->AddPage();

      $textcolors = '<h1>HTML Text Colors</h1>';
      $bgcolors = '<hr /><h1>HTML Background Colors</h1>';

      foreach(TCPDF_COLORS::$webcolor as $k => $v) {
          $textcolors .= '<span color="#'.$v.'">'.$v.'</span> ';
          $bgcolors .= '<span bgcolor="#'.$v.'" color="#333333">'.$v.'</span> ';
      }

      // output the HTML content
      $pdf->writeHTML($textcolors, true, false, true, false, '');
      $pdf->writeHTML($bgcolors, true, false, true, false, '');

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

      // Test word-wrap

      // create some HTML content
      $html = '<hr />
      <h1>Various tests</h1>
      <a href="#2">link to page 2</a><br />
      <font face="courier"><b>thisisaverylongword</b></font> <font face="helvetica"><i>thisisanotherverylongword</i></font> <font face="times"><b>thisisaverylongword</b></font> thisisanotherverylongword <font face="times">thisisaverylongword</font> <font face="courier"><b>thisisaverylongword</b></font> <font face="helvetica"><i>thisisanotherverylongword</i></font> <font face="times"><b>thisisaverylongword</b></font> thisisanotherverylongword <font face="times">thisisaverylongword</font> <font face="courier"><b>thisisaverylongword</b></font> <font face="helvetica"><i>thisisanotherverylongword</i></font> <font face="times"><b>thisisaverylongword</b></font> thisisanotherverylongword <font face="times">thisisaverylongword</font> <font face="courier"><b>thisisaverylongword</b></font> <font face="helvetica"><i>thisisanotherverylongword</i></font> <font face="times"><b>thisisaverylongword</b></font> thisisanotherverylongword <font face="times">thisisaverylongword</font> <font face="courier"><b>thisisaverylongword</b></font> <font face="helvetica"><i>thisisanotherverylongword</i></font> <font face="times"><b>thisisaverylongword</b></font> thisisanotherverylongword <font face="times">thisisaverylongword</font>';

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      // Test fonts nesting
      $html1 = 'Default <font face="courier">Courier <font face="helvetica">Helvetica <font face="times">Times <font face="dejavusans">dejavusans </font>Times </font>Helvetica </font>Courier </font>Default';
      $html2 = '<small>small text</small> normal <small>small text</small> normal <sub>subscript</sub> normal <sup>superscript</sup> normal';
      $html3 = '<font size="10" color="#ff7f50">The</font> <font size="10" color="#6495ed">quick</font> <font size="14" color="#dc143c">brown</font> <font size="18" color="#008000">fox</font> <font size="22"><a href="http://www.tcpdf.org">jumps</a></font> <font size="22" color="#a0522d">over</font> <font size="18" color="#da70d6">the</font> <font size="14" color="#9400d3">lazy</font> <font size="10" color="#4169el">dog</font>.';

      $html = $html1.'<br />'.$html2.'<br />'.$html3.'<br />'.$html3.'<br />'.$html2;

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      // - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

      // reset pointer to the last page
      $pdf->lastPage();*/

      //Close and output PDF document
      $file_name = 'contract_'.$this->request->get['customer_id'].".pdf";

      if (!file_exists(DIR_IMAGE.'seller_contracts/'))
        mkdir(DIR_IMAGE.'seller_contracts/', 0777);

      $this->db->query("UPDATE customerpartner_to_customer set contract_path='".$this->db->escape($file_name)."' WHERE customer_id=".(int)$this->request->get['customer_id']);
      $pdf->Output(DIR_IMAGE.'seller_contracts/'.$file_name, 'F');
      $pdf->Output($file_name, 'I');
    }
  }
