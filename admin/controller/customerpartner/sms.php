<?php

class Controllercustomerpartnersms extends Controller {

    private $error = array();
    private $data = array();

    public function index() {
        $this->getlist();
    }

    public function getlist() {

        $this->data = array_merge($this->data, $this->load->language('customerpartner/sms'));
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('customerpartner/sms');

        $filter_array = array(
            'filter_id',
            'filter_name',
            'filter_message',
            'page',
            'sort',
            'order',
            'start',
            'limit',
        );

        $url = '';

        foreach ($filter_array as $unsetKey => $key) {

            if (isset($this->request->get[$key])) {
                $filter_array[$key] = $this->request->get[$key];
            } else {
                if ($key == 'page')
                    $filter_array[$key] = 1;
                elseif ($key == 'sort')
                    $filter_array[$key] = 'cc.id';
                elseif ($key == 'order')
                    $filter_array[$key] = 'ASC';
                elseif ($key == 'start')
                    $filter_array[$key] = ($filter_array['page'] - 1) * 10;
                elseif ($key == 'limit')
                    $filter_array[$key] = 10;
                else
                    $filter_array[$key] = null;
            }
            unset($filter_array[$unsetKey]);

            if (isset($this->request->get[$key])) {
                if ($key == 'filter_name' || $key == 'filter_message')
                    $url .= '&' . $key . '=' . urlencode(html_entity_decode($filter_array[$key], ENT_QUOTES, 'UTF-8'));
                else
                    $url .= '&' . $key . '=' . $filter_array[$key];
            }
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('customerpartner/sms', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['delete'] = $this->url->link('customerpartner/sms/delete', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['insert'] = $this->url->link('customerpartner/sms/addSms', 'token=' . $this->session->data['token'], 'SSL');

        $results = $this->model_customerpartner_sms->viewtotal($filter_array);

        $product_total = $this->model_customerpartner_sms->viewtotalentry($filter_array);



        $this->data['sms'] = array();

        foreach ($results as $result) {

            $this->data['sms'][] = array(
                'selected' => False,
                'id' => $result['id'],
                'name' => $result['name'],
                'message' => $result['message'],
                'action' => $this->url->link('customerpartner/sms/addSms', 'token=' . $this->session->data['token'] . '&sms_id=' . $result['id'], 'SSL'),
            );
        }

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        foreach ($filter_array as $key => $value) {
            if (isset($this->request->get[$key])) {
                if (!isset($this->request->get['order']) AND isset($this->request->get['sort']))
                    $url .= '&order=DESC';
                if ($key == 'filter_name' || $key == 'filter_message')
                    $url .= '&' . $key . '=' . urlencode(html_entity_decode($filter_array[$key], ENT_QUOTES, 'UTF-8'));
                elseif ($key == 'order')
                    $url .= $value == 'ASC' ? '&order=DESC' : '&order=ASC';
                elseif ($key != 'start' AND $key != 'limit' AND $key != 'sort')
                    $url .= '&' . $key . '=' . $filter_array[$key];
            }
        }

        $this->data['sort_name'] = $this->url->link('customerpartner/sms', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
        $this->data['sort_id'] = $this->url->link('customerpartner/sms', 'token=' . $this->session->data['token'] . '&sort=id' . $url, 'SSL');
        $this->data['sort_message'] = $this->url->link('customerpartner/sms', 'token=' . $this->session->data['token'] . '&sort=message' . $url, 'SSL');

        $url = '';

        foreach ($filter_array as $key => $value) {
            if (isset($this->request->get[$key])) {
                if (!isset($this->request->get['order']) AND isset($this->request->get['sort']))
                    $url .= '&order=DESC';
                if ($key == 'filter_name' || $key == 'filter_message')
                    $url .= '&' . $key . '=' . urlencode(html_entity_decode($filter_array[$key], ENT_QUOTES, 'UTF-8'));
                elseif ($key != 'page')
                    $url .= '&' . $key . '=' . $filter_array[$key];
            }
        }

        $pagination = new Pagination();
        $pagination->total = $product_total;
        $pagination->page = $filter_array['page'];
        $pagination->limit = 10;
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('customerpartner/sms', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();
        $this->data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($filter_array['page'] - 1) * 10) + 1 : 0, ((($filter_array['page'] - 1) * 10) > ($product_total - 10)) ? $product_total : ((($filter_array['page'] - 1) * 10) + 10), $product_total, ceil($product_total / 10));

        foreach ($filter_array as $key => $value) {
            if ($key != 'start' AND $key != 'end')
                $this->data[$key] = $value;
        }

        $this->data['header'] = $this->load->controller('common/header');
        $this->data['footer'] = $this->load->controller('common/footer');
        $this->data['column_left'] = $this->load->controller('common/column_left');
        if (VERSION == '2.2.0.0') {
            $this->response->setOutput($this->load->view('customerpartner/sms_list', $this->data));
        } else {
            $this->response->setOutput($this->load->view('customerpartner/sms_list.tpl', $this->data));
        }
    }

    public function addSms() {

        $this->data = array_merge($this->data, $this->load->language('customerpartner/sms'));
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customerpartner/sms');

        $post_data = array(
            'sms_id',
            'name',
            'message',
        );

        foreach ($post_data as $post) {
            if (isset($this->request->post[$post])) {
                $this->data[$post] = $this->request->post[$post];
            } else {
                $this->data[$post] = '';
            }
        }

        if (isset($this->request->get['sms_id'])) {

            $result = $this->model_customerpartner_sms->getSmsData($this->request->get['sms_id']);
            if ($result)
                foreach ($result as $key => $value) {
                    $this->data[$key] = $value;
                }

            $this->data['sms_id'] = $this->request->get['sms_id'];
        }


        $this->data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        $url = '';

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('customerpartner/sms', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['cancel'] = $this->url->link('customerpartner/sms', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->data['save'] = $this->url->link('customerpartner/sms/smsSave', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $smsValues = str_replace('<br />', ',', nl2br($this->config->get('marketplace_sms_keywords')));
        $smsValues = explode(',', $smsValues);
        $find = array();
        foreach ($smsValues as $key => $value) {
            $find[] = trim($value);
        }

        $this->data['sms_help'] = $find;

        $this->data['header'] = $this->load->controller('common/header');
        $this->data['footer'] = $this->load->controller('common/footer');
        $this->data['column_left'] = $this->load->controller('common/column_left');
        if (VERSION == '2.2.0.0') {
            $this->response->setOutput($this->load->view('customerpartner/sms_form', $this->data));
        } else {
            $this->response->setOutput($this->load->view('customerpartner/sms_form.tpl', $this->data));
        }
    }

    public function smsSave() {

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->load->language('customerpartner/sms');

            $this->load->model('customerpartner/sms');

            /* if ((utf8_strlen($this->request->post['message']) < 25) || (utf8_strlen($this->request->post['message']) > 5000)) {	    	
              $this->error['warning'] = $this->language->get('error_message');
              } */

            if (utf8_strlen($this->request->post['message']) > 5000) {
                $this->error['warning'] = $this->language->get('error_message');
            }

            if ((utf8_strlen($this->request->post['name']) < 1)) {
                $this->error['warning'] = $this->language->get('error_name');
            }

            if (!isset($this->error['warning'])) {

                $this->model_customerpartner_sms->addSms($this->request->post);

                if ($this->request->post['sms_id'])
                    $this->session->data['success'] = $this->language->get('text_update');
                else
                    $this->session->data['success'] = $this->language->get('text_success');

                $this->response->redirect($this->url->link('customerpartner/sms', 'token=' . $this->session->data['token'], 'SSL'));
            }
        }

        $this->addSms();
    }

    public function delete() {

        $this->language->load('customerpartner/sms');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customerpartner/sms');

        if (isset($this->request->post['selected']) && $this->validate()) {
            foreach ($this->request->post['selected'] as $id) {
                $this->model_customerpartner_sms->deleteentry($id);
            }

            $this->session->data['success'] = $this->language->get('text_delete');

            $url = '';

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('customerpartner/sms', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->index();
    }

    private function validate() {

        if (!$this->user->hasPermission('modify', 'customerpartner/sms')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

?>