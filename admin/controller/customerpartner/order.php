<?php

class ControllerCustomerpartnerOrder extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('customerpartner/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customerpartner/order');

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['filter_order_id'])) {
            $filter_order_id = $this->commonfunctions->getOrderNumber($this->request->get['filter_order_id']);
        } else {
            $filter_order_id = null;
        }

        if (isset($this->request->get['filter_customer'])) {
            $filter_customer = $this->request->get['filter_customer'];
        } else {
            $filter_customer = null;
        }

        if (isset($this->request->get['filter_seller'])) {
            $filter_seller = $this->request->get['filter_seller'];
        } else {
            $filter_seller = null;
        }

        if (isset($this->request->get['filter_seller_email'])) {
            $filter_seller_email = $this->request->get['filter_seller_email'];
        } else {
            $filter_seller_email = null;
        }

        if (isset($this->request->get['filter_seller_number'])) {
            $filter_seller_number = $this->request->get['filter_seller_number'];
        } else {
            $filter_seller_number = null;
        }

        if (isset($this->request->get['filter_customer_number'])) {
            $filter_customer_number = $this->request->get['filter_customer_number'];
        } else {
            $filter_customer_number = null;
        }

        if (isset($this->request->get['filter_customer_email'])) {
            $filter_customer_email = $this->request->get['filter_customer_email'];
        } else {
            $filter_customer_email = null;
        }

        if (isset($this->request->get['filter_order_status'])) {
            $filter_order_status = $this->request->get['filter_order_status'];
        } else {
            $filter_order_status = null;
        }

        if (isset($this->request->get['filter_total'])) {
            $filter_total = $this->request->get['filter_total'];
        } else {
            $filter_total = null;
        }

        if (isset($this->request->get['filter_date_added'])) {
            $filter_date_added = $this->request->get['filter_date_added'];
        } else {
            $filter_date_added = null;
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $filter_date_modified = $this->request->get['filter_date_modified'];
        } else {
            $filter_date_modified = null;
        }

        if (isset($this->request->get['filter_seller_country'])) {
            $filter_seller_country = $this->request->get['filter_seller_country'];
        } else {
            $filter_seller_country = null;
        }

        if (isset($this->request->get['filter_payment_country'])) {
            $filter_payment_country = $this->request->get['filter_payment_country'];
        } else {
            $filter_payment_country = null;
        }

        if (isset($this->request->get['filter_shipping_country'])) {
            $filter_shipping_country = $this->request->get['filter_shipping_country'];
        } else {
            $filter_shipping_country = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'o.order_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }

        if (isset($this->request->get['filter_customer'])) {
            $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_seller'])) {
            $url .= '&filter_seller=' . urlencode(html_entity_decode($this->request->get['filter_seller'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_seller_email'])) {
            $url .= '&filter_seller_email=' . urlencode(html_entity_decode($this->request->get['filter_seller_email'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_seller_number'])) {
            $url .= '&filter_seller_number=' . urlencode(html_entity_decode($this->request->get['filter_seller_number'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_customer_number'])) {
            $url .= '&filter_customer_number=' . urlencode(html_entity_decode($this->request->get['filter_customer_number'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_customer_email'])) {
            $url .= '&filter_customer_email=' . urlencode(html_entity_decode($this->request->get['filter_customer_email'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_total'])) {
            $url .= '&filter_total=' . $this->request->get['filter_total'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
        }

        if (isset($this->request->get['filter_seller_country'])) {
            $url .= '&filter_seller_country=' . $this->request->get['filter_seller_country'];
        }

        if (isset($this->request->get['filter_payment_country'])) {
            $url .= '&filter_payment_country=' . $this->request->get['filter_payment_country'];
        }

        if (isset($this->request->get['filter_shipping_country'])) {
            $url .= '&filter_shipping_country=' . $this->request->get['filter_shipping_country'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['orders'] = array();

        $filter_data = array(
            'filter_order_id' => $filter_order_id,
            'filter_customer' => $filter_customer,
            'filter_seller' => $filter_seller,
            'filter_order_status' => $filter_order_status,
            'filter_total' => $filter_total,
            'filter_date_added' => $filter_date_added,
            'filter_date_modified' => $filter_date_modified,
            'filter_seller_email' => $filter_seller_email,
            'filter_seller_number' => $filter_seller_number,
            'filter_customer_number' => $filter_customer_number,
            'filter_customer_email' => $filter_customer_email,
            'filter_seller_country' => $filter_seller_country,
            'filter_payment_country' => $filter_payment_country,
            'filter_shipping_country' => $filter_shipping_country,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $order_total = $this->model_customerpartner_order->getTotalOrders($filter_data);

        $results = $this->model_customerpartner_order->getOrders($filter_data);

        foreach ($results as $result) {
            $data['orders'][] = array(
                'order_id' => $this->commonfunctions->convertOrderNumber($result['order_id'],$result['date_added']),
                'customer' => $result['customer'],
                'status' => $result['status'],
                'shipping_country' => $result['shipping_country'],
                'total' => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'date_modified' => date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
                'shipping_code' => $result['shipping_code'],
                'view' => $this->url->link('customerpartner/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $this->commonfunctions->convertOrderNumber($result['order_id'],$result['date_added']) . $url, 'SSL'),
                'edit' => $this->url->link('customerpartner/order/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $this->commonfunctions->convertOrderNumber($result['order_id'],$result['date_added']) . $url, 'SSL'),
                'edit_order' => $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $this->commonfunctions->convertOrderNumber($result['order_id'],$result['date_added']) . $url, 'SSL'),
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_missing'] = $this->language->get('text_missing');
        $data['text_loading'] = $this->language->get('text_loading');

        $data['column_order_id'] = $this->language->get('column_order_id');
        $data['column_customer'] = $this->language->get('column_customer');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_shipping_coutry'] = $this->language->get('column_shipping_coutry');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_action'] = $this->language->get('column_action');

        $data['entry_return_id'] = $this->language->get('entry_return_id');
        $data['entry_shipping_country'] = $this->language->get('entry_shipping_country');
        $data['entry_payment_country'] = $this->language->get('entry_payment_country');
        $data['entry_seller_country'] = $this->language->get('entry_seller_country');
        $data['entry_order_id'] = $this->language->get('entry_order_id');
        $data['entry_customer'] = $this->language->get('entry_customer');
        $data['entry_seller'] = $this->language->get('entry_seller');
        $data['entry_seller_email'] = $this->language->get('entry_seller_email');
        $data['entry_seller_number'] = $this->language->get('entry_seller_number');
        $data['entry_customer_number'] = $this->language->get('entry_customer_number');
        $data['entry_customer_email'] = $this->language->get('entry_customer_email');
        $data['entry_order_status'] = $this->language->get('entry_order_status');
        $data['entry_change_whole_order_status'] = $this->language->get('entry_change_whole_order_status');
        $data['entry_total'] = $this->language->get('entry_total');
        $data['entry_date_added'] = $this->language->get('entry_date_added');
        $data['entry_date_modified'] = $this->language->get('entry_date_modified');

        $data['button_invoice_print'] = $this->language->get('button_invoice_print');
        $data['button_shipping_print'] = $this->language->get('button_shipping_print');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['button_view'] = $this->language->get('button_view');
        $data['button_ip_add'] = $this->language->get('button_ip_add');

        $data['monthly_report'] = $this->language->get('monthly_report');

        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }

        if (isset($this->request->get['filter_seller'])) {
            $url .= '&filter_seller=' . urlencode(html_entity_decode($this->request->get['filter_seller'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_seller_email'])) {
            $url .= '&filter_seller_email=' . urlencode(html_entity_decode($this->request->get['filter_seller_email'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_seller_number'])) {
            $url .= '&filter_seller_number=' . urlencode(html_entity_decode($this->request->get['filter_seller_number'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_customer_number'])) {
            $url .= '&filter_customer_number=' . urlencode(html_entity_decode($this->request->get['filter_customer_number'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_customer_email'])) {
            $url .= '&filter_customer_email=' . urlencode(html_entity_decode($this->request->get['filter_customer_email'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_customer'])) {
            $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_total'])) {
            $url .= '&filter_total=' . $this->request->get['filter_total'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
        }

        if (isset($this->request->get['filter_seller_country'])) {
            $url .= '&filter_seller_country=' . $this->request->get['filter_seller_country'];
        }

        if (isset($this->request->get['filter_payment_country'])) {
            $url .= '&filter_payment_country=' . $this->request->get['filter_payment_country'];
        }

        if (isset($this->request->get['filter_shipping_country'])) {
            $url .= '&filter_shipping_country=' . $this->request->get['filter_shipping_country'];
        }



        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_order'] = $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . '&sort=o.order_id' . $url, 'SSL');
        $data['sort_customer'] = $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . '&sort=customer' . $url, 'SSL');
        $data['sort_status'] = $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
        $data['sort_total'] = $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . '&sort=o.total' . $url, 'SSL');
        $data['sort_date_added'] = $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . '&sort=o.date_added' . $url, 'SSL');
        $data['sort_date_modified'] = $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . '&sort=o.date_modified' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }

        if (isset($this->request->get['filter_customer'])) {
            $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_customer_email'])) {
            $url .= '&filter_customer_email=' . urlencode(html_entity_decode($this->request->get['filter_customer_email'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_seller'])) {
            $url .= '&filter_seller=' . urlencode(html_entity_decode($this->request->get['filter_seller'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_seller_email'])) {
            $url .= '&filter_seller_email=' . urlencode(html_entity_decode($this->request->get['filter_seller_email'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_seller_number'])) {
            $url .= '&filter_seller_number=' . urlencode(html_entity_decode($this->request->get['filter_seller_number'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_customer_number'])) {
            $url .= '&filter_customer_number=' . urlencode(html_entity_decode($this->request->get['filter_customer_number'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_total'])) {
            $url .= '&filter_total=' . $this->request->get['filter_total'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
        }

        if (isset($this->request->get['filter_seller_country'])) {
            $url .= '&filter_seller_country=' . $this->request->get['filter_seller_country'];
        }

        if (isset($this->request->get['filter_payment_country'])) {
            $url .= '&filter_payment_country=' . $this->request->get['filter_payment_country'];
        }

        if (isset($this->request->get['filter_shipping_country'])) {
            $url .= '&filter_shipping_country=' . $this->request->get['filter_shipping_country'];
        }



        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

        $data['filter_order_id'] = isset($this->request->get['filter_order_id'])?$this->request->get['filter_order_id'] : $filter_order_id;
        $data['filter_customer'] = $filter_customer;
        $data['filter_seller'] = $filter_seller;
        $data['filter_order_status'] = $filter_order_status;
        $data['filter_total'] = $filter_total;
        $data['filter_date_added'] = $filter_date_added;
        $data['filter_date_modified'] = $filter_date_modified;
        $data['filter_seller_email'] = $filter_seller_email;
        $data['filter_seller_number'] = $filter_seller_number;
        $data['filter_customer_number'] = $filter_customer_number;
        $data['filter_customer_email'] = $filter_customer_email;
        $data['filter_seller_country'] = $filter_seller_country;
        $data['filter_payment_country'] = $filter_payment_country;
        $data['filter_shipping_country'] = $filter_shipping_country;


        $this->load->model('localisation/order_status');
        $this->load->model('localisation/country');


        $data['countries'] = $this->model_localisation_country->getAvailableCountries();


        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['store'] = HTTPS_CATALOG;

        $data['header'] = $this->load->controller('common/header', array('breadcrumbs' => $data['breadcrumbs']));
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customerpartner/order_list.tpl', $data));
    }

    public function info() {
        $data = array();
        $data = array_merge($data, $this->load->language('customerpartner/order'));
        $this->load->model('customerpartner/order');

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->commonfunctions->getOrderNumber($this->request->get['order_id']);
        } else {
            $order_id = 0;
        }

        $order_info = $this->model_customerpartner_order->getOrder($order_id);

        if ($order_info) {

            if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

                if ($order_id) {

                    if (isset($this->request->post['tracking'])) {
                        $this->model_customerpartner_order->addOdrTracking($order_id, $this->request->post['tracking']);
                        $this->session->data['success'] = $this->language->get('text_success');
                    }

                    $this->response->redirect($this->url->link('customerpartner/order/info&order_id=' . $order_id, 'token=' . $this->session->data['token'], 'SSL'));
                }
            }

            if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];
                unset($this->session->data['success']);
            } else {
                $data['success'] = '';
            }

            $data['order_id'] = $this->request->get['order_id'];
            $data['real_order_id'] = $order_id;

            $this->document->setTitle($this->language->get('heading_title'));

            $data['heading_title'] = $this->language->get('heading_title');
            $data['text_tracking'] = $this->language->get("text_tracking");
            $data['text_booking_tracking'] = $this->language->get("text_booking_tracking");
            $data['text_product_order_details'] = $this->language->get('text_product_order_details');
            $data['column_time_added'] = $this->language->get('column_time_added');
            $data['column_username'] = $this->language->get('column_username');
            $data['column_invoice_number'] = $this->language->get('column_invoice_number');

            $url = '';

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $data['text_home'],
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['breadcrumbs'][] = array(
                'text' => $data['heading_title'],
                'href' => $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . $url, 'SSL')
            );

            $data['cancel'] = $this->url->link('customerpartner/order', 'token=' . $this->session->data['token'] . $url, 'SSL');

            $data['wksellerorderstatus'] = $this->config->get('marketplace_sellerorderstatus');

            if ($order_info['invoice_no']) {
                $data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
            } else {
                $data['invoice_no'] = '';
            }
            $data['email'] = $order_info['email'];

            $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

            if ($order_info['payment_address_format']) {
                $format = $order_info['payment_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}' . "\n" . '{telephone}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{telephone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['payment_firstname'],
                'lastname' => $order_info['payment_lastname'],
                'company' => $order_info['payment_company'],
                'address_1' => $order_info['payment_address_1'],
                'address_2' => $order_info['payment_address_2'],
                'city' => $order_info['payment_city'],
                'postcode' => $order_info['payment_postcode'],
                'zone' => $order_info['payment_zone'],
                'telephone' => $order_info['telephone'],
                'zone_code' => $order_info['payment_zone_code'],
                'country' => $order_info['payment_country']
            );

            $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $data['payment_method'] = $order_info['payment_method'];

            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{telephone}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{telephone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['shipping_address_2'],
                'city' => $order_info['shipping_city'],
                'postcode' => $order_info['shipping_postcode'],
                'zone' => $order_info['shipping_zone'],
                'telephone' => $order_info['shipping_telephone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country']
            );

            $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $data['shipping_method'] = $order_info['shipping_method'];

            $this->load->model('localisation/order_status');
            $order_statuses = $this->model_localisation_order_status->getOrderStatuses();

            $data['products'] = array();

            $products = $this->model_customerpartner_order->getSellerOrderProducts($order_id);

            // Uploaded files
            $this->load->model('tool/upload');

            foreach ($products as $product) {

                $option_data = array();

                $options = $this->model_customerpartner_order->getOrderOptions($order_id, $product['order_product_id']);

                // code changes due to download file error
                foreach ($options as $option) {
                    if ($option['type'] != 'file') {
                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $option['value'],
                            'type' => $option['type']
                        );
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);
                        if ($upload_info) {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $upload_info['name'],
                                'type' => $option['type'],
                                'href' => $this->url->link('account/customerpartner/orderinfo/download', '&code=' . $upload_info['code'], 'SSL')
                            );
                        }
                    }
                }
                $seller_details = $this->db->query("SELECT c.customer_id,CONCAT(c.firstname,' ',c.lastname) name,c.email,cd.companyname FROM " . DB_PREFIX . "customerpartner_to_product c2p LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer c ON (c2p.customer_id = c.customer_id) left join customerpartner_to_customer_description cd on (c.customer_id = cd.customer_id) WHERE c2p.product_id = '" . (int) $product['product_id'] . "'")->row;

                if ($seller_details && isset($seller_details['name']) && $seller_details['name']) {
                    $product['name'] = $product['name'] . '</a> by Seller <a href="' . $this->url->link('customerpartner/partner', 'token=' . $this->session->data['token'] . '&view_all=1&filter_email=' . $seller_details['email'], 'SSL') . '"><b>' . $seller_details['companyname'] . '</b></a>';
                }

                $product_tracking = $this->model_customerpartner_order->getOdrTracking($data['order_id'], $product['product_id']);

                if ($product['paid_status'] == 1) {
                    $paid_status = $this->language->get('text_paid');
                } else {
                    $paid_status = $this->language->get('text_not_paid');
                }

                $data['products'][] = array(
                    'order_product_id' => $product['order_product_id'],
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'seller_name' => $product['sellerName'],
                    'seller_link' => $this->url->link('customerpartner/partner/update&customer_id=' . $product['customer_id'], 'token=' . $this->session->data['token'], 'SSL'),
                    'option' => $option_data,
                    'tracking' => isset($product_tracking['tracking']) ? $product_tracking['tracking'] : '',
                    'quantity' => $product['quantity'],
                    'paid_status' => $paid_status,
                    'invoice_name' => isset($product['invoice_name']) ? $product['invoice_name'] : '',
                    'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'order_product_status' => $order_statuses[$product['order_product_status']]['name'],
                    'order_product_status_id' => $order_statuses[$product['order_product_status']]['order_status_id']
                );
            }

            // Voucher
            $data['vouchers'] = array();

            $vouchers = $this->model_customerpartner_order->getOrderVouchers($order_id);

            foreach ($vouchers as $voucher) {
                $data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                );
            }

            $data['totals'] = array();

            $totals = $this->model_customerpartner_order->getOrderTotals($order_id);

            if ($totals && isset($totals[0]['total'])) {
                $data['totals'][]['total'] = $this->currency->format($totals[0]['total'], $order_info['currency_code'], $order_info['currency_value']);
            }

            $data['comment'] = nl2br($order_info['comment']);

            $data['histories'] = array();

            $results = $this->model_customerpartner_order->getOrderHistories($order_id);

            foreach ($results as $result) {
                $data['histories'][] = array(
                    'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                    'time_added' => date("H:i:s", strtotime($result['date_added'])),
                    'status' => $result['status'],
                    'comment' => nl2br($result['comment']),
                    'username' => $result['username']
                );
            }

            $data['start_time_all'] = '';
            $data['headerhistories'] = array();
            $headerhistory = $this->model_customerpartner_order->getFullOrderHistories($order_id);
            if (!empty($headerhistory)) {
                $attention_time = 0;
                $warning_time = 0;
                $start_time = FALSE;
                $order_start_time = FALSE;
                $last_status_id = FALSE;
                $last_status_name = FALSE;
                $yellow_attention = " #F9F2AC"; // yello
                $red_warning = "#FBC5C1"; // Red
                $green_success = "#AEE1CC"; // Green
                $counter = 0;

                foreach ($headerhistory as $row) {
                    if ($order_start_time === FALSE) {
                        $order_start_time = strtotime($row['date_added']);
                    }

                    if ($start_time == FALSE) {
                        $last_status_id = $row['order_status_id'];
                        $last_status_name = $row['status'];
                        $start_time = strtotime($row['date_added']);
                        $attention_time = $order_statuses[$last_status_id]['attention_time'];
                        $warning_time = $order_statuses[$last_status_id]['warning_time'];
                    }

                    if ($row['order_status_id'] != $last_status_id) {
                        $end_time = strtotime($row['date_added']);
                        $diff_hours = (int) (($end_time - $start_time) / 3600);
                        $diff_minutes = (int) ((($end_time - $start_time) - ($diff_hours * 3600)) / 60);
                        $diff_numrice = (float) ("{$diff_hours}.{$diff_minutes}");
                        $diff = "{$diff_hours} h :{$diff_minutes} m";
                        $attention_time = $order_statuses[$last_status_id]['attention_time'];
                        $warning_time = $order_statuses[$last_status_id]['warning_time'];
                        $background = $diff_numrice > $warning_time ? $red_warning : ($diff_numrice > $attention_time ? $yellow_attention : $green_success);

                        $data['headerhistories'][$counter] = array();
                        $data['headerhistories'][$counter]['color'] = $background;
                        $data['headerhistories'][$counter]['status'] = $last_status_name;
                        $data['headerhistories'][$counter]['hours'] = $diff ;

                        $last_status_id = $row['order_status_id'];
                        $last_status_name = $row['status'];
                        $start_time = $end_time;
                        $counter++;
                    }
                }

                $end_time = strtotime(date('Y-m-d H:i:s'));

                if ($last_status_id != $this->config->get('config_success_status_id')) {
                    $diff_hours = (int) (($end_time - $start_time) / 3600);
                    $diff_minutes = (int) ((($end_time - $start_time) - ($diff_hours * 3600)) / 60);
                    $diff_numrice = (float) ("{$diff_hours}.{$diff_minutes}");
                    $diff = "{$diff_hours} h :{$diff_minutes} m";
                    $background = $diff_numrice > $warning_time ? $red_warning : ($diff_numrice > $attention_time ? $yellow_attention : $green_success);
                } else {
                    $diff = "";
                    $background = $green_success;
                }

                $data['headerhistories'][$counter]['color'] = $background;
                $data['headerhistories'][$counter]['status'] = $last_status_name;
                $data['headerhistories'][$counter]['hours'] = $diff ;

                $diff_days = (int) (($end_time - $order_start_time) / (3600*24));
                $diff_hours = (int) (($end_time - $order_start_time - ($diff_days*3600*24)) / 3600);
                $diff_minutes = (int) ((($end_time - $order_start_time) - ($diff_hours * 3600) - ($diff_days*3600*24)) / 60);

                $num_hours = $diff_days*24+$diff_hours;
                $diff_numrice = (float) ("{$num_hours}.{$diff_minutes}");
                $data['start_time_all'] = '';
                $data['start_time_all'] .= ($diff_days>0?"{$diff_days} Days ":"");
                $data['start_time_all'] .= ($diff_days>0 && $diff_hours >0) ? " - ":"";
                $data['start_time_all'] .= ($diff_hours?" {$diff_hours} Hours ":"");
                //$data['start_time_all'] .= ($diff_minutes?"{$diff_minutes} minutes":"");

                $background = $diff_numrice > (float)$this->config->get('config_warning_time') ? $red_warning : ($diff_numrice > (float)$this->config->get('config_attension_time') ? $yellow_attention : $green_success);
                $data['start_time_color'] = $background;
            }

            //list of status
            $this->load->model('localisation/order_status');

            if ($this->config->get('marketplace_available_order_status')) {
                $data['marketplace_available_order_status'] = $this->config->get('marketplace_available_order_status');
                $data['marketplace_order_status_sequence'] = $this->config->get('marketplace_order_status_sequence');
            }

            if ($this->config->get('marketplace_cancel_order_status') && $this->config->get('marketplace_available_order_status')) {

                $data['marketplace_cancel_order_status'] = $this->config->get('marketplace_cancel_order_status');

                $cancel_order_statusId_key = array_search($this->config->get('marketplace_cancel_order_status'), $data['marketplace_available_order_status'], true);

                if ($cancel_order_statusId_key == 0 || $cancel_order_statusId_key) {

                    unset($data['marketplace_available_order_status'][$cancel_order_statusId_key]);
                }

                foreach ($data['marketplace_order_status_sequence'] as $key => $value) {

                    if ($value['order_status_id'] == $this->config->get('marketplace_cancel_order_status')) {

                        unset($data['marketplace_order_status_sequence'][$key]);
                    }
                }
            } else {
                $data['marketplace_cancel_order_status'] = '';
            }

            $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

            $data['order_status_id'] = $order_info['order_status_id'];

            $data['token'] = $this->session->data['token'];

            $data['text_fullorder'] = $this->language->get('text_fullorder');
            $data['fullorder'] = $this->url->link('sale/order/info&order_id=' . $this->request->get['order_id'], 'token=' . $this->session->data['token'], 'SSL');
            $data['action'] = $this->url->link('customerpartner/order/info&order_id=' . $order_id, 'token=' . $this->session->data['token'], 'SSL');

            $data['header'] = $this->load->controller('common/header', $data);
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('customerpartner/order_info.tpl', $data));
        } else {
            $this->load->language('error/not_found');

            $this->document->setTitle($this->language->get('heading_title'));

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_not_found'] = $this->language->get('text_not_found');

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['header'] = $this->load->controller('common/header', array('breadcrumbs' => $data['breadcrumbs']));
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('error/not_found.tpl', $data));
        }
    }

    public function history() {

        $this->language->load('customerpartner/order');
        $this->load->model('customerpartner/order');
        $this->load->model('catalog/product');

        $product_names = array();
        $json = array();

        if ($this->config->get('marketplace_cancel_order_status')) {
            $marketplace_cancel_order_status = $this->config->get('marketplace_cancel_order_status');
        } else {
            $marketplace_cancel_order_status = '';
        }

        $this->load->model('localisation/order_status');
        $order_statuses = $this->model_localisation_order_status->getOrderStatuses();
        if (isset($this->request->post['change_whole_status']) && $this->request->post['change_whole_status'] == 1 && isset($this->request->post['order_status_id'])) {

            $this->model_customerpartner_order->addOrderHistory($this->request->get['order_id'], $this->request->post);

            $this->model_customerpartner_order->changeWholeOrderStatus($this->request->get['order_id'], $this->request->post['order_status_id']);

            $json['success'] = $this->language->get('text_success_history');
        } elseif (isset($this->request->post['comment']) && !empty($this->request->post['comment']) && empty($this->request->post['product_ids'])) {

            $getOrderStatusId = $this->model_customerpartner_order->getOrderStatusId($this->request->get['order_id']);

            $this->request->post['order_status_id'] = $getOrderStatusId['order_status_id'];

            $this->model_customerpartner_order->addOrderHistory($this->request->get['order_id'], $this->request->post);

            $json['success'] = $this->language->get('text_success_history');
        } elseif (isset($this->request->post['product_ids']) && !empty($this->request->post['product_ids'])) {

            $products = explode(",", $this->request->post['product_ids']);
            $order_products = explode(",", $this->request->post['order_product_ids']);

            foreach ($products as $value) {
                $product_details = $this->model_catalog_product->getProduct($value);
                $product_names[] = $product_details['name'];
            }

            $product_name = implode(",", $product_names);

            foreach ($order_statuses as $value) {

                if (in_array($this->request->post['order_status_id'], $value)) {
                    $seller_change_order_status_name = $value['name'];
                }
            }

            if ($this->request->post['order_status_id'] == $marketplace_cancel_order_status) {
                $this->changeOrderStatus($this->request->get, $this->request->post, $products, $order_products, $marketplace_cancel_order_status, $seller_change_order_status_name);
            } else {
                $this->changeOrderStatus($this->request->get, $this->request->post, $products, $order_products, $marketplace_cancel_order_status, $seller_change_order_status_name);
            }


            $json['success'] = $this->language->get('text_success_history');
        } else {

            $json['error'] = $this->language->get('error_product_select');
        }

        $this->response->setOutput(json_encode($json));
    }

    private function changeOrderStatus($get, $post, $products, $order_products, $marketplace_cancel_order_status, $seller_change_order_status_name) {

        if ($this->config->get('marketplace_complete_order_status')) {
            $marketplace_complete_order_status = $this->config->get('marketplace_complete_order_status');
        } else {
            $marketplace_complete_order_status = '';
        }
        /**
         * First step - Add seller changing status for selected products
         */
$get['order_id'] = $this->commonfunctions->getOrderNumber($get['order_id']);
        $this->model_customerpartner_order->addsellerorderproductstatus($get['order_id'], $post['order_status_id'], $products, $order_products);


        // Second Step - add comment for each selected products
        $this->model_customerpartner_order->addSellerOrderStatus($get['order_id'], $post['order_status_id'], $post, $products, $order_products, $seller_change_order_status_name);

        // Thired Step - Get status Id that will be the whole order status id after changed the order product status by seller
        $getWholeOrderStatus = $this->model_customerpartner_order->getWholeOrderStatus($get['order_id'], $marketplace_complete_order_status, $marketplace_cancel_order_status);

        $post['comment'] = (isset($get['comment']) && '' != $get['comment']) ? " AWBNo: " . $get['comment'] : $post['comment'];
        // Fourth Step - add comment in order_history table and send mails to admin(If admin notify is enable) and customer
        $this->model_customerpartner_order->addOrderHistory($get['order_id'], $post, $seller_change_order_status_name);

        // Fifth Step - Update whole order status in order table
        if ($getWholeOrderStatus) {
            $this->model_customerpartner_order->changeWholeOrderStatus($get['order_id'], $getWholeOrderStatus);
        }
    }

    public function track_order($order_id) {

        $this->load->model('customerpartner/order');

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }
        $log_data = '';

        $AWBNo = $this->model_customerpartner_order->getAWBNo($order_id);

        if (!empty($AWBNo)) {

            foreach ($AWBNo as $country_id => $aw) {
                if ($AWBNo[$country_id]['shipping_company_id'] == 1) {
                    $this->FFServices = new FirstFlight($this->registry, SHIPPING_FIRST_FLIGHT_ID);
                    $log_data .= $this->FFServices->LogData($AWBNo[$country_id]['AWBNo'], $country_id, $AWBNo[$country_id]['shipping_company']);
                } else if ($AWBNo[$country_id]['shipping_company_id'] == 2) {
                    $this->NaqelServices = new Naqel($this->registry, SHIPPING_NAQEL_ID);
                    $log_data .= $this->NaqelServices->LogData($AWBNo[$country_id]['AWBNo'], $country_id, $AWBNo[$country_id]['shipping_company']);
                } else if ($AWBNo[$country_id]['shipping_company_id'] == 3) {
                    // samsa
                    $this->SamsaShipping = new Samsashipping($registry, SHIPPING_SMSA_ID);
                    $log_data .= $this->SamsaShipping->LogData($AWBNo[$country_id]['AWBNo'], $country_id, $AWBNo[$country_id]['shipping_company']);
                } else if ($AWBNo[$country_id]['shipping_company_id'] == 4) {
                    $this->PostPlus = new Postplus($registry, SHIPPING_POST_PLUS_ID);
                    $log_data .= $this->PostPlus->LogData($AWBNo[$country_id]['AWBNo'], $country_id, $AWBNo[$country_id]['shipping_company']);
                }
            }
        }
        echo $log_data;
    }

    public function track_booking_order($order_id) {

        $this->load->model('customerpartner/order');


        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }
        $log_data = '';
        $AWBNo = $this->model_customerpartner_order->getAWBNo($order_id);

        if (!empty($AWBNo)) {
            echo 'The Order Was Shipped';
        } else {
            $Book_AWBNo = $this->model_customerpartner_order->getBookingAWBNo($order_id);
            foreach ($Book_AWBNo as $country_id => $aw) {
              $this->FFServices = new FirstFlight($this->registry, SHIPPING_FIRST_FLIGHT_ID);
                $log_data .= $this->FFServices->BookingTrack($Book_AWBNo[$country_id]['AWBNo'], $country_id, $Book_AWBNo[$country_id]['shipping_company']);
            }
        }
        echo $log_data;
    }

    public function order_details() {

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }

        if (isset($this->request->get['product_id'])) {
            $product_id = $this->request->get['product_id'];
        } else {
            $product_id = 0;
        }

        if ($product_id == 0 || $order_id == 0) {
            echo "Invalid order or product id";
            return false;
        }

        $this->load->model('customerpartner/order');
        $result = $this->model_customerpartner_order->getOrderProductDetails($order_id, $product_id);

        $no_result= array('Note: ' => ' No data was sent');
        if ($result) {
            $res = unserialize($result);
            echo json_encode(isset($res['argAwbBooking'])?$res['argAwbBooking']:$no_result);
        } else {
            echo json_encode($no_result);
        }
    }

}
