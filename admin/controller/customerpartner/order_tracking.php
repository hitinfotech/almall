<?php

class ControllerCustomerpartnerOrdertracking extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('customerpartner/order_tracking');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customerpartner/order_tracking');

        $this->getList();
    }

    protected function getList() {

        if (isset($this->request->get['filter_order_id'])) {
            $filter_order_id = $this->commonfunctions->getOrderNumber($this->request->get['filter_order_id']);
        } else {
            $filter_order_id = null;
        }

        if (isset($this->request->get['filter_seller'])) {
            $filter_seller = $this->request->get['filter_seller'];
        } else {
            $filter_seller = null;
        }

        if (isset($this->request->get['filter_seller_country'])) {
            $filter_seller_country = $this->request->get['filter_seller_country'];
        } else {
            $filter_seller_country = null;
        }

        if (isset($this->request->get['filter_awbno'])) {
            $filter_awbno = $this->request->get['filter_awbno'];
        } else {
            $filter_awbno = null;
        }

        if (isset($this->request->get['filter_order_status'])) {
            $filter_order_status = $this->request->get['filter_order_status'];
        } else {
            $filter_order_status = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'o.order_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }

        if (isset($this->request->get['filter_seller'])) {
            $url .= '&filter_seller=' . urlencode(html_entity_decode($this->request->get['filter_seller'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_seller_country'])) {
            $url .= '&filter_seller_country=' . $this->request->get['filter_seller_country'];
        }

        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_awbno'])) {
            $url .= '&filter_awbno=' . $this->request->get['filter_awbno'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('customerpartner/order_tracking', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['orders'] = array();

        $filter_data = array(
            'filter_order_id' => $filter_order_id,
            'filter_seller' => $filter_seller,
            'filter_seller_country' => $filter_seller_country,
            'filter_order_status' => $filter_order_status,
            'filter_awbno' => $filter_awbno,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $order_total = $this->model_customerpartner_order_tracking->getTotalOrders($filter_data);

        $results = $this->model_customerpartner_order_tracking->getOrders($filter_data);

        foreach ($results as $result) {
            $data['orders'][] = array(
                'order_id' => $this->commonfunctions->convertOrderNumber($result['order_id'],$result['date_added']),
                'AWBNo' => $result['awbno'],
                'seller_name' => $result['seller_name'],
                'customer_name' => $result['customer_name'],
                'status' => $result['status'],
                'order_status' => $result['order_status'],
                'date_added' => $result['date_added'],
                'view' => $this->url->link('customerpartner/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $this->commonfunctions->convertOrderNumber($result['order_id'],$result['date_added']) . $url, 'SSL'),
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_missing'] = $this->language->get('text_missing');
        $data['text_loading'] = $this->language->get('text_loading');

        $data['column_order_id'] = $this->language->get('column_order_id');
        $data['column_awbno'] = $this->language->get('column_awbno');
        $data['column_seller_name'] = $this->language->get('column_seller_name');
        $data['column_customer_name'] = $this->language->get('column_customer_name');
        $data['column_order_status'] = $this->language->get('column_order_status');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_action'] = $this->language->get('column_action');

        $data['entry_order_id'] = $this->language->get('entry_order_id');
        $data['entry_order_status'] = $this->language->get('entry_order_status');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['entry_seller'] = $this->language->get('entry_seller');
        $data['entry_awbno'] = $this->language->get('entry_awbno');

        $data['button_filter'] = $this->language->get('button_filter');
        $data['button_view'] = $this->language->get('button_view');

        $data['token'] = $this->session->data['token'];

        $paginator_url = $url;

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }



        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $data['sort_order'] = $this->url->link('customerpartner/order_tracking', 'token=' . $this->session->data['token'] . '&sort=c2o.order_id' . $url, 'SSL');
        $data['sort_awbno'] = $this->url->link('customerpartner/order_tracking', 'token=' . $this->session->data['token'] . '&sort=awbno' . $url, 'SSL');
        $data['sort_seller_name'] = $this->url->link('customerpartner/order_tracking', 'token=' . $this->session->data['token'] . '&sort=seller_name' . $url, 'SSL');
        $data['sort_customer_name'] = $this->url->link('customerpartner/order_tracking', 'token=' . $this->session->data['token'] . '&sort=customer_name' . $url, 'SSL');
        $data['sort_order_status'] = $this->url->link('customerpartner/order_tracking', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
        $data['sort_date_added'] = $this->url->link('customerpartner/order_tracking', 'token=' . $this->session->data['token'] . '&sort=c2o.date_added' . $url, 'SSL');

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
            $paginator_url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if (isset($this->request->get['order'])) {
            $paginator_url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('customerpartner/order_tracking', 'token=' . $this->session->data['token'] . $paginator_url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

        $data['filter_order_id'] = $this->commonfunctions->convertOrderNumber($filter_order_id);
        $data['filter_seller'] = $filter_seller;
        $data['filter_seller_country'] = $filter_seller_country;
        $data['filter_order_status'] = $filter_order_status;
        $data['filter_awbno'] = $filter_awbno;


        $this->load->model('localisation/order_status');
        $this->load->model('localisation/country');


        $data['countries'] = $this->model_localisation_country->getAvailableCountries();


        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['store'] = HTTPS_CATALOG;

        $data['header'] = $this->load->controller('common/header', array('breadcrumbs' => $data['breadcrumbs']));
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customerpartner/order_tracking_list.tpl', $data));
    }
}
