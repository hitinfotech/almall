<?php

class ControllerMarketingPopup extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('marketing/popup');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('localisation/country');

        $this->getList();
    }

    public function add() {
        $this->load->language('marketing/popup');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->load->model('marketing/popup');
            $this->model_marketing_popup->addSetting('popup', $this->request->post);


            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketing/popup', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm('add');
    }

    public function edit() {
        $this->load->language('marketing/popup');


        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->load->model('marketing/popup');
            $this->model_marketing_popup->editSetting('popup', $this->request->post, $this->request->get['page_id']);
            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketing/popup', 'token=' . $this->session->data['token'], 'SSL'));

        }

        $this->getForm('edit');
    }

    protected function getList() {
        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('marketing/popup', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['add'] = $this->url->link('marketing/popup/add', 'token=' . $this->session->data['token'], 'SSL');
        $data['delete'] = $this->url->link('marketing/popup/delete', 'token=' . $this->session->data['token'], 'SSL');


        $data['countries'] = array();

        $available_coutnries = $this->model_localisation_country->getAvailableCountries();
        $country_names = array();
        foreach ($available_coutnries as $count) {
            $country_names[$count['country_id']] = $count['name'];
        }

        $Langauges = array(
            1 => 'English',
            2 => 'Arabic',
        );

        $this->load->model('marketing/popup');
        $pages = $this->model_marketing_popup->getPages();

        foreach ($pages as $page_id => $page) {
            $name = '';
            foreach ($page as $k => $value) {
                $name .= '( ' . $country_names[$value['country_id']] . ' ) ';
            }

            $data['countries'][] = array(
                'page_id' => $page_id,
                'name' => $name,
                'language' => $Langauges[$value['language_id']],
                'edit' => $this->url->link('marketing/popup/edit', 'token=' . $this->session->data['token'] . '&page_id=' . $page_id, 'SSL')
            );
        }


        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_url'] = $this->language->get('column_url');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('marketing/popup_list.tpl', $data));
    }

    public function getForm($type = 'new') {

        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_popup_block0_image1'] = $this->language->get('text_popup_block0_image1');
        $data['text_popup_block0_image2'] = $this->language->get('text_popup_block0_image2');
        $data['text_popup_block0_url1'] = $this->language->get('text_popup_block0_url1');
        $data['text_popup_block0_description1'] = $this->language->get('text_popup_block0_description1');
        $data['text_popup_block0_title1'] = $this->language->get('text_popup_block0_title1');
        $data['text_popup_block0_color1'] = $this->language->get('text_popup_block0_color1');
        $data['text_popup_block0_status1'] = $this->language->get('text_popup_block0_status1');
        $data['text_popup_block2_description1'] = $this->language->get('text_popup_block2_description1');
        $data['text_popup_block2_coupon1'] = $this->language->get('text_popup_block2_coupon1');
        $data['text_popup_block2_title1'] = $this->language->get('text_popup_block2_title1');

        $data['text_popup_block1_title1'] = $this->language->get('text_popup_block1_title1');
        $data['text_popup_block1_button1'] = $this->language->get('text_popup_block1_button1');
        $data['text_popup_block1_description1'] = $this->language->get('text_popup_block1_description1');
        $data['text_popup_block1_url1'] = $this->language->get('text_popup_block1_url1');
        $data['text_popup_block1_image1'] = $this->language->get('text_popup_block1_image1');
        $data['text_country'] = $this->language->get('text_country');


        $data['text_brand'] = $this->language->get('text_brand');
        $data['text_brand_name'] = $this->language->get('text_brand_name');
        $data['text_brand_desc'] = $this->language->get('text_brand_desc');
        $data['text_brand_button'] = $this->language->get('text_brand_button');

        $data['text_none'] = $this->language->get('text_none');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['tab_block0'] = $this->language->get('tab_block0');
        $data['tab_block1'] = $this->language->get('tab_block1');
        $data['tab_block2'] = $this->language->get('tab_block2');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['popup_block0_image1'])) {
            $data['error_popup_block0_image1'] = $this->error['popup_block0_image1'];
        } else {
            $data['error_popup_block0_image1'] = '';
        }
        if (isset($this->error['popup_block0_image2'])) {
            $data['error_popup_block0_image2'] = $this->error['popup_block0_image2'];
        } else {
            $data['error_popup_block0_image2'] = '';
        }

        if (isset($this->error['popup_block0_title1'])) {
            $data['error_popup_block0_title1'] = $this->error['popup_block0_title1'];
        } else {
            $data['error_popup_block0_title1'] = '';
        }


        if (isset($this->error['popup_block0_description1'])) {
            $data['error_popup_block0_description1'] = $this->error['popup_block0_description1'];
        } else {
            $data['error_popup_block0_description1'] = '';
        }

        if (isset($this->error['popup_block0_color1'])) {
            $data['error_popup_block0_color1'] = $this->error['popup_block0_color1'];
        } else {
            $data['error_popup_block0_color1'] = '';
        }

        if (isset($this->error['popup_block0_status1'])) {
            $data['error_popup_block0_status1'] = $this->error['popup_block0_status1'];
        } else {
            $data['error_popup_block0_status1'] = '';
        }

        if (isset($this->error['popup_block1_image1'])) {
            $data['error_popup_block1_image1'] = $this->error['popup_block1_image1'];
        } else {
            $data['error_popup_block1_image1'] = '';
        }

        if (isset($this->error['popup_block1_title1'])) {
            $data['error_popup_block1_title1'] = $this->error['popup_block1_title1'];
        } else {
            $data['error_popup_block1_title1'] = '';
        }

        if (isset($this->error['popup_block1_button2'])) {
            $data['error_popup_block1_button2'] = $this->error['popup_block1_button2'];
        } else {
            $data['error_popup_block1_button2'] = '';
        }

        if (isset($this->error['popup_block1_description1'])) {
            $data['error_popup_block1_description1'] = $this->error['popup_block1_description1'];
        } else {
            $data['error_popup_block1_description1'] = '';
        }

        if (isset($this->error['popup_block2_title1'])) {
            $data['error_popup_block2_title1'] = $this->error['popup_block2_title1'];
        } else {
            $data['error_popup_block2_title1'] = '';
        }

        if (isset($this->error['popup_block2_description1'])) {
            $data['error_popup_block2_description1'] = $this->error['popup_block2_description1'];
        } else {
            $data['error_popup_block2_description1'] = '';
        }
        if (isset($this->error['popup_block2_coupon1'])) {
            $data['error_popup_block2_coupon1'] = $this->error['popup_block2_coupon1'];
        } else {
            $data['error_popup_block2_coupon1'] = '';
        }


        $this->load->model('marketing/popup');


        $page_id = '';
        $url_page = '';
        $data['product_relateds'] = array();
        if (isset($this->request->get['page_id'])) {
            $page_id = $this->request->get['page_id'];
            $url_page = '&page_id=' . $page_id;
            $data['product_relateds'] = $this->model_marketing_popup->getPageProduct($page_id, 'popup');
        }


        $this->load->model('tool/image');
        $this->load->model('localisation/country');
        $this->load->model('localisation/language');

        $data['available_coutnries'] = $this->model_localisation_country->getAvailableCountries();


        $data['text_form'] = $this->language->get('text_edit');
        $data['action'] = $this->url->link('marketing/popup/' . $type, 'token=' . $this->session->data['token'] . $url_page, 'SSL');
        $data['cancel'] = $this->url->link('marketing/popup', 'token=' . $this->session->data['token'], 'SSL');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('marketing/popup', 'token=' . $this->session->data['token'], 'SSL')
        );

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $page_countries = array ();
        if (isset($this->request->get['page_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {

            $this->load->model('marketing/popup');

            $country_language_info = $this->model_marketing_popup->getSetting('popup', $page_id);
            $page_countries = $this->model_marketing_popup->getPageCountries('popup', $page_id);

        }
        $data['page_language'] = 1;
        if(!empty($page_countries)){
          $data['page_language']= array_values(array_unique($page_countries))[0];
        }

        $data['page_countries'] = $page_countries;

        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['popup_block0_image1'])) {
            $data['popup_block0_image1'] = $this->request->post['popup_block0_image1'];
        } elseif (isset($country_language_info['popup_block0_image1'])) {
            $data['popup_block0_image1'] = $country_language_info['popup_block0_image1'];
        } else {
            $data['popup_block0_image1'] = '';
        }
        if (isset($this->request->post['popup_block0_image1']) && is_file(DIR_IMAGE . $this->request->post['popup_block0_image1'])) {
            $data['thumb_01'] = $this->model_tool_image->resize($this->request->post['popup_block0_image1'], 100, 100, false);
        } elseif (isset($country_language_info['popup_block0_image1']) && is_file(DIR_IMAGE . $country_language_info['popup_block0_image1'])) {
            $data['thumb_01'] = $this->model_tool_image->resize($country_language_info['popup_block0_image1'], 100, 100, false);
        } else {
            $data['thumb_01'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }
        if (isset($this->request->post['popup_block0_image2'])) {
            $data['popup_block0_image2'] = $this->request->post['popup_block0_image2'];
        } elseif (isset($country_language_info['popup_block0_image2'])) {
            $data['popup_block0_image2'] = $country_language_info['popup_block0_image2'];
        } else {
            $data['popup_block0_image2'] = '';
        }
        if (isset($this->request->post['popup_block0_image2']) && is_file(DIR_IMAGE . $this->request->post['popup_block0_image2'])) {
            $data['thumb_02'] = $this->model_tool_image->resize($this->request->post['popup_block0_image2'], 100, 100, false);
        } elseif (isset($country_language_info['popup_block0_image2']) && is_file(DIR_IMAGE . $country_language_info['popup_block0_image2'])) {
            $data['thumb_02'] = $this->model_tool_image->resize($country_language_info['popup_block0_image2'], 100, 100, false);
        } else {
            $data['thumb_02'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        if (isset($this->request->post['popup_block0_title1'])) {
            $data['popup_block0_title1'] = $this->request->post['popup_block0_title1'];
        } elseif (isset($country_language_info['popup_block0_title1'])) {
            $data['popup_block0_title1'] = $country_language_info['popup_block0_title1'];
        } else {
            $data['popup_block0_title1'] = '';
        }

        if (isset($this->request->post['popup_block0_color1'])) {
            $data['popup_block0_color1'] = $this->request->post['popup_block0_color1'];
        } elseif (isset($country_language_info['popup_block0_color1'])) {
            $data['popup_block0_color1'] = $country_language_info['popup_block0_color1'];
        } else {
            $data['popup_block0_color1'] = '';
        }

        if (isset($this->request->post['popup_block0_status1'])) {
            $data['popup_block0_status1'] = $this->request->post['popup_block0_status1'];
        } elseif (isset($country_language_info['popup_block0_status1'])) {
            $data['popup_block0_status1'] = $country_language_info['popup_block0_status1'];
        } else {
            $data['popup_block0_status1'] = '';
        }

        if (isset($this->request->post['popup_block0_description1'])) {
            $data['popup_block0_description1'] = $this->request->post['popup_block0_description1'];
        } elseif (isset($country_language_info['popup_block0_description1'])) {
            $data['popup_block0_description1'] = $country_language_info['popup_block0_description1'];
        } else {
            $data['popup_block0_description1'] = '';
        }
        if (isset($this->request->post['popup_block1_url1'])) {
            $data['popup_block1_url1'] = $this->request->post['popup_block1_url1'];
        } elseif (isset($country_language_info['popup_block1_url1'])) {
            $data['popup_block1_url1'] = $country_language_info['popup_block1_url1'];
        } else {
            $data['popup_block1_url1'] = '';
        }
        if (isset($this->request->post['popup_block1_image1'])) {
            $data['popup_block1_image1'] = $this->request->post['popup_block1_image1'];
        } elseif (isset($country_language_info['popup_block1_image1'])) {
            $data['popup_block1_image1'] = $country_language_info['popup_block1_image1'];
        } else {
            $data['popup_block1_image1'] = '';
        }

        if (isset($this->request->post['popup_block1_title1'])) {
            $data['popup_block1_title1'] = $this->request->post['popup_block1_title1'];
        } elseif (isset($country_language_info['popup_block1_title1'])) {
            $data['popup_block1_title1'] = $country_language_info['popup_block1_title1'];
        } else {
            $data['popup_block1_title1'] = '';
        }

        if (isset($this->request->post['popup_block1_description1'])) {
            $data['popup_block1_description1'] = $this->request->post['popup_block1_description1'];
        } elseif (isset($country_language_info['popup_block1_description1'])) {
            $data['popup_block1_description1'] = $country_language_info['popup_block1_description1'];
        } else {
            $data['popup_block1_description1'] = '';
        }
        if (isset($this->request->post['popup_block2_title1'])) {
            $data['popup_block2_title1'] = $this->request->post['popup_block2_title1'];
        } elseif (isset($country_language_info['popup_block2_title1'])) {
            $data['popup_block2_title1'] = $country_language_info['popup_block2_title1'];
        } else {
            $data['popup_block2_title1'] = '';
        }

        if (isset($this->request->post['popup_block2_description1'])) {
            $data['popup_block2_description1'] = $this->request->post['popup_block2_description1'];
        } elseif (isset($country_language_info['popup_block2_description1'])) {
            $data['popup_block2_description1'] = $country_language_info['popup_block2_description1'];
        } else {
            $data['popup_block2_description1'] = '';
        }

        if (isset($this->request->post['popup_block2_coupon1'])) {
            $data['popup_block2_coupon1'] = $this->request->post['popup_block2_coupon1'];
        } elseif (isset($country_language_info['popup_block2_coupon1'])) {
            $data['popup_block2_coupon1'] = $country_language_info['popup_block2_coupon1'];
        } else {
            $data['popup_block2_coupon1'] = '';
        }


        if (isset($this->request->post['popup_block1_image2']) && is_file(DIR_IMAGE . $this->request->post['popup_block1_image2'])) {
            $data['thumb_12'] = $this->model_tool_image->resize($this->request->post['popup_block1_image2'], 100, 100, false);
        } elseif (isset($country_language_info['popup_block1_image2']) && is_file(DIR_IMAGE . $country_language_info['popup_block1_image2'])) {
            $data['thumb_12'] = $this->model_tool_image->resize($country_language_info['popup_block1_image2'], 100, 100, false);
        } else {
            $data['thumb_12'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        $data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('marketing/popup_form.tpl', $data));
    }

    public function delete() {
        $this->load->language('marketing/popup');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('marketing/popup');

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $page_id) {
                $this->model_marketing_popup->deleteSetting('popup', $page_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketing/popup', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getList();
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'marketing/popup')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

}
