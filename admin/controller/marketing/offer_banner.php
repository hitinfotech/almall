<?php

class ControllerMarketingOfferBanner extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('marketing/offer_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('localisation/country');

        $this->getList();
    }

    public function add() {
        $this->load->language('marketing/offer_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->load->model('marketing/offer_banner');
            $page_id = $this->model_marketing_offer_banner->addSetting('offer_banner', $this->request->post);
            $this->model_marketing_offer_banner->addProduct($this->request->post, $page_id);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketing/offer_banner', '', 'SSL'));
        }

        $this->getForm('add');
    }

    public function edit() {
        $this->load->language('marketing/offer_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->load->model('marketing/offer_banner');
            $this->model_marketing_offer_banner->editSetting('offer_banner', $this->request->post, $this->request->get['page_id']);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketing/offer_banner', '', 'SSL'));
        }

        $this->getForm('edit');
    }

    public function delete() {
        $this->load->language('marketing/offer_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('marketing/offer_banner');

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $page_id) {
                $this->model_marketing_offer_banner->deleteSetting('offer_banner', $page_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketing/offer_banner', '', 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {
        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', '', 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('marketing/offer_banner', '', 'SSL')
        );

        $data['countries'] = array();

        $available_coutnries = $this->model_localisation_country->getAvailableCountries();
        $country_names = array();
        foreach ($available_coutnries as $count) {
            $country_names[$count['country_id']] = $count['name'];
        }

        $Langauges = array(
            1 => 'English',
            2 => 'Arabic',
        );

        $this->load->model('marketing/offer_banner');
        $pages = $this->model_marketing_offer_banner->getPages('offer_banner');
        foreach ($pages as $page_id => $page) {
            $name = '';
            foreach ($page as $k => $value) {
                $name .= '( ' . $country_names[$value['country_id']] . ' ) ';
            }
            $data['countries'][] = array(
                'page_id' => $page_id,
                'name' => $name,
                'language' => $Langauges[$value['language_id']],
                'name' => $name,
                'edit' => $this->url->link('marketing/offer_banner/edit', '' . '&page_id=' . $page_id, 'SSL')
            );
        }

        $data['add'] = $this->url->link('marketing/offer_banner/add', '', 'SSL');
        $data['delete'] = $this->url->link('marketing/offer_banner/delete', '', 'SSL');


        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_countries'] = $this->language->get('column_countries');
        $data['column_language'] = $this->language->get('column_language');
        $data['column_page_id'] = $this->language->get('column_page_id');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('marketing/offer_banner_list.tpl', $data));
    }

    public function getForm($type = 'new') {

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_offer_banner_block5_image1'] = $this->language->get('text_offer_banner_block5_image1');


        $data['entry_products'] = $this->language->get('entry_products');



        $data['button_block_add'] = $this->language->get('button_block_add');
        $data['text_none'] = $this->language->get('text_none');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['tab_block1'] = $this->language->get('tab_block1');
        $data['tab_block2'] = $this->language->get('tab_block2');
        $data['tab_block3'] = $this->language->get('tab_block3');
        $data['tab_block4'] = $this->language->get('tab_block4');
        $data['tab_block5'] = $this->language->get('tab_block5');
        $data['tab_block6'] = $this->language->get('tab_block6');
        $data['tab_block'] = $this->language->get('tab_block');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['offer_banner_block5_image1'])) {
            $data['error_offer_banner_block5_image1'] = $this->error['offer_banner_block5_image1'];
        } else {
            $data['error_offer_banner_block5_image1'] = '';
        }


        $this->load->model('tool/image');
        $this->load->model('localisation/country');
        $this->load->model('localisation/language');
        $this->load->model('marketing/offer_banner');

        $data['text_form'] = $this->language->get('text_edit');
        $data['selectedproducts'] = $this->url->link('cms/productlandingFeaturedProducts', '', 'SSL');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', '', 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('marketing/offer_banner', '', 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_settings'),
            'href' => $this->url->link('marketing/offer_banner/edit', '', 'SSL')
        );


        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        
        $url_page = '';
        if (isset($this->request->get['page_id'])) {
            $page_id = $this->request->get['page_id'];
            $url_page = '&page_id=' . $page_id;
        }

        $page_countries = array();
        if (isset($this->request->get['page_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            
            $country_language_info = $this->model_marketing_offer_banner->getSetting('offer_banner', $page_id);
            $page_countries = $this->model_marketing_offer_banner->getPageCountries('offer_banner', $page_id);
        }
        $data['page_language'] = 1;
        if (!empty($page_countries)) {
            $data['page_language'] = $page_countries[array_keys($page_countries)[0]]['language_id'];
        }

        $data['page_gender'] = 1;
        if (!empty($page_countries)) {
            if ($page_countries[array_keys($page_countries)[0]]['gender'] > 0) {
                $data['page_gender'] = $page_countries[array_keys($page_countries)[0]]['gender'];
            }
        }

        $data['page_countries'] = $page_countries;

        $data['available_coutnries'] = $this->model_localisation_country->getAvailableCountries();

        $data['action'] = $this->url->link('marketing/offer_banner/' . $type, $url_page, 'SSL');

        $data['cancel'] = $this->url->link('marketing/offer_banner', '', 'SSL');

        if (isset($this->request->post['offer_banner_block5_image1'])) {
            $data['offer_banner_block5_image1'] = $this->request->post['offer_banner_block5_image1'];
        } elseif (isset($country_language_info['offer_banner_block5_image1'])) {
            $data['offer_banner_block5_image1'] = $country_language_info['offer_banner_block5_image1'];
        } else {
            $data['offer_banner_block5_image1'] = '';
        }

        if (isset($this->request->post['offer_banner_block5_image1']) && is_file(DIR_IMAGE . $this->request->post['offer_banner_block5_image1'])) {
            $data['thumb_51'] = $this->model_tool_image->resize($this->request->post['offer_banner_block5_image1'], 100, 100, false);
        } elseif (isset($country_language_info['offer_banner_block5_image1']) && is_file(DIR_IMAGE . $country_language_info['offer_banner_block5_image1'])) {
            $data['thumb_51'] = $this->model_tool_image->resize($country_language_info['offer_banner_block5_image1'], 100, 100, false);
        } else {
            $data['thumb_51'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        $data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('marketing/offer_banner_form.tpl', $data));
    }

    protected function validateForm() {

        if (!$this->user->hasPermission('modify', 'marketing/offer_banner')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /*
          if (!$this->request->post['offer_banner_block5_image1'] && ($this->request->files['offer_banner_block5_image1']['error'])) {
          $this->error['offer_banner_block5_image1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_offer_banner_block5_image1'));
          }
          if (!$this->request->post['offer_banner_block5_image1'] && ($this->request->files['offer_banner_block5_image1']['error'])) {
          $this->error['offer_banner_block5_image1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('offer_banner_block5_image1'));
          } */

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

}
