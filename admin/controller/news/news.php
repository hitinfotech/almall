<?php

class ControllerNewsNews extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('news/news');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('news/news');
        $this->getList();
    }

    public function add() {
        $this->language->load('news/news');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('news/news');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $news_id = $this->model_news_news->addNews($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($news_id) && !empty($news_id)) {
                $url .= '&filter_news_id=' . $news_id;
            }
            $this->response->redirect($this->url->link('news/news', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function edit() {
        $this->language->load('news/news');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('news/news');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_news_news->editNews($this->request->get['news_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success_add');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($this->request->get['news_id']) && !empty($this->request->get['news_id'])) {
                $url .= '&filter_news_id=' . $this->request->get['news_id'];
            }
            $this->response->redirect($this->url->link('news/news', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->language->load('news/news');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('news/news');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $news_id) {
                $this->model_news_news->deleteNews($news_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('news/news', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'n.start_date';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = $this->request->get['filter_country_id'];
            $data['filter_country_id'] = $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = null;
        }

        if (isset($this->request->get['filter_news_id'])) {
            $filter_news_id = $this->request->get['filter_news_id'];
            $data['filter_news_id'] = $this->request->get['filter_news_id'];
        } else {
            $filter_news_id = null;
        }

        // $offer_description = '';
        if (isset($this->request->get['filter_description'])) {
            $filter_description = $this->request->get['filter_description'];
            $data['filter_description'] = $this->request->get['filter_description'];
        } else {
            $filter_description = null;
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('news/news', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('news/news/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('news/news/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['refresh'] = $this->url->link('news/news/refresh', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['news'] = array();
        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        $data['countries'] = $aDBCountries;
        // echo "<pre>"; die(print_r($aDBCountries));

        $filter_data = array(
            'filter_news_id' => $filter_news_id,
            'filter_country_id' => $filter_country_id,
            'filter_description' => $filter_description,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $news_total = $this->model_news_news->getTotalNews($filter_data);
        $results = $this->model_news_news->getNewsList($filter_data);

        foreach ($results as $result) {
            $data['news'][] = array(
                'news_id' => $result['news_id'],
                'title' => $result['title'],
                'date_start' => $result['start_date'],
                'date_end' => $result['end_date'],
                'date_added' => $result['date_added'],
                'sort_order' => $result['sort_order'],
                'user_add' => $result['user_add'],
                'user_modify' => $result['user_modify'],
                'edit' => $this->url->link('news/news/edit', 'token=' . $this->session->data['token'] . '&news_id=' . $result['news_id'] . $url, 'SSL'),
                'delete' => $this->url->link('news/news/delete', 'token=' . $this->session->data['token'] . '&news_id=' . $result['news_id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['entry_title'] = $this->language->get('entry_title');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['column_user_id'] = $this->language->get('column_user_id');
        $data['column_last_mod_id'] = $this->language->get('column_last_mod_id');
        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['column_title'] = $this->language->get('column_title');
        $data['entry_start_date'] = $this->language->get('entry_start_date');
        $data['entry_end_date'] = $this->language->get('entry_end_date');
        $data['entry_date_added'] = $this->language->get('entry_date_added');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_featured'] = $this->language->get('column_featured');
        $data['column_action'] = $this->language->get('column_action');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['select_country'] = $this->language->get('select_country');
        $data['filter_discription'] = $this->language->get('filter_discription');

        $data['entry_description'] = $this->language->get('entry_description');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');
        $data['token'] = $this->session->data['token'];
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($filter_country_id) && !empty($filter_country_id)) {
            $url .= '&filter_country_id=' . $filter_country_id;
        }
        if (isset($filter_description) && !empty($filter_description)) {
            $url .= '&filter_description=' . $filter_description;
        }

        $data['sort_title'] = $this->url->link('news/news', 'token=' . $this->session->data['token'] . $url . '&sort=n.news_id', 'SSL');
        $data['sort_date_added'] = $this->url->link('news/news', 'token=' . $this->session->data['token'] . $url . '&sort=n.date_added', 'SSL');
        $data['sort_start_date'] = $this->url->link('news/news', 'token=' . $this->session->data['token'] . $url . '&sort=n.start_date', 'SSL');
        $data['sort_end_date'] = $this->url->link('news/news', 'token=' . $this->session->data['token'] . $url . '&sort=n.end_date', 'SSL');

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_news'] = $this->url->link('news/news', 'token=' . $this->session->data['token'] . '&sort=news_id' . $url, 'SSL');
        $data['sort_order'] = $this->url->link('news/news', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, 'SSL');

        $pagination = new Pagination();
        $pagination->total = $news_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('news/news', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($news_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($news_total - $this->config->get('config_limit_admin'))) ? $news_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $news_total, ceil($news_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['filter_country_id'] = $filter_country_id;
        $data['filter_discription'] = $filter_description;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('news/news_list.tpl', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['bActiveLanguageTabFlag'] = true;
        $data['bActiveLanguagePaneFlag'] = true;

        $data['user_id'] = $this->session->data['user_id'];

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_attribute'] = $this->language->get('tab_attribute');
        $data['tab_links'] = $this->language->get('tab_links');
        $data['tab_image'] = $this->language->get('tab_image');

        $data['text_form'] = !isset($this->request->get['_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');

        //-- Description/Attribute
        $data['entry_title'] = $this->language->get('entry_title');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_body_text'] = $this->language->get('entry_body_text');

        //-- General
        $data['entry_start_date'] = $this->language->get('entry_start_date');
        $data['entry_end_date'] = $this->language->get('entry_end_date');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['column_action'] = $this->language->get('column_action');

        //-- Images
        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_featured'] = $this->language->get('entry_featured');
        $data['button_image_add'] = $this->language->get('button_image_add');
        $data['button_remove'] = $this->language->get('button_remove');

        //-- Links
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_city_id'] = $this->language->get('entry_city_id');
        $data['entry_mall_id'] = $this->language->get('entry_mall_id');
        $data['entry_shop_id'] = $this->language->get('entry_shop_id');
        $data['entry_brand_id'] = $this->language->get('entry_brand_id');
        $data['entry_group_id'] = $this->language->get('entry_group_id');
        $data['entry_category_id'] = $this->language->get('entry_category_id');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_home'] = $this->language->get('entry_home');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['title'])) {
            $data['error_title'] = $this->error['title'];
        } else {
            $data['error_title'] = array();
        }

        if (isset($this->error['error_date_start'])) {
            $data['error_date_start'] = $this->error['error_date_start'] . date('Y-m-d H:i:s', strtotime($this->request->post['start_date'])) . "  " . date('Y-m-d H:i:s');
        } else {
            $data['error_date_start'] = '';
        }

        if (isset($this->error['error_date_end'])) {
            $data['error_date_end'] = $this->error['error_date_end'];
        } else {
            $data['error_date_end'] = '';
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('news/news', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['news_id'])) {
            $data['action'] = $this->url->link('news/news/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('news/news/edit', 'token=' . $this->session->data['token'] . '&news_id=' . $this->request->get['news_id'] . $url, 'SSL');
        }
        $data['cancel'] = $this->url->link('news/news', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['news_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $news_info = $this->model_news_news->getNews($this->request->get['news_id']);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();
        if (isset($this->request->post['news_description'])) {
            $data['news_description'] = $this->request->post['news_description'];
        } elseif (!empty($news_info)) {
            $data['news_description'] = $this->model_news_news->getNewsDescriptions($news_info['news_id']);
        } else {
            $data['news_description'] = array();
        }


        if (isset($this->request->post['start_date'])) {
            $data['start_date'] = $this->request->post['start_date'];
        } elseif (!empty($news_info)) {
            $data['start_date'] = $news_info['start_date'];
        } else {
            $data['start_date'] = "";
        }

        if (isset($this->request->post['end_date'])) {
            $data['end_date'] = $this->request->post['end_date'];
        } elseif (!empty($news_info)) {
            $data['end_date'] = $news_info['end_date'];
        } else {
            $data['end_date'] = "";
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (isset($this->request->get['news_id'])) {
            $data['sort_order'] = $news_info['sort_order'];
        } else {
            $data['sort_order'] = '';
        }
        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($news_info)) {
            $data['status'] = $news_info['status'];
        } else {
            $data['status'] = 0;
        }

        if (isset($this->request->post['home'])) {
            $data['home'] = $this->request->post['home'];
        } elseif (!empty($news_info)) {
            $data['home'] = $news_info['home'];
        } else {
            $data['home'] = 0;
        }

        //-- Images
        $this->load->model('tool/image');
        $data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 100, 100);

        // Images
        if (isset($this->request->post['news_image'])) {
            $news_images = $this->request->post['news_image'];
        } elseif (isset($this->request->get['news_id'])) {
            $news_images = $this->model_news_news->getNewsImages($this->request->get['news_id']);
        } else {
            $news_images = array();
        }

        $data['news_images'] = array();

        foreach ($news_images as $news_image) {
            if (is_file(DIR_IMAGE . $news_image['image'])) {
                $image = $news_image['image'];
                $thumb = $news_image['image'];
            } else {
                $image = '';
                $thumb = 'placeholder.png';
            }

            $data['news_images'][] = array(
                'image' => $image,
                'thumb' => $this->model_tool_image->resize($thumb, 100, 100),
                'sort_order' => $news_image['sort_order'],
                'featured' => $news_image['featured']
            );
        }

        $data['shops'] = array();
        if (isset($this->request->get['news_id'])) {
            $results = $this->model_news_news->getNewsShops($this->request->get['news_id']);
            foreach ($results as $row) {
                $data['shops'][] = array(
                    'shop_id' => $row['shop_id'],
                    'name' => strip_tags(html_entity_decode($row['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $data['brands'] = array();
        if (isset($this->request->get['news_id'])) {
            $results = $this->model_news_news->getNewsBrands($this->request->get['news_id']);
            foreach ($results as $row) {
                $data['brands'][] = array(
                    'brand_id' => $row['brand_id'],
                    'name' => strip_tags(html_entity_decode($row['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $this->load->model('design/layout');

        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('news/news_form.tpl', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'news/news')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (isset($this->request->post['news_id']) && (int) $this->request->post['news_id'] <= 0) {
            $this->error['error_news_id'] = $this->language->get('error_news_id');
        }

        // foreach ($this->request->post['news_description'] as $language_id => $value) {
        //     if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 255)) {
        //         $this->error['title'][$language_id] = $this->language->get('error_title');
        //     }
        //     if ((utf8_strlen($value['description']) < 3) || (utf8_strlen($value['description']) > 255)) {
        //         $this->error['description'][$language_id] = $this->language->get('error_description');
        //     }
        //     if ((utf8_strlen($value['body_text']) < 3) || (utf8_strlen($value['body_text']) > 255)) {
        //         $this->error['error_body_text'][$language_id] = $this->language->get('error_body_text');
        //     }
        // }

        /* if (!isset($this->request->post['start_date']) && date('Y-m-d', strtotime($this->request->post['start_date'])) < date('Y-m-d')) {
          $this->error['error_date_start'] = $this->language->get('error_date_start');
          }
          if (!isset($this->request->post['end_date']) && date('Y-m-d', strtotime($this->request->post['end_date'])) < date('Y-m-d')) {
          $this->error['error_date_end'] = $this->language->get('error_date_end');
          } */

        /* $count = 0;
          if (isset($this->request->post['link_country']) && is_array($this->request->post['link_country'])) {
          $count += count($this->request->post['link_country']);
          }
          if (isset($this->request->post['link_city']) && is_array($this->request->post['link_city'])) {
          $count += count($this->request->post['link_city']);
          }
          if (isset($this->request->post['link_mall']) && is_array($this->request->post['link_mall'])) {
          $count += count($this->request->post['link_mall']);
          }
          if (isset($this->request->post['link_shop']) && is_array($this->request->post['link_shop'])) {
          $count += count($this->request->post['link_shop']);
          }
          if (isset($this->request->post['link_group']) && is_array($this->request->post['link_group'])) {
          $count += count($this->request->post['link_group']);
          }
          if ($count <= 0) {
          $this->error['error_links'] = $this->language->get('error_links');
          } */

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'news/news')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete() {

        // die(print_r($this->request->get));

        $json = array();

        if (isset($this->request->get['query'])) {

            if (isset($this->request->get['query'])) {
                $filter_name = $this->request->get['query'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['type'])) {
                $type = $this->request->get['type'];
            } else {
                $type = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array('filter_name' => $filter_name);

            if ($type == 'shop') {
                $this->load->model('mall/shop');
                $results = $this->model_mall_shop->getShopsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')) . ' ( ' . strip_tags(html_entity_decode($result['mall_name'], ENT_QUOTES, 'UTF-8')) . ' ) '
                    );
                }
            } else if ($type == 'brand') {
                $this->load->model('mall/brand');
                $results = $this->model_mall_brand->getAutoCompleteBrandsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['brand_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
