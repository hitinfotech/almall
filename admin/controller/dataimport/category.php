<?php

class ControllerDataimportCategory extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('dataimport/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('dataimport/category');

        $this->getForm();
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['group_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');

        $data['entry_select_file'] = $this->language->get('entry_select_file');
        $data['confirm_message'] = $this->language->get('confirm_message');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_data'] = $this->language->get('tab_data');
        $data['tab_design'] = $this->language->get('tab_design');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['format'])) {
            $data['error_format'] = $this->error['format'];
        } else {
            $data['error_format'] = array();
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        
        if (isset($this->session->data['report'])) {
            $data['report'] = $this->session->data['report'];
            $data['success'] = '';
            unset($this->session->data['report']);
        } else {
            $data['report'] = '';
        }
        
        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('dataimport/category', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['action'] = $this->url->link('dataimport/category/add', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['cancel'] = $this->url->link('catalog/group', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['token'] = $this->session->data['token'];

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('dataimport/category_form.tpl', $data));
    }

    public function add() {
        $this->language->load('dataimport/category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('dataimport/category');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['group_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');

        $data['entry_select_file'] = $this->language->get('entry_select_file');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_data'] = $this->language->get('tab_data');
        $data['tab_design'] = $this->language->get('tab_design');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            //die(var_dump($_FILES));
            $this->session->data['report'] = $this->model_dataimport_category->addFile($_FILES);

            $this->session->data['success'] = $this->language->get('text_success');
            $url = '';
            $this->response->redirect($this->url->link('dataimport/category', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'dataimport/category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (isset($_FILES["category"]) && !empty($_FILES["category"])) {
            //die(var_dump($_FILES));
            if (!$_FILES['category']["tmp_name"]) {
                $this->error['warning'] = $this->language->get('error_filename');
            } else if ($_FILES["category"]["type"] != "text/csv") {
                $this->error['warning'] = $this->language->get('error_filetype');
            }
        }

        return !$this->error;
    }

}
