<?php

class ControllerCatalogLandingPages extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('catalog/landing_pages');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('localisation/country');

        $this->getLandingList();
    }

    public function add() {
        $this->load->language('catalog/landing_pages');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->get['parent_id']) && $this->request->get['parent_id'] > 0) {
            $this->load->model('catalog/landing_pages');
            $page_id = $this->model_catalog_landing_pages->addSetting($this->request->get['parent_id'], 'landing_pages', $this->request->post);
            $this->model_catalog_landing_pages->addProduct($this->request->post, 'landing_pages', $page_id);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/landing_pages/sub_pages', 'page_id=' . $this->request->get['parent_id'], 'SSL'));
        }

        $this->getLandingForm('add');
    }

    public function edit() {
        $this->load->language('catalog/landing_pages');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->get['parent_id']) && $this->request->get['parent_id'] > 0 && isset($this->request->get['page_id']) && $this->request->get['page_id'] > 0) {

            $this->load->model('catalog/landing_pages');
            $this->model_catalog_landing_pages->editSetting($this->request->get['parent_id'], 'landing_pages', $this->request->post, $this->request->get['page_id']);
            $this->model_catalog_landing_pages->addProduct($this->request->post, 'landing_pages', $this->request->get['page_id']);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/landing_pages/sub_pages', 'page_id=' . $this->request->get['parent_id'], 'SSL'));
        }

        $this->getLandingForm('edit');
    }

    public function delete_landing_sub_pages() {
        $this->load->language('catalog/landing_pages');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/landing_pages');

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $page_id) {
                $this->model_catalog_landing_pages->deleteSubPage($page_id, 'landing_pages', $this->request->get['parent_id']);
            }
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('catalog/landing_pages/sub_pages', 'page_id=' . $this->request->get['parent_id'], 'SSL'));
        }

        $this->getLandingList();
    }

    public function add_landing_page() {
        $this->load->language('catalog/landing_pages');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->load->model('catalog/landing_pages');
            $page_id = $this->model_catalog_landing_pages->addPage($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/landing_pages', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getLandingForm();
    }

    public function edit_landing_page() {
        $this->load->language('catalog/landing_pages');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->load->model('catalog/landing_pages');
            $page_id = $this->model_catalog_landing_pages->editPage($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/landing_pages', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getLandingForm();
    }

    public function delete_landing_page() {
        $this->load->language('catalog/landing_pages');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/landing_pages');

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $page_id) {
                $this->model_catalog_landing_pages->deletePage($page_id);
            }
            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/landing_pages', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getLandingList();
    }

    public function getLandingList() {

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/landing_pages', 'token=' . $this->session->data['token'], 'SSL')
        );


        $this->load->model('catalog/landing_pages');
        $pages = $this->model_catalog_landing_pages->getPages();
        if (!empty($pages)) {
            foreach ($pages as $page_id => $page) {
                $data['pages'][] = array(
                    'page_id' => $page['id'],
                    'name' => $page['page_name'],
                    'name_ar' => $page['page_name_ar'],
                    'date_modified' => $page['date_modified'],
                    'edit' => $this->url->link('catalog/landing_pages/edit_landing_page', 'page_id=' . $page['id'], 'SSL'),
                    'sub_pages' => $this->url->link('catalog/landing_pages/sub_pages', 'page_id=' . $page['id'], 'SSL')
                );
            }
        }

        $data['add'] = $this->url->link('catalog/landing_pages/add_landing_page', 'token=' . $this->session->data['token'], 'SSL');
        $data['delete'] = $this->url->link('catalog/landing_pages/delete_landing_page', '', 'SSL');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_sub_pages'] = $this->language->get('text_sub_pages');
        $data['button_sub_pages'] = $this->language->get('button_sub_pages');

        $data['column_id'] = $this->language->get('column_id');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_name_ar'] = $this->language->get('column_name_ar');
        $data['column_innserpages'] = $this->language->get('column_innserpages');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/landing_pages_list.tpl', $data));
    }

    public function getLandingForm() {

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/landing_pages', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['action'] = $this->url->link('catalog/landing_pages/add_landing_page', '', 'SSL');

        $page_id = 0;
        if (isset($this->request->get['page_id'])) {
            $this->load->model("catalog/landing_pages");
            $data['page_id'] = $this->request->get['page_id'];
            $data['page_data'] = $this->model_catalog_landing_pages->getPage($data['page_id']);
            $data['action'] = $this->url->link('catalog/landing_pages/edit_landing_page', '', 'SSL');
        }

        if (isset($this->request->post['icon'])) {
            $data['icon'] = $this->request->post['icon'];
        } elseif (!empty($data['page_data'])) {
            $data['icon'] = $data['page_data']['icon'];
        } else {
            $data['icon'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['icon']) && is_file(DIR_IMAGE . $this->request->post['icon'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['icon'], 130, 130, false);
        } elseif (!empty($data['page_data']) && is_file(DIR_IMAGE . $data['page_data']['icon'])) {
            $data['thumb'] = $this->model_tool_image->resize($data['page_data']['icon'], 130, 130, false);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('placeholder.png', 130, 130, false);
        }

        $data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 130, 130, false);


        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_form'] = $this->language->get('text_settings');
        $data['text_input_landing_page_name'] = $this->language->get('text_input_landing_page_name');
        $data['entry_icon'] = $this->language->get('entry_icon');
        $data['text_input_landing_color'] = $this->language->get('text_input_landing_page_color');
        $data['text_input_landing_hcolor'] = $this->language->get('text_input_landing_page_hcolor');
        $data['text_input_landing_background'] = $this->language->get('text_input_landing_page_background');
        $data['text_input_landing_hbackground'] = $this->language->get('text_input_landing_page_hbackground');
        $data['text_input_landing_page_name_ar'] = $this->language->get('text_input_landing_page_name_ar');
        $data['text_show_at_homepage'] = $this->language->get('text_show_at_homepage');
        $data['text_fontbold'] = $this->language->get('text_fontbold');
        $data['text_status'] = $this->language->get('text_status');
        $data['button_save'] = $this->language->get('button_save');


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/landing_pages_form.tpl', $data));
    }

    public function sub_pages() {

        $page_id = 0;
        if (isset($this->request->get['page_id'])) {
            $this->load->language('catalog/landing_pages');
            $this->load->model("catalog/landing_pages");
            $data['page_id'] = $this->request->get['page_id'];
            $page_info = $this->model_catalog_landing_pages->getPage($data['page_id']);
            $page_subs = $this->model_catalog_landing_pages->getSubPage($data['page_id']);
            if (!empty($page_subs)) {
                foreach ($page_subs as $key => $value) {
                    $data['page_subs'][$key] = $value;
                    $data['page_subs'][$key]['edit'] = $this->url->link('catalog/landing_pages/add_landing_sub_pages', 'type=edit&parent_id=' . $data['page_id'] . "&page_id=" . $value['page_id'], 'SSL');
                }
            }


            $title = $data['heading_title_subs'] = $data['text_list_subs'] = sprintf($this->language->get('heading_title_subs'), $page_info['page_name']);

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', '', 'SSL')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('catalog/landing_pages', '', 'SSL')
            );

            $data['breadcrumbs'][] = array(
                'text' => $title,
                'href' => $this->url->link('catalog/landing_pages/sub_pages', 'page_id=' . $this->request->get['page_id'], 'SSL')
            );

            if (isset($this->error['warning'])) {
                $data['error_warning'] = $this->error['warning'];
            } else {
                $data['error_warning'] = '';
            }

            if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];
                unset($this->session->data['success']);
            } else {
                $data['success'] = '';
            }

            if (isset($this->request->post['selected'])) {
                $data['selected'] = (array) $this->request->post['selected'];
            } else {
                $data['selected'] = array();
            }

            $data['add'] = $this->url->link('catalog/landing_pages/add_landing_sub_pages', 'type=new&parent_id=' . $data['page_id'], 'SSL');
            $data['delete'] = $this->url->link('catalog/landing_pages/delete_landing_sub_pages', 'parent_id=' . $data['page_id'], 'SSL');
            $data['button_add'] = $this->language->get('button_add');
            $data['button_edit'] = $this->language->get('button_edit');
            $data['button_delete'] = $this->language->get('button_delete');

            $data['text_confirm'] = $this->language->get('text_confirm');
            $data['text_no_results'] = $this->language->get('text_no_results');
            $data['column_action'] = $this->language->get('column_action');
            $data['column_country'] = $this->language->get('column_country');
            $data['column_language'] = $this->language->get('column_language');
            $data['column_page_h1'] = $this->language->get('column_page_h1');
            $data['column_page_title'] = $this->language->get('column_page_title');
            $data['column_date_modified'] = $this->language->get('column_date_modified');
            $data['column_id'] = $this->language->get('column_id');



            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('catalog/landing_pages_subs_list.tpl', $data));
        } else {
            $this->getLandingList();
        }
    }

    public function add_landing_sub_pages() {

        $this->load->language('catalog/landing_pages');
        $this->load->model("catalog/landing_pages");

        $type = 'new';
        if (isset($this->request->get['type']) && $this->request->get['type'] == 'edit') {
            $type = 'edit';
        }


        if (isset($this->request->get['parent_id']) && $this->request->get['parent_id'] > 0) {
            $parent_id = $this->request->get['parent_id'];
        } else {
            $this->response->redirect($this->url->link('catalog/landing_pages', '', 'SSL'));
        }
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_landing_pages_title'] = $this->language->get('text_landing_pages_title');
        $data['text_landing_pages_h1'] = $this->language->get('text_landing_pages_h1');
        $data['text_landing_pages_seo'] = $this->language->get('text_landing_pages_seo');

        $data['text_landing_pages_block1_title1'] = $this->language->get('text_landing_pages_block1_title1');
        $data['text_landing_pages_block1_button1'] = $this->language->get('text_landing_pages_block1_button1');
        $data['text_landing_pages_block1_description1'] = $this->language->get('text_landing_pages_block1_description1');
        $data['text_landing_pages_block1_url1'] = $this->language->get('text_landing_pages_block1_url1');
        $data['text_landing_pages_block1_image1'] = $this->language->get('text_landing_pages_block1_image1');

        $data['text_landing_pages_block1_title2'] = $this->language->get('text_landing_pages_block1_title2');
        $data['text_landing_pages_block1_button2'] = $this->language->get('text_landing_pages_block1_button2');
        $data['text_landing_pages_block1_description2'] = $this->language->get('text_landing_pages_block1_description2');
        $data['text_landing_pages_block1_url2'] = $this->language->get('text_landing_pages_block1_url2');
        $data['text_landing_pages_block1_image2'] = $this->language->get('text_landing_pages_block1_image2');

        $data['entry_products'] = $this->language->get('entry_products');
        $data['text_landing_pages_additional_block_title'] = $this->language->get('text_landing_pages_additional_block_title');
        $data['text_landing_pages_additional_block_action'] = $this->language->get('text_landing_pages_additional_block_action');
        $data['text_landing_pages_additional_block_position'] = $this->language->get('text_landing_pages_additional_block_position');
        $data['text_landing_pages_additional_block_url'] = $this->language->get('text_landing_pages_additional_block_url');
        $data['text_landing_pages_additional_block_image'] = $this->language->get('text_landing_pages_additional_block_image');

        $data['button_block_add'] = $this->language->get('button_block_add');
        $data['text_none'] = $this->language->get('text_none');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['entry_category'] = $this->language->get('entry_category');
        $data['entry_brand'] = $this->language->get('entry_brand');

        $data['tab_block1'] = $this->language->get('tab_block1');
        $data['tab_block2'] = $this->language->get('tab_block2');
        $data['tab_block3'] = $this->language->get('tab_block3');
        $data['tab_block4'] = $this->language->get('tab_block4');
        $data['tab_block5'] = $this->language->get('tab_block5');
        $data['tab_block'] = $this->language->get('tab_block');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $this->load->model('tool/image');
        $this->load->model('localisation/country');
        $this->load->model('localisation/language');
        $this->load->model('catalog/feature_page');
        $this->load->model('catalog/category');
        $this->load->model('mall/brand');

        $data['text_form'] = $this->language->get('text_edit');

        $data['action'] = $this->url->link('catalog/landing_pages/add', 'type=' . $type . '&parent_id=' . $parent_id, 'SSL');
        $data['cancel'] = $this->url->link('catalog/landing_pages/sub_pages', 'page_id=' . $parent_id, 'SSL');
        $data['selectedproducts'] = $this->url->link('cms/productlandingFeaturedProducts', 'token=' . $this->session->data['token'], 'SSL');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/landing_pages', '', 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_settings'),
            'href' => $this->url->link('catalog/landing_pages/sub_pages', 'page_id=' . $parent_id, 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_settings'),
            'href' => $this->url->link('catalog/landing_pages/add_landing_sub_pages', 'parent_id=' . $parent_id . '&type=' . $type, 'SSL')
        );


        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $url_page = '';
        $page_countries = array();
        if (isset($this->request->get['page_id'])) {
            $data['action'] = $this->url->link('catalog/landing_pages/edit', 'type=' . $type . '&parent_id=' . $parent_id . '&page_id=' . $this->request->get['page_id'], 'SSL');
            $page_id = $this->request->get['page_id'];
            $url_page = '&page_id=' . $page_id;
            $this->load->model('catalog/landing_pages');
            $country_language_info = $this->model_catalog_landing_pages->getSetting('landing_pages', $page_id);
            $page_countries = $this->model_catalog_landing_pages->getPageCountries('landing_pages', $page_id);
        }
        $data['page_language'] = 1;
        if (!empty($page_countries)) {
            $data['page_language'] = $page_countries[array_keys($page_countries)[0]]['language_id'];
        }

        $data['page_countries'] = $page_countries;

        $data['available_coutnries'] = $this->model_localisation_country->getAvailableCountries();

        $selected_products = array();
        if (isset($this->request->get['page_id'])) {
            $selected_products_array = $this->model_catalog_landing_pages->getSelectedProducts('landing_pages', $this->request->get['page_id']);

            foreach ($selected_products_array as $key => $prod) {
                $selected_products[] = array(
                    'product_id' => $prod['product_id'],
                    'product_name' => $prod['name'],
                );
            }
        }

        $data['selected_products'] = $selected_products;
        $additional_products = array();
        if (isset($country_language_info) && !empty($country_language_info)) {
            foreach ($country_language_info as $key => $value) {
                if (preg_match('/landing_pages_additional_/', $key)) {
                    $additional_bloacks[$key] = $value;
                }
            }
            if (!empty($additional_bloacks)) {
                $additional_products_array = $this->model_catalog_landing_pages->getProducts('landing_pages', $this->request->get['page_id']);

                foreach ($additional_products_array as $key => $prod) {
                    if (isset($additional_bloacks['landing_pages_additional_block' . $prod['block_id'] . '_title' . $prod['block_id']])) {
                        $additional_products[$prod['block_id']]['title'] = $additional_bloacks['landing_pages_additional_block' . $prod['block_id'] . '_title' . $prod['block_id']];
                        $additional_products[$prod['block_id']]['action'] = $additional_bloacks['landing_pages_additional_block' . $prod['block_id'] . '_action' . $prod['block_id']];
                        $additional_products[$prod['block_id']]['position'] = $this->db->query("SELECT position from product_land WHERE page_id=".$this->request->get['page_id']." AND `key` = 'landing_pages_additional_block". $prod['block_id'] . '_action' . $prod['block_id']."' AND deleted = '0' ")->row['position'];
                    }
                    $additional_products[$prod['block_id']]['products'][] = array(
                        'product_id' => $prod['product_id'],
                        'product_name' => $prod['name'],
                    );
                }
            }
        }
        $data['additional_products'] = $additional_products;


        if (isset($this->request->post['page_categories'])) {
            $categories = $this->request->post['product_category'];
        } elseif (isset($this->request->get['page_id'])) {
            $categories = $this->model_catalog_landing_pages->getPageCategories($this->request->get['page_id']);
        } else {
            $categories = array();
        }

        $data['page_categories'] = array();

        foreach ($categories as $category_id) {
            $category_info = $this->model_catalog_category->getCategory($category_id);

            if ($category_info) {
                $data['page_categories'][] = array(
                    'category_id' => $category_info['category_id'],
                    'name' => ($category_info['path']) ? $category_info['path'] . ' &gt; ' . $category_info['name'] : $category_info['name']
                );
            }
        }

        if (isset($this->request->post['page_brands'])) {
            $brands = $this->request->post['page_brands'];
        } elseif (isset($this->request->get['page_id'])) {
            $brands = $this->model_catalog_landing_pages->getPageBrands($this->request->get['page_id']);
        } else {
            $brands = array();
        }

        $data['page_brands'] = array();

        foreach ($brands as $brand_id) {
            $brand_info = $this->model_mall_brand->getBrand($brand_id);

            if ($brand_info) {
                $data['page_brands'][] = array(
                    'brand_id' => $brand_info['brand_id'],
                    'name' => $brand_info['name']
                );
            }
        }

        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['landing_pages_block1_title1'])) {
            $data['landing_pages_block1_title1'] = $this->request->post['landing_pages_block1_title1'];
        } elseif (isset($country_language_info['landing_pages_block1_title1'])) {
            $data['landing_pages_block1_title1'] = $country_language_info['landing_pages_block1_title1'];
        } else {
            $data['landing_pages_block1_title1'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_button1'])) {
            $data['landing_pages_block1_button1'] = $this->request->post['landing_pages_block1_button1'];
        } elseif (isset($country_language_info['landing_pages_block1_button1'])) {
            $data['landing_pages_block1_button1'] = $country_language_info['landing_pages_block1_button1'];
        } else {
            $data['landing_pages_block1_button1'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_description1'])) {
            $data['landing_pages_block1_description1'] = $this->request->post['landing_pages_block1_description1'];
        } elseif (isset($country_language_info['landing_pages_block1_description1'])) {
            $data['landing_pages_block1_description1'] = $country_language_info['landing_pages_block1_description1'];
        } else {
            $data['landing_pages_block1_description1'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_url1'])) {
            $data['landing_pages_block1_url1'] = $this->request->post['landing_pages_block1_url1'];
        } elseif (isset($country_language_info['landing_pages_block1_url1'])) {
            $data['landing_pages_block1_url1'] = $country_language_info['landing_pages_block1_url1'];
        } else {
            $data['landing_pages_block1_url1'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_image1'])) {
            $data['landing_pages_block1_image1'] = $this->request->post['landing_pages_block1_image1'];
        } elseif (isset($country_language_info['landing_pages_block1_image1'])) {
            $data['landing_pages_block1_image1'] = $country_language_info['landing_pages_block1_image1'];
        } else {
            $data['landing_pages_block1_image1'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_image1']) && is_file(DIR_IMAGE . $this->request->post['landing_pages_block1_image1'])) {
            $data['thumb_11'] = $this->model_tool_image->resize($this->request->post['landing_pages_block1_image1'], 100, 100, false);
        } elseif (isset($country_language_info['landing_pages_block1_image1']) && is_file(DIR_IMAGE . $country_language_info['landing_pages_block1_image1'])) {
            $data['thumb_11'] = $this->model_tool_image->resize($country_language_info['landing_pages_block1_image1'], 100, 100, false);
        } else {
            $data['thumb_11'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        if (isset($this->request->post['landing_pages_block1_title2'])) {
            $data['landing_pages_block1_title2'] = $this->request->post['landing_pages_block1_title2'];
        } elseif (isset($country_language_info['landing_pages_block1_title2'])) {
            $data['landing_pages_block1_title2'] = $country_language_info['landing_pages_block1_title2'];
        } else {
            $data['landing_pages_block1_title2'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_button2'])) {
            $data['landing_pages_block1_button2'] = $this->request->post['landing_pages_block1_button2'];
        } elseif (isset($country_language_info['landing_pages_block1_button2'])) {
            $data['landing_pages_block1_button2'] = $country_language_info['landing_pages_block1_button2'];
        } else {
            $data['landing_pages_block1_button2'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_description2'])) {
            $data['landing_pages_block1_description2'] = $this->request->post['landing_pages_block1_description2'];
        } elseif (isset($country_language_info['landing_pages_block1_description2'])) {
            $data['landing_pages_block1_description2'] = $country_language_info['landing_pages_block1_description2'];
        } else {
            $data['landing_pages_block1_description2'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_url2'])) {
            $data['landing_pages_block1_url2'] = $this->request->post['landing_pages_block1_url2'];
        } elseif (isset($country_language_info['landing_pages_block1_url2'])) {
            $data['landing_pages_block1_url2'] = $country_language_info['landing_pages_block1_url2'];
        } else {
            $data['landing_pages_block1_url2'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_image2'])) {
            $data['landing_pages_block1_image2'] = $this->request->post['landing_pages_block1_image2'];
        } elseif (isset($country_language_info['landing_pages_block1_image2'])) {
            $data['landing_pages_block1_image2'] = $country_language_info['landing_pages_block1_image2'];
        } else {
            $data['landing_pages_block1_image2'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_image2']) && is_file(DIR_IMAGE . $this->request->post['landing_pages_block1_image2'])) {
            $data['thumb_12'] = $this->model_tool_image->resize($this->request->post['landing_pages_block1_image2'], 100, 100, false);
        } elseif (isset($country_language_info['landing_pages_block1_image2']) && is_file(DIR_IMAGE . $country_language_info['landing_pages_block1_image2'])) {
            $data['thumb_12'] = $this->model_tool_image->resize($country_language_info['landing_pages_block1_image2'], 100, 100, false);
        } else {
            $data['thumb_12'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        if (isset($this->request->post['landing_pages_block1_title3'])) {
            $data['landing_pages_block1_title3'] = $this->request->post['landing_pages_block1_title3'];
        } elseif (isset($country_language_info['landing_pages_block1_title3'])) {
            $data['landing_pages_block1_title3'] = $country_language_info['landing_pages_block1_title3'];
        } else {
            $data['landing_pages_block1_title3'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_button3'])) {
            $data['landing_pages_block1_button3'] = $this->request->post['landing_pages_block1_button3'];
        } elseif (isset($country_language_info['landing_pages_block1_button3'])) {
            $data['landing_pages_block1_button3'] = $country_language_info['landing_pages_block1_button3'];
        } else {
            $data['landing_pages_block1_button3'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_description3'])) {
            $data['landing_pages_block1_description3'] = $this->request->post['landing_pages_block1_description3'];
        } elseif (isset($country_language_info['landing_pages_block1_description3'])) {
            $data['landing_pages_block1_description3'] = $country_language_info['landing_pages_block1_description3'];
        } else {
            $data['landing_pages_block1_description3'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_url3'])) {
            $data['landing_pages_block1_url3'] = $this->request->post['landing_pages_block1_url3'];
        } elseif (isset($country_language_info['landing_pages_block1_url3'])) {
            $data['landing_pages_block1_url3'] = $country_language_info['landing_pages_block1_url3'];
        } else {
            $data['landing_pages_block1_url3'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_image3'])) {
            $data['landing_pages_block1_image3'] = $this->request->post['landing_pages_block1_image3'];
        } elseif (isset($country_language_info['landing_pages_block1_image3'])) {
            $data['landing_pages_block1_image3'] = $country_language_info['landing_pages_block1_image3'];
        } else {
            $data['landing_pages_block1_image3'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_image3']) && is_file(DIR_IMAGE . $this->request->post['landing_pages_block1_image3'])) {
            $data['thumb_13'] = $this->model_tool_image->resize($this->request->post['landing_pages_block1_image3'], 100, 100, false);
        } elseif (isset($country_language_info['landing_pages_block1_image3']) && is_file(DIR_IMAGE . $country_language_info['landing_pages_block1_image3'])) {
            $data['thumb_13'] = $this->model_tool_image->resize($country_language_info['landing_pages_block1_image3'], 100, 100, false);
        } else {
            $data['thumb_13'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        if (isset($this->request->post['landing_pages_block1_title4'])) {
            $data['landing_pages_block1_title4'] = $this->request->post['landing_pages_block1_title4'];
        } elseif (isset($country_language_info['landing_pages_block1_title4'])) {
            $data['landing_pages_block1_title4'] = $country_language_info['landing_pages_block1_title4'];
        } else {
            $data['landing_pages_block1_title4'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_button4'])) {
            $data['landing_pages_block1_button4'] = $this->request->post['landing_pages_block1_button4'];
        } elseif (isset($country_language_info['landing_pages_block1_button4'])) {
            $data['landing_pages_block1_button4'] = $country_language_info['landing_pages_block1_button4'];
        } else {
            $data['landing_pages_block1_button4'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_description4'])) {
            $data['landing_pages_block1_description4'] = $this->request->post['landing_pages_block1_description4'];
        } elseif (isset($country_language_info['landing_pages_block1_description4'])) {
            $data['landing_pages_block1_description4'] = $country_language_info['landing_pages_block1_description4'];
        } else {
            $data['landing_pages_block1_description4'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_url4'])) {
            $data['landing_pages_block1_url4'] = $this->request->post['landing_pages_block1_url4'];
        } elseif (isset($country_language_info['landing_pages_block1_url4'])) {
            $data['landing_pages_block1_url4'] = $country_language_info['landing_pages_block1_url4'];
        } else {
            $data['landing_pages_block1_url4'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_image4'])) {
            $data['landing_pages_block1_image4'] = $this->request->post['landing_pages_block1_image4'];
        } elseif (isset($country_language_info['landing_pages_block1_image4'])) {
            $data['landing_pages_block1_image4'] = $country_language_info['landing_pages_block1_image4'];
        } else {
            $data['landing_pages_block1_image4'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_image4']) && is_file(DIR_IMAGE . $this->request->post['landing_pages_block1_image4'])) {
            $data['thumb_14'] = $this->model_tool_image->resize($this->request->post['landing_pages_block1_image4'], 100, 100, false);
        } elseif (isset($country_language_info['landing_pages_block1_image4']) && is_file(DIR_IMAGE . $country_language_info['landing_pages_block1_image4'])) {
            $data['thumb_14'] = $this->model_tool_image->resize($country_language_info['landing_pages_block1_image4'], 100, 100, false);
        } else {
            $data['thumb_14'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        if (isset($this->request->post['landing_pages_block1_title5'])) {
            $data['landing_pages_block1_title5'] = $this->request->post['landing_pages_block1_title5'];
        } elseif (isset($country_language_info['landing_pages_block1_title5'])) {
            $data['landing_pages_block1_title5'] = $country_language_info['landing_pages_block1_title5'];
        } else {
            $data['landing_pages_block1_title5'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_button5'])) {
            $data['landing_pages_block1_button5'] = $this->request->post['landing_pages_block1_button5'];
        } elseif (isset($country_language_info['landing_pages_block1_button5'])) {
            $data['landing_pages_block1_button5'] = $country_language_info['landing_pages_block1_button5'];
        } else {
            $data['landing_pages_block1_button5'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_description5'])) {
            $data['landing_pages_block1_description5'] = $this->request->post['landing_pages_block1_description5'];
        } elseif (isset($country_language_info['landing_pages_block1_description5'])) {
            $data['landing_pages_block1_description5'] = $country_language_info['landing_pages_block1_description5'];
        } else {
            $data['landing_pages_block1_description5'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_url5'])) {
            $data['landing_pages_block1_url5'] = $this->request->post['landing_pages_block1_url5'];
        } elseif (isset($country_language_info['landing_pages_block1_url5'])) {
            $data['landing_pages_block1_url5'] = $country_language_info['landing_pages_block1_url5'];
        } else {
            $data['landing_pages_block1_url5'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_image5'])) {
            $data['landing_pages_block1_image5'] = $this->request->post['landing_pages_block1_image5'];
        } elseif (isset($country_language_info['landing_pages_block1_image5'])) {
            $data['landing_pages_block1_image5'] = $country_language_info['landing_pages_block1_image5'];
        } else {
            $data['landing_pages_block1_image5'] = '';
        }
        if (isset($this->request->post['landing_pages_block1_image5']) && is_file(DIR_IMAGE . $this->request->post['landing_pages_block1_image5'])) {
            $data['thumb_15'] = $this->model_tool_image->resize($this->request->post['landing_pages_block1_image5'], 100, 100, false);
        } elseif (isset($country_language_info['landing_pages_block1_image5']) && is_file(DIR_IMAGE . $country_language_info['landing_pages_block1_image5'])) {
            $data['thumb_15'] = $this->model_tool_image->resize($country_language_info['landing_pages_block1_image5'], 100, 100, false);
        } else {
            $data['thumb_15'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        $data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/landing_sub_pages_form.tpl', $data));
    }

}
