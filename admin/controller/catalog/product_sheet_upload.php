<?php

class ControllerCatalogProductSheetUpload extends Controller {

  private $error = array();

  public function index() {

      $this->load->language('catalog/product_sheet_upload');

      $this->getList();
  }

  public function add() {
    $this->load->language('catalog/product_sheet_upload');

    $this->document->setTitle($this->language->get('heading_title'));

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
        $this->load->model('catalog/product_sheet_upload');
        $this->model_catalog_product_sheet_upload->add($this->request->post, $this->request->files);
        $this->session->data['success'] = $this->language->get('text_success');

        $this->response->redirect($this->url->link('catalog/product_sheet_upload', 'token=' . $this->session->data['token'], 'SSL'));
    }

    $this->getForm();
  }

  public function edit() {
    $this->load->language('catalog/product_sheet_upload');

    $this->document->setTitle($this->language->get('heading_title'));

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
        $this->load->model('catalog/product_sheet_upload');
        $this->model_catalog_product_sheet_upload->edit($this->request->post, $this->request->files,$this->request->get['sheet_id']);
        $this->session->data['success'] = $this->language->get('text_success');

        $this->response->redirect($this->url->link('catalog/product_sheet_upload', 'token=' . $this->session->data['token'], 'SSL'));
    }

    $this->getForm();
  }

  public function getList() {

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('text_home'),
        'href' => $this->url->link('common/dashboard', '', 'SSL')
    );

    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('catalog/product_sheet_upload','', 'SSL')
    );

    $data['heading_title'] = $this->language->get('heading_title');
    $data['text_list'] = $this->language->get('text_list');
    $data['text_no_results'] = $this->language->get('text_no_results');
    $data['button_add'] = $this->language->get('button_add');
    $data['button_enable'] = $this->language->get('button_enable');
    $data['button_disable'] = $this->language->get('button_disable');

    if (isset($this->session->data['success'])) {
        $data['success'] = $this->session->data['success'];

        unset($this->session->data['success']);
    } else {
        $data['success'] = '';
    }

    if (isset($this->request->post['selected'])) {
        $data['selected'] = (array) $this->request->post['selected'];
    } else {
        $data['selected'] = array();
    }

    $this->load->model('catalog/product_landing');

    $data['add'] = $this->url->link('catalog/product_sheet_upload/add','', 'SSL');

    $this->load->model('catalog/product_sheet_upload');

    $sheets_added =$this->model_catalog_product_sheet_upload->getSheets();
    if(!empty($sheets_added)){
      foreach($sheets_added as $key => $value){
        $sheets_added[$key]['sheet_link'] = str_replace('admin/','',HTTP_SERVER)."image/" . $value['file_name'];
        $sheets_added[$key]['enable'] =$this->url->link('catalog/product_sheet_upload/enable_products' , 'sheet_id='.$value['sheet_id'] ,'SSL');
        $sheets_added[$key]['disable'] =$this->url->link('catalog/product_sheet_upload/disable_products' , 'sheet_id='.$value['sheet_id'] ,'SSL');
      }
    }

    $data['sheets_added'] = $sheets_added;
    $data['column_seller_name'] = $this->language->get('column_seller_name');
    $data['column_username'] = $this->language->get('column_username');
    $data['column_sheet_link'] = $this->language->get('column_sheet_link');
    $data['column_date_added'] = $this->language->get('column_date_added');
    $data['column_action'] = $this->language->get('column_action');
    $data['template_sheet'] = 'https://s3-eu-west-1.amazonaws.com/qasem12/template.xls';


    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('catalog/product_sheets_list.tpl', $data));

  }

  public function enable_products(){
    if(isset($this->request->get['sheet_id'])){
      $this->load->model('catalog/product_sheet_upload');
      $this->model_catalog_product_sheet_upload->enable($this->request->get['sheet_id']);
      $this->session->data['success'] = $this->language->get('text_success_enabled');

      $this->response->redirect($this->url->link('catalog/product_sheet_upload', 'token=' . $this->session->data['token'], 'SSL'));
    }
  }

  public function disable_products(){
    if(isset($this->request->get['sheet_id'])){
      $this->load->model('catalog/product_sheet_upload');
      $this->model_catalog_product_sheet_upload->disable($this->request->get['sheet_id']);
      $this->session->data['success'] = $this->language->get('text_success_disabled');

      $this->response->redirect($this->url->link('catalog/product_sheet_upload', 'token=' . $this->session->data['token'], 'SSL'));
    }
  }

  public function getForm(){
    $this->document->setTitle($this->language->get('heading_title'));

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('text_home'),
        'href' => $this->url->link('common/dashboard', '', 'SSL')
    );

    $data['breadcrumbs'][] = array(
        'text' => $this->language->get('heading_title'),
        'href' => $this->url->link('catalog/product_sheet_upload', '', 'SSL')
    );

    if (isset($this->session->data['success'])) {
        $data['success'] = $this->session->data['success'];

        unset($this->session->data['success']);
    } else {
        $data['success'] = '';
    }

    if (isset($this->error['warning'])) {
        $data['error_warning'] = $this->error['warning'];
    } else {
        $data['error_warning'] = '';
    }

    if (isset($this->error['error_seller_id'])) {
        $data['error_seller_id'] = $this->error['error_seller_id'];
    } else {
        $data['error_seller_id'] = '';
    }

    if (isset($this->error['error_products_currency'])) {
        $data['error_products_currency'] = $this->error['error_products_currency'];
    } else {
        $data['error_products_currency'] = '';
    }

    if (isset($this->error['error_xls_file'])) {
        $data['error_xls_file'] = $this->error['error_xls_file'];
    } else {
        $data['error_xls_file'] = '';
    }

    $this->load->model('catalog/product_sheet_upload');

    $sheet_info = array();
    if(isset($this->request->get['sheet_id']))
      $sheet_info = $this->model_catalog_product_sheet_upload->getSheet($this->request->get['sheet_id']);

    if (isset($this->request->post['seller_name'])) {
        $data['seller_name'] = $this->request->post['seller_name'];
    } elseif (isset($sheet_info['seller_name'])) {
        $data['seller_name'] = $sheet_info['seller_name'];
    } else {
        $data['seller_name'] = '';
    }

    if (isset($this->request->post['seller_id'])) {
        $data['seller_id'] = $this->request->post['seller_id'];
    } elseif (isset($sheet_info['seller_id'])) {
        $data['seller_id'] = $sheet_info['seller_id'];
    } else {
        $data['seller_id'] = '';
    }

    if (isset($this->request->post['products_currency'])) {
        $data['products_currency'] = $this->request->post['products_currency'];
    } elseif (isset($sheet_info['currency_id'])) {
        $data['products_currency'] = $sheet_info['currency_id'];
    } else {
        $data['products_currency'] = 5;
    }

    $this->load->model('localisation/currency');
    $data['currencies'] = $this->model_localisation_currency->getCurrencies();


    $data['heading_title'] = $this->language->get('heading_title');
    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');

    $data['cancel'] = $this->url->link('catalog/product_sheet_upload', '', 'SSL');
    $data['action'] = $this->url->link('catalog/product_sheet_upload/add', '', 'SSL');
    if(isset($this->request->get['sheet_id'])){
      $data['action'] = $this->url->link('catalog/product_sheet_upload/edit', 'sheet_id='.$this->request->get['sheet_id'], 'SSL');
    }

    $data['text_form'] = $this->language->get('text_form');
    $data['text_seller_name'] = $this->language->get('text_seller_name');
    $data['text_products_currency'] = $this->language->get('text_products_currency');
    $data['text_upload_file'] = $this->language->get('text_upload_file');

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('catalog/product_sheet_upload_form.tpl', $data));

  }

  public function validateForm(){
    if (!$this->request->post['seller_id']) {
      $this->error['error_seller_id'] = $this->language->get('error_field_reuired');
    }

    if (!$this->request->post['products_currency']) {
      $this->error['error_products_currency'] = $this->language->get('error_field_reuired');
    }

    if(!isset($this->request->get['sheet_id'])){
      if (!$this->request->files['input_xls_file'] || $this->request->files['input_xls_file']['size'] == 0) {
        $this->error['error_xls_file'] = $this->language->get('error_field_reuired');
      }
    }

    return !$this->error;

  }
}
