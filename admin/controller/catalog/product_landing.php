<?php

class ControllerCatalogProductLanding extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('catalog/product_landing');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('localisation/country');

        $this->getList();
    }

    public function add() {
        $this->load->language('catalog/product_landing');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->load->model('catalog/product_landing');
            $page_id = $this->model_catalog_product_landing->addSetting('product_landing', $this->request->post);
            $this->model_catalog_product_landing->addProduct($this->request->post, $page_id);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/product_landing', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm('add');
    }

    public function edit() {
        $this->load->language('catalog/product_landing');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->load->model('catalog/product_landing');
            $this->model_catalog_product_landing->editSetting('product_landing', $this->request->post, $this->request->get['page_id']);

            $this->model_catalog_product_landing->addProduct($this->request->post, $this->request->get['page_id']);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/product_landing', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm('edit');
    }

    public function delete() {
        $this->load->language('catalog/product_landing');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/product_landing');

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $page_id) {
                $this->model_catalog_product_landing->deleteSetting('product_landing', $page_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/product_landing', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {
        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/product_landing', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['countries'] = array();

        $available_coutnries = $this->model_localisation_country->getAvailableCountries();
        $country_names = array();
        foreach ($available_coutnries as $count) {
            $country_names[$count['country_id']] = $count['name'];
        }

        $Langauges = array(
            1 => 'English',
            2 => 'Arabic',
        );
        $gender = array(
            1 => 'Men',
            2 => 'Women',
            4 => 'Home'
        );
        $this->load->model('catalog/product_landing');
        $pages = $this->model_catalog_product_landing->getPages('product_landing');
        foreach ($pages as $page_id => $page) {
            $name = '';
            foreach ($page as $k => $value) {
                $name .= '( ' . $country_names[$value['country_id']] . ' ) ';
            }
            $data['countries'][] = array(
                'page_id' => $page_id,
                'name' => $name,
                'language' => $Langauges[$value['language_id']],
                'gender' => $gender[$value['gender']],
                'name' => $name,
                'edit' => $this->url->link('catalog/product_landing/edit', 'token=' . $this->session->data['token'] . '&page_id=' . $page_id, 'SSL')
            );
        }

        $data['add'] = $this->url->link('catalog/product_landing/add', 'token=' . $this->session->data['token'], 'SSL');
        $data['delete'] = $this->url->link('catalog/product_landing/delete', 'token=' . $this->session->data['token'], 'SSL');


        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_countries'] = $this->language->get('column_countries');
        $data['column_language'] = $this->language->get('column_language');
        $data['column_gender'] = $this->language->get('column_gender');
        $data['column_page_id'] = $this->language->get('column_page_id');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/product_landing_list.tpl', $data));
    }

    public function getForm($type = 'new') {

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_product_landing_block1_title1'] = $this->language->get('text_product_landing_block1_title1');
        $data['text_product_landing_block1_button1'] = $this->language->get('text_product_landing_block1_button1');
        $data['text_product_landing_block1_description1'] = $this->language->get('text_product_landing_block1_description1');
        $data['text_product_landing_block1_url1'] = $this->language->get('text_product_landing_block1_url1');
        $data['text_product_landing_block1_image1'] = $this->language->get('text_product_landing_block1_image1');

        $data['text_product_landing_block1_title2'] = $this->language->get('text_product_landing_block1_title2');
        $data['text_product_landing_block1_button2'] = $this->language->get('text_product_landing_block1_button2');
        $data['text_product_landing_block1_description2'] = $this->language->get('text_product_landing_block1_description2');
        $data['text_product_landing_block1_url2'] = $this->language->get('text_product_landing_block1_url2');
        $data['text_product_landing_block1_image2'] = $this->language->get('text_product_landing_block1_image2');

        $data['text_product_landing_block3_title1'] = $this->language->get('text_product_landing_block3_title1');
        $data['text_product_landing_block3_description1'] = $this->language->get('text_product_landing_block3_description1');
        $data['text_product_landing_block3_url1'] = $this->language->get('text_product_landing_block3_url1');
        $data['text_product_landing_block3_image1'] = $this->language->get('text_product_landing_block3_image1');

        $data['text_product_landing_block3_title2'] = $this->language->get('text_product_landing_block3_title2');
        $data['text_product_landing_block3_description2'] = $this->language->get('text_product_landing_block3_description2');
        $data['text_product_landing_block3_url2'] = $this->language->get('text_product_landing_block3_url2');
        $data['text_product_landing_block3_image2'] = $this->language->get('text_product_landing_block3_image2');

        $data['text_product_landing_block3_title3'] = $this->language->get('text_product_landing_block3_title3');
        $data['text_product_landing_block3_description3'] = $this->language->get('text_product_landing_block3_description3');
        $data['text_product_landing_block3_url3'] = $this->language->get('text_product_landing_block3_url3');
        $data['text_product_landing_block3_image3'] = $this->language->get('text_product_landing_block3_image3');

        $data['text_product_landing_block3_title4'] = $this->language->get('text_product_landing_block3_title4');
        $data['text_product_landing_block3_description4'] = $this->language->get('text_product_landing_block3_description4');
        $data['text_product_landing_block3_url4'] = $this->language->get('text_product_landing_block3_url4');
        $data['text_product_landing_block3_image4'] = $this->language->get('text_product_landing_block3_image4');


        $data['text_product_landing_block2_title1'] = $this->language->get('text_product_landing_block2_title1');
        $data['text_product_landing_block2_description1'] = $this->language->get('text_product_landing_block2_description1');
        $data['text_product_landing_block2_url1'] = $this->language->get('text_product_landing_block2_url1');
        $data['text_product_landing_block2_image1'] = $this->language->get('text_product_landing_block2_image1');

        $data['text_product_landing_block2_title2'] = $this->language->get('text_product_landing_block2_title2');
        $data['text_product_landing_block2_description2'] = $this->language->get('text_product_landing_block2_description2');
        $data['text_product_landing_block2_url2'] = $this->language->get('text_product_landing_block2_url2');
        $data['text_product_landing_block2_image2'] = $this->language->get('text_product_landing_block2_image2');

        $data['text_product_landing_block2_title3'] = $this->language->get('text_product_landing_block2_title3');
        $data['text_product_landing_block2_description3'] = $this->language->get('text_product_landing_block2_description3');
        $data['text_product_landing_block2_url3'] = $this->language->get('text_product_landing_block2_url3');
        $data['text_product_landing_block2_image3'] = $this->language->get('text_product_landing_block2_image3');

        $data['text_product_landing_block2_title4'] = $this->language->get('text_product_landing_block2_title4');
        $data['text_product_landing_block2_description4'] = $this->language->get('text_product_landing_block2_description4');
        $data['text_product_landing_block2_url4'] = $this->language->get('text_product_landing_block2_url4');
        $data['text_product_landing_block2_image4'] = $this->language->get('text_product_landing_block2_image4');

        $data['text_product_landing_additional_block_title'] = $this->language->get('text_product_landing_additional_block_title');
        $data['text_product_landing_additional_block_action'] = $this->language->get('text_product_landing_additional_block_action');
        $data['text_product_landing_additional_block_url'] = $this->language->get('text_product_landing_additional_block_url');
        $data['text_product_landing_additional_block_position'] = $this->language->get('text_product_landing_additional_block_position');
        $data['text_product_landing_additional_block_image'] = $this->language->get('text_product_landing_additional_block_image');

        $data['entry_products'] = $this->language->get('entry_products');



        $data['button_block_add'] = $this->language->get('button_block_add');
        $data['text_none'] = $this->language->get('text_none');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['tab_block1'] = $this->language->get('tab_block1');
        $data['tab_block2'] = $this->language->get('tab_block2');
        $data['tab_block3'] = $this->language->get('tab_block3');
        $data['tab_block4'] = $this->language->get('tab_block4');
        $data['tab_block6'] = $this->language->get('tab_block6');
        $data['tab_block'] = $this->language->get('tab_block');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['product_landing_block1_title1'])) {
            $data['error_product_landing_block1_title1'] = $this->error['product_landing_block1_title1'];
        } else {
            $data['error_product_landing_block1_title1'] = '';
        }

        if (isset($this->error['product_landing_block1_button1'])) {
            $data['error_product_landing_block1_button1'] = $this->error['product_landing_block1_button1'];
        } else {
            $data['error_product_landing_block1_button1'] = '';
        }

        if (isset($this->error['product_landing_block1_description1'])) {
            $data['error_product_landing_block1_description1'] = $this->error['product_landing_block1_description1'];
        } else {
            $data['error_product_landing_block1_description1'] = '';
        }

        if (isset($this->error['product_landing_block1_url1'])) {
            $data['error_product_landing_block1_url1'] = $this->error['product_landing_block1_url1'];
        } else {
            $data['error_product_landing_block1_url1'] = '';
        }

        if (isset($this->error['product_landing_block1_image1'])) {
            $data['error_product_landing_block1_image1'] = $this->error['product_landing_block1_image1'];
        } else {
            $data['error_product_landing_block1_image1'] = '';
        }

        if (isset($this->error['product_landing_block1_title2'])) {
            $data['error_product_landing_block1_title2'] = $this->error['product_landing_block1_title2'];
        } else {
            $data['error_product_landing_block1_title2'] = '';
        }

        if (isset($this->error['product_landing_block1_button2'])) {
            $data['error_product_landing_block1_button2'] = $this->error['product_landing_block1_button2'];
        } else {
            $data['error_product_landing_block1_button2'] = '';
        }

        if (isset($this->error['product_landing_block1_description2'])) {
            $data['error_product_landing_block1_description2'] = $this->error['product_landing_block1_description2'];
        } else {
            $data['error_product_landing_block1_description2'] = '';
        }

        if (isset($this->error['product_landing_block1_url2'])) {
            $data['error_product_landing_block1_url2'] = $this->error['product_landing_block1_url2'];
        } else {
            $data['error_product_landing_block1_url2'] = '';
        }

        if (isset($this->error['product_landing_block1_image2'])) {
            $data['error_product_landing_block1_image2'] = $this->error['product_landing_block1_image2'];
        } else {
            $data['error_product_landing_block1_image2'] = '';
        }

        // tab3 block 1
        if (isset($this->error['product_landing_block2_title1'])) {
            $data['error_product_landing_block2_title1'] = $this->error['product_landing_block2_title1'];
        } else {
            $data['error_product_landing_block2_title1'] = '';
        }

        if (isset($this->error['product_landing_block2_description1'])) {
            $data['error_product_landing_block2_description1'] = $this->error['product_landing_block2_description1'];
        } else {
            $data['error_product_landing_block2_description1'] = '';
        }

        if (isset($this->error['product_landing_block2_url1'])) {
            $data['error_product_landing_block2_url1'] = $this->error['product_landing_block2_url1'];
        } else {
            $data['error_product_landing_block2_url1'] = '';
        }

        if (isset($this->error['product_landing_block2_image1'])) {
            $data['error_product_landing_block2_image1'] = $this->error['product_landing_block2_image1'];
        } else {
            $data['error_product_landing_block2_image1'] = '';
        }

        // tab3 block 2
        if (isset($this->error['product_landing_block2_title2'])) {
            $data['error_product_landing_block2_title2'] = $this->error['product_landing_block2_title2'];
        } else {
            $data['error_product_landing_block2_title2'] = '';
        }

        if (isset($this->error['product_landing_block2_description2'])) {
            $data['error_product_landing_block2_description2'] = $this->error['product_landing_block2_description2'];
        } else {
            $data['error_product_landing_block2_description2'] = '';
        }

        if (isset($this->error['product_landing_block2_url2'])) {
            $data['error_product_landing_block2_url2'] = $this->error['product_landing_block2_url2'];
        } else {
            $data['error_product_landing_block2_url2'] = '';
        }

        if (isset($this->error['product_landing_block2_image2'])) {
            $data['error_product_landing_block2_image2'] = $this->error['product_landing_block2_image2'];
        } else {
            $data['error_product_landing_block2_image2'] = '';
        }

        // tab3 block 3
        if (isset($this->error['product_landing_block2_title3'])) {
            $data['error_product_landing_block2_title3'] = $this->error['product_landing_block2_title3'];
        } else {
            $data['error_product_landing_block2_title3'] = '';
        }

        if (isset($this->error['product_landing_block2_description3'])) {
            $data['error_product_landing_block2_description3'] = $this->error['product_landing_block2_description3'];
        } else {
            $data['error_product_landing_block2_description3'] = '';
        }

        if (isset($this->error['product_landing_block2_url3'])) {
            $data['error_product_landing_block2_url3'] = $this->error['product_landing_block2_url3'];
        } else {
            $data['error_product_landing_block2_url3'] = '';
        }

        if (isset($this->error['product_landing_block2_image3'])) {
            $data['error_product_landing_block2_image3'] = $this->error['product_landing_block2_image3'];
        } else {
            $data['error_product_landing_block2_image3'] = '';
        }

        // tab3 block 4
        if (isset($this->error['product_landing_block2_title4'])) {
            $data['error_product_landing_block2_title4'] = $this->error['product_landing_block2_title4'];
        } else {
            $data['error_product_landing_block2_title4'] = '';
        }

        if (isset($this->error['product_landing_block2_description4'])) {
            $data['error_product_landing_block2_description4'] = $this->error['product_landing_block2_description4'];
        } else {
            $data['error_product_landing_block2_description4'] = '';
        }

        if (isset($this->error['product_landing_block2_url4'])) {
            $data['error_product_landing_block2_url4'] = $this->error['product_landing_block2_url4'];
        } else {
            $data['error_product_landing_block2_url4'] = '';
        }

        if (isset($this->error['product_landing_block2_image4'])) {
            $data['error_product_landing_block2_image4'] = $this->error['product_landing_block2_image4'];
        } else {
            $data['error_product_landing_block2_image4'] = '';
        }

        // tab3 block 1
        if (isset($this->error['product_landing_block3_title1'])) {
            $data['error_product_landing_block3_title1'] = $this->error['product_landing_block3_title1'];
        } else {
            $data['error_product_landing_block3_title1'] = '';
        }

        if (isset($this->error['product_landing_block3_description1'])) {
            $data['error_product_landing_block3_description1'] = $this->error['product_landing_block3_description1'];
        } else {
            $data['error_product_landing_block3_description1'] = '';
        }

        if (isset($this->error['product_landing_block3_url1'])) {
            $data['error_product_landing_block3_url1'] = $this->error['product_landing_block3_url1'];
        } else {
            $data['error_product_landing_block3_url1'] = '';
        }

        if (isset($this->error['product_landing_block3_image1'])) {
            $data['error_product_landing_block3_image1'] = $this->error['product_landing_block3_image1'];
        } else {
            $data['error_product_landing_block3_image1'] = '';
        }

        // tab3 block 2
        if (isset($this->error['product_landing_block3_title2'])) {
            $data['error_product_landing_block3_title2'] = $this->error['product_landing_block3_title2'];
        } else {
            $data['error_product_landing_block3_title2'] = '';
        }

        if (isset($this->error['product_landing_block3_description2'])) {
            $data['error_product_landing_block3_description2'] = $this->error['product_landing_block3_description2'];
        } else {
            $data['error_product_landing_block3_description2'] = '';
        }

        if (isset($this->error['product_landing_block3_url2'])) {
            $data['error_product_landing_block3_url2'] = $this->error['product_landing_block3_url2'];
        } else {
            $data['error_product_landing_block3_url2'] = '';
        }

        if (isset($this->error['product_landing_block3_image2'])) {
            $data['error_product_landing_block3_image2'] = $this->error['product_landing_block3_image2'];
        } else {
            $data['error_product_landing_block3_image2'] = '';
        }

        // tab3 block 3
        if (isset($this->error['product_landing_block3_title3'])) {
            $data['error_product_landing_block3_title3'] = $this->error['product_landing_block3_title3'];
        } else {
            $data['error_product_landing_block3_title3'] = '';
        }

        if (isset($this->error['product_landing_block3_description3'])) {
            $data['error_product_landing_block3_description3'] = $this->error['product_landing_block3_description3'];
        } else {
            $data['error_product_landing_block3_description3'] = '';
        }

        if (isset($this->error['product_landing_block3_url3'])) {
            $data['error_product_landing_block3_url3'] = $this->error['product_landing_block3_url3'];
        } else {
            $data['error_product_landing_block3_url3'] = '';
        }

        if (isset($this->error['product_landing_block3_image3'])) {
            $data['error_product_landing_block3_image3'] = $this->error['product_landing_block3_image3'];
        } else {
            $data['error_product_landing_block3_image3'] = '';
        }

        // tab3 block 4
        if (isset($this->error['product_landing_block3_title4'])) {
            $data['error_product_landing_block3_title4'] = $this->error['product_landing_block3_title4'];
        } else {
            $data['error_product_landing_block3_title4'] = '';
        }

        if (isset($this->error['product_landing_block3_description4'])) {
            $data['error_product_landing_block3_description4'] = $this->error['product_landing_block3_description4'];
        } else {
            $data['error_product_landing_block3_description4'] = '';
        }

        if (isset($this->error['product_landing_block3_url4'])) {
            $data['error_product_landing_block3_url4'] = $this->error['product_landing_block3_url4'];
        } else {
            $data['error_product_landing_block3_url4'] = '';
        }

        if (isset($this->error['product_landing_block3_image4'])) {
            $data['error_product_landing_block3_image4'] = $this->error['product_landing_block3_image4'];
        } else {
            $data['error_product_landing_block3_image4'] = '';
        }

        $this->load->model('tool/image');
        $this->load->model('localisation/country');
        $this->load->model('localisation/language');
        $this->load->model('catalog/product_landing');

        $data['text_form'] = $this->language->get('text_edit');
        $data['selectedproducts'] = $this->url->link('cms/productlandingFeaturedProducts', 'token=' . $this->session->data['token'], 'SSL');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/product_landing', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_settings'),
            'href' => $this->url->link('catalog/product_landing/edit', 'token=' . $this->session->data['token'], 'SSL')
        );


        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $selected_products = array();
        $url_page = '';
        if (isset($this->request->get['page_id'])) {
            $page_id = $this->request->get['page_id'];
            $url_page = '&page_id=' . $page_id;
            $selected_products_array = $this->model_catalog_product_landing->getSelectedProducts($page_id, 'product_landing');
            foreach ($selected_products_array as $key => $prod) {
                $selected_products[] = array(
                    'product_id' => $prod['product_id'],
                    'product_name' => $prod['name'],
                );
            }
        }

        $page_countries = array();
        if (isset($this->request->get['page_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {

            $country_language_info = $this->model_catalog_product_landing->getSetting('product_landing', $page_id);
            $page_countries = $this->model_catalog_product_landing->getPageCountries('product_landing', $page_id);
        }
        $data['page_language'] = 1;
        if (!empty($page_countries)) {
            $data['page_language'] = $page_countries[array_keys($page_countries)[0]]['language_id'];
        }

        $data['page_gender'] = 1;
        if (!empty($page_countries)) {
            if ($page_countries[array_keys($page_countries)[0]]['gender'] > 0) {
                $data['page_gender'] = $page_countries[array_keys($page_countries)[0]]['gender'];
            }
        }


        $data['page_countries'] = $page_countries;

        $data['available_coutnries'] = $this->model_localisation_country->getAvailableCountries();




        $data['action'] = $this->url->link('catalog/product_landing/' . $type, 'token=' . $this->session->data['token'] . $url_page, 'SSL');

        $data['cancel'] = $this->url->link('catalog/product_landing', 'token=' . $this->session->data['token'], 'SSL');



        $data['selected_products'] = $selected_products;


        $additional_products = array();
        if (isset($country_language_info) && !empty($country_language_info)) {
            foreach ($country_language_info as $key => $value) {
                if (preg_match('/product_landing_additional_/', $key)) {
                    $additional_bloacks[$key] = $value;
                }
            }
            if (!empty($additional_bloacks)) {
                $additional_products_array = $this->model_catalog_product_landing->getProducts($this->request->get['page_id']);
                foreach ($additional_products_array as $key => $prod) {
                    if (isset($additional_bloacks['product_landing_additional_block' . $prod['block_id'] . '_title' . $prod['block_id']])) {
                        $additional_products[$prod['block_id']]['title'] = $additional_bloacks['product_landing_additional_block' . $prod['block_id'] . '_title' . $prod['block_id']];
                        $additional_products[$prod['block_id']]['action'] = $additional_bloacks['product_landing_additional_block' . $prod['block_id'] . '_action' . $prod['block_id']];
                        $additional_products[$prod['block_id']]['position'] = $this->db->query("SELECT position from product_land WHERE page_id=".$this->request->get['page_id']." AND `key` = 'product_landing_additional_block". $prod['block_id'] . '_action' . $prod['block_id']."' AND deleted = '0' ")->row['position'];
                    }
                    $additional_products[$prod['block_id']]['products'][] = array(
                        'product_id' => $prod['product_id'],
                        'product_name' => $prod['name'],
                    );
                }
            }
        }
        $data['additional_products'] = $additional_products;
        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['product_landing_block1_title1'])) {
            $data['product_landing_block1_title1'] = $this->request->post['product_landing_block1_title1'];
        } elseif (isset($country_language_info['product_landing_block1_title1'])) {
            $data['product_landing_block1_title1'] = $country_language_info['product_landing_block1_title1'];
        } else {
            $data['product_landing_block1_title1'] = '';
        }
        if (isset($this->request->post['product_landing_block1_button1'])) {
            $data['product_landing_block1_button1'] = $this->request->post['product_landing_block1_button1'];
        } elseif (isset($country_language_info['product_landing_block1_button1'])) {
            $data['product_landing_block1_button1'] = $country_language_info['product_landing_block1_button1'];
        } else {
            $data['product_landing_block1_button1'] = '';
        }
        if (isset($this->request->post['product_landing_block1_description1'])) {
            $data['product_landing_block1_description1'] = $this->request->post['product_landing_block1_description1'];
        } elseif (isset($country_language_info['product_landing_block1_description1'])) {
            $data['product_landing_block1_description1'] = $country_language_info['product_landing_block1_description1'];
        } else {
            $data['product_landing_block1_description1'] = '';
        }
        if (isset($this->request->post['product_landing_block1_url1'])) {
            $data['product_landing_block1_url1'] = $this->request->post['product_landing_block1_url1'];
        } elseif (isset($country_language_info['product_landing_block1_url1'])) {
            $data['product_landing_block1_url1'] = $country_language_info['product_landing_block1_url1'];
        } else {
            $data['product_landing_block1_url1'] = '';
        }
        if (isset($this->request->post['product_landing_block1_image1'])) {
            $data['product_landing_block1_image1'] = $this->request->post['product_landing_block1_image1'];
        } elseif (isset($country_language_info['product_landing_block1_image1'])) {
            $data['product_landing_block1_image1'] = $country_language_info['product_landing_block1_image1'];
        } else {
            $data['product_landing_block1_image1'] = '';
        }
        if (isset($this->request->post['product_landing_block1_image1']) && is_file(DIR_IMAGE . $this->request->post['product_landing_block1_image1'])) {
            $data['thumb_11'] = $this->model_tool_image->resize($this->request->post['product_landing_block1_image1'], 100, 100, false);
        } elseif (isset($country_language_info['product_landing_block1_image1']) && is_file(DIR_IMAGE . $country_language_info['product_landing_block1_image1'])) {
            $data['thumb_11'] = $this->model_tool_image->resize($country_language_info['product_landing_block1_image1'], 100, 100, false);
        } else {
            $data['thumb_11'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        if (isset($this->request->post['product_landing_block1_title2'])) {
            $data['product_landing_block1_title2'] = $this->request->post['product_landing_block1_title2'];
        } elseif (isset($country_language_info['product_landing_block1_title2'])) {
            $data['product_landing_block1_title2'] = $country_language_info['product_landing_block1_title2'];
        } else {
            $data['product_landing_block1_title2'] = '';
        }
        if (isset($this->request->post['product_landing_block1_button2'])) {
            $data['product_landing_block1_button2'] = $this->request->post['product_landing_block1_button2'];
        } elseif (isset($country_language_info['product_landing_block1_button2'])) {
            $data['product_landing_block1_button2'] = $country_language_info['product_landing_block1_button2'];
        } else {
            $data['product_landing_block1_button2'] = '';
        }
        if (isset($this->request->post['product_landing_block1_description2'])) {
            $data['product_landing_block1_description2'] = $this->request->post['product_landing_block1_description2'];
        } elseif (isset($country_language_info['product_landing_block1_description2'])) {
            $data['product_landing_block1_description2'] = $country_language_info['product_landing_block1_description2'];
        } else {
            $data['product_landing_block1_description2'] = '';
        }
        if (isset($this->request->post['product_landing_block1_url2'])) {
            $data['product_landing_block1_url2'] = $this->request->post['product_landing_block1_url2'];
        } elseif (isset($country_language_info['product_landing_block1_url2'])) {
            $data['product_landing_block1_url2'] = $country_language_info['product_landing_block1_url2'];
        } else {
            $data['product_landing_block1_url2'] = '';
        }
        if (isset($this->request->post['product_landing_block1_image2'])) {
            $data['product_landing_block1_image2'] = $this->request->post['product_landing_block1_image2'];
        } elseif (isset($country_language_info['product_landing_block1_image2'])) {
            $data['product_landing_block1_image2'] = $country_language_info['product_landing_block1_image2'];
        } else {
            $data['product_landing_block1_image2'] = '';
        }
        if (isset($this->request->post['product_landing_block1_image2']) && is_file(DIR_IMAGE . $this->request->post['product_landing_block1_image2'])) {
            $data['thumb_12'] = $this->model_tool_image->resize($this->request->post['product_landing_block1_image2'], 100, 100, false);
        } elseif (isset($country_language_info['product_landing_block1_image2']) && is_file(DIR_IMAGE . $country_language_info['product_landing_block1_image2'])) {
            $data['thumb_12'] = $this->model_tool_image->resize($country_language_info['product_landing_block1_image2'], 100, 100, false);
        } else {
            $data['thumb_12'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        if (isset($this->request->post['product_landing_block1_title3'])) {
            $data['product_landing_block1_title3'] = $this->request->post['product_landing_block1_title3'];
        } elseif (isset($country_language_info['product_landing_block1_title3'])) {
            $data['product_landing_block1_title3'] = $country_language_info['product_landing_block1_title3'];
        } else {
            $data['product_landing_block1_title3'] = '';
        }
        if (isset($this->request->post['product_landing_block1_button3'])) {
            $data['product_landing_block1_button3'] = $this->request->post['product_landing_block1_button3'];
        } elseif (isset($country_language_info['product_landing_block1_button3'])) {
            $data['product_landing_block1_button3'] = $country_language_info['product_landing_block1_button3'];
        } else {
            $data['product_landing_block1_button3'] = '';
        }
        if (isset($this->request->post['product_landing_block1_description3'])) {
            $data['product_landing_block1_description3'] = $this->request->post['product_landing_block1_description3'];
        } elseif (isset($country_language_info['product_landing_block1_description3'])) {
            $data['product_landing_block1_description3'] = $country_language_info['product_landing_block1_description3'];
        } else {
            $data['product_landing_block1_description3'] = '';
        }
        if (isset($this->request->post['product_landing_block1_url3'])) {
            $data['product_landing_block1_url3'] = $this->request->post['product_landing_block1_url3'];
        } elseif (isset($country_language_info['product_landing_block1_url3'])) {
            $data['product_landing_block1_url3'] = $country_language_info['product_landing_block1_url3'];
        } else {
            $data['product_landing_block1_url3'] = '';
        }
        if (isset($this->request->post['product_landing_block1_image3'])) {
            $data['product_landing_block1_image3'] = $this->request->post['product_landing_block1_image3'];
        } elseif (isset($country_language_info['product_landing_block1_image3'])) {
            $data['product_landing_block1_image3'] = $country_language_info['product_landing_block1_image3'];
        } else {
            $data['product_landing_block1_image3'] = '';
        }
        if (isset($this->request->post['product_landing_block1_image3']) && is_file(DIR_IMAGE . $this->request->post['product_landing_block1_image3'])) {
            $data['thumb_13'] = $this->model_tool_image->resize($this->request->post['product_landing_block1_image3'], 100, 100, false);
        } elseif (isset($country_language_info['product_landing_block1_image3']) && is_file(DIR_IMAGE . $country_language_info['product_landing_block1_image3'])) {
            $data['thumb_13'] = $this->model_tool_image->resize($country_language_info['product_landing_block1_image3'], 100, 100, false);
        } else {
            $data['thumb_13'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        if (isset($this->request->post['product_landing_block1_title4'])) {
            $data['product_landing_block1_title4'] = $this->request->post['product_landing_block1_title4'];
        } elseif (isset($country_language_info['product_landing_block1_title4'])) {
            $data['product_landing_block1_title4'] = $country_language_info['product_landing_block1_title4'];
        } else {
            $data['product_landing_block1_title4'] = '';
        }
        if (isset($this->request->post['product_landing_block1_button4'])) {
            $data['product_landing_block1_button4'] = $this->request->post['product_landing_block1_button4'];
        } elseif (isset($country_language_info['product_landing_block1_button4'])) {
            $data['product_landing_block1_button4'] = $country_language_info['product_landing_block1_button4'];
        } else {
            $data['product_landing_block1_button4'] = '';
        }
        if (isset($this->request->post['product_landing_block1_description4'])) {
            $data['product_landing_block1_description4'] = $this->request->post['product_landing_block1_description4'];
        } elseif (isset($country_language_info['product_landing_block1_description4'])) {
            $data['product_landing_block1_description4'] = $country_language_info['product_landing_block1_description4'];
        } else {
            $data['product_landing_block1_description4'] = '';
        }
        if (isset($this->request->post['product_landing_block1_url4'])) {
            $data['product_landing_block1_url4'] = $this->request->post['product_landing_block1_url4'];
        } elseif (isset($country_language_info['product_landing_block1_url4'])) {
            $data['product_landing_block1_url4'] = $country_language_info['product_landing_block1_url4'];
        } else {
            $data['product_landing_block1_url4'] = '';
        }
        if (isset($this->request->post['product_landing_block1_image4'])) {
            $data['product_landing_block1_image4'] = $this->request->post['product_landing_block1_image4'];
        } elseif (isset($country_language_info['product_landing_block1_image4'])) {
            $data['product_landing_block1_image4'] = $country_language_info['product_landing_block1_image4'];
        } else {
            $data['product_landing_block1_image4'] = '';
        }
        if (isset($this->request->post['product_landing_block1_image4']) && is_file(DIR_IMAGE . $this->request->post['product_landing_block1_image4'])) {
            $data['thumb_14'] = $this->model_tool_image->resize($this->request->post['product_landing_block1_image4'], 100, 100, false);
        } elseif (isset($country_language_info['product_landing_block1_image4']) && is_file(DIR_IMAGE . $country_language_info['product_landing_block1_image4'])) {
            $data['thumb_14'] = $this->model_tool_image->resize($country_language_info['product_landing_block1_image4'], 100, 100, false);
        } else {
            $data['thumb_14'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        if (isset($this->request->post['product_landing_block1_title5'])) {
            $data['product_landing_block1_title5'] = $this->request->post['product_landing_block1_title5'];
        } elseif (isset($country_language_info['product_landing_block1_title5'])) {
            $data['product_landing_block1_title5'] = $country_language_info['product_landing_block1_title5'];
        } else {
            $data['product_landing_block1_title5'] = '';
        }
        if (isset($this->request->post['product_landing_block1_button5'])) {
            $data['product_landing_block1_button5'] = $this->request->post['product_landing_block1_button5'];
        } elseif (isset($country_language_info['product_landing_block1_button5'])) {
            $data['product_landing_block1_button5'] = $country_language_info['product_landing_block1_button5'];
        } else {
            $data['product_landing_block1_button5'] = '';
        }
        if (isset($this->request->post['product_landing_block1_description5'])) {
            $data['product_landing_block1_description5'] = $this->request->post['product_landing_block1_description5'];
        } elseif (isset($country_language_info['product_landing_block1_description5'])) {
            $data['product_landing_block1_description5'] = $country_language_info['product_landing_block1_description5'];
        } else {
            $data['product_landing_block1_description5'] = '';
        }
        if (isset($this->request->post['product_landing_block1_url5'])) {
            $data['product_landing_block1_url5'] = $this->request->post['product_landing_block1_url5'];
        } elseif (isset($country_language_info['product_landing_block1_url5'])) {
            $data['product_landing_block1_url5'] = $country_language_info['product_landing_block1_url5'];
        } else {
            $data['product_landing_block1_url5'] = '';
        }
        if (isset($this->request->post['product_landing_block1_image5'])) {
            $data['product_landing_block1_image5'] = $this->request->post['product_landing_block1_image5'];
        } elseif (isset($country_language_info['product_landing_block1_image5'])) {
            $data['product_landing_block1_image5'] = $country_language_info['product_landing_block1_image5'];
        } else {
            $data['product_landing_block1_image5'] = '';
        }
        if (isset($this->request->post['product_landing_block1_image5']) && is_file(DIR_IMAGE . $this->request->post['product_landing_block1_image5'])) {
            $data['thumb_15'] = $this->model_tool_image->resize($this->request->post['product_landing_block1_image5'], 100, 100, false);
        } elseif (isset($country_language_info['product_landing_block1_image5']) && is_file(DIR_IMAGE . $country_language_info['product_landing_block1_image5'])) {
            $data['thumb_15'] = $this->model_tool_image->resize($country_language_info['product_landing_block1_image5'], 100, 100, false);
        } else {
            $data['thumb_15'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        //tab3 // block 1
        if (isset($this->request->post['product_landing_block3_title1'])) {
            $data['product_landing_block3_title1'] = $this->request->post['product_landing_block3_title1'];
        } elseif (isset($country_language_info['product_landing_block3_title1'])) {
            $data['product_landing_block3_title1'] = $country_language_info['product_landing_block3_title1'];
        } else {
            $data['product_landing_block3_title1'] = '';
        }
        if (isset($this->request->post['product_landing_block3_description1'])) {
            $data['product_landing_block3_description1'] = $this->request->post['product_landing_block3_description1'];
        } elseif (isset($country_language_info['product_landing_block3_description1'])) {
            $data['product_landing_block3_description1'] = $country_language_info['product_landing_block3_description1'];
        } else {
            $data['product_landing_block3_description1'] = '';
        }
        if (isset($this->request->post['product_landing_block3_url1'])) {
            $data['product_landing_block3_url1'] = $this->request->post['product_landing_block3_url1'];
        } elseif (isset($country_language_info['product_landing_block1_url1'])) {
            $data['product_landing_block3_url1'] = $country_language_info['product_landing_block3_url1'];
        } else {
            $data['product_landing_block3_url1'] = '';
        }
        if (isset($this->request->post['product_landing_block3_image1'])) {
            $data['product_landing_block3_image1'] = $this->request->post['product_landing_block3_image1'];
        } elseif (isset($country_language_info['product_landing_block3_image1'])) {
            $data['product_landing_block3_image1'] = $country_language_info['product_landing_block3_image1'];
        } else {
            $data['product_landing_block3_image1'] = '';
        }
        if (isset($this->request->post['product_landing_block3_image1']) && is_file(DIR_IMAGE . $this->request->post['product_landing_block3_image1'])) {
            $data['thumb_31'] = $this->model_tool_image->resize($this->request->post['product_landing_block3_image1'], 100, 100, false);
        } elseif (isset($country_language_info['product_landing_block3_image1']) && is_file(DIR_IMAGE . $country_language_info['product_landing_block3_image1'])) {
            $data['thumb_31'] = $this->model_tool_image->resize($country_language_info['product_landing_block3_image1'], 100, 100, false);
        } else {
            $data['thumb_31'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        //tab3 // block 2
        if (isset($this->request->post['product_landing_block3_title2'])) {
            $data['product_landing_block3_title2'] = $this->request->post['product_landing_block3_title2'];
        } elseif (isset($country_language_info['product_landing_block3_title2'])) {
            $data['product_landing_block3_title2'] = $country_language_info['product_landing_block3_title2'];
        } else {
            $data['product_landing_block3_title2'] = '';
        }
        if (isset($this->request->post['product_landing_block3_description2'])) {
            $data['product_landing_block3_description2'] = $this->request->post['product_landing_block3_description2'];
        } elseif (isset($country_language_info['product_landing_block3_description2'])) {
            $data['product_landing_block3_description2'] = $country_language_info['product_landing_block3_description2'];
        } else {
            $data['product_landing_block3_description2'] = '';
        }
        if (isset($this->request->post['product_landing_block3_url2'])) {
            $data['product_landing_block3_url2'] = $this->request->post['product_landing_block3_url2'];
        } elseif (isset($country_language_info['product_landing_block1_url2'])) {
            $data['product_landing_block3_url2'] = $country_language_info['product_landing_block3_url2'];
        } else {
            $data['product_landing_block3_url2'] = '';
        }
        if (isset($this->request->post['product_landing_block3_image2'])) {
            $data['product_landing_block3_image2'] = $this->request->post['product_landing_block3_image2'];
        } elseif (isset($country_language_info['product_landing_block3_image2'])) {
            $data['product_landing_block3_image2'] = $country_language_info['product_landing_block3_image2'];
        } else {
            $data['product_landing_block3_image2'] = '';
        }
        if (isset($this->request->post['product_landing_block3_image2']) && is_file(DIR_IMAGE . $this->request->post['product_landing_block3_image2'])) {
            $data['thumb_32'] = $this->model_tool_image->resize($this->request->post['product_landing_block3_image2'], 100, 100, false);
        } elseif (isset($country_language_info['product_landing_block3_image2']) && is_file(DIR_IMAGE . $country_language_info['product_landing_block3_image2'])) {
            $data['thumb_32'] = $this->model_tool_image->resize($country_language_info['product_landing_block3_image2'], 100, 100, false);
        } else {
            $data['thumb_32'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        //tab3 // block 3
        if (isset($this->request->post['product_landing_block3_title3'])) {
            $data['product_landing_block3_title3'] = $this->request->post['product_landing_block3_title3'];
        } elseif (isset($country_language_info['product_landing_block3_title3'])) {
            $data['product_landing_block3_title3'] = $country_language_info['product_landing_block3_title3'];
        } else {
            $data['product_landing_block3_title3'] = '';
        }
        if (isset($this->request->post['product_landing_block3_description3'])) {
            $data['product_landing_block3_description3'] = $this->request->post['product_landing_block3_description3'];
        } elseif (isset($country_language_info['product_landing_block3_description3'])) {
            $data['product_landing_block3_description3'] = $country_language_info['product_landing_block3_description3'];
        } else {
            $data['product_landing_block3_description3'] = '';
        }
        if (isset($this->request->post['product_landing_block3_url3'])) {
            $data['product_landing_block3_url3'] = $this->request->post['product_landing_block3_url3'];
        } elseif (isset($country_language_info['product_landing_block3_url3'])) {
            $data['product_landing_block3_url3'] = $country_language_info['product_landing_block3_url3'];
        } else {
            $data['product_landing_block3_url3'] = '';
        }
        if (isset($this->request->post['product_landing_block3_image3'])) {
            $data['product_landing_block3_image3'] = $this->request->post['product_landing_block3_image3'];
        } elseif (isset($country_language_info['product_landing_block3_image3'])) {
            $data['product_landing_block3_image3'] = $country_language_info['product_landing_block3_image3'];
        } else {
            $data['product_landing_block3_image3'] = '';
        }
        if (isset($this->request->post['product_landing_block3_image3']) && is_file(DIR_IMAGE . $this->request->post['product_landing_block3_image3'])) {
            $data['thumb_33'] = $this->model_tool_image->resize($this->request->post['product_landing_block3_image3'], 100, 100, false);
        } elseif (isset($country_language_info['product_landing_block3_image3']) && is_file(DIR_IMAGE . $country_language_info['product_landing_block3_image3'])) {
            $data['thumb_33'] = $this->model_tool_image->resize($country_language_info['product_landing_block3_image3'], 100, 100, false);
        } else {
            $data['thumb_33'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        //tab3 // block 4
        if (isset($this->request->post['product_landing_block3_title4'])) {
            $data['product_landing_block3_title4'] = $this->request->post['product_landing_block3_title4'];
        } elseif (isset($country_language_info['product_landing_block3_title4'])) {
            $data['product_landing_block3_title4'] = $country_language_info['product_landing_block3_title4'];
        } else {
            $data['product_landing_block3_title4'] = '';
        }
        if (isset($this->request->post['product_landing_block3_description4'])) {
            $data['product_landing_block3_description4'] = $this->request->post['product_landing_block3_description4'];
        } elseif (isset($country_language_info['product_landing_block3_description4'])) {
            $data['product_landing_block3_description4'] = $country_language_info['product_landing_block3_description4'];
        } else {
            $data['product_landing_block3_description4'] = '';
        }
        if (isset($this->request->post['product_landing_block3_url4'])) {
            $data['product_landing_block3_url4'] = $this->request->post['product_landing_block3_url4'];
        } elseif (isset($country_language_info['product_landing_block3_url4'])) {
            $data['product_landing_block3_url4'] = $country_language_info['product_landing_block3_url4'];
        } else {
            $data['product_landing_block3_url4'] = '';
        }
        if (isset($this->request->post['product_landing_block3_image4'])) {
            $data['product_landing_block3_image4'] = $this->request->post['product_landing_block3_image4'];
        } elseif (isset($country_language_info['product_landing_block3_image4'])) {
            $data['product_landing_block3_image4'] = $country_language_info['product_landing_block3_image4'];
        } else {
            $data['product_landing_block3_image4'] = '';
        }
        if (isset($this->request->post['product_landing_block3_image4']) && is_file(DIR_IMAGE . $this->request->post['product_landing_block3_image4'])) {
            $data['thumb_34'] = $this->model_tool_image->resize($this->request->post['product_landing_block3_image4'], 100, 100, false);
        } elseif (isset($country_language_info['product_landing_block3_image4']) && is_file(DIR_IMAGE . $country_language_info['product_landing_block3_image4'])) {
            $data['thumb_34'] = $this->model_tool_image->resize($country_language_info['product_landing_block3_image4'], 100, 100, false);
        } else {
            $data['thumb_34'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        $data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/product_landing_form.tpl', $data));
    }

    protected function validateForm() {

        if (!$this->user->hasPermission('modify', 'catalog/product_landing')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /* if (!$this->request->post['product_landing_block1_title1']) {
          $this->error['product_landing_block1_title1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block1_title1'));
          }
          if (!$this->request->post['product_landing_block1_button1']) {
          $this->error['product_landing_block1_button1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block1_button1'));
          }
          if (!$this->request->post['product_landing_block1_description1']) {
          $this->error['product_landing_block1_description1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block1_description1'));
          }
          if (!$this->request->post['product_landing_block1_url1']) {
          $this->error['product_landing_block1_url1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block1_url1'));
          }
          if (!$this->request->post['product_landing_block1_image1'] && ($this->request->files['product_landing_block1_image1']['error'])) {
          $this->error['product_landing_block1_image1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block1_image1'));
          }


          if (!$this->request->post['product_landing_block1_title2']) {
          $this->error['product_landing_block1_title2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block1_title2'));
          }
          if (!$this->request->post['product_landing_block1_button2']) {
          $this->error['product_landing_block1_button2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block1_button2'));
          }
          if (!$this->request->post['product_landing_block1_description2']) {
          $this->error['product_landing_block1_description2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block1_description2'));
          }
          if (!$this->request->post['product_landing_block1_url2']) {
          $this->error['product_landing_block1_url2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block1_url2'));
          }
          if (!$this->request->post['product_landing_block1_image2'] && ($this->request->files['product_landing_block1_image2']['error'])) {
          $this->error['product_landing_block1_image2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block1_image2'));
          }

          if (!$this->request->post['product_landing_block3_title1']) {
          $this->error['product_landing_block3_title1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_title1'));
          }
          if (!$this->request->post['product_landing_block3_description1']) {
          $this->error['product_landing_block3_description1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_description1'));
          }
          if (!$this->request->post['product_landing_block3_url1']) {
          $this->error['product_landing_block3_url1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_url1'));
          }
          if (!$this->request->post['product_landing_block3_image1'] && ($this->request->files['product_landing_block3_image1']['error'])) {
          $this->error['product_landing_block3_image1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_image1'));
          }

          if (!$this->request->post['product_landing_block3_title2']) {
          $this->error['product_landing_block3_title2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_title2'));
          }
          if (!$this->request->post['product_landing_block3_description2']) {
          $this->error['product_landing_block3_description2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_description2'));
          }
          if (!$this->request->post['product_landing_block3_url2']) {
          $this->error['product_landing_block3_url2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_url2'));
          }
          if (!$this->request->post['product_landing_block3_image2'] && ($this->request->files['product_landing_block3_image2']['error'])) {
          $this->error['product_landing_block3_image2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block2_image1'));
          }

          if (!$this->request->post['product_landing_block3_title3']) {
          $this->error['product_landing_block3_title3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_title3'));
          }
          if (!$this->request->post['product_landing_block3_description3']) {
          $this->error['product_landing_block3_description3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_description3'));
          }
          if (!$this->request->post['product_landing_block3_url3']) {
          $this->error['product_landing_block3_url3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_url3'));
          }
          if (!$this->request->post['product_landing_block3_image3'] && ($this->request->files['product_landing_block3_image3']['error'])) {
          $this->error['product_landing_block3_image3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block2_image3'));
          }

          if (!$this->request->post['product_landing_block3_title4']) {
          $this->error['product_landing_block3_title4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_title4'));
          }
          if (!$this->request->post['product_landing_block3_description4']) {
          $this->error['product_landing_block3_description4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_description4'));
          }
          if (!$this->request->post['product_landing_block3_url4']) {
          $this->error['product_landing_block3_url4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block3_url4'));
          }
          if (!$this->request->post['product_landing_block3_image4'] && ($this->request->files['product_landing_block3_image4']['error'])) {
          $this->error['product_landing_block3_image4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_product_landing_block2_image4'));
          }
           */

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

}
