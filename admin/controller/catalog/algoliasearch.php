<?php

class ControllerCatalogAlgoliasearch extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('catalog/algoliasearch');

        $this->document->setTitle($this->language->get('heading_title2'));

        $this->load->model('catalog/algoliasearch');


        $this->getList();
    }

    public function edit() {

        $result = array();
        if (isset($this->request->post['product_id']) && isset($this->request->post['sort_order'])) {
            $this->load->model('catalog/algoliasearch');
            $this->model_catalog_algoliasearch->editProduct($this->request->post['product_id'], $this->request->post['sort_order']);

            $result['success'] = "Done";
        } else {
            $result['error'] = "Error Occured please try again...";
        }

        echo json_encode($result);
    }

    protected function getList() {

        $this->language->load('catalog/algoliasearch');
        if (isset($this->request->get['filter_product_id'])) {
            $filter_product_id = $this->request->get['filter_product_id'];
        } else {
            $filter_product_id = null;
        }

        if (isset($this->request->get['filter_country'])) {
            $filter_country = $this->request->get['filter_country'];
        } else {
            $filter_country = null;
        }
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_product_id'])) {
            $url .= '&filter_product_id=' . $this->request->get['filter_product_id'];
        }

        if (isset($this->request->get['filter_country'])) {
            $url .= '&filter_country=' . $this->request->get['filter_country'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title2'),
            'href' => $this->url->link('catalog/algoliasearch', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );


        $data['products'] = array();

        $filter_data = array(
            'filter_product_id' => $filter_product_id,
            'filter_country' => $filter_country,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $this->load->model('tool/image');

        $product_total = $this->model_catalog_algoliasearch->getTotalProducts($filter_data);

        $results = $this->model_catalog_algoliasearch->getProducts($filter_data);

        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $image = $this->model_tool_image->resize($result['image'], $this->config_image->get('product', 'config_image_cart', 'width'), $this->config_image->get('product', 'config_image_cart', 'hieght'), false);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('product', 'config_image_cart', 'width'), $this->config_image->get('product', 'config_image_cart', 'hieght'), false);
            }

            $data['products'][] = array(
                'product_id' => $result['product_id'],
                'image' => $image,
                'name' => $result['name'],
                'bname' => $result['bname'],
                'model' => $result['model'],
                'sku' => $result['sku'],
                'date_added' => $result['date_added'],
                'quantity' => $result['quantity'],
                'sort_order' => $result['sort_order'],
                'bsort_order' => $result['bsort_order'],
                'status' => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
            );
        }

        $data['heading_title2'] = $this->language->get('heading_title2');
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['config_otp_tool'] = $this->config->get('config_otp_tool');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_image'] = $this->language->get('column_image');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_sku'] = $this->language->get('column_sku');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_bsort_order'] = $this->language->get('column_bsort_order');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_date_added'] = $this->language->get('column_date_added');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_model'] = $this->language->get('entry_model');
        $data['entry_sku'] = $this->language->get('entry_sku');
        $data['entry_quantity'] = $this->language->get('entry_quantity');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['brand_name'] = $this->language->get('brand_name');
        $data['entry_algolia'] = $this->language->get('entry_algolia');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['select_country'] = $this->language->get('select_country');
        $data['button_add'] = $this->language->get('button_add');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_filter'] = $this->language->get('button_filter');

        $data['filter_product_id'] = $filter_product_id;
        $data['filter_country'] = $filter_country;
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        foreach ($aDBCountries as $key => $value) {
            $data['countries'][$value['country_id']] = $value['name'];
        }
        $data['token'] = $this->session->data['token'];

        $pagination = new Pagination();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/algoliasearch', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/algoliasearch_list.tpl', $data));
    }

}
