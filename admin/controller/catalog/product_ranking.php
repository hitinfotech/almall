<?php

class ControllerCatalogProductRanking extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('catalog/product_ranking');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->getForm();
    }

    public function update() {
        $this->load->language('catalog/product_ranking');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

            $this->load->model('catalog/product_ranking');
            $this->model_catalog_product_ranking->update( $this->request->post);

              $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/product_ranking','', 'SSL'));
        }

        $this->getForm('edit');
    }

    public function getForm($type = 'new') {

      $this->load->model('catalog/product_ranking');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['button_block_add'] = $this->language->get('button_block_add');
        $data['text_form'] = $this->language->get('text_form');
        $data['button_add_new_range'] = $this->language->get('button_add_new_range');
        $data['button_remove'] = $this->language->get('button_remove');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['tab_block1'] = $this->language->get('tab_block1');
        $data['tab_block2'] = $this->language->get('tab_block2');
        $data['tab_block3'] = $this->language->get('tab_block3');
        $data['tab_block4'] = $this->language->get('tab_block4');
        $data['tab_block5'] = $this->language->get('tab_block5');
        $data['tab_block'] = $this->language->get('tab_block');

        $data['text_from'] = $this->language->get('text_from');
        $data['text_to'] = $this->language->get('text_to');
        $data['text_value'] = $this->language->get('text_value');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', '', 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/product_ranking', '', 'SSL')
        );

        $settings = $this->model_catalog_product_ranking->getSettings();
        foreach($settings as $key => $value ){
          $data['settings'][$value['code']][] = $value;
        }

        $data['action'] = $this->url->link('catalog/product_ranking/update', '', 'SSL');
        $data['cancel'] = $this->url->link('common/dashboard', '', 'SSL');

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/product_ranking_form.tpl', $data));
    }

}
