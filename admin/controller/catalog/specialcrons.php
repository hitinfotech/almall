<?php

class ControllerCatalogSpecialcrons extends Controller {

    private $error = array();

    public function index() {
        $check1 = $this->db->query("DESC brand_discounts_cronj deleted ");
        if (!$check1->num_rows) {
            $this->db->query("ALTER TABLE brand_discounts_cronj ADD deleted INT(1) NOT NULL DEFAULT 0 ");
        }
        $check2 = $this->db->query("DESC brand_discounts_cronj deleted_on ");
        if (!$check2->num_rows) {
            $this->db->query("ALTER TABLE brand_discounts_cronj ADD deleted_on DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' ");
        }
        $check3 = $this->db->query("DESC brand_discounts_cronj deleted_user ");
        if (!$check3->num_rows) {
            $this->db->query("ALTER TABLE brand_discounts_cronj ADD deleted_user INT NOT NULL DEFAULT 0 ");
        }

        $this->load->language('catalog/specialcrons');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/specialcrons');

        $this->getList();
    }

    public function add() {
        $this->language->load('catalog/specialcrons');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/specialcrons');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateModify()) {

            $this->model_catalog_specialcrons->addSpecialCron($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->getList();
        }

        $this->getList();
    }

    public function delete() {
        if (isset($this->request->get['cron_id'])) {
            $cron_id = (int) $this->request->get['cron_id'];
        } else {
            $cron_id = 0;
        }

        if ($cron_id && $this->validateModify()) {
            
            $this->load->model('catalog/specialcrons');
            
            $this->model_catalog_specialcrons->deleteSpecialCron($cron_id);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/specialcrons', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['filter_category'])) {
            $filter_category = $this->request->get['filter_category'];
        } else {
            $filter_category = null;
        }

        if (isset($this->request->get['filter_country'])) {
            $filter_country = $this->request->get['filter_country'];
        } else {
            $filter_country = null;
        }

        if (isset($this->request->get['filter_brand'])) {
            $filter_brand = $this->request->get['filter_brand'];
        } else {
            $filter_brand = null;
        }

        if (isset($this->request->get['filter_user'])) {
            $filter_user = $this->request->get['filter_user'];
        } else {
            $filter_user = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'cj.id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_brand'])) {
            $url .= '&filter_brand=' . $this->request->get['filter_brand'];
        }

        if (isset($this->request->get['filter_user'])) {
            $url .= '&filter_user=' . $this->request->get['filter_user'];
        }

        if (isset($this->request->get['filter_category'])) {
            $url .= '&filter_category=' . $this->request->get['filter_category'];
        }

        if (isset($this->request->get['filter_country'])) {
            $url .= '&filter_country=' . $this->request->get['filter_country'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/specialcrons', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('catalog/specialcrons/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['action'] = $this->url->link('catalog/specialcrons/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('catalog/specialcrons/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->load->model('mall/brand');
        $data['brands'] = $this->model_mall_brand->getBrandsByProducts(array('sort' => 'bd1.name', 'order' => 'ASC', 'force_sort' => 1));

        $this->load->model('catalog/category');
        $data['categories'] = $this->model_catalog_category->getCategories(array('sort'=>'name','order'=>'ASC'));

        $filter_data = array(
            'filter_brand' => $filter_brand,
            'filter_category' => $filter_category,
            'filter_country' => $filter_country,
            'filter_user' => $filter_user,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $specials_total = $this->model_catalog_specialcrons->getTotalSpecialcrons($filter_data);

        $data['product_specials'] = array();
        $result = $this->model_catalog_specialcrons->getSpecialcrons($filter_data);
        foreach ($result as $row) {
            $row['brand_url'] = $this->url->link('mall/brand', 'token=' . $this->session->data['token'] . '&filter_brand_id=' . $row['brand_id']);
            $row['percent'] = number_format($row['percent'], 2);
            $row['done'] = ($row['done'] == 1) ? "Yes" : "No";

            $data['product_specials'][] = $row;
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['tab_special'] = $this->language->get('tab_special');
        $data['text_form'] = $this->language->get('text_form');

        $data['column_id'] = $this->language->get('column_id');
        $data['column_brand'] = $this->language->get('column_brand');
        $data['column_category'] = $this->language->get('column_category');
        $data['column_country'] = $this->language->get('column_country');
        $data['column_percent'] = $this->language->get('column_percent');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_start'] = $this->language->get('column_date_start');
        $data['column_date_end'] = $this->language->get('column_date_end');
        $data['column_done'] = $this->language->get('column_done');
        $data['column_user'] = $this->language->get('column_user');


        $data['button_add'] = $this->language->get('button_add');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if (isset($this->request->get['filter_brand'])) {
            $url .= '&filter_brand=' . $this->request->get['filter_brand'];
        }

        if (isset($this->request->get['filter_user'])) {
            $url .= '&filter_user=' . $this->request->get['filter_user'];
        }

        if (isset($this->request->get['filter_category'])) {
            $url .= '&filter_category=' . $this->request->get['filter_category'];
        }

        if (isset($this->request->get['filter_country'])) {
            $url .= '&filter_country=' . $this->request->get['filter_country'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_brand'] = $this->url->link('catalog/specialcrons', 'token=' . $this->session->data['token'] . '&sort=cj.brand_id' . $url, 'SSL');
        $data['sort_category'] = $this->url->link('catalog/specialcrons', 'token=' . $this->session->data['token'] . '&sort=cj.category_id' . $url, 'SSL');
        $data['sort_country_id'] = $this->url->link('catalog/specialcrons', 'token=' . $this->session->data['token'] . '&sort=cj.country_id' . $url, 'SSL');
        $data['sort_date_added'] = $this->url->link('catalog/specialcrons', 'token=' . $this->session->data['token'] . '&sort=cj.date_added' . $url, 'SSL');
        $data['sort_date_start'] = $this->url->link('catalog/specialcrons', 'token=' . $this->session->data['token'] . '&sort=cj.date_start' . $url, 'SSL');
        $data['sort_date_end'] = $this->url->link('catalog/specialcrons', 'token=' . $this->session->data['token'] . '&sort=cj.date_end' . $url, 'SSL');
        $data['sort_user'] = $this->url->link('catalog/specialcrons', 'token=' . $this->session->data['token'] . '&sort=cj.user_id' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['filter_brand'])) {
            $url .= '&filter_brand=' . $this->request->get['filter_brand'];
        }

        if (isset($this->request->get['filter_user'])) {
            $url .= '&filter_user=' . $this->request->get['filter_user'];
        }

        if (isset($this->request->get['filter_category'])) {
            $url .= '&filter_category=' . $this->request->get['filter_category'];
        }

        if (isset($this->request->get['filter_country'])) {
            $url .= '&filter_country=' . $this->request->get['filter_country'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $pagination = new Pagination();
        $pagination->total = $specials_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/specialcrons', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($specials_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($specials_total - $this->config->get('config_limit_admin'))) ? $specials_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $specials_total, ceil($specials_total / $this->config->get('config_limit_admin')));

        $data['filter_brand'] = $filter_brand;
        $data['filter_category'] = $filter_category;
        $data['filter_country'] = $filter_country;
        $data['filter_user'] = $filter_user;


        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/specialcrons.tpl', $data));
    }

    protected function validateModify() {
        if (!$this->user->hasPermission('modify', 'catalog/specialcrons')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

}
