<?php

class ControllerCatalogSearch extends Controller {

    private $error = array();
    private $keytypes = ['women', 'men', 'offer', 'brand', 'tip', 'look', 'mall', 'shop'];

    public function index() {
        $this->language->load('catalog/search');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/search');

        $this->getList();
    }

    public function approve() {
        $this->language->load('catalog/search');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/search');

        if ((isset($this->request->get['id'])) && $this->validate()) {

            $this->model_catalog_search->approveSearch($this->request->get['id']);

            $this->session->data['success'] = $this->language->get('text_success');
            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('catalog/search', $url, 'SSL'));
        }

        $this->getList();
    }

    public function update() {
        $data = array();
        $this->language->load('catalog/search');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/search');

        if ((isset($this->request->post['id'])) && $this->validateUpdate()) {

            $this->model_catalog_search->updateSearch($this->request->post);

            $data['message'] = $this->language->get('text_success');
        } else {
            $data['message'] = $this->language->get('text_error');
        }
        echo json_encode($data);
    }

    public function disapprove() {
        $this->language->load('catalog/search');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/search');

        if ((isset($this->request->get['id'])) && $this->validate()) {
            $this->model_catalog_search->disapproveSearch($this->request->get['id']);

            $this->session->data['success'] = $this->language->get('text_success');
            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('catalog/search', $url, 'SSL'));
        }

        $this->getList();
    }

    public function add() {
        $this->language->load('catalog/search');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/search');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_search->add($this->request->post);

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/search', '', 'SSL'));
        }

        $this->getForm();
    }

    public function edit() {
        $this->language->load('catalog/search');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/search');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_search->edit($this->request->post, $this->request->get['id']);

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/search', '', 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->language->load('catalog/search');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/search');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $search_id) {
                $this->model_catalog_search->deleteSearch($search_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');
            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('catalog/search', $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_approved'] = $this->language->get('text_approved');
        $data['text_disapproved'] = $this->language->get('text_disapproved');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_approved'] = $this->language->get('entry_approved');

        $data['entry_keyword'] = $this->language->get('entry_keyword');
        $data['entry_counter'] = $this->language->get('entry_counter');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['entry_language'] = $this->language->get('entry_language');
        $data['entry_keytype'] = $this->language->get('entry_keytype');
        $data['entry_url'] = $this->language->get('entry_url');


        $data['help_filter'] = $this->language->get('help_filter');
        $data['help_keyword'] = $this->language->get('help_keyword');
        $data['help_top'] = $this->language->get('help_top');
        $data['help_column'] = $this->language->get('help_column');
        $data['help_return_days'] = $this->language->get('help_return_days');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_data'] = $this->language->get('tab_data');
        $data['tab_design'] = $this->language->get('tab_design');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        if (isset($this->error['url'])) {
            $data['error_url'] = $this->error['url'];
        } else {
            $data['error_url'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', '', 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/search', $url, 'SSL')
        );

        if (!isset($this->request->get['id'])) {
            $data['action'] = $this->url->link('catalog/search/add', $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('catalog/search/edit', 'id=' . $this->request->get['id'] . $url, 'SSL');
        }

        $data['cancel'] = $this->url->link('catalog/search', $url, 'SSL');

        if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $search_info = $this->model_catalog_search->getKeyword($this->request->get['id']);
        }

        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($search_info)) {
            $data['name'] = $search_info['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['url'])) {
            $data['url'] = $this->request->post['url'];
        } elseif (!empty($search_info)) {
            $data['url'] = $search_info['url'];
        } else {
            $data['url'] = '';
        }

        if (isset($this->request->post['counter'])) {
            $data['counter'] = $this->request->post['counter'];
        } elseif (!empty($search_info)) {
            $data['counter'] = $search_info['counter'];
        } else {
            $data['counter'] = 0;
        }

        if (isset($this->request->post['approved'])) {
            $data['approved'] = $this->request->post['approved'];
        } elseif (!empty($search_info)) {
            $data['approved'] = $search_info['approved'];
        } else {
            $data['approved'] = true;
        }

        if (isset($this->request->post['keytype'])) {
            $data['keytype'] = $this->request->post['keytype'];
        } elseif (!empty($search_info)) {
            $data['keytype'] = $search_info['keytype'];
        } else {
            $data['keytype'] = '';
        }

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['language_id'])) {
            $data['language_id'] = $this->request->post['language_id'];
        } elseif (!empty($search_info)) {
            $data['language_id'] = $search_info['language_id'];
        } else {
            $data['language_id'] = 0;
        }

        $this->load->model('localisation/country');
        $data['countries'] = $this->model_localisation_country->getAvailableCountries();

        if (isset($this->request->post['country_id'])) {
            $data['country_id'] = $this->request->post['country_id'];
        } elseif (!empty($search_info)) {
            $data['country_id'] = $search_info['country_id'];
        } else {
            $data['country_id'] = 0;
        }

        $data['keytypes'] = $this->keytypes;

        if (isset($this->request->post['keytype'])) {
            $data['keytype'] = $this->request->post['keytype'];
        } elseif (!empty($search_info)) {
            $data['keytype'] = $search_info['keytype'];
        } else {
            $data['keytype'] = 'women';
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/search_form.tpl', $data));
    }

    protected function getList() {
        
        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }
        if (isset($this->request->get['filter_approved'])) {
            $filter_approved = $this->request->get['filter_approved'];
        } else {
            $filter_approved = null;
        }
        if (isset($this->request->get['filter_keytype'])) {
            $filter_keytype = $this->request->get['filter_keytype'];
        } else {
            $filter_keytype = null;
        }
        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = null;
        }
        if (isset($this->request->get['filter_url'])) {
            $filter_url = $this->request->get['filter_url'];
        } else {
            $filter_url = null;
        }
        if (isset($this->request->get['filter_language_id'])) {
            $filter_language_id = $this->request->get['filter_language_id'];
        } else {
            $filter_language_id = null;
        }


        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'keyword';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';
        
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name='.$this->request->get['filter_name'];
        }
        if (isset($this->request->get['filter_approved'])) {
            $url .=  '&filter_approved='.$this->request->get['filter_approved'];
        } 
        if (isset($this->request->get['filter_keytype'])) {
            $url .= '&filter_keytype='.$this->request->get['filter_keytype'];
        } 
        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id='.$this->request->get['filter_country_id'];
        } 
        if (isset($this->request->get['filter_url'])) {
            $url .= '&filter_url='.$this->request->get['filter_url'];
        }
        if (isset($this->request->get['filter_language_id'])) {
            $url .= '&filter_language_id='.$this->request->get['filter_language_id'];
        }
        
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', '', 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/search', $url, 'SSL')
        );

        $data['add'] = $this->url->link('catalog/search/add', $url, 'SSL');
        $data['delete'] = $this->url->link('catalog/search/delete', $url, 'SSL');

        $data['searchlist'] = array();

        $filter_data = array(
            'filter_name' => $filter_name,
            'filter_approved' => $filter_approved,
            'filter_keytype' => $filter_keytype,
            'filter_country_id' => $filter_country_id,
            'filter_language_id' => $filter_language_id,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $category_total = $this->model_catalog_search->getTotalSearch($filter_data);

        $results = $this->model_catalog_search->getSearch($filter_data);

        foreach ($results as $result) {
            $data['searchlist'][] = array(
                'id' => $result['id'],
                'keyword' => $result['keyword'],
                'keytype' => $result['keytype'],
                'counter' => $result['counter'],
                'url' => $result['url'] . '&q=' . $result['keyword'],
                'country' => $result['country'],
                'language' => $result['language'],
                'status' => $result['approved'] ? 'Approved' : '',
                'approved_by' => $result['approved_by'],
                'approved_on' => $result['approved_on'],
                'date_added' => $result['date_added'],
                'approve' => $this->url->link('catalog/search/approve', 'id=' . $result['id'] . $url, 'SSL'),
                'disapprove' => $this->url->link('catalog/search/disapprove', 'id=' . $result['id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_keytype'] = $this->language->get('column_keytype');
        $data['column_url'] = $this->language->get('column_url');
        $data['column_counter'] = $this->language->get('column_counter');
        $data['column_country'] = $this->language->get('column_country');
        $data['column_language'] = $this->language->get('column_language');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_approved'] = $this->language->get('column_approved');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_approve'] = $this->language->get('button_approve');
        $data['button_disapprove'] = $this->language->get('button_disapprove');
        $data['button_filter'] = $this->language->get('button_filter');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';
        
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name='.$this->request->get['filter_name'];
        }
        if (isset($this->request->get['filter_approved'])) {
            $url .=  '&filter_approved='.$this->request->get['filter_approved'];
        } 
        if (isset($this->request->get['filter_keytype'])) {
            $url .= '&filter_keytype='.$this->request->get['filter_keytype'];
        } 
        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id='.$this->request->get['filter_country_id'];
        } 
        if (isset($this->request->get['filter_url'])) {
            $url .= '&filter_url='.$this->request->get['filter_url'];
        }
        if (isset($this->request->get['filter_language_id'])) {
            $url .= '&filter_language_id='.$this->request->get['filter_language_id'];
        }
        
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('catalog/search', 'sort=keyword' . $url, 'SSL');
        $data['sort_counter'] = $this->url->link('catalog/search', 'sort=counter' . $url, 'SSL');

        $url = '';
        
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name='.$this->request->get['filter_name'];
        }
        if (isset($this->request->get['filter_approved'])) {
            $url .=  '&filter_approved='.$this->request->get['filter_approved'];
        } 
        if (isset($this->request->get['filter_keytype'])) {
            $url .= '&filter_keytype='.$this->request->get['filter_keytype'];
        } 
        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id='.$this->request->get['filter_country_id'];
        } 
        if (isset($this->request->get['filter_url'])) {
            $url .= '&filter_url='.$this->request->get['filter_url'];
        }
        if (isset($this->request->get['filter_language_id'])) {
            $url .= '&filter_language_id='.$this->request->get['filter_language_id'];
        }
        
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $category_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/search', $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render($this->language->get('direction'));

        $data['results'] = sprintf($this->language->get('text_pagination'), ($category_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($category_total - $this->config->get('config_limit_admin'))) ? $category_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $category_total, ceil($category_total / $this->config->get('config_limit_admin')));
        $data['keytypes'] = $this->keytypes;

        $this->load->model('localisation/country');
        $data['countries'] = $this->model_localisation_country->getAvailableCountries();

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();

        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['filter_name'] = $filter_name;
        $data['filter_url'] = $filter_url;
        $data['filter_approved'] = $filter_approved;
        $data['filter_keytype'] = $filter_keytype;
        $data['filter_country_id'] = $filter_country_id;
        $data['filter_language_id'] = $filter_language_id;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/search_list.tpl', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'catalog/search')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'catalog/search')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'catalog/search')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['name']) && (utf8_strlen($this->request->post['name']) < 4 || utf8_strlen($this->request->post['name']) < 32)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        if (!isset($this->request->post['url']) && utf8_strlen($this->request->post['url']) < 4) {
            $this->error['url'] = $this->language->get('error_url');
        }

        if (!isset($this->request->post['keytype']) || !in_array($this->request->post['keytype'], $this->keytypes)) {
            $this->error['keytype'] = $this->language->get('error_keytype');
        }

        return !$this->error;
    }

    protected function validateUpdate() {
        if (!$this->user->hasPermission('modify', 'catalog/search')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['keyword']) || (utf8_strlen($this->request->post['keyword']) < 4 || utf8_strlen($this->request->post['keyword']) > 32)) {
            $this->error['keyword'] = $this->language->get('keyword');
        }

        if (!isset($this->request->post['id']) || $this->request->post['id']<1) {
            $this->error['id'] = $this->language->get('id');
        }
        
        return !$this->error;
    }

}
