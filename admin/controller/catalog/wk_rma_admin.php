<?php

################################################################################################
# Marketplace RMA Opencart 2.x.x.x From Webkul  http://webkul.com 	#
################################################################################################

class ControllerCatalogwkrmaadmin extends Controller {

    private $error = array();
    private $data = array();

    private $status = array();

    protected function setValues(){
        $statuses = $this->db->query(" SELECT * FROM rma_status WHERE language_id = '".(int)$this->config->get('config_language_id')."' ");
        if($statuses->num_rows) {
            foreach ($statuses->rows as $row) {
                $this->status[$row['rma_status_id']] = $row;
            }
        }
    }

    public function index() {
        $this->language->load('catalog/wk_rma_admin');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/wk_rma_admin');

        $this->getList();
    }

    protected function getList() {
        $this->setValues();

        $this->language->load('catalog/wk_rma_admin');

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['button_invoice'] = $this->language->get('button_invoice');
        $this->data['button_delete'] = $this->language->get('button_delete');
        $this->data['text_confirm'] = $this->language->get('text_confirm');
        $this->data['button_filter'] = $this->language->get('button_filter');
        $this->data['button_clrfilter'] = $this->language->get('button_clrfilter');
        $this->data['text_list'] = $this->language->get('text_list');
        $this->data['text_no_recored'] = $this->language->get('text_no_recored');

        //user
        $this->data['wk_rma_admin_id'] = $this->language->get('wk_rma_admin_id');
        $this->data['wk_rma_admin_cid'] = $this->language->get('wk_rma_admin_cid');
        $this->data['wk_rma_admin_cname'] = $this->language->get('wk_rma_admin_cname');
        $this->data['wk_rma_admin_oid'] = $this->language->get('wk_rma_admin_oid');
        $this->data['wk_rma_admin_reason'] = $this->language->get('wk_rma_admin_reason');
        $this->data['wk_rma_admin_date'] = $this->language->get('wk_rma_admin_date');
        $this->data['wk_rma_admin_rmastatus'] = $this->language->get('wk_rma_admin_rmastatus');
        $this->data['wk_rma_admin_isconfirmed'] = $this->language->get('wk_rma_admin_isconfirmed');
        $this->data['wk_rma_admin_adminstatus'] = $this->language->get('wk_rma_admin_adminstatus');
        $this->data['wk_rma_admin_customerstatus'] = $this->language->get('wk_rma_admin_customerstatus');
        $this->data['wk_rma_admin_product'] = $this->language->get('wk_rma_admin_product');

        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }

        if (isset($this->request->get['filter_product'])) {
            $filter_product = $this->request->get['filter_product'];
        } else {
            $filter_product = null;
        }

        if (isset($this->request->get['filter_order'])) {
            $filter_order = $this->commonfunctions->getOrderNumber($this->request->get['filter_order']);
        } else {
            $filter_order = null;
        }

        if (isset($this->request->get['filter_reason'])) {
            $filter_reason = $this->request->get['filter_reason'];
        } else {
            $filter_reason = null;
        }

        if (isset($this->request->get['filter_rma_status'])) {
            $filter_rma_status = $this->request->get['filter_rma_status'];
        } else {
            $filter_rma_status = null;
        }

        if (isset($this->request->get['filter_date'])) {
            $filter_date = $this->request->get['filter_date'];
        } else {
            $filter_date = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data = array(
            'filter_name' => $filter_name,
            'filter_product' => $filter_product,
            'filter_order' => $filter_order,
            'filter_reason' => $filter_reason,
            'filter_rma_status' => $filter_rma_status,
            'filter_date' => $filter_date,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit')
        );

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_product'])) {
            $url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_order'])) {
            $url .= '&filter_order=' . $this->request->get['filter_order'];
        }

        if (isset($this->request->get['filter_reason'])) {
            $url .= '&filter_reason=' . $this->request->get['filter_reason'];
        }

        if (isset($this->request->get['filter_rma_status'])) {
            $url .= '&filter_rma_status=' . $this->request->get['filter_rma_status'];
        }

        if (isset($this->request->get['filter_date'])) {
            $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        $this->data['sort_name'] = $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . '&sort=c.firstname' . $url, 'SSL');
        $this->data['sort_product'] = $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . '&sort=wro.id' . $url, 'SSL');
        $this->data['sort_order'] = $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . '&sort=wro.order_id' . $url, 'SSL');
        $this->data['sort_reason'] = $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . '&sort=wrr.id' . $url, 'SSL');
        $this->data['sort_rma_status'] = $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . '&sort=wrs.order_status_id' . $url, 'SSL');
        $this->data['sort_date'] = $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . '&sort=wro.date' . $url, 'SSL');

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $product_total = $this->model_catalog_wk_rma_admin->viewtotalentry($data);

        $results = $this->model_catalog_wk_rma_admin->viewtotal($data);

        $this->data['delete'] = $this->url->link('catalog/wk_rma_admin/delete', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['rma_sta'] = $this->status;

        $this->data['result_rmaadmin'] = array();

        if ($results)
            foreach ($results as $result) {
                $action = array();

                $action[] = array(
                    'text' => $this->language->get('text_edit'),
                    'href' => $this->url->link('catalog/wk_rma_admin/getForm', 'token=' . $this->session->data['token'] . '&id=' . $result['id'], 'SSL')
                );

                $result_products = $this->model_catalog_wk_rma_admin->viewProducts($result['id'], $filter_product);

                $this->load->model('sale/order');

                foreach ($result_products as $productrma) {
                    $option_data = array();

                    $options = $this->model_sale_order->getOrderOptions($result['order_id'], $productrma['order_product_id']);

                    foreach ($options as $option) {
                        if ($option['type'] != 'file') {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $option['value'],
                                'type' => $option['type']
                            );
                        } else {
                            $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                            if ($upload_info) {
                                $option_data[] = array(
                                    'name' => $option['name'],
                                    'value' => $upload_info['name'],
                                    'type' => $option['type'],
                                    'href' => $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], 'SSL')
                                );
                            }
                        }
                    }
                }


                $product = $reason = '';
                foreach ($result_products as $products) {

                    $product .= $products['name'] . ' <br/> ';
                    $reason .= $products['reason'] . ' <br/> ';
                }

                if (!empty($filter_product) AND strpos(strtolower($product), strtolower($filter_product)) == false) {
                    $product_total--;
                    continue;
                }

                $color = $result['color'];

                $this->data['result_rmaadmin'][] = array(
                    'selected' => False,
                    'id' => $result['id'],
                    'name' => $result['name'],
                    'product' => $product,
                    'oid' => $this->commonfunctions->convertOrderNumber($result['order_id']),
                    'rmastatus' => $result['rma_status'],
                    'color' => $result['color'],
                    'reason' => $reason,
                    'date' => $result['date'],
                    'is_confirmed' => $result['is_confirmed'],
                    'action' => $action
                );
            }


        $this->data['reasons'] = $this->model_catalog_wk_rma_admin->getCustomerReason();


        $this->load->model('localisation/order_status');

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_product'])) {
            $url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_order'])) {
            $url .= '&filter_order=' . $this->request->get['filter_order'];
        }

        if (isset($this->request->get['filter_reason'])) {
            $url .= '&filter_reason=' . $this->request->get['filter_reason'];
        }

        if (isset($this->request->get['filter_rma_status'])) {
            $url .= '&filter_rma_status=' . $this->request->get['filter_rma_status'];
        }

        if (isset($this->request->get['filter_date'])) {
            $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $this->data['invoice'] = $this->url->link('catalog/wk_rma_admin/invoice', 'token=' . $this->session->data['token'], 'SSL');

        $pagination = new Pagination();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();
        $this->data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        $this->data['filter_name'] = $filter_name;
        $this->data['filter_product'] = $filter_product;
        $this->data['filter_order'] = isset($this->request->get['filter_order']) ? $this->request->get['filter_order'] : '' ;
        $this->data['filter_reason'] = $filter_reason;
        $this->data['filter_rma_status'] = $filter_rma_status;
        $this->data['filter_date'] = $filter_date;

        $this->data['header'] = $this->load->controller('common/header');
        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/wk_rma_admin.tpl', $this->data));
    }

    public function getForm() {
        $this->setValues();

        $this->language->load('catalog/wk_rma_admin');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/wk_rma_admin');

        $this->load->model('tool/image');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $file_name = '';

            if ($this->request->files['up_file']['name']) {
                $file = $this->request->files['up_file'];
                $file_name = $file['name'];

                $result = $this->model_catalog_wk_rma_admin->getRmaOrderid($this->request->post['rma_id']);

                if ($result && $result['images']) {
                    $target = DIR_IMAGE . 'rma/' . $result['images'] . "/files/" . $file['name'];
                    @move_uploaded_file($file['tmp_name'], $target);
                }
            }

            $this->model_catalog_wk_rma_admin->updateAdminStatus($this->request->post['wk_rma_admin_msg'], $this->request->post['wk_rma_admin_adminstatus'], $this->request->post['rma_id'], $file_name);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/wk_rma_admin/getForm&id=' . $this->request->post['rma_id'], 'token=' . $this->session->data['token'], 'SSL'));
        }

        $oid = 0;

        if (isset($this->request->get['id'])) {
            $oid = $this->request->get['id'];
        }

        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }

        if (isset($this->request->get['filter_message'])) {
            $filter_message = $this->request->get['filter_message'];
        } else {
            $filter_message = null;
        }

        if (isset($this->request->get['filter_date'])) {
            $filter_date = $this->request->get['filter_date'];
        } else {
            $filter_date = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data = array(
            'filter_name' => $filter_name,
            'filter_message' => $filter_message,
            'filter_date' => $filter_date,
            'filter_id' => $oid,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_message'])) {
            $url .= '&filter_message=' . urlencode(html_entity_decode($this->request->get['filter_message'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_date'])) {
            $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        $url .= '&id=' . $oid;

        $this->data['sort_name'] = $this->url->link('catalog/wk_rma_admin/getForm', 'token=' . $this->session->data['token'] . '&sort=wrm.writer' . $url, 'SSL');
        $this->data['sort_message'] = $this->url->link('catalog/wk_rma_admin/getForm', 'token=' . $this->session->data['token'] . '&sort=wrm.message' . $url, 'SSL');
        $this->data['sort_date'] = $this->url->link('catalog/wk_rma_admin/getForm', 'token=' . $this->session->data['token'] . '&sort=wrm.date' . $url, 'SSL');

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        $this->data['rma_sta'] = $this->status;

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['heading_title'] = $this->language->get('heading_title_details');
        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_back'] = $this->language->get('button_back');
        $this->data['button_confirm'] = $this->language->get('button_confirm');
        $this->data['button_table'] = $this->language->get('button_table');
        $this->data['button_alert'] = $this->language->get('button_alert');
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_filter'] = $this->language->get('button_filter');
        $this->data['button_invoice'] = $this->language->get('button_invoice');
        $this->data['text_form'] = $this->language->get('text_form') . '# ' . $oid;
        //user
        $this->data['wk_rma_admin_id'] = $this->language->get('wk_rma_admin_id');
        $this->data['wk_rma_admin_cid'] = $this->language->get('wk_rma_admin_cid');
        $this->data['wk_rma_admin_cname'] = $this->language->get('wk_rma_admin_cname');
        $this->data['wk_rma_admin_oid'] = $this->language->get('wk_rma_admin_oid');
        $this->data['wk_rma_admin_reason'] = $this->language->get('wk_rma_admin_reason');
        $this->data['wk_rma_admin_date'] = $this->language->get('wk_rma_admin_date');
        $this->data['wk_rma_admin_rmastatus'] = $this->language->get('wk_rma_admin_rmastatus');
        $this->data['wk_rma_admin_isconfirmed'] = $this->language->get('wk_rma_admin_isconfirmed');
        $this->data['wk_rma_admin_adminstatus'] = $this->language->get('wk_rma_admin_adminstatus');
        $this->data['wk_rma_admin_customerstatus'] = $this->language->get('wk_rma_admin_customerstatus');
        $this->data['wk_rma_admin_authno'] = $this->language->get('wk_rma_admin_authno');
        $this->data['wk_rma_admin_add_info'] = $this->language->get('wk_rma_admin_add_info');
        $this->data['wk_rma_admin_msg'] = $this->language->get('wk_rma_admin_msg');
        $this->data['wk_rma_admin_basic'] = $this->language->get('wk_rma_admin_basic');
        $this->data['wk_rma_admin_msg_tab'] = $this->language->get('wk_rma_admin_msg_tab');
        $this->data['wk_rma_admin_images'] = $this->language->get('wk_rma_admin_images');
        $this->data['text_no_option'] = $this->language->get('text_no_option');
        $this->data['wk_rma_admin_product'] = $this->language->get('wk_rma_admin_product');
        $this->data['button_clrfilter'] = $this->language->get('button_clrfilter');
        $this->data['text_no_recored'] = $this->language->get('text_no_recored');
        $this->data['text_quantity'] = $this->language->get('text_quantity');
        $this->data['text_download'] = $this->language->get('text_download');
        $this->data['button_upload'] = $this->language->get('button_upload');
        $this->data['wk_viewrma_msg'] = $this->language->get('wk_viewrma_msg');
        $this->data['wk_rma_admin_return'] = $this->language->get('wk_rma_admin_return');
        $this->data['text_shipping_lable'] = $this->language->get('text_shipping_lable');
        $this->data['text_shipping_info'] = $this->language->get('text_shipping_info');
        $this->data['wk_viewrma_shipping_label'] = $this->language->get('wk_viewrma_shipping_label');

        $this->data['text_lable_image'] = $this->language->get('text_lable_image');
        $this->data['text_lable_name'] = $this->language->get('text_lable_name');

        $this->data['text_select'] = $this->language->get('text_select');
        $this->data['text_decline'] = $this->language->get('text_decline');
        $this->data['text_return'] = $this->language->get('text_return');
        $this->data['text_complete'] = $this->language->get('text_complete');
        $this->data['help_adminStatus'] = $this->language->get('help_adminStatus');

        $result = $this->model_catalog_wk_rma_admin->getRmaOrderid($oid);
        $results_message = $this->model_catalog_wk_rma_admin->viewtotalMessageBy($data);
        $results_message_total = $this->model_catalog_wk_rma_admin->viewtotalNoMessageBy($data);

        $this->data['results_message'] = $results_message;

        $attachmentLinkDir = DIR_IMAGE . 'rma/' . $result['images'] . '/files/';
        $this->data['attachmentLink'] = HTTP_SERVER . '../image/rma/' . $result['images'] . '/files/';

        foreach ($results_message as $key => $value) {
            if (!file_exists($attachmentLinkDir . $value['attachment'])) {
                $this->data['results_message'][$key]['attachment'] = '';
            }
        }

        $this->data['save'] = $this->url->link('catalog/wk_rma_admin/getForm', 'token=' . $this->session->data['token'].'&id=' . $result['id'], 'SSL');
        $this->data['invoice'] = $this->url->link('catalog/wk_rma_admin/invoice&rma_id=' . $oid, 'token=' . $this->session->data['token'], 'SSL');
        $this->data['back'] = $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['savelabel'] = $this->url->link('catalog/wk_rma_admin/saveLabel&id=' . $oid, 'token=' . $this->session->data['token'], 'SSL');

        $this->data['vid'] = $oid;

        $this->data['result_rmaadmin'] = $this->data['result_rmaadmin_images'] = $this->data['result_products'] = array();

        $path = 'rma/' . $result['images'] . '/files/';

        if ($result) {

            $this->document->addScript('../catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
            $this->document->addStyle('../catalog/view/javascript/jquery/magnific/magnific-popup.css');

            $action = array();

            $action[] = array(
                'text' => $this->language->get('text_edit'),
                'href' => $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . '&id=' . $oid, 'SSL')
            );

            $customerDetails = $this->model_catalog_wk_rma_admin->viewCustomerDetails($result['order_id']);


            if ($customerDetails) {

                $chk_product = $this->model_catalog_wk_rma_admin->viewProducts($oid);

                $seller = $this->model_catalog_wk_rma_admin->chkAdminPRoduct($chk_product[0]['product_id']);

                $seller_details = $this->db->query("SELECT cp2p.customer_id, c.firstname FROM " . DB_PREFIX . "customerpartner_to_product cp2p LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer cp2c ON (cp2p.customer_id = cp2c.customer_id) LEFT JOIN " . DB_PREFIX . "customer c ON (cp2c.customer_id = c.customer_id) WHERE cp2p.product_id = '" . (int) $chk_product[0]['product_id'] . "' ORDER BY cp2p.id ASC LIMIT 1")->row;

                $this->data['is_confirmed'] = $result['is_confirmed'];


                if ($result['shipping_label']) {

                    if ($seller == 'admin') {
                        if (!file_exists($attachmentLinkDir . $result['shipping_label'])) {

                            $result['shipping_label'] = '';
                        } else
                            $result['shipping_label'] = $this->model_tool_image->resize($path . 'files/' . $result['shipping_label'], 300, 300, 'h');
                    }else {
                        $seller_path_attlink = DIR_IMAGE . 'rma/' . $seller_details['firstname'] . '/';
                        $seller_path = 'rma/' . $seller_details['firstname'] . '/files/';

                        if (!file_exists($seller_path_attlink . $result['shipping_label'])) {
                            $result['shipping_label'] = '';
                        } else {
                            $result['shipping_label'] = $this->model_tool_image->resize($seller_path . $result['shipping_label'], 300, 300, 'h');
                        }
                    }
                }

                $this->data['result_rmaadmin'] = array(
                    'selected' => False,
                    'id' => $oid,
                    'name' => $customerDetails['firstname'] . ' ' . $customerDetails['lastname'],
                    'oid' => '# ' . $this->commonfunctions->convertOrderNumber($result['order_id']),
                    'orderurl' => $this->url->link('sale/order/info&order_id=' . $this->commonfunctions->convertOrderNumber($result['order_id']) . '&token=' . $this->session->data['token']),
                    'customer_status_id' => $result['customer_st'],
                    'date' => $result['date'],
                    'rmastatus' => $result['rma_status'],
                    'color' => $result['color'],
                    'add_info' => $result['add_info'],
                    'auth_no' => $result['rma_auth_no'],
                    'shipping_label' => $result['shipping_label'],
                    'action' => $action
                );

                $this->load->model('tool/upload');

                $this->load->model('sale/order');

                $rma_products = $this->model_catalog_wk_rma_admin->viewProducts($oid);

                foreach ($rma_products as $productrma) {
                    $option_data = array();

                    $options = $this->model_sale_order->getOrderOptions($result['order_id'], $productrma['order_product_id']);

                    foreach ($options as $option) {
                        if ($option['type'] != 'file') {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $option['value'],
                                'type' => $option['type']
                            );
                        } else {
                            $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                            if ($upload_info) {
                                $option_data[] = array(
                                    'name' => $option['name'],
                                    'value' => $upload_info['name'],
                                    'type' => $option['type'],
                                    'href' => $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], 'SSL')
                                );
                            }
                        }
                    }

                    $this->data['result_products'][] = array(
                        'name' => $productrma['name'],
                        'quantity' => $productrma['quantity'],
                        'reason' => $productrma['reason'],
                        'product_id' => $productrma['product_id'],
                        'order_product_id' => $productrma['order_product_id'],
                        'options' => $option_data,
                    );
                }


                if ($result['images']) {
                    $dir = DIR_IMAGE . 'rma/' . $result['images'] . '/files/';
                    if (file_exists($dir)) {
                        if ($dh = opendir($dir)) {
                            while (($file = readdir($dh)) !== false) {
                                if (!is_dir($file)) {
                                    $this->data['result_rmaadmin_images'][] = array(
                                        'image' => $this->model_tool_image->resize($path . $file, 600, 600, 'h'),
                                        'resize' => $this->model_tool_image->resize($path . $file, 125, 125)
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($seller == 'admin') {
            $target = DIR_IMAGE . 'rma/files/';

            if (!file_exists($target))
                mkdir($target);

            $this->data['shipping_label_folder'] = array();

            if (file_exists($target)) {
                $opentarget = opendir($target);
                while ($image = readdir($opentarget)) {
                    if ($image != '.' AND $image != '..') {
                        $this->data['shipping_label_folder'][] = array('image' => $this->model_tool_image->resize('rma/files/' . $image, 50, 50),
                            'name' => $image);
                    }
                }
            }
        } else {

            $target = DIR_IMAGE . 'rma/' . $seller_details['firstname'] . 'all/';

            $this->data['shipping_label_folder'] = array();

            if (file_exists($target)) {

                $opentarget = opendir($target);
                while ($image = readdir($opentarget)) {
                    if ($image != '.' AND $image != '..') {
                        $this->data['shipping_label_folder'][] = array('image' => $this->model_tool_image->resize('rma/' . $seller_details['firstname'] . 'all/' . $image, 50, 50),
                            'name' => $image);
                    }
                }
            }
        }

        $this->data['token'] = $this->session->data['token'];

        if (isset($this->session->data['error_warning'])) {
            $this->error['warning'] = $this->session->data['error_warning'];
            unset($this->session->data['error_warning']);
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_message'])) {
            $url .= '&filter_message=' . urlencode(html_entity_decode($this->request->get['filter_message'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_date'])) {
            $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $url .= '&id=' . $oid;

        $pagination = new Pagination();
        $pagination->total = $results_message_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('catalog/wk_rma_admin/getForm', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();
        $this->data['results'] = sprintf($this->language->get('text_pagination'), ($results_message_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($results_message_total - $this->config->get('config_limit_admin'))) ? $results_message_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $results_message_total, ceil($results_message_total / $this->config->get('config_limit_admin')));

        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        $this->data['filter_name'] = $filter_name;
        $this->data['filter_message'] = $filter_message;
        $this->data['filter_date'] = $filter_date;

        $this->data['header'] = $this->load->controller('common/header');
        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/wk_rma_admin_details.tpl', $this->data));
    }

    public function invoice() {
        $this->setValues();
        $this->language->load('sale/order');
        $this->language->load('module/wk_rma');

        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $this->data['base'] = HTTPS_SERVER;
        } else {
            $this->data['base'] = HTTP_SERVER;
        }

        $this->data['direction'] = $this->language->get('direction');
        $this->data['language'] = $this->language->get('code');

        $this->data['text_order_id'] = $this->language->get('text_order_id');
        $this->data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $this->data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $this->data['text_date_added'] = $this->language->get('text_date_added');
        $this->data['text_telephone'] = $this->language->get('text_telephone');
        $this->data['text_fax'] = $this->language->get('text_fax');
        $this->data['text_to'] = $this->language->get('text_to');
        $this->data['text_company_id'] = $this->language->get('text_company_id');
        $this->data['text_tax_id'] = $this->language->get('text_tax_id');
        $this->data['text_ship_to'] = $this->language->get('text_ship_to');
        $this->data['text_payment_method'] = $this->language->get('text_payment_method');
        $this->data['text_shipping_method'] = $this->language->get('text_shipping_method');

        $this->data['column_product'] = $this->language->get('column_product');
        $this->data['column_model'] = $this->language->get('column_model');
        $this->data['column_quantity'] = $this->language->get('column_quantity');
        $this->data['column_price'] = $this->language->get('column_price');
        $this->data['column_total'] = $this->language->get('column_total');
        $this->data['column_comment'] = $this->language->get('column_comment');

        $this->language->load('catalog/wk_rma_admin');
        $this->data['title'] = $this->language->get('heading_title');
        $this->data['text_invoice'] = $this->language->get('text_invoice');
        $this->data['text_rmaid'] = $this->language->get('text_rmaid');
        $this->data['text_rma_status'] = $this->language->get('wk_rma_admin_rmastatus');
        $this->data['text_reason'] = $this->language->get('wk_rma_admin_reason');
        $this->data['text_addinfo'] = $this->language->get('wk_rma_admin_add_info');
        $this->data['text_images'] = $this->language->get('text_images');
        $this->data['text_customer_tracking'] = $this->language->get('text_customer_tracking');
        $this->data['wk_rma_admin_return'] = $this->language->get('wk_rma_admin_return');
        $this->data['wk_rma_admin_return_info'] = $this->language->get('wk_rma_admin_return_info');

        $this->load->model('sale/order');

        $this->load->model('setting/setting');

        $this->load->model('catalog/wk_rma_admin');

        $this->load->model('tool/image');

        $this->data['orders'] = array();

        $orders = array();

        $rma_order = array();

        if (isset($this->request->post['selected'])) {
            $rma_order = $this->request->post['selected'];
        } elseif (isset($this->request->get['rma_id'])) {
            $rma_order[] = $this->request->get['rma_id'];
        }

        $this->data['logo'] = '';
        if ($this->config->get('config_logo'))
            $this->data['logo'] = $this->model_tool_image->resize($this->config->get('config_logo'), 200, 200);

        foreach ($rma_order as $rma) {

            $rma_details = $this->model_catalog_wk_rma_admin->getRmaOrderid($rma);

            $order_id = 0;

            if ($rma_details && isset($rma_details['order_id'])) {
                $order_id = $rma_details['order_id'];
            }

            $order_info = $this->model_sale_order->getOrder($order_id);

            $total_data = 0;

            if ($order_info) {

                $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

                if ($store_info) {
                    $store_address = $store_info['config_address'];
                    $store_email = $store_info['config_email'];
                    $store_telephone = $store_info['config_telephone'];
                    $store_fax = $store_info['config_fax'];
                } else {
                    $store_address = $this->config->get('config_address');
                    $store_email = $this->config->get('config_email');
                    $store_telephone = $this->config->get('config_telephone');
                    $store_fax = $this->config->get('config_fax');
                }

                if ($order_info['invoice_no']) {
                    $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                } else {
                    $invoice_no = '';
                }

                if ($order_info['shipping_address_format']) {
                    $format = $order_info['shipping_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{postcode}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['shipping_firstname'],
                    'lastname' => $order_info['shipping_lastname'],
                    'company' => $order_info['shipping_company'],
                    'address_1' => $order_info['shipping_address_1'],
                    'address_2' => $order_info['shipping_address_2'],
                    'city' => $order_info['shipping_city'],
                    'postcode' => $order_info['shipping_postcode'],
                    'zone' => $order_info['shipping_zone'],
                    'zone_code' => $order_info['shipping_zone_code'],
                    'country' => $order_info['shipping_country']
                );

                $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                if ($order_info['payment_address_format']) {
                    $format = $order_info['payment_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{postcode}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['payment_firstname'],
                    'lastname' => $order_info['payment_lastname'],
                    'company' => $order_info['payment_company'],
                    'address_1' => $order_info['payment_address_1'],
                    'address_2' => $order_info['payment_address_2'],
                    'city' => $order_info['payment_city'],
                    'postcode' => $order_info['payment_postcode'],
                    'zone' => $order_info['payment_zone'],
                    'zone_code' => $order_info['payment_zone_code'],
                    'country' => $order_info['payment_country']
                );

                $payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                $product_data = array();

                //get RMA products instead of total Order products
                $products = $this->model_catalog_wk_rma_admin->getOrderProducts($order_id, $rma);

                foreach ($products as $product) {
                    $option_data = array();

                    $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

                    foreach ($options as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['value'];
                        } else {
                            $value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
                        }

                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $value
                        );
                    }

                    $product_data[] = array(
                        'name' => $product['name'],
                        'model' => $product['model'],
                        'option' => $option_data,
                        'quantity' => $product['returned'],
                        'reason' => $product['reason'],
                        'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                        'total' => $this->currency->format(($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0)) * $product['returned'], $order_info['currency_code'], $order_info['currency_value'])
                    );

                    $total_data = $total_data + ( $product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0 )) * $product['returned'];
                }

                $voucher_data = array();

                $vouchers = $this->model_sale_order->getOrderVouchers($order_id);

                foreach ($vouchers as $voucher) {
                    $voucher_data[] = array(
                        'description' => $voucher['description'],
                        'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                    );
                }

                $total_data_main = $this->model_sale_order->getOrderTotals($order_id);

                if ($total_data_main) {
                    foreach ($total_data_main as $key => $value) {
                        if ($value['code'] == 'sub_total') {
                            $total_data_main[$key]['text'] = $this->currency->format($total_data, $order_info['currency_code'], $order_info['currency_value']);
                        } elseif ($value['code'] == 'total') {
                            $total_data_main[$key]['text'] = $this->currency->format($total_data, $order_info['currency_code'], $order_info['currency_value']);
                        //with shipping
                        //$total_data_main[$key]['text'] = $this->currency->format($total_data + (int)substr($total_data_main[1]['text'],'1'), $order_info['currency_code'], $order_info['currency_value']);
                        } else {
                            unset($total_data_main[$key]);
                        }
                    }
                }

                $images = array();

                if ($rma_details['images']) {
                    $path = 'rma/' . $rma_details['images'] . '/';
                    $dir = DIR_IMAGE . 'rma/' . $rma_details['images'] . '/';
                    if (file_exists($dir)) {
                        if ($dh = opendir($dir)) {
                            while (($file = readdir($dh)) !== false) {
                                if (!is_dir($file)) {
                                    $images [] = $this->model_tool_image->resize($path . $file, 150, 150);
                                }
                            }
                        }
                    }
                }
                $rma_status_msg = $admin_return = $color = '';
                $admin_return = $rma_details['rma_status'];
                $color = $rma_details['color'];

                if ($rma_details['customer_st'] == 3) {
                    $rma_status_msg = $this->language->get('wk_rma_admin_return_text');
                }

                $this->data['orders'][] = array(
                    'order_id' => '# ' . $order_id,
                    'return_qty' => $this->url->link('catalog/wk_rma_admin/returnQty&id=' . $rma, 'token=' . $this->session->data['token'], 'SSL'),
                    'invoice_no' => $invoice_no,
                    'date_added' => $order_info['date_added'],
                    'store_name' => $order_info['store_name'],
                    'store_url' => rtrim($order_info['store_url'], '/'),
                    'store_address' => nl2br($store_address),
                    'store_email' => $store_email,
                    'store_telephone' => $store_telephone,
                    'store_fax' => $store_fax,
                    'email' => $order_info['email'],
                    'telephone' => $order_info['telephone'],
                    'shipping_address' => $shipping_address,
                    'shipping_method' => $order_info['shipping_method'],
                    'payment_address' => $payment_address,
                    'payment_method' => $order_info['payment_method'],
                    'product' => $product_data,
                    'voucher' => $voucher_data,
                    'total' => $total_data_main,
                    'comment' => nl2br($order_info['comment']),
                    'add_info' => $rma_details['add_info'],
                    'rma_status_msg' => ucfirst($rma_status_msg),
                    'auth_no' => $rma_details['rma_auth_no'],
                    'color' => $color,
                    'admin_return' => $admin_return,
                    'id' => '# ' . $rma,
                    'images' => $images,
                    'tracking' => $rma_details['rma_auth_no'],
                    'date_added_rma' => $rma_details['date'],
                );
            }
        }

        $this->response->setOutput($this->load->view('catalog/wk_rma_invoice.tpl', $this->data));
    }

    public function saveLabel() {

        $this->language->load('catalog/wk_rma_admin');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/wk_rma_admin');
        if (isset($this->request->post['selected']) || (isset($this->request->files['shipping_label']['name']) && $this->request->files['shipping_label']['name'])) {

            $file = $this->request->files['shipping_label'];



            if (isset($this->request->get['id']) && $this->validateForm()) {

                $result = $this->model_catalog_wk_rma_admin->getRmaOrderid($this->request->get['id']);

                $file_name = '';
                $path = DIR_IMAGE . 'rma/';
                $seller_name = $this->db->query("SELECT c.firstname, c.lastname FROM " . DB_PREFIX . "customerpartner_to_customer cp2c LEFT JOIN " . DB_PREFIX . "customer c ON (cp2c.customer_id = c.customer_id) WHERE cp2c.is_partner = '1' AND cp2c.customer_id = '" . (int) $result['customer_id'] . "'")->row;

                if (isset($seller_name['firstname']) && $seller_name['firstname']) {
                    $path = $path . $seller_name['firstname'];
                    $path_all = DIR_IMAGE . 'rma/' . $seller_name['firstname'] . 'all';
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                        mkdir($path_all, 0777, true);
                    }
                }


                if ($result) {

                    $file_name = $file['name'] ? $file['name'] : $this->request->post['selected'];

                    $target = $path . '/' . $file_name;

                    if ($file['name']) {
                        if ($result && $result['images']) {
                            @move_uploaded_file($file['tmp_name'], $target);
                            //for label list
                            @copy($target, DIR_IMAGE . 'rma/files/' . $file_name);
                            // @mkdir(DIR_IMAGE.'rma/files',0755,true);
                            // move_uploaded_file($file['tmp_name'], DIR_IMAGE.'rma/files/'.$file_name);
                        }
                    } elseif ($this->request->post['selected']) {
                        if (file_exists(DIR_IMAGE . 'rma/files/' . $this->request->post['selected'])) {
                            copy(DIR_IMAGE . 'rma/files/' . $this->request->post['selected'], $target);
                        }
                    }
                }
                if ($file_name) {
                    $this->model_catalog_wk_rma_admin->addLabel($this->request->get['id'], $file_name, $result['images']);
                    $this->session->data['success'] = $this->language->get('text_success');
                } else {
                    $this->session->data['error_warning'] = $this->language->get('error_label');
                    $this->response->redirect($this->url->link('catalog/wk_rma_admin/getForm&id=' . $this->request->get['id'], 'token=' . $this->session->data['token'] . $url, 'SSL'));
                }

                $url = '';

                if (isset($this->request->get['page'])) {
                    $url .= '&page=' . $this->request->get['page'];
                }

                $this->response->redirect($this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
            }
        } else {//chk
            $this->response->redirect($this->url->link('catalog/wk_rma_admin/getForm&id=' . $this->request->get['id'], 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getList();
    }

    public function returnQty() {

        $this->language->load('catalog/wk_rma_admin');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/wk_rma_admin');

        if (isset($this->request->get['id']) && $this->validateForm()) {

            $this->model_catalog_wk_rma_admin->returnQty($this->request->get['id']);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    public function delete() {
$this->index();
return false;
        $this->language->load('catalog/wk_rma_admin');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/wk_rma_admin');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $id) {
                $this->model_catalog_wk_rma_admin->deleteentry($id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }
        $this->getList();
    }

    private function validateForm() {

        if (!$this->user->hasPermission('modify', 'catalog/wk_rma_admin')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if(!isset($this->request->post['rma_id']) || $this->request->post['rma_id'] == ''){
            $this->error['warning'] = $this->language->get('error_warning');
        }

        if (!isset($this->request->post['wk_rma_admin_msg']) || utf8_strlen($this->request->post['wk_rma_admin_msg']) < 3){
            $this->error['warning'] = $this->language->get('error_message');
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    private function validateDelete() {

        if (!$this->user->hasPermission('modify', 'catalog/wk_rma_admin')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function confirm_rma() {
        if (isset($this->request->get['rma_id'])) {
            $rma_id = $this->request->get['rma_id'];
        } else {
            echo "RMA id is needed!";
            return;
        }

        $this->load->model('catalog/wk_rma_admin');
        $this->model_catalog_wk_rma_admin->confirm_rma($rma_id);

        echo json_encode("success");
    }

}
