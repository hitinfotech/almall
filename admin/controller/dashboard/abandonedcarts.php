<?php
class ControllerDashboardAbandonedCarts extends Controller {
	public function index() {
		$this->load->language('dashboard/abandonedcarts');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_view'] = $this->language->get('text_view');

		$data['token'] = $this->session->data['token'];

        $data['total'] = 0;

        $AB_count = $this->db->query("SELECT count(*) as total FROM `" . DB_PREFIX . "abandonedcarts` WHERE `notified`=0");
    
        $data['total'] = $AB_count->row['total'];

		$data['link'] = $this->url->link('module/abandonedcarts', 'token=' . $this->session->data['token'], true);

		return $this->load->view('dashboard/abandonedcarts.tpl', $data);
	}
}