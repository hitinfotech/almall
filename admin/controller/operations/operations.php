<?php

class ControllerOperationsOperations extends Controller {

    public function index() {
        $this->load->language('operations/operations');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_order'] = $this->language->get('text_order');
        $data['text_delayed_order'] = $this->language->get('text_delayed_order');
        $data['text_average_order'] = $this->language->get('text_average_order');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('operations/operations', 'token=' . $this->session->data['token'], 'SSL')
        );

        $this->load->model("sale/order");
        $orders_count = $this->model_sale_order->getTotalCountOrders("(1,3,4,17)");
        if(!empty($orders_count)){
          foreach($orders_count as $k => $order_c){
            $order_count_array[$order_c['order_status_id']] = $order_c;
            $order_count_array[$order_c['order_status_id']]['link'] = $this->url->link('customerpartner/order', 'filter_order_status=' . $order_c['order_status_id'], 'SSL');
          }
        }
        $data['orders_count'][0] = (!empty($order_count_array[4])) ? $order_count_array[4]:  array('name'=>'Verification' ,'order_count'=>0,'link'=>$this->url->link('customerpartner/order', 'filter_order_status=4' , 'SSL'));
        $data['orders_count'][1] = (!empty($order_count_array[1])) ? $order_count_array[1]:  array('name'=>'Pending' ,'order_count'=>0,'link'=>$this->url->link('customerpartner/order', 'filter_order_status=1' , 'SSL'));
        $data['orders_count'][2] = (!empty($order_count_array[17])) ? $order_count_array[17]:  array('name'=>'Ready to pickup' ,'order_count'=>0,'link'=>$this->url->link('customerpartner/order', 'filter_order_status=17' , 'SSL'));
        $data['orders_count'][3] = (!empty($order_count_array[3])) ? $order_count_array[3]:  array('name'=>'Shipped' ,'order_count'=>0,'link'=>$this->url->link('customerpartner/order', 'filter_order_status=3' , 'SSL'));


        $orders_count_by_period = $this->model_sale_order->getOperationsOrdersByPeriod("(4,1,17,3)");
        if(!empty($orders_count_by_period)){
          foreach($orders_count_by_period as $k => $order_c){
            $orders_count_by_period_array[$order_c['order_status_id']] = $order_c;
          }
        }
        $data['orders_count_by_period'] = array();
        if(!empty($orders_count_by_period_array)){
          $data['orders_count_by_period'][0] = (!empty($orders_count_by_period_array[4])) ? $orders_count_by_period_array[4]:  array('name'=>'Verification' ,'order_count'=>0);
          $data['orders_count_by_period'][1] = (!empty($orders_count_by_period_array[1])) ? $orders_count_by_period_array[1]:  array('name'=>'Pending' ,'order_count'=>0);
          $data['orders_count_by_period'][2] = (!empty($orders_count_by_period_array[17])) ? $orders_count_by_period_array[17]:  array('name'=>'Ready to pickup' ,'order_count'=>0);
          $data['orders_count_by_period'][3] = (!empty($orders_count_by_period_array[3])) ? $orders_count_by_period_array[3]:  array('name'=>'Shipped' ,'order_count'=>0);

        }
        $orders_latest_average = $this->model_sale_order->getOperationsLatestOrdersAverage("(1,3,4,17)");
        $latest_average = [];
        foreach ($orders_latest_average as $key => $order){
          $time = explode(':',$order['time_diff']);
          if (!empty ($latest_average[$order['order_status_id']])){
            $latest_average[$order['order_status_id']]['count']++;
            $latest_average[$order['order_status_id']]['hours'] += (int)$time[0];
            $latest_average[$order['order_status_id']]['minutes'] += (int)$time[1];
          }else{
            $latest_average[$order['order_status_id']]['count'] = 1;
            $latest_average[$order['order_status_id']]['name'] = $order['name'];
            $latest_average[$order['order_status_id']]['hours'] = (int)$time[0];
            $latest_average[$order['order_status_id']]['minutes'] = (int)$time[1];
          }
        }
        foreach($latest_average as $k => $v){
          $latest_average [$k]['hours_avg'] = round($latest_average [$k]['hours'] / $latest_average [$k]['count']);
          $latest_average [$k]['minutes_avg'] = round($latest_average [$k]['minutes'] / $latest_average [$k]['count']);
        }
        $pending_array = array('name'=>'Pending' ,'order_count'=>0,'hours_avg'=>0,'minutes_avg'=>0);
        $verification_array = array('name'=>'Verification' ,'order_count'=>0,'hours_avg'=>0,'minutes_avg'=>0);
        $shippied_array = array('name'=>'Shipped' ,'order_count'=>0,'hours_avg'=>0,'minutes_avg'=>0);
        $Ready_to_pickup_array = array('name'=>'Ready to pickup' ,'order_count'=>0,'hours_avg'=>0,'minutes_avg'=>0);

        foreach($latest_average as $num => $order_data){
          if($order_data['name'] == 'Pending')
            $pending_array = $order_data;
          elseif($order_data['name'] == 'Shipped')
            $shippied_array = $order_data;
          elseif($order_data['name'] == 'Verification')
            $verification_array = $order_data;
          elseif($order_data['name'] == 'Ready to pick up')
            $Ready_to_pickup_array = $order_data;
        }


        $data['latest_average'][0] = $verification_array;
        $data['latest_average'][1] = $pending_array;
        $data['latest_average'][2] = $Ready_to_pickup_array;
        $data['latest_average'][3] = $shippied_array;


        $data['token'] = $this->session->data['token'];
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');


        $this->response->setOutput($this->load->view('operations/operations.tpl', $data));
    }

}
