<?php

class ControllerOfferOffer extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('offer/offer');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('offer/offer');
        $this->getList();
    }

    public function add() {
        $this->language->load('offer/offer');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('offer/offer');
        //echo "<pre>";die(print_r($this->request->post));
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $offer_id = $this->model_offer_offer->addOffer($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($offer_id) && !empty($offer_id)) {
                $url .= '&filter_offer_id=' . $offer_id;
            }
            $this->response->redirect($this->url->link('offer/offer', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function edit() {
        $this->language->load('offer/offer');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('offer/offer');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_offer_offer->editOffer($this->request->get['offer_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success_add');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($this->request->get['offer_id']) && !empty($this->request->get['offer_id'])) {
                $url .= '&filter_offer_id=' . $this->request->get['offer_id'];
            }
            $this->response->redirect($this->url->link('offer/offer', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->language->load('offer/offer');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('offer/offer');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $offer_id) {
                $this->model_offer_offer->deleteoffer($offer_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('offer/offer', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = '';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
        if (isset($this->request->get['filter_date_added'])) {
            $filter_date_added = $this->request->get['filter_date_added'];
        } else {
            $filter_date_added = NULL;
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $filter_date_modified = $this->request->get['filter_date_modified'];
        } else {
            $filter_date_modified = NULL;
        }
        // $country_id = '';
        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = $this->request->get['filter_country_id'];
            $data['filter_country_id'] = $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = null;
        }

        if (isset($this->request->get['filter_offer_id'])) {
            $filter_offer_id = $this->request->get['filter_offer_id'];
            $data['filter_offer_id'] = $this->request->get['filter_offer_id'];
        } else {
            $filter_offer_id = null;
        }

        // $offer_description = '';
        if (isset($this->request->get['filter_description'])) {
            $filter_description = $this->request->get['filter_description'];
            $data['filter_description'] = $this->request->get['filter_description'];
        } else {
            $filter_description = null;
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('offer/offer', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('offer/offer/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('offer/offer/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['refresh'] = $this->url->link('offer/offer/refresh', 'token=' . $this->session->data['token'] . $url, 'SSL');


        //-- Lables
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['select_country'] = $this->language->get('select_country');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');

        $data['button_filter'] = $this->language->get('button_filter');

        $data['token'] = $this->session->data['token'];
        $data['action'] = $this->url->link('offer/offer/', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        $data['countries'] = $aDBCountries;
        // echo "<pre>"; die(print_r($aDBCountries));

        $data['offer'] = array();

        $filter_data = array(
            'filter_offer_id' => $filter_offer_id,
            'filter_country_id' => $filter_country_id,
            'filter_description' => $filter_description,
            'filter_date_added' => $filter_date_added,
            'filter_date_modified' => $filter_date_modified,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        // echo "<pre>"; die(print_r($filter_data));

        $offer_total = $this->model_offer_offer->getTotalOffer($filter_data);
        $results = $this->model_offer_offer->getOfferList($filter_data);

        foreach ($results as $result) {
            $data['offer'][] = array(
                'sort_offer' => $result['offer_id'],
                'title' => $result['name'],
                'sort_order' => $result['sort_order'],
                'date_added' => $result['date_added'],
                'date_modified' => $result['date_modified'],
                'user_add' => $result['user_add'],
                'user_modify' => $result['user_modify'],
                'edit' => $this->url->link('offer/offer/edit', 'token=' . $this->session->data['token'] . '&offer_id=' . $result['offer_id'] . $url, 'SSL'),
                'delete' => $this->url->link('offer/offer/delete', 'token=' . $this->session->data['token'] . '&offer_id=' . $result['offer_id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['column_user_id'] = $this->language->get('column_user_id');
        $data['column_last_mod_id'] = $this->language->get('column_last_mod_id');

        $data['column_title'] = $this->language->get('column_title');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_featured'] = $this->language->get('column_featured');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        $pagination = new Pagination();
        $pagination->total = $offer_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('offer/offer', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($offer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($offer_total - $this->config->get('config_limit_admin'))) ? $offer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $offer_total, ceil($offer_total / $this->config->get('config_limit_admin')));

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';



        if (isset($filter_country_id) && !empty($filter_country_id)) {
            $url .= '&filter_country_id=' . $filter_country_id;
        }
        if (isset($filter_description) && !empty($filter_description)) {
            $url .= '&filter_description=' . $filter_description;
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }
        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['sort_offer'] = $this->url->link('offer/offer', 'token=' . $this->session->data['token'] . $url . '&sort=sort_offer', 'SSL');
        $data['sort_date_added'] = $this->url->link('offer/offer', 'token=' . $this->session->data['token'] . $url . '&sort=date_added', 'SSL');
        $data['sort_date_modified'] = $this->url->link('offer/offer', 'token=' . $this->session->data['token'] . $url . '&sort=date_modified', 'SSL');

        $data['sort_order'] = $this->url->link('offer/offer', 'token=' . $this->session->data['token'] . $url . '&sort=sort_order', 'SSL');
        $data['filter_country_id'] = $filter_country_id;
        $data['filter_discription'] = $filter_description;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('offer/offer_list.tpl', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['bActiveLanguageTabFlag'] = true;
        $data['bActiveLanguagePaneFlag'] = true;

        $data['user_id'] = $this->session->data['user_id'];

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_attribute'] = $this->language->get('tab_attribute');
        $data['tab_links'] = $this->language->get('tab_links');
        $data['tab_image'] = $this->language->get('tab_image');

        $data['text_form'] = !isset($this->request->get['_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');

        //-- Description/Attribute
        $data['entry_title'] = $this->language->get('entry_title');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_body_text'] = $this->language->get('entry_body_text');
        $data['entry_period'] = $this->language->get('entry_period');

        //-- General
        $data['entry_start_date'] = $this->language->get('entry_start_date');
        $data['entry_end_date'] = $this->language->get('entry_end_date');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_home'] = $this->language->get('entry_home');

        $data['column_action'] = $this->language->get('column_action');

        //-- Images
        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_featured'] = $this->language->get('entry_featured');
        $data['button_image_add'] = $this->language->get('button_image_add');
        $data['button_remove'] = $this->language->get('button_remove');

        //-- Links
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_city_id'] = $this->language->get('entry_city_id');
        $data['entry_mall_id'] = $this->language->get('entry_mall_id');
        $data['entry_shop_id'] = $this->language->get('entry_shop_id');
        $data['entry_brand_id'] = $this->language->get('entry_brand_id');
        $data['entry_category_id'] = $this->language->get('entry_category_id');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['title'])) {
            $data['error_title'] = $this->error['title'];
        } else {
            $data['error_title'] = array();
        }

        if (isset($this->error['error_date_start'])) {
            $data['error_date_start'] = $this->error['error_date_start'];
        } else {
            $data['error_date_start'] = '';
        }

        if (isset($this->error['error_date_end'])) {
            $data['error_date_end'] = $this->error['error_date_end'];
        } else {
            $data['error_date_end'] = '';
        }

        if (isset($this->error['error_date_start_required'])) {
            $data['error_date_start_required'] = $this->error['error_date_start_required'];
        } else {
            $data['error_date_start_required'] = '';
        }

        if (isset($this->error['error_date_end_required'])) {
            $data['error_date_end_required'] = $this->error['error_date_end_required'];
        } else {
            $data['error_date_end_required'] = '';
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('offer/offer', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['offer_id'])) {
            $data['action'] = $this->url->link('offer/offer/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('offer/offer/edit', 'token=' . $this->session->data['token'] . '&offer_id=' . $this->request->get['offer_id'] . $url, 'SSL');
        }
        $data['cancel'] = $this->url->link('offer/offer', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['offer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $offer_info = $this->model_offer_offer->getOffer($this->request->get['offer_id']);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();
        if (isset($this->request->post['offer_description'])) {
            $data['offer_description'] = $this->request->post['offer_description'];
        } elseif (!empty($offer_info)) {
            $data['offer_description'] = $this->model_offer_offer->getOfferDescriptions($offer_info['offer_id']);
        } else {
            $data['offer_description'] = array();
        }


        if (isset($this->request->post['start_date'])) {
            $data['start_date'] = $this->request->post['start_date'];
        } elseif (!empty($offer_info)) {
            $data['start_date'] = $offer_info['start_date'];
        } else {
            $data['start_date'] = "";
        }

        if (isset($this->request->post['end_date'])) {
            $data['end_date'] = $this->request->post['end_date'];
        } elseif (!empty($offer_info)) {
            $data['end_date'] = $offer_info['end_date'];
        } else {
            $data['end_date'] = "";
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (isset($this->request->get['offer_id'])) {
            $data['sort_order'] = $offer_info['sort_order'];
        } else {
            $data['sort_order'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (isset($this->request->get['offer_id'])) {
            $data['status'] = $offer_info['status'];
        } else {
            $data['status'] = 0;
        }
        if (isset($this->request->post['home'])) {
            $data['home'] = $this->request->post['home'];
        } elseif (!empty($offer_info)) {
            $data['home'] = $offer_info['home'];
        } else {
            $data['home'] = 0;
        }
        //-- Images
        $this->load->model('tool/image');
        $data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 100, 100);

        // Images
        if (isset($this->request->post['offer_image'])) {
            $offer_images = $this->request->post['offer_image'];
        } elseif (isset($this->request->get['offer_id'])) {
            $offer_images = $this->model_offer_offer->getOfferImages($this->request->get['offer_id']);
        } else {
            $offer_images = array();
        }

        $data['offer_images'] = array();

        foreach ($offer_images as $offer_image) {
            if (is_file(DIR_IMAGE . $offer_image['image'])) {
                $image = $offer_image['image'];
                $thumb = $offer_image['image'];
            } else {
                $image = '';
                $thumb = 'placeholder.png';
            }

            $data['offer_images'][] = array(
                'image' => $image,
                'thumb' => $this->model_tool_image->resize($thumb, 100, 100),
                'sort_order' => $offer_image['sort_order'],
                'featured' => $offer_image['featured']
            );
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($data['image'])) {
            $data['image'] = $image;
        } else {
            $data['image'] = '';
        }


        $data['shops'] = array();
        if (isset($this->request->get['offer_id'])) {
            $results = $this->model_offer_offer->getOfferShops($this->request->get['offer_id']);
            foreach ($results as $row) {
                $data['shops'][] = array(
                    'shop_id' => $row['shop_id'],
                    'name' => strip_tags(html_entity_decode($row['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $data['brands'] = array();
        if (isset($this->request->get['offer_id'])) {
            $results = $this->model_offer_offer->getOfferBrands($this->request->get['offer_id']);
            foreach ($results as $row) {
                $data['brands'][] = array(
                    'brand_id' => $row['brand_id'],
                    'name' => strip_tags(html_entity_decode($row['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $data['category'] = array();
        if (isset($this->request->get['offer_id'])) {
            $results = $this->model_offer_offer->getOfferCategory($this->request->get['offer_id']);
            foreach ($results as $row) {
                $data['category'][] = array(
                    'category_id' => $row['category_id'],
                    'name' => strip_tags(html_entity_decode($row['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $this->load->model('design/layout');

        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

// echo "<pre>";print_r($data);die("OK !!");

        $this->response->setOutput($this->load->view('offer/offer_form.tpl', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'offer/offer')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post['offer_description'] as $language_id => $value) {
            if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 255)) {
                $this->error['title'][$language_id] = $this->language->get('error_title');
            }
        }

        if (isset($this->request->post['start_date']) && isset($this->request->post['end_date']) && $this->request->post['start_date'] != "" && $this->request->post['end_date'] != '') {
            if (strtotime($this->request->post['start_date']) > strtotime($this->request->post['end_date'])) {
                $this->error['error_date_start'] = $this->language->get('error_date_start');
                $this->error['error_date_end'] = $this->language->get('error_date_end');
            }
        }

        if (empty($this->request->post['start_date']) || $this->request->post['start_date'] == "") {
            $this->error['error_date_start_required'] = $this->language->get('error_date_start_required');
        }

        if (empty($this->request->post['end_date']) || $this->request->post['end_date'] == '') {
            $this->error['error_date_end_required'] = $this->language->get('error_date_end_required');
        }
        /*
          if (isset($this->request->post['offer_id']) && (int)$this->request->post['offer_id'] <= 0) {
          $this->error['error_offer_id'] = $this->language->get('error_offer_id');
          }

          foreach ($this->request->post['offer_description'] as $language_id => $value) {
          if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 255)) {
          $this->error['title'][$language_id] = $this->language->get('error_title');
          }

          if ((utf8_strlen($value['description']) < 3) || (utf8_strlen($value['description']) > 255)) {
          $this->error['description'][$language_id] = $this->language->get('error_description');
          }

          if ((utf8_strlen($value['body_text']) < 3) || (utf8_strlen($value['body_text']) > 255)) {
          $this->error['error_body_text'][$language_id] = $this->language->get('error_body_text');
          }
          } */
        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'offer/offer')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete() {

        // die(print_r($this->request->get));

        $json = array();

        if (isset($this->request->get['query'])) {

            if (isset($this->request->get['query'])) {
                $filter_name = $this->request->get['query'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['type'])) {
                $type = $this->request->get['type'];
            } else {
                $type = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array('filter_name' => $filter_name, 'filter_keyword' => $filter_name);

            if ($type == 'mall') {
                $this->load->model('mall/mall');
                $results = $this->model_mall_mall->getMallsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['mall_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'shop') {
                $this->load->model('mall/shop');
                $results = $this->model_mall_shop->getshopsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')) . ' ( ' . strip_tags(html_entity_decode($result['mall_name'], ENT_QUOTES, 'UTF-8')) . ' ) '
                    );
                }
            } else if ($type == 'brand') {
                $this->load->model('mall/brand');
                $results = $this->model_mall_brand->getBrandsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['brand_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'country') {
                $this->load->model('localisation/country');
                $results = $this->model_localisation_country->getCountriesByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['country_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'city') {
                $this->load->model('localisation/zone');
                $results = $this->model_localisation_zone->getZonesByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['zone_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'category') {
                $this->load->model('mall/store_category');
                $results = $this->model_mall_store_category->getStoreCategories($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_category_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
