<?php

class ControllerNewsletterNewsletter extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('newsletter/newsletter');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('newsletter/newsletter');
        $this->getList();
    }

    public function delete() {
        $this->language->load('newsletter/newsletter');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('newsletter/newsletter');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $id) {
                $this->model_newsletter_newsletter->deleteNewsletter($id);
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('newsletter/newsletter', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = '';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
        if (isset($this->request->get['filter_id'])) {
            $filter_id = $this->request->get['filter_id'];
            $data['filter_id'] = $this->request->get['filter_id'];
        } else {
            $filter_id = null;
        }
        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = $this->request->get['filter_country_id'];
            $data['filter_country_id'] = $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = null;
        }
        if (isset($this->request->get['filter_email'])) {
            $filter_email = $this->request->get['filter_email'];
            $data['filter_email'] = $this->request->get['filter_email'];
        } else {
            $filter_email = null;
        }
        if (isset($this->request->get['filter_status'])) {
            $filter_status = $this->request->get['filter_status'];
            $data['filter_status'] = $this->request->get['filter_status'];
        } else {
            $filter_status = null;
        }
        if (isset($this->request->get['filter_gender'])) {
            $filter_gender = $this->request->get['filter_gender'];
            $data['filter_gender'] = $this->request->get['filter_gender'];
        } else {
            $filter_gender = null;
        }
        
        
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('newsletter/newsletter', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['delete'] = $this->url->link('newsletter/newsletter/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['refresh'] = $this->url->link('newsletter/newsletter/refresh', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['newsletter'] = array();
        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        $data['countries'] = $aDBCountries;
        // echo "<pre>"; die(print_r($aDBCountries));

        $filter_data = array(
            'filter_id' => $filter_id,
            'filter_country_id' => $filter_country_id,
            'filter_email' => $filter_email,
            'filter_gender' => $filter_gender,
            'filter_status' => $filter_status,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $newsletter_total = $this->model_newsletter_newsletter->getTotalNewsletter($filter_data);
        $results = $this->model_newsletter_newsletter->getNewsletterList($filter_data);

        foreach ($results as $result) {
            $data['newsletter'][] = array(
                'id' => $result['id'],
                'email' => $result['email'],
                'status' => $result['status'],
                'created_date' => $result['created_date'],
                'updated_date' => $result['updated_date'],
                'gender' => $result['gender'],
                'language_id' => $result['language_id'],
                'country_id' => $result['country_id'],
                'delete' => $this->url->link('newsletter/newsletter/delete', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['column_email'] = $this->language->get('column_email');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['column_gender'] = $this->language->get('column_gender');

        $data['text_list'] = $this->language->get('text_list');        
        $data['column_id'] = $this->language->get('column_id');

        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['column_title'] = $this->language->get('column_title');
        $data['entry_start_date'] = $this->language->get('entry_start_date');
        $data['entry_updated_date'] = $this->language->get('entry_updated_date');
        $data['entry_created_date'] = $this->language->get('entry_created_date');
        $data['entry_language_id'] = $this->language->get('entry_language_id');
        $data['entry_gender'] = $this->language->get('entry_gender');
        $data['select_gender'] = $this->language->get('select_gender');

        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_featured'] = $this->language->get('column_featured');
        $data['column_action'] = $this->language->get('column_action');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['select_country'] = $this->language->get('select_country');
        $data['filter_email'] = $this->language->get('filter_email');
        $data['filter_gender'] = $this->language->get('filter_gender');
        $data['filter_status'] = $this->language->get('filter_status');

        $data['entry_email'] = $this->language->get('entry_email');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');
        $data['token'] = $this->session->data['token'];
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($filter_country_id) && !empty($filter_country_id)) {
            $url .= '&filter_country_id=' . $filter_country_id;
        }
        if (isset($filter_email) && !empty($filter_email)) {
            $url .= '&filter_email=' . $filter_email;
        }


        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_id'] = $this->url->link('newsletter/newsletter', 'token=' . $this->session->data['token'] . '&sort=n.id' . $url, 'SSL');
        $data['sort_email'] = $this->url->link('newsletter/newsletter', 'token=' . $this->session->data['token'] . '&sort=n.email' . $url, 'SSL');
        $data['sort_status'] = $this->url->link('newsletter/newsletter', 'token=' . $this->session->data['token'] . $url . '&sort=n.status', 'SSL');
        $data['sort_gender'] = $this->url->link('newsletter/newsletter', 'token=' . $this->session->data['token'] . $url . '&sort=n.gender', 'SSL');
        $data['sort_created_date'] = $this->url->link('newsletter/newsletter', 'token=' . $this->session->data['token'] . $url . '&sort=n.created_date', 'SSL');
        $data['sort_updated_date'] = $this->url->link('newsletter/newsletter', 'token=' . $this->session->data['token'] . $url . '&sort=n.updated_date', 'SSL');
        $data['sort_language_id'] = $this->url->link('newsletter/newsletter', 'token=' . $this->session->data['token'] . $url . '&sort=n.language_id', 'SSL');
        $data['sort_country_id'] = $this->url->link('newsletter/newsletter', 'token=' . $this->session->data['token'] . $url . '&sort=n.country_id', 'SSL');

        
        $pagination = new Pagination();
        $pagination->total = $newsletter_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('newsletter/newsletter', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($newsletter_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($newsletter_total - $this->config->get('config_limit_admin'))) ? $newsletter_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $newsletter_total, ceil($newsletter_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['filter_country_id'] = $filter_country_id;
        $data['filter_email'] = $filter_email;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('newsletter/newsletter_list.tpl', $data));
    }
     protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'newsletter/newsletter')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

}
