<?php

class ControllerModuleAbandonmentcarts extends Controller {

    private $moduleName;
    private $modulePath;
    private $moduleModel;
    private $moduleVersion;
    private $extensionsLink;
    private $callModel;
    private $error = array();
    private $data = array();

    public function __construct($registry) {
        parent::__construct($registry);

        // Config Loader
        $this->config->load('isenselabs/abandonmentcarts');

        /* Fill Main Variables - Begin */
        $this->moduleName = $this->config->get('abandonmentcarts_name');
        $this->callModel = $this->config->get('abandonmentcarts_model');
        $this->modulePath = $this->config->get('abandonmentcarts_path');
        $this->moduleVersion = $this->config->get('abandonmentcarts_version');
        $this->extensionsLink = $this->url->link($this->config->get('abandonmentcarts_link'), 'token=' . $this->session->data['token'] . $this->config->get('abandonmentcarts_link_params'), 'SSL');
        /* Fill Main Variables - End */

        // Load Model
        $this->load->model("module/abandonmentcarts");

        // Model Instance
        $this->moduleModel = $this->{$this->callModel};

        // Multi-Store
        $this->load->model('setting/store');
        // Settings
        $this->load->model('setting/setting');
        // Multi-Lingual
        $this->load->model('localisation/language');

        // Languages
        $this->language->load($this->modulePath);
        $language_strings = $this->language->load($this->modulePath);
        foreach ($language_strings as $code => $languageVariable) {
            $this->data[$code] = $languageVariable;
        }

        // Variables
        $this->data['moduleName'] = $this->moduleName;
        $this->data['modulePath'] = $this->modulePath;
    }

    // Main controller
    public function index() {
        // Title
        $this->document->setTitle($this->language->get('heading_title2') . ' ' . $this->moduleVersion);

        // Styles		
        $this->document->addStyle('view/stylesheet/' . $this->moduleName . '.css');

        // Scripts
        $this->document->addScript('view/javascript/' . $this->moduleName . '/' . $this->moduleName . '.js');
        $this->document->addScript('view/javascript/' . $this->moduleName . '/cron.js');
        $this->document->addScript('view/javascript/' . $this->moduleName . '/nprogress.js');

        // Store data
        if (!isset($this->request->get['store_id'])) {
            $this->request->get['store_id'] = 0;
        }

        $store = $this->getCurrentStore($this->request->get['store_id']);

        // New fields
        $check_update = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "" . $this->moduleName . "` LIKE 'notified'");
        if (!$check_update->rows) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "" . $this->moduleName . "` ADD `notified` SMALLINT NOT NULL DEFAULT 0");
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "" . $this->moduleName . "` ADD `ordered` TINYINT NOT NULL DEFAULT 0");
        }
        $check_update = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "" . $this->moduleName . "` LIKE 'country_id'");
        if (!$check_update->rows) {
            $this->db->query("ALTER TABLE `" . DB_PREFIX . "" . $this->moduleName . "` ADD `country_id` INT NOT NULL DEFAULT 0");
        }

        // Saving data		
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            if (!empty($this->request->post['OaXRyb1BhY2sgLSBDb21'])) {
                $this->request->post[$this->moduleName]['LicensedOn'] = $this->request->post['OaXRyb1BhY2sgLSBDb21'];
            }
            if (!empty($this->request->post['cHRpbWl6YXRpb24ef4fe'])) {
                $this->request->post[$this->moduleName]['License'] = json_decode(base64_decode($this->request->post['cHRpbWl6YXRpb24ef4fe']), true);
            }
            $store = $this->getCurrentStore($this->request->post['store_id']);

            $this->model_setting_setting->editSetting($this->moduleName, $this->request->post, $this->request->post['store_id']);
            $this->session->data['success'] = $this->language->get('text_success');

            if ($this->request->post[$this->moduleName]["ScheduleEnabled"] == 'yes') {
                $this->editCron($this->request->post, $store['store_id']);
            }

            $this->response->redirect($this->url->link($this->modulePath, 'token=' . $this->session->data['token'] . '&store_id=' . $store['store_id'], 'SSL'));
        }

        // Sucess & Error messages
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        $this->data['heading_title'] = $this->language->get('heading_title2') . ' ' . $this->moduleVersion;

        // Breadcrumbs
        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->extensionsLink,
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title2'),
            'href' => $this->url->link($this->modulePath, 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        // Variables
        $this->data['currency'] = $this->config->get('config_currency');
        $this->data['stores'] = array_merge(array(0 => array('store_id' => '0', 'name' => $this->config->get('config_name') . '&nbsp;' . $this->data['text_default'] . '', 'url' => HTTP_SERVER, 'ssl' => HTTPS_SERVER)), $this->model_setting_store->getStores());
        $languages = $this->model_localisation_language->getLanguages();
        $this->data['languages'] = $languages;
        $firstLanguage = array_shift($languages);
        $this->data['firstLanguageCode'] = $firstLanguage['code'];
        $this->data['store'] = $store;
        $this->data['action'] = $this->url->link($this->modulePath, 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->extensionsLink;
        $this->data['moduleSettings'] = $this->model_setting_setting->getSetting($this->moduleName, $store['store_id']);
        $this->data['moduleData'] = (isset($this->data['moduleSettings'][$this->moduleName])) ? $this->data['moduleSettings'][$this->moduleName] : array();
        $this->data['usedCoupons'] = $this->moduleModel->getTotalUsedCoupons();
        $this->data['givenCoupons'] = $this->moduleModel->getTotalGivenCoupons();
        $this->data['registeredCustomers'] = $this->moduleModel->getTotalRegisteredCustomers();
        $this->data['totalCustomers'] = $this->moduleModel->getTotalCustomers();
        $this->data['mostVisitedPages'] = $this->moduleModel->getMostVisitedPages();
        $this->data['token'] = $this->session->data['token'];
        $this->data['e_mail'] = $this->config->get('config_email');

        $this->data['cronPhpPath'] = '0 0 * * * ';
        if (function_exists('shell_exec') && trim(shell_exec('echo EXEC')) == 'EXEC') {
            $this->data['cronPhpPath'] .= shell_exec("which php") . ' ';
        } else {
            $this->data['cronPhpPath'] .= 'php ';
        }
        $this->data['cronPhpPath'] .= dirname(DIR_APPLICATION) . '/vendors/' . $this->moduleName . '/sendReminder.php';

        // Template data
        $this->data['header'] = $this->load->controller('common/header');
        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['footer'] = $this->load->controller('common/footer');

        // Template load
        $this->response->setOutput($this->load->view($this->modulePath . '/' . $this->moduleName . '.tpl', $this->data));
    }

    // Get mail template data
    public function get_mailtemplate_settings() {
        $this->data['currency'] = $this->config->get('config_currency');
        $this->data['languages'] = $this->model_localisation_language->getLanguages();
        $this->data['mailtemplate']['id'] = $this->request->get['mailtemplate_id'];
        $store_id = $this->request->get['store_id'];
        $this->data['data'] = $this->model_setting_setting->getSetting($this->moduleName, $store_id);
        $this->data['moduleName'] = $this->moduleName;
        $this->data['moduleData'] = (isset($this->data['data'][$this->moduleName])) ? $this->data['data'][$this->moduleName] : array();
        $this->data['newAddition'] = true;

        $this->response->setOutput($this->load->view($this->modulePath . '/tab_mailtab.tpl', $this->data));
    }

    // Validation against users with no permissions	
    protected function validateForm() {
        if (!$this->user->hasPermission('modify', $this->modulePath)) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        return !$this->error;
    }

    // Function that edits the cron jobs
    private function editCron($data = array(), $store_id) {
        return;
        if (function_exists('shell_exec') && trim(shell_exec('echo EXEC')) == 'EXEC') {
            $phpPath = str_replace(PHP_EOL, '', shell_exec("which php"));
        } else {
            $phpPath = 'php';
        }

        $cronCommands = array();
        $cronFolder = dirname(DIR_APPLICATION) . '/vendors/' . $this->moduleName . '/';
        $dateForSorting = array();

        if (isset($data[$this->moduleName]["ScheduleType"]) && $data[$this->moduleName]["ScheduleType"] == 'F') {
            if (isset($data[$this->moduleName]["FixedDates"])) {
                foreach ($data[$this->moduleName]["FixedDates"] as $date) {
                    $buffer = explode('/', $date);
                    $bufferDate = explode('.', $buffer[0]);
                    $bufferTime = explode(':', $buffer[1]);
                    $cronCommands[] = (int) $bufferTime[1] . ' ' . (int) $bufferTime[0] . ' ' . (int) $bufferDate[0] . ' ' . (int) $bufferDate[1] . ' * ' . $phpPath . ' ' . $cronFolder . 'sendReminder.php';
                    $dateForSorting[] = $bufferDate[2] . '.' . $bufferDate[1] . '.' . $bufferDate[0] . '.' . $buffer[1];
                }
                asort($dateForSorting);
                $sortedDates = array();
                foreach ($dateForSorting as $date) {
                    $newDate = explode('.', $date);
                    $sortedDates[] = $newDate[2] . '.' . $newDate[1] . '.' . $newDate[0] . '/' . $newDate[3];
                }
                $data = $sortedDates;
            }
        }
        if (isset($data[$this->moduleName]["ScheduleType"]) && $data[$this->moduleName]["ScheduleType"] == 'P') {
            $cronCommands[] = $data[$this->moduleName]['PeriodicCronValue'] . ' ' . $phpPath . ' ' . $cronFolder . 'sendReminder.php';
        }
        if (isset($cronCommands)) {
            $cronCommands = implode(PHP_EOL, $cronCommands);
            $currentCronBackup = shell_exec('crontab -l');
            $currentCronBackup = explode(PHP_EOL, $currentCronBackup);
            foreach ($currentCronBackup as $key => $command) {
                if (strpos($command, $phpPath . ' ' . $cronFolder . 'sendReminder.php') || empty($command)) {
                    unset($currentCronBackup[$key]);
                }
            }
            $currentCronBackup = implode(PHP_EOL, $currentCronBackup);
            file_put_contents($cronFolder . 'cron.txt', $currentCronBackup . PHP_EOL . $cronCommands . PHP_EOL);
            exec('crontab -r');
            exec('crontab ' . $cronFolder . 'cron.txt');
        }
    }

    // Gets catalog url
    private function getCatalogURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_CATALOG;
        } else {
            $storeURL = HTTP_CATALOG;
        }
        return $storeURL;
    }

    // Install module
    public function install() {
        $this->moduleModel->install();
    }

    // Uninstall module
    public function uninstall() {
        $this->model_setting_setting->deleteSetting($this->moduleName, 0);
        $stores = $this->model_setting_store->getStores();
        foreach ($stores as $store) {
            $this->model_setting_setting->deleteSetting($this->moduleName, $store['store_id']);
        }
        $this->moduleModel->uninstall();
    }

    // Get the current abandonment carts
    public function getabandonmentcarts() {
        if (!empty($this->request->get['page'])) {
            $page = (int) $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (!isset($this->request->get['store_id'])) {
            $this->request->get['store_id'] = 0;
        }

        if (isset($this->request->get['notified']) && $this->request->get['notified'] == '1') {
            $this->data['notified'] = true;
            $this->data['forFilter'] = 'notified';
        } else {
            $this->data['notified'] = false;
            $this->data['forFilter'] = 'default';
        }

        if (isset($this->request->get['ordered']) && $this->request->get['ordered'] == '1') {
            $this->data['ordered'] = true;
            $this->data['forFilter'] = 'ordered';
        } else {
            $this->data['ordered'] = false;
        }

        $url_link = '&store_id=' . $this->request->get['store_id'];
        $this->data['filterURL'] = $this->url->link($this->modulePath . '/getabandonmentcarts', 'token=' . $this->session->data['token'] . $url_link, 'SSL');

        $this->load->model('tool/image');

        if (VERSION >= '2.1.0.1') {
            $this->load->model('customer/customer');
            $this->data['model_sale_customer'] = $this->model_customer_customer;
        } else {
            $this->load->model('sale/customer');
            $this->data['model_sale_customer'] = $this->model_sale_customer;
        }

        $this->data['model_tool_image'] = $this->model_tool_image;
        $this->data['thiscurrency'] = $this->currency;
        $this->data['config_currency'] = $this->config->get('config_currency');
        $this->data['thisurl'] = $this->url;
        $this->data['store_id'] = $this->request->get['store_id'];
        $this->data['token'] = $this->session->data['token'];
        $this->data['limit'] = 8; // $this->config->get('config_limit_admin')
        $this->data['total'] = $this->moduleModel->getTotalAbandonmentCarts($this->data['store_id'], $this->data['notified'], $this->data['ordered']);
        $moduleSettings = $this->model_setting_setting->getSetting($this->moduleName, $this->data['store_id']);
        $moduleSettings = (isset($moduleSettings[$this->moduleName])) ? $moduleSettings[$this->moduleName] : array();

        $this->data['usable_templates'] = array();
        if (isset($moduleSettings['MailTemplate']) && sizeof($moduleSettings['MailTemplate']) > 0) {
            foreach ($moduleSettings['MailTemplate'] as $template) {
                if (isset($template['Enabled']) && $template['Enabled'] == 'yes') {
                    $this->data['usable_templates'][$template['id']] = $template['Name'];
                }
            }
        }

        if (sizeof($this->data['usable_templates']) == 0) {
            $this->data['usable_templates'][0] = 'No active template!';
        }

        $pagination = new Pagination();
        $pagination->total = $this->data['total'];
        $pagination->page = $page;
        $pagination->limit = $this->data['limit'];
        $pagination->url = $this->url->link($this->modulePath . '/getabandonmentcarts', 'token=' . $this->session->data['token'] . '&page={page}&store_id=' . $this->data['store_id'] . '&notified=' . $this->data['notified'] . '&ordered=' . $this->data['ordered'], 'SSL');
        $this->data['pagination'] = $pagination->render();
        $this->data['sources'] = $this->moduleModel->viewAbandonmentCarts($page, $this->data['limit'], $this->data['store_id'], $this->data['notified'], $this->data['ordered']);

        $this->data['results'] = sprintf($this->language->get('text_pagination'), ($this->data['total']) ? (($page - 1) * $this->data['limit']) + 1 : 0, ((($page - 1) * $this->data['limit']) > ($this->data['total'] - $this->data['limit'])) ? $this->data['total'] : ((($page - 1) * $this->data['limit']) + $this->data['limit']), $this->data['total'], ceil($this->data['total'] / $this->data['limit']));

        $this->response->setOutput($this->load->view($this->modulePath . '/view_abandonmentcarts.tpl', $this->data));
    }

    // Send reminder popup show
    public function sendreminder() {
        $this->load->model('tool/image');
        $this->data['newAddition'] = true;
        $languages = $this->model_localisation_language->getLanguages();
        $this->data['languages'] = $languages;
        $firstLanguage = array_shift($languages);
        $this->data['firstLanguageCode'] = $firstLanguage['code'];
        $this->data['data'] = $this->model_setting_setting->getSetting($this->moduleName, $this->request->get['store_id']);
        $this->data['id'] = $this->request->get['id'];
        $this->data['store_id'] = $this->request->get['store_id'];
        $this->data['currency'] = $this->config->get('config_currency');
        $this->data['result'] = $this->moduleModel->getCartInfo($this->data['id']);
        $this->data['result']['customer_info'] = json_decode($this->data['result']['customer_info'], true);
        $this->data['country_code'] = $this->data['result']['iso_code_2'];
        $this->data['curr'] = $this->data['result']['curcode'];
        $this->data['language_code'] = $this->data['result']['customer_info']['language'];
        $this->data['utm'] = '&utm_source=mall&utm_campaign=browsingabandonment&utm_medium=email';
        $this->data['language_id'] = $this->moduleModel->getLanguageId($this->data['result']['customer_info']['language']);
        $this->data['mailtemplate']['id'] = $this->request->get['template_id'];
        $this->data['moduleData'] = (isset($this->data['data'][$this->moduleName])) ? $this->data['data'][$this->moduleName] : array();
        $this->response->setOutput($this->load->view($this->modulePath . '/send_reminder.tpl', $this->data));
    }

    // Send manual email to the customers
    public function sendcustomemail() {
        if (isset($this->request->post) && isset($this->request->post['ABcart_id']) && isset($this->request->post['AB_template_id'])) {
            $this->load->model('marketing/coupon');
            $this->load->model('tool/image');
            $this->load->model('catalog/product');

            /* require_once(DIR_SYSTEM.'library/tax.php');
              require_once(DIR_SYSTEM.'library/customer.php'); */

            $result = $this->moduleModel->getCartInfo($this->request->post['ABcart_id']);
            $this->request->get['store_id'] = $result['store_id'];
            $template_id = $this->request->post['AB_template_id'];
            $language_id = $this->request->post['AB_language_id'];

            $setting = $this->model_setting_setting->getSetting($this->moduleName, $result['store_id']);
            $moduleData = (isset($setting[$this->moduleName])) ? $setting[$this->moduleName] : array();
            $settingTemplate = $this->request->post[$this->moduleName]['MailTemplate'][$template_id];
            $result['customer_info'] = json_decode($result['customer_info'], true);
            $Message = html_entity_decode($settingTemplate['Message'][$language_id]);




            $width = $settingTemplate['ProductWidth'];
            $height = $settingTemplate['ProductHeight'];
            //$language_id = $this->moduleModel->getLanguageId($result['customer_info']['language']);
            $result['cart'] = $this->moduleModel->getCustomerAbandonedments($result['customer_info']['id'],$language_id);//json_decode($result['cart'], true);


            $this->load->model('localisation/language');

            $language_info = $this->model_localisation_language->getLanguage($language_id);
            if ($language_info) {
                $language_directory = $language_info['directory'];
                $language = new Language($language_directory);
                $language->load($language_directory);
                $language->load('mail/customer');
                $shopNowButton = $language->get('cat_shop_now');
            }else{
                $shopNowButton = ($language_id == 2) ?  'تسوق الان' : 'Shop Now';
            }
            if(!$result['cart'] || !is_array($result['cart']) || count($result['cart']) < 1){
                echo "Invalid Cart Content";
                return;
            }
            $country_code = strtolower($result['iso_code_2']);
            $curr = $result['curcode'];
            $language_code = strtolower($result['customer_info']['language']);
            $store_data = $this->getCurrentStore($result['store_id']);
            $catalog_link = $store_data['url'];

            $CartProducts = '<table style="width:100%">';
            foreach ($result['cart'] as $product) {
                if ($product['image']) {
                    $image_thumb = $this->model_tool_image->getImageS3($product['image'], $width, $height);
                } else {
                    $image = false;
                }
                $CartProducts .='<tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="100%">
                                 <tr> 
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center" valign="top"> ';
                                                    $CartProducts .='<a href="' . $catalog_link . "{$country_code}" . ($language_code ? "/{$language_code}" : "") . '/products/' . $product['product_id'] . '&utm_source=mall&utm_campaign=browsingabandonment&utm_medium=email" target="_blank"><img src="' . str_replace(' ', '%20', $image_thumb) . '" width="250" /></a></td></tr><tr><td align="center" valign="top"><p style="font-family:Arial,  Helvetica, sans-serif; font-size: 14px; text-align: center">' . $product['name'] . '</p>';
                /*  foreach ($product['option'] as $option) {
                  $CartProducts .= '- <small>' . $option['name'] . ' ' . $option['value'] . '</small><br />';
                  } */
                if ($moduleData['Taxes'] == 'yes') {
                    if (VERSION < '2.2.0.0') {
                        global $registry;
                        $registry->set('customer', new Customer($registry));
                        $registry->set('tax', new Tax($registry));
                    }
                    $product_info = $this->model_catalog_product->getProduct($product['product_id']);
                    $this->customer->login($result['customer_info']['email'], '123', true);
                    $price = $this->tax->calculate($product['price'], $product_info['tax_class_id'], $this->config->get('config_tax'));
                    $this->customer->logout();
                } else {
                    $price = $product['price'];
                }
                $price = $this->currency->format($price, $curr);
                if(isset($product['special']) && $product['special']!= ''){
                    $dis = $this->currency->format($product['special'], $curr);
                    $price = "<span style='text-decoration: line-through;color: #adadad;'>$price</span> <span style='color: #e12415;'>$dis</span>";
                }
                //
                $CartProducts .= '<p style="font-family:Arial,  Helvetica, sans-serif; font-size: 14px; text-align: center;margin-bottom: 10px;">' .$price.'</p></td></tr>
                <tr><td style="padding-bottom:15px">
                <p style="text-align: center; padding: 10px 0; margin-top: 0">
                <a href="' . $catalog_link . "{$country_code}" . ($language_code ? "/{$language_code}" : "") . '/products/' . $product['product_id'] . '&utm_source=mall&utm_campaign=browsingabandonment&utm_medium=email" target="_blank" style="background: #000; color: white; padding: 10px 20px; border-radius: 5px; font-family:Arial,  Helvetica, sans-serif; font-size: 17px; text-decoration: none;">'.$shopNowButton.'</a>
                </p></td></tr></table></td></tr>';
            }
            $CartProducts .='</table>';

            if ($settingTemplate['DiscountType'] == 'N') {
                // do nothing here
            } else {
                if ($settingTemplate['DiscountApply'] == 'all_products') {
                    $DiscountCode = $this->moduleModel->generateuniquerandomcouponcode();
                    $TimeEnd = time() + $settingTemplate['DiscountValidity'] * 24 * 60 * 60;
                    $CouponData = array('name' => 'AbCart [' . $result['customer_info']['email'] . ']',
                        'code' => $DiscountCode,
                        'discount' => $settingTemplate['Discount'],
                        'type' => $settingTemplate['DiscountType'],
                        'total' => $settingTemplate['TotalAmount'],
                        'logged' => $settingTemplate['DiscountCustomerLogin'],
                        'shipping' => $settingTemplate['DiscountShipping'],
                        'date_start' => date('Y-m-d', time()),
                        'date_end' => date('Y-m-d', $TimeEnd),
                        'uses_total' => '1',
                        'uses_customer' => '1',
                        'status' => '1');
                    $this->model_marketing_coupon->addCoupon($CouponData);
                } else if ($settingTemplate['DiscountApply'] == 'cart_products') {
                    $cart_products = array();
                    foreach ($result['cart'] as $product) {
                        $cart_products[] = $product['product_id'];
                    }
                    $DiscountCode = $this->moduleModel->generateuniquerandomcouponcode();
                    $TimeEnd = time() + $settingTemplate['DiscountValidity'] * 24 * 60 * 60;
                    $CouponData = array('name' => 'AbCart [' . $result['customer_info']['email'] . ']',
                        'code' => $DiscountCode,
                        'discount' => $settingTemplate['Discount'],
                        'type' => $settingTemplate['DiscountType'],
                        'total' => $settingTemplate['TotalAmount'],
                        'logged' => $settingTemplate['DiscountCustomerLogin'],
                        'shipping' => $settingTemplate['DiscountShipping'],
                        'coupon_product' => $cart_products,
                        'date_start' => date('Y-m-d', time()),
                        'date_end' => date('Y-m-d', $TimeEnd),
                        'uses_total' => '1',
                        'uses_customer' => '1',
                        'status' => '1');
                    $this->model_marketing_coupon->addCoupon($CouponData);
                }
            }

            $patterns = array();
            $patterns[0] = '{firstname}';
            $patterns[1] = '{lastname}';
            $patterns[2] = '{cart_content}';
            if (!($settingTemplate['DiscountType'] == 'N')) {
                $patterns[3] = '{discount_code}';
                $patterns[4] = '{discount_value}';
                $patterns[5] = '{total_amount}';
                $patterns[6] = '{date_end}';
            }
            $patterns[7] = '{unsubscribe_link}';
            $replacements = array();
            $replacements[0] = $result['customer_info']['firstname'];
            $replacements[1] = $result['customer_info']['lastname'];
            $replacements[2] = $CartProducts;
            if (!($settingTemplate['DiscountType'] == 'N')) {
                $replacements[3] = $DiscountCode;
                $replacements[4] = $settingTemplate['Discount'];
                $replacements[5] = $settingTemplate['TotalAmount'];
                $replacements[6] = date($moduleData['DateFormat'], $TimeEnd);
            }
            $replacements[7] = '<a href="' . $this->getCatalogURL() . 'index.php?route=module/' . $this->moduleName . '/removeCart&id=' . $this->request->post['ABcart_id'] . '">' . $this->language->get('text_unsubscribe') . '</a>';
            $HTMLMail = str_replace($patterns, $replacements, $Message);

            $patterns_subject = array();
            $patterns_subject[0] = '{firstname}';
            $patterns_subject[1] = '{lastname}';
            $replacements = array();
            $replacements_subject[0] = $result['customer_info']['firstname'];
            $replacements_subject[1] = $result['customer_info']['lastname'];
            $HTMLSubject = str_replace($patterns_subject, $replacements_subject, $settingTemplate['Subject'][$language_id]);

            $MailData = array(
                'email' => $result['customer_info']['email'],
                'message' => $HTMLMail,
                'subject' => $HTMLSubject,
                'store_id' => $result['store_id'],
                'store_email' => $store_data['store_email']
            );

            $emailResult = $this->moduleModel->sendMail($MailData);
            $run_query = $this->db->query("UPDATE `" . DB_PREFIX . "" . $this->moduleName . "` SET notified = (notified + 1) WHERE `id`=" . (int) $this->request->post['ABcart_id']);
            if ($emailResult) {
                echo "The email is sent successfully.";
                if (isset($settingTemplate['RemoveAfterSend']))
                    $run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "" . $this->moduleName . "` WHERE `id`=" . (int) $this->request->post['ABcart_id']);
            } else {
                echo "There is an error with the provided data in the form below.";
            }
        } else {
            echo "There is an error with the provided data in the form below.";
        }
    }

    // Function that tests cron functionality
    public function testcron() {
        return;
        if (function_exists('shell_exec') && trim(shell_exec('echo EXEC')) == 'EXEC') {
            $this->data['shell_exec_status'] = 'Enabled';
        } else {
            $this->data['shell_exec_status'] = 'Disabled';
        }
        if ($this->data['shell_exec_status'] == 'Enabled') {
            $cronFolder = dirname(DIR_APPLICATION) . '/vendors/' . $this->moduleName . '/';
            if (shell_exec('crontab -l')) {
                $this->data['cronjob_status'] = 'Enabled';
                $curentCronjobs = shell_exec('crontab -l');
                $this->data['current_cron_jobs'] = explode(PHP_EOL, $curentCronjobs);
                file_put_contents($cronFolder . 'cron.txt', '* * * * * echo "test" ' . PHP_EOL);
            } else {
                file_put_contents($cronFolder . 'cron.txt', '* * * * * echo "test" ' . PHP_EOL);
                if (file_exists($cronFolder . 'cron.txt')) {
                    exec('crontab ' . $cronFolder . 'cron.txt');
                    if (shell_exec('crontab -l')) {
                        $this->data['cronjob_status'] = 'Enabled';
                        shell_exec('crontab -r');
                    } else {
                        $this->data['cronjob_status'] = 'Disabled';
                    }
                }
            }
            if (file_exists($cronFolder . 'cron.txt')) {
                $this->data['folder_permission'] = "Writable";
                unlink($cronFolder . 'cron.txt');
            } else {
                $this->data['folder_permission'] = "Unwritable";
            }
        }

        $this->data['cron_folder'] = $cronFolder;
        $this->data['token'] = $this->session->data['token'];

        $this->response->setOutput($this->load->view($this->modulePath . '/test_cron.tpl', $this->data));
    }

    // Remove all records
    public function removeallrecords() {
        if (isset($this->request->post['remove']) && ($this->request->post['remove'] == true) && isset($this->request->post['store'])) {
            $run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "" . $this->moduleName . "` WHERE `store_id`='" . $this->request->post['store'] . "'");
            if ($run_query)
                echo "Success!";
        }
    }

    // Remove all expired coupons
    public function removeallexpiredcoupons() {
        $date_end = date('Y-m-d', time() - 60 * 60 * 24);
        if (isset($this->request->post['remove']) && ($this->request->post['remove'] == true)) {
            $run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "coupon` WHERE `name` LIKE '%AbCart [%' AND `date_end`<='" . $date_end . "'");
            if ($run_query)
                echo "Success!";
        }
    }

    // Remove all empty records
    public function removeallemptyrecords() {
        if (isset($this->request->post['remove']) && ($this->request->post['remove'] == true) && isset($this->request->post['store'])) {
            $run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "" . $this->moduleName . "` WHERE `store_id`='" . $this->request->post['store'] . "' AND `customer_info` NOT LIKE '%email%'");
            if ($run_query)
                echo "Success!";
        }
    }

    // Remove single record
    public function removeabandonmentcart() {
        if (isset($this->request->post['cart_id'])) {
            $run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "" . $this->moduleName . "` WHERE `id`=" . (int) $this->request->post['cart_id']);
            if ($run_query)
                echo "Success!";
        }
    }

    // Get current store
    private function getCurrentStore($store_id) {
        if ($store_id && $store_id != 0) {
            $store = $this->model_setting_store->getStore($store_id);
            $store['store_email'] = $this->db->query("SELECT `value` FROM `" . DB_PREFIX . "setting` WHERE `key`= 'config_email' AND `store_id`=" . $this->db->escape($store_id))->row['value'];
        } else {
            $store['store_id'] = 0;
            $store['name'] = $this->config->get('config_name');
            $store['url'] = $this->getCatalogURL();
            $store['store_email'] = $this->config->get('config_email');
        }
        return $store;
    }

    // Show given coupons
    public function givenCoupons() {
        $this->listCoupons('givenCoupons');
    }

    // Show used coupons
    public function usedCoupons() {
        $this->listCoupons('usedCoupons');
    }

    // Function to list the coupon codes
    private function listCoupons($action) {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }
        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['givenCoupons'] = array();

        $dataInfo = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * 15,
            'limit' => 15
        );

        if ($action == 'usedCoupons') {
            $coupon_total = $this->moduleModel->getTotalUsedCoupons();
            $coupons = $this->moduleModel->getUsedCoupons($dataInfo);
        } else {
            $coupon_total = $this->moduleModel->getTotalGivenCoupons();
            $coupons = $this->moduleModel->getGivenCoupons($dataInfo);
        }

        if (!empty($coupons)) {
            foreach ($coupons as $coupon) {
                $this->data['coupons'][] = array(
                    'coupon_id' => $coupon['coupon_id'],
                    'name' => $coupon['name'],
                    'code' => $coupon['code'],
                    'discount' => $coupon['discount'],
                    'date_start' => date($this->language->get('date_format_short'), strtotime($coupon['date_start'])),
                    'date_end' => date($this->language->get('date_format_short'), strtotime($coupon['date_end'])),
                    'status' => ($coupon['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
                    'date_added' => $coupon['date_added']
                );
            }
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';
        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['sort_name'] = 'index.php?route=' . $this->modulePath . '/' . $action . '&token=' . $this->session->data['token'] . '&sort=name' . $url;
        $this->data['sort_code'] = 'index.php?route=' . $this->modulePath . '/' . $action . '&token=' . $this->session->data['token'] . '&sort=code' . $url;
        $this->data['sort_discount'] = 'index.php?route=' . $this->modulePath . '/' . $action . '&token=' . $this->session->data['token'] . '&sort=discount' . $url;
        $this->data['sort_date_start'] = 'index.php?route=' . $this->modulePath . '/' . $action . '&token=' . $this->session->data['token'] . '&sort=date_start' . $url;
        $this->data['sort_date_end'] = 'index.php?route=' . $this->modulePath . '/' . $action . '&token=' . $this->session->data['token'] . '&sort=date_end' . $url;
        $this->data['sort_status'] = 'index.php?route=' . $this->modulePath . '/' . $action . '&token=' . $this->session->data['token'] . '&sort=status' . $url;
        $this->data['sort_email'] = 'index.php?route=' . $this->modulePath . '/' . $action . '&token=' . $this->session->data['token'] . '&sort=email' . $url;
        $this->data['sort_discount_type'] = 'index.php?route=' . $this->modulePath . '/' . $action . '&token=' . $this->session->data['token'] . '&sort=discount_type' . $url;
        $this->data['sort_date_added'] = 'index.php?route=' . $this->modulePath . '/' . $action . '&token=' . $this->session->data['token'] . '&sort=date_added' . $url;

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $this->data['total'] = $coupon_total;
        $this->data['limit'] = 15;
        $pagination = new Pagination();
        $pagination->total = $this->data['total'];
        $pagination->page = $page;
        $pagination->limit = $this->data['limit'];
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = 'index.php?route=' . $this->modulePath . '/' . $action . '&token=' . $this->session->data['token'] . $url . '&page={page}';
        $this->data['pagination'] = $pagination->render();
        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        $this->data['token'] = $this->session->data['token'];

        $this->data['results'] = sprintf($this->language->get('text_pagination'), ($this->data['total']) ? (($page - 1) * $this->data['limit']) + 1 : 0, ((($page - 1) * $this->data['limit']) > ($this->data['total'] - $this->data['limit'])) ? $this->data['total'] : ((($page - 1) * $this->data['limit']) + $this->data['limit']), $this->data['total'], ceil($this->data['total'] / $this->data['limit']));

        $this->response->setOutput($this->load->view($this->modulePath . '/coupon.tpl', $this->data));
    }

}
