<?php

class ControllerModuleAutomatedNewsletter extends Controller {

    private $moduleName;
    private $modulePath;
    private $moduleModel;
    private $moduleVersion;
    private $extensionsLink;
    private $callModel;
    private $error = array();
    private $data = array();
    private $progress;

    public function __construct($registry) {
        parent::__construct($registry);

        // Config Loader
        $this->config->load('isenselabs/automatednewsletter');

        // Module Constants
        $this->moduleName = $this->config->get('automatednewsletter_name');
        $this->moduleNameSmall = $this->config->get('automatednewsletter_name_small');
        $this->callModel = $this->config->get('automatednewsletter_model');
        $this->modulePath = $this->config->get('automatednewsletter_path');
        $this->moduleVersion = $this->config->get('automatednewsletter_version');
        $this->moduleData_module = $this->config->get('automatednewsletter_module_data');
        $this->extensionsLink = $this->url->link($this->config->get('automatednewsletter_link'), 'token=' . $this->session->data['token'] . $this->config->get('automatednewsletter_link_params'), 'SSL');

        // Load Language
        $this->load->language($this->modulePath);

        // Load Model
        $this->load->model($this->modulePath);

        // Model Instance
        $this->moduleModel = $this->{$this->callModel};


        // Global Variables      
        $this->data['moduleName'] = $this->moduleName;
        $this->data['moduleNameSmall'] = $this->moduleNameSmall;
        $this->data['modulePath'] = $this->modulePath;
        $this->data['feedPath'] = $this->feedPath;
        $this->data['moduleData_module'] = $this->moduleData_module;
        $this->data['moduleModel'] = $this->moduleModel;

        $this->data['limit'] = 15;
    }

    public function index() {

        if (VERSION < '2.1.0.0') {
            $this->load->model('sale/customer_group');
        } else {
            $this->load->model('customer/customer_group');
        }
        $this->load->model('setting/store');
        $this->load->model('setting/setting');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('localisation/currency');
        $this->load->model('catalog/manufacturer');


        $catalogURL = $this->getCatalogURL();
        if (!isset($this->request->get['store_id'])) {
            $this->request->get['store_id'] = 0;
        }

        $store = $this->getCurrentStore($this->request->get['store_id']);
        $this->document->setTitle($this->language->get('heading_title') . ' ' . $this->moduleVersion);
        $this->document->addScript('view/javascript/' . $this->moduleNameSmall . '/cron.js');
        $this->document->addScript('view/javascript/' . $this->moduleNameSmall . '/main.js');
        $this->document->addStyle('view/javascript/' . $this->moduleNameSmall . '/font-awesome/css/font-awesome.min.css');
        $this->document->addStyle('view/stylesheet/' . $this->moduleNameSmall . '.css');
        $this->document->addScript($catalogURL . 'catalog/view/theme/sayidaty/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle($catalogURL . 'catalog/view/theme/sayidaty/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $this->upgradeIfNeeded();

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

            if (!$this->user->hasPermission('modify', $this->modulePath)) {
                $this->response->redirect($this->extensionsLink);
            }
            if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
                $this->request->post[$this->moduleName]['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
            }
            if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
                $this->request->post[$this->moduleName]['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']), true);
            }
            $store = $this->getCurrentStore($this->request->post['store_id']);
            $newsletter_receiver = $this->request->post[$this->moduleName]['newsletter_receiver'];

            if (!empty($newsletter_receiver) && ($newsletter_receiver == 'newsletter' || $newsletter_receiver == 'customer_all' || $newsletter_receiver == 'affiliate_all')) {
                $this->request->post[$this->moduleName]['to'] = NULL;
            } elseif ($newsletter_receiver == 'customer_group') {
                $this->request->post[$this->moduleName]['to'] = $this->request->post[$this->moduleName]['to']['customer_group_id'];
            } elseif ($newsletter_receiver == 'customer') {
                if (isset($this->request->post[$this->moduleName]['to']['customer'])) {
                    $this->request->post[$this->moduleName]['to'] = $this->request->post[$this->moduleName]['to']['customer'];
                }
            } elseif ($newsletter_receiver == 'affiliate') {
                if (isset($this->request->post[$this->moduleName]['to']['affiliate'])) {
                    $this->request->post[$this->moduleName]['to'] = $this->request->post[$this->moduleName]['to']['affiliate'];
                }
            } elseif ($newsletter_receiver == 'product') {
                if (isset($this->request->post[$this->moduleName]['to']['product'])) {
                    $this->request->post[$this->moduleName]['to'] = $this->request->post[$this->moduleName]['to']['product'];
                }
            }
            if ($this->request->post[$this->moduleName]["scheduleEnabled"] == 'yes') {
                $this->editCron($this->request->post, $store['store_id']);
            }
            //save settings
            if (isset($this->request->post['template_id']) && isset($this->request->post['template'])) {
                $this->saveTemplate($this->request->post['template_id'], $this->request->post['template'], $store['store_id']);
            }
            $this->model_setting_setting->editSetting($this->moduleName, array($this->moduleName => $this->request->post[$this->moduleName]), $this->request->post['store_id']);
            $this->session->data['success'] = $this->language->get('text_success');
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        $this->data['breadcrumbs'] = array();
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' false'
        );
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->extensionsLink,
            'separator' => ' :: '
        );
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link($this->modulePath, 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        $languageVariables = array(
            'entry_code',
            'entry_code_help',
            'text_enabled',
            'text_disabled',
            'subject_text',
            'user_email',
            'custom_design',
            'default_notification',
            'custom_css',
            'specials_text',
            'deals_text',
            'new_products_text',
            'text_customer_group',
            'text_customer',
            'text_affiliate_all',
            'text_affiliate',
            'text_product',
            'all_manufacturers',
            'selected_manufacturers',
            'entry_store',
            'entry_to',
            'entry_customer_group',
            'entry_customer',
            'entry_customer_help',
            'entry_affiliate',
            'entry_affiliate_help',
            'entry_product',
            'entry_product_help',
            'entry_spec_product',
            'entry_spec_product_help',
            'entry_category',
            'entry_category_help_one',
            'entry_category_help_two',
            'text_newsletter',
            'text_customer_all',
            'schedule_type',
            'fixed_dates',
            'periodic',
            'cron_time',
            'cron_successfully_changed',
            'default_subject',
            'schedule_tasks_status',
            'best_deals',
            'best_deals_top',
            'new_products_for_last',
            'specials_for_next',
            'product_image_size',
            'product_image_size_help',
            'count_of_products_per_row',
            'entry_store',
            'entry_store_email',
            'entry_newsletter',
            'entry_name',
            'entry_email',
            'error_input_form',
            'text_default',
            'selected_products_list',
            'text_products',
            'text_days',
            'text_limit',
            'text_close',
            'text_sending',
            'text_preparing',
            'text_abort',
            'text_settings',
            'text_toggle',
            'text_list',
            'text_sent_newsletter',
            'text_tasks',
            'text_support',
            'text_save',
            'text_cancel',
            'text_from',
            'text_cron',
            'text_schedule_cron',
            'text_back',
            'text_delete',
            'text_send_test',
            'test_cron_button',
            'cron_requirements',
            'select_currency',
            'select_currency_help',
            'text_stock_statuses',
            'admin_notification',
            'admin_notification_help',
            'cron_services',
            'cron_services_help',
            'name',
            'link',
            'entry_num_emails_per_request',
            'entry_num_emails_per_request_help',
        );

        foreach ($languageVariables as $languageVariable) {
            $this->data[$languageVariable] = $this->language->get($languageVariable);
        }

        $this->data['heading_title'] = $this->language->get('heading_title') . ' ' . $this->moduleVersion;

        $this->data['current_template_id'] = $this->moduleModel->getMaxTemplateId();
        $this->data['error_warning'] = '';
        $this->data['store'] = $store;
        $this->data['stores'] = array_merge(array(0 => array('store_id' => '0', 'name' => $this->config->get('config_name') . ' (' . $this->data['text_default'] . ')', 'url' => HTTP_SERVER, 'ssl' => HTTPS_SERVER)), $this->model_setting_store->getStores());
        $this->data['store_name'] = $this->config->get('config_name');
        $this->data['token'] = $this->session->data['token'];
        $this->data['action'] = $this->url->link($this->modulePath, 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->extensionsLink;
        $this->data['currency'] = $this->config->get('config_currency');
        $this->data['data'] = $this->model_setting_setting->getSetting($this->moduleName, $store['store_id']);

        if (VERSION < '2.1.0.0') {
            $this->data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups(0);
        } else {
            $this->data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups(0);
        }

        $this->data['currencies'] = $this->model_localisation_currency->getCurrencies();
        $this->data['e_mail'] = $this->config->get('config_email');
        $this->data['language_id'] = $this->config->get('config_language_id');
        //$this->data['stock_statuses']         = $this->moduleModel->getStockStatuses();
        $this->data['customers'] = array();
        $this->data['templates'] = $this->moduleModel->getTemplates(array('sort' => 'subject', 'order' => 'ASC', 'start' => 0, 'limit' => 20), $store['store_id']);

        if (!empty($this->data['data'][$this->moduleName]['newsletter_receiver']) && isset($this->data['data'][$this->moduleName]['to'])) {
            $this->data['customers'] = $this->data['affiliates'] = $this->data['products'] = array();
            if ($this->data['data'][$this->moduleName]['newsletter_receiver'] == 'customer') {
                $this->data['customers'] = $this->moduleModel->getCustomers($this->data['data'][$this->moduleName]['to'], $store['store_id']);
            }
            if ($this->data['data'][$this->moduleName]['newsletter_receiver'] == 'affiliate') {
                $this->data['affiliates'] = $this->moduleModel->getAffiliates($this->data['data'][$this->moduleName]['to']);
            }
            if ($this->data['data'][$this->moduleName]['newsletter_receiver'] == 'product') {
                $this->data['products'] = $this->moduleModel->getSelectedProducts($this->data['data'][$this->moduleName]['to'], $store['store_id']);
            }
        }

        $ENS = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . "easynewslettersubscription'");
        $this->data['ENS'] = false;
        if (sizeof($ENS->rows) > 0) {
            $this->data['ENS'] = true;
        }


        if (isset($this->request->post['custom_product'])) {
            $products = $this->request->post['custom_product'];
        } elseif (isset($this->data['data'][$this->moduleName]['custom_product'])) {
            $products = $this->data['data'][$this->moduleName]['custom_product'];
        } else {
            $products = array();
        }
        $this->data['custom_product'] = array();

        $custom_product = array();

        foreach ($products as $product_id) {
            $language_id = 1;
            $product_data = $this->model_catalog_product->getProductDescriptions($product_id);
            $name = isset($product_data[$language_id]['name']) ? $product_data[$language_id]['name'] : '';
            $product_info = $this->model_catalog_product->getProduct($product_id);
            if ($product_info) {
                $custom_product[] = array(
                    'product_id' => $product_info['product_id'],
                    'name' => $name
                );
            }
        }

        if (isset($this->request->post['specific_manufacturer'])) {
            $manufacturers = $this->request->post['specific_manufacturer'];
        } elseif (isset($this->data['data'][$this->moduleName]['specific_manufacturer'])) {
            $manufacturers = $this->data['data'][$this->moduleName]['specific_manufacturer'];
        } else {
            $manufacturers = array();
        }
        $this->data['specific_manufacturer'] = array();

        $specific_manufacturer = array();
        foreach ($manufacturers as $manufacturer_id) {
            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);
            if ($manufacturer_info) {
                $specific_manufacturer[] = array(
                    'manufacturer_id' => $manufacturer_info['manufacturer_id'],
                    'name' => $manufacturer_info['name']
                );
            }
        }



        if (isset($this->request->post['custom_category'])) {
            $categories = $this->request->post['custom_category'];
        } elseif (isset($this->data['data'][$this->moduleName]['custom_category'])) {
            $categories = $this->data['data'][$this->moduleName]['custom_category'];
        } else {
            $categories = array();
        }
        $this->data['custom_category'] = array();
        $custom_category = array();
        foreach ($categories as $category_id) {
            $category_info = $this->model_catalog_category->getCategory($category_id);
            if ($category_info) {
                $custom_category[] = array(
                    'category_id' => $category_info['category_id'],
                    'name' => ($category_info['path'] ? $category_info['path'] . ' &gt; ' : '') . $category_info['name']
                );
            }
        }
        $this->data['data'][$this->moduleName]['custom_product'] = $custom_product;
        $this->data['data'][$this->moduleName]['custom_category'] = $custom_category;
        $this->data['data'][$this->moduleName]['specific_manufacturer'] = $specific_manufacturer;


//Cron

        $this->data['cron_url'] = $this->getCatalogURL() . 'index.php?route=' . $this->modulePath . '/sendNewsletter&secretWord=Su3ov2JJpgT3QGhysaUN&task_name=sendNewsletter';

        if (isset($this->data['data'][$this->moduleName]['scheduleEnabled']) && $this->data['data'][$this->moduleName]['scheduleEnabled'] == 'yes') {

            if (isset($this->data['data'][$this->moduleName]["scheduleType"]) && $this->data['data'][$this->moduleName]["scheduleType"] == 'P' && isset($this->data['data'][$this->moduleName]['scheduled_template_periodic'])) {
                $templates[] = array(
                    'id' => $this->data['data'][$this->moduleName]['scheduled_template_periodic'],
                );
            } elseif (isset($this->data['data'][$this->moduleName]["scheduleType"]) && $this->data['data'][$this->moduleName]["scheduleType"] == 'F' && isset($this->data['data'][$this->moduleName]["fixedDates"])) {
                if (isset($this->data['data'][$this->moduleName]["fixedDates"])) {
                    foreach ($this->data['data'][$this->moduleName]["fixedDates"] as $val) {
                        $explode = explode("~~", $val);
                        $templates[] = array(
                            'id' => $explode[1],
                            'name' => $explode[2],
                        );
                    }
                }
            }

            if (isset($this->request->post['store_id'])) {
                $store_id = $this->request->post['store_id'];
            } elseif (isset($this->request->get['store_id'])) {
                $store_id = $this->request->get['store_id'];
            } else {
                $store_id = 0;
            }

            $secretWord = "Su3ov2JJpgT3QGhysaUN";
            $task_name = 'sendNewsletter';

            if (isset($templates)) {
                foreach ($templates as $template) {
                    if (isset($template['name'])) {
                        $name = $template['name'];
                    } else {
                        $name = null;
                    }

                    $this->data['template_info'][] = array(
                        'link' => $this->getCatalogURL() . 'index.php?route=' . $this->modulePath . '/sendNewsletter&secretWord=' . $secretWord . '&store_id=' . $store_id . '&task_name=' . $task_name . '&template_id=' . $template['id'],
                        'name' => $name,
                        'id' => $template['id'],
                    );
                }
            }
        }

//Cron End

        $this->data['header'] = $this->load->controller('common/header');
        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view($this->modulePath . '.tpl', $this->data));
    }

    public function sentNewsletters() {
        $this->load->model('localisation/language');

        $this->data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->get['store_id'])) {
            $store_id = $this->request->get['store_id'];
        } else {
            $store_id = 0;
        }


        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'time_added';
        }
        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }


        $data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $this->data['newsletters'] = $this->moduleModel->getSentNewsletters($data, $store_id);

        $this->data['newsletters_customers'] = $this->moduleModel->getSentNewslettersCustomers($store_id);

        $this->data['token'] = $this->session->data['token'];

        $languageVariables = array(
            'newsletter_subject',
            'date_sent',
            'language',
            'recepients',
            'text_no_results',
            'text_delete',
            'text_back',
            'entry_name',
            'entry_email',
        );

        foreach ($languageVariables as $languageVariable) {
            $this->data[$languageVariable] = $this->language->get($languageVariable);
        }

        $url = '';
        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['store_id'] = $store_id;

        $this->data['sort_subject'] = $this->getServerURL() . 'index.php?route=' . $this->modulePath . '/sentNewsletters&token=' . $this->session->data['token'] . '&store_id=' . $store_id . '&sort=subject' . $url;
        $this->data['sort_time_added'] = $this->getServerURL() . 'index.php?route=' . $this->modulePath . '/sentNewsletters&token=' . $this->session->data['token'] . '&store_id=' . $store_id . '&sort=time_added' . $url;
        $this->data['sort_customers_count'] = $this->getServerURL() . 'index.php?route=' . $this->modulePath . '/sentNewsletters&token=' . $this->session->data['token'] . '&store_id=' . $store_id . '&sort=customers_count' . $url;
        $this->data['sort_language'] = $this->getServerURL() . 'index.php?route=' . $this->modulePath . '/sentNewsletters&token=' . $this->session->data['token'] . '&store_id=' . $store_id . '&sort=language_id' . $url;

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }


        $pagination = new Pagination();
        $pagination->total = $this->moduleModel->getTotalSentNewsletters($store_id);
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->text = $this->language->get('text_pagination');

        $pagination->url = $this->url->link($this->modulePath . '/sentNewsletters&token=' . $this->session->data['token'] . '&store_id=' . $store_id . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();
        $this->data['sort'] = $sort;
        $this->data['order'] = $order;

        $this->response->setOutput($this->load->view($this->modulePath . '/sent_newsletters.tpl', $this->data));
    }

    public function newsletterView() {
        //$this->load->model($this->moduleNameSmall);
        $newsletter = $this->moduleModel->getSentNewsletter($this->request->get['newsletter_id']);
        $this->data['newsletter'] = $newsletter;
        $this->data['token'] = $this->session->data['token'];


        $this->response->setOutput($this->load->view($this->modulePath . '/newsletter_view.tpl', $this->data));
    }

    public function testCron() {
        $cronFolder = '';
        $curentCronjobs = '';
        $this->data['shell_exec_status'] = $this->data['cronjob_status'] = 'Disabled';
        $this->data['folder_permission'] = 'Unwritable';
        $cronFolder = dirname(DIR_APPLICATION) . '/vendors/' . $this->moduleNameSmall . '/';

        if (function_exists('shell_exec') && trim(shell_exec('echo EXEC')) == 'EXEC') {
            $this->data['shell_exec_status'] = 'Enabled';
        }

        if ($this->data['shell_exec_status'] == 'Enabled') {
            if (shell_exec('crontab -l')) {
                $this->data['cronjob_status'] = 'Enabled';
                $curentCronjobs = shell_exec('crontab -l');
                $this->data['current_cron_jobs'] = explode(PHP_EOL, $curentCronjobs);
                file_put_contents($cronFolder . 'cron.txt', '* * * * * echo "test" ' . PHP_EOL);
            } else {
                file_put_contents($cronFolder . 'cron.txt', '* * * * * echo "test" ' . PHP_EOL);
                if (file_exists($cronFolder . 'cron.txt')) {
                    shell_exec('crontab ' . $cronFolder . 'cron.txt');
                    if (shell_exec('crontab -l')) {
                        $this->data['cronjob_status'] = 'Enabled';
                        shell_exec('crontab -r');
                    }
                }
            }
            if (file_exists($cronFolder . 'cron.txt')) {
                $this->data['folder_permission'] = "Writable";
                unlink($cronFolder . 'cron.txt');
            }
        }

        $this->data['cron_folder'] = $cronFolder;

        $this->response->setOutput($this->load->view($this->modulePath . '/testCron.tpl', $this->data));
    }

    public function deleteNewsletter() {
        if (isset($this->request->post['newsletters'])) {
            //$this->load->model($this->moduleNameSmall);
            $this->moduleModel->deleteNewsletter($this->request->post['newsletters']);
        }
        $this->response->redirect($this->url->link($this->modulePath . '/sentNewsletters', 'token=' . $this->session->data['token'] . '&store_id=' . $this->request->post['store_id'], 'SSL'));
    }

    private function editCron($data = array(), $store_id) {
        $cronCommands = array();
        $cronFolder = dirname(DIR_APPLICATION) . '/vendors/' . $this->moduleNameSmall . '/';
        $dateForSorting = array();
        if (isset($data[$this->moduleName]["scheduleType"]) && $data[$this->moduleName]["scheduleType"] == 'F') {
            if (isset($data[$this->moduleName]["fixedDates"])) {
                foreach ($data[$this->moduleName]["fixedDates"] as $val) {

                    $explode = explode("~~", $val);
                    $date = $explode[0];
                    $buffer = explode(' . ', $date);
                    $bufferDate = explode('/', $buffer[0]);
                    $bufferTime = explode(':', $buffer[1]);
                    $cronCommands[] = (int) $bufferTime[1] . ' ' . (int) $bufferTime[0] . ' ' . (int) $bufferDate[0] . ' ' . (int) $bufferDate[1] . ' * php ' . $cronFolder . 'sendNewsletter.php ' . $store_id . ' ' . $explode[1] . ' ' . $_SERVER['HTTP_HOST'] . ' ' . $_SERVER['SERVER_NAME'];
                    $dateForSorting[] = $bufferDate[2] . '.' . $bufferDate[1] . '.' . $bufferDate[0] . '.' . $buffer[1];
                }
                asort($dateForSorting);
                $sortedDates = array();
                foreach ($dateForSorting as $date) {
                    $newDate = explode('.', $date);
                    $sortedDates[] = $newDate[2] . '.' . $newDate[1] . '.' . $newDate[0] . '/' . $newDate[3];
                }
                $data = $sortedDates;
            }
        }
        if (isset($data[$this->moduleName]["scheduleType"]) && isset($data[$this->moduleName]["scheduled_template_periodic"]) && $data[$this->moduleName]["scheduleType"] == 'P') {
            $cronCommands[] = $data[$this->moduleName]['periodicCronValue'] . ' php ' . $cronFolder . 'sendNewsletter.php ' . $store_id . ' ' . $data[$this->moduleName]['scheduled_template_periodic'] . ' ' . $_SERVER['HTTP_HOST'] . ' ' . $_SERVER['SERVER_NAME'];
        }

        if (isset($cronCommands)) {

            $cronCommands = implode(PHP_EOL, $cronCommands);
            $currentCronBackup = shell_exec('crontab -l');
            $currentCronBackup = explode(PHP_EOL, $currentCronBackup);
            foreach ($currentCronBackup as $key => $command) {
                if (strpos($command, 'php ' . $cronFolder . 'sendNewsletter.php ' . $store_id) || empty($command)) {
                    unset($currentCronBackup[$key]);
                }
            }
            $currentCronBackup = implode(PHP_EOL, $currentCronBackup);
            file_put_contents($cronFolder . 'cron.txt', $currentCronBackup . PHP_EOL . $cronCommands . PHP_EOL);
            shell_exec('crontab -r');
            shell_exec('crontab ' . $cronFolder . 'cron.txt');
        }
    }

    private function getCatalogURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_CATALOG;
        } else {
            $storeURL = HTTP_CATALOG;
        }
        return $storeURL;
    }

    private function getServerURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_SERVER;
        } else {
            $storeURL = HTTP_SERVER;
        }
        return $storeURL;
    }

    private function getCurrentStore($store_id) {
        if ($store_id && $store_id != 0) {
            $store = $this->model_setting_store->getStore($store_id);
        } else {
            $store['store_id'] = 0;
            $store['name'] = $this->config->get('config_name');
            $store['url'] = $this->getCatalogURL();
        }
        return $store;
    }

    public function autocompleteCustomer() {
        $json = array();
        if (isset($this->request->get['filter_name'])) {
            $data = array(
                'filter_name' => $this->request->get['filter_name'],
                'store_id' => $this->request->get['store_id']
            );

            $results = $this->moduleModel->getAutocompleteCustomers($data);
            foreach ($results as $result) {
                $json[] = array(
                    'customer_id' => $result['customer_id'],
                    'customer_group_id' => $result['customer_group_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                    'customer_group' => $result['customer_group'],
                    'firstname' => $result['firstname'],
                    'lastname' => $result['lastname'],
                );
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    public function autocompleteAffiliate() {
        $affiliate_data = array();

        if (isset($this->request->get['filter_name'])) {
            $data = array(
                'filter_name' => $this->request->get['filter_name'],
                'store_id' => $this->request->get['store_id']
            );

            $results = $this->moduleModel->getAutocompleteAffiliates($data);

            foreach ($results as $result) {
                $affiliate_data[] = array(
                    'affiliate_id' => $result['affiliate_id'],
                    'name' => html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')
                );
            }
        }
        $this->response->setOutput(json_encode($affiliate_data));
    }

    public function autocompleteProduct() {
        $json = array();
        if (isset($this->request->get['filter_name'])) {
            $results = $this->moduleModel->getProducts($this->request->get['filter_name'], $this->request->get['store_id']);
            foreach ($results as $result) {
                $json[] = array(
                    'product_id' => $result['product_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                );
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function templateForm() {
        $this->load->model('localisation/language');

        $this->data = array();

        $this->data['storeURL'] = $this->getCatalogURL();
        $this->data['unsubscribeURL'] = $this->getCatalogURL() . 'index.php?route=account/newsletter';
        $this->data['config_logo'] = $this->config->get('config_logo');
        $this->data['default_template'] = $this->load->view($this->modulePath . '/newsletter_template.tpl', $this->data);
        $this->data['e_mail'] = $this->config->get('config_email');

        $store_id = $this->config->get('config_store_id');
        if (isset($this->request->post['store_id'])) {
            $store_id = (int) $this->request->post['store_id'];
        }

        if (isset($this->request->get['template_id'])) {
            $this->data['template_id'] = $this->request->get['template_id'];
            $this->data['template'] = $this->moduleModel->getTemplate($this->data['template_id']);
        } else {
            $this->data['template_id'] = $this->moduleModel->getMaxTemplateId() + 1;
        }

        $this->data['language_id'] = $this->config->get('config_language_id');
        $this->data['languages'] = $this->model_localisation_language->getLanguages();

        //2.2.0.0 language flag image fix
        foreach ($this->data['languages'] as $key => $value) {
            if (version_compare(VERSION, '2.2.0.0', "<")) {
                $this->data['languages'][$key]['flag_url'] = 'view/image/flags/' . $this->data['languages'][$key]['image'];
            } else {
                $this->data['languages'][$key]['flag_url'] = 'language/' . $this->data['languages'][$key]['code'] . '/' . $this->data['languages'][$key]['code'] . '.png"';
            }
        }

        $this->data['current_template_id'] = $this->moduleModel->getMaxTemplateId();

        $languageVariables = array(
            'entry_code',
            'subject_text',
            'user_email',
            'default_message',
            'custom_css',
            'custom_css_help',
            'specials_text',
            'deals_text',
            'new_products_text',
            'newsletter_information_text',
            'newsletter_information_text_help',
            'text_customer_group',
            'text_customer',
            'text_affiliate_all',
            'text_affiliate',
            'text_product',
            'selected_products_list',
            'send_now_info',
            'send_now_button',
            'send_test_mail_button',
            'text_products',
            'text_days',
            'test_cron_button',
            'cron_requirements',
            'select_currency',
            'text_stock_statuses',
            'create_new_template',
            'entry_code',
            'entry_code_help',
            'text_enabled',
            'text_disabled',
            'subject_text',
            'user_email',
            'custom_design',
            'default_notification',
            'custom_css',
            'specials_text',
            'deals_text',
            'new_products_text',
            'text_customer_group',
            'text_customer',
            'text_affiliate_all',
            'text_affiliate',
            'text_product',
            'all_manufacturers',
            'selected_manufacturers',
            'entry_store',
            'entry_to',
            'entry_customer_group',
            'entry_customer',
            'entry_customer_help',
            'entry_affiliate',
            'entry_affiliate_help',
            'entry_product',
            'entry_product_help',
            'entry_spec_product',
            'entry_spec_product_help',
            'entry_category',
            'entry_category_help_one',
            'entry_category_help_two',
            'text_newsletter',
            'text_customer_all',
            'schedule_type',
            'fixed_dates',
            'periodic',
            'cron_time',
            'cron_successfully_changed',
            'default_subject',
            'schedule_tasks_status',
            'best_deals',
            'best_deals_top',
            'new_products_for_last',
            'specials_for_next',
            'product_image_size',
            'product_image_size_help',
            'count_of_products_per_row',
            'entry_store',
            'entry_store_email',
            'entry_newsletter',
            'entry_name',
            'entry_email',
            'error_input_form',
            'text_default',
            'selected_products_list',
            'text_products',
            'text_days',
            'text_limit',
            'text_close',
            'text_sending',
            'text_preparing',
            'text_abort',
            'text_settings',
            'text_toggle',
            'text_list',
            'text_sent_newsletter',
            'text_tasks',
            'text_support',
            'text_save',
            'text_cancel',
            'text_from',
            'text_cron',
            'text_schedule_cron',
            'text_back',
            'text_delete',
            'text_send_test',
            'test_cron_button',
            'cron_requirements',
            'select_currency',
            'select_currency_help',
            'text_stock_statuses',
            'admin_notification',
            'admin_notification_help',
            'cron_services',
            'cron_services_help',
            'name',
            'link',
            'entry_num_emails_per_request',
            'entry_num_emails_per_request_help',
        );

        foreach ($languageVariables as $key => $languageVariable) {
            $this->data[$languageVariable] = $this->language->get($languageVariable);
        }


        $this->response->setOutput($this->load->view($this->modulePath . '/template_form.tpl', $this->data));
    }

    public function templateList() {

        if (isset($this->request->get['store_id'])) {
            $store_id = $this->request->get['store_id'];
        } else {
            $store_id = 0;
        }


        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'time_added';
        }
        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );
        $this->data['templates'] = $this->moduleModel->getTemplates($data, $store_id);
        $this->data['language_id'] = $this->config->get('config_language_id');
        $this->data['token'] = $this->session->data['token'];

        $languageVariables = array(
            'newsletter_subject',
            'last_modified',
            'language',
            'recepients',
            'text_no_results',
            'create_new_template',
            'text_delete'
        );

        foreach ($languageVariables as $languageVariable) {
            $this->data[$languageVariable] = $this->language->get($languageVariable);
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['store_id'] = $store_id;

        $this->data['sort_subject'] = $this->getServerURL() . 'index.php?route=' . $this->modulePath . '/templateList&token=' . $this->session->data['token'] . '&store_id=' . $store_id . '&sort=subject' . $url;
        $this->data['sort_time_added'] = $this->getServerURL() . 'index.php?route=' . $this->modulePath . '/templateList&token=' . $this->session->data['token'] . '&store_id=' . $store_id . '&sort=time_added' . $url;

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $this->moduleModel->getTotalTemplates($store_id);
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link($this->modulePath . '/templateList&token=' . $this->session->data['token'] . '&store_id=' . $store_id . $url . '&page={page}');
        $this->data['pagination'] = $pagination->render();
        $this->data['sort'] = $sort;
        $this->data['order'] = $order;


        $this->response->setOutput($this->load->view($this->modulePath . '/template_list.tpl', $this->data));
    }

    public function deleteTemplate() {
        if (isset($this->request->post['templates'])) {
            $this->moduleModel->deleteTemplate($this->request->post['templates']);
        }
        $this->response->redirect($this->url->link($this->modulePath . '/templateList', 'token=' . $this->session->data['token'] . '&store_id=' . $this->request->post['store_id'], 'SSL'));
    }

    public function saveTemplate() {
        //if(isset($this->request->post['template']) && isset($this->request->post['template_id']) && isset($this->request->post['store_id']))
        //$this->load->model($this->moduleNameSmall);
        // else
        //  exit;

        if (isset($this->request->post['template_id'])) {
            $template_id = $this->request->post['template_id'];
        }


        if ($this->moduleModel->existTemplate($this->request->post['template_id'])) {
            $this->moduleModel->updateTemplate($this->request->post['template_id'], $this->request->post['template'], $this->request->post['store_id']);
        } else {
            $this->moduleModel->addTemplate($this->request->post['template'], $this->request->post['store_id']);
        }
    }

    public function install() {
        $this->moduleModel->install();
    }

    public function uninstall() {
        $this->load->model('setting/setting');
        $this->load->model('setting/store');
        $this->moduleModel->uninstall();
        $stores = $this->model_setting_store->getStores();
        $this->model_setting_setting->deleteSetting('AutomatedNewsletterMessages');
        foreach ($stores as $key => $store) {
            $this->model_setting_setting->deleteSetting('AutomatedNewsletterMessages', $store['store_id']);
        }
    }

    public function clearProgress() {
        $this->initProgress();
        $this->progress->clear();
    }

    public function getProgressMessage() {
        $this->initProgress();
        echo $this->progress->getLastMessage();
        exit;
    }

    public function getProgress() {
        $this->initProgress();
        echo $this->progress->getProgressPercent();
        exit;
    }

    public function abortProcess() {
        $this->initProgress();
        $this->progress->abort();
        exit;
    }

    private function initProgress() {
        session_write_close();
        if (VERSION < '2.1.0.0') {
            $this->load->library('iprogress');
        }
        $task_name = !empty($this->request->get['task_name']) ? $this->request->get['task_name'] : 'isense';
        $this->progress = new iProgress($task_name);
    }

    private function upgradeIfNeeded() {
        if ($this->config->get('automatednewsletter_version') < $this->version) {
            if ($this->moduleModel->upgrade()) {
                $this->load->model('setting/setting');
                $this->model_setting_setting->editSetting('automatednewsletter_settings', array(
                    'automatednewsletter_version' => $this->version
                ));
            }
        }
    }

}
