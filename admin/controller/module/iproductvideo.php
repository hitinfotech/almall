<?php
class ControllerModuleiProductVideo extends Controller {
	private $data = array();
	private $error = array();

	private $image_mime_types = array("image/bmp","image/cis-cod","image/gif","image/png","image/ief","image/jpeg","image/pipeg","image/svg+xml","image/tiff","image/x-cmu-raster","image/x-cmx","image/x-icon","image/x-portable-anymap","image/x-portable-bitmap","image/x-portable-graymap","image/x-portable-pixmap","image/x-rgb","image/x-xbitmap","image/x-xpixmap","image/x-xwindowdump");

	private $allowed_video_extensions = array("mp4","webm","ogg");

	private $module_events = array(
		'pre.admin.delete.product' 	=> 'module/iproductvideo/remove_product_video',
		'post.admin.delete.product' => 'module/iproductvideo/remove_product_video'
	);

	public function install() {
		// Register Events
		if (file_exists(DIR_APPLICATION . 'model/tool/event.php')) {
			$this->load->model('tool/event');
			// Event codes are numbered because the 'code' field is limited to 32 chars, which is so insufficient
			$i = 0; foreach ($this->module_events as $event => $event_trigger) {
				$this->model_tool_event->addEvent('iproductvideo_' . $i, $event, $event_trigger);
			$i++; }

		} else if (file_exists(DIR_APPLICATION . 'model/extension/event.php')) {
			$this->load->model('extension/event');

			$i = 0; foreach ($this->module_events as $event => $event_trigger) {
				$this->model_extension_event->addEvent('iproductvideo_' . $i, $event, $event_trigger);
			$i++; }
		}
	}

	public function uninstall() {
		// Remove Events
		if (file_exists(DIR_APPLICATION . 'model/tool/event.php')) {
			$this->load->model('tool/event');

			$i = 0; foreach ($this->module_events as $event => $event_trigger) {
				$this->model_tool_event->deleteEvent('iproductvideo_' . $i);
			$i++; }
		} else if (file_exists(DIR_APPLICATION . 'model/extension/event.php')) {
			$this->load->model('extension/event');

			$i = 0; foreach ($this->module_events as $event => $event_trigger) {
				$this->model_extension_event->deleteEvent('iproductvideo_' . $i);
			$i++; }
		}

	}

	private function load_static_template_data() {
		$this->load->language('module/iproductvideo');
		
		$this->data['token'] = $this->session->data['token'];

		// Set language data
		$variables = array(
			'heading_title',
			'text_enabled',
			'text_disabled',
			'text_content_top',
			'text_content_bottom',
			'text_column_left',
			'text_column_right',
			'text_activate',
			'text_not_activated',
			'text_click_activate',
			'entry_code',
			'button_save',
			'button_cancel',
			'entry_type',
			'text_type_image',
			'text_type_text',
			'entry_image',
			'text_max_size',
			'text_max_size_learn',
			'text_top_left',
			'text_top_right',
			'text_center',
			'text_bottom_left',
			'text_bottom_right',
			'entry_position',
			'entry_opacity',
			'entry_text',
			'text_default',
			'entry_video_limit_products',
			'entry_all_products',
			'entry_following_products',
			'entry_rotation'
		);
		
		foreach ($variables as $variable) $this->data[$variable] = $this->language->get($variable);

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = false;
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = false;
		}

		$this->load->model('module/iproductvideo');
		
		$this->data['maxSize'] = $this->model_module_iproductvideo->returnMaxUploadSize();
		$this->data['maxSizeReadable'] = $this->model_module_iproductvideo->returnMaxUploadSize(true);
		
		$this->data['error_code'] = isset($this->error['code']) ? $this->error['code'] : '';
		
		$this->data['uploaded_videos'] = array();
		
		$uploaded_videos_folder = IMODULE_ROOT.'vendors/iproductvideo/uploaded_videos/';

		// Uploaded Videos
		if (is_dir($uploaded_videos_folder)) {
			$uploaded_videos = $this->scan_dir($uploaded_videos_folder);
			foreach ($uploaded_videos as $uploaded_video) {
				if ($this->is_video(IMODULE_ROOT.'vendors/iproductvideo/uploaded_videos/' . $uploaded_video)) {
					$src = HTTP_CATALOG.'vendors/iproductvideo/uploaded_videos/' . $uploaded_video;

					$file = pathinfo($src);

					if (!empty($file['extension']) && in_array($file['extension'], $this->allowed_video_extensions)) {
						$type = 'video/' . $file['extension'];
					} else {
						$type = false;
					}

					$this->data['uploaded_videos'][] = array(
						'path'			=>	'vendors/iproductvideo/uploaded_videos/' . $uploaded_video,
						'src'			=>	$src,
						'type'			=>  $type,
						'name'			=>  $uploaded_video
					);
				}
			}
		}

		// Languages
		$this->load->model('localisation/language');

		$this->data['languages'] = array();

		$results = $this->model_localisation_language->getLanguages();
		
		foreach ($results as $result) {
			if ((int)$result['status'] == 1) {

				if (file_exists(DIR_APPLICATION . 'view/image/flags/' . $result['image'])) {
					if ($this->is_image(DIR_APPLICATION . 'view/image/flags/' . $result['image'])) {
						$flag = HTTP_SERVER . 'view/image/flags/' . $result['image'];
					} else {
						$flag = false;
					}
				} else {
					$flag = false;
				}

				$this->data['languages'][] = array(
					'language_id'	=> $result['language_id'],
					'name'			=> $result['name'],
					'flag'			=> $flag	
				);
			}
		}

		$this->data['HTTP_CATALOG'] = HTTP_CATALOG;
	}
	
	public function get_iproductvideo_settings() {
		/* Load static template data */
		$this->load_static_template_data();
		
		/* Get current IDs */
		$store_id		= 	$this->request->get['active_store_id'];
		$video_count	= 	$this->request->get['video_count'];

		// Store Image sizes
		$this->load->model('setting/setting');
		
		$this->data['store']['store_info'] = $this->model_setting_setting->getSetting('config', $store_id);

		$this->data['store']['store_id'] = $store_id;
		$this->data['video_id'] 		 = $video_count;
	
		$this->response->setOutput($this->load->view('module/iproductvideo/iproductvideo_settings.tpl', $this->data));
	}
	
	public function index() {
		/* Load static template data */
		$this->load_static_template_data();
		
		/* Generate default index */
		$this->document->setTitle($this->language->get('heading_title'));
		
		/* jQuery UI */
		$this->document->addScript('view/javascript/iproductvideo/jquery-ui/jquery-ui.min.js');
		$this->document->addStyle('view/javascript/iproductvideo/jquery-ui/jquery-ui.min.css');
		
		/* Bootstrap Form Helpers */
		$this->document->addScript('view/javascript/iproductvideo/bootstrap/js/bootstrap-formhelpers-colorpicker.js');
		$this->document->addScript('view/javascript/iproductvideo/bootstrap/js/bootstrap-formhelpers-selectbox.js');
		
		/* jQuery File Upload Plugin */
		$this->document->addScript('view/javascript/iproductvideo/jquery-fileupload/jquery.ui.widget.js');
		$this->document->addScript('view/javascript/iproductvideo/jquery-fileupload/jquery.iframe-transport.js');
		$this->document->addScript('view/javascript/iproductvideo/jquery-fileupload/jquery.postmessage-transport.js');
		$this->document->addScript('view/javascript/iproductvideo/jquery-fileupload/jquery.xdr-transport.js');
		$this->document->addScript('view/javascript/iproductvideo/jquery-fileupload/jquery.fileupload.js');

		/* NProgress */
		$this->document->addScript('view/javascript/iproductvideo/nprogress/nprogress.js');

		/* iProductVideo */
		$this->document->addStyle('view/stylesheet/iproductvideo.css');
		
		/* Permissions and compatibility checks */
		$this->warning_check();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if (!$this->user->hasPermission('modify', 'module/iproductvideo')) {
				$this->session->data['flash_error'][] = $this->language->get('error_permission');
				$this->response->redirect($this->url->link('module/iproductvideo', 'token=' . $this->session->data['token'], 'SSL'));
			}

			if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
                $this->request->post['iproductvideo']['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
            }

            if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
                $this->request->post['iproductvideo']['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']), true);
            }
            
			// Data validation
			$this->validate();

			if ($this->user->hasPermission('modify', 'module/iproductvideo')) {
				$this->model_module_iproductvideo->editSetting('iproductvideo', $this->request->post);
				$this->session->data['flash_success'][] = $this->language->get('text_success');
				$this->response->redirect($this->url->link('module/iproductvideo', 'token=' . $this->session->data['token'], 'SSL'));
			}
		}

  		$this->data['breadcrumbs'] = array(
			array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => false
			),
			array(
				'text'      => $this->language->get('text_module'),
				'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			),
			array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('module/iproductvideo', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			)
		);
		
		$this->data['action'] = $this->url->link('module/iproductvideo', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['iProductVideo'])) {
			foreach ($this->request->post['iProductVideo'] as $key => $value) {
				$this->data['data']['iProductVideo'][$key] = $this->request->post['iProductVideo'][$key];
			}
		} else {
			$configValue = $this->model_module_iproductvideo->getSetting('iproductvideo');
			$this->data['data'] = $configValue;
		}
		
		// Stores 
		$this->load->model('setting/store');
		
		$stores = array_merge(array
			(0 => array(
				'store_id' => '0',
				'name' => $this->config->get('config_name') . ' (' .$this->data['text_default'] . ')',
				'url' => NULL, 'ssl' => NULL)
			),
			$this->model_setting_store->getStores()
		);
		
		$this->data['stores'] = $stores;

		// Languages
		$this->load->model('localisation/language');

		$languages = array();
		$this->data['store_languages'] = array();

		$results = $this->model_localisation_language->getLanguages();
		
		foreach ($results as $result) {
			if ((int)$result['status'] == 1) {

				if (file_exists(DIR_APPLICATION . 'view/image/flags/' . $result['image'])) {
					if ($this->is_image(DIR_APPLICATION . 'view/image/flags/' . $result['image'])) {
						$flag = HTTP_SERVER . 'view/image/flags/' . $result['image'];
					} else {
						$flag = false;
					}
				} else {
					$flag = false;
				}

				$languages[] = array(
					'language_id'	=> $result['language_id'],
					'name'			=> $result['name'],
					'flag'			=> $flag	
				);
			}
		}

		$this->data['store_languages'] = $languages;

		// Products
		$this->load->model('catalog/product');
		$products = array();
		
		$this->data['products'] = array();
		
		foreach ($stores as $store) {
			if (!empty($this->data['data']['iProductVideo']) && !empty($this->data['data']['iProductVideo'][$store['store_id']]['Videos'])) {
				foreach ($this->data['data']['iProductVideo'][$store['store_id']]['Videos'] as $video_id => $video) {
					foreach ($languages as $language) {
						if (!empty($this->data['data']['iProductVideo'][$store['store_id']]['Videos'][$video_id][$language['language_id']]['LimitProductsList'])) {
							$products = $this->data['data']['iProductVideo'][$store['store_id']]['Videos'][$video_id][$language['language_id']]['LimitProductsList'];
						} else {
							$products = array();
						}
						
						$this->data['products'][$store['store_id']]['Videos'][$video_id][$language['language_id']] = array();
						
						foreach ($products as $product_id) {
		
							$product_info = $this->model_catalog_product->getProduct($product_id);
							
							if ($product_info) {
								$this->data['products'][$store['store_id']]['Videos'][$video_id][$language['language_id']][] = array(
									'product_id' 	=> $product_info['product_id'],
									'name'        	=> $product_info['name']
								);
							}
						}
					}
				}
			}
		}

		// Store Image sizes
		$this->load->model('setting/setting');
		
		foreach ($this->data['stores'] as $k => $store) {
			$this->data['stores'][$k]['store_info'] = $this->model_setting_setting->getSetting('config', $store['store_id']);
		}
		
		$this->data['header'] 		= $this->load->controller('common/header');
		$this->data['column_left'] 	= $this->load->controller('common/column_left');
		$this->data['footer'] 		= $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/iproductvideo.tpl', $this->data));
	}

	private function warning_check() {
		$default_dirs = array('uploaded_videos');

		foreach ($default_dirs as $dir) {
			if (!file_exists(IMODULE_ROOT . 'vendors/iproductvideo/' . $dir)) {
				mkdir(IMODULE_ROOT . 'vendors/iproductvideo/' . $dir, 0777, true);
			}
		}
		
		$this->data['warning_modal'] = false;
		$iproductvideo_dirs = $this->scan_dir(IMODULE_ROOT . 'vendors/iproductvideo');

		foreach ($iproductvideo_dirs as $dir) {
			if (!is_readable(IMODULE_ROOT . 'vendors/iproductvideo/' . $dir)) {
				$this->data['warning_modal']['errors'][] = 'No read permissions for <b>' . '/vendors/iproductvideo/' . $dir . '</b>';
			}
			if (!is_writable(IMODULE_ROOT . 'vendors/iproductvideo/' . $dir)) {
				$this->data['warning_modal']['errors'][] = 'No write permissions for <b>' . '/vendors/iproductvideo/' . $dir . '</b>';
			}
		}
	}


	private function validate() {
		foreach ($this->request->post['iProductVideo'] as $store_id => $store) {
			if (!empty($store['Enabled']) && $store['Enabled'] == 'true' && !empty($store['Videos'])) {
				foreach ($store['Videos'] as $video_id => $video) {
					foreach ($video as $language_id => $video_settings) {
						if ((int)$video_settings['SortOrder'] <= 0) {
							$this->request->post['iProductVideo'][$store_id]['Videos'][$video_id][$language_id]['SortOrder'] = 0;
						}
					}
				}
			}
		}

		if (!empty($this->session->data['flash_error'])) {
			$this->response->redirect($this->url->link('module/iproductvideo', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}
	
	public function autocomplete_product() {
		$json = array();
		
		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model']) || isset($this->request->get['filter_category_id'])) {
			$this->load->model('catalog/product');
			$this->load->model('catalog/option');
			
			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}
			
			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
			} else {
				$filter_model = '';
			}
			
			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];	
			} else {
				$limit = 20;	
			}			
						
			$data = array(
				'filter_name'  => $filter_name,
				'filter_model' => $filter_model,
				'start'        => 0,
				'limit'        => $limit
			);
			
			$results = $this->model_catalog_product->getProducts($data);
			
			foreach ($results as $result) {
				
				$json[] = array(
					'product_id' => $result['product_id'],
					'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),	
					'model'      => $result['model'],
					'price'      => $result['price']
				);	
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function upload_video() {
		$json = array();
		$this->load->language('module/iproductvideo');

		if (!$this->user->hasPermission('modify', 'module/iproductvideo')) {
			$json['error'] = $this->language->get('error_permission');
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		} else {
			$this->load->model('module/iproductvideo');
			$this->load->library('iProductVideoUploadHandler');

			$file_upload_options = array(
				'upload_dir'	=> IMODULE_ROOT . 'vendors/iproductvideo/uploaded_videos/',
				'upload_url'	=> HTTP_CATALOG . 'vendors/iproductvideo/uploaded_videos/',
				'param_name'	=> $this->request->get['upload_param'],
				'max_file_size'	=> $this->model_module_iproductvideo->returnMaxUploadSize()
			);

			$upload_handler = new UploadHandler($file_upload_options);
		}
	}

	public function delete_video($video = false) {
		$json = array();
		$this->load->language('module/iproductvideo');

		if (!$this->user->hasPermission('modify', 'module/iproductvideo')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			if (isset($this->request->get['video'])) {
				$video = $this->request->get['video'];
			} else {
				exit;	
			}
			
			$this->load->model('module/iproductvideo');
			if (file_exists(IMODULE_ROOT . 'vendors/iproductvideo/uploaded_videos/' . $video)) {
				@unlink(IMODULE_ROOT . 'vendors/iproductvideo/uploaded_videos/' . $video);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function load_uploaded_videos() {
		$this->load->model('module/iproductvideo');
		
		if (isset($this->request->get['store_id'])) {
			$this->data['store']['store_id'] = $this->request->get['store_id'];
		} else {
			exit;	
		}
		
		if (isset($this->request->get['video_id'])) {
			$this->data['video_id'] = $this->request->get['video_id'];	
		} else {
			exit;	
		}

		if (isset($this->request->get['language_id'])) {
			$this->data['language']['language_id'] = $this->request->get['language_id'];	
		} else {
			exit;	
		}
		
		$uploaded_videos_folder = IMODULE_ROOT . 'vendors/iproductvideo/uploaded_videos/';
		$uploaded_videos = $this->scan_dir($uploaded_videos_folder);

		foreach ($uploaded_videos as $uploaded_video) {
			if ($this->is_video(IMODULE_ROOT.'vendors/iproductvideo/uploaded_videos/' . $uploaded_video)) {
				$src = HTTP_CATALOG.'vendors/iproductvideo/uploaded_videos/' . $uploaded_video;
				
				$file = pathinfo($src);

				if (!empty($file['extension']) && in_array($file['extension'], $this->allowed_video_extensions)) {
					$type = 'video/' . $file['extension'];
				} else {
					$type = false;
				}

				$this->data['uploaded_videos'][] = array(
					'path'			=>	'vendors/iproductvideo/uploaded_videos/' . $uploaded_video,
					'src'			=>	$src,
					'type'			=>  $type,
					'name'			=>  $uploaded_video
				);
			}
		}
		
		if (!empty($this->data['uploaded_videos'])) {
			$this->response->setOutput($this->load->view('module/iproductvideo/videos_loop.tpl', $this->data));
		} else {
			$this->response->setOutput('<span> No uploaded videos.<br /> Use the upload form below<br /> or put your videos in this server directory<br /> <b>/vendors/iproductvideo/uploaded_videos</b> </span>');
		}
	}
	
	public function remove_product_video($product_id = 0) {
		if (!empty($product_id)) {
			$remove_product_id = $product_id;
		} elseif (!empty($this->request->get['product_id'])) {
			$remove_product_id = $this->request->get['product_id'];
		} else {
			return;
		}

		$this->load->model('module/iproductvideo');

		$db_data = $this->model_module_iproductvideo->getSetting('iproductvideo');

		foreach ($db_data['iProductVideo'] as $store_id => $store_settings) {
			if (!empty($store_settings['Videos'])) {
				foreach ($store_settings['Videos'] as $video_id => $video_settings) {
					if (!empty($video_settings)) {
						foreach ($video_settings as $language_id => $video) {
							if (!empty($video['LimitProductsList'])) {
								foreach ($video['LimitProductsList'] as $video_product_id_index => $video_product_id) {
									if ((int)$remove_product_id == (int)$video_product_id) {
										unset($db_data['iProductVideo'][$store_id]['Videos'][$video_id][$language_id]['LimitProductsList'][$video_product_id_index]);
									}
								} 
							} 
						}
					}
				}
			}
		}

		$this->model_module_iproductvideo->editSetting('iproductvideo', $db_data);
	}

	private function scan_dir($dir) {
		$ignored = array('.', '..');
	
		$files = array();
		
		foreach (scandir($dir) as $file) {
			if (!in_array($file, $ignored)) {
				$files[$file] = filemtime($dir . '/' . $file);
			}
		}
	
		arsort($files);
		$files = array_keys($files);

		return ($files) ? $files : array();
	}
	
	private function clean_filename($name) {
		$filename = preg_replace("([^\w\s\d\-_~,;:\[\]\(\].]|[\.]{2,})", '', $name);

		if (function_exists('mb_convert_encoding')) { 
			$filename = mb_convert_encoding($filename, 'UTF-8');
		} else {
			$filename = urlencode($filename);
		}

		$filename = preg_replace("([^\w\s\d\-_~,;:\[\]\(\].]|[\.]{2,})", '', $filename);
		
		return $filename;	
	}
	
	private function is_video($path) {
		$file = pathinfo($path);

		if (!empty($file['extension']) && in_array($file['extension'], $this->allowed_video_extensions)) {
			return true;
		}

		return false;
	}

	private function is_image($path) {
	    $a = getimagesize($path);

		if ($a !== false && !empty($a['mime']) && in_array($a['mime'], $this->image_mime_types)) {
			return true;
		}

	    return false;
	}
}
?>