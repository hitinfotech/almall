<?php
################################################################################################
#  Marketplace RMA Opencart 2.x.x.x From Webkul  http://webkul.com 	#
################################################################################################
class ControllerModulewkrma extends Controller {
	
	private $error = array(); 
	private $data = array(); 
	
	public function install() {
		$this->load->model('module/wk_rma');
		$this->model_module_wk_rma->createTable();		
	}
	
	public function index() {   
		$this->load->language('module/wk_rma');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('tool/image');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			// $this->request->post['wk_rma_admin_policy_setting'] = mysql_real_escape_string($this->request->post['wk_rma_admin_policy_setting']);
			$this->model_setting_setting->editSetting('wk_rma', $this->request->post);					
			$this->session->data['success'] = $this->language->get('text_success');						
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_rma_admin_policy'] = $this->language->get('text_rma_admin_policy');
		$data['text_rma_enable'] = $this->language->get('text_rma_enable');
		$data['text_rma_info_policy'] = $this->language->get('text_rma_info_policy');
		$data['text_rma_time'] = $this->language->get('text_rma_time');
		$data['text_rma_time_info'] = $this->language->get('text_rma_time_info');
		$data['text_rma_info_policy_info'] = $this->language->get('text_rma_info_policy_info');

		$data['text_order_status'] = $this->language->get('text_order_status');
		$data['text_order_status_info'] = $this->language->get('text_order_status_info');
		$data['text_image_extenstion'] = $this->language->get('text_image_extenstion');
		$data['text_file_extenstion'] = $this->language->get('text_file_extenstion');
		$data['text_extenstion_size'] = $this->language->get('text_extenstion_size');
		$data['text_extenstion_holder'] = $this->language->get('text_extenstion_holder');
		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['text_unselect_all'] = $this->language->get('text_unselect_all');
		$data['text_size_info'] = $this->language->get('text_size_info');
		$data['text_rma_add_status'] = $this->language->get('text_rma_add_status');
		$data['text_rma_add_reasons'] = $this->language->get('text_rma_add_reasons');
		$data['text_rma_manage'] = $this->language->get('text_rma_manage');
		$data['text_rma_return_add'] = $this->language->get('text_rma_return_add');
		$data['text_rma_return_add_info'] = $this->language->get('text_rma_return_add_info');
		$data['text_labels'] = $this->language->get('text_labels');
		$data['text_lable_image'] = $this->language->get('text_lable_image');
		$data['text_lable_name'] = $this->language->get('text_lable_name');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_info_mail'] = $this->language->get('text_info_mail');
		$data['entry_mail_seller'] = $this->language->get('entry_mail_seller');
		$data['entry_mail_customer'] = $this->language->get('entry_mail_customer');
		$data['entry_mail_admin'] = $this->language->get('entry_mail_admin');
		$data['entry_mail_keywords'] = $this->language->get('entry_mail_keywords');
		$data['mail_keywords_info'] = $this->language->get('mail_keywords_info');
		$data['entry_mail_seller_info'] = $this->language->get('entry_mail_seller_info');
		$data['entry_mail_customer_info'] = $this->language->get('entry_mail_customer_info');
		$data['entry_mail_admin_info'] = $this->language->get('entry_mail_admin_info');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_upload'] = $this->language->get('button_upload');
		$data['button_delete'] = $this->language->get('button_delete');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_labels'] = $this->language->get('tab_labels');

		$data['manage_rma'] = $this->url->link('catalog/wk_rma_admin', 'token=' . $this->session->data['token'], 'SSL');
		$data['status_rma'] = $this->url->link('catalog/wk_rma_status', 'token=' . $this->session->data['token'], 'SSL');
		$data['reason_rma'] = $this->url->link('catalog/wk_rma_reason', 'token=' . $this->session->data['token'], 'SSL');

		//CONFIG
		$config_data = array(
			'wk_rma_status',
			'wk_rma_address',
			'wk_rma_system_information',	
			'wk_rma_system_time',
			'wk_rma_system_orders',
			'wk_rma_system_image',
			'wk_rma_system_size',
			'wk_rma_system_file',
			'wk_rma_seller_mail',
			'wk_rma_customer_mail',
			'wk_rma_admin_mail',
		);
		
		foreach ($config_data as $conf) {
			if (isset($this->request->post[$conf])) {
				$data[$conf] = $this->request->post[$conf];
			} else {
				$data[$conf] = str_replace('\n','', str_replace('\r','',$this->config->get($conf)));
			}
		}

		$target = DIR_IMAGE.'rma/files/';

		$data['shipping_label_folder'] = array();
		
		if(is_dir($target)){
			$opentarget = opendir($target);
			while($image = readdir($opentarget)){
				if($image!='.' AND $image!='..'){
					$data['shipping_label_folder'][] = array('image' => $this->model_tool_image->resize('rma/files/'.$image,50,50),
																	'name' => $image);
				}
			}
		}

		$this->load->model('catalog/information');
		$data['information'] = $this->model_catalog_information->getInformations(array());

		$this->load->model('localisation/order_status');
		$data['order_status'] = $this->model_localisation_order_status->getOrderStatuses(array());

		$data['token'] = $this->session->data['token'];

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/wk_rma', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		//get total mail
		$this->load->model('customerpartner/mail');
		$data['mails'] = $this->model_customerpartner_mail->gettotal();
		
		$data['action'] = $this->url->link('module/wk_rma', 'token=' . $this->session->data['token'], 'SSL');
		$data['action_image'] = $this->url->link('module/wk_rma/image', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');				
		$data['delete'] = $this->url->link('module/wk_rma/delete', 'token=' . $this->session->data['token'], 'SSL');				

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/wk_rma.tpl', $data));

	}

	public function image() {

    	$this->language->load('module/wk_rma');

    	$this->document->setTitle($this->language->get('heading_title'));
				
		$file = $this->request->files['up_file'];

		if ($this->validate() && $file['name']) {
			
			$file_name = '';
			$file_no = 0;
			foreach($file['name'] as $key => $image){
				if($image){		
					$file_name .= $image.', ';
					$file_no++;
					$target = DIR_IMAGE.'rma/files/'.$image;
					@mkdir(DIR_IMAGE.'rma/files/',0755,true);
					move_uploaded_file($file['tmp_name'][$key],$target);
				}
			}

			if($file_no)
				$this->session->data['success'] = sprintf($this->language->get('text_image_success'),$file_no,substr($file_name,0,-2));	
			
			$this->response->redirect($this->url->link('module/wk_rma', 'token=' . $this->session->data['token'] , 'SSL'));
		}
	}

	public function delete() {

    	$this->language->load('module/wk_rma');

    	$this->document->setTitle($this->language->get('heading_title'));
				
		if (isset($this->request->post['selected']) && $this->validate()) {
			
			foreach ($this->request->post['selected'] as $image) {
				if(file_exists(DIR_IMAGE.'rma/files/'.$image))
					@unlink(DIR_IMAGE.'rma/files/'.$image);
	  		}

			$this->session->data['success'] = $this->language->get('text_success_delete');
						
			$this->response->redirect($this->url->link('module/wk_rma', 'token=' . $this->session->data['token'] , 'SSL'));
			
		}

    	$this->index();
  	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/wk_rma')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>