<?php
class ControllerModuleOrderFollowUp extends Controller {
	
	private $data = array();
	private $error = array();
	private $version;
	private $module_path;
	private $extensions_link;
	private $language_variables;
	private $moduleModel;
	private $moduleName;
	private $moduleNameBig;
	private $call_model;

	public function __construct($registry){
		parent::__construct($registry);
		$this->load->config('isenselabs/orderfollowup');
		$this->moduleName    = $this->config->get('orderfollowup_name');
		$this->call_model    = $this->config->get('orderfollowup_model');
		$this->module_path   = $this->config->get('orderfollowup_path');
		$this->version       = $this->config->get('orderfollowup_version');
		$this->moduleNameBig = $this->config->get('orderfollowup_name_big');

		if (version_compare(VERSION, '2.3.0.0', '>=')) {			
			$this->extensions_link = $this->url->link('extension/extension', 'token=' . $this->session->data['token'].'&type=module', 'SSL');
		} else {
			$this->extensions_link = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');	
		}
			
		$this->load->model($this->module_path);
		$this->moduleModel = $this->{$this->call_model};
    	$this->language_variables = $this->load->language($this->module_path);

    	//Loading framework models
	 	$this->load->model('setting/store');
		$this->load->model('setting/setting');
        $this->load->model('localisation/language');
        if(VERSION >= '2.1.0.1'){
			$this->load->model('customer/customer_group');
		} else {
			$this->load->model('sale/customer_group');
		}

		$this->document->addScript('view/javascript/summernote/summernote.js');
		$this->document->addStyle('view/javascript/summernote/summernote.css');

		$this->data['module_path']     = $this->module_path;
		$this->data['moduleNameSmall'] = $this->moduleName;
		$this->data['moduleName'] = $this->moduleNameBig;    
	}

    public function index() { 
        $this->moduleModel->init();
        
    	foreach ($this->language_variables as $code => $languageVariable) {
		    $this->data[$code] = $languageVariable;
		} 

        $this->load->model('design/layout');
		$this->load->model('catalog/product');
		
		if(VERSION >= '2.1.0.1'){
			$this->data['customerGroups'] = $this->model_customer_customer_group->getCustomerGroups();
		} else {
			$this->data['customerGroups'] = $this->model_sale_customer_group->getCustomerGroups();
		}
        
        
		
        $this->document->addStyle('view/stylesheet/'.$this->moduleName.'/'.$this->moduleName.'.css');
		$this->document->addScript('view/javascript/'.$this->moduleName.'/nprogress.js');

        $this->document->setTitle($this->language->get('heading_title'). ' ' . $this->version);

        if(!isset($this->request->get['store_id'])) {
           $this->request->get['store_id'] = 0; 
        }
	
        $store = $this->getCurrentStore($this->request->get['store_id']);
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) { 	
            if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
                $this->request->post[$this->moduleNameBig]['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
            }
            if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
                $this->request->post[$this->moduleNameBig]['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']), true);
            }

        	$this->model_setting_setting->editSetting($this->moduleNameBig, $this->request->post, $this->request->post['store_id']);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link($this->module_path, 'store_id='.$this->request->post['store_id'] . '&token=' . $this->session->data['token'], 'SSL'));
        }
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

        $this->data['breadcrumbs']   = array();
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
        );
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->extensions_link,
        );
        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'). ' ' . $this->version,
            'href' => $this->url->link($this->module_path, 'token=' . $this->session->data['token'], 'SSL'),
        );

		$this->data['heading_title'] = $this->language->get('heading_title'). ' ' . $this->version;
		
		$this->data['currency']      = $this->config->get('config_currency'); 
		$this->data['stores']        = array_merge(array(0 => array('store_id' => '0', 'name' => $this->config->get('config_name') . ' (' . $this->data['text_default'].')', 'url' => HTTP_SERVER, 'ssl' => HTTPS_SERVER)), $this->model_setting_store->getStores());
		$this->data['languages']     = $this->model_localisation_language->getLanguages();

        foreach ($this->data['languages'] as $key => $value) {
			if(version_compare(VERSION, '2.2.0.0', "<")) {
				$this->data['languages'][$key]['flag_url'] = 'view/image/flags/'.$this->data['languages'][$key]['image'];
			} else {
				$this->data['languages'][$key]['flag_url'] = 'language/'.$this->data['languages'][$key]['code'].'/'.$this->data['languages'][$key]['code'].'.png"';
			}
		}

		$this->data['store']          = $store;
		$this->data['token']          = $this->session->data['token'];
		$this->data['action']         = $this->url->link($this->module_path, 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel']         = $this->extensions_link;
		$this->data['moduleSettings'] = $this->model_setting_setting->getSetting($this->moduleNameBig, $store['store_id']);
		$this->data['moduleData']     = (isset($this->data['moduleSettings'][$this->moduleNameBig])) ? $this->data['moduleSettings'][$this->moduleNameBig] : array();
		$this->data['orderStatuses']  = $this->getAllOrderStatuses();

		if ($this->config->get($this->moduleName.'status')) {
			$this->data[$this->moduleName.'status'] = $this->config->get($this->moduleName.'status');
		} else {
			$this->data[$this->moduleName.'status'] = '0';
		}
		
		$this->data['catalog_product'] = $this->model_catalog_product;
		
		$this->data['header']          = $this->load->controller('common/header');
		$this->data['column_left']     = $this->load->controller('common/column_left');
		$this->data['footer']          = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view($this->module_path.'/'.$this->moduleName.'.tpl', $this->data));
    }
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', $this->module_path)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
	
	public function get_followup_settings() {
		$this->load->model('catalog/product');
		
		if(VERSION >= '2.1.0.1'){
			$this->data['customerGroups'] = $this->model_customer_customer_group->getCustomerGroups();
		} else {
			$this->data['customerGroups'] = $this->model_sale_customer_group->getCustomerGroups();
		}
		
		$this->data['currency']					= $this->config->get('config_currency');
		$this->data['languages']              	= $this->model_localisation_language->getLanguages();

		 foreach ($this->data['languages'] as $key => $value) {
			if(version_compare(VERSION, '2.2.0.0', "<")) {
				$this->data['languages'][$key]['flag_url'] = 'view/image/flags/'.$this->data['languages'][$key]['image'];
			} else {
				$this->data['languages'][$key]['flag_url'] = 'language/'.$this->data['languages'][$key]['code'].'/'.$this->data['languages'][$key]['code'].'.png"';
			}
		}
		$this->data['followup']['id'] = $this->request->get['followup_id'];
		$store_id                     = $this->request->get['store_id'];
		$this->data['data']           = $this->model_setting_setting->getSetting($this->moduleNameBig, $store_id);
		$this->data['moduleName']     = $this->moduleNameBig;
		$this->data['moduleData']     = (isset($this->data['data'][$this->moduleNameBig])) ? $this->data['data'][$this->moduleNameBig] : array();
		$this->data['orderStatuses']  = $this->getAllOrderStatuses();
		$this->data['newAddition']    = true;
				
		$this->response->setOutput($this->load->view($this->module_path.'/tab_followuptab.tpl', $this->data));
	}
	
	public function getAllOrderStatuses() {
		$query = 'SELECT * FROM ' . DB_PREFIX . 'order_status WHERE language_id='.$this->config->get('config_language_id');
		return $this->db->query($query)->rows;
	}
	
	public function getlogmessage() {	
		$id = $this->request->get['id'];
		$query =  $this->db->query("SELECT message FROM `" . DB_PREFIX . "orderfollowup_log`
			WHERE `id`=".$id);
		
		echo $query->row['message']; 
	}
    
	public function getlog() {
		$this->load->model('sale/order');
        if (!empty($this->request->get['page'])) {
            $page = (int) $this->request->get['page'];
        } else {
			$page = 1;	
		}
			
		if(!isset($this->request->get['store_id'])) {
           $this->request->get['store_id'] = 0;
        } 
				
		$this->data['url_link']   = $this->url;
		$this->data['sale_order'] = $this->model_sale_order;
		
		$this->data['store_id']   = $this->request->get['store_id'];
		$this->data['token']      = $this->session->data['token'];
		$this->data['limit']      = 8; // $this->config->get('config_limit_admin')
		$this->data['total']      = $this->moduleModel->getTotalLog($this->request->get['store_id']);
		
	    $pagination					= new Pagination();
        $pagination->total			= $this->data['total'];
        $pagination->page			= $page;
        $pagination->limit			= $this->data['limit']; 
        $pagination->url			= $this->url->link('module/'.$this->moduleName.'/getlog','token=' . $this->session->data['token'].'&page={page}&store_id='.$this->request->get['store_id'], 'SSL');
		$this->data['pagination']			= $pagination->render();
        $this->data['sources']			= $this->moduleModel->viewLogs($page, $this->data['limit'], $this->request->get['store_id']);

		$this->data['results'] 			= sprintf($this->language->get('text_pagination'), ($this->data['total']) ? (($page - 1) * $this->data['limit']) + 1 : 0, ((($page - 1) * $this->data['limit']) > ($this->data['total'] - $this->data['limit'])) ? $this->data['total'] : ((($page - 1) * $this->data['limit']) + $this->data['limit']), $this->data['total'], ceil($this->data['total'] / $this->data['limit']));
		$this->data['token']      = $this->session->data['token'];
        $this->data['store_id']   = $this->request->get['store_id']; 
		
		$this->response->setOutput($this->load->view($this->module_path.'/view_log.tpl', $this->data));
    }

    public function deleteLogEntry(){
        $data = $this->request->post['selected_log_entries'];
        $store_id = $this->request->post['store_id'];
        $this->moduleModel->deleteLogEntry($data, $store_id);       
    }
	
    public function install() {
	    $this->moduleModel->install();
    }
    
    public function uninstall() {
        $this->load->model('design/layout');
		
		$this->model_setting_setting->deleteSetting($this->moduleName,0);
		$stores=$this->model_setting_store->getStores();
		foreach ($stores as $store) {
			$this->model_setting_setting->deleteSetting($this->moduleName, $store['store_id']);
		}
        $this->moduleModel->uninstall();
    }
	
	
    private function getCatalogURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_CATALOG;
        } else {
            $storeURL = HTTP_CATALOG;
        } 
        return $storeURL;
    }

    private function getServerURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_SERVER;
        } else {
            $storeURL = HTTP_SERVER;
        } 
        return $storeURL;
    }

    private function getCurrentStore($store_id) {    
        if($store_id && $store_id != 0) {
            $store = $this->model_setting_store->getStore($store_id);
        } else {
            $store['store_id'] = 0;
            $store['name'] = $this->config->get('config_name');
            $store['url'] = $this->getCatalogURL(); 
        }
        return $store;
    }
}
?>