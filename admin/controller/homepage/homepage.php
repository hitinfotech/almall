<?php

class ControllerHomepageHomepage extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('homepage/homepage');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('localisation/country');

        $this->getList();
    }

    public function add() {
        $this->load->language('homepage/homepage');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->load->model('homepage/homepage');
            $this->model_homepage_homepage->addSetting('homepage', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('homepage/homepage', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm('add');
    }

    public function edit() {
        $this->load->language('homepage/homepage');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->load->model('homepage/homepage');
            $this->model_homepage_homepage->editSetting('homepage', $this->request->post, $this->request->get['page_id']);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('homepage/homepage', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getForm('edit');
    }

    public function delete() {
        $this->load->language('homepage/homepage');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('homepage/homepage');

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $page_id) {
                $this->model_homepage_homepage->deleteSetting('homepage',$page_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('homepage/homepage', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {
        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('homepage/homepage', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['add'] = $this->url->link('homepage/homepage/add', 'token=' . $this->session->data['token'], 'SSL');
        $data['delete'] = $this->url->link('homepage/homepage/delete', 'token=' . $this->session->data['token'], 'SSL');

        $data['countries'] = array();

        $available_coutnries = $this->model_localisation_country->getAvailableCountries();
        $country_names = array();
        foreach($available_coutnries as $count){
          $country_names[$count['country_id']] = $count['name'];
        }

        $Langauges = array(
          1 => 'English',
          2 => 'Arabic',
        );
        $this->load->model('homepage/homepage');
        $pages = $this->model_homepage_homepage->getPages();
        foreach ($pages as $page_id => $page) {
            $name = '';
            foreach($page as $k => $value){
                $name .= '( '.$country_names[$value['country_id']].' ) ';
            }
            $data['countries'][] = array(
                'page_id' => $page_id,
                'name' => $name,
                'language'=> $Langauges[$value['language_id']],
                'edit' => $this->url->link('homepage/homepage/edit', 'token=' . $this->session->data['token'] . '&page_id=' . $page_id, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_language'] = $this->language->get('column_language');
        $data['column_page_id'] = $this->language->get('column_page_id');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('homepage/homepage_list.tpl', $data));
    }

    public function getForm($type='new') {

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_homepage_block0_image1'] = $this->language->get('text_homepage_block0_image1');
        $data['text_homepage_block0_image2'] = $this->language->get('text_homepage_block0_image2');
        $data['text_homepage_block0_url1'] = $this->language->get('text_homepage_block0_url1');

        $data['text_homepage_block1_title1'] = $this->language->get('text_homepage_block1_title1');
        $data['text_homepage_block1_button1'] = $this->language->get('text_homepage_block1_button1');
        $data['text_homepage_block1_description1'] = $this->language->get('text_homepage_block1_description1');
        $data['text_homepage_block1_url1'] = $this->language->get('text_homepage_block1_url1');
        $data['text_homepage_block1_image1'] = $this->language->get('text_homepage_block1_image1');

        $data['text_homepage_block1_title2'] = $this->language->get('text_homepage_block1_title2');
        $data['text_homepage_block1_button2'] = $this->language->get('text_homepage_block1_button2');
        $data['text_homepage_block1_description2'] = $this->language->get('text_homepage_block1_description2');
        $data['text_homepage_block1_url2'] = $this->language->get('text_homepage_block1_url2');
        $data['text_homepage_block1_image2'] = $this->language->get('text_homepage_block1_image2');

        $data['text_homepage_block1_title3'] = $this->language->get('text_homepage_block1_title3');
        $data['text_homepage_block1_button3'] = $this->language->get('text_homepage_block1_button3');
        $data['text_homepage_block1_description3'] = $this->language->get('text_homepage_block1_description3');
        $data['text_homepage_block1_url3'] = $this->language->get('text_homepage_block1_url3');
        $data['text_homepage_block1_image3'] = $this->language->get('text_homepage_block1_image3');

        $data['text_homepage_block1_title4'] = $this->language->get('text_homepage_block1_title4');
        $data['text_homepage_block1_button4'] = $this->language->get('text_homepage_block1_button4');
        $data['text_homepage_block1_description4'] = $this->language->get('text_homepage_block1_description4');
        $data['text_homepage_block1_url4'] = $this->language->get('text_homepage_block1_url4');
        $data['text_homepage_block1_image4'] = $this->language->get('text_homepage_block1_image4');

        $data['text_homepage_block1_title5'] = $this->language->get('text_homepage_block1_title5');
        $data['text_homepage_block1_button5'] = $this->language->get('text_homepage_block1_button5');
        $data['text_homepage_block1_description5'] = $this->language->get('text_homepage_block1_description5');
        $data['text_homepage_block1_url5'] = $this->language->get('text_homepage_block1_url5');
        $data['text_homepage_block1_image5'] = $this->language->get('text_homepage_block1_image5');

        $data['text_homepage_block3_title'] = $this->language->get('text_homepage_block3_title');
        $data['text_homepage_block3_description'] = $this->language->get('text_homepage_block3_description');
        $data['text_homepage_block3_title1'] = $this->language->get('text_homepage_block3_title1');
        $data['text_homepage_block3_description1'] = $this->language->get('text_homepage_block3_description1');
        $data['text_homepage_block3_url1'] = $this->language->get('text_homepage_block3_url1');
        $data['text_homepage_block3_image1'] = $this->language->get('text_homepage_block3_image1');

        $data['text_homepage_block3_title2'] = $this->language->get('text_homepage_block3_title2');
        $data['text_homepage_block3_description2'] = $this->language->get('text_homepage_block3_description2');
        $data['text_homepage_block3_url2'] = $this->language->get('text_homepage_block3_url2');
        $data['text_homepage_block3_image2'] = $this->language->get('text_homepage_block3_image2');

        $data['text_homepage_block3_title3'] = $this->language->get('text_homepage_block3_title3');
        $data['text_homepage_block3_description3'] = $this->language->get('text_homepage_block3_description3');
        $data['text_homepage_block3_url3'] = $this->language->get('text_homepage_block3_url3');
        $data['text_homepage_block3_image3'] = $this->language->get('text_homepage_block3_image3');

        $data['text_homepage_block3_title4'] = $this->language->get('text_homepage_block3_title4');
        $data['text_homepage_block3_description4'] = $this->language->get('text_homepage_block3_description4');
        $data['text_homepage_block3_url4'] = $this->language->get('text_homepage_block3_url4');
        $data['text_homepage_block3_image4'] = $this->language->get('text_homepage_block3_image4');

        $data['text_homepage_block4_title'] = $this->language->get('text_homepage_block4_title');
        $data['text_homepage_block4_description'] = $this->language->get('text_homepage_block4_description');

        $data['text_homepage_block4_title1'] = $this->language->get('text_homepage_block4_title1');
        $data['text_homepage_block4_description1'] = $this->language->get('text_homepage_block4_description1');
        $data['text_homepage_block4_url1'] = $this->language->get('text_homepage_block4_url1');
        $data['text_homepage_block4_image1'] = $this->language->get('text_homepage_block4_image1');

        $data['text_homepage_block4_title2'] = $this->language->get('text_homepage_block4_title2');
        $data['text_homepage_block4_description2'] = $this->language->get('text_homepage_block4_description2');
        $data['text_homepage_block4_url2'] = $this->language->get('text_homepage_block4_url2');
        $data['text_homepage_block4_image2'] = $this->language->get('text_homepage_block4_image2');

        $data['text_homepage_block4_title3'] = $this->language->get('text_homepage_block4_title3');
        $data['text_homepage_block4_description3'] = $this->language->get('text_homepage_block4_description3');
        $data['text_homepage_block4_url3'] = $this->language->get('text_homepage_block4_url3');
        $data['text_homepage_block4_image3'] = $this->language->get('text_homepage_block4_image3');

        $data['text_homepage_block4_title4'] = $this->language->get('text_homepage_block4_title4');
        $data['text_homepage_block4_description4'] = $this->language->get('text_homepage_block4_description4');
        $data['text_homepage_block4_url4'] = $this->language->get('text_homepage_block4_url4');
        $data['text_homepage_block4_image4'] = $this->language->get('text_homepage_block4_image4');

        $data['text_brand'] = $this->language->get('text_brand');
        $data['text_brand_name'] = $this->language->get('text_brand_name');
        $data['text_brand_desc'] = $this->language->get('text_brand_desc');
        $data['text_brand_button'] = $this->language->get('text_brand_button');

        $data['text_homepage_block5_image1'] = $this->language->get('text_homepage_block5_image1');
        $data['text_homepage_block5_image2'] = $this->language->get('text_homepage_block5_image2');
        $data['text_homepage_block5_image3'] = $this->language->get('text_homepage_block5_image3');

        $data['text_none'] = $this->language->get('text_none');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['entry_related'] = $this->language->get('entry_related');
        $data['entry_offers'] = $this->language->get('entry_offers');


        $data['tab_block0'] = $this->language->get('tab_block0');
        $data['tab_block1'] = $this->language->get('tab_block1');
        $data['tab_block2'] = $this->language->get('tab_block2');
        $data['tab_block3'] = $this->language->get('tab_block3');
        $data['tab_block4'] = $this->language->get('tab_block4');
        $data['tab_block5'] = $this->language->get('tab_block5');
        $data['tab_block6'] = $this->language->get('tab_block6');
        $data['tab_block7'] = $this->language->get('tab_block7');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['homepage_block1_image1'])) {
            $data['error_homepage_block0_image1'] = $this->error['homepage_block0_image1'];
        } else {
            $data['error_homepage_block0_image1'] = '';
        }
        if (isset($this->error['homepage_block1_image2'])) {
            $data['error_homepage_block0_image2'] = $this->error['homepage_block0_image2'];
        } else {
            $data['error_homepage_block0_image2'] = '';
        }
        if (isset($this->error['homepage_block0_url1'])) {
            $data['error_homepage_block0_url1'] = $this->error['homepage_block0_url1'];
        } else {
            $data['error_homepage_block0_url1'] = '';
        }

        if (isset($this->error['homepage_block1_title1'])) {
            $data['error_homepage_block1_title1'] = $this->error['homepage_block1_title1'];
        } else {
            $data['error_homepage_block1_title1'] = '';
        }

        if (isset($this->error['homepage_block1_button1'])) {
            $data['error_homepage_block1_button1'] = $this->error['homepage_block1_button1'];
        } else {
            $data['error_homepage_block1_button1'] = '';
        }

        if (isset($this->error['homepage_block1_description1'])) {
            $data['error_homepage_block1_description1'] = $this->error['homepage_block1_description1'];
        } else {
            $data['error_homepage_block1_description1'] = '';
        }

        if (isset($this->error['homepage_block1_url1'])) {
            $data['error_homepage_block1_url1'] = $this->error['homepage_block1_url1'];
        } else {
            $data['error_homepage_block1_url1'] = '';
        }

        if (isset($this->error['homepage_block1_image1'])) {
            $data['error_homepage_block1_image1'] = $this->error['homepage_block1_image1'];
        } else {
            $data['error_homepage_block1_image1'] = '';
        }

        if (isset($this->error['homepage_block1_title2'])) {
            $data['error_homepage_block1_title2'] = $this->error['homepage_block1_title2'];
        } else {
            $data['error_homepage_block1_title2'] = '';
        }

        if (isset($this->error['homepage_block1_button2'])) {
            $data['error_homepage_block1_button2'] = $this->error['homepage_block1_button2'];
        } else {
            $data['error_homepage_block1_button2'] = '';
        }

        if (isset($this->error['homepage_block1_description2'])) {
            $data['error_homepage_block1_description2'] = $this->error['homepage_block1_description2'];
        } else {
            $data['error_homepage_block1_description2'] = '';
        }

        if (isset($this->error['homepage_block1_url2'])) {
            $data['error_homepage_block1_url2'] = $this->error['homepage_block1_url2'];
        } else {
            $data['error_homepage_block1_url2'] = '';
        }

        if (isset($this->error['homepage_block1_image2'])) {
            $data['error_homepage_block1_image2'] = $this->error['homepage_block1_image2'];
        } else {
            $data['error_homepage_block1_image2'] = '';
        }

        // block 3
        if (isset($this->error['homepage_block1_title3'])) {
            $data['error_homepage_block1_title3'] = $this->error['homepage_block1_title3'];
        } else {
            $data['error_homepage_block1_title3'] = '';
        }

        if (isset($this->error['homepage_block1_button3'])) {
            $data['error_homepage_block1_button3'] = $this->error['homepage_block1_button3'];
        } else {
            $data['error_homepage_block1_button3'] = '';
        }

        if (isset($this->error['homepage_block1_description3'])) {
            $data['error_homepage_block1_description3'] = $this->error['homepage_block1_description3'];
        } else {
            $data['error_homepage_block1_description3'] = '';
        }

        if (isset($this->error['homepage_block1_url3'])) {
            $data['error_homepage_block1_url3'] = $this->error['homepage_block1_url3'];
        } else {
            $data['error_homepage_block1_url3'] = '';
        }

        if (isset($this->error['homepage_block1_image3'])) {
            $data['error_homepage_block1_image3'] = $this->error['homepage_block1_image3'];
        } else {
            $data['error_homepage_block1_image3'] = '';
        }

        // block 4
        if (isset($this->error['homepage_block1_title4'])) {
            $data['error_homepage_block1_title4'] = $this->error['homepage_block1_title4'];
        } else {
            $data['error_homepage_block1_title4'] = '';
        }

        if (isset($this->error['homepage_block1_button4'])) {
            $data['error_homepage_block1_button4'] = $this->error['homepage_block1_button4'];
        } else {
            $data['error_homepage_block1_button4'] = '';
        }

        if (isset($this->error['homepage_block1_description4'])) {
            $data['error_homepage_block1_description4'] = $this->error['homepage_block1_description4'];
        } else {
            $data['error_homepage_block1_description4'] = '';
        }

        if (isset($this->error['homepage_block1_url4'])) {
            $data['error_homepage_block1_url4'] = $this->error['homepage_block1_url4'];
        } else {
            $data['error_homepage_block1_url4'] = '';
        }

        if (isset($this->error['homepage_block1_image4'])) {
            $data['error_homepage_block1_image4'] = $this->error['homepage_block1_image4'];
        } else {
            $data['error_homepage_block1_image4'] = '';
        }

        // block 5
        if (isset($this->error['homepage_block1_title5'])) {
            $data['error_homepage_block1_title5'] = $this->error['homepage_block1_title5'];
        } else {
            $data['error_homepage_block1_title5'] = '';
        }

        if (isset($this->error['homepage_block1_button5'])) {
            $data['error_homepage_block1_button5'] = $this->error['homepage_block1_button5'];
        } else {
            $data['error_homepage_block1_button5'] = '';
        }

        if (isset($this->error['homepage_block1_description5'])) {
            $data['error_homepage_block1_description5'] = $this->error['homepage_block1_description5'];
        } else {
            $data['error_homepage_block1_description5'] = '';
        }

        if (isset($this->error['homepage_block1_url5'])) {
            $data['error_homepage_block1_url5'] = $this->error['homepage_block1_url5'];
        } else {
            $data['error_homepage_block1_url5'] = '';
        }

        if (isset($this->error['homepage_block1_image5'])) {
            $data['error_homepage_block1_image5'] = $this->error['homepage_block1_image5'];
        } else {
            $data['error_homepage_block1_image5'] = '';
        }

        // tab3 block 1
        if (isset($this->error['homepage_block3_title'])) {
            $data['error_homepage_block3_title'] = $this->error['homepage_block3_title'];
        } else {
            $data['error_homepage_block3_title'] = '';
        }

        if (isset($this->error['homepage_block3_description'])) {
            $data['error_homepage_block3_description'] = $this->error['homepage_block3_description'];
        } else {
            $data['error_homepage_block3_description'] = '';
        }

        if (isset($this->error['homepage_block3_title1'])) {
            $data['error_homepage_block3_title1'] = $this->error['homepage_block3_title1'];
        } else {
            $data['error_homepage_block3_title1'] = '';
        }

        if (isset($this->error['homepage_block3_description1'])) {
            $data['error_homepage_block3_description1'] = $this->error['homepage_block3_description1'];
        } else {
            $data['error_homepage_block3_description1'] = '';
        }

        if (isset($this->error['homepage_block3_url1'])) {
            $data['error_homepage_block3_url1'] = $this->error['homepage_block3_url1'];
        } else {
            $data['error_homepage_block3_url1'] = '';
        }

        if (isset($this->error['homepage_block3_image1'])) {
            $data['error_homepage_block3_image1'] = $this->error['homepage_block3_image1'];
        } else {
            $data['error_homepage_block3_image1'] = '';
        }

        // tab3 block 2
        if (isset($this->error['homepage_block3_title2'])) {
            $data['error_homepage_block3_title2'] = $this->error['homepage_block3_title2'];
        } else {
            $data['error_homepage_block3_title2'] = '';
        }

        if (isset($this->error['homepage_block3_description2'])) {
            $data['error_homepage_block3_description2'] = $this->error['homepage_block3_description2'];
        } else {
            $data['error_homepage_block3_description2'] = '';
        }

        if (isset($this->error['homepage_block3_url2'])) {
            $data['error_homepage_block3_url2'] = $this->error['homepage_block3_url2'];
        } else {
            $data['error_homepage_block3_url2'] = '';
        }

        if (isset($this->error['homepage_block3_image2'])) {
            $data['error_homepage_block3_image2'] = $this->error['homepage_block3_image2'];
        } else {
            $data['error_homepage_block3_image2'] = '';
        }

        // tab3 block 3
        if (isset($this->error['homepage_block3_title3'])) {
            $data['error_homepage_block3_title3'] = $this->error['homepage_block3_title3'];
        } else {
            $data['error_homepage_block3_title3'] = '';
        }

        if (isset($this->error['homepage_block3_description3'])) {
            $data['error_homepage_block3_description3'] = $this->error['homepage_block3_description3'];
        } else {
            $data['error_homepage_block3_description3'] = '';
        }

        if (isset($this->error['homepage_block3_url3'])) {
            $data['error_homepage_block3_url3'] = $this->error['homepage_block3_url3'];
        } else {
            $data['error_homepage_block3_url3'] = '';
        }

        if (isset($this->error['homepage_block3_image3'])) {
            $data['error_homepage_block3_image3'] = $this->error['homepage_block3_image3'];
        } else {
            $data['error_homepage_block3_image3'] = '';
        }

        // tab3 block 4
        if (isset($this->error['homepage_block3_title4'])) {
            $data['error_homepage_block3_title4'] = $this->error['homepage_block3_title4'];
        } else {
            $data['error_homepage_block3_title4'] = '';
        }

        if (isset($this->error['homepage_block3_description4'])) {
            $data['error_homepage_block3_description4'] = $this->error['homepage_block3_description4'];
        } else {
            $data['error_homepage_block3_description4'] = '';
        }

        if (isset($this->error['homepage_block3_url4'])) {
            $data['error_homepage_block3_url4'] = $this->error['homepage_block3_url4'];
        } else {
            $data['error_homepage_block3_url4'] = '';
        }

        if (isset($this->error['homepage_block3_image4'])) {
            $data['error_homepage_block3_image4'] = $this->error['homepage_block3_image4'];
        } else {
            $data['error_homepage_block3_image4'] = '';
        }


        // tab4 block 1
        if (isset($this->error['homepage_block4_title'])) {
            $data['error_homepage_block4_title'] = $this->error['homepage_block4_title'];
        } else {
            $data['error_homepage_block4_title'] = '';
        }

        if (isset($this->error['homepage_block4_description'])) {
            $data['error_homepage_block4_description'] = $this->error['homepage_block4_description'];
        } else {
            $data['error_homepage_block4_description'] = '';
        }

        if (isset($this->error['homepage_block4_title1'])) {
            $data['error_homepage_block4_title1'] = $this->error['homepage_block4_title1'];
        } else {
            $data['error_homepage_block4_title1'] = '';
        }

        if (isset($this->error['homepage_block4_description1'])) {
            $data['error_homepage_block4_description1'] = $this->error['homepage_block4_description1'];
        } else {
            $data['error_homepage_block4_description1'] = '';
        }

        if (isset($this->error['homepage_block4_url1'])) {
            $data['error_homepage_block4_url1'] = $this->error['homepage_block4_url1'];
        } else {
            $data['error_homepage_block4_url1'] = '';
        }

        if (isset($this->error['homepage_block4_image1'])) {
            $data['error_homepage_block4_image1'] = $this->error['homepage_block4_image1'];
        } else {
            $data['error_homepage_block4_image1'] = '';
        }

        // tab3 block 2
        if (isset($this->error['homepage_block4_title2'])) {
            $data['error_homepage_block4_title2'] = $this->error['homepage_block4_title2'];
        } else {
            $data['error_homepage_block4_title2'] = '';
        }

        if (isset($this->error['homepage_block4_description2'])) {
            $data['error_homepage_block4_description2'] = $this->error['homepage_block4_description2'];
        } else {
            $data['error_homepage_block4_description2'] = '';
        }

        if (isset($this->error['homepage_block4_url2'])) {
            $data['error_homepage_block4_url2'] = $this->error['homepage_block4_url2'];
        } else {
            $data['error_homepage_block4_url2'] = '';
        }

        if (isset($this->error['homepage_block4_image2'])) {
            $data['error_homepage_block4_image2'] = $this->error['homepage_block4_image2'];
        } else {
            $data['error_homepage_block4_image2'] = '';
        }

        // tab3 block 3
        if (isset($this->error['homepage_block4_title3'])) {
            $data['error_homepage_block4_title3'] = $this->error['homepage_block4_title3'];
        } else {
            $data['error_homepage_block4_title3'] = '';
        }

        if (isset($this->error['homepage_block4_description3'])) {
            $data['error_homepage_block4_description3'] = $this->error['homepage_block4_description3'];
        } else {
            $data['error_homepage_block4_description3'] = '';
        }

        if (isset($this->error['homepage_block4_url3'])) {
            $data['error_homepage_block4_url3'] = $this->error['homepage_block4_url3'];
        } else {
            $data['error_homepage_block4_url3'] = '';
        }

        if (isset($this->error['homepage_block4_image3'])) {
            $data['error_homepage_block4_image3'] = $this->error['homepage_block4_image3'];
        } else {
            $data['error_homepage_block4_image3'] = '';
        }

        // tab3 block 4
        if (isset($this->error['homepage_block4_title4'])) {
            $data['error_homepage_block4_title4'] = $this->error['homepage_block4_title4'];
        } else {
            $data['error_homepage_block4_title4'] = '';
        }

        if (isset($this->error['homepage_block4_description4'])) {
            $data['error_homepage_block4_description4'] = $this->error['homepage_block4_description4'];
        } else {
            $data['error_homepage_block4_description4'] = '';
        }

        if (isset($this->error['homepage_block4_url4'])) {
            $data['error_homepage_block4_url4'] = $this->error['homepage_block4_url4'];
        } else {
            $data['error_homepage_block4_url4'] = '';
        }

        if (isset($this->error['homepage_block4_image4'])) {
            $data['error_homepage_block4_image4'] = $this->error['homepage_block4_image4'];
        } else {
            $data['error_homepage_block4_image4'] = '';
        }

        if (isset($this->error['homepage_brand1_id'])) {
            $data['error_homepage_brand1_id'] = $this->error['homepage_brand1_id'];
        } else {
            $data['error_homepage_brand1_id'] = '';
        }
        if (isset($this->error['homepage_brand1_name'])) {
            $data['error_homepage_brand1_name'] = $this->error['homepage_brand1_name'];
        } else {
            $data['error_homepage_brand1_name'] = '';
        }
        if (isset($this->error['homepage_brand1_desc'])) {
            $data['error_homepage_brand1_desc'] = $this->error['homepage_brand1_desc'];
        } else {
            $data['error_homepage_brand1_desc'] = '';
        }
        if (isset($this->error['homepage_brand1_button'])) {
            $data['error_homepage_brand1_button'] = $this->error['homepage_brand1_button'];
        } else {
            $data['error_homepage_brand1_button'] = '';
        }
        if (isset($this->error['homepage_block5_image1'])) {
            $data['error_homepage_block5_image1'] = $this->error['homepage_block5_image1'];
        } else {
            $data['error_homepage_block5_image1'] = '';
        }
        if (isset($this->error['homepage_brand2_id'])) {
            $data['error_homepage_brand2_id'] = $this->error['homepage_brand2_id'];
        } else {
            $data['error_homepage_brand2_id'] = '';
        }
        if (isset($this->error['homepage_brand2_name'])) {
            $data['error_homepage_brand2_name'] = $this->error['homepage_brand2_name'];
        } else {
            $data['error_homepage_brand2_name'] = '';
        }
        if (isset($this->error['homepage_brand2_desc'])) {
            $data['error_homepage_brand2_desc'] = $this->error['homepage_brand2_desc'];
        } else {
            $data['error_homepage_brand2_desc'] = '';
        }
        if (isset($this->error['homepage_brand2_button'])) {
            $data['error_homepage_brand2_button'] = $this->error['homepage_brand2_button'];
        } else {
            $data['error_homepage_brand2_button'] = '';
        }
        if (isset($this->error['homepage_block5_image2'])) {
            $data['error_homepage_block5_image2'] = $this->error['homepage_block5_image2'];
        } else {
            $data['error_homepage_block5_image2'] = '';
        }
        if (isset($this->error['homepage_brand3_id'])) {
            $data['error_homepage_brand3_id'] = $this->error['homepage_brand3_id'];
        } else {
            $data['error_homepage_brand3_id'] = '';
        }
        if (isset($this->error['homepage_brand3_name'])) {
            $data['error_homepage_brand3_name'] = $this->error['homepage_brand3_name'];
        } else {
            $data['error_homepage_brand3_name'] = '';
        }
        if (isset($this->error['homepage_brand3_desc'])) {
            $data['error_homepage_brand3_desc'] = $this->error['homepage_brand3_desc'];
        } else {
            $data['error_homepage_brand3_desc'] = '';
        }
        if (isset($this->error['homepage_brand3_button'])) {
            $data['error_homepage_brand3_button'] = $this->error['homepage_brand3_button'];
        } else {
            $data['error_homepage_brand3_button'] = '';
        }
        if (isset($this->error['homepage_block5_image3'])) {
            $data['error_homepage_block5_image3'] = $this->error['homepage_block5_image3'];
        } else {
            $data['error_homepage_block5_image3'] = '';
        }

        $this->load->model('homepage/homepage');

        $page_id = '';
        $url_page = '';
        $data['product_relateds'] = array();
        if (isset($this->request->get['page_id'])) {
            $page_id = $this->request->get['page_id'];
            $url_page = '&page_id='.$page_id;
            $data['product_relateds'] = $this->model_homepage_homepage->getPageProduct($page_id,'homepage');

        }

        $data['product_offers'] = array();
        if (isset($this->request->get['page_id'])) {
            $page_id = $this->request->get['page_id'];
            $url_page = '&page_id='.$page_id;
            $data['product_offers'] = $this->model_homepage_homepage->getPageOffersProduct($page_id,'homepage');

        }
        $this->load->model('tool/image');
        $this->load->model('localisation/country');
        $this->load->model('localisation/language');

        $data['available_coutnries'] = $this->model_localisation_country->getAvailableCountries();


        $data['text_form'] = $this->language->get('text_edit');
        $data['action'] = $this->url->link('homepage/homepage/'.$type, 'token=' . $this->session->data['token'] .$url_page, 'SSL');
        $data['cancel'] = $this->url->link('homepage/homepage', 'token=' . $this->session->data['token'], 'SSL');
        $data['selectedproducts'] = $this->url->link('cms/homepageFeaturedProducts', 'token=' . $this->session->data['token'], 'SSL');
        $data['offersproducts'] = $this->url->link('cms/homepageOffersProducts', 'token=' . $this->session->data['token'], 'SSL');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('homepage/homepage', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_settings'),
            'href' => $this->url->link('homepage/homepage/'.$type, 'token=' . $this->session->data['token'] . $url_page, 'SSL')
        );


        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        $page_countries = array ();
        if (isset($this->request->get['page_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $this->load->model('homepage/homepage');

            $country_language_info = $this->model_homepage_homepage->getSetting('homepage', $page_id);
            $page_countries = $this->model_homepage_homepage->getPageCountries('homepage', $page_id);

        }
        $data['page_language'] = 1;
        if(!empty($page_countries)){
          $data['page_language']= array_values(array_unique($page_countries))[0];
        }

        $data['page_countries'] = $page_countries;


        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['homepage_block0_image1'])) {
            $data['homepage_block0_image1'] = $this->request->post['homepage_block0_image1'];
        } elseif (isset($country_language_info['homepage_block0_image1'])) {
            $data['homepage_block0_image1'] = $country_language_info['homepage_block0_image1'];
        } else {
            $data['homepage_block0_image1'] = '';
        }
        if (isset($this->request->post['homepage_block0_image1']) && is_file(DIR_IMAGE . $this->request->post['homepage_block0_image1'])) {
            $data['thumb_01'] = $this->model_tool_image->resize($this->request->post['homepage_block0_image1'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block0_image1']) && is_file(DIR_IMAGE . $country_language_info['homepage_block0_image1'])) {
            $data['thumb_01'] = $this->model_tool_image->resize($country_language_info['homepage_block0_image1'], 100, 100, false);
        } else {
            $data['thumb_01'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }
        if (isset($this->request->post['homepage_block0_image2'])) {
            $data['homepage_block0_image2'] = $this->request->post['homepage_block0_image2'];
        } elseif (isset($country_language_info['homepage_block0_image2'])) {
            $data['homepage_block0_image2'] = $country_language_info['homepage_block0_image2'];
        } else {
            $data['homepage_block0_image2'] = '';
        }
        if (isset($this->request->post['homepage_block0_image2']) && is_file(DIR_IMAGE . $this->request->post['homepage_block0_image2'])) {
            $data['thumb_02'] = $this->model_tool_image->resize($this->request->post['homepage_block0_image2'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block0_image2']) && is_file(DIR_IMAGE . $country_language_info['homepage_block0_image2'])) {
            $data['thumb_02'] = $this->model_tool_image->resize($country_language_info['homepage_block0_image2'], 100, 100, false);
        } else {
            $data['thumb_02'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }
        if (isset($this->request->post['homepage_block0_url1'])) {
            $data['homepage_block0_url1'] = $this->request->post['homepage_block0_url1'];
        } elseif (isset($country_language_info['homepage_block0_url1'])) {
            $data['homepage_block0_url1'] = $country_language_info['homepage_block0_url1'];
        } else {
            $data['homepage_block0_url1'] = '';
        }

        if (isset($this->request->post['homepage_block1_title1'])) {
            $data['homepage_block1_title1'] = $this->request->post['homepage_block1_title1'];
        } elseif (isset($country_language_info['homepage_block1_title1'])) {
            $data['homepage_block1_title1'] = $country_language_info['homepage_block1_title1'];
        } else {
            $data['homepage_block1_title1'] = '';
        }
        if (isset($this->request->post['homepage_block1_button1'])) {
            $data['homepage_block1_button1'] = $this->request->post['homepage_block1_button1'];
        } elseif (isset($country_language_info['homepage_block1_button1'])) {
            $data['homepage_block1_button1'] = $country_language_info['homepage_block1_button1'];
        } else {
            $data['homepage_block1_button1'] = '';
        }
        if (isset($this->request->post['homepage_block1_description1'])) {
            $data['homepage_block1_description1'] = $this->request->post['homepage_block1_description1'];
        } elseif (isset($country_language_info['homepage_block1_description1'])) {
            $data['homepage_block1_description1'] = $country_language_info['homepage_block1_description1'];
        } else {
            $data['homepage_block1_description1'] = '';
        }
        if (isset($this->request->post['homepage_block1_url1'])) {
            $data['homepage_block1_url1'] = $this->request->post['homepage_block1_url1'];
        } elseif (isset($country_language_info['homepage_block1_url1'])) {
            $data['homepage_block1_url1'] = $country_language_info['homepage_block1_url1'];
        } else {
            $data['homepage_block1_url1'] = '';
        }
        if (isset($this->request->post['homepage_block1_image1'])) {
            $data['homepage_block1_image1'] = $this->request->post['homepage_block1_image1'];
        } elseif (isset($country_language_info['homepage_block1_image1'])) {
            $data['homepage_block1_image1'] = $country_language_info['homepage_block1_image1'];
        } else {
            $data['homepage_block1_image1'] = '';
        }
        if (isset($this->request->post['homepage_block1_image1']) && is_file(DIR_IMAGE . $this->request->post['homepage_block1_image1'])) {
            $data['thumb_11'] = $this->model_tool_image->resize($this->request->post['homepage_block1_image1'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block1_image1']) && is_file(DIR_IMAGE . $country_language_info['homepage_block1_image1'])) {
            $data['thumb_11'] = $this->model_tool_image->resize($country_language_info['homepage_block1_image1'], 100, 100, false);
        } else {
            $data['thumb_11'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        if (isset($this->request->post['homepage_block1_title2'])) {
            $data['homepage_block1_title2'] = $this->request->post['homepage_block1_title2'];
        } elseif (isset($country_language_info['homepage_block1_title2'])) {
            $data['homepage_block1_title2'] = $country_language_info['homepage_block1_title2'];
        } else {
            $data['homepage_block1_title2'] = '';
        }
        if (isset($this->request->post['homepage_block1_button2'])) {
            $data['homepage_block1_button2'] = $this->request->post['homepage_block1_button2'];
        } elseif (isset($country_language_info['homepage_block1_button2'])) {
            $data['homepage_block1_button2'] = $country_language_info['homepage_block1_button2'];
        } else {
            $data['homepage_block1_button2'] = '';
        }
        if (isset($this->request->post['homepage_block1_description2'])) {
            $data['homepage_block1_description2'] = $this->request->post['homepage_block1_description2'];
        } elseif (isset($country_language_info['homepage_block1_description2'])) {
            $data['homepage_block1_description2'] = $country_language_info['homepage_block1_description2'];
        } else {
            $data['homepage_block1_description2'] = '';
        }
        if (isset($this->request->post['homepage_block1_url2'])) {
            $data['homepage_block1_url2'] = $this->request->post['homepage_block1_url2'];
        } elseif (isset($country_language_info['homepage_block1_url2'])) {
            $data['homepage_block1_url2'] = $country_language_info['homepage_block1_url2'];
        } else {
            $data['homepage_block1_url2'] = '';
        }
        if (isset($this->request->post['homepage_block1_image2'])) {
            $data['homepage_block1_image2'] = $this->request->post['homepage_block1_image2'];
        } elseif (isset($country_language_info['homepage_block1_image2'])) {
            $data['homepage_block1_image2'] = $country_language_info['homepage_block1_image2'];
        } else {
            $data['homepage_block1_image2'] = '';
        }
        if (isset($this->request->post['homepage_block1_image2']) && is_file(DIR_IMAGE . $this->request->post['homepage_block1_image2'])) {
            $data['thumb_12'] = $this->model_tool_image->resize($this->request->post['homepage_block1_image2'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block1_image2']) && is_file(DIR_IMAGE . $country_language_info['homepage_block1_image2'])) {
            $data['thumb_12'] = $this->model_tool_image->resize($country_language_info['homepage_block1_image2'], 100, 100, false);
        } else {
            $data['thumb_12'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        if (isset($this->request->post['homepage_block1_title3'])) {
            $data['homepage_block1_title3'] = $this->request->post['homepage_block1_title3'];
        } elseif (isset($country_language_info['homepage_block1_title3'])) {
            $data['homepage_block1_title3'] = $country_language_info['homepage_block1_title3'];
        } else {
            $data['homepage_block1_title3'] = '';
        }
        if (isset($this->request->post['homepage_block1_button3'])) {
            $data['homepage_block1_button3'] = $this->request->post['homepage_block1_button3'];
        } elseif (isset($country_language_info['homepage_block1_button3'])) {
            $data['homepage_block1_button3'] = $country_language_info['homepage_block1_button3'];
        } else {
            $data['homepage_block1_button3'] = '';
        }
        if (isset($this->request->post['homepage_block1_description3'])) {
            $data['homepage_block1_description3'] = $this->request->post['homepage_block1_description3'];
        } elseif (isset($country_language_info['homepage_block1_description3'])) {
            $data['homepage_block1_description3'] = $country_language_info['homepage_block1_description3'];
        } else {
            $data['homepage_block1_description3'] = '';
        }
        if (isset($this->request->post['homepage_block1_url3'])) {
            $data['homepage_block1_url3'] = $this->request->post['homepage_block1_url3'];
        } elseif (isset($country_language_info['homepage_block1_url3'])) {
            $data['homepage_block1_url3'] = $country_language_info['homepage_block1_url3'];
        } else {
            $data['homepage_block1_url3'] = '';
        }
        if (isset($this->request->post['homepage_block1_image3'])) {
            $data['homepage_block1_image3'] = $this->request->post['homepage_block1_image3'];
        } elseif (isset($country_language_info['homepage_block1_image3'])) {
            $data['homepage_block1_image3'] = $country_language_info['homepage_block1_image3'];
        } else {
            $data['homepage_block1_image3'] = '';
        }
        if (isset($this->request->post['homepage_block1_image3']) && is_file(DIR_IMAGE . $this->request->post['homepage_block1_image3'])) {
            $data['thumb_13'] = $this->model_tool_image->resize($this->request->post['homepage_block1_image3'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block1_image3']) && is_file(DIR_IMAGE . $country_language_info['homepage_block1_image3'])) {
            $data['thumb_13'] = $this->model_tool_image->resize($country_language_info['homepage_block1_image3'], 100, 100, false);
        } else {
            $data['thumb_13'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        if (isset($this->request->post['homepage_block1_title4'])) {
            $data['homepage_block1_title4'] = $this->request->post['homepage_block1_title4'];
        } elseif (isset($country_language_info['homepage_block1_title4'])) {
            $data['homepage_block1_title4'] = $country_language_info['homepage_block1_title4'];
        } else {
            $data['homepage_block1_title4'] = '';
        }
        if (isset($this->request->post['homepage_block1_button4'])) {
            $data['homepage_block1_button4'] = $this->request->post['homepage_block1_button4'];
        } elseif (isset($country_language_info['homepage_block1_button4'])) {
            $data['homepage_block1_button4'] = $country_language_info['homepage_block1_button4'];
        } else {
            $data['homepage_block1_button4'] = '';
        }
        if (isset($this->request->post['homepage_block1_description4'])) {
            $data['homepage_block1_description4'] = $this->request->post['homepage_block1_description4'];
        } elseif (isset($country_language_info['homepage_block1_description4'])) {
            $data['homepage_block1_description4'] = $country_language_info['homepage_block1_description4'];
        } else {
            $data['homepage_block1_description4'] = '';
        }
        if (isset($this->request->post['homepage_block1_url4'])) {
            $data['homepage_block1_url4'] = $this->request->post['homepage_block1_url4'];
        } elseif (isset($country_language_info['homepage_block1_url4'])) {
            $data['homepage_block1_url4'] = $country_language_info['homepage_block1_url4'];
        } else {
            $data['homepage_block1_url4'] = '';
        }
        if (isset($this->request->post['homepage_block1_image4'])) {
            $data['homepage_block1_image4'] = $this->request->post['homepage_block1_image4'];
        } elseif (isset($country_language_info['homepage_block1_image4'])) {
            $data['homepage_block1_image4'] = $country_language_info['homepage_block1_image4'];
        } else {
            $data['homepage_block1_image4'] = '';
        }
        if (isset($this->request->post['homepage_block1_image4']) && is_file(DIR_IMAGE . $this->request->post['homepage_block1_image4'])) {
            $data['thumb_14'] = $this->model_tool_image->resize($this->request->post['homepage_block1_image4'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block1_image4']) && is_file(DIR_IMAGE . $country_language_info['homepage_block1_image4'])) {
            $data['thumb_14'] = $this->model_tool_image->resize($country_language_info['homepage_block1_image4'], 100, 100, false);
        } else {
            $data['thumb_14'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        if (isset($this->request->post['homepage_block1_title5'])) {
            $data['homepage_block1_title5'] = $this->request->post['homepage_block1_title5'];
        } elseif (isset($country_language_info['homepage_block1_title5'])) {
            $data['homepage_block1_title5'] = $country_language_info['homepage_block1_title5'];
        } else {
            $data['homepage_block1_title5'] = '';
        }
        if (isset($this->request->post['homepage_block1_button5'])) {
            $data['homepage_block1_button5'] = $this->request->post['homepage_block1_button5'];
        } elseif (isset($country_language_info['homepage_block1_button5'])) {
            $data['homepage_block1_button5'] = $country_language_info['homepage_block1_button5'];
        } else {
            $data['homepage_block1_button5'] = '';
        }
        if (isset($this->request->post['homepage_block1_description5'])) {
            $data['homepage_block1_description5'] = $this->request->post['homepage_block1_description5'];
        } elseif (isset($country_language_info['homepage_block1_description5'])) {
            $data['homepage_block1_description5'] = $country_language_info['homepage_block1_description5'];
        } else {
            $data['homepage_block1_description5'] = '';
        }
        if (isset($this->request->post['homepage_block1_url5'])) {
            $data['homepage_block1_url5'] = $this->request->post['homepage_block1_url5'];
        } elseif (isset($country_language_info['homepage_block1_url5'])) {
            $data['homepage_block1_url5'] = $country_language_info['homepage_block1_url5'];
        } else {
            $data['homepage_block1_url5'] = '';
        }
        if (isset($this->request->post['homepage_block1_image5'])) {
            $data['homepage_block1_image5'] = $this->request->post['homepage_block1_image5'];
        } elseif (isset($country_language_info['homepage_block1_image5'])) {
            $data['homepage_block1_image5'] = $country_language_info['homepage_block1_image5'];
        } else {
            $data['homepage_block1_image5'] = '';
        }
        if (isset($this->request->post['homepage_block1_image5']) && is_file(DIR_IMAGE . $this->request->post['homepage_block1_image5'])) {
            $data['thumb_15'] = $this->model_tool_image->resize($this->request->post['homepage_block1_image5'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block1_image5']) && is_file(DIR_IMAGE . $country_language_info['homepage_block1_image5'])) {
            $data['thumb_15'] = $this->model_tool_image->resize($country_language_info['homepage_block1_image5'], 100, 100, false);
        } else {
            $data['thumb_15'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        //tab3 // block 1
        if (isset($this->request->post['homepage_block3_title'])) {
            $data['homepage_block3_title'] = $this->request->post['homepage_block3_title'];
        } elseif (isset($country_language_info['homepage_block3_title'])) {
            $data['homepage_block3_title'] = $country_language_info['homepage_block3_title'];
        } else {
            $data['homepage_block3_title'] = '';
        }
        if (isset($this->request->post['homepage_block3_description'])) {
            $data['homepage_block3_description'] = $this->request->post['homepage_block3_description'];
        } elseif (isset($country_language_info['homepage_block3_description'])) {
            $data['homepage_block3_description'] = $country_language_info['homepage_block3_description'];
        } else {
            $data['homepage_block3_description'] = '';
        }
        if (isset($this->request->post['homepage_block3_title1'])) {
            $data['homepage_block3_title1'] = $this->request->post['homepage_block3_title1'];
        } elseif (isset($country_language_info['homepage_block3_title1'])) {
            $data['homepage_block3_title1'] = $country_language_info['homepage_block3_title1'];
        } else {
            $data['homepage_block3_title1'] = '';
        }
        if (isset($this->request->post['homepage_block3_description1'])) {
            $data['homepage_block3_description1'] = $this->request->post['homepage_block3_description1'];
        } elseif (isset($country_language_info['homepage_block3_description1'])) {
            $data['homepage_block3_description1'] = $country_language_info['homepage_block3_description1'];
        } else {
            $data['homepage_block3_description1'] = '';
        }
        if (isset($this->request->post['homepage_block3_url1'])) {
            $data['homepage_block3_url1'] = $this->request->post['homepage_block3_url1'];
        } elseif (isset($country_language_info['homepage_block3_url1'])) {
            $data['homepage_block3_url1'] = $country_language_info['homepage_block3_url1'];
        } else {
            $data['homepage_block3_url1'] = '';
        }
        if (isset($this->request->post['homepage_block3_image1'])) {
            $data['homepage_block3_image1'] = $this->request->post['homepage_block3_image1'];
        } elseif (isset($country_language_info['homepage_block3_image1'])) {
            $data['homepage_block3_image1'] = $country_language_info['homepage_block3_image1'];
        } else {
            $data['homepage_block3_image1'] = '';
        }
        if (isset($this->request->post['homepage_block3_image1']) && is_file(DIR_IMAGE . $this->request->post['homepage_block3_image1'])) {
            $data['thumb_31'] = $this->model_tool_image->resize($this->request->post['homepage_block3_image1'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block3_image1']) && is_file(DIR_IMAGE . $country_language_info['homepage_block3_image1'])) {
            $data['thumb_31'] = $this->model_tool_image->resize($country_language_info['homepage_block3_image1'], 100, 100, false);
        } else {
            $data['thumb_31'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        //tab3 // block 2
        if (isset($this->request->post['homepage_block3_title2'])) {
            $data['homepage_block3_title2'] = $this->request->post['homepage_block3_title2'];
        } elseif (isset($country_language_info['homepage_block3_title2'])) {
            $data['homepage_block3_title2'] = $country_language_info['homepage_block3_title2'];
        } else {
            $data['homepage_block3_title2'] = '';
        }
        if (isset($this->request->post['homepage_block3_description2'])) {
            $data['homepage_block3_description2'] = $this->request->post['homepage_block3_description2'];
        } elseif (isset($country_language_info['homepage_block3_description2'])) {
            $data['homepage_block3_description2'] = $country_language_info['homepage_block3_description2'];
        } else {
            $data['homepage_block3_description2'] = '';
        }
        if (isset($this->request->post['homepage_block3_url2'])) {
            $data['homepage_block3_url2'] = $this->request->post['homepage_block3_url2'];
        } elseif (isset($country_language_info['homepage_block1_url2'])) {
            $data['homepage_block3_url2'] = $country_language_info['homepage_block3_url2'];
        } else {
            $data['homepage_block3_url2'] = '';
        }
        if (isset($this->request->post['homepage_block3_image2'])) {
            $data['homepage_block3_image2'] = $this->request->post['homepage_block3_image2'];
        } elseif (isset($country_language_info['homepage_block3_image2'])) {
            $data['homepage_block3_image2'] = $country_language_info['homepage_block3_image2'];
        } else {
            $data['homepage_block3_image2'] = '';
        }
        if (isset($this->request->post['homepage_block3_image2']) && is_file(DIR_IMAGE . $this->request->post['homepage_block3_image2'])) {
            $data['thumb_32'] = $this->model_tool_image->resize($this->request->post['homepage_block3_image2'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block3_image2']) && is_file(DIR_IMAGE . $country_language_info['homepage_block3_image2'])) {
            $data['thumb_32'] = $this->model_tool_image->resize($country_language_info['homepage_block3_image2'], 100, 100, false);
        } else {
            $data['thumb_32'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        //tab3 // block 3
        if (isset($this->request->post['homepage_block3_title3'])) {
            $data['homepage_block3_title3'] = $this->request->post['homepage_block3_title3'];
        } elseif (isset($country_language_info['homepage_block3_title3'])) {
            $data['homepage_block3_title3'] = $country_language_info['homepage_block3_title3'];
        } else {
            $data['homepage_block3_title3'] = '';
        }
        if (isset($this->request->post['homepage_block3_description3'])) {
            $data['homepage_block3_description3'] = $this->request->post['homepage_block3_description3'];
        } elseif (isset($country_language_info['homepage_block3_description3'])) {
            $data['homepage_block3_description3'] = $country_language_info['homepage_block3_description3'];
        } else {
            $data['homepage_block3_description3'] = '';
        }
        if (isset($this->request->post['homepage_block3_url3'])) {
            $data['homepage_block3_url3'] = $this->request->post['homepage_block3_url3'];
        } elseif (isset($country_language_info['homepage_block1_url3'])) {
            $data['homepage_block3_url3'] = $country_language_info['homepage_block3_url3'];
        } else {
            $data['homepage_block3_url3'] = '';
        }
        if (isset($this->request->post['homepage_block3_image3'])) {
            $data['homepage_block3_image3'] = $this->request->post['homepage_block3_image3'];
        } elseif (isset($country_language_info['homepage_block3_image3'])) {
            $data['homepage_block3_image3'] = $country_language_info['homepage_block3_image3'];
        } else {
            $data['homepage_block3_image3'] = '';
        }
        if (isset($this->request->post['homepage_block3_image3']) && is_file(DIR_IMAGE . $this->request->post['homepage_block3_image3'])) {
            $data['thumb_33'] = $this->model_tool_image->resize($this->request->post['homepage_block3_image3'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block3_image3']) && is_file(DIR_IMAGE . $country_language_info['homepage_block3_image3'])) {
            $data['thumb_33'] = $this->model_tool_image->resize($country_language_info['homepage_block3_image3'], 100, 100, false);
        } else {
            $data['thumb_33'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        //tab3 // block 4
        if (isset($this->request->post['homepage_block3_title4'])) {
            $data['homepage_block3_title4'] = $this->request->post['homepage_block3_title4'];
        } elseif (isset($country_language_info['homepage_block3_title4'])) {
            $data['homepage_block3_title4'] = $country_language_info['homepage_block3_title4'];
        } else {
            $data['homepage_block3_title4'] = '';
        }
        if (isset($this->request->post['homepage_block3_description4'])) {
            $data['homepage_block3_description4'] = $this->request->post['homepage_block3_description4'];
        } elseif (isset($country_language_info['homepage_block3_description4'])) {
            $data['homepage_block3_description4'] = $country_language_info['homepage_block3_description4'];
        } else {
            $data['homepage_block3_description4'] = '';
        }
        if (isset($this->request->post['homepage_block3_url4'])) {
            $data['homepage_block3_url4'] = $this->request->post['homepage_block3_url4'];
        } elseif (isset($country_language_info['homepage_block1_url4'])) {
            $data['homepage_block3_url4'] = $country_language_info['homepage_block3_url4'];
        } else {
            $data['homepage_block3_url4'] = '';
        }
        if (isset($this->request->post['homepage_block3_image4'])) {
            $data['homepage_block3_image4'] = $this->request->post['homepage_block3_image4'];
        } elseif (isset($country_language_info['homepage_block3_image4'])) {
            $data['homepage_block3_image4'] = $country_language_info['homepage_block3_image4'];
        } else {
            $data['homepage_block3_image4'] = '';
        }
        if (isset($this->request->post['homepage_block3_image4']) && is_file(DIR_IMAGE . $this->request->post['homepage_block3_image4'])) {
            $data['thumb_34'] = $this->model_tool_image->resize($this->request->post['homepage_block3_image4'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block3_image4']) && is_file(DIR_IMAGE . $country_language_info['homepage_block3_image4'])) {
            $data['thumb_34'] = $this->model_tool_image->resize($country_language_info['homepage_block3_image4'], 100, 100, false);
        } else {
            $data['thumb_34'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        //tab4 // block 1
        if (isset($this->request->post['homepage_block4_title'])) {
            $data['homepage_block4_title'] = $this->request->post['homepage_block4_title'];
        } elseif (isset($country_language_info['homepage_block4_title'])) {
            $data['homepage_block4_title'] = $country_language_info['homepage_block4_title'];
        } else {
            $data['homepage_block4_title'] = '';
        }
        if (isset($this->request->post['homepage_block4_description'])) {
            $data['homepage_block4_description'] = $this->request->post['homepage_block4_description'];
        } elseif (isset($country_language_info['homepage_block4_description'])) {
            $data['homepage_block4_description'] = $country_language_info['homepage_block4_description'];
        } else {
            $data['homepage_block4_description'] = '';
        }
        if (isset($this->request->post['homepage_block4_title1'])) {
            $data['homepage_block4_title1'] = $this->request->post['homepage_block4_title1'];
        } elseif (isset($country_language_info['homepage_block4_title1'])) {
            $data['homepage_block4_title1'] = $country_language_info['homepage_block4_title1'];
        } else {
            $data['homepage_block4_title1'] = '';
        }
        if (isset($this->request->post['homepage_block4_description1'])) {
            $data['homepage_block4_description1'] = $this->request->post['homepage_block4_description1'];
        } elseif (isset($country_language_info['homepage_block4_description1'])) {
            $data['homepage_block4_description1'] = $country_language_info['homepage_block4_description1'];
        } else {
            $data['homepage_block4_description1'] = '';
        }
        if (isset($this->request->post['homepage_block4_url1'])) {
            $data['homepage_block4_url1'] = $this->request->post['homepage_block4_url1'];
        } elseif (isset($country_language_info['homepage_block4_url1'])) {
            $data['homepage_block4_url1'] = $country_language_info['homepage_block4_url1'];
        } else {
            $data['homepage_block4_url1'] = '';
        }
        if (isset($this->request->post['homepage_block4_image1'])) {
            $data['homepage_block4_image1'] = $this->request->post['homepage_block4_image1'];
        } elseif (isset($country_language_info['homepage_block4_image1'])) {
            $data['homepage_block4_image1'] = $country_language_info['homepage_block4_image1'];
        } else {
            $data['homepage_block4_image1'] = '';
        }
        if (isset($this->request->post['homepage_block4_image1']) && is_file(DIR_IMAGE . $this->request->post['homepage_block4_image1'])) {
            $data['thumb_41'] = $this->model_tool_image->resize($this->request->post['homepage_block4_image1'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block4_image1']) && is_file(DIR_IMAGE . $country_language_info['homepage_block4_image1'])) {
            $data['thumb_41'] = $this->model_tool_image->resize($country_language_info['homepage_block4_image1'], 100, 100, false);
        } else {
            $data['thumb_41'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        //tab4 // block 2
        if (isset($this->request->post['homepage_block4_title2'])) {
            $data['homepage_block4_title2'] = $this->request->post['homepage_block4_title2'];
        } elseif (isset($country_language_info['homepage_block4_title2'])) {
            $data['homepage_block4_title2'] = $country_language_info['homepage_block4_title2'];
        } else {
            $data['homepage_block4_title2'] = '';
        }
        if (isset($this->request->post['homepage_block4_description2'])) {
            $data['homepage_block4_description2'] = $this->request->post['homepage_block4_description2'];
        } elseif (isset($country_language_info['homepage_block4_description2'])) {
            $data['homepage_block4_description2'] = $country_language_info['homepage_block4_description2'];
        } else {
            $data['homepage_block4_description2'] = '';
        }
        if (isset($this->request->post['homepage_block4_url2'])) {
            $data['homepage_block4_url2'] = $this->request->post['homepage_block4_url2'];
        } elseif (isset($country_language_info['homepage_block1_url2'])) {
            $data['homepage_block4_url2'] = $country_language_info['homepage_block4_url2'];
        } else {
            $data['homepage_block4_url2'] = '';
        }
        if (isset($this->request->post['homepage_block4_image2'])) {
            $data['homepage_block4_image2'] = $this->request->post['homepage_block4_image2'];
        } elseif (isset($country_language_info['homepage_block4_image2'])) {
            $data['homepage_block4_image2'] = $country_language_info['homepage_block4_image2'];
        } else {
            $data['homepage_block4_image2'] = '';
        }
        if (isset($this->request->post['homepage_block4_image2']) && is_file(DIR_IMAGE . $this->request->post['homepage_block4_image2'])) {
            $data['thumb_42'] = $this->model_tool_image->resize($this->request->post['homepage_block4_image2'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block4_image2']) && is_file(DIR_IMAGE . $country_language_info['homepage_block4_image2'])) {
            $data['thumb_42'] = $this->model_tool_image->resize($country_language_info['homepage_block4_image2'], 100, 100, false);
        } else {
            $data['thumb_42'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        //tab4 // block 3
        if (isset($this->request->post['homepage_block4_title3'])) {
            $data['homepage_block4_title3'] = $this->request->post['homepage_block4_title3'];
        } elseif (isset($country_language_info['homepage_block4_title3'])) {
            $data['homepage_block4_title3'] = $country_language_info['homepage_block4_title3'];
        } else {
            $data['homepage_block4_title3'] = '';
        }
        if (isset($this->request->post['homepage_block4_description3'])) {
            $data['homepage_block4_description3'] = $this->request->post['homepage_block4_description3'];
        } elseif (isset($country_language_info['homepage_block4_description3'])) {
            $data['homepage_block4_description3'] = $country_language_info['homepage_block4_description3'];
        } else {
            $data['homepage_block4_description3'] = '';
        }
        if (isset($this->request->post['homepage_block4_url3'])) {
            $data['homepage_block4_url3'] = $this->request->post['homepage_block4_url3'];
        } elseif (isset($country_language_info['homepage_block1_url3'])) {
            $data['homepage_block4_url3'] = $country_language_info['homepage_block4_url3'];
        } else {
            $data['homepage_block4_url3'] = '';
        }
        if (isset($this->request->post['homepage_block4_image3'])) {
            $data['homepage_block4_image3'] = $this->request->post['homepage_block4_image3'];
        } elseif (isset($country_language_info['homepage_block4_image3'])) {
            $data['homepage_block4_image3'] = $country_language_info['homepage_block4_image3'];
        } else {
            $data['homepage_block4_image3'] = '';
        }
        if (isset($this->request->post['homepage_block4_image3']) && is_file(DIR_IMAGE . $this->request->post['homepage_block4_image3'])) {
            $data['thumb_43'] = $this->model_tool_image->resize($this->request->post['homepage_block4_image3'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block4_image3']) && is_file(DIR_IMAGE . $country_language_info['homepage_block4_image3'])) {
            $data['thumb_43'] = $this->model_tool_image->resize($country_language_info['homepage_block4_image3'], 100, 100, false);
        } else {
            $data['thumb_43'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }


        //tab4 // block 4
        if (isset($this->request->post['homepage_block4_title4'])) {
            $data['homepage_block4_title4'] = $this->request->post['homepage_block4_title4'];
        } elseif (isset($country_language_info['homepage_block4_title4'])) {
            $data['homepage_block4_title4'] = $country_language_info['homepage_block4_title4'];
        } else {
            $data['homepage_block4_title4'] = '';
        }
        if (isset($this->request->post['homepage_block4_description4'])) {
            $data['homepage_block4_description4'] = $this->request->post['homepage_block4_description4'];
        } elseif (isset($country_language_info['homepage_block4_description4'])) {
            $data['homepage_block4_description4'] = $country_language_info['homepage_block4_description4'];
        } else {
            $data['homepage_block4_description4'] = '';
        }
        if (isset($this->request->post['homepage_block4_url4'])) {
            $data['homepage_block4_url4'] = $this->request->post['homepage_block4_url4'];
        } elseif (isset($country_language_info['homepage_block1_url4'])) {
            $data['homepage_block4_url4'] = $country_language_info['homepage_block4_url4'];
        } else {
            $data['homepage_block4_url4'] = '';
        }
        if (isset($this->request->post['homepage_block4_image4'])) {
            $data['homepage_block4_image4'] = $this->request->post['homepage_block4_image4'];
        } elseif (isset($country_language_info['homepage_block4_image4'])) {
            $data['homepage_block4_image4'] = $country_language_info['homepage_block4_image4'];
        } else {
            $data['homepage_block4_image4'] = '';
        }
        if (isset($this->request->post['homepage_block4_image4']) && is_file(DIR_IMAGE . $this->request->post['homepage_block4_image4'])) {
            $data['thumb_44'] = $this->model_tool_image->resize($this->request->post['homepage_block4_image4'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block4_image4']) && is_file(DIR_IMAGE . $country_language_info['homepage_block4_image4'])) {
            $data['thumb_44'] = $this->model_tool_image->resize($country_language_info['homepage_block4_image4'], 100, 100, false);
        } else {
            $data['thumb_44'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        // block5 Brands
        if (isset($this->request->post['homepage_brand1'])) {
            $data['homepage_brand1'] = $this->request->post['homepage_brand1'];
        } elseif (isset($country_language_info['homepage_brand1'])) {
            $data['homepage_brand1'] = $country_language_info['homepage_brand1'];
        } else {
            $data['homepage_brand1'] = '';
        }
        if (isset($this->request->post['homepage_brand1_id'])) {
            $data['homepage_brand1_id'] = $this->request->post['homepage_brand1_id'];
        } elseif (isset($country_language_info['homepage_brand1_id'])) {
            $data['homepage_brand1_id'] = $country_language_info['homepage_brand1_id'];
        } else {
            $data['homepage_brand1_id'] = '';
        }
        if (isset($this->request->post['homepage_brand1_name'])) {
            $data['homepage_brand1_name'] = $this->request->post['homepage_brand1_name'];
        } elseif (isset($country_language_info['homepage_brand1_name'])) {
            $data['homepage_brand1_name'] = $country_language_info['homepage_brand1_name'];
        } else {
            $data['homepage_brand1_name'] = '';
        }
        if (isset($this->request->post['homepage_brand1_desc'])) {
            $data['homepage_brand1_desc'] = $this->request->post['homepage_brand1_desc'];
        } elseif (isset($country_language_info['homepage_brand1_desc'])) {
            $data['homepage_brand1_desc'] = $country_language_info['homepage_brand1_desc'];
        } else {
            $data['homepage_brand1_desc'] = '';
        }
        if (isset($this->request->post['homepage_brand1_button'])) {
            $data['homepage_brand1_button'] = $this->request->post['homepage_brand1_button'];
        } elseif (isset($country_language_info['homepage_brand1_button'])) {
            $data['homepage_brand1_button'] = $country_language_info['homepage_brand1_button'];
        } else {
            $data['homepage_brand1_button'] = '';
        }
        if (isset($this->request->post['homepage_block5_image1'])) {
            $data['homepage_block5_image1'] = $this->request->post['homepage_block5_image1'];
        } elseif (isset($country_language_info['homepage_block5_image1'])) {
            $data['homepage_block5_image1'] = $country_language_info['homepage_block5_image1'];
        } else {
            $data['homepage_block5_image1'] = '';
        }
        if (isset($this->request->post['homepage_block5_image1']) && is_file(DIR_IMAGE . $this->request->post['homepage_block5_image1'])) {
            $data['thumb_51'] = $this->model_tool_image->resize($this->request->post['homepage_block5_image1'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block5_image1']) && is_file(DIR_IMAGE . $country_language_info['homepage_block5_image1'])) {
            $data['thumb_51'] = $this->model_tool_image->resize($country_language_info['homepage_block5_image1'], 100, 100, false);
        } else {
            $data['thumb_51'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        if (isset($this->request->post['homepage_brand2'])) {
            $data['homepage_brand2'] = $this->request->post['homepage_brand2'];
        } elseif (isset($country_language_info['homepage_brand2'])) {
            $data['homepage_brand2'] = $country_language_info['homepage_brand2'];
        } else {
            $data['homepage_brand2'] = '';
        }
        if (isset($this->request->post['homepage_brand2_name'])) {
            $data['homepage_brand2_name'] = $this->request->post['homepage_brand2_name'];
        } elseif (isset($country_language_info['homepage_brand2_name'])) {
            $data['homepage_brand2_name'] = $country_language_info['homepage_brand2_name'];
        } else {
            $data['homepage_brand2_name'] = '';
        }
        if (isset($this->request->post['homepage_brand2_desc'])) {
            $data['homepage_brand2_desc'] = $this->request->post['homepage_brand2_desc'];
        } elseif (isset($country_language_info['homepage_brand2_desc'])) {
            $data['homepage_brand2_desc'] = $country_language_info['homepage_brand2_desc'];
        } else {
            $data['homepage_brand2_desc'] = '';
        }
        if (isset($this->request->post['homepage_brand2_button'])) {
            $data['homepage_brand2_button'] = $this->request->post['homepage_brand2_button'];
        } elseif (isset($country_language_info['homepage_brand2_button'])) {
            $data['homepage_brand2_button'] = $country_language_info['homepage_brand2_button'];
        } else {
            $data['homepage_brand2_button'] = '';
        }
        if (isset($this->request->post['homepage_brand2_id'])) {
            $data['homepage_brand2_id'] = $this->request->post['homepage_brand2_id'];
        } elseif (isset($country_language_info['homepage_brand2_id'])) {
            $data['homepage_brand2_id'] = $country_language_info['homepage_brand2_id'];
        } else {
            $data['homepage_brand2_id'] = '';
        }
        if (isset($this->request->post['homepage_block5_image2'])) {
            $data['homepage_block5_image2'] = $this->request->post['homepage_block5_image2'];
        } elseif (isset($country_language_info['homepage_block5_image2'])) {
            $data['homepage_block5_image2'] = $country_language_info['homepage_block5_image2'];
        } else {
            $data['homepage_block5_image2'] = '';
        }
        if (isset($this->request->post['homepage_block5_image2']) && is_file(DIR_IMAGE . $this->request->post['homepage_block5_image2'])) {
            $data['thumb_52'] = $this->model_tool_image->resize($this->request->post['homepage_block5_image2'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block5_image2']) && is_file(DIR_IMAGE . $country_language_info['homepage_block5_image2'])) {
            $data['thumb_52'] = $this->model_tool_image->resize($country_language_info['homepage_block5_image2'], 100, 100, false);
        } else {
            $data['thumb_52'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        if (isset($this->request->post['homepage_brand3'])) {
            $data['homepage_brand3'] = $this->request->post['homepage_brand3'];
        } elseif (isset($country_language_info['homepage_brand3'])) {
            $data['homepage_brand3'] = $country_language_info['homepage_brand3'];
        } else {
            $data['homepage_brand3'] = '';
        }
        if (isset($this->request->post['homepage_brand3_id'])) {
            $data['homepage_brand3_id'] = $this->request->post['homepage_brand3_id'];
        } elseif (isset($country_language_info['homepage_brand3_id'])) {
            $data['homepage_brand3_id'] = $country_language_info['homepage_brand3_id'];
        } else {
            $data['homepage_brand3_id'] = '';
        }
        if (isset($this->request->post['homepage_brand3_name'])) {
            $data['homepage_brand3_name'] = $this->request->post['homepage_brand3_name'];
        } elseif (isset($country_language_info['homepage_brand3_name'])) {
            $data['homepage_brand3_name'] = $country_language_info['homepage_brand3_name'];
        } else {
            $data['homepage_brand3_name'] = '';
        }
        if (isset($this->request->post['homepage_brand3_desc'])) {
            $data['homepage_brand3_desc'] = $this->request->post['homepage_brand3_desc'];
        } elseif (isset($country_language_info['homepage_brand3_desc'])) {
            $data['homepage_brand3_desc'] = $country_language_info['homepage_brand3_desc'];
        } else {
            $data['homepage_brand3_desc'] = '';
        }
        if (isset($this->request->post['homepage_brand3_button'])) {
            $data['homepage_brand3_button'] = $this->request->post['homepage_brand3_button'];
        } elseif (isset($country_language_info['homepage_brand3_button'])) {
            $data['homepage_brand3_button'] = $country_language_info['homepage_brand3_button'];
        } else {
            $data['homepage_brand3_button'] = '';
        }
        if (isset($this->request->post['homepage_block5_image3'])) {
            $data['homepage_block5_image3'] = $this->request->post['homepage_block5_image3'];
        } elseif (isset($country_language_info['homepage_block5_image3'])) {
            $data['homepage_block5_image3'] = $country_language_info['homepage_block5_image3'];
        } else {
            $data['homepage_block5_image3'] = '';
        }
        if (isset($this->request->post['homepage_block5_image3']) && is_file(DIR_IMAGE . $this->request->post['homepage_block5_image3'])) {
            $data['thumb_53'] = $this->model_tool_image->resize($this->request->post['homepage_block5_image3'], 100, 100, false);
        } elseif (isset($country_language_info['homepage_block5_image3']) && is_file(DIR_IMAGE . $country_language_info['homepage_block5_image3'])) {
            $data['thumb_53'] = $this->model_tool_image->resize($country_language_info['homepage_block5_image3'], 100, 100, false);
        } else {
            $data['thumb_53'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        $data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('homepage/homepage_form.tpl', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'homepage/homepage')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /*if (!$this->request->post['homepage_block0_url1']) {
            $this->error['homepage_block0_url1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block0_url1'));
        }
        if (!$this->request->post['homepage_block0_image1'] && ($this->request->files['homepage_block0_image1']['error'])) {
            $this->error['homepage_block0_image1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block0_image1'));
        }
        if (!$this->request->post['homepage_block0_image2'] && ($this->request->files['homepage_block0_image2']['error'])) {
            $this->error['homepage_block0_image2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block0_image2'));
        }

        if (!$this->request->post['homepage_block1_title1']) {
            $this->error['homepage_block1_title1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_title1'));
        }
        if (!$this->request->post['homepage_block1_button1']) {
            $this->error['homepage_block1_button1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_button1'));
        }
        if (!$this->request->post['homepage_block1_description1']) {
            $this->error['homepage_block1_description1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_description1'));
        }
        if (!$this->request->post['homepage_block1_url1']) {
            $this->error['homepage_block1_url1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_url1'));
        }
        if (!$this->request->post['homepage_block1_image1'] && ($this->request->files['homepage_block1_image1']['error'])) {
            $this->error['homepage_block1_image1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_image1'));
        }


        if (!$this->request->post['homepage_block1_title2']) {
            $this->error['homepage_block1_title2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_title2'));
        }
        if (!$this->request->post['homepage_block1_button2']) {
            $this->error['homepage_block1_button2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_button2'));
        }
        if (!$this->request->post['homepage_block1_description2']) {
            $this->error['homepage_block1_description2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_description2'));
        }
        if (!$this->request->post['homepage_block1_url2']) {
            $this->error['homepage_block1_url2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_url2'));
        }
        if (!$this->request->post['homepage_block1_image2'] && ($this->request->files['homepage_block1_image2']['error'])) {
            $this->error['homepage_block1_image2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_image2'));
        }

        if (!$this->request->post['homepage_block1_title3']) {
            $this->error['homepage_block1_title3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_title3'));
        }
        if (!$this->request->post['homepage_block1_button3']) {
            $this->error['homepage_block1_button3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_button3'));
        }
        if (!$this->request->post['homepage_block1_description3']) {
            $this->error['homepage_block1_description3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_description3'));
        }
        if (!$this->request->post['homepage_block1_url3']) {
            $this->error['homepage_block1_url3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_url3'));
        }
        if (!$this->request->post['homepage_block1_image3'] && ($this->request->files['homepage_block1_image3']['error'])) {
            $this->error['homepage_block1_image3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_image3'));
        }

        if (!$this->request->post['homepage_block1_title4']) {
            $this->error['homepage_block1_title4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_title4'));
        }
        if (!$this->request->post['homepage_block1_button4']) {
            $this->error['homepage_block1_button4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_button4'));
        }
        if (!$this->request->post['homepage_block1_description4']) {
            $this->error['homepage_block1_description4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_description4'));
        }
        if (!$this->request->post['homepage_block1_url4']) {
            $this->error['homepage_block1_url4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_url4'));
        }
        if (!$this->request->post['homepage_block1_image4'] && ($this->request->files['homepage_block1_image4']['error'])) {
            $this->error['homepage_block1_image4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_image4'));
        }

        if (!$this->request->post['homepage_block1_title5']) {
            $this->error['homepage_block1_title5'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_title5'));
        }
        if (!$this->request->post['homepage_block1_button5']) {
            $this->error['homepage_block1_button5'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_button5'));
        }
        if (!$this->request->post['homepage_block1_description5']) {
            $this->error['homepage_block1_description5'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_description5'));
        }
        if (!$this->request->post['homepage_block1_url5']) {
            $this->error['homepage_block1_url5'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_url5'));
        }
        if (!$this->request->post['homepage_block1_image5'] && ($this->request->files['homepage_block1_image5']['error'])) {
            $this->error['homepage_block1_image5'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block1_image5'));
        }

        if (!$this->request->post['homepage_block3_title']) {
            $this->error['homepage_block3_title'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_title'));
        }
        if (!$this->request->post['homepage_block3_description']) {
            $this->error['homepage_block3_description'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_description'));
        }
        if (!$this->request->post['homepage_block3_title1']) {
            $this->error['homepage_block3_title1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_title1'));
        }
        if (!$this->request->post['homepage_block3_description1']) {
            $this->error['homepage_block3_description1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_description1'));
        }
        if (!$this->request->post['homepage_block3_url1']) {
            $this->error['homepage_block3_url1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_url1'));
        }
        if (!$this->request->post['homepage_block3_image1'] && ($this->request->files['homepage_block3_image1']['error'])) {
            $this->error['homepage_block3_image1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_image1'));
        }

        if (!$this->request->post['homepage_block3_title2']) {
            $this->error['homepage_block3_title2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_title2'));
        }
        if (!$this->request->post['homepage_block3_description2']) {
            $this->error['homepage_block3_description2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_description2'));
        }
        if (!$this->request->post['homepage_block3_url2']) {
            $this->error['homepage_block3_url2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_url2'));
        }
        if (!$this->request->post['homepage_block3_image2'] && ($this->request->files['homepage_block3_image2']['error'])) {
            $this->error['homepage_block3_image2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block2_image1'));
        }

        if (!$this->request->post['homepage_block3_title3']) {
            $this->error['homepage_block3_title3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_title3'));
        }
        if (!$this->request->post['homepage_block3_description3']) {
            $this->error['homepage_block3_description3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_description3'));
        }
        if (!$this->request->post['homepage_block3_url3']) {
            $this->error['homepage_block3_url3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_url3'));
        }
        if (!$this->request->post['homepage_block3_image3'] && ($this->request->files['homepage_block3_image3']['error'])) {
            $this->error['homepage_block3_image3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block2_image3'));
        }

        if (!$this->request->post['homepage_block3_title4']) {
            $this->error['homepage_block3_title4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_title4'));
        }
        if (!$this->request->post['homepage_block3_description4']) {
            $this->error['homepage_block3_description4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_description4'));
        }
        if (!$this->request->post['homepage_block3_url4']) {
            $this->error['homepage_block3_url4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block3_url4'));
        }
        if (!$this->request->post['homepage_block3_image4'] && ($this->request->files['homepage_block3_image4']['error'])) {
            $this->error['homepage_block3_image4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block2_image4'));
        }

        // block 4
        if (!$this->request->post['homepage_block4_title']) {
            $this->error['homepage_block4_title'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_title'));
        }
        if (!$this->request->post['homepage_block4_description']) {
            $this->error['homepage_block4_description'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_description'));
        }
        if (!$this->request->post['homepage_block4_title1']) {
            $this->error['homepage_block4_title1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_title1'));
        }
        if (!$this->request->post['homepage_block4_description1']) {
            $this->error['homepage_block4_description1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_description1'));
        }
        if (!$this->request->post['homepage_block4_url1']) {
            $this->error['homepage_block4_url1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_url1'));
        }
        if (!$this->request->post['homepage_block4_image1'] && ($this->request->files['homepage_block4_image1']['error'])) {
            $this->error['homepage_block4_image1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_image1'));
        }

        if (!$this->request->post['homepage_block4_title2']) {
            $this->error['homepage_block4_title2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_title2'));
        }
        if (!$this->request->post['homepage_block4_description2']) {
            $this->error['homepage_block4_description2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_description2'));
        }
        if (!$this->request->post['homepage_block4_url2']) {
            $this->error['homepage_block4_url2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_url2'));
        }
        if (!$this->request->post['homepage_block4_image2'] && ($this->request->files['homepage_block4_image2']['error'])) {
            $this->error['homepage_block4_image2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block2_image1'));
        }

        if (!$this->request->post['homepage_block4_title3']) {
            $this->error['homepage_block4_title3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_title3'));
        }
        if (!$this->request->post['homepage_block4_description3']) {
            $this->error['homepage_block4_description3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_description3'));
        }
        if (!$this->request->post['homepage_block4_url3']) {
            $this->error['homepage_block4_url3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_url3'));
        }
        if (!$this->request->post['homepage_block4_image3'] && ($this->request->files['homepage_block4_image3']['error'])) {
            $this->error['homepage_block4_image3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block2_image3'));
        }

        if (!$this->request->post['homepage_block4_title4']) {
            $this->error['homepage_block4_title4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_title4'));
        }
        if (!$this->request->post['homepage_block4_description4']) {
            $this->error['homepage_block4_description4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_description4'));
        }
        if (!$this->request->post['homepage_block4_url4']) {
            $this->error['homepage_block4_url4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block4_url4'));
        }
        if (!$this->request->post['homepage_block4_image4'] && ($this->request->files['homepage_block4_image4']['error'])) {
            $this->error['homepage_block4_image4'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block2_image4'));
        }


        // block 5 Brands
        if (!$this->request->post['homepage_brand1_name']) {
            $this->error['homepage_brand1_name'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand_name'));
        }
        if (!$this->request->post['homepage_brand1_desc']) {
            $this->error['homepage_brand1_desc'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand_desc'));
        }
        if (!$this->request->post['homepage_brand1_button']) {
            $this->error['homepage_brand1_button'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand_button'));
        }
        if (!$this->request->post['homepage_brand2_name']) {
            $this->error['homepage_brand2_name'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand_name'));
        }
        if (!$this->request->post['homepage_brand2_desc']) {
            $this->error['homepage_brand2_desc'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand_desc'));
        }
        if (!$this->request->post['homepage_brand2_button']) {
            $this->error['homepage_brand2_button'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand_button'));
        }
        if (!$this->request->post['homepage_brand3_name']) {
            $this->error['homepage_brand3_name'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand_name'));
        }
        if (!$this->request->post['homepage_brand3_desc']) {
            $this->error['homepage_brand3_desc'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand_desc'));
        }
        if (!$this->request->post['homepage_brand3_button']) {
            $this->error['homepage_brand3_button'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand_button'));
        }

        if ((!isset($this->request->post['homepage_brand1_id']) ) || empty($this->request->post['homepage_brand1_id'])) {
            $this->error['homepage_brand1_id'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand'));
        }
        if (!$this->request->post['homepage_block5_image1'] && ($this->request->files['homepage_block5_image1']['error'])) {
            $this->error['homepage_block5_image1'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block5_image1'));
        }
        if ((!isset($this->request->post['homepage_brand2_id']) ) || empty($this->request->post['homepage_brand2_id'])) {
            $this->error['homepage_brand2_id'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand'));
        }
        if (!$this->request->post['homepage_block5_image2'] && ($this->request->files['homepage_block5_image2']['error'])) {
            $this->error['homepage_block5_image2'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block5_image2'));
        }
        if ((!isset($this->request->post['homepage_brand3_id']) ) || empty($this->request->post['homepage_brand3_id'])) {
            $this->error['homepage_brand3_id'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_brand'));
        }
        if (!$this->request->post['homepage_block5_image3'] && ($this->request->files['homepage_block5_image3']['error'])) {
            $this->error['homepage_block5_image3'] = sprintf($this->language->get('error_field_reuired'), $this->language->get('text_homepage_block5_image3'));
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }*/

        return !$this->error;
    }

}
