<?php

class ControllerMallMall extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('mall/mall');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/mall');
        $this->getList();
    }

    public function add() {
        $this->language->load('mall/mall');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/mall');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $mall_id = $this->model_mall_mall->addMall($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($mall_id) && !empty($mall_id)) {
                $url .= '&filter_mall_id=' . $mall_id;
            }
            $this->response->redirect($this->url->link('mall/mall', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }
        $this->getForm();
    }

    public function edit() {
        $this->language->load('mall/mall');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/mall');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_mall_mall->editMall($this->request->get['mall_id'], $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($this->request->get['mall_id']) && !empty($this->request->get['mall_id'])) {
                $url .= '&filter_mall_id=' . $this->request->get['mall_id'];
            }
            $this->response->redirect($this->url->link('mall/mall', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->language->load('mall/mall');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/mall');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $mall_id) {
                $this->model_mall_mall->deleteMall($mall_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('mall/mall', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {

        if (isset($this->request->get['filter_filter'])) {
            $filter_filter = $this->request->get['filter_filter'];
        } else {
            $filter_filter = NULL;
        }

        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = NULL;
        }

        if (isset($this->request->get['filter_city_id'])) {
            $filter_city_id = $this->request->get['filter_city_id'];
        } else {
            $filter_city_id = NULL;
        }

        if (isset($this->request->get['filter_mall_id'])) {
            $filter_mall_id = $this->request->get['filter_mall_id'];
            $data['filter_mall_id'] = $this->request->get['filter_mall_id'];
        } else {
            $filter_mall_id = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'md.name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_filter'])) {
            $url .= '&filter_filter=' . $this->request->get['filter_filter'];
        }

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_mall_id'])) {
            $url .= '&filter_mall_id=' . $this->request->get['filter_mall_id'];
        }

        if (isset($this->request->get['filter_city_id'])) {
            $url .= '&filter_city_id=' . $this->request->get['filter_city_id'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/mall', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('mall/mall/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('mall/mall/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->load->model('localisation/country');
        $data['countries'] = $this->model_localisation_country->getAvailableCountries();

        $this->load->model('localisation/zone');
        $data['cities'] = $this->model_mall_mall->getZones($filter_country_id);


        $data['malls'] = array();

        $filter_data = array(
            'filter_mall_id' => $filter_mall_id,
            'filter_filter' => $filter_filter,
            'filter_country_id' => $filter_country_id,
            'filter_city_id' => $filter_city_id,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $products_total = $this->model_mall_mall->getTotalMall($filter_data);
        $results = $this->model_mall_mall->getMalls($filter_data);
        //die(var_dump($results));
        foreach ($results as $result) {
            $data['malls'][$result['mall_id']] = array(
                'mall_id' => $result['mall_id'],
                'name' => $result['name'],
                'city' => $result['city'],
                'filter_by_city' => $this->url->link('mall/mall', 'token=' . $this->session->data['token'] . '&filter_city_id=' . $result['zone_id'] . '&filter_country_id=' . $result['country_id']),
                'filetr_shops_by_mall' => $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . '&filter_city_id=' . $result['zone_id'] . '&filter_country_id=' . $result['country_id'] . '&filter_mall_id=' . $result['mall_id']),
                'shop_count' => $result['shop_count'],
                'sort_order' => $result['sort_order'],
                'user_add' => $result['user_add'],
                'user_modify' => $result['user_modify'],
                'edit' => $this->url->link('mall/mall/edit', 'token=' . $this->session->data['token'] . '&mall_id=' . $result['mall_id'] . $url, 'SSL'),
                'date_added' => $result['date_added'],
                'date_modified' => $result['date_modified']
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_select'] = $this->language->get('text_select');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_city'] = $this->language->get('entry_city');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['column_user_id'] = $this->language->get('column_user_id');
        $data['column_last_mod_id'] = $this->language->get('column_last_mod_id');

        $data['select_city'] = $this->language->get('select_city');
        $data['select_country'] = $this->language->get('select_country');
        $data['button_filter'] = $this->language->get('button_filter');

        $data['token'] = $this->session->data['token'];

        $data['column_name'] = $this->language->get('column_name');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_shop_count'] = $this->language->get('column_shop_count');
        $data['column_city'] = $this->language->get('column_city');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');


        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $pagination = new Pagination();
        $pagination->total = $products_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('mall/mall', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($products_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($products_total - $this->config->get('config_limit_admin'))) ? $products_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $products_total, ceil($products_total / $this->config->get('config_limit_admin')));

        $data['filter_filter'] = $filter_filter;
        $data['filter_country_id'] = $filter_country_id;
        $data['filter_city_id'] = $filter_city_id;

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }
        $data['sort_name'] = $this->url->link('mall/mall', 'token=' . $this->session->data['token'] . $url . '&sort=md.name', 'SSL');
        $data['sort_order'] = $this->url->link('mall/mall', 'token=' . $this->session->data['token'] . $url . '&sort=m.sort_order', 'SSL');
        $data['sort_shop'] = $this->url->link('mall/mall', 'token=' . $this->session->data['token'] . $url . '&sort=m.shop_count', 'SSL');
        $data['sort_city'] = $this->url->link('mall/mall', 'token=' . $this->session->data['token'] . $url . '&sort=zd.name', 'SSL');

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('mall/mall_list.tpl', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['bActiveLanguageTabFlag'] = true;
        $data['bActiveLanguagePaneFlag'] = true;

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_attribute'] = $this->language->get('tab_attribute');

        $data['text_form'] = !isset($this->request->get['_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');

        //-- Description/Attribute
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_location'] = $this->language->get('entry_location');
        $data['entry_open'] = $this->language->get('entry_open');
        $data['entry_meta_title'] = $this->language->get('entry_meta_title');
        $data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');

        $data['entry_latitude'] = $this->language->get('entry_latitude');
        $data['entry_longitude'] = $this->language->get('entry_longitude');
        $data['entry_zoom'] = $this->language->get('entry_zoom');

        //-- General
        $data['entry_city_id'] = $this->language->get('entry_city_id');
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_phone'] = $this->language->get('entry_phone');
        $data['entry_fax'] = $this->language->get('entry_fax');
        $data['entry_website'] = $this->language->get('entry_website');
        $data['entry_social_jeeran'] = $this->language->get('entry_social_jeeran');
        $data['entry_social_fb'] = $this->language->get('entry_social_fb');
        $data['entry_social_tw'] = $this->language->get('entry_social_tw');
        $data['entry_template'] = $this->language->get('entry_template');
        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_active'] = $this->language->get('entry_active');
        $data['entry_status'] = $this->language->get('entry_status');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }
        if (isset($this->error['countries'])) {
            $data['error_countries'] = $this->error['countries'];
        } else {
            $data['error_countries'] = array();
        }


        if (isset($this->error['error_google_latitude'])) {
            $data['error_google_latitude'] = $this->error['error_google_latitude'];
        } else {
            $data['error_google_latitude'] = '';
        }

        if (isset($this->error['error_google_longitude'])) {
            $data['error_google_longitude'] = $this->error['error_google_longitude'];
        } else {
            $data['error_google_longitude'] = '';
        }

        if (isset($this->error['error_google_zoom'])) {
            $data['error_google_zoom'] = $this->error['error_google_zoom'];
        } else {
            $data['error_google_zoom'] = '';
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/mall', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['mall_id'])) {
            $data['action'] = $this->url->link('mall/mall/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('mall/mall/edit', 'token=' . $this->session->data['token'] . '&mall_id=' . $this->request->get['mall_id'] . $url, 'SSL');
        }
        $data['cancel'] = $this->url->link('mall/mall', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['mall_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $mall_info = $this->model_mall_mall->getMall($this->request->get['mall_id']);
            $google = $this->model_mall_mall->getMap($this->request->get['mall_id']);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();


        if (isset($this->request->post['mall_description'])) {
            $data['mall_description'] = $this->request->post['mall_description'];
        } elseif (isset($this->request->get['mall_id'])) {
            $data['mall_description'] = $this->model_mall_mall->getMallDescriptions($this->request->get['mall_id']);
        } else {
            $data['mall_description'] = array();
        }

        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        foreach ($aDBCountries as $key => $value) {
            $data['countries'][$value['country_id']] = $value['name'];
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($mall_info)) {
            $data['image'] = $mall_info['image'];
        } else {
            $data['image'] = '';
        }

        $this->load->model('tool/image');
        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($mall_info) && is_file(DIR_IMAGE . $mall_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($mall_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('placeholder.png', 100, 100);
        }

        $data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 100, 100);

        if (isset($this->request->post['city_id'])) {
            $data['city_id'] = $this->request->post['city_id'];
        } elseif (!empty($mall_info)) {
            $data['city_id'] = $mall_info['city_id'];
        } else {
            $data['city_id'] = 0;
        }

        $this->load->model('localisation/zone');
        $data['country_id'] = 0;
        if (isset($data['city_id']) && $data['city_id'] > 0) {
            $zone_info = $this->model_localisation_zone->getZone($data['city_id']);
            if (!empty($zone_info)) {
                $data['country_id'] = $zone_info['country_id'];
            }
        }

        if (isset($this->request->post['phone'])) {
            $data['phone'] = $this->request->post['phone'];
        } elseif (!empty($mall_info)) {
            $data['phone'] = $mall_info['phone'];
        } else {
            $data['phone'] = "";
        }

        if (isset($this->request->post['fax'])) {
            $data['fax'] = $this->request->post['fax'];
        } elseif (!empty($mall_info)) {
            $data['fax'] = $mall_info['fax'];
        } else {
            $data['fax'] = "";
        }

        if (isset($this->request->post['website'])) {
            $data['website'] = $this->request->post['website'];
        } elseif (!empty($mall_info)) {
            $data['website'] = $mall_info['website'];
        } else {
            $data['website'] = "";
        }

        if (isset($this->request->post['social_jeeran'])) {
            $data['social_jeeran'] = $this->request->post['social_jeeran'];
        } elseif (!empty($mall_info)) {
            $data['social_jeeran'] = $mall_info['social_jeeran'];
        } else {
            $data['social_jeeran'] = "";
        }

        if (isset($this->request->post['social_fb'])) {
            $data['social_fb'] = $this->request->post['social_fb'];
        } elseif (!empty($mall_info)) {
            $data['social_fb'] = $mall_info['social_fb'];
        } else {
            $data['social_fb'] = "";
        }

        if (isset($this->request->post['social_tw'])) {
            $data['social_tw'] = $this->request->post['social_tw'];
        } elseif (!empty($mall_info)) {
            $data['social_tw'] = $mall_info['social_tw'];
        } else {
            $data['social_tw'] = "";
        }

        if (isset($this->request->post['template'])) {
            $data['template'] = $this->request->post['template'];
        } elseif (!empty($mall_info)) {
            $data['template'] = $mall_info['template'];
        } else {
            $data['template'] = "";
        }

        if (isset($this->request->post['latitude'])) {
            $data['latitude'] = $this->request->post['latitude'];
        } elseif (!empty($google)) {
            $data['latitude'] = $google['latitude'];
        } else {
            $data['latitude'] = '';
        }

        if (isset($this->request->post['longitude'])) {
            $data['longitude'] = $this->request->post['longitude'];
        } elseif (!empty($google)) {
            $data['longitude'] = $google['longitude'];
        } else {
            $data['longitude'] = '';
        }

        if (isset($this->request->post['zoom'])) {
            $data['zoom'] = $this->request->post['zoom'];
        } elseif (!empty($google)) {
            $data['zoom'] = $google['zoom'];
        } else {
            $data['zoom'] = 0;
        }

        if (!isset($this->request->post['user_id'])) {
            $data['user_id'] = $this->session->data['user_id'];
        } elseif (!empty($mall_info)) {
            $data['user_id'] = $mall_info['user_id'];
        } else {
            $data['user_id'] = ($this->request->post['user_id'] != $this->session->data['user_id']) ? $this->session->data['user_id'] : $this->request->post['user_id'];
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($mall_info)) {
            $data['status'] = $mall_info['status'];
        } else {
            $data['status'] = 0;
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($mall_info)) {
            $data['sort_order'] = $mall_info['sort_order'];
        } else {
            $data['sort_order'] = 0;
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('mall/mall_form.tpl', $data));
    }

    public function getmalls() {
        $json = array();

        $filter_data = array(
            'filter_country_id' => (isset($this->request->get['filter_country_id'])) ? $this->request->get['filter_country_id'] : '',
            'filter_city_id' => (isset($this->request->get['filter_city_id'])) ? $this->request->get['filter_city_id'] : ''
        );
        $this->load->model('mall/mall');
        $results = $this->model_mall_mall->getMalls($filter_data);

        foreach ($results as $result) {
            $json[] = array(
                'mall_id' => $result['mall_id'],
                'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        // echo "<pre>"; die(print_r($json));
    }

    public function getZone() {
        $json = array();

        $this->load->model('mall/mall');
        $results = $this->model_mall_mall->getZones($this->request->get['country_id']);

        foreach ($results as $result) {
            $json[] = array(
                'zone_id' => $result['zone_id'],
                'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        // echo "<pre>"; die(print_r($json));
    }

    protected function validateRefresh() {
        if (!$this->user->hasPermission('modify', 'mall/mall')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'mall/mall')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        return !$this->error;
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'mall/mall')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((!isset($this->request->post['city_id'])) || ((int) $this->request->post['city_id'] < 0)) {
            $this->error['city_id'] = $this->language->get('error_city_id');
        }

        /* if ((float) $this->request->post['latitude'] <= 0 || (float) $this->request->post['latitude'] > 10000.000000) {
          $this->error['google'] = $this->language->get('error_google');
          }
          if ((float) $this->request->post['longitude'] <= 0 || (float) $this->request->post['longitude'] > 10000.000000) {
          $this->error['google'] = $this->language->get('error_google');
          } */
        if (isset($this->request->post['latitude']) && strlen($this->request->post['latitude'])) {
            ///^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}$/
            if (!(preg_match("/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/", $this->request->post['latitude']))) {
                $this->error['error_google_latitude'] = $this->language->get('error_google_latitude');
            }
        }
        if (isset($this->request->post['longitude']) && strlen($this->request->post['longitude'])) {
            // if( !(preg_match("/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/", $this->request->post['longitude'])) ){

            if (!preg_match("/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}$/", $this->request->post['longitude'])) {
                $this->error['error_google_longitude'] = $this->language->get('error_google_longitude');
            }
        }
        if (isset($this->request->post['zoom'])) {
            if ($this->request->post['zoom'] < 1 || $this->request->post['zoom'] > 21) {
                $this->error['error_google_zoom'] = $this->language->get('error_google_zoom');
            }
        }
        return !$this->error;
    }

}
