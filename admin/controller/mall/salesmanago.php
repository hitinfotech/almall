<?php
/**
 * Created by PhpStorm.
 * User: bahaaodeh
 * Date: 1/18/18
 * Time: 10:19 AM
 */
class ControllerMallSalesmanago extends Controller{

    public function index() {
        $this->document->setTitle('Sales Manago');
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');


        if(isset($this->request->post['EnglishPopup'])){
            if(!empty($this->request->files['popup_image']) && !empty($this->request->files['popup_image']['tmp_name'])){
                $image =  $this->model_tool_image->upload_file("salesmanago",'popup_image','1');
                if(!empty($image)){
                    $this->model_tool_image->resize($image, 400, 400,true);
                    $this->dynamic_values->set('salesmanago','popup_image',$image,1);
                }
            }

            foreach ($this->request->post['EnglishPopup'] as $key=>$value){
                if(!empty($value)){
                    $this->dynamic_values->set('salesmanago',$key,$value,1);
                }
            }
        }

        if(isset($this->request->post['ArabicPopup'])){
            if(!empty($this->request->files['popup_image'])  && !empty($this->request->files['popup_image']['tmp_name'])){

                $image =  $this->model_tool_image->upload_file("salesmanago",'popup_image','2');
                if(!empty($image)){
                    $this->model_tool_image->resize($image, 400, 400,true);
                    $this->dynamic_values->set('salesmanago','popup_image',$image,2);
                }
            }

            foreach ($this->request->post['ArabicPopup'] as $key=>$value){
                if(!empty($value)){
                    $this->dynamic_values->set('salesmanago',$key,$value,2);
                }
            }
        }



        $data['english_popup'] = [
            'popup_title'=>  $this->dynamic_values->get('salesmanago','popup_title',1),
            'popup_description'=>  $this->dynamic_values->get('salesmanago','popup_description',1),
            'popup_yes'=>  $this->dynamic_values->get('salesmanago','popup_yes',1),
            'popup_no'=>  $this->dynamic_values->get('salesmanago','popup_no',1),
            'popup_image' => $this->model_tool_image->resize($this->dynamic_values->get('salesmanago','popup_image',1), 400, 400,false),
        ];
        $data['arabic_popup'] = [
            'popup_title'=>  $this->dynamic_values->get('salesmanago','popup_title',2),
            'popup_description'=>  $this->dynamic_values->get('salesmanago','popup_description',2),
            'popup_yes'=>  $this->dynamic_values->get('salesmanago','popup_yes',2),
            'popup_no'=>  $this->dynamic_values->get('salesmanago','popup_no',2),
            'popup_image' => $this->model_tool_image->resize($this->dynamic_values->get('salesmanago','popup_image',2), 400, 400,false),
        ];


        $this->response->setOutput($this->load->view('mall/salesmanago.tpl', $data));
    }

}