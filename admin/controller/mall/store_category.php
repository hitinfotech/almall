<?php

class ControllerMallStoreCategory extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('mall/store_category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('mall/store_category');

        $this->getList();
    }

    public function add() {
        $this->language->load('mall/store_category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('mall/store_category');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_mall_store_category->addStoreCategory($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('mall/store_category', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function edit() {
        $this->language->load('mall/store_category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('mall/store_category');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_mall_store_category->editStoreCategory($this->request->get['store_category_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('mall/store_category', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->language->load('mall/store_category');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('mall/store_category');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $store_category_id) {
                $this->model_mall_store_category->deleteStoreCategory($store_category_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('mall/store_category', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {


        if (isset($this->request->get['filter_keyword'])) {
            $filter_keyword = $this->request->get['filter_keyword'];
        } else {
            $filter_keyword = NULL;
        }


        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }


        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/store_category', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('mall/store_category/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('mall/store_category/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['sort_name'] = $this->url->link('mall/store_category', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
        $data['sort_sort_order'] = $this->url->link('mall/store_category', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, 'SSL');

        $data['stores'] = array();


        $this->load->model('catalog/group');
        // $groupResult = $this->model_catalog_group->getgroups();
        //echo "<pre>";die(print_r($results));
        $data['groups'] = array();
        // foreach ($groupResult as $row){
        //     $data['groups'][]=array(
        //         'group_id' => $row['group_id'],
        //         'name' => $row['name']
        //     );
        // }


        $filter_data = array(
            'sort' => $sort,
            'filter_keyword'=>$filter_keyword,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $main_store_total = $this->model_mall_store_category->getTotalStoreCategories($filter_data);

        $results = $this->model_mall_store_category->getStoreCategories($filter_data);

        $data['stores_categories'] = array();
        foreach ($results as $result) {
            $name = '';
            $sep = '';
            $names = $this->model_mall_store_category->getStoreCategoriesDescriptions($result['store_category_id']);
            foreach($names as $row){
                $name .= $sep . $row['name'];
                $sep = ' | ';
            }

            $data['stores_categories'][] = array(
                'store_category_id' => $result['store_category_id'],
                'name' => $name, //$result['name'],
                'parent_category_name' => $result['parent_category_name'],
                'date_modified' => $result['date_modified'],
                'date_added' => $result['date_added'],
                'sort_order' => $result['sort_order'],
                'edit' => $this->url->link('mall/store_category/edit', 'token=' . $this->session->data['token'] . '&store_category_id=' . $result['store_category_id'] . $url, 'SSL'),
                'delete' => $this->url->link('mall/store_category/delete', 'token=' . $this->session->data['token'] . '&store_category_id=' . $result['store_category_id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_city'] = $this->language->get('entry_city');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['entry_category'] = $this->language->get('entry_category');
        $data['entry_mall'] = $this->language->get('entry_mall');
        $data['text_select'] = $this->language->get('text_select');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_select_all'] = $this->language->get('text_select_all');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_parent_category'] = $this->language->get('column_parent_category');
        $data['column_creat_date'] = $this->language->get('column_creat_date');
        $data['column_update_date'] = $this->language->get('column_update_date');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $data['filter_keyword'] = $filter_keyword;

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('mall/store_category', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
        $data['sort_sort_order'] = $this->url->link('mall/store_category', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['filter_keyword'])) {
            $url .= '&filter_keyword=' . $this->request->get['filter_keyword'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $main_store_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('mall/store_category', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($main_store_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($main_store_total - $this->config->get('config_limit_admin'))) ? $main_store_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $main_store_total, ceil($main_store_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $data['token'] = $this->session->data['token'];

        $this->response->setOutput($this->load->view('mall/store_category_list.tpl', $data));
    }

    protected function getForm() {
        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['store_category_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_meta_title'] = $this->language->get('entry_meta_title');
        $data['entry_meta_keywords'] = $this->language->get('entry_meta_keywords');
        $data['entry_meta_tags'] = $this->language->get('entry_meta_tags');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_group_id'] = $this->language->get('entry_group_id');
        $data['entry_parent_id'] = $this->language->get('entry_parent_id');

        $data['help_filter'] = $this->language->get('help_filter');
        $data['help_keyword'] = $this->language->get('help_keyword');
        $data['help_top'] = $this->language->get('help_top');
        $data['help_column'] = $this->language->get('help_column');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_data'] = $this->language->get('tab_data');
        $data['tab_design'] = $this->language->get('tab_design');

        $errors_array = array ('warning','name','location','google','meta_title','meta_keywords','meta_tag','parent_id');

        foreach ($errors_array as $err)
        if (isset($this->error[$err])) {
            $data['error_'.$err] = $this->error[$err];
        } else {
            $data['error_'.$err] = '';
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/store_category', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['store_category_id'])) {
            $data['action'] = $this->url->link('mall/store_category/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('mall/store_category/edit', 'token=' . $this->session->data['token'] . '&store_category_id=' . $this->request->get['store_category_id'] . $url, 'SSL');
        }

        $data['cancel'] = $this->url->link('mall/store_category', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['store_category_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $data['store_category_id'] = $this->request->get['store_category_id'];
            $store_info = $this->model_mall_store_category->getStoreCategory($this->request->get['store_category_id']);
        }else{
            $data['store_category_id'] = 0;
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();


        if (isset($this->request->post['store_category_description'])) {
            $data['store_category_description'] = $this->request->post['store_category_description'];
        } elseif (isset($this->request->get['store_category_id'])) {
            $data['store_category_description'] = $this->model_mall_store_category->getStoreCategoriesDescriptions($this->request->get['store_category_id']);
        } else {
            $data['store_category_description'] = array();
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($store_info)) {
            $data['sort_order'] = $store_info['sort_order'];
        } else {
            $data['sort_order'] = 0;
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($store_info)) {
            $data['status'] = $store_info['status'];
        } else {
            $data['status'] = 0;
        }

        if (isset($this->request->post['parent_id'])) {
            $data['parent_id'] = $this->request->post['parent_id'];
        } elseif (!empty($store_info)) {
            $data['parent_id'] = $store_info['parent_id'];
        } else {
            $data['parent_id'] = 0;
        }

        // $this->load->model('catalog/group');
        // $groups = $this->model_catalog_group->getGroups();
        // echo "<pre>";die(print_r($results));
        $data['groups'] = array();
        // foreach ($groups as $row){
        //     $data['groups'][]=array(
        //         'group_id' => $row['group_id'],
        //         'name' => $row['name']
        //     );
        // }

        if (isset($this->request->post['group_id'])) {
            $data['group_id'] = $this->request->post['group_id'];
        } elseif (!empty($store_info)) {
            $data['group_id'] = $store_info['group_id'];
        } else {
            $data['group_id'] = 0;
        }


        $this->load->model('mall/store_category');
        $parent_category = $this->model_mall_store_category->getStoreParentCategories(array('store_category_id' => $data['store_category_id']));
        //echo "<pre>";die(print_r($results));
        $data['parent_category'] = array();
        foreach ($parent_category as $row){
            $data['parent_category'][]=array(
                'store_category_id' => $row['store_category_id'],
                'name' => $row['name']
            );
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('mall/store_category_form.tpl', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'mall/store_category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        $this->load->model('localisation/language');
        $Languages = $this->model_localisation_language->getLanguages();

        // checking the mandatory fields in the General tab
        $fields_to_validate = array('name','meta_title','meta_keywords','meta_tag');
        foreach ($Languages as $value)
            foreach ($fields_to_validate as $field)
                if (!isset($this->request->post['store_category_description'][$value['language_id']][$field]) || empty($this->request->post['store_category_description'][$value['language_id']][$field])) {
                    $this->error[$field][$value['language_id']] = $this->language->get("error_$field");
                }

        if ( (!isset($this->request->post['parent_id'])) || empty($this->request->post['parent_id']) ) {
            $this->error['parent_id'] = $this->language->get('error_parent_id');
        }
        /*
        if ( (!isset($this->request->post['group_id'])) || empty($this->request->post['group_id']) ) {
            $this->error['group_id'] = $this->language->get('error_group_id');
        }*/


        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'mall/store_category')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('mall/store_category');

            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'sort' => 'name',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 20
            );

            $results = $this->model_mall_store_category->getStoreCategoryByName($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    'store_category_id' => $result['store_category_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
