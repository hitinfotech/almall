<?php

class ControllerMallShop extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('mall/shop');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/shop');
        $this->getList();
    }

    public function add() {
        $this->language->load('mall/shop');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/shop');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $shop_id = $this->model_mall_shop->addShop($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($shop_id) && !empty($shop_id)) {
                $url .= '&filter_shop_id=' . $shop_id;
            }
            $this->response->redirect($this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }
        $this->getForm();
    }

    public function edit() {
        $this->language->load('mall/shop');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/shop');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_mall_shop->editShop($this->request->get['shop_id'], $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($this->request->get['shop_id']) && !empty($this->request->get['shop_id'])) {
                $url .= '&filter_shop_id=' . $this->request->get['shop_id'];
            }
            $this->response->redirect($this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->language->load('mall/shop');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/shop');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $shop_id) {
                $this->model_mall_shop->deleteShop($shop_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {

        if (isset($this->request->get['filter_shop_id'])) {
            $filter_shop_id = $this->request->get['filter_shop_id'];
        } else {
            $filter_shop_id = NULL;
        }

        if (isset($this->request->get['filter_filter'])) {
            $filter_filter = $this->request->get['filter_filter'];
        } else {
            $filter_filter = NULL;
        }

        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = NULL;
        }

        if (isset($this->request->get['filter_city_id'])) {
            $filter_city_id = $this->request->get['filter_city_id'];
        } else {
            $filter_city_id = NULL;
        }

        if (isset($this->request->get['filter_mall_id'])) {
            $filter_mall_id = $this->request->get['filter_mall_id'];
        } else {
            $filter_mall_id = NULL;
        }

        if (isset($this->request->get['filter_main_store_id'])) {
            $filter_main_store_id = $this->request->get['filter_main_store_id'];
        } else {
            $filter_main_store_id = NULL;
        }

        if (isset($this->request->get['filter_store_category_id'])) {
            $filter_store_category_id = $this->request->get['filter_store_category_id'];
        } else {
            $filter_store_category_id = NULL;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'sd1.name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_filter'])) {
            $url .= '&filter_filter=' . $this->request->get['filter_filter'];
        }

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_shop_id'])) {
            $url .= '&filter_shop_id=' . $this->request->get['filter_shop_id'];
        }

        if (isset($this->request->get['filter_city_id'])) {
            $url .= '&filter_city_id=' . $this->request->get['filter_city_id'];
        }

        if (isset($this->request->get['filter_mall_id'])) {
            $url .= '&filter_mall_id=' . $this->request->get['filter_mall_id'];
        }

        if (isset($this->request->get['filter_main_store_id'])) {
            $url .= '&filter_main_store_id=' . $this->request->get['filter_main_store_id'];
        }

        if (isset($this->request->get['filter_store_category_id'])) {
            $url .= '&filter_store_category_id=' . $this->request->get['filter_store_category_id'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('mall/shop/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('mall/shop/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->load->model('localisation/country');
        $data['countries'] = $this->model_localisation_country->getAvailableCountries();

        $this->load->model('mall/store_category');
        $data['store_categories'] = $this->model_mall_store_category->getStoreCategories();

        $this->load->model('localisation/zone');
        $data['cities'] = $this->model_localisation_zone->getZonesByCountryId($filter_country_id);

        $filter_data = array(
            'filter_filter' => $filter_filter,
            'filter_shop_id' => $filter_shop_id,
            'filter_country_id' => $filter_country_id,
            'filter_city_id' => $filter_city_id,
            'filter_mall_id' => $filter_mall_id,
            'filter_main_store_id' => $filter_main_store_id,
            'filter_store_category_id' => $filter_store_category_id,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $shops_total = $this->model_mall_shop->getTotalShops($filter_data);

        $results = $this->model_mall_shop->getShops($filter_data);

        $data['shops'] = array();

        foreach ($results as $result) {
            $data['shops'][$result['store_id']] = array(
                'shop_id' => $result['store_id'],
                'status' => $result['status'],
                'name' => $result['name'],
                'main_store' => $result['main_store'],
                'filter_by_mall' => $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . '&filter_city_id=' . $result['zone_id'] . '&filter_country_id=' . $result['country_id']) . '&filter_mall_id=' . $result['mall_id'],
                'mall_name' => $result['mall_name'],
                'city' => $result['city'],
                'filter_by_city' => $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . '&filter_city_id=' . $result['zone_id'] . '&filter_country_id=' . $result['country_id']),
                'products_count' => $result['products_count'],
                'sort_order' => $result['sort_order'],
                'user_add' => $result['user_add'],
                'user_modify' => $result['user_modify'],
                'edit' => $this->url->link('mall/shop/edit', 'token=' . $this->session->data['token'] . '&shop_id=' . $result['store_id'] . $url, 'SSL'),
                'delete' => $this->url->link('mall/shop/delete', 'token=' . $this->session->data['token'] . '&shop_id=' . $result['store_id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_select'] = $this->language->get('text_select');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_city'] = $this->language->get('entry_city');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['entry_mall'] = $this->language->get('entry_mall');
        $data['entry_main_store'] = $this->language->get('entry_main_store');
        $data['entry_store_category'] = $this->language->get('entry_store_category');
        $data['column_user_id'] = $this->language->get('column_user_id');
        $data['column_last_mod_id'] = $this->language->get('column_last_mod_id');

        $data['select_city'] = $this->language->get('select_city');
        $data['select_country'] = $this->language->get('select_country');
        $data['button_filter'] = $this->language->get('button_filter');

        $data['token'] = $this->session->data['token'];

        $data['column_main_store'] = $this->language->get('column_main_store');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_mall_name'] = $this->language->get('column_mall_name');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_products_count'] = $this->language->get('column_products_count');
        $data['column_city'] = $this->language->get('column_city');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $pagination = new Pagination();
        $pagination->total = $shops_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($shops_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($shops_total - $this->config->get('config_limit_admin'))) ? $shops_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $shops_total, ceil($shops_total / $this->config->get('config_limit_admin')));

        $data['filter_shop_id'] = $filter_shop_id;
        $data['filter_filter'] = $filter_filter;
        $data['filter_country_id'] = $filter_country_id;
        $data['filter_city_id'] = $filter_city_id;
        $data['filter_mall_id'] = $filter_mall_id;
        $data['filter_main_store_id'] = $filter_main_store_id;
        $data['filter_store_category_id'] = $filter_store_category_id;

        $data['sort_name'] = $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url . '&sort=md.name', 'SSL');
        $data['sort_order'] = $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url . '&sort=m.sort_order', 'SSL');
        $data['sort_shop'] = $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url . '&sort=m.shop_count', 'SSL');
        $data['sort_city'] = $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url . '&sort=zd.name', 'SSL');
        $data['sort_mall'] = $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url . '&sort=md.name', 'SSL');
        $data['sort_main_store'] = $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url . '&sort=msd2.main_store_id', 'SSL');

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('mall/shop_list.tpl', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_attribute'] = $this->language->get('tab_attribute');

        $data['text_form'] = !isset($this->request->get['shop_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');

        //-- Description/Attribute
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_location'] = $this->language->get('entry_location');

        //-- General
        $data['entry_phone'] = $this->language->get('entry_phone');
        $data['entry_url'] = $this->language->get('entry_url');
        $data['entry_ssl'] = $this->language->get('entry_ssl');
        $data['entry_city_id'] = $this->language->get('entry_city_id');
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_mall_id'] = $this->language->get('entry_mall_id');
        $data['entry_main_store_id'] = $this->language->get('entry_main_store_id');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_open'] = $this->language->get('entry_open');
        $data['entry_approved'] = $this->language->get('entry_approved');
        $data['entry_landing'] = $this->language->get('entry_landing');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_display'] = $this->language->get('entry_display');
        $data['entry_latitude'] = $this->language->get('entry_latitude');
        $data['entry_longitude'] = $this->language->get('entry_longitude');
        $data['entry_zoom'] = $this->language->get('entry_zoom');
        $data['entry_website'] = $this->language->get('entry_website');
        $data['entry_social_jeeran'] = $this->language->get('entry_social_jeeran');
        $data['entry_social_fb'] = $this->language->get('entry_social_fb');
        $data['entry_social_tw'] = $this->language->get('entry_social_tw');
        $data['entry_meta_title'] = $this->language->get('entry_meta_title');
        $data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
        $data['entry_meta_tag'] = $this->language->get('entry_meta_tag');
        $data['entry_form_store_category'] = $this->language->get('entry_form_store_category');
        $data['entry_store_brands'] = $this->language->get('entry_store_brands');


        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }
        if (isset($this->error['countries'])) {
            $data['error_countries'] = $this->error['countries'];
        } else {
            $data['error_countries'] = array();
        }
        if (isset($this->error['mall_id'])) {
            $data['error_mall_id'] = $this->error['mall_id'];
        } else {
            $data['error_mall_id'] = array();
        }

        if (isset($this->error['error_google_latitude'])) {
            $data['error_google_latitude'] = $this->error['error_google_latitude'];
        } else {
            $data['error_google_latitude'] = '';
        }

        if (isset($this->error['error_google_longitude'])) {
            $data['error_google_longitude'] = $this->error['error_google_longitude'];
        } else {
            $data['error_google_longitude'] = '';
        }

        if (isset($this->error['error_google_zoom'])) {
            $data['error_google_zoom'] = $this->error['error_google_zoom'];
        } else {
            $data['error_google_zoom'] = '';
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['shop_id'])) {
            $data['action'] = $this->url->link('mall/shop/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('mall/shop/edit', 'token=' . $this->session->data['token'] . '&shop_id=' . $this->request->get['shop_id'] . $url, 'SSL');
        }

        $data['cancel'] = $this->url->link('mall/shop', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['shop_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $shop_info = $this->model_mall_shop->getShop($this->request->get['shop_id']);
            $google = $this->model_mall_shop->getGoogle($this->request->get['shop_id']);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();


        if (isset($this->request->post['shop_description'])) {
            $data['shop_description'] = $this->request->post['shop_description'];
        } elseif (isset($this->request->get['shop_id'])) {
            $data['shop_description'] = $this->model_mall_shop->getShopDescriptions($this->request->get['shop_id']);
        } else {
            $data['shop_description'] = array();
        }

        $this->load->model('localisation/country');
        $countries = $this->model_localisation_country->getAvailableCountries();

        foreach ($countries as $value) {
            $data['countries'][$value['country_id']] = $value['name'];
        }

        if (isset($this->request->post['city_id'])) {
            $data['city_id'] = $this->request->post['city_id'];
        } elseif (!empty($shop_info)) {
            $data['city_id'] = $shop_info['zone_id'];
        } else {
            $data['city_id'] = 0;
        }

        $this->load->model('localisation/zone');
        $zone_info = $this->model_localisation_zone->getZone($data['city_id']);
        $data['country_id'] = $zone_info['country_id'];

        if (isset($this->request->post['mall_id'])) {
            $data['mall_id'] = $this->request->post['mall_id'];
        } elseif (!empty($shop_info)) {
            $data['mall_id'] = $shop_info['mall_id'];
        } else {
            $data['mall_id'] = 0;
        }

        $this->load->model('mall/mall');
        $data['malls'] = $this->model_mall_mall->getMalls(array('filter_city_id' => $data['city_id']));

        if (isset($this->request->post['main_store_id'])) {
            $data['main_store_id'] = $this->request->post['main_store_id'];
        } elseif (!empty($shop_info)) {
            $data['main_store_id'] = $shop_info['main_store_id'];
        } else {
            $data['main_store_id'] = 0;
        }

        $this->load->model('mall/main_store');
        $data['main_stores'] = $this->model_mall_main_store->getMainStores(array('filter_country_id' => $data['country_id']));

        if (isset($this->request->post['phone'])) {
            $data['phone'] = $this->request->post['phone'];
        } elseif (!empty($shop_info)) {
            $data['phone'] = $shop_info['phone'];
        } else {
            $data['phone'] = 0;
        }

        if (isset($this->request->post['url'])) {
            $data['url'] = $this->request->post['url'];
        } elseif (!empty($shop_info)) {
            $data['url'] = $shop_info['url'];
        } else {
            $data['url'] = "";
        }

        if (isset($this->request->post['ssl'])) {
            $data['ssl'] = $this->request->post['ssl'];
        } elseif (!empty($shop_info)) {
            $data['ssl'] = $shop_info['ssl'];
        } else {
            $data['ssl'] = "";
        }

        if (isset($this->request->post['website'])) {
            $data['website'] = $this->request->post['website'];
        } elseif (!empty($shop_info)) {
            $data['website'] = $shop_info['social_website'];
        } else {
            $data['website'] = "";
        }

        if (isset($this->request->post['social_jeeran'])) {
            $data['social_jeeran'] = $this->request->post['social_jeeran'];
        } elseif (!empty($shop_info)) {
            $data['social_jeeran'] = $shop_info['social_jeeran'];
        } else {
            $data['social_jeeran'] = "";
        }

        if (isset($this->request->post['social_fb'])) {
            $data['social_fb'] = $this->request->post['social_fb'];
        } elseif (!empty($shop_info)) {
            $data['social_fb'] = $shop_info['social_facebook'];
        } else {
            $data['social_fb'] = "";
        }

        if (isset($this->request->post['social_tw'])) {
            $data['social_tw'] = $this->request->post['social_tw'];
        } elseif (!empty($shop_info)) {
            $data['social_tw'] = $shop_info['social_twitter'];
        } else {
            $data['social_tw'] = "";
        }

        if (isset($this->request->post['landing'])) {
            $data['landing'] = $this->request->post['landing'];
        } elseif (!empty($shop_info)) {
            $data['landing'] = $shop_info['landing'];
        } else {
            $data['landing'] = "";
        }

        if (isset($this->request->post['latitude'])) {
            $data['latitude'] = $this->request->post['latitude'];
        } elseif (!empty($shop_info) && $google['latitude'] > 0) {
            $data['latitude'] = $google['latitude'];
        } else {
            $data['latitude'] = '';
        }

        if (isset($this->request->post['longitude'])) {
            $data['longitude'] = $this->request->post['longitude'];
        } elseif (!empty($shop_info) && $google['longitude'] > 0) {
            $data['longitude'] = $google['longitude'];
        } else {
            $data['longitude'] = '';
        }

        if (isset($this->request->post['zoom'])) {
            $data['zoom'] = $this->request->post['zoom'];
        } elseif (!empty($shop_info)) {
            $data['zoom'] = $google['zoom'];
        } else {
            $data['zoom'] = 14;
        }

        if (!isset($this->request->post['user_id'])) {
            $data['user_id'] = $this->session->data['user_id'];
        } elseif (!empty($shop_info)) {
            $data['user_id'] = $shop_info['user_id'];
        } else {
            $data['user_id'] = ($this->request->post['user_id'] != $this->session->data['user_id']) ? $this->session->data['user_id'] : $this->request->post['user_id'];
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($shop_info)) {
            $data['status'] = $shop_info['status'];
        } else {
            $data['status'] = 0;
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($shop_info)) {
            $data['sort_order'] = $shop_info['sort_order'];
        } else {
            $data['sort_order'] = 0;
        }

        if (isset($this->request->post['display'])) {
            $data['display'] = $this->request->post['display'];
        } elseif (!empty($shop_info)) {
            $data['display'] = $shop_info['display'];
        } else {
            $data['display'] = 0;
        }

        //store_category
        $this->load->model('mall/store_category');
        $data['store_category'] = array();
        if (isset($this->request->get['shop_id'])) {
            $filter_store_id = (int) $this->request->get['shop_id'];
            $results = $this->model_mall_store_category->getStoreCategoriesByStore($filter_store_id);
            foreach ($results as $result) {
                $data['store_category'][] = array(
                    'store_category_id' => $result['store_category_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        //store_brands
        $this->load->model('mall/brand');
        $data['store_brands'] = array();
        if (isset($this->request->get['shop_id'])) {
            $filter_store_id = (int) $this->request->get['shop_id'];
            $results = $this->model_mall_brand->getBrandsByStore($filter_store_id);
            foreach ($results as $result) {
                $data['store_brands'][] = array(
                    'brand_id' => $result['brand_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // echo "<pre>";print_r($data);die("OK !!");

        $this->response->setOutput($this->load->view('mall/shop_form.tpl', $data));
    }

    public function getmalls() {
        $json = array();

        $filter_data = array(
            'filter_city_id' => $this->request->get['filter_city_id']
        );
        $this->load->model('mall/mall');
        $results = $this->model_mall_mall->getMalls($filter_data);

        foreach ($results as $result) {
            $json[] = array(
                'mall_id' => $result['mall_id'],
                'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getZone() {
        $json = array();

        $this->load->model('localisation/zone');
        $results = $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']);

        foreach ($results as $result) {
            $json[] = array(
                'zone_id' => $result['zone_id'],
                'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function validateRefresh() {
        if (!$this->user->hasPermission('modify', 'mall/shop')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'mall/shop')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        return !$this->error;
    }

    protected function validateForm() {

        if (!$this->user->hasPermission('modify', 'mall/shop')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->model('localisation/language');
        $Languages = $this->model_localisation_language->getLanguages();

        foreach ($Languages as $value) {
            if (!isset($this->request->post['shop_description'][$value['language_id']]['name']) || empty($this->request->post['shop_description'][$value['language_id']]['name'])) {
                $this->error['name'][$value['language_id']] = $this->language->get('error_name');
            }
        }

        if ((!isset($this->request->post['city_id'])) || ((int) $this->request->post['city_id'] <= 0)) {
            $this->error['city_id'] = $this->language->get('error_city_id');
        }

        if ((!isset($this->request->post['mall_id'])) || ((int) $this->request->post['mall_id'] <= 0)) {
            $this->error['mall_id'] = $this->language->get('error_mall_id');
        }

        if ((!isset($this->request->post['main_store_id'])) || ((int) $this->request->post['main_store_id'] <= 0)) {
            $this->error['main_store_id'] = $this->language->get('error_main_store_id');
        }

        if ((isset($this->request->post['landing']))) {
            if ($this->request->post['landing'] < 0 && $this->request->post['landing'] > 9999) {
                $this->error['landing'] = $this->language->get('error_landing');
            }
        }

        if ((isset($this->request->post['approved']))) {
            if ($this->request->post['approved'] < 0 && $this->request->post['approved'] > 9999) {
                $this->error['approved'] = $this->language->get('error_approved');
            }
        }

        if (isset($this->request->post['latitude']) && strlen($this->request->post['latitude'])) {
            ///^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}$/
            if (!(preg_match("/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/", $this->request->post['latitude']))) {
                $this->error['error_google_latitude'] = $this->language->get('error_google_latitude');
            }
        }
        if (isset($this->request->post['longitude']) && strlen($this->request->post['longitude'])) {
            // if( !(preg_match("/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/", $this->request->post['longitude'])) ){

            if (!preg_match("/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}$/", $this->request->post['longitude'])) {
                $this->error['error_google_longitude'] = $this->language->get('error_google_longitude');
            }
        }
        if (isset($this->request->post['zoom'])) {
            if ($this->request->post['zoom'] < 1 || $this->request->post['zoom'] > 21) {
                $this->error['error_google_zoom'] = $this->language->get('error_google_zoom');
            }
        }

        return !$this->error;
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('mall/shop');

            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'sort' => 'name',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 20
            );

            $results = $this->model_mall_shop->getShopsByName($filter_data);

            foreach ($results as $result) {
                if (isset($this->request->get['product']) && $this->request->get['product'] == 1) {
                    $json[] = array(
                        'store_id' => $result['store_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                } else {
                    $json[] = array(
                        'id' => $result['store_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
