<?php

class ControllerMallTag extends Controller {

    protected $name = 'tag';
    private $error = array();

    public function index() {
        $this->language->load("mall/{$this->name}");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/{$this->name}");
        $this->getList();
    }

    public function add() {
        $this->language->load("mall/{$this->name}");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/{$this->name}");

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm(false)) {
            $this->model_mall_tag->addTag($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link("mall/{$this->name}", 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }
        $this->getForm();
    }

    public function edit() {
        $this->language->load("mall/{$this->name}");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/{$this->name}");

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm(false)) {
            $this->model_mall_tag->editTag($this->request->get["{$this->name}_id"], $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get["{$this->name}_id"])) {
                $url .= "&{$this->name}_id=" . $this->request->get["{$this->name}_id"];
            }
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link("mall/{$this->name}/edit", 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    protected function validateForm($exists = true) {
        if (!$this->user->hasPermission('modify', "mall/{$this->name}")) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post['item_description'] as $language_id => $value) {
            if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 255)) {
                $this->error['name'][$language_id] = $this->language->get('error_title');
            }
        }

        if (!isset($this->request->post['author']) || !utf8_strlen($this->request->post['author'])) {
            $this->error['error_author'] = $this->language->get('error_author');
        }

        return !$this->error;
    }

    /**
     * TODO: add country filter
     */
    protected function getList() {
        $data = $search = $this->getSearchValues();
        $data['controller_name'] = $this->name;

        $url = '';

        foreach ($search['filter'] as $for_search_filter_name => $for_search_filter_val) {
            if (isset($for_search_filter_val)) {
                $url .= "&filter[{$for_search_filter_name}]={$for_search_filter_val}";
            }
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link("mall/{$this->name}", 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link("mall/{$this->name}/add", 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link("mall/{$this->name}/delete", 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->load->model('tool/image');

        $data['items'] = array();

        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        $data['countries'] = $aDBCountries;

        $tags_total = $this->model_mall_tag->getTotalTag($search);
        $results = $this->model_mall_tag->getTags($search);

        foreach ($results as $result) {

            if (is_file(DIR_IMAGE . $result['image'])) {
                $image = $this->model_tool_image->resize($result['image'], 40, 40, false);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 40, 40, false);
            }

            $data['items'][$result["{$this->name}_id"]] = array(
                "{$this->name}_id" => $result["{$this->name}_id"],
                'image' => $image,
                'name' => $result['name'],
                'status' => $result['status'],
                'date_added' => $result['date_added'],
                'date_modified' => $result['date_modified'],
                'sort_order' => $result['sort_order'],
                'edit' => $this->url->link("mall/{$this->name}/edit", 'token=' . $this->session->data['token'] . "&{$this->name}_id=" . $result["{$this->name}_id"] . $url, 'SSL'),
                'delete' => $this->url->link("mall/{$this->name}/delete", 'token=' . $this->session->data['token'] . "&{$this->name}_id=" . $result["{$this->name}_id"] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_select'] = $this->language->get('text_select');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_country'] = $this->language->get('entry_country');

        $data['select_status'] = $this->language->get('select_status');
        $data['select_country'] = $this->language->get('select_country');
        $data['button_filter'] = $this->language->get('button_filter');

        $data['token'] = $this->session->data['token'];

        $data['column_image'] = $this->language->get('column_image');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $pagination = new Pagination();
        $pagination->total = $tags_total;
        $pagination->page = $search['page'];
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link("mall/{$this->name}", 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($tags_total) ? (($search['page'] - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($search['page'] - 1) * $this->config->get('config_limit_admin')) > ($tags_total - $this->config->get('config_limit_admin'))) ? $tags_total : ((($search['page'] - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $tags_total, ceil($tags_total / $this->config->get('config_limit_admin')));


        if ($search['sort']['order'] == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }
        $data['sort_name'] = $this->url->link("mall/{$this->name}", 'token=' . $this->session->data['token'] . $url . '&sort=name', 'SSL');
        $data['sort_status'] = $this->url->link("mall/{$this->name}", 'token=' . $this->session->data['token'] . $url . '&sort=status', 'SSL');
        $data['sort_date_added'] = $this->url->link("mall/{$this->name}", 'token=' . $this->session->data['token'] . $url . '&sort=date_added', 'SSL');
        $data['sort_date_modified'] = $this->url->link("mall/{$this->name}", 'token=' . $this->session->data['token'] . $url . '&sort=date_modified', 'SSL');
        $data['sort_order'] = $this->url->link("mall/{$this->name}", 'token=' . $this->session->data['token'] . $url . '&sort=sort_order', 'SSL');

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');


        $this->response->setOutput($this->load->view("mall/{$this->name}_list.tpl", $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');
        $data['controller_name'] = $this->name;

        $data['bActiveLanguageTabFlag'] = true;
        $data['bActiveLanguagePaneFlag'] = true;

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_attribute'] = $this->language->get('tab_attribute');
        $data['tab_links'] = $this->language->get('tab_links');
        $data['tab_image'] = $this->language->get('tab_image');

        $data['text_form'] = !isset($this->request->get['tip_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');

        $data['text_home'] = $this->language->get('text_home');
        $data['text_is_home'] = $this->language->get('text_is_home');
        $data['text_not_home'] = $this->language->get('text_not_home');

        //-- Description/Attribute
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_location'] = $this->language->get('entry_location');

        $data['entry_related'] = $this->language->get('entry_related');
        $data['help_related'] = $this->language->get('help_related');



        //-- General
        $data['entry_home'] = $this->language->get('entry_home');
        $data['entry_author'] = $this->language->get('entry_author');
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_mall_id'] = $this->language->get('entry_mall_id');
        $data['entry_main_store_id'] = $this->language->get('entry_main_store_id');
        $data['entry_shop_id'] = $this->language->get('entry_shop_id');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_website'] = $this->language->get('entry_website');
        $data['entry_social_fb'] = $this->language->get('entry_social_fb');
        $data['entry_social_tw'] = $this->language->get('entry_social_tw');

        $data['entry_related_product'] = $this->language->get('entry_related_product');
        $data['entry_help_product'] = $this->language->get('entry_help_product');

        $data['entry_related_tip'] = $this->language->get('entry_related_tip');
        $data['entry_help_tip'] = $this->language->get('entry_help_tip');

        $data['entry_related_look'] = $this->language->get('entry_related_look');
        $data['entry_help_look'] = $this->language->get('entry_help_look');

        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_category_id'] = $this->language->get('entry_category_id');

        $data['entry_start_date'] = $this->language->get('entry_start_date');
        $data['entry_end_date '] = $this->language->get('entry_end_date');

        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_featured'] = $this->language->get('entry_featured');

        $data['entry_meta_title'] = $this->language->get('entry_meta_title');
        $data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $data['entry_meta_keywords'] = $this->language->get('entry_meta_keywords');

        $data['button_remove'] = $this->language->get('button_remove');
        $data['button_image_add'] = $this->language->get('button_image_add');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }
        if (isset($this->error['countries'])) {
            $data['error_countries'] = $this->error['countries'];
        } else {
            $data['error_countries'] = array();
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/tag', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get["{$this->name}_id"])) {
            $data['action'] = $this->url->link("mall/{$this->name}/add", 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link("mall/{$this->name}/edit", 'token=' . $this->session->data['token'] . "&{$this->name}_id=" . $this->request->get["{$this->name}_id"] . $url, 'SSL');
        }
        $data['cancel'] = $this->url->link("mall/{$this->name}", 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get["{$this->name}_id"])) {
            $item_info = $this->model_mall_tag->getTag($this->request->get["{$this->name}_id"]);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();


        // echo "<pre>";die(print_r($data['item_description']));

        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        foreach ($aDBCountries as $key => $value) {
            $data['countries'][$value['country_id']] = $value['name'];
        }


        $this->load->model('localisation/zone');
        $data['country_id'] = 0;
        if (isset($data['city_id']) && $data['city_id'] > 0) {
            $zone_info = $this->model_localisation_zone->getZone($data['city_id']);
            if (!empty($zone_info)) {
                $data['country_id'] = $zone_info['country_id'];
            }
        }

        if (isset($this->request->post['item_description'])) {
            $data['item_description'] = $this->request->post['item_description'];
        } elseif (isset($this->request->get["{$this->name}_id"])) {
            $data['item_description'] = $this->model_mall_tag->gettagDescriptions($this->request->get["{$this->name}_id"]);
        } else {
            $data['item_description'] = array();
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($item_info)) {
            $data['image'] = $item_info['image'];
        } else {
            $data['image'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($item_info)) {
            $data['status'] = $item_info['status'];
        } else {
            $data['status'] = 0;
        }

        if (isset($this->request->post['home'])) {
            $data['home'] = $this->request->post['home'];
        } elseif (!empty($item_info)) {
            $data['home'] = $item_info['home'];
        } else {
            $data['home'] = 0;
        }

        if (isset($this->request->post['author'])) {
            $data['author'] = $this->request->post['author'];
        } elseif (!empty($item_info)) {
            $data['author'] = $item_info['author'];
        } else {
            $data['author'] = '';
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($item_info)) {
            $data['sort_order'] = $item_info['sort_order'];
        } else {
            $data['sort_order'] = 0;
        }

        //-- Images

        $this->load->model('tool/image');
        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($item_info) && is_file(DIR_IMAGE . $item_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($item_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('placeholder.png', 100, 100);
        }

        $data['item_link'] = array();
        if (isset($this->request->get["{$this->name}_id"])) {
            $filter_item_id = $this->request->get["{$this->name}_id"];
            $item_link_results = $this->model_mall_tag->getTagLink($filter_item_id);
            foreach ($item_link_results as $link_type => $dataSet) {
                foreach ($dataSet as $result) {
                    $data['item_link'][$link_type][] = array(
                        'id' => $result['id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        }

        $data['data'] = $this->model_mall_tag;

        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view("mall/{$this->name}_form.tpl", $data));
    }

    public function delete() {
        $this->language->load("mall/{$this->name}");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/{$this->name}");

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $item_id) {
                $this->model_mall_tag->deleteTag($item_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link("mall/{$this->name}", 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function validateDelete() {
        return true;
    }

    public function autocomplete() {

        // die(print_r($this->request->get));

        $json = array();

        if (isset($this->request->get['query'])) {

            if (isset($this->request->get['query'])) {
                $filter_name = $this->request->get['query'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['type'])) {
                $type = $this->request->get['type'];
            } else {
                $type = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array('filter_name' => $filter_name, 'filter_keyword' => $filter_name);

            if ($type == 'country') {
                $this->load->model('localisation/country');
                $results = $this->model_localisation_country->getCountriesByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['country_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'category') {
                $this->load->model('mall/store_category');
                $results = $this->model_mall_store_category->getStoreCategoryByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_category_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'shop') {
                $this->load->model('mall/shop');
                $results = $this->model_mall_shop->getshopsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')) . ' ( ' . strip_tags(html_entity_decode($result['mall_name'], ENT_QUOTES, 'UTF-8')) . ' ) '
                    );
                }
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function getSearchValues() {
        $config_limit = $this->config->get('config_limit_admin');

        $columns = array_keys($this->model_mall_tag->getColumns());
        $filter = array_combine($columns, array_pad(array(), count($columns), ''));
        if (isset($this->request->get['filter'])) {
            $filter = array_map(function($value) {
                return ($value === '') ? null : (is_numeric($value) ? intval($value) : $value);
            }, array_merge($filter, (array) $this->request->get['filter']));
        }

        $page = 1;
        if (isset($this->request->get['page'])) {
            $page = intval($this->request->get['page'])? : $page;
        }

        $sort = array('column' => null, 'order' => null);
        if (isset($this->request->get['sort'])) {
            $sort['column'] = $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $sort['order'] = $this->request->get['order'];
        }

        return array(
            'filter' => $filter,
            'sort' => $sort,
            'page' => $page,
            'start' => ($page - 1) * $config_limit,
            'limit' => $config_limit
        );
    }

}
