<?php

class ControllerMallBrand extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('mall/brand');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/brand');
        $this->getList();
    }

    public function add() {
        $this->language->load('mall/brand');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/brand');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $brand_id = $this->model_mall_brand->addBrand($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($brand_id) && !empty($brand_id)) {
                $url .= '&filter_brand_id=' . $brand_id;
            }
            $this->response->redirect($this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }
        $this->getForm();
    }

    public function edit() {
        $this->language->load('mall/brand');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/brand');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

            $this->model_mall_brand->editBrand($this->request->get['brand_id'], $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($this->request->get['brand_id']) && !empty($this->request->get['brand_id'])) {
                $url .= '&filter_brand_id=' . $this->request->get['brand_id'];
            }
            $this->response->redirect($this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    protected function getList() {
        if (isset($this->request->get['filter_filter'])) {
            $filter_filter = $this->request->get['filter_filter'];
        } else {
            $filter_filter = NULL;
        }

        if (isset($this->request->get['filter_brand_id'])) {
            $filter_brand_id = $this->request->get['filter_brand_id'];
            $data['filter_brand_id'] = $this->request->get['filter_brand_id'];
        } else {
            $filter_brand_id = null;
        }

        if (isset($this->request->get['filter_status'])) {
            $filter_status = (int) $this->request->get['filter_status'];
        } else {
            $filter_status = NULL;
        }

        if (isset($this->request->get['filter_shopable'])) {
            $filter_shopable = (int) $this->request->get['filter_shopable'];
        } else {
            $filter_shopable = NULL;
        }

        if (isset($this->request->get['filter_country'])) {
            $filter_country = (int) $this->request->get['filter_country'];
        } else {
            $filter_country = NULL;
        }

        if (isset($this->request->get['filter_category'])) {
            $filter_category = (int) $this->request->get['filter_category'];
        } else {
            $filter_category = NULL;
        }

        if (isset($this->request->get['filter_date_added'])) {
            $filter_date_added = $this->request->get['filter_date_added'];
        } else {
            $filter_date_added = NULL;
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $filter_date_modified = $this->request->get['filter_date_modified'];
        } else {
            $filter_date_modified = NULL;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'bd' . (int) $this->config->get('config_language_id') . '.name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_filter'])) {
            $url .= '&filter_filter=' . $this->request->get['filter_filter'];
        }

        if (isset($this->request->get['filter_brand_id'])) {
            $url .= '&filter_brand_id=' . $this->request->get['filter_brand_id'];
        }

        if (isset($this->request->get['filter_country'])) {
            $url .= '&filter_country=' . $this->request->get['filter_country'];
        }

        if (isset($this->request->get['filter_category'])) {
            $url .= '&filter_category=' . $this->request->get['filter_category'];
        }

        if (isset($this->request->get['filter_shopable'])) {
            $url .= "&filter_shopable=" . $this->request->get['filter_shopable'];
        }

        if (isset($this->request->get['filter_city'])) {
            $url .= '&filter_city=' . $this->request->get['filter_city'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('mall/brand/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('mall/brand/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->load->model('tool/image');

        $data['brands'] = array();

        $filter_data = array(
            'filter_filter' => $filter_filter,
            'filter_brand_id' => $filter_brand_id,
            'filter_status' => $filter_status,
            'filter_shopable' => $filter_shopable,
            'filter_country' => $filter_country,
            'filter_category' => $filter_category,
            'filter_date_added' => $filter_date_added,
            'filter_date_modified' => $filter_date_modified,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $brands_total = $this->model_mall_brand->getTotalBrand($filter_data);

        $results = $this->model_mall_brand->getBrands($filter_data);

        foreach ($results as $result) {

            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], 40, 40,false);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 40, 40,false);
            }

            $data['brands'][$result['brand_id']] = array(
                'brand_id' => $result['brand_id'],
                'image' => $image,
                'name' => $result['name'],
                'status' => $result['status'],
                'date_added' => $result['date_added'],
                'date_modified' => $result['date_modified'],
                'sort_order' => $result['sort_order'],
                'user_add' => $result['user_add'],
                'user_modify' => $result['user_modify'],
                'edit' => $this->url->link('mall/brand/edit', 'token=' . $this->session->data['token'] . '&brand_id=' . $result['brand_id'] . $url, 'SSL'),
                'delete' => $this->url->link('mall/brand/delete', 'token=' . $this->session->data['token'] . '&brand_id=' . $result['brand_id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_select'] = $this->language->get('text_select');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['entry_shopable'] = $this->language->get('entry_shopable');
        $data['select_shopable'] = $this->language->get('select_shopable');
        $data['select_country'] = $this->language->get('select_country');
        $data['text_shopable'] = $this->language->get('text_shopable');
        $data['text_all_products'] = $this->language->get('text_all_products');
        $data['column_user_id'] = $this->language->get('column_user_id');
        $data['column_last_mod_id'] = $this->language->get('column_last_mod_id');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['entry_category'] = $this->language->get('entry_category');
        $data['entry_algolia'] = $this->language->get('entry_algolia');



        $data['select_status'] = $this->language->get('select_status');
        $data['select_category'] = $this->language->get('select_category');
        $data['button_filter'] = $this->language->get('button_filter');

        $data['token'] = $this->session->data['token'];

        $data['column_image'] = $this->language->get('column_image');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $pagination = new Pagination();
        $pagination->total = $brands_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($brands_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($brands_total - $this->config->get('config_limit_admin'))) ? $brands_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $brands_total, ceil($brands_total / $this->config->get('config_limit_admin')));

        $data['filter_filter'] = $filter_filter;
        $data['filter_shopable'] = $filter_shopable;
        $data['filter_country'] = $filter_country;
        $data['filter_category'] = $filter_category;
        $data['filter_status'] = $filter_status;
        $data['filter_date_added'] = $filter_date_added;
        $data['filter_date_modified'] = $filter_date_modified;

        $this->load->model('localisation/stock_status');
        $data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }
        $data['sort_name'] = $this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url . '&sort=bd.name', 'SSL');
        $data['sort_status'] = $this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url . '&sort=b.status', 'SSL');
        $data['sort_date_added'] = $this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url . '&sort=b.date_added', 'SSL');
        $data['sort_date_modified'] = $this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url . '&sort=b.date_modified', 'SSL');
        $data['sort_order'] = $this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url . '&sort=b.sort_order', 'SSL');

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');


        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        foreach ($aDBCountries as $key => $value) {
            $data['countries'][$value['country_id']] = $value['name'];
        }

        $data['categories'] = $this->model_mall_brand->getFilterCategories();

        $this->response->setOutput($this->load->view('mall/brand_list.tpl', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['bActiveLanguageTabFlag'] = true;
        $data['bActiveLanguagePaneFlag'] = true;

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_attribute'] = $this->language->get('tab_attribute');
        $data['tab_links'] = $this->language->get('tab_links');
        $data['tab_image'] = $this->language->get('tab_image');

        $data['text_form'] = !isset($this->request->get['brand_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');

        //-- Description/Attribute
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_location'] = $this->language->get('entry_location');
        $data['entry_meta_title'] = $this->language->get('entry_meta_title');
        $data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');


        //-- General
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_mall_id'] = $this->language->get('entry_mall_id');
        $data['entry_main_store_id'] = $this->language->get('entry_main_store_id');
        $data['entry_shop_id'] = $this->language->get('entry_shop_id');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_fbfeeds'] = $this->language->get('entry_fbfeeds');
        $data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_website'] = $this->language->get('entry_website');
        $data['entry_social_fb'] = $this->language->get('entry_social_fb');
        $data['entry_social_tw'] = $this->language->get('entry_social_tw');
        $data['entry_show_api'] = $this->language->get('entry_show_api');

        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_category_id'] = $this->language->get('entry_category_id');

        $data['entry_start_date'] = $this->language->get('entry_start_date');
        $data['entry_end_date '] = $this->language->get('entry_end_date');

        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_featured'] = $this->language->get('entry_featured');
        $data['entry_algolia'] = $this->language->get('entry_algolia');


        $data['button_remove'] = $this->language->get('button_remove');
        $data['button_image_add'] = $this->language->get('button_image_add');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }
        if (isset($this->error['countries'])) {
            $data['error_countries'] = $this->error['countries'];
        } else {
            $data['error_countries'] = array();
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['brand_id'])) {
            $data['action'] = $this->url->link('mall/brand/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('mall/brand/edit', 'token=' . $this->session->data['token'] . '&brand_id=' . $this->request->get['brand_id'] . $url, 'SSL');
        }
        $data['cancel'] = $this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['brand_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $brand_info = $this->model_mall_brand->getBrand($this->request->get['brand_id']);
        }


        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();


        if (isset($this->request->post['brand_description'])) {
            $data['brand_description'] = $this->request->post['brand_description'];
        } elseif (isset($this->request->get['brand_id'])) {
            $data['brand_description'] = $this->model_mall_brand->getBrandDescriptions($this->request->get['brand_id']);
        } else {
            $data['brand_description'] = array();
        }


        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($brand_info)) {
            $data['image'] = $brand_info['image'];
        } else {
            $data['image'] = '';
        }
        $this->load->model('tool/image');

        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100,false);
        } elseif (!empty($brand_info) && is_file(DIR_IMAGE . $brand_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($brand_info['image'], 100, 100,false);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('placeholder.png', 100, 100,false);
        }

        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        foreach ($aDBCountries as $key => $value) {
            $data['countries'][$value['country_id']] = $value['name'];
        }


        $this->load->model('localisation/zone');
        $data['country_id'] = 0;
        if (isset($data['city_id']) && $data['city_id'] > 0) {
            $zone_info = $this->model_localisation_zone->getZone($data['city_id']);
            if (!empty($zone_info)) {
                $data['country_id'] = $zone_info['country_id'];
            }
        }

        if (isset($this->request->post['website'])) {
            $data['website'] = $this->request->post['website'];
        } elseif (!empty($brand_info)) {
            $data['website'] = $brand_info['link'];
        } else {
            $data['website'] = "";
        }

        if (isset($this->request->post['social_fb'])) {
            $data['social_fb'] = $this->request->post['social_fb'];
        } elseif (!empty($brand_info)) {
            $data['social_fb'] = $brand_info['social_fb'];
        } else {
            $data['social_fb'] = "";
        }

        if (isset($this->request->post['social_tw'])) {
            $data['social_tw'] = $this->request->post['social_tw'];
        } elseif (!empty($brand_info)) {
            $data['social_tw'] = $brand_info['social_tw'];
        } else {
            $data['social_tw'] = "";
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($brand_info)) {
            $data['status'] = $brand_info['status'];
        } else {
            $data['status'] = 0;
        }

        if (isset($this->request->post['fbfeeds'])) {
            $data['status'] = $this->request->post['fbfeeds'];
        } elseif (!empty($brand_info)) {
            $data['fbfeeds'] = $brand_info['fbfeeds'];
        } else {
            $data['fbfeeds'] = 1;
        }

        if (isset($this->request->post['is_algolia'])) {
            $data['is_algolia'] = $this->request->post['is_algolia'];
        } elseif (!empty($brand_info)) {
            $data['is_algolia'] = $brand_info['is_algolia'];
        } else {
            $data['is_algolia'] = 0;
        }

        if (isset($this->request->post['show_api'])) {
            $data['show_api'] = $this->request->post['show_api'];
        } elseif (!empty($brand_info)) {
            $data['show_api'] = $brand_info['show_api'];
        } else {
            $data['show_api'] = 'yes';
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($brand_info)) {
            $data['sort_order'] = $brand_info['sort_order'];
        } else {
            $data['sort_order'] = 0;
        }

        if (isset($this->request->get['brand_id'])) {
            $filter_brand_id = $this->request->get['brand_id'];
            $brand_link_results = $this->model_mall_brand->getBrandLink($filter_brand_id);
            foreach ($brand_link_results as $link_type => $dataSet) {
                foreach ($dataSet as $result) {
                    $data['brand_link'][$link_type][] = array(
                        'id' => $result['id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        }

        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('mall/brand_form.tpl', $data));
    }

    public function delete($brand_id) {
        $this->language->load('mall/brand');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/brand');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $brand_id) {
                $this->model_mall_brand->deleteBrand($brand_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('mall/brand', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function validateDelete() {
        return true;
    }

    public function autocomplete() {

        // die(print_r($this->request->get));

        $json = array();
        $keyword = '';

        if (isset($this->request->get['filter_keyword'])) {
            $keyword = $this->request->get['filter_keyword'];
        } elseif (isset($this->request->get['query'])) {
            $keyword = $this->request->get['query'];
        }

        if (isset($keyword) && !empty($keyword)) {

            if (isset($keyword)) {
                $filter_name = $keyword;
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['type'])) {
                $type = $this->request->get['type'];
            } else {
                $type = '';
            }

            if (isset($this->request->get['start'])) {
                $start = $this->request->get['start'];
            } else {
                $start = 0;
            }

            if(isset($this->request->get['filter_status'])){
                $filter_status = $this->request->get['filter_status'];
            } else {
                $filter_status = null;
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 10;
            }

            $filter_data = array(
                'filter_name' => $filter_name,
                'filter_keyword' => $filter_name,
                'filter_status' => $filter_status,
                'start'=>$start,
                'limit' => $limit
                    );

            if ($type == 'country') {
                $this->load->model('localisation/country');
                $results = $this->model_localisation_country->getCountriesByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['country_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'category') {
                $this->load->model('mall/store_category');
                $results = $this->model_mall_store_category->getStoreCategoryByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_category_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'shop') {
                $this->load->model('mall/shop');
                $results = $this->model_mall_shop->getshopsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')) . ' ( ' . strip_tags(html_entity_decode($result['mall_name'], ENT_QUOTES, 'UTF-8')) . ' ) '
                    );
                }
            } else if ($type == 'brand') {

                $this->load->model('mall/brand');

                $filter_data = array('filter_filter' => $filter_name, 'start' => 0, 'limit' => 7);

                $results = $this->model_mall_brand->getBrands($filter_data);

                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['brand_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else {

                $this->load->model('mall/brand');
                $results = $this->model_mall_brand->getBrands($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['brand_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        }


        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
