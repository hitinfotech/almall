<?php

class ControllerMallKeyword extends Controller {

    private $error = array();

    public function index() {
        $this->language->load("mall/keyword");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/keyword");

        $this->getList();
    }

    public function add() {
        $this->language->load("mall/keyword");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/keyword");

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $keyword_id = $this->model_mall_keyword->add($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($keyword_id) && !empty($keyword_id)) {
                $url .= '&filter[id]=' . $keyword_id;
            }
            $this->response->redirect($this->url->link("mall/keyword", 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }
        $this->getForm();
    }

    public function edit() {
        $this->language->load("mall/keyword");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/keyword");

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_mall_keyword->edit($this->request->get["keyword_id"], $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
//            if (isset($this->request->get["keyword_id"])) {
//                $url .= "&keyword_id=" . $this->request->get["keyword_id"];
//            }
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($this->request->get["keyword_id"]) && !empty($this->request->get["keyword_id"])) {
                $url .= '&filter[id]=' . $this->request->get["keyword_id"];
            }
            $this->response->redirect($this->url->link("mall/keyword", 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    protected function validateForm() {

        if (!$this->user->hasPermission('modify', "mall/keyword")) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        foreach ($this->request->post['keyword_description'] as $language_id => $value) {
            if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 255)) {
                $this->error['name'][$language_id] = $this->language->get('error_title');
            }
        }

        return !$this->error;
    }

    protected function getList() {
        if (isset($this->request->get['filter_filter'])) {
            $filter_filter = $this->request->get['filter_filter'];
        } else {
            $filter_filter = '';
        }

        if (isset($this->request->get['filter_keyword_id'])) {
            $filter_keyword_id = $this->request->get['filter_keyword_id'];
            $data['filter_keyword_id'] = $this->request->get['filter_keyword_id'];
        } else {
            $filter_keyword_id = NULL;
        }


        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'ld' . (int) $this->config->get('config_language_id') . '.title';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_filter'])) {
            $url .= '&filter_filter=' . $this->request->get['filter_filter'];
        }

        if (isset($this->request->get['filter_keyword_id'])) {
            $url .= '&filter_keyword_id=' . $this->request->get['filter_keyword_id'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/keyword', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('mall/keyword/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('mall/keyword/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');


        $this->load->model('mall/keyword');

        $data['items'] = array();
        $filter_data = array(
            'filter_filter' => $filter_filter,
            'filter_keyword_id' => $filter_keyword_id,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $data['items'] = array();

        $keywords_total = $this->model_mall_keyword->getTotalkeywords($filter_data);
        $results = $this->model_mall_keyword->getAll($filter_data);
        foreach ($results as $result) {



            $data['items'][$result['keyword_id']] = array(
                'keyword_id' => $result['keyword_id'],
                'name' => $result['name'],
                'date_added' => $result['date_added'],
                'date_modified' => $result['date_modified'],
                'user_add' => $result['user_add'],
                'user_modify' => $result['user_modify'],
                'edit' => $this->url->link('mall/keyword/edit', 'token=' . $this->session->data['token'] . '&keyword_id=' . $result['keyword_id'] . $url, 'SSL'),
                'delete' => $this->url->link('mall/keyword/delete', 'token=' . $this->session->data['token'] . '&keyword_id=' . $result['keyword_id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_select'] = $this->language->get('text_select');

        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['entry_group'] = $this->language->get('entry_group');

        $data['select_status'] = $this->language->get('select_status');
        $data['select_group'] = $this->language->get('select_group');
        $data['select_country'] = $this->language->get('select_country');

        $data['button_filter'] = $this->language->get('button_filter');

        $data['token'] = $this->session->data['token'];

        $data['column_image'] = $this->language->get('column_image');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_user_id'] = $this->language->get('column_user_id');
        $data['column_last_mod_id'] = $this->language->get('column_last_mod_id');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }
        $pagination = new Pagination();
        $pagination->total = $keywords_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('mall/keyword', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($keywords_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($keywords_total - $this->config->get('config_limit_admin'))) ? $keywords_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $keywords_total, ceil($keywords_total / $this->config->get('config_limit_admin')));
        $data['filter_filter'] = $filter_filter;
        $data['filter_keyword_id'] = $filter_keyword_id;


        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['sort_name'] = $this->url->link('mall/keyword', 'token=' . $this->session->data['token'] . $url . '&sort=ld' . (int) $this->config->get('config_language_id') . '.name', 'SSL');
        $data['sort_date_added'] = $this->url->link('mall/keyword', 'token=' . $this->session->data['token'] . $url . '&sort=l.date_added', 'SSL');
        $data['sort_date_modified'] = $this->url->link('mall/keyword', 'token=' . $this->session->data['token'] . $url . '&sort=l.date_modified', 'SSL');
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('mall/keyword_list.tpl', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');


        $data['tab_general'] = $this->language->get('tab_general');
        $data['text_form'] = !isset($this->request->get['keyword_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');

        //-- Description/Attribute
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_description'] = $this->language->get('entry_description');

        $data['controller_name'] = 'keyword';

        //-- General
        $data['entry_home'] = $this->language->get('entry_home');
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['placeholder'] = $this->language->get('placeholder');

        $data['entry_start_date'] = $this->language->get('entry_start_date');
        $data['entry_end_date '] = $this->language->get('entry_end_date');

        $data['button_remove'] = $this->language->get('button_remove');
        $data['button_image_add'] = $this->language->get('button_image_add');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = ''; //array();
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link("mall/keyword", 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get["keyword_id"])) {
            $data['action'] = $this->url->link("mall/keyword/add", 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link("mall/keyword/edit", 'token=' . $this->session->data['token'] . "&keyword_id=" . $this->request->get["keyword_id"] . $url, 'SSL');
        }
        $data['cancel'] = $this->url->link("mall/keyword", 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get["keyword_id"])) {
            $item_info = $this->model_mall_keyword->getItem($this->request->get["keyword_id"]);
        }

        // echo "<pre>";die(print_r($item_info));

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();


        // echo "<pre>";die(print_r($data['keyword_description']));


        if (isset($this->request->post['keyword_description'])) {
            $data['keyword_description'] = $this->request->post['keyword_description'];
        } elseif (isset($this->request->get["keyword_id"])) {
            $data['keyword_description'] = $this->model_mall_keyword->getItemDescription($this->request->get["keyword_id"]);
        } else {
            $data['keyword_description'] = array();
        }

        $data['data'] = $this->model_mall_keyword;

        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view("mall/keyword_form.tpl", $data));
    }

    public function delete() {
        $this->language->load("mall/keyword");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/keyword");

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $item_id) {
                $this->model_mall_keyword->delete($item_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link("mall/keyword", 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }


    protected function validateDelete() {
        return true;
    }

    public function autocomplete() {

        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model("mall/keyword");

            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array(
                'filter' => array(
                    'name' => $filter_name
                ),
                'start' => 0,
                'limit' => $limit
            );

            $results = $this->model_mall_keyword->getAll($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    "keyword_id" => $result["keyword_id"],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                );
            }
        } else if (isset($this->request->get['query'])) {

            if (isset($this->request->get['query'])) {
                $filter_name = $this->request->get['query'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['type'])) {
                $type = $this->request->get['type'];
            } else {
                $type = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array('filter_name' => $filter_name, 'filter_keyword' => $filter_name);

            if ($type == 'country') {
                $this->load->model('localisation/country');
                $results = $this->model_localisation_country->getAvailableCountries($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['country_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'category') {
                $this->load->model('mall/store_category');
                $results = $this->model_mall_store_category->getStoreCategoryByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_category_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'shop') {
                $this->load->model('mall/shop');
                $results = $this->model_mall_shop->getshopsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')) . ' ( ' . strip_tags(html_entity_decode($result['mall_name'], ENT_QUOTES, 'UTF-8')) . ' ) '
                    );
                }
            } else if ($type == 'group') {
                $this->load->model('mall/keyword');
                $results = $this->model_mall_keyword->getkeywordGroupsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['keyword_group_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
