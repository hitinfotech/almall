<?php

class ControllerMallTip extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('mall/tip');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/tip');
        $this->getList();
    }

    public function add() {
        $this->language->load('mall/tip');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/tip');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm(false)) {
            $tip_id = $this->model_mall_tip->addTip($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($tip_id) && !empty($tip_id)) {
                $url .= '&filter_tip_id=' . $tip_id;
            }
            $this->response->redirect($this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }
        $this->getForm();
    }

    public function edit() {
        $this->language->load('mall/tip');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/tip');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm(true)) {
            $this->model_mall_tip->editTip($this->request->get['tip_id'], $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($this->request->get['tip_id']) && !empty($this->request->get['tip_id'])) {
                $url .= '&filter_tip_id=' . $this->request->get['tip_id'];
            }
            $this->response->redirect($this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    protected function validateForm($exists = true) {
        if (!$this->user->hasPermission('modify', 'mall/tip')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post['tip_description'] as $language_id => $value) {
            if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 255)) {
                $this->error['name'][$language_id] = $this->language->get('error_title');
                $this->error['error_author'] = $this->language->get('error_author');
            }
        }

        return !$this->error;
    }

    protected function getList() {

        if (isset($this->request->get['filter_filter'])) {
            $filter_filter = $this->request->get['filter_filter'];
        } else {
            $filter_filter = NULL;
        }

        if (isset($this->request->get['filter_tip_id'])) {
            $filter_tip_id = $this->request->get['filter_tip_id'];
        } else {
            $filter_tip_id = NULL;
        }

        if (isset($this->request->get['filter_status'])) {
            $filter_status = $this->request->get['filter_status'];
        } else {
            $filter_status = NULL;
        }
        if (isset($this->request->get['filter_author'])) {
            $filter_author = $this->request->get['filter_author'];
        } else {
            $filter_author = NULL;
        }

        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = NULL;
        }

        if (isset($this->request->get['filter_date_added'])) {
            $filter_date_added = $this->request->get['filter_date_added'];
        } else {
            $filter_date_added = NULL;
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $filter_date_modified = $this->request->get['filter_date_modified'];
        } else {
            $filter_date_modified = NULL;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 't.date_added';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_filter'])) {
            $url .= '&filter_filter=' . $this->request->get['filter_filter'];
        }

        if (isset($this->request->get['filter_tip_id'])) {
            $url .= '&filter_tip_id=' . $this->request->get['filter_tip_id'];
        }

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['filter_author'])) {
            $url .= '&filter_author=' . $this->request->get['filter_author'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('mall/tip/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('mall/tip/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->load->model('tool/image');

        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        $data['countries'] = $aDBCountries;

        $filter_data = array(
            'filter_filter' => $filter_filter,
            'filter_tip_id' => $filter_tip_id,
            'filter_status' => $filter_status,
            'filter_author' => $filter_author,
            'filter_country_id' => $filter_country_id,
            'filter_date_added' => $filter_date_added,
            'filter_date_modified' => $filter_date_modified,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $data['items'] = array();

        $tips_total = $this->model_mall_tip->getTotalTip($filter_data);
        $results = $this->model_mall_tip->getTips($filter_data);

        foreach ($results as $result) {

            if (is_file(DIR_IMAGE . $result['image'])) {
                $image = $this->model_tool_image->resize($result['image'], 40, 40, FALSE);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 40, 40, FALSE);
            }

            $data['items'][$result['tip_id']] = array(
                'tip_id' => $result['tip_id'],
                'image' => $image,
                'name' => $result['name'],
                'status' => $result['status'],
                'author' => $result['author'],
                'date_added' => $result['date_added'],
                'date_modified' => $result['date_modified'],
                'sort_order' => $result['sort_order'],
                'user_add' => $result['user_add'],
                'user_modify' => $result['user_modify'],
                'edit' => $this->url->link('mall/tip/edit', 'token=' . $this->session->data['token'] . '&tip_id=' . $result['tip_id'] . $url, 'SSL'),
                'delete' => $this->url->link('mall/tip/delete', 'token=' . $this->session->data['token'] . '&tip_id=' . $result['tip_id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_select'] = $this->language->get('text_select');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['select_shopable'] = $this->language->get('select_shopable');
        $data['text_shopable'] = $this->language->get('text_shopable');
        $data['text_all_products'] = $this->language->get('text_all_products');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['select_country'] = $this->language->get('select_country');
        $data['select_status'] = $this->language->get('select_status');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['column_user_id'] = $this->language->get('column_user_id');
        $data['column_last_mod_id'] = $this->language->get('column_last_mod_id');

        $data['token'] = $this->session->data['token'];

        $data['column_image'] = $this->language->get('column_image');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');
        $data['entry_author'] = $this->language->get('entry_author');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }
        $pagination = new Pagination();
        $pagination->total = $tips_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($tips_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($tips_total - $this->config->get('config_limit_admin'))) ? $tips_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $tips_total, ceil($tips_total / $this->config->get('config_limit_admin')));
        $data['filter_filter'] = $filter_filter;
        $data['filter_tip_id'] = $filter_tip_id;
        $data['filter_country_id'] = $filter_country_id;
        $data['filter_status'] = $filter_status;
        $data['filter_author'] = $filter_author;
        $data['filter_date_added'] = $filter_date_added;
        $data['filter_date_modified'] = $filter_date_modified;

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['sort_name'] = $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url . '&sort=td' . (int) $this->config->get('config_language_id') . '.name', 'SSL');
        $data['sort_status'] = $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url . '&sort=t.status', 'SSL');
        $data['sort_author'] = $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url . '&sort=t.author', 'SSL');
        $data['sort_date_added'] = $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url . '&sort=t.date_added', 'SSL');
        $data['sort_date_modified'] = $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url . '&sort=t.date_modified', 'SSL');
        $data['sort_user_id'] = $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . '&sort=t.sort_user_id' . $url, 'SSL');
        $data['sort_last_mod_id'] = $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . '&sort=t.sort_last_mod_id' . $url, 'SSL');

        $data['sort_order'] = $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url . '&sort=t.sort_order', 'SSL');
        $data['filter_country_id'] = $filter_country_id;
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('mall/tip_list.tpl', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['bActiveLanguageTabFlag'] = true;
        $data['bActiveLanguagePaneFlag'] = true;

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_attribute'] = $this->language->get('tab_attribute');
        $data['tab_links'] = $this->language->get('tab_links');
        $data['tab_image'] = $this->language->get('tab_image');

        $data['text_form'] = !isset($this->request->get['tip_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['select_country'] = $this->language->get('select_country');
        $data['text_home'] = $this->language->get('text_home');
        $data['text_is_home'] = $this->language->get('text_is_home');
        $data['text_not_home'] = $this->language->get('text_not_home');

        //-- Description/Attribute
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_location'] = $this->language->get('entry_location');
        $data['entry_language'] = $this->language->get('entry_language');
        $data['arabic_lang'] = $this->language->get('arabic_lang');
        $data['english_lang'] = $this->language->get('english_lang');


        //-- General
        $data['entry_home'] = $this->language->get('entry_home');
        $data['entry_author'] = $this->language->get('entry_author');
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_mall_id'] = $this->language->get('entry_mall_id');
        $data['entry_main_store_id'] = $this->language->get('entry_main_store_id');
        $data['entry_shop_id'] = $this->language->get('entry_shop_id');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_website'] = $this->language->get('entry_website');
        $data['entry_social_fb'] = $this->language->get('entry_social_fb');
        $data['entry_social_tw'] = $this->language->get('entry_social_tw');
        $data['entry_product'] = $this->language->get('entry_product');

        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_category_id'] = $this->language->get('entry_category_id');

        $data['entry_group_id'] = $this->language->get('entry_group_id');

        $data['entry_related'] = $this->language->get('entry_related');
        $data['entry_related_product'] = $this->language->get('entry_related_product');
        $data['help_related'] = $this->language->get('help_related');
        $data['entry_keywords'] = $this->language->get('entry_keywords');

        $data['entry_start_date'] = $this->language->get('entry_start_date');
        $data['entry_end_date '] = $this->language->get('entry_end_date');

        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_featured'] = $this->language->get('entry_featured');

        $data['button_remove'] = $this->language->get('button_remove');
        $data['button_image_add'] = $this->language->get('button_image_add');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
            $data['error_warning'] .= ' <br> ' . $this->error['name'][$this->config->get('config_language_id')];
        } else {
            $data['error_name'] = '';
        }

        if (isset($this->error['countries'])) {
            $data['error_countries'] = $this->error['countries'];
            $data['error_warning'] .= ' <br> ' . $this->error['countries'];
        } else {
            $data['error_countries'] = '';
        }
        if (isset($this->error['error_image'])) {
            $data['error_image'] = $this->error['error_image'];
            $data['error_warning'] .= ' <br> ' . $this->error['error_image'];
        } else {
            $data['error_image'] = '';
        }
        if (isset($this->error['error_author'])) {
            $data['error_author'] = $this->error['error_author'];
            $data['error_warning'] .= ' <br> ' . $this->error['error_author'];
        } else {
            $data['error_author'] = '';
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['tip_id'])) {
            $data['action'] = $this->url->link('mall/tip/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('mall/tip/edit', 'token=' . $this->session->data['token'] . '&tip_id=' . $this->request->get['tip_id'] . $url, 'SSL');
        }

        $data['cancel'] = $this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['tip_id'])) {
            $tip_info = $this->model_mall_tip->getTip($this->request->get['tip_id']);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();


        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        foreach ($aDBCountries as $value) {
            $data['countries'][$value['country_id']] = $value['name'];
        }


        $this->load->model('localisation/zone');
        $data['country_id'] = 0;
        if (isset($data['city_id']) && $data['city_id'] > 0) {
            $zone_info = $this->model_localisation_zone->getZone($data['city_id']);
            if (!empty($zone_info)) {
                $data['country_id'] = $zone_info['country_id'];
            }
        }

        if (isset($this->request->post['tip_description'])) {
            $data['tip_description'] = $this->request->post['tip_description'];
        } elseif (isset($this->request->get['tip_id'])) {
            $data['tip_description'] = $this->model_mall_tip->getTipDescriptions($this->request->get['tip_id']);
        } else {
            $data['tip_description'] = array();
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($tip_info)) {
            $data['image'] = $tip_info['image'];
        } else {
            $data['image'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($tip_info)) {
            $data['status'] = $tip_info['status'];
        } else {
            $data['status'] = 0;
        }

        if (isset($this->request->post['home'])) {
            $data['home'] = $this->request->post['home'];
        } elseif (!empty($tip_info)) {
            $data['home'] = $tip_info['home'];
        } else {
            $data['home'] = 0;
        }

        if (isset($this->request->post['author'])) {
            $data['author'] = $this->request->post['author'];
        } elseif (!empty($tip_info)) {
            $data['author'] = $tip_info['author'];
        } else {
            $data['author'] = '';
        }


        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($tip_info)) {
            $data['sort_order'] = $tip_info['sort_order'];
        } else {
            $data['sort_order'] = 0;
        }

        if (isset($this->request->post['is_arabic'])) {
            $data['is_arabic'] = $this->request->post['is_arabic'];
        } elseif (!empty($tip_info)) {
            $data['is_arabic'] = $tip_info['is_arabic'];
        } else {
            $data['is_arabic'] = 0;
        }

        if (isset($this->request->post['is_english'])) {
            $data['is_english'] = $this->request->post['is_english'];
        } elseif (!empty($tip_info)) {
            $data['is_english'] = $tip_info['is_english'];
        } else {
            $data['is_english'] = 0;
        }

        if (isset($this->request->post['product_related'])) {
            $products = $this->request->post['product_related'];
        } elseif (isset($this->request->get['tip_id'])) {
            $products = $this->model_mall_tip->getRelatedProduct($this->request->get['tip_id']);
        } else {
            $products = array();
        }

        $data['product_relateds'] = array();

        $this->load->model('catalog/product');
        foreach ($products as $product_id) {

            $related_info = $this->model_catalog_product->getProductDescriptions($product_id);
            $language_id = 2;
            $name = isset($related_info[$language_id]['name']) ? $related_info[$language_id]['name'] : $related_info[1]['name'];
            if ($related_info) {
                $data['product_relateds'][] = array(
                    'product_id' => $product_id,
                    'name' => $name
                );
            }
        }

        if (isset($this->request->post['tip_sections'])) {
            $tip_sections = $this->request->post['tip_sections'];
        } elseif (isset($this->request->get['tip_id'])) {
            $tip_sections = $this->model_mall_tip->getTipSections($this->request->get['tip_id']);
        } else {
            $tip_sections = array();
        }

        $data['tip_sections'] = array();

        foreach ($tip_sections as $key => $rows) {
            foreach ($rows as $row) {
                $this->load->model('tool/image');
                $image = '';
                $thumb = '';
                if (isset($row['image']) && is_file(DIR_IMAGE . $row['image'])) {
                    $image = $row['image'];
                    $thumb = $row['image'];
                } else {
                    $image = '';
                    $thumb = 'placeholder.png';
                }
                $data['tip_sections'][$key][] = array(
                    'title' => isset($row['title']) ? $row['title'] : '',
                    'description' => isset($row['description']) ? $row['description'] : '',
                    'product_id' => isset($row['product_id']) ? $row['product_id'] : 0,
                    'product_name' => isset($row['product_name']) ? $row['product_name'] : '',
                    'image' => $image,
                    'is_english' => $data['is_english'],
                    'is_arabic' => $data['is_arabic'],
                    'thumb' => $this->model_tool_image->resize($thumb, 100, 100, false),
                    'sort_order' => isset($row['sort_order']) ? $row['sort_order'] : 0
                );
            }
        }

        //-- Images
        $this->load->model('tool/image');
        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100, false);
        } elseif (!empty($tip_info) && is_file(DIR_IMAGE . $tip_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($tip_info['image'], 100, 100, false);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        }

        if (isset($this->request->get['tip_id'])) {
            $data['tip_link'] = $this->model_mall_tip->getTipLink($this->request->get['tip_id']);
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('mall/tip_form.tpl', $data));
    }

    public function delete() {
        $this->language->load('mall/tip');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('mall/tip');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $tip_id) {
                $this->model_mall_tip->deleteTip($tip_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('mall/tip', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'mall/tip')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model("mall/tip");

            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array(
                'filter' => array(
                    'name' => $filter_name
                ),
                'start' => 0,
                'limit' => $limit
            );

            $results = $this->model_mall_tip->getTips($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    "tip_id" => $result["tip_id"],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                );
            }
        } else if (isset($this->request->get['query'])) {

            if (isset($this->request->get['query'])) {
                $filter_name = $this->request->get['query'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['type'])) {
                $type = $this->request->get['type'];
            } else {
                $type = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array('filter_name' => $filter_name, 'filter_keyword' => $filter_name);

            if ($type == 'country') {
                $this->load->model('localisation/country');
                $results = $this->model_localisation_country->getAvailableCountries($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['country_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'group') {
                $this->load->model('mall/tip_group');
                $results = $this->model_mall_tip_group->getByName($filter_data);

                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['tip_group_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'category') {
                $this->load->model('mall/store_category');
                $results = $this->model_mall_store_category->getStoreCategoryByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_category_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'shop') {
                $this->load->model('mall/shop');
                $results = $this->model_mall_shop->getshopsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')) . ' ( ' . strip_tags(html_entity_decode($result['mall_name'], ENT_QUOTES, 'UTF-8')) . ' ) '
                    );
                }
            } else if ($type == 'keyword') {
                $this->load->model('mall/keyword');
                $results = $this->model_mall_keyword->getKeywordsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['keyword_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function getSearchValues() {
        $config_limit = $this->config->get('config_limit_admin');

        $columns = array_keys($this->model_mall_tip->getColumns());
        $filter = array_combine($columns, array_pad(array(), count($columns), ''));
        if (isset($this->request->get['filter'])) {
            $filter = array_map(function($value) {
                return ($value === '') ? null : (is_numeric($value) ? intval($value) : $value);
            }, array_merge($filter, (array) $this->request->get['filter']));
        }

        $page = 1;
        if (isset($this->request->get['page'])) {
            $page = intval($this->request->get['page'])? : $page;
        }

        $sort = array('column' => null, 'order' => null);
        if (isset($this->request->get['sort'])) {
            $sort['column'] = $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $sort['order'] = $this->request->get['order'];
        }

        return array(
            'filter' => $filter,
            'sort' => $sort,
            'page' => $page,
            'start' => ($page - 1) * $config_limit,
            'limit' => $config_limit
        );
    }

}
