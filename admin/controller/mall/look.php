<?php

class ControllerMallLook extends Controller {

    private $error = array();

    public function index() {
        $this->language->load("mall/look");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/look");

        $this->getList();
    }

    public function add() {
        $this->language->load("mall/look");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/look");

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $look_id = $this->model_mall_look->add($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($look_id) && !empty($look_id)) {
                $url .= '&filter[id]=' . $look_id;
            }
            $this->response->redirect($this->url->link("mall/look", 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }
        $this->getForm();
    }

    public function edit() {
        $this->language->load("mall/look");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/look");

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_mall_look->edit($this->request->get["look_id"], $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get["look_id"])) {
                $url .= "&look_id=" . $this->request->get["look_id"];
            }
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($this->request->get["look_id"]) && !empty($this->request->get["look_id"])) {
                $url .= '&filter[id]=' . $this->request->get["look_id"];
            }
            $this->response->redirect($this->url->link("mall/look", 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function editPublish() {
        $this->language->load("mall/look");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/look");

        if (($this->request->server['REQUEST_METHOD'] == 'GET')) {
            $this->model_mall_look->Publish($this->request->get["look_id"]);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get["look_id"])) {
                $url .= "&look_id=" . $this->request->get["look_id"];
            }
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link("mall/look", 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->response->redirect($this->url->link("mall/look", 'token=' . $this->session->data['token'], 'SSL'));
    }

    protected function validateForm() {

        if (!$this->user->hasPermission('modify', "mall/look")) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post['look_description'] as $language_id => $value) {
            if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 255)) {
                $this->error['name'][$language_id] = $this->language->get('error_title');
            }
        }

        return !$this->error;
    }

    protected function getList() {
        if (isset($this->request->get['filter_filter'])) {
            $filter_filter = $this->request->get['filter_filter'];
        } else {
            $filter_filter = NULL;
        }

        if (isset($this->request->get['filter_look_id'])) {
            $filter_look_id = $this->request->get['filter_look_id'];
            $data['filter_look_id'] = $this->request->get['filter_look_id'];
        } else {
            $filter_look_id = NULL;
        }

        if (isset($this->request->get['filter_status'])) {
            $filter_status = (int) $this->request->get['filter_status'];
        } else {
            $filter_status = NULL;
        }

        if (isset($this->request->get['filter_group'])) {
            $filter_group = (int) $this->request->get['filter_group'];
        } else {
            $filter_group = NULL;
        }

        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = (int) $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = NULL;
        }

        if (isset($this->request->get['filter_date_added'])) {
            $filter_date_added = $this->request->get['filter_date_added'];
        } else {
            $filter_date_added = NULL;
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $filter_date_modified = $this->request->get['filter_date_modified'];
        } else {
            $filter_date_modified = NULL;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'l.date_added';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_filter'])) {
            $url .= '&filter_filter=' . $this->request->get['filter_filter'];
        }

        if (isset($this->request->get['filter_look_id'])) {
            $url .= '&filter_look_id=' . $this->request->get['filter_look_id'];
        }

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/look', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('mall/look/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('mall/look/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->load->model('tool/image');

        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        $data['countries'] = $aDBCountries;
        $this->load->model('mall/look');
        $aLookGroups = $this->model_mall_look->getLookGroups();
//        echo"<pre>";        print_r($aLookGroups);
        $data['aLookGroups'] = $aLookGroups;

        $data['items'] = array();
        $filter_data = array(
            'filter_filter' => $filter_filter,
            'filter_look_id' => $filter_look_id,
            'filter_group' => $filter_group,
            'filter_status' => $filter_status,
            'filter_country_id' => $filter_country_id,
            'filter_date_added' => $filter_date_added,
            'filter_date_modified' => $filter_date_modified,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $data['items'] = array();

        $looks_total = $this->model_mall_look->getTotalLooks($filter_data);
        $results = $this->model_mall_look->getAll($filter_data);
        foreach ($results as $result) {



            $data['items'][$result['look_id']] = array(
                'look_id' => $result['look_id'],
                'name' => $result['name'],
                'status' => $result['status'],
                'date_added' => $result['date_added'],
                'date_modified' => $result['date_modified'],
                'sort_order' => $result['sort_order'],
                'user_add' => $result['user_add'],
                'user_modify' => $result['user_modify'],
                'edit' => $this->url->link('mall/look/edit', 'token=' . $this->session->data['token'] . '&look_id=' . $result['look_id'] . $url, 'SSL'),
                'edit_status' => $this->url->link("mall/look/editPublish", 'token=' . $this->session->data['token'] . "&look_id=" . $result["look_id"] . "&status=" . !(int) $result['status'] . $url, 'SSL'),
                'edit_status_text' => $this->language->get('button_change_status_' . (int) $result['status']),
                'delete' => $this->url->link('mall/look/delete', 'token=' . $this->session->data['token'] . '&look_id=' . $result['look_id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_select'] = $this->language->get('text_select');

        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['entry_group'] = $this->language->get('entry_group');

        $data['select_status'] = $this->language->get('select_status');
        $data['select_group'] = $this->language->get('select_group');
        $data['select_country'] = $this->language->get('select_country');

        $data['button_filter'] = $this->language->get('button_filter');

        $data['token'] = $this->session->data['token'];

        $data['column_image'] = $this->language->get('column_image');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_user_id'] = $this->language->get('column_user_id');
        $data['column_last_mod_id'] = $this->language->get('column_last_mod_id');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }
        $pagination = new Pagination();
        $pagination->total = $looks_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('mall/look', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($looks_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($looks_total - $this->config->get('config_limit_admin'))) ? $looks_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $looks_total, ceil($looks_total / $this->config->get('config_limit_admin')));
        $data['filter'] = $filter_filter;
        $data['filter_look_id'] = $filter_look_id;
        $data['filter_country_id'] = $filter_country_id;
        $data['filter_group'] = $filter_group;
        $data['filter_status'] = $filter_status;
        $data['filter_date_added'] = $filter_date_added;
        $data['filter_date_modified'] = $filter_date_modified;

        $this->load->model('localisation/stock_status');
        $data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['sort_name'] = $this->url->link('mall/look', 'token=' . $this->session->data['token'] . $url . '&sort=ld' . (int) $this->config->get('config_language_id') . '.name', 'SSL');
        $data['sort_status'] = $this->url->link('mall/look', 'token=' . $this->session->data['token'] . $url . '&sort=l.status', 'SSL');
        $data['sort_date_added'] = $this->url->link('mall/look', 'token=' . $this->session->data['token'] . $url . '&sort=l.date_added', 'SSL');
        $data['sort_date_modified'] = $this->url->link('mall/look', 'token=' . $this->session->data['token'] . $url . '&sort=l.date_modified', 'SSL');
        $data['sort_order'] = $this->url->link('mall/look', 'token=' . $this->session->data['token'] . $url . '&sort=l.sort_order', 'SSL');
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('mall/look_list.tpl', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');
        $data['controller_name'] = $this->name;

        $data['bActiveLanguageTabFlag'] = true;
        $data['bActiveLanguagePaneFlag'] = true;

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_attribute'] = $this->language->get('tab_attribute');
        $data['tab_links'] = $this->language->get('tab_links');
        $data['tab_image'] = $this->language->get('tab_image');
        $data['entry_language'] = $this->language->get('entry_language');
        $data['arabic_lang'] = $this->language->get('arabic_lang');
        $data['english_lang'] = $this->language->get('english_lang');


        $data['text_form'] = !isset($this->request->get['look_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');
        $data['select_group'] = $this->language->get('select_group');

        $data['text_home'] = $this->language->get('text_home');
        $data['text_is_home'] = $this->language->get('text_is_home');
        $data['text_not_home'] = $this->language->get('text_not_home');

        //-- Description/Attribute
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_location'] = $this->language->get('entry_location');

        $data['entry_group'] = $this->language->get('entry_group');

        $data['entry_related'] = $this->language->get('entry_related');
        $data['entry_related_product'] = $this->language->get('entry_related_product');
        $data['help_related'] = $this->language->get('help_related');



        //-- General
        $data['entry_home'] = $this->language->get('entry_home');
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_mall_id'] = $this->language->get('entry_mall_id');
        $data['entry_main_store_id'] = $this->language->get('entry_main_store_id');
        $data['entry_shop_id'] = $this->language->get('entry_shop_id');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_website'] = $this->language->get('entry_website');
        $data['entry_social_fb'] = $this->language->get('entry_social_fb');
        $data['entry_social_tw'] = $this->language->get('entry_social_tw');
        $data['placeholder'] = $this->language->get('placeholder');

        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_category_id'] = $this->language->get('entry_category_id');
        $data['entry_keywords'] = $this->language->get('entry_keywords');

        $data['entry_start_date'] = $this->language->get('entry_start_date');
        $data['entry_end_date '] = $this->language->get('entry_end_date');

        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_featured'] = $this->language->get('entry_featured');

        $data['entry_meta_title'] = $this->language->get('entry_meta_title');
        $data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $data['entry_meta_keywords'] = $this->language->get('entry_meta_keywords');

        $data['button_remove'] = $this->language->get('button_remove');
        $data['button_image_add'] = $this->language->get('button_image_add');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = ''; //array();
        }
        if (isset($this->error['countries'])) {
            $data['error_countries'] = $this->error['countries'];
        } else {
            $data['error_countries'] = ''; //array();
        }

        $url = '';
        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link("mall/look", 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get["look_id"])) {
            $data['action'] = $this->url->link("mall/look/add", 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link("mall/look/edit", 'token=' . $this->session->data['token'] . "&look_id=" . $this->request->get["look_id"] . $url, 'SSL');
        }

        $data['cancel'] = $this->url->link("mall/look", 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get["look_id"])) {
            $item_info = $this->model_mall_look->getItem($this->request->get["look_id"]);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();

        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        foreach ($aDBCountries as $key => $value) {
            $data['countries'][$value['country_id']] = $value['name'];
        }


        $this->load->model('localisation/zone');
        $data['country_id'] = 0;
        if (isset($data['city_id']) && $data['city_id'] > 0) {
            $zone_info = $this->model_localisation_zone->getZone($data['city_id']);
            if (!empty($zone_info)) {
                $data['country_id'] = $zone_info['country_id'];
            }
        }

        // Images
        $this->load->model('tool/image');

        $data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 100, 100, FALSE);

        if (isset($this->request->post['look_image'])) {
            $look_images = $this->request->post['look_image'];
        } elseif (isset($this->request->get['look_id'])) {
            $look_images = $this->model_mall_look->getLookImages($this->request->get['look_id']);
        } else {
            $look_images = array();
        }

        $data['look_images'] = array();

        foreach ($look_images as $look_image) {
            if (is_file(DIR_IMAGE . $look_image['image'])) {
                $image = $look_image['image'];
                $thumb = $look_image['image'];
            } else {
                $image = '';
                $thumb = 'placeholder.png';
            }

            $data['look_images'][] = array(
                'image' => $image,
                'thumb' => $this->model_tool_image->resize($thumb, 100, 100, FALSE),
                'sort_order' => $look_image['sort_order']
            );
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($data['image'])) {
            $data['image'] = $image;
        } else {
            $data['image'] = '';
        }


        if (isset($this->request->post['look_description'])) {
            $data['look_description'] = $this->request->post['look_description'];
        } elseif (isset($this->request->get["look_id"])) {
            $data['look_description'] = $this->model_mall_look->getItemDescription($this->request->get["look_id"]);
        } else {
            $data['look_description'] = array();
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($item_info)) {
            $data['status'] = $item_info['status'];
        } else {
            $data['status'] = 0;
        }

        if (isset($this->request->post['home'])) {
            $data['home'] = $this->request->post['home'];
        } elseif (!empty($item_info)) {
            $data['home'] = $item_info['home'];
        } else {
            $data['home'] = 0;
        }
        if (isset($this->request->post['is_arabic'])) {
            $data['is_arabic'] = $this->request->post['is_arabic'];
        } elseif (!empty($item_info)) {
            $data['is_arabic'] = $item_info['is_arabic'];
        } else {
            $data['is_arabic'] = 0;
        }

        if (isset($this->request->post['is_english'])) {
            $data['is_english'] = $this->request->post['is_english'];
        } elseif (!empty($item_info)) {
            $data['is_english'] = $item_info['is_english'];
        } else {
            $data['is_english'] = 0;
        }

        /* if (isset($this->request->post['group_id'])) {
          $data['group_id'] = $this->request->post['group_id'];
          } elseif (!empty($item_info)) {
          $data['group_id'] = $item_info['group_id'];
          } else {
          $data['group_id'] = "-1";
          }

          if (isset($this->request->post['author'])) {
          $data['author'] = $this->request->post['author'];
          } elseif (!empty($item_info)) {
          $data['author'] = $item_info['author'];
          } else {
          $data['author'] = '';
          } */

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($item_info)) {
            $data['sort_order'] = $item_info['sort_order'];
        } else {
            $data['sort_order'] = '';
        }

        if (isset($this->request->post['meta_title'])) {
            $data['meta_title'] = $this->request->post['meta_title'];
        } elseif (!empty($item_info)) {
            $data['meta_title'] = $item_info['meta_title'];
        } else {
            $data['meta_title'] = '';
        }

        if (isset($this->request->post['meta_description'])) {
            $data['meta_description'] = $this->request->post['meta_description'];
        } elseif (!empty($item_info)) {
            $data['meta_description'] = $item_info['meta_description'];
        } else {
            $data['meta_description'] = '';
        }

        if (isset($this->request->post['meta_keywords'])) {
            $data['meta_keywords'] = $this->request->post['meta_keywords'];
        } elseif (!empty($item_info)) {
            $data['meta_keywords'] = $item_info['meta_keywords'];
        } else {
            $data['meta_keywords'] = '';
        }
        if (isset($this->request->post['look_id'])) {
            $products = $this->request->post['look_id'];
        } elseif (isset($this->request->get['look_id'])) {
            $products = $this->model_mall_look->getProductRelated($this->request->get['look_id']);
        } else {
            $products = array();
        }

        $data['product_relateds'] = array();

        $this->load->model('catalog/product');
        foreach ($products as $product) {

            $related_info = $this->model_catalog_product->getProductDescriptions($product['product_id']);

            if (!empty($related_info)) {

                $name = '';

                foreach ($related_info as $key => $row) {
                    $name .= $row['name'] . ' | ';
                }

                $data['product_relateds'][] = array(
                    'product_id' => $product['product_id'],
                    'name' => rtrim($name, ' | '),
                    'sort_order' => $product['sort_order']
                );
            }
        }


        $data['item_link'] = array();
        if (isset($this->request->get["look_id"])) {
            $filter_item_id = $this->request->get["look_id"];
            $item_link_results = $this->model_mall_look->getItemLinks($filter_item_id);
            foreach ($item_link_results as $link_type => $dataSet) {
                foreach ($dataSet as $result) {
                    $data['item_link'][$link_type][] = array(
                        'id' => $result['id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        }

        $data['data'] = $this->model_mall_look;

        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view("mall/look_form.tpl", $data));
    }

    public function delete() {
        $this->language->load("mall/look");
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model("mall/look");

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $item_id) {
                $this->model_mall_tag->deleteTag($item_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link("mall/look", 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    public function getZone() {
        $json = array();

        $this->load->model('mall/mall');
        $results = $this->model_mall_mall->getZones($this->request->get['country_id']);

        foreach ($results as $result) {
            $json[] = array(
                'zone_id' => $result['zone_id'],
                'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        // echo "<pre>"; die(print_r($json));
    }

    protected function validateDelete() {
        return true;
    }

    public function autocomplete() {

        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model("mall/look");

            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array(
                'filter' => array(
                    'name' => $filter_name
                ),
                'start' => 0,
                'limit' => $limit
            );

            $results = $this->model_mall_look->getAll($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    "look_id" => $result["look_id"],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
                );
            }
        } else if (isset($this->request->get['query'])) {

            if (isset($this->request->get['query'])) {
                $filter_name = $this->request->get['query'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['type'])) {
                $type = $this->request->get['type'];
            } else {
                $type = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array('filter_name' => $filter_name, 'filter_keyword' => $filter_name);

            if ($type == 'country') {
                $this->load->model('localisation/country');
                $results = $this->model_localisation_country->getAvailableCountries($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['country_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'category') {
                $this->load->model('mall/store_category');
                $results = $this->model_mall_store_category->getStoreCategoryByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_category_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'shop') {
                $this->load->model('mall/shop');
                $results = $this->model_mall_shop->getshopsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['store_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')) . ' ( ' . strip_tags(html_entity_decode($result['mall_name'], ENT_QUOTES, 'UTF-8')) . ' ) '
                    );
                }
            } else if ($type == 'group') {
                $this->load->model('mall/look');
                $results = $this->model_mall_look->getLookGroupsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['look_group_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            } else if ($type == 'keyword') {
                $this->load->model('mall/keyword');
                $results = $this->model_mall_keyword->getKeywordsByName($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['keyword_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
