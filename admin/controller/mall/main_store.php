<?php

class ControllerMallMainStore extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('mall/main_store');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('mall/main_store');

        $this->getList();
    }

    public function add() {
        $this->language->load('mall/main_store');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('mall/main_store');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_mall_main_store->addMainStore($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('mall/main_store', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function edit() {
        $this->language->load('mall/main_store');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('mall/main_store');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_mall_main_store->editMainStore($this->request->get['main_store_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('mall/main_store', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->language->load('mall/main_store');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('mall/main_store');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $main_store_id) {
                $this->model_mall_main_store->deleteMainStore($main_store_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('mall/main_store', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {


        if (isset($this->request->get['filter_keyword'])) {
            $filter_keyword = $this->request->get['filter_keyword'];
        } else {
            $filter_keyword = NULL;
        }
        if (isset($this->request->get['filter_category'])) {
            $filter_category = $this->request->get['filter_category'];
        } else {
            $filter_category = NULL;
        }
        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = NULL;
        }
        if (isset($this->request->get['filter_city_id'])) {
            $filter_city_id = $this->request->get['filter_city_id'];
        } else {
            $filter_city_id = NULL;
        }
        if (isset($this->request->get['filter_mall_id'])) {
            $filter_mall_id = $this->request->get['filter_mall_id'];
        } else {
            $filter_mall_id = NULL;
        }


        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }


        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/main_store', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('mall/main_store/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('mall/main_store/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['sort_name'] = $this->url->link('mall/main_store', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
        $data['sort_sort_order'] = $this->url->link('mall/main_store', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, 'SSL');

        $data['stores'] = array();


        $this->load->model('localisation/country');
        $data['countries'] = $this->model_localisation_country->getAvailableCountries();

        $this->load->model('catalog/category');
        $categoryResult = $this->model_catalog_category->getCategories();
        //echo "<pre>";die(print_r($results));
        $data['categories'] = array();
        foreach ($categoryResult as $row) {
            $data['categories'][] = array(
                'category_id' => $row['category_id'],
                'name' => $row['name']
            );
        }


        $filter_data = array(
            'sort' => $sort,
            'filter_keyword' => $filter_keyword,
            'filter_category' => $filter_category,
            'filter_country_id' => $filter_country_id,
            'filter_city_id' => $filter_city_id,
            'filter_mall_id' => $filter_mall_id,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $main_store_total = $this->model_mall_main_store->getTotalMainStores($filter_data);

        // die(print_r($filter_data));
        $results = $this->model_mall_main_store->getMainStores($filter_data);

        foreach ($results as $result) {
            $name = '';
            $sep = '';
            $names = $this->model_mall_main_store->getMainStoreDescriptions($result['main_store_id']);
            foreach ($names as $row) {
                $name .= $sep . $row['name'];
                $sep = ' | ';
            }

            $data['stores'][] = array(
                'main_store_id' => $result['main_store_id'],
                'name' => $name, //$result['name'],
                'date_modified' => $result['date_modified'],
                'date_added' => $result['date_added'],
                'sort_order' => $result['sort_order'],
                'products_count' => $result['products_count'],
                'user_add' => $result['user_add'],
                'user_modify' => $result['user_modify'],
                'edit' => $this->url->link('mall/main_store/edit', 'token=' . $this->session->data['token'] . '&main_store_id=' . $result['main_store_id'] . $url, 'SSL'),
                'delete' => $this->url->link('mall/main_store/delete', 'token=' . $this->session->data['token'] . '&main_store_id=' . $result['main_store_id'] . $url, 'SSL')
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_city'] = $this->language->get('entry_city');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['entry_category'] = $this->language->get('entry_category');
        $data['entry_mall'] = $this->language->get('entry_mall');
        $data['text_select'] = $this->language->get('text_select');
        $data['column_user_id'] = $this->language->get('column_user_id');
        $data['column_last_mod_id'] = $this->language->get('column_last_mod_id');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_select_all'] = $this->language->get('text_select_all');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_products_count'] = $this->language->get('column_products_count');
        $data['column_creat_date'] = $this->language->get('column_creat_date');
        $data['column_update_date'] = $this->language->get('column_update_date');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $data['filter_keyword'] = $filter_keyword;
        $data['filter_category'] = $filter_category;
        $data['filter_country_id'] = $filter_country_id;
        $data['filter_city_id'] = $filter_city_id;
        $data['filter_mall_id'] = $filter_mall_id;

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('mall/main_store', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
        $data['sort_sort_order'] = $this->url->link('mall/main_store', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, 'SSL');

        $url = '';

        if (isset($this->request->get['filter_keyword'])) {
            $url .= '&filter_keyword=' . $this->request->get['filter_keyword'];
        }

        if (isset($this->request->get['filter_category'])) {
            $url .= '&filter_category=' . $this->request->get['filter_category'];
        }

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_city_id'])) {
            $url .= '&filter_city_id=' . $this->request->get['filter_city_id'];
        }

        if (isset($this->request->get['filter_mall_id'])) {
            $url .= '&filter_mall_id=' . $this->request->get['filter_mall_id'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $main_store_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('mall/main_store', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($main_store_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($main_store_total - $this->config->get('config_limit_admin'))) ? $main_store_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $main_store_total, ceil($main_store_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $data['token'] = $this->session->data['token'];

        $this->response->setOutput($this->load->view('mall/main_store_list.tpl', $data));
    }

    protected function getForm() {

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['main_store_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_location'] = $this->language->get('entry_location');
        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_website'] = $this->language->get('entry_website');
        $data['entry_social_fb'] = $this->language->get('entry_social_fb');
        $data['entry_social_tw'] = $this->language->get('entry_social_tw');
        $data['entry_social_jeeran'] = $this->language->get('entry_social_jeeran');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_latitude'] = $this->language->get('entry_latitude');
        $data['entry_longitude'] = $this->language->get('entry_longitude');
        $data['entry_zoom'] = $this->language->get('entry_zoom');
        $data['entry_add_brand_items'] = $this->language->get('entry_add_brand_items');
        $data['entry_brand_name'] = $this->language->get('entry_brand_name');
        $data['entry_add_category_items'] = $this->language->get('entry_add_category_items');
        $data['entry_category_name'] = $this->language->get('entry_category_name');
        $data['entry_brand'] = $this->language->get('entry_brand');
        $data['entry_enable_category'] = $this->language->get('entry_enable_category');
        $data['entry_enable_brand'] = $this->language->get('entry_enable_brand');


        $data['entry_city'] = $this->language->get('entry_city');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['entry_phone'] = $this->language->get('entry_phone');

        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_main_group'] = $this->language->get('entry_main_group');
        $data['entry_sub_stores'] = $this->language->get('entry_sub_stores');
        $data['entry_category'] = $this->language->get('entry_category');
        $data['entry_store_category'] = $this->language->get('entry_store_category');
        $data['text_select'] = $this->language->get('text_select');

        $data['help_filter'] = $this->language->get('help_filter');
        $data['help_keyword'] = $this->language->get('help_keyword');
        $data['help_top'] = $this->language->get('help_top');
        $data['help_column'] = $this->language->get('help_column');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_data'] = $this->language->get('tab_data');
        $data['tab_design'] = $this->language->get('tab_design');

        $array_of_errors = array('warning', 'name', 'location', 'google', 'main_group_id', 'country_id', 'city_id');

        foreach ($array_of_errors as $item) {
            if (isset($this->error[$item])) {
                $data['error_' . $item] = $this->error[$item];
            } else {
                $data['error_' . $item] = '';
            }
        }
        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('mall/main_store', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['main_store_id'])) {
            $data['action'] = $this->url->link('mall/main_store/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('mall/main_store/edit', 'token=' . $this->session->data['token'] . '&main_store_id=' . $this->request->get['main_store_id'] . $url, 'SSL');
        }

        $data['cancel'] = $this->url->link('mall/main_store', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['main_store_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $store_info = $this->model_mall_main_store->getMainStore($this->request->get['main_store_id']);
            $google = $this->model_mall_main_store->getMap($this->request->get['main_store_id']);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $this->load->model('mall/brand');
        $this->load->model('catalog/category');


        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['main_store_description'])) {
            $data['main_store_description'] = $this->request->post['main_store_description'];
        } elseif (isset($this->request->get['main_store_id'])) {
            $data['main_store_description'] = $this->model_mall_main_store->getMainStoreDescriptions($this->request->get['main_store_id']);
        } else {
            $data['main_store_description'] = array();
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($store_info)) {
            $data['image'] = $store_info['image'];
        } else {
            $data['image'] = '';
        }

        if (isset($this->request->post['brand_id'])) {
            $data['brand_id'] = $this->request->post['brand_id'];
        } elseif (!empty($store_info)) {
            $aBrands = explode(',',$store_info['related_brand']);
            if (!empty($aBrands)) {
                foreach ($aBrands as $brand_id) {
                    if (trim($brand_id) != '') {
                        $aNames = $this->db->query("SELECT name FROM brand_description WHERE brand_id = $brand_id;");
                        $aNames = $aNames->rows;
                        $sName = $aNames[0]['name'] . ' | ' . $aNames[1]['name'];
                        $data['brands'][] = array(
                            'brand_id' => $brand_id,
                            'name' => strip_tags(html_entity_decode($sName, ENT_QUOTES, 'UTF-8'))
                        );
                    }
                }
            }
        } else {
            $data['brands'] = '';
        }

        if (isset($this->request->post['show_related_brand'])) {
            $data['show_related_brand'] = $this->request->post['show_related_brand'];
        } elseif (!empty($store_info)) {
            $data['show_related_brand'] = $store_info['show_related_brand'];
        } else {
            $data['show_related_brand'] = 0;
        }

        if (isset($this->request->post['brand'])) {
            $data['brand'] = $this->request->post['brand'];
        } elseif (!empty($store_info)) {
            $brand_info = $this->model_mall_brand->getBrand($store_info['related_brand']);

            if ($brand_info) {
                $data['brand'] = $brand_info['name'];
            } else {
                $data['brand'] = '';
            }
        } else {
            $data['brand'] = '';
        }

        if (isset($this->request->post['related_category'])) {
            $data['related_category'] = $this->request->post['related_category'];
        } elseif (!empty($store_info)) {
            if (trim($store_info['related_category']) != '') {
                $filter_data = array(
                    'category_ids' => $store_info['related_category'],
                    'sort' => 'name',
                    'order' => 'ASC',
                );
                $this->config->set('config_language_id', 2);
                $results = $this->model_catalog_category->getCategories($filter_data);
                foreach ($results as $result) {
                    $data['related_categories'][] = array(
                        'category_id' => $result['category_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        } else {
            $data['related_category'] = '';
        }

        if (isset($this->request->post['show_related_category'])) {
            $data['show_related_category'] = $this->request->post['show_related_category'];
        } elseif (!empty($store_info)) {
            $data['show_related_category'] = $store_info['show_related_category'];
        } else {
            $data['show_related_category'] = 0;
        }

        if (isset($this->request->post['category'])) {
            $data['category'] = $this->request->post['category'];
        } elseif (!empty($store_info)) {
            $category_info = $this->model_catalog_category->getCategory($store_info['related_category']);

            if ($category_info) {
                $data['category'] = ($category_info['path']) ? $category_info['path'] . ' &gt; ' . $category_info['name'] : $category_info['name'];
            } else {
                $data['category'] = '';
            }
        } else {
            $data['category'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($store_info) && is_file(DIR_IMAGE . $store_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($store_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('placeholder.png', 100, 100);
        }

        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        foreach ($aDBCountries as $key => $value) {
            $data['countries'][$value['country_id']] = $value['name'];
        }

        if (isset($this->request->post['city_id'])) {
            $data['city_id'] = $this->request->post['city_id'];
        } elseif (!empty($store_info)) {
            $data['city_id'] = $store_info['city_id'];
        } else {
            $data['city_id'] = 0;
        }

        if (isset($this->request->post['phone'])) {
            $data['phone'] = $this->request->post['phone'];
        } elseif (!empty($store_info)) {
            $data['phone'] = $store_info['phone'];
        } else {
            $data['phone'] = 0;
        }

        $this->load->model('localisation/zone');
        $data['country_id'] = 0;
        if (isset($data['city_id']) && $data['city_id'] > 0) {
            $zone_info = $this->model_localisation_zone->getZone($data['city_id']);
            if (!empty($zone_info)) {
                $data['country_id'] = $zone_info['country_id'];
            }
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($store_info) && is_file(DIR_IMAGE . $store_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($store_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('placeholder.png', 100, 100);
        }

        $data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 100, 100);

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($store_info)) {
            $data['sort_order'] = $store_info['sort_order'];
        } else {
            $data['sort_order'] = 0;
        }

        if (isset($this->request->post['latitude'])) {
            $data['latitude'] = $this->request->post['latitude'];
        } elseif (!empty($google)) {
            $data['latitude'] = $google['latitude'];
        } else {
            $data['latitude'] = '';
        }

        if (isset($this->request->post['longitude'])) {
            $data['longitude'] = $this->request->post['longitude'];
        } elseif (!empty($google)) {
            $data['longitude'] = $google['longitude'];
        } else {
            $data['longitude'] = '';
        }

        if (isset($this->request->post['zoom'])) {
            $data['zoom'] = $this->request->post['zoom'];
        } elseif (!empty($google)) {
            $data['zoom'] = $google['zoom'];
        } else {
            $data['zoom'] = 0;
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($store_info)) {
            $data['status'] = $store_info['status'];
        } else {
            $data['status'] = 0;
        }

        if (isset($this->request->post['website'])) {
            $data['website'] = $this->request->post['website'];
        } elseif (!empty($store_info)) {
            $data['website'] = $store_info['website'];
        } else {
            $data['website'] = '';
        }

        if (isset($this->request->post['social_fb'])) {
            $data['social_fb'] = $this->request->post['social_fb'];
        } elseif (!empty($store_info)) {
            $data['social_fb'] = $store_info['social_fb'];
        } else {
            $data['social_fb'] = '';
        }

        if (isset($this->request->post['social_tw'])) {
            $data['social_tw'] = $this->request->post['social_tw'];
        } elseif (!empty($store_info)) {
            $data['social_tw'] = $store_info['social_tw'];
        } else {
            $data['social_tw'] = '';
        }

        if (isset($this->request->post['social_jeeran'])) {
            $data['social_jeeran'] = ''; //$this->request->post['social_jeeran'];
        } elseif (!empty($store_info)) {
            $data['social_jeeran'] = $store_info['social_jeeran'];
        } else {
            $data['social_jeeran'] = '';
        }

        if (isset($this->request->post['main_group_id'])) {
            $data['main_group_id'] = $this->request->post['main_group_id'];
        } elseif (!empty($store_info)) {
            $data['main_group_id'] = $store_info['main_group_id'];
        } else {
            $data['main_group_id'] = 0;
        }

        if (isset($this->request->post['category_id'])) {
            $data['category_id'] = $this->request->post['category_id'];
        } elseif (!empty($store_info)) {
            $data['category_id'] = $store_info['category_id'];
        } else {
            $data['category_id'] = 0;
        }

        $this->load->model('mall/main_group');
        $groups = $this->model_mall_main_group->getMainGroups();
        //echo "<pre>";die(print_r($results));
        $data['main_groups'] = array();
        foreach ($groups as $row) {
            $data['main_groups'][] = array(
                'main_group_id' => $row['main_group_id'],
                'name' => $row['name']
            );
        }

        $this->load->model('catalog/category');
        $categoryResult = $this->model_catalog_category->getCategories();
        //echo "<pre>";die(print_r($results));
        $data['categories'] = array();
        foreach ($categoryResult as $row) {
            $data['categories'][] = array(
                'category_id' => $row['category_id'],
                'name' => $row['name']
            );
        }

        // $this->load->model('setting/store');
        $this->load->model('mall/shop');
        $data['sub_stores'] = array();
        if (isset($this->request->get['main_store_id'])) {
            $filter_main_store_id = (int) $this->request->get['main_store_id'];
            // $results = $this->model_setting_store->getStoresByMainStore($filter_main_store_id);
            $results = $this->model_mall_shop->getStoresByMainStore($filter_main_store_id);
            foreach ($results as $result) {
                $data['sub_stores'][] = array(
                    'store_id' => $result['store_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')) . ' ( ' . strip_tags(html_entity_decode($result['mall_name'], ENT_QUOTES, 'UTF-8')) . ' ) '
                );
            }
        }

        //store_category
        $this->load->model('mall/store_category');
        $data['store_category'] = array();
        if (isset($this->request->get['main_store_id'])) {
            $filter_main_store_id = (int) $this->request->get['main_store_id'];
            $results = $this->model_mall_store_category->getStoreCategoriesByMainStore($filter_main_store_id);
            foreach ($results as $result) {
                $data['store_category'][] = array(
                    'store_category_id' => $result['store_category_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('mall/main_store_form.tpl', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'mall/main_store')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        /*if (!isset($this->request->post['main_group_id']) || trim($this->request->post['main_group_id']) == "") {
            $this->error['main_group_id'] = $this->language->get('error_main_group_id');
        }*/

        foreach ($this->request->post['main_store_description'] as $language_id => $value) {
            if ((utf8_strlen($value['name']) < 2) || (utf8_strlen($value['name']) > 255)) {
                $this->error['name'][$language_id] = $this->language->get('error_name');
            }

            if ((utf8_strlen($value['location']) < 3) || (utf8_strlen($value['location']) > 255)) {
                $this->error['location'][$language_id] = $this->language->get('error_location');
            }
        }

        if ((float) $this->request->post['latitude'] <= 0 || (float) $this->request->post['latitude'] > 10000.000000) {
            $this->error['google'] = $this->language->get('error_google');
        }
        if ((float) $this->request->post['longitude'] <= 0 || (float) $this->request->post['longitude'] > 10000.000000) {
            $this->error['google'] = $this->language->get('error_google');
        }
        if (!isset($this->request->post['country_id']) || trim($this->request->post['country_id']) == "") {
            $this->error['country_id'] = $this->language->get('error_country_id');
        }
        if (!isset($this->request->post['city_id']) || empty($this->request->post['city_id'])) {
            $this->error['city_id'] = $this->language->get('error_city_id');
        }
        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'mall/main_store')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('mall/shop');

            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'sort' => 'mgd1.name',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 20
            );

            // $results = $this->model_setting_store->getStoresByName($filter_data);
            $results = $this->model_mall_shop->getshopsByName($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    'store_id' => $result['store_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')) . ' ( ' . strip_tags(html_entity_decode($result['mall_name'], ENT_QUOTES, 'UTF-8')) . ' ) '
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getMainStores() {
        $json = array();

        $filter_data = array(
            'filter_country_id' => (isset($this->request->get['filter_country_id'])) ? $this->request->get['filter_country_id'] : '',
            'filter_city_id' => (isset($this->request->get['filter_city_id'])) ? $this->request->get['filter_city_id'] : '',
            'sort'=>'mgd1.name'
        );

        $this->load->model('mall/main_store');
        $results = $this->model_mall_main_store->getMainStores($filter_data);

        foreach ($results as $result) {
            $json[] = array(
                'main_store_id' => $result['main_store_id'],
                'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
            );
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
        // echo "<pre>"; die(print_r($json));
    }
}
