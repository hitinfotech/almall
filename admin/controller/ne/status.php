<?php
//-----------------------------------------------------
// Newsletter Enhancements for Opencart
// Created by @DmitryNek (Dmitry Shkoliar)
// exmail.Nek@gmail.com
//-----------------------------------------------------

/**
 * Class ControllerNeStatus
 * @property ModelNeCampaign $model_ne_campaign
 */
class ControllerNeStatus extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('ne/status');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('ne/campaign');


        $data['delete'] = $this->url->link('ne/status/delete', 'token=' . $this->session->data['token'], 'SSL');
        $data['pause'] = $this->url->link('ne/status/pause', 'token=' . $this->session->data['token'], 'SSL');
        $data['continue'] = $this->url->link('ne/status/resume', 'token=' . $this->session->data['token'], 'SSL');



        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_no_results'] = $this->language->get('text_no_results');

//        $data['column_subject'] = $this->language->get('column_subject');
//        $data['column_date'] = $this->language->get('column_date');
//        $data['column_to'] = $this->language->get('column_to');
        $data['column_actions'] = $this->language->get('column_actions');

        $data['text_status_newsletters'] = $this->language->get('text_status_newsletters');
        $data['head_ready_to_share_id'] = $this->language->get('ready_to_share_id');
        $data['head_is_done'] = $this->language->get('is_done');
        $data['head_chunks'] = $this->language->get('chunks');
        $data['head_total'] = $this->language->get('total');
        $data['head_fail'] = $this->language->get('fail');
        $data['head_success'] = $this->language->get('success');
        $data['head_subject'] = $this->language->get('subject');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_pause'] = $this->language->get('button_pause');
        $data['is_done_0'] = $this->language->get('is_done_0');
        $data['is_done_1'] = $this->language->get('is_done_1');


        $data['token'] = $this->session->data['token'];
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = $this->config->get('ne_warning');
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->session->data['error_warning'])) {
            $data['error_warning'] = $this->session->data['error_warning'];
            unset($this->session->data['error_warning']);
        } else {
            $data['error_warning'] = '';
        }

//        echo $this->model_ne_campaign->count();die();
        $pagination = new Pagination();
        $pagination->total = $this->model_ne_campaign->count();
        $pagination->page = (int)isset($this->request->get['page']) ? $this->request->get['page'] : 1;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('ne/status', 'token=' . $this->session->data['token'] .'&page={page}', 'SSL');
        $data['pagination'] = $pagination->render();
        $data['results'] = sprintf($this->language->get('text_pagination'), ($pagination->total) ? (($pagination->page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($pagination->page - 1) * $this->config->get('config_limit_admin')) > ($pagination->total - $this->config->get('config_limit_admin'))) ? $pagination->total : ((($pagination->page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $pagination->total, ceil($pagination->total / $this->config->get('config_limit_admin')));


        $start =  ($pagination->page - 1) * $this->config->get('config_limit_admin');
        $limt = $this->config->get('config_limit_admin');
        $list = $this->model_ne_campaign->getList($start,$limt);
        $data['list'] = $list;


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('ne/status.tpl', $data));
    }

    public function delete()
    {
        $this->load->model('ne/campaign');
        $this->model_ne_campaign->removeById($this->request->get['id']);
        $this->load->language('ne/status');
        $this->session->data['success'] = $this->language->get('text_success');
        $this->response->redirect($this->url->link('ne/status', 'token=' . $this->session->data['token'], 'SSL'));
    }

    public function pause()
    {
        $this->load->model('ne/campaign');
        $this->load->language('ne/status');
        $info = $this->model_ne_campaign->getById($this->request->get['id']);
        if ($info && $info['status'] != 'COMPLETED' && $info['status'] != 'FETCHINGMAILS') {
            $this->model_ne_campaign->pause($this->request->get['id']);
            $this->session->data['success'] = $this->language->get('text_pause_success');
        }else{
            $this->session->data['error_warning'] = $this->language->get('text_pause_fail');

        }
        $this->response->redirect($this->url->link('ne/status', 'token=' . $this->session->data['token'], 'SSL'));
    }

    public function resume()
    {
        $this->load->model('ne/campaign');
        $this->model_ne_campaign->resume($this->request->get['id']);
        $this->load->language('ne/status');
        $this->session->data['success'] = $this->language->get('text_continue_success');
        $this->response->redirect($this->url->link('ne/status', 'token=' . $this->session->data['token'], 'SSL'));

    }

    private function validate()
    {
        if (!$this->user->hasPermission('modify', 'ne/status')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}