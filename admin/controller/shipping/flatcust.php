<?php

class ControllerShippingFlatcust extends Controller {

    private $error = array();

    public function index() {
        
        $this->load->language('shipping/flatcust');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('flatcust', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_all_zones'] = $this->language->get('text_all_zones');
        $data['text_none'] = $this->language->get('text_none');
        $data['entry_cost'] = $this->language->get('entry_cost');
        $data['entry_cost_mix'] = $this->language->get('entry_cost_mix');
        $data['entry_zero_cost'] = $this->language->get('entry_zero_cost');
        $data['entry_zero_cost_mix'] = $this->language->get('entry_zero_cost_mix');
        $data['between_0_100'] = $this->language->get('between_0_100');
        $data['between_101_200'] = $this->language->get('between_101_200');
        $data['between_201_300'] = $this->language->get('between_201_300');
        $data['entry_tax_class'] = $this->language->get('entry_tax_class');
        $data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_shipping'),
            'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('shipping/flat', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['action'] = $this->url->link('shipping/flatcust', 'token=' . $this->session->data['token'], 'SSL');

        $data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['flatcust_cost'])) {
            $data['flatcust_cost'] = $this->request->post['flatcust_cost'];
        } else {
            $data['flatcust_cost'] = $this->config->get('flatcust_cost');
        }
        
        if (isset($this->request->post['flatcust_cost_mix'])) {
            $data['flatcust_cost_mix'] = $this->request->post['flatcust_cost_mix'];
        } else {
            $data['flatcust_cost_mix'] = $this->config->get('flatcust_cost_mix');
        }

        if (isset($this->request->post['flatcust_zero_cost'])) {
            $data['flatcust_zero_cost'] = $this->request->post['flatcust_zero_cost'];
        } else {
            $data['flatcust_zero_cost'] = $this->config->get('flatcust_zero_cost');
        }
        
        if (isset($this->request->post['flatcust_zero_cost_mix'])) {
            $data['flatcust_zero_cost_mix'] = $this->request->post['flatcust_zero_cost_mix'];
        } else {
            $data['flatcust_zero_cost_mix'] = $this->config->get('flatcust_zero_cost_mix');
        }

        if (isset($this->request->post['flatcust_between_0_100'])) {
            $data['flatcust_between_0_100'] = $this->request->post['flatcust_between_0_100'];
        } else {
            $data['flatcust_between_0_100'] = $this->config->get('flatcust_between_0_100');
        }

        if (isset($this->request->post['flatcust_between_101_200'])) {
            $data['flatcust_between_101_200'] = $this->request->post['flatcust_between_101_200'];
        } else {
            $data['flatcust_between_101_200'] = $this->config->get('flatcust_between_101_200');
        }

        if (isset($this->request->post['flatcust_between_201_300'])) {
            $data['flatcust_between_201_300'] = $this->request->post['flatcust_between_201_300'];
        } else {
            $data['flatcust_between_201_300'] = $this->config->get('flatcust_between_201_300');
        }

        if (isset($this->request->post['flatcust_tax_class_id'])) {
            $data['flatcust_tax_class_id'] = $this->request->post['flatcust_tax_class_id'];
        } else {
            $data['flatcust_tax_class_id'] = $this->config->get('flatcust_tax_class_id');
        }

        $this->load->model('localisation/tax_class');

        $data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

        if (isset($this->request->post['flatcust_geo_zone_id'])) {
            $data['flatcust_geo_zone_id'] = $this->request->post['flatcust_geo_zone_id'];
        } else {
            $data['flatcust_geo_zone_id'] = $this->config->get('flatcust_geo_zone_id');
        }

        $this->load->model('localisation/geo_zone');

        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        if (isset($this->request->post['flatcust_status'])) {
            $data['flatcust_status'] = $this->request->post['flatcust_status'];
        } else {
            $data['flatcust_status'] = $this->config->get('flatcust_status');
        }

        if (isset($this->request->post['flatcust_sort_order'])) {
            $data['flatcust_sort_order'] = $this->request->post['flatcust_sort_order'];
        } else {
            $data['flatcust_sort_order'] = $this->config->get('flatcust_sort_order');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('shipping/flatcust.tpl', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'shipping/flatcust')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

}
