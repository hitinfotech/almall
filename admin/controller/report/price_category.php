<?php

class ControllerReportPriceCategory extends Controller {


    public function __construct($registry)
    {

        parent::__construct($registry);
        $this->load->model('report/product');

    }

    public function index() {

        $this->getAvgPrice();

    }


    private function getAvgPrice(){


        $avg_price = $this->model_report_product->getAvgPrice();

        /** Set default timezone (will throw a notice otherwise) */
        date_default_timezone_set('Asia/Riyadh');


        // create new PHPExcel object
        $objPHPExcel = new PHPExcel;

        // set default font
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');

        // set default font size
        $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);

        // create the writer
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

        /**

         * Define currency and number format.

         */

        // SHEET 1

        // writer already created the first sheet for us, let's get it
        $objSheet = $objPHPExcel->createSheet(0);

        // rename the sheet
        $objSheet->setTitle('Price Category');



        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->getStyle('A1:C1')->getFont()->setBold(true)->setSize(12);


        // write header

        $objSheet->getCell('A1')->setValue('Count');
        $objSheet->getCell('B1')->setValue('AvgPrice');
        $objSheet->getCell('C1')->setValue('Name');

        // we could get this data from database, but here we are writing for simplicity

        $i= 2;
        foreach ($avg_price as $row){


            $objSheet->getCell("A$i")->setValue(" $row[count]");
            $objSheet->getCell("B$i")->setValue("  $row[avgPrice]");
            $objSheet->getCell("C$i")->setValue("  $row[name]");
            $i++ ;
        }


        // autosize the columns
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);



        // SHEET 2


        $parent_avg_price = $this->model_report_product->getParentAvgPrice();

        // writer already created the first sheet for us, let's get it
        $objSheet = $objPHPExcel->createSheet(1);

        // rename the sheet
        $objSheet->setTitle('Parent Price Sheet');


        // let's bold and size the header font and write the header
        // as you can see, we can specify a range of cells, like here: cells from A1 to A4
        $objSheet->getStyle('A1:D1')->getFont()->setBold(true)->setSize(12);



        // write header

        $objSheet->getCell('A1')->setValue('Count');
        $objSheet->getCell('B1')->setValue('AvgPrice');
        $objSheet->getCell('C1')->setValue('Name');

        // we could get this data from database, but here we are writing for simplicity

        $i= 2;
        foreach ($parent_avg_price as $row){


            $objSheet->getCell("A$i")->setValue(" $row[count]");
            $objSheet->getCell("B$i")->setValue("  $row[avgPrice]");
            $objSheet->getCell("C$i")->setValue("  $row[name]");
            $i++ ;
        }



        // autosize the columns
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);



        //Setting the header type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="priceCategory.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');

    }

}