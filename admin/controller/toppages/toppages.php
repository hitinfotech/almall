<?php

class ControllerToppagesToppages extends Controller {

    private $error = array();

    public function index() {
        $this->language->load('toppages/toppages');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('toppages/toppages');
        $this->getList();
    }

    public function add() {
        $this->language->load('toppages/toppages');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('toppages/toppages');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $top_pages_id = $this->model_toppages_toppages->addToppages($this->request->post);
            $this->session->data['success'] = $this->language->get('text_success_add');
            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($top_pages_id) && !empty($top_pages_id)) {
                $url .= '&filter_top_pages_id=' . $top_pages_id;
            }
            $this->response->redirect($this->url->link('toppages/toppages', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function edit() {
        $this->language->load('toppages/toppages');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('toppages/toppages');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_toppages_toppages->editToppages($this->request->get['top_pages_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success_add');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            if (isset($this->request->get['top_pages_id']) && !empty($this->request->get['top_pages_id'])) {
                $url .= '&filter_top_pages_id=' . $this->request->get['top_pages_id'];
            }
            $this->response->redirect($this->url->link('toppages/toppages', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->language->load('toppages/toppages');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('toppages/toppages');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $top_pages_id) {
                $this->model_toppages_toppages->deletetoppages($top_pages_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_edit');

            $url = '';
            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }
            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }
            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('toppages/toppages', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    protected function getList() {
        // var_dump($this->request->get['filter_filter']);die;
        if (isset($this->request->get['filter_filter'])) {
            $filter_filter = $this->request->get['filter_filter'];
        } else {
            $filter_filter = NULL;
        }

        if (isset($this->request->get['filter_status'])) {
            $filter_status = $this->request->get['filter_status'];
        } else {
            $filter_status = NULL;
        }
        if (isset($this->request->get['filter_group'])) {
            $filter_group = (int) $this->request->get['filter_group'];
        } else {
            $filter_group = NULL;
        }
        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = NULL;
        }


        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = '';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }
        if (isset($this->request->get['filter_filter'])) {
            $url .= '&filter_filter=' . $this->request->get['filter_filter'];
        }


        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }


        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('toppages/toppages', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        $data['add'] = $this->url->link('toppages/toppages/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('toppages/toppages/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['refresh'] = $this->url->link('toppages/toppages/refresh', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $this->load->model('mall/look');

        $atoppagesGroups = $this->model_toppages_toppages->getToppagessGroups();
        $data['atoppagesGroups'] = $atoppagesGroups;

        //-- Lables
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['select_country'] = $this->language->get('select_country');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');
        $data['column_image'] = $this->language->get('column_image');
        $data['column_desc'] = $this->language->get('column_desc');
        $data['column_title'] = $this->language->get('column_title');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_country'] = $this->language->get('column_country');

        $data['button_filter'] = $this->language->get('button_filter');

        $data['token'] = $this->session->data['token'];
        $data['action'] = $this->url->link('toppages/toppages/', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries();
        $data['countries'] = $aDBCountries;

        $data['toppages'] = array();

        $filter_data = array(
            'filter_filter' => $filter_filter,
            'filter_country_id' => $filter_country_id,
            'filter_status' => $filter_status,
            'filter_group' => $filter_group,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );
        $toppages_total = $this->model_toppages_toppages->getTotalToppages($filter_data);
        $results = $this->model_toppages_toppages->getToppagesList($filter_data);

        $aCountries = array('0' => '');
        $this->load->model('localisation/country');
        $aDBCountries = $this->model_localisation_country->getAvailableCountries(); //$this->model_localisation_country->getCountries(array('where'=>array('available'=>'1')));
        foreach ($aDBCountries as $key => $value) {
            $aCountries[$value['country_id']] = $value['name'];
        }

        $this->load->model('tool/image');

        foreach ($results as $result) {

            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config_image->get('toppages', 'admin_listing', 'width'), $this->config_image->get('toppages', 'admin_listing', 'hieght'), false);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('toppages', 'admin_listing', 'width'), $this->config_image->get('toppages', 'admin_listing', 'hieght'), false);
            }

            $data['toppages'][] = array(
                'sort_toppages' => $result['top_pages_id'],
                'day' => $result['day'],
                'image' => $image,
                'body' => strip_tags(html_entity_decode($result['body'], ENT_QUOTES, 'UTF-8')),
                'name' => $result['name'],
                'status' => $result['status'],
                'date_added' => $result['date_added'],
                'country_id' => (isset($aCountries[$result['country_id']])) ? $aCountries[$result['country_id']] : '',
                'date_modified' => $result['date_modified'],
                'user_add' => $result['user_add'],
                'user_modify' => $result['user_modify'],
                'edit' => $this->url->link('toppages/toppages/edit', 'token=' . $this->session->data['token'] . '&top_pages_id=' . $result['top_pages_id'] . $url, 'SSL'),
                'delete' => $this->url->link('toppages/toppages/delete', 'token=' . $this->session->data['token'] . '&top_pages_id=' . $result['top_pages_id'] . $url, 'SSL')
            );
        }
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['column_user_id'] = $this->language->get('column_user_id');
        $data['column_day'] = $this->language->get('column_day');
        $data['column_country_id'] = $this->language->get('column_country_id');
        $data['column_last_mod_id'] = $this->language->get('column_last_mod_id');
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['select_country'] = $this->language->get('select_country');
        $data['select_status'] = $this->language->get('select_status');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['column_user_id'] = $this->language->get('column_user_id');
        $data['column_last_mod_id'] = $this->language->get('column_last_mod_id');
        $data['column_title'] = $this->language->get('column_title');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_featured'] = $this->language->get('column_featured');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');
        $data['select_group'] = $this->language->get('select_group');
        $data['entry_group'] = $this->language->get('entry_group');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        $pagination = new Pagination();
        $pagination->total = $toppages_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('toppages/toppages', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($toppages_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($toppages_total - $this->config->get('config_limit_admin'))) ? $toppages_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $toppages_total, ceil($toppages_total / $this->config->get('config_limit_admin')));

        $data['filter_filter'] = $filter_filter;
        $data['filter_country_id'] = $filter_country_id;
        $data['filter_status'] = $filter_status;

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';



        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }
        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['sort_toppages'] = $this->url->link('toppages/toppages', 'token=' . $this->session->data['token'] . $url . '&sort=sort_toppages', 'SSL');
        $data['sort_date_added'] = $this->url->link('toppages/toppages', 'token=' . $this->session->data['token'] . $url . '&sort=date_added', 'SSL');
        $data['sort_date_modified'] = $this->url->link('toppages/toppages', 'token=' . $this->session->data['token'] . $url . '&sort=date_modified', 'SSL');
        $data['filter_group'] = $filter_group;

        $data['sort_order'] = $this->url->link('toppages/toppages', 'token=' . $this->session->data['token'] . $url . '&sort=sort_order', 'SSL');
        $data['filter_country_id'] = $filter_country_id;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('toppages/toppages_list.tpl', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['bActiveLanguageTabFlag'] = true;
        $data['bActiveLanguagePaneFlag'] = true;
        $data['column_image'] = $this->language->get('column_image');

        $data['user_id'] = $this->session->data['user_id'];
        $data['text_form'] = !isset($this->request->get['_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select'] = $this->language->get('text_select');
        //-- Description/Attribute
        $data['entry_title'] = $this->language->get('entry_title');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_body_text'] = $this->language->get('entry_body_text');
        $data['entry_period'] = $this->language->get('entry_period');
        $data['entry_product_id'] = $this->language->get('entry_product_id');
        $data['select_group'] = $this->language->get('select_group');

        //-- General
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_date'] = $this->language->get('entry_date');
        $data['column_action'] = $this->language->get('column_action');
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_type'] = $this->language->get('entry_type');
        $data['entry_title'] = $this->language->get('entry_title');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_group'] = $this->language->get('entry_group');
        $data['entry_keywords'] = $this->language->get('entry_keywords');
        $data['entry_language'] = $this->language->get('entry_language');
        $data['arabic_lang'] = $this->language->get('arabic_lang');
        $data['english_lang'] = $this->language->get('english_lang');

        //-- Links
        $data['entry_country_id'] = $this->language->get('entry_country_id');
        $data['entry_desc'] = $this->language->get('entry_desc');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('toppages/toppages', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['top_pages_id'])) {
            $data['action'] = $this->url->link('toppages/toppages/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('toppages/toppages/edit', 'token=' . $this->session->data['token'] . '&top_pages_id=' . $this->request->get['top_pages_id'] . $url, 'SSL');
        }
        $data['cancel'] = $this->url->link('toppages/toppages', 'token=' . $this->session->data['token'] . $url, 'SSL');
        
        if (isset($this->request->post['is_arabic'])) {
            $data['is_arabic'] = $this->request->post['is_arabic'];
        } else {
            $data['is_arabic'] = 0;
        }
         if (isset($this->request->post['is_english'])) {
            $data['is_english'] = $this->request->post['is_english'];
        } else {
            $data['is_english'] = 0;
        }
       
        if (isset($this->request->get['top_pages_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $result = $this->model_toppages_toppages->getToppages($this->request->get['top_pages_id']);
            $this->load->model('tool/image');
           if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config_image->get('toppages', 'admin_listing', 'width'), $this->config_image->get('toppages', 'admin_listing', 'hieght'), false);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config_image->get('toppages', 'admin_listing', 'width'), $this->config_image->get('toppages', 'admin_listing', 'hieght'), false);
            }

            $data['toppages'][] = array(
                'sort_toppages' => $result['top_pages_id'],
                'day' => $result['day'],
                'image' => $image,
                'is_english' => $result['is_english'],
                'is_arabic' => $result['is_arabic'],
                'body' => $result['body'],
                'name' => $result['name'],
                'status' => $result['status'],
                'date_added' => $result['date_added']
            );
        }
        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['toppages_description'])) {
            $data['toppages_description'] = $this->request->post['toppages_description'];
        } elseif (!empty($result)) {
            $data['toppages_description'] = $this->model_toppages_toppages->getToppagesDescriptions($result['top_pages_id']);
            $data['toppages_details'] = $this->model_toppages_toppages->getToppagesDetails($result['top_pages_id']);
        } else {
            $data['toppages_description'] = array();
            $data['toppages_details'] = array();
        }
        if (isset($this->request->get['homepage_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {

            $data['homepage_id'] = $this->request->get['homepage_id'];


            $_aFilter = array(
                'where' => array(
                    'homepage_id' => $this->request->get['homepage_id']
                )
            );
            $slider_info = $this->model_cms_homepageTopProducts->getHomepageTopProducts($_aFilter)[0];
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();

        $this->load->model('localisation/country');

        if (isset($this->request->post['link_type'])) {
            $data['link_type'] = $this->request->post['link_type'];
        } elseif (!empty($slider_info)) {
            $data['link_type'] = $slider_info['link_type'];
        } else {
            $data['link_type'] = '';
        }

        if (isset($this->request->post['link_id'])) {
            $data['link_id'] = $this->request->post['link_id'];
        } elseif (!empty($slider_info)) {
            $data['link_id'] = $slider_info['link_id'];
        } else {
            $data['link_id'] = 0;
        }

        if (!isset($this->request->post['user_id'])) {
            $data['user_id'] = $this->session->data['user_id'];
        } elseif (!empty($slider_info)) {
            $data['user_id'] = $slider_info['user_id'];
        } else {
            $data['user_id'] = ($this->request->post['user_id'] != $this->session->data['user_id']) ? $this->session->data['user_id'] : $this->request->post['user_id'];
        }


        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($slider_info)) {
            $data['image'] = $slider_info['image'];
        } else {
            $data['image'] = '';
        }

        //-- Images

        $this->load->model('tool/image');
        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($item_info) && is_file(DIR_IMAGE . $slider_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($slider_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('placeholder.png', 100, 100);
        }

        if (isset($this->request->post['desc'])) {
            $data['desc'] = $this->request->post['desc'];
        } elseif (!empty($slider_info)) {
            $data['desc'] = $slider_info['desc'];
        } else {
            $data['desc'] = '';
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($slider_info)) {
            $data['sort_order'] = $slider_info['sort_order'];
        } else {
            $data['sort_order'] = 0;
        }

        $slider_info_name = array();
        if (isset($slider_info['link_type']) && !empty($slider_info['link_type'])) {
            $_aFilterTitle = array(
                'homepage_id' => $slider_info['homepage_id'],
                'link_type' => $slider_info['link_type']
            );
            $slider_info_name = $this->model_cms_homepageTopProducts->getHomepageTopProductsTitle($_aFilterTitle)[0];
        }

        $data['link_id_name'] = "";
        if (isset($slider_info_name) && !empty($slider_info_name)) {
            $data['link_id_name'] = $slider_info_name['name'];
        }

        $data['top_pages_link'] = array();
        if (isset($this->request->get['top_pages_id'])) {
            $filter_top_pages_id = $this->request->get['top_pages_id'];
            $top_pages_link_results = $this->model_toppages_toppages->getToppagesLink($filter_top_pages_id);
            foreach ($top_pages_link_results as $link_type => $dataSet) {
                foreach ($dataSet as $result) {
                    $data['top_pages_link'][$link_type][] = array(
                        'id' => $result['id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
            }
        }

        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('toppages/toppages_form.tpl', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'toppages/toppages')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'toppages/toppages')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])) {
            $this->load->model('catalog/product');
            $this->load->model('catalog/option');

            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['filter_model'])) {
                $filter_model = $this->request->get['filter_model'];
            } else {
                $filter_model = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array(
                'filter_name' => $filter_name,
                'filter_model' => $filter_model,
                'start' => 0,
                'limit' => $limit
            );

            $results = $this->model_catalog_product->getProducts($filter_data);

            foreach ($results as $result) {
                $option_data = array();
                $result['price'] = 0;
                $product_options = $this->model_catalog_product->getProductOptions($result['product_id']);

                foreach ($product_options as $product_option) {
                    $option_info = $this->model_catalog_option->getOption($product_option['option_id']);

                    if ($option_info) {
                        $product_option_value_data = array();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $option_value_info = $this->model_catalog_option->getOptionValue($product_option_value['option_value_id']);

                            if ($option_value_info) {
                                $product_option_value_data[] = array(
                                    'product_option_value_id' => $product_option_value['product_option_value_id'],
                                    'option_value_id' => $product_option_value['option_value_id'],
                                    'name' => $option_value_info['name'],
                                    'price' => (float) $product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
                                    'price_prefix' => $product_option_value['price_prefix']
                                );
                            }
                        }

                        $option_data[] = array(
                            'product_option_id' => $product_option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id' => $product_option['option_id'],
                            'name' => $option_info['name'],
                            'type' => $option_info['type'],
                            'value' => $product_option['value'],
                            'required' => $product_option['required']
                        );
                    }
                }

                $status = $result['status'] == 1 ? 'ENABEL' : 'DISABLED';
                $json[] = array(
                    'product_id' => $result['product_id'],
                    'name' => strip_tags(html_entity_decode('( ' . $status . ' - ' . $result['product_id'] . ')' . ' - ' . $result['name'], ENT_QUOTES, 'UTF-8')),
                    'model' => $result['model'],
                    'option' => $option_data,
                    'price' => $result['price']
                );
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
