  <?php

class ControllerSaleShippingProcess extends Controller {

    private $error = array();

    public function index()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/shipping_process');

        $this->info();
    }


    public function add()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/shipping_process');

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/shipping_process');

        $this->getForm();
    }

    public function info()
    {
        $this->load->model('sale/shipping_process');

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->commonfunctions->getOrderNumber($this->request->get['order_id']);
        } else {
            $order_id = 0;
        }

        $order_info = $this->model_sale_shipping_process->getOrder($order_id);

        if ($order_info) {

            $has_access = false;
            if(!empty($order_info['shipping_companies'])){
              foreach($order_info['shipping_companies'] as $key => $value){
                if($value['shipping_company'] == $this->user->getShippingCompany())
                  $has_access = true;
              }
              if($this->user->getShippingCompany() == 0)
                $has_access = true;
            }else
              if($this->user->getShippingCompany() == 0)
                $has_access = true;

            if(! $has_access){
              $this->response->redirect($this->url->link('common/blank', '', 'SSL'));
            }

            $this->load->language('sale/order');

            $this->document->setTitle('Bar Code System');

            $data['heading_title'] = 'Bar Code System';

            $data['text_ip_add'] = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
            $data['text_order_detail'] = $this->language->get('text_order_detail');
            $data['text_customer_detail'] = $this->language->get('text_customer_detail');
            $data['text_option'] = $this->language->get('text_option');
            $data['text_store'] = $this->language->get('text_store');
            $data['text_date_added'] = $this->language->get('text_date_added');
            $data['text_payment_method'] = $this->language->get('text_payment_method');
            $data['text_shipping_method'] = $this->language->get('text_shipping_method');
            $data['text_customer'] = $this->language->get('text_customer');
            $data['text_customer_group'] = $this->language->get('text_customer_group');
            $data['text_email'] = $this->language->get('text_email');
            $data['text_telephone'] = $this->language->get('text_telephone');
            $data['text_invoice'] = $this->language->get('text_invoice');
            $data['text_reward'] = $this->language->get('text_reward');
            $data['text_affiliate'] = $this->language->get('text_affiliate');
            $data['text_order'] = sprintf($this->language->get('text_order'), $this->request->get['order_id']);
            $data['text_payment_address'] = $this->language->get('text_payment_address');
            $data['text_shipping_address'] = $this->language->get('text_shipping_address');
            $data['text_comment'] = $this->language->get('text_comment');

            $data['text_account_custom_field'] = $this->language->get('text_account_custom_field');
            $data['text_payment_custom_field'] = $this->language->get('text_payment_custom_field');
            $data['text_shipping_custom_field'] = $this->language->get('text_shipping_custom_field');
            $data['text_browser'] = $this->language->get('text_browser');
            $data['text_ip'] = $this->language->get('text_ip');
            $data['text_forwarded_ip'] = $this->language->get('text_forwarded_ip');
            $data['text_user_agent'] = $this->language->get('text_user_agent');
            $data['text_accept_language'] = $this->language->get('text_accept_language');
            $data['text_history'] = $this->language->get('text_history');
            $data['text_history_add'] = $this->language->get('text_history_add');
            $data['text_loading'] = $this->language->get('text_loading');

            $data['column_product'] = $this->language->get('column_product');
            $data['column_model'] = $this->language->get('column_model');
            $data['column_quantity'] = $this->language->get('column_quantity');
            $data['column_price'] = $this->language->get('column_price');
            $data['column_total'] = $this->language->get('column_total');

            $data['entry_order_status'] = $this->language->get('entry_order_status');
            $data['entry_notify'] = $this->language->get('entry_notify');
            $data['entry_override'] = $this->language->get('entry_override');
            $data['entry_comment'] = $this->language->get('entry_comment');

            $data['help_override'] = $this->language->get('help_override');

            $data['button_invoice_print'] = $this->language->get('button_invoice_print');
            $data['button_shipping_print'] = $this->language->get('button_shipping_print');
            $data['button_edit'] = $this->language->get('button_edit');
            $data['button_cancel'] = $this->language->get('button_cancel');
            $data['button_generate'] = $this->language->get('button_generate');
            $data['button_reward_add'] = $this->language->get('button_reward_add');
            $data['button_reward_remove'] = $this->language->get('button_reward_remove');
            $data['button_commission_add'] = $this->language->get('button_commission_add');
            $data['button_commission_remove'] = $this->language->get('button_commission_remove');
            $data['button_history_add'] = $this->language->get('button_history_add');
            $data['button_ip_add'] = $this->language->get('button_ip_add');

            $data['tab_history'] = $this->language->get('tab_history');
            $data['tab_additional'] = $this->language->get('tab_additional');
            $data['tab_firstflight'] = $this->language->get('tab_firstflight');
            $data['entry_order_id'] = $this->language->get('entry_order_id');
            $data['filter_order_id'] = $this->language->get('filter_order_id');
            $data['button_filter'] = $this->language->get('button_filter');


            $data['token'] = $this->session->data['token'];

            $url = '';

            if (isset($this->request->get['filter_order_id'])) {
                $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
            }

            if (isset($this->request->get['filter_customer'])) {
                $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_order_status'])) {
                $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
            }

            if (isset($this->request->get['filter_total'])) {
                $url .= '&filter_total=' . $this->request->get['filter_total'];
            }

            if (isset($this->request->get['filter_date_added'])) {
                $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
            }

            if (isset($this->request->get['filter_date_modified'])) {
                $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('sale/shipping_process', 'token=' . $this->session->data['token'] . $url, 'SSL')
            );

            $data['shipping'] = $this->url->link('sale/shipping_process/shipping', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], 'SSL');
            $data['invoice'] = $this->url->link('sale/shipping_process/order_confirm', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], 'SSL');
            $data['edit'] = $this->url->link('sale/shipping_process/edit', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], 'SSL');
            $data['cancel'] = $this->url->link('sale/shipping_process', 'token=' . $this->session->data['token'] . $url, 'SSL');

            $data['order_id'] =isset($this->request->get['order_id']) ? $this->request->get['order_id'] :$order_id;

            $data['store_name'] = $order_info['store_name'];
            $data['store_url'] = $order_info['store_url'];

            if ($order_info['invoice_no']) {
                $data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
            } else {
                $data['invoice_no'] = '';
            }

            $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

            $data['firstname'] = $order_info['firstname'];
            $data['lastname'] = $order_info['lastname'];

            if ($order_info['customer_id']) {
                $data['customer'] = $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $order_info['customer_id'], 'SSL');
            } else {
                $data['customer'] = '';
            }

            $this->load->model('customer/customer_group');

            $customer_group_info = $this->model_customer_customer_group->getCustomerGroup($order_info['customer_group_id']);

            if ($customer_group_info) {
                $data['customer_group'] = $customer_group_info['name'];
            } else {
                $data['customer_group'] = '';
            }

            $data['email'] = $order_info['email'];
            $data['telephone'] = $order_info['telephone'];

            $data['shipping_method'] = $order_info['shipping_method'];
            $data['payment_method'] = $order_info['payment_method'];

            // Payment Address
            if ($order_info['payment_address_format']) {
                $format = $order_info['payment_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['payment_firstname'],
                'lastname' => $order_info['payment_lastname'],
                'company' => $order_info['payment_company'],
                'address_1' => $order_info['payment_address_1'],
                'address_2' => $order_info['payment_address_2'],
                'city' => $order_info['payment_city'],
                'postcode' => $order_info['payment_postcode'],
                'zone' => $order_info['payment_zone'],
                'zone_code' => $order_info['payment_zone_code'],
                'country' => $order_info['payment_country']
            );

            $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            // Shipping Address
            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['shipping_address_2'],
                'city' => $order_info['shipping_city'],
                'postcode' => $order_info['shipping_postcode'],
                'zone' => $order_info['shipping_zone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country']
            );

            $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            // Uploaded files
            $this->load->model('tool/upload');
            $this->load->model('sale/shipping_process');
            $products = $this->model_sale_shipping_process->getOrderProductsforShipment($order_id);

            foreach ($products as $product) {
                $option_data = array();

                $options = $this->model_sale_shipping_process->getOrderOptions($order_id, $product['order_product_id']);

                foreach ($options as $option) {
                    if ($option['type'] != 'file') {
                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $option['value'],
                            'type' => $option['type']
                        );
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $upload_info['name'],
                                'type' => $option['type'],
                                'href' => $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], 'SSL')
                            );
                        }
                    }
                }

                $seller_details = $this->db->query("SELECT c2c.customer_id,CONCAT(c2c.firstname,' ',c2c.lastname) name,c2c.email FROM " . DB_PREFIX . "customerpartner_to_order c2o LEFT JOIN customerpartner_to_product c2p ON (c2o.product_id = c2p.product_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer c2c ON (c2o.customer_id = c2c.customer_id) WHERE c2o.product_id = '" . (int)$product['product_id'] . "'  AND c2o.order_id='" . (int)$this->commonfunctions->getOrderNumber($data['order_id']) . "'")->row;

                if ($seller_details && isset($seller_details['name']) && $seller_details['name']) {
                    $product['name'] = $product['name'];
                    $product['seller'] = '<a href="' . $this->url->link('customerpartner/partner', 'token=' . $this->session->data['token'] . '&view_all=1&filter_email=' . $seller_details['email'], 'SSL') . '"><b>' . $seller_details['name'] . '</b></a>';
                }
                $data['products'][$product['customer_id']][] = array(
                    'order_product_id' => $product['order_product_id'],
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'seller' => $product['seller'],
                    'model' => $product['model'],
                    'option' => $option_data,
                    'quantity' => $product['quantity'],
                    'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'href' => $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $product['product_id'], 'SSL'),
                    'product_invoice' => $this->url->link('sale/shipping_process/show_product', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'] . '&seller_id=' . $product['customer_id'], 'SSL')
                );
            }

            $data['vouchers'] = array();

            $vouchers = $this->model_sale_shipping_process->getOrderVouchers($order_id);

            foreach ($vouchers as $voucher) {
                $data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
                    'href' => $this->url->link('sale/voucher/edit', 'token=' . $this->session->data['token'] . '&voucher_id=' . $voucher['voucher_id'], 'SSL')
                );
            }

            $data['totals'] = array();

            $totals = $this->model_sale_shipping_process->getOrderTotals($order_id);

            foreach ($totals as $total) {
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                );
            }

            $data['comment'] = nl2br($order_info['comment']);

            $this->load->model('customer/customer');

            $data['reward'] = $order_info['reward'];

            $data['reward_total'] = $this->model_customer_customer->getTotalCustomerRewardsByOrderId($order_id);

            $data['affiliate_firstname'] = $order_info['affiliate_firstname'];
            $data['affiliate_lastname'] = $order_info['affiliate_lastname'];

            if ($order_info['affiliate_id']) {
                $data['affiliate'] = $this->url->link('marketing/affiliate/edit', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $order_info['affiliate_id'], 'SSL');
            } else {
                $data['affiliate'] = '';
            }

            $data['commission'] = $this->currency->format($order_info['commission'], $order_info['currency_code'], $order_info['currency_value']);

            $this->load->model('marketing/affiliate');

            $data['commission_total'] = $this->model_marketing_affiliate->getTotalTransactionsByOrderId($order_id);

            $this->load->model('localisation/order_status');

            $order_status_info = $this->model_localisation_order_status->getOrderStatus($order_info['order_status_id']);

            if ($order_status_info) {
                $data['order_status'] = $order_status_info['name'];
            } else {
                $data['order_status'] = '';
            }

            $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

            $data['order_status_id'] = $order_info['order_status_id'];

            $data['account_custom_field'] = $order_info['custom_field'];

            // Custom Fields
            $this->load->model('customer/custom_field');

            $data['account_custom_fields'] = array();

            $filter_data = array(
                'sort' => 'cf.sort_order',
                'order' => 'ASC',
            );

            $custom_fields = $this->model_customer_custom_field->getCustomFields($filter_data);

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'account' && isset($order_info['custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['account_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['account_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['account_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['custom_field'][$custom_field['custom_field_id']]
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['account_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name']
                            );
                        }
                    }
                }
            }

            // Custom fields
            $data['payment_custom_fields'] = array();

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'address' && isset($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['payment_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['payment_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['payment_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name'],
                                    'sort_order' => $custom_field['sort_order']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['payment_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['payment_custom_field'][$custom_field['custom_field_id']],
                            'sort_order' => $custom_field['sort_order']
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['payment_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }
                }
            }

            // Shipping
            $data['shipping_custom_fields'] = array();

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'address' && isset($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['shipping_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['shipping_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['shipping_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name'],
                                    'sort_order' => $custom_field['sort_order']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['shipping_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['shipping_custom_field'][$custom_field['custom_field_id']],
                            'sort_order' => $custom_field['sort_order']
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['shipping_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }
                }
            }

            //FirstFlight
            $AWBNo = $this->model_sale_shipping_process->getAWBNo($order_id);

            $data['ip'] = $order_info['ip'];
            $data['forwarded_ip'] = $order_info['forwarded_ip'];
            $data['user_agent'] = $order_info['user_agent'];
            $data['accept_language'] = $order_info['accept_language'];

            // Additional Tabs
            $data['tabs'] = array();

            $this->load->model('extension/extension');

            $content = $this->load->controller('payment/' . $order_info['payment_code'] . '/order');

            if ($content) {
                $this->load->language('payment/' . $order_info['payment_code']);

                $data['tabs'][] = array(
                    'code' => $order_info['payment_code'],
                    'title' => $this->language->get('heading_title'),
                    'content' => $content
                );
            }

            $extensions = $this->model_extension_extension->getInstalled('fraud');

            foreach ($extensions as $extension) {
                if ($this->config->get($extension . '_status')) {
                    $this->load->language('fraud/' . $extension);

                    $content = $this->load->controller('fraud/' . $extension . '/order');

                    if ($content) {
                        $data['tabs'][] = array(
                            'code' => $extension,
                            'title' => $this->language->get('heading_title'),
                            'content' => $content
                        );
                    }
                }
            }

            // API login
            $this->load->model('user/api');

            $api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

            if ($api_info) {
                $data['api_id'] = $api_info['api_id'];
                $data['api_key'] = $api_info['key'];
                $data['api_ip'] = $this->request->server['REMOTE_ADDR'];
            } else {
                $data['api_id'] = '';
                $data['api_key'] = '';
                $data['api_ip'] = '';
            }

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('sale/shipping_process_info.tpl', $data));
        } else {
            $this->load->language('sale/order');

            $this->document->setTitle('Bar Code System');

            $data['heading_title'] = 'Bar Code System';

            $data['text_not_found'] = $this->language->get('text_not_found');
            $data['entry_order_id'] = $this->language->get('entry_order_id');
            $data['filter_order_id'] = $this->language->get('filter_order_id');
            $data['button_filter'] = $this->language->get('button_filter');
            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('sale/shipping_process', 'token=' . $this->session->data['token'], 'SSL')
            );
            $data['token'] = $this->session->data['token'];
            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');
            $data['order_id'] = '';
            $this->response->setOutput($this->load->view('sale/shipping_process_info.tpl', $data));
        }
    }

    public function createInvoiceNo()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } elseif (isset($this->request->get['order_id'])) {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $invoice_no = $this->model_sale_order->createInvoiceNo($order_id);

            if ($invoice_no) {
                $json['invoice_no'] = $invoice_no;
            } else {
                $json['error'] = $this->language->get('error_action');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addReward()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info && $order_info['customer_id'] && ($order_info['reward'] > 0)) {
                $this->load->model('customer/customer');

                $reward_total = $this->model_customer_customer->getTotalCustomerRewardsByOrderId($order_id);

                if (!$reward_total) {
                    $this->model_customer_customer->addReward($order_info['customer_id'], $this->language->get('text_order_id') . ' #' . $order_id, $order_info['reward'], $order_id);
                }
            }

            $json['success'] = $this->language->get('text_reward_added');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function removeReward()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info) {
                $this->load->model('customer/customer');

                $this->model_customer_customer->deleteReward($order_id);
            }

            $json['success'] = $this->language->get('text_reward_removed');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addCommission()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info) {
                $this->load->model('marketing/affiliate');

                $affiliate_total = $this->model_marketing_affiliate->getTotalTransactionsByOrderId($order_id);

                if (!$affiliate_total) {
                    $this->model_marketing_affiliate->addTransaction($order_info['affiliate_id'], $this->language->get('text_order_id') . ' #' . $order_id, $order_info['commission'], $order_id);
                }
            }

            $json['success'] = $this->language->get('text_commission_added');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function removeCommission()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info) {
                $this->load->model('marketing/affiliate');

                $this->model_marketing_affiliate->deleteTransaction($order_id);
            }

            $json['success'] = $this->language->get('text_commission_removed');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function history()
    {
        $this->load->language('sale/order');

        $data['text_no_results'] = $this->language->get('text_no_results');

        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_notify'] = $this->language->get('column_notify');
        $data['column_comment'] = $this->language->get('column_comment');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['histories'] = array();

        $this->load->model('sale/order');

        $results = $this->model_sale_order->getOrderHistories($this->request->get['order_id'], ($page - 1) * 10, 10);

        foreach ($results as $result) {
            $data['histories'][] = array(
                'notify' => $result['notify'] ? $this->language->get('text_yes') : $this->language->get('text_no'),
                'status' => $result['status'],
                'comment' => nl2br($result['comment']),
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
            );
        }

        $history_total = $this->model_sale_order->getTotalOrderHistories($this->request->get['order_id']);

        $pagination = new Pagination();
        $pagination->total = $history_total;
        $pagination->page = $page;
        $pagination->limit = 10;
        $pagination->url = $this->url->link('sale/order/history', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['order_id'] . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($history_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($history_total - 10)) ? $history_total : ((($page - 1) * 10) + 10), $history_total, ceil($history_total / 10));

        $this->response->setOutput($this->load->view('sale/order_history.tpl', $data));
    }

    public function order_confirm()
    {

        if (isset($this->request->get['shipping_company'])) {
            $filter_shipping_company = $this->request->get['shipping_company'];
            $data['shipping_company'] = $this->request->get['shipping_company'];
        } else {
            $filter_shipping_company = null;
        }

        if(($filter_shipping_company != $this->user->getShippingCompany()) &&  ($this->user->getShippingCompany() != 0)){
          $this->response->redirect($this->url->link('common/blank', '', 'SSL'));
        }

        if (isset($this->request->get['order_case'])) {
            $order_case = $this->request->get['order_case'];
        } else {
            $order_case = null;
        }

        $url = '';

        if (isset($this->request->get['shipping_company'])) {
            $url .= '&shipping_company=' . $this->request->get['shipping_company'];
        }

        if (isset($this->request->get['order_case'])) {
            $url .= '&order_case=' . $this->request->get['order_case'];
        }


        $this->load->language('sale/order');

        $data['title'] = $this->language->get('text_invoice');

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');
        $data['logo'] = HTTPS_IMAGE_S3 . $this->config->get('config_logo_ltr');


        $data['text_invoice'] = $this->language->get('text_invoice');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_website'] = $this->language->get('text_website');
        $data['text_payment_address'] = $this->language->get('text_payment_address');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_comment'] = $this->language->get('text_comment');
        $data['text_invoice_title'] = $this->language->get('text_invoice_title');
        $data['text_more_details'] = $this->language->get('text_more_details');

        $data['column_product'] = $this->language->get('column_product');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_sku'] = $this->language->get('column_sku');

        $data['back_button'] = $this->url->link('customerpartner/shipping_order', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['invoice_url'] = $this->url->link('sale/shipping_process/country_invoice', '', 'SSL');

        $data['token'] = $this->session->data['token'];

        $this->load->model('sale/shipping_process');

        $this->load->model('setting/setting');

        $data['orders'] = array();

        $orders = array();

        if (isset($this->request->post['selected'])) {
            $orders = $this->request->post['selected'];
        } elseif (isset($this->request->get['order_id'])) {
            $orders[] = $this->request->get['order_id'];
        }

        foreach ($orders as $order_id) {
            $order_info = $this->model_sale_shipping_process->getOrder($order_id);
            $order_count = $this->model_sale_shipping_process->getOrderCount($order_id);
            if ($order_info) {
                $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

                if ($store_info) {
                    $store_address = $store_info['config_address'];
                    $store_email = $store_info['config_email'];
                    $store_telephone = $store_info['config_telephone'];
                    $store_fax = $store_info['config_fax'];
                } else {
                    $store_address = $this->config->get('config_address');
                    $store_email = $this->config->get('config_email');
                    $store_telephone = $this->config->get('config_telephone');
                    $store_fax = $this->config->get('config_fax');
                }

                if ($order_info['invoice_no']) {
                    $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                } else {
                    $invoice_no = '';
                }

                if ($order_info['payment_address_format']) {
                    $format = $order_info['payment_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['payment_firstname'],
                    'lastname' => $order_info['payment_lastname'],
                    'company' => $order_info['payment_company'],
                    'address_1' => $order_info['payment_address_1'],
                    'address_2' => $order_info['payment_address_2'],
                    'city' => $order_info['payment_city'],
                    'zone' => $order_info['payment_zone'],
                    'zone_code' => $order_info['payment_zone_code'],
                    'country' => $order_info['payment_country']
                );

                $payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                if ($order_info['shipping_address_format']) {
                    $format = $order_info['shipping_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{contact}' . "\n" . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{zone}',
                    '{contact}',
                    '{country}'
                );
                $replace = array(
                    'firstname' => $order_info['shipping_firstname'],
                    'lastname' => $order_info['shipping_lastname'],
                    'company' => $order_info['shipping_company'],
                    'address_1' => $order_info['shipping_address_1'],
                    'address_2' => $order_info['shipping_address_2'],
                    'city' => $order_info['shipping_city'],
                    'zone' => $order_info['payment_zone_code'],
                    'contact' => $order_info['telephone'],
                    'country' => $order_info['payment_iso_code_2'],
                    'zone_code' => $order_info['payment_zone_code'],
                    'payment_iso_code_2' => $order_info['payment_iso_code_2']
                );

                $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                $this->load->model('tool/upload');

                $product_data = array();

                $products = $this->model_sale_shipping_process->getOrderProducts($order_id);
                $data['closed_countries'] = array_column($this->model_sale_shipping_process->getOrderClosedCountries($order_id), 'country_id');

                foreach ($products as $product) {
                    $option_data = array();

                    $options = $this->model_sale_shipping_process->getOrderOptions($order_id, $product['order_product_id']);

                    foreach ($options as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['value'];
                        } else {
                            $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                            if ($upload_info) {
                                $value = $upload_info['name'];
                            } else {
                                $value = '';
                            }
                        }

                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $value
                        );
                    }

                    $product_data[$product['country_id']][] = array(
                        'product_id' => $product['product_id'],
                        'name' => $product['name'],
                        'model' => $product['model'],
                        'sku' => $product['sku'],
                        'option' => $option_data,
                        'quantity' => $product['quantity'],
                        'seller_name' => $product['seller_name'],
                        'seller_country' => $product['seller_country'],
                        'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                        'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
                    );
                }

                $voucher_data = array();

                $vouchers = $this->model_sale_shipping_process->getOrderVouchers($order_id);

                foreach ($vouchers as $voucher) {
                    $voucher_data[] = array(
                        'description' => $voucher['description'],
                        'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                    );
                }

                $total_data = array();

                $totals = $this->model_sale_shipping_process->getOrderTotals($order_id);

                foreach ($totals as $total) {
                    $total_data[] = array(
                        'title' => $total['title'],
                        'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                    );
                }

                $data['orders'][] = array(
                    'order_id' => $this->commonfunctions->convertOrderNumber($order_id,$order_info['date_added']),
                    'invoice_no' => $invoice_no,
                    'date_added' => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                    'store_name' => $order_info['store_name'],
                    'store_url' => rtrim($order_info['store_url'], '/'),
                    'store_address' => nl2br($store_address),
                    'store_email' => $store_email,
                    'store_telephone' => $store_telephone,
                    'store_fax' => $store_fax,
                    'email' => $order_info['email'],
                    'telephone' => $order_info['telephone'],
                    'shipping_address' => $shipping_address,
                    'shipping_method' => $order_info['shipping_method'],
                    'payment_address' => $payment_address,
                    'payment_iso_code_2' => $order_info['payment_iso_code_2'],
                    'count_order' => $order_count,
                    'payment_method' => $order_info['payment_method'],
                    'product' => $product_data,
                    'voucher' => $voucher_data,
                    'total' => $total_data,
                    'comment' => nl2br($order_info['comment'])
                );
            }
        }

        $this->response->setOutput($this->load->view('sale/shipping_order_confirm.tpl', $data));
    }

    public function return_confirm()
    {

        if (isset($this->request->get['shipping_company'])) {
            $filter_shipping_company = $this->request->get['shipping_company'];
        } else {
            $filter_shipping_company = null;
        }

        if (isset($this->request->get['id'])) {
            $id = $this->request->get['id'];
        } else {
            $id = null;
        }

        $url = '';

        if (isset($this->request->get['shipping_company'])) {
            $url .= '&shipping_company=' . $this->request->get['shipping_company'];
        }

        if (isset($this->request->get['return_case'])) {
            $url .= '&return_case=' . $this->request->get['return_case'];
        }

        $this->load->model('sale/shipping_process');
        $this->load->model('setting/setting');
        $this->load->model('catalog/product');
        $this->load->language('sale/order');
        $data['title'] = $this->language->get('text_invoice');

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');
        $data['logo'] = HTTPS_IMAGE_S3 . $this->config->get('config_logo_ltr');


        $data['text_invoice'] = $this->language->get('text_invoice');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_website'] = $this->language->get('text_website');
        $data['text_payment_address'] = $this->language->get('text_payment_address');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_comment'] = $this->language->get('text_comment');
        $data['text_invoice_title'] = $this->language->get('text_invoice_title');
        $data['text_more_details'] = $this->language->get('text_more_details');

        $data['column_product'] = $this->language->get('column_product');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_sku'] = $this->language->get('column_sku');

        $data['back_button'] = $this->url->link('customerpartner/shipping_returns', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $data['invoice_url'] = $this->url->link('sale/shipping_process/rma_invoice', '', 'SSL');

        $data['token'] = $this->session->data['token'];


        $data['orders'] = array();


        $return_info = $this->model_sale_shipping_process->getReturn($id);
        $data['is_closed'] = $return_info['is_closed'];
        $data['rma_id'] = $id;
        $order_id = $return_info['order_id'];
        $seller_data = $this->model_sale_shipping_process->getSeller($return_info['customer_id']);
        $order_info = $this->model_sale_shipping_process->getOrder($return_info['order_id']);
        $order_count = $this->model_sale_shipping_process->getOrderCount($return_info['order_id']);
        if ($return_info) {
            $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

            if ($store_info) {
                $store_name = $store_info['config_name'];
                $store_company = $store_info['config_owner'];
                $store_address = $store_info['config_address'];
                $store_email = $store_info['config_email'];
                $store_telephone = $store_info['config_telephone'];
                $store_fax = $store_info['config_fax'];
            } else {
                $store_address = $this->config->get('config_address');
                $store_email = $this->config->get('config_email');
                $store_telephone = $this->config->get('config_telephone');
                $store_fax = $this->config->get('config_fax');
            }

            if ($order_info['invoice_no']) {
                $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
            } else {
                $invoice_no = '';
            }

            /* $format = '{store_name}' . "\n" . '{company}' . "\n" . '{config_address}';

              $find = array(
              '{store_name}',
              '{company}',
              '{config_address}',
              );

              $replace = array(
              'store_name' => $store_name,
              'company' => $store_company,
              'config_address' => $store_address,
              );

              $store_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

              if ($order_info['shipping_address_format']) {
              $format = $order_info['shipping_address_format'];
              } else {
              $format = '{firstname} {lastname}' . "\n" . '{contact}' . "\n" ."\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{zone}' . "\n" . '{country}';
              } */

            $format = '{screenname} {companyname}' . "\n" . '{mobile_number}' . "\n" . '{zone_name}' . "\n" . '{country_name}' . "\n" . '{address}' . "\n" . '{opening_time}';

            $find = array(
                '{screenname}',
                '{companyname}',
                '{mobile_number}',
                '{zone_name}',
                '{country_name}',
                '{address}',
                '{opening_time}'
            );

            $replace = array(
                '{screenname}' => $seller_data['screenname'],
                '{companyname}' => $seller_data['companyname'],
                '{mobile_number}' => $seller_data['telephone'],
                '{zone_name}' => $seller_data['zone_name'],
                '{country_name}' => $seller_data['country_name'],
                '{address}' => $seller_data['warehouse_address'],
                '{opening_time} ' => $seller_data['opening_time']
            );

            $store_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $format = '{firstname} {lastname}' . "\n" . '{mobile_number}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{zone}' . "\n" . '{country}';

            $find = array(
                '{firstname}',
                '{lastname}',
                '{mobile_number}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{zone}',
                '{contact}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'mobile_number' => $order_info['telephone'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['shipping_address_2'],
                'city' => $order_info['shipping_city'],
                'zone' => $order_info['shipping_zone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country']
            );


            $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $this->load->model('tool/upload');

            $product_data = array();

            $product = $this->model_sale_shipping_process->getOrderProduct($order_id, $return_info['product_id']);

            $data['closed_countries'] = array_column($this->model_sale_shipping_process->getOrderClosedCountries($order_id), 'country_id');

            $option_data = array();

            $options = $this->model_sale_shipping_process->getOrderOptions($order_id, $product['order_product_id']);

            foreach ($options as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name' => $option['name'],
                    'value' => $value
                );
            }

            $product_data[] = array(
                'product_id' => $product['product_id'],
                'name' => $product['name'],
                'model' => $product['model'],
                'sku' => $product['sku'],
                'option' => $option_data,
                'quantity' => $product['quantity'],
                'seller_name' => $product['seller_name'],
                'seller_country' => $product['seller_country'],
                'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
            );

            $voucher_data = array();

            $vouchers = $this->model_sale_shipping_process->getOrderVouchers($order_id);

            foreach ($vouchers as $voucher) {
                $voucher_data[] = array(
                    'description' => $voucher['description'],
                    'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                );
            }

            $total_data = array();

            $totals = $this->model_sale_shipping_process->getOrderTotals($order_id);

            foreach ($totals as $total) {
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                );
            }

            $data['orders'][] = array(
                'order_id' => $order_id,
                'invoice_no' => $invoice_no,
                'date_added' => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                'store_name' => $order_info['store_name'],
                'store_url' => rtrim($order_info['store_url'], '/'),
                'store_address' => nl2br($store_address),
                'store_email' => $store_email,
                'store_telephone' => $store_telephone,
                'store_fax' => $store_fax,
                'email' => $order_info['email'],
                'telephone' => $order_info['telephone'],
                'shipping_address' => $shipping_address,
                'shipping_method' => $order_info['shipping_method'],
                'store_address' => $store_address,
                'payment_iso_code_2' => $order_info['payment_iso_code_2'],
                'count_order' => $order_count,
                'payment_method' => $order_info['payment_method'],
                'product' => $product_data,
                'voucher' => $voucher_data,
                'total' => $total_data,
                'comment' => nl2br($order_info['comment'])
            );
        }


        $this->response->setOutput($this->load->view('sale/shipping_return_confirm.tpl', $data));
    }

    public function shipping()
    {
        $this->load->language('sale/order');

        $data['title'] = $this->language->get('text_shipping');

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');

        $data['text_shipping'] = $this->language->get('text_shipping');
        $data['text_picklist'] = $this->language->get('text_picklist');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_website'] = $this->language->get('text_website');
        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_payment_address'] = $this->language->get('text_payment_address');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_sku'] = $this->language->get('text_sku');
        $data['text_upc'] = $this->language->get('text_upc');
        $data['text_ean'] = $this->language->get('text_ean');
        $data['text_jan'] = $this->language->get('text_jan');
        $data['text_isbn'] = $this->language->get('text_isbn');
        $data['text_mpn'] = $this->language->get('text_mpn');
        $data['text_comment'] = $this->language->get('text_comment');

        $data['column_location'] = $this->language->get('column_location');
        $data['column_reference'] = $this->language->get('column_reference');
        $data['column_product'] = $this->language->get('column_product');
        $data['column_weight'] = $this->language->get('column_weight');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');


        $this->load->model('sale/order');

        $this->load->model('catalog/product');

        $this->load->model('setting/setting');

        $data['orders'] = array();

        $orders = array();

        if (isset($this->request->post['selected'])) {
            $orders = $this->request->post['selected'];
        } elseif (isset($this->request->get['order_id'])) {
            $orders[] = $this->request->get['order_id'];
        }

        foreach ($orders as $order_id) {
            $order_info = $this->model_sale_order->getOrder($order_id);

            // Make sure there is a shipping method
            if ($order_info && $order_info['shipping_code']) {
                $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

                if ($store_info) {
                    $store_address = $store_info['config_address'];
                    $store_email = $store_info['config_email'];
                    $store_telephone = $store_info['config_telephone'];
                    $store_fax = $store_info['config_fax'];
                } else {
                    $store_address = $this->config->get('config_address');
                    $store_email = $this->config->get('config_email');
                    $store_telephone = $this->config->get('config_telephone');
                    $store_fax = $this->config->get('config_fax');
                }

                if ($order_info['invoice_no']) {
                    $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                } else {
                    $invoice_no = '';
                }

                if ($order_info['shipping_address_format']) {
                    $format = $order_info['shipping_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{postcode}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['shipping_firstname'],
                    'lastname' => $order_info['shipping_lastname'],
                    'company' => $order_info['shipping_company'],
                    'address_1' => $order_info['shipping_address_1'],
                    'address_2' => $order_info['shipping_address_2'],
                    'city' => $order_info['shipping_city'],
                    'postcode' => $order_info['shipping_postcode'],
                    'zone' => $order_info['shipping_zone'],
                    'zone_code' => $order_info['shipping_zone_code'],
                    'country' => $order_info['shipping_country']
                );

                $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                $this->load->model('tool/upload');

                $product_data = array();

                $products = $this->model_sale_order->getOrderProducts($order_id);

                foreach ($products as $product) {
                    $option_weight = '';

                    $product_info = $this->model_catalog_product->getProduct($product['product_id']);

                    if ($product_info) {
                        $option_data = array();

                        $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

                        foreach ($options as $option) {
                            $option_value_info = $this->model_catalog_product->getProductOptionValue($order_id, $product['order_product_id']);

                            if ($option['type'] != 'file') {
                                $value = $option['value'];
                            } else {
                                $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                                if ($upload_info) {
                                    $value = $upload_info['name'];
                                } else {
                                    $value = '';
                                }
                            }

                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $value
                            );

                            $product_option_value_info = $this->model_catalog_product->getProductOptionValue($product['product_id'], $option['product_option_value_id']);

                            if ($product_option_value_info) {
                                if ($product_option_value_info['weight_prefix'] == '+') {
                                    $option_weight += $product_option_value_info['weight'];
                                } elseif ($product_option_value_info['weight_prefix'] == '-') {
                                    $option_weight -= $product_option_value_info['weight'];
                                }
                            }
                        }

                        $product_data[] = array(
                            'name' => $product_info['name'],
                            'model' => $product_info['model'],
                            'option' => $option_data,
                            'quantity' => $product['quantity'],
                            'location' => $product_info['location'],
                            'sku' => $product_info['sku'],
                            'upc' => $product_info['upc'],
                            'ean' => $product_info['ean'],
                            'jan' => $product_info['jan'],
                            'isbn' => $product_info['isbn'],
                            'mpn' => $product_info['mpn'],
                            'weight' => $this->weight->format(($product_info['weight'] + $option_weight) * $product['quantity'], $product_info['weight_class_id'], $this->language->get('decimal_point'), $this->language->get('thousand_point'))
                        );
                    }
                }

                $data['orders'][] = array(
                    'order_id' => $order_id,
                    'invoice_no' => $invoice_no,
                    'date_added' => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                    'store_name' => $order_info['store_name'],
                    'store_url' => rtrim($order_info['store_url'], '/'),
                    'store_address' => nl2br($store_address),
                    'store_email' => $store_email,
                    'store_telephone' => $store_telephone,
                    'store_fax' => $store_fax,
                    'email' => $order_info['email'],
                    'telephone' => $order_info['telephone'],
                    'shipping_address' => $shipping_address,
                    'shipping_method' => $order_info['shipping_method'],
                    'product' => $product_data,
                    'comment' => nl2br($order_info['comment'])
                );
            }
        }

        $this->response->setOutput($this->load->view('sale/order_shipping.tpl', $data));
    }

    public function shipment()
    {
        $this->load->model('sale/order');

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }

        $order_info = $this->model_sale_order->getOrder($order_id);

        if ($order_info) {
            $this->load->language('sale/order');

            $this->document->setTitle($this->language->get('heading_title'));

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_ip_add'] = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
            $data['text_order_detail'] = $this->language->get('text_order_detail');
            $data['text_customer_detail'] = $this->language->get('text_customer_detail');
            $data['text_option'] = $this->language->get('text_option');
            $data['text_store'] = $this->language->get('text_store');
            $data['text_date_added'] = $this->language->get('text_date_added');
            $data['text_payment_method'] = $this->language->get('text_payment_method');
            $data['text_shipping_method'] = $this->language->get('text_shipping_method');
            $data['text_customer'] = $this->language->get('text_customer');
            $data['text_customer_group'] = $this->language->get('text_customer_group');
            $data['text_email'] = $this->language->get('text_email');
            $data['text_telephone'] = $this->language->get('text_telephone');
            $data['text_invoice'] = $this->language->get('text_invoice');
            $data['text_reward'] = $this->language->get('text_reward');
            $data['text_affiliate'] = $this->language->get('text_affiliate');
            $data['text_order'] = sprintf($this->language->get('text_order'), $this->request->get['order_id']);
            $data['text_payment_address'] = $this->language->get('text_payment_address');
            $data['text_shipping_address'] = $this->language->get('text_shipping_address');
            $data['text_comment'] = $this->language->get('text_comment');

            $data['text_account_custom_field'] = $this->language->get('text_account_custom_field');
            $data['text_payment_custom_field'] = $this->language->get('text_payment_custom_field');
            $data['text_shipping_custom_field'] = $this->language->get('text_shipping_custom_field');
            $data['text_browser'] = $this->language->get('text_browser');
            $data['text_ip'] = $this->language->get('text_ip');
            $data['text_forwarded_ip'] = $this->language->get('text_forwarded_ip');
            $data['text_user_agent'] = $this->language->get('text_user_agent');
            $data['text_accept_language'] = $this->language->get('text_accept_language');
            $data['text_history'] = $this->language->get('text_history');
            $data['text_history_add'] = $this->language->get('text_history_add');
            $data['text_loading'] = $this->language->get('text_loading');

            $data['column_product'] = $this->language->get('column_product');
            $data['column_seller'] = $this->language->get('column_seller');
            $data['column_quantity'] = $this->language->get('column_quantity');
            $data['column_price'] = $this->language->get('column_price');
            $data['column_total'] = $this->language->get('column_total');

            $data['entry_order_status'] = $this->language->get('entry_order_status');
            $data['entry_notify'] = $this->language->get('entry_notify');
            $data['entry_override'] = $this->language->get('entry_override');
            $data['entry_comment'] = $this->language->get('entry_comment');

            $data['help_override'] = $this->language->get('help_override');

            $data['button_invoice_print'] = $this->language->get('button_invoice_print');
            $data['button_shipping_print'] = $this->language->get('button_shipping_print');
            $data['button_edit'] = $this->language->get('button_edit');
            $data['button_cancel'] = $this->language->get('button_cancel');
            $data['button_generate'] = $this->language->get('button_generate');
            $data['button_reward_add'] = $this->language->get('button_reward_add');
            $data['button_reward_remove'] = $this->language->get('button_reward_remove');
            $data['button_commission_add'] = $this->language->get('button_commission_add');
            $data['button_commission_remove'] = $this->language->get('button_commission_remove');
            $data['button_history_add'] = $this->language->get('button_history_add');
            $data['button_ip_add'] = $this->language->get('button_ip_add');
            $data['button_send_shipment_request'] = $this->language->get('button_send_shipment_request');

            $data['tab_history'] = $this->language->get('tab_history');
            $data['tab_additional'] = $this->language->get('tab_additional');
            $data['tab_firstflight'] = $this->language->get('tab_firstflight');

            $data['token'] = $this->session->data['token'];

            $url = '';

            if (isset($this->request->get['filter_order_id'])) {
                $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
            }

            if (isset($this->request->get['filter_customer'])) {
                $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_order_status'])) {
                $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
            }

            if (isset($this->request->get['filter_total'])) {
                $url .= '&filter_total=' . $this->request->get['filter_total'];
            }

            if (isset($this->request->get['filter_date_added'])) {
                $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
            }

            if (isset($this->request->get['filter_date_modified'])) {
                $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, 'SSL')
            );

            $data['shipping'] = $this->url->link('sale/order/shipping', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], 'SSL');
            $data['invoice'] = $this->url->link('sale/order/order_confirm', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], 'SSL');
            $data['edit'] = $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], 'SSL');
            $data['cancel'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, 'SSL');

            $data['order_id'] = $this->request->get['order_id'];

            $data['store_name'] = $order_info['store_name'];
            $data['store_url'] = $order_info['store_url'];

            if ($order_info['invoice_no']) {
                $data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
            } else {
                $data['invoice_no'] = '';
            }

            $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

            $data['firstname'] = $order_info['firstname'];
            $data['lastname'] = $order_info['lastname'];

            if ($order_info['customer_id']) {
                $data['customer'] = $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $order_info['customer_id'], 'SSL');
            } else {
                $data['customer'] = '';
            }

            $this->load->model('customer/customer_group');

            $customer_group_info = $this->model_customer_customer_group->getCustomerGroup($order_info['customer_group_id']);

            if ($customer_group_info) {
                $data['customer_group'] = $customer_group_info['name'];
            } else {
                $data['customer_group'] = '';
            }

            $data['email'] = $order_info['email'];
            $data['telephone'] = $order_info['telephone'];

            $data['shipping_method'] = $order_info['shipping_method'];
            $data['payment_method'] = $order_info['payment_method'];

            // Payment Address
            if ($order_info['payment_address_format']) {
                $format = $order_info['payment_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['payment_firstname'],
                'lastname' => $order_info['payment_lastname'],
                'company' => $order_info['payment_company'],
                'address_1' => $order_info['payment_address_1'],
                'address_2' => $order_info['payment_address_2'],
                'city' => $order_info['payment_city'],
                'postcode' => $order_info['payment_postcode'],
                'zone' => $order_info['payment_zone'],
                'zone_code' => $order_info['payment_zone_code'],
                'country' => $order_info['payment_country']
            );

            $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            // Shipping Address
            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['shipping_address_2'],
                'city' => $order_info['shipping_city'],
                'postcode' => $order_info['shipping_postcode'],
                'zone' => $order_info['shipping_zone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country']
            );

            $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            // Uploaded files
            $this->load->model('tool/upload');

            $data['products'] = array();

            $products = $this->model_sale_order->getOrderProductsforShipment($this->request->get['order_id']);

            foreach ($products as $product) {
                $option_data = array();

                $options = $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

                foreach ($options as $option) {
                    if ($option['type'] != 'file') {
                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $option['value'],
                            'type' => $option['type']
                        );
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $upload_info['name'],
                                'type' => $option['type'],
                                'href' => $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], 'SSL')
                            );
                        }
                    }
                }

                $data['products'][$product['customer_id']][] = array(
                    'order_product_id' => $product['order_product_id'],
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'seller' => $product['seller_name'],
                    'option' => $option_data,
                    'quantity' => $product['quantity'],
                    'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'href' => $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $product['product_id'], 'SSL')
                );
            }

            $data['vouchers'] = array();

            $vouchers = $this->model_sale_order->getOrderVouchers($this->request->get['order_id']);

            foreach ($vouchers as $voucher) {
                $data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
                    'href' => $this->url->link('sale/voucher/edit', 'token=' . $this->session->data['token'] . '&voucher_id=' . $voucher['voucher_id'], 'SSL')
                );
            }

            $data['totals'] = array();

            $totals = $this->model_sale_order->getOrderTotals($this->request->get['order_id']);

            foreach ($totals as $total) {
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                );
            }

            $data['comment'] = nl2br($order_info['comment']);

            $this->load->model('customer/customer');

            $data['reward'] = $order_info['reward'];

            $data['reward_total'] = $this->model_customer_customer->getTotalCustomerRewardsByOrderId($this->request->get['order_id']);

            $data['affiliate_firstname'] = $order_info['affiliate_firstname'];
            $data['affiliate_lastname'] = $order_info['affiliate_lastname'];

            if ($order_info['affiliate_id']) {
                $data['affiliate'] = $this->url->link('marketing/affiliate/edit', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $order_info['affiliate_id'], 'SSL');
            } else {
                $data['affiliate'] = '';
            }

            $data['commission'] = $this->currency->format($order_info['commission'], $order_info['currency_code'], $order_info['currency_value']);

            $this->load->model('marketing/affiliate');

            $data['commission_total'] = $this->model_marketing_affiliate->getTotalTransactionsByOrderId($this->request->get['order_id']);

            $this->load->model('localisation/order_status');

            $order_status_info = $this->model_localisation_order_status->getOrderStatus($order_info['order_status_id']);

            if ($order_status_info) {
                $data['order_status'] = $order_status_info['name'];
            } else {
                $data['order_status'] = '';
            }

            $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

            $data['order_status_id'] = $order_info['order_status_id'];

            $data['account_custom_field'] = $order_info['custom_field'];

            // Custom Fields
            $this->load->model('customer/custom_field');

            $data['account_custom_fields'] = array();

            $filter_data = array(
                'sort' => 'cf.sort_order',
                'order' => 'ASC',
            );

            $custom_fields = $this->model_customer_custom_field->getCustomFields($filter_data);

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'account' && isset($order_info['custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['account_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['account_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['account_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['custom_field'][$custom_field['custom_field_id']]
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['account_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name']
                            );
                        }
                    }
                }
            }

            // Custom fields
            $data['payment_custom_fields'] = array();

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'address' && isset($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['payment_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['payment_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['payment_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name'],
                                    'sort_order' => $custom_field['sort_order']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['payment_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['payment_custom_field'][$custom_field['custom_field_id']],
                            'sort_order' => $custom_field['sort_order']
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['payment_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }
                }
            }

            // Shipping
            $data['shipping_custom_fields'] = array();

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'address' && isset($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['shipping_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['shipping_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['shipping_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name'],
                                    'sort_order' => $custom_field['sort_order']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['shipping_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['shipping_custom_field'][$custom_field['custom_field_id']],
                            'sort_order' => $custom_field['sort_order']
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['shipping_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }
                }
            }

            $data['ip'] = $order_info['ip'];
            $data['forwarded_ip'] = $order_info['forwarded_ip'];
            $data['user_agent'] = $order_info['user_agent'];
            $data['accept_language'] = $order_info['accept_language'];

            // Additional Tabs
            $data['tabs'] = array();

            $this->load->model('extension/extension');

            $content = $this->load->controller('payment/' . $order_info['payment_code'] . '/order');

            if ($content) {
                $this->load->language('payment/' . $order_info['payment_code']);

                $data['tabs'][] = array(
                    'code' => $order_info['payment_code'],
                    'title' => $this->language->get('heading_title'),
                    'content' => $content
                );
            }

            $extensions = $this->model_extension_extension->getInstalled('fraud');

            foreach ($extensions as $extension) {
                if ($this->config->get($extension . '_status')) {
                    $this->load->language('fraud/' . $extension);

                    $content = $this->load->controller('fraud/' . $extension . '/order');

                    if ($content) {
                        $data['tabs'][] = array(
                            'code' => $extension,
                            'title' => $this->language->get('heading_title'),
                            'content' => $content
                        );
                    }
                }
            }

            // API login
            $this->load->model('user/api');

            $api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

            if ($api_info) {
                $data['api_id'] = $api_info['api_id'];
                $data['api_key'] = $api_info['key'];
                $data['api_ip'] = $this->request->server['REMOTE_ADDR'];
            } else {
                $data['api_id'] = '';
                $data['api_key'] = '';
                $data['api_ip'] = '';
            }

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('sale/order_shipment.tpl', $data));
        } else {
            $this->load->language('error/not_found');

            $this->document->setTitle($this->language->get('heading_title'));

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_not_found'] = $this->language->get('text_not_found');

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('error/not_found.tpl', $data));
        }
    }

    public function send_shipment_request()
    {

        $json = array();

        $this->load->model("sale/order");

        if (isset($this->request->post['order_id'])) {
            $order_id = $this->request->post['order_id'];
        } else {
            $json['error_order'] = $this->language->get('error_order');
            $json["status"] = 0;
        }

        if (isset($this->request->post['product_ids'])) {
            $product_ids = $this->request->post['product_ids'];
        } else {
            $json['error_product_selected'] = $this->language->get('error_product_selected');
            $json["status"] = 0;
        }

        if (isset($this->request->post['seller_id'])) {
            $seller_id = $this->request->post['seller_id'];
        } else {
            $json['error_seller_selected'] = $this->language->get('error_seller_selected');
            $json["status"] = 0;
        }

        if (isset($this->request->post['seller_id'])) {
            $seller_id = $this->request->post['seller_id'];
        } else {
            $json['error_seller_selected'] = $this->language->get('error_seller_selected');
            $json["status"] = 0;
        }

        if (!$json) {
            //if any of the products are not assigned for this seller then the shipment operation will be cancelled
            $check_products_seller = $this->model_sale_order->check_products_seller($product_ids, $seller_id);

            if (count($check_products_seller) < count(explode(",", $product_ids))) {
                $json['error_check_seller_products'] = $this->language->get('error_check_seller_products'); //"Error: some products are not related to this seller ";
                $json["status"] = 0;
                echo json_encode($json);
                return;
            }

            $order_info = $this->model_sale_order->getOrder($order_id);
            $destination_code = $this->model_sale_order->get_destination_code($order_info['shipping_zone_id']);

            $shipment_status = $this->model_sale_order->check_shipment($order_id);
            $data = array(
                "order_id" => $order_info['order_id'],
                "AWBNO" => $order_info['customer_id'] . "_" . $order_id . "_" . $seller_id,
                "prod_type" => "DOX"
            );

            if (!$shipment_status) {
                //getting order information


                $operation_data = array();
                $operation_data['argAwbBooking']['AWBNo'] = $data['AWBNO'];
                $operation_data['argAwbBooking']['Consignee'] = $order_info['customer'];
                #$operation_data['argAwbBooking']['ConsigneeCPerson'] = '';
                $operation_data['argAwbBooking']['ConsigneeAddress1'] = $order_info['shipping_address_1'];
                $operation_data['argAwbBooking']['ConsigneeAddress2'] = $order_info['shipping_address_2'];
                $operation_data['argAwbBooking']['ConsigneePhone'] = $order_info['telephone'];
                $operation_data['argAwbBooking']['ConsigneeCity'] = $order_info['shipping_city'];
                $operation_data['argAwbBooking']['ConsigneeCountry'] = $order_info['shipping_country'];
                $operation_data['argAwbBooking']['DestCode'] = $destination_code['destination_code'];
                $operation_data['argAwbBooking']['CountryCode'] = $order_info['shipping_country_id'];
                #$operation_data['argAwbBooking']['RetailZipCode'] = '';
                $operation_data['argAwbBooking']['Price'] = $order_info['total'];
                $operation_data['argAwbBooking']['Weight'] = '0';
                $operation_data['argAwbBooking']['CustCash'] = '';
                $operation_data['argAwbBooking']['ServiceType'] = '';
                $operation_data['argAwbBooking']['Quantity'] = '';
                $operation_data['argAwbBooking']['AgentCode'] = '';
                $operation_data['argAwbBooking']['AgentAWBNo'] = '';
                $operation_data['argAwbBooking']['ShipperRefNo'] = '';
                $operation_data['argAwbBooking']['GoodsDescription'] = '';
                $operation_data['argAwbBooking']['SpecialInstruction'] = '';
                $operation_data['argAwbBooking']['ValueCurrency'] = '';
                $operation_data['argAwbBooking']['ValueOfShipment'] = '';
                $operation_data['argAwbBooking']['ProductType'] = $data['prod_type'];
                $operation_data['argAwbBooking']['DOX'] = '';
                $operation_data['argAwbBooking']['ConsigneeMob'] = '';
                $operation_data['argAwbBooking']['Dimension'] = '';
                $operation_data['argAwbBooking']['ChargeableWeight'] = '';

                //if the shipment operation is not created then send the request.
                $response = $this->FFServices->NewOrder($operation_data);
                //$this->model_sale_order->add_shipment($data);
            }

            $no_of_orders = $this->model_sale_order->get_booking_count($data['AWBNO']);

            $operation_data = array();
            $operation_data['argAwbBooking']['AWBNo'] = $data['AWBNO'];
            $operation_data['argAwbBooking']['Origin'] = '';
            $operation_data['argAwbBooking']['Destination'] = '';
            $operation_data['argAwbBooking']['AddnlInfo'] = '';
            $operation_data['argAwbBooking']['CustomerCode'] = '';
            $operation_data['argAwbBooking']['Shipper'] = '';
            $operation_data['argAwbBooking']['ShipperCPErson'] = '';
            $operation_data['argAwbBooking']['ShipperAddress1'] = '';
            $operation_data['argAwbBooking']['ShipperAddress2'] = '';
            $operation_data['argAwbBooking']['ShipperPhone'] = '';
            $operation_data['argAwbBooking']['ShipperPin'] = '';
            $operation_data['argAwbBooking']['ShipperCity'] = '';
            $operation_data['argAwbBooking']['ShipperCountry'] = '';
            $operation_data['argAwbBooking']['Consignee'] = $order_info['customer'];
            $operation_data['argAwbBooking']['ConsigneeCPerson'] = '';
            $operation_data['argAwbBooking']['ConsigneeAddress1'] = $order_info['payment_address_1'];
            $operation_data['argAwbBooking']['ConsigneeAddress2'] = $order_info['payment_address_2'];
            $operation_data['argAwbBooking']['ConsigneePhone'] = $order_info['telephone'];
            $operation_data['argAwbBooking']['ConsigneeMobile'] = '';
            $operation_data['argAwbBooking']['ConsigneePin'] = '';
            $operation_data['argAwbBooking']['ConsigneeCity'] = '';
            $operation_data['argAwbBooking']['ConsigneeCountry'] = '';
            $operation_data['argAwbBooking']['ServiceType'] = '';
            $operation_data['argAwbBooking']['ProductType'] = 'DOX';
            $operation_data['argAwbBooking']['InvoiceValue'] = '';
            $operation_data['argAwbBooking']['CODAmount'] = '';
            $operation_data['argAwbBooking']['SpecialInst'] = '';
            $operation_data['argAwbBooking']['Weight'] = '';
            $operation_data['argAwbBooking']['email'] = '';
            $operation_data['argAwbBooking']['RTOShipment'] = '';
            $operation_data['argAwbBooking']['VehType'] = '';
            $operation_data['argAwbBooking']['PBranch'] = '';
            $operation_data['argAwbBooking']['ReadyDate'] = '';
            $operation_data['argAwbBooking']['ReadyTime'] = '';
            $operation_data['argAwbBooking']['ClsoingTime'] = '';

            $booking_response = $this->FFServices->CreateBooking($operation_data);
            $data['book_response'] = $booking_response->CreateBookingResult;
            $this->model_sale_order->add_booking($data);
            $json = ["status" => 1, "MSG" => "Success"];
        }
        echo json_encode($json);
    }

    /// validation
    public function check_order_products()
    {


        $return = false;

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->commonfunctions->getOrderNumber($this->request->get['order_id']);
        } else {
            echo json_encode(array("Error:" => "Order id is required"));
            return false;
        }

        if (isset($this->request->get['country_id'])) {
            $country_id = $this->request->get['country_id'];
        } else {
            echo json_encode(array("Error:" => "Country Id is required"));
            return false;
        }

        $product_ids = array();
        if (isset($this->request->get['product_skus'])) {
            $product_skus = explode('*,', trim($this->request->get['product_skus'],'*'));
        } else {
            $json [] = array("Error:" => "Product SKUs are required");
            $return = true;
        }

        $product_quantities = array();
        foreach($product_skus as $sku){
            if(isset($product_quantities[trim($sku,'*')])){
              $product_quantities[trim($sku,'*')] ++;
            }else{
              $product_quantities[trim($sku,'*')] = 1;
            }
        }

        $this->load->model("sale/order");
        $missing_products = array();
        $extra_products = $order_product_quantities = array();
        $order_products = $this->model_sale_order->getOrderNonFailedProducts($order_id, $country_id);
        $order_products_skus = array_column($order_products, 'sku');
        foreach ($order_products as $product) {
            if (!in_array($product['sku'], $product_skus)) {
                $missing_products[] = $product['product_id'];
            }
        }
        if (!empty($missing_products)) {
            $json [] = array("Error" => "Missing products", 'ids' => $missing_products);
            $return = true;
        }

        foreach ($order_products as $prod) {
            if (isset($order_product_quantities[$prod['sku']])) {
                $order_product_quantities[$prod['sku']] += $prod['quantity'];
            } else {
                $order_product_quantities[$prod['sku']] = $prod['quantity'];
            }
        }


        foreach ($product_skus as $sku) {
            if (!in_array($sku, $order_products_skus)) {
                $extra_products[] = html_entity_decode($sku);
            }
        }

        if (!empty($extra_products)) {
            $json [] = array("Error2" => "Extra products", 'skus' => $extra_products);
            $return = true;
        }


        // to check same product was bought with a different options
        if (empty($json)) {

          foreach($order_product_quantities as $key => $product){
            if($product_quantities[$key] != $product){
              $json [] = array("Error3" => "quantity mismatch", 'skus' => array(html_entity_decode($key)), 'msg' => 'Scanning error quantity mismatch.  ');
                $return = true;
            }
          }
        }


        if ($return) {
            echo json_encode($json);
            return;
        }

        echo json_encode('success');
        return;
    }

    public function check_return_products()
    {

        $return = false;

        if (isset($this->request->get['rma_id'])) {
            $rma_id = $this->request->get['rma_id'];
        } else {
            echo json_encode(array("Error:" => "Return id is required"));
            return false;
        }

        $product_ids = array();
        if (isset($this->request->get['product_sku'])) {
            $product_skus = explode(',', $this->request->get['product_sku']);
        } else {
            $json [] = array("Error:" => "Product SKUs are required");
            $return = true;
        }

        $this->load->model("sale/order");
        $missing_products = array();
        $extra_products = array();
        $order_products = $this->model_sale_order->getReturnNonFailedProducts($rma_id);
        $order_products_skus = array_column($order_products, 'sku');
        foreach ($order_products as $product) {
            if (!in_array($product['sku'], $product_skus)) {
                $missing_products[] = $product['product_id'];
            }
        }
        if (!empty($missing_products)) {
            $json [] = array("Error" => "Missing products", 'ids' => $missing_products);
            $return = true;
        }

        foreach ($product_skus as $sku) {
            if (!in_array($sku, $order_products_skus)) {
                $extra_products[] = $sku;
            }
        }

        if (!empty($extra_products)) {
            $json [] = array("Error2" => "Extra products", 'skus' => $extra_products);
            $return = true;
        }

        if ($return) {
            echo json_encode($json);
            return;
        }


        echo json_encode('success');
        return;
    }

    public function show_product()
    {

        $this->load->language('sale/order');

        $data['title'] = $this->language->get('text_invoice');

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');
        $data['logo'] = HTTPS_IMAGE_S3 . $this->config->get('config_logo_ltr');


        $data['text_invoice'] = $this->language->get('text_invoice');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_website'] = $this->language->get('text_website');
        $data['text_seller_info'] = $this->language->get('text_seller_info');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_comment'] = $this->language->get('text_comment');
        $data['text_invoice_title'] = $this->language->get('text_invoice_title');
        $data['text_more_details'] = $this->language->get('text_more_details');

        $data['column_product'] = $this->language->get('column_product');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_sku'] = $this->language->get('column_sku');

        $data['token'] = $this->session->data['token'];

        $this->load->model('sale/shipping_process');
        $this->load->model('localisation/zone');
        $this->load->model('setting/setting');

        $data['orders'] = array();

        $orders = array();

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }

        if (isset($this->request->get['seller_id'])) {
            $seller_id = $this->request->get['seller_id'];
        } else {
            $seller_id = 0;
        }

        $order_info = $this->model_sale_shipping_process->getOrder($order_id);
        $seller_info = $this->model_sale_shipping_process->getSeller($seller_id);
        $order_count = $this->model_sale_shipping_process->getOrderCount($order_id);
        $products_info = $this->model_sale_shipping_process->getSellerOrderProducts($order_id, $seller_id);

        if ($order_info) {

            $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

            if ($store_info) {
                $store_email = $store_info['config_email'];
                $store_telephone = $store_info['config_telephone'];
            } else {
                $store_email = $this->config->get('config_email');
                $store_telephone = $this->config->get('config_telephone');
            }

            if ($order_info['invoice_no']) {
                $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
            } else {
                $invoice_no = '';
            }

            if ($order_info['payment_address_format']) {
                $format = $order_info['payment_address_format'];
            } else {
                $format = '{companyname}' . "\n" . '{screenname}' . "\n" . '{country}' . "\n" . '{zone}' . "\n" . '{bookingNo}' . "\n" . '{payment_method}' . "\n";
            }

            $find = array(
                '{companyname}',
                '{screenname}',
                '{country}',
                '{zone}',
                '{bookingNo}',
                '{payment_method}',
            );

            $replace = array(
                'companyname' => $seller_info['companyname'],
                'screenname' => $seller_info['screenname'],
                'country' => $seller_info['country_name'],
                'zone' => $seller_info['zone_name'],
                'bookingNo' => '',
                'payment_method' => $order_info['payment_method']
            );

            $payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                $format = '{config_name}' . "\n" . '{config_telephone}' . "\n" . '{config_address}' . "\n" . '{config_geocode}' . "\n" . '{Consignee}' . "\n" . '{ConsigneeAddress1}' . "\n" . '{ConsigneeCity}' . "\n" . '{ConsigneeCountry}';
            }

            $zone_info = $this->model_localisation_zone->getZone($order_info['shipping_zone_id']);

            $find = array(
                '{config_name}',
                '{config_telephone}',
                '{config_address}',
                '{config_geocode}',
                '{Consignee}',
                '{ConsigneeAddress1}',
                '{ConsigneeCity}',
                '{ConsigneeCountry}'
            );
            $replace = array(
                'config_name' => $store_info['config_name'],
                'config_telephone' => $store_info['config_telephone'],
                'config_address' => $store_info['config_address'],
                'config_geocode' => $store_info['config_geocode'],
                'Consignee' => 'Sayidaty Mall Fulfillment',
                'ConsigneeAddress1' => 'First Flight Ecom Team',
                'ConsigneeCity' => $zone_info['name'],
                'ConsigneeCountry' => $order_info['shipping_iso_code_2']
            );

            $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $this->load->model('tool/upload');

            $product_data = array();

            $option_data = array();
            foreach ($products_info as $product) {
                $options = $this->model_sale_shipping_process->getOrderOptions($order_id, $product['order_product_id']);

                foreach ($options as $option) {
                    if ($option['type'] != 'file') {
                        $value = $option['value'];
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $value = $upload_info['name'];
                        } else {
                            $value = '';
                        }
                    }

                    $option_data[] = array(
                        'name' => $option['name'],
                        'value' => $value
                    );
                }

                $product_data[] = array(
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'sku' => $product['sku'],
                    'option' => $option_data,
                    'quantity' => $product['quantity'],
                    'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'total' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
                );
                $voucher_data = array();

                $vouchers = $this->model_sale_shipping_process->getOrderVouchers($order_id);

                foreach ($vouchers as $voucher) {
                    $voucher_data[] = array(
                        'description' => $voucher['description'],
                        'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                    );
                }
            }

            $total_data = array();

            $totals = $this->model_sale_shipping_process->getOrderTotals($order_id);

            foreach ($totals as $total) {
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                );
            }

            $data['orders'][] = array(
                'order_id' => $order_id,
                'invoice_no' => $invoice_no,
                'date_added' => $order_info['date_added'],
                'store_name' => $order_info['store_name'],
                'store_url' => rtrim($order_info['store_url'], '/'),
                'store_email' => $store_email,
                'store_telephone' => $store_telephone,
                'email' => $order_info['email'],
                'telephone' => $order_info['telephone'],
                'shipping_address' => $shipping_address,
                'shipping_method' => $order_info['shipping_method'],
                'payment_address' => $payment_address,
                'payment_iso_code_2' => $order_info['payment_iso_code_2'],
                'count_order' => $order_count,
                'payment_method' => $order_info['payment_method'],
                'product' => $product_data,
                'voucher' => $voucher_data,
                'total' => $total_data,
                'comment' => nl2br($order_info['comment'])
            );
        }

        $this->response->setOutput($this->load->view('sale/shipping_product.tpl', $data));
    }

    public function country_invoice()
    {
        $this->load->language('sale/order');

        $data['title'] = $this->language->get('text_invoice');

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        if ($this->request->get['order_id']) {
            $order_id = $this->commonfunctions->getOrderNumber($this->request->get['order_id']);
        } else {
            $order_id = 0;
        }

        if ($this->request->get['country_id']) {
            $country_id = $this->request->get['country_id'];
        } else {
            $country_id = 0;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');
        $data['logo'] = HTTPS_IMAGE_S3 . $this->config->get('config_logo_ltr');


        $data['text_invoice'] = $this->language->get('text_invoice');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_website'] = $this->language->get('text_website');
        $data['text_payment_address'] = $this->language->get('text_payment_address');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_comment'] = $this->language->get('text_comment');
        $data['text_invoice_title'] = $this->language->get('text_invoice_title');
        $data['text_more_details'] = $this->language->get('text_more_details');

        $data['column_product'] = $this->language->get('column_product');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_sku'] = $this->language->get('column_sku');

        $this->load->model('sale/order');

        $this->load->model('setting/setting');

        $data['orders'] = array();

        $orders = array();

        if (isset($this->request->post['selected'])) {
            $orders = $this->request->post['selected'];
        } elseif (isset($this->request->get['order_id'])) {
            $orders[] = $order_id;
        }

        foreach ($orders as $order_id) {
            $order_info = $this->model_sale_order->getOrder($order_id);
            $order_countries = $this->model_sale_order->getOrderCountries($order_id);

            if ($order_info) {
                $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

                if ($store_info) {
                    $store_address = $store_info['config_address'];
                    $store_email = $store_info['config_email'];
                    $store_telephone = $store_info['config_telephone'];
                    $store_fax = $store_info['config_fax'];
                } else {
                    $store_address = $this->config->get('config_address');
                    $store_email = $this->config->get('config_email');
                    $store_telephone = $this->config->get('config_telephone');
                    $store_fax = $this->config->get('config_fax');
                }

                if ($order_info['invoice_no']) {
                    $data['invoice_no'] = $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                } else {
                    $data['invoice_no'] = $invoice_no = '';
                }

                if(trim($order_info['payment_method'])=='الدفع عند التسليم'){
          				$order_info['payment_method']= 'Cash On Delivery';
          			}elseif (trim($order_info['payment_method'])== 'بطاقة ائتمان') {
          				$order_info['payment_method']=  'Credit / Debit Card';
          			}
                if(trim($order_info['shipping_method'])=='رسوم التوصيل')
          				$order_info['shipping_method']= 'Flat Shipping Rate';

                $data['payment_method'] = $order_info['payment_method'];
                $data['shipping_method'] = $order_info['shipping_method'];

                if ($order_info['payment_address_format']) {
                    $format = $order_info['payment_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['payment_firstname'],
                    'lastname' => $order_info['payment_lastname'],
                    'company' => $order_info['payment_company'],
                    'address_1' => $order_info['payment_address_1'],
                    'address_2' => $order_info['payment_address_2'],
                    'city' => $order_info['payment_city'],
                    'zone' => $order_info['payment_zone'],
                    'zone_code' => $order_info['payment_zone_code'],
                    'country' => $order_info['payment_country']
                );

                $payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                if ($order_info['shipping_address_format']) {
                    $format = $order_info['shipping_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['shipping_firstname'],
                    'lastname' => $order_info['shipping_lastname'],
                    'company' => $order_info['shipping_company'],
                    'address_1' => $order_info['shipping_address_1'],
                    'address_2' => $order_info['shipping_address_2'],
                    'city' => $order_info['shipping_city'],
                    'zone' => $order_info['shipping_zone'],
                    'zone_code' => $order_info['shipping_zone_code'],
                    'country' => $order_info['shipping_country']
                );

                $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                $this->load->model('tool/upload');

                $product_data = array();

                $products = $this->model_sale_order->getOrderProductsByCountry($order_id, $country_id);
                $sub_total_country_price = 0;
                foreach ($products as $product) {
                    $option_data = array();
                    $sub_total_country_price += $product['total'];
                    $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

                    foreach ($options as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['value'];
                        } else {
                            $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                            if ($upload_info) {
                                $value = $upload_info['name'];
                            } else {
                                $value = '';
                            }
                        }

                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $value
                        );
                    }

                    $product_data[] = array(
                        'name' => $product['name'],
                        'model' => $product['model'],
                        'sku' => $product['sku'],
                        'option' => $option_data,
                        'quantity' => $product['quantity'],
                        'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                        'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
                    );
                }

                $voucher_data = array();

                $vouchers = $this->model_sale_order->getOrderVouchers($order_id);

                foreach ($vouchers as $voucher) {
                    $voucher_data[] = array(
                        'description' => $voucher['description'],
                        'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                    );
                }

                $total_data = array();

                $totals = $this->model_sale_order->getOrderTotals($order_id);
                $original_sub_total = 1;
                $total_value = 0;


                foreach ($totals as $total) {
                    if ($total['code'] == 'sub_total') {
                        $total_data[] = array(
                            'title' => $this->language->get('sub_total'),
                            'text' => $this->currency->format($sub_total_country_price, $order_info['currency_code'], $order_info['currency_value']),
                        );
                        $original_sub_total = $total['value'];
                        $total_value += $sub_total_country_price;
                    } elseif ($total['code'] == 'shipping') {
                        $total_data[] = array(
                            'title' => $this->language->get('shipping'),
                            'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                        );
                        $total_value += $total['value'] / $order_countries;
                    } elseif ($total['code'] == 'coupon') {
                        $total_data[] = array(
                            'title' => str_replace( 'قسيمة التخفيض',  'Coupon',$total['title']),
                            'text' => $this->currency->format($total['value'] * ($sub_total_country_price / $original_sub_total), $order_info['currency_code'], $order_info['currency_value']),
                        );
                        $total_value += $total['value'] * ($sub_total_country_price / $original_sub_total);
                    } elseif ($total['code'] == 'total') {
                        $vat_data = $this->db->query("SELECT vat_info FROM `order` WHERE order_id = $order_id");
                        $vat_info = array();
                        if ($vat_data->num_rows)
                            $vat_info = unserialize($vat_data->row['vat_info']);
                        if (isset($vat_info['vat_value']) && $vat_info['vat_value'] > 0) {
                            $total_data[] = array(
                                'title' => 'VAT '.$vat_info['vat_value'].'%',
                                'text' => $this->currency->format($vat_info['vat_amount'], $order_info['currency_code'], $order_info['currency_value']),
                            );
                            $total_value += ($vat_info['vat_value']/100)*$total_value;
                        }
                        $total_data[] = array(
                            'title' => $this->language->get('total'),
                            'text' => $this->currency->format($total_value, $order_info['currency_code'], $order_info['currency_value']),
                        );
                    } elseif ($total['code'] == 'codfees') {
                        $total_data[] = array(
                            'title' => $this->language->get('codfees'),
                            'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                        );
                        $total_value += $total['value'] / $order_countries;
                    }  else {
                        $total_data[] = array(
                            'title' => $total['title'],
                            'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                        );
                        $total_value += $total['value'] / $order_countries;
                    }
                }

                $data['orders'][] = array(
                    'order_id' => $this->request->get['order_id'],
                    'invoice_no' => $invoice_no,
                    'date_added' => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                    'store_name' => $order_info['store_name'],
                    'store_url' => rtrim($order_info['store_url'], '/'),
                    'store_address' => nl2br($store_address),
                    'store_email' => $store_email,
                    'store_telephone' => $store_telephone,
                    'store_fax' => $store_fax,
                    'email' => $order_info['email'],
                    'telephone' => $order_info['telephone'],
                    'shipping_address' => $order_info['telephone']."<br />".$shipping_address,
                    'shipping_method' => $order_info['shipping_method'],
                    'payment_address' => $payment_address,
                    'payment_method' => $order_info['payment_method'],
                    'product' => $product_data,
                    'voucher' => $voucher_data,
                    'total' => $total_data,
                    'comment' => nl2br($order_info['comment'])
                );
            }
        }

        $this->response->setOutput($this->load->view('sale/country_order_invoice.tpl', $data));
    }

    public function rma_invoice()
    {
        $this->load->language('sale/order');

        $data['title'] = $this->language->get('text_invoice');

        if ($this->request->get['rma_id']) {
            $rma_id = $this->request->get['rma_id'];
        } else {
            $rma_id = 0;
        }

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');
        $data['logo'] = HTTPS_IMAGE_S3 . $this->config->get('config_logo_ltr');


        $data['text_invoice'] = $this->language->get('text_invoice');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_return_no'] = $this->language->get('text_return_no');
        $data['text_return_details'] = $this->language->get('text_rma_details');
        $data['text_customer'] = $this->language->get('text_customer');
        $data['text_return_status'] = $this->language->get('text_return_status');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_website'] = $this->language->get('text_website');
        $data['text_payment_address'] = $this->language->get('text_payment_address');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_comment'] = $this->language->get('text_comment');
        $data['text_invoice_title'] = $this->language->get('text_invoice_title');
        $data['text_more_details'] = $this->language->get('text_more_details');

        $data['column_product'] = $this->language->get('column_product');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_sku'] = $this->language->get('column_sku');

        $this->load->model('sale/order');
        $this->load->model('setting/setting');
        $this->load->model('sale/shipping_process');
        $this->load->model('catalog/wk_rma_admin');

        $data['orders'] = array();

        $return_info = $this->model_sale_shipping_process->getReturn($rma_id);
        $data['is_closed'] = $return_info['is_closed'];
        $data['rma_id'] = $rma_id;

        $result = $this->model_catalog_wk_rma_admin->getRmaOrderid($rma_id);
        $customerDetails = $this->model_catalog_wk_rma_admin->viewCustomerDetails($result['order_id']);

        $rma_sta = array(
            '0' => 'Pending',
            '1' => 'Accept',
            '2' => 'Shipped',
            '3' => 'Decline',
            '4' => 'Return',
            '5' => 'Complete',
            '6' => 'Returned',
        );
        $rma_status = $rma_sta[$result['customer_st']];

        $rma_customer_name = '';
        if (!empty($customerDetails)) {
            $rma_customer_name = $customerDetails['firstname'] . ' ' . $customerDetails['lastname'];
        }

        $data['rma_details'] = array(
            'customer_name' => $rma_customer_name,
            'return_status' => $rma_status,
            'return_id' => $rma_id,
            'order_id' => $result['order_id']
        );


        $seller_data = $this->model_sale_shipping_process->getSeller($return_info['customer_id']);
        $order_id = $return_info['order_id'];
        $order_info = $this->model_sale_shipping_process->getOrder($return_info['order_id']);
        $order_count = $this->model_sale_shipping_process->getOrderCount($return_info['order_id']);
        $order_countries = $this->model_sale_order->getOrderCountries($order_id);

        if ($order_info) {
            $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

            if ($store_info) {
                $store_name = $store_info['config_name'];
                $store_company = $store_info['config_owner'];
                $store_address = $store_info['config_address'];
                $store_email = $store_info['config_email'];
                $store_telephone = $store_info['config_telephone'];
                $store_fax = $store_info['config_fax'];
            } else {
                $store_address = $this->config->get('config_address');
                $store_email = $this->config->get('config_email');
                $store_telephone = $this->config->get('config_telephone');
                $store_fax = $this->config->get('config_fax');
            }

            if ($order_info['invoice_no']) {
                $data['invoice_no'] = $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
            } else {
                $data['invoice_no'] = $invoice_no = '';
            }

            $data['payment_method'] = $order_info['payment_method'];
            $data['shipping_method'] = $order_info['shipping_method'];


            $format = '{screenname} {companyname}' . "\n" . '{mobile_number}' . "\n" . '{zone_name}' . "\n" . '{country_name}' . "\n" . '{address}' . "\n" . '{opening_time}';

            $find = array(
                '{screenname}',
                '{companyname}',
                '{mobile_number}',
                '{zone_name}',
                '{country_name}',
                '{address}',
                '{opening_time}'
            );

            $replace = array(
                '{screenname}' => $seller_data['screenname'],
                '{companyname}' => $seller_data['companyname'],
                '{mobile_number}' => $seller_data['telephone'],
                '{zone_name}' => $seller_data['zone_name'],
                '{country_name}' => $seller_data['country_name'],
                '{address}' => $seller_data['warehouse_address'],
                '{opening_time} ' => $seller_data['opening_time']
            );

            $store_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{mobile_number}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{mobile_number}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'mobile_number' => $order_info['telephone'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['shipping_address_2'],
                'city' => $order_info['shipping_city'],
                'zone' => $order_info['shipping_zone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country']
            );

            $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $this->load->model('tool/upload');

            $product_data = array();

            $product = $this->model_sale_shipping_process->getOrderProduct($order_id, $return_info['product_id']);

            $sub_total_country_price = 0;
            $option_data = array();
            $sub_total_country_price += $product['total'];
            $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

            foreach ($options as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name' => $option['name'],
                    'value' => $value
                );
            }

            $product_data[] = array(
                'name' => $product['name'],
                'model' => $product['model'],
                'sku' => $product['sku'],
                'option' => $option_data,
                'quantity' => $product['quantity'],
                'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
            );

            $voucher_data = array();

            $vouchers = $this->model_sale_order->getOrderVouchers($order_id);

            foreach ($vouchers as $voucher) {
                $voucher_data[] = array(
                    'description' => $voucher['description'],
                    'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                );
            }

            $total_data = array();

            $totals = $this->model_sale_order->getOrderTotals($order_id);
            $original_sub_total = 1;
            $total_value = 0;


            foreach ($totals as $total) {
                if ($total['code'] == 'sub_total') {
                    $total_data[] = array(
                        'title' => $total['title'],
                        'text' => $this->currency->format($sub_total_country_price, $order_info['currency_code'], $order_info['currency_value']),
                    );
                    $original_sub_total = $total['value'];
                    $total_value += $sub_total_country_price;
                } elseif ($total['code'] == 'shipping') {
                    $total_data[] = array(
                        'title' => $total['title'],
                        'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                    );
                    $total_value += $total['value'] / $order_countries;
                } elseif ($total['code'] == 'coupon') {
                    $total_data[] = array(
                        'title' => $total['title'],
                        'text' => $this->currency->format($total['value'] * ($sub_total_country_price / $original_sub_total), $order_info['currency_code'], $order_info['currency_value']),
                    );
                    $total_value += $total['value'] * ($sub_total_country_price / $original_sub_total);
                } elseif ($total['code'] == 'total') {
                    $total_data[] = array(
                        'title' => $total['title'],
                        'text' => $this->currency->format($total_value, $order_info['currency_code'], $order_info['currency_value']),
                    );
                } else {
                    $total_data[] = array(
                        'title' => $total['title'],
                        'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                    );
                    $total_value += $total['value'] / $order_countries;
                }
            }

            $data['orders'][] = array(
                'order_id' => $order_id,
                'invoice_no' => $invoice_no,
                'date_added' => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                'store_name' => $order_info['store_name'],
                'store_url' => rtrim($order_info['store_url'], '/'),
                'store_address' => nl2br($store_address),
                'store_email' => $store_email,
                'store_telephone' => $store_telephone,
                'store_fax' => $store_fax,
                'email' => $order_info['email'],
                'telephone' => $order_info['telephone'],
                'shipping_address' => $shipping_address,
                'shipping_method' => $order_info['shipping_method'],
                'store_address' => $store_address,
                'payment_method' => $order_info['payment_method'],
                'product' => $product_data,
                'voucher' => $voucher_data,
                'total' => $total_data,
                'comment' => nl2br($order_info['comment'])
            );
        }


        $this->response->setOutput($this->load->view('sale/return_invoice.tpl', $data));
    }

    public function close_order_country()
    {
        if ($this->request->get['order_id']) {
            $order_id = $this->commonfunctions->getOrderNumber($this->request->get['order_id']);
        } else {
            return false;
        }

        if ($this->request->get['country_id']) {
            $country_id = $this->request->get['country_id'];
        } else {
            return false;
        }

        $this->load->model('sale/order');
        $this->load->model('customerpartner/shipping_order');

        // Sending a shipping request to the shipping company
        $order_info = $this->model_sale_order->getOrder($order_id);
        $total_array = $this->getOrderTotalsByCountry($order_info, $country_id);
        $order_categories = $this->getOrderProductsCategories($order_info, $country_id);

        $total_order_by_country_value = 0;
        $ServiceType = 'NOR';
        if ($order_info['payment_code'] == 'cod') {
            $total_order_by_country_value = $total_array['total_value'];
            $ServiceType = 'NCND';
        }
        $total_order_by_country_value = $this->currency->format($total_order_by_country_value, $order_info['currency_code'], $order_info['currency_value']);
        $total_order_by_country_value = str_replace(',', '', $total_order_by_country_value);
        $total_order_by_country_value = (double)str_replace($order_info['currency_code'], '', $total_order_by_country_value);


        $total_price = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value']);
        $total_price = str_replace(',', '', $total_price);
        $total_price = (double)str_replace($order_info['currency_code'], '', $total_price);

        $destination_code = $this->model_sale_order->get_destination_code($order_info['shipping_zone_id']);

        $shipment_status = $this->model_sale_order->check_shipment($order_id, $country_id);

        $data = array(
            "order_id" => $order_info['order_id'],
            "prod_type" => "XPS",
            'country_id' => $country_id
        );

        if ( ! $shipment_status) {

            $countries = $this->getOrderCountries($order_id);


            //getting order information

            $operation_data = array();
            $operation_data['argAwbBooking']['Consignee'] = $order_info['firstname'] . " " . $order_info['lastname'];
            $operation_data['argAwbBooking']['ConsigneeCPerson'] = $order_info['firstname'] . " " . $order_info['lastname'];
            $operation_data['argAwbBooking']['ConsigneeAddress1'] = $order_info['shipping_address_1'];
            $operation_data['argAwbBooking']['ConsigneeAddress2'] = $order_info['shipping_address_2'];
            $operation_data['argAwbBooking']['ConsigneePhone'] = $order_info['telephone'];
            $operation_data['argAwbBooking']['ConsigneeCity'] = $order_info['shipping_city'];
            $operation_data['argAwbBooking']['ConsigneeCountry'] = $order_info['shipping_country'];
            $operation_data['argAwbBooking']['DestCode'] = $destination_code['destination_code'];
            $operation_data['argAwbBooking']['CountryCode'] = $order_info['shipping_country_id'];
            #$operation_data['argAwbBooking']['RetailZipCode'] = '';
            $operation_data['argAwbBooking']['Price'] = $total_price;
            $operation_data['argAwbBooking']['Weight'] = '0';
            $operation_data['argAwbBooking']['CustCash'] = $total_order_by_country_value;
            $operation_data['argAwbBooking']['ServiceType'] = $ServiceType;
            $operation_data['argAwbBooking']['Quantity'] = '1';
            $operation_data['argAwbBooking']['AgentCode'] = '';
            $operation_data['argAwbBooking']['AgentAWBNo'] = '';
            $operation_data['argAwbBooking']['ShipperRefNo'] = $this->commonfunctions->convertOrderNumber($order_info['order_id']);
            $operation_data['argAwbBooking']['GoodsDescription'] = $order_categories;
            $operation_data['argAwbBooking']['SpecialInstruction'] = '';
            $operation_data['argAwbBooking']['ValueCurrency'] = $order_info['currency_code'];
            $operation_data['argAwbBooking']['ValueOfShipment'] = $total_order_by_country_value;
            $operation_data['argAwbBooking']['ProductType'] = $data['prod_type'];
            $operation_data['argAwbBooking']['DOX'] = '';
            $operation_data['argAwbBooking']['ConsigneeMob'] = '';
            $operation_data['argAwbBooking']['Dimension'] = '';
            $operation_data['argAwbBooking']['ChargeableWeight'] = '';

            if($_GET['shipping_company'] == '2') {


		if ($order_info['payment_code'] != 'cod') {
            		$total_order_by_country_value = 0;
		}

                $book = $this->db->query("SELECT * FROM order_booking where order_id = '{$order_id}' AND country_id='{$country_id}' ");
                //$country_info = $this->getCountryINFO($order_info['shipping_country_id']);
                $country_info = $this->db->query("SELECT name, iso_code_2, telecode FROM country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE cd.language_id = 1 AND c.country_id = '" . (int)$order_info['shipping_country_id'] . "' ");
                $country_info = $country_info->row;

                $quantity = $this->db->query("select sum(quantity) quantity from customerpartner_to_order cto inner join customerpartner_to_customer ctc ON (ctc.customer_id = cto.customer_id) WHERE order_product_status = 18 AND order_id = '".(int)$order_id."' AND country_id = '".(int)$order_info['shipping_country_id']."'  AND cto.deleted='0'  ");
                $quantity =  $quantity->row['quantity'];
                if($book && $book->row){
                    $booking_ref =  $this->db->query("select GROUP_CONCAT(AWBNO) book_ref from order_booking where order_id = '$order_id' AND country_id = '$country_id';");

                    $bookData  = unserialize($book->row['sent_data']);
                    $result = array_merge($bookData,$bookData['real_consigne']);
                    $result['cod_amount'] = $total_order_by_country_value;
                    $result['RefNo'] = $country_info['iso_code_2'].'-'.$this->commonfunctions->convertOrderNumber($order_id).'-'.$quantity;
                    $result['GoodDesc'] = $booking_ref->row['book_ref'];
		    $this->NaqelServices = new Naqel($this->registry, SHIPPING_NAQEL_ID);
                    $AWB = $this->NaqelServices->BookingRequest($result);
                    if ($AWB) {
                     $data['AWBNO'] =  $AWB['BookAWB'];
			echo json_encode(['awbno' => $data['AWBNO']]);
                     $this->model_sale_order->add_shipment($data);
                    }else{
                     print_r($AWB);
                     echo 'error missing data';
                     return false;
                    }

                }

            }else if ($_GET['shipping_company'] == '3'  OR $_GET['shipping_company'] == '4' ) {


                $data['AWBNO'] = $this->model_sale_order->get_awbno($data['order_id']);
                $this->model_sale_order->add_shipment($data);
            } else {
              $this->FFServices = new FirstFlight($this->registry, SHIPPING_FIRST_FLIGHT_ID);
              $response = $this->FFServices->NewOrderWithAutoAWBNO($operation_data);
              if(empty($response['error']) && $response['error'] != ''){
                $data['AWBNO'] = $response;
                echo json_encode(['awbno' => $data['AWBNO']]);
                $this->model_sale_order->add_shipment($data);
              }else{
                $data['error'] = $response['error'];
                $this->model_sale_order->add_shipment_error($data);
              }
            }
        }

       $this->model_customerpartner_shipping_order->close_order_country($order_id, $country_id);

        return true;
    }

    public function getOrderTotalsByCountry($order_info, $country_id)
    {

        $this->load->model("sale/order");
        $product_data = array();
        $order_id = $order_info['order_id'];
        $order_countries = $this->model_sale_order->getOrderCountries($order_id);

        $products = $this->model_sale_order->getOrderProductsByCountry($order_id, $country_id);
        $sub_total_country_price = 0;
        foreach ($products as $product) {
            $sub_total_country_price += $product['total'];
        }

        $total_data = array();

        $totals = $this->model_sale_order->getOrderTotals($order_id);
        $original_sub_total = 1;
        $total_value = 0;


        foreach ($totals as $total) {
            if ($total['code'] == 'sub_total') {
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($sub_total_country_price, $order_info['currency_code'], $order_info['currency_value']),
                );
                $original_sub_total = $total['value'];
                $total_value += $sub_total_country_price;
            } elseif ($total['code'] == 'shipping') {
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                );
                $total_value += $total['value'] / $order_countries;
            } elseif ($total['code'] == 'coupon') {
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'] * ($sub_total_country_price / $original_sub_total), $order_info['currency_code'], $order_info['currency_value']),
                );
                $total_value += $total['value'] * ($sub_total_country_price / $original_sub_total);
            } elseif ($total['code'] == 'total') {
                $vat_data = $this->db->query("SELECT vat_info FROM `order` WHERE order_id = $order_id");
                $vat_info = array();
                if ($vat_data->num_rows)
                    $vat_info = unserialize($vat_data->row['vat_info']);
                if (isset($vat_info['vat_value']) && $vat_info['vat_value'] > 0) {
                    $total_data[] = array(
                        'title' => 'VAT '.$vat_info['vat_value'].'%',
                        'text' => $this->currency->format($vat_info['vat_amount'], $order_info['currency_code'], $order_info['currency_value']),
                    );
                    $total_value += ($vat_info['vat_value']/100)*$total_value;
                }
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total_value, $order_info['currency_code'], $order_info['currency_value']),
                );
            } else {
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                );
                $total_value += $total['value'] / $order_countries;
            }
        }
        $total_data['total_value'] = $total_value;
        return $total_data;
    }

    private function getOrderCountries($order_id)
    {

        $countries = array();

        $result = $this->db->query("SELECT cp.country_id FROM customerpartner_to_order cp2o LEFT JOIN customerpartner_to_customer cp ON(cp2o.customer_id=cp.customer_id) WHERE cp2o.order_id='" . (int)$order_id . "'  AND cp2o.deleted='0' AND cp2o.order_product_status NOT IN (7)");

        foreach ($result->rows as $row) {
            $countries[$row['country_id']] = $row['country_id'];
        }

        return $countries;
    }

    public function close_return()
    {

        if ($this->request->get['rma_id']) {
            $rma_id = $this->request->get['rma_id'];
        } else {
            $rma_id = 0;
        }

        if ($this->request->get['order_id']) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }


        $this->load->model('sale/order');
        $this->load->model('customerpartner/shipping_order');
        $this->model_customerpartner_shipping_order->close_return($rma_id);


        $rmaInfo = $this->model_sale_order->getReturnBookin($rma_id);
        $order_info = $this->model_sale_order->getOrder($order_id);

        if ($rmaInfo['shipping_company'] == 1) {

            // Sending a shipping request to the shipping company

            $destination_code = $this->model_sale_order->get_destination_code($order_info['shipping_zone_id']);

            $shipment_status = $this->model_sale_order->check_return_shipment($rma_id);
            $data = array(
                "order_id" => $order_info['order_id'],
                "prod_type" => "DOX",
                'return_id' => $rma_id
            );

            if (!$shipment_status) {
                //getting order information

                $operation_data = array();
                $operation_data['argAwbBooking']['Consignee'] = $order_info['customer'];
                $operation_data['argAwbBooking']['ConsigneeAddress1'] = $order_info['shipping_address_1'];
                $operation_data['argAwbBooking']['ConsigneeAddress2'] = $order_info['shipping_address_2'];
                $operation_data['argAwbBooking']['ConsigneePhone'] = $order_info['telephone'];
                $operation_data['argAwbBooking']['ConsigneeCity'] = $order_info['shipping_city'];
                $operation_data['argAwbBooking']['ConsigneeCountry'] = $order_info['shipping_country'];
                $operation_data['argAwbBooking']['DestCode'] = $destination_code['destination_code'];
                $operation_data['argAwbBooking']['CountryCode'] = $order_info['shipping_country_id'];
                #$operation_data['argAwbBooking']['RetailZipCode'] = '';
                $operation_data['argAwbBooking']['Price'] = $order_info['total'];
                $operation_data['argAwbBooking']['Weight'] = '0';
                $operation_data['argAwbBooking']['CustCash'] = '';
                $operation_data['argAwbBooking']['ServiceType'] = '';
                $operation_data['argAwbBooking']['Quantity'] = '';
                $operation_data['argAwbBooking']['AgentCode'] = '';
                $operation_data['argAwbBooking']['AgentAWBNo'] = '';
                $operation_data['argAwbBooking']['ShipperRefNo'] = '';
                $operation_data['argAwbBooking']['GoodsDescription'] = '';
                $operation_data['argAwbBooking']['SpecialInstruction'] = '';
                $operation_data['argAwbBooking']['ValueCurrency'] = '';
                $operation_data['argAwbBooking']['ValueOfShipment'] = '';
                $operation_data['argAwbBooking']['ProductType'] = $data['prod_type'];
                $operation_data['argAwbBooking']['DOX'] = '';
                $operation_data['argAwbBooking']['ConsigneeMob'] = '';
                $operation_data['argAwbBooking']['Dimension'] = '';
                $operation_data['argAwbBooking']['ChargeableWeight'] = '';

                ///
                $response = $this->FFServices->NewOrderWithAutoAWBNO($operation_data);
                $data['AWBNO'] = $response;
               // $data['return_id'] = $rma_id;
                ///
                $this->model_sale_order->add_return_shipment($data);
            }

        }else if($rmaInfo['shipping_company'] == 2 || $rmaInfo['shipping_company'] == 3 || $rmaInfo['shipping_company'] == 4){
             $data['AWBNO'] = $rmaInfo['AWBNO'];
            // $data['return_id'] = $rma_id;
            $data = array(
                "order_id" => $order_info['order_id'],
                "prod_type" => "DOX",
                'return_id' => $rma_id
            );
            $this->model_sale_order->add_return_shipment($data);

        }
        return true;
    }

    private function validate_createbooking($data = array())
    {

        $this->load->model("sale/order");
        $product_data = array();
        $order_id = $order_info['order_id'];
        $order_countries = $this->model_sale_order->getOrderCountries($order_id);

        $products = $this->model_sale_order->getOrderProductsByCountry($order_id, $country_id);
        $sub_total_country_price = 0;
        foreach ($products as $product) {
            $sub_total_country_price += $product['total'];
        }

        $total_data = array();

        $totals = $this->model_sale_order->getOrderTotals($order_id);
        $original_sub_total = 1;
        $total_value = 0;


        foreach ($totals as $total) {
            if ($total['code'] == 'sub_total') {
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($sub_total_country_price, $order_info['currency_code'], $order_info['currency_value']),
                );
                $original_sub_total = $total['value'];
                $total_value += $sub_total_country_price;
            } elseif ($total['code'] == 'shipping') {
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                );
                $total_value += $total['value'] / $order_countries;
            } elseif ($total['code'] == 'coupon') {
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'] * ($sub_total_country_price / $original_sub_total), $order_info['currency_code'], $order_info['currency_value']),
                );
                $total_value += $total['value'] * ($sub_total_country_price / $original_sub_total);
            } elseif ($total['code'] == 'total') {
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total_value, $order_info['currency_code'], $order_info['currency_value']),
                );
            } else {
                $total_data[] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                );
                $total_value += $total['value'] / $order_countries;
            }
        }
        $total_data['total_value'] = $total_value;
        return $total_data;
    }

    public function getOrderProductsCategories($order_info, $country_id)
    {
        $this->load->model("sale/order");
        $product_categories = '';
        $order_id = $order_info['order_id'];

        $products = $this->model_sale_order->getOrderProductsByCountry($order_id, $country_id);
        foreach ($products as $product) {
            foreach ($this->model_sale_order->getProductCategories($product['product_id']) as $num => $cat) {
                $product_categories .= $cat['name'] . " ";
            }
        }

        return $product_categories;
    }

}
