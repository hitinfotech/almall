<?php

class ControllerCommonFileuploader extends Controller {

    public function index() {
        echo " welcome in files upload ";
    }

    public function upload() {

        $field = 'upload';
        $height = 600;
        $width = 600;

        if (!isset($this->request->files[$field]) || is_array($this->request->files[$field]['name'])) {
            return FALSE;
        }

        $tempname = basename(html_entity_decode($this->request->files[$field]['name'], ENT_QUOTES, 'UTF-8'));

        // Allowed file extension types
        $allowed = array(
            'jpg',
            'jpeg',
            'gif',
            'png'
        );

        if (!in_array(utf8_strtolower(utf8_substr(strrchr($tempname, '.'), 1)), $allowed)) {
            die("extension {$tempname}");
            return FALSE;
        }

        // Allowed file mime types
        $allowed_mime = array(
            'image/jpeg',
            'image/pjpeg',
            'image/png',
            'image/x-png',
            'image/gif'
        );

        if (!in_array($this->request->files[$field]['type'], $allowed_mime)) {
            die("mime {$tempname} ");
            return FALSE;
        }


        ini_set('memory_limit', '2M');   //  handle large images

        $path = 'upload/' . date('Y_m_d_h_i_s') . '/';
        $this->create_path(DIR_IMAGE . $path);
        $filename = time() . '.' . utf8_strtolower(utf8_substr(strrchr($tempname, '.'), 1));

        try {
            move_uploaded_file($this->request->files[$field]['tmp_name'], DIR_IMAGE . $path . 'tmp' . $filename);

            $im = new imagick(DIR_IMAGE . $path . 'tmp' . $filename);
            $imageprops = $im->getImageGeometry();
            $old_width = (int) $imageprops['width'];
            $old_height = (int) $imageprops['height'];
            if ($old_width > $old_height) {
                $newHeight = (int) ($height);
                $newWidth = (int) ($height / $old_height * $old_width);
            } else {
                $newWidth = (int) ($width);
                $newHeight = (int) (($width / $old_width) * $old_height);
            }
            
            $im->resizeimage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1, true);
            $im->cropImage($newWidth, $newHeight, 0, 0);
            $im->writeImage(DIR_IMAGE . $path . $filename);
            unlink(DIR_IMAGE . $path . 'tmp' . $filename);

            //$this->aws_s3->s3()->putObjectFile(DIR_IMAGE . $path . $filename, S3_BUKET_NAME, $path . $filename);
            $this->db->query("INSERT INTO image_s3 SET image='" . $this->db->escape($path . $filename) . "' ,new_image_path='" . $this->db->escape($path . $filename) . "' , date_added=NOW(), user_id='" . (int) $this->user->getid() . "' ");
                    
            echo HTTPS_IMAGE_S3 . $path . $filename;
            //echo  HTTPS_CATALOG . "image/" . $path . $filename."<br/>";
            echo "<img width='100px'src='" . HTTPS_IMAGE_S3 . $path . $filename . "' alt='" . $filename . "' name='" . $filename . "'>";
            return $path . $filename;
        } catch (Exception $error) {
            return FALSE;
        }
    }

    private function create_path($dir_path) {

        $path = rtrim($dir_path, '/');

        $directories = explode("/", $path);

        $string = '';

        foreach ($directories as $directory) {
            $string .= "/{$directory}";
            if (!is_dir($string)) {
                @mkdir($string, 0777);
            }
        }
    }

}
