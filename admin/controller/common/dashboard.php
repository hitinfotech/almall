<?php

class ControllerCommonDashboard extends Controller {

    public function index() {
        $this->load->language('common/dashboard');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_sale'] = $this->language->get('text_sale');
        $data['text_map'] = $this->language->get('text_map');
        $data['text_activity'] = $this->language->get('text_activity');
        $data['text_recent'] = $this->language->get('text_recent');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        // Check install directory exists
        if (is_dir(dirname(DIR_APPLICATION) . '/install')) {
            $data['error_install'] = $this->language->get('error_install');
        } else {
            $data['error_install'] = '';
        }

        $data['token'] = $this->session->data['token'];

        $data['header'] = $this->load->controller('common/header');
        /* AbandonedCarts - Begin */
        $this->load->model('setting/setting');
        $abandonedCartsSettings = $this->model_setting_setting->getSetting('abandonedcarts', $this->config->get('store_id'));
        if (isset($abandonedCartsSettings['abandonedcarts']['Enabled']) && $abandonedCartsSettings['abandonedcarts']['Enabled'] == 'yes' && isset($abandonedCartsSettings['abandonedcarts']['DashboardWidget']) && $abandonedCartsSettings['abandonedcarts']['DashboardWidget'] == 'yes') {
            $data['show_ac'] = true;
        } else {
            $data['show_ac'] = false;
        }
        $data['abandonedcarts'] = $this->load->controller('dashboard/abandonedcarts');
        /* AbandonedCarts - End */


        $data['column_left'] = $this->load->controller('common/column_left');
        $data['order'] = $this->load->controller('dashboard/order');
        $data['sale'] = $this->load->controller('dashboard/sale');
        $data['customer'] = $this->load->controller('dashboard/customer');
        $data['online'] = $this->load->controller('dashboard/online');
        $data['map'] = $this->load->controller('dashboard/map');
        $data['chart'] = $this->load->controller('dashboard/chart');
        $data['activity'] = $this->load->controller('dashboard/activity');
        $data['recent'] = $this->load->controller('dashboard/recent');
        $data['footer'] = $this->load->controller('common/footer');

        // Run currency update
        if ($this->config->get('config_currency_auto')) {
            $this->load->model('localisation/currency');

            $this->model_localisation_currency->refresh();
        }

        $this->response->setOutput($this->load->view('common/dashboard.tpl', $data));
    }

}
