<?php

class ControllerCommonMenu extends Controller {

    public function index() {

        global $registry; $userac = $registry->get('user');
        $data['userac'] = $userac;

        /* Start Newsletter Enhancement */
        if ($this->config->get('ne_key')) {
            $this->load->language('module/ne');
            $data['text_ne_status'] = $this->language->get('text_ne_status');
            $data['text_ne_email'] = $this->language->get('text_ne_email');
            $data['text_ne_draft'] = $this->language->get('text_ne_draft');
            $data['text_ne_marketing'] = $this->language->get('text_ne_marketing');
            $data['text_ne_subscribers'] = $this->language->get('text_ne_subscribers');
            $data['text_ne_stats'] = $this->language->get('text_ne_stats');
            $data['text_ne_robot'] = $this->language->get('text_ne_robot');
            $data['text_ne_template'] = $this->language->get('text_ne_template');
            $data['text_ne_subscribe_box'] = $this->language->get('text_ne_subscribe_box');
            $data['text_ne_blacklist'] = $this->language->get('text_ne_blacklist');
            $data['text_ne_support'] = $this->language->get('text_ne_support');
            $data['text_ne_support_register'] = $this->language->get('text_ne_support_register');
            $data['text_ne_support_login'] = $this->language->get('text_ne_support_login');
            $data['text_ne_support_dashboard'] = $this->language->get('text_ne_support_dashboard');
            $data['text_ne'] = $this->language->get('text_ne');
            $data['text_ne_update_check'] = $this->language->get('text_ne_update_check');
            $data['ne_email'] = $this->url->link('ne/newsletter', '', 'SSL');
            $data['ne_status'] = $this->url->link('ne/status', '', 'SSL');

            $data['ne_draft'] = $this->url->link('ne/draft', '', 'SSL');
            $data['ne_marketing'] = $this->url->link('ne/marketing', '', 'SSL');
            $data['ne_subscribers'] = $this->url->link('ne/subscribers', '', 'SSL');
            $data['ne_stats'] = $this->url->link('ne/stats', '', 'SSL');
            $data['ne_robot'] = $this->url->link('ne/schedule', '', 'SSL');
            $data['ne_template'] = $this->url->link('ne/template', '', 'SSL');
            $data['ne_subscribe_box'] = $this->url->link('ne/subscribe_box', '', 'SSL');
            $data['ne_blacklist'] = $this->url->link('ne/blacklist', '', 'SSL');
            $data['ne_update_check'] = $this->url->link('ne/check_update', '', 'SSL');
        }
        /* END Newsletter Enhancement */
        /* AbandonedCarts - Begin */
        $this->load->model('setting/setting');

        $abandonedCartsSettings = $this->model_setting_setting->getSetting('abandonedcarts', $this->config->get('store_id'));

        if (isset($abandonedCartsSettings['abandonedcarts']['Enabled']) && $abandonedCartsSettings['abandonedcarts']['Enabled'] == 'yes' && isset($abandonedCartsSettings['abandonedcarts']['MenuWidget']) && $abandonedCartsSettings['abandonedcarts']['MenuWidget'] == 'yes') {
            $AB_count = $this->db->query("SELECT count(*) as total FROM `" . DB_PREFIX . "abandonedcarts` WHERE `notified`=0");
            $data['text_abandonedCarts'] = 'Abandoned Carts <span class="label label-danger">' . $AB_count->row['total'] . '</span>';
            $data['link_abandonedCarts'] = $this->url->link('module/abandonedcarts', '', 'SSL');
        }
        /* AbandonedCarts - End */

        $abandonmentCartsSettings = $this->model_setting_setting->getSetting('abandonmentcarts', $this->config->get('store_id'));

        if (isset($abandonmentCartsSettings['abandonmentcarts']['Enabled']) && $abandonmentCartsSettings['abandonmentcarts']['Enabled'] == 'yes' && isset($abandonmentCartsSettings['abandonmentcarts']['MenuWidget']) && $abandonmentCartsSettings['abandonmentcarts']['MenuWidget'] == 'yes') {
            $AB_count = $this->db->query("SELECT count(*) as total FROM `" . DB_PREFIX . "abandonmentcarts` WHERE `notified`=0");
            $data['text_abandonmentCarts'] = 'Browsing Abandontments <span class="label label-danger">' . $AB_count->row['total'] . '</span>';
            $data['link_abandonmentCarts'] = $this->url->link('module/abandonmentcarts', '', 'SSL');
        }


        $this->load->model('shipping/shipping');
        $shipping_companies = $this->model_shipping_shipping->getShippingCompanies();
        $ship_company = $this->user->getShippingCompany();
        $data['shipping_companies'] = array();
        switch ($ship_company) {
            case -1:
                $data['shipping_companies'] = array();
                break;
            case 0:
                foreach ($shipping_companies as $company) {
                    $data['shipping_companies'][] = $company;
                }
                break;
            case 1:
                foreach ($shipping_companies as $company){
                    if ($company['ship_company_id'] == 1){
                        $data['shipping_companies'][] = $company;
                    }
                }
                break;
            case 2:
                foreach ($shipping_companies as $company){
                    if ($company['ship_company_id'] == 2){
                        $data['shipping_companies'][] = $company;
                    }
                }
                break;
            case 3:
                foreach ($shipping_companies as $company){
                    if ($company['ship_company_id'] == 3){
                        $data['shipping_companies'][] = $company;
                    }
                }
                break;
            case 4:
                foreach ($shipping_companies as $company){
                    if ($company['ship_company_id'] == 4){
                        $data['shipping_companies'][] = $company;
                    }
                }
                break;
        }

        $data['text_barcode'] = 'Barcode ';
        $data['link_barcode'] = $this->url->link('sale/shipping_process', '', 'SSL');

        $this->load->language('common/menu');
        $data['text_shipping_process'] = $this->language->get('text_shipping_process'); //'Barcode ';
        $data['link_shipping_process'] = $this->url->link('sale/shipping_process', '', 'SSL');

        $data['AutomatedNewsletterURL'] = $this->url->link('module/automatednewsletter', '', 'SSL');
        //excel export - start
        $this->load->language('tool/excel_export_orders');
        $data['text_excel_export_orders'] = $this->language->get('heading_title');
        $token = "";
        if (isset($this->session->data['token'])) {
            $token = '';
        }
        $data['excel_export_orders'] = $this->url->link('tool/excel_export_orders', $token, 'SSL');
        //excel export - end
        $data['text_analytics'] = $this->language->get('text_analytics');
        $data['text_homepage'] = $this->language->get('text_homepage');
        $data['text_homepage'] = $this->language->get('text_homepage');

        $data['text_toppages'] = $this->language->get('text_toppages');

        $data['text_popup'] = $this->language->get('text_popup');
        $data['text_homepage_selected_brands'] = $this->language->get('text_homepage_selected_brands');
        $data['text_homepage_featured_categories'] = $this->language->get('text_homepage_featured_categories');
        $data['text_affiliate'] = $this->language->get('text_affiliate');
        $data['text_api'] = $this->language->get('text_api');
        $data['text_attribute'] = $this->language->get('text_attribute');
        $data['text_attribute_group'] = $this->language->get('text_attribute_group');
        $data['text_backup'] = $this->language->get('text_backup');
        $data['text_banner'] = $this->language->get('text_banner');
        $data['text_captcha'] = $this->language->get('text_captcha');
        $data['text_catalog'] = $this->language->get('text_catalog');
        $data['text_category'] = $this->language->get('text_category');
        $data['text_search'] = $this->language->get('text_search');
        $data['text_group'] = $this->language->get('text_group');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_country'] = $this->language->get('text_country');
        $data['text_coupon'] = $this->language->get('text_coupon');
        $data['text_currency'] = $this->language->get('text_currency');
        $data['text_customer'] = $this->language->get('text_customer');
        $data['text_customer_group'] = $this->language->get('text_customer_group');
        $data['text_customer_field'] = $this->language->get('text_customer_field');
        $data['text_custom_field'] = $this->language->get('text_custom_field');
        $data['text_sale'] = $this->language->get('text_sale');
        $data['text_paypal'] = $this->language->get('text_paypal');
        $data['text_paypal_search'] = $this->language->get('text_paypal_search');
        $data['text_design'] = $this->language->get('text_design');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_error_log'] = $this->language->get('text_error_log');
        $data['text_extension'] = $this->language->get('text_extension');
        $data['text_feed'] = $this->language->get('text_feed');
        $data['text_fraud'] = $this->language->get('text_fraud');
        $data['text_filter'] = $this->language->get('text_filter');
        $data['text_geo_zone'] = $this->language->get('text_geo_zone');
        $data['text_dashboard'] = $this->language->get('text_dashboard');
        $data['text_help'] = $this->language->get('text_help');
        $data['text_information'] = $this->language->get('text_information');
        $data['text_installer'] = $this->language->get('text_installer');
        $data['text_language'] = $this->language->get('text_language');
        $data['text_layout'] = $this->language->get('text_layout');
        $data['text_localisation'] = $this->language->get('text_localisation');
        $data['text_location'] = $this->language->get('text_location');
        $data['text_marketing'] = $this->language->get('text_marketing');
        $data['text_modification'] = $this->language->get('text_modification');
        $data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $data['text_module'] = $this->language->get('text_module');
        $data['text_option'] = $this->language->get('text_option');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_order_status'] = $this->language->get('text_order_status');
        $data['text_rma_status'] = $this->language->get('text_rma_status');
        $data['text_opencart'] = $this->language->get('text_opencart');
        $data['text_payment'] = $this->language->get('text_payment');
        $data['text_product'] = $this->language->get('text_product');
        $data['text_product_ranking'] = $this->language->get('text_product_ranking');
        $data['text_product_landing'] = $this->language->get('text_product_landing');
        $data['text_product_mass_upload'] = $this->language->get('text_product_mass_upload');
        $data['text_offer_banner'] = $this->language->get('text_offer_banner');
        $data['text_upper_banner'] = $this->language->get('text_upper_banner');
        $data['text_add_video_to_product'] = $this->language->get('text_add_video_to_product');
        $data['text_reports'] = $this->language->get('text_reports');
        $data['text_report_sale_order'] = $this->language->get('text_report_sale_order');
        $data['text_report_sale_tax'] = $this->language->get('text_report_sale_tax');
        $data['text_report_sale_shipping'] = $this->language->get('text_report_sale_shipping');
        $data['text_report_sale_return'] = $this->language->get('text_report_sale_return');
        $data['text_report_sale_coupon'] = $this->language->get('text_report_sale_coupon');
        $data['text_report_sale_return'] = $this->language->get('text_report_sale_return');
        $data['text_report_product_viewed'] = $this->language->get('text_report_product_viewed');
        $data['text_report_product_purchased'] = $this->language->get('text_report_product_purchased');
        $data['text_report_price_category'] = $this->language->get('text_report_price_category');

        $data['text_report_customer_activity'] = $this->language->get('text_report_customer_activity');
        $data['text_report_customer_online'] = $this->language->get('text_report_customer_online');
        $data['text_report_customer_order'] = $this->language->get('text_report_customer_order');
        $data['text_report_customer_reward'] = $this->language->get('text_report_customer_reward');
        $data['text_report_customer_credit'] = $this->language->get('text_report_customer_credit');
        $data['text_report_customer_order'] = $this->language->get('text_report_customer_order');
        $data['text_report_affiliate'] = $this->language->get('text_report_affiliate');
        $data['text_report_affiliate_activity'] = $this->language->get('text_report_affiliate_activity');
        $data['text_review'] = $this->language->get('text_review');
        $data['text_return'] = $this->language->get('text_return');
        $data['text_return_action'] = $this->language->get('text_return_action');
        $data['text_return_reason'] = $this->language->get('text_return_reason');
        $data['text_return_status'] = $this->language->get('text_return_status');
        $data['text_shipping'] = $this->language->get('text_shipping');
        $data['text_setting'] = $this->language->get('text_setting');
        $data['text_stock_status'] = $this->language->get('text_stock_status');
        $data['text_system'] = $this->language->get('text_system');
        $data['text_tax'] = $this->language->get('text_tax');
        $data['text_tax_class'] = $this->language->get('text_tax_class');
        $data['text_tax_rate'] = $this->language->get('text_tax_rate');
        $data['text_tools'] = $this->language->get('text_tools');
        $data['text_total'] = $this->language->get('text_total');
        $data['text_upload'] = $this->language->get('text_upload');
        $data['text_tracking'] = $this->language->get('text_tracking');
        $data['text_user'] = $this->language->get('text_user');
        $data['text_user_group'] = $this->language->get('text_user_group');
        $data['text_users'] = $this->language->get('text_users');
        $data['text_voucher'] = $this->language->get('text_voucher');
        $data['text_voucher_theme'] = $this->language->get('text_voucher_theme');
        $data['text_weight_class'] = $this->language->get('text_weight_class');
        $data['text_length_class'] = $this->language->get('text_length_class');
        $data['text_zone'] = $this->language->get('text_zone');
        $data['text_recurring'] = $this->language->get('text_recurring');
        $data['text_mother_day'] = $this->language->get('text_mother_day');
        $data['text_ramadan'] = $this->language->get('text_new_in');
        $data['text_order_recurring'] = $this->language->get('text_order_recurring');
        $data['text_openbay_extension'] = $this->language->get('text_openbay_extension');
        $data['text_openbay_dashboard'] = $this->language->get('text_openbay_dashboard');
        $data['text_openbay_orders'] = $this->language->get('text_openbay_orders');
        $data['text_openbay_items'] = $this->language->get('text_openbay_items');
        $data['text_openbay_ebay'] = $this->language->get('text_openbay_ebay');
        $data['text_openbay_etsy'] = $this->language->get('text_openbay_etsy');
        $data['text_openbay_amazon'] = $this->language->get('text_openbay_amazon');
        $data['text_openbay_amazonus'] = $this->language->get('text_openbay_amazonus');
        $data['text_openbay_settings'] = $this->language->get('text_openbay_settings');
        $data['text_openbay_links'] = $this->language->get('text_openbay_links');
        $data['text_openbay_report_price'] = $this->language->get('text_openbay_report_price');
        $data['text_openbay_order_import'] = $this->language->get('text_openbay_order_import');
        $data['text_dataimport'] = $this->language->get('text_dataimport');
        $data['text_dataimport_category'] = $this->language->get('text_dataimport_category');
        $data['text_dataimport_product'] = $this->language->get('text_dataimport_product');
        $data['text_new_features'] = $this->language->get('text_new_features');
        $data['text_main_store'] = $this->language->get('text_main_store');
        $data['text_main_group'] = $this->language->get('text_main_group');
        $data['text_salesmanago'] = $this->language->get('text_salesmanago');
        $data['text_store_category'] = $this->language->get('text_store_category');
        $data['text_malls'] = $this->language->get('text_malls');
        $data['text_offers'] = $this->language->get('text_offers');
        $data['text_news'] = $this->language->get('text_news');
        $data['text_newsletter'] = $this->language->get('text_newsletter');
        $data['text_shop'] = $this->language->get('text_shop');
        $data['text_shopdesigner'] = $this->language->get('text_shopdesigner');
        $data['text_brand'] = $this->language->get('text_brand');
        $data['text_tip'] = $this->language->get('text_tip');
        $data['text_tag'] = $this->language->get('text_tag');
        $data['text_look'] = $this->language->get('text_look');
        $data['text_keyword'] = $this->language->get('text_keyword');
        $data['text_marketplace_shipping'] = $this->language->get('text_marketplace_shipping');
        $data['text_3pl'] = $this->language->get('text_3pl');
        $data['text_order_in_pickup'] = $this->language->get('text_order_in_pickup');
        $data['text_fullfillment_page'] = $this->language->get('text_fullfillment_page');
        $data['text_invioce_page'] = $this->language->get('text_invioce_page');
        $data['text_order_in_fullfillment'] = $this->language->get('text_order_in_fullfillment');
        $data['text_orders_in_delivery'] = $this->language->get('text_orders_in_delivery');
        $data['text_delivered_orders'] = $this->language->get('text_delivered_orders');
        $data['text_returned_orders'] = $this->language->get('text_returned_orders');
        $data['text_returned_orders_in_pickup'] = $this->language->get('text_returned_orders_in_pickup');
        $data['text_returned_orders_in_fullfillment'] = $this->language->get('text_returned_orders_in_fullfillment');
        $data['text_returned_orders_in_delivery'] = $this->language->get('text_returned_orders_in_delivery');
        $data['text_specialcrons'] = $this->language->get('text_specialcrons');
        $data['text_landing_pages'] = $this->language->get('text_landing_pages');
        $data['text_operations'] = $this->language->get('text_operations');

        $data['landing_pages'] = $this->url->link('catalog/landing_pages', '', 'SSL');
        $data['specialcrons'] = $this->url->link('catalog/specialcrons', '', 'SSL');
        $data['main_store'] = $this->url->link('mall/main_store', '', 'SSL');
        $data['main_group'] = $this->url->link('mall/main_group', '', 'SSL');
        $data['salesmanago'] = $this->url->link('mall/salesmanago', '', 'SSL');
        $data['store_category'] = $this->url->link('mall/store_category', '', 'SSL');
        $data['malls'] = $this->url->link('mall/mall', '', 'SSL');
        $data['offers'] = $this->url->link('offer/offer', '', 'SSL');
        $data['news'] = $this->url->link('news/news', '', 'SSL');
        $data['newsletter'] = $this->url->link('newsletter/newsletter', '', 'SSL');
        $data['shop'] = $this->url->link('mall/shop', '', 'SSL');
        $data['shopdesigner'] = $this->url->link('mall/shopdesigner', '', 'SSL');
        $data['brand'] = $this->url->link('mall/brand', '', 'SSL');
        $data['tip'] = $this->url->link('mall/tip', '', 'SSL');
        $data['tag'] = $this->url->link('mall/tag', '', 'SSL');
        $data['look'] = $this->url->link('mall/look', '', 'SSL');
        $data['keyword'] = $this->url->link('mall/keyword', '', 'SSL');

        $data['dataimport_category'] = $this->url->link('dataimport/category', '', 'SSL');
        $data['dataimport_product'] = $this->url->link('dataimport/product', '', 'SSL');
        $data['analytics'] = $this->url->link('extension/analytics', '', 'SSL');
        $data['home'] = $this->url->link('common/dashboard', '', 'SSL');
        $data['affiliate'] = $this->url->link('marketing/affiliate', '', 'SSL');
        $data['api'] = $this->url->link('user/api', '', 'SSL');
        $data['attribute'] = $this->url->link('catalog/attribute', '', 'SSL');
        $data['attribute_group'] = $this->url->link('catalog/attribute_group', '', 'SSL');
        $data['backup'] = $this->url->link('tool/backup', '', 'SSL');
        $data['banner'] = $this->url->link('design/banner', '', 'SSL');
        $data['captcha'] = $this->url->link('extension/captcha', '', 'SSL');
        $data['category'] = $this->url->link('catalog/category', '', 'SSL');
        $data['search'] = $this->url->link('catalog/search', '', 'SSL');
        $data['homepage'] = $this->url->link('homepage/homepage', '', 'SSL');
        $data['slider'] = $this->url->link('cms/homepageSlider', '', 'SSL');
        $data['featured_categories'] = $this->url->link('cms/homepageFeaturedCategories', '', 'SSL');
        $data['selected_brands'] = $this->url->link('cms/homepageSelectedBrands', '', 'SSL');
        $data['top_products'] = $this->url->link('cms/homepageTopProducts', '', 'SSL');
        $data['toppages'] = $this->url->link('toppages/toppages', '', 'SSL');
        $data['featured_products'] = $this->url->link('cms/homepageFeaturedProducts', '', 'SSL');
        $data['bottom_products'] = $this->url->link('cms/homepageBottomProducts', '', 'SSL');
        $data['popup'] = $this->url->link('marketing/popup', '', 'SSL');

        $data['group'] = $this->url->link('catalog/group', '', 'SSL');
        $data['country'] = $this->url->link('localisation/country', '', 'SSL');
        $data['contact'] = $this->url->link('marketing/contact', '', 'SSL');
        $data['coupon'] = $this->url->link('marketing/coupon', '', 'SSL');
        $data['currency'] = $this->url->link('localisation/currency', '', 'SSL');
        $data['customer'] = $this->url->link('customer/customer', '', 'SSL');
        $data['customer_fields'] = $this->url->link('customer/customer_field', '', 'SSL');
        $data['customer_group'] = $this->url->link('customer/customer_group', '', 'SSL');
        $data['custom_field'] = $this->url->link('customer/custom_field', '', 'SSL');
        $data['download'] = $this->url->link('catalog/download', '', 'SSL');
        $data['error_log'] = $this->url->link('tool/error_log', '', 'SSL');
        $data['feed'] = $this->url->link('extension/feed', '', 'SSL');
        $data['filter'] = $this->url->link('catalog/filter', '', 'SSL');
        $data['fraud'] = $this->url->link('extension/fraud', '', 'SSL');
        $data['geo_zone'] = $this->url->link('localisation/geo_zone', '', 'SSL');
        $data['information'] = $this->url->link('catalog/information', '', 'SSL');
        $data['installer'] = $this->url->link('extension/installer', '', 'SSL');
        $data['language'] = $this->url->link('localisation/language', '', 'SSL');
        $data['layout'] = $this->url->link('design/layout', '', 'SSL');
        $data['location'] = $this->url->link('localisation/location', '', 'SSL');
        $data['modification'] = $this->url->link('extension/modification', '', 'SSL');
        $data['manufacturer'] = $this->url->link('catalog/manufacturer', '', 'SSL');
        $data['marketing'] = $this->url->link('marketing/marketing', '', 'SSL');
        $data['module'] = $this->url->link('extension/module', '', 'SSL');
        $data['option'] = $this->url->link('catalog/option', '', 'SSL');
        $data['algolialist'] = $this->url->link('catalog/algoliasearch', '', 'SSL');
        $data['order'] = $this->url->link('sale/order', '', 'SSL');
        $data['order_status'] = $this->url->link('localisation/order_status', '', 'SSL');
        $data['rma_status'] = $this->url->link('localisation/rma_status', '', 'SSL');
        $data['payment'] = $this->url->link('extension/payment', '', 'SSL');
        $data['paypal_search'] = $this->url->link('payment/pp_express/search', '', 'SSL');
        $data['product'] = $this->url->link('catalog/product', '', 'SSL');
        $data['product_landing'] = $this->url->link('catalog/product_landing', '', 'SSL');
        $data['product_sheet_upload'] = $this->url->link('catalog/product_sheet_upload', '', 'SSL');
        $data['product_ranking'] = $this->url->link('catalog/product_ranking', '', 'SSL');
        $data['offer_banner'] = $this->url->link('marketing/offer_banner', '', 'SSL');
        $data['upper_banner'] = $this->url->link('marketing/upper_banner', '', 'SSL');
        $data['add_video_to_product'] = $this->url->link('module/iproductvideo', '', 'SSL');
        $data['report_sale_order'] = $this->url->link('report/sale_order', '', 'SSL');
        $data['report_sale_tax'] = $this->url->link('report/sale_tax', '', 'SSL');
        $data['report_sale_shipping'] = $this->url->link('report/sale_shipping', '', 'SSL');
        $data['report_sale_return'] = $this->url->link('report/sale_return', '', 'SSL');
        $data['report_sale_coupon'] = $this->url->link('report/sale_coupon', '', 'SSL');
        $data['report_product_viewed'] = $this->url->link('report/product_viewed', '', 'SSL');
        $data['report_product_purchased'] = $this->url->link('report/product_purchased', '', 'SSL');
        $data['report_price_category'] = $this->url->link('report/price_category', '', 'SSL');
        $data['report_customer_activity'] = $this->url->link('report/customer_activity', '', 'SSL');
        $data['report_customer_online'] = $this->url->link('report/customer_online', '', 'SSL');
        $data['report_customer_order'] = $this->url->link('report/customer_order', '', 'SSL');
        $data['report_customer_reward'] = $this->url->link('report/customer_reward', '', 'SSL');
        $data['report_customer_credit'] = $this->url->link('report/customer_credit', '', 'SSL');
        $data['report_marketing'] = $this->url->link('report/marketing', '', 'SSL');
        $data['report_affiliate'] = $this->url->link('report/affiliate', '', 'SSL');
        $data['report_affiliate_activity'] = $this->url->link('report/affiliate_activity', '', 'SSL');
        $data['review'] = $this->url->link('catalog/review', '', 'SSL');
        $data['return'] = $this->url->link('sale/return', '', 'SSL');
        $data['return_action'] = $this->url->link('localisation/return_action', '', 'SSL');
        $data['return_reason'] = $this->url->link('localisation/return_reason', '', 'SSL');
        $data['return_status'] = $this->url->link('localisation/return_status', '', 'SSL');
        $data['shipping'] = $this->url->link('extension/shipping', '', 'SSL');
        $data['setting'] = $this->url->link('setting/store', '', 'SSL');
        $data['stock_status'] = $this->url->link('localisation/stock_status', '', 'SSL');
        $data['tax_class'] = $this->url->link('localisation/tax_class', '', 'SSL');
        $data['tax_rate'] = $this->url->link('localisation/tax_rate', '', 'SSL');
        $data['total'] = $this->url->link('extension/total', '', 'SSL');
        $data['upload'] = $this->url->link('tool/upload', '', 'SSL');
        $data['user'] = $this->url->link('user/user', '', 'SSL');
        $data['user_group'] = $this->url->link('user/user_permission', '', 'SSL');
        $data['voucher'] = $this->url->link('sale/voucher', '', 'SSL');
        $data['voucher_theme'] = $this->url->link('sale/voucher_theme', '', 'SSL');
        $data['weight_class'] = $this->url->link('localisation/weight_class', '', 'SSL');
        $data['length_class'] = $this->url->link('localisation/length_class', '', 'SSL');
        $data['zone'] = $this->url->link('localisation/zone', '', 'SSL');
        $data['recurring'] = $this->url->link('catalog/recurring', '', 'SSL');
        $data['mother_day'] = $this->url->link('catalog/mother_day', '', 'SSL');
        $data['ramadan'] = $this->url->link('catalog/newin', '', 'SSL');
        $data['order_recurring'] = $this->url->link('sale/recurring', '', 'SSL');

        $data['openbay_show_menu'] = $this->config->get('openbaypro_menu');
        $data['openbay_link_extension'] = $this->url->link('extension/openbay', '', 'SSL');
        $data['openbay_link_orders'] = $this->url->link('extension/openbay/orderlist', '', 'SSL');
        $data['openbay_link_items'] = $this->url->link('extension/openbay/items', '', 'SSL');
        $data['openbay_link_ebay'] = $this->url->link('openbay/ebay', '', 'SSL');
        $data['openbay_link_ebay_settings'] = $this->url->link('openbay/ebay/settings', '', 'SSL');
        $data['openbay_link_ebay_links'] = $this->url->link('openbay/ebay/viewitemlinks', '', 'SSL');
        $data['openbay_link_etsy'] = $this->url->link('openbay/etsy', '', 'SSL');
        $data['openbay_link_etsy_settings'] = $this->url->link('openbay/etsy/settings', '', 'SSL');
        $data['openbay_link_etsy_links'] = $this->url->link('openbay/etsy_product/links', '', 'SSL');
        $data['openbay_link_ebay_orderimport'] = $this->url->link('openbay/ebay/vieworderimport', '', 'SSL');
        $data['openbay_link_amazon'] = $this->url->link('openbay/amazon', '', 'SSL');
        $data['openbay_link_amazon_settings'] = $this->url->link('openbay/amazon/settings', '', 'SSL');
        $data['openbay_link_amazon_links'] = $this->url->link('openbay/amazon/itemlinks', '', 'SSL');
        $data['openbay_link_amazonus'] = $this->url->link('openbay/amazonus', '', 'SSL');
        $data['openbay_link_amazonus_settings'] = $this->url->link('openbay/amazonus/settings', '', 'SSL');
        $data['openbay_link_amazonus_links'] = $this->url->link('openbay/amazonus/itemlinks', '', 'SSL');
        $data['openbay_markets'] = array(
            'ebay' => $this->config->get('ebay_status'),
            'amazon' => $this->config->get('openbay_amazon_status'),
            'amazonus' => $this->config->get('openbay_amazonus_status'),
            'etsy' => $this->config->get('etsy_status'),
        );

        $data['orders_in_dilevery'] = $this->url->link('customerpartner/shipping_order', '', 'SSL');
        $data['orders_in_fullfillment'] = $this->url->link('customerpartner/shipping_order', '', 'SSL');
        $data['orders_in_pickup'] = $this->url->link('customerpartner/shipping_order', '', 'SSL');
        $data['delivered_orders'] = $this->url->link('customerpartner/shipping_order', '', 'SSL');
        $data['company_returns'] = $this->url->link('customerpartner/shipping_returns', '', 'SSL');
        $data['smartnotifications'] = $this->url->link('module/smartnotifications', '', 'SSL');
        $data['entry_smartnotifications'] = $this->language->get('entry_smartnotifications');

        //helpdesk ocmod code
        $ticketSystemLinks = $this->TsLoader->TsDefault->getMenuLink();

        $tsBase = new Controller\TicketSystem\TsBase($this->registry);
        $this->load->language('ticketsystem/menu');

        $data['ticketSystem'] = array();
        $data['text_ticket_system'] = $this->language->get('text_ticket_system');

        foreach ($ticketSystemLinks as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $value2) {
                    $tsBase->setFalsePassedRoute('ticketsystem/' . $value2);
                    $tsBase->checkAccessPermission();
                    if (!$tsBase->getAccessPermissionErrorStatus() && $userac->hasPermission('access','ticketsystem/' . $value2,1)) {
                        $data['ticketSystem'][$key][] = array('href' => $this->url->link('ticketsystem/' . $value2, '', 'SSL'),
                            'text' => $this->language->get('text_' . $value2)
                        );
                        $data['ticketSystem'][$key]['realArray'] = $this->language->get('text_' . $key);
                    }
                }
            } elseif ($value == 'rma') {
                if ($userac->hasPermission('access','catalog/wk_rma_admin')) {
                    $data['ticketSystem'][] = array('href' => $this->url->link('catalog/wk_rma_admin', '', 'SSL'),
                        'text' => 'Users RMA Requests'
                    );
                }
            } else {
                // $roleKey = $this->TsLoader->TsHelper->getRoleKey($value);
                $tsBase->setFalsePassedRoute('ticketsystem/' . $value);
                $tsBase->checkAccessPermission();

                // if($roleKey){
                if (!$tsBase->getAccessPermissionErrorStatus() && $userac->hasPermission('access','ticketsystem/' . $value,1))
                    $data['ticketSystem'][] = array('href' => $this->url->link('ticketsystem/' . $value, '', 'SSL'),
                        'text' => $this->language->get('text_' . $value)
                    );
            }
        }

        $data['marketplace_status'] = false;
        if ($this->config->get('marketplace_status')) {
            $data['marketplace_status'] = true;
        }
        $data['cp_barcode'] = $this->url->link('sale/shipping_process', 'token=' . $this->session->data['token'], 'SSL');
        $data['operations'] = $this->url->link('operations/operations', 'token=' . $this->session->data['token'], 'SSL');

        $data['cp_partnerlist'] = $this->url->link('customerpartner/partner', '', 'SSL');
        $data['cp_seller'] = $this->url->link('customerpartner/seller', '', 'SSL');
        $data['cp_productlist'] = $this->url->link('customerpartner/product', '', 'SSL');
        $data['cp_order'] = $this->url->link('customerpartner/order', '', 'SSL');
        $data['cp_order_tracking'] = $this->url->link('customerpartner/order_tracking', '', 'SSL');
        $data['cp_order_report'] = $this->url->link('customerpartner/order_finance_report', '', 'SSL');
        $data['cp_commission'] = $this->url->link('customerpartner/commission', '', 'SSL');
        $data['cp_income'] = $this->url->link('customerpartner/income', '', 'SSL');
        $data['cp_transaction'] = $this->url->link('customerpartner/transaction', '', 'SSL');
        $data['cp_shipping'] = $this->url->link('customerpartner/shipping', '', 'SSL');
        $data['cp_mail'] = $this->url->link('customerpartner/mails', '', 'SSL');
        $data['wk_customfield'] = $this->url->link('wkcustomfield/wkcustomfield', '', 'SSL');
        $data['rma_rma'] = $this->url->link('catalog/wk_rma_admin', '', 'SSL');
        $data['rma_reason'] = $this->url->link('catalog/wk_rma_reason', '', 'SSL');
        $data['rma_mail'] = $this->url->link('catalog/wk_rmamail_template', '', 'SSL');
        $data['wk_rma_status'] = $this->config->get('wk_rma_status');

        return $this->load->view('common/menu.tpl', $data);
    }

}
