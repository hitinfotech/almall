<?php
class ControllerTotalCodFees extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('total/codfees');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('codfees', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_none'] = $this->language->get('text_none');

		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_fee'] = $this->language->get('entry_fee');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_total'),
			'href' => $this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('total/codfees', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('total/codfees', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['codfees_total'])) {
			$data['codfees_total'] = $this->request->post['codfees_total'];
		} else {
			$data['codfees_total'] = $this->config->get('codfees_total');
		}

		if (isset($this->request->post['codfees_fee'])) {
			$data['codfees_fee'] = $this->request->post['codfees_fee'];
		} else {
			$data['codfees_fee'] = $this->config->get('codfees_fee');
		}

		if (isset($this->request->post['codfees_tax_class_id'])) {
			$data['codfees_tax_class_id'] = $this->request->post['codfees_tax_class_id'];
		} else {
			$data['codfees_tax_class_id'] = $this->config->get('codfees_tax_class_id');
		}

		$this->load->model('localisation/tax_class');

		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		if (isset($this->request->post['codfees_status'])) {
			$data['codfees_status'] = $this->request->post['codfees_status'];
		} else {
			$data['codfees_status'] = $this->config->get('codfees_status');
		}

		if (isset($this->request->post['codfees_sort_order'])) {
			$data['codfees_sort_order'] = $this->request->post['codfees_sort_order'];
		} else {
			$data['codfees_sort_order'] = $this->config->get('codfees_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('total/codfees.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'total/codfees')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
