<?php
require 'vendor/autoload.php';
header('Content-Type: text/html; charset=utf-8');
$max_age = 60 * 60 + time();
header("Cache-Control: max-age={$max_age}, must-revalidate");

///// Version
define('VERSION', '2.1.0.1');

if (getenv('ENVIROMENT')) {
    define('ENVIROMENT', getenv('ENVIROMENT'));
}
if (!defined('ENVIROMENT')) {
    define('ENVIROMENT', 'aws_development');
}
if (isset($_GET["_route_"]) && !strpos($_GET["_route_"], "?") && strpos($_GET["_route_"], "&")) {
    $exploded = explode('&', $_GET["_route_"]);
    $_GET["_route_"] = $exploded[0];
    array_shift($exploded);
    foreach ($exploded as $item) {
        $i = explode('=', $item);
        $_GET["{$i[0]}"] = $i[1];
    }
}

// Configuration
require_once('config/config.php');

// Install
if (!defined('DIR_APPLICATION')) {
    header('Location: install/index.php');
    exit;
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

// Registry
$registry = Registry::getInstance();


// Cache
$cache = Cache::getInstance(CONFIG_CACHE);
$registry->set('cache', $cache);

// Loader
$loader = Loader::getInstance($registry);
$registry->set('load', $loader);

// Config
$config = Config::getInstance();
$registry->set('config', $config);

// Database
$db = DB::getInstance(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
$registry->set('db', $db);


// First Flight
$registry->set('FFServices', new FirstFlight($registry, SHIPPING_FIRST_FLIGHT_ID));

// Store
$config->set('config_store_id', 0);
/*
  if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
  $query = ("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`ssl`, 'www.', '') = '" . $db->escape('https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
  } else {
  $query = ("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`url`, 'www.', '') = '" . $db->escape('http://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
  }
*/

// Settings
$result = $cache->get(MAIN_SETTING_CACHE_KEY);
if (!$result) {
    $query = $db->query("SELECT * FROM `" . DB_PREFIX . "setting` WHERE store_id = '0' OR store_id = '" . (int) $config->get('config_store_id') . "' ORDER BY store_id ASC");
    if ($query->num_rows) {
        $cache->set(MAIN_SETTING_CACHE_KEY, $query->rows);
        $result = $query->rows;
    } else {
        $result = array();
    }
}

foreach ($result as $row) {
    if (!$row['serialized']) {
        $config->set($row['key'], $row['value']);
    } else {
        $config->set($row['key'], json_decode($row['value'], true));
    }
}

$config->set('config_url', HTTP_SERVER);
$config->set('config_ssl', HTTPS_SERVER);

// Url
$url = new Url($config->get('config_url'), $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url'));
$registry->set('url', $url);

// Log
$log = new Log($config->get('config_error_filename'));
$registry->set('log', $log);

function error_handler($code, $message, $file, $line) {
    global $log, $config;

    // error suppressed with @
    if (error_reporting() === 0) {
        return false;
    }

    switch ($code) {
        case E_NOTICE:
        case E_USER_NOTICE:
            $error = 'Notice';
            break;
        case E_WARNING:
        case E_USER_WARNING:
            $error = 'Warning';
            break;
        case E_ERROR:
        case E_USER_ERROR:
            $error = 'Fatal Error';
            break;
        default:
            $error = 'Unknown';
            break;
    }

    if ($config->get('config_error_display')) {
        echo '<b>' . $error . '</b>: ' . $message . ' in <b>' . $file . '</b> on line <b>' . $line . '</b>';
    }

    if ($config->get('config_error_log')) {
        $log->write('PHP ' . $error . ':  ' . $message . ' in ' . $file . ' on line ' . $line);
    }

    return true;
}

// Error Handler
set_error_handler('error_handler');

// Request
$request = new Request();
$registry->set('request', $request);

// Response
$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$response->setCompression($config->get('config_compression'));
$registry->set('response', $response);

// Session
if (isset($request->get['token']) && isset($request->get['route']) && substr($request->get['route'], 0, 4) == 'api/') {
    $db->query("DELETE FROM `" . DB_PREFIX . "api_session` WHERE TIMESTAMPADD(HOUR, 1, date_modified) < NOW()");

    if (!empty($request->server['HTTP_CLIENT_IP'])) {
        $tmpip = $request->server['HTTP_CLIENT_IP'];
    } elseif (!empty($request->server['HTTP_X_FORWARDED_FOR'])) {
        $tmpip = $request->server['HTTP_X_FORWARDED_FOR'];
    } elseif (!empty($request->server['REMOTE_ADDR'])) {
        $tmpip = $request->server['REMOTE_ADDR'];
    } else {
        $tmpip = '';
    }
    $result = explode(",", $tmpip);
    $ip = trim(reset($result));

    $query = $db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "api` `a` LEFT JOIN `" . DB_PREFIX . "api_session` `as` ON (a.api_id = as.api_id) LEFT JOIN " . DB_PREFIX . "api_ip `ai` ON (as.api_id = ai.api_id) WHERE a.status = '1' AND as.token = '" . $db->escape($request->get['token']) . "' AND ai.ip = '" . $db->escape($ip) . "'");

    if ($query->num_rows) {
        // Does not seem PHP is able to handle sessions as objects properly so so wrote my own class
        $session = Session::getInstance($query->row['session_id'], $query->row['session_name']);
        $session->data['api_id'] = $query->row['api_id'];
        
        if(isset($request->get['order_id'])){
            $session->data['order_id'] = $request->get['order_id'];
        }
        
        $registry->set('session', $session);
        
        // keep the session alive
        $db->query("UPDATE `" . DB_PREFIX . "api_session` SET date_modified = NOW() WHERE api_session_id = '" . $query->row['api_session_id'] . "'");
    }
} else {
    $session = Session::getInstance();
    $registry->set('session', $session);
}

$route = "";
if (isset($request->get['_route_'])) {
    $route = $request->get['_route_'];
} elseif (isset($request->get['route'])) {
    $route = $request->get['route'];
}

$countries = $cache->get('country.getavailablecountries.0.0');

if (!$countries) {

    $avcountries = $db->query(" SELECT c.country_id, iso_code_2, c.language_code AS language, c.curcode, cd.name,c.telecode FROM country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE c.available='1' AND cd.language_id='1' ");

    foreach ($avcountries->rows as $row) {
        $row['iso_code_2'] = strtolower($row['iso_code_2']);
        $countries[$row['iso_code_2']] = $row;
    }

    if ($countries) {
        $cache->set('country.getavailablecountries.0.0', $countries);
    }
}

define('COUNTRIES', serialize($countries));

// Country Detection Library
$country = new Country($registry);
$registry->set('country', $country);

$parts = explode('/', $route);

$country_code = (isset($parts[0]) && array_key_exists($parts[0], unserialize(COUNTRIES)) ? strtolower($parts[0]) : $country->getCode());

$session->data['country'] = $country_code;

if (!isset($session->data['country']) || $session->data['country'] != $country_code) {
    $session->data['country'] = $country_code;
}
if (!isset($request->cookie['country']) || $request->cookie['country'] != $country_code) {
    setcookie('country', $country_code, COOKIES_PERIOD, '/', $request->server['HTTP_HOST']);
}

if (is_array($parts) && !empty($parts) && array_key_exists($parts[0], unserialize(COUNTRIES)) && $country_code && array_key_exists($country_code, unserialize(COUNTRIES))) {
    array_shift($parts);
} else {
    $route = '';
    $url = '';
    if (!isset($request->get['route'])) {
        $url_data = $request->get;
        if (isset($url_data['_route_'])) {
            $route = $url_data['_route_'];
            unset($url_data['_route_']);
            $url_sep = '?';
            if ($url_data) {
                $url = $url_sep . urldecode(http_build_query($url_data, '', '&'));
            }
        }

        if ($route != 'sitemap.xml') {
            //header("HTTP/1.1 301 Moved Permanently");
            //header("Location: " . HTTPS_SERVER . $country->getCode() . "/" . $route . $url);
        }
    }
}

// Language Detection
$languages = array();
$result = $cache->get('index.languages');
if (!$result) {
    $query = $db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE status = '1'");

    if ($query->num_rows) {
        $cache->set('index.languages', $query->rows);
        $result = $query->rows;
    } else {
        $result = array();
    }
}

foreach ($result as $row) {
    $languages[$row['code']] = $row;
}

$language_code = '';
if (is_array($parts) && !empty($parts) && in_array($parts[0], array('ar', 'AR', 'en', 'EN')) && !isset($request->get['route'])) {
    if ($parts[0] == 'en' || $parts[0] == 'EN') {
        $language_code = 'en';
    }
    if ($parts[0] == 'ar' || $parts[0] == 'AR') {
        $language_code = 'ar';
    }
    array_shift($parts);
} else {
    if (isset($language_code) && array_key_exists($language_code, $languages)) {
        $language_code = $language_code;
        $session->data['language'] = $language_code;
    } elseif (isset($session->data['language']) && array_key_exists($session->data['language'], $languages)) {
        $language_code = $session->data['language'];
    } elseif (isset($request->cookie['language']) && array_key_exists($request->cookie['language'], $languages)) {
        $language_code = $request->cookie['language'];
    } else {
        $detect = '';

        if (isset($request->server['HTTP_ACCEPT_LANGUAGE']) && $request->server['HTTP_ACCEPT_LANGUAGE']) {

            $browser_languages = explode(',', $request->server['HTTP_ACCEPT_LANGUAGE']);
            foreach ($browser_languages as $browser_language) {
                $browser_language = substr($browser_language, 0, 2);

                foreach ($languages as $key => $value) {
                    if ($value['status']) {
                        $locale = explode(',', substr($value['locale'], 0, 2));
                        if (in_array($browser_language, $locale)) {
                            $detect = $key;
                            break 2;
                        }
                    }
                }
            }
        }
        if ($detect) {
            $language_code = $detect;
        } else {
            $language_country_id = $registry->get('country')->getRealId();
            $rows = $db->query("SELECT language_code FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int) $language_country_id . "'");
            if ($rows->num_rows && in_array($rows->row['language_code'], array('ar', 'en'))) {
                $language_code = $rows->row['language_code'];
            } else {
                $language_code = $config->get('config_language');
            }
        }
    }

    $route = '';
    $url = '';
    if (!isset($request->get['route'])) {
        $url_data = $request->get;
        if (isset($url_data['_route_'])) {
            $route = ltrim(strtolower($url_data['_route_']), $registry->get('country')->getcode() . "/");

            unset($url_data['_route_']);
            $url_sep = '?';
            if ($url_data) {
                $url = $url_sep . urldecode(http_build_query($url_data, '', '&'));
            }
        }

        if ($route != 'sitemap.xml') {
            //header("HTTP/1.1 301 Moved Permanently");
            //header("Location: " . HTTPS_SERVER . $registry->get('country')->getcode() . "/" . $language_code . "/" . $route . $url);
        }
    }
}

$session->data['language'] = $language_code;
if (!isset($session->data['language']) || $session->data['language'] != $language_code) {
    $session->data['language'] = $language_code;
}

if (!isset($request->cookie['language']) || $request->cookie['language'] != $language_code) {
    setcookie('language', $language_code, COOKIES_PERIOD, '/', $request->server['HTTP_HOST']);
}

$config->set('config_language_id', $languages[$language_code]['language_id']);
$config->set('config_language', $languages[$language_code]['code']);

// Language
$language = new Language($languages[$language_code]['directory']);
$language->load($languages[$language_code]['directory']);
$registry->set('language', $language);


// Document
$registry->set('document', new Document());

// Customer
$customer = new Customer($registry);
$registry->set('customer', $customer);

$session->data['wishlist'] = $customer->getWishlist();

// Customer Group
if ($customer->isLogged()) {
    $config->set('config_customer_group_id', $customer->getGroupId());
} elseif (isset($session->data['customer']) && isset($session->data['customer']['customer_group_id'])) {
    // For API calls
    $config->set('config_customer_group_id', $session->data['customer']['customer_group_id']);
} elseif (isset($session->data['guest']) && isset($session->data['guest']['customer_group_id'])) {
    $config->set('config_customer_group_id', $session->data['guest']['customer_group_id']);
}

// Tracking Code
if (isset($request->get['tracking'])) {
    setcookie('tracking', $request->get['tracking'], COOKIES_PERIOD, '/');

    $db->query("UPDATE `" . DB_PREFIX . "marketing` SET clicks = (clicks + 1) WHERE code = '" . $db->escape($request->get['tracking']) . "'");
}

// Affiliate
$registry->set('affiliate', new Affiliate($registry));

// Currency
$registry->set('currency', new Currency($registry));
$registry->get('currency')->set($registry->get('country')->getCurcode());

// Facebook
$registry->set('facebook', new FacebookLibrary($registry));

// Dynamic Values
$registry->set('dynamic_values', new DynamicValues($registry));

// Tax
$registry->set('tax', new Tax($registry));

// Weight
$registry->set('weight', new Weight($registry));

// Length
$registry->set('length', new Length($registry));

//Cloudinary
$registry->set('cloudinary', new Cloudinaryclass($registry));

//Common function
$registry->set('commonfunctions', new commonfunctions($registry));


// Cart
$registry->set('cart', new Cart($registry));

// Cart
$registry->set('admin_cart', new Admin_Cart($registry));

// Encryption
$registry->set('encryption', new Encryption($config->get('config_encryption')));

// OpenBay Pro
$registry->set('openbay', new Openbay($registry));

// Algolia
$registry->set('algolia', new Algolia($registry));


// AWS S3
$registry->set('aws_s3', new Aws_S3($registry));

// AWS SQS
$registry->set('aws_sqs', new Aws_SQS($registry));


$salesManago = SalesManago::getInstance($registry);
$registry->set('salesmanago', $salesManago);


//GTM Initalize
$registry->set('gtm', New Gtm($registry));

// Event
$event = new Event($registry);
$registry->set('event', $event);


$query = $db->query("SELECT * FROM " . DB_PREFIX . "event");

foreach ($query->rows as $result) {
    $event->register($result['trigger'], $result['action']);
}

// Front Controller
$controller = new Front($registry);

// Maintenance Mode
$controller->addPreAction(new Action('common/maintenance'));

// SEO URL's
$controller->addPreAction(new Action('common/seo_url'));

// Router
if (isset($request->get['route'])) {
    $action = new Action($request->get['route']);
} else {
    $action = new Action('common/home');
}

// Dispatch
$controller->dispatch($action, new Action('error/not_found'));

// Output
$response->output();
