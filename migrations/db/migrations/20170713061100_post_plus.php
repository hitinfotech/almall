<?php

use Phinx\Migration\AbstractMigration;

class PostPlus extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {

        $this->query("INSERT INTO `ship_company` (`ship_company_id`, `contact_person`, `contact_email`, `mobile`, `phone`, `access_no`, `password`, `username`, `customer_code`, `status`, `date_added`, `user_id`, `last_mod_id`, `last_mod_date`) 
                                               VALUES (4, 'postplus', 'postplus', 'postplus', 'x', 'CR2931', 'sayidaty1234', 'CR2931', 'CR2931', 1, NULL, NULL, NULL, NULL);");


        $this->query("INSERT INTO `ship_company_description` (`ship_company_id`, `language_id`, `name`) VALUES
(4, 1, 'PostPlus'),
(4, 2, 'PostPlus')

");

        $this->query("ALTER TABLE `zone` ADD `postaplus_code` VARCHAR(10) NOT NULL DEFAULT 'NA' AFTER `naqel_destination_code`;");
        $this->query("UPDATE `zone` SET `postaplus_code` = 'AREA9' WHERE `zone`.`zone_id` = 1789;");

    }
}
