<?php

use Phinx\Migration\AbstractMigration;

class NaqelCode extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->query("ALTER TABLE `zone` ADD `firstflight_destination_code` VARCHAR(10) NOT NULL AFTER `available`;");
        $this->query("UPDATE `zone` JOIN firstflight_destination_code
ON zone.zone_id = firstflight_destination_code.zone_id
SET zone.firstflight_destination_code = firstflight_destination_code.destination_code,
zone.zone_id = firstflight_destination_code.zone_id");


        $this->query("ALTER TABLE `zone` ADD `naqel_destination_code` VARCHAR(10) NOT NULL AFTER `firstflight_destination_code`;");

        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'ABT' WHERE `zone_id`= '2874' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'AJF' WHERE `zone_id`= '2876' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'MED' WHERE `zone_id`= '2877' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'ELQ' WHERE `zone_id`= '2878' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'RUH' WHERE `zone_id`= '2879' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'DMM' WHERE `zone_id`= '2880' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'KMT' WHERE `zone_id`= '2881' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'HAS' WHERE `zone_id`= '2882' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'DAMAD' WHERE `zone_id`= '2883' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'MAC' WHERE `zone_id`= '2884' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'EAM' WHERE `zone_id`= '2885' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'TUU' WHERE `zone_id`= '2886' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'JED' WHERE `zone_id`= '4232' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'DMM' WHERE `zone_id`= '4233' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'DMM' WHERE `zone_id`= '4234' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'KHO' WHERE `zone_id`= '4235' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'TIF' WHERE `zone_id`= '4236' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'DMM' WHERE `zone_id`= '4237' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'AKH' WHERE `zone_id`= '4238' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'HOF' WHERE `zone_id`= '4239' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'YNB' WHERE `zone_id`= '4240' ");
        $this->query(" UPDATE `zone` set `naqel_destination_code`= 'QJB' WHERE `zone_id`= '4241' ");




        $this->query("CREATE TABLE IF NOT EXISTS `ship_company` (
  `ship_company_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `access_no` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `customer_code` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  `last_mod_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ship_company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;");

        $this->query("truncate ship_company");
        $this->query("INSERT INTO `ship_company` (`ship_company_id`, `contact_person`, `contact_email`, `mobile`, `phone`, `access_no`, `password`, `username`, `customer_code`, `status`, `date_added`, `user_id`, `last_mod_id`, `last_mod_date`) VALUES
(1, 'Monisha', 'monisha@firstflightme.com', '+971 56 4083511', '+971 600545456', '14028', 's@ff!p', '14028', '14028', 1, '2017-04-19 10:19:44', 4, NULL, NULL),
(2, 'Waleed', 'sofan@naqel.com.sa', '', '', '9017927', '9017Sayidaty927', '9017927', '9017927', 1, '2017-04-19 10:19:57', 4, NULL, NULL),
(3, 'Mohammed_Z_Mustafa', 'mmustaffa@smsaexpress.com', '+966 59 512 7876', ' +966 11 218 7716', 'Testing0', 'srpc@4455', 'srpcuser', 'srpcuser', 1, '2017-06-12 00:00:00', 4, NULL, NULL);");



        $this->query("CREATE TABLE IF NOT EXISTS `ship_company_description` (
  `ship_company_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ship_company_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        $this->query("truncate ship_company_description");

        $this->query("INSERT INTO `ship_company_description` (`ship_company_id`, `language_id`, `name`) VALUES
(1, 1, 'FirstFlight'),
(1, 2, 'FirstFlight'),
(2, 1, 'naqel'),
(2, 2, 'naqel'),
(3, 1, 'samsa'),
(3, 2, 'samsa');
");

        $this->query("DROP TABLE IF EXISTS `ship_company_to_country`");
                $this->query("CREATE TABLE IF NOT EXISTS `ship_company_to_country` (
  `id` int(11) NOT NULL DEFAULT '0',
  `origin` varchar(255) DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  `ship_company_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        $this->query("INSERT INTO `ship_company_to_country` (`id`, `origin`, `destination`, `ship_company_id`) VALUES
(48, 'AE', 'AE', 1),
(50, 'AE', 'QA', 1),
(51, 'AE', 'BH', 1),
(52, 'AE', 'SA', 1),
(53, 'AE', 'KW', 4),
(54, 'AE', 'OM', 1),
(55, 'QA', 'AE', 1),
(56, 'QA', 'QA', 1),
(57, 'QA', 'BH', 1),
(58, 'QA', 'SA', 1),
(59, 'QA', 'KW', 4),
(60, 'QA', 'OM', 1),
(61, 'BH', 'AE', 1),
(62, 'BH', 'QA', 1),
(63, 'BH', 'BH', 1),
(64, 'BH', 'SA', 1),
(65, 'BH', 'KW', 4),
(66, 'BH', 'OM', 1),
(73, 'SA', 'AE', 3),
(74, 'SA', 'QA', 3),
(75, 'SA', 'BH', 3),
(76, 'SA', 'SA', 2),
(78, 'SA', 'KW', 3),
(79, 'SA', 'OM', 4),
(80, 'KW', 'AE', 4),
(81, 'KW', 'QA', 4),
(82, 'KW', 'BH', 4),
(83, 'KW', 'SA', 4),
(84, 'KW', 'KW', 4),
(85, 'KW', 'OM', 4),
(86, 'OM', 'AE', 1),
(87, 'OM', 'QA', 1),
(88, 'OM', 'BH', 1),
(89, 'OM', 'SA', 1),
(90, 'OM', 'KW', 4),
(91, 'OM', 'OM', 1),
(92, 'JO', 'AE', 1),
(93, 'JO', 'QA', 1),
(94, 'JO', 'BH', 1),
(95, 'JO', 'SA', 1),
(96, 'JO', 'OM', 1),
(97, 'EG', 'AE', 1);");
    }
    public function down()
    {

    }
}
