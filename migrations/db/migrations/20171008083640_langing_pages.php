<?php

use Phinx\Migration\AbstractMigration;

class LangingPages extends AbstractMigration
{

    public function up()
    {

      $this->query("SET sql_mode='';");
      $this->query("SET NAMES utf8;");
      $this->query("
      CREATE TABLE `landing_pages` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `page_name` varchar(255) NOT NULL,
        `page_name_ar` varchar(255) DEFAULT '',
        `show_at_homepage` tinyint(1) DEFAULT '1',
        `status` tinyint(1) DEFAULT '1',
        `deleted` int(1) DEFAULT '0',
        `user_id` int(11) NOT NULL,
        `user_modified_id` int(11) NOT NULL,
        `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
      ");

    }
}
