<?php

use Phinx\Migration\AbstractMigration;

class FbsFieldsInProduct extends AbstractMigration
{
  public function  up()
  {
      $this->query(" alter table product add column  `is_fbs` enum('0','1') NOT NULL DEFAULT '0' ");

      $this->query(" alter table product add column fbs_damaged_quantity int(11) NOT NULL default 0");
      $this->query(" alter table product add column fbs_lost_quantity int(11) NOT NULL default 0");

      $this->query(" alter table product add column fbs_bin varchar(255) NOT NULL default ''");
      $this->query(" alter table product add column fbs_stack varchar(255) NOT NULL default ''");
      $this->query(" alter table product add column fbs_date_in date NOT NULL DEFAULT '0000-00-00'");
      $this->query(" alter table product add column fbs_date_out date NOT NULL DEFAULT '0000-00-00'");
  }
}
