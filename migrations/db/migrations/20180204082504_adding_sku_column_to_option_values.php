<?php

use Phinx\Migration\AbstractMigration;

class AddingSkuColumnToOptionValues extends AbstractMigration
{
  public function up()
  {
      $this->query("SET sql_mode=''");
      $this->query("SET NAMES utf8");
      $this->query("ALTER TABLE `product_option_value` ADD sku_size VARCHAR(50)");
  }
}
