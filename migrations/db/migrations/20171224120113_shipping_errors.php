<?php

use Phinx\Migration\AbstractMigration;

class ShippingErrors extends AbstractMigration
{
    public function up()
    {

      $this->query("
            CREATE TABLE `order_shipping_errors` (
        `order_shipping_error_id` int(11) NOT NULL AUTO_INCREMENT,
        `order_id` int(11) DEFAULT NULL,
        `country_id` int(11) DEFAULT NULL,
        `error` varchar(50) DEFAULT NULL,
        `shipping_company` int(11) DEFAULT NULL,
        `prod_type` varchar(50) DEFAULT NULL,
        `date_added` datetime DEFAULT NULL,
        `date_modified` datetime DEFAULT NULL,
        PRIMARY KEY (`order_shipping_error_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
      ");

    }
}
