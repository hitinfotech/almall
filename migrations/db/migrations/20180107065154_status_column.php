<?php

use Phinx\Migration\AbstractMigration;

class StatusColumn extends AbstractMigration
{
    public function up()
    {
      $this->query("ALTER TABLE product_upload_sheet add column sheet_status enum ('ADDED','INQUEUE','DONE')");

    }
}
