<?php

use Phinx\Migration\AbstractMigration;

class Visitedproductsfromcookietodb20171003 extends AbstractMigration {

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $this->query("SET sql_mode=''; SET names utf8; "
                . " CREATE TABLE IF NOT EXISTS `guest_view` ( "
                . "`guest_view_id` int(11) NOT NULL AUTO_INCREMENT,"
                . "`session_id` varchar(100) DEFAULT NULL,"
                . "`ip` varchar(40) DEFAULT NULL,"
                . "`product_id` int(11) DEFAULT NULL,"
                . "`counter` int(11) DEFAULT NULL,"
                . "`date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                . "PRIMARY KEY (`guest_view_id`),"
                . "UNIQUE KEY `session_id` (`session_id`,`product_id`)"
                . ") ENGINE=InnoDB DEFAULT CHARSET=utf8;");
    }

    public function down() {
        
    }

}
