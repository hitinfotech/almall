<?php

use Phinx\Migration\AbstractMigration;

class Accounturls extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(){
        $this->query(" DROP TABLE IF EXISTS url_alias; ");
        $this->query(" INSERT INTO url_alias_en SET `query`='account/credit', `keyword` = 'profile-credit' ");
        $this->query(" INSERT INTO url_alias_ar SET `query`='account/credit', `keyword` = 'profile-credit' ");
    }
}
