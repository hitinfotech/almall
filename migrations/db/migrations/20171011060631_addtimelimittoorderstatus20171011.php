<?php

use Phinx\Migration\AbstractMigration;

class Addtimelimittoorderstatus20171011 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $this->query("SET sql_mode=''; ALTER TABLE order_status ADD attention_time  INT NOT NULL DEFAULT 0 "); 
        $this->query("SET sql_mode=''; ALTER TABLE order_status ADD warning_time    INT NOT NULL DEFAULT 0 "); 
        $this->query("SET sql_mode=''; UPDATE order_status SET warning_time='24' WHERE order_status_id='4'"); // Verification
        $this->query("SET sql_mode=''; UPDATE order_status SET warning_time='12' WHERE order_status_id='1'"); // Pending
        $this->query("SET sql_mode=''; UPDATE order_status SET warning_time='20' WHERE order_status_id='17'");// Ready to pikup
        $this->query("SET sql_mode=''; UPDATE order_status SET warning_time='24' WHERE order_status_id='3'"); // Shipped
        $this->query("SET sql_mode=''; CREATE TABLE `final_order_status` ("
                . "  `order_history_id` int(11) NOT NULL AUTO_INCREMENT,"
                . "  `order_id` int(11) NOT NULL,"
                . "  `order_status_id` int(11) NOT NULL,"
                . "  `notify` tinyint(1) NOT NULL DEFAULT '0',"
                . "  `comment` text NOT NULL,"
                . "  `date_added` datetime NOT NULL,"
                . "  `user_id` int(11) NOT NULL DEFAULT '0',"
                . "  PRIMARY KEY (`order_history_id`),"
                . "  KEY `order_id` (`order_id`),"
                . "  KEY `order_status_id` (`order_status_id`)"
                . ") ENGINE=InnoDB DEFAULT CHARSET=utf8"); // Shipped
    }

    public function down() {
        
    }
}
