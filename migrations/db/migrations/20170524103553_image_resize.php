<?php

use Phinx\Migration\AbstractMigration;

class ImageResize extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->query("alter table image_s3 add new_image_path text after image");
        $this->query("alter table image_s3 add width int");
        $this->query("alter table image_s3 add height int");
        $this->query("ALTER TABLE `image_s3` ADD `status` ENUM('NEW','INQUEUE','RESIZING','DONE') NOT NULL DEFAULT 'NEW' AFTER `height`");
        $this->query("ALTER TABLE `image_s3` ADD `upload_image`  int(1) NOT NULL DEFAULT 1 AFTER `height`");
	$this->query("update `image_s3` set `status` = 'DONE'");
    }
    public function down()
    {

    }
}
