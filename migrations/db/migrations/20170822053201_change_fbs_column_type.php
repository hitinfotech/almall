<?php

use Phinx\Migration\AbstractMigration;

class ChangeFbsColumnType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {

      $this->query("alter table customerpartner_to_customer modify column fbs_fullfillment_charges double(11,3) default 0");
      $this->query("alter table customerpartner_to_customer modify column fbs_storage_charges double(11,3) default 0");

    }
}
