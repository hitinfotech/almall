<?php

use Phinx\Migration\AbstractMigration;

class Newsletter extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $ne_draft = $this->query(" DESC ne_draft regency ");
        if($ne_draft->num_rows){
        $this->query(" alter table ne_draft add regency int(1) default 0");
        }
        $ne_draft = $this->query(" DESC ne_draft frequency ");
        if($ne_draft->num_rows){
        $this->query(" alter table ne_draft add frequency int(1) default 0");
        }
        $ne_draft = $this->query(" DESC ne_draft life_time_value ");
        if($ne_draft->num_rows){
        $this->query(" alter table ne_draft add life_time_value int(1) default 0");
        }
        $ne_draft = $this->query(" DESC ne_draft visited_category ");
        if($ne_draft->num_rows){
        $this->query(" alter table ne_draft add visited_category text;");
        }
        $ne_draft = $this->query(" DESC ne_draft purchased_category ");
        if($ne_draft->num_rows){
        $this->query(" alter table ne_draft add purchased_category text");
        }
        $ne_draft = $this->query(" DESC ne_draft newsletter_id ");
        if($ne_draft->num_rows){
        $this->query(" alter table ne_draft add newsletter_id int");
        }
        $ne_draft = $this->query(" DESC ne_draft defined_category ");
        if($ne_draft->num_rows){
        $this->query(" alter table ne_draft add defined_category text");
        }
        
        $this->query("CREATE TABLE IF NOT EXISTS `emails_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('NEWSLETTER') NOT NULL DEFAULT 'NEWSLETTER',
  `draft_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('NEW','FETCHINGMAILS','READY','PROCESSING','PAUSED','COMPLETED') NOT NULL DEFAULT 'NEW',
  `from_email` mediumtext NOT NULL,
  `sender_name` mediumtext NOT NULL,
  `subject` mediumtext NOT NULL,
  `message` mediumtext NOT NULL,
  `attachments` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `draft_id` (`draft_id`)
)");

        $this->query("CREATE TABLE IF NOT EXISTS  `emails_chunk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emails_campaign_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `to_mails` text,
  `to_plain` text,
  `status` enum('NEW','INQUEUE','INPROGRESS','DONE','PAUSED') NOT NULL DEFAULT 'NEW',
  `success` int(11) NOT NULL DEFAULT '0',
  `fail` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ready_to_share_id` (`emails_campaign_id`)
) ");

    }
    public function down()
    {

    }
}
