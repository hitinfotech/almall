<?php

use Phinx\Migration\AbstractMigration;

class CreateTableEligibleVat extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->query("SET sql_mode= ''");
        $this->query("SET NAMES utf8");

        $this->query("CREATE TABLE IF NOT EXISTS `eligible_vat` ( 
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `customer_id` INT(11) NOT NULL,
            `document_path` VARCHAR(255) DEFAULT NULL,
            `company_name` VARCHAR(50) DEFAULT NULL ,
            `business_address` VARCHAR(255) DEFAULT NULL ,
            `tax_registration_number` VARCHAR(50) DEFAULT NULL ,
            `issue_date` date DEFAULT NULL,
            `checked` tinyint(1) DEFAULT 1,
            PRIMARY KEY (`id`),
            FOREIGN KEY (`customer_id`) REFERENCES customerpartner_to_customer(`customer_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
             ");
    }
}
