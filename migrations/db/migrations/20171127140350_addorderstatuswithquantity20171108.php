<?php

use Phinx\Migration\AbstractMigration;

class Addorderstatuswithquantity20171108 extends AbstractMigration {

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $this->query("SET sql_mode=''; SET NAMES utf8; SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'order_status' AND table_schema = DATABASE() AND column_name = 'quantity') > 0, \"ALTER TABLE order_status DROP quantity\", \" SELECT 1 \"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
        $this->query("SET sql_mode=''; SET NAMES utf8; SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'order_status' AND table_schema = DATABASE() AND column_name = 'stock') > 0, \"ALTER TABLE order_status DROP stock\", \" SELECT 1 \"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
        $this->query("SET sql_mode=''; SET NAMES utf8; ALTER TABLE order_status ADD quantity ENUM('add','subtract','purge') NOT NULL DEFAULT 'subtract';");
        $this->query("SET sql_mode=''; SET NAMES utf8; ALTER TABLE order_status ADD stock ENUM('in','out') NOT NULL DEFAULT 'in';");
        $this->query("SET sql_mode=''; SET NAMES utf8; UPDATE order_status SET quantity='subtract' WHERE order_status_id IN('13','1','18','15','5','2','17','19','20','12','3','4');");
        $this->query("SET sql_mode=''; SET NAMES utf8; UPDATE order_status SET quantity='add' WHERE order_status_id IN('11','14','21');");
        $this->query("SET sql_mode=''; SET NAMES utf8; UPDATE order_status SET name='Canceled By Seller',quantity='purge'  WHERE order_status_id='7';");
        $this->query("SET sql_mode=''; SET NAMES utf8; INSERT INTO order_status SET name='Canceled By Customer', quantity='add', language_id='1', order_status_id='21' ");
        $this->query("SET sql_mode=''; SET NAMES utf8; INSERT INTO order_status SET name='Canceled By Customer', quantity='add', language_id='2', order_status_id='21' ");
        $this->query("SET sql_mode=''; SET NAMES utf8; CREATE TABLE IF NOT EXISTS `change_quantity_log` ("
                . "  `change_quantity_log_id` int(11) NOT NULL AUTO_INCREMENT,"
                . "  `order_product_id` int(11) DEFAULT NULL,"
                . "  `order_id` int(11) DEFAULT NULL,"
                . "  `product_id` int(11) DEFAULT NULL,"
                . "  `original_quantity` int(11) DEFAULT NULL,"
                . "  `order_quantity` int(11) DEFAULT NULL,"
                . "  `new_product_quantity` int(11) DEFAULT NULL,"
                . "  `original_order_status_id` int(11) DEFAULT NULL,"
                . "  `new_order_status_id` int(11) DEFAULT NULL,"
                . "  `original_status_name` varchar(50) DEFAULT NULL,"
                . "  `new_status_name` varchar(50) DEFAULT NULL,"
                . "  `user_id` int(11) DEFAULT NULL,"
                . "  `date_added` datetime DEFAULT NULL,"
                . "  PRIMARY KEY (`change_quantity_log_id`)"
                . ") ENGINE=InnoDB DEFAULT CHARSET=utf8; ");
        $this->query("SET sql_mode=''; SET NAMES utf8; UPDATE product_option_value SET subtract=1 ");
    }

    public function down() {
        
    }

}
