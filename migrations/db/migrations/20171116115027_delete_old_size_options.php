<?php

use Phinx\Migration\AbstractMigration;

class DeleteOldSizeOptions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->query("DELETE FROM product_option WHERE option_id IN (55, 59, 58, 48, 54, 56, 47, 50, 60, 62, 40, 41, 52, 69, 51, 53, 61, 67, 37, 35, 64, 34, 26, 68);");
        
        $this->query("DELETE POV FROM product_option_value POV LEFT JOIN product_option PO ON POV.option_id=PO.option_id WHERE PO.option_id is null;");
        
        $this->query("DELETE OVD FROM option_value_description OVD LEFT JOIN product_option PO ON OVD.option_id=PO.option_id WHERE PO.option_id is null;");
        
        $this->query("DELETE OV FROM option_value OV LEFT JOIN product_option PO ON OV.option_id=PO.option_id WHERE PO.option_id is null;");
        
        $this->query("DELETE OD FROM option_description OD LEFT JOIN product_option PO ON OD.option_id=PO.option_id WHERE PO.option_id is null;");
        
        $this->query("DELETE O FROM `option` O LEFT JOIN product_option PO ON O.option_id=PO.option_id WHERE PO.option_id is null;");
        
        $this->query("DELETE OC FROM option_to_category OC LEFT JOIN product_option PO ON OC.option_id=PO.option_id WHERE PO.option_id is null;");
    }
}
