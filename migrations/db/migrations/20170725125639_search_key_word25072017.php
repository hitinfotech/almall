<?php

use Phinx\Migration\AbstractMigration;

class SearchKeyWord25072017 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->query("SET sql_mode=''; CREATE TABLE IF NOT EXISTS `popular_search` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
                `country_id` int(11) DEFAULT NULL,
                  `language_id` int(11) DEFAULT NULL,
                    `keyword` varchar(100) DEFAULT NULL,
                    `keytype` varchar(100) DEFAULT NULL,
                      `counter` int(11) NOT NULL DEFAULT '0',
                        `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `approved` int(11) NOT NULL DEFAULT '0',
                            `approved_on` datetime DEFAULT NULL,
                              `approved_by` int(11) DEFAULT NULL,
                                PRIMARY KEY (`id`),
                                  UNIQUE KEY `UC_Keyword` (`country_id`,`language_id`,`keyword`,`keytype`),
                                    KEY `approved_by` (`approved_by`),
                                      CONSTRAINT `popular_search_user_ibfk_1` FOREIGN KEY (`approved_by`) REFERENCES `user` (`user_id`)
                                      ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
    }
    
    public function down()
    {

    }
}
