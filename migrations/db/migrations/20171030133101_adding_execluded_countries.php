<?php

use Phinx\Migration\AbstractMigration;

class AddingExecludedCountries extends AbstractMigration
{
    public function up()
    {
      $this->query("
      CREATE TABLE `seller_execluded_countries` (
         `customer_id` int(11) NOT NULL,
         `country_id` int(11) NOT NULL,
         PRIMARY KEY (`customer_id`,`country_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
      ");

    }
}
