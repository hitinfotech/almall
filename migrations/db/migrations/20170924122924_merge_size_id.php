<?php

use Phinx\Migration\AbstractMigration;

class MergeSizeId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
	$Migrate=array(
1663=> array(1431, 1223),
1664=> array(1432),
1665=> array(1433),
1666=> array(1434, 1224),
1667=> array(1435, 1468),
1668=> array(1436, 1225),
1669=> array(1437, 1427),
1670=> array(1438, 1226),
1671=> array(1439, 1428),
1672=> array(1440, 1227),
1673=> array(1441, 1429),
1674=> array(1442, 1228),
1675=> array(1443, 1430),
1676=> array(1444, 1229),
1677=> array(1445, 1623),
1678=> array(1446, 1520),
1679=> array(1447),
1680=> array(708, 1291, 1610, 1489, 1406, 1300, 1405, 1404),
1681=> array(1282, 1292, 1611, 1490, 1408, 1301, 1407),
1682=> array(1283, 1293, 1612, 1491, 1410, 1302, 1409),
1683=> array(1492, 1584),
1684=> array(1284, 1294, 1613, 1493, 1585),
1685=> array(1494),
1686=> array(1285, 1295, 1614, 1495),
1687=> array(1496),
1688=> array(1286, 1296, 1615, 1497),
1689=> array(1287, 1297, 1616, 1498),
1690=> array(1288, 1298, 1617, 1499),
1691=> array(1500),
1692=> array(1289, 1299, 1501),
1693=> array(1502),
1694=> array(1503),
1648=> array(1624, 390, 1391, 1394, 1392, 1393),
1649=> array(1414, 1415, 1209, 1342, 1341, 1378, 1377, 1211, 1474, 1378, 1377, 1211, 1474),
1650=> array(1210, 1413, 1212, 1343, 1379, 1475, 1330, 1354),
1651=> array(1208, 1574, 1213, 1344, 1380, 1380, 1476, 1331, 1355),
1652=> array(1231, 1416, 1214, 1345, 1381, 1477, 1332, 1356),
1653=> array(1232, 1417, 1215, 1346, 1382, 1478, 1333, 1357),
1654=> array(1418, 1347, 1383, 1479, 1334, 1358),
1655=> array(1233, 1419, 1216, 1348, 1384, 1480, 1335, 1359),
1656=> array(1420, 1349, 1481, 1336, 1385, 1360),
1657=> array(1234, 1421, 1217, 1386, 1482, 1337, 1361, 1350),
1658=> array(1426, 1483, 1338, 1387, 1395, 1351),
1659=> array(1235, 1575, 1218, 1388, 1352, 1484, 1339, 1362),
1660=> array(1236, 1219, 1389, 1504, 1353, 1485, 1340, 1363),
1661=> array(1397, 1505, 1486, 1364),
1662=> array(1237, 1521, 1487, 1488, 1396),
1625=> array(1311),
1626=> array(1312),
1627=> array(1313),
1628=> array(1314),
1629=> array(1448, 1315),
1630=> array(1449, 1316),
1631=> array(1450, 1318, 1317),
1632=> array(1451, 1319),
1633=> array(1452, 1320),
1634=> array(1453,  1322, 1321),
1635=> array(1454, 1323),
1636=> array(1455, 1324),
1637=> array(1456, 1326, 1325),
1638=> array(1457, 1327),
1639=> array(1458, 1328),
1640=> array(1464, 1465),
1641=> array(1459, 1329),
1642=> array(1466, 1467),
1643=> array(1460),
1644=> array(),
1645=> array(1461),
1646=> array(1462),
1647=> array(1463),
1695=> array(),
1696=> array(561, 1511),
1697=> array(562),
1698=> array(1506, 1246, 1512),
1699=> array(1507, 1247, 1513),
1700=> array(1508, 1248, 1514, 1248),
1701=> array(655, 1249),
1702=> array(659),
1703=> array(681),
1704=> array(796),
1705=> array(1250),
1706=> array(1251),
1707=> array(1252),
1708=> array(1253),
1709=> array(1254),
1710=> array(1255),
1711=> array(1256),
1712=> array(1257),
1713=> array(1258),
1714=> array(1259),
1715=> array(1260),
1716=> array(1274),
1717=> array(1261),
1718=> array(1262),
1719=> array(1263),
1720=> array(1264),
1721=> array(1265),
1722=> array(1266),
1723=> array(1267),
1724=> array(1268),
1725=> array(1269),
1726=> array(1270),
1727=> array(1271),
1728=> array(1272),
1729=> array(1273),
1730=> array(1509),
1731=> array(1510),
1732=> array(1576),
1733=> array(1577),
1734=> array(1578),
1735=> array(1579),
1736=> array(1580),
1737=> array(1581),
1738=> array(1582),
1739=> array(1583),
1741=> array(1238),
1742=> array(1239),
1743=> array(1240),
1744=> array(1241),
1745=> array(1242),
1746=> array(1243),
1748=> array(653, 1244),
1750=> array(654,1245),
1752=> array(660, 1589),
1754=> array(1275),
1756=> array(1276),
1758=> array(1277),
1760=> array(1278),
1762=> array(1279, 1591),
1764=> array(1280, 1604),
1766=> array(1281, 1605),
1768=> array(1606),
1770=> array(1590, 1607),
1772=> array(1608),
1774=> array(1609)
);
	foreach ($Migrate as $new => $old){
		foreach ($old as $option_value_id){
			$this->query("select option_id into @x from option_value where option_value_id=$new; 
			update product_option_value POV, product_option PO set POV.option_id=@x, 
			POV.option_value_id=$new, PO.option_id=@x where 
			POV.option_value_id=$option_value_id and PO.product_option_id=POV.product_option_id ;");
    	}
	}
	}
}
