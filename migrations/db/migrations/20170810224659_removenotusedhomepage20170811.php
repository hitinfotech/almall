<?php

use Phinx\Migration\AbstractMigration;

class Removenotusedhomepage20170811 extends AbstractMigration {

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $this->query("SET SESSION sql_mode = '';DELETE FROM homepage_new WHERE page_id IS NULL;DELETE FROM homepage_new WHERE page_id <=0;");
        $this->query("SET SESSION sql_mode = '';DELETE FROM product_land WHERE page_id IS NULL;DELETE FROM product_land WHERE page_id <=0;");
    }

    public function down() {
        
    }

}
