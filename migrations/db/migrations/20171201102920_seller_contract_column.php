<?php

use Phinx\Migration\AbstractMigration;

class SellerContractColumn extends AbstractMigration
{
    public function up()
    {

      $this->query("ALTER TABLE customerpartner_to_customer ADD COLUMN contract_path varchar(255)");

    }
}
