<?php

use Phinx\Migration\AbstractMigration;

class Special extends AbstractMigration {

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $this->query(" SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'product_discount' AND table_schema = DATABASE() AND column_name = 'country_id') > 0, \"ALTER TABLE `product_discount` DROP `country_id` \", \"SELECT 1\"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
        $this->query(" SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'product_special' AND table_schema = DATABASE() AND column_name = 'country_id') > 0, \"ALTER TABLE `product_special` DROP `country_id` \", \"SELECT 1\"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
        
        //newsletter
        $this->query(" SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ne_draft' AND table_schema = DATABASE() AND column_name = 'regency') > 0, \"SELECT 1\", \"ALTER TABLE `ne_draft` ADD `regency` int(1) default 0\"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
        $this->query(" SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ne_draft' AND table_schema = DATABASE() AND column_name = 'frequency') > 0, \"SELECT 1\",\"ALTER TABLE `ne_draft` ADD `frequency` int(1) default 0\"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
        $this->query(" SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ne_draft' AND table_schema = DATABASE() AND column_name = 'life_time_value') > 0, \"SELECT 1\",\"ALTER TABLE `ne_draft` ADD `life_time_value` int(1) default 0\"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
        $this->query(" SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ne_draft' AND table_schema = DATABASE() AND column_name = 'visited_category') > 0, \"SELECT 1\",\"ALTER TABLE `ne_draft` ADD `visited_category` text\"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
        $this->query(" SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ne_draft' AND table_schema = DATABASE() AND column_name = 'purchased_category') > 0, \"SELECT 1\",\"ALTER TABLE `ne_draft` ADD `purchased_category` text\"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
        $this->query(" SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ne_draft' AND table_schema = DATABASE() AND column_name = 'defined_category') > 0, \"SELECT 1\",\"ALTER TABLE `ne_draft` ADD `defined_category` text\"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
        $this->query(" SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ne_draft' AND table_schema = DATABASE() AND column_name = 'newsletter_id') > 0, \"SELECT 1\",\"ALTER TABLE `ne_draft` ADD `newsletter_id` int\"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
        
        $this->query(" ALTER TABLE `emails_chunk` charset utf8 engine innodb collate utf8_general_ci;");
        $this->query(" ALTER TABLE `emails_campaign` charset utf8 engine innodb collate utf8_general_ci;");
        
        
    }

    public function down() {
        
    }

}
