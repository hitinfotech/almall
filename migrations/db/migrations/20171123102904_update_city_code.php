<?php

use Phinx\Migration\AbstractMigration;

class UpdateCityCode extends AbstractMigration
{
    public function up()
    {

      $this->query("UPDATE zone set firstflight_destination_code='BAH', naqel_destination_code='BAH' , postaplus_code='BAH' WHERE country_id=17");
      $this->query("UPDATE zone set firstflight_destination_code='MMC', naqel_destination_code='MMC' , postaplus_code='MMC' WHERE country_id=161");
      $this->query("UPDATE zone set firstflight_destination_code='MCT', naqel_destination_code='MCT' , postaplus_code='MCT' WHERE country_id=161 AND code='MA' ");
      $this->query("INSERT INTO zone set country_id=161, name_old='Salalah' , code='SLL' , status=1, available=1 ,firstflight_destination_code='SLL', naqel_destination_code='SLL' , postaplus_code='SLL' ");
      $this->query("INSERT INTO zone_description (zone_id, language_id, name)  SELECT zone_id,'1','Salalah' from zone WHERE country_id=161 AND name_old='Salalah'");
      $this->query("INSERT INTO zone_description (zone_id, language_id, name)  SELECT zone_id,'2','صلالة' from zone WHERE country_id=161 AND name_old='Salalah'");

    }
}
