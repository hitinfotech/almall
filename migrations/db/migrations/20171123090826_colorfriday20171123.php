<?php

use Phinx\Migration\AbstractMigration;

class Colorfriday20171123 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
$this->query("SET sql_mode=''; SET NAMES utf8; alter table landing_pages add color varchar(15) after id;alter table landing_pages add background varchar(15) after color;alter table landing_pages add hbackground varchar(15) after color;alter table landing_pages add hcolor varchar(15) after color;alter table landing_pages add icon varchar(255) after id;alter table landing_pages add fontbold tinyint(1) after icon;");
    }
    public function down(){
        
    }
}
