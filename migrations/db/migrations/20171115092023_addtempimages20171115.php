<?php

use Phinx\Migration\AbstractMigration;

class Addtempimages20171115 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $this->query("SET sql_mode=''; SET NAMES utf8; "
                . " CREATE TABLE IF NOT EXISTS `product_image_add` ( "
                . " `product_image_id` int(11) NOT NULL AUTO_INCREMENT, "
                . " `product_id` int(11) NOT NULL, "
                . " `session_id` varchar(40) NOT NULL, "
                . " `image` varchar(255) DEFAULT NULL, "
                . " `sort_order` int(3) NOT NULL DEFAULT '0', "
                . " PRIMARY KEY (`product_image_id`), "
                . " KEY `product_id` (`product_id`) "
                . " ) ENGINE=InnoDB DEFAULT CHARSET=utf8 "
                . "");
    }

    public function down() {
        
    }

}