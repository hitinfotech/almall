<?php

use Phinx\Migration\AbstractMigration;

class AddSalesManagoPopup extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->insert('dynamic_values', [
            'code' => 'salesmanago',
            'key' => 'popup_title',
            'value' => 'Get Exclusive offers',
            'language_id' => 1,
        ]);
        $this->insert('dynamic_values', [
            'code' => 'salesmanago',
            'key' => 'popup_description',
            'value' => 'To Be updated with our latest offers before anyone else, Click Yes!',
            'language_id' => 1,
        ]);
        $this->insert('dynamic_values', [
            'code' => 'salesmanago',
            'key' => 'popup_yes',
            'value' => 'Yes',
            'language_id' => 1,
        ]);
        $this->insert('dynamic_values', [
            'code' => 'salesmanago',
            'key' => 'popup_no',
            'value' => 'No',
            'language_id' => 1,
        ]);
        $this->insert('dynamic_values', [
            'code' => 'salesmanago',
            'key' => 'popup_image',
            'value' => '',
            'language_id' => 1,
        ]);


        $this->insert('dynamic_values', [
            'code' => 'salesmanago',
            'key' => 'popup_title',
            'value' => 'احصل على العروض الحصرية',
            'language_id' => 2,
        ]);
        $this->insert('dynamic_values', [
            'code' => 'salesmanago',
            'key' => 'popup_description',
            'value' => 'للحصول على أحدث عروضنا قبل أي شخص آخر، انقر فوق نعم',
            'language_id' => 2,
        ]);
        $this->insert('dynamic_values', [
            'code' => 'salesmanago',
            'key' => 'popup_yes',
            'value' => 'نعم',
            'language_id' => 2,
        ]);
        $this->insert('dynamic_values', [
            'code' => 'salesmanago',
            'key' => 'popup_no',
            'value' => 'لا',
            'language_id' => 2,
        ]);
        $this->insert('dynamic_values', [
            'code' => 'salesmanago',
            'key' => 'popup_image',
            'value' => '',
            'language_id' => 2,
        ]);

    }
}
