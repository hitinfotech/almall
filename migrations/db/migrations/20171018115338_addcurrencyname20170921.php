<?php

use Phinx\Migration\AbstractMigration;

class Addcurrencyname20170921 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    
    
    
    
    
    
    
    
    public function up() {
        $sql = "SET sql_mode=''; SET NAMES UTF8;"
                . "CREATE TABLE IF NOT EXISTS `currency_description` ( "
                . "`currency_id` int(11) NOT NULL,"
                . "`language_id` int(11) NOT NULL,"
                . "`name` varchar(255) NOT NULL DEFAULT '',"
                . " PRIMARY KEY (`currency_id`,`language_id`) "
                . " ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
                . "INSERT INTO currency_description SELECT currency_id , 1 , '' FROM currency;"
                . "INSERT INTO currency_description SELECT currency_id , 2 , '' FROM currency;";
        $this->query($sql);
    }
    
    public function down()
    {

    }
}
