<?php

use Phinx\Migration\AbstractMigration;

class AdminCartTable extends AbstractMigration
{
    public function up()
    {

      $this->query("
      CREATE TABLE `admin_cart` (
        `cart_id` int(11) NOT NULL AUTO_INCREMENT,
        `country_id` int(11) DEFAULT NULL,
        `customer_id` int(11) DEFAULT NULL,
        `session_id` varchar(32) NOT NULL,
        `product_id` int(11) NOT NULL,
        `recurring_id` int(11) NOT NULL,
        `option` text NOT NULL,
        `quantity` int(5) NOT NULL,
        `date_added` datetime NOT NULL,
        `otp_id` int(11) NOT NULL,
        `swap_id` int(11) NOT NULL,
        `op_size` varchar(10) DEFAULT NULL,
        `isadmin` tinyint(4) NOT NULL DEFAULT '0',
        PRIMARY KEY (`cart_id`),
        KEY `cart_id` (`customer_id`,`session_id`,`product_id`,`recurring_id`),
        KEY `session_id` (`session_id`),
        KEY `product_id` (`product_id`),
        KEY `recurring_id` (`recurring_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
      ");

    }
}
