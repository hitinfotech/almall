<?php

use Phinx\Migration\AbstractMigration;

class AddingDeletedColumn extends AbstractMigration
{
    public function up()
    {
      $this->query("ALTER TABLE `customerpartner_to_order` add column deleted enum('0','1') default '0' ");
      $this->query("ALTER TABLE `customerpartner_to_order` add column deleting_user_id int(11) default 0 ");
      $this->query("ALTER TABLE `customerpartner_to_order` add column deleting_date datetime ");

      $this->query("ALTER TABLE `order` add column deleted enum('0','1') default '0' ");
      $this->query("ALTER TABLE `order` add column deleting_user_id int(11) default 0 ");
      $this->query("ALTER TABLE `order` add column deleting_date datetime ");

      $this->query("ALTER TABLE `order_product` add column deleted enum('0','1') default '0' ");
      $this->query("ALTER TABLE `order_product` add column deleting_user_id int(11) default 0 ");
      $this->query("ALTER TABLE `order_product` add column deleting_date datetime ");

      $this->query("ALTER TABLE `order_option` add column deleted enum('0','1') default '0' ");
      $this->query("ALTER TABLE `order_option` add column deleting_user_id int(11) default 0 ");
      $this->query("ALTER TABLE `order_option` add column deleting_date datetime ");

      $this->query("ALTER TABLE `order_voucher` add column deleted enum('0','1') default '0' ");
      $this->query("ALTER TABLE `order_voucher` add column deleting_user_id int(11) default 0 ");
      $this->query("ALTER TABLE `order_voucher` add column deleting_date datetime ");

      $this->query("ALTER TABLE `order_total` add column deleted enum('0','1') default '0' ");
      $this->query("ALTER TABLE `order_total` add column deleting_user_id int(11) default 0 ");
      $this->query("ALTER TABLE `order_total` add column deleting_date datetime ");

      $this->query("ALTER TABLE `order_history` add column deleted enum('0','1') default '0' ");
      $this->query("ALTER TABLE `order_history` add column deleting_user_id int(11) default 0 ");
      $this->query("ALTER TABLE `order_history` add column deleting_date datetime ");

      $this->query("ALTER TABLE `customerpartner_to_order_status` add column deleted enum('0','1') default '0' ");
      $this->query("ALTER TABLE `customerpartner_to_order_status` add column deleting_user_id int(11) default 0 ");
      $this->query("ALTER TABLE `customerpartner_to_order_status` add column deleting_date datetime ");

      $this->query("ALTER TABLE `customerpartner_sold_tracking` add column deleted enum('0','1') default '0' ");
      $this->query("ALTER TABLE `customerpartner_sold_tracking` add column deleting_user_id int(11) default 0 ");
      $this->query("ALTER TABLE `customerpartner_sold_tracking` add column deleting_date datetime ");

    }
}
