<?php

use Phinx\Migration\AbstractMigration;

class AddingNewStatus extends AbstractMigration
{

    public function up()
    {
      $this->query("INSERT INTO order_status set order_status_id=22, language_id=1, name='Invoiced',quantity='subtract', stock='in'");
      $this->query("INSERT INTO order_status set order_status_id=22, language_id=2, name='Invoiced',quantity='subtract', stock='in'");
    }
}
