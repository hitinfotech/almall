<?php

use Phinx\Migration\AbstractMigration;

class AddingNewDynamicValues extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
      $this->query("INSERT INTO dynamic_values (`code`,`key`,`value`) VALUES ('account_manager','0',''),('account_manager','3','Murad Abdallah'),('account_manager','19','Silvia Rodriguez'),('account_manager','38','Munirah Alqahtani')");
    }
}
