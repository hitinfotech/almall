<?php

use Phinx\Migration\AbstractMigration;

class AddGust extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {

        $this->query("alter table customer_abandonmentcarts modify COLUMN  customer_id int(11) ");
        $this->query("alter table customer_abandonmentcarts add COLUMN  guest_session varchar(50)");
        $this->query("alter table abandonmentcarts add COLUMN  guest_session varchar(50)");
    }
}
