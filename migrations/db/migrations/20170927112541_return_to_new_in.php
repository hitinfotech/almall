<?php

use Phinx\Migration\AbstractMigration;

class ReturnToNewIn extends AbstractMigration
{

    public function up()
    {
      $this->query("UPDATE url_alias_en set keyword='New-in' WHERE query ='search/ramadan' ");
      $this->query(" UPDATE url_alias_ar set keyword='وصل-حديثا' WHERE query ='search/ramadan' ");

    }
}
