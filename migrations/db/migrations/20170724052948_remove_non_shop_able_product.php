<?php

use Phinx\Migration\AbstractMigration;

class RemoveNonShopAbleProduct extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->query("delete pd from wk_rma_product pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from wk_custom_field_product_options pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from wk_custom_field_product pd,product p WHERE p.product_id = pd.productId and p.stock_status_id = 10;");
//        $this->query("delete pd from tmpx_product_prices pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
//        $this->query("delete pd from tmp_product_special pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
//        $this->query("delete pd from tmp_product_prices_20170521 pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
//        $this->query("delete pd from tmp_product_prices20170525 pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
//        $this->query("delete pd from tmp_product_latest pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
//        $this->query("delete pd from tmp_product_20170604 pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
//        $this->query("delete pd from tmp_product_20170525 pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
//        $this->query("delete pd from tmp_product_20170522 pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from tip_product pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from tag_product pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from selected_products pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_wear pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_to_store pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_to_layout pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_to_group pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_to_download pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_to_country pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_to_category pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_special pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_reward pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_related pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_recurring pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_prices pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_option_value pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_option pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_offers pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_latest pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_landing_blocks pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_image pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_fulltext pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_filter pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_discount pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_description pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_color pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from product_attribute pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from order_product_temp pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from order_product pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from order_package_product pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from my_product_image pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from mother_day_selected_products pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from look_product pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from featured_pages_selected_products pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from customerpartner_to_product pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from coupon_product pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from cms_homepage_top_products pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from cms_homepage_bottom_products pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");


        $this->query("delete pd from cms_homepage_bottom_products pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete pd from cms_homepage_bottom_products pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;");
        $this->query("delete p from product p WHERE p.stock_status_id = 10;");








    }
}
//select count(pd.product_id) from product_image pd LEFT JOIN product p ON ( p.product_id = pd.product_id) where p.stock_status_id = 10;
//delete pd from wk_rma_product pd,product p WHERE p.product_id = pd.product_id and p.stock_status_id = 10;