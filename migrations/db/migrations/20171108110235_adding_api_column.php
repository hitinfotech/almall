<?php

use Phinx\Migration\AbstractMigration;

class AddingApiColumn extends AbstractMigration
{

    public function up()
    {

      $this->query("ALTER TABLE brand ADD COLUMN show_api enum('yes','no') default 'yes'");

    }
}
