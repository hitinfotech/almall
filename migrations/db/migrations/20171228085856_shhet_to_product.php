<?php

use Phinx\Migration\AbstractMigration;

class ShhetToProduct extends AbstractMigration
{
    public function up()
    {

      $this->query("
          CREATE TABLE `sheet_to_product` (
         `sheet_id` int(11) NOT NULL,
         `product_id` int(11) NOT NULL,
         PRIMARY KEY (`sheet_id`,`product_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
      ");

      $this->query("
          CREATE TABLE `sheet_enabling_track` (
         `sheet_track_id` int(11) NOT NULL AUTO_INCREMENT,
         `sheet_id` int(11) NOT NULL,
         `status` enum('enabled' , 'disabled') DEFAULT NULL,
         `user_id` int(11) DEFAULT NULL,
         `date_added` datetime DEFAULT NULL,
         PRIMARY KEY (`sheet_track_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
      ");



    }
}
