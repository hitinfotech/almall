<?php

use Phinx\Migration\AbstractMigration;

class Preventdeleteproductoptatt20171026 extends AbstractMigration {

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        /*
        $this->query("SET sql_mode=''; SET NAMES utf8; "
                . "drop table if exists tmp_qasim;"
                . "create table tmp_qasim select count(*)  cnt, product_id,option_id from product_option group by product_id,option_id  having count(*)>1;"
                . "update tmp_qasim T , product_option O set T.cnt=O.product_option_id where T.product_id=O.product_id and T.option_id=O.option_id;"
                . "delete O from product_option O, tmp_qasim T where T.product_id=O.product_id and T.option_id=O.option_id and T.cnt!=O.product_option_id;"
                . "drop table if exists tmp_qasim;"
                . "create table tmp_qasim select count(*)  cnt, product_option_id, product_id, option_id, option_value_id from product_option_value group by product_option_id, product_id, option_id, option_value_id  having count(*)>1;"
                . "update tmp_qasim T , product_option_value O set T.cnt=O.product_option_value_id where T.product_option_id=O.product_option_id and T.product_id=O.product_id and T.option_id=O.option_id and T.option_value_id=O.option_value_id;"
                . "delete O from product_option_value O, tmp_qasim T where T.product_option_id and T.product_id=O.product_id and T.option_id=O.option_id and T.option_value_id=O.option_value_id and T.cnt!=O.product_option_id;"
                . "");
        */
        $this->query("SET sql_mode=''; SET NAMES utf8; "
                . " ALTER TABLE product_attribute ADD deleted INT NOT NULL DEFAULT 0;"
                . " ALTER TABLE product_attribute ADD CONSTRAINT `unique_product_attribute` UNIQUE (product_id,attribute_id, language_id); "
                . " ALTER TABLE product_option ADD deleted INT NOT NULL DEFAULT 0; "
                . " ALTER TABLE product_option_value ADD deleted INT NOT NULL DEFAULT 0; "
                . " ALTER TABLE product_option ADD CONSTRAINT `unique_product_option` UNIQUE (product_id,option_id); "
                . " ALTER TABLE product_option_value ADD CONSTRAINT `unique_product_option_value` UNIQUE (product_option_id, product_id, option_id, option_value_id ); "
                . "");
    }

    public function down() {
        
    }

}
