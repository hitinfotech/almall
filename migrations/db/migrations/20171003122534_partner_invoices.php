<?php

use Phinx\Migration\AbstractMigration;

class PartnerInvoices extends AbstractMigration
{

    public function up()
    {

      $this->query("
       CREATE TABLE `customerpartner_to_invoice` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `customer_id` int(11) NOT NULL,
          `month` int(11) NOT NULL,
          `year` int(11) NOT NULL,
          `invoice_name` varchar(100) NOT NULL,
          `user_id` int(11) NOT NULL,
          `date_added` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
      );

      $this->query("
      ALTER TABLE customerpartner_to_order add column invoice_name varchar(255)
      ");

    }
}
