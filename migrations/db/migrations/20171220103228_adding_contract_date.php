<?php

use Phinx\Migration\AbstractMigration;

class AddingContractDate extends AbstractMigration
{
    public function up()
    {
      $this->query("Alter table customerpartner_to_customer add column contract_date datetime");

    }
}
