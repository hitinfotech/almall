<?php

use Phinx\Migration\AbstractMigration;

class PhoneColumn extends AbstractMigration
{
    public function up()
    {
      $this->query("ALTER table country add column telephone varchar(30) after telecode");
      $this->query("UPDATE country set telephone='97145588932' ");
      $this->query("UPDATE country set telephone='8004330033' WHERE country_id in (184, 221) ");

    }
}
