<?php

use Phinx\Migration\AbstractMigration;

class ModifyMainStoreCOLUMNsBrandsCategories extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addCOLUMN
     *    renameCOLUMN
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->query("SET sql_mode=''; SET names utf8; ALTER TABLE main_store modify COLUMN related_brand VARCHAR (225) DEFAULT NULL;");
        $this->query("SET sql_mode=''; SET names utf8; ALTER TABLE main_store modify COLUMN related_category VARCHAR(225) DEFAULT NULL; ");

    }

    public function change()
    {

    }
}
