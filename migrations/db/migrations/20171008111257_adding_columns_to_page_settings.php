<?php

use Phinx\Migration\AbstractMigration;

class AddingColumnsToPageSettings extends AbstractMigration
{

    public function up()
    {
      $this->query("DROP INDEX page_id ON page_settings");
      $this->query("DROP INDEX code ON featured_pages_blocks");
      $this->query("DROP INDEX code ON featured_pages_selected_products");
      $this->query("alter table page_settings add column parent_page_id int(11) after page_settings_id");
      $this->query("alter table page_settings add column seo_url  varchar(100) not null after gender");
      $this->query("alter table page_settings add column page_title  varchar(255) not null after gender");
      $this->query("alter table page_settings add column page_h1 varchar(255) not null after gender");
      $this->query("alter table page_settings add column deleted enum('0','1') default '0' after seo_url");
      $this->query("alter table product_land  add column deleted enum('0','1') default '0' ");
      $this->query("alter table featured_pages_selected_products   add column deleted enum('0','1') default '0' after language_id");
      $this->query("alter table featured_pages_blocks   add column deleted enum('0','1') default '0' after language_id");

    }
}
