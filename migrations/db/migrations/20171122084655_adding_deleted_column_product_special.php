<?php

use Phinx\Migration\AbstractMigration;

class AddingDeletedColumnProductSpecial extends AbstractMigration
{
    public function up()
    {

      $this->query("ALTER TABLE product_special add column deleted enum('yes','no') default 'no'");
      $this->query("ALTER TABLE product_special add column deleted_by int(11) default 0");
      $this->query("ALTER TABLE product_special add column deleted_date datetime");

    }
}
