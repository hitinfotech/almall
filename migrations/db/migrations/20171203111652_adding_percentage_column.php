<?php

use Phinx\Migration\AbstractMigration;

class AddingPercentageColumn extends AbstractMigration
{
    public function up()
    {
      $this->query("ALTER TABLE product_special ADD COLUMN percentage decimal(15,4) after price ");

    }
}
