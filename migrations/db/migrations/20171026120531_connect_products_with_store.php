<?php

use Phinx\Migration\AbstractMigration;

class ConnectProductsWithStore extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    public function up()
    {
        $this->query("INSERT INTO product_to_store (product_id,store_id)  
                          SELECT P.product_id,0 
                          FROM product P 
                          LEFT JOIN product_to_store S ON P.product_id=S.product_id 
                          WHERE S.product_id IS NULL;");

    }

}
