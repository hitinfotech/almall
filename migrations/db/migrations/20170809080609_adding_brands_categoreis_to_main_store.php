<?php

use Phinx\Migration\AbstractMigration;

class AddingBrandsCategoreisToMainStore extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
      $this->query("alter table main_store add related_brand int(11) after city_id");
      $this->query("alter table main_store add show_related_brand int(11) after city_id");
      $this->query("alter table main_store add related_category int(11) after related_brand");
      $this->query("alter table main_store add show_related_category int(11) after related_brand");


    }
}
