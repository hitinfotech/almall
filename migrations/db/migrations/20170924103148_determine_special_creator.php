<?php

use Phinx\Migration\AbstractMigration;

class DetermineSpecialCreator extends AbstractMigration
{
    public function up()
    {
      $this->query("alter table product_special add column  generated_by enum ('product_form', 'admin_mass','seller_form','seller_mass','xls_to_product','nationalize') ");
      $this->query("alter table product_special add column  user_id int(11) ");
    }
}
