<?php

use Phinx\Migration\AbstractMigration;

class AddShipCompanyToUser extends AbstractMigration
{

    public function up()
    {
      $this->query("ALTER table user add column access_to_ship_company int(11) NOT NULL DEFAULT '-1'");

    }
}
