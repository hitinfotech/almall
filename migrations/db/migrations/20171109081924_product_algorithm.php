<?php

use Phinx\Migration\AbstractMigration;

class ProductAlgorithm extends AbstractMigration
{
    public function up()
    {

      $this->query("SET sql_mode='';");
      $this->query("SET NAMES utf8;");
      $this->query("
      CREATE TABLE IF NOT EXISTS `product_ranking_algorithm` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `code` varchar(255) NOT NULL,
        `from` float(11,2) DEFAULT '0',
        `to` float(11,2) DEFAULT '0',
        `value` float(11,2) DEFAULT '0',
        `user_added` int(11) NOT NULL,
        `user_modified` int(11) NOT NULL,
        `deleted` enum('yes','no'),
        `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
      ");

    }
}
