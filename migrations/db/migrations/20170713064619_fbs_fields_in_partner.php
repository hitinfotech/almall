<?php

use Phinx\Migration\AbstractMigration;

class FbsFieldsInPartner extends AbstractMigration
{
  public function  up()
  {
      $this->query(" alter table customerpartner_to_customer add column  `is_fbs` enum('0','1') NOT NULL DEFAULT '0' ");

      $this->query(" alter table customerpartner_to_customer add column fbs_inventory_volume int(11) NOT NULL default 0");
      $this->query(" alter table customerpartner_to_customer add column fbs_inventory_volume_unit int(11) NOT NULL default 0");
      $this->query(" alter table customerpartner_to_customer add column fbs_fullfillment_charges int(11) NOT NULL default 0");
      $this->query(" alter table customerpartner_to_customer add column fbs_storage_charges int(11) NOT NULL default 0");

      $this->query(" alter table customerpartner_to_customer add column fbs_store_location varchar(255) NOT NULL default ''");
      $this->query(" alter table customerpartner_to_customer add column fbs_email varchar(255) NOT NULL default ''");
      $this->query(" alter table customerpartner_to_customer add column fbs_free_storage_days int(11) NOT NULL default 0");


      $this->query(" alter table customerpartner_to_customer add column fbs_date_in date NOT NULL DEFAULT '0000-00-00'");
      $this->query(" alter table customerpartner_to_customer add column fbs_date_out date NOT NULL DEFAULT '0000-00-00'");
  }
}
