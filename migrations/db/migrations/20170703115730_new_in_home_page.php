<?php

use Phinx\Migration\AbstractMigration;

class NewInHomePage extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {

        $this->query("update `url_alias_en` set `keyword` = 'NEW-IN' WHERE url_alias_id = 127776;");
        $this->query("update `url_alias_ar` set `keyword` = 'وصل-حديثا' WHERE url_alias_id = 127785;");



    }
}
