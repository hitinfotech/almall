<?php

use Phinx\Migration\AbstractMigration;

class Addalgoliafield20170914 extends AbstractMigration {

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $this->query("SET sql_mode=''; SET names utf8; ALTER TABLE product ADD algolia INT NOT NULL DEFAULT 0;create index algolia_index on product(algolia);");
    }

    public function down() {
        
    }

}
