<?php

use Phinx\Migration\AbstractMigration;

class SiteMapUrl extends AbstractMigration
{

    public function up()
    {
      $this->query("INSERT INTO url_alias_en SET query = 'information/sitemap', keyword = 'sitemap'");
      $this->query("INSERT INTO url_alias_ar SET query = 'information/sitemap', keyword = 'sitemap'");

    }
}
