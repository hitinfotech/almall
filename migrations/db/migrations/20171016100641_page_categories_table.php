<?php

use Phinx\Migration\AbstractMigration;

class PageCategoriesTable extends AbstractMigration
{

    public function up()
    {
        $this->query("SET sql_mode='';");
      $this->query("SET NAMES utf8;");
      $this->query("
      
      CREATE TABLE `landing_page_to_category` (
        `landing_page_id` int(11) NOT NULL ,
        `category_id` int(11) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
      ");

      $this->query("
      CREATE TABLE `landing_page_to_brand` (
        `landing_page_id` int(11) NOT NULL ,
        `brand_id` int(11) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
      ");

    }
}
