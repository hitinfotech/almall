<?php

use Phinx\Migration\AbstractMigration;

class ProductUploadSheet extends AbstractMigration
{
    public function up()
    {

      $this->query("
        CREATE TABLE `product_upload_sheet` (
          `sheet_id` int(11) NOT NULL AUTO_INCREMENT,
          `seller_id` int(11) DEFAULT NULL,
          `currency_id` int(11) DEFAULT NULL,
          `file_name`  varchar(100) DEFAULT NULL,
          `user_id` int(11) DEFAULT NULL,
          `date_added` datetime DEFAULT NULL,
          `last_modified_id` int(11) DEFAULT NULL,
          `date_modified` datetime DEFAULT NULL,
          PRIMARY KEY (`sheet_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
      ");

    }
}
