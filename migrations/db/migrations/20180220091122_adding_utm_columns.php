<?php

use Phinx\Migration\AbstractMigration;

class AddingUtmColumns extends AbstractMigration
{
    public function up()
    {
      $this->query("SET sql_mode= ''");
      $this->query("SET NAMES utf8");

      $this->query("ALTER TABLE customer_views ADD column utm_source varchar(50) ");
      $this->query("ALTER TABLE customer_views ADD column utm_medium varchar(50) ");
      $this->query("ALTER TABLE customer ADD column utm_source varchar(50) ");
      $this->query("ALTER TABLE customer ADD column utm_medium varchar(50) ");

    }
}
