<?php

use Phinx\Migration\AbstractMigration;

class SellerDiscountsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
      $this->query("SET sql_mode=''; CREATE TABLE IF NOT EXISTS `seller_discounts` (
       `seller_discount_id` int(11) NOT NULL AUTO_INCREMENT,
       `discount_id` int(11) NOT NULL,
       `product_special_id` int(11) NOT NULL,
       `seller_id` int(11) NOT NULL,
       `brand_id` int(11) NOT NULL,
       `percentage` int(11) NOT NULL,
       `date_added` TIMESTAMP NOT NULL DEFAULT 0,
       `date_modified` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
       PRIMARY KEY (`seller_discount_id`)
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ");
    }
}
