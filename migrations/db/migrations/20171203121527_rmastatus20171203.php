<?php

use Phinx\Migration\AbstractMigration;

class Rmastatus20171203 extends AbstractMigration {

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $this->query("SET sql_mode= ''");
        $this->query("SET NAMES utf8");
        
        $this->query("INSERT INTO order_status SET order_status_id = 23, language_id = 1, name='Return Processing', quantity = 'add', stock='out', attention_time='5', warning_time='48' ON DUPLICATE KEY UPDATE name='Return Processing', quantity = 'add', stock='out', attention_time='5', warning_time='48' ");
        $this->query("INSERT INTO order_status SET order_status_id = 23, language_id = 2, name='Return Processing', quantity = 'add', stock='out', attention_time='5', warning_time='48' ON DUPLICATE KEY UPDATE name='Return Processing', quantity = 'add', stock='out', attention_time='5', warning_time='48' ");
        
        
        $this->query(" CREATE TABLE IF NOT EXISTS `rma_status` ( 
            `rma_status_id` int(11) NOT NULL AUTO_INCREMENT,
            `language_id` int(11) NOT NULL,
            `name` varchar(32) NOT NULL,
            `attention_time` int(11) NOT NULL DEFAULT '0',
            `warning_time` int(11) NOT NULL DEFAULT '0',
            `quantity` enum('add','subtract','purge') NOT NULL DEFAULT 'subtract',
            `stock` enum('in','out') NOT NULL DEFAULT 'in',
            `color` varchar(15),
            `partner` enum('yes','no') not null default 'no',
            `order_status_id` INT ,
            PRIMARY KEY (`rma_status_id`,`language_id`),
            KEY `language_id` (`language_id`),
            KEY `name` (`name`),
            FOREIGN KEY (order_status_id) REFERENCES order_status(order_status_id) 
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 
             ");
        
        // 0 old pending
        $this->query(" INSERT INTO rma_status SET rma_status_id=1, language_id=1, name='Return request pending', order_status_id = '20', color='blue', quantity='subtract', partner='no', stock='out' ");
        $this->query(" INSERT INTO rma_status SET rma_status_id=1, language_id=2, name='Return request pending', order_status_id = '20', color='blue', quantity='subtract', partner='no', stock='out' ");
        
        // 1 old accept
        $this->query(" INSERT INTO rma_status SET rma_status_id=2, language_id=1, name='Return accepted', order_status_id = '23', color='red', quantity='subtract', partner='no', stock='out' ");
        $this->query(" INSERT INTO rma_status SET rma_status_id=2, language_id=2, name='Return accepted', order_status_id = '23', color='red', quantity='subtract', partner='no', stock='out' ");
        
        // 2 old Shipped
        $this->query(" INSERT INTO rma_status SET rma_status_id=3, language_id=1, name='Shipped to Seller', order_status_id = '23', color='green', quantity='subtract', partner='no', stock='out' ");
        $this->query(" INSERT INTO rma_status SET rma_status_id=3, language_id=2, name='Shipped to Seller', order_status_id = '23', color='green', quantity='subtract', partner='no', stock='out' ");
        
        // 3 old Decline
        $this->query(" INSERT INTO rma_status SET rma_status_id=4, language_id=1, name='Declined by Customer service', order_status_id = '5', color='black', quantity='subtract', partner='no', stock='out' ");
        $this->query(" INSERT INTO rma_status SET rma_status_id=4, language_id=2, name='Declined by Customer service', order_status_id = '5', color='black', quantity='subtract', partner='no', stock='out' ");
        
        // 4 New when seller declined
        $this->query(" INSERT INTO rma_status SET rma_status_id=5, language_id=1, name='Declined by Seller', order_status_id = '5', color='black', quantity='subtract', partner='yes', stock='out' ");
        $this->query(" INSERT INTO rma_status SET rma_status_id=5, language_id=2, name='Declined by Seller', order_status_id = '5', color='black', quantity='subtract', partner='yes', stock='out' ");
        
        //5 New Seller received the item and approved it
        $this->query(" INSERT INTO rma_status SET rma_status_id=6, language_id=1, name='Return completed', order_status_id = '19', color='black', quantity='add', partner='no', stock='in' ");
        $this->query(" INSERT INTO rma_status SET rma_status_id=6, language_id=2, name='Return completed', order_status_id = '19', color='black', quantity='add', partner='no', stock='in' ");
        
        // 6 Return processing
        $this->query(" INSERT INTO rma_status SET rma_status_id=7, language_id=1, name='Return processing', order_status_id = '23', color='black', quantity='subtract', partner='no', stock='out' ");
        $this->query(" INSERT INTO rma_status SET rma_status_id=7, language_id=2, name='Return processing', order_status_id = '23', color='black', quantity='subtract', partner='no', stock='out' ");
        
        $this->query(" ALTER TABLE wk_rma_order CHANGE customer_status customer_status_id INT ; ");
        
        $this->query(" UPDATE wk_rma_order SET customer_status_id = 6 WHERE customer_status_id IN (5, 6, 4); ");
        $this->query(" UPDATE wk_rma_order SET customer_status_id = 4 WHERE customer_status_id IN (3); ");
        $this->query(" UPDATE wk_rma_order SET customer_status_id = 3 WHERE customer_status_id IN (2); ");
        $this->query(" UPDATE wk_rma_order SET customer_status_id = 2 WHERE customer_status_id IN (1); ");
        $this->query(" UPDATE wk_rma_order SET customer_status_id = 1 WHERE customer_status_id IN (0); ");
        
        $this->query(" INSERT INTO setting    SET store_id=0, code='config', `key`='config_rma_status_id', `value`=1,serialized=0; ");
    }

    public function down() {
        
    }
}
