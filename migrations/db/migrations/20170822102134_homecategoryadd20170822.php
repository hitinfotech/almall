<?php

use Phinx\Migration\AbstractMigration;

class Homecategoryadd20170822 extends AbstractMigration {

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $this->query("SET sql_mode=''; UPDATE category SET top=1 WHERE category_id=4;");
        $this->query("SET sql_mode=''; INSERT INTO look_group SET look_group_id=5, sort_order=5,status=1, date_added=now()");
        $this->query("SET sql_mode=''; INSERT INTO look_group_description SET look_group_id=5, language_id=1, name='Home',description='Home'");
        $this->query("SET sql_mode=''; INSERT INTO look_group_description SET look_group_id=5, language_id=2, name='home',description='منزل'");
        $this->query("SET sql_mode=''; UPDATE tip_group_description SET name='Home' WHERE tip_group_id=3 AND language_id= 1");
        $this->query("SET sql_mode=''; UPDATE tip_group_description SET name='منزل' WHERE tip_group_id=3 AND language_id= 2");
        
    }

    public function down() {
        
    }

}
