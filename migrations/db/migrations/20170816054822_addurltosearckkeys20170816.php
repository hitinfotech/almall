<?php

use Phinx\Migration\AbstractMigration;

class Addurltosearckkeys20170816 extends AbstractMigration {

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up() {
        $this->query(" SET @s = (SELECT IF((SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'popular_search' AND table_schema = DATABASE() AND column_name = 'url') > 0, \"SELECT 1\", \"ALTER TABLE `popular_search` ADD url varchar(1024) AFTER keytype \"));PREPARE stmt FROM @s;EXECUTE stmt;DEALLOCATE PREPARE stmt;");
    }

    public function down() {
        
    }

}
