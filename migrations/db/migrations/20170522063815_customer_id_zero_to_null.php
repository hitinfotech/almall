<?php

use Phinx\Migration\AbstractMigration;

class CustomerIdZeroToNull extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->query("SET SESSION sql_mode = ''");
        $this->query("alter table coupon_history modify customer_id int");
        $this->query("update coupon_history set customer_id = null where customer_id = 0");

        $this->query("alter table `order` modify customer_id int");
        $this->query("update `order` set customer_id = null where customer_id = 0");

        $this->query("alter table customer_ip modify customer_id int");
        $this->query("update customer_ip set customer_id = null where customer_id = 0");


        $this->query("alter table customer_views modify customer_id int");
        $this->query("update customer_views set customer_id = null WHERE customer_id = 0");
        
        $this->query("alter table `review` modify customer_id int");
        $this->query("update `review` set customer_id = null WHERE customer_id = 0");


        $this->query("alter table `order` modify customer_id int");
        $this->query("update `order` set customer_id = null WHERE customer_id = 0");


        $this->query("alter table `wk_rma_customer` modify customer_id int");
        $this->query("update `wk_rma_customer` set customer_id = null WHERE customer_id = 0");


        $this->query("alter table `wk_rma_reason` modify customer_id int");
        $this->query("update `wk_rma_reason` set customer_id = null WHERE customer_id = 0");


        $this->query("alter table `customer_activity` modify customer_id int");
        $this->query("update `customer_activity` set customer_id = null WHERE customer_id = 0");

        $this->query("alter table `address` modify customer_id int");
        $this->query("update `address` set customer_id = null WHERE customer_id = 0");


        $this->query("alter table `cart` modify customer_id int");
        $this->query("update `cart` set customer_id = null WHERE customer_id = 0");

        $this->query("alter table `ts_customers` modify customer_id int");
        $this->query("update `ts_customers` set customer_id = null WHERE customer_id = 0");


        // newsletter customer_id primary key
       // $this->query("alter table `newsletter_category_to_customer` modify customer_id int");
       // $this->query("update `newsletter_category_to_customer` set customer_id = null WHERE customer_id = 0");


        $this->query("DROP INDEX customer_id ON  customer_online");
        $this->query("alter table `customer_online` modify customer_id int");
        $this->query("update `customer_online` set customer_id = null WHERE customer_id = 0");

    }
    public function down()
    {
    }
}
