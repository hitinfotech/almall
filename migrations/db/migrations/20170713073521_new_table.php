<?php

use Phinx\Migration\AbstractMigration;

class NewTable extends AbstractMigration
{
    public function up()
    {
      $this->query("CREATE TABLE `dynamic_values` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `code` varchar(255) NOT NULL,
          `key` varchar(255) NOT NULL,
          `value` varchar(255),
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
      );

      $this->query("INSERT INTO dynamic_values (`code`,`key`,`value`) values ('fbs_inventory_volume_unit','1','Square Meter'),('fbs_inventory_volume_unit','2','Square Feet')");
    }
}
