/* Begin iProductVideo */
(function($){
	// Default OpenCart 2.0 Theme
	if (typeof $.magnificPopup !== "undefined") {
		$(window).load(function() {
			$('.thumbnails').data().magnificPopup.callbacks = {
				elementParse: function(item) {

					// Internet
					if (item.src.indexOf('#iproductvideo') >= 0) {
						item.type = 'iframe';
					}

					// Uploaded
					if (item.src.indexOf('#iproductvideo_local') >= 0) {
						item.type = 'inline';

						var rgx = /(?:\.([^.]+))?$/;
						var ext = rgx.exec(item.src.replace("#iproductvideo_local", ""))[1];

						var	video_html = 	'<div class="iproductvideo-popup">';
							video_html +=		'<button title="Close (Esc)" type="button" class="mfp-close">×</button>';
							video_html += 		'<div class="iproductvideo-wrap"><video class="iproductvideo-video" controls>';
							video_html +=			'<source src="' + item.src + '" type="video/' + ext + '">';
							video_html +=			'Your browser does not support the HTML5 video tag.';
							video_html += 		'</video></div>';
							video_html += 	'</div>'

						item.src  = video_html;
					}
				}
			}
		});
	}
})(jQuery);

/* End iProductVideo */