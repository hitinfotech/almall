alter table ne_draft add column gender int(1) default 0 after currency ;
alter table ne_draft add column location int(5) default 0 after gender ;
alter table ne_draft add column defined_product_text varchar(255) after product;

alter table customerpartner_to_customer add column admin_telecode varchar(7) after admin_email;
alter table financial_info              add column fin_telephone_telecode varchar(7)  after telephone;
alter table legal_info                  add column direct_officer_number_telecode varchar(7) after direct_officer_number;
alter table customerpartner_to_customer add column telecode varchar(7) after telephone;
alter table customerpartner_to_customer add trackcode tinyint(1) not null default 0;
alter table customerpartner_to_customer add trackcodeview text not null default '';
alter table customerpartner_to_customer add trackcodeadd  text not null default '';


alter table customer change source source enum('site','facebook','twitter','google','popup','checkout','ticketingsystem');
alter table customer change popup pending int not null default 0 after status;
CREATE TABLE `popup_settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT '0',
  `language_id` int(11) DEFAULT '0',
  `code` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `store_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table country add curcode varchar(3) after language;
alter table product add price decimal(15,4) not null default 0 after location;

alter table customerpartner_to_customer add column admin_name varchar(100) after opening_time;
alter table customerpartner_to_customer add column admin_email varchar(50) after admin_name;
alter table customerpartner_to_customer add column admin_phone int(20) after admin_email;
alter table customerpartner_to_customer add column admin_telecode varchar(7) after admin_email;

CREATE TABLE `pb_bundles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `slug` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `discount_type` tinyint(10) NOT NULL DEFAULT '1',
  `discount_value` decimal(15,2) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `products` text NOT NULL,
  `products_show` text NOT NULL,
  `categories_show` text NOT NULL,
  `store_id` int(11) unsigned NOT NULL,
  `date_available` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `attribute_to_category` (
  `attribute_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`attribute_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `option_to_category` (
  `option_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`option_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `legal_info` (
 `partner_id` int(11) NOT NULL,
 `legal_name` varchar(255) NOT NULL,
 `company_name` varchar(255) DEFAULT NULL,
 `trade_license_number` int(20) DEFAULT NULL,
 `address_license_number` varchar(50) DEFAULT NULL,
 `administrator_contact` varchar(255) NOT NULL,
 `nationality` varchar(32) NOT NULL,
 `passport_num` varchar(20) DEFAULT NULL,
 `email` varchar(96) NOT NULL,
 `direct_officer_number` int(20) DEFAULT NULL,
 `direct_officer_number_telecode` int(6) DEFAULT NULL,
 `telephone` int(20) DEFAULT NULL,
 `telephone_telecode` int(6) DEFAULT NULL,
 `passport` varchar(50) NOT NULL,
 `license` varchar(50) NOT NULL,
 PRIMARY KEY (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `financial_info` (
  `partner_id` int(11) NOT NULL,
  `company_officer_name` varchar(255) NOT NULL,
  `email` varchar(55) NOT NULL,
  `telephone` int(20) NOT NULL,
  `fin_telephone_telecode` int(6) DEFAULT NULL,
  `beneficiary_name` varchar(50) NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `bank_account` varchar(50) DEFAULT NULL,
  `iban_code` varchar(50) DEFAULT NULL,
  `swift_code` varchar(50) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `is_both` tinyint(1) DEFAULT NULL,
  `bank_number` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



alter table customerpartner_to_customer drop isreturn ;
alter table category drop isreturn ;
alter table product drop isreturn ;

alter table customerpartner_to_customer drop return_days ;
alter table category drop return_days ;
alter table product drop return_days ;

alter table customerpartner_to_customer add column available_country int (5) default 0 ;
alter table customerpartner_to_customer add return_policy_id int ;
alter table category add return_policy_id int ;
alter table product add return_policy_id int ;

CREATE TABLE `return_policy` (
  `return_policy_id` int(11) NOT NULL AUTO_INCREMENT,
  `isadmin` int(11) NOT NULL DEFAULT '0',
  `num_days` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`return_policy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `return_policy_description` (
  `return_policy_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(512) NOT NULL DEFAULT '',
  PRIMARY KEY (`return_policy_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


insert into return_policy set return_policy_id=1,isadmin=0,num_days=0,sort_order=0,date_added=now();
insert into return_policy set return_policy_id=2,isadmin=1,num_days=3,sort_order=0,date_added=now();
insert into return_policy set return_policy_id=3,isadmin=1,num_days=14,sort_order=0,date_added=now();

set names utf8;
insert into return_policy_description set return_policy_id=1, language_id=1,name='NO RETURN POLICY', description = 'This product cannot be returned , please contact our Customer Care for more information';
insert into return_policy_description set return_policy_id=1, language_id=2,name='NO RETURN POLICY', description = 'لا يسمح بإرجاع واستبدال هذا المنتج';

insert into return_policy_description set return_policy_id=2, language_id=1,name='Yes - 3 Days', description = '3 days return policy after receiving the item';
insert into return_policy_description set return_policy_id=2, language_id=2,name='Yes - 3 Days', description = 'يسمح بإرجاع المنتج خلال 3 يوم بعد استلامه';

insert into return_policy_description set return_policy_id=3, language_id=1,name='Yes - 14 Days', description = '14 days return policy after receiving the item';
insert into return_policy_description set return_policy_id=3, language_id=2,name='Yes - 14 Days', description = 'يسمح بإرجاع المنتج خلال 14 يوم بعد استلامه';

alter table top_pages add is_english int (1) after top_pages_id;
alter table top_pages add is_arabic int (1) after is_english;

alter table customerpartner_to_customer add column  `instagramid` varchar(255) NOT NULL after facebookid;
alter table customerpartner_to_customer add column  `website` varchar(255) NOT NULL after avatar;
alter table customerpartner_to_customer add column  `warehouse_address` varchar(512)  NOT NULL after address;
alter table customerpartner_to_customer add column  `num_of_branches` int(5)  NOT NULL after telephone;


create table url_alias_ar like url_alias ;
create table url_alias_en like url_alias ;

alter table ne_draft add column utm_campaign varchar(50) after message ;

ALTER TABLE `product_special` ADD cron INT(1) NOT NULL DEFAULT 0 ;
ALTER TABLE `ne_draft` ADD COLUMN utm_campaign varchar(50) after message ;
ALTER TABLE `order` change payment_address_1 payment_address_1 varchar(512);
ALTER TABLE `order` change shipping_address_1 shipping_address_1 varchar(512);

ALTER TABLE wk_rma_order ADD COLUMN  is_confirmed tinyint( 0 ) default 0 ;

ALTER TABLE return_booking ADD COLUMN `seller_id` int(11)   DEFAULT 0 after book_response;
ALTER TABLE return_booking ADD COLUMN `country_id` int(11)   DEFAULT 0 after seller_id;
ALTER TABLE return_booking ADD COLUMN `shipping_company` int(11)   DEFAULT 0 after country_id;
ALTER TABLE return_booking ADD COLUMN `is_closed` tinyint(1)   DEFAULT 0 after shipping_company;
ALTER TABLE return_booking ADD COLUMN `order_id` int(11)   DEFAULT 0 after country_id;

ALTER TABLE product ADD is_return int(1) ;
ALTER TABLE product DROP country_sale_id ;
ALTER TABLE product DROP country_origin_id ;
ALTER TABLE product DROP FOREIGN KEY `product_artical_idx`;
ALTER TABLE product DROP artical_id ;

ALTER TABLE product ADD seller_id INT AFTER product_id;
ALTER TABLE product ADD brand_id INT AFTER seller_id;
ALTER TABLE product ADD order_range int ;
ALTER TABLE product ADD last_mod_id int;
ALTER TABLE product ADD ship_info text AFTER shipping;
ALTER TABLE product ADD user_id int not null default 0;

ALTER TABLE option_description ADD front_name VARCHAR(128);

CREATE TABLE `mother_day_blocks` (
`country_id` int(11) NOT NULL DEFAULT '0',
`language_id` int(11) NOT NULL DEFAULT '0',
`product_id` int(11) NOT NULL DEFAULT '0',
`block_id` int(11) NOT NULL DEFAULT '0',
PRIMARY KEY (`country_id`,`language_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mother_day_selected_products` (
  `country_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `langauge_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`country_id`,`langauge_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `brand_discounts_cronj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `percent` float NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `done` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `return_shipping` (
  `return_shipping_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `ref_no` varchar(50) DEFAULT NULL,
  `AWBNO` varchar(50) DEFAULT NULL,
  `shipping_company` int(11) DEFAULT NULL,
  `service_type` varchar(50) DEFAULT NULL,
  `prod_type` varchar(50) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `rtoshipment` int(11) DEFAULT NULL,
  `veh_type` varchar(50) DEFAULT NULL,
  `pbrand` varchar(50) DEFAULT NULL,
  `ready_date` datetime DEFAULT NULL,
  `closing_time` datetime DEFAULT NULL,
  `is_closed` tinyint(1) DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`return_shipping_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;


CREATE TABLE `product_color` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`related_id`),
  KEY `related_id` (`related_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_latest` (
  `country_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`country_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `welcome_email` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `language_id` int(1) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `type` enum('fd','sd','td') NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

alter table country add sort_order int default 0 not null;
update country set sort_order = 100;
update country set sort_order = 1 where country_id =184 ;
update country set sort_order = 2 where country_id =221 ;
update country set sort_order = 3 where country_id =114 ;
update country set sort_order = 4 where country_id =161 ;
update country set sort_order = 5 where country_id =173 ;
update country set sort_order = 6 where country_id =17 ;

alter table order_shipping add column is_closed tinyint(1) default 0 after closing_time ;
alter table order_booking add column is_closed tinyint(1) default 0 after closing_time ;

CREATE TABLE `ship_company` (
  `ship_company_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `access_no` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `customer_code` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  `last_mod_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ship_company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ship_company_description` (
  `ship_company_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ship_company_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--delete from ship_company;
--delete from ship_company_description;
--drop table  ship_company_to_country;

CREATE TABLE `ship_company_to_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin` varchar(2) NOT NULL,
  `destination` varchar(2) NOT NULL,
  `ship_company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into ship_company set ship_company_id=1, contact_person='Monisha', contact_email='monisha@firstflightme.com', mobile='+971 56 4083511', phone='+971 600545456', access_no = '14028', username = '14028', password='s@ff!p', customer_code = '14028', status = 1, date_added = NOW(), user_id= 4;
insert into ship_company_description set ship_company_id=1, language_id =1, `name`='FirstFlight';
insert into ship_company_description set ship_company_id=1, language_id =2, `name`='FirstFlight';

insert into ship_company set ship_company_id=2, contact_person='Waleed', contact_email='sofan@naqel.com.sa',         mobile='',                phone='',               access_no = '9017927', username = '9017927', password='9017Sayidaty927', customer_code = '9017927', status = 1, date_added = NOW(), user_id= 4;
insert into ship_company_description set ship_company_id=2, language_id =1, `name`='Naqel';
insert into ship_company_description set ship_company_id=2, language_id =2, `name`='Naqel';

insert into ship_company set ship_company_id=3, contact_person='Waleed', contact_email='sofan@naqel.com.sa',         mobile='',                phone='',               access_no = '9017927', username = '9017927', password='9017Sayidaty927', customer_code = '9017927', status = 1, date_added = NOW(), user_id= 4;
insert into ship_company_description set ship_company_id=2, language_id =1, `name`='Naqel Express';
insert into ship_company_description set ship_company_id=2, language_id =2, `name`='Naqel Express';


INSERT INTO ship_company_to_country SET origin='AE',destination='AE',ship_company_id='1'; -- AE= AE = FFC

CREATE TABLE `active_range` (
  `active_range_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(1) DEFAULT NULL,
  `type` enum('booking','shipping') DEFAULT NULL,
  `first_value` int(11) DEFAULT NULL,
  `last_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`active_range_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customer_views` (
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`customer_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table tip_description add column `author` varchar(255) DEFAULT NULL;

alter table order_booking  add column shipping_company int(11) after AWBNO;
alter table order_shipping  add column shipping_company int(11) after AWBNO;

alter table tip add is_arabic int(1) not null default 0 after tip_id;
alter table tip add is_english int(1) not null default 0 after tip_id;
alter table look add is_arabic int(1) not null default 0 after look_id;
alter table look add is_english int(1) not null default 0 after look_id;

CREATE TABLE `homepage_new` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT '0',
  `code` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_offers` (
  `country_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  `code` varchar(50) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`country_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `page_settings` (
  `page_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `gender` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`page_id`,`country_id`,`language_id`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `selected_products` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `featured_pages_selected_products` (
  `code` varchar(50) NOT NULL DEFAULT '',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code`,`page_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `featured_pages_blocks` (
  `code` varchar(50) NOT NULL DEFAULT '',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `block_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code`,`page_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_land` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT '0',
  `code` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_landing_blocks` (
  `country_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `block_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`country_id`,`page_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table `option` add cod tinyint(1);

drop table mother_day_selected_products;
drop table mother_day_blocks;

ALTER TABLE coupon add notification tinyint(1) after code ;
ALTER TABLE product_latest change column country_id page_id int(11) default 0 ;
ALTER TABLE product_latest add column code varchar(50)  default ''  after page_id;
CREATE TABLE `keepcart` (
  `keepcart_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(50) NOT NULL,
  `cart` text,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`keepcart_id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into order_status set order_status_id=4,language_id=1,name='Verification';
insert into order_status set order_status_id=4,language_id=2,name='Verification';


ALTER TABLE customerpartner_to_customer DROP   PRIMARY KEY ;
ALTER TABLE customerpartner_to_customer CHANGE customer_id  customer_id  INT(11) AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE customerpartner_to_customer CHANGE country country_id int not null default 0 AFTER flat_rate;
ALTER TABLE customerpartner_to_customer CHANGE zone zone_id int not null default 0;
ALTER TABLE customerpartner_to_customer ADD    firstname varchar(32 ) not null default '' AFTER is_partner;
ALTER TABLE customerpartner_to_customer ADD    lastname varchar(32) not null default '' AFTER firstname;
ALTER TABLE customerpartner_to_customer ADD    email varchar(96) not null default '' AFTER lastname;
ALTER TABLE customerpartner_to_customer ADD    password varchar(40) not null default '' AFTER email;
ALTER TABLE customerpartner_to_customer ADD    `salt` varchar(9) not null default '' AFTER password;
ALTER TABLE customerpartner_to_customer ADD    `token` text NOT NULL AFTER password;
ALTER TABLE customerpartner_to_customer ADD    telephone varchar(32)  not null after password ;
ALTER TABLE customerpartner_to_customer ADD    status tinyint(1) default 1 after gender;
ALTER TABLE customerpartner_to_customer ADD    date_added datetime not null default '0000-00-00';
ALTER TABLE customerpartner_to_customer ADD    date_modified datetime not null default '0000-00-00';
ALTER TABLE customerpartner_to_customer ADD    user_id int not null default '0';
ALTER TABLE customerpartner_to_customer ADD    last_mod_id int not null default '0';

ALTER TABLE customerpartner_to_order_status  ADD user_id int not null default 0;
ALTER TABLE order_history  ADD user_id int not null default 0;

CREATE TABLE IF NOT EXISTS `otp_data` ( `otp_id` int(11) NOT NULL, `product_id` int(11) NOT NULL, `model` varchar(64) NOT NULL, `extra` varchar(255) NOT NULL, `quantity` int(11) NOT NULL, `subtract` tinyint(1) NOT NULL, `price_prefix` varchar(1) NOT NULL, `price` decimal(15,4) NOT NULL, `special` decimal(15,4) NOT NULL, `weight_prefix` varchar(1) NOT NULL, `weight` decimal(15,4) NOT NULL, PRIMARY KEY (`otp_id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
CREATE TABLE IF NOT EXISTS `otp_image` ( `id` int(11) NOT NULL AUTO_INCREMENT, `product_id` int(11) NOT NULL, `option_id` int(11) NOT NULL, `option_value_id` int(11) NOT NULL, `image` varchar(255) NOT NULL, `sort_order` int(11) NOT NULL, PRIMARY KEY (`id`) )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
CREATE TABLE IF NOT EXISTS `otp_option_value` ( `id` int(11) NOT NULL AUTO_INCREMENT, `product_id` int(11) NOT NULL, `parent_option_id` int(11) NOT NULL, `child_option_id` int(11) NOT NULL, `grandchild_option_id` int(11) NOT NULL, `parent_option_value_id` int(11) NOT NULL, `child_option_value_id` int(11) NOT NULL, `grandchild_option_value_id` int(11) NOT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
ALTER TABLE `cart` ADD `otp_id` int(11) NOT NULL;
ALTER TABLE `cart` ADD `swap_id` int(11) NOT NULL;
ALTER TABLE `order_product` ADD `otp_id` int(11) NOT NULL;
INSERT INTO `setting` (`store_id`, `code`, `key`, `value`, `serialized`) VALUES (0, 'config', 'config_otp_stock', '1', 0),(0, 'config', 'config_otp_extra', '1', 0),(0, 'config', 'config_otp_price', '1', 0),(0, 'config', 'config_otp_special', '1', 0),(0, 'config', 'config_otp_weight', '1', 0),(0, 'config', 'config_otp_list_image', '1', 0),(0, 'config', 'config_otp_list_width', '24', 0),(0, 'config', 'config_otp_list_height', '24', 0), (0, 'config', 'config_otp_list_radius', '0', 0),(0, 'config', 'config_otp_bullet', '1', 0),(0, 'config', 'config_otp_bullet_width', '24', 0),(0, 'config', 'config_otp_bullet_height', '24', 0),(0, 'config', 'config_otp_bullet_radius', '0', 0),(0, 'config', 'config_otp_tool', '1', 0),(0, 'config', 'config_otp_csv', ',', 0);


CREATE TABLE `voucher_type` (
  `voucher_type_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`voucher_type_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
alter table voucher add voucher_type int(2);

CREATE TABLE `image_s3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(512) NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `uploaded` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customer_device` (
  `customer_device_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `udid` varchar(100) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`customer_device_id`),
  UNIQUE KEY `customer_id` (`customer_id`,`udid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customer_checkin` (
  `customer_checkin_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `link_id` int(11) DEFAULT NULL,
  `link_type` enum('mall','shop') DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`customer_checkin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `customer_to_size` (
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `option_id` int(11) NOT NULL DEFAULT '0',
  `option_value_id` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`customer_id`,`option_id`,`option_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--###### customerpartner_to_product
CREATE TABLE `customerpartner_to_product` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `customer_id` int(100) NOT NULL,
 `product_id` int(100) NOT NULL,
 `price` float NOT NULL,
 `seller_price` float NOT NULL,
 `currency_code` varchar(11) NOT NULL,
 `quantity` int(100) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--###### customerpartner_to_transaction
CREATE TABLE `customerpartner_to_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `order_id` varchar(500) NOT NULL,
  `order_product_id` varchar(500) NOT NULL,
  `amount` float NOT NULL,
  `text` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `date_added` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--##### customerpartner_to_payment
CREATE TABLE `customerpartner_to_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `text` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `request_type` varchar(255) NOT NULL,
  `paid` int(10) NOT NULL,
  `balance_reduced` int(10) NOT NULL,
  `payment` varchar(255) NOT NULL,
  `date_added` date NOT NULL DEFAULT '0000-00-00',
  `date_modified` date NOT NULL DEFAULT '0000-00-00',
  `added_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--##### customerpartner_to_order_status
CREATE TABLE `customerpartner_to_order_status` (
  `change_orderstatus_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`change_orderstatus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--##### customerpartner_to_order
CREATE TABLE `customerpartner_to_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_product_id` int(100) NOT NULL,
  `price` float NOT NULL,
  `quantity` float NOT NULL,
  `shipping` varchar(255) NOT NULL,
  `shipping_rate` float NOT NULL,
  `payment` varchar(255) NOT NULL,
  `payment_rate` float NOT NULL,
  `admin` float NOT NULL,
  `customer` float NOT NULL,
  `shipping_applied` float NOT NULL,
  `commission_applied` decimal(10,2) NOT NULL,
  `currency_code` varchar(10) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL,
  `details` varchar(255) NOT NULL,
  `paid_status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_product_status` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--##### customerpartner_to_feedback
CREATE TABLE `customerpartner_to_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` smallint(6) NOT NULL,
  `seller_id` smallint(6) NOT NULL,
  `feedprice` smallint(6) NOT NULL,
  `feedvalue` smallint(6) NOT NULL,
  `feedquality` smallint(6) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `review` text NOT NULL,
  `createdate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--##### customerpartner_to_commission
CREATE TABLE `customerpartner_to_commission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(100) NOT NULL,
  `commission_id` int(100) NOT NULL,
  `fixed` float NOT NULL,
  `percentage` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--###### customerpartner_sold_tracking
CREATE TABLE `customerpartner_sold_tracking` (
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date_added` date NOT NULL DEFAULT '0000-00-00',
  `tracking` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--##### customerpartner_shipping
CREATE TABLE `customerpartner_shipping` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seller_id` int(10) NOT NULL,
  `country_code` varchar(100) NOT NULL,
  `zip_to` varchar(100) NOT NULL,
  `zip_from` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `weight_from` decimal(10,2) NOT NULL,
  `weight_to` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--###### customerpartner_mail
CREATE TABLE `customerpartner_mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `subject` varchar(1000) NOT NULL,
  `message` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--##### customerpartner_flatshipping
CREATE TABLE `customerpartner_flatshipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) NOT NULL,
  `amount` float DEFAULT NULL,
  `tax_class_id` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--##### customerpartner download #####
CREATE TABLE `customerpartner_download` (
  `download_id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  PRIMARY KEY (`download_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--##### customerpartner commission category ###
CREATE TABLE `customerpartner_commission_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(100) NOT NULL,
  `fixed` float NOT NULL,
  `percentage` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--#########customerpartner to customer table #####
CREATE TABLE `customerpartner_to_customer` (
  `customer_id` int(11) NOT NULL,
  `is_partner` int(1) NOT NULL,
  `screenname` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `shortprofile` text NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `twitterid` varchar(255) NOT NULL,
  `paypalid` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `facebookid` varchar(255) NOT NULL,
  `backgroundcolor` varchar(255) NOT NULL,
  `companybanner` varchar(255) NOT NULL,
  `companylogo` varchar(255) NOT NULL,
  `companylocality` varchar(255) NOT NULL,
  `companyname` varchar(255) NOT NULL,
  `companydescription` text NOT NULL,
  `countrylogo` varchar(1000) NOT NULL,
  `otherpayment` text NOT NULL,
  `commission` decimal(10,2) NOT NULL,
  `flat_rate` decimal(10,2) DEFAULT NULL,
  `zone` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `opening_time` text,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--- ### shipping ###
CREATE TABLE `order_booking` (
  `order_booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `book_response` varchar(256) DEFAULT NULL,
  `sent_data` text,
  `AWBNO` varchar(50) DEFAULT NULL,
  `service_type` varchar(50) DEFAULT NULL,
  `prod_type` varchar(50) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `rtoshipment` int(11) DEFAULT NULL,
  `veh_type` varchar(50) DEFAULT NULL,
  `pbrand` varchar(50) DEFAULT NULL,
  `ready_date` datetime DEFAULT NULL,
  `closing_time` datetime DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `return_booking` (
  `return_booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `book_response` varchar(256) DEFAULT NULL,
  `sent_data` text,
  `AWBNO` varchar(50) DEFAULT NULL,
  `service_type` varchar(50) DEFAULT NULL,
  `prod_type` varchar(50) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `rtoshipment` int(11) DEFAULT NULL,
  `veh_type` varchar(50) DEFAULT NULL,
  `pbrand` varchar(50) DEFAULT NULL,
  `ready_date` datetime DEFAULT NULL,
  `closing_time` datetime DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`return_booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE `order_shipping` (
  `order_shipping_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `ref_no` varchar(50) DEFAULT NULL,
  `AWBNO` varchar(50) DEFAULT NULL,
  `service_type` varchar(50) DEFAULT NULL,
  `prod_type` varchar(50) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `rtoshipment` int(11) DEFAULT NULL,
  `veh_type` varchar(50) DEFAULT NULL,
  `pbrand` varchar(50) DEFAULT NULL,
  `ready_date` datetime DEFAULT NULL,
  `closing_time` datetime DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_shipping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE order_status ADD quantity enum('+','-','no-change');
ALTER TABLE order_status ADD stock enum('in','out');

CREATE TABLE `package` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `width` int(11) DEFAULT NULL,
  `hieght` int(11) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `package_description` (
  `package_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(512) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`package_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_package` (
  `order_package_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_package_product` (
  `order_package_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_package_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_package_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE `firstflight_destination_code` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `destination_code` varchar(32) NOT NULL,
  PRIMARY KEY (`zone_id`,`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- #### 2015-11-11 Qassem ##############################
ALTER TABLE `user` ADD `store_id` int NOT null default -1 AFTER password;
-- # UPDATE user set store_id = 0 where user_id=1;

ALTER TABLE `user` ADD `core` INT(1) AFTER `user_id`;
ALTER TABLE `user_group` ADD `core` INT(1) default 0 AFTER `user_group_id`;

#############  ABANDONCART TABLE  #########
CREATE TABLE `abandonedcarts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `cart` text,
  `customer_info` text,
  `last_page` varchar(255) DEFAULT NULL,
  `restore_id` varchar(255) DEFAULT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `notified` smallint(6) NOT NULL DEFAULT '0',
  `ordered` tinyint(4) NOT NULL DEFAULT '0',
  `store_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



########    PARTNER DESCRIPTION TABLE  #########
CREATE TABLE `customerpartner_to_customer_description` (
`customer_id` int(11) NOT NULL,
`language_id` int(11) NOT NULL,
`screenname` varchar(50) NOT NULL,
`shortprofile` varchar(255) NOT NULL,
`companyname` varchar(50) NOT NULL,
PRIMARY KEY (`customer_id`,`language_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customerpartner_to_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(100) NOT NULL,
  `product_id` int(100) NOT NULL,
  `price` float NOT NULL,
  `seller_price` float NOT NULL,
  `currency_code` varchar(11) NOT NULL,
  `quantity` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


---- ##### LOOKS keyword table table #####
CREATE TABLE `looks_keyword` (
`look_id` int(11) NOT NULL,
`keyword_id` int(11) NOT NULL,
`date_added` datetime NOT NULL,
PRIMARY KEY (`look_id`, `keyword_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


---- ##### TIPS  keyword table #####
CREATE TABLE `tips_keyword` (
  `tip_id` int(11) NOT NULL,
  `keyword_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`tip_id`,`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tip_section` (
  `tip_section_id` int(11) NOT NULL AUTO_INCREMENT,
  `tip_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `product_id` int(11) NOT NULL,
  `image` varchar(512) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  PRIMARY KEY (`tip_section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tip_section_image` (
  `tip_section_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `tip_section_id` int(11) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`tip_section_image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

---- ##### Keywords description table #####
ALTER TABLE cart ADD country_id INT AFTER cart_id;

CREATE TABLE `keyword_description` (
`keyword_id` int (11) NOT NULL,
`language_id` int (11) NOT NULL,
`name` varchar (50) NOT NULL,
`body` varchar (255) NOT NULL,
PRIMARY KEY (`keyword_id`,`language_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


---- ##### Keywords table #####
CREATE TABLE `keyword` (
`keyword_id` int (11) NOT NULL AUTO_INCREMENT,
`user_id` int (11) NOT NULL,
`last_mod_id` int (11) NOT NULL,
`date_added` datetime NOT NULL,
`date_modified` datetime NOT NULL,
PRIMARY KEY (`keyword_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


---- ### favorite tips #####
CREATE TABLE `customer_tips` (
  `customer_id` int(11) NOT NULL,
  `tip_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`,`tip_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

---- ### favorite looks #####
CREATE TABLE `customer_looks` (
  `customer_id` int(11) NOT NULL,
  `look_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`,`look_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


---- ### favorite categories #####
CREATE TABLE `customer_categories` (
  `customer_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

---- ### favorite brands #####
CREATE TABLE `customer_brands` (
  `customer_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`,`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

---- ### favorite sellers #####
CREATE TABLE `customer_sellers` (
  `customer_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`,`seller_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `top_pages` (
  `top_pages_id` int(11) NOT NULL AUTO_INCREMENT,
  `day` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `image` varchar(300) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`top_pages_id`),
  UNIQUE KEY `day_country` (`day`,`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `top_pages_detail` (
  `top_pages_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `top_pages_id` int(11) DEFAULT NULL,
  `type` enum('product','look','tip') NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`top_pages_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `top_pages_to_country` (
  `top_pages_id` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`top_pages_id`,`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE `top_pages_description` (
  `top_pages_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `body` text,
  PRIMARY KEY (`top_pages_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `top_pages_to_group` (
  `top_pages_id` int(11) NOT NULL DEFAULT '0',
  `top_pages_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`top_pages_id`,`top_pages_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `top_pages_group` (
  `top_pages_group_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`top_pages_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `top_pages_group_description` (
  `top_pages_group_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`top_pages_group_id`,`language_id`),
  KEY `language_id` (`language_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

set names utf8;

insert into top_pages_group             set top_pages_group_id = 1, sort_order=0, status=0;
insert into top_pages_group             set top_pages_group_id = 2, sort_order=0, status=0;
insert into top_pages_group             set top_pages_group_id = 3, sort_order=0, status=0;
insert into top_pages_group             set top_pages_group_id = 4, sort_order=0, status=0;

insert into top_pages_group_description set top_pages_group_id = 1, language_id=2, name='المشاهير', description ='إطلالات المشاهير';
insert into top_pages_group_description set top_pages_group_id = 1, language_id=1, name='Celebrities', description ='Celebrities looks';
insert into top_pages_group_description set top_pages_group_id = 2, language_id=1, name='Women', description ='Women looks';
insert into top_pages_group_description set top_pages_group_id = 2, language_id=2, name='المرأة', description ='إطلالات المرأة';
insert into top_pages_group_description set top_pages_group_id = 3, language_id=1, name='Men', description ='Men';
insert into top_pages_group_description set top_pages_group_id = 3, language_id=2, name='الرجل', description ='إطلالات الرجل  ';
insert into top_pages_group_description set top_pages_group_id = 4, language_id=1, name='Kids', description ='Baby';
insert into top_pages_group_description set top_pages_group_id = 4, language_id=2, name='الطفل', description ='إطلالات الطفل  ';

---- ##### top_pages  keyword table #####
CREATE TABLE `top_pages_keyword` (
  `top_pages_id` int(11) NOT NULL,
  `keyword_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`top_pages_id`,`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `url_partner` (
  `url_partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(255) DEFAULT NULL,
  `keyword` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`url_partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `popup_email` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  `confirm_sent` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0: disabled; 1: enabled; 2: blacklist; 3: un-subscribed; 4: not verified',
  `gender` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- #### ADD group to products 2015-11-17 ########
DROP TABLE IF EXISTS `group`;
DROP TABLE IF EXISTS `group_description`;
DROP TABLE IF EXISTS `group_filter`;
DROP TABLE IF EXISTS `group_path`;
DROP TABLE IF EXISTS `group_to_layout`;
DROP TABLE IF EXISTS `group_to_store`;
DROP TABLE IF EXISTS `coupon_group`;
DROP TABLE IF EXISTS `product_to_group`;

-- #### 2015-12-11 Qassem ##############################

CREATE TABLE  `brand` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(256) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `social_tw` varchar(255) DEFAULT NULL,
  `social_fb` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `viewed` int(11) NOT NULL DEFAULT '0',
  `power` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `brand_description` (
  `brand_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`brand_id`,`language_id`),
  KEY `language_id` (`language_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE country ADD telecode VARCHAR(7) NOT NULL DEFAULT '' AFTER `language`;

CREATE TABLE  `country_description` (
  `country_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`country_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `brand_related` (
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `related_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`brand_id`,`related_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `brand_to_category` (
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `store_category_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`brand_id`,`store_category_id`),
  KEY `store_category_id` (`store_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `brand_to_country` (
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`brand_id`,`country_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `algolia` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `piked` int(11) DEFAULT NULL,
  `piked_date` datetime DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  UNIQUE KEY `unique_index` (`type`,`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cache` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `piked` int(11) DEFAULT NULL,
  `piked_date` datetime DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  UNIQUE KEY `unique_index` (`type`,`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `cms_homepage_slider_images` (
  `cms_homepage_slider_images_id` INT(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(256) DEFAULT NULL,
  `order` INT(11) NOT NULL DEFAULT '0',
  `enable` TINYINT(1) NOT NULL DEFAULT '0',
  `date_added` datetime,
  `date_modified` datetime,
  PRIMARY KEY (`cms_homepage_slider_images_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `cms_homepage_slider_images_details` (
  `cms_homepage_slider_images_id` INT(11) NOT NULL,
  `language_id` INT(11) NOT NULL DEFAULT '1',
  `description` varchar(255),
  `link_url` varchar(255)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `cms_homepage_slider_to_country` (
  `cms_homepage_slider_images_id` INT(11) NOT NULL,
  `country_id` INT(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ####################################################################################


-- ## QASEM FOR grouping categories AFTER importable

ALTER TABLE category ADD group_id int AFTER right_id;


-- ###################### : CMS - Homepage Products Top #########################
CREATE TABLE  `cms_homepage_top_products` (
  `cms_homepage_top_products_id` INT(11) NOT NULL AUTO_INCREMENT,
  `country_id` INT(11) DEFAULT NULL,
  `product_id` INT(11) DEFAULT NULL,
  `featured` TINYINT(1) NOT NULL DEFAULT '0',
  `user_id` INT(11) DEFAULT NULL,
  `sort_order` INT(11) NOT NULL DEFAULT '0',
  `date_added` datetime,
  `date_modified` datetime,
  PRIMARY KEY (`cms_homepage_top_products_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ####################################################################################

CREATE TABLE  `selected_brands` (
  `selected_brands_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`selected_brands_id`),
  UNIQUE KEY `country_id_2` (`country_id`,`brand_id`),
  KEY `country_id` (`country_id`),
  KEY `brand_id` (`brand_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `selected_categories` (
  `selected_categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) DEFAULT NULL,
  `text` varchar(512) NOT NULL DEFAULT '',
  `image` varchar(512) NOT NULL DEFAULT '',
  `user_id` int(11) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`selected_categories_id`),
  UNIQUE KEY `country_id_2` (`country_id`,`category_id`),
  KEY `country_id` (`country_id`),
  KEY `brand_id` (`category_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ####################################################################################

-- ## : Country/Zone - Add 'available' column + make Saudi and UAE available on site ##
ALTER TABLE `country` ADD `available` TINYINT(1) DEFAULT '0' AFTER `status`;
ALTER TABLE `zone` ADD `available` TINYINT(1) DEFAULT '1' AFTER `status`;
UPDATE `country` SET `available` = 1 WHERE country_id = '184'; -- KSA
UPDATE `country` SET `available` = 1 WHERE country_id = '221'; -- UAE
-- ##########################################################################################

-- ## qasem 2016-02-04 ###

CREATE TABLE `mall` (
  `mall_id` int(10) NOT NULL AUTO_INCREMENT,
  `city_id` int(10) unsigned NOT NULL,
  `image` varchar(512) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `website` varchar(512) DEFAULT NULL,
  `social_jeeran` varchar(512) DEFAULT NULL,
  `social_fb` varchar(512) DEFAULT NULL,
  `social_tw` varchar(512) DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shop_count` int(11) DEFAULT NULL,
  `status` tinyint(255) DEFAULT '0',
  `meta_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `sort_order` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `viewed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mall_id`),
  KEY `malls_city_id_index` (`city_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE  `mall_description` (
  `mall_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `location` text,
  `open` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL
) ENGINE=InnoDB;

ALTER TABLE mall CHANGE logo image varchar(512);
ALTER TABLE mall ADD sort_order int;



ALTER TABLE brand ADD date_modified datetime AFTER sort_order;
ALTER TABLE brand ADD link varchar(255) AFTER image;
ALTER TABLE brand ADD social_fb varchar(255) AFTER `link`;
ALTER TABLE brand ADD social_tw varchar(255) AFTER `link`;
ALTER TABLE brand_description ADD description text;


CREATE TABLE `main_group` (
  `main_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(512) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `website` varchar(512) DEFAULT NULL,
  `social_fb` varchar(225) DEFAULT NULL,
  `social_tw` varchar(255) DEFAULT NULL,
  `social_jeeran` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`main_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `main_group_description` (
  `main_group_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `location` text,
  PRIMARY KEY (`main_group_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `main_store` (
  `main_store_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_designer` int(1) NOT NULL DEFAULT '0',
  `main_group_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `products_count` int(11) DEFAULT '0',
  `website` varchar(512) DEFAULT NULL,
  `social_fb` varchar(225) DEFAULT NULL,
  `social_tw` varchar(255) DEFAULT NULL,
  `social_jeeran` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`main_store_id`),
  KEY `main_group_id` (`main_group_id`),
  KEY `category_id` (`category_id`),
  KEY `city_id` (`city_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `main_store_description` (
  `main_store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `location` text,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`main_store_id`,`language_id`),
  KEY `language_id` (`language_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ############################ Offer  ########################################
 CREATE TABLE  `offer` (
  `offer_id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `approved` int(11) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`offer_id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -- `offer_description`;
CREATE TABLE `offer_description` (
  `offer_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `percent` varchar(255) NOT NULL DEFAULT '',
  `body_text` varchar(255) NOT NULL DEFAULT '',
  `period` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`offer_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ############################ Offer Images ##################################

CREATE TABLE `offer_image` (
  `offer_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_id` int(11) NOT NULL,
  `image` varchar(512) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`offer_image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ############################ Offer Link  ###################################
CREATE TABLE  `offer_to_shop` (
  `offer_id` int(11) NOT NULL DEFAULT '0',
  `shop_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`offer_id`,`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE  `offer_to_brand` (
  `offer_id` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`offer_id`,`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE  `offer_to_category` (
  `offer_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`offer_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ############################ Offer Category  ###################################
CREATE TABLE `offer_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(1) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `offer_category_description` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(512) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`category_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ####################################################################################


CREATE TABLE `news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `status` int(11) DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `approved` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `news_description` (
  `news_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `body_text` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`news_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS brand_to_news;
DROP TABLE IF EXISTS news_link;

CREATE TABLE `news_to_brand` (
  `news_id` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`news_id`, `brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `news_to_shop` (
  `news_id` int(11) NOT NULL DEFAULT '0',
  `shop_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`news_id`, `shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `news_image` (
  `news_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`news_image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table product_special add country_id int AFTER product_special_id;
alter table product_discount add country_id int AFTER product_discount_id;

CREATE TABLE `news_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(1) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE `news_category_description` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(512) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`category_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE `offer_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(1) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `offer_category_description` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(512) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`category_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `look` (
  `look_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_english` int(1) NOT NULL DEFAULT '0',
  `is_arabic` int(1) NOT NULL DEFAULT '0',
  `is_famous` int(11) DEFAULT NULL,
  `mall_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `home` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`look_id`),
  KEY `mall_id` (`mall_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `look_description` (
  `look_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`look_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `look_image` (
  `look_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `look_id` int(11) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`look_image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `look_product` (
  `look_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`look_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `look_to_country` (
  `look_id` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`look_id`,`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `country` CHANGE `name` `name_old` varchar(255);
ALTER TABLE country ADD language INT AFTER name_old;

CREATE TABLE `zone_description` (
`zone_id` int(11) NOT NULL DEFAULT '0',
`language_id` int(11) NOT NULL DEFAULT '0',
`name` varchar(255) DEFAULT NULL,
PRIMARY KEY (`zone_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `zone` CHANGE `name` `name_old` varchar(255);

CREATE TABLE `maps` (
  `map_id` int(11) NOT NULL AUTO_INCREMENT,
  `link_id` int(11) NOT NULL,
  `link_type` enum('mall','shop','event','mainstore') DEFAULT NULL,
  `latitude` float(10,6) NOT NULL,
  `longitude` float(10,6) NOT NULL,
  `zoom` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `store_category` (
  `store_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`store_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `store_category_description` (
  `store_category_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(256) DEFAULT NULL,
  `description` text,
  `meta_title` varchar(256) DEFAULT NULL,
  `meta_keywords` varchar(256) DEFAULT NULL,
  `meta_tag` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`store_category_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `store_category_to_store` (
  `store_category_id` int(11) NOT NULL DEFAULT '0',
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`store_category_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `store_category_to_main_store` (
  `store_category_id` int(11) NOT NULL DEFAULT '0',
  `main_store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`store_category_id`,`main_store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE customer ADD `popup` INT AFTER newsletter;
ALTER TABLE customer ADD `source` ENUM('site', 'facebook', 'twitter', 'google') not null default 'site';
ALTER TABLE customer ADD `oauth_uid` varchar(255);
ALTER TABLE customer ADD `image` varchar(255) AFTER `email`;
ALTER TABLE customer ADD `birthdate` date AFTER `password`;
ALTER TABLE customer ADD `gender` ENUM('male','female') AFTER `birthdate`;
ALTER TABLE customer ADD `country_id` int(11) AFTER `gender`;
ALTER TABLE customer ADD `zone_id` int(11) AFTER `country_id`;
ALTER TABLE customer ADD `interface` INT AFTER zone_id;
ALTER TABLE customer ADD whatsapp int(1);

CREATE TABLE `tip` (
  `tip_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_english` int(1) NOT NULL DEFAULT '0',
  `is_arabic` int(1) NOT NULL DEFAULT '0',
  `author` varchar(255) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `home` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `last_mod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`tip_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;


 CREATE TABLE `tip_description` (
  `tip_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `body` text,
  PRIMARY KEY (`tip_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tip_to_country` (
  `tip_id` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tip_id`,`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tip_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `tip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`review_id`),
  KEY `tip_id` (`tip_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tip_related` (
  `tip_id` int(11) NOT NULL DEFAULT '0',
  `related_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tip_id`,`related_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tip_product` (
  `tip_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`tip_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tip_to_group` (
  `tip_id` int(11) NOT NULL DEFAULT '0',
  `tip_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tip_id`,`tip_group_id`),
  KEY `group_id` (`tip_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `order` ADD whatsapp INT(1);
ALTER TABLE store CHANGE name store_name varchar(255);
ALTER TABLE store ADD viewed int not null default 0;
ALTER TABLE store ADD is_designer int(1) not null default 0 AFTER store_id;

ALTER TABLE store ADD `products_count` INT NOT NULL DEFAULT '0' AFTER `main_store_id`;
ALTER TABLE store ADD `social_facebook` varchar(255);
ALTER TABLE store ADD `social_jeeran` varchar(255);
ALTER TABLE store ADD `social_twitter` varchar(255);
ALTER TABLE store ADD `social_website` varchar(255);
ALTER TABLE store ADD `date_added` datetime;
ALTER TABLE store ADD `date_modified` datetime;
ALTER TABLE store ADD zone_id int AFTER mall_id;
ALTER TABLE store ADD main_store_id int AFTER mall_id;
ALTER TABLE store ADD mall_id int default 0;
ALTER TABLE store ADD user_id int default 0;
ALTER TABLE store ADD status int(1);
ALTER TABLE store ADD landing int(4);
ALTER TABLE store ADD sort_order int;
ALTER TABLE store ADD display varchar(255);


CREATE TABLE `store_description` (
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `open` text,
  `location` varchar(512) DEFAULT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`store_id`,`language_id`),
  KEY `language_id` (`language_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `product_wear` (
  `product_id` int(11) NOT NULL,
  `wear_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`,`wear_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tip_group` (
  `tip_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`tip_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tip_group_description` (
  `tip_group_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`tip_group_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

truncate table tip_group;
truncate table tip_group_description;

insert into tip_group (sort_order, status, date_added, date_modified) values ('1', '1', now(), now());
insert into tip_group_description (tip_group_id, language_id, name, description) values ('1', '2', 'أزياء وأحذية', 'أزياء وأحذية');
insert into tip_group_description (tip_group_id, language_id, name, description) values ('1', '1', 'Fashion & Shoes', 'Fashion & Shoes');

insert into tip_group (sort_order, status, date_added, date_modified) values ('2', '1', now(), now());
insert into tip_group_description (tip_group_id, language_id, name, description) values ('2', '2', 'التسوق', 'التسوق');
insert into tip_group_description (tip_group_id, language_id, name, description) values ('2', '1', 'Shopping', 'Shopping');

insert into tip_group (sort_order, status, date_added, date_modified) values ('3', '1', now(), now());
insert into tip_group_description (tip_group_id, language_id, name, description) values ('3', '2', 'نصائح الديكور', 'نصائح الديكور');
insert into tip_group_description (tip_group_id, language_id, name, description) values ('3', '1', 'Decoration Tips', 'Decoration Tips');

insert into tip_group (sort_order, status, date_added, date_modified) values ('4', '1', now(), now());
insert into tip_group_description (tip_group_id, language_id, name, description) values ('4', '2', 'جمال ومكياج', 'جمال ومكياج');
insert into tip_group_description (tip_group_id, language_id, name, description) values ('4', '1', 'Makeup & Beauty', 'Makeup & Beauty');

insert into tip_group (sort_order, status, date_added, date_modified) values ('5', '1', now(), now());
insert into tip_group_description (tip_group_id, language_id, name, description) values ('5', '2', 'نصائح المحجبات', 'نصائح المحجبات');
insert into tip_group_description (tip_group_id, language_id, name, description) values ('5', '1', 'Hijab Tips', 'Hijab Tips');


 CREATE TABLE `tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(512) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `home` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`tag_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tag_description` (
  `tag_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `body` text,
  PRIMARY KEY (`tag_id`,`language_id`),
  KEY `language_id` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `tag_product` (
  `tag_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`tag_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tag_look` (
  `tag_id` int(11) NOT NULL DEFAULT '0',
  `look_id` int(11) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`tag_id`,`look_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tag_tip` (
  `tag_id` int(11) NOT NULL DEFAULT '0',
  `tip_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`tag_id`,`tip_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE `tag_to_country` (
  `tag_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tag_id`,`country_id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `homepage` (
  `homepage_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `link_id` int(11) DEFAULT NULL,
  `link_type` enum('tip','product','look') DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`homepage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE newsletter_category (newsletter_category_id int primary key AUTO_INCREMENT, country_id int, sort_order int, date_added datetime, date_modified datetime, user_id int);
CREATE TABLE newsletter_category_description (newsletter_category_id int,language_id int, name varchar(512), description text, primary key(newsletter_category_id, language_id));
CREATE TABLE newsletter_category_to_customer (customer_id int, newsletter_category_id int, primary key(customer_id, newsletter_category_id));

insert into newsletter_category (country_id, sort_order, date_added, date_modified, user_id) values ('221', '1', now(), now(), '4');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('1', '2', 'منتجات', 'منتجات');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('1', '1', 'Products', 'Products');

insert into newsletter_category (country_id, sort_order, date_added, date_modified, user_id) values ('184', '1', now(), now(), '4');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('2', '2', 'منتجات', 'منتجات');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('2', '1', 'Products', 'Products');


insert into newsletter_category (country_id, sort_order, date_added, date_modified, user_id) values ('221', '2', now(), now(), '4');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('3', '2', 'عروض و تخفيضات', 'عروض و تخفيضات');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('3', '1', 'Offers & Sales', 'Offers & Sales');

insert into newsletter_category (country_id, sort_order, date_added, date_modified, user_id) values ('184', '2', now(), now(), '4');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('4', '2', 'عروض و تخفيضات', 'عروض و تخفيضات');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('4', '1', 'Offers & Sales', 'Offers & Sales');


insert into newsletter_category (country_id, sort_order, date_added, date_modified, user_id) values ('221', '3', now(), now(), '4');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('5', '2', 'اسواق و مولات', 'اسواق و مولات');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('5', '1', 'Shops & Malls', 'Shops & Malls');

insert into newsletter_category (country_id, sort_order, date_added, date_modified, user_id) values ('184', '3', now(), now(), '4');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('6', '2', 'اسواق و مولات', 'اسواق و مولات');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('6', '1', 'Shops & Malls', 'Shops & Malls');


insert into newsletter_category (country_id, sort_order, date_added, date_modified, user_id) values ('221', '4', now(), now(), '4');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('7', '2', 'موضة و نصائح', 'موضة و نصائح');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('7', '1', 'Tags & Tips', 'Tags & Tips');

insert into newsletter_category (country_id, sort_order, date_added, date_modified, user_id) values ('184', '4', now(), now(), '4');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('8', '2', 'موضة و نصائح', 'موضة و نصائح');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('8', '1', 'Tags & Tips', 'Tags & Tips');


insert into newsletter_category (country_id, sort_order, date_added, date_modified, user_id) values ('221', '5', now(), now(), '4');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('9', '2', 'أخبار السوق', 'أخبار السوق');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('9', '1', 'Market News', 'Market News');

insert into newsletter_category (country_id, sort_order, date_added, date_modified, user_id) values ('184', '5', now(), now(), '4');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('10', '2', 'أخبار السوق', 'أخبار السوق');
insert into newsletter_category_description (newsletter_category_id, language_id, name, description) values ('10', '1', 'Market News', 'Market News');


ALTER TABLE address ADD telephone VARCHAR(32) AFTER lastname;



/**/
CREATE TABLE `look_group` (
  `look_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`look_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `look_group_description` (
  `look_group_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`look_group_id`,`language_id`),
  KEY `language_id` (`language_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `look_to_group` (
  `look_id` int(11) NOT NULL DEFAULT '0',
  `look_group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`look_id`,`look_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into look_group (sort_order, status, date_added, date_modified) values ('1', '1', now(), now());
insert into look_group_description (look_group_id, language_id, name, description) values ('1', '2', 'إطلالات المشاهير', 'إطلالات المشاهير');
insert into look_group_description (look_group_id, language_id, name, description) values ('1', '1', 'Famous Looks', 'Famous Looks');

insert into look_group (sort_order, status, date_added, date_modified) values ('2', '1', now(), now());
insert into look_group_description (look_group_id, language_id, name, description) values ('2', '2', 'إطلالات المرأة', 'إطلالات المرأة');
insert into look_group_description (look_group_id, language_id, name, description) values ('2', '1', 'Women Looks', 'Women Looks');

insert into look_group (sort_order, status, date_added, date_modified) values ('3', '1', now(), now());
insert into look_group_description (look_group_id, language_id, name, description) values ('3', '2', 'إطلالات الرجل', 'إطلالات الرجل');
insert into look_group_description (look_group_id, language_id, name, description) values ('3', '1', 'Men Looks', 'Men Looks');

insert into look_group (sort_order, status, date_added, date_modified) values ('4', '1', now(), now());
insert into look_group_description (look_group_id, language_id, name, description) values ('4', '2', 'إطلالات الطفل', 'إطلالات الطفل');
insert into look_group_description (look_group_id, language_id, name, description) values ('4', '1', 'Baby Looks', 'Baby Looks');
/**/


/*MUAYAD: LEENA REQUEST*/
UPDATE tip_group_description SET name='إكسسوارات', description='إكسسوارات' WHERE tip_group_id='2' AND language_id='2';
UPDATE tip_group_description SET name='Accessories', description='Accessories' WHERE tip_group_id='2' AND language_id='1';
/*MUAYAD: LEENA REQUEST*/

/*MUAYAD: BRAND TABLE PRIMARY KEY IS NOT AUTO INCREMENT*/
ALTER TABLE brand MODIFY COLUMN brand_id INT NOT NULL AUTO_INCREMENT;
/*MUAYAD: BRAND TABLE PRIMARY KEY IS NOT AUTO INCREMENT*/


-- sql queries related to the helpDesk.
-- http://www.phpmyadmin.net
--
-- Generation Time: Aug 18, 2015 at 01:43 PM

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ticketsys`
--

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_agent_level`
--

ALTER TABLE ts_tickets ADD country int;
ALTER TABLE ts_tickets ADD city int;
ALTER TABLE ts_tickets ADD telephone int;
ALTER TABLE ts_tickets ADD zone_id int AFTER country ;


INSERT INTO `ticket_ts_agent_level` (`id`, `status`, `date_added`, `date_updated`) VALUES
(1, 1, '2015-07-01 14:49:22', '2015-07-01 14:49:22'),
(2, 1, '2015-07-01 14:49:49', '2015-07-01 14:49:49'),
(3, 1, '2015-07-01 14:49:55', '2015-07-01 14:49:55'),
(4, 1, '2015-07-01 14:50:05', '2015-07-01 14:50:05');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_agent_level_description`
--

INSERT INTO `ticket_ts_agent_level_description` (`id`, `level_id`, `name`, `description`, `language_id`) VALUES
(1, 1, 'Low', 'Agent has new but good in skills. ', 1),
(2, 2, 'High', 'Agent has High level of knowledge.', 1),
(3, 3, 'Pro', 'Agent has Pro and can handle all issues.', 1),
(4, 4, 'Expert', 'Agent has Expert level of knowledge.', 1);

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_agent_roles`
--

INSERT INTO `ticket_ts_agent_roles` (`id`, `agent_id`, `role_id`) VALUES
(1, 4, 8);

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_business_hours`
--

INSERT INTO `ticket_ts_business_hours` (`id`, `name`, `description`, `timezone`, `date_added`, `date_updated`, `timings`, `sizes`, `positions`) VALUES
(1, 'India', 'This is like Indian working hours', 'Asia/Kolkata', '2015-06-03 19:29:50', '2015-08-18 13:26:24', 'a:4:{s:6:"monday";a:1:{i:0;s:13:"08:00 - 18:30";}s:7:"tuesday";a:2:{i:0;s:13:"05:00 - 11:00";i:1;s:13:"12:00 - 19:30";}s:8:"thursday";a:1:{i:0;s:13:"08:00 - 15:30";}s:6:"friday";a:1:{i:0;s:13:"07:30 - 16:30";}}', 'a:4:{s:6:"monday";a:1:{i:0;s:3:"529";}s:7:"tuesday";a:2:{i:0;s:3:"302";i:1;s:3:"381";}s:8:"thursday";a:1:{i:0;s:3:"375";}s:6:"friday";a:1:{i:0;s:3:"452";}}', 'a:4:{s:6:"monday";a:1:{i:0;s:3:"400";}s:7:"tuesday";a:2:{i:0;s:3:"250";i:1;s:3:"600";}s:8:"thursday";a:1:{i:0;s:3:"400";}s:6:"friday";a:1:{i:0;s:3:"375";}}'),
(2, 'Default', 'Default', 'Europe/London', '2015-06-03 19:36:27', '2015-08-18 13:25:15', 'a:4:{s:6:"monday";a:1:{i:0;s:13:"08:00 - 18:00";}s:7:"tuesday";a:1:{i:0;s:13:"06:00 - 14:00";}s:9:"wednesday";a:1:{i:0;s:13:"07:30 - 17:30";}s:6:"friday";a:1:{i:0;s:13:"06:30 - 14:30";}}', 'a:4:{s:6:"monday";a:1:{i:0;s:3:"504";}s:7:"tuesday";a:1:{i:0;s:3:"402";}s:9:"wednesday";a:1:{i:0;s:3:"502";}s:6:"friday";a:1:{i:0;s:3:"402";}}', 'a:4:{s:6:"monday";a:1:{i:0;s:3:"400";}s:7:"tuesday";a:1:{i:0;s:3:"300";}s:9:"wednesday";a:1:{i:0;s:3:"375";}s:6:"friday";a:1:{i:0;s:3:"325";}}');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_category`
--

INSERT INTO `ticket_ts_category` (`id`, `status`, `date_added`, `date_updated`) VALUES
(1, 1, '2015-07-07 17:40:13', '2015-07-07 17:40:13'),
(2, 1, '2015-07-07 19:43:39', '2015-07-07 19:43:39'),
(3, 1, '2015-07-07 19:44:17', '2015-07-07 19:44:17');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_category_description`
--

INSERT INTO `ticket_ts_category_description` (`id`, `category_id`, `name`, `description`, `language_id`) VALUES
(1, 1, 'Support Related Query', 'Oh you want Support .\r\nIs it related to Installation or any small issues ?\r\n\r\nWhy don''t you try our added docs FIRST :)', 1),
(2, 3, 'Error / Bug', 'Module Showing any issue on your site, can be Version issue. Please check given solution.\r\nMay be your issue will solve here.\r\n\r\nNo need to generate Ticket :)', 1),
(3, 2, 'Solved / Queries', 'In this category we Display customers and agents conversation that mostly occur.\r\nSo Public this for all, any issue with same problem will solve like this.\r\n\r\nNo need to generate tickets :)', 1);

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_category_information`
--

INSERT INTO `ticket_ts_category_information` (`id`, `category_id`, `information_id`) VALUES
(1, 2, 10),
(2, 1, 7),
(3, 1, 6),
(4, 2, 9),
(5, 2, 8),
(6, 3, 3),
(7, 3, 12),
(8, 3, 11);

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_emailtemplates`
--

INSERT INTO `ticket_ts_emailtemplates` (`id`, `name`, `message`, `status`, `date_added`, `date_updated`) VALUES
(1, 'Ticket Created', '&lt;p&gt;Hi,&lt;/p&gt;&lt;p&gt;&amp;nbsp;{{ticket.description}}&lt;/p&gt;&lt;p&gt;Thanks &amp;nbsp;for generating ticket, we will solve your query as soon as possible.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards,&lt;/p&gt;&lt;p&gt;Webkul Team&lt;/p&gt;', 1, '2015-08-04 18:46:51', '2015-08-04 19:55:32'),
(2, ' {{ticket.customername}} added query {{ticket.id}}', '&lt;p&gt;&amp;nbsp;&amp;nbsp;{{ticket.threaddescription}}&lt;/p&gt;', 1, '2015-08-10 11:31:09', '2015-08-18 13:37:07');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_events`
--

INSERT INTO `ticket_ts_events` (`id`, `name`, `description`, `performer`, `events`, `actions`, `conditions_all`, `conditions_one`, `status`, `date_added`, `date_updated`) VALUES
(1, 'Ticket Created ', 'Ticket Created ', 'a:1:{s:5:"value";s:8:"customer";}', 'a:1:{i:0;a:2:{s:4:"type";s:6:"ticket";s:4:"from";s:7:"created";}}', 'a:1:{i:0;a:2:{s:4:"type";s:13:"mail_customer";s:6:"action";a:3:{s:13:"emailtemplate";s:0:"";s:7:"subject";s:47:"We received your query and will reach you soon ";s:7:"message";s:524:"&lt;p&gt;Hi {{ticket.customername}},&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 1.42857143;&quot;&gt;We received your query and will reach you soon .&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 1.42857143;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 1.42857143;&quot;&gt;Regards,&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;line-height: 1.42857143;&quot;&gt;Webkul HelpDesk&lt;br&gt;&lt;/span&gt;&lt;br&gt;&lt;/p&gt;";}}}', 'a:1:{i:1;a:3:{s:4:"type";s:6:"source";s:14:"matchCondition";s:2:"is";s:5:"match";s:3:"web";}}', 'a:1:{i:0;a:3:{s:4:"type";s:6:"source";s:14:"matchCondition";s:2:"is";s:5:"match";s:4:"mail";}}', 1, '2015-07-30 13:17:26', '2015-08-18 13:34:47'),
(2, 'Ticket Update', 'Ticket Update', 'a:2:{s:5:"value";s:6:"agents";s:6:"agents";a:1:{i:0;s:3:"all";}}', 'a:3:{i:0;a:3:{s:4:"type";s:16:"priority_updated";s:4:"from";s:3:"any";s:2:"to";s:3:"any";}i:1;a:3:{s:4:"type";s:13:"agent_updated";s:4:"from";s:3:"any";s:2:"to";s:3:"any";}i:3;a:3:{s:4:"type";s:13:"group_updated";s:4:"from";s:3:"any";s:2:"to";s:3:"any";}}', 'a:1:{i:0;a:2:{s:4:"type";s:13:"mail_customer";s:6:"action";a:3:{s:13:"emailtemplate";s:0:"";s:7:"subject";s:33:"Your Ticket{{ticket.id}} updated ";s:7:"message";s:180:"&lt;p&gt;Hi,&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Your Ticket updated.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards,&lt;/p&gt;&lt;p&gt;Webkul Helpdesk&lt;/p&gt;";}}}', 'a:0:{}', 'a:1:{i:1;a:3:{s:4:"type";s:7:"subject";s:14:"matchCondition";s:8:"contains";s:5:"match";s:5:"Sales";}}', 1, '2015-07-06 20:50:21', '2015-08-18 13:36:23'),
(3, 'Send Mail to Customer after adding reply to ticket', 'Send Mail to Customer after adding reply to ticket', 'a:2:{s:5:"value";s:6:"agents";s:6:"agents";a:1:{i:0;s:3:"all";}}', 'a:1:{i:0;a:1:{s:4:"type";s:11:"reply_added";}}', 'a:1:{i:0;a:2:{s:4:"type";s:13:"mail_customer";s:6:"action";a:3:{s:13:"emailtemplate";s:0:"";s:7:"subject";s:54:" {{ticket.agentname}} replied you ticket {{ticket.id}}";s:7:"message";s:57:"&lt;p&gt;&amp;nbsp;{{ticket.threaddescription}}&lt;/p&gt;";}}}', 'a:0:{}', 'a:1:{i:0;a:3:{s:4:"type";s:7:"created";s:14:"matchCondition";s:5:"after";s:5:"match";s:10:"2015-08-01";}}', 1, '2015-08-05 19:02:12', '2015-08-18 13:33:09'),
(4, 'Send Mail to Agent after adding query to ticket', 'Send Mail to Agent after adding query to ticket', 'a:1:{s:5:"value";s:8:"customer";}', 'a:1:{i:0;a:1:{s:4:"type";s:11:"reply_added";}}', 'a:1:{i:2;a:2:{s:4:"type";s:10:"mail_agent";s:6:"action";a:4:{s:5:"agent";s:7:"current";s:13:"emailtemplate";s:1:"2";s:7:"subject";s:0:"";s:7:"message";s:29:"&lt;p&gt;&lt;br&gt;&lt;/p&gt;";}}}', 'a:1:{i:0;a:3:{s:4:"type";s:6:"source";s:14:"matchCondition";s:2:"is";s:5:"match";s:4:"mail";}}', 'a:1:{i:0;a:3:{s:4:"type";s:6:"source";s:14:"matchCondition";s:2:"is";s:5:"match";s:3:"web";}}', 1, '2015-08-10 11:31:28', '2015-08-18 13:32:43');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_groups`
--

INSERT INTO `ticket_ts_groups` (`id`, `automatic_assign`, `inform_time`, `inform_agent`, `date_added`, `date_updated`, `businesshour_id`) VALUES
(1, 1, '30', 2, '2015-06-08 15:00:59', '2015-08-18 13:20:50', 1),
(2, 1, '120', 2, '2015-06-08 15:22:46', '2015-08-18 13:20:58', 2);

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_group_descriptions`
--

INSERT INTO `ticket_ts_group_descriptions` (`id`, `group_id`, `name`, `description`, `language_id`) VALUES
(1, 1, 'Symfony', 'Symfony', 1),
(2, 2, 'Opencart', 'Opencart support extension module is a next generation support ticket management system which allows tons of modern ticket system features including email piping.', 1);

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_holidays`
--

INSERT INTO `ticket_ts_holidays` (`id`, `business_hour_id`, `name`, `from_date`, `to_date`, `date_added`, `date_updated`) VALUES
(1, 2, 'New Year Party', '2016-01-03', '2015-12-30', '2015-08-18 13:25:15', '2015-08-18 13:25:15'),
(2, 2, 'christmas', '2015-12-25', '2015-12-25', '2015-08-18 13:25:15', '2015-08-18 13:25:15');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_organizations`
--

INSERT INTO `ticket_ts_organizations` (`id`, `name`, `description`, `domain`, `note`, `image`, `customer_role`, `date_added`, `date_updated`) VALUES
(1, 'Webkul', 'Webkul', 'webkul.com', 'Webkul', '5', 1, '2015-07-16 15:05:22', '2015-07-16 15:22:52');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_responses`
--

INSERT INTO `ticket_ts_responses` (`id`, `name`, `actions`, `valid_for`, `date_added`, `date_updated`, `status`, `description`) VALUES
(1, 'Apply to Opencart', 'a:4:{i:0;a:2:{s:4:"type";s:12:"assign_group";s:6:"action";s:1:"3";}i:1;a:2:{s:4:"type";s:8:"priority";s:6:"action";s:1:"1";}i:2;a:2:{s:4:"type";s:4:"note";s:6:"action";a:1:{s:4:"note";s:104:"Hi  {{ticket.customeremail}},\r\n\r\nPlease provide your FTP and Opencart Admin details.\r\n\r\nRegards,\r\nNikhil";}}i:3;a:2:{s:4:"type";s:10:"mail_agent";s:6:"action";a:4:{s:5:"agent";s:6:"assign";s:13:"emailtemplate";s:1:"1";s:7:"subject";s:0:"";s:7:"message";s:29:"&lt;p&gt;&lt;br&gt;&lt;/p&gt;";}}}', 'a:2:{s:5:"value";s:6:"groups";s:6:"groups";a:1:{i:0;s:1:"3";}}', '2015-07-27 17:59:05', '2015-08-18 13:27:45', 1, 'Using this Response, you can assign ticket to Opencart and asked Customer for FTP details.');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_roles`
--

INSERT INTO `ticket_ts_roles` (`id`, `name`, `description`, `role`, `date_added`, `date_updated`) VALUES
(1, 'Admin', 'Admin can access complete HelpDesk system and can manage all pages and functionality.', 'a:3:{s:7:"tickets";a:13:{i:0;s:7:"tickets";i:1;s:14:"tickets.create";i:2;s:14:"tickets.delete";i:3;s:14:"tickets.assign";i:4;s:14:"tickets.update";i:5;s:12:"tickets.edit";i:6;s:13:"tickets.merge";i:7;s:13:"tickets.split";i:8;s:13:"tickets.addcc";i:9;s:13:"tickets.reply";i:10;s:15:"tickets.forword";i:11;s:16:"tickets.internal";i:12;s:20:"tickets.deletethread";}s:6:"agents";a:4:{i:0;s:6:"agents";i:1;s:10:"agents.add";i:2;s:11:"agents.edit";i:3;s:13:"agents.delete";}s:5:"admin";a:22:{i:0;s:5:"admin";i:1;s:14:"admin.activity";i:2;s:11:"admin.types";i:3;s:14:"admin.priority";i:4;s:12:"admin.status";i:5;s:18:"admin.customfields";i:6;s:19:"admin.ticketsfields";i:7;s:10:"admin.tags";i:8;s:12:"admin.emails";i:9;s:20:"admin.emailtemplates";i:10;s:11:"admin.level";i:11;s:11:"admin.roles";i:12;s:15:"admin.responses";i:13;s:12:"admin.groups";i:14;s:15:"admin.customers";i:15;s:19:"admin.organizations";i:16;s:19:"admin.businesshours";i:17;s:9:"admin.sla";i:18;s:11:"admin.rules";i:19;s:12:"admin.events";i:20;s:19:"admin.supportcenter";i:21;s:15:"admin.reporting";}}', '2015-06-11 21:08:02', '2015-08-18 13:22:18'),
(2, 'Default', 'Default can access only assign HelpDesk system and can manage Tickets, reply those, edit those, can forward etc.\r\nFeel free to change :)', 'a:1:{s:7:"tickets";a:6:{i:0;s:7:"tickets";i:1;s:12:"tickets.edit";i:2;s:13:"tickets.addcc";i:3;s:13:"tickets.reply";i:4;s:15:"tickets.forword";i:5;s:16:"tickets.internal";}}', '2015-06-11 21:08:02', '2015-08-18 13:23:20');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_rules`
--

INSERT INTO `ticket_ts_rules` (`id`, `name`, `description`, `actions`, `conditions_one`, `conditions_all`, `status`, `date_added`, `date_updated`) VALUES
(1, 'Check if it''s for opencart', 'Check if it''s for opencart', 'a:1:{i:0;a:2:{s:4:"type";s:12:"assign_agent";s:6:"action";s:1:"0";}}', 'a:1:{i:0;a:3:{s:4:"type";s:22:"subject_or_description";s:14:"matchCondition";s:8:"contains";s:5:"match";s:8:"opencart";}}', 'a:1:{i:0;a:3:{s:4:"type";s:7:"subject";s:14:"matchCondition";s:8:"contains";s:5:"match";s:8:"opencart";}}', 1, '2015-07-29 18:59:03', '2015-07-29 18:59:03');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_sla`
--

INSERT INTO `ticket_ts_sla` (`id`, `name`, `description`, `status`, `sort_order`, `conditions_all`, `conditions_one`, `date_added`, `date_updated`) VALUES
(1, 'SLA Default', 'SLA Default', 1, 1, 'a:0:{}', 'a:0:{}', '2015-08-13 12:32:47', '2015-08-18 13:30:52');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_sla_priority`
--

INSERT INTO `ticket_ts_sla_priority` (`id`, `sla_id`, `priority_id`, `respond_within`, `resolve_within`, `hours_type`, `status`) VALUES
(1, 1, 1, 'a:2:{s:4:"time";s:2:"10";s:4:"type";s:6:"minute";}', 'a:2:{s:4:"time";s:1:"1";s:4:"type";s:4:"days";}', 1, 1),
(2, 1, 2, 'a:2:{s:4:"time";s:2:"20";s:4:"type";s:6:"minute";}', 'a:2:{s:4:"time";s:1:"5";s:4:"type";s:5:"hours";}', 0, 1),
(3, 1, 3, 'a:2:{s:4:"time";s:0:"";s:4:"type";s:3:"min";}', 'a:2:{s:4:"time";s:0:"";s:4:"type";s:6:"minute";}', 0, 0),
(4, 1, 4, 'a:2:{s:4:"time";s:0:"";s:4:"type";s:3:"min";}', 'a:2:{s:4:"time";s:0:"";s:4:"type";s:3:"min";}', 0, 0),
(5, 1, 5, 'a:2:{s:4:"time";s:0:"";s:4:"type";s:3:"min";}', 'a:2:{s:4:"time";s:0:"";s:4:"type";s:3:"min";}', 0, 0);

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_ticket_priority`
--

INSERT INTO `ticket_ts_ticket_priority` (`id`, `status`, `date_added`, `date_updated`) VALUES
(1, 1, '2015-06-26 17:52:29', '2015-06-26 17:52:29'),
(2, 1, '2015-06-26 17:53:30', '2015-06-26 17:53:30'),
(3, 1, '2015-06-26 17:53:38', '2015-06-26 17:53:38'),
(4, 1, '2015-06-26 17:53:48', '2015-06-26 17:53:48'),
(5, 1, '2015-07-02 20:46:54', '2015-07-02 20:46:54');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_ticket_priority_description`
--

INSERT INTO `ticket_ts_ticket_priority_description` (`id`, `priority_id`, `name`, `description`, `language_id`) VALUES
(1, 1, 'High', 'If ticket is added with this priority means it''s related to big issue and agents will focus more on this priority added ticket(s)', 1),
(2, 2, 'Medium', '', 1),
(3, 3, 'Low', 'This Priority shows customer can wait and his/her task is not valuable than other customers.', 1),
(4, 4, 'Urgent', '', 1),
(5, 5, 'Webkul', 'This is Demo Priority.', 1);

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_ticket_status`
--

INSERT INTO `ticket_ts_ticket_status` (`id`, `status`, `date_added`, `date_updated`) VALUES
(3, 1, '2015-06-26 17:19:28', '2015-06-26 17:19:28'),
(4, 1, '2015-06-26 17:20:21', '2015-06-26 17:20:21'),
(5, 1, '2015-06-26 17:21:33', '2015-06-26 17:21:33'),
(6, 1, '2015-06-26 17:23:17', '2015-06-26 17:23:17'),
(7, 1, '2015-06-26 17:24:13', '2015-06-26 17:24:13'),
(8, 1, '2015-06-26 17:24:58', '2015-06-26 17:24:58'),
(9, 0, '2015-06-26 17:26:15', '2015-06-26 17:26:15');

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_ticket_status_description`
--

INSERT INTO `ticket_ts_ticket_status_description` (`id`, `status_id`, `name`, `description`, `language_id`) VALUES
(1, 3, 'Pending', 'Pending Status will be the status when ticket processing will be pending because of any reason.', 1),
(2, 4, 'New', 'When Ticket is generated by customer, New status will append to ticket. ', 1),
(3, 5, 'Open', 'Open status refer to a ticket that is not solved yet.', 1),
(4, 6, 'Closed', 'Closed status refer that agents are not working on this status applied ticket(s).', 1),
(5, 7, 'Resolved', 'For those tickets which once are successfully solved for both end and customer is satisfied :)', 1),
(6, 8, 'Spam', 'Status for those ticket which once are spam, not  real  ticket etc', 1),
(7, 9, 'Waiting For Customer', 'This status can be used is agents are working properly but form customer end they are not getting proper reply or getting very rarely.', 1);

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_ticket_types`
--

INSERT INTO `ticket_ts_ticket_types` (`id`, `date_added`, `date_updated`, `status`) VALUES
(8, '2015-06-09 13:02:29', '2015-06-09 13:02:29', 1),
(7, '2015-06-09 13:02:03', '2015-06-09 13:02:03', 1),
(6, '2015-06-08 20:16:41', '2015-06-08 20:16:41', 1),
(9, '2015-06-09 13:02:52', '2015-06-09 13:02:52', 1),
(10, '2015-07-06 19:47:29', '2015-07-06 19:47:29', 1),
(11, '2015-07-06 19:47:37', '2015-07-06 19:47:37', 1),
(12, '2015-07-06 19:48:16', '2015-07-06 19:48:16', 0);

-- --------------------------------------------------------

--
-- Dumping data for table `ticket_ts_ticket_types_description`
--

INSERT INTO `ticket_ts_ticket_types_description` (`id`, `type_id`, `name`, `description`, `language_id`) VALUES
(108, 7, 'Support', 'If customer is has any any problem regarding product / service and need support.', 1),
(105, 6, 'Pre-Sale Query', 'If customer is asking for any product and service in advance.', 1),
(107, 8, 'Refund', 'If customer is asking for refund :(', 1),
(106, 9, 'Question', 'If customer is has any question regarding any product and service.', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- end of help desk queries By Odai
