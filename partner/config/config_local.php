<?php
// HTTP
define('HTTP_SERVER', "http://commerce.sayidaty.local/partner/"); // server
define('HTTP_CATALOG', "http://commerce.sayidaty.local/");

// HTTPS
define('HTTPS_SERVER', "http://commerce.sayidaty.local/partner/"); // server
define('HTTPS_CATALOG', "http://commerce.sayidaty.local/");
define("MONGO_DEFUALT_HOST","localhost");

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'sayidaty');
define('DB_PASSWORD', 'sayidaty');
define('DB_DATABASE', 'sayidaty_net');
define('DB_PORT', '3306');
define('DB_PREFIX', '');
