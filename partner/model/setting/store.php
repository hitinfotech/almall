<?php

class ModelSettingStore extends Model {

    public function getStores($data = array()) {
        $cache = "store.getstores." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store ORDER BY url");

            if($query->num_rows){
                $this->cache->set($cache, $query->rows);
            }
            
            return $query->rows;
                
        } else {
            return $result;
        }
    }

    public function getStore($store_id) {

        $store = $this->db->query("SELECT s.*, md.name as mall_name FROM " . DB_PREFIX . "store s LEFT JOIN mall m ON(s.mall_id=m.mall_id) LEFT JOIN mall_description md ON(m.mall_id=md.mall_id) WHERE s.store_id='" . (int) $store_id . "' AND md.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY url ");
        return $store->row;
    }

    public function getCity($store_id) {

        $store = $this->db->query("SELECT * FROM " . DB_PREFIX . "store WHERE store_id='" . (int) $store_id . "' ORDER BY url ");
        return $store->row;
    }

}
