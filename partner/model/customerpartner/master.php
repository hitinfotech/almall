<?php

class ModelCustomerpartnerMaster extends Model {

    public function getPartnerIdBasedonProduct($productid) {
        return $this->db->query("SELECT c2p.customer_id as id FROM " . DB_PREFIX . "customerpartner_to_product c2p LEFT JOIN " . DB_PREFIX . "product p ON(c2p.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE c2p.product_id = '" . (int) $productid . "' AND p.status = 1 AND p2s.store_id = '" . $this->config->get('config_store_id') . "' ORDER BY c2p.id ASC ")->row;
    }

    public function getLatest() {
        $sql = "SELECT p.product_id,pd.description,p.image,p.price,p.tax_class_id,pd.name,c2c.avatar,c2c.backgroundcolor,c.customer_id,CONCAT(c.firstname ,' ',c.lastname) seller_name,cod.name country, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.deleted='no' AND ps.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM " . DB_PREFIX . "customerpartner_to_product c2p LEFT JOIN " . DB_PREFIX . "product p ON (c2p.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer c2c ON (c2c.customer_id = c2p.customer_id) LEFT JOIN " . DB_PREFIX . "customer c ON (c2c.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "country co ON (c2c.country_id = co.iso_code_2) LEFT JOIN " . DB_PREFIX . "country_description cod ON (co.country_id = cod.country_id AND cod.language_id = '" . (int) $this->config->get('config_language_id') . "') LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE c2c.is_partner = '1' AND p.status = '1' AND p.date_available <= NOW() AND pd.language_id = '" . $this->config->get('config_language_id') . "' AND p2s.store_id = '" . $this->config->get('config_store_id') . "'";

        if ($this->config->get('marketplace_seller_product_list_limit')) {
            $sql .= 'ORDER BY c2p.product_id DESC limit ' . $this->config->get('marketplace_seller_product_list_limit') . '';
        } else {
            $sql .= 'ORDER BY c2p.product_id DESC limit 8';
        }
        $query = $this->db->query($sql);

        $products = array();
        foreach ($query->rows as $key => $value) {
            $products[$value['product_id']] = $value;
        }

        return $products;
    }

    public function getPartnerCollectionCount($customerid) {
        return count($this->db->query("SELECT DISTINCT p.product_id FROM " . DB_PREFIX . "customerpartner_to_product c2p LEFT JOIN " . DB_PREFIX . "product p ON (c2p.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE c2p.customer_id='" . $customerid . "' AND p.status='1' AND p.date_available <= NOW() AND p2s.store_id = '" . $this->config->get('config_store_id') . "' ORDER BY c2p.product_id ")->rows);
    }

    public function getOldPartner() {

        if ($this->config->get('marketplace_seller_list_limit')) {
            return $this->db->query("SELECT *,cod.name as country,companylocality FROM " . DB_PREFIX . "customerpartner_to_customer c2c LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer_description cp2cd ON (c2c.customer_id = cp2cd.customer_id AND cp2cd.language_id='" . (int) $this->config->get('config_language_id') . "') LEFT JOIN " . DB_PREFIX . "country co ON (c2c.country_id = co.iso_code_2) LEFT JOIN " . DB_PREFIX . "country_description cod ON (co.country_id = cod.country_id AND cod.language_id = '" . (int) $this->config->get('config_language_id') . "') WHERE is_partner = 1 AND c2c.status = '1' ORDER BY c2c.customer_id ASC LIMIT " . $this->config->get('marketplace_seller_list_limit') . "")->rows;
        } else {
            return $this->db->query("SELECT *,cod.name as country,companylocality FROM " . DB_PREFIX . "customerpartner_to_customer c2c LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer_description cp2cd ON (c2c.customer_id = cp2cd.customer_id AND cp2cd.language_id='" . (int) $this->config->get('config_language_id') . "') LEFT JOIN " . DB_PREFIX . "country co ON (c2c.country_id = co.iso_code_2) LEFT JOIN " . DB_PREFIX . "country_description cod ON (co.country_id = cod.country_id AND cod.language_id = '" . (int) $this->config->get('config_language_id') . "') WHERE is_partner = 1 AND c2c.status = '1' ORDER BY c2c.customer_id ASC LIMIT 4")->rows;
        }
    }

    public function getProfile($customerid) {
        $sql = "SELECT c2c.customer_id,cp2cd.shortprofile,c2c.companybanner,c2c.companylogo,c2c.avatar,cp2cd.screenname,c2c.companylocality,c2c.twitterid,c2c.facebookid,c.firstname, c.lastname, cod.name as country FROM " . DB_PREFIX . "customerpartner_to_customer c2c LEFT JOIN " . DB_PREFIX . "customer c ON (c2c.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer_description cp2cd ON (c2c.customer_id = cp2cd.customer_id) LEFT JOIN " . DB_PREFIX . "address a ON (c.address_id = a.address_id) LEFT JOIN " . DB_PREFIX . "country co ON (c2c.country = co.country_id) LEFT JOIN " . DB_PREFIX . "country_description cod ON(co.country_id=cod.country_id) WHERE cod.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2c.customer_id = '" . (int) $customerid . "' AND c2c.is_partner = '1' AND c.status = '1' AND cp2cd.language_id='" . (int) $this->config->get('config_language_id') . "'";

        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getFeedbackList($customerid) {
        $sql = "SELECT c2f.* FROM " . DB_PREFIX . "customerpartner_to_feedback c2f LEFT JOIN " . DB_PREFIX . "customer c ON (c2f.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer cpc ON (cpc.customer_id = c.customer_id) where c2f.seller_id = '" . (int) $customerid . "'";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getTotalFeedback($customerid) {
        $query = $this->db->query("SELECT id FROM " . DB_PREFIX . "customerpartner_to_feedback c2f where c2f.seller_id='" . (int) $customerid . "'");
        return count($query->rows);
    }

    public function getAverageFeedback($customerid) {

        $sum = 0;

        $avg = $this->db->query("SELECT avg(feedprice) AS p,avg(feedvalue) AS v,avg(feedquality) AS q FROM `" . DB_PREFIX . "customerpartner_to_feedback` WHERE seller_id='" . (int) $customerid . "'")->row;

        foreach ($avg as $value) {

            $sum += $value;
        }

        $rating = $sum / 3;

        return $rating;
    }

    public function getProductFeedbackList($customerid) {
        $query = $this->db->query("SELECT r.*,pd.name,r.text FROM " . DB_PREFIX . "customerpartner_to_product c2p INNER JOIN " . DB_PREFIX . "review r ON (c2p.product_id = r.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (pd.product_id = c2p.product_id) WHERE c2p.customer_id = '" . (int) $customerid . "' AND pd.language_id = '" . $this->config->get('config_language_id') . "' AND r.status = 1 ");
        return $query->rows;
    }

    public function getTotalProductFeedbackList($customerid) {
        $query = $this->db->query("SELECT r.* FROM " . DB_PREFIX . "customerpartner_to_product c2p INNER JOIN " . DB_PREFIX . "review r ON (c2p.product_id = r.product_id) WHERE c2p.customer_id = '" . (int) $customerid . "' AND r.status = 1 ");
        return count($query->rows);
    }

    public function saveFeedback($data, $seller_id) {

        $result = $this->db->query("SELECT id FROM " . DB_PREFIX . "customerpartner_to_feedback WHERE customer_id = " . $this->partner->getId() . " AND seller_id = '" . (int) $seller_id . "'")->row;

        if (!$result) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "customerpartner_to_feedback SET customer_id = '" . $this->partner->getId() . "',seller_id = '" . (int) $seller_id . "', feedprice = '" . (int) $this->db->escape($data['price_rating']) . "',feedvalue = '" . $this->db->escape($data['value_rating']) . "',feedquality = '" . $this->db->escape($data['quality_rating']) . "', nickname = '" . $this->db->escape($data['name']) . "',  review = '" . $this->db->escape($data['text']) . "', createdate = NOW() ");
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_feedback set feedprice='" . $this->db->escape($data['price_rating']) . "',feedvalue='" . $this->db->escape($data['value_rating']) . "',feedquality='" . $this->db->escape($data['quality_rating']) . "', nickname='" . $this->db->escape($data['name']) . "', review='" . $this->db->escape($data['text']) . "',createdate = NOW() WHERE id = '" . $result['id'] . "'");
        }
    }

    public function getShopData($shop) {
        $sql = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer where companyname = '" . $this->db->escape($shop) . "'")->row;
        if ($sql)
            return false;
        return true;
    }

    public function getPartnerByEmail($email) {
        $result = $this->db->query("SELECT customer_id FROM customerpartner_to_customer cp2c WHERE email = '" . $this->db->escape($email) . "'");
        if ($result->num_rows) {
            return $result->row['customer_id'];
        }
        return 0;
    }

    public function add_customer($section_num, $data, $partner_id) {
        switch ($section_num) {
            case 1:
                if ($partner_id == 0) {
                    $this->db->query("INSERT INTO customerpartner_to_customer set email='" . $this->db->escape($data['email']) . "', fbs_email='" . $this->db->escape($data['fbs_email']) . "', firstname='" . $this->db->escape($data['firstname']) . "', lastname='" . $this->db->escape($data['lastname']) . "', telephone='" . $this->db->escape($data['telephone']) . "' , telecode='" . $this->db->escape($data['telecode']) . "' , salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', is_partner='0',commission='" . $this->config->get('config_commission') . "',flat_rate='" . $this->config->get('config_flatrate') . "', date_added=NOW(), date_modified=NOW() ");
                } else {
                    $this->db->query("UPDATE customerpartner_to_customer set email='" . $this->db->escape($data['email']) . "', fbs_email='" . $this->db->escape($data['fbs_email']) . "', firstname='" . $this->db->escape($data['firstname']) . "', lastname='" . $this->db->escape($data['lastname']) . "', telephone='" . $this->db->escape($data['telephone']) . "' , telecode='" . $this->db->escape($data['telecode']) . "' , salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', is_partner='0',commission='" . $this->config->get('config_commission') . "',flat_rate='" . $this->config->get('config_flatrate') . "', date_modified=NOW() WHERE customer_id='" . (int) $partner_id . "'");
                }
                break;

            case 2:
                $this->db->query(" UPDATE customerpartner_to_customer set num_of_branches='" . (int) $data['num_of_branches'] . "',admin_telecode = '" . $this->db->escape($data['admin_telecode']) . "', country_id='" . (int) $data['country_id'] . "', zone_id='" . (int) $data['zone_id'] . "', website='" . $this->db->escape($data['website']) . "', twitterid='" . $this->db->escape($data['twitter']) . "', facebookid='" . $this->db->escape($data['facebook']) . "', instagramid='" . $this->db->escape($data['instagram']) . "',  warehouse_address='" . $this->db->escape($data['warehouse_address']) . "',  opening_time='" . $this->db->escape($data['opening_hours']) . "', admin_name='" . $this->db->escape($data['admin_name']) . "',admin_email='" . $this->db->escape($data['admin_email']) . "',admin_phone='" . $this->db->escape($data['admin_phone']) . "', available_country='" . $this->db->escape($data['available_countries']) . "', companybanner='" . $this->db->escape($data['logo']) . "', date_modified=NOW() WHERE customer_id='" . (int) $partner_id . "'");
                $this->db->query("DELETE FROM  customerpartner_to_customer_description WHERE customer_id='" . (int) $partner_id . "' ");
                $this->db->query("INSERT INTO customerpartner_to_customer_description set customer_id='" . (int) $partner_id . "', language_id='1', screenname='" . $this->db->escape($data['screen_name']) . "',shortprofile='" . $this->db->escape($data['product_description']) . "' ");
                $this->db->query("INSERT INTO customerpartner_to_customer_description set customer_id='" . (int) $partner_id . "', language_id='2', screenname='" . $this->db->escape($data['screen_name']) . "',shortprofile='" . $this->db->escape($data['product_description']) . "' ");

                $this->db->query("DELETE FROM  url_partner WHERE query='partner_id=" . (int) $partner_id . "' ");
                $this->db->query("INSERT INTO url_partner set query='partner_id=" . (int) $partner_id . "', keyword='seller/" . $this->db->escape($data['clienturl']) . "'");

                if(!empty($data['execluded_countries'])){
                  $available_countries = array_column($this->getExecludedCountries($partner_id),'country_id');
                  foreach($data['execluded_countries'] as $key2 => $country){
                    if(! in_array($country,$available_countries)){
                      $this->db->query("INSERT IGNORE INTO seller_execluded_countries (customer_id,country_id) values (".$partner_id.",".$country.")");
                    }
                  }
                  foreach($available_countries as $key1 => $country1){
                    if(!in_array($country1,$data['execluded_countries'])){
                      $this->db->query("DELETE FROM seller_execluded_countries WHERE country_id=".$country1." AND customer_id=".$partner_id);
                    }
                  }
                }else{
                  $this->db->query("DELETE FROM seller_execluded_countries WHERE  customer_id=".$partner_id);
                }
                break;

            case 3:

                $this->db->query("DELETE FROM legal_info WHERE  partner_id='" . (int) $partner_id . "'");
                $this->db->query(" INSERT INTO legal_info set partner_id='" . (int) $partner_id . "', legal_name='" . $this->db->escape($data['legal_name']) . "', company_name='" . $this->db->escape($data['company_name']) . "',trade_license_number = '" . $this->db->escape($data['trade_license_number']) . "',address_license_number = '" . $this->db->escape($data['address_license_number']) . "',direct_officer_number_telecode = '" . $this->db->escape($data['office_num_telcode']) . "',telephone_telecode = '" . $this->db->escape($data['moblie_num_telcode']) . "', nationality='" . $this->db->escape($data['nationality']) . "',passport_num='" . $this->db->escape($data['passport_num']) . "', email='" . $this->db->escape($data['email_address']) . "', direct_officer_number='" . $this->db->escape($data['office_num']) . "', telephone='" . $this->db->escape($data['moblie_num']) . "' , passport='" . $this->db->escape($data['passport_copy']) . "' , license='" . $this->db->escape($data['license']) . "' ");
                $this->db->query(" UPDATE customerpartner_to_customer_description set companyname='" . $this->db->escape($data['company_name']) . "' WHERE customer_id='" . (int) $partner_id . "' ");

                break;
            case 4 :
                if (isset($data['eligible_vat']) && $partner_id != 0) {

                    $path=$company_name=$business_address=$tax_registration_number=$issue_date='';

                    if (trim($data['eligible_vat']['doc_company_name']) != '') {
                        $company_name = $this->db->escape($data['eligible_vat']['doc_company_name']);
                    }
                    if (trim($data['eligible_vat']['business_address']) != '') {
                        $business_address = $this->db->escape($data['eligible_vat']['business_address']);
                    }
                    if (trim($data['eligible_vat']['tax_reg_num']) != '') {
                        $tax_registration_number = $this->db->escape($data['eligible_vat']['tax_reg_num']);
                    }
                    if (trim($data['eligible_vat']['issue_date']) != '') {
                        $issue_date = $this->db->escape($data['eligible_vat']['issue_date']);
                    }
                    $eligible_vat_info = $this->db->query("SELECT customer_id FROM eligible_vat WHERE customer_id = '" . (int) $partner_id . "'");

                    if ($eligible_vat_info->num_rows) {
                        $eligible_sql = "UPDATE eligible_vat SET company_name='".$company_name."',business_address='".$business_address."',tax_registration_number='".$tax_registration_number."',issue_date='$issue_date',checked=1 $path WHERE customer_id='" . (int) $partner_id . "' ";
                    } else {
                        $eligible_sql = "INSERT INTO eligible_vat SET company_name='".$company_name."',business_address='".$business_address."',tax_registration_number='".$tax_registration_number."',issue_date='$issue_date',customer_id='" . (int) $partner_id . "',checked=1 $path";
                    }
                    $this->db->query($eligible_sql);
                }
                $this->db->query("DELETE FROM financial_info WHERE  partner_id='" . (int) $partner_id . "'");
                $this->db->query(" INSERT INTO financial_info set partner_id='" . (int) $partner_id . "', company_officer_name='" . $this->db->escape($data['fin_contact_name']) . "', email='" . $this->db->escape($data['fin_email']) . "', telephone='" . $this->db->escape($data['fin_num']) . "',fin_telephone_telecode='" . $this->db->escape($data['fin_telephone_telecode']) . "', beneficiary_name='" . $this->db->escape($data['beneficiary_name']) . "', bank_name='" . $this->db->escape($data['bank_name']) . "', bank_account='" . $this->db->escape($data['bank_account']) . "', iban_code='" . $this->db->escape($data['iban_code']) . "', swift_code='" . $this->db->escape($data['swift_code']) . "', currency='" . $this->db->escape($data['currency']) . "', bank_number ='" . (int) $data['bank_number'] . "', is_both ='" . (int) $data['is_both'] . "'");

                break;
        }

        return true;
    }

    public function upload_image($uploads) {

        $name = key($uploads);
        $this->load->model("tool/image");
        $renamedImage = $license = $logo = $passport = '';
        if (!empty($uploads)) {
            if (isset($uploads['passport_copy']['name']) AND $uploads['passport_copy']['name']) {
                $renamedImage = rand(100000, 999999) . basename(preg_replace('~[^\w\./\\\\]+~', '', $uploads['passport_copy']["name"]));
                $dest = DIR_IMAGE . 'catalog/';
                $this->create_path($dest);
                move_uploaded_file($uploads['passport_copy']["tmp_name"], $dest . $renamedImage);
                $this->model_tool_image->uploadimage('catalog/' . $renamedImage, 500, 500);
                $this->model_tool_image->uploadimage('catalog/' . $renamedImage, 125, 125);
                $passport = 'catalog/' . $renamedImage;
                return array("success" => $passport);
            }

            if (isset($uploads['license']['name']) AND $uploads['license']['name']) {
                $renamedImage = rand(100000, 999999) . basename(preg_replace('~[^\w\./\\\\]+~', '', $uploads['license']["name"]));
                $dest = DIR_IMAGE . 'catalog/';
                $this->create_path($dest);
                move_uploaded_file($uploads['license']["tmp_name"], $dest . $renamedImage);
                $this->model_tool_image->uploadimage('catalog/' . $renamedImage, 500, 500);
                $this->model_tool_image->uploadimage('catalog/' . $renamedImage, 125, 125);
                $license = 'catalog/' . $renamedImage;
                return array("success" => $license);
            }

            if (isset($uploads['logo']['name']) AND $uploads['logo']['name']) {
                $renamedImage = rand(100000, 999999) . basename(preg_replace('~[^\w\./\\\\]+~', '', $uploads['logo']["name"]));
                $dest = DIR_IMAGE . 'catalog/';
                $this->create_path($dest);
                move_uploaded_file($uploads['logo']["tmp_name"], $dest . $renamedImage);
                $this->model_tool_image->uploadimage('catalog/' . $renamedImage, 500, 500);
                $this->model_tool_image->uploadimage('catalog/' . $renamedImage, 125, 125);
                $logo = 'catalog/' . $renamedImage;
                return array("success" => $logo);
            }
        }
    }

    public function upload_document($document,$partner_id)
    {
        if (isset($document['document'])) {
            $doc_path = $this->upload_file('customerpartner_vat_document', 'document', $partner_id);
            return $doc_path;
        }
    }

    private function create_path($path) {
        $directories = explode("/", $path);
        $string = '';
        foreach ($directories as $directory) {
            $string .= "/{$directory}";

            if (!is_dir($string)) {
                exec("mkdir -m 0777 {$string}");
            }
        }
    }

    public function getExecludedCountries($customer_id){
      $query = $this->db->query("SELECT * FROM seller_execluded_countries WHERE customer_id=".(int)$customer_id);
      if(!empty($query->num_rows)){
        return $query->rows;
      }
      return array();
    }

}
