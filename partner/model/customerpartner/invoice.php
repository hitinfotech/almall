<?php
class ModelCustomerpartnerInvoice extends Model {

	public function viewtotal($data){

		$sql ="SELECT cp2i.*,cp2cd.companyname FROM customerpartner_to_invoice cp2i LEFT JOIN customerpartner_to_customer_description cp2cd ON (cp2i.customer_id=cp2cd.customer_id) WHERE cp2cd.language_id= ".(int) $this->config->get('config_language_id')." AND cp2i.customer_id= ".(int) $this->partner->getId();

		if (!empty($data['filter_month'])) {
			$sql .= " AND cp2i.month=".(int)$data['filter_month'];
		}

		if (!empty($data['filter_year'])) {
			$sql .= " AND cp2i.year=".(int)$data['filter_year'];
		}

		$sort_data = array(
			'cp2i.id',
			'cp2i.month',
			'cp2i.year',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY cp2i.id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$result=$this->db->query($sql);

		return $result->rows;
	}

	public function viewtotalentry($data){

		$sql ="SELECT count(*) as invoices_count FROM customerpartner_to_invoice cp2i LEFT JOIN customerpartner_to_customer cp2c ON (cp2i.customer_id=cp2c.customer_id) WHERE cp2i.customer_id= ".(int) $this->partner->getId();

		if (!empty($data['filter_month'])) {
			$sql .= " AND cp2i.month=".(int)$data['filter_month'];
		}

		if (!empty($data['filter_year'])) {
			$sql .= " AND cp2i.year=".(int)$data['filter_year'];
		}

		$result = $this->db->query($sql);
			if(!empty($result->row))
					return $result->row['invoices_count'];
		return 0;
	}


	public function getPartnerAmount($partner_id){
		$total = $this->db->query("SELECT SUM(c2o.quantity) quantity,SUM(c2o.price) total,SUM(c2o.admin) admin,SUM(c2o.customer) customer FROM ".DB_PREFIX ."customerpartner_to_order c2o WHERE c2o.customer_id ='".(int)$partner_id."'")->row;

		$paid = $this->db->query("SELECT SUM(c2t.amount) total FROM ".DB_PREFIX ."customerpartner_to_transaction c2t WHERE c2t.customer_id ='".(int)$partner_id."'")->row;

		$total['paid'] = $paid['total'];

		return($total);
	}


	public function getTransactions($customer_id, $start = 0, $limit = 10) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 10;
		}

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_transaction WHERE customer_id = '" . (int)$customer_id . "' ORDER BY date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalTransactions($customer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total  FROM " . DB_PREFIX . "customerpartner_to_transaction WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getTransactionTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customerpartner_to_transaction WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

}
?>
