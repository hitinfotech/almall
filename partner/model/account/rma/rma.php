<?php

class ModelAccountRmarma extends Model {

    public function getrmaorders($data) {

        $query = '';

        if (!$this->customer->getId()) {
            $query .= "AND email = '" . $this->db->escape($data['email']) . "'";
        }
        
        if ((int) $this->config->get('wk_rma_system_time')) {
            $query .= " AND o.date_added >= DATE_SUB(CURDATE(), INTERVAL '" . (int) $this->config->get('wk_rma_system_time') . "' DAY) ";
        }
        
        // AND o.order_id NOT IN (SELECT order_id FROM ".DB_PREFIX."wk_rma_order)

        $sql = "SELECT DISTINCT o.order_id,o.total,o.date_added FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE o.customer_id = '" . $this->customer->getId() . "'   AND o.deleted='0' $query";

        if ($this->config->get('wk_rma_system_orders')) {
            $sql.= " AND o.order_status_id IN (" . implode(',', $this->config->get('wk_rma_system_orders')) . ")";
        }
        $implode = array();

        if (!empty($data['filter_model'])) {
            $implode[] = "op.model LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_model'])) . "%'";
        }

        if (isset($data['filter_order']) && !is_null($data['filter_order'])) {
            $implode[] = "o.order_id = '" . (int) $data['filter_order'] . "'";
        }

        if (!empty($data['filter_date'])) {
            $implode[] = "LCASE(o.date_added) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function insertOrderRma($data, $img_folder) {

        // // if price comparison enabled
        // if($this->config->get('wk_mpaddproduct_status')){
        // 	//chk product is of admin or seller
        // 	$cp2p_chk = count($this->db->query("SELECT * FROM ".DB_PREFIX."customerpartner_to_product WHERE product_id = '".(int)$data['selected'][0]."' ")->rows);
        // 	$pc_chk = count($this->db->query("SELECT * FROM ".DB_PREFIX."price_comparison WHERE product_id = '".(int)$data['selected'][0]."' ")->rows);
        // 	if($cp2p_chk != $pc_chk){
        // 		if()
        // 		//seller product
        // 		$sellerId = $this->db->query("SELECT cp2p.customer_id FROM ".DB_PREFIX."customerpartner_to_product cp2p LEFT JOIN ".DB_PREFIX."customerpartner_to_customer cp2c ON (cp2p.customer_id = cp2c.customer_id) WHERE cp2p.product_id = '".(int)$data['selected'][0]."' ")->row;
        // 		if(!empty($sellerId['customer_id']) && isset($sellerId))
        // 			$customer_id = $sellerId['customer_id'];
        // 		else
        // 			$customer_id = 0;
        // 	}else{
        // 		//admin product
        // 		$customer_id = 0;
        // 	}
        // }else{// not price comparison
        // 	//chk product is of admin or seller
        // 	$cp2p_chk = $this->db->query("SELECT cp2p.customer_id FROM ".DB_PREFIX."customerpartner_to_product cp2p LEFT JOIN ".DB_PREFIX."product p ON(cp2p.product_id = p.product_id) WHERE cp2p.product_id = '".(int)$data['selected'][0]."' ")->row;
        // 	if(isset($cp2p_chk['customer_id']) && $cp2p_chk['customer_id']){ // seller product
        // 		$customer_id = $cp2p_chk['customer_id'];
        // 	}else{ // admin product
        // 		$customer_id = 0;
        // 	}
        // }
        // $getDefaultStatus = $this->getDefaultStatus();

        $seller_id = $data['seller'][0];
        $seller_name = $this->db->query("SELECT screenname from customerpartner_to_customer_description where customer_id='".$seller_id."' and language_id='1'")->row['screenname'];

        $products = array();

        foreach ($data['selected'] as $value) {
            $product_name = $this->db->query("SELECT name FROM " . DB_PREFIX . "product_description WHERE product_id= '" . $value . "'")->row;
            $products[] = $product_name['name'];
        }

        $product_name = implode(',', $products);

        $this->db->query("INSERT INTO " . DB_PREFIX . "wk_rma_order SET order_id = '" . $data['order'] . "', images = '" . $img_folder . "',add_info = '" . $this->db->escape(nl2br($data['info'])) . "', customer_status_id = '" . $data['status'] . "',rma_auth_no = '" . $this->db->escape($data['autono']) . "', customer_id = '" . (int) $seller_id . "', date = NOW() ");
        $rma_id = $this->db->getLastId();
        $email = $this->customer->getEmail();

        $this->load->model('localisation/rmaquantity');
        $this->model_localisation_rmaquantity->ChangeProductOrderQuantity($rma_id);
        
        
        if (isset($this->session->data['rma_login'])) {
            $email = $this->session->data['rma_login'];
        }
        $this->db->query("INSERT INTO " . DB_PREFIX . "wk_rma_customer SET rma_id = '" . $rma_id . "',customer_id = '" . $this->customer->getId() . "',email = '" . $this->db->escape($email) . "'");

        foreach ($data['selected'] as $key => $product) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "wk_rma_product SET rma_id = '" . $rma_id . "',product_id = '" . (int) $product . "', customer_id = '" . (int) $seller_id . "', quantity = '" . $data['quantity'][$key] . "',reason = '" . (int) $data['reason'][$key] . "', order_product_id = '" . $data['product'][$key] . "'");
            $products_ids[] = $product;
            $reason = $this->getReason((int) $data['reason'][$key]);
            $reasons[] = $reason;
        }
        //$data['prod_ids'] = implode(',',$products_ids);
        //$data['reasons_data'] = implode(',',$reasons);
        $mailData['rma_id'] = $rma_id;


        if ($this->config->get('wk_rma_status')) {
            $this->load->model('customerpartner/mail');

            $this->load->model('account/order');
            $this->load->model('account/customerpartnerorder');
            $order_info = $this->model_account_order->getOrder($data['order']);
            if ($order_info) {
                $order_id = $order_info['order_id'];
                $order_status = $order_info['order_status_id'];
                // Shipping Address
                $this->load->model('account/address');
                $this->load->model('shipping/wk_custom_shipping');

                $order_product_query = $this->db->query("SELECT op.product_id,op.order_product_id,op.total,op.price, op.name, op.model, op.quantity,p.sku FROM " . DB_PREFIX . "order_product op LEFT JOIN product p ON (op.product_id = p.product_id) WHERE order_id = '" . (int) $order_id . "'");

                $mailToSellers = array();

                $shipping = 0;
                $paid_shipping = false;
                $admin_shipping_method = false;
                $resultData = $this->model_account_customerpartnerorder->sellerAdminData($this->cart->getProducts());

                if ($this->config->get('marketplace_allowed_shipping_method')) {
                    if (in_array($order_info['shipping_code'], $this->config->get('marketplace_allowed_shipping_method'))) {
                        $admin_shipping_method = true;
                        // if($this->config->get('marketplace_divide_shipping') &&  $this->config->get('marketplace_divide_shipping') == 'yes'){
                        $sellers_count = count($resultData);
                        foreach ($resultData as $key => $value) {
                            if ($value['seller'] == 'admin') {
                                $sellers_count = count($resultData) - 1;
                            }
                        }
                        if ($sellers_count == 1) {
                            $shipping = $this->session->data['shipping_method']['cost'];
                        }
                        // }
                    }
                }


                foreach ($order_product_query->rows as $product) {

                    $prsql = '';

                    $mpSellers = $this->db->query("SELECT c2c.email,c2c.customer_id,p.product_id,p.subtract,c2c.commission FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "customerpartner_to_product c2p ON (p.product_id = c2p.product_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer c2c ON (c2c.customer_id = c2p.customer_id) WHERE p.product_id = '" . $product['product_id'] . "' $prsql ORDER BY c2p.id ASC ")->row;

                    if (isset($mpSellers['email']) AND ! empty($mpSellers['email'])) {

                        $option_data = array();

                        $order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . (int) $product['order_product_id'] . "'   AND deleted='0'");

                        foreach ($order_option_query->rows as $option) {
                            if ($option['type'] != 'file') {
                                $value = $option['value'];
                            } else {
                                $value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
                            }

                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                            );
                        }

                        $product_total = $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0);

                        $products = array(
                            'product_id' => $product['product_id'],
                            'name' => $product['name'],
                            'model' => $product['model'],
                            'sku' => $product['sku'],
                            'option' => $option_data,
                            'quantity' => $product['quantity'],
                            'product_total' => $product_total, // without symbol
                            'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                            'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
                        );

                        $i = 0;

                        if ($mailToSellers) {

                            foreach ($mailToSellers as $key => $value) {
                                foreach ($value as $key1 => $value1) {
                                    $i = 0;
                                    if ($key1 == 'email' AND $value1 == $mpSellers['email']) {
                                        $mailToSellers[$key]['products'][] = $products;
                                        $mailToSellers[$key]['total'] = $product_total + $mailToSellers[$key]['total'];
                                        break;
                                    } else {
                                        $i++;
                                    }
                                }
                            }
                        } else {
                            $mailToSellers[] = array('email' => $mpSellers['email'],
                                'customer_id' => $mpSellers['customer_id'],
                                'seller_email' => $mpSellers['email'],
                                'products' => array(0 => $products),
                                'total' => $product_total
                            );
                        }

                        if ($i > 0) {
                            $mailToSellers[] = array('email' => $mpSellers['email'],
                                'customer_id' => $mpSellers['customer_id'],
                                'seller_email' => $mpSellers['email'],
                                'products' => array(0 => $products),
                                'total' => $product_total
                            );
                        }
                    }
                }


                if ($this->config->get('marketplace_mailtoseller')) {

                    // Send out order confirmation mail
                    $this->load->language('default');
                    $this->load->language('mail/rma');

                    $subject = sprintf($this->language->get('text_new_subject'), $order_info['store_name'], $order_id);

                    // HTML Mail for seller
                    $data = array();
                    $data['prod_ids'] = implode(',', $products_ids);
                    $data['reasons_data'] = implode(',', $reasons);

                    $data['title'] = sprintf($this->language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

                    $data['text_greeting'] = sprintf($this->language->get('text_rma_greeting'),$seller_name,$data['prod_ids'],$order_id);
                    $data['text_shipping'] = $this->language->get('text_shipping');

                    $data['text_link'] = $this->language->get('text_new_link');
                    $data['text_order_detail'] = $this->language->get('text_new_order_detail');
                    $data['text_order_status'] = $this->language->get('text_order_status');

                    $data['text_instruction'] = $this->language->get('text_new_instruction');
                    $data['text_order_id'] = $this->language->get('text_new_order_id');
                    $data['text_date_added'] = $this->language->get('text_new_date_added');
                    $data['text_payment_method'] = $this->language->get('text_new_payment_method');
                    $data['text_shipping_method'] = $this->language->get('text_new_shipping_method');
                    $data['text_email'] = $this->language->get('text_new_email');
                    $data['text_telephone'] = $this->language->get('text_new_telephone');
                    $data['text_ip'] = $this->language->get('text_new_ip');
                    $data['text_payment_address'] = $this->language->get('text_new_payment_address');
                    $data['text_shipping_address'] = $this->language->get('text_new_shipping_address');
                    $data['text_product'] = $this->language->get('text_new_product');
                    $data['text_model'] = $this->language->get('text_new_model');
                    $data['text_sku'] = $this->language->get('text_new_sku');
                    $data['text_quantity'] = $this->language->get('text_new_quantity');
                    $data['text_price'] = $this->language->get('text_new_price');
                    $data['text_total'] = $this->language->get('text_new_total');
                    //$data['text_footer'] = $this->language->get('text_new_footer');
                    $data['text_powered'] = $this->language->get('text_new_powered');
                    $data['text_buyer_name'] = $this->language->get('text_buyer_name');
                    $data['text_buyer_country'] = $this->language->get('text_buyer_country');
                    $data['text_item_number'] = $this->language->get('text_item_number');
                    $data['text_below_order_summary'] = $this->language->get('text_below_order_summary');
                    $data['text_order_number'] = $this->language->get('text_order_number');
                    $data['text_order_date'] = $this->language->get('text_order_date');
                    $data['text_rma_date'] = $this->language->get('text_rma_date');

                    $data['text_reason_for_return'] = $this->language->get('text_reason_for_return');
                    $data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
                    $data['store_name'] = $order_info['store_name'];
                    $data['store_url'] = $order_info['store_url'];
                    $data['customer_id'] = $order_info['customer_id'];
                    $data['link'] = $order_info['store_url'] . 'index.php?route=account/customerpartner/orderinfo&order_id=' . $order_id;
                    $data['order_id'] = $order_id;
                    $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
                    $data['payment_method'] = $order_info['payment_method'];
                    $data['shipping_method'] = $order_info['shipping_method'];
                    $data['email'] = $order_info['email'];
                    $data['telephone'] = $order_info['telephone'];
                    $data['ip'] = $order_info['ip'];
                    $data['order_status'] = $order_status;
                    $data['buyer_name'] = $order_info['payment_firstname'] . " " . $order_info['payment_lastname'];
                    $data['buyer_country'] = $order_info['payment_country'];
                    $data['rma_date'] = date("M d, Y");
                    $data['rma_details_page'] = $this->url->link("account/rma/viewrma",'vid='.$rma_id,'SSL');


                    $data['comment'] = false;


                    // Products
                    $data['products'] = array();

                    // Text Mail for seller
                    $textBasic = $this->language->get('text_new_order_id') . ' ' . $order_id . "\n";
                    $textBasic .= $this->language->get('text_new_date_added') . ' ' . date($this->language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
                    $textBasic .= $this->language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

                    // Order Totals
                    $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int) $order_id . "' AND deleted='0' ORDER BY sort_order ASC");

                    foreach ($order_total_query->rows as $total) {
                        $data['totals'][] = array(
                            'title' => $total['title'],
                            'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                        );
                    }
                    foreach ($mailToSellers as $value) {

                        //for template for seller
                        $data['products'] = $value['products'];

                        $html = $this->load->view($this->config->get('config_template') . '/template/mail/rma.tpl', $data);

                        //for text for seller
                        $products = $value['products'];
                        $text = $textBasic;
                        foreach ($products as $product) {
                            $text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($product['total']) . "\n";

                            foreach ($product['option'] as $option) {
                                $text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($option['value'])) . "\n";
                            }
                        }

                        $text .= $this->language->get('text_new_order_total') . "\n";

                        $text .= $this->currency->format($value['total'], $order_info['currency_code'], 1) . ': ' . html_entity_decode($this->currency->format($value['total'], $order_info['currency_code'], 1), ENT_NOQUOTES, 'UTF-8') . "\n";
                    }

                    $values = array(
                        'seller_name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
                        'product_name' => $product_name,
                        'order_id' => $order_id,
                        'order' => $html
                    );

                    $mailData['title'] = $this->language->get('text_rma_title');

                    //send mail to seller when rma created
                    if ($seller_id != 0) {
                        $mailData['seller_id'] = $seller_id;
                        $mailData['mail_id'] = $this->config->get('wk_rma_seller_mail');
                        $mailData['mail_from'] = $this->config->get('marketplace_adminmail');
                        $mailData['mail_to'] = $this->getSellerMail($seller_id);

                        $this->model_customerpartner_mail->mail($mailData, $values);
                    }
                    $mailData['mail_id'] = $this->config->get('wk_rma_customer_mail');
                    $mailData['mail_from'] = $this->config->get('marketplace_adminmail');
                    $mailData['mail_to'] = $this->customer->getEmail();
                    $this->model_customerpartner_mail->mail($mailData, $values);

                    $mailData['mail_id'] = $this->config->get('wk_rma_admin_mail');
                    $mailData['mail_from'] = $this->customer->getEmail();
                    $mailData['mail_to'] = $this->config->get('marketplace_adminmail');
                    $this->model_customerpartner_mail->mail($mailData, $values);
                }
            }
        }
    }

    protected function getSellerMail($seller_id) {
        $query = $this->db->query("SELECT email FROM " . DB_PREFIX . "customerpartner_to_customer WHERE customer_id = " . $seller_id);
        return $query->row['email'];
    }

    public function ramDetails($data) {

        $implode_inner = '';
        if (isset($data['filter_qty']) && !is_null($data['filter_qty'])) {
            $implode_inner = " AND wop.quantity = '" . (int) $data['filter_qty'] . "'";
        }

        if (!$this->customer->getId() AND isset($this->session->data['rma_login'])) {
            $sqlForCustomer = " wrc.customer_id = 0 AND email = '" . $this->db->escape($this->session->data['rma_login']) . "'";
        } else {
            $sqlForCustomer = " wrc.customer_id = '" . $this->customer->getId() . "'";
        }
        
        // $ss = $this->db->query("SELECT wop.quantity * op.price AS new FROM ".DB_PREFIX."wk_rma_product wop LEFT JOIN ".DB_PREFIX."wk_rma_order wro ON (wop.rma_id = wro.id) LEFT JOIN ".DB_PREFIX."order_product op ON (wop.order_product_id = op.order_product_id) ")->rows;
        // $sql = "SELECT wro.id,wro.order_id,wro.date,ot.value,wro.customer_status_id, (SELECT SUM(wop.quantity * op.price) AS total FROM ".DB_PREFIX."wk_rma_product wop LEFT JOIN ".DB_PREFIX."wk_rma_order wro ON (wop.rma_id = wro.id) LEFT JOIN ".DB_PREFIX."order_product op ON (wop.order_product_id=op.order_product_id)) AS total, (SELECT SUM(wop.quantity) FROM ".DB_PREFIX."wk_rma_product wop WHERE wop.rma_id = wro.id $implode_inner) as quantity FROM " . DB_PREFIX . "wk_rma_order wro LEFT JOIN " . DB_PREFIX . "wk_rma_customer wrc ON (wrc.rma_id = wro.id) LEFT JOIN ". DB_PREFIX ."order_total ot ON (wro.order_id = ot.order_id) WHERE ot.code = 'total' AND $sqlForCustomer ";

        $sql = "SELECT wro.id, wro.order_id, wro.date, ot.value, wro.customer_status_id, (SELECT SUM(wop.quantity) FROM " . DB_PREFIX . "wk_rma_product wop WHERE wop.rma_id = wro.id $implode_inner) as quantity, rs.name as rma_status, rs.color FROM " . DB_PREFIX . "wk_rma_order wro LEFT JOIN " . DB_PREFIX . "wk_rma_customer wrc ON (wrc.rma_id = wro.id) LEFT JOIN " . DB_PREFIX . "order_total ot ON (wro.order_id = ot.order_id) LEFT JOIN rma_status rs ON(wro.customer_status_id=rs.rma_status_id AND rs.language_id='". (int)$this->config->get('config_language_id') ."') WHERE ot.code = 'total' AND $sqlForCustomer ";


        $implode = array();

        if (!empty($data['filter_id'])) {
            $implode[] = "wro.id = '" . (int) $data['filter_id'] . "'";
        }

        if (isset($data['filter_order']) && !is_null($data['filter_order'])) {
            $implode[] = "wro.order_id = '" . (int) $data['filter_order'] . "'";
        }

        if (isset($data['filter_rma_status']) && !is_null($data['filter_rma_status'])) {
            $implode[] = "wro.customer_status_id = '" . (int) $data['filter_rma_status'] . "'";
        }

        if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
            $implode[] = " ot.value LIKE '%" . (float) $data['filter_price'] . "%'";
        }

        if (!empty($data['filter_date'])) {
            $implode[] = "LCASE(wro.date) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'wro.id',
            'wro.order_id',
            'ot.value',
            'wro.customer_status_id',
            'wop.quantity',
            'wro.date',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY wro.id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function rmaTotal($data) {

        $implode_inner = '';

        if (isset($data['filter_qty']) && !is_null($data['filter_qty'])) {
            $implode_inner = " AND wop.quantity = '" . (int) $data['filter_qty'] . "'";
        }

        if (!$this->customer->getId() AND isset($this->session->data['rma_login']))
            $sqlForCustomer = " wrc.customer_id = 0 AND email = '" . $this->db->escape($this->session->data['rma_login']) . "'";
        else
            $sqlForCustomer = " wrc.customer_id = '" . $this->customer->getId() . "'";

        $sql = "SELECT wro.id,wro.order_id,wro.date,ot.value,(SELECT SUM(quantity) FROM " . DB_PREFIX . "wk_rma_product wop WHERE wop.rma_id = wro.id $implode_inner) as quantity FROM " . DB_PREFIX . "wk_rma_order wro LEFT JOIN " . DB_PREFIX . "wk_rma_customer wrc ON (wrc.rma_id = wro.id) LEFT JOIN " . DB_PREFIX . "order_total ot ON (wro.order_id = ot.order_id) WHERE ot.code = 'total' AND $sqlForCustomer ";

        $implode = array();

        if (!empty($data['filter_id'])) {
            $implode[] = "wro.id = '" . (int) $data['filter_id'] . "'";
        }

        if (isset($data['filter_order']) && !is_null($data['filter_order'])) {
            $implode[] = "wro.order_id = '" . (int) $data['filter_order'] . "'";
        }

        if (isset($data['filter_rma_status']) && !is_null($data['filter_rma_status'])) {
            $implode[] = "wro.customer_status_id = '" . (int) $data['filter_rma_status'] . "'";
        }

        if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
            $implode[] = " ot.value LIKE '%" . (float) $data['filter_price'] . "%'";
        }

        if (!empty($data['filter_date'])) {
            $implode[] = "LCASE(wro.date) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $result = $this->db->query($sql);

        $i = 0;
        foreach ($result->rows as $newResult) {
            if ($newResult['quantity'])
                $i++;
        }

        return $i;
    }

    public function viewRmaid($id) {

        $query = '';

        if (!$this->customer->getId()) {
            $query = "AND email = '" . $this->db->escape($this->session->data['rma_login']) . "'";
        }
        
        $result = $this->db->query("SELECT wro.*,wro.customer_status_id as customer_st, rs.name rma_status, rs.color FROM " . DB_PREFIX . "wk_rma_order wro LEFT JOIN " . DB_PREFIX . "wk_rma_customer wrc ON (wrc.rma_id = wro.id) LEFT JOIN rma_status rs ON(rs.rma_status_id=wro.customer_status_id AND rs.language_id='". (int)$this->config->get('config_language_id') ."') WHERE wro.id ='" . (int) $id . "' AND wrc.customer_id='" . (int) $this->customer->getId() . "' $query ");

        return $result->row;
    }

    public function prodetails($id, $order_id) {

        $sql = $this->db->query("SELECT op.product_id,op.name,op.model,op.quantity as ordered,op.tax tax,op.price,wrr.reason,wrp.quantity as returned, wrp.customer_id,op.order_product_id FROM " . DB_PREFIX . "wk_rma_product wrp LEFT JOIN " . DB_PREFIX . "order_product op ON (wrp.product_id = op.product_id AND wrp.order_product_id = op.order_product_id) LEFT JOIN " . DB_PREFIX . "wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) WHERE op.order_id = '" . (int) $order_id . "' AND wrp.rma_id='" . (int) $id . "' AND wrr.language_id= '" . $this->config->get('config_language_id') . "'")->rows;

        if (!$sql) {
            $sql = $this->db->query("SELECT p.product_id,pd.name,p.model,p.price,wrr.reason,wrp.quantity as returned,wrp.customer_id FROM " . DB_PREFIX . "wk_rma_product wrp LEFT JOIN " . DB_PREFIX . "product p ON (wrp.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) WHERE wrp.rma_id='" . (int) $id . "' AND pd.language_id = '" . $this->config->get('config_language_id') . "' AND wrr.language_id= '" . $this->config->get('config_language_id') . "'")->rows;

            if ($sql)
                foreach ($sql as $key => $value) {
                    $sql[$key]['ordered'] = '0';
                    $sql[$key]['tax'] = '0';
                }
        }

        return $sql;
    }

    public function updateRmaSta($status, $vid, $reopen = false) {
        if ($reopen) {
            $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_order SET customer_status_id = '" . (int) $status . "' WHERE id ='" . (int) $vid . "'");
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_order SET customer_status_id = '" . (int) $status . "' WHERE id ='" . (int) $vid . "'");
        }
        $this->load->model('localisation/rmaquantity');
        $this->model_localisation_rmaquantity->ChangeProductOrderQuantity($vid);
        
        $this->load->model('localisation/rma_status');
        $rma_status_info = $this->model_localisation_rma_status->getRmaStatus($status);
        if(!empty($rma_status_info)){
            $status_name = $rma_status_info['name'];
        } else {
            $status_name = '';
        }
        
        $data = array('status' => $status_name,
            'rma_id' => $vid,
        );

        $this->load->model('account/rma/mail');
        $this->model_account_rma_mail->mail($data, 'status_to_admin');
        $this->model_account_rma_mail->mail($data, 'status_to_customer');
    }

    public function updateRmaAuth($auth_no, $vid) {
        $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_order SET rma_auth_no = '" . $this->db->escape($auth_no) . "' WHERE id ='" . (int) $vid . "'");
    }

    public function insertMessageRma($msg, $writer, $vid, $file, $to) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "wk_rma_order_message SET rma_id = '" . $this->db->escape($vid) . "', writer = '" . $this->db->escape($writer) . "', msg_to = '" . (int) $to . "', message = '" . $this->db->escape(nl2br($msg)) . "', date = NOW(), attachment = '" . $this->db->escape($file) . "'");

        $result_data = array();
        $result_data['rma_id'] = $vid;
        $result_data['message'] = $msg;

        $seller_id = $this->db->query("SELECT wrp.customer_id FROM " . DB_PREFIX . "wk_rma_order wro LEFT JOIN " . DB_PREFIX . "wk_rma_product wrp ON (wro.id = wrp.rma_id) WHERE wro.id = '" . (int) $vid . "' LIMIT 1 ")->row;

        if (isset($seller_id['customer_id']) && $seller_id['customer_id'] != 0) {

            $seller_details = $this->db->query("SELECT cp2c.email,cp2c.firstname,cp2c.lastname FROM " . DB_PREFIX . "customerpartner_to_customer cp2c  WHERE cp2c.customer_id = '" . (int) $seller_id['customer_id'] . "' AND cp2c.is_partner = '1'")->row;

            if (isset($seller_details['email'])) {
                $result_data['seller_email'] = $seller_details['email'];
                $result_data['seller_name'] = $seller_details['firstname'] . ' ' . $seller_details['lastname'];
                $this->load->model('account/rma/mail');
                $this->model_account_rma_mail->mail($result_data, 'message_to_seller');
            }
        }

        $this->load->model('account/rma/mail');
        $this->model_account_rma_mail->mail($result_data, 'message_to_admin');
    }

    public function viewRmaMessage($vid, $start, $limit) {
        $sql = $this->db->query("SELECT * FROM " . DB_PREFIX . "wk_rma_order_message WHERE rma_id='" . (int) $vid . "' ORDER BY id DESC LIMIT " . $start . "," . $limit);
        return $sql->rows;
    }

    public function viewTotalRmaMessage($vid) {
        $sql = $this->db->query("SELECT * FROM " . DB_PREFIX . "wk_rma_order_message WHERE rma_id='" . (int) $vid . "' ORDER BY id");
        return count($sql->rows);
    }

    public function rmaprice($id) {
        $result = $this->db->query("SELECT total FROM " . DB_PREFIX . "order WHERE order_id='$id' ");
        return $result->row;
    }

    public function getOrderProducts($order_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int) $order_id . "'");
        foreach ($query->rows as $key => $value) {

            $result = $this->db->query("SELECT id FROM " . DB_PREFIX . "wk_rma_product WHERE order_product_id = '" . (int) $value['order_product_id'] . "' ")->row;
            if (isset($result['id']) && $result['id'])
                unset($query->rows[$key]);
        }
        return $query->rows;
    }

    public function getOrderOptions($order_id, $order_product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . (int) $order_product_id . "'   AND deleted='0'");

        return $query->rows;
    }

    public function getUploadByCode($code) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "upload WHERE code = '" . $this->db->escape($code) . "'");

        return $query->row;
    }

    public function getsellerId($product_id, $option_data = array()) {

        if ($this->config->get('wk_mpaddproduct_status')) {

            //chk product is of admin or seller
            $cp2p_chk = count($this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_product WHERE product_id = '" . ($product_id) . "' ")->rows);
            $pc_chk = count($this->db->query("SELECT * FROM " . DB_PREFIX . "price_comparison WHERE product_id = '" . ($product_id) . "' ")->rows);

            if ($cp2p_chk != $pc_chk) { //seller product
                if (isset($option_data) && !empty($option_data)) {

                    foreach ($option_data as $key => $opt_value) { //check assign seller entry
                        $customer_id = $this->db->query("SELECT om.customer_id FROM " . DB_PREFIX . "option_mapping om LEFT JOIN " . DB_PREFIX . "product_option_value pov ON (om.option_value_id = pov.option_value_id) WHERE pov.product_id= '" . (int) $product_id . "' AND pov.product_option_value_id = '" . (int) $opt_value['product_option_value_id'] . "' ")->row;

                        if (isset($customer_id['customer_id']) && $customer_id['customer_id']) {
                            $result = $customer_id['customer_id'];
                        } else {
                            //normal seller's product (other option not price comparison)
                            $custId = $this->db->query("SELECT cp2p.customer_id FROM " . DB_PREFIX . "product_description pd LEFT JOIN " . DB_PREFIX . "customerpartner_to_product cp2p ON(pd.product_id = cp2p.product_id) WHERE pd.product_id = '" . (int) $product_id . "' ORDER BY cp2p.id ASC LIMIT 1 ")->row;
                            if (!empty($custId['customer_id'])) {
                                $result = $custId['customer_id'];
                            } else {
                                $result = 0;
                            }
                        }
                    }
                } else {
                    //seller product
                    $custId = $this->db->query("SELECT cp2p.customer_id FROM " . DB_PREFIX . "product_description pd LEFT JOIN " . DB_PREFIX . "customerpartner_to_product cp2p ON(pd.product_id = cp2p.product_id) WHERE pd.product_id = '" . (int) $product_id . "' ORDER BY cp2p.id ASC LIMIT 1 ")->row;
                    if (!empty($custId['customer_id']))
                        $result = $custId['customer_id'];
                    else
                        $result = 0;
                }
            }else {
                //store product
                $result = 0;
            }
        } else {
            $cp2p_chk = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customerpartner_to_product WHERE product_id = '" . ($product_id) . "' ORDER BY id ASC LIMIT 1 ")->row;
            if (isset($cp2p_chk['customer_id']) && $cp2p_chk['customer_id']) {
                $result = $cp2p_chk['customer_id'];
            } else {
                $result = 0;
            }
        }

        return $result;
    }

    public function orderprodetails($order) {

        $results = $this->db->query("SELECT op.name,op.product_id,op.model,op.quantity,op.order_product_id FROM " . DB_PREFIX . "order_product op LEFT JOIN `" . DB_PREFIX . "order` o ON (o.order_id = op.order_id   AND o.deleted='0') WHERE o.order_id='" . $this->db->escape($order) . "' AND o.order_status_id > 0 ")->rows;

        foreach ($results as $key => $value) {
            $custId = $this->db->query("SELECT cp2p.customer_id FROM " . DB_PREFIX . "product_description pd LEFT JOIN " . DB_PREFIX . "customerpartner_to_product cp2p ON(pd.product_id = cp2p.product_id) WHERE pd.product_id = '" . (int) $value['product_id'] . "' ORDER BY cp2p.id ASC LIMIT 1 ")->row;

            if (!empty($custId['customer_id']))
                $results[$key]['customer_id'] = $custId['customer_id'];
            else
                $results[$key]['customer_id'] = 0;
        }

        return $results;
    }

    public function validateOrder($qty, $order_p_id) {

        $query = '';

        if ((int) $this->config->get('wk_rma_system_time'))
            $query = " AND o.date_added >= DATE_SUB(CURDATE(), INTERVAL '" . (int) $this->config->get('wk_rma_system_time') . "' DAY)"; // AND o.date_added <= CURDATE()


//AND o.order_id NOT IN (SELECT order_id FROM ".DB_PREFIX."wk_rma_order)
        $sql = $this->db->query("SELECT o.order_id FROM " . DB_PREFIX . "order_product op LEFT JOIN `" . DB_PREFIX . "order` o ON (o.order_id = op.order_id   AND o.deleted='0') WHERE order_product_id='" . (int) $order_p_id . "' AND quantity >= '" . (int) $qty . "' $query AND o.order_status_id > 0 ")->row;

        if ($sql) {
            return true;
        } else {
            return false;
        }
    }

    //for reason
    public function getCustomerReason() {
        $sql = $this->db->query("SELECT reason ,reason_id FROM " . DB_PREFIX . "wk_rma_reason WHERE language_id ='" . $this->config->get('config_language_id') . "' AND status = 1 ORDER BY id ");
        return $sql->rows;
    }

    public function getSellerReason($product_id, $seller_id) {

        // $customer_id = 0;
        // if($this->config->get('wk_mpaddproduct_status')){
        // 	//chk product is of admin or seller
        // 	$cp2p_chk = count($this->db->query("SELECT * FROM ".DB_PREFIX."customerpartner_to_product WHERE product_id = '".($product_id)."' ")->rows);
        // 	$pc_chk = count($this->db->query("SELECT * FROM ".DB_PREFIX."price_comparison WHERE product_id = '".($product_id)."' ")->rows);
        // 	if($cp2p_chk != $pc_chk){
        // 		//seller product
        // 		$chkSeller = $this->db->query("SELECT cp2p.customer_id FROM ".DB_PREFIX."customerpartner_to_product cp2p LEFT JOIN ".DB_PREFIX."customerpartner_to_customer cp2c ON (cp2p.customer_id = cp2c.customer_id) WHERE cp2p.product_id = '".(int)$product_id."' ORDER BY cp2p.id ASC LIMIT 1 ")->row;
        // 		if(isset($chkSeller) && $chkSeller)
        // 			$customer_id = $chkSeller['customer_id'];
        // 		else
        // 			$customer_id = 0;
        // 	}else{
        // 		//admin product
        // 		$customer_id = 0;
        // 	}
        // }else{
        // 	$cp2p_chk = $this->db->query("SELECT cp2p.customer_id FROM ".DB_PREFIX."customerpartner_to_product WHERE product_id = '".($product_id)."' ")->row;
        // 	if(isset($cp2p_chk['customer_id']) && $cp2p_chk['customer_id']){
        // 		$customer_id = $cp2p_chk['customer_id'];
        // 	}else{
        // 		$customer_id = 0;
        // 	}
        // }


        $sql = $this->db->query("SELECT reason ,reason_id as id FROM " . DB_PREFIX . "wk_rma_reason WHERE customer_id = '" . (int) $seller_id . "' AND language_id ='" . $this->config->get('config_language_id') . "' AND status = 1 ORDER BY id ")->rows;


        return $sql;
    }

    //check guest login
    public function getGuestStatus($data) {
        $sql = $this->db->query("SELECT customer_id FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int) $data['orderinfo'] . "' AND  email = '" . $this->db->escape($data['email']) . "'   AND deleted='0'");
        return $sql->row;
    }

    public function getReason($reason_id) {
        return $this->db->query("SELECT reason FROM wk_rma_reason WHERE reason_id='" . (int) $reason_id . "' AND language_id='1'")->row['reason'];
    }

    public function getShippingAddressId($order_id) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer cp2c ON o.customer_id=cp2c.customer_id WHERE order_id = '" . $order_id . "'   AND o.deleted='0' ";

        $result = $this->db->query($sql)->row;
        return $result;
    }

}
