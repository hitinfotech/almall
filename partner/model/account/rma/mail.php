<?php
class ModelAccountRMAMail extends Model {

	private $data;

	private $generate_customer = 'generate_customer';
	private $generate_admin = 'generate_admin';
	private $generate_seller = 'generate_seller';
	private $message_to_admin = 'message_to_admin';
	private $message_to_seller = 'message_to_seller';
	private $status_to_admin = 'status_to_admin';
	private $status_to_customer = 'status_to_customer';

	public function getCustomer($id,$rma){
		$customer = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '".(int)$id."'")->row;
		if(!$customer){
			$customer = $this->db->query("SELECT o.* FROM `" . DB_PREFIX . "order` o LEFT JOIN ".DB_PREFIX."wk_rma_order wro ON (wro.order_id = o.order_id) WHERE wro.id = '".(int)$rma."' AND o.deleted='0' ")->row;
		}
		return $customer;
	}

	public function getseller($customer_id){
		$seller_email = $this->db->query("SELECT c.email,c.firstname,c.lastname FROM ".DB_PREFIX."customerpartner_to_customer cp2c LEFT JOIN ".DB_PREFIX."customer c ON (cp2c.customer_id = c.customer_id) WHERE cp2c.customer_id = '".(int)$customer_id."' AND cp2c.is_partner = '1' ")->row;
		return $seller_email;

	}

	public function mail($data, $mail_type = ''){

		$value_index = array();

		$this->load->language('account/rma/mail');

		$mail_message = '';

		switch($mail_type){

			//customer added RMA
			case $this->generate_customer ://to customer

				$mail_subject = $this->language->get($this->generate_customer .'_subject');
				$mail_message = nl2br($this->language->get($this->generate_customer.'_message'));

				$customer_info = $this->getCustomer($this->customer->getId(),$data['rma_id']);
				$mail_to = $customer_info['email'];
				$mail_from = $this->config->get('config_email');

				$value_index = array(
									'customer_name' => $customer_info['firstname'].' '.$customer_info['lastname'],
									'order_id' => $data['order'],
									'rma_id' => $data['rma_id'],
									'link' => $this->url->link('account/rma/viewrma&vid='.$data['rma_id'],'','SSL')
									);
				break;

			case $this->generate_admin ://to admin

				$mail_subject = $this->language->get($this->generate_admin .'_subject');
				$mail_message = nl2br($this->language->get($this->generate_admin.'_message'));

				$customer_info = $this->getCustomer($this->customer->getId(),$data['rma_id']);
				$mail_to = $this->config->get('config_email');
				$mail_from = $customer_info['email'];

				$value_index = array(
									'customer_name' => $customer_info['firstname'].' '.$customer_info['lastname'],
									'rma_id' => $data['rma_id'],
									'order_id' => $data['order'],
									);
				break;

			//customer send message to admin
			case $this->message_to_admin :

				$mail_subject = $this->language->get($this->message_to_admin .'_subject');
				$mail_message = nl2br($this->language->get($this->message_to_admin.'_message'));

				$customer_info = $this->getCustomer($this->customer->getId(),$data['rma_id']);
				$mail_to = $this->config->get('config_email');
				$mail_from = $customer_info['email'];

				$value_index = array(
									'customer_name' => $customer_info['firstname'].' '.$customer_info['lastname'],
									'customer_message' => nl2br($data['message']),
									'rma_id' => $data['rma_id'],
									);
				break;

			//customer changed status - message to admin
			case $this->status_to_admin :

				$mail_subject = $this->language->get($this->status_to_admin .'_subject');
				$mail_message = nl2br(sprintf($this->language->get($this->status_to_admin.'_message'),ucfirst($data['status'])));

				$customer_info = $this->getCustomer($this->customer->getId(),$data['rma_id']);
				$mail_to = $this->config->get('config_email');
				$mail_from = $customer_info['email'];

				$value_index = array(
									'customer_name' => $customer_info['firstname'].' '.$customer_info['lastname'],
									'rma_id' => $data['rma_id'],
									);
				break;

			//customer changed status - message to customer
			case $this->status_to_customer :

				$mail_subject = $this->language->get($this->status_to_customer .'_subject');
				$mail_message = nl2br(sprintf($this->language->get($this->status_to_customer.'_message'),ucfirst($data['status'])));

				$customer_info = $this->getCustomer($this->customer->getId(),$data['rma_id']);
				$mail_to = $customer_info['email'];
				$mail_from = $this->config->get('config_email');

				$value_index = array(
									'customer_name' => $customer_info['firstname'].' '.$customer_info['lastname'],
									'rma_id' => $data['rma_id'],
									);
				break;

				//seller rma - message to seller
			case $this->generate_seller :

				$mail_subject = $this->language->get($this->generate_seller .'_subject');
				$mail_message = nl2br($this->language->get($this->generate_seller.'_message'));

				$customer_info = $this->getCustomer($this->customer->getId(),$data['rma_id']);
				$seller_info = $this->getseller($data['seller_id']);
				$mail_to = $seller_info['email'];
				$mail_from = $this->config->get('config_email');

				$value_index = array(
									'seller_name'	=> $seller_info['firstname'].' '.$seller_info['lastname'],
									'customer_name' => $customer_info['firstname'].' '.$customer_info['lastname'],
									'rma_id' => $data['rma_id'],
									'order_id' => $data['order'],
									);
				break;

				//message from customer to seller
			case $this->message_to_seller :

				$customer_info = $this->getCustomer($this->customer->getId(),$data['rma_id']);
				$mail_to = $data['seller_email'];
				$mail_from = $this->config->get('config_email');

				$mail_subject = $this->language->get($this->message_to_seller .'_subject');
				$mail_message = nl2br($this->language->get($this->message_to_seller.'_message'));

				$value_index = array(
									'seller_name'	=> $data['seller_name'],
									'customer_name' => $customer_info['firstname'].' '.$customer_info['lastname'],
									'rma_id' => $data['rma_id'],
									'customer_message' => nl2br($data['message']),
									);
				break;

			default :
				return;
		}


		if($mail_message){

			$this->data['store_name'] = $this->config->get('config_name');
			$this->data['store_url'] = HTTP_SERVER;
			$this->data['logo'] = HTTP_SERVER.'image/' . $this->config->get('config_logo');

			$find = array(
				'{order_id}',
				'{rma_id}',
				'{seller_name}',
				'{product_name}',
				'{customer_name}',
				'{customer_message}',
				'{link}',
				'{config_logo}',
				'{config_icon}',
				'{config_currency}',
				'{config_image}',
				'{config_name}',
				'{config_owner}',
				'{config_address}',
				'{config_geocode}',
				'{config_email}',
				'{config_telephone}',
				);

			$replace = array(
				'order_id' => '',
				'rma_id' => '',
				'seller_name' => '',
				'product_name' => '',
				'customer_name' => '',
				'customer_message' => '',
				'link' => '',
				'config_logo' => '<a href="'.HTTP_SERVER.'" title="'.$this->data['store_name'].'"><img src="'.HTTP_SERVER.'image/' . $this->config->get('config_logo').'" alt="'.$this->data['store_name'].'" style="max-width:200px;"/></a>',
				'config_icon' => '<img src="'.HTTP_SERVER.'image/' . $this->config->get('config_icon').'" style="max-width:200px;">',
				'config_currency' => $this->config->get('config_currency'),
				'config_image' => '<img src="'.HTTP_SERVER.'image/' . $this->config->get('config_image').'" style="max-width:200px;">',
				'config_name' => $this->config->get('config_name'),
				'config_owner' => $this->config->get('config_owner'),
				'config_address' => $this->config->get('config_address'),
				'config_geocode' => $this->config->get('config_geocode'),
				'config_email' => $this->config->get('config_email'),
				'config_telephone' => $this->config->get('config_telephone'),
			);

			$replace = array_merge($replace,$value_index);

			$mail_message = trim(str_replace($find, $replace, $mail_message));

			$this->data['subject'] = $mail_subject;
			$this->data['message'] = $mail_message;

			$html = $this->load->view('default/template/account/rma/mail.tpl', $this->data);

			$mail_sender = $this->config->get('config_name') ? $this->config->get('config_name') : HTTP_SERVER;

			if (preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $mail_to) AND preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $mail_from) ) {

				if(VERSION == '2.0.0.0' || VERSION == '2.0.1.0' || VERSION == '2.0.1.1'  ) {
					/*Old mail code*/
					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($mail_to);
					$mail->setFrom($mail_from);
					$mail->setSender($mail_sender);
					$mail->setSubject($mail_subject);
					$mail->setHtml($html);
					$mail->setText(strip_tags($html));
					$mail->send();
				} else {
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
					$mail->setTo($mail_to);
					$mail->setFrom($mail_from);
					$mail->setSender($mail_sender);
					$mail->setSubject($mail_subject);
					$mail->setHtml($html);
					$mail->setText(strip_tags($html));
					$mail->send();
				}
			}

		}
	}

}
?>
