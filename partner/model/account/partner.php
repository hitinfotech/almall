<?php

class ModelAccountPartner extends Model {

    public function getPartner($Partner_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer WHERE customer_id = '" . (int) $Partner_id . "'");

        return $query->row;
    }

    public function getPartnerByEmail($email) {

        if (empty($email)) {
            return array();
        } else {

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
            return $query->row;
        }
    }
      public function getTotalPartnersByEmail($email) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customerpartner_to_customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        return $query->row['total'];
    }

    public function getPartnerByToken($token) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

        $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_customer SET token = ''");

        return $query->row;
    }

    public function editPassword($email, $password) {
        $this->event->trigger('pre.customer.edit.password');

        $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        $this->event->trigger('post.customer.edit.password');
    }

    public function editEmails($partner_id, $email,$fbs_email) {
        $this->event->trigger('pre.customer.edit.email');

        $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_customer SET email = '" . $this->db->escape($email) . "', fbs_email='" . $this->db->escape($fbs_email) . "' WHERE customer_id = " . (int)$partner_id);

        $this->event->trigger('post.customer.edit.email');
    }

    public function currentPasswordChecker($partner_id, $current_password) {
        $query_customer = $this->db->query("SELECT salt FROM " . DB_PREFIX . "customerpartner_to_customer WHERE customer_id = '" . (int) $partner_id . "'");

        if ($query_customer->num_rows) {
            if (isset($query_customer->row['salt']) && !empty($query_customer->row['salt'])) {

                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer WHERE password = '" . $this->db->escape(sha1($query_customer->row['salt'] . sha1($query_customer->row['salt'] . sha1($current_password)))) . "'");
                if (!$query->num_rows) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    public function addActivity($key, $data) {
        if (isset($data['customer_id']) && $data['customer_id'] > 0) {
            $customer_id = (int) $data['customer_id'];
        } else {
            $customer_id = 'NULL';
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "customer_activity` SET `customer_id` = " . $customer_id . ", `key` = '" . $this->db->escape($key) . "', `data` = '" . $this->db->escape(json_encode($data)) . "', `ip` = '" . $this->db->escape($this->partner->getipaddress()) . "', `date_added` = NOW()");
    }


}
