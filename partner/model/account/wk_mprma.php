<?php

class ModelAccountWkmprma extends Model {

    public function viewProducts($id) {

        $sql = "SELECT pd.name,wrp.quantity,wrr.reason,wrp.order_product_id,wro.order_id,wrp.product_id FROM " . DB_PREFIX . "product_description pd LEFT JOIN " . DB_PREFIX . "wk_rma_product wrp ON (wrp.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) LEFT JOIN " . DB_PREFIX . "wk_rma_order wro ON (wrp.rma_id=wro.id) WHERE wrp.rma_id = '" . (int) $id . "' AND wrp.customer_id = '" . (int) $this->partner->getId() . "' AND pd.language_id = '" . $this->config->get('config_language_id') . "' AND wrr.language_id = '" . $this->config->get('config_language_id') . "'";
        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function viewtotal($data = array()) {
        $implodeInner = '';

        if (isset($data['filter_customer_status_id']) && !is_null($data['filter_customer_status_id'])) {
            $implodeInner = " AND wrs.id = '" . (int) $data['filter_customer_status_id'] . "'";
        }

        $sql = "SELECT CONCAT(c.firstname,' ', c.lastname) AS name,wro.id,wro.order_id,wro.add_info,wro.rma_auth_no,wro.date,wro.customer_status_id as customer_st, rs.name rma_status, rs.color FROM " . DB_PREFIX . "wk_rma_order wro LEFT JOIN " . DB_PREFIX . "wk_rma_customer wrc ON (wro.id = wrc.rma_id) LEFT JOIN `" . DB_PREFIX . "order` c ON ((wrc.customer_id = c.customer_id || wrc.email = c.email ) AND c.order_id = wro.order_id   AND c.deleted='0') LEFT JOIN rma_status rs ON(rs.rma_status_id=wro.customer_status_id AND rs.language_id='". (int)$this->config->get('config_language_id') ."') WHERE wro.customer_id = '" . (int) $this->partner->getId() . "' AND is_confirmed='1'";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(CONCAT(c.firstname, c.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (isset($data['filter_order']) && !is_null($data['filter_order'])) {
            $implode[] = "wro.order_id = '" . (int) $data['filter_order'] . "'";
        }

        if (isset($data['filter_rma_status']) && !is_null($data['filter_rma_status'])) {
            $implode[] = "wro.customer_status_id = '" . (int) $data['filter_rma_status'] . "'";
        }

        if (!empty($data['filter_date'])) {
            $implode[] = "LCASE(wro.date) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'c.firstname',
            'c.order_id',
            'wro.id',
            'wro.date',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY c.firstname";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $result = $this->db->query($sql);

        if (!empty($data['dashboard_rma']) && ($data['dashboard_rma'] == 1)) {
            return count($result->rows);
        }
        return $result->rows;
    }

    public function viewtotalentry($data = array()) {

        $implodeInner = '';

        if (isset($data['filter_customer_status_id']) && !is_null($data['filter_customer_status_id'])) {
            $implodeInner = " AND wrs.id = '" . (int) $data['filter_customer_status_id'] . "'";
        }

		$sql = "SELECT CONCAT(c.firstname,' ', c.lastname) AS name,wro.id,wro.order_id,wro.add_info,wro.rma_auth_no,wro.date,wro.customer_status as customer_st FROM " . DB_PREFIX . "wk_rma_order wro LEFT JOIN " . DB_PREFIX . "wk_rma_customer wrc ON (wro.id = wrc.rma_id) LEFT JOIN `" . DB_PREFIX . "order` c ON ((wrc.customer_id = c.customer_id || wrc.email = c.email ) AND c.order_id = wro.order_id   AND c.deleted='0') WHERE wro.customer_id = '".(int)$this->partner->getId()."' AND is_confirmed='1' ";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(CONCAT(c.firstname, c.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (isset($data['filter_order']) && !is_null($data['filter_order'])) {
            $implode[] = "wro.order_id = '" . (int) $data['filter_order'] . "'";
        }

        if (isset($data['filter_rma_status']) && !is_null($data['filter_rma_status'])) {
            $implode[] = "wro.customer_status_id = '" . (int) $data['filter_rma_status'] . "'";
        }

        if (!empty($data['filter_date'])) {
            $implode[] = "LCASE(wro.date) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'c.firstname',
            'c.order_id',
            'wro.id',
            'wro.date',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY c.firstname";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        $result = $this->db->query($sql);

        return count($result->rows);
    }

    public function deleteentry($id) {
// echo "<pre>";
// print_r($id);
// echo "</pre>";
// die();
        $imagefolder = $this->db->query("SELECT images FROM " . DB_PREFIX . "wk_rma_order WHERE id = '" . (int) $id . "'")->row;

        $this->db->query("DELETE FROM " . DB_PREFIX . "wk_rma_order WHERE id = '" . (int) $id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "wk_rma_order_message WHERE rma_id = '" . (int) $id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "wk_rma_customer WHERE rma_id = '" . (int) $id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "wk_rma_product WHERE rma_id = '" . (int) $id . "'");

        if ($imagefolder) {
            $dirPath = DIR_IMAGE . 'rma/' . $imagefolder['images'];
            if (file_exists($dirPath)) {
                $this->deleteDir($dirPath);
            }
        }
    }

    public function deleteDir($dirPath) {
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        if (file_exists($dirPath))
            rmdir($dirPath);
    }

    //mineFunction

    public function getProductRma($id, $order_id) {

        $sql = $this->db->query("SELECT op.product_id,op.name,op.model,op.quantity as ordered,op.tax tax,op.price,op.order_id,wrr.reason,wrp.quantity as returned,wrp.order_product_id FROM " . DB_PREFIX . "wk_rma_product wrp LEFT JOIN " . DB_PREFIX . "order_product op ON (wrp.product_id = op.product_id AND wrp.order_product_id = op.order_product_id) LEFT JOIN " . DB_PREFIX . "wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) WHERE wrp.customer_id = '" . (int) $this->partner->getId() . "' AND op.order_id = '" . (int) $order_id . "' AND wrp.rma_id='" . (int) $id . "' AND wrr.language_id= '" . $this->config->get('config_language_id') . "'")->rows;

        if (!$sql) {
            $sql = $this->db->query("SELECT p.product_id,pd.name,p.model,p.price,wrr.reason,wrp.quantity as returned,wrp.order_product_id FROM " . DB_PREFIX . "wk_rma_product wrp LEFT JOIN " . DB_PREFIX . "product p ON (wrp.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) WHERE wrp.customer_id = '" . (int) $this->partner->getId() . "' AND wrp.rma_id='" . (int) $id . "' AND pd.language_id = '" . $this->config->get('config_language_id') . "' AND wrr.language_id= '" . $this->config->get('config_language_id') . "'")->rows;

            if ($sql)
                foreach ($sql as $key => $value) {
                    $sql[$key]['ordered'] = '0';
                    $sql[$key]['tax'] = '0';
                }
        }

        return $sql;
    }

    public function viewtotalMessageBy($data) {

        $sql = "SELECT * FROM " . DB_PREFIX . "wk_rma_order_message wrm WHERE wrm.rma_id = '" . (int) $data['filter_id'] . "'";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(wrm.writer) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (isset($data['filter_message']) && !empty($data['filter_message'])) {
            $implode[] = "LCASE(wrm.message) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_message'])) . "%'";
        }

        if (isset($data['filter_date']) && !empty($data['filter_date'])) {
            $implode[] = "LCASE(wrm.date) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'wrm.writer',
            'wrm.message',
            'wrm.date',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY wrm.id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function viewtotalNoMessageBy($data) {

        $sql = "SELECT * FROM " . DB_PREFIX . "wk_rma_order_message wrm WHERE wrm.rma_id = '" . (int) $data['filter_id'] . "'";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(wrm.writer) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (isset($data['filter_message']) && !empty($data['filter_message'])) {
            $implode[] = "LCASE(wrm.message) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_message'])) . "%'";
        }

        if (isset($data['filter_date']) && !empty($data['filter_date'])) {
            $implode[] = "LCASE(wrm.date) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $result = $this->db->query($sql);

        return count($result->rows);
    }

    public function updateAdminStatus($msg, $status, $vid, $file_name, $customer_id) {

        $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_order SET customer_status_id='" . (int) $status . "' WHERE id='" . (int) $vid . "' ");

        $this->load->model('localisation/rmaquantity');
        $this->model_localisation_rmaquantity->ChangeProductOrderQuantity($vid);

        $order_id = $this->db->query("SELECT order_id FROM  " . DB_PREFIX . "wk_rma_order WHERE id='" . (int) $vid . "' ")->row['order_id'];

        if ($msg != '') {
            $this->db->query("INSERT INTO " . DB_PREFIX . "wk_rma_order_message SET rma_id = '" . $this->db->escape($vid) . "', writer = 'seller', msg_to = '" . (int) $customer_id . "', message = '" . $this->db->escape(nl2br($msg)) . "', date = NOW(), attachment = '" . $this->db->escape($file_name) . "'");
        }

        if ($status == 6 || $status == 4) {
            $file1 = 'rma_accept_seller';
            $file2 = 'rma_accept_cs';
            $subject = 'Return Accepted';
            if ($status == 4) {
                $file1 = 'rma_reject_seller';
                $file2 = 'rma_reject_cs';
                $subject = 'Return Rejected';
            }
            $data = array(
                'file1' => $file1,
                'file2' => $file2,
                'rma_id' => $vid,
                'message' => $msg,
                'subject' => $subject,
            );
            $cust = $this->db->query("SELECT email,firstname,lastname FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int) $customer_id . "' ")->row;
            if (!$cust) {
                $cust = $this->db->query("SELECT o.email,o.firstname,o.lastname FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "wk_rma_order wro ON (wro.order_id = o.order_id) WHERE wro.id = '" . (int) $vid . "'")->row;
            }
            $seller = $this->db->query("SELECT cp2c.firstname, cp2c.lastname,cp2c.email FROM " . DB_PREFIX . "customerpartner_to_customer cp2c LEFT JOIN " . DB_PREFIX . "customer c ON (cp2c.customer_id = c.customer_id) WHERE cp2c.customer_id = '" . (int) $this->partner->getId() . "' ")->row;
            $data['seller'] = $seller;
            $data['seller_email'] = $seller['email'];
            $data['email'] = $cust['email'];
            $data['rma_link'] = str_replace("/partner/", '/admin/', $this->url->link('catalog/wk_rma_admin/getForm', '&id=' . $vid, 'SSL'));
            $data['rma_seller_link'] = $this->url->link('wk_mprma_manage/getForm', '&return_id=' . $vid, 'SSL');
            $data['order_id'] = $order_id;
            $this->sendMail($data);
            $this->sendBookRequest($vid);
        }
    }

    public function addLabel($id, $file, $folder = '', $sellername) {

        $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_order SET `shipping_label` = '" . $this->db->escape($file) . "' WHERE id = '" . (int) $id . "'");

        $data = array('rma_id' => $id,
            'link' => HTTPS_SERVER . 'index.php?route=account/rma/viewrma/printlable&vid=' . $id,
            'label' => 'rma/' . $sellername . '/' . $file
        );

        // $this->load->model('catalog/rma_mail');
        // $this->model_catalog_rma_mail->mail($data,'label_to_customer');
    }

    public function viewCustomerDetails($order_id) {
        $result = $this->db->query("SELECT o.firstname,o.lastname,o.customer_id AS id FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '$order_id' ");
        return $result->row;
    }

    public function getRmaOrderid($id) {
        $result = $this->db->query("SELECT wro.*,wro.customer_status_id as customer_st, rs.name rma_status, rs.color FROM " . DB_PREFIX . "wk_rma_order wro LEFT JOIN " . DB_PREFIX . "wk_rma_customer wrc ON (wrc.rma_id = wro.id) LEFT JOIN rma_status rs ON(rs.rma_status_id=wro.customer_status_id AND language_id='".(int)$this->config->get('config_language_id')."') WHERE wro.id ='" . (int) $id . "' ");

        return $result->row;
    }

    //for reason list
    public function viewreason($data) {

        $sellerId = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customerpartner_to_customer WHERE customer_id = '" . (int) $this->partner->getId() . "' AND is_partner = '1'")->row;

        if (isset($sellerId) && $sellerId)
            $sql = "SELECT * FROM " . DB_PREFIX . "wk_rma_reason wrr WHERE language_id ='" . $this->config->get('config_language_id') . "' AND customer_id = '" . (int) $sellerId['customer_id'] . "' ";

        $implode = array();

        if (!empty($data['filter_reason'])) {
            $implode[] = "LCASE(wrr.reason) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_reason'])) . "%'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "wrr.status = '" . (int) $data['filter_status'] . "'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'wrr.reason',
            'wrr.status',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $result = $this->db->query($sql);

        return $result->rows;
    }

    public function viewreasonbyId($id) {
        $sql = $this->db->query("SELECT * FROM " . DB_PREFIX . "wk_rma_reason WHERE reason_id='" . (int) $id . "' AND customer_id = '" . (int) $this->partner->getId() . "' ");
        return $sql->rows;
    }

    public function viewtotalreason($data) {

        $sql = "SELECT * FROM " . DB_PREFIX . "wk_rma_reason wrr WHERE customer_id = '" . (int) $this->partner->getId() . "' AND language_id ='" . $this->config->get('config_language_id') . "' ";

        $implode = array();

        if (!empty($data['filter_reason'])) {
            $implode[] = "LCASE(wrr.reason) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_reason'])) . "%'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $implode[] = "wrr.status = '" . (int) $data['filter_status'] . "'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $result = $this->db->query($sql);
        return count($result->rows);
    }

    // reason function
    public function addReason($data) {

        $reason_id = 1;
        $last_reason_id = $this->db->query("SELECT reason_id FROM " . DB_PREFIX . "wk_rma_reason ORDER BY reason_id DESC LIMIT 1")->row;
        if (isset($last_reason_id['reason_id']))
            $reason_id = $last_reason_id['reason_id'] + 1;
        foreach ($data['reason'] as $key => $value)
            $this->db->query("INSERT INTO " . DB_PREFIX . "wk_rma_reason SET reason_id = '" . (int) $reason_id . "', customer_id = '" . (int) $this->partner->getId() . "',`reason` = '" . $this->db->escape($value) . "',`language_id` ='" . (int) $key . "', `status` = '" . (int) $data['status'] . "', status_info = '" . $data['status_info'] . "' ");
    }

    public function UpdateReason($data) {

        $reason_id = $data['id'];
        foreach ($data['reason'] as $key => $value)
            $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_reason SET `reason` = '" . $this->db->escape($value) . "',`status` = '" . (int) $data['status'] . "' WHERE reason_id = '" . (int) $reason_id . "' AND customer_id = '" . (int) $this->partner->getId() . "' AND `language_id` ='" . (int) $key . "'");
    }

    public function deleteReason($id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "wk_rma_reason WHERE reason_id = '" . (int) $id . "' AND customer_id = '" . (int) $this->partner->getId() . "'");
    }

    public function getCustomerReason() {
        $sql = $this->db->query("SELECT reason ,id FROM " . DB_PREFIX . "wk_rma_reason WHERE language_id ='" . $this->config->get('config_language_id') . "' AND status = 1 ORDER BY id ");
        return $sql->rows;
    }

    public function getOrderProducts($order_id, $id) {

        $sql = $this->db->query("SELECT op.product_id,op.name,op.model,op.quantity as ordered,op.tax tax,op.price,wrp.quantity as returned,op.order_product_id,wrr.reason FROM " . DB_PREFIX . "wk_rma_product wrp LEFT JOIN " . DB_PREFIX . "order_product op ON (wrp.product_id = op.product_id AND wrp.order_product_id = op.order_product_id) LEFT JOIN " . DB_PREFIX . "wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) WHERE wrp.customer_id = '" . (int) $this->partner->getId() . "' AND op.order_id = '" . (int) $order_id . "' AND wrp.rma_id='" . (int) $id . "' AND wrr.language_id = '" . $this->config->get('config_language_id') . "'")->rows;

        if (!$sql) {
            $sql = $this->db->query("SELECT p.product_id,pd.name,p.model,p.price,wrp.quantity as returned,wrr.reason FROM " . DB_PREFIX . "wk_rma_product wrp LEFT JOIN " . DB_PREFIX . "product p ON (wrp.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "wk_rma_reason wrr ON (wrp.reason = wrr.reason_id) WHERE wrp.rma_id='" . (int) $id . "' AND pd.language_id = '" . $this->config->get('config_language_id') . "' AND wrr.language_id = '" . $this->config->get('config_language_id') . "'")->rows;

            if ($sql)
                foreach ($sql as $key => $value) {
                    $sql[$key]['ordered'] = '0';
                    $sql[$key]['tax'] = '0';
                    $sql[$key]['order_product_id'] = 0;
                }
        }

        return $sql;
    }

    public function returnQty($rma_id) {

        $sql = $this->db->query("SELECT order_id FROM " . DB_PREFIX . "wk_rma_order WHERE id ='" . (int) $rma_id . "'")->row;

        $order_product_query = false;

        if ($sql) {
            $order_product_query = $this->getOrderProducts($sql['order_id'], $rma_id);
        }

        if ($order_product_query) {

            //load opencart order model and get order info
            $this->load->model('checkout/order');
            $This_order = $this->model_checkout_order->getOrder($sql['order_id']);

            //make shipping ,payment, store array for get tax
            $shipping_address = array(
                'country_id' => $This_order['shipping_country_id'],
                'zone_id' => $This_order['shipping_zone_id']
            );

            $payment_address = array(
                'country_id' => $This_order['payment_country_id'],
                'zone_id' => $This_order['payment_zone_id']
            );

            $store_address = array(
                'country_id' => $this->config->get('config_country_id'),
                'zone_id' => $this->config->get('config_zone_id')
            );

            foreach ($order_product_query as $order_product) {

                if ($order_product['returned'] <= $order_product['ordered']) {

                    // $product = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE product_id = '".(int)$order_product['product_id']."' ")->row;
                    // if(isset($product['subtract']) && $product['subtract']){
                    // 	$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity + " . (int)$order_product['returned'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");
                    // }

                    $order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int) $sql['order_id'] . "' AND order_product_id = '" . (int) $order_product['order_product_id'] . "'")->rows;

                    if (!empty($order_option_query)) {
                        foreach ($order_option_query as $option) {
                            if ($this->config->get('wk_mpaddproduct_status')) {

                                $customer_id = $this->db->query("SELECT om.customer_id FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_mapping om ON (pov.option_value_id = om.option_value_id) WHERE pov.deleted != '1' AND pov.product_option_value_id = '" . (int) $option['product_option_value_id'] . "' ")->row;

                                if (isset($customer_id['customer_id']) && $customer_id['customer_id']) {

                                    $product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int) $order_product['product_id'] . "' ")->row;

                                    if (isset($product['subtract']) && $product['subtract']) {
                                        $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity + " . (int) $order_product['returned'] . ") WHERE product_id = '" . (int) $order_product['product_id'] . "' AND subtract = '1'");

                                        $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_product SET quantity = (quantity + " . (int) $order_product['returned'] . ") WHERE product_id = '" . (int) $order_product['product_id'] . "' AND customer_id = '" . (int) $customer_id['customer_id'] . "' ");

                                        $this->db->query("UPDATE " . DB_PREFIX . "price_comparison SET quantity = (quantity + " . (int) $order_product['returned'] . ") WHERE product_id = '" . (int) $order_product['product_id'] . "' AND customer_id = '" . (int) $customer_id['customer_id'] . "' ");
                                    }

                                    $chk_subtract = $this->db->query("SELECT subtract FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . (int) $option['product_option_value_id'] . "' ")->row;

                                    if (isset($chk_subtract['subtract']) && $chk_subtract['subtract']) {
                                        $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int) $order_product['returned'] . ") WHERE product_option_value_id = '" . (int) $option['product_option_value_id'] . "' AND subtract = '1'");
                                    }
                                } else { // store product
                                    $product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int) $order_product['product_id'] . "' ")->row;

                                    if (isset($product['subtract']) && $product['subtract']) {
                                        $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity + " . (int) $order_product['returned'] . ") WHERE product_id = '" . (int) $order_product['product_id'] . "' AND subtract = '1'");
                                    }

                                    $chk_subtract = $this->db->query("SELECT subtract FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . (int) $option['product_option_value_id'] . "' ")->row;

                                    if (isset($chk_subtract['subtract']) && $chk_subtract['subtract']) {
                                        $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int) $order_product['returned'] . ") WHERE product_option_value_id = '" . (int) $option['product_option_value_id'] . "' AND subtract = '1'");
                                    }
                                }
                            } else {
                                $product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int) $order_product['product_id'] . "' ")->row;

                                if (isset($product['subtract']) && $product['subtract']) {
                                    $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity + " . (int) $order_product['returned'] . ") WHERE product_id = '" . (int) $order_product['product_id'] . "' AND subtract = '1'");
                                }

                                $chk_subtract = $this->db->query("SELECT subtract FROM " . DB_PREFIX . "product_option_value WHERE product_option_value_id = '" . (int) $option['product_option_value_id'] . "' ")->row;

                                if (isset($chk_subtract['subtract']) && $chk_subtract['subtract']) {
                                    $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int) $order_product['returned'] . ") WHERE product_option_value_id = '" . (int) $option['product_option_value_id'] . "' AND subtract = '1'");
                                }
                            }
                        }
                    } else { // not option on product
                        $product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int) $order_product['product_id'] . "' ")->row;

                        if (isset($product['subtract']) && $product['subtract']) {
                            $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity + " . (int) $order_product['returned'] . ") WHERE product_id = '" . (int) $order_product['product_id'] . "' AND subtract = '1'");
                        }
                    }

                    $this->db->query("UPDATE " . DB_PREFIX . "order_product SET quantity = (quantity - " . (int) $order_product['returned'] . "), `total` = (`total`-(`total`*" . (int) $order_product['returned'] . ")/" . (int) $order_product['ordered'] . "), reward = (`reward`-(`reward`*" . (int) $order_product['returned'] . ")/" . (int) $order_product['ordered'] . ") WHERE order_product_id = '" . (int) $order_product['order_product_id'] . "'");
                } else { //remove product from order
                    // $this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_product_id = '" . (int)$order_product['order_product_id'] . "'");
                    // $this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_product_id = '" . (int)$order_product['order_product_id'] . "'");
                }
            }


            //get tax from function
            $tax_class_id = $this->db->query("SELECT tax_class_id FROM " . DB_PREFIX . "product WHERE product_id = '" . $order_product['product_id'] . "'")->row;

            $tax_per_product = 0;
            $ItemBasePrice = $this->currency->convert($order_product['price'], $this->config->get('config_currency'), $This_order['currency_code']);
            if ($tax_class_id && isset($This_order['customer_group_id']))
                $tax_per_product = $this->getRates($ItemBasePrice, $tax_class_id['tax_class_id'], $This_order['customer_group_id'], $shipping_address, $payment_address, $store_address);

            if ($tax_per_product) {
                foreach ($tax_per_product as $key => $value) {
                    $this->db->query("UPDATE " . DB_PREFIX . "order_total SET value = (value - " . (float) $value['amount'] * $order_product['returned'] . ") WHERE title = '" . $this->db->escape($value['name']) . "' AND code = 'tax' AND order_id = '" . $sql['order_id'] . "'");
                }
            }

            // code for update total price of order
            $getProducts = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . $sql['order_id'] . "'")->rows;

            if ($getProducts) {
                $sub_total = $total = 0;
                foreach ($getProducts as $key => $value) {
                    $sub_total = $sub_total + $value['total'];
                }
                $getOrderTotal = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . $sql['order_id'] . "'")->rows;

                foreach ($getOrderTotal as $key => $value) {
                    if ($value['code'] != 'sub_total' AND $value['code'] != 'total')
                        $total = $total + $value['value'];
                }
                $total = $total + $sub_total;
                $subtotalWidCurrency = $this->currency->format($sub_total, $This_order['currency_code']);
                $totalWidCurrency = $this->currency->format($total, $This_order['currency_code']);

                $this->db->query("UPDATE " . DB_PREFIX . "order_total SET value = '" . (float) $total . "' WHERE order_id = '" . $sql['order_id'] . "' AND code = 'total'");
                $this->db->query("UPDATE " . DB_PREFIX . "order_total SET value = '" . (float) $sub_total . "' WHERE order_id = '" . $sql['order_id'] . "' AND code = 'sub_total'");
            }else { // delete order from table
                // $this->db->query("DELETE FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM " . DB_PREFIX . "order_fraud WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM `" . DB_PREFIX . "order_history` WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$sql['order_id'] . "'");
                // $this->db->query("DELETE FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$sql['order_id'] . "'");
            }

            $this->db->query("UPDATE " . DB_PREFIX . "wk_rma_order SET customer_status_id = '6' WHERE id = '" . (int) $rma_id . "'");

            $this->load->model('localisation/rmaquantity');
            $this->model_localisation_rmaquantity->ChangeProductOrderQuantity($rma_id);
            //$this->mailToAdminMessage(false,$rma_id);
        }
    }

    public function getRates($value, $tax_class_id, $customer_group_id, $shipping_address, $payment_address, $store_address) {
        $tax_rates = array();

        if (!$customer_group_id) {
            $customer_group_id = $this->config->get('config_customer_group_id');
        }

        if ($shipping_address) {
            $tax_query = $this->db->query("SELECT tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.tax_class_id = '" . (int) $tax_class_id . "' AND tr1.based = 'shipping' AND tr2cg.customer_group_id = '" . (int) $customer_group_id . "' AND z2gz.country_id = '" . (int) $shipping_address['country_id'] . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int) $shipping_address['zone_id'] . "') ORDER BY tr1.priority ASC");

            foreach ($tax_query->rows as $result) {
                $tax_rates[$result['tax_rate_id']] = array(
                    'tax_rate_id' => $result['tax_rate_id'],
                    'name' => $result['name'],
                    'rate' => $result['rate'],
                    'type' => $result['type'],
                    'priority' => $result['priority']
                );
            }
        }

        if ($payment_address) {
            $tax_query = $this->db->query("SELECT tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.tax_class_id = '" . (int) $tax_class_id . "' AND tr1.based = 'payment' AND tr2cg.customer_group_id = '" . (int) $customer_group_id . "' AND z2gz.country_id = '" . (int) $payment_address['country_id'] . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int) $payment_address['zone_id'] . "') ORDER BY tr1.priority ASC");

            foreach ($tax_query->rows as $result) {
                $tax_rates[$result['tax_rate_id']] = array(
                    'tax_rate_id' => $result['tax_rate_id'],
                    'name' => $result['name'],
                    'rate' => $result['rate'],
                    'type' => $result['type'],
                    'priority' => $result['priority']
                );
            }
        }

        if ($store_address) {
            $tax_query = $this->db->query("SELECT tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.tax_class_id = '" . (int) $tax_class_id . "' AND tr1.based = 'store' AND tr2cg.customer_group_id = '" . (int) $customer_group_id . "' AND z2gz.country_id = '" . (int) $store_address['country_id'] . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int) $store_address['zone_id'] . "') ORDER BY tr1.priority ASC");

            foreach ($tax_query->rows as $result) {
                $tax_rates[$result['tax_rate_id']] = array(
                    'tax_rate_id' => $result['tax_rate_id'],
                    'name' => $result['name'],
                    'rate' => $result['rate'],
                    'type' => $result['type'],
                    'priority' => $result['priority']
                );
            }
        }

        $tax_rate_data = array();

        foreach ($tax_rates as $tax_rate) {
            if (isset($tax_rate_data[$tax_rate['tax_rate_id']])) {
                $amount = $tax_rate_data[$tax_rate['tax_rate_id']]['amount'];
            } else {
                $amount = 0;
            }

            if ($tax_rate['type'] == 'F') {
                $amount += $tax_rate['rate'];
            } elseif ($tax_rate['type'] == 'P') {
                $amount += ($value / 100 * $tax_rate['rate']);
            }

            $tax_rate_data[$tax_rate['tax_rate_id']] = array(
                'tax_rate_id' => $tax_rate['tax_rate_id'],
                'name' => $tax_rate['name'],
                'rate' => $tax_rate['rate'],
                'type' => $tax_rate['type'],
                'amount' => $amount
            );
        }

        return $tax_rate_data;
    }

    public function check_if_product_related_to_seller($rma_id, $seller_id) {
        $res = $this->db->query("SELECT  product_id  FROM wk_rma_product  where rma_id='" . (int) $rma_id . "'");
        $product_id = 0;
        if (count($res->rows) > 0) {
            $product_id = $res->row['product_id'];
        }
        $query = $this->db->query("SELECT * FROM customerpartner_to_product cp2p where cp2p.customer_id='" . (int) $seller_id . "' and cp2p.product_id='" . (int) $product_id . "'");
        return count($query->rows);
    }

    // public function mailToAdminMessage($msg = false,$vid){
    // 	return;
    // 	// Send mail to admin
    // 	$this->load->model('localisation/language');
    // 	$language_info = $this->model_localisation_language->getLanguage($this->config->get('config_language_id'));
    // 	// Send mail to customer and admin
    // 	if($language_info){
    // 		$language = new Language($language_info['directory']);
    // 		$language->load($language_info['filename']);
    // 		$language->load('catalog/rma_mail');
    // 	}else{
    // 		$language = new Language('catalog/');
    // 		$language->load('catalog/rma_mail');
    // 	}
    // 	$template = new Template();
    // 	$template->data['text_hello'] = $language->get('text_hello');
    // 	$template->data['email'] = $language->get('email');
    // 	$template->data['text_thanksadmin'] = $language->get('text_thanksadmin');
    // 	$template->data['name'] = $language->get('name');
    // 	$template->data['message'] = $language->get('message');
    // 	$template->data['message_for'] = $language->get('message_for');
    // 	$template->data['message_heading'] = $language->get('message_heading');
    // 	$template->data['text_returnInfo'] = $language->get('text_returnInfo');
    // 	$template->data['customer_rma_id'] = $language->get('customer_rma_id');
    // 	$template->data['rma_return'] = $language->get('rma_return');
    // 	$template->data['id'] = $vid;
    // 	$template->data['rmalink'] = HTTP_CATALOG.'index.php?route=account/rma/rmalogin';
    // 	$template->data['store_name'] = $this->config->get('store_name');
    // 	$template->data['store_url'] = HTTP_CATALOG;
    // 	$template->data['logo'] = HTTP_CATALOG.'image/' . $this->config->get('config_logo');
    // 	$template->data['comment'] = nl2br($msg);
    // 	$template->data['customerDetails'] = $customerDetails = $this->db->query("SELECT o.* FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "wk_rma_order wro ON (o.order_id = wro.order_id) WHERE wro.id = '".(int)$vid."'")->row;
    // 	if($msg){
    // 		//mail to buyer after rma message by admin
    // 		$template->data['admin_message'] = true;
    // 	}elseif($customerDetails){
    // 		//mail to buyer after rma returned by admin
    // 		$template->data['customer_message'] = true;
    // 	}
    // 	if($customerDetails){
    // 		$template->data['text_hello'] = $template->data['text_hello'].$customerDetails['firstname'].',';
    // 		$html = $template->fetch('catalog/rma_mail.tpl');
    // 		$toCustomer = array();
    // 		$toCustomer['emailto'] = $customerDetails['email'];
    // 		$toCustomer['message'] = $html;
    // 		$toCustomer['mailfrom']= $this->config->get('config_email');
    // 		$toCustomer['subject'] = $template->data['message_heading'];
    // 		$toCustomer['name'] = $this->config->get('config_name');
    // 		$this->sendMail($toCustomer);
    // 	}
    // }
    // public function sendMail($data){
    // 	$text = $data['message'];
    // 	$mail = new Mail();
    // 	$mail->protocol = $this->config->get('config_mail_protocol');
    // 	$mail->parameter = $this->config->get('config_mail_parameter');
    // 	$mail->hostname = $this->config->get('config_smtp_host');
    // 	$mail->username = $this->config->get('config_smtp_username');
    // 	$mail->password = $this->config->get('config_smtp_password');
    // 	$mail->port = $this->config->get('config_smtp_port');
    // 	$mail->timeout = $this->config->get('config_smtp_timeout');
    // 	$mail->setTo($data['emailto']);
    // 	$mail->setFrom($data['mailfrom']);
    // 	$mail->setSender($data['name']);
    // 	$mail->setSubject(html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8'));
    // 	$mail->setHtml($data['message']);
    // 	$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
    // 	$mail->send();
    // }

    /*
      I commited this function due to the new task MDS-57
      public function sendMail($data){
      // $sellerEmail = $this->customer->getEmail();

      // Shipping Address
      $this->load->model('account/order');
      $this->load->model('account/customerpartnerorder');
      $this->load->model('shipping/wk_custom_shipping');

      $order_id = $data['order_id'];
      $order_info = $this->model_account_order->getOrder($order_id);

      $paid_shipping = false;
      $admin_shipping_method = false;

      if ($order_info['payment_address_format']) {
      $format = $order_info['payment_address_format'];
      } else {
      $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{zone}' . "\n" . '{country}';
      }

      $find = array(
      '{firstname}',
      '{lastname}',
      '{company}',
      '{address_1}',
      '{address_2}',
      '{city}',
      '{zone}',
      '{zone_code}',
      '{country}'
      );

      $replace = array(
      'firstname' => $order_info['payment_firstname'],
      'lastname' => $order_info['payment_lastname'],
      'company' => $order_info['payment_company'],
      'address_1' => $order_info['payment_address_1'],
      'address_2' => $order_info['payment_address_2'],
      'city' => $order_info['payment_city'],
      'zone' => $order_info['payment_zone'],
      'zone_code' => $order_info['payment_zone_code'],
      'country' => $order_info['payment_country']
      );

      $mail_data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

      if ($order_info['shipping_address_format']) {
      $format = $order_info['shipping_address_format'];
      } else {
      $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} ' . "\n" . '{zone}' . "\n" . '{country}';
      }

      $find = array(
      '{firstname}',
      '{lastname}',
      '{company}',
      '{address_1}',
      '{address_2}',
      '{city}',
      '{zone}',
      '{zone_code}',
      '{country}'
      );

      $replace = array(
      'firstname' => $order_info['shipping_firstname'],
      'lastname' => $order_info['shipping_lastname'],
      'company' => $order_info['shipping_company'],
      'address_1' => $order_info['shipping_address_1'],
      'address_2' => $order_info['shipping_address_2'],
      'city' => $order_info['shipping_city'],
      'zone' => $order_info['shipping_zone'],
      'zone_code' => $order_info['shipping_zone_code'],
      'country' => $order_info['shipping_country']
      );

      $mail_data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

      if(isset($order_info['payment_method']) && $order_info['payment_method']!=''){
      $mail_data['payment_method'] = $order_info['payment_method'];
      }else{
      $mail_data['payment_method'] = false;
      }

      $mail_data['date_added'] = $order_info['date_added'];
      $mail_data['payment_method'] = $order_info['payment_method'];
      $mail_data['return_date'] = date('M d, Y');
      $mail_data['rma_number'] = $data['rma_id'];
      $mail_data['order_id'] = $order_id;

      $order_product_query = $this->db->query("SELECT op.product_id,op.order_product_id,op.total,op.price, op.name, op.model, op.quantity,p.sku FROM " . DB_PREFIX . "order_product op LEFT JOIN product p ON (op.product_id = p.product_id) WHERE order_id = '" . (int) $order_id . "'");

      foreach ($order_product_query->rows as $product) {

      $option_data = array();

      $order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . (int) $product['order_product_id'] . "'");

      foreach ($order_option_query->rows as $option) {
      if ($option['type'] != 'file') {
      $value = $option['value'];
      } else {
      $value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
      }

      $option_data[] = array(
      'name' => $option['name'],
      'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
      );
      }

      $product_total = $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0);


      $products[] = array(
      'product_id' => $product['product_id'],
      'name' => $product['name'],
      'model' => $product['model'],
      'SKU' => $product['sku'],
      'option' => $option_data,
      'quantity' => $product['quantity'],
      'product_total' => $product_total, // without symbol
      'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
      'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
      );

      }
      $mail_data['products'] = $products;

      // Order Totals
      $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int) $order_id . "' ORDER BY sort_order ASC");

      foreach ($order_total_query->rows as $total) {
      $mail_data['totals'][$total['code']] = $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']);
      }

      if(! isset($mail_data['totals']['duties']) || $mail_data['totals']['duties']== ''){
      $mail_data['totals']['duties']  = $this->currency->format(0, $order_info['currency_code'], $order_info['currency_value']);
      }

      if(! isset($mail_data['totals']['coupon']) || $mail_data['totals']['coupon']== ''){
      $mail_data['totals']['coupon']  = $this->currency->format(0, $order_info['currency_code'], $order_info['currency_value']);
      }

      if(! isset($mail_data['totals']['discount']) || $mail_data['totals']['discount']== ''){
      $mail_data['totals']['discount']  = $this->currency->format(0, $order_info['currency_code'], $order_info['currency_value']);
      }

      if(! isset($mail_data['totals']['return_shipping']) || $mail_data['totals']['return_shipping']== ''){
      $mail_data['totals']['return_shipping']  = $this->currency->format(0, $order_info['currency_code'], $order_info['currency_value']);
      }

      if(! isset($mail_data['totals']['total_charges']) || $mail_data['totals']['total_charges']== ''){
      $mail_data['totals']['total_charges']  = $this->currency->format(0, $order_info['currency_code'], $order_info['currency_value']);
      }

      if(! isset($mail_data['totals']['total_charges']) || $mail_data['totals']['total_charges']== ''){
      $mail_data['totals']['total_charges']  = $this->currency->format(0, $order_info['currency_code'], $order_info['currency_value']);
      }

      $this->load->language("account/wk_mprma");
      $subject = sprintf($this->language->get("text_subject"),$data['order_id']);
      $mail_data['title'] = $subject;
      $mail_data['header'] = $this->language->get("text_head");
      $mail_data['text_greeting'] 			= sprintf($this->language->get("text_greeting"),$order_info['firstname'].' '.$order_info['lastname']);
      $mail_data['text_more_info'] 			= $this->language->get("text_more_info");
      $mail_data['billing_title']				 = $this->language->get("text_billing_title");
      $mail_data['text_payment_address'] = $this->language->get("text_payment_address");
      $mail_data['text_shipping_address'] = $this->language->get("text_shipping_address");
      $mail_data['text_payment_method'] = $this->language->get("text_payment_method");
      $mail_data['text_return_summary'] = $this->language->get("text_return_summary");
      $mail_data['text_order_date'] 		= $this->language->get("text_order_date");
      $mail_data['text_order_number'] 	= $this->language->get("text_order_number");
      $mail_data['text_return_date'] 		= $this->language->get("text_return_date");
      $mail_data['text_return_number']	 = $this->language->get("text_return_number");
      $mail_data['text_item'] 					= $this->language->get('text_item');
      $mail_data['text_price'] 					= $this->language->get('text_price');
      $mail_data['text_sku'] 					= $this->language->get('text_sku');
      $mail_data['text_quantity'] 			= $this->language->get('text_quantity');
      $mail_data['text_total'] 					= $this->language->get('text_total');

      $mail_data['text_return_credits'] 	= $this->language->get('text_return_credits');
      $mail_data['text_item_subtotal'] 		= $this->language->get('text_item_subtotal');
      $mail_data['text_original_shipping']= $this->language->get('text_original_shipping');
      $mail_data['text_duties'] 					= $this->language->get('text_duties');
      $mail_data['text_coupon'] 					= $this->language->get('text_coupon');
      $mail_data['text_total_credits'] 		= $this->language->get('text_total_credits');
      $mail_data['text_return_charges'] 	= $this->language->get('text_return_charges');
      $mail_data['text_original_order_discaount'] = $this->language->get('text_original_order_discaount');
      $mail_data['text_return_shipping'] 	= $this->language->get('text_return_shipping');
      $mail_data['text_total_charges'] 		= $this->language->get('text_total_charges');
      $mail_data['text_total_refund'] 		= $this->language->get('text_total_refund');


      $message =  $html = $this->load->view($this->config->get('config_template') . '/template/mail/rma_change_status.tpl', $mail_data);
      //'seller '.$data['seller']['firstname'].' '.$data['seller']['lastname']. ' changed rma status to '.$data['status'].' Rma-Id : #'.$data['rma_id'];
      $store_name = $this->config->get('config_name');




      $mail = new Mail();
      $mail->protocol = $this->config->get('config_mail_protocol');
      $mail->parameter = $this->config->get('config_mail_parameter');
      $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
      $mail->smtp_username = $this->config->get('config_mail_smtp_username');
      $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
      $mail->smtp_port = $this->config->get('config_mail_smtp_port');
      $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
      $mail->setTo($data['email']);
      $mail->setFrom($this->config->get('marketplace_adminmail'));
      $mail->setSender($store_name);
      $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
      $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
      $mail->send();

      } */

    public function sendMail($data) {

        $data['email_header'] = $this->load->view($this->config->get('config_template') . '/template/mail/header_en.tpl');
        $data['email_footer'] = $this->load->view($this->config->get('config_template') . '/template/mail/footer_en.tpl');


        $seller_html = $this->load->view($this->config->get('config_template') . '/template/mail/' . $data['file1'] . '.tpl', $data);
        $cs_html = $this->load->view($this->config->get('config_template') . '/template/mail/' . $data['file2'] . '.tpl', $data);

        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
        $mail->setTo($data['seller_email']);
        $mail->setFrom($this->config->get('marketplace_adminmail'));
        $mail->setSender($this->config->get('config_name'));
        $mail->setSubject(html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8'));
        $mail->setHtml($seller_html);
        $mail->send();

        $mail->setHtml($cs_html);
        $mail->setTo($data['email']);
        $mail->send();
    }

    public function getShippingAddressId($order_id) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "customer c ON o.customer_id=c.customer_id WHERE order_id = '" . $order_id . "'  ";

        $result = $this->db->query($sql)->row;
        return $result;
    }

    public function getProfile($customer_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer c2c LEFT JOIN customerpartner_to_customer_description c2cd ON(c2c.customer_id=c2cd.customer_id) where c2c.customer_id = '" . $customer_id . "' and c2cd.language_id = 1 ");
        $data = $query->row;
        $country = $this->getCountryINFO($data['country_id']);
        $zone = $this->getZoneINFO($data['zone_id']);

        $data['zone_name'] = $zone['name'];
        $data['country_name'] = $country['name'];


        return $data;
    }

    public function getCountryINFO($country_id) {

        $query = $this->db->query("SELECT name,iso_code_3,telecode FROM country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE cd.language_id = 1 AND c.country_id = '" . (int) $country_id . "' ");
        return $query->row;
    }

    public function getZoneINFO($zone_id) {

        $query = $this->db->query("SELECT name FROM zone z LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE zd.language_id = 1 AND z.zone_id = '" . (int) $zone_id . "' ");
        return $query->row;
    }

    public function add_booking($data = array()) {
        if (isset($data['book_response'])) {

            $sql = "INSERT INTO return_booking SET customer_id = '" . (int) $data['customer_id'] . "' , return_id = '" . (int) $data['return_id'] . "', order_id = '" . (int) $data['order_id'] . "', book_response='" . $this->db->escape($data['book_response']) . "', country_id='" . (int) $data['country_id'] . "', seller_id='" . (int) $data['seller_id'] . "', shipping_company='" . (int) $data['shipping_company'] . "' ,date_added = NOW() , date_modified= NOW()";

            $this->db->query($sql);
        }
    }

    public function getShippingCompany($country_id) {
        $query = $this->db->query("SELECT ship_company_id FROM ship_company_to_country WHERE country_id='" . (int) $country_id . "'");
        return $query->row['ship_company_id'];
    }

    public function get_FFCitycode_code($zone_id) {
        $query = $this->db->query("SELECT destination_code FROM firstflight_destination_code where zone_id='" . $zone_id . "' ");
        if ($query->num_rows) {
            return $query->row['destination_code'];
        }
        return '';
    }

    public function sendBookRequest($rma_id) {
        $this->load->model('account/order');
        $this->load->model('account/customerpartnerorder');
        $this->load->model('shipping/wk_custom_shipping');

        $is_exist = $this->db->query("SELECT count(return_id) as count FROM return_booking WHERE return_id='" . (int) $rma_id . "'")->row['count'];
        if ($is_exist == 0) {
            $return_info = $this->getRmaOrderid($rma_id);

            $data = array(
                'return_id' => $rma_id,
                "prod_type" => "DOX"
            );

            $order_id = $return_info['order_id'];
            $order_info = $this->model_account_order->getOrder($order_id);

            $seller_info = $this->getProfile($return_info['customer_id']);
            $shipping_company = $this->getShippingCompany($seller_info['country_id']);

            $operation_data = array();

            $operation_data['argAwbBooking']['Destination'] = $this->get_FFCitycode_code($order_info['shipping_zone_id']);

            $operation_data['argAwbBooking']['BRef'] = "{$order_id}/1";

            $operation_data['argAwbBooking']['ThirdParty'] = "O";
            $operation_data['argAwbBooking']['Consignee'] = $seller_info['firstname'] . ' ' . $seller_info['lastname'];
            $operation_data['argAwbBooking']['ConsigneeCPerson'] = $seller_info['firstname'] . ' ' . $seller_info['lastname'];
            $operation_data['argAwbBooking']['ConsigneeAddress1'] = $seller_info['address'];
            $operation_data['argAwbBooking']['ConsigneeAddress2'] = '';
            $operation_data['argAwbBooking']['ConsigneePhone'] = $seller_info['telephone'];
            $operation_data['argAwbBooking']['ConsigneeMobile'] = $seller_info['telephone'];
            $operation_data['argAwbBooking']['ConsigneeCity'] = $seller_info['zone_name'];
            $operation_data['argAwbBooking']['ConsigneeCountry'] = $seller_info['country_name'];
            $operation_data['argAwbBooking']['ConsigneePin'] = '';

            $operation_data['argAwbBooking']['ServiceType'] = 'NOR';
            $operation_data['argAwbBooking']['ProductType'] = 'XPS';
            $operation_data['argAwbBooking']['InvoiceValue'] = '0';
            $operation_data['argAwbBooking']['CODAmount'] = '0';

            $operation_data['argAwbBooking']['Weight'] = '0.00';
            $operation_data['argAwbBooking']['email'] = $seller_info['email'];
            $operation_data['argAwbBooking']['RTOShipment'] = '';
            $operation_data['argAwbBooking']['VehType'] = 'B';
            //product 1 , product 2,

            $operation_data['argAwbBooking']['AddnlInfo'] = ''; ////////
            $operation_data['argAwbBooking']['CustomerCode'] = ''; ///////
            $operation_data['argAwbBooking']['BDimension'] = '10*10*10';

            $operation_data['argAwbBooking']['PBranch'] = $this->get_FFCitycode_code($seller_info['zone_id']);

            $operation_data['argAwbBooking']['Origin'] = $this->get_FFCitycode_code($seller_info['zone_id']);
            $operation_data['argAwbBooking']['ReadyDate'] = date('Y-m-d');
            $operation_data['argAwbBooking']['ReadyTime'] = '08:00';
            $operation_data['argAwbBooking']['ClsoingTime'] = '16:00';
            $operation_data['argAwbBooking']['SpecialInst'] = 'shipper opening times:' . $seller_info['opening_time'];
            //$operation_data['argAwbBooking']['AWBNo'] = $data['AWBNO'];
            $operation_data['argAwbBooking']['Shipper'] = $order_info['firstname'] . " " . $order_info['lastname'];
            $operation_data['argAwbBooking']['ShipperCPErson'] = $order_info['firstname'] . " " . $order_info['lastname'];
            $operation_data['argAwbBooking']['ShipperAddress1'] = $order_info['payment_address_1'];
            $operation_data['argAwbBooking']['ShipperAddress2'] = $order_info['payment_address_2'];
            $operation_data['argAwbBooking']['ShipperPhone'] = $order_info['telephone'];
            //$operation_data['argAwbBooking']['ShipperPin'] = '';
            $operation_data['argAwbBooking']['ShipperCity'] = $order_info['payment_zone'];
            $operation_data['argAwbBooking']['ShipperCountry'] = $order_info['payment_country'];
            $booking_response = $this->FFServices->BookingRequest($operation_data);
            if ($booking_response) {
                $data["customer_id"] = $order_info['customer_id'];
                $data['book_response'] = $booking_response;
                $data['order_id'] = $order_id;
                $data['shipping_company'] = $shipping_company;
                $data['seller_id'] = $seller_info['customer_id'];
                $data['country_id'] = $seller_info['country_id'];


                $this->add_booking($data);
            }
        }
    }

}
