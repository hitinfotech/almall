<?php

class ModelAccountActivity extends Model {

    public function addActivity($key, $data) {
        if (isset($data['customer_id']) && $data['customer_id'] > 0) {
            $customer_id = (int) $data['customer_id'];
        } else {
            $customer_id = 'null';
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "customer_activity` SET `customer_id` = " .  $customer_id . ", `key` = '" . $this->db->escape($key) . "', `data` = '" . $this->db->escape(json_encode($data)) . "', `ip` = '', `date_added` = NOW()");
    }

}
