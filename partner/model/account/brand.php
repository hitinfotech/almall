<?php

class ModelAccountBrand extends Model {

    private $errors;

    public function addBrand($data) {

        $this->db->query(" INSERT INTO " . DB_PREFIX . "brand SET  `status` = '0', `is_algolia` = '0', `sort_order` = '0', `date_added`=NOW(), `date_modified`=NOW(), user_id = '" . $this->partner->getid() . "' ");
        $brand_id = $this->db->getLastId();

        foreach ($data['brand_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "brand_description SET brand_id = '" . (int) $brand_id . "', language_id = '" . (int) $language_id . "', `name` = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($this->stripTagsStyle(htmlspecialchars_decode($value['description']))) . "'");
        }

        if (isset($this->request->files['image']) && $this->request->files['image']["error"] == 0) {
            $image = $this->upload_file('brand', 'image', $brand_id);
            if ($image) {
                $this->load->model('tool/image');
                foreach ($this->config_image->get('shops_brands_designer') as $row) {
                    if ($row['width'] > 10) {
                        $this->model_tool_image->uploadImage($image, $row['width'], $row['hieght']);
                    }
                }
            } else {
                $image = '';
            }
            $this->db->query(" UPDATE brand SET `image` = '" . $this->db->escape($image) . "' WHERE brand_id='" . (int) $brand_id . "' ");
        }

        $this->url_custom->create_URL('brand', $brand_id);
        return $brand_id;
    }

  }
