<?php

class ModelLookLook extends Model {

    public function getLook($look_id) {
        $data = array();
        $sql = "SELECT l.look_id, l.date_added, l.date_modified, ld1.title title_en, ld1.description description_en, ld2.title title_ar, ld2.description description_ar, l2g.look_group_id AS group_id FROM " . DB_PREFIX . "look l LEFT JOIN " . DB_PREFIX . "look_description ld1 ON (l.look_id = ld1.look_id) LEFT JOIN " . DB_PREFIX . "look_description ld2 ON (l.look_id = ld2.look_id) LEFT JOIN look_to_group l2g ON (l.look_id = l2g.look_id) WHERE l.look_id = '" . (int) $look_id . "' AND ld1.language_id = '1' AND ld2.language_id = '2' AND l.status = '1'";
        if ($this->config->get('config_language_id') == 1) {
            $sql .= " AND l.is_english = 1 ";
        } else {
            $sql .= " AND l.is_arabic = 1 ";
        }

        $sql .= " GROUP BY l.look_id ";

        $query = $this->db->query($sql);

        if ($query->num_rows) {
            $data = $query->row;

            if ((int) $this->config->get('config_language_id') == 1) {
                $data['title'] = $data['title_en'];
                $data['description'] = $data['description_en'];
            }
            if ((int) $this->config->get('config_language_id') == 2) {
                $data['title'] = $data['title_ar'];
                $data['description'] = $data['description_ar'];
            }

            $images = $this->getLookImages($look_id);

            if (!empty($images)) {
                $data['image'] = $images[0]['image'];
                $data['images'] = $images;
            } else {
                $data['image'] = array();
                $data['images'] = array();
            }

            $data['products'] = $this->getLookProducts($look_id);
        }

        return $data;
    }

    public function getLooks($data = array()) {

        $cache = "look.getlooks." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);
        if (!$result) {
            $keyword_join = $keyword_where = $keyword_group = '';
            if (!empty($data['keyword_id'])) {
                $keyword_join = "LEFT JOIN " . DB_PREFIX . "looks_keyword lk ON (l.look_id = lk.look_id)";
                $keyword_where = "AND lk.keyword_id in (" . implode(",", $data['keyword_id']) . ")";
                $keyword_group = "GROUP BY lk.look_id";
            }

            $sql = " SELECT DISTINCT l.look_id FROM " . DB_PREFIX . "look l "
                    . "LEFT JOIN " . DB_PREFIX . "look_description ld ON (l.look_id = ld.look_id) "
                    . "LEFT JOIN " . DB_PREFIX . "look_to_group l2g ON (l.look_id = l2g.look_id) "
                    . "LEFT JOIN " . DB_PREFIX . "look_to_country l2c ON (l.look_id = l2c.look_id) "
                    . $keyword_join
                    . "WHERE l2c.country_id = '" . $this->country->getid() . "' AND l.status = '1' AND ld.language_id = '" . (int) $this->config->get('config_language_id') . "' ";

            if ($this->config->get('config_language_id') == 2) {
                $sql .= " AND l.is_arabic = 1 ";
            }
            if ($this->config->get('config_language_id') == 1) {
                $sql .= " AND l.is_english = 1 ";
            }

            if (!empty($data['filter_look_id'])) {
                $sql .= " AND l.look_id='" . (int) $data['filter_look_id'] . "' ";
            }

            if (!empty($data['look_id'])) {
                $sql .= " AND l.look_id<>'" . (int) $data['look_id'] . "' ";
            }

            if (!empty($data['filter_filter'])) {
                $sql .= " AND ld.title LIKE '%" . $this->db->escape($data['filter_filter']) . "%' ";
            }

            if (!empty($data['filter_mall_id'])) {
                $sql .= " AND l.mall_id='" . (int) $data['filter_mall_id'] . "' ";
            }

            if (!empty($data['filter_group_id'])) {
                $sql .= " AND l2g.look_group_id='" . (int) $data['filter_group_id'] . "' ";
            }

            if (!empty($data['filter_look_id'])) {
                $sql .= " AND l.look_id='" . (int) $data['filter_look_id'] . "' ";
            }
            $sql .= $keyword_where;
            $sql .= $keyword_group;

            $sort_data = array(
                'ld.title',
                'l.sort_order',
                'l.date_added',
                'l.date_modified'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                if ($data['sort'] == 'ld.title') {
                    $sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
                } else {
                    $sql .= " ORDER BY " . $data['sort'];
                }
            } else {
                $sql .= " ORDER BY l.date_added ";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC, LCASE(ld.title) DESC";
            } else {
                $sql .= " DESC, LCASE(ld.title) ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = $this->config->get('config_look_limit');
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $query = $this->db->query($sql);

            $result = array();

            foreach ($query->rows as $row) {
                $result[$row['look_id']] = $this->getLook($row['look_id']);
            }
            if (!empty($result)) {
                $this->cache->set($cache, $result);
            }
            //var_dump($sql);die;
            return $result;
        } else {
            return $result;
        }
    }

    public function getTotalLooks($data = array()) {
        $cache = "look.gettotallooks." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);

        if (!$result) {

            $sql = " SELECT COUNT(DISTINCT l.look_id) AS total FROM " . DB_PREFIX . "look l "
                    . "LEFT JOIN " . DB_PREFIX . "look_description ld ON (l.look_id = ld.look_id) "
                    . "LEFT JOIN " . DB_PREFIX . "look_to_group l2g ON (l.look_id = l2g.look_id) "
                    . "LEFT JOIN " . DB_PREFIX . "look_to_country l2c ON (l.look_id = l2c.look_id) "
                    . "WHERE l2c.country_id = '" . $this->country->getid() . "' AND l.status = '1' AND ld.language_id = '" . (int) $this->config->get('config_language_id') . "' ";

            if (!empty($data['filter_look_id'])) {
                $sql .= " AND l.look_id='" . (int) $data['filter_look_id'] . "' ";
            }

            if (!empty($data['filter_filter'])) {
                $sql .= " AND ld.title LIKE '%" . $this->db->escape($data['filter_filter']) . "%' ";
            }

            if (!empty($data['filter_mall_id'])) {
                $sql .= " AND l.mall_id='" . (int) $data['filter_mall_id'] . "' ";
            }
            if ($this->config->get('config_language_id') == 2) {
                $sql .= " AND l.is_arabic = 1 ";
            }
            if ($this->config->get('config_language_id') == 1) {
                $sql .= " AND l.is_english = 1 ";
            }
            if (!empty($data['filter_group_id'])) {
                $sql .= " AND l2g.look_group_id='" . (int) $data['filter_group_id'] . "' ";
            }

            $query = $this->db->query($sql);
            if (!empty($query->row['total'])) {
                $this->cache->set($cache, $query->row['total']);
            }
            return $query->row['total'];
        } else {
            return $result;
        }
    }

    public function getLookImages($look_id) {

        $cache = "look.getlookimages." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($look_id));

        $result = $this->cache->get($cache);

        if (!$result) {
            $query = $this->db->query("SELECT * FROM look_image WHERE look_id = '" . (int) $look_id . "' ORDER BY featured='1' ");

            if (!empty($query->rows)) {
                $this->cache->set($cache, $query->rows);
            }

            return $query->rows;
        } else {
            return $result;
        }
    }

    public function getLookProducts($look_id) {

        $cache = "look.getlookproducts." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($look_id));

        $result = $this->cache->get($cache);

        if (!$result) {

            $query = $this->db->query("SELECT product_id FROM look_product WHERE look_id = '" . (int) $look_id . "' ORDER BY sort_order ");

            $result = array();

            $this->load->model('catalog/product');

            foreach ($query->rows as $row) {

                $product_info = $this->model_catalog_product->getProduct($row['product_id']);

                if ($product_info) {
                    $result[$row['product_id']] = $product_info;
                }
            }

            if (!empty($result)) {
                $this->cache->set($cache, $result);
            }

            return $result;
        }

        return $result;
    }

    public function getLookTags($look_id) {
        $cache = "look.getLookTags." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($look_id));
        $result = $this->cache->get($cache);

        if (!$result) {
            $query = $this->db->query("SELECT tag_id FROM tag_look WHERE look_id = '" . (int) $look_id . "' ORDER BY sort_order ");

            $result = array();

            $this->load->model('tags/tags');
            foreach ($query->rows as $row) {
                $tag_info = $this->model_tags_tags->getTag($row['product_id']);
                if ($tag_info) {
                    $result[$row['tag_id']] = $tag_info;
                }
            }
            if (!empty($result)) {
                $this->cache->set($cache, $result);
            }

            return $result;
        } else {
            return $result;
        }
    }

    public function getLooksByCity($city_id = 0) {
        $cache = "look.getLooksByCity." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($city_id));
        $result = $this->cache->get($cache);

        if (!$result) {
            $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "look l LEFT JOIN " . DB_PREFIX . "look_description ld ON (l.look_id = ld.look_id) WHERE m.city_id = '" . (int) $city_id . "' AND ld.language_id = '" . (int) $this->config->get('config_language_id') . "' l.status = '1'");

            if (!empty($query->rows)) {
                $this->cache->set($cache, $query->rows);
            }

            return $query->rows;
        } else {
            return $result;
        }
    }

    public function getLooksGroups() {
        $key = 'look.groups.' . (int) $this->config->get('config_language_id');
        $result = $this->cache->get($key);
        if (!$result) {
            $sql = "SELECT * FROM " . DB_PREFIX . "look_group tg LEFT JOIN " . DB_PREFIX . "look_group_description tgd ON (tg.look_group_id = tgd.look_group_id) WHERE tg.status = '1' AND tgd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY tg.sort_order ";

            $query = $this->db->query($sql);
            if ($query->num_rows) {
                $this->cache->set($key, $query->rows);
            }

            return $query->rows;
        } else {
            return $result;
        }
    }

    public function getRelatedLooks($look_id, $data = array()) {
        $key = 'look.getRelatedLooks.' . (int) $this->config->get('config_language_id') . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));
        $result = $this->cache->get($key);
        if (!$result) {
            $keyword_join = $keyword_where = $keyword_group = '';
            if (isset($data['keyword_id']) && !empty($data['keyword_id'])) {

                $keyword_join = "  LEFT JOIN " . DB_PREFIX . "looks_keyword lk ON (l.look_id = lk.look_id)";
                $keyword_where = "AND lk.keyword_id in (" . implode(",", $data['keyword_id']) . ")";
                $keyword_group = "Group By lk.look_id";
            }
            $sql = " SELECT l.*,ld.* FROM " . DB_PREFIX . "look l LEFT JOIN " . DB_PREFIX . "look_description ld ON (l.look_id = ld.look_id) $keyword_join WHERE l.status = '1' AND ld.language_id = '" . (int) $this->config->get('config_language_id') . "' ";

            if (!empty($data['filter_look_id'])) {
                $sql .= " AND l.look_id='" . (int) $data['filter_look_id'] . "' ";
            }

            if (!empty($data['filter_is_famous'])) {
                $sql .= " AND l.is_famous='" . (int) $data['filter_is_famous'] . "' ";
            }

            if (!empty($data['filter_filter'])) {
                $sql .= " AND ld.title LIKE '%" . $this->db->escape($data['filter_filter']) . "%' ";
            }

            if (!empty($data['filter_mall_id'])) {
                $sql .= " AND l.mall_id='" . (int) $data['filter_mall_id'] . "' ";
            }
            if ($this->config->get('config_language_id') == 2) {
                $sql .= " AND l.is_arabic = 1 ";
            }
            if ($this->config->get('config_language_id') == 1) {
                $sql .= " AND l.is_english = 1 ";
            }
            if (!empty($data['filter_group_id'])) {
                $sql .= " AND l.group_id='" . (int) $data['filter_group_id'] . "' ";
            }

            $sql .=$keyword_where;
            $sql .=$keyword_group;
            $sort_data = array(
                'ld.title',
                'l.sort_order',
                'l.date_added'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                if ($data['sort'] == 'ld.title') {
                    $sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
                } else {
                    $sql .= " ORDER BY " . $data['sort'];
                }
            } else {
                $sql .= " ORDER BY l.date_added ";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC, LCASE(ld.title) DESC";
            } else {
                $sql .= " DESC, LCASE(ld.title) ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }
            //var_dump($sql);die;
            $query = $this->db->query($sql);

            $result = array();

            foreach ($query->rows as $row) {
                $result[$row['look_id']] = $this->getLook($row['look_id']);
            }
            if ($result) {
                $this->cache->set($key, $result);
            }

            return $result;
        } else {
            return $result;
        }
    }

    public function getLookKeywords($look_id) {
        $query = $this->db->query("SELECT lk.keyword_id,kd.name FROM looks_keyword lk LEFT JOIN keyword_description kd ON (lk.keyword_id = kd.keyword_id) WHERE lk.look_id='" . $look_id . "' and kd.language_id='" . (int) $this->config->get('config_language_id') . "'");
        return $query->rows;
    }

}
