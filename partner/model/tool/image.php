<?php

class ModelToolImage extends Model {

    public function resize($filename, $width, $height, $force = false) {

        //$this->uploadImage($filename,$width,$height,true);

        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (defined('HTTPS_IMAGE_S3')) {
            return HTTPS_IMAGE_S3 . $new_image;
        } else {
            if ($this->request->server['HTTPS']) {
                return $this->config->get('config_ssl') . 'image/' . $new_image;
            } else {
                return $this->config->get('config_url') . 'image/' . $new_image;
            }
        }
    }

    public function uploadImage($filename, $width, $height, $upload = true) {
        if (!is_file(DIR_IMAGE . $filename)) {
            return;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $old_image = $filename;
        $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if ($upload) {
            $this->db->query("INSERT INTO image_s3 SET image='" . $this->db->escape($old_image) . "' ,
                                                   new_image_path='" . $this->db->escape($new_image) . "' ,
                                                   width='" . $this->db->escape($width) . "' ,
                                                   height='" . $this->db->escape($height) . "' , 
                                                   date_added=NOW(), user_id=NULL ");
            return HTTPS_SERVER . (ltrim(DIR_IMAGE, DIR_ROOT)) . $new_image;
        } else {
            $this->db->query("INSERT INTO image_s3 SET image='" . $this->db->escape($old_image) . "' ,
                                                   new_image_path='" . $this->db->escape($new_image) . "' ,
                                                   width='" . $this->db->escape($width) . "' ,
                                                   height='" . $this->db->escape($height) . "' , 
                                                   upload_image='0', 
                                                   date_added=NOW(), user_id=NULL ");
        }

        return HTTPS_SERVER . (ltrim(DIR_IMAGE, DIR_ROOT)) . $new_image;
    }

}
