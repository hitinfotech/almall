<?php

class ModelCatalogCategory extends Model {

    public static $ignore = array(
        748 => array(904, 903, 1047, 750, 862),
        722 => array(1023),
        838 => array(1022),
        982 => array(723, 727, 867, 754),
        863 => array(762),
        984 => array(1145, 1184),
        1150 => array(981, 1052, 983),
        758 => array(761),
        718 => array(
            901, 719, 965, 768, 1095, 773, 774, 940, 1025, 938, 1006,
            967, 772, 1175, 1046, 937, 1029, 771, 778, 885, 890, 1171,
            775, 7756, 777, 1030, 1180, 966, 1020
        ),
        941 => array(
            1144, 1021, 1019, 1027, 1153, 970, 1024, 1178,
            1042, 971, 974, 1045, 976, 991, 1105, 1032, 1124,
            980, 975, 1172, 978, 1010, 979, 977, 1018, 1157, 1158
        ),
        1057 => array(
            1189, 1155, 1060, 1187, 1122, 1059,
            1058, 1143, 1016, 1177, 1017
        ),
        961 => array(1011, 1106, 1012, 1014, 1013, 1015),
        726 => array(780, 781, 782, 932),
        936 => array(972, 1068, 995, 943, 993, 1125, 994),
        785 => array(793),
        796 => array(797),
        829 => array(1031),
        946 => array(1115, 957),
        878 => array(884, 902, 882, 880, 883),
        821 => array(1113, 1114, 1067, 935, 824, 825),
        816 => array(1009, 817),
        1112 => array(820, 1110),
        1119 => array(1121),
        1118 => array(1120),
        1070 => array(
            1084, 1087, 1073, 1072, 1090, 1074,
            1077, 1078, 1091, 1076, 1117
        ),
        1071 => array(
            1086, 1088, 1083, 1079, 1080, 1082, 1092, 1081, 1116
        ),
        1096 => array(
            1102, 1136, 1100, 1099, 1097, 1154, 1101, 1098
        ),
        933 => array(952),
        1005 => array(1050, 989, 1123, 951, 1111, 741, 958),
        739 => array(1134, 1135),
        1039 => array(1041, 1040),
        745 => array(1183, 1181, 1182, 857, 1063),
        746 => array(907, 1174, 955, 850, 874),
        742 => array(
            1007, 1028, 998, 999, 1000, 1002, 1004, 1003,
            949, 1008, 1103, 948, 997, 1001, 996
        ),
        747 => array(
            939, 909, 1066, 920, 875, 1162, 918, 1054, 1159, 1133,
            1165, 872, 1161, 1107, 1026, 1164, 871, 1104, 910,
            1163, 1049, 1151, 1160, 908
        ),
        743 => array(950, 876, 1188, 942, 849, 921, 906, 912),
        744 => array(
            924, 905, 954, 1131, 1128, 1130, 1132, 1129, 1127,
            1170, 919, 852, 1126, 960, 1137, 1152, 1138, 853,
            851, 856, 1048, 854, 959, 866, 1140, 1139, 1173, 855
        ),
        868 => array(870),
        1146 => array(1167, 1148, 1166, 1147, 1149),
        981 => array(
            1056, 900, 1051, 894, 900, 1051, 894, 892, 896, 895
        ),
        1033 => array(
            1037, 1036, 1168, 1169, 1061, 1062, 986, 988, 1043, 1044
        ),
    );

    public function getIgnoredCategoryId($category_id) {
        foreach (static::$ignore as $key => $row) {
            $row_id = array_search($category_id, $row);

            if ($row_id !== false) {
                return $key;
            }
        }

        return $category_id;
    }

    public function getCategory($category_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int) $category_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND c.status = '1'");

        return $query->row;
    }

    public function getCategorySearchName($category_id) {
        $query = $this->db->query(" SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR ':') AS name, GROUP_CONCAT(cd1.category_id ORDER BY cp.level SEPARATOR '_') AS path , c1.parent_id, c1.sort_order FROM category_path cp LEFT JOIN category c1 ON (cp.category_id = c1.category_id) LEFT JOIN category c2 ON (cp.path_id = c2.category_id) LEFT JOIN category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int) $this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int) $this->config->get('config_language_id') . "' AND cp.category_id = '" . (int) $category_id . "' GROUP BY cp.category_id ORDER BY name ASC ");

        return $query->row;
    }

    public function getCategories($parent_id = 0) {
        $key = 'categories.' . (int) $parent_id . '.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id');
        $result = $this->cache->get($key);
        if (!$result) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

            if ($query->num_rows) {
                $this->cache->set($key, $query->rows);
            }

            return $query->rows;
        }
        return $result;
    }

    public function getCategoriesWithCount($parent_id = 0) {

        $key = 'category.getcategorieswithcount.' . (int) $parent_id . '.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id');

        $result = $this->cache->get($key);

        if (!$result) {

            $query = $this->db->query("SELECT DISTINCT *, COUNT(p2c.product_id) pcounter FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) LEFT JOIN product_to_category p2c ON(c.category_id=p2c.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND c.status = '1' AND c.top='1' GROUP BY c.category_id ORDER BY c.sort_order, LCASE(cd.name)");

            if ($query->num_rows) {
                $this->cache->set($key, $query->rows);
            }

            return $query->rows;
        }
        return $result;
    }

    public function getCategoryFilters($category_id) {
        $implode = array();

        $query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int) $category_id . "'");

        foreach ($query->rows as $result) {
            $implode[] = (int) $result['filter_id'];
        }

        $filter_group_data = array();

        if ($implode) {
            $filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int) $this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

            foreach ($filter_group_query->rows as $filter_group) {
                $filter_data = array();

                $filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int) $filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

                foreach ($filter_query->rows as $filter) {
                    $filter_data[] = array(
                        'filter_id' => $filter['filter_id'],
                        'name' => $filter['name']
                    );
                }

                if ($filter_data) {
                    $filter_group_data[] = array(
                        'filter_group_id' => $filter_group['filter_group_id'],
                        'name' => $filter_group['name'],
                        'filter' => $filter_data
                    );
                }
            }
        }

        return $filter_group_data;
    }

    public function getCategoryLayoutId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int) $category_id . "' AND store_id = '" . (int) $this->config->get('config_store_id') . "'");

        if ($query->num_rows) {
            return $query->row['layout_id'];
        } else {
            return 0;
        }
    }

    public function getTotalCategoriesByCategoryId($parent_id = 0) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND c.status = '1'");

        return $query->row['total'];
    }

    ///// temp funcs by qasem
    public function getList($from = null, $limit = null, $where = [], $children = true) {
        $query = "SELECT %s FROM category ";

        if (!empty($where)) {
            $query .= "WHERE 1 ";
            foreach ($where as $for_key => $for_val) {
                $for_val = (array) $for_val;
                if (!empty($for_val)) {
                    $query.= "AND category.{$for_key} IN(" .
                            implode(',', $for_val) . ") ";
                }
            }
        }

        $db_total = $this->db->query(sprintf($query, 'COUNT(category.category_id) AS total'));

        if (isset($from) || isset($limit)) {
            $limit = $limit? : 0;
            $query .= "LIMIT ";

            if (isset($from)) {
                $query .= "{$from}, ";
            }
            if ($limit) {
                $query .= "{$limit} ";
            }
        }
        $db_query = $this->db->query(sprintf($query, 'category.*'));

        $rows = array();
        foreach ($db_query->rows as $for_item) {
            $for_item_desc = $this->getDescriptions($for_item['category_id']);

            $row = array();
            $row['type'] = 'product';
            $row['id'] = $for_item['category_id'];
            $row['category_id'] = $for_item['parent_id'];
            $row['priority'] = $for_item['sort_order'];
            $row['created_at'] = $for_item['date_added'];
            $row['updated_at'] = $for_item['date_modified'];
            $row['hidden'] = 0;

            $row = array_merge($row, $for_item_desc);

            if ($children) {
                $row_children = $this->{__FUNCTION__}(null, null, [
                    'parent_id' => $row['id']
                ]);
                $row['children'] = $row_children->rows;
            }
            $rows[] = $row;
        }

        return (object) array(
                    'rows' => $rows,
                    'total' => $db_total->row['total']
        );
    }



    protected function getPathSelectQuery($select, $where = null, $limit = null, $group_by = null) {
        $query = '
            SELECT %1$s FROM category AS cat
              LEFT JOIN category_path AS cat_path
                ON cat_path.category_id = cat.category_id
            WHERE %2$s
        ';
        $where = $where? : '1';
        if ($group_by) {
            $query.= 'GROUP BY %3$s ';
        }
        if ($limit) {
            $query.= 'LIMIT %4$s ';
        }

        return $this->db->query(vsprintf(
                                $query, [$select, $where, $group_by, $limit]
        ));
    }

    public function getPathList($from = null, $limit = null, $where = null, $children = true) {
        $selector = [ 'cat.*'];

        $query_select = $this->getPathSelectQuery(
                implode(',', $selector), $where, "{$from}, {$limit}", 'cat.category_id'
        );

        $query_total = $this->getPathSelectQuery(
                'COUNT(cat.category_id) AS total', $where
        );

        $total = isset($query_total->row['total']) ? $query_total->row['total'] : 0;

        $rows = [];
        foreach ($query_select->rows as $for_item) {
            $for_item_desc = $this->getDescriptions($for_item['category_id']);

            $row = array();
            $row['type'] = 'product';
            $row['id'] = $for_item['category_id'];
            $row['category_id'] = $for_item['parent_id'];
            $row['priority'] = $for_item['sort_order'];
            $row['created_at'] = $for_item['date_added'];
            $row['updated_at'] = $for_item['date_modified'];

            $row = array_merge($row, $for_item_desc);

            if ($children) {
                // $this->{__FUNCTION__}(null, null, '', true);
            }
            $rows[] = $row;
        }

        return (object) [
                    'rows' => $rows,
                    'total' => $total
        ];
    }

    public function getDescriptions($category_id) {
        $query = "SELECT %s FROM category_description ";
        $query .= "WHERE category_description.category_id=%d";

        $db_query = $this->db->query(sprintf(
                        $query, 'category_description.*', $category_id
        ));

        $names = array();
        foreach ($db_query->rows as $for_item) {
            $key_suffix = ($for_item['language_id'] == 1) ? 'en' : 'ar';
            $names["name_{$key_suffix}"] = $for_item['name'];
        }
        return $names;
    }

    public function getCategoriesAttributes($category_id){
        $attributes = array ();
        $query = $this->db->query(" SELECT agt.attribute_group_id as group_id , name FROM attribute_group_to_category agt  LEFT JOIN attribute_group_description ad ON (agt.attribute_group_id = ad.attribute_group_id) WHERE agt.category_id = '" . (int) $category_id . "' AND ad.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        if ($query->num_rows) {
          foreach($query->rows as $group_attr){
            $attributes[$group_attr['group_id']]['name'] = $group_attr['name'];
            $attribute_query = $this->db->query('SELECT a.attribute_id,name FROM attribute a LEFT JOIN attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE attribute_group_id='.(int)$group_attr['group_id'] .' and language_id= '.(int) $this->config->get('config_language_id') .' ');
            if($attribute_query->num_rows > 0){
              $attributes[$group_attr['group_id']]['attributes'] = $attribute_query->rows;
            }
          }
            return $attributes;
        }

        return array();


    }
}
