<?php

class ModelCatalogProduct extends Model {

    public function getCustomFieldOptionId($pro_id, $id) {
        $result = $this->db->query("SELECT option_id FROM " . DB_PREFIX . "wk_custom_field_product_options WHERE fieldId = '" . (int) $id . "' AND product_id = '" . (int) $pro_id . "' ")->rows;
        return $result;
    }

    public function getProductCustomFields($id) {
        $result = $this->db->query("SELECT fieldId FROM " . DB_PREFIX . "wk_custom_field_product WHERE productId = '" . (int) $id . "' ")->rows;
        return $result;
    }

    public function getCustomFieldName($id) {
        $result = $this->db->query("SELECT fieldName FROM " . DB_PREFIX . "wk_custom_field_description WHERE fieldId = '" . (int) $id . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "' ")->row;
        return $result['fieldName'];
    }

    public function getCustomFieldOption($id) {
        $result = $this->db->query("SELECT optionValue FROM " . DB_PREFIX . "wk_custom_field_option_description WHERE optionId = '" . (int) $id . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "' ")->row;
        return $result['optionValue'];
    }

    public function updateViewed($product_id) {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET viewed = (viewed + 1) WHERE product_id = '" . (int) $product_id . "'");
    }

    public function getProduct($product_id, $country_id = 0) {


        $country_id = $this->country->getid();

        $query = $this->db->query("SELECT DISTINCT *,p.date_added as added_on, b.image AS brand_logo, (SELECT name FROM brand_description bd WHERE p.brand_id = bd.brand_id AND bd.language_id='" . (int) $this->config->get('config_language_id') . "')AS brand_name , pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.country_id = '" . (int) $country_id . "' AND pd2.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.deleted='no' AND ps.country_id = '" . (int) $country_id . "' AND ps.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int) $this->config->get('config_language_id') . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int) $this->config->get('config_language_id') . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int) $this->config->get('config_language_id') . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) LEFT JOIN brand b ON(p.brand_id=b.brand_id) WHERE p.product_id = '" . (int) $product_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.status = '1' AND b.status = '1' AND p.date_available <= NOW() ");


        if ($query->num_rows) {
            $seller_info = $this->db->query(" SELECT available_country FROM " . DB_PREFIX . "customerpartner_to_customer WHERE customer_id IN (SELECT customer_id FROM customerpartner_to_product WHERE product_id = '" . (int) $product_id . "') ");

            $available = 0;

            if ($seller_info->num_rows) {
                if ($seller_info->row['available_country'] == 0 || $seller_info->row['available_country'] == $this->country->getId()) {
                    $available = 1;
                }
            }

            $stock_status_id = $query->row['stock_status_id'];

            return array(
                'product_id' => $query->row['product_id'],
                'name' => $query->row['name'],
                'description' => $query->row['description'],
                'meta_title' => $query->row['meta_title'],
                'meta_description' => $query->row['meta_description'],
                'meta_keyword' => $query->row['meta_keyword'],
                'tag' => $query->row['tag'],
                'model' => $query->row['model'],
                'sku' => $query->row['sku'],
                'upc' => $query->row['upc'],
                'ean' => $query->row['ean'],
                'jan' => $query->row['jan'],
                'isbn' => $query->row['isbn'],
                'mpn' => $query->row['mpn'],
                'location' => $query->row['location'],
                'quantity' => $query->row['quantity'],
                'stock_status' => $query->row['stock_status'],
                'brand_id' => $query->row['brand_id'],
                'brand_logo' => $query->row['brand_logo'],
                'brand_name' => $query->row['brand_name'],
                'stock_status_id' => $stock_status_id,
                'product_available' => $available,
                'image' => $query->row['image'],
                'manufacturer_id' => $query->row['manufacturer_id'],
                'manufacturer' => $query->row['manufacturer'],
                'price' => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
                'special' => $query->row['special'],
                //'shop_logo' => $store_logo,
                //'store_id' => $store_id,
                'reward' => $query->row['reward'],
                'points' => $query->row['points'],
                'tax_class_id' => $query->row['tax_class_id'],
                'date_available' => $query->row['date_available'],
                'weight' => $query->row['weight'],
                'weight_class_id' => $query->row['weight_class_id'],
                'length' => $query->row['length'],
                'width' => $query->row['width'],
                'height' => $query->row['height'],
                'length_class_id' => $query->row['length_class_id'],
                'subtract' => $query->row['subtract'],
                'rating' => round($query->row['rating']),
                'reviews' => $query->row['reviews'] ? $query->row['reviews'] : 0,
                'minimum' => $query->row['minimum'],
                'sort_order' => $query->row['sort_order'],
                'status' => $query->row['status'],
                'date_added' => $query->row['added_on'],
                'date_modified' => $query->row['date_modified'],
                'viewed' => $query->row['viewed']
            );
        } else {
            return false;
        }
    }

    public function getCustLatestProducts() {

        $sql = "SELECT DISTINCT p.product_id FROM product p LEFT JOIN product_description pd ON (p.product_id = pd.product_id) LEFT JOIN product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN store s ON(p2s.store_id=s.store_id) LEFT JOIN zone z ON(s.zone_id=z.zone_id) WHERE pd.language_id = '1' AND p.status = '1' AND p.date_available <= NOW() AND z.country_id = '" . $this->country->getid() . "' GROUP BY p.brand_id ORDER BY p.date_added DESC, LCASE(pd.name) DESC LIMIT 0,8";

        $product_data = array();

        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
            if (!$product_data[$result['product_id']]) {

                unset($product_data[$result['product_id']]);
            }
        }

        return $product_data;
    }

    public function getCustLatestProducts2($data = array()) {

        $cache = "product.getcustlatestproducts2." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $sql = "SELECT pl.product_id FROM product_latest pl LEFT JOIN product p ON(pl.product_id=p.product_id) WHERE p.status = '1' AND pl.country_id = '" . $this->country->getid() . "' ORDER BY pl.sort_order LIMIT 0,12 ";

            $product_data = array();

            $query = $this->db->query($sql);

            foreach ($query->rows as $result) {
                $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
                if (!$product_data[$result['product_id']]) {

                    unset($product_data[$result['product_id']]);
                }
            }
            if (!empty($product_data)) {
                $this->cache->set($cache, $product_data);
            }
            return $product_data;
        } else {
            return $result;
        }
    }

    public function getProducts($data = array()) {
        $cache = "product.getproducts." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            //$sql = "SELECT p.product_id, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special";

            $sql = "SELECT p.product_id, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.deleted='no' AND ps.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special ";

            if (!empty($data['filter_category_id'])) {
                if (!empty($data['filter_sub_category'])) {
                    $sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
                } else {
                    $sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
                }

                if (!empty($data['filter_filter'])) {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
                } else {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
                }
            } else {
                $sql .= " FROM " . DB_PREFIX . "product p";
            }

            $sql .= " LEFT JOIN customerpartner_to_product cp2p ON(p.product_id=cp2p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN store s ON(p2s.store_id=s.store_id) LEFT JOIN zone z ON(s.zone_id=z.zone_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND z.country_id = '" . (int) $this->country->getid() . "'";

            if (!empty($data['filter_category_id'])) {
                if (!empty($data['filter_sub_category'])) {
                    $sql .= " AND cp.path_id = '" . (int) $data['filter_category_id'] . "'";
                } else {
                    $sql .= " AND p2c.category_id = '" . (int) $data['filter_category_id'] . "'";
                }

                if (!empty($data['filter_filter'])) {
                    $implode = array();

                    $filters = explode(',', $data['filter_filter']);

                    foreach ($filters as $filter_id) {
                        $implode[] = (int) $filter_id;
                    }

                    $sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
                }
            }

            if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
                $sql .= " AND (";

                if (!empty($data['filter_name'])) {
                    $implode = array();

                    $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

                    foreach ($words as $word) {
                        $implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
                    }

                    if ($implode) {
                        $sql .= " " . implode(" AND ", $implode) . "";
                    }

                    if (!empty($data['filter_description'])) {
                        $sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
                    }
                }

                if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                    $sql .= " OR ";
                }

                if (!empty($data['filter_tag'])) {
                    $sql .= "pd.tag LIKE '%" . $this->db->escape($data['filter_tag']) . "%'";
                }

                if (!empty($data['filter_name'])) {
                    $sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                }

                $sql .= ")";
            }

            if (!empty($data['filter_manufacturer_id'])) {
                $sql .= " AND p.manufacturer_id = '" . (int) $data['filter_manufacturer_id'] . "'";
            }

            if (!empty($data['filter_stock_status_id'])) {
                $sql .= " AND p.stock_status_id <> 10 ";
            }

            if (!empty($data['filter_seller_id'])) {
                $sql .= " AND cp2p.customer_id='" . (int) $data['filter_seller_id'] . "' ";
            }

            if (!empty($data['filter_brand_id'])) {
                $sql .= " AND p.brand_id='" . (int) $data['filter_brand_id'] . "' ";
            }

            if (!empty($data['range_price'])) {
                $price = $data['range_price'] - ($data['range_price'] * 30 / 100);
                $sql .= " AND p.order_range > '" . (int) $price . "' ";
            }

            if (!empty($data['ex_product_id'])) {
                $sql .= " AND p.product_id != " . (int) $data['ex_product_id'] . " ";
            }

            $sql .= " GROUP BY p.product_id";

            $sort_data = array(
                'pd.name',
                'p.model',
                'p.quantity',
                'p.price',
                'p.order_range',
                'rating',
                'p.sort_order',
                'p.date_added'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
                    $sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
                } elseif ($data['sort'] == 'p.price') {
                    $sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
                } else {
                    $sql .= " ORDER BY " . $data['sort'];
                }
            } else {
                $sql .= " ORDER BY p.sort_order";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC, LCASE(pd.name) DESC";
            } else {
                $sql .= " ASC, LCASE(pd.name) ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $product_data = array();

            $query = $this->db->query($sql);

            foreach ($query->rows as $result) {
                $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
                if (!$product_data[$result['product_id']]) {

                    unset($product_data[$result['product_id']]);
                }
            }

            if (!empty($product_data)) {
                $this->cache->set($cache, $product_data);
            }
            return $product_data;
        } else {
            return $result;
        }
    }

    public function getProductSpecials($data = array()) {
        $sql = "SELECT DISTINCT ps.product_id, (SELECT AVG(rating) FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = ps.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND ps.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND ps.deleted='no' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id";

        $sort_data = array(
            'pd.name',
            'p.model',
            'ps.price',
            'rating',
            'p.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
                $sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
            } else {
                $sql .= " ORDER BY " . $data['sort'];
            }
        } else {
            $sql .= " ORDER BY p.sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC, LCASE(pd.name) DESC";
        } else {
            $sql .= " ASC, LCASE(pd.name) ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $product_data = array();

        $query = $this->db->query($sql);

        foreach ($query->rows as $result) {
            $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
            if (!$product_data[$result['product_id']]) {
                unset($product_data[$result['product_id']]);
            }
        }

        return $product_data;
    }

    public function getLatestProducts($limit) {
        $product_data = $this->cache->get('product.latest.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int) $limit);

        if (!$product_data) {
            $query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p WHERE p.status = '1' AND p.date_available <= NOW() ORDER BY p.date_added DESC LIMIT " . (int) $limit);

            foreach ($query->rows as $result) {
                $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
                if (!$product_data[$result['product_id']]) {

                    unset($product_data[$result['product_id']]);
                }
            }

            $this->cache->set('product.latest.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int) $limit, $product_data);
        }

        return $product_data;
    }

    public function getPopularProducts($limit) {
        $product_data = array();

        $query = $this->db->query("SELECT p.product_id FROM " . DB_PREFIX . "product p WHERE p.status = '1' AND p.date_available <= NOW() ORDER BY p.viewed DESC, p.date_added DESC LIMIT " . (int) $limit);

        foreach ($query->rows as $result) {
            $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
            if (!$product_data[$result['product_id']]) {

                unset($product_data[$result['product_id']]);
            }
        }

        return $product_data;
    }

    public function getBestSellerProducts($limit) {
        $product_data = $this->cache->get('product.bestseller.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int) $limit);

        if (!$product_data) {
            $product_data = array();

            $query = $this->db->query("SELECT op.product_id, SUM(op.quantity) AS total FROM " . DB_PREFIX . "order_product op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id  AND o.deleted='0') LEFT JOIN `" . DB_PREFIX . "product` p ON (op.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN store s ON(p2s.store_id=s.store_id) LEFT JOIN zone z ON(s.zone_id=z.zone_id) WHERE o.order_status_id > '0' AND p.status = '1' AND p.date_available <= NOW() AND z.country_id = '" . (int) $this->country->getid() . "' GROUP BY op.product_id ORDER BY total DESC LIMIT " . (int) $limit);

            foreach ($query->rows as $result) {
                $product_data[$result['product_id']] = $this->getProduct($result['product_id']);
                if (!$product_data[$result['product_id']]) {

                    unset($product_data[$result['product_id']]);
                }
            }

            $this->cache->set('product.bestseller.' . (int) $this->config->get('config_language_id') . '.' . (int) $this->config->get('config_store_id') . '.' . $this->config->get('config_customer_group_id') . '.' . (int) $limit, $product_data);
        }

        return $product_data;
    }

    public function getProductAttributes($product_id) {
        $product_attribute_group_data = array();

        $product_attribute_group_query = $this->db->query("SELECT ag.attribute_group_id, agd.name FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_group ag ON (a.attribute_group_id = ag.attribute_group_id) LEFT JOIN " . DB_PREFIX . "attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) WHERE pa.deleted != '1' AND pa.product_id = '" . (int) $product_id . "' AND agd.language_id = '" . (int) $this->config->get('config_language_id') . "' GROUP BY ag.attribute_group_id ORDER BY ag.sort_order, agd.name");

        foreach ($product_attribute_group_query->rows as $product_attribute_group) {
            $product_attribute_data = array();

            $product_attribute_query = $this->db->query("SELECT a.attribute_id, ad.name, pa.text FROM " . DB_PREFIX . "product_attribute pa LEFT JOIN " . DB_PREFIX . "attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE pa.deleted != '1' AND pa.product_id = '" . (int) $product_id . "' AND a.attribute_group_id = '" . (int) $product_attribute_group['attribute_group_id'] . "' AND ad.language_id = '" . (int) $this->config->get('config_language_id') . "' AND pa.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY a.sort_order, ad.name");

            foreach ($product_attribute_query->rows as $product_attribute) {
                $product_attribute_data[] = array(
                    'attribute_id' => $product_attribute['attribute_id'],
                    'name' => $product_attribute['name'],
                    'text' => $product_attribute['text']
                );
            }

            $product_attribute_group_data[] = array(
                'attribute_group_id' => $product_attribute_group['attribute_group_id'],
                'name' => $product_attribute_group['name'],
                'attribute' => $product_attribute_data
            );
        }

        return $product_attribute_group_data;
    }

    public function getProductOptions($product_id) {
        $product_option_data = array();

        $product_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.deleted != '1' AND po.product_id = '" . (int) $product_id . "' AND od.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY o.sort_order");

        foreach ($product_option_query->rows as $product_option) {
            $product_option_value_data = array();

            $product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.deleted != '1' AND  pov.product_id = '" . (int) $product_id . "' AND pov.product_option_id = '" . (int) $product_option['product_option_id'] . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY ov.sort_order");
            foreach ($product_option_value_query->rows as $product_option_value) {
                $product_option_value_data[] = array(
                    'product_option_value_id' => $product_option_value['product_option_value_id'],
                    'option_value_id' => $product_option_value['option_value_id'],
                    'name' => $product_option_value['name'],
                    'image' => $product_option_value['image'],
                    'quantity' => $product_option_value['quantity'],
                    'subtract' => $product_option_value['subtract'],
                    'price' => $product_option_value['price'],
                    'price_prefix' => $product_option_value['price_prefix'],
                    'weight' => $product_option_value['weight'],
                    'weight_prefix' => $product_option_value['weight_prefix'],
                );
            }

            $product_option_data[] = array(
                'product_option_id' => $product_option['product_option_id'],
                'product_option_value' => $product_option_value_data,
                'option_id' => $product_option['option_id'],
                'name' => $product_option['front_name'],
                'type' => $product_option['type'],
                'value' => $product_option['value'],
                'required' => $product_option['required']
            );
        }
        return $product_option_data;
    }

    public function getProductDiscounts($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int) $product_id . "' AND customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND quantity > 1 AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity ASC, priority ASC, price ASC");

        return $query->rows;
    }

    public function getProductImages($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

    public function getProductRelated($product_id) {
        $product_data = array();

        $query = $this->db->query("SELECT pr.related_id FROM " . DB_PREFIX . "product_related pr LEFT JOIN " . DB_PREFIX . "product p ON (pr.related_id = p.product_id) WHERE pr.product_id = '" . (int) $product_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p.date_available<=NOW() ORDER BY p.date_available LIMIT 4 ");

        foreach ($query->rows as $result) {
            $product_data[$result['related_id']] = $this->getProduct($result['related_id']);
        }

        return $product_data;
    }

    public function getProductColor($product_id) {
        $product_data = array();

        $query = $this->db->query("SELECT pr.related_id FROM " . DB_PREFIX . "product_color pr LEFT JOIN " . DB_PREFIX . "product p ON (pr.related_id = p.product_id) WHERE pr.product_id = '" . (int) $product_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p.date_available<=NOW() ORDER BY p.date_available LIMIT 4 ");

        foreach ($query->rows as $result) {
            $product_data[$result['related_id']] = $this->getProduct($result['related_id']);
        }

        return $product_data;
    }

    public function getProductLayoutId($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int) $product_id . "' AND store_id = '" . (int) $this->config->get('config_store_id') . "'");

        if ($query->num_rows) {
            return $query->row['layout_id'];
        } else {
            return 0;
        }
    }

    public function getCategories($product_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");

        return $query->rows;
    }

    public function getTotalProducts($data = array()) {
        $cache = "product.gettotalproducts." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = null; //$this->cache->get($cache);

        if (!$result) {
            $sql = "SELECT COUNT(DISTINCT p.product_id) AS total";

            if (!empty($data['filter_category_id'])) {
                if (!empty($data['filter_sub_category'])) {
                    $sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
                } else {
                    $sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
                }

                if (!empty($data['filter_filter'])) {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
                } else {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
                }
            } else {
                $sql .= " FROM " . DB_PREFIX . "product p";
            }

            $sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN store s ON(p2s.store_id=s.store_id) LEFT JOIN zone z ON(s.zone_id=z.zone_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND z.country_id = '" . (int) $this->country->getId() . "'";

            if (!empty($data['filter_category_id'])) {
                if (!empty($data['filter_sub_category'])) {
                    $sql .= " AND cp.path_id = '" . (int) $data['filter_category_id'] . "'";
                } else {
                    $sql .= " AND p2c.category_id = '" . (int) $data['filter_category_id'] . "'";
                }

                if (!empty($data['filter_filter'])) {
                    $implode = array();

                    $filters = explode(',', $data['filter_filter']);

                    foreach ($filters as $filter_id) {
                        $implode[] = (int) $filter_id;
                    }

                    $sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
                }
            }

            if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
                $sql .= " AND (";

                if (!empty($data['filter_name'])) {
                    $implode = array();

                    $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

                    foreach ($words as $word) {
                        $implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
                    }

                    if ($implode) {
                        $sql .= " " . implode(" AND ", $implode) . "";
                    }

                    if (!empty($data['filter_description'])) {
                        $sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
                    }
                }

                if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                    $sql .= " OR ";
                }

                if (!empty($data['filter_tag'])) {
                    $sql .= "pd.tag LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_tag'])) . "%'";
                }

                if (!empty($data['filter_name'])) {
                    $sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                }

                $sql .= ")";
            }

            if (!empty($data['filter_manufacturer_id'])) {
                $sql .= " AND p.manufacturer_id = '" . (int) $data['filter_manufacturer_id'] . "'";
            }

            if (!empty($data['filter_brand_id'])) {
                $sql .= " AND p.brand_id='" . (int) $data['filter_brand_id'] . "' ";
            }
            $query = $this->db->query($sql);

            return $query->row['total'];
        } else {
            return $result;
        }
    }

     public function getTotalProductsForAlgolia($data = array()) {

        $cache = "product.gettotalproductsforalgolia." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $sql = "SELECT COUNT(DISTINCT p.product_id) AS total";

            if (!empty($data['filter_category_id'])) {
                if (!empty($data['filter_sub_category'])) {
                    $sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
                } else {
                    $sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
                }

                if (!empty($data['filter_filter'])) {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
                } else {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
                }
            } else {
                $sql .= " FROM " . DB_PREFIX . "product p";
            }

            $sql .= " INNER JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) INNER JOIN brand b ON(p.brand_id=b.brand_id) INNER JOIN customerpartner_to_product cp2p ON(p.product_id=cp2p.product_id) INNER JOIN customerpartner_to_customer cp2c ON(cp2p.customer_id=cp2c.customer_id) WHERE cp2c.available_country IN(0,'" . (int) $this->country->getId() . "') AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW()  ";

            if (!empty($data['filter_category_id'])) {
                if (!empty($data['filter_sub_category'])) {
                    $sql .= " AND cp.path_id = '" . (int) $data['filter_category_id'] . "'";
                } else {
                    $sql .= " AND p2c.category_id = '" . (int) $data['filter_category_id'] . "'";
                }

                if (!empty($data['filter_filter'])) {
                    $implode = array();

                    $filters = explode(',', $data['filter_filter']);

                    foreach ($filters as $filter_id) {
                        $implode[] = (int) $filter_id;
                    }

                    $sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
                }
            }

            if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
                $sql .= " AND (";

                if (!empty($data['filter_name'])) {
                    $implode = array();

                    $words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

                    foreach ($words as $word) {
                        $implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
                    }

                    if ($implode) {
                        $sql .= " " . implode(" AND ", $implode) . "";
                    }

                    if (!empty($data['filter_description'])) {
                        $sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
                    }
                }

                if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
                    $sql .= " OR ";
                }

                if (!empty($data['filter_tag'])) {
                    $sql .= "pd.tag LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_tag'])) . "%'";
                }

                if (!empty($data['filter_name'])) {
                    $sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(otp.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                    $sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
                }

                $sql .= ")";
            }

            if (!empty($data['filter_manufacturer_id'])) {
                $sql .= " AND p.manufacturer_id = '" . (int) $data['filter_manufacturer_id'] . "'";
            }

            if (!empty($data['filter_brand_id'])) {
                $sql .= " AND p.brand_id='" . (int) $data['filter_brand_id'] . "' ";
            }
            $sql.=' AND p.is_algolia=1 AND b.is_algolia=1 ';
            $query = $this->db->query($sql);
//die($sql);
            $this->cache->set($cache, $query->row['total']);
            return $query->row['total'];
        } else {
            return $result;
        }
    }

    public function getProfile($product_id, $recurring_id) {
        return $this->db->query("SELECT * FROM `" . DB_PREFIX . "recurring` `p` JOIN `" . DB_PREFIX . "product_recurring` `pp` ON `pp`.`recurring_id` = `p`.`recurring_id` AND `pp`.`product_id` = " . (int) $product_id . " WHERE `pp`.`recurring_id` = " . (int) $recurring_id . " AND `status` = 1 AND `pp`.`customer_group_id` = " . (int) $this->config->get('config_customer_group_id'))->row;
    }

    public function getProfiles($product_id) {
        return $this->db->query("SELECT `pd`.* FROM `" . DB_PREFIX . "product_recurring` `pp` JOIN `" . DB_PREFIX . "recurring_description` `pd` ON `pd`.`language_id` = " . (int) $this->config->get('config_language_id') . " AND `pd`.`recurring_id` = `pp`.`recurring_id` JOIN `" . DB_PREFIX . "recurring` `p` ON `p`.`recurring_id` = `pd`.`recurring_id` WHERE `product_id` = " . (int) $product_id . " AND `status` = 1 AND `customer_group_id` = " . (int) $this->config->get('config_customer_group_id') . " ORDER BY `sort_order` ASC")->rows;
    }

    public function getTotalProductSpecials() {
        $query = $this->db->query("SELECT COUNT(DISTINCT ps.product_id) AS total FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND ps.deleted='no' AND ps.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()))");

        if (isset($query->row['total'])) {
            return $query->row['total'];
        } else {
            return 0;
        }
    }

    public function gethowToWareProduct($product_id) {
        $how_to_ware_product = array();

        $query = $this->db->query(" SELECT w.wear_id, w.sort_order, d1.name, p.image "
                . " FROM " . DB_PREFIX . "product_wear w "
                . " INNER JOIN " . DB_PREFIX . "product p ON (w.wear_id = p.product_id) "
                . " INNER JOIN " . DB_PREFIX . "product_description d1 ON (w.wear_id = d1.product_id) "
                . " WHERE w.product_id = '" . (int) $product_id . "' "
                . " AND p.status = '1' "
                . " AND d1.language_id = '" . (int) $this->config->get('config_language_id') . "' "
                . " ORDER BY w.sort_order ASC");

        foreach ($query->rows as $result) {
            $how_to_ware_product[] = array('ware_id' => $result['wear_id'], 'name' => $result['name'], 'image' => $result['image'], 'sort_order' => $result['sort_order']);
        }

        return $how_to_ware_product;
    }

    public function getProductShops($product_id) {
        $shops = array();

        $query = $this->db->query(" SELECT s.store_id, sd.name filter, CONCAT(sd.name,' - ',md.name) as name FROM " . DB_PREFIX . "store s INNER JOIN " . DB_PREFIX . "store_description sd ON (s.store_id = sd.store_id) INNER JOIN " . DB_PREFIX . "mall m ON (s.mall_id = m.mall_id) INNER JOIN " . DB_PREFIX . "mall_description md ON (m.mall_id = md.mall_id) INNER JOIN " . DB_PREFIX . "product_to_store p2s ON (s.store_id = p2s.store_id) INNER JOIN " . DB_PREFIX . "zone z ON (s.zone_id = z.zone_id) WHERE p2s.product_id = '" . (int) $product_id . "' AND s.status = '1' AND z.country_id = '" . (int) $this->country->getid() . "' AND sd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND m.status = '1' AND md.language_id = '" . (int) $this->config->get('config_language_id') . "' ");

        foreach ($query->rows as $result) {
            $shops[] = array(
                'shop_id' => $result['store_id'],
                'name' => $result['name'],
                'filter' => $result['filter']
            );
        }

        return $shops;
    }

    public function getCustomerProducts($data = array()) {
        $cache = "product.getcustomerproducts." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $sql = "SELECT cp2p.product_id FROM " . DB_PREFIX . "customerpartner_to_product cp2p LEFT JOIN " . DB_PREFIX . "product p ON (cp2p.product_id=p.product_id)";
            $where = "WHERE p.status=1";

            if (!empty($data['filter_category_id'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp2p.product_id = p2c.product_id)";
                $where .= " AND p2c.category_id='" . $this->db->escape($data['filter_category_id']) . "'";
            }


            if (!empty($data['filter_main_category_id'])) {
                if (!empty($data['filter_category_id'])) {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "category_path cp ON (p2c.category_id = cp.category_id and level ='0') ";
                } else {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp2p.product_id = p2c.product_id)  LEFT JOIN " . DB_PREFIX . "category_path cp ON (p2c.category_id = cp.category_id and cp.level ='0') ";
                }
                $where .= " AND cp.path_id='" . $this->db->escape($data['filter_main_category_id']) . "'";
            }

            if (!empty($data['filter_filter'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (cp2p.product_id = pd.product_id )";
                $where .= " AND pd.name like '%" . $this->db->escape($data['filter_filter']) . "%' AND pd.language_id ='" . (int) $this->config->get('config_language_id') . "'";
            }


            if (!empty($data['customer_id'])) {
                $where .= " AND cp2p.customer_id = '" . (int) $data['customer_id'] . "' ";
            }

            $sql .= $where;
            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }
            $query = $this->db->query($sql);
            $result = array();
            foreach ($query->rows as $row) {
                $result[$row['product_id']] = $this->getProduct($row['product_id']);
            }
            if (!empty($result)) {
                $this->cache->set($cache, $result);
            }
            return $result;
        } else {
            return $result;
        }
    }

    public function getCustomerProductCategories($data = array()) {
        $cache = "product.getcustomerproductcategories." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $sql = "SELECT DISTINCT c.category_id,cp.path_id as group_id, cd.name FROM " . DB_PREFIX . "customerpartner_to_product cp2p LEFT JOIN " . DB_PREFIX . "product p ON (cp2p.product_id=p.product_id) LEFT JOIN product_description pd on (cp2p.product_id = pd.product_id) LEFT JOIN product_to_category p2c ON(p.product_id=p2c.product_id) LEFT JOIN category c ON(p2c.category_id=c.category_id) LEFT JOIN category_description cd ON(c.category_id=cd.category_id) LEFT JOIN category_path cp ON(c.category_id=cp.category_id and cp.level=0) WHERE p.status='1' AND c.status='1' AND cd.language_id='" . (int) $this->config->get('config_language_id') . "' ";
            if (!empty($data['customer_id'])) {
                $sql.= " AND cp2p.customer_id = '" . (int) $data['customer_id'] . "' ";
            }
            if (!empty($data['group_id']) && $data['group_id'] != 0) {
                $sql.= " AND cp.path_id = '" . (int) $data['group_id'] . "' ";
            }

            if (!empty($data['filter_filter']) && $data['filter_filter'] != '') {
                $sql.= " AND pd.name like '%" . $data['filter_filter'] . "%' ";
            }

            $query = $this->db->query($sql);
            if ($query->num_rows) {
                $this->cache->set($cache, $query->rows);
            }
            return $query->rows;
        } else {
            return $result;
        }
    }

    public function getCustomerProductMainCategories($data = array()) {
        $cache = "product.getCustomerProductMainCategories." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $query = $this->db->query("SELECT DISTINCT category_id,name FROM " . DB_PREFIX . "category_description where category_id in (" . $data['main_cat'] . ") AND language_id='" . (int) $this->config->get('config_language_id') . "'");
            if ($query->num_rows) {
                $this->cache->set($cache, $query->rows);
            }
            return $query->rows;
        } else {
            return $result;
        }
    }

    public function getSellerData($product_id) {
        //$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customerpartner_to_product where product_id ='" . (int) $product_id . "'");
        $query = $this->db->query(" SELECT cp2p.product_id, cp2cd.screenname, up.keyword AS keyword, cp2p.customer_id AS seller_id  FROM " . DB_PREFIX . "customerpartner_to_product cp2p  LEFT JOIN customerpartner_to_customer_description cp2cd ON (cp2p.customer_id = cp2cd.customer_id) LEFT JOIN url_partner up ON (up.query=CONCAT('partner_id=',cp2p.customer_id)) WHERE product_id ='" . (int) $product_id . "' AND cp2cd.language_id='" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getBrandProducts($data = array()) {
        $cache = "product.getbrandproducts." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $sql = "SELECT p.product_id FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "brand b ON (p.brand_id=b.brand_id) ";

            $where = "WHERE p.status=1";
            if ($data['is_shopable']) {
                $where = "WHERE p.status=1 AND p.stock_status_id <> 10";
            }

            if (!empty($data['filter_category_id'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
                $where .= " AND p2c.category_id='" . $this->db->escape($data['filter_category_id']) . "'";
            }


            if (!empty($data['filter_main_category_id'])) {
                if (!empty($data['filter_category_id'])) {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "category_path cp ON (p2c.category_id = cp.category_id and level ='0') ";
                } else {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)  LEFT JOIN " . DB_PREFIX . "category_path cp ON (p2c.category_id = cp.category_id and cp.level ='0') ";
                }
                $where .= " AND cp.path_id='" . $this->db->escape($data['filter_main_category_id']) . "'";
            }

            if (!empty($data['filter_shop_now']) && $data['filter_shop_now'] != 10) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "customerpartner_to_product cp2p ON (p.product_id = cp2p.product_id)  LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer cp2c ON (cp2p.customer_id = cp2c.customer_id)";
                $where .= " AND p.stock_status_id <> 10";
            }

            if (!empty($data['filter_brand_id'])) {
                $where .= " AND p.brand_id='" . $this->db->escape($data['filter_brand_id']) . "'";
            }


            $sql .= $where;
            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }
            $query = $this->db->query($sql);
            $result = array();
            foreach ($query->rows as $row) {
                $result[$row['product_id']] = $this->getProduct($row['product_id']);
            }
            if (!empty($result)) {
                $this->cache->set($cache, $result);
            }
            return $result;
        } else {
            return $result;
        }
    }

    public function grtTotalBrandProduct($data = array()) {
        $cache = "product.gettotalbrandproduct." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $sql = "SELECT COUNT(p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "brand b ON (p.brand_id=b.brand_id) ";
            $where = "WHERE p.status=1";
            if ($data['is_shopable']) {
                $where = "WHERE p.status=1 AND p.stock_status_id <> 10";
            }

            if (!empty($data['filter_category_id'])) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";
                $where .= " AND p2c.category_id='" . $this->db->escape($data['filter_category_id']) . "'";
            }

            if (!empty($data['filter_shop_now']) && $data['filter_shop_now'] != 10) {
                $sql .= " LEFT JOIN " . DB_PREFIX . "customerpartner_to_product cp2p ON (p.product_id = cp2p.product_id)  LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer cp2c ON (cp2p.customer_id = cp2c.customer_id)";
                $where .= " AND p.stock_status_id <> 10";
            }


            if (!empty($data['filter_main_category_id'])) {
                if (!empty($data['filter_category_id'])) {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "category_path cp ON (p2c.category_id = cp.category_id and level ='0') ";
                } else {
                    $sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)  LEFT JOIN " . DB_PREFIX . "category_path cp ON (p2c.category_id = cp.category_id and cp.level ='0') ";
                }
                $where .= " AND cp.path_id='" . $this->db->escape($data['filter_main_category_id']) . "'";
            }

            if (!empty($data['filter_brand_id'])) {
                $where .= " AND p.brand_id='" . $this->db->escape($data['filter_brand_id']) . "'";
            }


            $sql .= $where;

            $query = $this->db->query($sql);

            if (!empty($result)) {
                $this->cache->set($cache, $query->row['total']);
            }
            return $query->row['total'];
        } else {
            return $result;
        }
    }

    public function getAvailableCountries($product_id) {
        $result = array();
        $allcountries = false;
        $data = $this->db->query("SELECT cp2c.available_country FROM customerpartner_to_customer INNER JOIN customerpartner_to_product cp2p ON(cp2c.customer_id=cp2p.customer_id) INNER JOIN product p ON(cp2p.product_id=p.product_id) WHERE p.status=1 AND p.product_id =  '" . (int) $product_id . "' ");
        foreach ($data->rows as $row) {
            if ($row['available_country'] == 0) {
                $allcountries = true;
            }
        }
        if ($allcountries === true) {
            $query = $this->db->query("SELECT cd.name FROM country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE c.available='1' AND cd.language_id='" . (int) $this->config->get('config_language_id') . "' ");
        } else {
            $query = $this->db->query("SELECT cd.name FROM country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) LEFT JOIN customerpartner_to_customer cp2c ON(cp2c.available_country=c.country_id) LEFT JOIN customerpartner_to_product cp2p ON(cp2c.customer_id=cp2p.customer_id) LEFT JOIN product p ON(cp2p.product_id=p.product_id) WHERE p.product_id='" . (int) $product_id . "' AND p.status=1 AND c.available='1' AND cd.language_id='" . (int) $this->config->get('config_language_id') . "' ");
        }
        return $query->rows;
    }

    public function getInCartProducts($customer_id) {
        $query = $this->db->query("SELECT product_id from cart WHERE customer_id='" . (int) $customer_id . "'");
        $return_array = array();
        foreach ($query->rows as $product) {
            $return_array [] = $product['product_id'];
        }
        return $return_array;
    }

    public function getOptions($data){
      $sql = "SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) LEFT JOIN " . DB_PREFIX . "option_to_category o2c  ON (o.option_id=o2c.option_id)  WHERE od.language_id = '" . (int) $this->config->get('config_language_id') . "'";

      if (isset($data['filter_name']) && !is_null($data['filter_name'])) {
          $sql .= " AND od.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
      }

      if (isset($data['category_id']) && !is_null($data['category_id'])) {
          $sql .= " AND o2c.category_id = '" .(int) $data['category_id'] . "'";
      }

      $sort_data = array(
          'od.name',
          'o.type',
          'o.sort_order'
      );

      if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
          $sql .= " ORDER BY " . $data['sort'];
      } else {
          $sql .= " ORDER BY od.name";
      }

      if (isset($data['order']) && ($data['order'] == 'DESC')) {
          $sql .= " DESC";
      } else {
          $sql .= " ASC";
      }

      if (isset($data['start']) || isset($data['limit'])) {
          if ($data['start'] < 0) {
              $data['start'] = 0;
          }

          if ($data['limit'] < 1) {
              $data['limit'] = 20;
          }

          $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
      }
      $query = $this->db->query($sql);

      return $query->rows;
    }

    public function getEnabledSize($option_id){
        $sizes = [
            'is_uk'=>1,
            'is_us'=>0,
            'is_eur'=>0,
            'is_xmls'=>0,
            'is_it'=>0
        ];

        $query = $this->db->query("select * from dynamic_values where code='option_option_size_".(int)$option_id."' AND `key` IN ('is_uk','is_us','is_eur','is_xmls','is_it');");
        if(isset($query) && isset($query->rows)){
            foreach ($query->rows as $row){
                if(isset($sizes[$row['key']])){
                    $sizes[$row['key']] =  $row['value'];
                }
            }
        }
        return $sizes;
    }

    public function getSizesForOptionId($id){
        $id = (int)$id;
        $query = $this->db->query("SELECT * FROM option_to_size where option_value_id = '$id'");
        return $query->row;
    }

    public function getSizes($option_id){
        $option_id = (int)$option_id;
        $query = $this->db->query("SELECT o.option_id, o.type, ov.option_value_id,ov.sort_order ,ots.uk, ots.us, ots.eur, ots.xmls, ots.it FROM `option` o LEFT JOIN option_value ov ON (o.option_id = ov.option_id) LEFT JOIN option_to_size ots ON ( ots.option_value_id = ov.option_value_id ) WHERE o.type = 'size' && o.option_id = ".$option_id);
        if(isset($query->rows) && count($query->rows)>0){
            $outout = [];
            foreach ($query->rows as $row){
                $outout[$row['option_value_id']] = [
                    'sort_order'=>$row['sort_order'],
                    'uk'=>$row['uk'],
                    'us'=>$row['us'],
                    'eur'=>$row['eur'],
                    'xmls'=>$row['xmls'],
                    'it'=>$row['it'],
                ];
            }
            return $outout;
        }
        return array();
    }

}
