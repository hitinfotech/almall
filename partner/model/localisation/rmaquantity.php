<?php

class ModelLocalisationRmaQuantity extends Model {

    // order products should be array of product_id, order_product_id in each row
    // wk_rma_order is the rma order_id 
    // order_id is the order ID

    public function ChangeProductOrderQuantity($wk_rma_order_id) {

        $wk_rma_order_info = $this->db->query(" SELECT * FROM wk_rma_order WHERE id='" . (int) $wk_rma_order_id . "' ");

        if ($wk_rma_order_info->num_rows) {

            $order_id = $wk_rma_order_info->row['order_id'];

            $new_rma_order_status_info = $this->db->query(" SELECT * FROM rma_status WHERE rma_status_id = '" . (int) $wk_rma_order_info->row['customer_status_id'] . "'");

            if ($new_rma_order_status_info->num_rows > 0 && ((int) $new_rma_order_status_info->row['order_status_id']) > 0 ) {
                
                $new_rma_order_status_id = $new_rma_order_status_info->row['order_status_id'];

                $products = $this->db->query(" SELECT  * from wk_rma_product WHERE rma_id= '" . (int) $wk_rma_order_id . "' ");

                foreach ($products->rows as $row) {

                    $product_id = $row['product_id'];

                    $quantity = $row['quantity'];

                    $order_product_id = $row['order_product_id'];

                    $customerpartner_to_order_info = $this->db->query(" SELECT order_product_status FROM customerpartner_to_order WHERE deleted!='1' AND order_id='" . (int) $order_id . "' AND product_id = '" . (int) $product_id . "' AND order_product_id='" . (int) $order_product_id . "' ");

                    if ($customerpartner_to_order_info->num_rows) {

                        $product_info = $this->db->query("SELECT * FROM product WHERE product_id = '" . (int) $product_id . "' ");
                        $product_name = $this->db->query("SELECT * FROM product_description WHERE language_id = '1' AND product_id = '" . (int) $product_id . "' ");

                        $order_status = $this->db->query("SELECT cp2o.quantity, cp2o.order_product_status, os.quantity status_quantity, os.name status_name FROM " . DB_PREFIX . "customerpartner_to_order cp2o LEFT JOIN order_status os ON(cp2o.order_product_status=os.order_status_id) WHERE os.language_id='1' AND cp2o.order_id = '" . (int) $order_id . "' AND cp2o.product_id = '" . (int) $product_id . "' AND cp2o.order_product_id = '" . (int) $order_product_id . "'");

                        $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_order SET order_product_status = '" . (int) $new_rma_order_status_id . "' WHERE order_id = '" . (int) $order_id . "' AND product_id = '" . (int) $product_id . "' AND order_product_id = '" . $order_product_id . "'");
                        
                        // add history
                        $this->db->query("INSERT INTO customerpartner_to_order_status SET order_id='" . (int) $order_id . "', order_status_id = '" . (int) $new_rma_order_status_id . "', comment='".$this->db->escape($product_name->row['name'])."(product:".(int)$product_id.", order product:". (int)$order_product_id .")"." has RMA with RMA ID # ". (int)$wk_rma_order_id." status has been changed to ".$this->db->escape($new_rma_order_status_info->row['name'])."', date_added=NOW(), user_id='". (int)$this->user->getid() ."', customer_id='". (int) $order_status->row['customer_id'] ."', product_id = '" . (int) $product_id . "'");
                        $this->db->query("INSERT INTO order_history                   SET order_id='" . (int) $order_id . "', order_status_id = '" . (int) $new_rma_order_status_id . "', comment='".$this->db->escape($product_name->row['name'])."(product:".(int)$product_id.", order product:". (int)$order_product_id .")"." has RMA with RMA ID # ". (int)$wk_rma_order_id." status has been changed to ".$this->db->escape($new_rma_order_status_info->row['name'])."', date_added=NOW(), user_id='". (int)$this->user->getid() ."' ");

                        $new_order_status = $this->db->query("SELECT cp2o.quantity, cp2o.order_product_status, os.quantity status_quantity, os.name status_name FROM " . DB_PREFIX . "customerpartner_to_order cp2o LEFT JOIN order_status os ON(cp2o.order_product_status=os.order_status_id) WHERE os.language_id='1' AND cp2o.order_id = '" . (int) $order_id . "' AND cp2o.product_id = '" . (int) $product_id . "' AND cp2o.order_product_id = '" . (int) $order_product_id . "'");

                        if ($order_status->row['status_quantity'] != $new_order_status->row['status_quantity']) {

                            if ($order_status->row['status_quantity'] == 'subtract' && $new_order_status->row['status_quantity'] == 'add') {
                                $this->db->query("UPDATE product SET quantity= (quantity + " . (int) $quantity . ") WHERE product_id='" . (int) $product_id . "'");
                                $option_info = $this->db->query(" SELECT * FROM order_option oo WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . (int) $order_product_id . "'");
                                if ($option_info->num_rows) {
                                    foreach ($option_info->rows as $option) {
                                        $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int) $quantity . ") WHERE product_option_value_id = '" . (int) $option['product_option_value_id'] . "' AND subtract = '1'");
                                    }
                                }
                            }

                            if ($order_status->row['status_quantity'] == 'add' && $new_order_status->row['status_quantity'] == 'subtract') {
                                $this->db->query("UPDATE product SET quantity= (quantity - " . (int) $quantity . ") WHERE product_id='" . (int) $product_id . "'");
                                $option_info = $this->db->query(" SELECT * FROM order_option oo WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . (int) $order_product_id . "'");
                                if ($option_info->num_rows) {
                                    foreach ($option_info->rows as $option) {
                                        $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . (int) $quantity . ") WHERE product_option_value_id = '" . (int) $option['product_option_value_id'] . "' AND subtract = '1'");
                                    }
                                }
                            }

                            if ($new_order_status->row['status_quantity'] == 'purge') {
                                $option_info = $this->db->query(" SELECT * FROM order_option oo WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . (int) $order_product_id . "'");
                                if ($option_info->num_rows) {
                                    foreach ($option_info->rows as $option) {
                                        $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = 0 WHERE product_option_value_id = '" . (int) $option['product_option_value_id'] . "' AND subtract = '1'");
                                    }
                                    $this->db->query("UPDATE product p SET p.quantity = (SELECT sum(quantity) FROM product_option_value pov WHERE pov.deleted != '1' AND  pov.product_id=p.product_id GROUP BY option_id LIMIT 1) WHERE p.product_id = '" . (int) $product_id . "'");
                                } else {
                                    $this->db->query("UPDATE product SET quantity= 0 WHERE product_id='" . (int) $product_id . "'");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
