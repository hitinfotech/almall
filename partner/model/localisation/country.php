<?php

class ModelLocalisationCountry extends Model {

    public function getCountry($country_id) {

        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE cd.language_id = '" . $this->config->get('config_language_id') . "' AND c.country_id = '" . (int) $country_id . "' AND c.status = '1' AND c.isvirtual='0' ");

        return $query->row;
    }

    public function getCountries() {
        $cache = 'country.getcountries' . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id');

        $country_data = $this->cache->get($cache);

        if (!$country_data) {

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE cd.language_id = '" . $this->config->get('config_language_id') . "' AND c.status = '1' AND c.isvirtual=0 ORDER BY c.sort_order ASC, name ASC ");

            $country_data = $query->rows;

            $this->cache->set($cache, $country_data);

            return $country_data;
        }

        return $country_data;
    }

    public function getAvailableCountries($data = array()) {

        $cache = 'country.getavailablecountries.' . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id');

        $country_data = $this->cache->get($cache);

        if (!$country_data) {

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country c LEFT JOIN country_description cd ON(c.country_id = cd.country_id) WHERE cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c.available = '1' AND c.isvirtual = '0' ORDER BY c.sort_order ASC, cd.name ASC");

            $country_data = $query->rows;

            $this->cache->set($cache, $country_data);

            return $country_data;
        }

        $this->cache->delete("catalog.country.1", $country_data);

        return $country_data;
    }

    public function get_IDS_AvailableCountries() {

        $cache = 'country.ids.getavailablecountries.' . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id');

        $country_data = $this->cache->get($cache);

        if (!$country_data) {

            $query = $this->db->query("SELECT country_id FROM " . DB_PREFIX . "country c WHERE available = '1' AND c.isvirtual=0 ");

            $country_data = array();

            foreach ($query->rows as $row) {
                $country_data[] = $row['country_id'];
            }

            $this->cache->set($cache, $country_data);

            return $country_data;
        }

        return $country_data;
    }

    //This funtion for the API getCountries request.
    public function getDeatailedCountries($language_id) {
        $cache = 'country.ids.getCountriesNames.' . (int) $language_id;
        $country_data = $this->cache->get($cache);

        if (!$country_data) {
            $query = $this->db->query("SELECT c.country_id,cd.name FROM " . DB_PREFIX . "country c LEFT JOIN country_description cd ON(c.country_id = cd.country_id) WHERE cd.language_id ='" . (int) $language_id . "' AND c.status=0 AND c.isvirtual=0 ORDER BY c.sort_order, name ");

            $country_data = array();
            foreach ($query->rows as $key => $row) {
                $country_data[$key]['country_id'] = $row['country_id'];
                $country_data[$key]['name'] = $row['name'];
                $country_data[$key]['cities'] = $this->db->query("SELECT z.zone_id,zd.name FROM " . DB_PREFIX . "zone z LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE zd.language_id='" . (int) $language_id . "' AND z.country_id = '" . (int) $row['country_id'] . "' AND status = '1' ORDER BY zd.name ")->rows;
            }

            $this->cache->set($cache, $country_data);
            return $country_data;
        }

        return $country_data;
    }

    public function getHeaderCountries($data = array()) {

        $cache = 'country.getheadercountries.' . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id');

        $country_data = $this->cache->get($cache);

        if (!$country_data) {

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country c LEFT JOIN country_description cd ON(c.country_id = cd.country_id) WHERE cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c.available = '1' ORDER BY c.sort_order ASC, cd.name ASC");

            $country_data = $query->rows;
            $country_data[''] = array(
                'country_id' => 0,
                'language' => 'en',
                'curcode' => 'USD',
                'telecode' => '',
                'iso_code_2' => '',
                'iso_code_3' => '',
                'address_format' => '',
                'postcode_required' => 0,
                'status' => 1,
                'available' => 1,
                'flag' => 'flags/globe.png',
                'sort_order' => 20,
                'isvirtual' => 1,
                'name' => 'WorldWide'
            );

            $this->cache->set($cache, $country_data);

            return $country_data;
        }

        $this->cache->delete("catalog.country.1", $country_data);

        return $country_data;
    }

}
