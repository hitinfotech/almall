<?php

class ModelLocalisationZone extends Model {

    public function getZone($zone_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone z LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE z.zone_id = '" . (int) $zone_id . "' AND status = '1'");

        return $query->row;
    }

    public function getZonesByCountryId($country_id) {
        $key = 'zone.getzonesbycountryid.' . (int) $country_id . '.' . (int) $this->config->get('config_language_id');

        $zone_data = $this->cache->get($key);
        // die("SELECT * FROM " . DB_PREFIX . "zone z LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE zd.language_id='". (int)$this->config->get('config_language_id') ."' AND z.country_id = '" . (int) $country_id . "' AND status = '1' ORDER BY zd.name");
        if (!$zone_data) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone z LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE zd.language_id='" . (int) $this->config->get('config_language_id') . "' AND z.country_id = '" . (int) $country_id . "' AND status = '1' ORDER BY zd.name ");

            $zone_data = $query->rows;
            if ($query->num_rows) {
                $this->cache->set($key, $zone_data);
            }
        }

        return $zone_data;
    }

}
