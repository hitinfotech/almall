<?php

class ModelLocalisationRmaStatus extends Model {

    public function getRmaStatus($rma_status_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "rma_status WHERE rma_status_id = '" . (int) $rma_status_id . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getRmaStatuses($data = array()) {
        if ($data) {
            $sql = "SELECT * FROM " . DB_PREFIX . "rma_status WHERE language_id = '" . (int) $this->config->get('config_language_id') . "' ";

            $sql .= " ORDER BY name";

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $rma_status_data = $this->cache->get('rma_status.' . (int) $this->config->get('config_language_id'));

            if (!$rma_status_data) {
                $query = $this->db->query("SELECT rma_status_id, name FROM " . DB_PREFIX . "rma_status WHERE language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY name");

                $rma_status_data = $query->rows;

                $this->cache->set('rma_status.' . (int) $this->config->get('config_language_id'), $rma_status_data);
            }

            return $rma_status_data;
        }
    }

    public function getRmaStatusDescriptions($rma_status_id) {
        $rma_status_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "rma_status WHERE rma_status_id = '" . (int) $rma_status_id . "'");

        foreach ($query->rows as $result) {
            $rma_status_data[$result['language_id']] = array('name' => $result['name']);
        }

        return $rma_status_data;
    }

    public function getTotalRmaStatuses() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "rma_status WHERE language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row['total'];
    }

}
