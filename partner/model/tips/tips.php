<?php

class ModelTipsTips extends Model {

    public function getTip($tip_id) {

        $sql = "SELECT * FROM " . DB_PREFIX . "tip t LEFT JOIN " . DB_PREFIX . "tip_description td ON (t.tip_id = td.tip_id) WHERE t.tip_id = '" . (int) $tip_id . "' AND td.language_id = '" . (int) $this->config->get('config_language_id') . "' AND t.status = '1' ";
        if ($this->config->get('config_language_id') == 1) {
            $sql .= " AND t.is_english = 1 ";
        } else {
            $sql .= " AND t.is_arabic = 1 ";
        }


        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getTipSection($tip_id) {
        $query = $this->db->query("SELECT tip_id,language_id,description,product_id,image,title,sort_order FROM " . DB_PREFIX . "tip_section WHERE tip_id = '" . (int) $tip_id . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "' ");
        return $query->rows;
    }

    public function getTotalTips($data = array()) {
        $cache = "tips.gettotaltips." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(http_build_query($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $sql = "SELECT COUNT(DISTINCT t.tip_id) as total "
                    . "FROM " . DB_PREFIX . "tip t "
                    . "LEFT JOIN " . DB_PREFIX . "tip_description td ON (t.tip_id = td.tip_id) "
                    . "LEFT JOIN " . DB_PREFIX . "tip_to_country ttc on (t.tip_id=ttc.tip_id) "
                    . "LEFT JOIN " . DB_PREFIX . "tip_to_group tg ON (t.tip_id = tg.tip_id) "
                    . "WHERE t.status = '1' "
                    . "AND td.language_id = '" . (int) $this->config->get('config_language_id') . "' "
                    . "AND ttc.country_id = '" . (int) $this->config->get('config_country_id') . "' ";

            if (!empty($data['filter_filter'])) {
                $sql .= " AND CONCAT(td.name, ' ', td.body) LIKE '" . $this->db->escape($data['filter_filter']) . "'";
            }
            if ($this->config->get('config_language_id') == 2) {
                $sql .= " AND t.is_arabic = 1 ";
            }
            if ($this->config->get('config_language_id') == 1) {
                $sql .= " AND t.is_english = 1 ";
            }

            if (!empty($data['filter_group_id'])) {
                $sql .= " AND tg.tip_group_id='" . (int) $data['filter_group_id'] . "' ";
            }



            $query = $this->db->query($sql);

            if (!empty($result)) {
                $this->cache->set($cache, $query->row['total']);
            }
            return $query->row['total'];
        } else {
            return $result;
        }
    }

    public function getTips($data = array()) {

        $cache = "tips.gettips." . (int) $this->country->getid() . "." . (int) $this->config->get('config_language_id') . "." . md5(serialize($data));

        $result = $this->cache->get($cache);

        if (!$result) {
            $sql = "SELECT DISTINCT t.tip_id,t.image,td.name, t.date_added, t.date_modified,td.body FROM " . DB_PREFIX . "tip t "
                    . "LEFT JOIN " . DB_PREFIX . "tip_description td ON (t.tip_id = td.tip_id) "
                    . "LEFT JOIN " . DB_PREFIX . "tip_to_country ttc on (t.tip_id=ttc.tip_id) "
                    . "LEFT JOIN " . DB_PREFIX . "tip_to_group tg ON (t.tip_id = tg.tip_id) "
                    . "WHERE t.status = '1' "
                    . "AND td.language_id = '" . (int) $this->config->get('config_language_id') . "' "
                    . "AND ttc.country_id = '" . (int) $this->country->getid() . "' ";

            if (!empty($data['filter_filter'])) {
                $sql .= " AND CONCAT(td.name, ' ', td.body) LIKE '%" . $this->db->escape($data['filter_filter']) . "%'";
            }
            if ($this->config->get('config_language_id') == 2) {
                $sql .= " AND t.is_arabic = 1 ";
            }
            if ($this->config->get('config_language_id') == 1) {
                $sql .= " AND t.is_english = 1 ";
            }
            if (!empty($data['filter_group_id'])) {
                $sql .= " AND tg.tip_group_id='" . (int) $data['filter_group_id'] . "' ";
            }

            $sort_data = array(
                'td.name',
                't.sort_order',
                't.date_added',
                't.date_modified'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                if ($data['sort'] == 'td.name') {
                    $sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
                } else {
                    $sql .= " ORDER BY " . $data['sort'];
                }
            } else {
                $sql .= " ORDER BY t.date_added";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC, LCASE(td.name) DESC";
            } else {
                $sql .= " ASC, LCASE(td.name) ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {

                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = $this->config->get('config_tip_limit');
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $query = $this->db->query($sql);

            $result = array();

            if (isset($query->rows) && !empty($query->rows)) {
                $result = $query->rows;
            }

            if (!empty($result)) {
                $this->cache->set($cache, $result);
            }

            return $result;
        } else {
            return $result;
        }
    }

    public function getOtherTips($data = array()) {
        // echo "<pre>"; die(var_dump($data));
        
        $sqlExtensionTable = $sqlGroupExtensionTable = $sqlExtensionWhere = $sqlGroupExtensionWhere = $sqlExecludedTipsExtensionWhere = $sExecludedTips = $sqlExtensionSort = $TipsInKeywords = $GroupByKeywords = '';
        if ($this->config->get('config_language_id') == 1) {
            $sqlExtensionWhere = " AND t.is_english=1 ";
        } else {
            $sqlExtensionWhere = " AND t.is_arabic=1 ";
        }
        if (!empty($data['filter_group_id'])) {
            $sqlGroupExtensionTable = " LEFT JOIN " . DB_PREFIX . " tip_to_group tg ON (t.tip_id = tg.tip_id) ";
            $sqlGroupExtensionWhere = " AND tg.tip_group_id='" . (int) $data['filter_group_id'] . "' ";
        }

        if (isset($data['order_by']) && !empty($data['order_by'])) {
            $sqlExtensionSort = " ORDER BY {$data['order_by']} DESC ";
        } else {
            $sqlExtensionSort = " ORDER BY t.sort_order DESC ";
        }

        if (isset($data['limit']) && !empty($data['limit'])) {
            $limit = $data['limit'];
        } else {
            $limit = 4;
        }

        if (isset($data['tips']) && !empty($data['tips'])) {
            $sExecludedTips = implode(",", $data['tips']);
            if ($sExecludedTips != '') {
                $sqlExecludedTipsExtensionWhere = " AND t.tip_id NOT IN ({$sExecludedTips}) ";
            }
        }

        if (isset($data['keyword_id']) && !empty($data['keyword_id'])) {
            $TipsInKeywords = implode(",", $data['keyword_id']);
            if ($TipsInKeywords != '') {
                $TipsInKeywords = " AND tk.keyword_id IN ($TipsInKeywords) ";
                $GroupByKeywords = 'GROUP BY td.tip_id';
            }
        }

        $sql = "SELECT t.tip_id,t.image,t.date_modified,td.name,td.body
				FROM " . DB_PREFIX . "tip t
				LEFT JOIN " . DB_PREFIX . "tip_description td ON (t.tip_id = td.tip_id)
        LEFT JOIN " . DB_PREFIX . "tip_to_country ttc on (t.tip_id=ttc.tip_id)
        LEFT JOIN " . DB_PREFIX . "tips_keyword tk on (t.tip_id=tk.tip_id)
				{$sqlGroupExtensionTable}
				WHERE t.status = '1' 
        $TipsInKeywords
        AND t.tip_id != " . $data['tip_id'] . "
				AND td.language_id = '" . (int) $this->config->get('config_language_id') . "'
				AND ttc.country_id = '" . (int) $this->config->get('config_country_id') . "'
                                {$sqlExtensionWhere}
				{$sqlGroupExtensionWhere}
				{$sqlExecludedTipsExtensionWhere}
        $GroupByKeywords
				{$sqlExtensionSort}
				LIMIT $limit ";
//echo $sql;
        //echo "<pre>"; die(print_r($sql));
        $query = $this->db->query($sql);

        $result = array();

        // echo "<pre>"; die(print_r($query->rows));

        if (isset($query->rows) && !empty($query->rows)) {
            $result = $query->rows;
        }

        return $result;
    }

    public function getTipProducts($tip_id) {
        // die("SELECT product_id FROM tip_product WHERE tip_id = '". (int)$tip_id ."' ORDER BY sort_order ");
        $query = $this->db->query("SELECT product_id FROM tip_product WHERE tip_id = '" . (int) $tip_id . "' ORDER BY sort_order ");

        $result = array();
        // echo "<pre>"; die(print_r($query->rows));

        $this->load->model('catalog/product');
        foreach ($query->rows as $row) {
            $product_info = $this->model_catalog_product->getProduct($row['product_id']);
            if ($product_info) {
                $result[$row['product_id']] = $product_info;
            }
        }

        return $result;
    }

    public function getTipsGroups() {

        $key = 'tip.groups.' . (int) $this->config->get('config_language_id');
        $result = $this->cache->get($key);
        if (!$result) {
            $sql = "SELECT *  FROM " . DB_PREFIX . "tip_group tg LEFT JOIN " . DB_PREFIX . "tip_group_description tgd ON (tg.tip_group_id = tgd.tip_group_id) WHERE tg.status = '1' AND tgd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY tg.sort_order ";

            $query = $this->db->query($sql);

            if ($query->num_rows) {
                $this->cache->set($key, $query->rows);
            }

            return $query->rows;
        }
        return $result;
    }

    public function getTipsGroupsByTip($tip_id) {
        $sql = "SELECT t.tip_id, tg.tip_group_id, tgd.name
				FROM " . DB_PREFIX . "tip t
				LEFT JOIN " . DB_PREFIX . "tip_to_group ttg on (t.tip_id=ttg.tip_id)
				LEFT JOIN " . DB_PREFIX . "tip_group tg on (ttg.tip_group_id = tg.tip_group_id)
				LEFT JOIN " . DB_PREFIX . "tip_group_description tgd on (tg.tip_group_id=tgd.tip_group_id)
				LEFT JOIN " . DB_PREFIX . "tip_to_country ttc on (t.tip_id=ttc.tip_id)
				WHERE tg.status = '1'
				AND tgd.language_id = '" . (int) $this->config->get('config_language_id') . "'
				AND ttc.country_id = '" . (int) $this->config->get('config_country_id') . "'
				AND t.tip_id = '" . (int) $tip_id . "'
				ORDER BY tg.sort_order ";

        $query = $this->db->query($sql);

        $result = array();

        // echo "<pre>"; die($sql);
        // echo "<pre>"; die(print_r($query->rows));

        if (isset($query->rows) && !empty($query->rows)) {
            $result = $query->rows;
        }

        return $result;
    }

    public function getTagsByTip($tip_id) {
        $sql = "SELECT t.tag_id, tgd.name
				FROM " . DB_PREFIX . "tag_tip tt
				LEFT JOIN " . DB_PREFIX . "tag t on (tt.tag_id=t.tag_id)
				LEFT JOIN " . DB_PREFIX . "tag_description tgd on (tt.tag_id=tgd.tag_id)
				WHERE t.status = '1'
				AND tgd.language_id = '" . (int) $this->config->get('config_language_id') . "'
				AND tt.tip_id = '" . (int) $tip_id . "'
				ORDER BY tt.sort_order ";

        $query = $this->db->query($sql);

        $result = array();

        // echo "<pre>"; die($sql);
        // echo "<pre>"; die(print_r($query->rows));

        if (isset($query->rows) && !empty($query->rows)) {
            $result = $query->rows;
        }

        return $result;
    }

    public function getTipKeywords($tip_id) {
        $query = $this->db->query("SELECT tk.keyword_id,kd.name FROM tips_keyword tk LEFT JOIN keyword_description kd ON (tk.keyword_id=kd.keyword_id) where tk.tip_id='" . $tip_id . "' and kd.language_id='" . (int) $this->config->get('config_language_id') . "'");
        return $query->rows;
    }

}
