<?php
// Heading
$_['heading_title'] 		= 'Market Place';
$_['heading_title_partner'] = 'Become A Partner';

// Text
$_['text_marketplace']		= 'Marketplace';
$_['text_my_profile']		= 'ملفي الشخصي';
$_['text_addproduct']		= 'أضف منتجا';
$_['text_productlist']		= 'قائمة المنتجات';
$_['text_dashboard']		= 'اللوحة الرئيسية';
$_['text_wkshipping']		= 'إدارة الشحن';
$_['text_orderhistory']		= 'تاريخ الطلبات';
$_['text_orderhistory_pending']		= 'الطلبات قيد الانتظار';
$_['text_transaction']		= 'حركات الشراء';
$_['text_download']			= 'التنزيلات';
$_['text_ask_admin']		= 'Ask to Admin';
$_['text_ask_seller']		= 'تواصل مع البائع';
$_['text_ask_seller_log']	= 'الرجاء الدخول للتواصل مع البائع';
$_['text_profile']			= 'ملفي الشخصي';
$_['text_manageshipping']	= 'إدارة الشحن';
$_['text_downloads']		= 'التنزيلات';
$_['text_asktoadmin']		= 'Ask to Admin';

// membership
$_['text_membership']    = 'إضافة عضوية';


$_['text_ask_question']		= 'إسأل الادمن سؤالا';
$_['text_subject']			= 'الموضوع ';
$_['text_ask']				= 'إسأل ';
$_['text_close']			= 'إغلاق';
$_['text_send']				= 'أرسل ';
$_['text_sell_header']		= 'بيع';
$_['text_becomePartner']	= 'نعم, اريد ان اصبح شريكا';

$_['text_error_mail']		= 'إملأ كل الحقول.';
$_['text_success_mail']		= 'تم ارسال ايميلك بنجاح, سيتم التواصل معك قريبا';

$_['text_welcome']			= 'أهلا, ';
$_['text_low_stock']		= 'Low stocks';
$_['text_most_viewed']		= 'الاكثر مشاهدة';
$_['text_becomePartner']	= 'نعم, اريد ان اصبح شريكا';
$_['text_productname']		= 'إسم المنتج';
$_['text_model']			= 'الموديل';
$_['text_views']			= 'عدد المشاهدات';
$_['text_quantity']			= 'الكمية';
$_['text_more_work']		= 'اضف منتجات جديدو لتجذب انتباه المشترين';

//for mp
$_['text_from']     	    = 'From ';
$_['text_seller']     	    = 'البائع ';
$_['text_total_products']   = 'عدد المنتجات';
$_['text_latest_product']   = 'المزيد من المنتجات من نفس البائع ';
