<?php

$_['heading_title'] = 'الإطلالات';
$_['heading_title_filter'] = 'إطلالات %s';


$_['text_women_looks']   = 'إطلالات المرأة';
$_['text_famous_looks']  = 'إطلالات المشاهير';
$_['text_men_looks']     = 'إطلالات الرجل';
$_['text_kids_looks']    = 'إطلالات الطفل';
$_['text_latest']        = 'تابعي موضة وصيحات ';
$_['text_buy_look']      = 'اشتري هذه الاطلالة';
$_['text_more']          = 'المزيد';
$_['text_choose']        = 'اختاري';
$_['text_all_look']      = 'كل الاطلالات';
// $_['text_look_products'] = 'منتجات هذه الاطلالة';
$_['text_look_products'] = 'احصلي على نفس الاطلالة';
$_['text_look_products_men'] = 'احصل على نفس الاطلالة';
$_['text_price']         = 'السعر  ';
$_['text_other_looks']   = 'إطلالات أخرى';
$_['text_more_looks']    = 'المزيد';
$_['sizes']              = 'المقاسات';
$_['entry_qty']          = 'الكمية';
$_['unavailable']        = 'غير متوفر';
$_['text_not_available'] = 'هذا المنتج غير متوفر شحنه في منطقتك';
$_['addtocart']          = 'اضف الى سلة التسوق ';

$_['heading_meta_title']    = ' إطلالات %s | سيدتي مول | دليل المولات والماركات العالمية في %s ';
$_['text_meta_description'] = "شاهدي بالصور إطلالات المشاهير، أسعار فساتين المشاهير، إطلالات للرجل والمرأة والطفل، مع تفاصيل عن كيفية تنسيق الملابس بالأسعار والصور وموضة 2016 على سيدتي مول";
$_['heading_meta_title_detail_page'] ='%s | سيدتي مول | تسوق اون لاين في %s';
