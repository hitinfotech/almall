<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$_['heading_title'] = "منتجات سيدتي مول";

$_['text_categories'] = 'التصنيفات الرئيسية';
$_['text_brands'] = 'الماركات';
$_['text_attributes'] = 'المواصفات';
$_['text_clear_filters'] = 'إزالة التصفية';
$_['text_search_for'] = 'بحث عن : ';
$_['text_shop_now'] = 'تسوق الآن';
$_['text_shop_online'] = 'تسوق اون لاين';
$_['text_all_products'] = 'جميع المنتجات';
$_['text_search_website'] = 'البحث في المنتجات';
$_['filter']            = 'تصفية';

$_['text_sort_by'] = 'ترتيب';
$_['text_top_brand'] = 'رتب حسب أهم الماركات';
$_['text_products_price_asc'] = 'أسعار عاليه';
$_['text_products_price_desc'] = 'أسعار منخفضة';
$_['text_latest_products'] = 'رتب حسب آخر المنتجات';
$_['text_prices_asc'] = 'أقل الاسعار';
$_['text_prices_desc'] = 'أعلى الاسعار';
$_['text_views'] = 'الأكثر رواجا';
$_['text_color'] = 'لون';
$_['text_pattern'] = 'النمط';
$_['text_collection'] = 'التشكيلة';
$_['text_fabric'] = 'القماش';
$_['text_Bag_Type'] = 'شنط موديل';
$_['text_Heel_Height'] = 'طول الكعب';
$_['text_Length'] = 'الطول';
$_['text_Occasion'] = 'المناسبة';
$_['text_Options'] = 'القياسات';
$_['text_Shoes_type'] = 'موديل الحذاء';
$_['text_Skirt_Length'] = 'طول التنورة';
$_['text_Sleeves_Length'] = 'طول الأكمام';
$_['text_size']           = 'القياس';
$_['text_price']           = 'السعر';
$_['text_back']             ='رجوع';
$_['text_view_results']     ='عرض النتائج';
$_['text_no_result']        ='400 نتيجة';
$_['text_view_as']      ='اعرض حسب';

$_['prod'] = 'منتجات';
$_['heading_meta_title']    = 'تسوق اون لاين في %s | سيدتي مول | ازياء، اكسسوارات، فساتين سهرة، ملابس اطفال، شنط، ساعات';
$_['heading_meta_title1']    = ' سيدتي مول | دليل المولات والماركات العالمية في #country#';
$_['heading_page_title1']    = 'جميع المنتجات';


$_['heading_meta_title2']    = 'منتجات لل#main_category# |سيدتي مول | دليل المولات والماركات العالمية في #country#';
$_['heading_page_title2']    = ' منتجات لل#main_category#';

$_['heading_meta_title3']    = '#sub_category# |سيدتي مول |دليل المولات والماركات العالمية في #country# ';
$_['heading_page_title3']    = ' #sub_category# لل#main_category#';

$_['heading_meta_title4']    = '#sub_sub_category# لل#main_category# |سيدتي مول | دليل المولات والماركات العالمية في #country#';
$_['heading_page_title4']    = ' #sub_sub_category# لل#main_category#';

$_['heading_meta_title5']    = '#sub_sub_category# من #brand# لل#main_category# |سيدتي مول | دليل المولات والماركات العالمية في #country# ';
$_['heading_page_title5']    = ' #sub_sub_category# من #brand#';

$_['heading_meta_title6']    = '#sub_sub_category# من #brand#, #str_options# لل#main_category# |سيدتي مول | دليل المولات والماركات العالمية في #country# ';
$_['heading_meta_title6_2']  = '#sub_sub_category# لل#main_category#, #str_options# لل#main_category# |سيدتي مول | دليل المولات والماركات العالمية في #country# ';
$_['heading_page_title6']    = ' #sub_sub_category# من #brand#';

$_['heading_meta_title7']    = 'لقد بحثت عن "#keyword#"| سيدتي مول | دليل المولات والماركات العالمية في  #country#';
$_['heading_page_title7']    = 'لقد بحثت عن "#keyword#"';

$_['heading_meta_title8']    =  'منتجات من #brand# | سيدتي مول | دليل المولات والماركات العالمية في  #country# ';
$_['heading_page_title8']    = '  منتجات من #brand#';


$_['text_meta_description'] = ' ارقي الماركات على موقع سيدتي مول، شراء فساتين سهرة، شنط، احذية، ساعات، ملابس، فساتين ايلي صعب و زهير مراد، عبايات، مجوهرات، و اكسسوارات في %s';
