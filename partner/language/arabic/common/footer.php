<?php
// Text
$_['text_information']          = 'Information';
$_['text_service']              = 'خدمة العملاء';
$_['text_extra']                = 'إضافات';
$_['text_contact']              = 'اتصل بنا';
$_['text_contact']              = 'تواصل ';
$_['text_return']               = 'Returns';
$_['text_sitemap']              = 'خريطة الموقع';
$_['text_manufacturer']         = 'الماركات';
$_['text_voucher']              = 'Gift Vouchers';
$_['text_affiliate']            = 'Affiliates';
$_['text_special']              = 'Specials';
$_['text_account']              = 'حسابي';
$_['text_order']                = 'سجل الطلبات';
$_['text_wishlist']             = 'Wish List';
$_['text_newsletter']           = 'النشرة البريدية';
$_['text_powered']              = 'Powered By <a href="http://mall.sayidaty.net/">Saydati</a><br /> %s &copy; %s';
$_['text_call_us_form']         = 'نموذج التواصل معنا ';
$_['text_do_you_have_questions']= 'هل لديك سؤال؟';
$_['text_job_times']            = "من الأحد - الخميس: 9:00 ص - 5:00 م";

$_['text_brands']               = 'ماركات حصرية';
$_['text_freeshipping']         = 'التوصيل مجاني <small>(تطبق الشروط)</small>';
$_['text_return']               = 'الارجاع خلال 14 يوم';
$_['text_cod']                  = 'الدفع عند التسليم';

$_['text_shopping']             = 'تسوق';
$_['text_woman']                = 'مرأة';
$_['text_man']                  = 'رجل';
$_['text_newarrival']           = 'وصل حديثاً';
$_['text_bestseller']           = 'الأكثر مبيعاً';
$_['text_designer']             = 'مصممين';
$_['text_sizes']                = 'جدول المقاسات';

$_['text_sayidaty_mall']        = 'سيدتي مول ';

$_['text_subscribe']            = 'اشتركي بالمجلة الالكترونية';
$_['text_subscribe_desc']       = 'كوني أول من يحصل على آخر المنتجات و الصيحات و العروض من سيدتي مول';
$_['text_female']               = 'انثى';
$_['text_male']                 = 'ذكر';
$_['text_email']                = "البريد الالكتروني";

$_['text_about_us']             = 'من نحن';
$_['text_shop_online']          = 'سيدتي';
$_['text_customer_service']     = 'خدمة العملاء';
$_['text_follow_us']            = 'تابعونا على';
