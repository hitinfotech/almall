<?php
// Text
$_['text_home']                 = 'الرئيسية';
$_['text_wishlist']             = 'قائمة رغباتي (%s)';
$_['text_notifications']        =  'قائمتي المفضلة';
$_['text_shopping_cart']        = 'عربة التسوق';
$_['text_category']             = 'التصنيفات';
$_['text_account']              = 'الدخول';
$_['text_tracking']             = 'تتبع الطلبات';
$_['text_profile']              = 'الملف الشخصي';
$_['text_welcome']              = 'أهلا %s';
$_['text_register']             = 'تسجيل';
$_['text_login']                = 'تسجيل الدخول';
$_['text_login_header']         = 'تسجيل الدخول';
$_['text_login_header_cart']    = 'عليك تسجيل الدخول للاستفادة من القسيمة';
$_['text_order']                = 'سجلات الطلبات';
$_['text_transaction']          = 'Transactions';
$_['text_download']             = 'Downloads';
$_['text_seller_account']       = 'صفحة البائع';
$_['text_logout']               = 'تسجيل خروج';
$_['text_checkout']             = 'شراء';
$_['text_search']               = 'بحث';
$_['text_all']                  = 'عرض الكل';

$_['text_sayidaty_dot_net_site']= 'سيدتي.نت';
$_['text_sayidaty_kitchen_site']= 'سيدتي مطبخ';
$_['text_sayidaty_fashion_site']= 'أزياء';

$_['text_call_us']              = 'اتصل بنا';
$_['text_free_exchange']        = 'الاستبدال مجانا';
$_['text_delivery_and_exchange']= 'التوصيل و الإستبدال';

$_['text_facebook_login']       = 'الدخول عن طريق الفيس بوك';
$_['text_email']                = 'البريد الإلكتروني';
$_['text_or']                   = 'أو';
$_['text_password']             = 'كلمة السر';
$_['text_forgot_password']      = 'نسيت كلمة المرور؟';
$_['text_new_user']             = 'مستخدم جديد؟';
$_['text_close']                = 'إغلاق';

$_['text_search_website']       = 'البحث في الموقع...';
$_['text_shopping_bag']         = 'حقيبة التسوق';

$_['text_new']                  = 'جديد';
$_['text_favorit']              = 'المفضلة';
$_['text_tags']                 = 'صيحات';
$_['text_more']                 = 'المزيد';

$_['text_welcome2']             = 'أهلا بكم،';

$_['text_newscon']              = 'لا تفوتي آخر المنتجات والعروض وجديد الأسواق على موقع سيدتي مول واشتركي في المجلة الإلكترونية.';

$_['male']                      = 'ذكر';
$_['female']                    = 'أنثى';
$_['text_thanks']               = 'شكراً!';
$_['text_done_add']             = 'لقد تم تسجيلك في مجلة سيدتي مول الإلكترونية.';
$_['text_done2']                = 'يمكنك مراجعة بريدك الإلكتروني للمزيد من العروض في المستقبل.';
$_['shop_now_text']             = "اشتر الآن";
$_['sold_out_text']             = "نفذت الكمية";

$_['text_rma_rma']              = 'الطلبات المسترجعة';
$_['text_rma_reason']           = 'اسباب الارجاع';
$_['text_loginto_seller']       = 'ابدأ البيع معنا';
$_['text_register_seller']      = 'التسجيل';
$_['text_howtosell']            = 'كيفية البيع';
$_['text_login_seller']       = 'الدخول';
