<?php

$_['text_menu_title_shop']      = 'تسوق';
$_['text_online']               = 'اون لاين';

$_['text_menu_title_shop']      = 'دليل المنتجات';
$_['text_menu_title_shop2']     = 'تسوق اون لاين ';
$_['text_online']               = '';


$_['text_we_choose_for_you']    = 'اخترنا لك';
$_['text_menu_shop']            = 'تسوق';

$_['text_shirts']               = 'قمصان';
$_['text_suits']                = 'بدلات';
$_['text_sport_wears']          = 'ملابس رياضية';
$_['text_trousers_pants']       = 'سراويل و بناطيل';
$_['text_slippers']             = 'شباشب';
$_['text_casual_shoes']         = 'احذية عملية';
$_['text_ties']                 = 'ربطات عنق';
$_['text_belts']                = 'احزمة';
$_['text_hats']                 = 'قبعات';
$_['text_casual_bags']          = 'حقائب عملية';
$_['text_designers']            = 'مصممين';
$_['text_full_looks']           = 'إطلالات';
$_['text_full_shops']           = 'الدليل';
$_['text_products']             = 'دليل المنتجات';

$_['text_men']                  = 'رجل';
$_['text_women']                = 'مرأة';
$_['text_clothes']              = 'ملابس';
$_['text_dresses']              = 'فساتين';
$_['text_shirts_tshirts']       = 'بلايز وقمصان';
$_['text_jackets_coats']        = 'جاكيتات و معاطف';
$_['text_abaya']                = 'عبايات';
$_['text_swimming_suits']       = 'ملابس بحر';
$_['text_shoes']                = 'احذية';
$_['text_boots']                = 'جزم';
$_['text_sandals']              = 'صنادل';
$_['text_flat_shoes']           = 'احذية فلات';
$_['text_sport_shoes']          = 'احذية رياضية';
$_['text_high_heels']           = 'كعب عالي';
$_['text_accessories']          = 'اكسسوارات';
$_['text_nicklesses']           = 'سلاسل';
$_['text_diamonds']             = 'الماس';
$_['text_watches']              = 'ساعات';
$_['text_sunglasses']           = 'نظارات';
$_['text_rings']                = 'خواتم';
$_['text_bags']                 = 'حقائب';
$_['text_big_bags']             = 'حقائب كبيرة';
$_['text_small_bags']           = 'حقائب صغيرة';
$_['text_clatch']               = 'كلاتش';
$_['text_wallets']              = 'محافظ';
$_['text_travel_bags']          = 'حقائب سفر';


$_['text_brands']               = 'الماركات';
$_['text_offers']               = 'تخفيضات';
$_['text_offer']                = 'تخفيضات';
$_['text_mother_day']           = 'عيد الأم';
$_['text_news']                 = 'الأخبار';
$_['text_looks']                = 'إطلالات';
$_['text_tips']                 = 'نصائح ';

$_['text_malls_catalog']        = 'دليل المولات';
$_['text_shops_catalog']        = 'دليل المحلات';

$_['text_more']                 = 'المزيد';
$_['text_tag']                  = '#الوسوم ';
$_['text_toppages']             = 'توب5';
