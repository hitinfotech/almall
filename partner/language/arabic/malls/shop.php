<?php

$_['text_shops_catalog']                = 'دليل المحلات';

$_['shops_in_zone']                     = 'دليل المحلات في %s';
$_['shops_in_city']                     = 'محلات %s %s في %s %s';

$_['shops_in']                          = 'محلات %s %s في %s %s';
$_['shop_in']                           = 'دليل المحلات %s %s في %s %s';

$_['text_shop_online']= 'تسوق اون لاين';
$_['shop_h1_title_1'] = 'دليل المحلات';
$_['shop_h1_title_3'] = 'دليل المحلات في #mall# #zone#';
$_['shop_h1_title_4'] = 'محلات #category# #brand#';
$_['shop_h1_title_5'] = 'محلات #category# #brand# في #mall# #zone#';

$_['heading_meta_title_catalog']                = 'دليل المحلات| سيدتي مول | دليل المولات والماركات العالمية في %s';
$_['heading_meta_title']    = 'دليل المحلات في %s  %s %s | سيدتي مول  | دليل المولات والماركات العالمية في %s ';
$_['heading_meta_title_2']    = ' محلات %s %s | سيدتي مول | دليل المولات والماركات العالمية في %s';
$_['heading_meta_title_3']    = ' محلات %s %s في  %s %s | سيدتي مول  | دليل المولات والماركات العالمية في %s';
$_['heading_meta_title_detail_page']    = '%s | سيدتي مول  | دليل المولات والماركات العالمية في %s';

$_['text_shops_intro']                  = 'دليل سيدتي مول الشامل لأهم مراكز ومحلات التسوق. ابحث الآن عن اسم المحل أو المول أو من بين تصنيفات محلات الأزياء والإكسسورات والعناية والتجميل والأثاث والمراكز الأعراس والمناسبات وجميع تصنيفات التسوق في مدينتك.';
$_['text_shops_all_cities']             = 'كل المدن';
$_['text_shops_all_malls']              = 'كل المولات';
$_['text_shops_all_categories']         = 'كل التصنيفات';
$_['text_shops_all_brands']             = 'كل الماركات';
$_['text_search_all_shops']             = 'ابحث في المحلات';
$_['text_shops_you_searched_for']       = 'لقد بحثت عن';
$_['text_shops_arabic_name']            = 'الاسم بالعربي';
$_['text_shops_english_name']           = 'الاسم بالانجليزي';
$_['text_shops_categories']             = 'التصنيفات';
$_['text_shops_brands']                 = 'الماركات';
$_['text_shops_phone']                  = 'هاتف';
$_['text_shops_address']                = 'العنوان';
$_['text_shops_available_in_branshes']  = 'متاح فى محلات';
$_['text_shops_official_links']         = 'الروابط الرسمية';
$_['text_shops_products_from']          = 'منتجات من';
$_['text_shops_discounts_from']         = 'تخفيضات من';
$_['text_shops_discount']               = 'خصم';
$_['text_shops_more']                   = 'المزيد';
$_['text_no_result']                    = 'لا يوجد نتائج بحث';
$_['heading_meta_description']    =  "دليل محلات سيدتي مول، عناوين وفروع جميع محلات الرياض، جدة، دبي، أبوظبي، السعودية والإمارات. ابحثي عن عنوان و فرع اي محل أو مول وأسعار كل المحلات وصور منتجات ومقارنات، ";
$_['heading_meta_description2']    =  "%s فرع %s، إذا كنت تبحثين عن فروع %s في %s، %s";
