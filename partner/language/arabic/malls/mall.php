<?php

$_['text_malls_catalog']            = 'دليل المولات';

$_['text_malls_in']                 = 'دليل المولات في %s';
$_['text_shop_online']              = 'تسوق اون لاين';
$_['text_malls_all_cities']         = 'كل المدن';
$_['text_malls_search_mall_name']   = 'ابحث عن اسم المول';
$_['text_malls_recent_models_from'] = 'احدث الموديلات من';
$_['text_malls_categories']         = 'التصنيفات';
$_['text_malls_text_shop']          = 'محل';
$_['text_malls_text_shops']         = 'محلات';
$_['text_malls_mall_name_arabic']   = 'الاسم بالعربي';
$_['text_malls_mall_name_english']  = 'الاسم بالانجليزي';
$_['text_malls_shops_number']       = 'عدد المحلات';
$_['text_malls_phone_number']       = 'هاتف';
$_['text_malls_address']            = 'العنوان';
$_['text_malls_official_links']     = 'الروابط الرسمية';
$_['text_malls_latest_products']    = 'آخر عروض';
$_['text_malls_discount']           = 'خصم';
$_['text_malls_more']               = 'المزيد';
$_['text_malls_latest_news']        = 'آخر أخبار';

$_['heading_meta_title']    = 'دليل المولات %s | سيدتي مول | دليل المولات والماركات العالمية في %s ';
$_['heading_meta_description']    = 'اذا كنت تبحثين عن عنوان مول معين، سوف تجدين هنا عناوين مولات الرياض، مولات جدة، دبي مول، الراشد مول، ابن بطوطة مول وجميع مولات المملكة العربية السعودية والامارات';

$_['heading_meta_description_details']    =  'تعرفي علي %s ، و شاهدي قائمة المحلات الموجودة في %s ، سوف تجدين هنا ايضا عنوان %s وكيفية الوصول إليه ومعلومات عن مواعيد العمل في %s علي سيدتي مول';;
