<?php

// Text

$_['text_subject'] = 'كلمة المرور الجديده ل %s';

$_['text_greeting2'] = 'aa %s %s';

$_['text_greeting'] = '<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style type="text/css">
            body {margin: 0;}
            body, table, td, p, a, li, blockquote {-webkit-text-size-adjust: none!important;font-family: Merienda, \'Times New Roman\', serif;font-style: normal;font-weight: 400;}
            button{ width:90%;}
            @media screen and (max-width:600px) {
                
                body, table, td, p, a, li, blockquote {
                    -webkit-text-size-adjust: none!important;
                    font-family: Merienda, \'Times New Roman\', serif;
                }
                table {
                    
                    width: 100%;
                }
                .footer {
                    
                    height: auto !important;
                    max-width: 96% !important;
                    width: 96% !important;
                }
                table.responsiveImage {
                    
                    height: auto !important;
                    max-width: 30% !important;
                    width: 30% !important;
                }
                table.responsiveContent {
                    
                    height: auto !important;
                    max-width: 66% !important;
                    width: 66% !important;
                }
                .top {
                    
                    height: auto !important;
                    max-width: 48% !important;
                    width: 48% !important;
                }
                .catalog {
                    margin-left: 0%!important;
                }

            }
            @media screen and (max-width:480px) {
                
                body, table, td, p, a, li, blockquote {
                    -webkit-text-size-adjust: none!important;
                    font-family: Merienda, \'Times New Roman\', serif;
                }
                table {
                    
                    width: 100% !important;
                    border-style: none !important;
                }
                .footer {
                    
                    height: auto !important;
                    max-width: 96% !important;
                    width: 96% !important;
                }
                .table.responsiveImage {
                    
                    height: auto !important;
                    max-width: 96% !important;
                    width: 96% !important;
                }
                .table.responsiveContent {
                    
                    height: auto !important;
                    max-width: 96% !important;
                    width: 96% !important;
                }
                .top {
                    
                    height: auto !important;
                    max-width: 100% !important;
                    width: 100% !important;
                }
                .catalog {
                    margin-left: 0%!important;
                }
                button{
                    width:90%!important;
                }
            }
        </style>
    </head>
    <body yahoo="yahoo" dir="rtl">
        <table width="100%"  cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td><table width="600"  align="center" cellpadding="0" cellspacing="0">
                            <!-- Main Wrapper Table with initial width set to 60opx -->
                            <tbody>
                                <tr>
                                    <td align="center" style="padding:10px;">
                                        <a href="'.HTTPS_CATALOG.'/"><img src="'.HTTPS_CATALOG.'catalog/view/theme/sayidaty/images/logo-ar.png"></a>
                                    </td>
                                </tr>
                                <tr> 
                                    <!-- HTML Spacer row -->
                                    <td style="font-size: 0; line-height: 0;" height="20"><table width="96%" align="left"  cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                                            </tr>
                                        </table>
                                        </td>
                                </tr>
                                <tr> 
                                    <!-- HTML IMAGE SPACER -->
                                    <td style="font-size: 0; line-height: 0;" ><table align="center"  cellpadding="0" width="100%" cellspacing="0" >
                                            <tr>
                                                <td style="padding: 0 20px;">
                                                    <p style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 18px; padding: 10px 0;">أهلاً <span style="color: #d91a5d;">%s</span>,</p>
                                                    <p style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 10px 0; font-weight: normal;">
                                                        لقد تم طلب كلمة مرور جديدة من سيدتي مول.
                                                    </p>

                                                    <p  style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 10px 0; font-weight: normal;">
                                                        كلمة المرور الجديدة:</p>
                                                    <p  style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 10px 0; font-weight: normal;">
                                                         %s
                                                    </p>
                                                </td>
                                            </tr>

                                        </table></td>
                                </tr>

                                <tr> 
                                    <!-- HTML Spacer row -->
                                    <td style="font-size: 0; line-height: 0;" height="10"><table width="96%" align="left"  cellpadding="0" cellspacing="0" >

                                        </table></td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 20px 10px;">
                                        <p style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 0px 0; font-weight: bold;">
                                            فريق دعم سيدتي مول</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 40px 20px 20px;" align="center">
                                        <img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/follow-ar.png">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="padding: 0 20px 10px;">
                                        <a  href="https://www.facebook.com/Sayidatymall/?fref=ts"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/fb.png" width="40"></a>
                                        <a href="https://twitter.com/sayidaty_mall"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/tw.png" width="40"></a>
                                        <a href="https://www.youtube.com/channel/UCw0FpmUg229kGcDqKKVj-Aw"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/yt.png" width="40"></a>
                                        <a href="https://www.instagram.com/sayidatymall"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/ins.png" width="40"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="footer" width="96%"  align="left" cellpadding="0" cellspacing="0">
                                            <!-- Second column of footer content -->
                                            <tr>
                                                <td style="border-top: 1px solid #ddd;"><p style="font-size: 12px; font-style: normal; font-weight:normal; color: #555; line-height: 1.8; text-align:justify;padding-top:10px; margin-left:20px; margin-right:20px; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center;">This email was sent to you by Sayidaty Mall. To ensure delivery to your inbox (not bulk or junk folders), please add our e-mail address, mall@sayidaty.net, to your address book.
                                                    </p>
                                                    <p style="font-size: 12px; font-style: normal; font-weight:normal; color: #555; line-height: 1.8; text-align:justify;padding-top:10px; margin-left:20px; margin-right:20px; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center;">
                                                        To view our Privacy Policy click here.
                                                        To unsubscribe click here.<br>

                                                        © جميع الحقوق محفوظة للشركة السعودية للأبحاث والنشر وتخضع لشروط وإتفاق الإستخدام. </p>
                                                </td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>';


$_['text_greeting_p1'] = '<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style type="text/css">
            body {margin: 0;}
            body, table, td, p, a, li, blockquote {-webkit-text-size-adjust: none!important;font-family: Merienda, \'Times New Roman\', serif;font-style: normal;font-weight: 400;}
            button{ width:90%;}
            @media screen and (max-width:600px) {
                
                body, table, td, p, a, li, blockquote {
                    -webkit-text-size-adjust: none!important;
                    font-family: Merienda, \'Times New Roman\', serif;
                }
                table {
                    
                    width: 100%;
                }
                .footer {
                    
                    height: auto !important;
                    max-width: 96% !important;
                    width: 96% !important;
                }
                table.responsiveImage {
                    
                    height: auto !important;
                    max-width: 30% !important;
                    width: 30% !important;
                }
                table.responsiveContent {
                    
                    height: auto !important;
                    max-width: 66% !important;
                    width: 66% !important;
                }
                .top {
                    
                    height: auto !important;
                    max-width: 48% !important;
                    width: 48% !important;
                }
                .catalog {
                    margin-left: 0%!important;
                }

            }
            @media screen and (max-width:480px) {
                
                body, table, td, p, a, li, blockquote {
                    -webkit-text-size-adjust: none!important;
                    font-family: Merienda, \'Times New Roman\', serif;
                }
                table {
                    
                    width: 100% !important;
                    border-style: none !important;
                }
                .footer {
                    
                    height: auto !important;
                    max-width: 96% !important;
                    width: 96% !important;
                }
                .table.responsiveImage {
                    
                    height: auto !important;
                    max-width: 96% !important;
                    width: 96% !important;
                }
                .table.responsiveContent {
                    
                    height: auto !important;
                    max-width: 96% !important;
                    width: 96% !important;
                }
                .top {
                    
                    height: auto !important;
                    max-width: 100% !important;
                    width: 100% !important;
                }
                .catalog {
                    margin-left: 0%!important;
                }
                button{
                    width:90%!important;
                }
            }
        </style>
    </head>
    <body yahoo="yahoo" dir="rtl">
        <table width="100%"  cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td><table width="600"  align="center" cellpadding="0" cellspacing="0">
                            <!-- Main Wrapper Table with initial width set to 60opx -->
                            <tbody>
                                <tr>
                                    <td align="center" style="padding:10px;">
                                        <a href="'.HTTPS_CATALOG.'/"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/logo-ar.png"></a>
                                    </td>
                                </tr>
                                <tr> 
                                    <!-- HTML Spacer row -->
                                    <td style="font-size: 0; line-height: 0;" height="20"><table width="96%" align="left"  cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                                            </tr>
                                        </table>
                                        </td>
                                </tr>
                                <tr> 
                                    <!-- HTML IMAGE SPACER -->
                                    <td style="font-size: 0; line-height: 0;" ><table align="center"  cellpadding="0" width="100%" cellspacing="0" >
                                            <tr>
                                                <td style="padding: 0 20px;">
                                                    <p style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 18px; padding: 10px 0;">أهلاً <span style="color: #d91a5d;">';
$_['text_greeting_p2'] ='</span>,</p>
                                                    <p style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 10px 0; font-weight: normal;">
                                                        لقد تم طلب كلمة مرور جديدة من سيدتي مول.
                                                    </p>

                                                    <p  style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 10px 0; font-weight: normal;">
                                                        كلمة المرور الجديدة:</p>
                                                    <p  style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 10px 0; font-weight: normal;">';
$_['text_greeting_p3'] ='</p>
                                                </td>
                                            </tr>

                                        </table></td>
                                </tr>

                                <tr> 
                                    <!-- HTML Spacer row -->
                                    <td style="font-size: 0; line-height: 0;" height="10"><table width="96%" align="left"  cellpadding="0" cellspacing="0" >

                                        </table></td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 20px 10px;">
                                        <p style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 0px 0; font-weight: bold;">
                                            فريق دعم سيدتي مول</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding: 40px 20px 20px;" align="center">
                                        <img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/follow-ar.png">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="padding: 0 20px 10px;">
                                        <a  href="https://www.facebook.com/Sayidatymall/?fref=ts"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/fb.png" width="40"></a>
                                        <a href="https://twitter.com/sayidaty_mall"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/tw.png" width="40"></a>
                                        <a href="https://www.youtube.com/channel/UCw0FpmUg229kGcDqKKVj-Aw"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/yt.png" width="40"></a>
                                        <a href="https://www.instagram.com/sayidatymall"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/ins.png" width="40"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="footer" width="96%"  align="left" cellpadding="0" cellspacing="0">
                                            <!-- Second column of footer content -->
                                            <tr>
                                                <td style="border-top: 1px solid #ddd;"><p style="font-size: 12px; font-style: normal; font-weight:normal; color: #555; line-height: 1.8; text-align:justify;padding-top:10px; margin-left:20px; margin-right:20px; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center;">This email was sent to you by Sayidaty Mall. To ensure delivery to your inbox (not bulk or junk folders), please add our e-mail address, mall@sayidaty.net, to your address book.
                                                    </p>
                                                    <p style="font-size: 12px; font-style: normal; font-weight:normal; color: #555; line-height: 1.8; text-align:justify;padding-top:10px; margin-left:20px; margin-right:20px; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center;">
                                                        To view our Privacy Policy click here.
                                                        To unsubscribe click here.<br>

                                                        © جميع الحقوق محفوظة للشركة السعودية للأبحاث والنشر وتخضع لشروط وإتفاق الإستخدام. </p>
                                                </td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>';
