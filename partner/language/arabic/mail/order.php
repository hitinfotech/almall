<?php
// Text
$_['text_new_subject']          = 'تم استلام طلبك - %s';
$_['text_new_greeting']         = '<p><b>تأكيد إتمام طلبية</b> </p><p><b>عميلنا العزيز  <span style="color:red">%s</span></b></p><p>
    شكراً لتسوقك معنا!. </p> <p>
    لقد استلمنا طلب الشراء  ، ونعمل على شحنه لك خلال 3 إلى 10 أيام عمل وإرسال معلومات وصولهارقم طلبيتك هو <span style="color:red">%s</span>.  </p> <p>بإمكانك مراجعة معلومات طلب شرائك أدناه. ويرجى العلم أن الطلبيات التي تحتوي على أكثر من قطعة قد يتم شحنها إليك في علب منفصلة، وبدون أي رسوم شحن إضافية. إذا كان هناك أي قطع أخرى في طلبيتك، سوف نرسل لك بريد إلكتروني عندما يتم شحنها
</p><p>للاستفسارات الأخرى، يمكنك التواصل معنا من الإمارات والسعودية على الرقم 8004330033 أو البريد الإلكتروني support@sayidatymall.net
</p> <p><b>مع خالص تحياتنا </p><p>فريق خدمة عملاء سيدتي مول</b></p>';
$_['text_new_received']         = 'You have received an order.';
$_['text_new_link']             = 'لعرض طلبك انقر على الرابط أدناه:';
$_['text_new_order_detail']     = 'معلومات الطلبية';
$_['text_new_instruction']      = 'تعليمات';
$_['text_new_order_id']         = 'رقم الطلبية:';
$_['text_new_date_added']       = 'تاريخ الاضافة:';
$_['text_new_order_status']     = 'حاله الطلب:';
$_['text_new_payment_method']   = 'طريقة الدفع:';
$_['text_new_shipping_method']  = 'طريقة الشحن:';
$_['text_new_email']  	        = 'البريد الالكتروني:';
$_['text_new_telephone']  	= 'التلفون:';
$_['text_new_ip']  		= 'عنوان اي بي:';
$_['text_new_payment_address']  = 'عنوان الدفع';
$_['text_new_shipping_address'] = 'عنوان الشحن';
$_['text_new_products']         = 'المنتجات';
$_['text_new_product']          = 'المنتج';
$_['text_new_model']            = 'النوع';
$_['text_new_quantity']         = 'الكمية';
$_['text_new_price']            = 'السعر';
$_['text_new_order_total']      = 'المجموع الكلي';
$_['text_new_total']            = 'المجموع';
$_['text_new_download']         = 'Once your payment has been confirmed you can click on the link below to access your downloadable products:';
$_['text_new_comment']          = 'The comments for your order are:';
$_['text_new_footer']           = 'Please reply to this e-mail if you have any questions.';
$_['text_update_subject']       = '%s - Order Update %s';
$_['text_update_order']         = 'Order ID:';
$_['text_update_date_added']    = 'Date Ordered:';
$_['text_update_order_status']  = 'Your order has been updated to the following status:';
$_['text_update_comment']       = 'The comments for your order are:';
$_['text_update_link']          = 'To view your order click on the link below:';
$_['text_update_footer']        = 'Please reply to this email if you have any questions.';
$_['text_update_order_status_admin']  = 'Order has been updated to the following status:';
$_['text_update_comment_admin']       = 'The comments for above order are:';
$_['text_buyer_name']           = 'Buyer name: ';
$_['text_buyer_country']        = 'Country: ';
$_['text_item_number']          = 'Item number: ';
$_['text_order_number']         = 'Order number: ';
$_['text_reason_for_return']          = 'Reason for return: ';
$_['text_below_order_summary']    ='Below is the return information.<br/>A delivery agent will collect the item from the customer and deliver it to you within 3 working days.<br/>Please note that in order for us to refund the customer, you will need to grant the return request.<br/>Your Return Summary';
$_['text_order_date']           = 'Order Date: ';
$_['text_rma_date']           = 'Return request Date:';
$_['text_new_sku']            = 'SKU';
$_['text_rma_title']            = 'Attention: A return request has been made on one of your items.';
