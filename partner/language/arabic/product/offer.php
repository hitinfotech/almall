<?php

/*
 * Language for ARABIC offers page
 */

$_['head_title_catalog']    = ' العروض والتخفيضات في %s';
$_['head_title']            = 'العروض والتخفيضات';
$_['text_search']           = 'ابحث في المحلات';
$_['text_zones']            = 'كل المدن';
$_['text_offer_type']       = 'نوع العرض';
$_['text_offer_time']       = 'مدة العرض';
$_['text_categories']       = 'التصنيفات';
$_['text_malls']            = 'المولات';
$_['text_available_shops']  = 'متاح فى محلات:';

$_['text_similer_offers']   = 'آخر العروض';
$_['text_more_offers']      =  'المزيد';
$_['text_similer_news']     = 'أخبار متعلقة';
$_['text_latest_news']      = 'آخر الأخبار';
$_['text_more_news']        =  'المزيد';
$_['text_no_result']        =  'لا يوجد نتائج بحث';
$_['text_no_result_title']  = 'نتائج البحث عن "%s" في العروض والتخفيضات';
//$_['text_results_for']      = 'نتائج البحث عن "%s" في ';

$_['offer_in']              = 'العروض و التخفيضات %s في %s %s';
$_['offer_in_2']              = 'العروض و التخفيضات %s';
$_['heading_meta_title']    = 'العروض و التخفيضات %s  %s %s | سيدتي مول | دليل المولات والماركات العالمية في %s ';
$_['heading_meta_description']    = 'شاهدي كل العروض والتخفيضات من كبرى الماركات، عروض  2016 و تخفيضات مولات السعودية والإمارات، عروض الصيف، تخفيضات الشتاء، خصومات حقيقية على الحقائب والأحذية والفساتين';
