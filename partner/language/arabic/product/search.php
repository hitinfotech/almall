<?php
// Heading
$_['heading_title']     = 'بحث';
$_['heading_tag']	= 'Tag - ';

// Text
$_['text_search']       = 'Products meeting the search criteria';
$_['text_keyword']      = 'Keywords';
$_['text_category']     = 'All Categories';
$_['text_sub_category'] = 'Search in subcategories';
$_['text_empty']        = 'There is no product that matches the search criteria.';
$_['text_quantity']     = 'Qty:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model']        = 'Product Code:';
$_['text_points']       = 'Reward Points:';
$_['text_price']        = 'Price:';
$_['text_tax']          = 'Ex Tax:';
$_['text_reviews']      = 'Based on %s reviews.';
$_['text_compare']      = 'Product Compare (%s)';
$_['text_sort']         = 'ترتيب حسب:';
$_['text_default']      = 'الإفتراضي';
$_['text_name_asc']     = 'الإسم (أ - ي)';
$_['text_name_desc']    = 'الإسم (ي - أ)';
$_['text_price_asc']    = 'السعر (الأقل &gt; الأعلى)';
$_['text_price_desc']   = 'السعر (الأعلى &gt; الأقل)';
$_['text_rating_asc']   = 'Rating (Lowest)';
$_['text_rating_desc']  = 'Rating (Highest)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'عرض:';
$_['filter']            = 'فلتر';

// Entry
$_['entry_search']      = 'Search Criteria';
$_['entry_description'] = 'Search in product descriptions';