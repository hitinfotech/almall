<?php
// Locale
$_['code']                  = 'ar';
$_['direction']             = 'rtl';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'Y-m-d H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

$_['arabic_language']       = 'عربي';
$_['english_language']      = 'إنجليزي';

// Text
$_['text_home']             = 'الرئيسية';	//'<i class="fa fa-home"></i>';
$_['text_yes']              = 'نعم';
$_['text_no']               = 'لا';
$_['text_none']             = ' --- لا يوجد--- ';
$_['text_select']           = ' --- الرجاء الإختيار --- ';
$_['text_all_zones']        = 'كل المناطق';
$_['text_pagination']       = 'إظهار %d الى %d من %d (%d صفحات)';
$_['text_loading']          = 'تحميل ...';
$_['text_no_result']        = 'لم يتم العثور على نتائج';
$_['text_site_name']        = 'سيدتي.نت';
//$_['text_results_for']      = 'نتائج البحث عن "%s" في ';
$_['text_results_for']      = 'لقد بحثت عن "%s"';
$_['heading_meta_title_detail_page']    = '%s | سيدتي مول  | دليل المولات والماركات العالمية في %s';
$_['heading_meta_title_search']    = 'لقد بحثت عن "%s"| سيدتي مول  %s';
$_['text_in']          = ' في ';

// Social
$_['text_share']   			= 'شــير';
$_['text_share_facebook']   = 'شير على فيسبوك';
$_['text_share_twitter']    = 'شير على تويتر';
$_['text_share_google']     = 'شير على جوجل';

// Buttons
$_['button_address_add']    = 'إضافة عنوان';
$_['button_back']           = 'عودة';
$_['button_continue']       = 'إستمرار';
$_['button_cart']           = 'إشتر الان';
$_['sold_out_text']           = 'نفذت الكمية';
$_['button_cancel']         = 'إلغاء';
$_['button_compare']        = 'مقارنة هذا المنتج';
$_['button_wishlist']       = 'إضافة الى قائمة رغباتي';
$_['button_checkout']       = 'إتمام الشراء';
$_['button_confirm']        = 'تأكيد الطلب';
$_['button_coupon']         = 'إعتماد القسيمة';
$_['button_delete']         = 'حذف';
$_['button_download']       = 'تنزيل';
$_['button_edit']           = 'تعديل';
$_['button_filter']         = 'Refine Search';
$_['button_new_address']    = 'عنوان جديد';
$_['button_change_address'] = 'تغيير العنوان';
$_['button_reviews']        = 'Reviews';
$_['button_write']          = 'Write Review';
$_['button_login']          = 'تسجيل دخول';
$_['button_update']         = 'تحديث';
$_['button_remove']         = 'إزالة';
$_['button_reorder']        = 'سجل';
$_['button_return']         = 'إعادة';
$_['button_shopping']       = 'متابعة التسوق';
$_['button_search']         = 'بحث';
$_['button_shipping']       = 'إعتماد الشحن';
$_['button_submit']         = 'إعتمد';
$_['button_guest']          = 'Guest Checkout';
$_['button_view']           = 'عرض';
$_['button_voucher']        = 'Apply Voucher';
$_['button_upload']         = 'تجميل ملف';
$_['button_reward']         = 'Apply Points';
$_['button_quote']          = 'Get Quotes';
$_['button_list']           = 'قائمة';
$_['button_grid']           = 'Grid';
$_['button_map']            = 'View Google Map';
$_['text_more']             = 'المزيد';
$_['text_latest']           = 'آخر المنتجات';
$_['text_bestseller']       = 'الأكثر مبيعاً';
$_['text_get_it_now']       = 'احصلي عليها الآن';

// Error
$_['error_exception']       = 'رقم الخطأ(%s): %s في %s رقم السطر %s';
$_['error_upload_1']        = 'تحذير: The uploaded file exceeds the upload_max_filesize directive in php.ini!';
$_['error_upload_2']        = 'تحذير: The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form!';
$_['error_upload_3']        = 'تحذير: The uploaded file was only partially uploaded!';
$_['error_upload_4']        = 'تحذير: No file was uploaded!';
$_['error_upload_6']        = 'تحذير: Missing a temporary folder!';
$_['error_upload_7']        = 'تحذير: Failed to write file to disk!';
$_['error_upload_8']        = 'تحذير: File upload stopped by extension!';
$_['error_upload_999']      = 'تحذير: No error code available!';
