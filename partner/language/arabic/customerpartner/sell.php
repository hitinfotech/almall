<?php
// Heading
$_['heading_title']     	  = 'Sell';

//Text
$_['text_tax']     	   		  = 'Tax : ';
$_['text_from']     	      = 'From ';
$_['text_seller']     	      = 'Seller ';
$_['text_total_products']     = 'Total Products ';
$_['text_long_time_seller']   = 'Long Time Sellers';
$_['text_latest_product']     = 'Latest Products';

$_['text_persinal_info']      = 'المعلومات الشخصية';
$_['text_business_info']      = 'معلومات العمل';

$_['text_firstname']          = 'الاسم الاول';
$_['text_email']              = 'الايميل';
$_['text_phone']              = 'الهاتف';
$_['text_password']          = 'كلمه السر';
$_['text_confirm_password']  = 'تأكيد كلمة السر';


$_['text_country']            = 'الدولة';
$_['text_city']            = 'المدينة';
$_['text_num_of_branches']            = 'عدد الفروع';
$_['text_website']            = 'الموقع الالكتروني';
$_['text_facebook']            = 'عنوان صفحة الفيس بوك';
$_['text_instagram']            = 'عنوان صفحة الانستاجرام';
$_['text_twitter']            = 'عنوان حساب التويتر ';
$_['text_selling_countries']  = 'Authorized selling Countries';
$_['text_office_address']      = 'عنوان الموقع';
$_['text_warehouse_address']    ='عنوان المستودع';
$_['text_opening_hours']            = 'ساعات العمل';
$_['text_brand_description']    = 'وصف الماركة';

$_['text_legal_info']        = 'المعلومات القانونية';
$_['text_legal_name']            = 'الاسم القانوني';
$_['text_legal_info']        = 'Legal Information';
$_['text_legal_name']        = 'Legal Name';
$_['text_brand_name']        = 'Brand Name';
$_['text_admin_contact']     = 'Administrator Name';
$_['text_nationality']        = 'Nationality';
$_['text_email_address']        = 'Email Address';
$_['text_office_num']        = 'Direct Office Phone Number';
$_['text_moblie_num']        = 'Mobile Number';
$_['text_passport_copy']        = 'Passport Copy';
$_['text_license']        = 'License';
$_['text_logo']        = 'HD Logo';



$_['text_financail_info']        = 'Financial Information';
$_['text_fin_contact_name']        = 'Company Financial officer contact Name';
$_['text_fin_email']        = 'email address';
$_['text_fin_num']        = 'Phone Number';
$_['text_beneficiary_name']        = 'Beneficiary Name';
$_['text_bank_name']        = 'Bank Name';
$_['text_bank_account']        = 'Bank Account';
$_['text_iban_code']        = 'IBAN code';
$_['text_swift_code']        = 'Swift code';
$_['text_currency']        = 'Currency';

$_['text_next']            = 'التالي';;
