<?php
// Heading
$_['heading_title']     = 'صفحة البائع' ;

//Text
$_['text_profile']     		= 'صفحة البائع';
$_['text_store']     		= 'عن المتجر';
$_['text_collection']    	= 'المجموعة';
$_['text_location']    		= 'الموقع';
$_['text_reviews']    		= 'المراجعات';
$_['text_product_reviews']  = 'مراجعات عن المنتج';
$_['text_total_products']   = 'المنتجات';

$_['text_from']     		= "We're from";
$_['text_feedback']     	= 'Feedback';
$_['text_connect']     		= 'تواصل معنا';
$_['text_write_review']		= 'اضف مراجعة';
$_['text_login_contact']	= 'الرجاء الدخول لكتابة المراجعة';
$_['text_login_review']		= 'الرجاء الدخول لإضافة المراجعة';
$_['text_message']     		= 'أرسل لنا رسالة';
$_['text_facebook']     	= 'حسابنا على الفيس بوك';
$_['text_twitter']     		= 'تابعنا على تويتر ';
$_['text_partner']     		= 'عن الشريك';
$_['text_our_collection']   = 'مجموعاتنا';
$_['text_more']     		= 'المزيد ..';
$_['text_latest_product']   = 'اخر المنتجات';
$_['text_no_location_added']   = '<div class="mp-no-location-found text-danger">لم يتم اضافة مكان من البائع</div>';
$_['text_brands_catalog']   = 'دليل الماركات';
$_['text_search_site']   = 'بحث في المنتجات ..';
$_['text_all_products']   = 'كل التصنيفات';
$_['button_cart'] = 'إشتر الان';
$_['text_main_categories'] = 'التصنيفات الرئيسية';
$_['text_follow_seller'] = 'تابع %s';
$_['text_unfollow_seller'] = 'الغ متابعة %s';
$_['text_shops_official_links']  = 'الروابط الرسمية';
$_['text_products'] = "المنتجات";
