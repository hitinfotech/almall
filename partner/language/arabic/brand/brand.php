<?php

$_['head_title']                    = 'دليل الماركات';
$_['button_cart']                   = 'إشتر الان';

$_['text_brands']                   = 'ماركات %s';
$_['text_brands_catalog']           = 'دليل الماركات';
$_['text_brands_search_brands']     = 'البحث في الماركات';
$_['text_brands_all_categories']    = 'كل التصنيفات';
$_['text_brands_shop_now']          = 'تسوق الآن';
$_['text_brands_all_products']      = 'جميع المنتجات';
$_['text_brands_read_more']         = 'اقرأ المزيد';
$_['text_brands_more']              = 'المزيد';

$_['text_shops']                    = 'المحلات التي تباع بها %s';
$_['text_more_shops']               = 'المزيد';

$_['text_brands_arabic_name']       = 'الاسم بالعربي';
$_['text_brands_english_name']      = 'الاسم بالعربي';
$_['text_brands_categories']        = 'التصنيفات';
$_['text_brands_main_links']        = 'روابط رئيسية';
$_['text_brands_products']          = 'منتجات';
$_['text_brands_search_site']       = 'البحث في الموقع';
$_['text_brands_to_shops']          = 'المحلات التي تباع بها هذه الماركة';
$_['text_all_products']             = 'منتجات من %s';
$_['text_buy_brand']                = 'يمكنك الآن شراء منتجات %s الانيقة من سيدتي مول او الاحتفاظ بها بمفضلتك.';

$_['text_error']               = 'الماركة غير موجود';

$_['heading_meta_title']    = 'تسوق من اشهر الماركات | سيدتي مول | دليل المولات والماركات العالمية في %s';
$_['heading_meta_title_with_category']    = 'تسوق من اشهر ماركات %s | سيدتي مول | دليل المولات والماركات العالمية في %s';
$_['text_meta_description'] = 'تسوق الان من جميع الماركات العالمية، شانيل، ايلي صعب، زهير مراد، روبرتو كفالي، بيرشكا، ديور، فان كليف، كارولينا هيريرا، برادا، جيفنشي، قوتشي، بولغري، بيربري في %s';
$_['text_meta_description_with_category'] = 'تسوق الان %s من جميع الماركات العالمية ، شانيل، ايلي صعب، زهير مراد، روبرتو كفالي، بيرشكا، ديور، فان كليف، كارولينا هيريرا، برادا، جيفنشي، قوتشي، بولغري، بيربري في %s';
$_['text_meta_description_details'] = '%s ، %s  في %s ، وتمتع بالتوصيل حتى باب المنزل';
$_['heading_meta_title_detail_page'] = 'تسوق من %s | سيدتي مول | دليل المولات والماركات العالمية في  %s';
$_['heading_meta_title_detail_page_with_category'] = 'تسوق #cat من #brand | سيدتي مول | دليل المولات والماركات العالمية في  %s';
$_['text_main_categories'] = 'التصنيفات الرئيسية';
