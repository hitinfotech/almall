<?php

// Heading
$_['heading_title'] = 'تسجيل الدخول';
$_['text_fblogin'] = 'الدخول عن طريق فيسبوك';

// Text
$_['text_account'] = 'الملف الشخصي';
$_['text_login'] = 'دخول';
$_['text_new_customer'] = 'مستخدم جديد';
$_['text_register'] = 'تسجيل حساب';
$_['text_register_account'] = 'لكي تقوم بإنهاء الطلب قم بإنشاء حساب جديد معنا، فهو يُمكنك من الشراء بصورة أسرع و متابعة طلبيات الشراء التي تقدمت بها, و مراجعة سجل الطلبيات القديمة واسعتراض الفواتير وغير ذلك الكثير...';
$_['text_returning_customer'] = 'تسجيل الدخول';
$_['text_i_am_returning_customer'] = 'انا عميل';
$_['text_forgotten'] = 'نسيت كلمة المرور';

// Entry
$_['entry_email'] = 'عنوان البريد الالكتروني';
$_['entry_password'] = 'كلمة المرور';

// Error
// $_['error_login']                  = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_login'] = 'إنتبه: لم يحصل تطابق بين البريد الإكتروني و كلمة السر.';
$_['error_attempts'] = 'تحذير: لقد تجاوزت عدد المحاولات المسموحة لتسجيل الدخول. الرجاء اعادة المحاولة بعد ساعة.';
$_['error_approved'] = 'تحذير : يجب الموافقة على حسابك من الادارة قبل تسجيل الدخول.';
