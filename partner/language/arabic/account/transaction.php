<?php
// Heading
$_['heading_title']      = 'رصيد الحساب';

// Column
$_['column_date_added']  = 'تاريخ الإضافة';
$_['column_description'] = 'تفاصيل';
$_['column_amount']      = 'القيمة (%s)';

// Text
$_['text_account']       = 'الملف الشخصي';
$_['text_transaction']   = 'رصيد حسابك';
$_['text_total']         = 'رصيد حسابك:';
$_['text_empty']         = 'لم يتم العثور على حركات في حسابك';