<?php
// Heading
$_['heading_title']         = 'رصيد الحساب';

// Text
$_['text_account']          = 'الملف الشخصي';
$_['text_order']            = 'معلومات الطلب';
$_['text_order_detail']     = 'تفاصيل الطلب';
$_['text_invoice_no']       = 'رقم الفاتورة:';
$_['text_order_id']         = 'رقم الطلب:';
$_['text_date_added']       = 'تاريخ الإضافة:';
$_['text_shipping_address'] = 'عنوان الشحن';
$_['text_shipping_method']  = 'طريقة الشحن:';
$_['text_payment_address']  = 'عنوان الدفع';
$_['text_payment_method']   = 'طريقة الدفع:';
$_['text_comment']          = 'تعليقات على الطلب';
$_['text_history']          = 'سجل الطلب';
$_['text_success']          = 'تنبيه: لقد أضفت <a href="%s">%s</a> إلى <a href="%s">حقيبة التسوق</a>!';
$_['text_empty']            = 'لم تقم بإضافة أي طلبية لحد الآن';
$_['text_error']            = 'لم يتم العثور على الطلب';
$_['text_track']            = 'تتبع الطلب';

// Column
$_['column_date']          = 'التاريخ';
$_['column_type']          = 'نوع العملية ';
$_['column_credit']        = 'رصيد حساب ';
$_['column_total']        = 'المجموع الكلي ';



// Error
$_['error_reorder']         = '%s غير متاح لإعادة الطلب';
$_['text_tracking']         = 'تفاصيل شحن الطلب';
$_['column_description']    = 'تفاصيل';
$_['column_amount']         = 'القيمه';
$_['column_date_added']     = 'تاريخ الاضافه';
$_['column_comment']        = 'ملاحظات';
$_['text_balance']          = 'الرصيد المتوفر';
$_['text_no_results']       = 'لا يوجد بيانات';

