<?php

// Heading
$_['heading_title']   = 'ملفات التنزيل';

// Text
$_['text_account']    = 'الملف الشخصي';
$_['text_downloads']  = 'التنزيلات';
$_['text_empty']        = 'لم تقم بطلب أي ملف رقمي للتنزيل حتى الآن!';

// Column
$_['column_order_id']   = 'رقم الطلب';
$_['column_name']       = 'اسم الملف الرقمي';
$_['column_size']       = 'حجم الملف';
$_['column_date_added'] = 'تاريخ الطلب';