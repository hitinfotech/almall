<?php

// Text
$_['text_account']     = 'الملف الشخصي';
$_['text_newsletter']  = 'المفضلة';
$_['text_success']     = 'تم التعديل !';

$_['text_notifications'] =  'قائمتي المفضلة';

$_['tab_fav_products'] = 'المنتجات ';
$_['tab_fav_sellers']= 'البائعون ';
$_['tab_fav_brands']= 'الماركات ';
$_['tab_fav_categories']= 'التصنيفات ';
$_['tab_fav_tips']= 'النصائح ';
$_['tab_fav_looks']= 'الاطلالات';

$_['add_to_cart'] = 'أضف الى  الحقيبة';

$_['no_favorite_tips'] = 'لا يوجد لديك نصائح مفضلة! <br> قم بالذهاب لصفحة  <a href="%s">النصائح</a> لاضافة نصيحة';
$_['no_favorite_looks'] = 'لا يوجد لديك إطلالات مفضلة! <br> قم بالذهاب لصفحة  <a href="%s">الإطلالات</a> لإضافة إطلالة';
$_['no_favorite_products'] ='لا يوجد لديك منتجات مفضلة! <br> قم بالذهاب لصفحة  <a href="%s">المنتجات</a> لإضافة منتجات';
$_['no_favorite_brands'] ='لا يوجد لديك ماركات مفضلة! <br> قم بالذهاب لصفحة  <a href="%s">الماركات</a> لاضافة ماركات';
$_['no_favorite_sellers'] ='لا يوجد لديك بائع مفضل! <br>';
