<?php
// Heading
$_['heading_title']      = 'معلومات الملف الشخصي';

// Text
$_['text_account']       = 'الملف الشخصي';
$_['text_edit']          = 'تعديل المعلومات';
$_['text_your_details']  = 'المعلومات الشخصية';
$_['text_success']       = 'العملية ناجحة: تم تعديل معلومات الملف الشخصي بنجاح';
$_['text_change_pic']    = 'تغيير الصورة';
$_['text_year']    		 = 'السنة';
$_['text_month']    	 = 'الشهر';
$_['text_day']    		 = 'اليوم';
$_['text_male']    		 = 'ذكر';
$_['text_female']    	 = 'أنثى';

// Entry
$_['entry_firstname']    = 'الإسم الأول';
$_['entry_lastname']     = 'الإسم الأخير';
$_['entry_email']        = 'البريد الإلكتروني';
$_['entry_telephone']    = 'تلفون';
$_['entry_fax']          = 'فاكس';
$_['entry_birthday']     = 'تاريخ الميلاد';
$_['entry_gender']       = 'الجنس';
$_['entry_country']      = 'البلد';

// Error
$_['error_exists']       = 'تنبيه: الإيميل مسجل في الموقع من قبل';
$_['error_firstname']    = 'الإسم الأول لا بد أن يتكون من 1 إلى 32 حرف';
$_['error_lastname']     = 'الإسم الأخير لا بد أن يتكون من 1 إلى 32 حرف';
$_['error_email']        = 'البريد الإلكتروني المدخل يحتوي أخطاء';
$_['error_telephone']    = 'التلفون لا بد أن يحتوي على 3 إلى 32 رقم';
$_['error_custom_field'] = '%s أساسي';


