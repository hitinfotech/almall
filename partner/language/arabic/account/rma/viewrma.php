<?php
// Heading
$_['heading_title']     	= 'تفاصيل الإرجاع';
$_['heading_title_list']	= 'تاريخ الإرجاع';
$_['heading_title_label']	= 'Return Mailing Label';

// Text
$_['text_account']      	= 'الحساب';
$_['text_from']      		= 'من ';
$_['text_to']      			= 'إلى ';
$_['text_shipping_label']   = 'بطاقة الشحن';
$_['text_auth_label']		= 'بطاقة صلاحية الإرجاع';
$_['text_rma_id']		  	= 'رقم الإرجاع :';
$_['text_print_lable']		= 'طباعة بطاقة الشحن';

$_['text_success']      	= 'تهانينا لقد تم حل مشكلة مرتجعاتك.';
$_['text_success_msg']      = 'تهانينا لقد تم حفظ رسالة الإرجاع.';
$_['text_success_reopen']   = 'تهانينا لقد تم حفظ رسالة الإرجاع وتم إعادة فتح مرتجعاتك.';
$_['text_select']			= 'اختيار';
$_['text_allowed_ex']       = 'الامتدادات المسموح بها %s الحجم الأعظم %d kB';
$_['text_error']			= 'ليس لديك الصلاحية لإعادة هذه المنتجات';

// Entry
$_['button_back'] 			= 'عودة';
$_['button_continue'] 	    = 'متابعة';
$_['button_save'] 			= 'حفظ';

// user
$_['wk_viewrma_orderid']  			= 'رقم الطلبية :';
$_['wk_viewrma_status']  			= 'الحالة ';
$_['wk_viewrma_reason']  			= 'السبب ';
$_['wk_viewrma_rma_tatus']			= 'حالة الإرجاع :';
$_['wk_viewrma_status_admin']  		= 'حالة المسؤول :';
$_['wk_viewrma_status_customer']    = 'حالة الزبون:';
$_['wk_viewrma_authno']  			= 'رقم الشحنة :';
$_['wk_viewrma_add_info']  			= 'معلومات إضافية';
$_['wk_viewrma_image']  			= 'الصور)';
$_['wk_viewrma_close_rma'] 			= 'إغلاق الإرجاع ';
$_['wk_viewrma_close_rma_text'] 	= 'الرجاء على الموافقة إن تم حلها ';
$_['wk_viewrma_item_req']  			= 'المواد المراد إعادتها ';
$_['wk_viewrma_pname']  			= 'رقم المنتج';
$_['wk_viewrma_model'] 			    = 'الموديل';
$_['wk_viewrma_price'] 			    = 'السعر';
$_['wk_viewrma_qty'] 				= 'الكمية';
$_['wk_viewrma_ret_qty'] 			= 'الكمية المعادة';
$_['wk_viewrma_subtotal']  			= 'الإجمالي مع الضريبة';
$_['wk_viewrma_enter_cncs_no']  	= 'اضغط لإدخال عدد الشحنة ';
$_['wk_viewrma_valid_no']  			= 'الرجاء إدخال رقم شحنة صالح !!';
$_['wk_viewrma_msg']  				= 'إرسال رسالة';
$_['wk_viewrma_enter_msg'] 			= 'إدخال رسالة';
// $_['wk_viewrma_submsg'] 			= 'اعتماد رسالة';
$_['wk_viewrma_reopen']  			= 'الرجاء التحقق من إعادة فتح هذا الإرجاع إن تم إلغاؤه';
$_['wk_rma_admin_return_text']    	= 'لقد قام المسؤول فعلاً بإعادة هذه الكمية المطلوب إعادتها.';
$_['wk_rma_admin_return']    		= 'الكمية المطلوب إعادتها';
$_['text_productReturn']	=	'كل المنتجات تمت إعادتها !!';
$_['wk_mprma_manage_return']	=	'الكمية المعادة';
$_['wk_mprma_manage_return_info']	=	'سوف تحتسب الكمية المعادة بناء على السعر +الضريبة+ المكافآت.';


//text
$_['text_order_details']	 = 'تفاصيل الطلبية';
$_['text_messages']			 = 'الرسائل';
$_['text_download']			 = 'اضغط للتحميل';

$_['text_print']		 	 = 'طباعة';
// $_['text_delivered']		 = 'تم التسليم';
// $_['text_not_delivered']	 = 'لم يتم التسليم بعد';
$_['text_canceled'] 		 = 'تم الحذف';
$_['text_solved']			 = 'تم حلها';
// $_['text_pending'] 		     = 'قيد التسليم';
// $_['text_processing']		 = 'Processing';
$_['text_upload_img']      	 = 'تحميل الصور';

// $_['text_pac_not_rec'] 		 = 'لم يتم تسليم الشحنة بعد';
// $_['text_pac_rec']			 = 'لقد تم استلام الشحنة';
// $_['text_pac_dis'] 		     = 'تم إرسال الحزمة';
// $_['text_return_dec']		 = 'رفض/عودة';
$_['text_upload_success']	 = 'تم التحديث بنجاح';
?>