<?php
// Heading
$_['heading_title']     	 = 'إرجاع منتج جديد';
$_['heading_title_list']	 = 'مذكرة إرجاع المنتجات';
// Text
$_['text_account']      	 = 'الحساب';
$_['text_success']      	 = 'تم بنجاح: تم حفظ طلب إرجاع المنتج';
$_['text_select']			 = 'اختيار';
// Entry
$_['button_back']  			 = 'عودة';
$_['button_continue']   	 = 'متابعة';
$_['button_save']  			 = 'حفظ';
$_['button_filter']  		 = 'تصنيف';
// user
$_['text_rmano_info']    	 = 'الرجاء إضافة رقم الشحنة إذا أردت إرجاع المنتج';
$_['text_rma_quantity']  	 = 'عدد المنتجات المراد إرجاعها ';

$_['text_select']        	 = 'لا توجد ملاحظات من قبل المسؤول';
$_['text_select_o']      	 = 'اختر طلبك';
$_['text_select_no_o']   	 = 'لا توجد طلبات !!';
$_['text_no_deliver']    	 = 'لم تسلم بعد';
$_['text_deliver']       	 = 'تم التسليم';
$_['text_i_agree']       	 = 'أوافق';
$_['text_filter_order']  	 = 'البحث حسب رقم الطلبية';
$_['text_filter_date']       = 'البحث حسب التاريخ';
$_['text_filter_model']      = 'البحث حسب نوع المنتج';
$_['text_upload_img']        = 'تحميل الصور';
$_['text_allowed_ex']        = 'الملحقات المسموحة s% الحجم الأقصى d% KB';
$_['text_order_info']        = 'الوقت المسموح لإرجاع الطلبية d% أيام';

$_['text_agree']           	 = 'لقد قرأت الشروط وأوافق <a class="agree" href="%s" alt="%s"><b>%s</b></a>';

$_['wk_addrma_order']  		 = ' اختر رقم الطلب';
$_['wk_addrma_itemorder']    = 'تفاصيل منتجات الطلب';
$_['wk_addrma_product']      = 'اسم المنتج';
$_['wk_addrma_sku']    		 = 'الموديل';
$_['wk_addrma_quantity']     = 'العدد';
$_['wk_addrma_imagespro']    = 'صورة المنتج المراد إرجاعه';
$_['wk_addrma_addinfo']      = 'معلومات إضافية ';
$_['wk_addrma_reason']       = 'اختر سبب الإرجاع';
$_['wk_addrma_rmano']        = 'ادخل رقم الشحنة ';
$_['wk_addrma_status']       = 'وضع تسليم الشحنة ';
$_['error_too_much_images']  = 'تحذير: يرجى تحديد صورك قبل تحميلها، فهي تؤثر على باقي معلومات النموذج !!';
$_['error_order']   		 = 'تحذير : يرجى تحديد الطلب  !!';
$_['error_product']   		 = 'تحذير: يرجى تحديد المنتجات المراد إرجاعها  !!';
$_['error_product_qty']   	 = 'تحذير: يرجى تحديد عدد المنتجات  !!';
$_['error_product_qty_error']= 'تحذير: هذه المنتجات المطلوبة أقل عدد من التي لديك !!';
$_['error_reason']   		 = 'تحذير: يرجى توضيح سبب الإرجاع !!';
$_['error_status']   		 = 'تحذير: يرجى تحديد حالة طلب الإرجاع  !!';
$_['error_agree']   		 = 'تحذير: يرجى الموافقة على شروط وبنود سياسة إرجاع المنتجات !!';
$_['error_info']   			 = 'تحذير: يرجى إضافة معلومات أخرى عن طلب إرجاع المنتج بين 1 الى 4000!!';
?>
