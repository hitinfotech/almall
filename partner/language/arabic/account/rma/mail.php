<?php
################################################################################################
# RMA for Opencart 1.5.1.x From webkul http://webkul.com  	  	       #
################################################################################################
/*
This language file also contain some data which will be used to send more information data to admin / customer and that's why because we can not write that data directly because that will change condition to condition so we used sort codes.
i explaining few here like
- {config_logo} - will be your Store logo
- {config_name} - will be your Store Name
- {customer_name} - will be your Customer Name related to this RMA
- {order_id} - will be your Order Id for this RMA
- {link} - will be <a> link for your site according to condition 
- {rma_id} - will be your RMA Id for this RMA
- {customer_message} - will be Customer Message which will send to admin
[NIk] 
*/

//RMA Generate
$_['generate_admin_subject']    	  = $_['generate_customer_subject']    	  =	$_['generate_seller_subject'] = 'تم إنشاء طلب إرجاع المشتريات الخاص بالطلبية'; 

$_['generate_admin_message']  = 
'{config_logo}
Hi {config_owner},

Customer {customer_name} has been generated RMA for Order #{order_id}.
Reply ASAP.

Thanks,
{config_name}';

$_['generate_customer_message']  = 
'{config_logo}
 ,{customer_name}مرحبا 

.{order_id}#تم إنشاء طلب إرجاع المشتريات الخاص بك للطلبية 
.سوف نقوم بالرد عليك في أقرب وقت ممكن وذلك بعد مراجعة طلبك
 .{link}يمكنك مراجعة تفاصيل طلب إرجاع المشتريات الخاص بك هنا 

,شكراً
{config_name}';

$_['generate_seller_message'] = '
,{seller_name}مرحبا

.{order_id}# بإنشاء طلب إرجاع مشتريات للطلبية ذات الرقم{customer_name}لقد قام العميل 
.يرجى الرد في أقرب وقت ممكن 

,شكراً
{config_name}';

//customer send message to admin
$_['message_to_admin_subject']    	  = 'أرسل العميل رسالة بما يخص طلب إرجاع المشتريات'; 
$_['message_to_admin_message']  = 
'{config_logo}
Hi {config_owner},

Customer {customer_name} sent message regarding RMA Id - #{rma_id}.
{customer_message}

Thanks,
{config_name}';

//customer message send message to seller
$_['message_to_seller_subject'] = 'أرسل العميل رسالة بما يخص طلب إرجاع المشتريات';
$_['message_to_seller_message'] = 
'{config_logo}
,{seller_name}مرحبا 
.{rma_id}#-رسالة بما يخص طلب إرجاع المشتريات ذو الرقم {customer_name} أرسل العميل  
Message: {customer_message}


,شكراًّ
{config_name}';

//customer changed status
$_['status_to_admin_subject']    	  = $_['status_to_customer_subject']    	  = 'تم تغيير حالة طلب إرجاع المشتريات'; 
$_['status_to_admin_message']  = 
'{config_logo}
Hi {config_owner},

Customer {customer_name} changed RMA #{rma_id} status to following - %s.

Thanks,
{config_name}';

$_['status_to_customer_message']  = 
'{config_logo}
,{customer_name}مرحبا  

 . %s-إلى وضع المتابعة من قبل الفريق {rma_id}# لقد تم تحديث وضع طلبك لإرجاع المشتريات 

,شكراً
{config_name}';