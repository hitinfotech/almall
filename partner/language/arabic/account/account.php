<?php
// Heading
$_['heading_title']      = 'الملف الشخصي';

// Text
$_['text_account']       = 'الملف الشخصي';
$_['text_my_account']    = 'حسابي';
$_['text_my_orders']     = 'طلباتي';
$_['text_my_newsletter'] = 'النشرة البريدية';
$_['text_edit']          = 'تعديل معلومات حسابي';
$_['text_password']      = 'تغيير كلمة المرور';
$_['text_address']       = 'تحديث مدخلات سجل العناوين';
$_['text_wishlist']      = 'تعديل قائمة رغباتي';
$_['text_order']         = 'عرض سجل الطلبات';
$_['text_download']      = 'التنزيلات';
$_['text_reward']        = 'نقاط المكافآت الخاصة بك'; 
$_['text_return']        = 'عرض الطلبات المعادة';
$_['text_transaction']   = 'عرض رصيدك المتوفر'; 
$_['text_newsletter']    = 'النشرة البريدية';
$_['text_recurring']     = 'المدفوعات الدورية';
$_['text_transactions']  = 'رصيد العميل';
$_['text_change_pic']    = 'تغيير الصورة';
$_['txt_edit_profile']   = 'تعديل الملف الشخصي';
$_['txt_member_since']   = 'عضو منذ';
$_['text_birthdate']     = 'تاريخ الميلاد';