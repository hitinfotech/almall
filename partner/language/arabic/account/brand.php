<?php

// Heading
$_['heading_title'] = 'ماركاتي المفضلة';

// Text
$_['text_account']  = 'الملف الشخصي';
$_['text_instock']  = 'متوفر';
$_['text_brand'] = 'قائمة رغباتك (%s)';
$_['text_login']    = 'يجب عليك <a href="%s">تسجيل الدخول</a> أو <a href="%s">إنشاء حساب جديد</a> لحفظ <a href="%s">%s</a>في قائمة <a href="%s"> رغباتك</a> !';
$_['text_success']  = 'تم : لقد قمت بإضافة <a href="%s">%s</a> إلى <a href="%s">ماركاتك المفضلة</a> !';
$_['text_remove']   = 'تم تعديل قائمة ماركاتك المفضلة !';
$_['text_empty']    = 'قائمة ماركاتك المفضلة فارغة.';

// Column
$_['column_image']  = 'صورة';
$_['column_name']   = 'اسم الماركة';
$_['column_model']  = 'النوع';
$_['column_stock']  = 'حالة التوفر';
$_['column_price']  = 'سعر الوحدة';
$_['column_action'] = 'تحرير';

$_['shop_now'] = 'تسوق الان';
$_['price'] = 'السعر ';
