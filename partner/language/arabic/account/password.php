<?php
// Heading
$_['heading_title']  = 'تغيير كلمة السر';

// Text
$_['text_account']   = 'الملف الشخصي';
$_['text_current_password']  = 'كلمة السر الحالية';
$_['text_password']  = 'كلمة السر';
$_['text_success']   = 'العملية ناجحة: تم تغيير كلمة السر بنجاح';

// Entry
$_['entry_current_password'] = 'كلمة السر الحالية';
$_['entry_password'] = 'كلمة السر';
$_['entry_confirm']  = 'تأكيد كلمة السر';

// Error
$_['error_current_password'] = 'خطأ في كلمة السر الحالية';
$_['error_password'] = 'كلمة السر لابد أن تكون من 4 إلى 20 حرف';
$_['error_confirm']  = 'كلمة السر لم تتطابق';