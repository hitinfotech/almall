<?php
// Heading
$_['heading_title']        = 'تسجيل حساب على سيدتي مول';
$_['heading_title2']        = 'تسجيل حساب جديد';
// Text
$_['text_account']         = 'الملف الشخصي';
$_['text_register']        = 'التسجيل';
$_['text_do_register']     = 'تسجيــل';
$_['text_account_already'] = 'إذا كنت تمتلك حساب على موقعنا، فتفضل بتسجيل دخولك هنا <a href="%s">تسجيل الدخول</a>.';
$_['text_account_already'] = '
    أكمل طلب التسجيل أدناه بمعلوماتك للتمكن من تسجيل حساب جديد على سيدتي مول .  <p>
إذا كنت تمتلك حساب على موقعنا، فتفضل بتسجيل دخولك هن <a href="#" data-toggle="modal" data-target="#logIn">تسجيل الدخول</a> </p>.  

';
$_['text_your_details']    = 'معلوماتك الشخصية';
$_['text_your_address']    = 'عنوانك';
$_['text_newsletter']      = 'أود الاشتراك بالمجلة الالكترونية لإستقبال أخر العروض والتخفيضات ونصائح التسوق والموضة وأحدث ما نزل في الاسواق .';
$_['text_your_password']   = 'كلمة المرور';
$_['text_agree']           = 'لقد قرأت و وافقت على <a href="%s" class="agree"><b>%s</b></a>';
$_['text_or']           	= ' أو ';
$_['registerfaild']           	= 'فشل التسجيل من خلال الفيسبوك الرجاء التسجيل هنا';

$_['text_fbRegistration']  = 'التسجيل عن طريق الفيسبوك';

// Entry
$_['entry_customer_group'] = 'مجموعه العميل';
$_['entry_firstname']      = 'الإسم الأول';
$_['entry_lastname']       = 'اسم العائلة';
$_['entry_email']          = 'البريد الإلكتروني';
$_['entry_telephone']      = 'تلفون';
$_['entry_fax']            = 'فاكس';
$_['entry_company']        = 'الشركة';
$_['entry_address_1']      = 'عنوان';
$_['entry_address_2']      = 'عنوان 2';
$_['entry_postcode']       = 'الرمز البريدي';
$_['entry_city']           = 'المدينة';
$_['entry_country']        = 'البلد';
$_['entry_zone']           = 'المدينة';
$_['entry_newsletter']     = 'إشترك';
$_['entry_password']       = 'كلمة المرور';
$_['entry_confirm']        = 'تأكيد كلمة المرور';
$_['entry_gender']         = 'الجنس';
$_['entry_gender_male']    = 'ذكر';
$_['entry_gender_female']  = 'أنثى';
$_['entry_birthdate']		= 'تاريخ الميلاد';

// Error
$_['error_exists']         = 'تحذير: البريد الالكتروني تم تسجيلة مسبقا!';
$_['error_firstname']      = 'الاسم الاول يجب ان يكون من 2-32 حرف!';
$_['error_lastname']       = 'اسم العائلة يجب ان يكون من 2-32 حرف!';
$_['error_email']          = 'البريد الالكتروني غير صحيح!';
$_['error_telephone']      = 'رقم الهاتف يجب ان يكون من 3-32 رقم!';
$_['error_address_1']      = 'العنوان يجب ان يكون من 1- 128 حرف!';
$_['error_city']           = 'المدينة يجب ان تكون من 2 -128 حرف!';
$_['error_postcode']       = 'الرمز البريدي يجب ان يكون من 2-10 حروف!';
$_['error_country']        = 'الرحاء اختيار الدولة!';
$_['error_zone']           = 'الرجاء اختيار المنطقة!';
$_['error_custom_field']   = '%s اجباري!';
$_['error_password']       = 'كلمه المرور يجب ان تكون من 4-20 حرف!';
$_['error_birthdate']        = 'الرجاء ادخال تاريخ الميلاد';
$_['error_confirm']        = 'كلمه المرور غير متوافقة!';
$_['error_agree']          = 'تحذير : يجب ان توافق على  %s!';