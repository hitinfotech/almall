<?php
################################################################################################
# RMA for Opencart 1.5.1.x From webkul http://webkul.com  	  	       #
################################################################################################
/*
This language file also contain some data which will be used to send more information data to admin / customer and that's why because we can not write that data directly because that will change condition to condition so we used sort codes.
i explaining few here like
- {config_logo} - will be your Store logo
- {config_name} - will be your Store Name
- {customer_name} - will be your Customer Name related to this RMA
- {order_id} - will be your Order Id for this RMA
- {link} - will be <a> link for your site according to condition 
- {rma_id} - will be your RMA Id for this RMA
- {admin_message} - will be Admin Message which will send to customer
[NIk] 
*/

//admin send message to customer
$_['message_to_customer_subject']    	  = 'رسالة بخصوص طلب الإرجاع'; 
$_['message_to_customer_message']  = 
'{config_logo}
,{اسم العميل}مرحبا

{config_name} تم تغيير حالة طلب الإرجاع #{rma_id} إلى المتابعة - %s .
{admin_message}

شكراً,
{config_name}';

//customer changed status
$_['label_to_customer_subject']    	  =  'Shipping label Regarding your RMA'; 
$_['label_to_customer_message']  = 
'{config_logo}
Hi {customer_name},

{seller_name} sent you shipping label regarding RMA #{rma_id}, you can use that to return product(s).
{link}

Thanks,
{config_name}';

?>