<?php
// Heading
$_['heading_title']     = 'الملف الشخصي';

// Text
$_['text_success']      			= 'تم تعديل ملفك الشخصي بنجاح';
$_['text_select']					= 'اختر';
$_['text_firstname']				= 'الاسم الاول';
$_['text_lastname']					= 'الاسم الاخير';
$_['text_email']					= 'الايميل';
$_['text_sef_url']      			= 'الرجاء عدم استخدام الرموز الخاصة, استخدم اشارة - بدلا منها';
$_['text_male']						= 'ذكر';
$_['text_female']					= 'انثى';
$_['text_view_store']				= 'إظهار المتجر';
$_['text_account']      			= 'الملف الشخصي';
$_['text_view_profile']				= 'اظهار الملف الشخصي';

//profile
$_['text_screen_name']				='SEF Keyword for Shop';
$_['text_gender']					='الجنس';
$_['text_short_profile']			='Short profile';
$_['text_avatar']					='الصورة الشخصية';
$_['text_twitter_id']				='حساب التويتر';
$_['text_facebook_id']				='حساب الفيس بوك';
$_['text_theme_background_color']	='Theme';
$_['text_company_banner']			='Company Banner';
$_['text_company_logo']				='Company Logo';
$_['text_company_locality']			='Company Locality';
$_['text_company_name']				='إسم الشركة';
$_['text_company_description']		='وصف الشركة';
$_['text_country_logo']				='الدولة';
$_['text_otherpayment']				='Other Payment Info';
$_['text_payment_mode']				='Payment Mode';
$_['text_profile']					='Profile';
$_['text_payment_detail']			='Paypal ID';
$_['text_account_information']		='Account Information';

//hover
$_['hover_avatar']					=' 200px X 200px';
$_['hover_banner']					=' 1130px X 150px';
$_['hover_company_logo']			=' 200px X 200px';

//tab
$_['tab_general']					='عام';
$_['tab_profile_details']			='تفاصيل الملف الشخصي';
$_['tab_paymentmode']				='Payment Mode';

$_['text_general']					= 'تفاصيل الزبون';
$_['text_profile_info'] 			= 'Add your details about you and your shop which will display to customers like name, company logo, name etc.';
$_['text_paymentmode']				= 'Fill your Bank Account Details';

$_['text_edit'] = 'Edit';
$_['text_remove'] = 'Remove';


//warning
$_['warning_become_seller']			= 'For Become Seller inform Admin';


// Error
$_['error_exists']     				= ' Warning: E-Mail address is already registered!';
$_['error_check_form']     			= ' Warning: Please check the form carefully for error(s)';
$_['error_seo_keyword']     		= ' SEO keyword can not be blank';
$_['error_company_name']     		= ' Company name can not be blank';
$_['error_firstname']  				= ' First Name must be between 1 and 32 characters!';
$_['error_lastname']   				= ' Last Name must be between 1 and 32 characters!';
$_['error_email']      				= ' E-Mail Address does not appear to be valid!';
$_['error_telephone']  				= ' Telephone must be between 3 and 32 characters!';
$_['error_paypal']  				= ' Enter valid Paypal ID';

// image file upload check error
$_['error_filename']   = 'Warning: Filename must be a between 3 and 255!';
$_['error_folder']     = 'Warning: Folder name must be a between 3 and 255!';
$_['error_exists']     = 'Warning: A file or directory with the same name already exists!';
$_['error_directory']  = 'Warning: Directory does not exist!';
$_['error_filetype']   = 'Warning: Incorrect file type!';
$_['error_upload']     = 'Warning: File could not be uploaded for an unknown reason!';
?>
