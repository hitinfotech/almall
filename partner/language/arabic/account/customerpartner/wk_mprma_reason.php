<?php
################################################################################################
# wk_rma_admin for Opencart 1.5.1.x From webkul http://webkul.com  	  	       #
################################################################################################

// Heading Goes here:
$_['heading_title']      	  = 'سبب طلب الإرجاع ';
$_['heading_title_insert']    = 'سبب جديد ';
$_['heading_title_update']    = 'تحديث السبب';

// Text
$_['text_account']     	 	  = 'الحساب';
$_['text_module']     	 	  = 'الوحدات';
$_['text_form']     	 	  = 'إضافة سبب';
$_['text_edit_form']   	 	  = 'تحديث السبب';

$_['text_success']    	 	  = 'لقد قمت بتعديل سبب طلب الإرجاع بنجاح .';
$_['text_success_insert']     = 'لقد قمت بإضافة سبب جديد لطلب الإرجاع بنجاح .';
$_['text_success_update']     = 'لقد قمت بتعديل سبب طلب الإرجاع بنجاح .';

// Entry
$_['text_disable']        		  = 'تعطيل';
$_['text_enable']      		  	  = 'تشغيل';
$_['text_no_records']      		  = 'لا يوجد تسجيلات !!';
$_['text_list']      		  	  = 'قائمة بالأسباب';
$_['entry_status']      		  = 'الحالة';
$_['entry_reason']      	      = 'السبب';

$_['button_clrfilter']    	      = 'إزالة التصفية';
$_['button_filter']					= 'التصفية';
$_['button_back']    	          = 'عودة';
$_['button_save']    	          = 'حفظ';
$_['button_insert']    	          = 'إضافة سبب';
$_['button_cancel']    	          = 'إلغاء';
$_['button_delete']    	          = 'حذف';
$_['text_edit']    	          	  = 'تعديل';
// Error
$_['error_delete']  	  	  	  = 'تحذير: إن قمت بحذف سبب طلب الإرجاع سوف يتم حذف بعض التفاصيل المتعلقة فيه !!';
$_['error_permission']  	 	  = 'تحذير: ليس لديك الصلاحية لتحديث سبب طلب الإرجاع !!';
$_['error_reason']  	  	  	  = 'تحذير: يجب أن يتكون السبب من 5 إلى 50 حرف !!';
?>