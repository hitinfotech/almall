<?php
// Heading
$_['heading_title']     		   = 'اللوحة الرئيسية';

// Text
$_['text_account']     			   = 'الملف الشخصي';
$_['text_dashboard'] 			   = 'اللوحة الرئيسية';
$_['text_order_total']             = 'عدد الطلبات';
$_['text_customer_total']          = 'عدد الزبائن';
$_['text_sale_total']              = 'عدد المبيعات';
$_['text_online_total']            = 'الاشخاص المتواجدين حاليا';
$_['text_map']                     = 'خريطة العالم';
$_['text_sale']                    = 'تحليل المبيعات';
$_['text_activity']                = 'اخر النشاطات';
$_['text_recent']                  = 'اخر الطلبات';
$_['text_order']                   = 'الطلبات';
$_['text_customer']                = 'الزبائن';
$_['text_day']                     = 'هذا اليوم';
$_['text_week']                    = 'هذا الاسبوع';
$_['text_month']                   = 'هذا الشهر ';
$_['text_year']                    = 'هذه السنة';
$_['text_view']                    = 'المزيد ...';
$_['text_no_results']              = 'لا يوجد نتائج حالي!';


// Error
$_['text_error']      			   = 'Warning: You are not Authorized !';
?>
