<?php
// Heading
$_['heading_title']     = 'تفاصيل الطلبات';

// Text
$_['text_account']      = 'الملف الشخصي';
$_['text_orderdetaillist']  = 'قائمة تفاصيل طلباتك';
$_['text_added_date']  = 'تاريخ الاضافة';
$_['text_comment']  = 'تعليق';
$_['text_orderstatus']  = 'حالة الطلب';
$_['text_no_results']  = 'لا يوجد نتائج';
$_['text_orderinfo']   = 'معلومات الطلب';


?>
