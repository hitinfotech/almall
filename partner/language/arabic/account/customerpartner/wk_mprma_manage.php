<?php
################################################################################################
# wk_rma_admin for Opencart 1.5.1.x From webkul http://webkul.com  	  	       #
################################################################################################

// Heading Goes here:
$_['heading_title']      	  = 'نظام سياسة الاسترجاع ';
$_['heading_title_details']   = 'تفاصيل سياسة الاسترجاع ';
$_['heading_title_setting']   = 'ضبط سياسة الاسترجاع ';

// Text
$_['text_account']      		  = 'الحساب';
$_['text_success']    	 	  = 'لقد قمت بتحديث طلب الاسترجاع الخاص بك بنجاح !';
$_['text_success_setting']    = 'لقد قمت بتحديث نظام ضبط طلب الاسترجاع بنجاح!';
$_['text_list']      		  = 'قائمة طلبات الاسترجاع الخاصة بالعملاء';
$_['text_edit']      		  = 'المزيد';
$_['button_clrfilter'] 		  = 'إزالة التصفية';
$_['button_invoice'] 		  = 'طباعة الفاتورة';
$_['button_alert'] 		  	  = 'ظهور التنبيه';
$_['button_table'] 		  	  = 'ظهور الجدول';
$_['text_no_recored']      	  = 'لا توجد تسجيلات!!';
$_['text_form'] 		  	  = 'إظهار رقم طلب الإرجاع ';
$_['text_shipping_lable'] 	  = 'بطاقة الشحن';
$_['text_confirm'] 	  = 'هل أنت متأكد من حذف طلب الإرجاع?';

$_['text_shipping_info'] 	  = 'An identification label affixed to a container which specifies contents of the shipping container. If the merchandise is subject to any inspections such as an FDA inspection, the shipping label must contain that inspection information as well. A shipping label used by a mail carrier lists the originating and destination addresses. <a href="http://www.businessdictionary.com/definition/shipping-label.html#ixzz3K42zCxi9" target="_blank">Read more</a> <br/> <b>You can upload and send label from here and can select from previously added label(s).</b>';

//user
$_['text_invoice']    			  		 	  = 'رخصة إرجاع البضائع';
$_['text_rmaid']    			  		 	  = 'رقم رخصة طلب الإرجاع';
$_['text_images']    			  		 	  = 'الصور';
$_['text_customer_tracking']    			  = 'رقم تتبع الشحنة. : ';
$_['text_no_option']    			  		  = 'لا يوجد حالة متوفرة';
$_['text_quantity']    			  			  = 'الكمية المعادة إلى المخزن';
$_['text_canceled']    			  		 	  = 'تم إلغاؤه من قبل العميل';
$_['text_solved']    			  			  = 'تم حله من قبل العميل';
$_['text_download']    			  			  = 'ملحق';
$_['wk_viewrma_msg']    			  		  = 'إرسال رسالة';
$_['wk_viewrma_shipping_label']    			  = 'حفظ وإرسال البطاقة';

$_['wk_rma_admin_return_text']    			  = 'لقد قمت فعلاً بإعادةهذه الكمية من البضائع .';
$_['wk_rma_admin_return_info']    			  = 'سوف تحتسب الكمسة المعادة بناء على السعر+الضريبة+ المكافآت .';
$_['wk_rma_admin_return']    			  	  = 'الكمية المعادة';
$_['wk_rma_admin_images']    			  	  = 'صور طلب الإرجاع';
$_['wk_rma_admin_id']    			  		  = 'الحالة';
$_['wk_rma_admin_product']    			  	  = 'المنتجات';
$_['wk_rma_admin_cid']    			  		  = 'رقم الزبون';
$_['wk_rma_admin_cname']    			 	  = 'اسم الزبون ';
$_['wk_rma_admin_oid']    			  		  = 'رقم الطلبية ';
$_['wk_rma_admin_reason']    		  		  = 'السبب ';
$_['wk_rma_admin_date']    			  		  = ' التاريخ';
$_['wk_rma_admin_rmastatus']    	  		  = 'حالة طلب الإرجاع ';
$_['wk_rma_admin_customerstatus']  	  		  = 'حالات الزبون ';
$_['wk_rma_admin_authno']   	  		 	  = 'رقم الشحنة ';
$_['wk_rma_admin_add_info']  	  		  	  = 'معلومات إضافية ';
$_['wk_rma_admin_msg']  			  		  = 'رسالة';
$_['wk_rma_admin_basic']  			  		  = 'تفاصيل طلب الإرجاع ';
$_['wk_rma_admin_msg_tab']		  	  		  = 'المحاذثة';
$_['wk_rma_admin_policy']		  	  		  = 'إدخال توصيف سياسة الإرجاع';

$_['text_lable_image']	  	  	  = 'الصورة';
$_['text_lable_name']	  	  	  = 'الاسم';
$_['text_select']	=	' -- اختار -- ';
$_['text_decline']	=	'رفض';
$_['text_return']	=	'إعادة';
$_['text_complete']	=	'اكتمل';
$_['wk_rma_sellerStatus'] = 'حالة البائع';
$_['help_sellerStatus']			= 'البائع يستطيع تغيير حالته هنا!';

// Entry
$_['entry_name']      	          = 'اسم';
$_['button_back']    	          = 'عودة';
$_['button_save']    	          = 'حفظ';
$_['button_cancel']    	          = 'حذف';
$_['button_delete']    	          = 'مسح';
$_['button_filter']    	          	  = 'تصفية';
$_['text_edit']    	          	  = 'تعديل';

// Error
$_['error_label']  	 		  = 'تحذير: رجاء تحديد أو تحميل التسمية لهذا !!';
$_['error_permission']  	  = 'تحذير: ليس لديك الصلاحية لتحديث طلب الإرجاع !!';
?>