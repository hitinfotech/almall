<?php
// Heading
$_['heading_title'] = 'تحليل المبيعات';

// Text
$_['text_order']    = 'الطلبات';
$_['text_customer'] = 'الزبائن';
$_['text_day']      = 'هذا اليوم';
$_['text_week']     = 'هذا الاسبوع';
$_['text_month']    = 'هذا الشهر';
$_['text_year']     = 'هذه السنة';
