<?php
// Heading
$_['heading_title']     = 'اخر الطلبات';

// Column
$_['column_order_id']   = 'رقم الطلب';
$_['column_customer']   = 'الزبون';
$_['column_status']     = 'الحالة';
$_['column_total']      = 'المجموع';
$_['column_date_added'] = 'تاريخ الاضافة';
$_['column_action']     = 'الإجراء';
