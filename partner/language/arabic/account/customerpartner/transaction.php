<?php
// Heading
$_['heading_title']    	     = 'حركات الشراء';

// Text
$_['text_account']     		 = 'الملف السخصي';
$_['text_transactionList']	=	'لائحة حركات الشراء';
$_['text_transactionId']	=	'رقم الحركة';
$_['text_transactionAmount']	=	'قيمة الحركة';
$_['text_transactionDetails']	=	'تفاصيل الحركه';
$_['text_transactionDate']	=	'تاريخ الحركة';

// Button
$_['button_save']			 = 'حفظ';
$_['button_back']			 = 'رجوع';
$_['button_cancel']			 = 'الغاء';
$_['button_insert']			 = 'اضف';
$_['button_delete']			 = 'حذف';
$_['button_filter']			 = 'اظهر النتائج';

// Entry
$_['entry_id']        		 = 'الرقم';
$_['entry_transaction']      = 'حركة الشراء';
$_['entry_details']          = 'التفاصيل';
$_['entry_amount']      	 = 'قيمة الحركة';
$_['entry_date']         	 = 'تاريخ الاضافة';
$_['entry_seller']         	 = 'البائع';

$_['entry_total']            = 'مجموع الدخل: ';
$_['entry_paid']      	     = 'مجموع المدفوع للبائع:';
$_['entry_admin']         	 = 'دخل المشرف';
$_['entry_customer']         = 'دخل البائع';

// Error

// Info

?>