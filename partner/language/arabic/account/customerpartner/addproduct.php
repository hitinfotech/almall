<?php
// Heading
$_['heading_title']     		= 'إضافة منتج جديد';
$_['heading_title_update']     	= 'تعديل المنتج';
$_['heading_title_productlist'] ='قائمة المنتجات';

// Text
$_['text_soldlist_info']		 = 'إضغط هنا لمزيد من التفاصيل ';
$_['text_account']     			 = 'الملف الشخصي';
$_['text_product']  			 = 'المنتج';
$_['text_product_details'] 		 = 'تفاصيل المنتج';
$_['text_success']      		 = 'تم حفظ المنتج بنجاح';
$_['text_select']				 = 'اختر';
$_['text_categories']		     = 'الفئات';
$_['text_add_image']		     = 'أضف صورة';
$_['text_access']			     = 'لا يوجد لديك صلاحية للتعديل على هذا المنتج';
$_['text_confirm']        		 = 'هل انت متاكد انك تريد حذف هذا المنتج';

// Entry
$_['entry_productcategory']  	 = 'فئات المنتج';
$_['entry_productname']     	 = 'اسم المنتج';
$_['entry_description_short']	 = 'وصف تعريفي';
$_['entry_stock'] 				 ='الكمية';
$_['entry_productmodel']	 	 ='الموديل';
$_['entry_specialprice']		 ='سعر خاص';
$_['entry_specialtimestart']	 ='تاريخ بدء الاسعار الخاصة';
$_['entry_specialtimeend']   	 ='تاريخ انتهاء الاسعار الخاصة';
$_['entry_seokeyword']			 ='Seo Keyword';
$_['entry_stockavailable']  	 ='إنتهى من المخزن';


// Error
$_['error_warning']      	 = 'الرجاء تعبئة كل الحقول';
$_['error_extension']    	 = 'الرجاء ادخال امتداد صورة صحيح';
$_['error_size'] 		 	 = 'الرجاء اختيار صورة حجمها اصغر من  ';
$_['error_filetype'] 		 = 'خطا في نوع الملف ';
$_['error_filename'] 		 = 'خطأ في اسم الملف';
$_['error_upload'] 		     = 'لم يتم تحميل الملف';

// Button
$_['button_add_attribute']   = 'أضف صفة';
$_['button_add_option']      = 'أضف  خيارا';
$_['button_add_option_value']= 'AddOptionValue';
$_['button_add_discount']    = 'أضف خصما';
$_['button_add_special']     = 'أضف سعر خاص';
$_['button_add_image']       = 'أضف صورة';
$_['button_remove']          = 'حذف';
$_['button_insert']          = 'إضافة';
$_['button_copy']            = 'نسخ';
$_['button_filter']          = 'عرض النتائج';

// Text
$_['text_success_update']    = 'تم تعديل المنتج بنجاح!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'الافتراضي';
$_['text_image_manager']     = 'مدير المنتجات';
$_['text_browse']            = 'عرض';
$_['text_clear']             = 'حذف';
$_['text_option']            = 'خيارات';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'النسبة';
$_['text_amount']            = 'قيمة ثابتة';
$_['text_enabled']           = 'مفعل';
$_['text_disabled']          = 'غير مفعل';
$_['text_choose']            = 'اختر';
$_['text_input']             = 'إدخال';
$_['text_file']              = 'ملف';
$_['text_date']              = 'تاريخ';
$_['text_edit']              = 'تعديل';
$_['text_no_results']        = 'لم يتم ادخال نتائج';
$_['text_change_base']       = 'Click to Change Base Image';

// Column
$_['column_name']            = 'اسم المنتج';
$_['column_model']           = 'الموديل';
$_['column_image']           = 'الصورة';
$_['column_price']           = 'السعر';
$_['column_quantity']        = 'الكمية';
$_['column_sold']        	 = 'الكمية مباعة';
$_['column_earned']          = 'الربح';
$_['column_status']          = 'الحالة';
$_['column_action']          = 'الاجراء';

// Tab
$_['tab_general']            = 'عام';
$_['tab_data']           	 = 'التاريخ';
$_['tab_attribute']          = 'الصفة';
$_['tab_option']           	 = 'خيار';
$_['tab_discount']        	 = 'خصم';
$_['tab_special']            = 'سعر خاص';
$_['tab_image']          	 = 'الصور';
$_['tab_links']        		 = 'وصلات خارجية';
$_['tab_reward']          	 = 'مكافئة';

// Entry
$_['entry_name']             = 'اسم المنتج';
$_['entry_meta_title'] 	 	 = 'عنوان تعريفي';
$_['entry_meta_keyword'] 	 = 'كلمات تعريفيه مفتاحية';
$_['entry_meta_description'] = 'وصف تعريفي';
$_['entry_description']      = 'الوصف';
$_['entry_store']            = 'المتاجر';
$_['entry_keyword']          = 'SEO Keyword';
$_['entry_model']            = 'الموديل';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'المكان';
$_['entry_shipping']         = 'يحتاج لسحن';
$_['entry_manufacturer']     = 'المنتج';
$_['entry_date_available']   = 'تاريخ الاتاحة';
$_['entry_quantity']         = 'الكمية';
$_['entry_minimum']          = 'أقل كمية';
$_['entry_stock_status']     = 'انتهى من المخزن';
$_['entry_price']            = 'السعر';
$_['entry_tax_class']        = 'Tax Class';
$_['entry_points']           = 'النقاط';
$_['entry_option_points']    = 'النقاط';
$_['entry_subtract']         = 'SubtractStock';
$_['entry_weight_class']     = 'Weight Class';
$_['entry_weight']           = 'Weight';
$_['entry_length']           = 'Length Class';
$_['entry_dimension']        = 'Dimensions (L x W x H)';
$_['entry_image']            = 'الصورة';
$_['entry_customer_group']   = 'CustomerGroup';
$_['entry_date_start']       = 'تاريخ البدء';
$_['entry_date_end']         = 'تاريخ الانتهاء ';
$_['entry_priority']         = 'الاولوية';
$_['entry_attribute']        = 'الصفة';
$_['entry_attribute_group']  = 'مجموعة الصفة';
$_['entry_text']             = 'نص';
$_['entry_option']           = 'خيار';
$_['entry_option_value']     = 'OptionValue';
$_['entry_required']         = 'مطلوب';
$_['entry_status']           = 'الحالة';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_category']         = 'الفئات';
$_['entry_filter']           = 'Filters';
$_['entry_download']         = 'التنزيلات';
$_['entry_related']          = 'منتجات ذات علاقة';
$_['entry_tag']              = 'كلمات دلاليه للمنتج';
$_['entry_reward']           = 'Reward Points';
$_['entry_layout']           = 'Layout Override';
$_['entry_profile']          = 'الملف الشخصي';
// custom field
$_['text_custom_field']        	 = 'حقل محدد';
$_['entry_select_option']          = 'اختر خيارا :';
$_['entry_select_date']          = 'اختر تاريخا :';
$_['entry_select_datetime']          = 'اختر تاريخ-وقت :';
$_['entry_select_time']          = 'اختر وقت :';
$_['entry_enter_text']          = 'ادخل نصا :';

//help
$_['help_keyword'] 				= 'Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.';
$_['help_sku']					= 'Stock Keeping Unit';
$_['help_upc']					= 'Universal Product Code';
$_['help_ean']					= 'European Article Number';
$_['help_jan']					= 'Japanese Article Number';
$_['help_isbn']					= 'International Standard Book Number';
$_['help_mpn']					= 'Manufacturer Part Number';
$_['help_manufacturer']			= '(Autocomplete)';
$_['help_minimum']				= 'Force a minimum ordered quantity';
$_['help_stock_status']			= 'Status shown when a product is out of stock';
$_['help_points']				= 'Number of points needed to buy this item. If you don\'t want this product to be purchased with points leave as 0.';
$_['help_category']				= '(Autocomplete)';
$_['help_filter']				= '(Autocomplete)';
$_['help_download']				= '(Autocomplete)';
$_['help_related']				= '(Autocomplete)';
$_['help_tag']					= 'comma separated';
$_['help_length']				= 'Length';
$_['help_width']				= 'Width';
$_['help_height']				= 'Height';
$_['help_weight']				= 'Weight';
$_['help_image']				= 'jpg/JPG, jpeg/JPEG, gif/GIF, png/PNG only';






$_['text_recurring_help']    = 'Recurring amounts are calculated by the frequency and cycles. <br />For example if you use a frequency of "week" and a cycle of "2", then the user will be billed every 2 weeks. <br />The length is the number of times the user will make a payment, set this to 0 if you want payments until they are cancelled.';
$_['text_recurring_title']   = 'Recurring payments';
$_['text_recurring_trial']   = 'Trial period';
$_['entry_recurring']        = 'Recurring billing:';
$_['entry_recurring_price']  = 'Recurring price:';
$_['entry_recurring_freq']   = 'Recurring frequency:';
$_['entry_recurring_cycle']  = 'Recurring cycles:<span class="help">How often its billed, must be 1 or more</span>';
$_['entry_recurring_length'] = 'Recurring length:<span class="help">0 = until cancelled</span>';
$_['entry_trial']            = 'Trial period:';
$_['entry_trial_price']      = 'Trial recurring price:';
$_['entry_trial_freq']       = 'Trial recurring frequency:';
$_['entry_trial_cycle']      = 'Trial recurring cycles:<span class="help">How often its billed, must be 1 or more</span>';
$_['entry_trial_length']     = 'Trial recurring length:';

$_['text_length_day']        = 'Day';
$_['text_length_week']       = 'Week';
$_['text_length_month']      = 'Month';
$_['text_length_month_semi'] = 'Semi Month';
$_['text_length_year']       = 'Year';

// Error
$_['error_warning_mandetory']= ' Warning: This field is mandetory!';
$_['error_warning']          = ' Warning: Please check the form carefully for errors!';
$_['error_permission']       = ' Warning: You do not have permission to modify products!';
$_['error_name']             = ' Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']            = ' Product Model must be greater than 3 and less than 64 characters!';
$_['error_meta_title']       = ' Product Meta Title must be greater than 3 and less than 64 characters!';
$_['error_no_of_images']     = ' Warning: Product Images are more than limit - ';

// image file upload check error
$_['error_filename']   = 'Warning: Filename must be a between 3 and 255!';
$_['error_folder']     = 'Warning: Folder name must be a between 3 and 255!';
$_['error_exists']     = 'Warning: A file or directory with the same name already exists!';
$_['error_directory']  = 'Warning: Directory does not exist!';
$_['error_filetype']   = 'Warning: Incorrect file type!';
$_['error_upload']     = 'Warning: File could not be uploaded for an unknown reason!';

// membership code
$_['entry_expiring_date']	 ='Expiring date';
$_['entry_auto_relist']   	 ='Auto Re-list';
$_['entry_auto_relist_lable']			 ='Enable auto-relist';
$_['entry_relist_duration']  	 ='Listing Duration';

$_['text_relist']  	 ='Re-list';
$_['text_publish']   ='Publish';
$_['text_unpublish'] ='Unpublish';
$_['text_clone_product'] ='Clone';
$_['entry_list_header'] ='Click here to see membership plans';
?>
