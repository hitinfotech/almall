<?php 

//heading

$_['heading_title']		=	'حقل مخصص';
$_['heading_title_insert']		=	'إدخال الحقل المخصص';
$_['heading_title_list']		=	'قائمة الحقل المخصص';
$_['heading_title_edit']		=	'تعديل الحقل المخصص';

//text

$_['noOption']			=	'لا يوجد خيار';
$_['button_update']		=	'تحديث';
$_['text_field_name']	=	'اسم الحقل:';
$_['text_desc']			=	'الوصف:';
$_['text_is_req']		=	'مطلوب';
$_['text_for_seller']	=	'للبائع:';
$_['text_field_type']	=	'نمط الحقل: ';
$_['text_action']		=	'التصرف';
$_['text_option_value']	=	'قيمة الخيار';
$_['text_add_option']	=	'إضافة خيار';
$_['text_remove']		=	'إزالة';
$_['entry_is_required'] =	'<font style="font-weight:bold;color:#F00">&nbsp;*</font>';

//error or success

$_['success_insert']		=	"لقد قمت بإدخال حقل مخصص بنجاح !";
$_['error_insert']			=	"تحذير: الرجاء تعبئة الحقول المطلوبة!";
$_['success_update']		=	"لقد قمت بتحديث الحقل المخصص بنجاح !";
$_['error_delete']			=	"تحذير: الرجاء تحديد خيار واحد على الأقل لحذفه !";
$_['success_delete']		=	"لقد تم بنجاح حذف الملفات ";

?>