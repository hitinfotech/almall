<?php
// Heading
$_['heading_title']     = 'قائمة الطلبات';

// Text
$_['text_account']      = 'الملف الشخصي';
$_['text_productlist']  = 'لائحة طلباتك';
$_['text_product_details'] = 'تفاصيل المنتج';
$_['text_success']      = 'تم تعديل ملفك الشخصي بنجاح';
$_['text_select']		='اختر';

$_['text_orderid']      = 'رقم الطلب';
$_['text_added_date']  = 'تاريخ الاضافة';
$_['text_products'] = 'المنتجات';
$_['text_customer']      = 'الزبون';
$_['text_total']		='المجموع';
$_['text_status']		='الحالة';
$_['text_action']	=	'الإجراء';

$_['text_processing'] = 'قيد المعالجة';
$_['text_shipped'] = 'في الشحن';
$_['text_canceled'] = 'ملغي';
$_['text_complete'] = 'تم';
$_['text_denied'] = 'ممنوع';
$_['text_canceled_reversal'] = 'Canceled Reversal';
$_['text_failed'] = 'فشل';
$_['text_refunded'] = 'تم الارجاء';
$_['text_reversed'] = 'Reversed';
$_['text_chargeback'] = 'تم رد الملبغ المدفوع';
$_['text_pending'] = 'قيد الانتظار';
$_['text_voided'] = 'ملغاة';
$_['text_processed'] = 'تمت معالجتها';
$_['text_expired'] = 'انتهت صلاحيته';
$_['text_no_results']        = 'لا يوجد نتائج !';
$_['text_tracking'] = 'تفاصيل حالة الشحن';



$_['button_filter']           = 'أظهر النتائج';

// Entry
$_['entry_orderstatus']  = 'حالة الطلب';
$_['entry_orderinfo']   = 'معلومات الطلب';
$_['entry_orderprice']      = 'التكلفة';

// Error
$_['error_exists']     = 'تحذير: هذا الايميل تم استخدامه مسبقا !';
$_['error_firstname']  = 'الاسم الاول يجب ان يكون بين 1 و 32 حرفا!';
$_['error_lastname']   = 'الاسم الاخير يحب ان يكون بين 1 و32 حرفا';
$_['error_email']      = 'ضيغة الايميل غير صحيحه';
$_['error_telephone']  = 'رقم الهاتف يجب ان يكون بين 3 و32 رقما';
?>
