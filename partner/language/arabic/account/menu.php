<?php

// Text
$_['text_personal_account']       = 'الملف الشخصي';
$_['text_notifications']          = 'قائمتي المفضلة';
$_['text_fav_shops']              = 'محلات مفضلة';
$_['text_fav_products']           = 'قائمة رغباتي';
$_['text_change_password']        = 'تغيير كلمة السر';
$_['text_address']                = 'العناوين';
$_['text_language']               = 'اللغة المفضلة';
$_['text_my_orders']              = 'طلباتي';
$_['text_balance']                = 'رصيد';
$_['text_RMA']                    = 'طلبات الإرجاع';
$_['text_newsletter']             = 'النشرة البريدية';
$_['text_voucher']                = 'الرصيد';


