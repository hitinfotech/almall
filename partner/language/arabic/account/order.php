<?php
// Heading
$_['heading_title']         = 'طلباتي';

// Text
$_['text_account']          = 'الملف الشخصي';
$_['text_order']            = 'معلومات الطلب';
$_['text_order_detail']     = 'تفاصيل الطلب';
$_['text_invoice_no']       = 'رقم الفاتورة:';
$_['text_order_id']         = 'رقم الطلب:';
$_['text_date_added']       = 'تاريخ الإضافة:';
$_['text_shipping_address'] = 'عنوان الشحن';
$_['text_shipping_method']  = 'طريقة الشحن:';
$_['text_payment_address']  = 'عنوان الدفع';
$_['text_payment_method']   = 'طريقة الدفع:';
$_['text_comment']          = 'تعليقات على الطلب';
$_['text_history']          = 'سجل الطلب';
$_['text_success']          = 'تنبيه: لقد أضفت <a href="%s">%s</a> إلى <a href="%s">حقيبة التسوق</a>!';
$_['text_empty']            = 'لم تقم بإضافة أي طلبية لحد الآن';
$_['text_error']            = 'لم يتم العثور على الطلب';
$_['text_track']            = 'تتبع الطلب';

// Column
$_['column_order_id']       = 'رقم الطلب';
$_['column_product']        = 'عدد المنتجات';
$_['column_customer']       = 'الزبون';
$_['column_name']           = 'إسم المنتج';
$_['column_model']          = 'موديل';
$_['column_quantity']       = 'الكمية';
$_['column_price']          = 'السعر';
$_['column_total']          = 'المجموع';
$_['column_action']         = 'نشاطات';
$_['column_date_added']     = 'تاريخ الإضافة';
$_['column_status']         = 'حالة الطلب';
$_['column_comment']        = 'تعليقات';

// Error
$_['error_reorder']         = '%s غير متاح لإعادة الطلب';
$_['text_tracking']         = 'تفاصيل شحن الطلب';
