<?php

// Heading
$_['heading_title'] = 'للتواصل معنا';

// Text
$_['text_location'] = 'موقعنا';
$_['text_store'] = 'متاجرنا';
$_['text_contact'] = 'ملاحظات :';
$_['text_address'] = 'العنوان';
$_['text_telephone'] = 'تلفون';
$_['text_fax'] = 'فاكس';
$_['text_open'] = 'مواعيد العمل';
$_['text_comment'] = 'ملاحظات';
$_['text_success'] = '<p>تم ارسال ملاحظاتك بنجاح ! شكرا</p>';
$_['text_details'] = '<p>الأعزاء والعزيزات زوار موقع سيدتي مول</p>
<p>
أسئلتكم واستفساراتكم واقتراحاتكم موضع ترحيب دائم،
<br>
ولتبقوا على تواصلٍ معنا ومعفريقنا يمكنكم مراسلتنا عن طريق البريد الالكتروني:
</p>
<p>
<a href="mailto:mall@sayidaty.net" data-cke-saved-href="mailto:mall@sayidaty.net">mall@sayidaty.net</a>
</p>
<p>سيدتي مول وجهتكم الأولى للتسوق في عالم الموضة والأزياء.</p>';

// Entry
$_['entry_name'] = 'الاسم';
$_['entry_email'] = 'البريد الالكتروني';
$_['entry_enquiry'] = 'ملاحظات';

// Email
$_['email_subject'] = 'ملاحظات %s';

// Errors
$_['error_name'] = 'الاسم يجب ان يتكون من3 - 31 حرف';
$_['error_email'] = 'البريد الالكتروني غير صحيح';
$_['error_enquiry'] = 'ملاحظات يجب ان يكون من 10 الى 100 حرف';
