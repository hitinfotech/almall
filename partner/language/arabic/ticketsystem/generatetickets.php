<?php
// Heading
$_['heading_title'] = 'للتواصل معنا';

// Text
$_['text_info']          = 'Complete this field(s) and we will solve your query in no time.';

$_['text_name']    = 'الاسم';
$_['text_email']    = 'البريد الالكتروني';
$_['text_subject']    = 'الموضوع';
$_['text_message']   = 'الملاحظات';
$_['text_description']   = 'الوصف';
$_['text_id']            = 'Ticket Id';
$_['text_tags']   = 'Ticket Tags';
$_['text_ticketnotes']   = 'Ticket Notes';
$_['text_groupname']   = 'Group Name';
$_['text_agentname']   = 'Agent Name';
$_['text_agentemail']   = 'Agent Mail';
$_['text_source']   = 'Ticket Source';
$_['text_status']   = 'Ticket Status';
$_['text_priority']   = 'Ticket Priority';
$_['text_tickettype']   = 'Ticket Type';
$_['text_group']   = 'Group';
$_['text_agent']   = 'Agent';
$_['submit']   = 'ارسال';
$_['text_fileupload']   = 'File Upload';
$_['text_details'] = '<p>الأعزاء والعزيزات زوار موقع سيدتي مول</p>
<p>
أسئلتكم واستفساراتكم واقتراحاتكم موضع ترحيب دائم،
<br>
ولتبقوا على تواصلٍ معنا ومعفريقنا يمكنكم مراسلتنا عن طريق البريد الالكتروني:
</p>
<p>
<a href="mailto:mall@sayidaty.net" data-cke-saved-href="mailto:mall@sayidaty.net">mall@sayidaty.net</a>
</p>
<p>سيدتي مول وجهتكم الأولى للتسوق في عالم الموضة والأزياء.</p>';

$_['text_success'] = '<p>تم ارسال ملاحظاتك بنجاح ! شكرا</p>';

$_['text_login_info']   = 'Info ! Login is not mandatory but login can give you more power and options in this site and as well in HelpDesk Support. Can try <a href="%s" target="_blank">Here</a>';
$_['error_warning']   = 'خطا! الرجاء ادخال البيانات بشكل صحيح';
$_['error_name'] = 'الاسم يجب ان يتكون من3 - 31 حرف';
$_['error_email'] = 'البريد الالكتروني غير صحيح';
$_['error_message'] = 'ملاحظات يجب ان يكون من 10 الى 100 حرف';
$_['error_subject']   = 'الرجاء ادخال الموضوع';
$_['error_group']   = 'Warning ! Please select Group for Ticket.';
$_['error_agent']   = 'Warning ! Please select Agent for Ticket.';
$_['error_tickettype']   = 'Warning ! Please select Ticket Type.';
$_['error_status']   = 'Warning ! Please select Status for Ticket.';
$_['error_priority']   = 'Warning ! Please select Priority for Ticket.';
$_['error_fileupload']   = 'Warning ! Please add Subject for Ticket.';
$_['error_custom_field']   = 'Warning ! Please complete %s for Ticket.';
$_['error_upload']   = 'Warning ! Please select file to upload.';
$_['error_filename'] = 'Filename must be between 3 and 64 characters!';
$_['error_filetype'] = 'Invalid file type!';
$_['error_filesize'] = 'File is larger than allowed file size!';

