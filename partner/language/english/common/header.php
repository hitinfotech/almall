<?php
// Text
$_['text_home']                 = 'Home';
$_['text_wishlist']             = 'Wish List (%s)';
$_['text_notifications']        = 'My favorite';
$_['text_shopping_cart']        = 'Shopping Cart';
$_['text_category']             = 'Categories';
$_['text_account']              = 'My Account';
$_['text_tracking']             = 'Order tracking';

$_['text_profile']              = 'My account';
$_['text_welcome']              = 'Welcome %s';
$_['text_register']             = 'Register';
$_['text_login']                = 'Login';
$_['text_login_header']         = 'Login';
$_['text_login_header_cart']    = 'Please login to use the coupon';
$_['text_order']                = 'Order History';
$_['text_transaction']          = 'Transactions';
$_['text_download']             = 'Downloads';
$_['text_logout']               = 'Logout';
$_['text_checkout']             = 'Checkout';
$_['text_search']               = 'Search';
$_['text_all']                  = 'Show All';
$_['text_malls_catalog']        = 'Malls Catalog';
$_['text_shops_catalog']        = 'Shops Catalog';
$_['text_brands']               = 'Brands Catalog';
$_['text_offers']               = 'Offers';
$_['text_news']                 = 'News';
$_['text_looks']                  = 'Looks';
$_['text_tips']                   = 'Shopping Tips';
$_['text_seller_account']         = 'Seller page';
$_['text_loginto_seller']         = 'Sell with us';
$_['text_register_seller']      = 'Register';
$_['text_howtosell']            = 'How to sell';
$_['text_login_seller']       = 'Login';
$_['text_edit_info']            = 'Edit Information';
$_['text_seller_contract']      = 'My Contract';

$_['text_seller_discounts']    = 'Brand Offers';
$_['text_seller_invoices']    = 'My Invoices';

$_['text_rma_rma']                = 'Returns system';
$_['text_rma_reason']             = 'Returns reason';

$_['text_newsletter']             = 'Newsletter';
$_['text_more']                   = 'More';
$_['text_sayidaty_dot_net_site']  = 'Sayidaty.net';
$_['text_sayidaty_kitchen_site']  = 'Sayidaty Kitchen';
$_['text_sayidaty_fashion_site']  = 'Fashion';

$_['text_call_us']  = 'Call Us';
$_['text_free_exchange']  = 'Free Exchange';
$_['text_delivery_and_exchange']  = 'delivery and Exchange';

$_['text_facebook_login']  = 'Facebook Login';
$_['text_email']  = 'Email';
$_['text_or']  = 'OR';
$_['text_password']  = 'Password';
$_['text_remeber_me'] = 'Remember Me';
$_['text_forgot_password']  = 'Forgot Password ?';
$_['text_new_user']  = 'New User ?';
$_['text_close']  = 'Close';

$_['text_search_website']  = 'Site Search ...';
$_['text_shopping_bag']  = ' Shopping Bag';



$_['text_menu_title_shop']  = 'Shop';
$_['text_online']  = 'Online';
$_['text_we_choose_for_you']  = 'Choosen For You';

$_['text_women']  = 'Women';
$_['text_clothes']  = 'Clothes';
$_['text_dresses']  = 'Dresses';
$_['text_shirts_tshirts']  = 'Shirts and T-Shirts';
$_['text_jackets_coats']  = 'Jackets and Coats';
$_['text_abaya']  = 'Abayat';
$_['text_swimming_suits']  = 'Swimming Suits';
$_['text_shoes']  = 'Shoes';
$_['text_boots']  = 'Boots';
$_['text_sandals']  = 'Sandals';
$_['text_flat_shoes']  = 'Flat Shoes';
$_['text_sport_shoes']  = 'Sport Shoes';
$_['text_high_heels']  = 'High Heels';
$_['text_accessories']  = 'Accessories';
$_['text_nicklesses']  = 'Nicklesses';
$_['text_diamonds']  = 'Diamonds';
$_['text_watches']  = 'Watches';
$_['text_sunglasses']  = 'Sunglasses';
$_['text_rings']  = 'Rings';
$_['text_bags']  = 'Bags';
$_['text_big_bags']  = 'Big Bags';
$_['text_small_bags']  = 'Small Bags';
$_['text_clatch']  = 'Clatch';
$_['text_wallets']  = 'Wallets';
$_['text_travel_bags']  = 'Travel Bags';

$_['text_men']  = 'Men';
$_['text_shirts']  = 'Shirts';
$_['text_suits']  = 'Suits';
$_['text_sport_wears']  = 'Sport Wears';
$_['text_trousers_pants']  = 'Trousers and Pants';
$_['text_slippers']  = 'Slippers';
$_['text_casual_shoes']  = 'Casual Shoes';
$_['text_ties']  = 'Ties';
$_['text_belts']  = 'Belts';
$_['text_hats']  = 'Hats';
$_['text_casual_bags']  = 'Casual Bags';
$_['text_designers']  = 'Designers';
$_['text_full_looks']  = 'Full Looks';
$_['text_full_shops']  = 'Shops Catalog';
$_['text_products']  = 'Products';
$_['text_news']  = 'News';



$_['text_new']         = 'New';
$_['text_favorit']     = 'Favorit';
$_['text_tags']        = 'Tags';

$_['text_welcome2']    = 'Be the first to know...';

$_['text_newscon']     = 'Sign up to get the latest trends & tips in Shopping with Sayidaty Mall experts.';

$_['male']             = 'Male';
$_['female']           = 'Female';
$_['text_thanks']      = 'Thank you';
$_['text_done_add']    = 'You have been successfully subscribed.';
$_['text_done2']       = 'Check your email for more promotions in the future.';
$_['shop_now_text']             = "SHOP NOW";
$_['sold_out_text']             = "SOLD OUT";
$_['text_loginto_seller']         = 'Sell with us';
$_['text_register_seller']      = 'Register';
$_['text_howtosell']            = 'How to sell';
$_['text_login_seller']       = 'Login';
