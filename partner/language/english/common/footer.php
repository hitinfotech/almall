<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a href="http://mall.sayidaty.net/">Mall Sayidaty</a><br /> %s &copy; %s';
$_['text_call_us_form'] ='Contact Us form';
$_['text_do_you_have_questions'] = 'Do you have questions? ';
$_['text_job_times']    = "From Sunday - Thursday 9:00 AM - 5:00 PM";

$_['text_brands']               = 'Exclusive brands';
$_['text_freeshipping']         = 'Free Shipping <small>(T&C apply)</small>';
$_['text_return']               = '14 days Return';
$_['text_cod']                  = 'Cash On Delivery';

$_['text_shopping']             = 'Shopping';
$_['text_woman']                = 'Women';
$_['text_man']                  = 'Men';
$_['text_newarrival']           = 'New arrivals';
$_['text_bestseller']           = 'Best sellers';
$_['text_designer']             = 'Designers';
$_['text_sizes']                = 'Size Chart';

$_['text_sayidaty_mall']        = 'SayidatyMall';

$_['text_subscribe']            = 'Subscribe to the newsletter';
$_['text_subscribe_desc']       = 'Get our latest offers and exclusive arrivals before everyone';
$_['text_female']               = 'Female';
$_['text_male']                 = 'Male';
$_['text_email']                = "Your email";

$_['text_about_us']   = 'About Us';
$_['text_shop_online']   = 'Shop Online';
$_['text_customer_service']   = 'Customer Service';
$_['text_follow_us']   = 'Follow Us On';
