<?php

$_['text_menu_title_shop']      = 'Shop';
$_['text_online']               = 'Online';

$_['text_menu_title_shop']      = 'PRODUCTS';
$_['text_menu_title_shop2']     = "SHOP ONLINE";
$_['text_online']               = '';

$_['text_we_choose_for_you']    = 'For You';

$_['text_men']                  = 'MEN';
$_['text_shirts']               = 'قمصان';
$_['text_suits']                = 'بدلات';
$_['text_sport_wears']          = 'ملابس رياضية';
$_['text_trousers_pants']       = 'سراويل و بناطيل';
$_['text_slippers']             = 'شباشب';
$_['text_casual_shoes']         = 'احذية عملية';
$_['text_ties']                 = 'ربطات عنق';
$_['text_belts']                = 'احزمة';
$_['text_hats']                 = 'قبعات';
$_['text_casual_bags']          = 'حقائب عملية';
$_['text_designers']            = 'مصممين';
$_['text_full_looks']           = 'LOOKS';
$_['text_full_shops']           = 'DIRECTORY ';
$_['text_products']             = 'Products';


$_['text_women']                = 'WOMEN';
$_['text_menu_shop']            = 'SHOP';
$_['text_clothes']              = 'ملابس';
$_['text_dresses']              = 'فساتين';
$_['text_shirts_tshirts']       = 'بلايز وقمصان';
$_['text_jackets_coats']        = 'جاكيتات و معاطف';
$_['text_abaya']                = 'عبايات';
$_['text_swimming_suits']       = 'ملابس بحر';
$_['text_shoes']                = 'احذية';
$_['text_boots']                = 'جزم';
$_['text_sandals']              = 'صنادل';
$_['text_flat_shoes']           = 'احذية فلات';
$_['text_sport_shoes']          = 'احذية رياضية';
$_['text_high_heels']           = 'كعب عالي';
$_['text_accessories']          = 'اكسسوارات';
$_['text_nicklesses']           = 'سلاسل';
$_['text_diamonds']             = 'الماس';
$_['text_watches']              = 'ساعات';
$_['text_sunglasses']           = 'نظارات';
$_['text_rings']                = 'خواتم';
$_['text_bags']                 = 'حقائب';
$_['text_big_bags']             = 'حقائب كبيرة';
$_['text_small_bags']           = 'حقائب صغيرة';
$_['text_clatch']               = 'كلاتش';
$_['text_wallets']              = 'محافظ';
$_['text_travel_bags']          = 'حقائب سفر';
$_['text_bundle']               = 'Bundles';


$_['text_brands']               = 'BRANDS';
$_['text_offers']               = 'Offers';
$_['text_offer']                = 'SALE';
$_['text_mother_day']           = 'Mother\'s Day';
$_['text_news']                 = 'News';
$_['text_looks']                = 'LOOKS';
$_['text_tips']                 = 'TIPS';

$_['text_malls_catalog']        = 'Malls ';
$_['text_shops_catalog']        = 'SHOPS ';

$_['text_more']                 = 'More';
$_['text_tag']                  = '#Tags';
$_['text_toppages']             = 'Top5';
$_['text_seller_discounts']    = 'Brand Offers';
