<?php

$_['heading_title'] = 'Looks';
$_['heading_title_filter'] = '%s Looks';


$_['text_women_looks']   = 'Women Looks';
$_['text_famous_looks']  = 'Famous Looks';
$_['text_men_looks']     = 'Men Looks';
$_['text_kids_looks']    = 'Kids Looks';
$_['text_latest']        = 'Latest Looks ';
$_['text_buy_look']      = 'Buy This Look';
$_['text_more']          = 'More';
$_['text_choose']        = 'Choose';
$_['text_all_look']      = 'All Looks';

// $_['text_look_products'] = 'Look Products';
$_['text_look_products'] = 'Get the same look';
$_['text_price']         = 'Price ';
$_['text_other_looks']   = 'Other Looks';
$_['text_more_looks']    = 'More';
$_['sizes']              = 'Sizes';
$_['unavailable']        = 'Unavailable ';
$_['text_not_available'] = 'This product is not available in your country';
$_['addtocart']          = 'Add to cart ';
$_['entry_qty']          = 'Quantity';



$_['heading_meta_title']    = '%s Looks | Sayidaty Mall | Online Shopping in %s, Shop Clothes, Makeup, Dresses, Accessories, Watches and Bags';
$_['text_meta_description'] = 'Watch photos and videos for celebrity looks, price of celebrity dresses, looks for men, women and kids with information about 2017 style outlines on Sayidaty Mall';
$_['heading_meta_title_detail_page'] ='%s | Sayidaty Mall | Online Shopping in %s';
