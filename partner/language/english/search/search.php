<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$_['heading_title'] = "Sayidaty Mall Products";


$_['text_categories'] = 'Categories';
$_['text_brands'] = 'Brands';
$_['text_attributes'] = 'Attributes';
$_['text_clear_filters'] = 'Clear all filters';
$_['text_search_for'] = 'searched for : ';
$_['text_shop_now'] = 'Shop Now';
$_['text_shop_online'] = 'Shop Online';
$_['text_all_products'] = 'All Products';
$_['text_search_website'] = 'Search in products';

$_['text_sort_by'] = 'Sort By';
$_['text_top_brand'] = 'Sort by top brands';
$_['text_products_price_asc'] = 'Lower prices';
$_['text_products_price_desc'] = 'Higher prices';
$_['text_latest_products'] = 'Sort By Latest Products';
$_['text_prices_asc'] = 'Lower Prices';
$_['text_prices_desc'] = 'Higher Prices';
$_['text_views'] = 'Popular Products';
$_['prod'] = 'Products';
$_['filter']            = 'Filter';

$_['text_color'] = 'Color';
$_['text_pattern'] = 'Pattern';
$_['text_collection'] = 'Collection';
$_['text_fabric'] = 'Fabric';
$_['text_Bag_Type'] = 'Bag Type';
$_['text_Heel_Height'] = 'Heel Height';
$_['text_Length'] = 'Length';
$_['text_Occasion'] = 'Occasion';
$_['text_Options'] = 'Options';
$_['text_Shoes_type'] = 'Shoes type';
$_['text_Skirt_Length'] = 'Skirt Length';
$_['text_Sleeves_Length'] = 'Sleeves Length';
$_['text_size']           = 'Size';
$_['text_options']        = 'Options';
$_['text_price']           = 'Price Range';
$_['text_back']             ='Back';
$_['text_view_results']     ='Show Results';
$_['text_no_result']        ='400 result';
$_['text_view_as']          ='View Regarding to';
$_['text_to']               = 'To' ;
$_['text_from']             = 'From';

$_['heading_meta_title']    ="Shop Online in %s | Sayidaty Mall| Fashion, accessories, evening dresses, children's clothing, Bags, Watches";

$_['heading_meta_title1']    = ' #country# | سيدتي مول | ازياء، اكسسوارات، فساتين سهرة، ملابس اطفال، شنط، ساعات';
$_['heading_page_title1']    = 'Shop Online Products';

$_['heading_meta_title2']    = 'منتجات لل#main_category# |سيدتي مول |  #country# | ازياء، اكسسوارات، فساتين سهرة، ملابس اطفال، شنط، ساعات';
$_['heading_page_title2']    = 'Shop Online #main_category# Products';

$_['heading_meta_title3']    = '#sub_category# |سيدتي مول |  #country# | ازياء، اكسسوارات، فساتين سهرة، ملابس اطفال، شنط، ساعات';
$_['heading_page_title3']    = 'Shop Online #main_category# #sub_category#';

$_['heading_meta_title4']    = '#sub_sub_category# لل#main_category# |سيدتي مول |  #country# | ازياء، اكسسوارات، فساتين سهرة، ملابس اطفال، شنط، ساعات';
$_['heading_page_title4']    = 'Shop Online #main_category# #sub_sub_category#';

$_['heading_meta_title5']    = '#sub_sub_category# من #brand# لل#main_category# |سيدتي مول  #country# | ازياء، اكسسوارات، فساتين سهرة، ملابس اطفال، شنط، ساعات';
$_['heading_page_title5']    = 'Shop Online #main_category# #sub_sub_category# from #brand#';

$_['heading_meta_title6']    = '#sub_sub_category# من #brand#, #str_options# لل#main_category# |سيدتي مول |  #country# ';
$_['heading_page_title6']    = 'Shop Online #sub_sub_category# for #brand#';

$_['heading_meta_title7']    = 'لقد بحثت عن "#keyword#"| سيدتي مول |  #country#';
$_['heading_page_title7']    = 'You searched for "#keyword#"';

$_['heading_meta_title8']    =  'منتجات من #brand# | سيدتي مول | دليل المولات والماركات العالمية في  #country# ';
$_['heading_page_title8']    = 'Shop Online Products from #brand#';

$_['text_meta_description'] = ' ارقي الماركات على موقع سيدتي مول، شراء فساتين سهرة، شنط، احذية، ساعات، ملابس، فساتين ايلي صعب و زهير مراد، عبايات، مجوهرات، و اكسسوارات في %s';
