<?php

$_['head_title']                    = 'Brands Catalog';

$_['text_brands']                   = '%s Brands';
$_['text_brands_catalog']           = 'Brands Catalog';
$_['text_brands_search_brands']     = 'Search in brands';
$_['text_brands_all_categories']    = 'All categories';
$_['text_brands_shop_now']          = 'Shop now';
$_['text_brands_all_products']      = 'All products';
$_['text_brands_read_more']         = 'Read more';
$_['text_brands_more']              = 'More';

$_['text_shops']                    = 'Shops for %s';
$_['text_more_shops']               = 'More';


$_['text_brands_arabic_name']       = 'Arabic name';
$_['text_brands_english_name']      = 'English name';
$_['text_brands_categories']        = 'Categories';
$_['text_brands_main_links']        = 'Main links';
$_['text_brands_products']          = 'Products';
$_['text_brands_search_site']       = 'Search in site';
$_['text_brands_to_shops']          = 'Shops that have this brand';
$_['text_all_products']             = 'Products from %s';
$_['text_buy_brand']                = 'you can buy products from %s or keep it in your favourite';

$_['text_error']                    = 'The brand dose not exits';

$_['heading_meta_title']             = 'Shop Top Designers | Sayidaty Mall | Online Shopping in %s';
$_['heading_meta_title_detail_page'] = 'Shop From %s | Sayidaty Mall | Online Shopping in %s';



$_['text_meta_description']          = 'Shop now all brands, buy online from CHANEL, Elie Saab, Zuhair Murad, Roberto Cavalli, Bershka, Dior, Van Cleef, Prada, Carolina Herrera, Givenchy, Gucci and Burberry in Saudi Arabia';
$_['text_meta_description_details']  = 'Shop women clothes from %s, %s, %s, buy online from %s in %s and have it delivered to your doorstep';

$_['heading_meta_title_with_category']                = 'تسوق من اشهر ماركات %s | سيدتي مول | %s';
$_['text_meta_description_with_category']             = 'تسوق الان %s من جميع الماركات العالمية ، شانيل، ايلي صعب، زهير مراد، روبرتو كفالي، بيرشكا، ديور، فان كليف، كارولينا هيريرا، برادا، جيفنشي، قوتشي، بولغري، بيربري في %s';
$_['heading_meta_title_detail_page_with_category']    = 'تسوق #cat من #brand | سيدتي مول | دليل المولات والماركات العالمية في  %s';

$_['text_main_categories']                            = 'main categories';
