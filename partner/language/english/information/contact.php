<?php
// Heading
$_['heading_title']  = 'Contact Us';

// Text
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Telephone';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';
$_['text_details'] = 'Dear Sayidaty Mall Customers,</p>
<p>Your questions, inquiries and suggestions are always welcome, and to stay connected with us and our team, please contact us through our email:</p>
<p>
<a href="mailto:mall@sayidaty.net" data-cke-saved-href="mailto:mall@sayidaty.net">mall@sayidaty.net</a>
</p>
<p>Restyle your look with the best shopping place in the UAE and KSA.';
// Entry
$_['entry_name']     = 'Your Name';
$_['entry_email']    = 'E-Mail Address';
$_['entry_enquiry']  = 'Enquiry';

// Email
$_['email_subject']  = 'Enquiry %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';
