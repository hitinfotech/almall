<?php

$_['heading_title']             = 'Top 5';
$_['top5_look']                 = 'Suggestion Look';
$_['top5_tips']                 = 'Suggestion Tips';
$_['text_tips']                 = '%s Tips';
$_['text_read_more']            = 'Read More';
$_['text_all_tips']		= 'All Tips';
$_['text_more']                 = 'More';
$_['more_top_pages']            = 'other Top 5';

$_['text_sayidaty_dot_net']	= 'sayidaty.net';
$_['text_tip_products']         = 'Products from this tip';
$_['text_other_tip']            = 'Other Tips';
$_['text_latest_looks']         = 'Latest Looks';
$_['next']                      = 'Next';
$_['previous']                  = 'Previous';
$_['text_all_top_pages']        = 'All Items';

$_['text_error'] = 'The page you requested cannot be found.';
$_['heading_meta_title']    = ' Top 5 I Sayidaty Mall | The Ultimate Shopping Guide in  %s, Clothes, Makeup, Dresses, Accessories, Watches and Bags';
$_['heading_meta_title_detail_page']    = '%s | Top 5 | Sayidaty Mall | The Ultimate Shopping Guide in %s, Clothes, Makeup, Dresses, Accessories, Watches and Bags';
$_['heading_meta_description']    = 'Sayidaty Mall editors’ choice, top 5 dresses, best accessories,best bags, best watches, top 5 shoes, best jewelries, jeans, makeup and top 5 skin care product for you';
