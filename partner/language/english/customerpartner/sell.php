<?php
// Heading
$_['heading_title']     	  = 'Sell';

//Text
$_['text_tax']     	   		  = 'Tax : ';
$_['text_from']     	      = 'From ';
$_['text_seller']     	      = 'Seller ';
$_['text_total_products']     = 'Total Products ';
$_['text_long_time_seller']   = 'Long Time Sellers';
$_['text_latest_product']     = 'Latest Products';

$_['text_persinal_info']      = 'Personal Information';
$_['text_firstname']          = 'First name';
$_['text_lastname']           = 'Last name';
$_['text_email']              = 'Email';
$_['text_fbs_email']          = 'Second Email';
$_['text_phone']              = 'Phone';
$_['text_password']          = 'Password';
$_['text_confirm_password']  = 'Confirm Password';

$_['text_business_info']      = 'Business Information';
$_['text_country']            = 'Country';
$_['text_city']            = 'City';
$_['text_num_of_branches']            = 'Number of branches';
$_['text_screen_name']            = 'Brand Name';
$_['text_website']            = 'Website ';
$_['text_facebook']            = 'Facebook Id ';
$_['text_instagram']            = 'Instagram Id';
$_['text_twitter']            = 'Twitter Id';
$_['text_selling_countries']  = 'Authorized selling Countries';
$_['text_next']            = 'Next';
$_['text_office_address']    = 'Office address';
$_['text_warehouse_address'] = 'Warehouse address';
$_['text_opening_hours']            = 'Opening hours';
$_['text_brand_description']    = 'Brand Description';
$_['text_excluded_countries']  = 'Execluded countries';

$_['text_legal_info']        = 'Legal Information';
$_['text_legal_name']        = 'Legal name/ Representative';
$_['text_legal_name_tooltip']= 'The Owner/ Manager under the trade license';
$_['text_brand_name']        = 'Brand Name';
$_['text_company_name']      = 'Company Name';
$_['text_trade_license_number'] = 'Trade License Number';
$_['text_address_license_number'] = 'License Registration Address';
$_['text_passport_number'] = 'Passport Number';

$_['text_admin_name']     = 'Administrator Name';
$_['text_admin_email']     = 'Administrator email';
$_['text_admin_phone']     = 'Administrator Phone';
$_['text_admin_contact_tooltip'] = 'The Address of Registration of Trade License';
$_['text_nationality']        = 'Nationality';
$_['text_email_address']        = 'Email Address';
$_['text_office_num']        = 'Direct Office Phone Number';
$_['text_moblie_num']        = 'Mobile Number';
$_['text_passport_copy']        = ' Passport / National ID copy';
$_['text_license']        = 'License';
$_['text_logo']        = 'HD Logo';
$_['text_both_iban_swift'] = 'Use Both IBAN and SWIFT CODE';



$_['text_financail_info']        = 'Financial Information';
$_['text_fin_contact_name']        = 'Company Financial officer contact Name';
$_['text_fin_email']        = 'email address';
$_['text_fin_num']        = 'Phone Number';
$_['text_beneficiary_name']        = 'Beneficiary Name';
$_['text_bank_name']        = 'Bank Name';
$_['text_bank_account']        = 'Bank Account';
$_['text_iban_code']        = 'IBAN code';
$_['text_swift_code']        = 'Swift code';
$_['text_currency']        = 'Currency';
$_['complete_registration'] = 'Register';

$_['entry_eligible_vat'] = 'Eligible for VAT';
$_['entry_eligible_upload'] = 'Upload VAT registration document';
$_['entry_company_name'] = 'Company Name';
$_['entry_eligible_business_address'] = 'Registered Business Address';
$_['entry_eligible_tax_reg_num'] = 'Tax Registration Number';
$_['entry_eligible_issue_date'] = 'Issue Date';


