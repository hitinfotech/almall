<?php

$_['heading_title']             = 'Shopping Tips';

$_['text_tips']                 = '%s Tips';
$_['text_read_more']            = 'Read More';
$_['text_all_tips']		= 'All Tips';
$_['text_more']                 = 'More';
$_['text_sayidaty_dot_net']	= 'sayidaty.net';
$_['text_tip_products']         = 'Products from this tip';
$_['text_other_tip']            = 'Other Tips';
$_['text_latest_looks']         = 'Latest Looks';
$_['next']                      = 'Next';
$_['previous']                  = 'Previous';
$_['buynow']	                = 'Buy Now';
$_['related_product']	        = 'Related Products';

$_['text_error'] = 'The page you requested cannot be found.';
$_['heading_meta_title']    = 'Tips %s | Sayidaty Mall |  %s Fashion, Accessories, Dresses, Children Clothes, Bags, Watches';
$_['heading_meta_title2']    = '%s Tips | Sayidaty Mall |  %s Fashion, Accessories, Dresses, Children Clothes, Bags, Watches';
$_['heading_meta_description']    = 'نصائح للتسوق و افكار اكسسوارات، نصائح اختيار الحذاء المناسب، وموديلات صنادل حديثة، مع نصائح في المكياج والعناية بالبشرة، نصائح للمحجبات، نصائح للعروس واختيار الساعات';
