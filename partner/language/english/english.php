<?php
// Locale
$_['code']                  = 'en';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'Y-m-d H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

$_['arabic_language']       = 'Arabic';
$_['english_language']      = 'English';

// Text
$_['text_home']             = 'Home'; // '<i class="fa fa-home"></i>';
$_['text_yes']              = 'Yes';
$_['text_no']               = 'No';
$_['text_none']             = ' --- None --- ';
$_['text_select']           = ' --- Please Select --- ';
$_['text_all_zones']        = 'All Zones';
$_['text_pagination']       = 'Showing %d to %d of %d (%d Pages)';
$_['text_loading']          = 'Loading...';
$_['text_no_result']        = 'No Result Found';
$_['text_site_name']        = 'sayidaty.net';
//$_['text_results_for']      = 'Search result for "%s" in ';
$_['text_results_for']      = 'Search result for "%s" ';
$_['heading_meta_title_detail_page']    = '%s | Sayidaty Mall  %s';
$_['heading_meta_title_search']    = 'Search result for "%s" | Sayidaty Mall  %s';
$_['text_in']          = ' in ';

// Social
$_['text_share']  		 	= 'Share';
$_['text_share_facebook']   = 'Facebook Share';
$_['text_share_twitter']    = 'Twitter Share';
$_['text_share_google']     = 'Google Share';

// Buttons
$_['button_address_add']    = 'Add Address';
$_['button_back']           = 'Back';
$_['button_continue']       = 'Continue';
$_['button_cart']           = 'Add to Cart';
$_['sold_out_text']         = 'SOLD OUT';
$_['button_cancel']         = 'Cancel';
$_['button_compare']        = 'Compare this Product';
$_['button_wishlist']       = 'Add to Wish List';
$_['button_checkout']       = 'Checkout';
$_['button_confirm']        = 'Confirm Order';
$_['button_coupon']         = 'Apply Coupon';
$_['button_delete']         = 'Delete';
$_['button_download']       = 'Download';
$_['button_edit']           = 'Edit';
$_['button_filter']         = 'Refine Search';
$_['button_new_address']    = 'New Address';
$_['button_change_address'] = 'Change Address';
$_['button_reviews']        = 'Reviews';
$_['button_write']          = 'Write Review';
$_['button_login']          = 'Login';
$_['button_update']         = 'Update';
$_['button_remove']         = 'Remove';
$_['button_reorder']        = 'Reorder';
$_['button_return']         = 'Return';
$_['button_shopping']       = 'Continue Shopping';
$_['button_search']         = 'Search';
$_['button_shipping']       = 'Apply Shipping';
$_['button_submit']         = 'Submit';
$_['button_guest']          = 'Guest Checkout';
$_['button_view']           = 'View';
$_['button_voucher']        = 'Apply Voucher';
$_['button_upload']         = 'Upload File';
$_['button_reward']         = 'Apply Points';
$_['button_quote']          = 'Get Quotes';
$_['button_list']           = 'List';
$_['button_grid']           = 'Grid';
$_['button_map']            = 'View Google Map';
$_['text_more']             = 'more';
$_['text_latest']           = 'Latest Products';
$_['text_bestseller']       = 'Best Seller';
$_['text_get_it_now']       = 'Get it now';

// Error
$_['error_exception']       = 'Error Code(%s): %s in %s on line %s';
$_['error_upload_1']        = 'Warning: The uploaded file exceeds the upload_max_filesize directive in php.ini!';
$_['error_upload_2']        = 'Warning: The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form!';
$_['error_upload_3']        = 'Warning: The uploaded file was only partially uploaded!';
$_['error_upload_4']        = 'Warning: No file was uploaded!';
$_['error_upload_6']        = 'Warning: Missing a temporary folder!';
$_['error_upload_7']        = 'Warning: Failed to write file to disk!';
$_['error_upload_8']        = 'Warning: File upload stopped by extension!';
$_['error_upload_999']      = 'Warning: No error code available!';

$_['text_login']    = 'You must <a href="%s">login</a> or <a href="%s">create an account</a> to save <a href="%s">%s</a> to your <a href="%s">%s</a>!';
$_['text_success']  = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">%s</a>!';
$_['text_success_deleted']    	 	  = 'Success: You have deleted the return successfully !';
$_['text_looks_tips_success']  = 'Success: You have added this %s to your <a href="%s">%s</a>!';
$_['text_remove']   = 'Success: You have modified your %s!';
$_['text_empty']    = 'Your %s is empty.';
