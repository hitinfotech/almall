<?php

$_['heading_title']		= 'Fashion and Trends';
$_['text_read_more']	= 'Read More';
$_['text_more']	= 'More';
$_['text_sayidaty_dot_net']	= 'sayidaty.net';
$_['text_tag_products']	= 'Products of ';
$_['text_tag_looks']	= 'Looks of ';
$_['text_tag_tip']	= 'Tips of ';

$_['text_meta_title'] = "موضة وصيحات | سيدتي مول |   في %s";
$_['text_meta_description'] = "تابعي كل ما هو جديد في عالم الموضة والازياء على موقع سيدتي مول من خلال تصفح السمات، تعرفي بالصور والفيديو على كل جديد في عالم الموضة وال   مع الأسعار على موقع سيدتي مول";
$_['text_meta_description_details'] = "تعرفي على المزيد من المعلومات عن %s، صور %s، منتجات %s من اشهر المصممين العالميين والعرب، كما يمكنك قراءة مقالات عن %s بشكل حصري على سيدتي مول";
