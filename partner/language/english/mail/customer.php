<?php
// Text
$_['text_subject']        = '%s - Thank you for registering';
$_['text_welcome']        = 'Welcome and thank you for registering at %s!';
$_['text_login']          = 'Your account has now been created and you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_approval']       = 'Your account must be approved before you can login. Once approved you can log in by using your email address and password by visiting our website or at the following URL:';
$_['text_services']       = 'Upon logging in, you will be able to access other services including reviewing past orders, printing invoices and editing your account information.';
$_['text_thanks']         = 'Thanks,';
$_['text_new_customer']   = 'New customer';
$_['text_signup']         = 'A new customer has signed up:';
$_['text_website']        = 'Web Site:';
$_['text_customer_group'] = 'Customer Group:';
$_['text_firstname']      = 'First Name:';
$_['text_lastname']       = 'Last Name:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Telephone:';

$_['text_greeting2'] = 'aa %s';

$_['text_greeting_p1'] = '  
<!doctype html>
<!--Quite a few clients strip your Doctype out, and some even apply their own. Many clients do honor your doctype and it can make things much easier if you can validate constantly against a Doctype.-->
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Email template By Adobe Dreamweaver CC</title>
<style type="text/css">
body {
	margin: 0;
}
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, \'Times New Roman\', serif;
	font-style: normal;
	font-weight: 400;
}
button{
	width:90%;
}
@media screen and (max-width:600px) {
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, \'Times New Roman\', serif;
}
table {
	width: 100%;
}
.footer {
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
table.responsiveImage {
	height: auto !important;
	max-width: 30% !important;
	width: 30% !important;
}
table.responsiveContent {
	height: auto !important;
	max-width: 66% !important;
	width: 66% !important;
}
.top {
	height: auto !important;
	max-width: 48% !important;
	width: 48% !important;
}
.catalog {
	margin-left: 0%!important;
}

}
@media screen and (max-width:480px) {
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, \'Times New Roman\', serif;
}
table {
	width: 100% !important;
	border-style: none !important;
}
.footer {
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.table.responsiveImage {
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.table.responsiveContent {
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.top {
	height: auto !important;
	max-width: 100% !important;
	width: 100% !important;
}
.catalog {
	margin-left: 0%!important;
}
button{
	width:90%!important;
}
}
</style>
</head>
<body yahoo="yahoo">
<table width="100%"  cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td><table width="600"  align="center" cellpadding="0" cellspacing="0">
          <!-- Main Wrapper Table with initial width set to 60opx -->
          <tbody>
            <tr>
              <td align="center" style="padding:10px;">
                <a href="'.HTTPS_CATALOG.'/"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/logo.png"></a>
              </td>
            </tr>
            <tr> 
              <!-- HTML Spacer row -->
              <td style="font-size: 0; line-height: 0;" height="20"><table width="96%" align="left"  cellpadding="0" cellspacing="0">
                  <tr>
                    <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                  </tr>
                </table></td>
            </tr>
            <tr> 
              <!-- HTML IMAGE SPACER -->
              <td style="font-size: 0; line-height: 0;" ><table align="center"  cellpadding="0" cellspacing="0" >
                  <tr>
                    <td style="padding: 0 20px;">
                      <p style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 18px; padding: 10px 0;">Dear <span style="color: #d91a5d;">';
$_['text_greeting_p2'] ='</span>,</p>
                      <p style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 10px 0; font-weight: normal;">Thank you for registering at Sayidaty Mall Store!</p>
                      
                     


                    </td>
                  </tr>
                  <tr>
                    <td style="padding: 0 20px;"><a href="'.HTTPS_CATALOG.'/ae/en/search?q=&c=&b=&color=&path="><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/welcome.jpg"  alt=""  width="100%" class=""></a></td>
                  </tr>
                </table></td>
            </tr>
            <tr> 
              <!-- HTML Spacer row -->
              <td style="font-size: 0; line-height: 0;" height="20"><table width="96%" align="left"  cellpadding="0" cellspacing="0">
                  <tr>
                    <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                  </tr>
                </table></td>
            </tr>
           
            <tr> 
              <!-- HTML Spacer row -->
              <td style="font-size: 0; line-height: 0;" height="10"><table width="96%" align="left"  cellpadding="0" cellspacing="0" >
                
                </table></td>
            </tr>
            <tr>
              <td><table cellpadding="0" cellspacing="0" align="center" width="96%"  class="catalog"  style="margin-left: 4%;">
                  <!-- Table for catalog -->
                  <tr>
                                    <td style="text-align: center; padding: 10px;" align="center">
                                      <img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/follow.png">
                                    </td>
                                  </tr>
                                  
                  <tr>
                    <td ><table class ="responsive-table" width="48%"  cellspacing="0" cellpadding="0" align="left" style="margin: 10px 0px 10px 0px;">
                        <!-- Table container for each image and description in catalog -->
                        <tbody>
                          <tr>
                            <td><table class="table.responsiveImage" width="66%"  cellspacing="0" cellpadding="0" align="center">
                                <!-- Table container for image -->
                                <tbody>
                                  
                                  <tr>
                                    <td align="center" style="padding:10px 3px 10px 3px;"><a href="https://www.facebook.com/Sayidatymall/?fref=ts"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/fblike.jpg" alt="sample" style="width: 255px;"></a></td>
                                  </tr>
                                </tbody>
                              </table>
                              </td>
                          </tr>
                        </tbody>
                      </table>
                      <table class ="responsive-table" width="48%"  cellspacing="0" cellpadding="0" align="left" style="margin: 10px 0px 10px 0px;">
                        <!-- Table container for each image and description in catalog -->
                        <tbody>
                          <tr>
                            <td><table class="table.responsiveImage" width="66%"  cellspacing="0" cellpadding="0" align="center">
                                <!-- Table container for image -->
                                <tbody>
                                  <tr>
                                    <td align="center" style="padding:10px 3px 10px 3px;"><a href="https://twitter.com/sayidaty_mall"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/twfollow.jpg" alt="sample" style="width: 255px;"></a></td>
                                  </tr>
                                </tbody>
                              </table>
                              </td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                  
                  <tr>
                    <td ><table class ="responsive-table" width="48%"  cellspacing="0" cellpadding="0" align="left" style="margin: 10px 0px 10px 0px;">
                        <!-- Table container for each image and description in catalog -->
                        <tbody>
                          <tr>
                            <td><table class="table.responsiveImage" width="66%"  cellspacing="0" cellpadding="0" align="center">
                                <!-- Table container for image -->
                                <tbody>
                                  <tr>
                                    <td align="center" style="padding:10px 3px 10px 3px;"><a href="https://www.youtube.com/channel/UCw0FpmUg229kGcDqKKVj-Aw"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/ytsub.jpg" alt="sample" style="width: 255px;"></a></td>
                                  </tr>
                                </tbody>
                              </table>
                              </td>
                          </tr>
                        </tbody>
                      </table>
                      <table class ="responsive-table" width="48%"  cellspacing="0" cellpadding="0" align="left" style="margin: 10px 0px 10px 0px;">
                        <!-- Table container for each image and description in catalog -->
                        <tbody>
                          <tr>
                            <td><table class="table.responsiveImage" width="66%"  cellspacing="0" cellpadding="0" align="center">
                                <!-- Table container for image -->
                                <tbody>
                                  <tr>
                                    <td align="center" style="padding:10px 3px 10px 3px;"><a href="https://www.instagram.com/sayidatymall/"><img src="'.HTTPS_CATALOG.'/catalog/view/theme/sayidaty/images/insta.jpg" alt="sample" style="width: 255px;"></a></td>
                                  </tr>
                                </tbody>
                              </table>
                              </td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
</table></td>
            </tr>
            <tr> 
              <!-- HTML spacer row -->
              <td style="font-size: 0; line-height: 0;" height="20"><table width="96%" align="left"  cellpadding="0" cellspacing="0">
                 
                </table></td>
            </tr>
            
            <tr>
              <td style="padding: 0 20px 10px;">
                 <p style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 0; font-weight: normal; margin-top: 30px">Thanks,</p>
                      <p style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; padding: 0px 0; font-weight: bold;">
Sayidaty Mall Team</p>
              </td>
            </tr>
            <tr>
              <td>
                <table class="footer" width="96%"  align="left" cellpadding="0" cellspacing="0">
                  <!-- Second column of footer content -->
                  <tr>
                    <td style="border-top: 1px solid #ddd;"><p style="font-size: 12px; font-style: normal; font-weight:normal; color: #555; line-height: 1.8; text-align:justify;padding-top:10px; margin-left:20px; margin-right:20px; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center;">This email was sent to you by Sayidaty Mall. To ensure delivery to your inbox (not bulk or junk folders), please add our e-mail address, mall@sayidaty.net, to your address book.
</p>
<p style="font-size: 12px; font-style: normal; font-weight:normal; color: #555; line-height: 1.8; text-align:justify;padding-top:10px; margin-left:20px; margin-right:20px; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; text-align: center;">
To view our Privacy Policy click here.
To unsubscribe click here.
Copyright © 2016 SRPC. All rights reserved. </p>
                      </td>
                  </tr>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
</body>
</html> ';
                            
                            