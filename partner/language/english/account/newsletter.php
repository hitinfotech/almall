<?php
// Heading
$_['heading_title']    = 'Newsletter Subscription';

// Text
$_['text_account']     = 'Account';
$_['text_newsletter']  = 'Newsletter';
$_['text_success']     = 'Success: Your newsletter subscription has been successfully updated!';

// Entry
$_['entry_newsletter'] = 'Subscribe';


$_['text_welcome2']    = 'Be the first to know...';

$_['text_newscon']     = 'Sign up to get the latest trends & tips in Shopping with Sayidaty Mall experts.';
$_['error_email']      = 'Please insert correct email.';
$_['error_exists']     = 'Your email already exist';

$_['male']             = 'Male';
$_['female']           = 'Female';
$_['text_thanks']      = 'Thank you';
$_['text_done_add']    = 'You have been registered in the Journal of Sayidaty mall.';
$_['text_done2']       = 'You can check your email to see our offers';
