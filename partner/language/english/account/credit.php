<?php
// Heading
$_['heading_title']         = 'Credit account';

// Text
$_['text_account']          = 'Account';
$_['text_order']            = 'Order Information';
$_['text_order_detail']     = 'Order Details';
$_['text_invoice_no']       = 'Invoice No.:';
$_['text_order_id']         = 'Order ID:';
$_['text_date_added']       = 'Date Added:';
$_['text_shipping_address'] = 'Shipping Address';
$_['text_shipping_method']  = 'Shipping Method:';
$_['text_payment_address']  = 'Payment Address';
$_['text_payment_method']   = 'Payment Method:';
$_['text_comment']          = 'Order Comments';
$_['text_history']          = 'Order History';
$_['text_success']          = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">shopping cart</a>!';
$_['text_empty']            = 'You have not made any previous orders!';
$_['text_error']            = 'The order you requested could not be found!';
$_['text_track']            = 'track order';

// Column
$_['column_date']          = 'Date';
$_['column_type']          = 'Type ';
$_['column_credit']        = 'Credit ';
$_['column_total']         = 'Total ';

// Error
$_['error_reorder']         = '%s is not currently available to be reordered.';
// marketplace
$_['button_order_detail']   = 'Track Order';
$_['text_tracking']         = 'Order Tracking Details';

$_['column_description']    = 'Description';
$_['column_amount']         = 'Amount';
$_['column_date_added']     = 'Date Added';
$_['column_comment']        = 'Comment';
$_['text_balance']          = 'Balance';
$_['text_no_results']       = 'No Results';

