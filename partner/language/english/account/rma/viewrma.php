<?php
// Heading
$_['heading_title']     	= 'Returns Details';
$_['heading_title_list']	= 'Returns History';
$_['heading_title_label']	= 'Return Mailing Label';

// Text
$_['text_account']      	= 'Account';
$_['text_from']      		= 'From ';
$_['text_to']      			= 'To ';
$_['text_shipping_label']   = 'Shipping Label';
$_['text_auth_label']		= 'Return Authorization Label';
$_['text_rma_id']		  	= 'Returns Id :';
$_['text_print_lable']		= 'Print Shipping Label';

$_['text_success']      	= 'Congratulations Your Returns is being solved now.';
$_['text_success_msg']      = 'Congratulations Your Returns Message saved Successfully.';
$_['text_success_reopen']   = 'Congratulations Your Returns Message saved Successfully And Reopened Your Returns.';
$_['text_select']			= 'Select';
$_['text_allowed_ex']       = 'Allowed Extension(s) %s max size %d kB';
$_['text_error']			= 'You are not Authorized for this Product Returns';

// Entry
$_['button_back'] 			= 'Back';
$_['button_continue'] 	    = 'Continue';
$_['button_save'] 			= 'Save';

// user
$_['wk_viewrma_orderid']  			= 'Order Id :';
$_['wk_viewrma_status']  			= 'Status ';
$_['wk_viewrma_reason']  			= 'Reason ';
$_['wk_viewrma_rma_tatus']			= 'Returns Status :';
$_['wk_viewrma_status_admin']  		= 'Admin Status :';
$_['wk_viewrma_status_customer']    = 'Customer Status :';
$_['wk_viewrma_authno']  			= 'Consignment Number :';
$_['wk_viewrma_add_info']  			= 'Additional Information';
$_['wk_viewrma_image']  			= 'Image(s)';
$_['wk_viewrma_close_rma'] 			= 'Close Returns ';
$_['wk_viewrma_close_rma_text'] 	= 'Please Agree To Mark if, it as solved ';
$_['wk_viewrma_item_req']  			= 'Item(s) Requested for Returns ';
$_['wk_viewrma_pname']  			= 'Product Name';
$_['wk_viewrma_model'] 			    = 'Model';
$_['wk_viewrma_price'] 			    = 'Price';
$_['wk_viewrma_qty'] 				= 'Quantity';
$_['wk_viewrma_ret_qty'] 			= 'Retuned Quantity';
$_['wk_viewrma_subtotal']  			= 'Total (with tax)';
$_['wk_viewrma_enter_cncs_no']  	= 'Click To Enter Consignment Number ';
$_['wk_viewrma_valid_no']  			= 'Please Enter Valid Consignment Number !!';
$_['wk_viewrma_msg']  				= 'Send Message';
$_['wk_viewrma_enter_msg'] 			= 'Enter Message';
// $_['wk_viewrma_submsg'] 			= 'Submit Message';
$_['wk_viewrma_reopen']  			= 'Please Check To Reopen This Returns If Cancelled';
$_['wk_rma_admin_return_text']    	= 'Admin Already Returned this Returns Quantity.';
$_['wk_rma_admin_return']    		= 'Quantity Returned';
$_['text_productReturn']	=	'All Products Returned !!';
$_['wk_mprma_manage_return']	=	'Return Quantity';
$_['wk_mprma_manage_return_info']	=	'Return Quantity will work on price + tax + rewards .';


//text
$_['text_order_details']	 = 'Order Details';
$_['text_messages']			 = 'Messages';
$_['text_download']			 = 'Click to Download';

$_['text_print']		 	 = 'Print';
// $_['text_delivered']		 = 'Delivered';
// $_['text_not_delivered']	 = 'Not Delivered Yet';
$_['text_canceled'] 		 = 'Canceled';
$_['text_solved']			 = 'Solved';
// $_['text_pending'] 		     = 'Pending';
// $_['text_processing']		 = 'Processing';
$_['text_upload_img']      	 = 'Upload Images';

// $_['text_pac_not_rec'] 		 = 'Package not Receive Yet';
// $_['text_pac_rec']			 = 'Recieved Package';
// $_['text_pac_dis'] 		     = 'Dispatched Package';
// $_['text_return_dec']		 = 'Declined/Return';
$_['text_upload_success']	 = 'Updated Successfully';
?>
