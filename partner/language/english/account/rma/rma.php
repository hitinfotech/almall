<?php
// Heading
$_['heading_title']     	= 'Returns History';
$_['heading_title_guest']   = 'Guest Returns';
$_['heading_title_login']   = 'Returns Login';
// Text
$_['text_account']      = 'Account';
$_['text_success']      = 'Success: Your Returns has saved.';
$_['text_select']		='Select';
// Entry
$_['button_back']       = 'Back';
$_['button_continue']   = 'Continue';
$_['button_request']    = 'Submit Request';
$_['button_filter']    = 'Filter';
// user
$_['wk_rma_id']          = 'Id';
$_['wk_rma_orderid']     = 'Order Id';
$_['wk_rma_status']      = 'Returns Status';
$_['wk_rma_date']   	 = 'Date';
$_['wk_rma_action']      = 'Action';
$_['wk_rma_price']     	 = 'Price';
$_['wk_rma_viewdetails'] = 'View Details';
$_['wk_rma_cancel']      = 'Cancel';
$_['wk_rma_quantity']    = 'Quantity';
$_['wk_rma_empty']       = 'No Data Available ';
$_['wk_rma_add']     	 = 'Request New Returns ';
$_['wk_rma_r_u_sure']    = 'Are You Sure ?';
$_['wk_rma_cncl_rma']    = 'Cancel Returns';
$_['text_canceled'] 		 = 'Canceled';
$_['text_solved']			 = 'Solved';
// $_['text_pending'] 		     = 'Pending';
// $_['text_processing']		 = 'Processing';
$_['text_create_new_return'] = 'Create new application';
$_['text_return_description'] = 'You can return your orders from this page';

$_['wk_rma_admin_return_text']  = 'Admin Already Returned this Returns Quantity.';
$_['wk_rma_admin_return']    	= 'Quantity Returned';

$_['text_login_type'] 		 = 'Login Type';
$_['text_guest']			 = 'Guest';
$_['text_registered']		 = 'Registered';
$_['text_order_info']		 = 'Order Information';
$_['text_login_info']	  	 = 'Please select Login type for request your Returns.';
$_['text_guest_info']	  	 = 'Please enter your Order and Email Info for Returns.';

$_['text_cancel_rma']	  	 = 'Success - successfully updated Returns status to Cancel.';
$_['text_mpRma']			 = 'Returns';
$_['text_manageRma']		 = 'Manage Returns';
$_['text_manageRmaReason']	 = 'Manage Returns Reason';

$_['error_rma_delete']	  	 = 'Warning - You are not Authorized to Cancel Returns, more attempt can follow to Punishment !!';
$_['error_logintype']	  	 = 'Warning - Please select Login type !!';
$_['error_email']	  	 	 = 'Warning - Please Enter Valid Email address !!';
$_['error_orderinfo']	  	 = 'Warning - Please Enter Order Id !!';
$_['error_pleaselogin']	  	 = 'Warning - This email id related to an account so please login for Returns !!';
$_['error_order']	  	 	 = 'Warning - These details are not related to valid order !!';
$_['text_email'] 			 = 'Email';

