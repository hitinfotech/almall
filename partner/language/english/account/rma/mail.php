<?php
################################################################################################
# RMA for Opencart 1.5.1.x From webkul http://webkul.com  	  	       #
################################################################################################
/*
This language file also contain some data which will be used to send more information data to admin / customer and that's why because we can not write that data directly because that will change condition to condition so we used sort codes.
i explaining few here like
- {config_logo} - will be your Store logo
- {config_name} - will be your Store Name
- {customer_name} - will be your Customer Name related to this RMA
- {order_id} - will be your Order Id for this RMA
- {link} - will be <a> link for your site according to condition
- {rma_id} - will be your RMA Id for this RMA
- {customer_message} - will be Customer Message which will send to admin
[NIk]
*/

//RMA Generate
$_['generate_admin_subject']    	  = $_['generate_customer_subject']    	  =	$_['generate_seller_subject'] = 'Returns has been Generated For Order Id';

$_['generate_admin_message']  =
'{config_logo}
Hi {config_owner},

Customer {customer_name} has been generated Returns for Order #{order_id}.
Reply ASAP.

Thanks,
{config_name}';

$_['generate_customer_message']  =
'{config_logo}
Hi {customer_name},

Your Returns has been generated for Order #{order_id}.
We will reply you, as soon as possible after reviewing your request.
You can check your Returns details here {link}.

Thanks,
{config_name}';

$_['generate_seller_message'] =
'Hello {seller_name},

Customer {customer_name} has been generated Returns for Order #{order_id}.
Reply ASAP.

Thanks,
{config_name}';

//customer send message to admin
$_['message_to_admin_subject']    	  = 'Customer Send Message regarding Returns';
$_['message_to_admin_message']  =
'{config_logo}
Hi {config_owner},

Customer {customer_name} sent message regarding Returns Id - #{rma_id}.
{customer_message}

Thanks,
{config_name}';

//customer message send message to seller
$_['message_to_seller_subject'] = 'Customer send message regarding Returns';
$_['message_to_seller_message'] =
'{config_logo}
Hello {seller_name},
Customer {customer_name} send message regarding Returns Id - #{rma_id}.
Message: {customer_message}


Thanks,
{config_name}';

//customer changed status
$_['status_to_admin_subject']    	  = $_['status_to_customer_subject']    	  = 'Returns Status has been Changed';
$_['status_to_admin_message']  =
'{config_logo}
Hi {config_owner},

Customer {customer_name} changed Returns #{rma_id} status to following - %s.

Thanks,
{config_name}';

$_['status_to_customer_message']  =
'{config_logo}
Hi {customer_name},

Your Returns #{rma_id} status has been successfully changed to following - %s.

Thanks,
{config_name}';

?>
