<?php

// Heading
$_['heading_title']        = 'favorite';
// Text
$_['text_account']         = 'Account';
$_['text_newsletter']      = 'Newsletter';
$_['text_success']         = 'تم التعديل !';

$_['text_notifications']   = 'My favorite';

$_['tab_fav_products']     = 'Products';
$_['tab_fav_sellers']      = 'Sellers';
$_['tab_fav_brands']       = 'Brands';
$_['tab_fav_categories']   = 'Categories';
$_['tab_fav_tips']         = 'Tips';
$_['tab_fav_looks']        = 'Looks';
