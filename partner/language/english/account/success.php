<?php
// Heading
$_['heading_title'] = 'Your Account Has Been Created!';

// Text
$_['text_hello'] = ' Hello  %s،';
$_['text_welcome'] = 'Thank you for applying to Sayidaty Mall Marketplace';
$_['text_text'] = 'Your application has been submitted and is currently under review. We\'ll email you at %s once your account will be approved.';
$_['text_approval'] = '<p>Thank you for registering with %s!</p><p>You will be notified by e-mail once your account has been activated by the store owner.</p><p>If you have ANY questions about the operation of this online shop, please <a href="%s">contact the store owner</a>.</p>';
$_['text_account']  = 'Account';
$_['text_success']  = 'Success';
