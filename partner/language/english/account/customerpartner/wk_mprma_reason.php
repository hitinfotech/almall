<?php
################################################################################################
# wk_rma_admin for Opencart 1.5.1.x From webkul http://webkul.com  	  	       #
################################################################################################

// Heading Goes here:
$_['heading_title']      	  = 'RMA Reason ';
$_['heading_title_insert']    = 'New Reason ';
$_['heading_title_update']    = 'Update Reason';

// Text
$_['text_account']     	 	  = 'Account';
$_['text_module']     	 	  = 'Modules';
$_['text_form']     	 	  = 'Add Reason';
$_['text_edit_form']   	 	  = 'Edit Reason';

$_['text_success']    	 	  = 'Success: You have been successfully modified RMA Reason .';
$_['text_success_insert']     = 'Success: You have been successfully added New RMA Reason .';
$_['text_success_update']     = 'Success: You have been successfully updated RMA Reason .';

// Entry
$_['text_disable']        		  = 'Disable';
$_['text_enable']      		  	  = 'Enable';
$_['text_no_records']      		  = 'No Recoreds Found !!';
$_['text_list']      		  	  = 'Reasons List';
$_['entry_status']      		  = 'Status';
$_['entry_reason']      	      = 'Reason';

$_['button_clrfilter']    	      = 'Clear Filter';
$_['button_filter']					= 'Filter';
$_['button_back']    	          = 'Back';
$_['button_save']    	          = 'Save';
$_['button_insert']    	          = 'Add Reason';
$_['button_cancel']    	          = 'Cancel';
$_['button_delete']    	          = 'Delete';
$_['text_edit']    	          	  = 'Edit';
// Error
$_['error_delete']  	  	  	  = 'Warning: Detele Reason also affect RMA Order and You can lost some data related to these Reasons !!';
$_['error_permission']  	 	  = 'Warning: You do not have permission to modify Product Return RMA - Reason !!';
$_['error_reason']  	  	  	  = 'Warning: Reason must have between 5 to 50 characters !!';
?>
