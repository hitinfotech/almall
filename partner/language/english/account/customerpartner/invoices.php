<?php
// Heading
$_['heading_title']    	     = 'My Invoices';

// Text
$_['text_account']     		 = 'Account';
$_['text_invoicesList']	=	'Invoices List';
$_['text_invoiceId']	=	'Invoice Id';

// Button
$_['button_filter']			 = 'Filter';

// Entry
$_['entry_id']        		 = 'Id';
$_['entry_invoice_id']      = 'Invoice';
$_['entry_year']      = 'Year';
$_['entry_month']      = 'Month';
$_['entry_companyname']      = 'Company name';
$_['entry_month_year']      = 'Month / Year';
$_['entry_action']      = 'Action';


?>
