<?php
// Heading
$_['heading_title']         = 'Order tracking';

// Text
$_['text_account']          = 'Account';
$_['text_order']            = 'Order info';
$_['text_order_detail']     = 'Order details';
$_['text_invoice_no']       = 'Invoice No:';
$_['text_order_id']         = 'Order Id:';
$_['text_date_added']       = 'Date added:';
$_['text_shipping_address'] = 'shipping address';
$_['text_shipping_method']  = 'shipping method:';
$_['text_payment_address']  = 'payment address';
$_['text_payment_method']   = 'payment method:';
$_['text_comment']          = 'comments';
$_['text_history']          = 'history';
$_['text_success']          = 'تنبيه: لقد أضفت <a href="%s">%s</a> إلى <a href="%s">حقيبة التسوق</a>!';
$_['text_empty']            = 'empty';
$_['text_error']            = 'Not Found';
$_['text_track']            = 'Track';
$_['text_login'] = 'Login';
$_['text_new_customer'] = 'New Customer';
$_['text_register'] = 'Register';
$_['text_register_account'] = 'لكي تقوم بإنهاء الطلب قم بإنشاء حساب جديد معنا، فهو يُمكنك من الشراء بصورة أسرع و متابعة طلبيات الشراء التي تقدمت بها, و مراجعة سجل الطلبيات القديمة واسعتراض الفواتير وغير ذلك الكثير...';
$_['text_returning_customer'] = 'Login';
$_['text_i_am_returning_customer'] = 'I am Customer';
$_['text_forgotten'] = 'Forget Password';

// Entry
$_['entry_email'] = 'Email';

// Column
$_['column_order_id']       = 'Order Id';
$_['column_product']        = 'Products';
$_['column_customer']       = 'Customer';
$_['column_name']           = 'Name';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Quantity';
$_['column_price']          = 'Price';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';
$_['column_date_added']     = 'Date Added';
$_['column_status']         = 'status';
$_['column_comment']        = 'Comments';

// Error
$_['error_login'] = 'إنتبه: لم يحصل تطابق بين البريد الإكتروني و رقم الطلب المدخل .';

$_['error_reorder']         = '%s غير متاح لإعادة الطلب';
$_['text_tracking']         = 'تفاصيل شحن الطلب';
