<?php
// Heading
$_['heading_title'] = 'Add Brand';

// Text
$_['entry_image'] = 'Image';
$_['text_add'] = 'Add Brand';
$_['entry_name'] = 'Brand Name';
$_['entry_description'] = 'Brand description';
$_['button_save'] = 'Save';
