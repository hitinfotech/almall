<?php
$_['text_subject'] = 'Return Processed on order number %s';
$_['text_head'] = 'Return Processed';
$_['text_greeting'] = 'Dear %s,<br><br>Your return has been received and processed!<br><br>A credit for your return has been issued and will be reflected on your next credit card statement, according to the billing cycle. Please note that your refund total reflects all returned items from this order. Below are the details of your return order and fees.';
$_['text_more_info'] = 'If you have questions, please call us from UAE or KSA on 8004330033 or e-mail support@sayidatymall.net <br><br>Sincerely,<br><br>Sayidaty mall Customer Service';
$_['text_billing_title'] = 'Your Billing &amp; Shipping Information';
$_['text_payment_address'] = 'Billing Address:';
$_['text_shipping_address'] = 'Shipping Address:';
$_['text_payment_method'] = 'Payment Method:';
$_['text_return_summary'] = 'Your Return Summary';
$_['text_order_date'] = 'Order Date: ';
$_['text_order_number'] = 'Order Number: ';
$_['text_return_date'] = 'Return Authorization Date: ';
$_['text_return_number'] = 'Return Authorization Number: ';
$_['text_item'] = 'Item';
$_['text_new_price'] = 'Price';
$_['text_price'] = 'Price';
$_['text_sku'] = 'SKU';
$_['text_quantity'] = 'Quantity';
$_['text_total'] = 'Total';
$_['text_return_credits'] = 'Return Credits';
$_['text_item_subtotal'] = 'Item Subtotal:';
$_['text_original_shipping'] = 'Original Shipping:';
$_['text_duties'] = 'Duties:';
$_['text_coupon'] = 'Coupon:';
$_['text_total_credits'] = 'Total Credits:';
$_['text_return_charges'] = 'Return Charges';
$_['text_original_order_discaount'] = 'Original Order Discount:';
$_['text_return_shipping'] = 'Return Shipping:';
$_['text_total_charges'] = 'Total Charges: ';
$_['text_total_refund'] = 'Total Refund: ';

