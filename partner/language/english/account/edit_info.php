<?php
// Heading
$_['heading_title']  = 'Edit Information';

// Text
$_['text_account']   = 'Account';
$_['text_current_password']  = 'Your Current Password';
$_['text_password']  = 'Edit Password';
$_['text_success']   = 'Success: Your Information has been successfully updated.';

$_['text_email']  = 'Edit Email';
$_['entry_email']  = 'Email';
$_['entry_fbs_email'] = 'Second Email';


// Entry
$_['entry_current_password'] = 'Current Password';
$_['entry_password'] = 'Password';
$_['entry_confirm']  = 'Password Confirm';

// Error
$_['error_current_password'] = 'Error in current password';
$_['error_password'] = 'Password must be between 4 and 20 characters!';
$_['error_confirm']  = 'Password confirmation does not match password!';

$_['button_save'] = 'Save';
