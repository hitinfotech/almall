<?php
// Heading
$_['heading_title']     		= 'Brand Offer';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';


$_['entry_brand']              = 'Brand';
$_['entry_percentage']        = 'Percentage';

$_['button_add_discount']    = 'AddDiscount';
$_['button_add_special']     = 'AddSpecial';
$_['button_delete']          = 'Delete Brand Offers';
$_['button_insert']          = 'Add new Brand Offer';

$_['tab_special']   = 'Special';
$_['entry_date_start']   = 'DateStart';
$_['entry_date_end']   = 'DateEnd';

$_['heading_title_discountlist'] = 'Brand Offers';
$_['text_no_results'] = 'No Result was found';
$_['column_discount_id'] = '';
$_['column_name'] = 'Brand Name';
$_['column_percentage'] = 'Percentage';
$_['column_date_end'] = 'Date End';
$_['column_date_start'] = 'Date Start';
$_['column_action'] = 'Action';
$_['text_confirm']    = 'Are you sure ?';

$_['text_success_modify'] = 'Success: Your Brand Offer has been successfully modified.';

$_['error_brand']  = 'Brand Name is required';
$_['error_percentage']   = 'Percentage not correnct!';
$_['error_brand_no_products'] = 'you dnt have any product for this brand';

$_['text_success_modify']   = 'Your Brand Offers have been updated successfully';
