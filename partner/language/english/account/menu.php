<?php

// Text
$_['text_personal_account']  = 'Personal Account';
$_['text_notifications']     = 'My favorite';
$_['text_fav_shops']         = 'Favourite Shops';
$_['text_fav_products']      = 'Favourite Products';
$_['text_change_password']   = 'Change Password';
$_['text_address']           = 'Address';
$_['text_language']          = 'Favorite language';

$_['text_my_orders']         = 'My Orders';
$_['text_balance']           = 'My Balance';
$_['text_RMA']               = 'Return orders';
$_['text_newsletter']        = 'Newsletter';
$_['text_voucher']                = 'Credit';
