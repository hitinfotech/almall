<?php

/*
 * Language for ENGLISH offer page
 */

$_['head_title_catalog'] = 'Offers & Promotions in %s';
$_['head_title']         = 'Offers & Promotions';
$_['text_search']        = 'Search Shops';
$_['text_zones']         = 'all cities';
$_['text_offer_type']    = 'Offer Type';
$_['text_offer_time']    = 'Offer Time';
$_['text_categories']    = 'Categories';
$_['text_malls']         = 'Malls';

$_['text_available_shops']= 'Shops';
$_['text_similer_offers']   = 'Latest Offers';
$_['text_more_offers']      = 'more';
$_['text_similer_news']     = 'Related News';

$_['text_latest_news']      = 'Latest News';

$_['text_more_news']        = 'more';
//$_['text_results_for']      = 'Search result for "%s" in ';

$_['offer_in']               = '%s Offers & Promotions in %s %s';
$_['heading_meta_title']    = 'Offers & Promotions %s %s %s | Sayidaty Mall |  %s Fashion, Accessories, Dresses, Children Clothes, Bags, Watches';
$_['heading_meta_description']    = 'شاهدي كل العروض والتخفيضات من كبرى الماركات، عروض  2016 و تخفيضات مولات السعودية والإمارات، عروض الصيف، تخفيضات الشتاء، خصومات حقيقية على الحقائب والأحذية والفساتين';
