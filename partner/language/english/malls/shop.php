<?php

$_['text_shops_catalog']                = "Shops ";

$_['shops_in_zone']                     = 'Shops in %s';

$_['shops_in']                          = '%s %s Shops in %s %s';
$_['shop_in']                           = '%s Shops %s %s %s';

$_['text_shop_online']= 'Shop Online';
$_['shop_h1_title_1'] = 'Shops';
$_['shop_h1_title_2'] = '#zone# Shops';
$_['shop_h1_title_3'] = '#mall# Shops #zone#';
$_['shop_h1_title_4'] = '#category# Shops in #mall# #zone#';
$_['shop_h1_title_5'] = '#brand# #category# Shops in #mall# #zone#';


$_['heading_meta_title']    = 'Shops %s %s %s | Sayidaty Mall |  %s Fashion, Accessories, Dresses, Children Clothes, Bags, Watches';

$_['text_shops_intro']                  = " Sayidaty Mall is the ultimate guide for malls, shops and brands. search now for the shop name, mall or within the categories of fashion, accessories, skin care, beauty, furniture and all shopping criteria's in your city";
$_['text_shops_all_cities']             = "All Countries";
$_['text_shops_all_malls']              = "All Malls";
$_['text_shops_all_categories']         = 'All Categories';
$_['text_shops_all_brands']             = 'All Brands';
$_['text_search_all_shops']             = 'Search in Shops';
$_['text_shops_you_searched_for']       = 'You Shearched For';
$_['text_shops_arabic_name']            = 'Arabi Name';
$_['text_shops_english_name']           = 'English Name';
$_['text_shops_categories']             = 'Categories';
$_['text_shops_brands']                 = 'Brands';
$_['text_shops_phone']                  = 'Phone';
$_['text_shops_address']                = 'Address';
$_['text_shops_available_in_branshes']  = 'Available In Branshes';
$_['text_shops_official_links']         = 'Official Links';
$_['text_shops_products_from']          = 'Products From';
$_['text_shops_discounts_from']         = 'Discount from';
$_['text_shops_discount']               = 'Discount ';
$_['text_shops_more']                   = 'More';

$_['heading_meta_description']    =  "دليل محلات سيدتي مول، عناوين وفروع جميع محلات الرياض، جدة، دبي، أبوظبي، السعودية والإمارات. ابحثي عن عنوان و فرع اي محل أو مول وأسعار كل المحلات وصور منتجات ومقارنات، ";
$_['heading_meta_description2']    =  "%s فرع %s، إذا كنت تبحثين عن فروع %s في %s، %s";
