<?php

$_['head_title']            = 'Shopping news';
$_['heading_meta_title']    = 'Shopping News %s %s %s | Sayidaty Mall | %s Fashion, Accessories, Dresses, Children Clothes, Bags, Watches';

$_['text_news_catalog']     = "Shopping news ";
$_['news_in_zone']          = 'Shopping news in %s';
$_['news_in']               = '%s Shopping news in %s %s';

$_['text_search']           = 'Search in News';
$_['text_zones']            = 'All Countries';
$_['text_categories']       = 'Categories';
$_['text_malls']            = 'All malls';
$_['text_latest_news']      = 'Latest News';

$_['text_similer_offers']   = 'Simillar Offers';
$_['text_more_offers']      =  'More Offers';
$_['text_similer_news']     = 'Latest News';
$_['text_more_news']        =  'More';
$_['text_similer_products'] = 'Simillar Products';
$_['text_more_products']    =  'More Products';
$_['text_no_result']        =  'No search results found';
$_['text_no_result_title']  = 'Search result for "%s" in news';


$_['heading_meta_description']    = " تابعي كل جديد في عالم الموضة والجمال، اخبار اطلاق التشكيلات الجديدة من الماركات العالمية، أحداث وفعاليات الأزياء والموضة، نصائح في المكياج مع دليل للفعاليات في المولات والمراكز التجارية";
