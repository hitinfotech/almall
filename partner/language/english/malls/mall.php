<?php

$_['text_malls_catalog']            = 'Malls';
//$_['text_malls_in']                 = 'Malls in %s';
$_['text_shop_online']              = 'Shop Online';
$_['text_malls_in']                 = '%s Malls';
$_['text_malls_all_cities']         = 'All Countries';
$_['text_malls_search_mall_name']   = 'Search Mall Name';
$_['text_malls_recent_models_from'] = 'Latest Models';
$_['text_malls_categories']         = 'Categories ';
$_['text_malls_text_shop']          = 'Shop';
$_['text_malls_text_shops']         = 'Shops';
$_['text_malls_mall_name_arabic']   = 'Arabic Name';
$_['text_malls_mall_name_english']  = 'English Name';
$_['text_malls_shops_number']       = 'Number of shops';
$_['text_malls_phone_number']       = 'Phone';
$_['text_malls_address']            = 'Address';
$_['text_malls_official_links']     = 'Official Links';
$_['text_malls_latest_products']    = 'Latest Offers';
$_['text_malls_discount']           = 'Discount';
$_['text_malls_more']               = 'More';
$_['text_malls_latest_news']        = 'Latest News';

$_['heading_meta_title']    = 'Malls %s | Sayidaty Mall | %s Fashion, Accessories, Dresses, Children Clothes, Bags, Watches';
