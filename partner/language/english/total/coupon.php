<?php
// Heading
$_['heading_title'] = 'Use Coupon Code';

// Text
$_['text_coupon']   = 'Coupon (%s)';
$_['text_success']  = 'Success: Your coupon discount has been applied!';

// Entry
$_['entry_coupon']  = 'Enter your coupon here';

// Error
$_['error_coupon']  = 'Warning: Coupon is either invalid, expired or reached its usage limit!';
$_['error_empty']   = 'Warning: Please enter a coupon code!';

$_['error_coupon_subtotal'] = 'Warning: The value of lower demand reduction of the value of a coupon! ';
$_['error_coupon_uses_total'] = 'Warning: You have exceeded the maximum number of times to use the reduction coupon!';
$_['error_coupon_loggin'] = 'Warning: You must be logged in to use discount coupon!';
$_['error_coupon_products'] = 'Warning: reduction Coupon does not include these products!';

