<?php

header('Content-Type: text/html; charset=utf-8');
$max_age = 60 * 60 + time();
header("Cache-Control: max-age={$max_age}, must-revalidate");

///// Version
define('VERSION', '2.1.0.1');
define('ENVIROMENT', getenv('ENVIROMENT'));

// Configuration
require_once('config/config.php');

// Install
if (!defined('DIR_APPLICATION')) {
    header('Location: install/index.php');
    exit;
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

// Registry
$registry = Registry::getInstance();

// Cache
$cache = Cache::getInstance(CONFIG_CACHE);
$registry->set('cache', $cache);

// Loader
$loader = Loader::getInstance($registry);
$registry->set('load', $loader);

// Config
$config = Config::getInstance();
$registry->set('config', $config);

// Database
$db = DB::getInstance(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
$registry->set('db', $db);


// First Flight
$registry->set('FFServices', new FirstFlight($registry, SHIPPING_FIRST_FLIGHT_ID));

// Store
/*
  if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
  $query = ("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`ssl`, 'www.', '') = '" . $db->escape('https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
  } else {
  $query = ("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`url`, 'www.', '') = '" . $db->escape('http://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
  }

  $rows = $cache->get('store.http');
  if (!$rows) {
  $store = $db->query($query);
  $rows = $store->rows;
  if (!empty($query)) {
  $cache->set('store.http', $query);
  }
  }

  if (is_array($rows) && count($rows) > 0) {
  $config->set('config_store_id', $rows[0]['store_id']);
  } else {
  $config->set('config_store_id', 0);
  }
 */

$config->set('config_store_id', 0);

// Settings
$query = null; //$cache->get('settings');
if (!$query) {
    $query2 = $db->query("SELECT * FROM `" . DB_PREFIX . "setting` WHERE store_id = '0' OR store_id = '" . (int) $config->get('config_store_id') . "' ORDER BY store_id ASC");
    $query = $query2->rows;
    if (!empty($query)) {
        $cache->set('settings', $query);
    }
}

foreach ($query as $result) {
    if (!$result['serialized']) {
        $config->set($result['key'], $result['value']);
    } else {
        $config->set($result['key'], json_decode($result['value'], true));
    }
}

//if (count($rows) == 0) {
$config->set('config_url', HTTP_SERVER);
$config->set('config_ssl', HTTPS_SERVER);
//}
// Url
$url = new Url($config->get('config_url'), $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url'));
$registry->set('url', $url);

// Log
$log = new Log($config->get('config_error_filename'));
$registry->set('log', $log);

function error_handler($code, $message, $file, $line) {
    global $log, $config;

    // error suppressed with @
    if (error_reporting() === 0) {
        return false;
    }

    switch ($code) {
        case E_NOTICE:
        case E_USER_NOTICE:
            $error = 'Notice';
            break;
        case E_WARNING:
        case E_USER_WARNING:
            $error = 'Warning';
            break;
        case E_ERROR:
        case E_USER_ERROR:
            $error = 'Fatal Error';
            break;
        default:
            $error = 'Unknown';
            break;
    }

    if ($config->get('config_error_display')) {
        echo '<b>' . $error . '</b>: ' . $message . ' in <b>' . $file . '</b> on line <b>' . $line . '</b>';
    }

    if ($config->get('config_error_log')) {
        $log->write('PHP ' . $error . ':  ' . $message . ' in ' . $file . ' on line ' . $line);
    }

    return true;
}

// Error Handler
set_error_handler('error_handler');

// Request
$request = new Request();
$registry->set('request', $request);

// Response
$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$response->setCompression($config->get('config_compression'));
$registry->set('response', $response);

// Session
if (isset($request->get['token']) && isset($request->get['route']) && substr($request->get['route'], 0, 4) == 'api/') {
    $db->query("DELETE FROM `" . DB_PREFIX . "api_session` WHERE TIMESTAMPADD(HOUR, 1, date_modified) < NOW()");

    $query = $db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "api` `a` LEFT JOIN `" . DB_PREFIX . "api_session` `as` ON (a.api_id = as.api_id) LEFT JOIN " . DB_PREFIX . "api_ip `ai` ON (as.api_id = ai.api_id) WHERE a.status = '1' AND as.token = '" . $db->escape($request->get['token']) . "' AND ai.ip = '" . $db->escape($request->server['REMOTE_ADDR']) . "'");

    if ($query->num_rows) {
        // Does not seem PHP is able to handle sessions as objects properly so so wrote my own class
        $session = Session::getInstance($query->row['session_id'], $query->row['session_name']);
        $registry->set('session', $session);

        // keep the session alive
        $db->query("UPDATE `" . DB_PREFIX . "api_session` SET date_modified = NOW() WHERE api_session_id = '" . $query->row['api_session_id'] . "'");
    }
} else {
    $session = Session::getInstance();
    $registry->set('session', $session);
}

$route = "";
if (isset($request->get['_route_'])) {
    $route = $request->get['_route_'];
} elseif (isset($request->get['route'])) {
    $route = $request->get['route'];
}

$parts = explode('/', $route);

if (is_array($parts) && !empty($parts) && in_array($parts[0], array('sa', 'ae', 'SA', 'AE'))) {
    if ($parts[0] == 'ae' || $parts[0] == 'AE') {
        $country_code = 'ae';
    } else {
        $country_code = 'sa';
    }
    array_shift($parts);
}

if (is_array($parts) && !empty($parts) && in_array($parts[0], array('ar', 'AR', 'en', 'EN')) && !isset($request->get['route'])) {
    if ($parts[0] == 'en' || $parts[0] == 'EN') {
        $language_code = 'en';
    } else {
        $language_code = 'ar';
    }
    array_shift($parts);
} else {
    if(isset($request->get['route'])){
        $language_code = "";
    } else {
        $language_code = "en";
    }
}

$language_code = 'en';

// Language Detection
$languages = array();
$query = $cache->get('languages');
if (!$query) {
    $query = $db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE status = '1'");
    $query = $query->rows;
    if (!empty($query)) {
        $cache->set('languages', $query);
    }
}

foreach ($query as $result) {
    $languages[$result['code']] = $result;
}

if (isset($language_code) && array_key_exists($language_code, $languages)) {
    $language_code = $language_code;
    $session->data['language'] = $language_code;
} elseif (isset($session->data['language']) && array_key_exists($session->data['language'], $languages)) {
    $language_code = $session->data['language'];
} elseif (isset($request->cookie['language']) && array_key_exists($request->cookie['language'], $languages)) {
    $language_code = $request->cookie['language'];
} else {
    $detect = '';

    if (isset($request->server['HTTP_ACCEPT_LANGUAGE']) && $request->server['HTTP_ACCEPT_LANGUAGE']) {
        $browser_languages = explode(',', $request->server['HTTP_ACCEPT_LANGUAGE']);

        foreach ($browser_languages as $browser_language) {
            foreach ($languages as $key => $value) {
                if ($value['status']) {
                    $locale = explode(',', $value['locale']);

                    if (in_array($browser_language, $locale)) {
                        $detect = $key;
                        break 2;
                    }
                }
            }
        }
    }

    $language_code = $detect ? $detect : $config->get('config_language');
}

$session->data['language'] = $language_code;
if (!isset($session->data['language']) || $session->data['language'] != $language_code) {
    $session->data['language'] = $language_code;
}

if (!isset($request->cookie['language']) || $request->cookie['language'] != $language_code) {
    setcookie('language', $language_code, COOKIES_PERIOD, '/', $request->server['HTTP_HOST']);
}

$config->set('config_language_id', $languages[$language_code]['language_id']);
$config->set('config_language', $languages[$language_code]['code']);

// Language
$language = new Language($languages[$language_code]['directory']);
$language->load($languages[$language_code]['directory']);
$registry->set('language', $language);

$country = new Country($registry);
$registry->set('country', $country);
$country_code = strtolower($country->getCode());

if (!isset($session->data['country']) || $session->data['country'] != $country_code) {
    $session->data['country'] = $country_code;
}

if (!isset($request->cookie['country']) || $request->cookie['country'] != $country_code) {
    setcookie('country', $country_code, time() + 60 * 60 * 24 * 30, '/', $request->server['HTTP_HOST']);
}

// Document
$registry->set('document', new Document());

// Partner
$partner = new Partner($registry);
$registry->set('partner', $partner);

// Partner Group
if ($partner->isLogged()) {
    $config->set('config_partner_group_id', $partner->getGroupId());
} elseif (isset($session->data['partner']) && isset($session->data['partner']['partner_group_id'])) {
    // For API calls
    $config->set('config_partner_group_id', $session->data['partner']['partner_group_id']);
} elseif (isset($session->data['guest']) && isset($session->data['guest']['partner_group_id'])) {
    $config->set('config_partner_group_id', $session->data['guest']['partner_group_id']);
}

// Tracking Code
if (isset($request->get['tracking'])) {
    setcookie('tracking', $request->get['tracking'], COOKIES_PERIOD, '/');

    $db->query("UPDATE `" . DB_PREFIX . "marketing` SET clicks = (clicks + 1) WHERE code = '" . $db->escape($request->get['tracking']) . "'");
}

// Currency
$registry->set('currency', new Currency($registry));

// Facebook
$registry->set('facebook', new FacebookLibrary($registry));
// Tax
$registry->set('tax', new Tax($registry));

// Weight
$registry->set('weight', new Weight($registry));

// Length
$registry->set('length', new Length($registry));

// Custom URL
$registry->set('url_custom', new Url_custom($registry));

// Encryption
$registry->set('encryption', new Encryption($config->get('config_encryption')));

// AWS S3
$registry->set('aws_s3', new Aws_S3($registry));

// AWS SQS
$registry->set('aws_sqs', new Aws_SQS($registry));

//Common function
$registry->set('commonfunctions', new commonfunctions($registry));


// Event
$event = new Event($registry);
$registry->set('event', $event);

$query = $db->query("SELECT * FROM " . DB_PREFIX . "event");

foreach ($query->rows as $result) {
    $event->register($result['trigger'], $result['action']);
}

// Front Controller
$controller = new Front($registry);

// Maintenance Mode
$controller->addPreAction(new Action('common/maintenance'));

// SEO URL's
$controller->addPreAction(new Action('common/seo_url'));

// Router
if (isset($request->get['route'])) {
    $action = new Action($request->get['route']);
} else {
    $action = new Action('dashboard');
}

// Dispatch
$controller->dispatch($action, new Action('error/not_found'));

// Output
$response->output();
