<?php

class ControllerDashboardsReturns extends Controller {

    public function index() {
        // return;

        $this->load->language('account/customerpartner/dashboards/returns');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_view'] = $this->language->get('text_view');

        // Total Orders
        $this->load->model('account/wk_mprma');

        /* Removed becasue no need for percentage
          $today = $this->model_account_wk_mprma->viewtotal(array('filter_date' => date('Y-m-d', strtotime('-1 day')),'dashboard_rma' => 1));

          $yesterday = $this->model_account_wk_mprma->viewtotal(array('filter_date' => date('Y-m-d', strtotime('-2 day')),'dashboard_rma' => 1));

          $difference = $today - $yesterday;

          if ($difference && $today) {
          $data['percentage'] = round(($difference / $today) * 100);
          } else {
          $data['percentage'] = 0;
          } */

        $returns_total = $this->model_account_wk_mprma->viewtotal(array('dashboard_rma' => 1, 'filter_rma_status' => 0));

        if ($returns_total > 1000000000000) {
            $data['total'] = round($returns_total / 1000000000000, 1) . 'T';
        } elseif ($returns_total > 1000000000) {
            $data['total'] = round($returns_total / 1000000000, 1) . 'B';
        } elseif ($returns_total > 1000000) {
            $data['total'] = round($returns_total / 1000000, 1) . 'M';
        } elseif ($returns_total > 1000) {
            $data['total'] = round($returns_total / 1000, 1) . 'K';
        } else {
            $data['total'] = $returns_total;
        }

        $data['returns'] = $this->url->link('wk_mprma_manage', '', 'SSL');

        return ($this->load->view($this->config->get('config_template') . '/template/account/customerpartner/dashboards/returns.tpl', $data));
    }

}
