<?php

class ControllerInvoices extends Controller {

    private $error = array();

    public function index() {

        if (!$this->partner->isLogged()) {
            $this->session->data['response'] = $this->url->link('invoices', '', 'SSL');
            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        if ($this->config->get("config_language_id") == 2) {
            if ($this->language->get('code') == "ar") {
                $language_code = "en";
            } else {
                $language_code = "ar";
            }
            $url_data = $this->request->get;
            unset($url_data['_route_']);
            $route = $url_data['route'];
            unset($url_data['route']);
            $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            $this->response->redirect($this->url->link($route, $url, $this->request->server['HTTPS'], $language_code));
        }

        $this->load->model('account/customerpartner');

        $data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

        if (!$data['chkIsPartner']) {
            $this->response->redirect($this->url->link('account/login'));
        }
        $this->getlist();
    }

    public function getlist() {

        $this->load->language('account/customerpartner/invoices');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customerpartner/invoice');

        $filter_array = array(
            'filter_month',
            'filter_year',
            'page',
            'sort',
            'order',
            'start',
            'limit',
        );

        $url = '';

        foreach ($filter_array as $unsetKey => $key) {

            if (isset($this->request->get[$key])) {
                $filter_array[$key] = $this->request->get[$key];
                $data[$key] =$this->request->get[$key];
            } else {
                if ($key == 'page')
                    $filter_array[$key] = 1;
                elseif ($key == 'sort')
                    $filter_array[$key] = 'cc.id';
                elseif ($key == 'order')
                    $filter_array[$key] = 'ASC';
                elseif ($key == 'start')
                    $filter_array[$key] = ($filter_array['page'] - 1) * $this->config->get('config_product_limit');
                elseif ($key == 'limit')
                    $filter_array[$key] = $this->config->get('config_product_limit');
                else
                    $filter_array[$key] = null;
            }
            unset($filter_array[$unsetKey]);
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('dashboard')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('invoice')
        );

        $data['banners'] = false;

        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/moment.min.js');
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'css/sell.min.css');

        $results = $this->model_customerpartner_invoice->viewtotal($filter_array);

        $product_total = $this->model_customerpartner_invoice->viewtotalentry($filter_array);

        $lang_array = array('heading_title',
            'entry_id',
            'entry_month',
            'entry_year',
            'entry_action',
            'entry_view',
            'text_invoicesList',
            'button_filter',
            'entry_companyname',
            'entry_month_year',
            'entry_action'
        );

        foreach ($lang_array as $language) {
            $data[$language] = $this->language->get($language);
        }

        $data['invoices'] = array();

        foreach ($results as $result) {

            $data['invoices'][] = array(
                'selected' => False,
                'id' => $result['id'],
                'name' => $result['companyname'],
                'month_year' => $result['month'].'/'.$result['year'],
                'invoice_name' => $result['invoice_name'],
                'url' => HTTP_SERVER.'../image/'.$result['invoice_name']
            );
        }

        $url = '';

        foreach ($filter_array as $key => $value) {
            if (isset($this->request->get[$key])) {
                if (!isset($this->request->get['order']) AND isset($this->request->get['sort']))
                    $url .= '&order=DESC';
                if ($key == 'filter_name' || $key == 'filter_details' || $key == 'filter_date')
                    $url .= '&' . $key . '=' . urlencode(html_entity_decode($filter_array[$key], ENT_QUOTES, 'UTF-8'));
                elseif ($key == 'order')
                    $url .= $value == 'ASC' ? '&order=DESC' : '&order=ASC';
                elseif ($key != 'start' AND $key != 'limit' AND $key != 'sort')
                    $url .= '&' . $key . '=' . $filter_array[$key];
            }
        }

        $data['sort_id'] = $this->url->link('invoices', '' . '&sort=ct.id' . $url, 'SSL');
        $data['monthes_array'] = array(
          1 => 'January',
          2 => 'February',
          3 => 'March',
          4 => 'April ',
          5 => 'May',
          6 => 'June',
          7 => 'July',
          8 => 'August ',
          9 => 'September',
          10 => 'October ',
          11 => 'November',
          12 => 'December ',
        );

        $url = '';

        foreach ($filter_array as $key => $value) {
            if (isset($this->request->get[$key])) {
                if (!isset($this->request->get['order']) AND isset($this->request->get['sort']))
                    $url .= '&order=DESC';
                if ($key == 'filter_name' || $key == 'filter_details' || $key == 'filter_date')
                    $url .= '&' . $key . '=' . urlencode(html_entity_decode($filter_array[$key], ENT_QUOTES, 'UTF-8'));
                elseif ($key != 'page')
                    $url .= '&' . $key . '=' . $filter_array[$key];
            }
        }

        $pagination = new Pagination();
        $pagination->total = $product_total;
        $pagination->page = $filter_array['page'];
        $pagination->limit = $this->config->get('config_product_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('invoice', '' . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();
        $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($filter_array['page'] - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($filter_array['page'] - 1) * $this->config->get('config_product_limit')) > ($product_total - $this->config->get('config_product_limit'))) ? $product_total : ((($filter_array['page'] - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_product_limit')), $product_total, ceil($product_total / $this->config->get('config_product_limit')));

        foreach ($filter_array as $key => $value) {
            if ($key != 'start' AND $key != 'end')
                $data[$key] = $value;
        }

          $data['isMember'] = true;


        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header', $data);

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/customerpartner/invoice.tpl', $data));
    }

}
