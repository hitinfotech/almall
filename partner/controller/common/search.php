<?php

class ControllerCommonSearch extends Controller {

    public function index() {
        $this->load->language('common/search');


        $data['text_search'] = $this->language->get('text_search');

        if (isset($this->request->get['q'])) {
            $data['search'] = $this->request->get['q'];
        } else {
            $data['search'] = '';
        }
        if (isset($this->request->get['htype'])) {
            $data['htype'] = $this->request->get['htype'];
        } else {
            $data['htype'] = 'product';
        }

        $data['search_list'] = array(
            'product' => array('index' => 'product', 'image' => HTTPS_IMAGE_S3 . 'prod-icn.png', 'name' => $this->language->get('text_product'), 'link' => $this->url->link('search/category', '', 'SSL')),
            'brand' => array('index' => 'brand', 'image' => HTTPS_IMAGE_S3 . 'brand-icn.png', 'name' => $this->language->get('text_brand'), 'link' => $this->url->link('brands/brands', '', 'SSL')),
            'offer' => array('index' => 'offer', 'image' => HTTPS_IMAGE_S3 . 'offer-icn.png', 'name' => $this->language->get('text_offer'), 'link' => $this->url->link('search/offer', '', 'SSL')),
            'tip' => array('index' => 'tip', 'image' => HTTPS_IMAGE_S3 . 'tip-icn.png', 'name' => $this->language->get('text_tip'), 'link' => $this->url->link('tips/tips', '', 'SSL')),
            'look' => array('index' => 'look', 'image' => HTTPS_IMAGE_S3 . 'look-icn.png', 'name' => $this->language->get('text_look'), 'link' => $this->url->link('look/looks', '', 'SSL')),
                //'all' => array('index' => 'all', 'image' => 'image/all-icn.png', 'name' => $this->language->get('text_all'), 'link' => $this->url->link('search/category', '', 'SSL'))
        );

        if (!in_array(ENVIROMENT, array('local', 'development')) && $this->language->get('code') == "en") {
            
        } else {
            $data['search_list']['mall'] = array('index' => 'mall', 'image' => HTTPS_IMAGE_S3 . 'mall-icn.png', 'name' => $this->language->get('text_mall'), 'link' => $this->url->link('malls/malls', '', 'SSL'));
            $data['search_list']['shop'] = array('index' => 'shop', 'image' => HTTPS_IMAGE_S3 . 'shop-icn.png', 'name' => $this->language->get('text_shop'), 'link' => $this->url->link('malls/shops', '', 'SSL'));
        }

        $data['lang_code'] = $this->language->get('code');

        $data['action'] = $this->url->link('search/category', '', 'SSL');

        foreach ($data['search_list'] as $item) {
            if ($item['index'] == $data['htype']) {
                $data['action'] = $item['link'];
            }
        }
        return $this->load->view($this->config->get('config_template') . '/template/common/search.tpl', $data);
    }

}
