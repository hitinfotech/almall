<?php

class ControllerCommonMenu extends Controller {

    // Menu
    public function index($params = array()) {
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $this->load->model('tips/tips');

        $this->load->model('look/look');

        $data['categories'] = array();

        $data['language_id'] = $this->config->get('config_language_id');

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['top']) {

                // Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $child) {

                    // Level 3
                    $children2_data = array();

                    $child2 = $this->model_catalog_category->getCategoriesWithCount($child['category_id']);

                    foreach ($child2 as $child_child) {
                        if ($child_child['pcounter']) {
                            $filter_data2 = array(
                                'filter_category_id' => $child_child['category_id'],
                                'filter_sub_category' => true
                            );

                            $children2_data[] = array(
                                'category_id' => $child_child['category_id'],
                                'name' => $child_child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProductsForAlgolia($filter_data2) . ')' : ''),
                                'href' => $this->url->link('search/category', "q=&c=" . $category['name'] . ':' . $child['name'] . ':' . $child_child['name'] . '&path=' . $child_child['category_id'] . '_' . $child['category_id'] . '_' . $child_child['category_id'] . '&cpath=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child_child['category_id'], 'SSL', false, false, false)
                            );
                        }
                    }

                    $filter_data = array(
                        'filter_category_id' => $child['category_id'],
                        'filter_sub_category' => true
                    );

                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProductsForAlgolia($filter_data) . ')' : ''),
                        'children' => $children2_data,
                        'href' => $this->url->link('search/category', "q=&c=" . $category['name'] . ':' . $child['name'] . '&path=' . $child['category_id'] . '&cpath=' . $category['category_id'] . '_' . $child['category_id'], 'SSL', false, false, false)
                    );
                }

                $filter_data_1 = array(
                    'filter_category_id' => $category['category_id'],
                    'filter_sub_category' => true
                );
                // Level 1
                $data['categories'][] = array(
                    'category_id' => $category['category_id'],
                    'name' => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProductsForAlgolia($filter_data_1) . ')' : ''),
                    'children' => $children_data,
                    'column' => $category['column'] ? $category['column'] : 1,
                    'href' => $this->url->link('search/category', "c=" . $category['name'] . '&path=' . $category['category_id'] . '&cpath=' . $category['category_id'], 'SSL', false, false, false)
                );
            }
        }

        $tips_groups_result = $this->model_tips_tips->getTipsGroups();

        $data['tips_groups'] = array();

        $dTipsGroupsCounter = 5;

        foreach ($tips_groups_result as $row) {
            if ($dTipsGroupsCounter < 1)
                break;
            $data['tips_groups'][] = array(
                'tip_group_id' => $row['tip_group_id'],
                'name' => $row['name'],
                'href' => $this->url->link('tips/tips', '&filter_group_id=' . $row['tip_group_id'], 'SSL', false, false, false)
            );
            $dTipsGroupsCounter--;
        }

        $looks_groups = $this->model_look_look->getLooksGroups();
        $data['looks_groups'] = array();

        foreach ($looks_groups as $row) {
            $data['looks_groups'][] = array(
                'look_group_id' => $row['look_group_id'],
                'name' => $row['name'],
                'href' => $this->url->link('look/looks', '&filter_group_id=' . $row['look_group_id'], 'SSL', false, false, false)
            );
        }
        $this->load->language('common/header');
        $this->load->language('common/menu');

        $data['text_men'] = $this->language->get('text_men');
        $data['text_shirts'] = $this->language->get('text_shirts');
        $data['text_suits'] = $this->language->get('text_suits');
        $data['text_sport_wears'] = $this->language->get('text_sport_wears');
        $data['text_trousers_pants'] = $this->language->get('text_trousers_pants');
        $data['text_slippers'] = $this->language->get('text_slippers');
        $data['text_casual_shoes'] = $this->language->get('text_casual_shoes');
        $data['text_ties'] = $this->language->get('text_ties');
        $data['text_belts'] = $this->language->get('text_belts');
        $data['text_hats'] = $this->language->get('text_hats');
        $data['text_casual_bags'] = $this->language->get('text_casual_bags');
        $data['text_designers'] = $this->language->get('text_designers');
        $data['text_full_looks'] = $this->language->get('text_full_looks');
        $data['text_full_shops'] = $this->language->get('text_full_shops');
        $data['text_products'] = $this->language->get('text_products');
        $data['text_loginto_seller'] = $this->language->get("text_loginto_seller");


        $data['text_women'] = $this->language->get('text_women');
        $data['text_clothes'] = $this->language->get('text_clothes');
        $data['text_dresses'] = $this->language->get('text_dresses');
        $data['text_shirts_tshirts'] = $this->language->get('text_shirts_tshirts');
        $data['text_jackets_coats'] = $this->language->get('text_jackets_coats');
        $data['text_abaya'] = $this->language->get('text_abaya');
        $data['text_swimming_suits'] = $this->language->get('text_swimming_suits');
        $data['text_shoes'] = $this->language->get('text_shoes');
        $data['text_boots'] = $this->language->get('text_boots');
        $data['text_sandals'] = $this->language->get('text_sandals');
        $data['text_flat_shoes'] = $this->language->get('text_flat_shoes');
        $data['text_sport_shoes'] = $this->language->get('text_sport_shoes');
        $data['text_high_heels'] = $this->language->get('text_high_heels');
        $data['text_accessories'] = $this->language->get('text_accessories');
        $data['text_nicklesses'] = $this->language->get('text_nicklesses');
        $data['text_diamonds'] = $this->language->get('text_diamonds');
        $data['text_watches'] = $this->language->get('text_watches');
        $data['text_sunglasses'] = $this->language->get('text_sunglasses');
        $data['text_rings'] = $this->language->get('text_rings');
        $data['text_bags'] = $this->language->get('text_bags');
        $data['text_big_bags'] = $this->language->get('text_big_bags');
        $data['text_small_bags'] = $this->language->get('text_small_bags');

        $data['text_clatch'] = $this->language->get('text_clatch');
        $data['text_wallets'] = $this->language->get('text_wallets');
        $data['text_travel_bags'] = $this->language->get('text_travel_bags');
        $data['text_online'] = $this->language->get('text_online');

        $data['text_we_choose_for_you'] = $this->language->get('text_we_choose_for_you');

        $data['text_more'] = $this->language->get('text_more');
        $data['text_login'] = $this->language->get('text_login');
        $data['text_close'] = $this->language->get('text_close');
        $data['text_new_user'] = $this->language->get('text_new_user');
        $data['text_forgot_password'] = $this->language->get('text_forgot_password');
        $data['text_password'] = $this->language->get('text_password');
        $data['text_facebook_login'] = $this->language->get('text_facebook_login');
        $data['text_or'] = $this->language->get('text_or');
        $data['text_email'] = $this->language->get('text_email');

        $data['text_menu_shop'] = $this->language->get('text_menu_shop');
        $data['text_menu_title_shop'] = $this->language->get('text_menu_title_shop');
        $data['text_menu_title_shop2'] = $this->language->get('text_menu_title_shop2');


        $data['rma_rma'] = $this->url->link('wk_mprma_manage', '', 'SSL');
        $data['rma_reason'] = $this->url->link('wk_mprma_reason', '', 'SSL');
        $this->language->load('module/marketplace');
        $data['marketplace_status'] = $this->config->get('marketplace_status');
        $data['text_sell_header'] = $this->language->get('text_sell_header');
        $data['text_my_profile'] = $this->language->get('text_my_profile');
        $data['text_addproduct'] = $this->language->get('text_addproduct');
        $data['text_wkshipping'] = $this->language->get('text_wkshipping');
        $data['text_productlist'] = $this->language->get('text_productlist');
        $data['text_addproduct'] = $this->language->get('text_addproduct');
        $data['text_dashboard'] = $this->language->get('text_dashboard');
        $data['text_orderhistory'] = $this->language->get('text_orderhistory');
        $data['text_orderhistory_pending'] = $this->language->get('text_orderhistory_pending');
        $data['text_becomePartner'] = $this->language->get('text_becomePartner');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_transaction'] = $this->language->get('text_transaction');
        $data['text_seller_account'] = $this->language->get('text_seller_account');
        $data['text_edit_info'] = $this->language->get('text_edit_info');
        $data['text_seller_discounts'] = $this->language->get('text_seller_discounts');

        $data['marketplace_allowed_account_menu'] = $this->config->get('marketplace_allowed_account_menu');
        $data['mp_addproduct'] = $this->url->link('addproduct', '', 'SSL');
        $data['mp_productlist'] = $this->url->link('productlist', '', 'SSL');
        $data['mp_addproduct'] = $this->url->link('addproduct', '', 'SSL');
        $data['mp_dashboard'] = $this->url->link('dashboard', '', 'SSL');
        $data['mp_add_shipping_mod'] = $this->url->link('add_shipping_mod', '', 'SSL');
        $data['mp_orderhistory'] = $this->url->link('orderlist', '', 'SSL');
        $data['mp_orderhistory_pending'] = $this->url->link('orderlist', '&filter_status=Pending', 'SSL');
        $data['mp_download'] = $this->url->link('download', '', 'SSL');
        $data['mp_profile'] = $this->url->link('profile', '', 'SSL');
        $data['mp_want_partner'] = $this->url->link('become_partner', '', 'SSL');
        $data['mp_transaction'] = $this->url->link('transaction', '', 'SSL');
        $data['mp_seller_account'] = $this->url->link('customerpartner/profile', '&id=' . $this->partner->getId(), 'SSL');
        $data['edit_info'] = $this->url->link('account/editInfo','','SSL');
        $data['mp_seller_discounts'] = $this->url->link('discountlist', '', 'SSL');

        $data['chkIsPartner'] = true;

        $data['text_rma_rma'] = $this->language->get('text_rma_rma');
        $data['text_rma_reason'] = $this->language->get('text_rma_reason');



        if ($this->config->get('wk_rma_status') && $this->config->get('wk_rma_status') == 1) {
            $data['wk_rma_status'] = 1;
        } else {
            $data['wk_rma_status'] = false;
        }

        //Links
        $data['text_mother_day'] = $this->language->get('text_mother_day');
        $data['mother_day'] = $this->url->link('search/mother_day', '', 'SSL', false, false, false);

        $data['brands'] = $this->url->link('brands/brands', '', 'SSL', false, false, false);
        $data['text_brands'] = $this->language->get('text_brands');

        $data['offers'] = $this->url->link('product/offer', '', 'SSL', false, false, false);
        $data['text_offers'] = $this->language->get('text_offers');

        $data['bundle_link'] = $this->url->link('module/productbundles/listing', '', 'SSL', false, false, false);
        $data['text_bundle'] = $this->language->get('text_bundle');

        $data['text_offer'] = $this->language->get('text_offer');
        $data['offer'] = $this->url->link('search/offer', '', 'SSL', false, false, false);

        $data['news'] = $this->url->link('malls/news', '', 'SSL', false, false, false);
        $data['text_news'] = $this->language->get('text_news');

        $data['text_tag'] = $this->language->get('text_tag');
        $data['tag'] = $this->url->link('tags/tags', '', 'SSL', false, false, false);

        $data['looks'] = $this->url->link('look/looks', '', 'SSL', false, false, false);
        $data['text_looks'] = $this->language->get('text_looks');

        $data['tips'] = $this->url->link('tips/tips', '', 'SSL', false, false, false);
        $data['text_tips'] = $this->language->get('text_tips');

        $data['malls'] = $this->url->link('malls/malls', '', 'SSL', false, false, false);
        $data['text_malls_catalog'] = $this->language->get('text_malls_catalog');

        $data['shops'] = $this->url->link('malls/shops', '', 'SSL', false, false, false);
        $data['text_shops_catalog'] = $this->language->get('text_shops_catalog');

        $data['product_search'] = $this->url->link('search/category', '', 'SSL', false, false, false);
        $data['text_product_search'] = $this->language->get('text_product_search');

        $data['text_tracking'] = $this->language->get('text_tracking');
        $data['tracking'] = $this->url->link('account/tracking', '', 'SSL', false, false, false);

        $data['text_toppages'] = $this->language->get('text_toppages');
        $data['toppages'] = $this->url->link('toppages/toppages', '', 'SSL', false, false, false);

        $data['logo'] = $params['logo'];

        $data['logged'] = $this->partner->isLogged();

        $data['login_url'] = "#";
        if ($this->partner->isLogged()) {
            $data['login_url'] = $this->url->link('account/account', '', 'SSL');
        }

        $data['register_url'] = $this->url->link('account/register', '', 'SSL');
        $data['forgot_password'] = $this->url->link('account/forgotten', '', 'SSL');
        $data['fbLoginUrl'] = $this->facebook->getRedirectLogin();

        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $data['language'] = $this->load->controller('common/language');
        $data['country'] = $this->load->controller('common/country');

        $data['isMobile'] = $this->is_mobile();

        if ($this->partner->isLogged()) {
            $data['account'] = $this->url->link('account/account', '', 'SSL');
            $data['text_account'] = sprintf($this->language->get("text_welcome"), $this->partner->getFirstName() . ' ' . $this->partner->getLastName());
            $data['text_logout'] = $this->language->get("text_logout");
            $data['logout'] = $this->url->link('account/logout', '', 'SSL');
        } else {
            $data['login'] = $this->url->link('account/login', '', 'SSL');
            $data['text_account'] = $this->language->get('text_account');
        }

        $data['active'] = 'shopable';
        if (!isset($this->request->get['route'])) {
            $data['active'] = 'shopable';
        } else {
            $url_data = $this->request->get;

            unset($url_data['_route_']);

            $route = $url_data['route'];

            unset($url_data['route']);

            $data['active'] = 'shopable';

            switch ($route) {
                case 'tips/tips':
                case 'tips/details':
                    $data['active'] = 'tips';
                    break;
                case 'look/looks':
                case 'look/details':
                    $data['active'] = 'looks';
                    break;
                case 'brands/brands':
                case 'brands/brand':
                    $data['active'] = 'brands';
                    break;
                case 'malls/malls':
                case 'malls/mall':
                case 'malls/shops':
                case 'malls/shop':
                case 'malls/news':
                case 'malls/news/details':
                case 'product/offer':
                case 'product/offer/details':
                    $data['active'] = 'product';
                    break;
                case 'product/product':
                    $product_info = $this->db->query("SELECT stock_status_id FROM product WHERE product_id = '" . (int) (isset($this->request->get['product_id']) ? $this->request->get['product_id'] : 0) . "' ");
                    if ($product_info->num_rows && $product_info->row['stock_status_id'] != 10) {
                        $data['active'] = 'shopable';
                    } else {
                        $data['active'] = 'product';
                    }
                    break;
                default :
                    $data['active'] = 'shopable';
            }
        }

        return $this->load->view($this->config->get('config_template') . '/template/common/menu.tpl', $data);
    }

}
