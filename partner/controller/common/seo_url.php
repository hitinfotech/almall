<?php

class ControllerCommonSeoUrl extends Controller {

    private $landing_pages_routes = array(
        'brands/brands',
        'brands/brand',
        'look/looks',
        'tips/tips',
        'malls/malls',
        'malls/news',
        'malls/shops',
        'search/product',
        'search/shopable',
        'search/category',
        'search/search',
        'product/offer',
        'tags/tags',
        'account/newsletter',
        'checkout/cart',
        'checkout/checkout',
        'checkout/checkout/confirm',
        'checkout/checkout/country',
        'toppages/toppages',
        'toppages/details',
        'shoppingsurvey/shoppingsurvey'
    );
    private $exceptions = array('sign', 'seller', 'نصائح التسوق', 'الماركات', 'looks', 'products', 'brands', 'has_tags', 'malls', 'المولات', 'shops', 'الأخبار', 'news', 'offers-promotions', 'pages', 'top5');
    private $exceptions_ids = array('tip_id', 'partner_id', 'tag_id', 'look_id', 'product_id', 'category_id', 'brand_id', 'mall_id', 'shop_id', 'news_id', 'offer_id', 'information_id', 'manufacturer_id', 'top_pages_id', 'return_id');

    public function index() {
        // Add rewrite to url class
        if ($this->config->get('config_seo_url')) {
            $this->url->addRewrite($this);
        }

        // Decode URL
        if (isset($this->request->get['_route_'])) {
            $parts = explode('/', $this->request->get['_route_']);
            if (is_array($parts) && !empty($parts) && in_array($parts[0], array('sa', 'ae', 'SA', 'AE'))) {
                if ($parts[0] == 'ae' || $parts[0] == 'AE') {
                    $this->country->set('ae');
                    $this->currency->set('AED');
                }
                if ($parts[0] == 'sa' || $parts[0] == 'SA') {
                    $this->country->set('sa');
                    $this->currency->set('SAR');
                }
                array_shift($parts);
            }

            if (is_array($parts) && !empty($parts) && in_array($parts[0], array('ar', 'AR', 'en', 'EN'))) {
                if ($parts[0] == 'en' || $parts[0] == 'EN') {
                    $this->session->data['language'] = 'en';
                }
                if ($parts[0] == 'ar' || $parts[0] == 'AR') {
                    $this->session->data['language'] = 'ar';
                }
                array_shift($parts);
            }

            $collecttrim = '';
            foreach ($parts as $row) {
                $collecttrim .= $row . '/';
            }
            $trim = rtrim($collecttrim, '/');

            if (!empty($parts)) {
                if (in_array($parts[0], $this->exceptions)) {
                    $parts = array(0 => rtrim($trim, '/'));
                }

                if (utf8_strlen(end($parts)) == 0) {
                    array_pop($parts);
                }

                foreach ($parts as $part) {

                    if (strpos($part, 'seller') !== false) {
                        $sql = "SELECT * FROM " . DB_PREFIX . "url_partner WHERE keyword = '" . $this->db->escape($part) . "'";
                        $query = $this->db->query($sql);
                    } else {
                        $sql = "SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE keyword = '" . $this->db->escape($part) . "'";
                        $query = $this->db->query($sql);
                    }

                    if ($query->num_rows) {
                        $url = explode('=', $query->row['query']);

                        if ($url[0] == 'product_id') {
                            $this->request->get['product_id'] = $url[1];
                        }

                        if ($url[0] == 'tag_id') {
                            $this->request->get['tag_id'] = $url[1];
                        }

                        if ($url[0] == 'partner_id') {
                            $this->request->get['id'] = $url[1];
                            $this->request->get['partner_id'] = $url[1];
                        }

                        if ($url[0] == 'brand_id') {
                            $this->request->get['brand_id'] = $url[1];
                        }

                        if ($url[0] == 'offer_id') {
                            $this->request->get['offer_id'] = $url[1];
                        }

                        if ($url[0] == 'look_id') {
                            $this->request->get['look_id'] = $url[1];
                        }

                        if ($url[0] == 'tip_id') {
                            $this->request->get['tip_id'] = $url[1];
                        }

                        if ($url[0] == 'mall_id') {
                            $this->request->get['mall_id'] = $url[1];
                        }

                        if ($url[0] == 'shop_id') {
                            $this->request->get['shop_id'] = $url[1];
                        }

                        if ($url[0] == 'news_id') {
                            $this->request->get['news_id'] = $url[1];
                        }

                        if ($url[0] == 'category_id') {
                            if (!isset($this->request->get['path'])) {
                                $this->request->get['path'] = $url[1];
                            } else {
                                $this->request->get['path'] .= '_' . $url[1];
                            }
                        }

                        if ($url[0] == 'manufacturer_id') {
                            $this->request->get['manufacturer_id'] = $url[1];
                        }

                        if ($url[0] == 'information_id') {
                            $this->request->get['information_id'] = $url[1];
                        }

                        if ($url[0] == 'top_pages_id') {
                            $this->request->get['top_pages_id'] = $url[1];
                        }

                        if ($url[0] == 'return_id') {
                            $this->request->get['return_id'] = $url[1];
                        }

                        if (!in_array($url[0], $this->exceptions_ids)) {
                            $this->request->get['route'] = $query->row['query'];
                        }
                    } else {
                        if (!isset($this->request->server['HTTP_REDIRECTED']) || $this->request->server['HTTP_REDIRECTED'] == false) {
                            $this->redurl($part, $this->country->getcode(), $this->session->data['language']);
                        }

                        $this->request->get['route'] = 'error/not_found';

                        break;
                    }
                }

                if (!isset($this->request->get['route'])) {
                    if (isset($this->request->get['product_id'])) {
                        $this->request->get['route'] = 'product/product';
                    } elseif (isset($this->request->get['brand_id'])) {
                        $this->request->get['route'] = 'brands/brand';
                    } elseif (isset($this->request->get['look_id'])) {
                        $this->request->get['route'] = 'look/details';
                    } elseif (isset($this->request->get['tip_id'])) {
                        $this->request->get['route'] = 'tips/details';
                    } elseif (isset($this->request->get['partner_id'])) {
                        $this->request->get['route'] = 'customerpartner/profile';
                    } elseif (isset($this->request->get['tag_id'])) {
                        $this->request->get['route'] = 'tags/details';
                    } elseif (isset($this->request->get['mall_id'])) {
                        $this->request->get['route'] = 'malls/mall';
                    } elseif (isset($this->request->get['shop_id'])) {
                        $this->request->get['route'] = 'malls/shop';
                    } elseif (isset($this->request->get['news_id'])) {
                        $this->request->get['route'] = 'malls/news/details';
                    } elseif (isset($this->request->get['offer_id'])) {
                        $this->request->get['route'] = 'product/offer/details';
                    } elseif (isset($this->request->get['tip_id'])) {
                        $this->request->get['route'] = 'tips/details';
                    } elseif (isset($this->request->get['path'])) {
                        $this->request->get['route'] = 'search/category';
                    } elseif (isset($this->request->get['manufacturer_id'])) {
                        $this->request->get['route'] = 'product/manufacturer/info';
                    } elseif (isset($this->request->get['information_id'])) {
                        $this->request->get['route'] = 'information/information';
                    } elseif (isset($this->request->get['top_pages_id'])) {
                        $this->request->get['route'] = 'toppages/details';
                    }
                }

                if (isset($this->request->get['route'])) {
                    return new Action($this->request->get['route']);
                }
            }
        }
    }

    private function redurl($url, $country = 'sa', $language = 'ar') {
        if ($language == 'ar') {
            $language = '';
        }
        //PRODUCTS
        if (strpos($url, 'products/') === 0) {
            $ob_id = (int) ltrim($url, 'products/');
            header('REDIRECTED: true');
            $query = $this->db->query("SELECT keyword FROM url_alias_".$this->language->get('code')." WHERE query='product_id=" . (int) $ob_id . "'");
            if ($query->num_rows) {
                $url = HTTPS_CATALOG . $country . '/' . ($language != '' ? $language . '/' : '') . $query->row['keyword'];
                $this->response->redirect($url);
                exit;
            }
        }

        //TIPS
        if (strpos($url, 'نصائح التسوق') === 0) {
            $ob_id = (int) ltrim($url, ('نصائح التسوق/'));
            header('REDIRECTED: true');
            $query = $this->db->query("SELECT keyword FROM url_alias_".$this->language->get('code')." WHERE query='tip_id=" . (int) $ob_id . "'");
            $url = HTTPS_CATALOG . $country . '/' . ($language != '' ? $language . '/' : '') . $query->row['keyword'];
            $this->response->redirect($url);
            exit;
        }

        //LOOKS
        if (strpos($url, 'looks/') === 0) {
            $ob_id = (int) ltrim($url, 'looks/');
            header('REDIRECTED: true');
            $query = $this->db->query("SELECT keyword FROM url_alias_".$this->language->get('code')." WHERE query='look_id=" . (int) $ob_id . "'");
            if ($query->num_rows) {
                $url = HTTPS_CATALOG . $country . '/' . ($language != '' ? $language . '/' : '') . $query->row['keyword'];
                $this->response->redirect($url);
                exit;
            }
        }

        //BRANDS
        if (strpos($url, 'الماركات/brands/') === 0) {
            $ob_id = (int) ltrim($url, 'الماركات/brands/');
            header('REDIRECTED: true');
            $query = $this->db->query("SELECT keyword FROM url_alias_".$this->language->get('code')." WHERE query='brand_id=" . (int) $ob_id . "'");
            if ($query->num_rows) {
                $url = HTTPS_CATALOG . $country . '/' . ($language != '' ? $language . '/' : '') . $query->row['keyword'];
                $this->response->redirect($url);
                exit;
            }
        }

        //MALLS
        if (strpos($url, 'malls/') === 0) {
            $ob_id = (int) ltrim($url, 'malls/');
            header('REDIRECTED: true');
            $query = $this->db->query("SELECT keyword FROM url_alias_".$this->language->get('code')." WHERE query='mall_id=" . (int) $ob_id . "'");
            if ($query->num_rows) {
                $url = HTTPS_CATALOG . $country . '/' . ($language != '' ? $language . '/' : '') . $query->row['keyword'];
                $this->response->redirect($url);
                exit;
            }
        }

        //SHOPS
        if (strpos($url, 'shops/') === 0) {
            $ob_id = (int) ltrim($url, 'shops/');
            header('REDIRECTED: true');
            $query = $this->db->query("SELECT keyword FROM url_alias_".$this->language->get('code')." WHERE query='shop_id=" . (int) $ob_id . "'");
            if ($query->num_rows) {
                $url = HTTPS_CATALOG . $country . '/' . ($language != '' ? $language . '/' : '') . $query->row['keyword'];
                $this->response->redirect($url);
                exit;
            }
        }

        //NEWS
        if (strpos($url, 'الأخبار/news/') === 0) {
            $ob_id = (int) ltrim($url, 'الأخبار/news/');
            header('REDIRECTED: true');
            $query = $this->db->query("SELECT keyword FROM url_alias_".$this->language->get('code')." WHERE query='news_id=" . (int) $ob_id . "'");
            if ($query->num_rows) {
                $url = HTTPS_CATALOG . $country . '/' . ($language != '' ? $language . '/' : '') . $query->row['keyword'];
                $this->response->redirect($url);
                exit;
            }
        }

        //OFFERS
        if (strpos($url, 'offers-promotions/') === 0) {
            $ob_id = (int) ltrim($url, 'offers-promotions/');
            header('REDIRECTED: true');
            $query = $this->db->query("SELECT keyword FROM url_alias_".$this->language->get('code')." WHERE query='offer_id=" . (int) $ob_id . "'");
            if ($query->num_rows) {
                $url = HTTPS_CATALOG . $country . '/' . ($language != '' ? $language . '/' : '') . $query->row['keyword'];
                $this->response->redirect($url);
                exit;
            }
        }
    }

    public function rewrite($link, $orig_language = false, $orig_country = false, $partner_link = true) {
        $url_info = parse_url(str_replace('&amp;', '&', $link));


        $url = '';

        if (isset($this->request->get['language'])) {
            if ($this->request->get['language'] == 'ar') {
                $language_code = "";
            } else {
                $language_code = "en";
            }
        } else {
            if (!in_array($orig_language, array('en', 'ar'))) {
                $language_code = ($this->language->get('code') == "ar" ? "" : "en");
            } else {
                $language_code = $orig_language == "ar" ? "" : "en";
            }
        }
        if (!in_array(strtolower($orig_country), array('sa', 'ae'))) {
            $country_code = strtolower($this->country->getCode());
        } else {
            $country_code = $orig_country;
        }

        $data = array();

        parse_str($url_info['query'], $data);

        foreach ($data as $key => $value) {

            if (isset($data['route'])) {

                if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'toppages/toppages')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = 'toppages/toppages'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'toppages/details' && $key == 'top_pages_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'malls/mall' && $key == 'mall_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'customerpartner/profile' && $key == 'id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_partner WHERE `query` = 'partner_" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'account/customerpartner/addproduct' && $key == 'product_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_partner WHERE `query` = 'account/customerpartner/addproduct'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'] . '?product_id=' . $value;

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'account/customerpartner/orderinfo' && $key == 'order_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_partner WHERE `query` = 'account/customerpartner/orderinfo'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'] . '?order_id=' . $value;

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'account/customerpartner/wk_mprma_manage/getForm' && $key == 'return_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_partner WHERE `query` = 'account/customerpartner/wk_mprma_manage/getForm'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'] . '?return_id=' . $value;

                        unset($data[$key]);
                    }
                } elseif (in_array($data['route'], array('account/customerpartner/dashboard', 'account/customerpartner/orderlist', 'account/customerpartner/transaction', 'account/customerpartner/productlist', 'account/customerpartner/wk_mprma_manage'))) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_partner WHERE `query` = '" . $this->db->escape($data['route']) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'feed/google_sitemap')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = 'feed/google_sitemap'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'tags/details' && $key == 'tag_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'malls/shop' && $key == 'shop_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'look/details' && $key == 'look_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'tips/details' && $key == 'tip_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'brands/brand' && $key == 'brand_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif (($data['route'] == 'common/home')) {
                    $url .= '/' . $country_code . ($language_code == "" ? "" : '/' . $language_code);

                    unset($data[$key]);
                } elseif (($data['route'] == 'account/login')) {
                    
                } elseif (($data['route'] == 'account/account')) {
                    
                } elseif (($data['route'] == 'account/logout')) {
                    
                } elseif (($data['route'] == 'account/tracking')) {
                    
                } elseif (($data['route'] == 'product/offer/details' && $key == 'offer_id')) {
                    
                } elseif (($data['route'] == 'malls/news/details' && $key == 'news_id')) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = '" . $this->db->escape($key . '=' . (int) $value) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                } elseif ($data['route'] == 'product/category' && $key == 'path') {
                    $categories = explode('_', $value);

                    foreach ($categories as $category) {
                        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = 'category_id=" . (int) $category . "'");

                        if ($query->num_rows && $query->row['keyword']) {
                            $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];
                        } else {
                            $url = '';
                            break;
                        }
                    }

                    unset($data[$key]);
                } elseif ($data['route'] == 'search/category' && $key == 'path') {
                    $categories = explode('_', $value);
                    $last_idx = end($categories);

                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = 'category_id=" . (int) $last_idx . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];
                    } else {
                        $url = '';

                        break;
                    }

                    unset($data[$key]);
                } elseif (in_array($data['route'], $this->landing_pages_routes)) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias_".$this->language->get('code')." WHERE `query` = '" . $this->db->escape($data['route']) . "'");

                    if ($query->num_rows && $query->row['keyword']) {
                        $url .= '/' . ($country_code!=''? $country_code . '/':'') . ($language_code == "" ? "" : $language_code . '/') . $query->row['keyword'];

                        unset($data[$key]);
                    }
                }
            }
        }
        if ($url) {
            unset($data['route']);
            $query = '';
            if ($data) {
                foreach ($data as $key => $value) {
                    $query .= '&' . rawurlencode((string) $key) . '=' . rawurlencode((string) $value);
                }
                if ($query) {
                    $query = '?' . str_replace('&', '&amp;', trim($query, '&'));
                }
            }

            /* $arr = array(
              'schem' => $url_info['scheme'],
              'host' => $url_info['host'],
              'port' => ($url_info['port'] ? $url_info['port'] : ''),
              'link' => str_replace('/index.php', '', $url_info['path']),
              'url' => $url,
              'quer' => $query);
              echo "<pre>";
              die(var_dump($arr)); */

            if ($partner_link) {
                return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
            } else {
                return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace(array('/index.php', '/partner'), '', $url_info['path']) . $url . $query;
            }
        } else {
            if ($partner_link) {
                return $link;
            } else {
                return str_replace('/partner', '', $link);
            }
        }
    }
}