<?php

class ControllerCommonFooter extends Controller {

    public function index() {
        $this->load->language('common/footer');

        $data['scripts'] = $this->document->getScripts('footer');

        $data['text_information'] = $this->language->get('text_information');
        $data['text_service'] = $this->language->get('text_service');
        $data['text_extra'] = $this->language->get('text_extra');
        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_return'] = $this->language->get('text_return');
        $data['text_sitemap'] = $this->language->get('text_sitemap');
        $data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $data['text_voucher'] = $this->language->get('text_voucher');
        $data['text_affiliate'] = $this->language->get('text_affiliate');
        $data['text_special'] = $this->language->get('text_special');
        $data['text_account'] = $this->language->get('text_account');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_wishlist'] = $this->language->get('text_wishlist');
        $data['text_newsletter'] = $this->language->get('text_newsletter');

        $data['text_about_us'] = $this->language->get('text_about_us');
        $data['text_shop_online'] = $this->language->get('text_shop_online');
        $data['text_customer_service'] = $this->language->get('text_customer_service');
        $data['text_follow_us'] = $this->language->get('text_follow_us');
        $data['text_call_us_form'] = $this->language->get('text_call_us_form');
        $data['text_do_you_have_questions'] = $this->language->get('text_do_you_have_questions');
        $data['text_job_times'] = $this->language->get('text_job_times');

        $data['text_brands'] = $this->language->get('text_brands');
        $data['text_freeshipping'] = $this->language->get('text_freeshipping');
        $data['text_return'] = $this->language->get('text_return');
        $data['text_cod'] = $this->language->get('text_cod');

        $data['text_shopping'] = $this->language->get('text_shopping');
        $data['text_woman'] = $this->language->get('text_woman');
        $data['text_man'] = $this->language->get('text_man');
        $data['text_newarrival'] = $this->language->get('text_newarrival');
        $data['text_bestseller'] = $this->language->get('text_bestseller');
        $data['text_designer'] = $this->language->get('text_designer');
        $data['text_sizes'] = $this->language->get('text_sizes');

        $data['text_sayidaty_mall'] = $this->language->get('text_sayidaty_mall');
        $data['text_subscribe'] = $this->language->get('text_subscribe');
        $data['text_subscribe_desc'] = $this->language->get('text_subscribe_desc');
        $data['text_female'] = $this->language->get('text_female');
        $data['text_male'] = $this->language->get('text_male');
        $data['text_email'] = $this->language->get('text_email');

        $this->load->model('catalog/information');

        $data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
            if ($result['bottom']) {
                $data['informations'][] = array(
                    'title' => $result['title'],
                    'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'], 'SSL', false, false, false)
                );
            }
        }

        $route = isset($this->request->get['route']) ? $this->request->get['route'] : '';
        if ($route != 'checkout/checkout') {
            $data['addthis'] = true;
        } else {
            $data['addthis'] = false;
        }

        $data['contact'] = $this->url->link('ticketsystem/generatetickets', '', 'SSL', false, false, false);
        $data['return'] = $this->url->link('account/return/add', '', 'SSL');
        $data['sitemap'] = $this->url->link('information/sitemap', '', 'SSL', false, false, false);
        $data['manufacturer'] = $this->url->link('product/manufacturer', '', 'SSL', false, false, false);
        $data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
        $data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
        $data['special'] = $this->url->link('product/special', '', 'SSL', false, false, false);
        $data['account'] = $this->url->link('account/account', '', 'SSL');
        $data['order'] = $this->url->link('account/order', '', 'SSL');
        $data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL', false, false, false);
        $data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
        $data['wk_rma_status'] = $this->config->get('wk_rma_status');
        $data['wkrma_system'] = $this->url->link('account/rma/rmalogin', '', 'SSL');
        $data['newarrival'] = $this->url->link('search/category', 'sort=new_' . $this->country->getcode(), 'SSL', false, false, false);

        $data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

        // Whos Online
        if ($this->config->get('config_customer_online')) {
            $this->load->model('tool/online');

            $ip = $this->customer->getIPAddress();

            if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
                $url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
            } else {
                $url = '';
            }

            if (isset($this->request->server['HTTP_REFERER'])) {
                $referer = $this->request->server['HTTP_REFERER'];
            } else {
                $referer = '';
            }

            $this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
        }

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['top']) {

                // Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $child) {

                    // Level 3
                    $children2_data = array();

                    $child2 = $this->model_catalog_category->getCategoriesWithCount($child['category_id']);

                    foreach ($child2 as $child_child) {
                        if ($child_child['pcounter']) {
                            $filter_data2 = array(
                                'filter_category_id' => $child_child['category_id'],
                                'filter_sub_category' => true
                            );

                            $children2_data[] = array(
                                'category_id' => $child_child['category_id'],
                                'name' => $child_child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProductsForAlgolia($filter_data2) . ')' : ''),
                                'href' => $this->url->link('search/category', "q=&c=" . $category['name'] . ':' . $child['name'] . ':' . $child_child['name'] . '&path=' . $child_child['category_id'] . '_' . $child['category_id'] . '_' . $child_child['category_id'] . '&cpath=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child_child['category_id'], 'SSL', false, false, false)
                            );
                        }
                    }

                    $filter_data = array(
                        'filter_category_id' => $child['category_id'],
                        'filter_sub_category' => true
                    );

                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProductsForAlgolia($filter_data) . ')' : ''),
                        'children' => $children2_data,
                        'href' => $this->url->link('search/category', "q=&c=" . $category['name'] . ':' . $child['name'] . '&path=' . $child['category_id'] . '&cpath=' . $category['category_id'] . '_' . $child['category_id'], 'SSL', false, false, false)
                    );
                }

                $filter_data_1 = array(
                    'filter_category_id' => $category['category_id'],
                    'filter_sub_category' => true
                );
                // Level 1
                $data['categories'][] = array(
                    'category_id' => $category['category_id'],
                    'name' => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProductsForAlgolia($filter_data_1) . ')' : ''),
                    'children' => $children_data,
                    'column' => $category['column'] ? $category['column'] : 1,
                    'href' => $this->url->link('search/category', "c=" . $category['name'] . '&path=' . $category['category_id'] . '&cpath=' . $category['category_id'], 'SSL', false, false, false)
                );
            }
        }

        return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
    }

}
