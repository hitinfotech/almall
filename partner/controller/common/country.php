<?php

class ControllerCommonCountry extends Controller {

    public function index() {

        $this->load->language('common/country');

        $data['text_country'] = $this->language->get('text_country');

        $data['action'] = $this->url->link('common/country/country', '', $this->request->server['HTTPS']);

        $data['code'] = $this->country->getCode();

        $this->load->model('localisation/country');

        $data['countries'] = array();

        $results = $this->model_localisation_country->getHeaderCountries();

        foreach ($results as $result) {
            if ($result['status']) {

                $result['iso_code_2'] = strtolower($result['iso_code_2']);

                $data['countries'][$result['iso_code_2']] = array(
                    'name' => $result['name'],
                    'code' => $result['iso_code_2'],
                    'flag' => HTTPS_IMAGE_S3 . $result['flag']
                );

                if (!isset($this->request->get['route'])) {
                    $data['countries'][$result['iso_code_2']]['redirect'] = $this->url->link('common/home', 'country=' . $result['iso_code_2'], 'SSL', '', $result['iso_code_2']);
                } else {
                    $url_data = $this->request->get;

                    unset($url_data['_route_']);

                    $route = $url_data['route'];

                    unset($url_data['route']);

                    $url = '';

                    if ($url_data) {
                        //die($route);
                        //$url = '&' . urldecode(http_build_query($url_data, '', '&'));
                        if (!in_array($route, array('malls/shop', 'malls/shops', 'malls/mall', 'malls/malls'))) {
                            $url = '&' . urldecode(http_build_query($url_data, '', '&'));
                        }
                    }

                    $data['countries'][$result['iso_code_2']]['redirect'] = $this->url->link($route, $url . '&country=' . $result['iso_code_2'], $this->request->server['HTTPS'], '', $result['iso_code_2']);
                }
            }
        }

        $data['redirect'] = '';

        return $this->load->view($this->config->get('config_template') . '/template/common/country.tpl', $data);
    }

    public function country() {

        if (isset($this->request->post['code'])) {

            $this->country->set($this->request->post['code']);

            if (in_array($this->request->post['code'], array('AE', 'ar'))) {
                $this->currency->set('AED');
            } else {
                $this->currency->set('SAR');
            }

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
        }

        if (isset($this->request->post['redirect'])) {
            $this->response->redirect($this->request->post['redirect']);
        } else {
            $this->response->redirect($this->url->link('common/home'));
        }
    }

}
