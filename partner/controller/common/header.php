<?php

class ControllerCommonHeader extends Controller {

    public function index($params = array()) {

        defined('DIR_COLLECTION') or define('DIR_COLLECTION', DIR_TEMPLATE . $this->config->get('config_template') . '/template/collection/');
        // Analytics
        $this->load->model('extension/extension');

        // Start of analytics code
        $data['analytics'] = array();

        if (isset($params['no_header']) && $params['no_header'] == true) {
            $data['no_header'] = true;
        } else {
            $data['no_header'] = false;
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        $this->document->addLink(HTTPS_IMAGE_S3 . $this->config->get('config_icon'), 'icon');

        $data['title'] = $this->document->getTitle();

        $data['base'] = $server;
        $data['home'] = $this->url->link('common/home');
        if (isset($this->request->get['route']) && ($this->request->get['route'] == 'look/looks' || $this->request->get['route'] == 'tips/tips' || $this->request->get['route'] == 'search/shopable' || $this->request->get['route'] == 'product/product')) {
            $data['description'] = $this->document->getDescription();
        } else {
            $data['description'] = $this->document->getDescription();
        }

        $data['keywords'] = $this->document->getKeywords();
        $data['country_code'] = $this->country->getCode();
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        /**
         * Sharing Data
         */
        $data['sShareUrl'] = ( isset($params['sShareUrl']) && !empty($params['sShareUrl']) ) ? $params['sShareUrl'] : "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $data['sShareImage'] = ( isset($params['sShareImage']) && !empty($params['sShareImage']) ) ? $params['sShareImage'] : SHARE_IMAGE;
        $data['FACEBOOK_APP_ID'] = $this->facebook->getFacebookAppId();
        $data['fbLoginUrl'] = $this->facebook->getRedirectLogin();

        /**
         * Adding Css & Js Files
         */
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . "css/bootstrap.min.css");
        if ($data['direction'] == "rtl") {
            $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . "css/bootstrap-rtl.min.css");
        }

        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . "css/font-awesome.min.css");
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . "css/mall.min.css");
        if ($data['direction'] == "ltr") {
            $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . "css/mall-en.min.css");
        }
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . "css/dev.min.css");
        if ($data['direction'] == "ltr") {
            $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . "css/dev-en.min.css");
        }

        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'css/slick.min.css');
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'css/slick-theme.min.css');

        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . "javascript/scripts.min.js");
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . "javascript/common.min.js");
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jcarousel/slick.min.js');
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . "javascript/bootstrap/js/bootstrap.min.js");

        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . "javascript/jquery.email-autocomplete.min.js");
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . "javascript/jquery/jquery-2.2.0.min.js");

        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts();
        $data['scripts'] = array_reverse($data['scripts']);

        $data['name'] = $this->config->get('config_name');


        $data['logo'] = HTTPS_IMAGE_S3 . $this->config->get('config_logo_' . $this->language->get('direction'));
        $data['upper_logo'] = HTTPS_IMAGE_S3 . $this->config->get('config_logo');

        $this->load->language('common/header');

        $shop_now_text = $this->language->get("shop_now_text");
        $sold_out_text = $this->language->get("sold_out_text");

        $data['text_home'] = $this->language->get('text_home');
        $data['text_tracking'] = $this->language->get("text_tracking");

        // Wishlist & loginUrl
        $data['tracking'] = $this->url->link('account/tracking', '', 'SSL');
        $data['login_url'] = "#";
        if ($this->partner->isLogged()) {

            $data['cust_email'] = $this->partner->getEmail();
            $data['cust_telephone'] = $this->partner->getTelephone();
            $data['cust_firstname'] = $this->partner->getFirstName();
            $data['cust_lastname'] = $this->partner->getLastName();
            $data['login_url'] = $this->url->link('account/account', '', 'SSL');
            $data['text_account'] = sprintf($this->language->get("text_welcome"), $this->partner->getFirstName() . ' ' . $this->partner->getLastName());
            $data['text_account_drop'] = $this->language->get("text_profile");
            $data['text_logout'] = $this->language->get("text_logout");
            $data['logout'] = $this->url->link('account/logout', '', 'SSL');
        } else {
            $data['text_account'] = $this->language->get('text_account');
            $data['text_loginto_seller'] = $this->language->get("text_loginto_seller");
            $data['text_register_seller'] = $this->language->get("text_register_seller");
            $data['text_howtosell'] = $this->language->get("text_howtosell");
            $data['text_login_seller'] = $this->language->get("text_login_seller");
        }


        $data['partner_login'] = $this->url->link('account/login', '', 'SSL');
        $data['partner_register'] = $this->url->link('account/register', '', 'SSL');
        $data['howtosell'] = str_replace('/partner', '', $this->url->link('common/howtosell', '', 'SSL'));

        $data['register_url'] = $this->url->link('account/register', '', 'SSL', false, false, false);
        $data['forgot_password'] = $this->url->link('account/forgotten', '', 'SSL');
        $data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
//        $data['text_logged'] = sprintf($this->language->get('text_logged'), str_replace('/partner', '', $this->url->link('account/account', '', 'SSL'), $this->partner->getFirstName(), str_replace('/partner', '', $this->url->link('account/logout', '', 'SSL'));

        if (isset($params['breadcrumbs'])) {
            $data['breadcrumbs'] = $params['breadcrumbs'];
        } else {
            $data['breadcrumbs'] = array();
        }

        $data['arabic_language'] = $this->language->get('arabic_language');
        $data['english_language'] = $this->language->get('english_language');
        if (isset($this->request->get['route']) && $this->request->get['route'] == 'checkout/cart') {
            $data['text_login_header'] = $this->language->get('text_login_header_cart');
        } else {
            $data['text_login_header'] = $this->language->get('text_login_header');
        }
        $data['text_login'] = $this->language->get('text_login');
        $data['text_register'] = $this->language->get('text_register');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_transaction'] = $this->language->get('text_transaction');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_logout'] = $this->language->get('text_logout');
        $data['text_checkout'] = $this->language->get('text_checkout');
        $data['text_category'] = $this->language->get('text_category');
        $data['text_more'] = $this->language->get('text_more');
        $data['text_all'] = $this->language->get('text_all');
        $data['button_cart'] = $this->language->get('button_cart');
        $data['text_edit_info'] = $this->language->get('text_edit_info');

        $data['text_rma_rma'] = $this->language->get('text_rma_rma');
        $data['text_rma_reason'] = $this->language->get('text_rma_reason');

        $data['config_otp_bullet'] = $this->config->get('config_otp_bullet');
        $data['config_otp_bullet_radius'] = $this->config->get('config_otp_bullet_radius');

        $data['ismobile'] = $this->is_mobile();

        if ($data['ismobile'] == true) {
            $banner_routes = array('malls/shops', 'malls/shop', 'malls/malls', 'malls/mall');

            if (isset($this->request->get['route']) && in_array($this->request->get['route'], $banner_routes)) {
                $params['banners'] = true;
            } else {
                $params['banners'] = false;
            }
        }

        if (isset($params['banners']) && $params['banners'] == false) {
            $data['banners'] = false;
        } else {
            $data['banners'] = true;
        }

        $popup_setting = array();
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "popup_settings` WHERE country_id = '" . (int) $this->country->getid() . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "'");
        foreach ($query->rows as $row) {
            if (!$row['serialized']) {
                $popup_setting[$row['key']] = $row['value'];
            } else {
                $popup_setting[$row['key']] = json_decode($row['value']);
            }
        }
        $data['popup_status'] = 0;
        if (!empty($popup_setting) && isset($popup_setting['popup_block0_status1']) && $popup_setting['popup_block0_status1'] == 1) {
            $data['popup_status'] = 1;
        }

        $data['home'] = $this->url->link('common/home', '', 'SSL', false, false, false);
        $data['logged'] = $this->partner->isLogged();
        $data['account'] = $this->url->link('account/account', '', 'SSL');
        $data['register'] = $this->url->link('account/register', '', 'SSL', false, false, false);
        $data['login'] = $this->url->link('account/login', '', 'SSL');
        $data['order'] = $this->url->link('account/order', '', 'SSL');
        $data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
        $data['download'] = $this->url->link('account/download', '', 'SSL');
        $data['logout'] = $this->url->link('account/logout', '', 'SSL');
        $data['shopping_cart'] = $this->url->link('checkout/cart', '', 'SSL', false, false, false);
        $data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL', false, false, false);
        $data['menusell'] = $this->url->link('customerpartner/sell', '', 'SSL');
        $data['rma_rma'] = $this->url->link('wk_mprma_manage', '', 'SSL');
        $data['rma_reason'] = $this->url->link('wk_mprma_reason', '', 'SSL');
        $data['edit_info'] = $this->url->link('account/editInfo','','SSL');
        $this->language->load('module/marketplace');
        $data['marketplace_status'] = $this->config->get('marketplace_status');
        $data['text_sell_header'] = $this->language->get('text_sell_header');
        $data['text_my_profile'] = $this->language->get('text_my_profile');
        $data['text_addproduct'] = $this->language->get('text_addproduct');
        $data['text_wkshipping'] = $this->language->get('text_wkshipping');
        $data['text_productlist'] = $this->language->get('text_productlist');
        $data['text_addproduct'] = $this->language->get('text_addproduct');
        $data['text_dashboard'] = $this->language->get('text_dashboard');
        $data['text_orderhistory'] = $this->language->get('text_orderhistory');
        $data['text_orderhistory_pending'] = $this->language->get('text_orderhistory_pending');
        $data['text_becomePartner'] = $this->language->get('text_becomePartner');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_transaction'] = $this->language->get('text_transaction');
        $data['text_seller_account'] = $this->language->get('text_seller_account');
        $data['marketplace_allowed_account_menu'] = $this->config->get('marketplace_allowed_account_menu');
        $data['text_seller_discounts'] = $this->language->get('text_seller_discounts');
        $data['text_seller_invoices'] = $this->language->get('text_seller_invoices');
        $data['mp_addproduct'] = $this->url->link('addproduct', '', 'SSL');
        $data['mp_productlist'] = $this->url->link('productlist', '', 'SSL');
        $data['mp_addproduct'] = $this->url->link('addproduct', '', 'SSL');
        $data['mp_dashboard'] = $this->url->link('dashboard', '', 'SSL');
        $data['mp_add_shipping_mod'] = $this->url->link('add_shipping_mod', '', 'SSL');
        $data['mp_orderhistory'] = $this->url->link('orderlist', '', 'SSL');
        $data['mp_orderhistory_pending'] = $this->url->link('orderlist', '&filter_status=Pending', 'SSL');
        $data['mp_download'] = $this->url->link('download', '', 'SSL');
        $data['mp_profile'] = $this->url->link('profile', '', 'SSL');
        $data['mp_want_partner'] = $this->url->link('become_partner', '', 'SSL');
        $data['mp_transaction'] = $this->url->link('transaction', '', 'SSL');
        $data['mp_seller_account'] = $this->url->link('customerpartner/profile', '&id=' . $this->partner->getId(), 'SSL');
        $data['chkIsPartner'] = true;
        $data['mp_seller_discounts'] = $this->url->link('discountlist', '', 'SSL');
        $data['mp_seller_invoices'] = $this->url->link('invoices', '', 'SSL');


        if ($this->config->get('wk_rma_status') && $this->config->get('wk_rma_status') == 1) {
            $data['wk_rma_status'] = 1;
        } else {
            $data['wk_rma_status'] = false;
        }

        $this->config->set('ts_status', 0);
        if ($this->config->get('ts_status')) {
            $data['tsSupportCenterLink'] = $this->url->link('ticketsystem/supportcenter', '', 'SSL');
            $this->load->language('ticketsystem/header');
            $data['text_ts_header'] = $this->language->get('text_ts_header');
        }
        $data['contact'] = $this->url->link('information/contact');
        $data['telephone'] = $this->config->get('config_telephone');

        $data['text_top_menu_more'] = $this->language->get('text_top_menu_more');
        $data['text_sayidaty_dot_net_site'] = $this->language->get('text_sayidaty_dot_net_site');
        $data['text_sayidaty_kitchen_site'] = $this->language->get('text_sayidaty_kitchen_site');
        $data['text_sayidaty_fashion_site'] = $this->language->get('text_sayidaty_fashion_site');
        $data['text_call_us'] = $this->language->get('text_call_us');
        $data['text_free_exchange'] = $this->language->get('text_free_exchange');
        $data['text_delivery_and_exchange'] = $this->language->get('text_delivery_and_exchange');
        $data['text_my_account'] = $this->language->get('text_my_account');
        $data['text_facebook_login'] = $this->language->get('text_facebook_login');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_or'] = $this->language->get('text_or');
        $data['text_password'] = $this->language->get('text_password');
        $data['text_remeber_me'] = $this->language->get('text_remeber_me');
        $data['text_forgot_password'] = $this->language->get('text_forgot_password');
        $data['text_new_user'] = $this->language->get('text_new_user');
        $data['text_close'] = $this->language->get('text_close');

        $data['text_search_website'] = $this->language->get('text_search_website');
        $data['text_shopping_bag'] = $this->language->get('text_shopping_bag');

        $data['text_menu_title_shop'] = $this->language->get('text_menu_title_shop');
        $data['text_online'] = $this->language->get('text_online');
        $data['text_we_choose_for_you'] = $this->language->get('text_we_choose_for_you');

        $data['text_women'] = $this->language->get('text_women');
        $data['text_clothes'] = $this->language->get('text_clothes');
        $data['text_dresses'] = $this->language->get('text_dresses');
        $data['text_shirts_tshirts'] = $this->language->get('text_shirts_tshirts');
        $data['text_jackets_coats'] = $this->language->get('text_jackets_coats');
        $data['text_abaya'] = $this->language->get('text_abaya');
        $data['text_swimming_suits'] = $this->language->get('text_swimming_suits');
        $data['text_shoes'] = $this->language->get('text_shoes');
        $data['text_boots'] = $this->language->get('text_boots');
        $data['text_sandals'] = $this->language->get('text_sandals');
        $data['text_flat_shoes'] = $this->language->get('text_flat_shoes');
        $data['text_sport_shoes'] = $this->language->get('text_sport_shoes');
        $data['text_high_heels'] = $this->language->get('text_high_heels');
        $data['text_accessories'] = $this->language->get('text_accessories');
        $data['text_nicklesses'] = $this->language->get('text_nicklesses');
        $data['text_diamonds'] = $this->language->get('text_diamonds');
        $data['text_watches'] = $this->language->get('text_watches');
        $data['text_sunglasses'] = $this->language->get('text_sunglasses');
        $data['text_rings'] = $this->language->get('text_rings');
        $data['text_bags'] = $this->language->get('text_bags');
        $data['text_big_bags'] = $this->language->get('text_big_bags');
        $data['text_small_bags'] = $this->language->get('text_small_bags');
        $data['text_clatch'] = $this->language->get('text_clatch');
        $data['text_wallets'] = $this->language->get('text_wallets');
        $data['text_travel_bags'] = $this->language->get('text_travel_bags');

        $data['text_men'] = $this->language->get('text_men');
        $data['text_shirts'] = $this->language->get('text_shirts');
        $data['text_suits'] = $this->language->get('text_suits');
        $data['text_sport_wears'] = $this->language->get('text_sport_wears');
        $data['text_trousers_pants'] = $this->language->get('text_trousers_pants');
        $data['text_slippers'] = $this->language->get('text_slippers');
        $data['text_casual_shoes'] = $this->language->get('text_casual_shoes');
        $data['text_ties'] = $this->language->get('text_ties');
        $data['text_belts'] = $this->language->get('text_belts');
        $data['text_hats'] = $this->language->get('text_hats');
        $data['text_casual_bags'] = $this->language->get('text_casual_bags');
        $data['text_designers'] = $this->language->get('text_designers');
        $data['text_full_looks'] = $this->language->get('text_full_looks');
        $data['text_full_shops'] = $this->language->get('text_full_shops');
        $data['text_products'] = $this->language->get('text_products');
        $data['text_news'] = $this->language->get('text_news');

        $data['text_new'] = $this->language->get('text_new');
        $data['text_favorit'] = $this->language->get('text_favorit');
        $data['text_tags'] = $this->language->get('text_tags');

        $status = true;

        if (isset($this->request->server['HTTP_USER_AGENT'])) {
            $robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

            foreach ($robots as $robot) {
                if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
                    $status = false;

                    break;
                }
            }
        }
        // checking if the seller has a contract
        $contract_url = HTTP_CATALOG.'image/seller_contracts/contract_'.$this->partner->getId().'.pdf';
        $ch = curl_init($contract_url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($code == 200){
           $data['text_seller_contract'] = $this->language->get('text_seller_contract');
           $data['seller_contract'] = $contract_url;
        }else{
          $data['seller_contract'] = false;
        }
        curl_close($ch);
        // end checking 

        $data['menu'] = $this->load->controller('common/menu', array('logo' => $data['logo']));
        $data['country'] = $this->load->controller('common/country');
        $data['language'] = $this->load->controller('common/language');
        //$data['popup'] = $this->load->controller('collection/popup');
        //$data['popup'] = $this->load->controller('collection/subscribepopup');
        //$data['currency'] = $this->load->controller('common/currency');
        $data['search'] = $this->load->controller('common/search');
        //$data['search'] = '';

        $this->load->model('localisation/country');
        $data['countries'] = $this->model_localisation_country->getAvailableCountries();

        // For page specific css
        if (isset($this->request->get['route'])) {
            if (isset($this->request->get['product_id'])) {
                $class = '-' . $this->request->get['product_id'];
            } elseif (isset($this->request->get['path'])) {
                $class = '-' . $this->request->get['path'];
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $class = '-' . $this->request->get['manufacturer_id'];
            } else {
                $class = '';
            }
            $data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
        } else {
            $data['class'] = 'common-home';
        }

        if (isset($this->session->data['wishlist'])) {
            $wishlist = $this->session->data['wishlist'];
        } else {
            $wishlist = array();
        }

        $data['js_vars'] = array(
            'country' => $data['country_code'],
            'country_id' => $this->country->getid(),
            'text_discount' => $this->language->get('text_discount'),
            'currency' => $this->currency->getCode(),
            'lang' => $data['lang'],
            'wishlist' => $wishlist,
            'logged' => $data['logged'],
            'newslettersupscripe_url' => $this->url->link('account/newsletter/subscribe', '', 'SSL', false, false, false),
            'newslettersupscripe_url2' => $this->url->link('account/newsletter/subscribe_step2', '', 'SSL', false, false, false),
            'login_url' => $data['login_url'],
            'signin_url' => $this->url->link('account/login', '', 'SSL'),
            'tracking' => $data['tracking'],
            'image_url' => HTTPS_IMAGE_S3,
            'algolia' => array(
                'APPID' => ALGOLIA_APPID,
                'SEARCH_API_KEY' => ALGOLIA_SEARCH_API_KEY
            ),
            'FACEBOOK_APP_ID' => FACEBOOK_APP_ID,
            'base_url' => array(
                'component' => ''
            ),
            'shop_now_text' => $shop_now_text,
            'sold_out_text' => $sold_out_text
        );

        if (!isset($this->request->get['route'])) {
            $data['redirect'] = str_replace('/partner', '', $this->url->link('common/home', '', 'SSL'));
        } else {

            $url_data = $this->request->get;

            unset($url_data['_route_']);

            $route = $url_data['route'];

            unset($url_data['route']);

            $url = '';

            if ($url_data) {
                $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            }

            $data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
        }

        $this->session->data['logout_redirect'] = $data['redirect'];

        return $this->load->view($this->config->get('config_template') . '/template/common/header.tpl', $data);
    }

}
