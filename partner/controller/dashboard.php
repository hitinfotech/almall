<?php

class ControllerDashboard extends Controller {

    private $error = array();
    private $data = array();

    public function index() {

        if (!$this->partner->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('dashboard', '', 'SSL');
            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        /*if ($this->config->get("config_language_id") == 2) {
            $this->config->set("config_language_id",1);
            if ($this->language->get('code') == "ar") {
                $language_code = "en";
            } else {
                $language_code = "ar";
            }
            $url_data = $this->request->get;
            unset($url_data['_route_']);
            $route = $url_data['route'];
            unset($url_data['route']);
            $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            $this->response->redirect($this->url->link($route, $url, $this->request->server['HTTPS'], $language_code));
        }*/

        $this->load->model('account/customerpartner');

        $this->data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

        if (!$this->data['chkIsPartner']){
            $this->response->redirect($this->url->link('account/completeRegister'));
          }

        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'css/sell.min.css');

        $this->language->load('account/customerpartner/dashboard');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_sale'] = $this->language->get('text_sale');
        $this->data['text_map'] = $this->language->get('text_map');
        $this->data['text_activity'] = $this->language->get('text_activity');
        $this->data['text_recent'] = $this->language->get('text_recent');

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('dashboard'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_dashboard'),
            'href' => $this->url->link('dashboard', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $this->data['banners'] = false;

        $this->data['order'] = $this->load->controller('dashboards/order');
        $this->data['returns'] = $this->load->controller('dashboards/returns');
        $this->data['sale'] = $this->load->controller('dashboards/sale');
        $this->data['customer'] = $this->load->controller('dashboards/customer');
        // $this->data['seller_sale'] = '';
        $this->data['map'] = $this->load->controller('dashboards/map');
        $this->data['chart'] = $this->load->controller('dashboards/chart');
        // $this->data['activity'] = $this->load->controller('account/customerpartner/dashboards/activity');
        $this->data['recent'] = $this->load->controller('dashboards/recent');

        $this->data['isMember'] = true;
        if ($this->config->get('wk_seller_group_status')) {
            $this->data['wk_seller_group_status'] = true;
            $this->load->model('account/customer_group');
            $isMember = $this->model_account_customer_group->getSellerMembershipGroup($this->customer->getId());
            if ($isMember) {
                $allowedAccountMenu = $this->model_account_customer_group->getaccountMenu($isMember['gid']);
                if ($allowedAccountMenu['value']) {
                    $accountMenu = explode(',', $allowedAccountMenu['value']);
                    if ($accountMenu && !in_array('dashboard:dashboard', $accountMenu)) {
                        $this->data['isMember'] = false;
                    }
                }
            } else {
                $this->data['isMember'] = false;
            }
        } else {
            if (!in_array('dashboard', $this->config->get('marketplace_allowed_account_menu'))) {
                $this->response->redirect($this->url->link('account/login', '', 'SSL'));
            }
        }

        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['column_right'] = $this->load->controller('common/column_right');
        $this->data['content_top'] = $this->load->controller('common/content_top');
        $this->data['content_bottom'] = $this->load->controller('common/content_bottom');
        $this->data['footer'] = $this->load->controller('common/footer');
        $this->data['header'] = $this->load->controller('common/header', $this->data);

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/customerpartner/dashboard.tpl', $this->data));
    }

    public function changereview() {

        $this->language->load('account/customerpartner/dashboard');

        $this->load->model('account/customerpartner');

        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST' AND $this->customer->getID()) {

            if ($this->model_account_customerpartner->chkIsPartner() AND isset($this->request->post['review'])) {
                $latestcomment = $this->model_account_customerpartner->UpdateReview($this->request->post['review']);
                $json['success'] = $this->language->get('text_change_review');
            } else {
                $json['error'] = $this->language->get('text_error');
            }
        }

        $this->response->setOutput(json_encode($json));
    }

}
