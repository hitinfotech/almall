<?php

class ControllerAccountLogout extends Controller {

    public function index() {
        if ($this->partner->isLogged()) {
            $this->event->trigger('pre.Partner.logout');

            $this->partner->logout();

            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        if(isset($this->session->data['logout_redirect'])){
            $this->response->redirect($this->session->data['logout_redirect']);
        } else {
        $this->response->redirect($this->url->link('common/home'));
        }
        $this->load->language('account/logout');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('dashboard')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_logout'),
            'href' => $this->url->link('account/logout', '', 'SSL')
        );

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_message'] = $this->language->get('text_message');

        $data['button_continue'] = $this->language->get('button_continue');

        $data['continue'] = $this->url->link('common/home');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header', array('breadcrumbs'=>$data['breadcrumbs']));

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));

    }

}
