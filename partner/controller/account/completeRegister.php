<?php

class ControllerAccountCompleteRegister extends Controller {

    private $error = array();
    private $data = array();

    public function index() {

      //$this->response->redirect($this->url->link('account/login', '', 'SSL'));

        $this->load->model('account/customerpartner');
        $this->load->model('localisation/language');
        $this->load->model('localisation/country');
        $this->load->model('localisation/currency');
        $this->load->model('localisation/zone');
        $this->load->model('account/partner');
        $this->load->model('tool/image');

        $seller_data = array();
        if ($this->model_account_customerpartner->checkIsMemberByEmail($this->partner->getEmail()) && $this->partner->getEmail() != ''){
          $seller_data =  $this->model_account_customerpartner->getPartnerDataByEmail($this->partner->getEmail());
        }else{
          $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }
        $this->data['placeholder'] = $this->model_tool_image->resize('placeholder.png', 100, 100, false);
        if (!empty($seller_data)){
          foreach(array('passport' => $seller_data['passport'], 'license' => $seller_data['license'],'hd_logo' => $seller_data['companybanner']) as $key => $value) {

              if (is_file(DIR_IMAGE . $value)) {
                  foreach ($this->config_image->get('partner_' . $key) as $k => $v) {
                      $this->data[$key . '_placeholder_'.$k] = $this->model_tool_image->resize($value, $v['width'], $v['hieght'], false);
                  }
              } else {
                  $this->data[$key . '_placeholder'] = $this->data['placeholder'];
              }
          }
        }

        if (!empty($seller_data)) {
            $this->data['execluded_countries'] = array_column($this->model_account_customerpartner->getExecludedCountries($seller_data['customer_id']),'country_id');
        } else {
            $this->data['execluded_countries'] = array();
        }

        $this->data['seller_data'] = $seller_data;

        $results = $this->model_localisation_currency->getCurrencies();

        $this->data['currencies'] = $results;

        $this->language->load('customerpartner/sell');


        $this->document->setTitle($this->language->get('heading_title'));
        $this->data['languages'] = $this->model_localisation_language->getLanguages();

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );

        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'css/sell.css');

        $buttontitle = $this->config->get('marketplace_sellbuttontitle');
        $sellerHeader = $this->config->get('marketplace_sellheader');
        $this->data['action'] = $this->url->link('account/success', '', 'SSL');
        $this->data['sell_title'] = $this->language->get('complete_registration');
        $this->data['sell_header'] = $sellerHeader[$this->config->get('config_language_id')];
        $this->data['showpartners'] = $this->config->get('marketplace_showpartners');
        $this->data['showproducts'] = $this->config->get('marketplace_showproducts');

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_long_time_seller'] = $this->language->get('text_long_time_seller');
        $this->data['text_latest_product'] = $this->language->get('text_latest_product');

        $this->data['text_persinal_info'] = $this->language->get('text_persinal_info');
        $this->data['text_firstname'] = $this->language->get('text_firstname');
        $this->data['text_lastname'] = $this->language->get('text_lastname');
        $this->data['text_email'] = $this->language->get('text_email');
        $this->data['text_fbs_email'] = $this->language->get('text_fbs_email');
        $this->data['text_phone'] = $this->language->get('text_phone');
        $this->data['text_password'] = $this->language->get('text_password');
        $this->data['text_confirm_password'] = $this->language->get('text_confirm_password');

        $this->data['text_business_info'] = $this->language->get('text_business_info');
        $this->data['text_country'] = $this->language->get('text_country');
        $this->data['text_city'] = $this->language->get('text_city');
        $this->data['text_num_of_branches'] = $this->language->get('text_num_of_branches');
        $this->data['text_facebook'] = $this->language->get('text_facebook');
        $this->data['text_website'] = $this->language->get('text_website');
        $this->data['text_instagram'] = $this->language->get('text_instagram');
        $this->data['text_twitter'] = $this->language->get('text_twitter');
        $this->data['text_selling_countries'] = $this->language->get('text_selling_countries');
        $this->data['text_next'] = $this->language->get('text_next');
        $this->data['text_warehouse_address'] = $this->language->get('text_warehouse_address');
        $this->data['text_opening_hours'] = $this->language->get('text_opening_hours');
        $this->data['text_legal_info'] = $this->language->get('text_legal_info');
        $this->data['text_instagram'] = $this->language->get('text_instagram');
        $this->data['text_twitter'] = $this->language->get('text_twitter');
        $this->data['text_brand_description'] = $this->language->get('text_brand_description');
        $this->data['text_screen_name'] = $this->language->get('text_screen_name');
        $this->data['text_passport_number'] = $this->language->get('text_passport_number');
        $this->data['text_excluded_countries'] = $this->language->get('text_excluded_countries');

        $this->data['text_legal_info'] = $this->language->get('text_legal_info');
        $this->data['text_legal_name'] = $this->language->get('text_legal_name');
        $this->data['text_legal_name_tooltip'] = $this->language->get('text_legal_name_tooltip');
        $this->data['text_company_name'] = $this->language->get('text_company_name');
        $this->data['text_trade_license_number'] = $this->language->get('text_trade_license_number');
        $this->data['text_address_license_number'] = $this->language->get('text_address_license_number');
        $this->data['text_both_iban_swift'] = $this->language->get("text_both_iban_swift");


        $this->data['text_admin_name'] = $this->language->get('text_admin_name');
        $this->data['text_admin_email'] = $this->language->get('text_admin_email');
        $this->data['text_admin_phone'] = $this->language->get('text_admin_phone');
        $this->data['text_admin_contact_tooltip'] = $this->language->get("text_admin_contact_tooltip");
        $this->data['text_nationality'] = $this->language->get('text_nationality');
        $this->data['text_email_address'] = $this->language->get('text_email_address');
        $this->data['text_office_num'] = $this->language->get('text_office_num');
        $this->data['text_moblie_num'] = $this->language->get('text_moblie_num');
        $this->data['text_passport_copy'] = $this->language->get('text_passport_copy');
        $this->data['text_license'] = $this->language->get('text_license');
        $this->data['text_logo'] = $this->language->get('text_logo');


        $this->data['text_financail_info'] = $this->language->get('text_financail_info');
        $this->data['text_fin_contact_name'] = $this->language->get('text_fin_contact_name');
        $this->data['text_fin_email'] = $this->language->get('text_fin_email');
        $this->data['text_fin_num'] = $this->language->get('text_fin_num');
        $this->data['text_beneficiary_name'] = $this->language->get('text_beneficiary_name');
        $this->data['text_bank_name'] = $this->language->get('text_bank_name');
        $this->data['text_bank_account'] = $this->language->get('text_bank_account');
        $this->data['text_iban_code'] = $this->language->get('text_iban_code');
        $this->data['text_swift_code'] = $this->language->get('text_swift_code');
        $this->data['text_currency'] = $this->language->get('text_currency');

        $this->data['text_tax'] = $this->language->get('text_tax');
        $this->data['text_from'] = $this->language->get('text_from');
        $this->data['text_seller'] = $this->language->get('text_seller');
        $this->data['text_total_products'] = $this->language->get('text_total_products');

        $this->data['button_cart'] = $this->language->get('button_cart');
        $this->data['button_wishlist'] = $this->language->get('button_wishlist');
        $this->data['button_compare'] = $this->language->get('button_compare');

        $partner_data = $this->model_account_partner->getPartner($this->partner->getId());
        $this->data['partner_data'] = $partner_data;

        $this->data['available_countries'] = $this->model_localisation_country->getAvailableCountries();
        $this->data['countries'] = $this->model_localisation_country->getCountries();

        $cities = array();
        foreach($this->data['countries'] as $country){
            $cities[$country['country_id']] =  $this->model_localisation_zone->getZonesByCountryId($country['country_id']);
        }
        $this->data['cities'] = $cities;

        $this->data['current_country'] = $this->country->getid();

        $this->data['is_mobile'] = ($this->is_mobile()) ? 1 : 0;

        $this->data['tabs'] = array();

        $marketplace_tab = $this->config->get('marketplace_tab');

        if (isset($marketplace_tab['heading']) AND $marketplace_tab['heading']) {
            ksort($marketplace_tab['heading']);
            ksort($marketplace_tab['description']);
            foreach ($marketplace_tab['heading'] as $key => $value) {
                $text = $marketplace_tab['description'][$key][$this->config->get('config_language_id')];
                $text = trim(html_entity_decode($text));
                $this->data['tabs'][] = array(
                    'id' => $key,
                    'hrefValue' => $value[$this->config->get('config_language_id')],
                    'description' => $text,
                );
            }
        }

        $this->load->model('tool/image');
        $this->load->model('customerpartner/master');

        $partners = $this->model_customerpartner_master->getOldPartner();

        $this->data['partners'] = array();

        foreach ($partners as $key => $result) {

            if ($result['avatar']) {
                $image = $this->model_tool_image->resize($result['avatar'], 100, 100);
            } else if ($result['avatar'] == 'removed') {
                $image = '';
            } else if ($this->config->get('marketplace_default_image_name')) {
                $image = $this->model_tool_image->resize($this->config->get('marketplace_default_image_name'), 100, 100);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', 100, 100);
            }

            $this->data['partners'][] = array(
                'customer_id' => $result['customer_id'],
                'name' => $result['firstname'] . ' ' . $result['lastname'],
                'companyname' => $result['companyname'],
                'backgroundcolor' => $result['backgroundcolor'],
                'country' => $result['country'],
                'sellerHref' => $this->url->link('customerpartner/profile', 'id=' . $result['customer_id']),
                'thumb' => $image,
                'total_products' => $this->model_customerpartner_master->getPartnerCollectionCount($result['customer_id']),
            );
        }

        //products
        $latest = $this->model_customerpartner_master->getLatest();
        $this->data['latest'] = array();

        foreach ($latest as $key => $result) {

            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
            }

            if ($result['avatar']) {
                $avatar = $this->model_tool_image->resize($result['avatar'], 70, 70);
            } else if ($result['avatar'] == 'removed') {
                $avatar = '';
            } else if ($this->config->get('marketplace_default_image_name')) {
                $avatar = $this->model_tool_image->resize($this->config->get('marketplace_default_image_name'), 50, 50);
            } else {
                $avatar = $this->model_tool_image->resize('placeholder.png', 50, 50);
            }

            if (($this->config->get('config_customer_price') && $this->partner->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $price = false;
            }

            if ((float) $result['special']) {
                $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $special = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float) $result['special'] ? $result['special'] : $result['price']);
            } else {
                $tax = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = (int) $result['rating'];
            } else {
                $rating = false;
            }

            $this->data['latest'][] = array(
                'product_id' => $result['product_id'],
                'seller_name' => $result['seller_name'],
                'country' => $result['country'],
                'avatar' => $avatar,
                'backgroundcolor' => $result['backgroundcolor'],
                'sellerHref' => $this->url->link('customerpartner/profile', 'id=' . $result['customer_id']),
                'thumb' => $image,
                'name' => $result['name'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                'price' => $price,
                'special' => $special,
                'tax' => $tax,
                'rating' => $result['rating'],
                'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'])
            );
        }

        $this->data['footer'] = $this->load->controller('common/footer');
        $this->data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/customerpartner/CompleteRegister.tpl', $this->data));

    }

    public function wkmpregistation() {

        $this->load->model('customerpartner/master');

        $json = array();

        if (isset($this->request->post['shop'])) {
            $data = urldecode(html_entity_decode($this->request->post['shop'], ENT_QUOTES, 'UTF-8'));
            if ($this->model_customerpartner_master->getShopData($data)) {
                $json['success'] = true;
            } else {
                $json['error'] = true;
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    protected function validate() {
        if (!$this->partner->isLogged()) {
            $this->error['error_login'] = $this->language->get("error_login");
        }
        return !$this->error;
    }

    public function save_data(){

      $this->load->model('customerpartner/master');

      $section_num = $partner_id = 0;
      $email = '';

      if(isset($this->request->post['section_num'])){
        $section_num = $this->request->post['section_num'];
      }

      if(isset($this->request->post['email'])){
        $email = $this->request->post['email'];
      }
      if ($email !=''){
          $partner_id = $this->model_customerpartner_master->getPartnerByEmail($email);
      }

      switch ($section_num){
        case 1 :
            if(isset($this->request->post['firstname'])){
              $data['firstname'] = $this->request->post['firstname'];
            }

            if(isset($this->request->post['lastname'])){
              $data['lastname'] = $this->request->post['lastname'];
            }

            if(isset($this->request->post['email'])){
              $data['email'] = $this->request->post['email'];
            }

            if(isset($this->request->post['fbs_email'])){
              $data['fbs_email'] = $this->request->post['fbs_email'];
            }else{
              $data['fbs_email'] = '';
            }

            if(isset($this->request->post['telephone'])){
              $data['telephone'] = $this->request->post['telephone'];
            }

            if(isset($this->request->post['telcode'])){
              $data['telecode'] = $this->request->post['telcode'];
            }

            if(isset($this->request->post['user_password'])){
              $data['password'] = $this->request->post['user_password'];
            }

            if(isset($data['email']) && $data['email'] != ''){
              $email_not_in_use = $this->validate_user_email($data['email']);
            }

            if(!empty($data) && $email_not_in_use){
              $result = $this->model_customerpartner_master->add_customer($section_num,$data,$partner_id);
            }

        break;

        case 2 :
            if(isset($this->request->post['country'])){
              $data['country_id'] = $this->request->post['country'];
            }

            if(isset($this->request->post['city'])){
              $data['zone_id'] = $this->request->post['city'];
            }

            if(isset($this->request->post['admin_name'])){
              $data['admin_name'] = $this->request->post['admin_name'];
            }

            if(isset($this->request->post['admin_email'])){
              $data['admin_email'] = $this->request->post['admin_email'];
            }

            if(isset($this->request->post['admin_phone'])){
              $data['admin_phone'] = $this->request->post['admin_phone'];
            }

            if(isset($this->request->post['admin_num_telcode'])){
              $data['admin_telecode'] = $this->request->post['admin_num_telcode'];
            }

            if(isset($this->request->post['num_of_branches'])){
              $data['num_of_branches'] = $this->request->post['num_of_branches'];
            }

            if(isset($this->request->post['website'])){
              $data['website'] = $this->request->post['website'];
            }

            if(isset($this->request->post['facebook'])){
              $data['facebook'] = $this->request->post['facebook'];
            }

            if(isset($this->request->post['twitter'])){
              $data['twitter'] = $this->request->post['twitter'];
            }

            if(isset($this->request->post['instagram'])){
              $data['instagram'] = $this->request->post['instagram'];
            }

            if(isset($this->request->post['warehouse_address'])){
              $data['warehouse_address'] = $this->request->post['warehouse_address'];
            }

            if(isset($this->request->post['opening_hours'])){
              $data['opening_hours'] = $this->request->post['opening_hours'];
            }

            if(isset($this->request->post['screen_name'])){
              $data['screen_name'] = $this->request->post['screen_name'];
              $data['clienturl'] = preg_replace("/[^A-Za-z0-9-]/", '', $data['screen_name']);
            }



            if(isset($this->request->post['product_description'])){
              $data['product_description'] = $this->request->post['product_description'];
            }

            if(isset($this->request->post['logo_hidden'])){
              $data['logo'] = $this->request->post['logo_hidden'];
            }

            if(isset($this->request->post['available_countries'])){
              $data['available_countries'] = $this->request->post['available_countries'];
            }

            if(isset($this->request->post['execluded_countries'])){
              if(!empty($this->request->post['execluded_countries']))
                foreach($this->request->post['execluded_countries'] as $key => $value){
                  if($value === 'true')
                    $data['execluded_countries'][] = $key;
                }
            }else{
                $data['execluded_countries'] = array();
            }

            // this section to handle the available countries.
            $data['is_ksa'] = 0;
            $data['is_uae'] = 0;


            if(isset($this->request->post['available_countries'])){
                switch($this->request->post['available_countries']){
                  case 0:
                    $data['is_ksa'] = 1;
                    $data['is_uae'] = 1;
                  break;

                  case 184 :
                    $data['is_ksa'] = 1;
                  break ;

                  case 221 :
                    $data['is_uae'] = 1;
                  break;
                }
            }

            if(!empty($data)){
              $result = $this->model_customerpartner_master->add_customer($section_num,$data,$partner_id);
            }

        break;

        case 3 :

            if(isset($this->request->post["legal_info"]['legal_name'])){
              $data['legal_name'] = $this->request->post["legal_info"]['legal_name'];
            }

            if(isset($this->request->post["legal_info"]['trade_license_number'])){
              $data['trade_license_number'] = $this->request->post["legal_info"]['trade_license_number'];
            }

            if(isset($this->request->post["legal_info"]['address_license_number'])){
              $data['address_license_number'] = $this->request->post["legal_info"]['address_license_number'];
            }

            if(isset($this->request->post['company_name'])){
              $data['company_name'] = $this->request->post['company_name'];
            }

            if(isset($this->request->post["legal_info"]['admin_contact'])){
              $data['admin_contact'] = $this->request->post["legal_info"]['admin_contact'];
            }

            if(isset($this->request->post['nationality'])){
              $data['nationality'] = $this->request->post['nationality'];
            }

            if(isset($this->request->post['passport_num'])){
              $data['passport_num'] = $this->request->post['passport_num'];
            }

            if(isset($this->request->post["legal_info"]['email_address'])){
              $data['email_address'] = $this->request->post["legal_info"]['email_address'];
            }

            if(isset($this->request->post["legal_info"]['office_num'])){
              $data['office_num'] = $this->request->post["legal_info"]['office_num'];
            }

            if(isset($this->request->post['office_num_telcode'])){
              $data['office_num_telcode'] = $this->request->post['office_num_telcode'];
            }

            if(isset($this->request->post["legal_info"]['moblie_num'])){
              $data['moblie_num'] = $this->request->post["legal_info"]['moblie_num'];
            }

            if(isset($this->request->post['moblie_num_telcode'])){
              $data['moblie_num_telcode'] = $this->request->post['moblie_num_telcode'];
            }

            if(isset($this->request->post['passport_copy_hidden'])){
              $data['passport_copy'] = $this->request->post['passport_copy_hidden'];
            }

            if(isset($this->request->post['license_hidden'])){
              $data['license'] = $this->request->post['license_hidden'];
            }

            if(!empty($data)){
              $result = $this->model_customerpartner_master->add_customer($section_num,$data,$partner_id);
            }

        break;

        case 4 :

            if(isset($this->request->post["financail_info"]['fin_contact_name'])){
              $data['fin_contact_name'] = $this->request->post["financail_info"]['fin_contact_name'];
            }

            if(isset($this->request->post["financail_info"]['fin_email'])){
              $data['fin_email'] = $this->request->post["financail_info"]['fin_email'];
            }

            if(isset($this->request->post["financail_info"]['fin_num'])){
              $data['fin_num'] = $this->request->post["financail_info"]['fin_num'];
            }

            if(isset($this->request->post['fin_telephone_telecode'])){
              $data['fin_telephone_telecode'] = $this->request->post['fin_telephone_telecode'];
            }

            if(isset($this->request->post["financail_info"]['beneficiary_namee'])){
              $data['beneficiary_name'] = $this->request->post["financail_info"]['beneficiary_namee'];
            }

            if(isset($this->request->post["financail_info"]['bank_name'])){
              $data['bank_name'] = $this->request->post["financail_info"]['bank_name'];
            }

            if(isset($this->request->post["financail_info"]['bank_accountt'])){
              $data['bank_account'] = $this->request->post["financail_info"]['bank_accountt'];
            }

            if(isset($this->request->post["financail_info"]['iban_code'])){
              $data['iban_code'] = $this->request->post["financail_info"]['iban_code'];
            }

            if(isset($this->request->post["financail_info"]['swift_code'])){
              $data['swift_code'] = $this->request->post["financail_info"]['swift_code'];
            }

            if(isset($this->request->post["financail_info"]['currency'])){
              $data['currency'] = $this->request->post["financail_info"]['currency'];
            }

            if(isset($this->request->post['bank_number'])){
              $data['bank_number'] = $this->request->post['bank_number'];
            }

            if(isset($this->request->post['is_both'])){
              $data['is_both'] = ($this->request->post['is_both']=='true'?1:0);
            }

            if(!empty($data)){
              $result = $this->model_customerpartner_master->add_customer($section_num,$data,$partner_id);
            }

        break;
      }

      if (! empty($this->request->files)){

        $result = $this->model_customerpartner_master->upload_image($this->request->files);
        if (isset($result['success'])){
          echo json_encode($result);
          return;
        }else{
          echo json_encode(array('error' => "Error In uploading the Image"));
          return;
        }

      }

      echo json_encode($result);
    }


    public function validate_user_email($user_mail = ''){

      $this->load->model('customerpartner/master');
      $email = '';
      $partner_id = 0;
      $login_link = $this->url->link('account/login', '', 'SSL');
      if(isset($this->request->get['email']) ) {
        $email = $this->request->get['email'];
      }elseif($user_mail != ''){
        $email = $user_mail;
      }
      if ($email !='' && $email != $this->partner->getEmail()){
          $partner_id = $this->model_customerpartner_master->getPartnerByEmail($email);
      }

      if($partner_id){
        echo json_encode(array("Error"=>"Email In use! If you Registered Before Please Continue Registration from this <a class= 'gray-text' href='".$login_link."'>Link</a>"));
      }else{
        return true;
      }
      return;

    }
}
