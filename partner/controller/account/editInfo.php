<?php

class ControllerAccountEditInfo extends Controller {

    private $error = array();

    public function index() {
        if (!$this->partner->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('dashboard', '', 'SSL');

            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->load->language('account/edit_info');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->load->model('account/partner');

            $this->model_account_partner->editPassword($this->partner->getEmail(), $this->request->post['password']);
            $this->model_account_partner->editEmails($this->partner->getId(), $this->request->post['email'], $this->request->post['fbs_email']);

            $this->session->data['success'] = $this->language->get('text_success');

            $activity_data = array(
                'customer_id' => $this->partner->getId(),
                'name' => $this->partner->getFirstName() . ' ' . $this->partner->getLastName()
            );

            $this->model_account_partner->addActivity('password', $activity_data);

            $this->response->redirect($this->url->link('account/editInfo', '', 'SSL'));
        }elseif(($this->request->server['REQUEST_METHOD'] == 'POST') && (!isset($this->request->post['password']) || $this->request->post['password']=='')){

          $this->error = array();
          $this->load->model('account/partner');

          $this->model_account_partner->editEmails($this->partner->getId(), $this->request->post['email'], $this->request->post['fbs_email']);

          $this->session->data['success'] = $this->language->get('text_success');


        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/editInfo', '', 'SSL')
        );

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_email'] = $this->language->get('text_email');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_fbs_email'] = $this->language->get('entry_fbs_email');


        $data['text_current_password'] = $this->language->get('text_current_password');
        $data['text_password'] = $this->language->get('text_password');

        $data['entry_current_password'] = $this->language->get('entry_current_password');
        $data['entry_password'] = $this->language->get('entry_password');
        $data['entry_confirm'] = $this->language->get('entry_confirm');

        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_back'] = $this->language->get('button_back');
        $data['button_save'] = $this->language->get('button_save');

        if (isset($this->error['current_password'])) {
            $data['error_current_password'] = $this->error['current_password'];
        } else {
            $data['error_current_password'] = '';
        }

        if (isset($this->error['password'])) {
            $data['error_password'] = $this->error['password'];
        } else {
            $data['error_password'] = '';
        }

        if (isset($this->error['confirm'])) {
            $data['error_confirm'] = $this->error['confirm'];
        } else {
            $data['error_confirm'] = '';
        }

        $data['action'] = $this->url->link('account/editInfo', '', 'SSL');

        if (isset($this->request->post['current_password'])) {
            $data['current_password'] = $this->request->post['current_password'];
        } else {
            $data['current_password'] = '';
        }

        if (isset($this->request->post['password'])) {
            $data['password'] = $this->request->post['password'];
        } else {
            $data['password'] = '';
        }

        if (isset($this->request->post['confirm'])) {
            $data['confirm'] = $this->request->post['confirm'];
        } else {
            $data['confirm'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['current_email'] = $this->request->post['email'];
        } else {
            $data['current_email'] = $this->partner->getEmail();
        }

        if (isset($this->request->post['fbs_email'])) {
            $data['fbs_email'] = $this->request->post['fbs_email'];
        } else {
            $data['fbs_email'] = $this->partner->getFBSEmail();
        }

        $data['back'] = $this->url->link('account/account', '', 'SSL');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header', $data);

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/edit_info.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/edit_info.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/account/edit_info.tpl', $data));
        }
    }

    protected function validate() {

        if (!$this->currentPasswordChecker($this->partner->getId(), $this->request->post['current_password'])) {
            $this->error['current_password'] = $this->language->get('error_current_password');
        }

        if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
            $this->error['password'] = $this->language->get('error_password');
        }

        if ($this->request->post['confirm'] != $this->request->post['password']) {
            $this->error['confirm'] = $this->language->get('error_confirm');
        }

        return !$this->error;
    }

    protected function currentPasswordChecker($customer_id, $current_password) {
        $this->load->model('account/partner');
        return $this->model_account_partner->currentPasswordChecker($customer_id, $current_password);
    }

}
