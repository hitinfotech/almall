<?php

class ControllerAccountSuccess extends Controller {

    public function index() {
        $this->load->language('account/success');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('dashboard', '', 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_success'),
            'href' => $this->url->link('account/success')
        );


        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_hello'] = sprintf($this->language->get("text_hello"),  $this->partner->getFirstName());
        $data['first_par'] = $this->language->get('first_par');
        $data['text_welcome'] = $this->language->get("text_welcome");
        $data['text_text'] = sprintf($this->language->get('text_text'),$this->partner->getEmail());

        $data['button_continue'] = $this->language->get('button_continue');

        $data['continue'] = $this->url->link('dashboard', '', 'SSL');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/success.tpl', $data));
    }

}
