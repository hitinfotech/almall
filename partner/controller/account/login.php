<?php

class ControllerAccountLogin extends Controller {

    private $error = array();

    public function index() {

        $this->load->model('account/partner');

        // Login override for admin users
        if (!empty($this->request->get['token'])) {
            $this->partner->logout();

            unset($this->session->data['order_id']);
            unset($this->session->data['payment_address']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['shipping_address']);
            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['comment']);
            unset($this->session->data['coupon']);
            unset($this->session->data['reward']);
            unset($this->session->data['voucher']);
            unset($this->session->data['vouchers']);

            $partner_info = $this->model_account_partner->getPartnerByToken($this->request->get['token']);

            if ($partner_info && $this->partner->login($partner_info['email'], '', true)) {
                if (isset($this->request->post['redirect']) && $this->request->post['redirect']) {
                    $this->response->redirect($this->request->post['redirect']);
                } else {
                    $this->response->redirect($this->url->link('dashboard', '', 'SSL'));
                }
            }
        }

        if ($this->partner->isLogged()) {
            $this->response->redirect($this->url->link('dashboard', '', 'SSL'));
        }

        $this->load->language('account/login');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['ajaxlogin'])) {
            if (!$this->validate()) {
                if (!isset($this->error['warning'])) {
                    $this->error['warning'] = $this->language->get('error_login');
                }
                die(json_encode($this->error));
            } else {
                $data['msg'] = 1;

                $data['chkIsPartner'] = 1;
                $data['redirect'] = $this->url->link('dashboard', '', 'SSL');

                die(json_encode($data));
            }
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            // Trigger partner pre login event
            $this->event->trigger('pre.partner.login');

            // Unset guest
            unset($this->session->data['guest']);

            // Trigger partner post login event
            $this->event->trigger('post.partner.login');

            // Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)

            if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
                $this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
            } else {
                $this->response->redirect($this->url->link('dashboard', '', 'SSL'));
            }
        }

        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/moment.min.js');
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('dashboard')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_login'),
            'href' => $this->url->link('account/login', '', 'SSL')
        );

        $data['heading_title'] = $this->language->get('heading_title');
        $data['heading_title2'] = $this->language->get('heading_title2');

        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_password'] = $this->language->get('entry_password');
        $data['text_forgotten'] = $this->language->get('text_forgotten');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_login'] = $this->language->get('button_login');
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', 'SSL'));
        $data['text_your_password'] = $this->language->get('text_your_password');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_password'] = $this->language->get('entry_password');

        $data['button_continue'] = $this->language->get('button_continue');
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->error['password'])) {
            $data['error_password'] = $this->error['password'];
        } else {
            $data['error_password'] = '';
        }
        $data['forgotten'] = $this->url->link('account/forgotten', '', 'SSL');

        $data['action'] = $this->url->link('account/login', '', 'SSL');

        // Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
        if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
            $data['redirect'] = $this->request->post['redirect'];
        } elseif (isset($this->session->data['redirect'])) {
            $data['redirect'] = $this->session->data['redirect'];

            unset($this->session->data['redirect']);
        } else {
            $data['redirect'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['password'])) {
            $data['password'] = $this->request->post['password'];
        } else {
            $data['password'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } else {
            $data['email'] = '';
        }

        // Captcha
        if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('register', (array) $this->config->get('config_captcha_page'))) {
            $data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'), $this->error);
        } else {
            $data['captcha'] = '';
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header', $data);

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/login.tpl', $data));
    }

    protected function validate() {
        $this->event->trigger('pre.partner.login');

        if (!$this->partner->login($this->request->post['email'], $this->request->post['password'])) {
            $this->error['warning'] = $this->language->get('error_login');
        }

        return !$this->error;
    }

    public function customfield() {
        $json = array();

        $this->load->model('account/custom_field');

        // partner Group
        if (isset($this->request->get['partner_group_id']) && is_array($this->config->get('config_partner_group_display')) && in_array($this->request->get['partner_group_id'], $this->config->get('config_partner_group_display'))) {
            $partner_group_id = $this->request->get['partner_group_id'];
        } else {
            $partner_group_id = $this->config->get('config_partner_group_id');
        }

        $custom_fields = $this->model_account_custom_field->getCustomFields($partner_group_id);

        foreach ($custom_fields as $custom_field) {
            $json[] = array(
                'custom_field_id' => $custom_field['custom_field_id'],
                'required' => $custom_field['required']
            );
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
