<?php
################################################################################################
# wk_rma_admin_setting Opencart 1.5.1.x From Webkul  http://webkul.com 	#
################################################################################################
class Controllerwkmprmareason extends Controller {

	private $error = array();
	private $data = array();

	public function index() {

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/customerpartner/wk_mprma_reason', '', 'SSL');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		$this->load->model('account/customerpartner');

		$data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

		if(!$data['chkIsPartner'])
			$this->response->redirect($this->url->link('account/account'));

		$this->language->load('account/customerpartner/wk_mprma_reason');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/wk_mprma');

		$filter_array = array(
							  'filter_reason',
							  'filter_status',
							  'page',
							  'sort',
							  'order',
							  'start',
							  'limit',
							  );

		$url = '';

		foreach ($filter_array as $unsetKey => $key) {

			if (isset($this->request->get[$key])) {
				$filter_array[$key] = $this->request->get[$key];
			} else {
				if ($key=='page')
					$filter_array[$key] = 1;
				elseif($key=='sort')
					$filter_array[$key] = 'cs.id';
				elseif($key=='order')
					$filter_array[$key] = 'ASC';
				elseif($key=='start')
					$filter_array[$key] = ($filter_array['page'] - 1) * $this->config->get('config_admin_limit');
				elseif($key=='limit')
					$filter_array[$key] = $this->config->get('config_admin_limit');
				else
					$filter_array[$key] = null;
			}
			unset($filter_array[$unsetKey]);

			if(isset($this->request->get[$key])){
				if ($key=='filter_reason')
					$url .= '&'.$key.'=' . urlencode(html_entity_decode($filter_array[$key], ENT_QUOTES, 'UTF-8'));
				else
					$url .= '&'.$key.'='. $filter_array[$key];
			}
		}

		$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('dashboard'),
        	'separator' => false
      	);

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account'),
        	'separator' => $this->language->get('text_separator')
      	);

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/customerpartner/wk_mprma_reason', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['entry_reason'] = $this->language->get('entry_reason');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['text_enable'] = $this->language->get('text_enable');
		$this->data['text_no_records'] = $this->language->get('text_no_records');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_disable'] = $this->language->get('text_disable');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_clrfilter'] = $this->language->get('button_clrfilter');
		$this->data['error_delete'] = $this->language->get('error_delete');

		$reason_total = $this->model_account_wk_mprma->viewtotalreason($filter_array);

		$results = $this->model_account_wk_mprma->viewreason($filter_array);

		$this->data['result'] = array();

		foreach ($results as $result) {
			$action = array();

			$action = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('account/customerpartner/wk_mprma_reason/update', '&id=' . $result['reason_id'], 'SSL')
			);

			$this->data['result'][] = array('selected'=>False,
											'id' => $result['reason_id'],
											'reason' => $result['reason'],
											'status' => $result['status'],
											'action' => $action
											);

		}

		$this->data['delete'] = $this->url->link('account/customerpartner/wk_mprma_reason/delete', '', 'SSL');
		$this->data['insert'] = $this->url->link('account/customerpartner/wk_mprma_reason/insert', '', 'SSL');



 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		foreach ($filter_array as $key => $value) {
			if(isset($this->request->get[$key])){
				if(!isset($this->request->get['order']) AND isset($this->request->get['sort']))
					$url .= '&order=DESC';
				elseif ($key=='filter_reason')
					$url .= '&'.$key.'=' . urlencode(html_entity_decode($filter_array[$key], ENT_QUOTES, 'UTF-8'));
				elseif($key=='order')
					$url .= $value=='ASC' ? '&order=DESC' : '&order=ASC';
				elseif($key!='start' AND $key!='limit' AND $key!='sort')
					$url .= '&'.$key.'='. $filter_array[$key];
			}
		}

		$this->data['sort_reason'] = $this->url->link('account/customerpartner/wk_mprma_reason', '&sort=wrr.reason' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('account/customerpartner/wk_mprma_reason', '&sort=wrr.status' . $url, 'SSL');

		$url = '';

		foreach ($filter_array as $key => $value) {
			if(isset($this->request->get[$key])){
				if(!isset($this->request->get['order']) AND isset($this->request->get['sort']))
					$url .= '&order=DESC';
				elseif ($key=='filter_reason')
					$url .= '&'.$key.'=' . urlencode(html_entity_decode($filter_array[$key], ENT_QUOTES, 'UTF-8'));
				elseif($key!='page')
					$url .= '&'.$key.'='. $filter_array[$key];
			}
		}

		$pagination = new Pagination();
		$pagination->total = $reason_total;
		$pagination->page = $filter_array['page'];
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('account/customerpartner/wk_mprma_reason', '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($reason_total) ? (($filter_array['page'] - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($filter_array['page'] - 1) * $this->config->get('config_limit_admin')) > ($reason_total - $this->config->get('config_limit_admin'))) ? $reason_total : ((($filter_array['page'] - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $reason_total, ceil($reason_total / $this->config->get('config_limit_admin')));

		foreach ($filter_array as $key => $value) {
			if($key!='start' AND $key!='end')
				$this->data[$key] = $value;
		}

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['column_right'] = $this->load->controller('common/column_right');
		$this->data['content_top'] = $this->load->controller('common/content_top');
		$this->data['content_bottom'] = $this->load->controller('common/content_bottom');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header',array('breadcrumbs' => $this->data['breadcrumbs']));

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/customerpartner/wk_mprma_reason.tpl')) {
			$this->response->setOutput($this->load->view( $this->config->get('config_template') . '/template/account/customerpartner/wk_mprma_reason.tpl' , $this->data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/customerpartner/wk_mprma_reason.tpl' , $this->data));
		}

  	}

  	public function insert() {

  		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/customerpartner/wk_mprma_manage', '', 'SSL');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		$this->load->model('account/customerpartner');

		$data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

		if(!$data['chkIsPartner'])
			$this->response->redirect($this->url->link('account/account'));

    	$this->language->load('account/customerpartner/wk_mprma_reason');

    	$this->document->setTitle($this->language->get('heading_title_insert'));

		$this->data['heading_title'] = $this->language->get('heading_title_insert');

		$this->load->model('account/wk_mprma');

    	if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

    		foreach ($this->request->post['reason'] as $key => $value) {
    			if ((utf8_strlen($value) < 5) || (utf8_strlen($value) > 50)) {
					$error = $this->language->get('error_reason');
					break;
				}
    		}

			if (!isset($error)) {

				$this->model_account_wk_mprma->addReason($this->request->post);

				$this->session->data['success'] = $this->language->get('text_success_insert');

				$this->response->redirect($this->url->link('account/customerpartner/wk_mprma_reason', '', 'SSL'));

			}

			$this->error['warning'] = $error ;
    	}

    	$this->getform();
  	}

  	public function update() {

  		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/customerpartner/wk_mprma_manage', '', 'SSL');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		$this->load->model('account/customerpartner');

		$data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

		if(!$data['chkIsPartner'])
			$this->response->redirect($this->url->link('account/account'));

    	$this->language->load('account/customerpartner/wk_mprma_reason');

    	$this->document->setTitle($this->language->get('heading_title_insert'));

		$this->data['heading_title'] = $this->language->get('heading_title_update');

		$this->load->model('account/wk_mprma');

    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

    		foreach ($this->request->post['reason'] as $key => $value) {
    			if ((utf8_strlen($value) < 5) || (utf8_strlen($value) > 50)) {
					$error = $this->language->get('error_reason');
					break;
				}
    		}

			if (!isset($error)) {

				$this->model_account_wk_mprma->UpdateReason($this->request->post);

				$this->session->data['success'] = $this->language->get('text_success_update');

				$this->response->redirect($this->url->link('account/customerpartner/wk_mprma_reason', '', 'SSL'));

			}

			$this->error['warning'] = $error ;
    	}

    	$this->getform();
  	}

  	private function getform() {

  	if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/customerpartner/wk_mprma_manage', '', 'SSL');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}
		$this->load->model('account/customerpartner');

		$data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

		if(!$data['chkIsPartner'])
			$this->response->redirect($this->url->link('account/account'));

  		$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('dashboard'),
        	'separator' => false
      	);

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account'),
        	'separator' => $this->language->get('text_separator')
      	);

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title_insert'),
			'href'      => $this->url->link('account/customerpartner/wk_mprma_reason', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);

		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['button_save'] = $this->language->get('button_save');

		$this->data['text_form'] = $this->language->get('text_form');

		//user
		$this->data['entry_reason'] = $this->language->get('entry_reason');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['text_disable'] = $this->language->get('text_disable');
		$this->data['text_enable'] = $this->language->get('text_enable');

		$config_data = array(
				'id',
				'reason',
				'status',
			);

		foreach ($config_data as $conf) {
			if (isset($this->request->post[$conf])) {
				$this->data[$conf] = $this->request->post[$conf];
			}
		}

		$this->data['save'] = $this->url->link('account/customerpartner/wk_mprma_reason/insert', '', 'SSL');

		if(isset($this->request->get['id'])){
			$id = $this->request->get['id'];

			$results = $this->model_account_wk_mprma->viewreasonbyId($id);

			if($results){
				foreach ($results as $key => $result) {
					$this->data['id'] = $result['reason_id'];
					$this->data['status'] = $result['status'];
					$this->data['reason'][$result['language_id']] = $result['reason'];
				}
			}
			$this->data['text_form'] = $this->language->get('text_edit_form');
			$this->data['save'] = $this->url->link('account/customerpartner/wk_mprma_reason/update', '', 'SSL');
		}

		if(!empty($this->data['id'])){
			$this->data['text_form'] = $this->language->get('text_edit_form');
			$this->data['save'] = $this->url->link('account/customerpartner/wk_mprma_reason/update', '','SSL');
		}

		$this->data['back'] = $this->url->link('account/customerpartner/wk_mprma_reason', '', 'SSL');


 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['column_right'] = $this->load->controller('common/column_right');
		$this->data['content_top'] = $this->load->controller('common/content_top');
		$this->data['content_bottom'] = $this->load->controller('common/content_bottom');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/customerpartner/wk_mprma_reason_form.tpl')) {
			$this->response->setOutput($this->load->view( $this->config->get('config_template') . '/template/account/customerpartner/wk_mprma_reason_form.tpl' , $this->data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/customerpartner/wk_mprma_reason_form.tpl' , $this->data));
		}

  	}

  	public function delete() {

    	$this->language->load('account/customerpartner/wk_mprma_reason');

    	$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/wk_mprma');

		if (isset($this->request->post['selected']) && $this->validateForm()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_account_wk_mprma->deleteReason($id);
	  		}

			$this->session->data['success'] = $this->language->get('text_success');

			$url='';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('account/customerpartner/wk_mprma_reason', '' . $url, 'SSL'));
		}

    	$this->index();
  	}

	private function validateForm() {

		// if (!$this->user->hasPermission('modify', 'account/customerpartner/wk_mprma_reason')) {
		// 	$this->error['warning'] = $this->language->get('error_permission');
		// }

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

}
?>
