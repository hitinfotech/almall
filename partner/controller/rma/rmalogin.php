<?php
class ControllerAccountRmaRmalogin extends Controller {

	private $error = array();
	private $data = array();

	public function index() {

		if(!$this->config->get('wk_rma_status'))
			$this->response->redirect($this->url->link('account/login'));

		$this->language->load('account/rma/rma');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$data = $this->request->post;
			if(isset($data['logintype'])){
				if($data['logintype']==1)
					$this->response->redirect($this->url->link('account/login'));
				else
					$this->response->redirect($this->url->link('account/rma/rmalogin/guest'));
			}else
				$this->error['warning'] = $this->language->get('error_logintype');
		}

		if ($this->customer->isLogged()){ // || isset($this->session->data['rma_login'])) {
			$this->response->redirect($this->url->link('account/rma/rma'));
		}

		$this->document->setTitle($this->language->get('heading_title_login'));
		$this->getlist();

	}

	public function getlist(){

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('dashboard'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_login'),
			'href'      => $this->url->link('account/rma/rmalogin'),
      		'separator' => ' :: '
   		);

		$this->data['action'] = $this->url->link('account/rma/rmalogin');

		$this->data['heading_title'] = $this->language->get('heading_title_login');
		$this->data['text_login_type'] = $this->language->get('text_login_type');
		$this->data['text_guest'] = $this->language->get('text_guest');
		$this->data['text_registered'] = $this->language->get('text_registered');
		$this->data['button_request'] = $this->language->get('button_request');
		$this->data['text_star'] = $this->language->get('text_star');
		$this->data['text_login_info'] = $this->language->get('text_login_info');

		$this->data['logintype'] = true;

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['header'] = $this->load->controller('common/header',array('breadcrumbs' => $this->data['breadcrumbs']));
		$this->data['footer'] = $this->load->controller('common/footer');
		$data['menu'] = $this->load->controller('account/menu');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['column_right'] = $this->load->controller('common/column_right');
		$this->data['content_top'] = $this->load->controller('common/content_top');
		$this->data['content_bottom'] = $this->load->controller('common/content_bottom');

		$this->response->setOutput($this->load->view($this->config->get('config_template').'/template/account/rma/rmalogin.tpl', $this->data));

	}

	public function guest() {

		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/rma/rma'));
		}

		$this->language->load('account/rma/rma');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$data = $this->request->post;

			if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
				$this->error['warning'] = $this->language->get('error_email');
			}

			if ((utf8_strlen($this->request->post['orderinfo']) < 1)) {
				$this->error['warning'] = $this->language->get('error_orderinfo');
			}

			if(!isset($this->error['warning'])){
				$this->load->model('account/rma/rma');
				$result = $this->model_account_rma_rma->getGuestStatus($data);
				if($result AND $result['customer_id']==0){
					$this->session->data['rma_login'] = $data['email'];
					$this->response->redirect($this->url->link('account/rma/rma'));
				}elseif($result AND $result['customer_id']!=0){
					$this->error['warning'] = $this->language->get('error_pleaselogin');
				}else{
					$this->error['warning'] = $this->language->get('error_order');
				}
			}

		}

		if(isset($this->request->post['email'])){
			$this->data['email'] = $this->request->post['email'];
		}else{
			$this->data['email'] = '';
		}

		if(isset($this->request->post['orderinfo'])){
			$this->data['orderinfo'] = $this->request->post['orderinfo'];
		}else{
			$this->data['orderinfo'] = '';
		}

		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('dashboard'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_login'),
			'href'      => $this->url->link('account/rma/rmalogin'),
      		'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_guest'),
			'href'      => $this->url->link('account/rma/rmalogin/guest'),
      		'separator' => ' :: '
   		);

   		$this->document->setTitle($this->language->get('heading_title_guest'));
		$this->data['action'] = $this->url->link('account/rma/rmalogin/guest');

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['wk_rma_orderid'] = $this->language->get('wk_rma_orderid');
		$this->data['text_email'] = $this->language->get('text_email');
		$this->data['button_request'] = $this->language->get('button_request');
		$this->data['text_star'] = $this->language->get('text_star');
		$this->data['text_guest_info'] = $this->language->get('text_guest_info');
		$this->data['text_order_info'] = $this->language->get('text_order_info');

		$this->data['guestlogin'] = true;

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['header'] = $this->load->controller('common/header',array('breadcrumbs' => $this->data['breadcrumbs']));
		$this->data['footer'] = $this->load->controller('common/footer');
		$data['menu'] = $this->load->controller('account/menu');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['column_right'] = $this->load->controller('common/column_right');
		$this->data['content_top'] = $this->load->controller('common/content_top');
		$this->data['content_bottom'] = $this->load->controller('common/content_bottom');

		$this->response->setOutput($this->load->view($this->config->get('config_template').'/template/account/rma/rmalogin.tpl', $this->data));
	}

}
?>
