<?php

class ControllerAccountRmarma extends Controller {

    private $error = array();
    private $data = array();
    
    private $status = array(
        '0' => 'Pending',
        '1' => 'Accept',
        '2' => 'Shipped',
        '3' => 'Decline',
        '4' => 'Return',
        '5' => 'Complete',
        '6' => 'Returned',
    );
    
    private $status_colors = array(
        '0' => 'blue',
        '1' => 'red',
        '2' => 'green',
        '3' => 'black',
        '4' => 'black',
        '5' => 'black',
        '6' => 'black'
    );
    
    protected function setValues(){
        $this->status = array();
        $this->status_colors = array();
        
        $statuses = $this->db->query(" SELECT * FROM rma_status ");
        if($statuses->num_rows) {
            foreach ($statuses->rows as $row) {
                $this->status[$row['rma_status_id']] = $row;
            }
        }
    }

    public function index() {
$this->setValues();
        if (!$this->config->get('wk_rma_status')) {
            $this->response->redirect($this->url->link('account/login'));
        }

        if (!$this->customer->isLogged() && ! isset($this->session->data['rma_login'])) {
            $this->session->data['redirect'] = $this->url->link('account/rma/rma');
            $this->response->redirect($this->url->link('account/rma/rmalogin'));
        }

        $this->language->load('account/rma/rma');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/moment.min.js');
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
        $this->document->addstyle(HTTPS_IMAGE_S3_STATIC . 'css/rma.min.css');
        $this->getlist();
    }

    private function getlist() {
$this->setValues();
        if (isset($this->request->get['filter_id'])) {
            $filter_id = $this->request->get['filter_id'];
        } else {
            $filter_id = null;
        }

        if (isset($this->request->get['filter_order'])) {
            $filter_order = $this->request->get['filter_order'];
        } else {
            $filter_order = null;
        }

        if (isset($this->request->get['filter_qty'])) {
            $filter_qty = $this->request->get['filter_qty'];
        } else {
            $filter_qty = null;
        }

        if (isset($this->request->get['filter_rma_status'])) {
            $filter_rma_status = $this->request->get['filter_rma_status'];
        } else {
            $filter_rma_status = null;
        }

        if (isset($this->request->get['filter_price'])) {
            $filter_price = $this->request->get['filter_price'];
        } else {
            $filter_price = null;
        }

        if (isset($this->request->get['filter_date'])) {
            $filter_date = $this->request->get['filter_date'];
        } else {
            $filter_date = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data = array(
            'filter_id' => $filter_id,
            'filter_qty' => $filter_qty,
            'filter_order' => $filter_order,
            'filter_price' => $filter_price,
            'filter_rma_status' => $filter_rma_status,
            'filter_date' => $filter_date,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit'),
            'email' => $this->customer->getEmail() ? $this->customer->getEmail() : $this->session->data['rma_login']
        );

        $url = '';

        if (isset($this->request->get['filter_id'])) {
            $url .= '&filter_id=' . $this->request->get['filter_id'];
        }

        if (isset($this->request->get['filter_order'])) {
            $url .= '&filter_order=' . $this->request->get['filter_order'];
        }

        if (isset($this->request->get['filter_qty'])) {
            $url .= '&filter_qty=' . $this->request->get['filter_qty'];
        }

        if (isset($this->request->get['filter_rma_status'])) {
            $url .= '&filter_rma_status=' . $this->request->get['filter_rma_status'];
        }

        if (isset($this->request->get['filter_price'])) {
            $url .= '&filter_price=' . $this->request->get['filter_price'];
        }

        if (isset($this->request->get['filter_date'])) {
            $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        $data['sort_id'] = $this->url->link('account/rma/rma', '' . '&sort=wro.id' . $url, 'SSL');
        $data['sort_qty'] = $this->url->link('account/rma/rma', '' . '&sort=wrp.quantity' . $url, 'SSL');
        $data['sort_order'] = $this->url->link('account/rma/rma', '' . '&sort=wro.order_id' . $url, 'SSL');
        $data['sort_price'] = $this->url->link('account/rma/rma', '' . '&sort=ot.value' . $url, 'SSL');
        $data['sort_rma_status'] = $this->url->link('account/rma/rma', '' . '&sort=wrs.order_status_id' . $url, 'SSL');
        $data['sort_date'] = $this->url->link('account/rma/rma', '' . '&sort=wro.date' . $url, 'SSL');

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        $this->load->model('account/rma/rma');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('dashboard' . $url),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/rma/rma' . $url),
            'separator' => ' :: '
        );

        $data['newrma'] = $this->url->link('account/rma/addrma');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['heading_title'] = $this->language->get('heading_title');
        $data['button_insert'] = $this->language->get('button_insert');
        $data['button_delete'] = $this->language->get('button_delete');

        $data['wk_rma_id'] = $this->language->get('wk_rma_id');
        $data['wk_rma_orderid'] = $this->language->get('wk_rma_orderid');
        $data['wk_rma_status'] = $this->language->get('wk_rma_status');
        $data['wk_rma_date'] = $this->language->get('wk_rma_date');
        $data['wk_rma_action'] = $this->language->get('wk_rma_action');
        $data['wk_rma_price'] = $this->language->get('wk_rma_price');
        $data['wk_rma_viewdetails'] = $this->language->get('wk_rma_viewdetails');
        $data['wk_rma_cancel'] = $this->language->get('wk_rma_cancel');
        $data['wk_rma_empty'] = $this->language->get('wk_rma_empty');
        $data['wk_rma_add'] = $this->language->get('wk_rma_add');
        $data['wk_rma_r_u_sure'] = $this->language->get('wk_rma_r_u_sure');
        $data['wk_rma_cncl_rma'] = $this->language->get('wk_rma_cncl_rma');
        $data['wk_rma_quantity'] = $this->language->get('wk_rma_quantity');
        $data['button_filter'] = $this->language->get('button_filter');

        $data['text_canceled'] = $this->language->get('text_canceled');
        $data['text_solved'] = $this->language->get('text_solved');
        $data['text_pending'] = $this->language->get('text_pending');
        $data['text_processing'] = $this->language->get('text_processing');
        $data['text_return_description'] = $this->language->get('text_return_description');
        $data['text_create_new_return'] = $this->language->get('text_create_new_return');


        $data['rma_ress'] = array();

        $data['rma_sta'] = $this->status;

        $results = $this->model_account_rma_rma->ramDetails($data);

        $resultrmatotal = $this->model_account_rma_rma->rmaTotal($data);

        foreach ($results as $result) {

            $action = array();

            $action[] = array(
                'text' => $this->language->get('wk_rma_viewdetails'),
                'href' => $this->url->link('account/rma/viewrma' . '&vid=' . $result['id'])
            );

            if (!$result['quantity'])
                continue;
            
            $prodetails = $this->model_account_rma_rma->prodetails($result['id'], $result['order_id']);

            if ($prodetails) {
                $rma_total = 0;
                foreach ($prodetails as $products) {
                    $rma_total += ($products['price'] + $products['tax']) * $products['returned'];
                }
            }
            $data['rma_ress'][] = array(
                'selected' => False,
                'id' => $result['id'],
                'order_id' => $result['order_id'],
                // 'price' => $this->currency->format($result['value']),
                'price' => $this->currency->format($rma_total),
                'date' => date($this->language->get('date_format_short'), strtotime($result['date'])),
                'color' => $result['color'],
                'cancel_rma_link' => $this->url->link('account/rma/rma/cnclrma&rma=' . $result['id'], '' . $url, 'SSL'),
                'rma_status' => $$result['rma_status'],
                'quantity' => $result['quantity'],
                'action' => $action,
            );
        }

        if (isset($this->session->data['rma_error'])) {
            $this->error['warning'] = $this->session->data['rma_error'];
            unset($this->session->data['rma_error']);
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_id'])) {
            $url .= '&filter_id=' . $this->request->get['filter_id'];
        }

        if (isset($this->request->get['filter_order'])) {
            $url .= '&filter_order=' . $this->request->get['filter_order'];
        }

        if (isset($this->request->get['filter_qty'])) {
            $url .= '&filter_qty=' . $this->request->get['filter_qty'];
        }

        if (isset($this->request->get['filter_rma_status'])) {
            $url .= '&filter_rma_status=' . $this->request->get['filter_rma_status'];
        }

        if (isset($this->request->get['filter_price'])) {
            $url .= '&filter_price=' . $this->request->get['filter_price'];
        }

        if (isset($this->request->get['filter_date'])) {
            $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $resultrmatotal;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_product_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('account/rma/rma' . $url, '&page={page}');

        $data['pagination'] = $pagination->render();
        $data['results'] = sprintf($this->language->get('text_pagination'), ($resultrmatotal) ? (($page - 1) * $this->config->get('config_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get('config_product_limit')) > ($resultrmatotal - $this->config->get('config_product_limit'))) ? $resultrmatotal : ((($page - 1) * $this->config->get('config_product_limit')) + $this->config->get('config_limit_admin')), $resultrmatotal, ceil($resultrmatotal / $this->config->get('config_product_limit')));

        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['filter_id'] = $filter_id;
        $data['filter_qty'] = $filter_qty;
        $data['filter_order'] = $filter_order;
        $data['filter_price'] = $filter_price;
        $data['filter_rma_status'] = $filter_rma_status;
        $data['filter_date'] = $filter_date;

        $data['header'] = $this->load->controller('common/header', array('breadcrumbs' => $data['breadcrumbs']));
        $data['footer'] = $this->load->controller('common/footer');
        $data['menu'] = $this->load->controller('account/menu');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/rma/rma.tpl', $data));
    }

    public function cnclrma() {

        $this->load->model('account/rma/rma');
        $this->language->load('account/rma/rma');

        if (isset($this->request->get['rma']) AND $this->request->get['rma']) {
            if ($this->customer->getId() || isset($this->session->data['rma_login'])) {
                $results = $this->model_account_rma_rma->viewRmaid($this->request->get['rma']);
                if ($results) {
                    $getCustomerCancel = $this->model_account_rma_rma->updateRmaSta(1, (int) $this->request->get['rma']);
                    $this->session->data['success'] = $this->language->get('text_cancel_rma');
                } else {
                    $this->session->data['rma_error'] = $this->language->get('error_rma_delete');
                }
            } else {
                $this->session->data['rma_error'] = $this->language->get('error_rma_delete');
            }
        }
        $this->response->redirect($this->url->link('account/rma/rma'));
    }

    public function addcons() {

        $this->load->model('account/rma/rma');

        $json = array();

        if (isset($this->request->post['auth_no']) AND $this->request->post['auth_no'] AND isset($this->request->post['vid']) AND $this->request->post['vid']) {
            if ($this->customer->getId() || isset($this->session->data['rma_login'])) {
                $results = $this->model_account_rma_rma->viewRmaid($this->request->post['vid']);
                if ($results) {
                    $this->model_account_rma_rma->updateRmaAuth($this->request->post['auth_no'], $this->request->post['vid']);
                    $json['success'] = 'done';
                }
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    public function getorder() {

        $this->load->model('account/rma/rma');

        if (isset($this->request->post['order']) AND $this->request->post['order']) {
            $result = array();
            $product_data = array();
            $products = $this->model_account_rma_rma->getOrderProducts($this->request->post['order']);



            foreach ($products as $product) {
                $option_data = array();

                $options = $this->model_account_rma_rma->getOrderOptions($this->request->post['order'], $product['order_product_id']);

                foreach ($options as $option) {
                    if ($option['type'] != 'file') {
                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $option['value'],
                            'type' => $option['type'],
                            'product_option_value_id' => $option['product_option_value_id'],
                        );
                    } else {
                        $upload_info = $this->model_account_rma_rma->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $upload_info['name'],
                                'type' => $option['type'],
                                'product_option_value_id' => $option['product_option_value_id'],
                            );
                        }
                    }
                }

                if (isset($option_data) && !empty($option_data)) {
                    $sellerId = $this->model_account_rma_rma->getsellerId($product['product_id'], $option_data);
                } else {
                    $sellerId = $this->model_account_rma_rma->getsellerId($product['product_id']);
                }

                $product_data[] = array(
                    'name' => $product['name'],
                    'product_id' => $product['product_id'],
                    'model' => $product['model'],
                    'quantity' => $product['quantity'],
                    'order_product_id' => $product['order_product_id'],
                    'option' => $option_data,
                    'customer_id' => $sellerId,
                );
            }

            $result['productInfo'] = $product_data;
            foreach ($result['productInfo'] as $key => $value) {
                $result['sellerReason'] = $this->model_account_rma_rma->getCustomerReason();
            }

            $this->response->setOutput(json_encode($result));
        }
    }

}
