<?php

class ControllerAccountRmaviewrma extends Controller {

    private $error = array();
    private $data = array();

    public function index() {

        if (!$this->config->get('wk_rma_status')) {
            $this->response->redirect($this->url->link('account/login'));
        }

        $this->load->model('account/rma/rma');
        $this->load->model('tool/image');

        $this->language->load('account/rma/viewrma');

        if (!$this->customer->isLogged() && ! isset($this->session->data['rma_login'])) {
            $this->session->data['redirect'] = $this->url->link('account/rma/viewrma&vid=' . $this->request->get['vid']);
            $this->response->redirect($this->url->link('account/rma/rmalogin'));
        }

        if (!isset($this->request->get['vid'])) {
            $this->response->redirect($this->url->link('account/rma/rma'));
        }
        
        $this->document->addstyle(HTTPS_IMAGE_S3_STATIC . 'css/rma.min.css');

        $vid = $this->data['vid'] = $this->request->get['vid'];

        //check customer is authorized for this RMA or not
        $result = $this->model_account_rma_rma->viewRmaid($vid);

        if (!$result) {
            $this->session->data['rma_error'] = $this->language->get('text_error');
            $this->response->redirect($this->url->link('account/rma/rma'));
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

            if (isset($this->request->post['solve'])) {
                if (!$result) {
                    $this->error['warning'] = $this->language->get('text_error');
                } else {
                    $this->model_account_rma_rma->updateRmaSta($this->request->post['solve'], $this->request->post['wk_viewrma_grp_id']);
                    $this->session->data['success'] = $this->language->get('text_success');
                    $this->response->redirect($this->url->link('account/rma/viewrma&vid=' . $this->request->post['wk_viewrma_grp_id'], '', 'SSL'));
                }
            }

            if (isset($this->request->post['wk_view_message'])) {

                if (!$result) {
                    $this->session->data['rma_error'] = $this->language->get('text_error');
                    $this->response->redirect($this->url->link('account/rma/rma'));
                }

                $file_name = '';
                if ($this->request->files['up_file']['name']) {
                    $file = $this->request->files['up_file'];
                    if ($this->fileExtensioncheck($file['name'], $file['size'])) {
                        $file_name = $file['name'];
                        $target = DIR_IMAGE . 'rma/' . $result['images'] . "/files/" . $file['name'];
                        @move_uploaded_file($file['tmp_name'], $target);
                    }
                }

                $writer = 'me';
                $this->model_account_rma_rma->insertMessageRma($this->request->post['wk_view_message'], $writer, $this->request->post['wk_viewrma_grp_id'], $file_name, $this->request->post['seller_id']);
                $this->session->data['success'] = $this->language->get('text_success_msg');

                if (isset($this->request->post['wk_view_reopensolved'])) {
                    $this->model_account_rma_rma->updateRmaSta($this->request->post['wk_view_reopensolved'], $this->request->post['wk_viewrma_grp_id'], true);
                    $this->session->data['success'] = $this->language->get('text_success_reopen');
                }

                $this->response->redirect($this->url->link('account/rma/viewrma&vid=' . $this->request->post['wk_viewrma_grp_id'], '', 'SSL'));
            }
        }

        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['button_back'] = $this->language->get('button_back');
        $this->data['button_save'] = $this->language->get('button_save');

        $this->data['wk_viewrma_orderid'] = $this->language->get('wk_viewrma_orderid');
        $this->data['wk_viewrma_status'] = $this->language->get('wk_viewrma_status');
        $this->data['wk_viewrma_reason'] = $this->language->get('wk_viewrma_reason');
        $this->data['wk_viewrma_rma_tatus'] = $this->language->get('wk_viewrma_rma_tatus');
        $this->data['wk_viewrma_status_admin'] = $this->language->get('wk_viewrma_status_admin');
        $this->data['wk_viewrma_status_customer'] = $this->language->get('wk_viewrma_status_customer');
        $this->data['wk_viewrma_authno'] = $this->language->get('wk_viewrma_authno');
        $this->data['wk_viewrma_add_info'] = $this->language->get('wk_viewrma_add_info');
        $this->data['wk_viewrma_image'] = $this->language->get('wk_viewrma_image');
        $this->data['wk_viewrma_close_rma'] = $this->language->get('wk_viewrma_close_rma');
        $this->data['wk_viewrma_close_rma_text'] = $this->language->get('wk_viewrma_close_rma_text');
        $this->data['wk_viewrma_item_req'] = $this->language->get('wk_viewrma_item_req');
        $this->data['wk_viewrma_pname'] = $this->language->get('wk_viewrma_pname');
        $this->data['wk_viewrma_enter_cncs_no'] = $this->language->get('wk_viewrma_enter_cncs_no');
        $this->data['wk_viewrma_model'] = $this->language->get('wk_viewrma_model');
        $this->data['wk_viewrma_price'] = $this->language->get('wk_viewrma_price');
        $this->data['wk_viewrma_qty'] = $this->language->get('wk_viewrma_qty');
        $this->data['wk_viewrma_subtotal'] = $this->language->get('wk_viewrma_subtotal');
        $this->data['wk_viewrma_msg'] = $this->language->get('wk_viewrma_msg');
        $this->data['wk_viewrma_enter_msg'] = $this->language->get('wk_viewrma_enter_msg');
        $this->data['wk_viewrma_submsg'] = $this->language->get('wk_viewrma_submsg');
        $this->data['wk_viewrma_reopen'] = $this->language->get('wk_viewrma_reopen');
        $this->data['wk_viewrma_valid_no'] = $this->language->get('wk_viewrma_valid_no');
        $this->data['wk_viewrma_ret_qty'] = $this->language->get('wk_viewrma_ret_qty');
        $this->data['text_delivered'] = $this->language->get('text_delivered');
        $this->data['text_not_delivered'] = $this->language->get('text_not_delivered');
        $this->data['text_canceled'] = $this->language->get('text_canceled');
        $this->data['text_solved'] = $this->language->get('text_solved');
        $this->data['text_pending'] = $this->language->get('text_pending');
        $this->data['text_processing'] = $this->language->get('text_processing');
        $this->data['text_pac_not_rec'] = $this->language->get('text_pac_not_rec');
        $this->data['text_pac_rec'] = $this->language->get('text_pac_rec');
        $this->data['text_pac_dis'] = $this->language->get('text_pac_dis');
        $this->data['text_return_dec'] = $this->language->get('text_return_dec');
        $this->data['text_upload_success'] = $this->language->get('text_upload_success');
        $this->data['text_print'] = $this->language->get('text_print');
        $this->data['text_upload_img'] = $this->language->get('text_upload_img');
        $this->data['text_order_details'] = $this->language->get('text_order_details');
        $this->data['text_messages'] = $this->language->get('text_messages');
        $this->data['text_download'] = $this->language->get('text_download');
        $this->data['text_print_lable'] = $this->language->get('text_print_lable');
        $this->data['button_upload'] = $this->language->get('button_upload');
        $this->data['text_allowed_ex'] = sprintf($this->language->get('text_allowed_ex'), $this->config->get('wk_rma_system_file'), $this->config->get('wk_rma_system_size'));

        $this->data['action'] = $this->url->link('account/rma/viewrma&vid=' . $vid, '', 'SSL');
        $this->data['print'] = $this->url->link('account/rma/viewrma&print&vid=' . $vid, '', 'SSL');
        $this->data['print_shipping_lable'] = $this->url->link('account/rma/viewrma/printlable&vid=' . $vid, '', 'SSL');
        $this->data['back'] = $this->url->link('account/rma/rma', '', 'SSL');

        $this->data['viewrma'] = array();

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('dashboard'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title_list'),
            'href' => $this->url->link('account/rma/rma'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/rma/viewrma&vid=' . $vid),
            'separator' => ' :: '
        );

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $limit = 5;
        $start = ($page - 1) * $limit;

        //get RMA messages
        $result_messages = $this->model_account_rma_rma->viewRmaMessage($vid, $start, $limit);
        $total_result_messages = $this->model_account_rma_rma->viewTotalRmaMessage($vid);

        $this->data['rma_messages'] = $result_messages;
        $this->data['attachmentLink'] = HTTP_SERVER . 'image/rma/' . $result['images'] . '/files/';

        if ($result) {

            $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/RMA/ajaxfileupload.min.js');
            $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/magnific/jquery.magnific-popup.min.js');
            $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/magnific/magnific-popup.min.css');

            $sellerId = 0;
            $this->data['prodetails'] = array();
            $prodetails = $this->model_account_rma_rma->prodetails($vid, $result['order_id']);

            if ($prodetails) {
                foreach ($prodetails as $products) {

                    $option_data = array();

                    $options = $this->model_account_rma_rma->getOrderOptions($result['order_id'], $products['order_product_id']);

                    foreach ($options as $option) {
                        if ($option['type'] != 'file') {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $option['value'],
                                'type' => $option['type']
                            );
                        } else {
                            $upload_info = $this->model_account_rma_rma->getUploadByCode($option['value']);

                            if ($upload_info) {
                                $option_data[] = array(
                                    'name' => $option['name'],
                                    'value' => $upload_info['name'],
                                    'type' => $option['type']
                                );
                            }
                        }
                    }

                    $this->data['prodetails'][] = array('price' => $this->currency->format(($products['price'] + $products['tax']), $this->currency->getCode()),
                        'total' => $this->currency->format(($products['price'] + $products['tax']) * $products['returned'], $this->currency->getCode()),
                        'name' => $products['name'],
                        'link' => $this->url->link('product/product&product_id=' . $products['product_id'], '', 'SSL'),
                        'ordered' => $products['ordered'],
                        'reason' => $products['reason'],
                        'model' => $products['model'],
                        'returned' => $products['returned'],
                        'option' => $option_data,
                    );
                }
                $this->data['sellerId'] = $prodetails[0]['customer_id'];
            }

            $images = array();

            if ($result['images']) {
                $path = 'rma/' . $result['images'] . '/files/';
                $dir = DIR_IMAGE . 'rma/' . $result['images'] . '/files/';
                if (file_exists($dir)) {
                    if ($dh = opendir($dir)) {
                        while (($file = readdir($dh)) !== false) {
                            if (!is_dir($file)) {
                                $images [] = array('resize' => $this->model_tool_image->resize($path . $file, 125, 125),
                                    'image' => $this->model_tool_image->resize($path . $file, 500, 500),
                                );
                            }
                        }
                    }
                }
            }
            
            $this->data['viewrma'] = array(
                'id' => $vid,
                'order_id' => $result['order_id'],
                'order_url' => $this->url->link('account/order/info&order_id=' . $result['order_id']),
                'images' => $images,
                'add_info' => $result['add_info'],
                'date' => date($this->language->get('date_format_short'), strtotime($result['date'])),
                'color' => $result['color'],
                'rma_status' => $result['rma_status'],
                'customer_status' => $result['customer_status_id'],
                'shipping_label' => $result['shipping_label'],
                'rma_auth_no' => $result['rma_auth_no']
            );
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $pagination = new Pagination();
        $pagination->total = $total_result_messages;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('account/rma/viewrma&vid=' . $vid, '&page={page}');
        $this->data['pagination'] = $pagination->render();

        $this->data['results'] = sprintf($this->language->get('text_pagination'), ($total_result_messages) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($total_result_messages - $limit)) ? $total_result_messages : ((($page - 1) * $limit) + $limit), $total_result_messages, ceil($total_result_messages / $limit));

        $this->data['header'] = $this->load->controller('common/header', array('breadcrumbs' => $this->data['breadcrumbs']));
        $this->data['footer'] = $this->load->controller('common/footer');
        $this->data['menu'] = $this->load->controller('account/menu');
        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['column_right'] = $this->load->controller('common/column_right');
        $this->data['content_top'] = $this->load->controller('common/content_top');
        $this->data['content_bottom'] = $this->load->controller('common/content_bottom');

        $tpl = '';
        if (isset($this->request->get['print'])) {
            $tpl = 'print';
            $this->data['direction'] = 'ltr';
            $this->data['lang'] = 'en';
            $this->data['base'] = HTTP_SERVER;
        }

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/rma/viewrma' . $tpl . '.tpl', $this->data));
    }

    private function fileExtensioncheck($name, $size, $config = 'wk_rma_system_file') {

        $type = explode('.', $name);

        if (($this->config->get($config) == '*') || in_array(end($type), explode(',', $this->config->get($config)))) {
            if (($size / 1000) <= (int) $this->config->get('wk_rma_system_size'))
                return true;
            else
                return false;
        }else {
            return false;
        }
    }

    public function printlable() {

        if (!isset($this->request->get['vid'])) {
            $this->response->redirect($this->url->link('account/account'));
        }

        if (!$this->customer->isLogged() AND ! isset($this->session->data['rma_login'])) {
            $this->session->data['redirect'] = $this->url->link('account/rma/viewrma/printlable&vid=' . $this->request->get['vid']);
            $this->response->redirect($this->url->link('account/rma/rmalogin'));
        }

        $this->load->model('tool/image');
        $this->load->model('account/rma/rma');

        $result = $this->model_account_rma_rma->viewRmaid($this->request->get['vid']);

        $this->language->load('account/rma/viewrma');
        $this->document->setTitle($this->language->get('heading_title_label'));

        $this->data['heading_title'] = $this->language->get('heading_title_label');
        $this->data['text_from'] = $this->language->get('text_from');
        $this->data['text_to'] = $this->language->get('text_to');
        $this->data['text_shipping_label'] = $this->language->get('text_shipping_label');
        $this->data['text_quantity'] = $this->language->get('wk_viewrma_qty');
        $this->data['text_product_name'] = $this->language->get('wk_viewrma_pname');
        $this->data['text_reason'] = $this->language->get('wk_viewrma_reason');
        $this->data['text_auth_label'] = $this->language->get('text_auth_label');
        $this->data['text_order_id'] = $this->language->get('wk_viewrma_orderid');
        $this->data['text_rma_id'] = $this->language->get('text_rma_id');

        $this->data['rma_id'] = $this->request->get['vid'];
        $this->data['direction'] = 'ltr';
        $this->data['lang'] = 'en';
        $this->data['base'] = HTTP_SERVER;
        $this->data['address'] = $this->config->get('wk_rma_address');
        $this->data['label'] = $this->data['order_id'] = '';
        $this->data['prodetails'] = array();

        if ($result AND $result['images'] AND $result['shipping_label']) {
            $this->load->model('account/wk_mprma');
            $order_products = $this->model_account_wk_mprma->getOrderProducts($result['order_id'], $result['id']);

            if ($this->config->get('wk_mpaddproduct_status')) {

                $cp2p_chk = count($this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_product WHERE product_id = '" . (int) $order_products[0]['product_id'] . "' ")->rows);
                $pc_chk = count($this->db->query("SELECT * FROM " . DB_PREFIX . "price_comparison WHERE product_id = '" . (int) $order_products[0]['product_id'] . "' ")->rows);
                if ($cp2p_chk != $pc_chk) {
                    //seller products
                    $seller = $this->db->query("SELECT cp2p.customer_id, c.firstname FROM " . DB_PREFIX . "customerpartner_to_product cp2p LEFT JOIN " . DB_PREFIX . "customer c ON (cp2p.customer_id=c.customer_id) WHERE cp2p.product_id = '" . (int) $order_products[0]['product_id'] . "' ORDER BY cp2p.id LIMIT 1 ")->row;
                    if (isset($seller['customer_id']) && $seller['customer_id']) {
                        $label = "rma/" . $seller['firstname'] . '/' . $result['shipping_label'];
                        if (file_exists(DIR_IMAGE . $label)) {
                            $this->data['label'] = $this->model_tool_image->resize($label, 200, 200);
                        }
                    } else {
                        $label = "rma/files/" . $result['shipping_label'];

                        if (file_exists(DIR_IMAGE . $label)) {
                            $this->data['label'] = $this->model_tool_image->resize($label, 200, 200);
                        }
                    }
                } else {
                    //admin products
                    $label = "rma/files/" . $result['shipping_label'];

                    if (file_exists(DIR_IMAGE . $label)) {
                        $this->data['label'] = $this->model_tool_image->resize($label, 200, 200);
                    }
                }
            } else {
                $cp2p_chk = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customerpartner_to_product WHERE product_id = '" . (int) $order_products[0]['product_id'] . "' ")->row;
                if (isset($cp2p_chk['customer_id']) && $cp2p_chk['customer_id']) {
                    //seller products
                    $seller = $this->db->query("SELECT cp2p.customer_id, c.firstname FROM " . DB_PREFIX . "customerpartner_to_product cp2p LEFT JOIN " . DB_PREFIX . "customer c ON (cp2p.customer_id=c.customer_id) WHERE cp2p.product_id = '" . (int) $order_products[0]['product_id'] . "' ORDER BY cp2p.id LIMIT 1 ")->row;
                    if (isset($seller['customer_id']) && $seller['customer_id']) {
                        $label = "rma/" . $seller['firstname'] . '/' . $result['shipping_label'];
                        if (file_exists(DIR_IMAGE . $label)) {
                            $this->data['label'] = $this->model_tool_image->resize($label, 200, 200);
                        }
                    } else {
                        $label = "rma/files/" . $result['shipping_label'];

                        if (file_exists(DIR_IMAGE . $label)) {
                            $this->data['label'] = $this->model_tool_image->resize($label, 200, 200);
                        }
                    }
                } else {
                    //admin products
                    $label = "rma/files/" . $result['shipping_label'];

                    if (file_exists(DIR_IMAGE . $label)) {
                        $this->data['label'] = $this->model_tool_image->resize($label, 200, 200);
                    }
                }
            }

            $this->data['order_id'] = $result['order_id'];

            $prodetails = $this->model_account_rma_rma->prodetails($this->request->get['vid'], $result['order_id']);

            if ($prodetails) {
                foreach ($prodetails as $products) {

                    $option_data = array();

                    $options = $this->model_account_rma_rma->getOrderOptions($result['order_id'], $products['order_product_id']);

                    foreach ($options as $option) {
                        if ($option['type'] != 'file') {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $option['value'],
                                'type' => $option['type']
                            );
                        } else {
                            $upload_info = $this->model_account_rma_rma->getUploadByCode($option['value']);

                            if ($upload_info) {
                                $option_data[] = array(
                                    'name' => $option['name'],
                                    'value' => $upload_info['name'],
                                    'type' => $option['type']
                                );
                            }
                        }
                    }

                    $this->data['prodetails'][] = array(
                        'product_id' => $products['product_id'],
                        'name' => $products['name'],
                        'model' => $products['model'],
                        'ordered' => $products['ordered'],
                        'tax' => $products['tax'],
                        'price' => $products['price'],
                        'reason' => $products['reason'],
                        'returned' => $products['returned'],
                        'customer_id' => $products['customer_id'],
                        'order_product_id' => $products['order_product_id'],
                        'option' => $option_data,
                    );
                }
            }
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/rma/printlable.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/rma/printlable.tpl', $this->data));
        } else {
            $this->response->setOutput($this->load->view('default/template/account/rma/printlable.tpl', $this->data));
        }
    }

    public function imageupload() {

        $json = '';

        if (!isset($this->request->post['id']) || !isset($this->request->files['rma_file']))
            return;

        $this->load->model('tool/image');

        $this->load->model('account/rma/rma');
        $result = $this->model_account_rma_rma->viewRmaid($this->request->post['id']);

        if ($result AND $result['images']) {

            $img_folder = DIR_IMAGE . "rma/" . $result['images'];

            $files = $this->request->files['rma_file'];

            if ($files['name'] AND $files['name'][0]) {

                $path = 'rma/' . $result['images'] . '/';

                foreach ($files['name'] as $key => $value) {
                    if (isset($files['tmp_name'][$key]) AND $files['tmp_name'][$key]) {
                        if (!$this->fileExtensioncheck($value, $files['size'][$key], 'wk_rma_system_image'))
                            continue;
                        $target = $img_folder . "/" . $value;

                        move_uploaded_file($files['tmp_name'][$key], $target);

                        $resize = $this->model_tool_image->resize($path . $value, 125, 125);
                        $image = $this->model_tool_image->resize($path . $value, 500, 500);

                        if ($this->config->get('config_template') == 'journal2') {
                            $json .= '<li class="pull-left"><a href="' . $image . '" data-toggle="tooltip" title="View Images" class=""><img src="' . $resize . '"/></a></li>';
                        } else {
                            $json .= '<li class="image-additional"><a href="' . $image . '" data-toggle="tooltip" title="View Images" class="thumbnail"><img src="' . $image . '"/></a></li>';
                        }
                    }
                }
            }
        }

        $this->response->setOutput($json);
    }

}
