<?php

class ControllerCollectionPopup extends Controller {

    private $cookie_name = 'newsletterpopup';

    public function index($params = array()) {

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "popup_settings` WHERE country_id = '" . (int) $this->country->getid() . "' AND language_id = '" . (int) $this->config->get('config_language_id') . "'");
        $setting = array();

        foreach ($query->rows as $row) {
            if (!$row['serialized']) {
                $setting[$row['key']] = $row['value'];
            } else {
                $setting[$row['key']] = json_decode($row['value']);
            }
        }

        if (!empty($setting) && isset($setting['popup_block0_status1']) && $setting['popup_block0_status1'] == 1) {
            $this->load->language('collection/popup');

            if ($this->config->get('config_account_id')) {
                $this->load->model('catalog/information');
                $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
                if ($information_info) {
                    $data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), 'SSL'), $information_info['title'], $information_info['title']);
                } else {
                    $data['text_agree'] = '';
                }
            } else {
                $data['text_agree'] = '';
            }

            $this->load->model('localisation/country');
            $data['countries'] = $this->model_localisation_country->getCountries();
            $data['country_id'] = $this->country->getid() ;
            $data['birthdate'] = date('Y-m-d') ;
            $data['male'] = $this->language->get('male');
            $data['female'] = $this->language->get('female');
            $data['text_thanks'] = $this->language->get('text_thanks');
            $data['text_done_add'] = $this->language->get('text_done_add');
            $data['text_done2'] = $this->language->get('text_done2');
            $data['text_welcome2'] = $this->language->get('text_welcome2');
            $data['text_newscon'] = $this->language->get('text_newscon');
            $data['text_email'] = $this->language->get('text_email');
            $data['entry_firstname'] = $this->language->get('entry_firstname');
            $data['entry_lastname'] = $this->language->get('entry_lastname');
            $data['entry_email'] = $this->language->get('entry_email');
            $data['entry_telephone'] = $this->language->get('entry_telephone');
            $data['entry_fax'] = $this->language->get('entry_fax');
            $data['entry_company'] = $this->language->get('entry_company');
            $data['entry_address_1'] = $this->language->get('entry_address_1');
            $data['entry_address_2'] = $this->language->get('entry_address_2');
            $data['entry_postcode'] = $this->language->get('entry_postcode');
            $data['entry_city'] = $this->language->get('entry_city');
            $data['entry_country'] = $this->language->get('entry_country');
            $data['entry_zone'] = $this->language->get('entry_zone');
            $data['entry_newsletter'] = $this->language->get('entry_newsletter');
            $data['entry_password'] = $this->language->get('entry_password');
            $data['entry_confirm'] = $this->language->get('entry_confirm');
            $data['entry_gender'] = $this->language->get('entry_gender');
            $data['entry_gender_male'] = $this->language->get('entry_gender_male');
            $data['entry_gender_female'] = $this->language->get('entry_gender_female');
            $data['entry_birthdate'] = $this->language->get('entry_birthdate');
            $data['month'] = $this->language->get('month');
            $data['day'] = $this->language->get('day');
            $data['year'] = $this->language->get('year');
            $data['register'] = $this->language->get('register');
            $data['close'] = $this->language->get('close');
            $data['error_email'] = $this->language->get('error_email');
            $data['error_exists'] = $this->language->get('error_exists');
            $data['lang'] = $this->language->get('code');
            $data['text_submite'] = $this->url->link('account/newsletter/subscribe2', 'SSL');

            $setting['text_shopnow'] = $this->language->get('text_shopnow');
            $data['setting']['popup_block0_title1'] = (strip_tags(html_entity_decode($setting['popup_block0_title1'], ENT_QUOTES, 'UTF-8'), 0));
            $data['setting']['popup_block0_color1'] = (strip_tags(html_entity_decode($setting['popup_block0_color1'], ENT_QUOTES, 'UTF-8'), 0));
            $data['setting']['popup_block0_description1'] = (strip_tags(html_entity_decode($setting['popup_block0_description1'], ENT_QUOTES, 'UTF-8'), 0));
            $this->load->model('tool/image');
            $data['setting']['popup_block0_image1'] = $this->model_tool_image->resize($setting['popup_block0_image1'], $this->config_image->get('getpopup', 'popup_block0_image1', 'width'), $this->config_image->get('getpopup', 'popup_block0_image1', 'hieght'));
            $data['setting']['popup_block0_image2'] = $this->model_tool_image->resize($setting['popup_block0_image2'], $this->config_image->get('getpopup', 'popup_block0_image2', 'width'), $this->config_image->get('getpopup', 'popup_block0_image2', 'hieght'));

            $data['setting']['popup_block1_description1'] = (strip_tags(html_entity_decode($setting['popup_block1_description1'], ENT_QUOTES, 'UTF-8'), 0));
            $data['setting']['popup_block1_title1'] = (strip_tags(html_entity_decode($setting['popup_block1_title1'], ENT_QUOTES, 'UTF-8'), 0));

            $data['setting']['popup_block2_description1'] = (strip_tags(html_entity_decode($setting['popup_block2_description1'], ENT_QUOTES, 'UTF-8'), 0));
            $data['setting']['popup_block2_title1'] = (strip_tags(html_entity_decode($setting['popup_block2_title1'], ENT_QUOTES, 'UTF-8'), 0));
            $data['setting']['popup_block2_coupon1'] = (strip_tags(html_entity_decode($setting['popup_block2_coupon1'], ENT_QUOTES, 'UTF-8'), 0));

            return $this->load->view($this->config->get('config_template') . '/template/collection/popup.tpl', $data);
        }
        return '';
    }

}
