<?php

class ControllerAddBrand extends Controller {

  public function index() {

    $this->language->load('account/brand');
    $this->document->setTitle($this->language->get('heading_title'));
    $this->load->model('account/brand');

    if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
        $brand_id = $this->model_account_brand->addBrand($this->request->post);
        $this->success();
    }

    $this->load->model('localisation/language');
    $data['languages'] = $this->model_localisation_language->getLanguages();


    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_form'] = !isset($this->request->get['brand_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
    $data['button_save'] = $this->language->get('button_save');

    //-- Description/Attribute
    $data['entry_name'] = $this->language->get('entry_name');
    $data['entry_description'] = $this->language->get('entry_description');

    $data['entry_image'] = $this->language->get('entry_image');
    $data['action'] = $this->url->link('addBrand', '', 'SSL');

    if (isset($this->error['warning'])) {
        $data['error_warning'] = $this->error['warning'];
    } else {
        $data['error_warning'] = '';
    }
    if (isset($this->error['name'])) {
        $data['error_name'] = $this->error['name'];
    } else {
        $data['error_name'] = array();
    }

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/brand_form.tpl', $data));
  }

  public function success(){
    $response =  "<br><div class='f17' style='font-size: 18px' >Thank you for adding your Brand! <br> <br> ";
    $response .= 'your brand is under review and will be approved shortly <br> <br>';
    $response .= 'In the meanwhile, you can still add your product without the brand.<br></div>';
    echo $response ;
    exit();
  }
}
