<?php

class ControllerDiscountlist extends Controller {

    private $error = array();
    private $data = array();

    public function index() {

        if (!$this->partner->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('discountlist', '', 'SSL');
            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }
        if ($this->config->get("config_language_id") == 2) {
            if ($this->language->get('code') == "ar") {
                $language_code = "en";
            } else {
                $language_code = "ar";
            }
            $url_data = $this->request->get;
            unset($url_data['_route_']);
            $route = $url_data['route'];
            unset($url_data['route']);
            $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            $this->response->redirect($this->url->link($route, $url, $this->request->server['HTTPS'], $language_code));
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->post['selected'])) {
            $this->data['selected'] = (array) $this->request->post['selected'];
        } else {
            $this->data['selected'] = array();
        }

        $this->load->model('account/customerpartner');

        $this->data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

        if (!$this->data['chkIsPartner']) {
            $this->response->redirect($this->url->link('account/login'));
        }

        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'css/sell.min.css');

        $this->language->load('account/sellerDiscount');

        $this->document->setTitle($this->language->get('heading_title_discountlist'));

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('dashboard'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title_discountlist'),
            'href' => $this->url->link('discountlist', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['insert'] = $this->url->link('sellerDiscounts', '', 'SSL');
        $this->data['delete'] = $this->url->link('discountlist/delete','', 'SSL');

        $discount_total = $this->model_account_customerpartner->getTotalSellerDiscounts();

        $results = $this->model_account_customerpartner->getSellerDiscounts();

        if (!empty($results)){

          foreach ($results as $key => $result) {


              $results[$key]['discount_id'] = $result['discount_id'];
              $results[$key]['name'] = $result['name'];
              $results[$key]['percentage'] = $result['percentage'];
              $results[$key]['date_start'] = ($result['date_start'] != '0000-00-00' ) ? date('Y-m-d',strtotime($result['date_start'])) : '';
              $results[$key]['date_end'] = ($result['date_end'] != '0000-00-00' ) ? date('Y-m-d',strtotime($result['date_end'])) : '';
              $results[$key]['action'] = $this->url->link('sellerDiscounts','&discount_id='.$result['discount_id'] , 'SSL');
          }
        }

        $this->data['discounts'] = $results;

        $this->data['heading_title'] = $this->language->get('heading_title_discountlist');

        $this->data['text_no_results'] = $this->language->get('text_no_results');
        $this->data['column_discount_id'] = $this->language->get('column_discount_id');
        $this->data['column_name'] = $this->language->get('column_name');
        $this->data['column_percentage'] = $this->language->get('column_percentage');
        $this->data['column_date_start'] = $this->language->get('column_date_start');
        $this->data['column_date_end'] = $this->language->get('column_date_end');
        $this->data['column_action'] = $this->language->get('column_action');
        $this->data['text_confirm'] = $this->language->get('text_confirm');
        $this->data['button_delete']  = $this->language->get('button_delete');
        $this->data['button_insert']  = $this->language->get('button_insert');

        if (isset($this->session->data['warning'])) {
            $this->data['error_warning'] = $this->session->data['warning'];
            unset($this->session->data['warning']);
        }else{
          $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $pagination = new Pagination();
        $pagination->total = $discount_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('marketplace_seller_product_list_limit');
        $pagination->url = $this->url->link('discountlist', '' . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->data['results'] = sprintf($this->language->get('text_pagination'), ($discount_total) ? (($page - 1) * $this->config->get('marketplace_seller_product_list_limit')) + 1 : 0, ((($page - 1) * $this->config->get('marketplace_seller_product_list_limit')) > ($discount_total - $this->config->get('marketplace_seller_product_list_limit'))) ? $discount_total : ((($page - 1) * $this->config->get('marketplace_seller_product_list_limit')) + $this->config->get('marketplace_seller_product_list_limit')), $discount_total, ceil($discount_total / $this->config->get('marketplace_seller_product_list_limit')));


        $this->data['back'] = $this->url->link('dashboard', '', 'SSL');

        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['column_right'] = $this->load->controller('common/column_right');
        $this->data['content_top'] = $this->load->controller('common/content_top');
        $this->data['content_bottom'] = $this->load->controller('common/content_bottom');
        $this->data['footer'] = $this->load->controller('common/footer');
        $this->data['header'] = $this->load->controller('common/header', $this->data);

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/customerpartner/discountlist.tpl', $this->data));
    }

    public function delete() {
        $this->load->language('account/customerpartner/discountlist');
        $this->language->load('account/sellerDiscount');

        $this->document->setTitle($this->language->get('heading_title_discountlist'));

        $this->load->model('account/customerpartner');

        if (isset($this->request->post['selected'])) {

            foreach ($this->request->post['selected'] as $discount_id) {
                $this->model_account_customerpartner->deleteDiscount($discount_id);
            }
            $this->session->data['success'] = $this->language->get('text_success_modify');

            $this->response->redirect($this->url->link('discountlist', '', 'SSL'));
        }

        $this->index();
    }
}
