<?php

################################################################################################
# Marketplace RMA Opencart 2.x.x.x From Webkul  http://webkul.com 	#
################################################################################################

class ControllerWkmprmamanage extends Controller {

    private $error = array();
    private $data = array();

    private $status = array();
    
    protected function setValues(){
        $statuses = $this->db->query(" SELECT * FROM rma_status WHERE partner='yes' AND language_id = '".(int)$this->config->get('config_language_id')."' ");
        if($statuses->num_rows) {
            foreach ($statuses->rows as $row) {
                $this->status[$row['rma_status_id']] = $row;
            }
        }
    }

    public function index() {
        
        $this->setValues();

        if (!$this->partner->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('wk_mprma_manage', '', 'SSL');
            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }
        /*if ($this->config->get("config_language_id") == 2) {
            if ($this->language->get('code') == "ar") {
                $language_code = "en";
            } else {
                $language_code = "ar";
            }
            $url_data = $this->request->get;
            unset($url_data['_route_']);
            $route = $url_data['route'];
            unset($url_data['route']);
            $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            $this->response->redirect($this->url->link($route, $url, $this->request->server['HTTPS'], $language_code));
        }*/

        $this->load->model('account/customerpartner');

        $this->data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

        $this->load->model('account/wk_mprma');

        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/moment.min.js');
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'css/sell.min.css');

        $this->language->load('account/customerpartner/wk_mprma_manage');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['button_invoice'] = $this->language->get('button_invoice');
        $this->data['button_delete'] = $this->language->get('button_delete');
        $this->data['text_confirm'] = $this->language->get('text_confirm');
        $this->data['button_filter'] = $this->language->get('button_filter');
        $this->data['button_clrfilter'] = $this->language->get('button_clrfilter');
        $this->data['text_list'] = $this->language->get('text_list');
        $this->data['text_no_recored'] = $this->language->get('text_no_recored');
        $this->data['text_confirm'] = $this->language->get('text_confirm');

        //user
        $this->data['wk_rma_admin_id'] = $this->language->get('wk_rma_admin_id');
        $this->data['wk_rma_admin_cid'] = $this->language->get('wk_rma_admin_cid');
        $this->data['wk_rma_admin_cname'] = $this->language->get('wk_rma_admin_cname');
        $this->data['wk_rma_admin_oid'] = $this->language->get('wk_rma_admin_oid');
        $this->data['wk_rma_admin_reason'] = $this->language->get('wk_rma_admin_reason');
        $this->data['wk_rma_admin_date'] = $this->language->get('wk_rma_admin_date');
        $this->data['wk_rma_admin_rmastatus'] = $this->language->get('wk_rma_admin_rmastatus');
        $this->data['wk_rma_admin_adminstatus'] = $this->language->get('wk_rma_admin_adminstatus');
        $this->data['wk_rma_admin_customerstatus'] = $this->language->get('wk_rma_admin_customerstatus');
        $this->data['wk_rma_admin_product'] = $this->language->get('wk_rma_admin_product');

        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }

        if (isset($this->request->get['filter_product'])) {
            $filter_product = $this->request->get['filter_product'];
        } else {
            $filter_product = null;
        }

        if (isset($this->request->get['filter_order'])) {
            $filter_order = $this->commonfunctions->getOrderNumber($this->request->get['filter_order']);
        } else {
            $filter_order = null;
        }

        if (isset($this->request->get['filter_rma_status'])) {
            $filter_rma_status = $this->request->get['filter_rma_status'];
        } else {
            $filter_rma_status = null;
        }

        if (isset($this->request->get['filter_date'])) {
            $filter_date = $this->request->get['filter_date'];
        } else {
            $filter_date = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data = array(
            'filter_name' => $filter_name,
            'filter_product' => $filter_product,
            'filter_order' => $filter_order,
            'filter_rma_status' => $filter_rma_status,
            'filter_date' => $filter_date,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit')
        );

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_product'])) {
            $url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_order'])) {
            $url .= '&filter_order=' . $this->request->get['filter_order'];
        }

        if (isset($this->request->get['filter_rma_status'])) {
            $url .= '&filter_rma_status=' . $this->request->get['filter_rma_status'];
        }

        if (isset($this->request->get['filter_date'])) {
            $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        $this->data['sort_name'] = $this->url->link('wk_mprma_manage', '' . '&sort=c.firstname' . $url, 'SSL');
        $this->data['sort_product'] = $this->url->link('wk_mprma_manage', '' . '&sort=wro.id' . $url, 'SSL');
        $this->data['sort_order'] = $this->url->link('wk_mprma_manage', '' . '&sort=wro.order_id' . $url, 'SSL');
        $this->data['sort_reason'] = $this->url->link('wk_mprma_manage', '' . '&sort=wrr.id' . $url, 'SSL');
        $this->data['sort_rma_status'] = $this->url->link('wk_mprma_manage', '' . '&sort=wrs.order_status_id' . $url, 'SSL');
        $this->data['sort_date'] = $this->url->link('wk_mprma_manage', '' . '&sort=wro.date' . $url, 'SSL');

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('dashboard'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title_details'),
            'href' => $this->url->link('wk_mprma_manage', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $this->data['banners'] = false;

        $this->data['current'] = $this->url->link('wk_mprma_manage', '', 'SSL');

        $this->data['back'] = $this->url->link('account/account', '', 'SSL');


        $product_total = $this->model_account_wk_mprma->viewtotalentry($data);

        $results = $this->model_account_wk_mprma->viewtotal($data);

        $this->data['delete'] = $this->url->link('wk_mprma_manage/delete', '', 'SSL');

        $statuses = $this->db->query(" SELECT * FROM rma_status WHERE language_id = '".(int)$this->config->get('config_language_id')."' ");
        if($statuses->num_rows) {
            foreach ($statuses->rows as $row) {
                $this->status[$row['rma_status_id']] = $row;
            }
        }
        $this->data['rma_sta'] = $this->status;

        $this->data['result_rmaadmin'] = array();

        if ($results) {
            foreach ($results as $result) {
                $action = array();

                $action[] = array(
                    'text' => $this->language->get('text_edit'),
                    'href' => $this->url->link('wk_mprma_manage/getForm', '&return_id=' . $result['id'], 'SSL')
                );

                $result_products = $this->model_account_wk_mprma->viewProducts($result['id'], $filter_product);

                $product = $reason = '';
                foreach ($result_products as $products) {
                    $product .= $products['name'] . ' <br/> ';
                    $reason .= $products['reason'] . ' <br/> ';
                }

                if (!empty($filter_product) && strpos(strtolower($product), strtolower($filter_product)) == false) {
                    $product_total--;
                    continue;
                }

                $this->data['result_rmaadmin'][] = array(
                    'selected' => False,
                    'id' => $result['id'],
                    'name' => $result['name'],
                    'product' => $product,
                    'oid' => $this->commonfunctions->convertOrderNumber($result['order_id']),
                    'rmastatus' => $result['rma_status'],
                    'color' => $result['color'],
                    'reason' => $reason,
                    'date' => date($this->language->get('date_format_short'), strtotime($result['date'])),
                    'action' => $action
                );
            }
        }

        $this->data['reasons'] = $this->model_account_wk_mprma->getCustomerReason();


        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_product'])) {
            $url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_order'])) {
            $url .= '&filter_order=' . $this->request->get['filter_order'];
        }

        if (isset($this->request->get['filter_rma_status'])) {
            $url .= '&filter_rma_status=' . $this->request->get['filter_rma_status'];
        }

        if (isset($this->request->get['filter_date'])) {
            $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $this->data['invoice'] = $this->url->link('wk_mprma_manage/invoice', '', 'SSL');

        $pagination = new Pagination();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_product_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('productlist', '' . $url . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();
        $this->data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($product_total - 10)) ? $product_total : ((($page - 1) * 10) + 10), $product_total, ceil($product_total / 10));

        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        $this->data['filter_name'] = $filter_name;
        $this->data['filter_product'] = $filter_product;
        $this->data['filter_order'] = isset($this->request->get['filter_order']) ? $this->request->get['filter_order'] : '' ;
        $this->data['filter_rma_status'] = $filter_rma_status;
        $this->data['filter_date'] = $filter_date;

        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['column_right'] = $this->load->controller('common/column_right');
        $this->data['content_top'] = $this->load->controller('common/content_top');
        $this->data['content_bottom'] = $this->load->controller('common/content_bottom');
        $this->data['footer'] = $this->load->controller('common/footer', $this->data);
        $this->data['header'] = $this->load->controller('common/header', $this->data);

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/customerpartner/wk_mprma_manage.tpl', $this->data));
    }

    public function getForm() {

        $this->setValues();
        
        if (!$this->partner->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('wk_mprma_manage', '', 'SSL');
            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        /*if ($this->config->get("config_language_id") == 2) {
            if ($this->language->get('code') == "ar") {
                $language_code = "en";
            } else {
                $language_code = "ar";
            }
            $url_data = $this->request->get;
            unset($url_data['_route_']);
            $route = $url_data['route'];
            unset($url_data['route']);
            $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            $this->response->redirect($this->url->link($route, $url, $this->request->server['HTTPS'], $language_code));
        }*/

        $this->data['rma_sta'] = $this->status;

        $this->load->model('account/customerpartner');

        $this->data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

        $this->language->load('account/customerpartner/wk_mprma_manage');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('account/wk_mprma');

        $this->load->model('tool/image');
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/moment.min.js');
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $file_name = '';
            if ($this->request->files['up_file']['name']) {
                $file = $this->request->files['up_file'];
                $file_name = $file['name'];
                
                $result = $this->model_account_wk_mprma->getRmaOrderid($this->request->post['rma_id']);

                if ($result && $result['images']) {
                    $target = DIR_IMAGE . 'rma/' . $result['images'] . "/files/" . $file['name'];
                    
                    @move_uploaded_file($file['tmp_name'], $target);
                }
            }
            
            $this->model_account_wk_mprma->updateAdminStatus($this->request->post['wk_rma_admin_msg'], $this->request->post['wk_rma_admin_adminstatus'], $this->request->post['rma_id'], $file_name, $this->request->post['customer_id']);
            
            $this->session->data['success'] = $this->language->get('text_success');
            
            $this->response->redirect($this->url->link('wk_mprma_manage/getForm&return_id=' . $this->request->post['rma_id'], '', 'SSL'));
        }

        $oid = 0;

        if (isset($this->request->get['return_id'])) {
            $oid = $this->request->get['return_id'];
        }

        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }

        if (isset($this->request->get['filter_message'])) {
            $filter_message = $this->request->get['filter_message'];
        } else {
            $filter_message = null;
        }

        if (isset($this->request->get['filter_date'])) {
            $filter_date = $this->request->get['filter_date'];
        } else {
            $filter_date = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data = array(
            'filter_name' => $filter_name,
            'filter_message' => $filter_message,
            'filter_date' => $filter_date,
            'filter_id' => $oid,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_message'])) {
            $url .= '&filter_message=' . urlencode(html_entity_decode($this->request->get['filter_message'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_date'])) {
            $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        $url .= '&return_id=' . $oid;

        $this->data['sort_name'] = $this->url->link('wk_mprma_manage/getForm', '' . '&sort=wrm.writer' . $url, 'SSL');
        $this->data['sort_message'] = $this->url->link('wk_mprma_manage/getForm', '' . '&sort=wrm.message' . $url, 'SSL');
        $this->data['sort_date'] = $this->url->link('wk_mprma_manage/getForm', '' . $url, 'SSL');

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('dashboard', '' . $url, 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('wk_mprma_manage', '' . $url, 'SSL'),
            'separator' => ' :: '
        );

        $this->data['banners'] = false;

        $this->data['heading_title'] = $this->language->get('heading_title_details');
        $this->data['entry_name'] = $this->language->get('entry_name');
        $this->data['button_insert'] = $this->language->get('button_insert');
        $this->data['button_back'] = $this->language->get('button_back');
        $this->data['button_table'] = $this->language->get('button_table');
        $this->data['button_alert'] = $this->language->get('button_alert');
        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_filter'] = $this->language->get('button_filter');
        $this->data['button_invoice'] = $this->language->get('button_invoice');
        $this->data['text_form'] = $this->language->get('text_form') . '# ' . $oid;
        //user
        $this->data['wk_rma_admin_id'] = $this->language->get('wk_rma_admin_id');
        $this->data['wk_rma_admin_cid'] = $this->language->get('wk_rma_admin_cid');
        $this->data['wk_rma_admin_cname'] = $this->language->get('wk_rma_admin_cname');
        $this->data['wk_rma_admin_oid'] = $this->language->get('wk_rma_admin_oid');
        $this->data['wk_rma_admin_reason'] = $this->language->get('wk_rma_admin_reason');
        $this->data['wk_rma_admin_date'] = $this->language->get('wk_rma_admin_date');
        $this->data['wk_rma_admin_rmastatus'] = $this->language->get('wk_rma_admin_rmastatus');
        $this->data['wk_rma_sellerStatus'] = $this->language->get('wk_rma_sellerStatus');
        $this->data['wk_rma_admin_customerstatus'] = $this->language->get('wk_rma_admin_customerstatus');
        $this->data['wk_rma_admin_authno'] = $this->language->get('wk_rma_admin_authno');
        $this->data['wk_rma_admin_add_info'] = $this->language->get('wk_rma_admin_add_info');
        $this->data['wk_rma_admin_msg'] = $this->language->get('wk_rma_admin_msg');
        $this->data['wk_rma_admin_basic'] = $this->language->get('wk_rma_admin_basic');
        $this->data['wk_rma_admin_msg_tab'] = $this->language->get('wk_rma_admin_msg_tab');
        $this->data['wk_rma_admin_images'] = $this->language->get('wk_rma_admin_images');
        $this->data['text_no_option'] = $this->language->get('text_no_option');
        $this->data['wk_rma_admin_product'] = $this->language->get('wk_rma_admin_product');
        $this->data['button_clrfilter'] = $this->language->get('button_clrfilter');
        $this->data['text_no_recored'] = $this->language->get('text_no_recored');
        $this->data['text_quantity'] = $this->language->get('text_quantity');
        $this->data['text_download'] = $this->language->get('text_download');
        $this->data['button_upload'] = $this->language->get('button_upload');
        $this->data['wk_viewrma_msg'] = $this->language->get('wk_viewrma_msg');
        $this->data['wk_rma_admin_return'] = $this->language->get('wk_rma_admin_return');
        $this->data['text_shipping_lable'] = $this->language->get('text_shipping_lable');
        $this->data['text_shipping_info'] = $this->language->get('text_shipping_info');
        $this->data['wk_viewrma_shipping_label'] = $this->language->get('wk_viewrma_shipping_label');

        $this->data['text_lable_image'] = $this->language->get('text_lable_image');
        $this->data['text_lable_name'] = $this->language->get('text_lable_name');

        $this->data['text_select'] = $this->language->get('text_select');
        $this->data['text_decline'] = $this->language->get('text_decline');
        $this->data['text_return'] = $this->language->get('text_return');
        $this->data['text_complete'] = $this->language->get('text_complete');
        $this->data['help_sellerStatus'] = $this->language->get('help_sellerStatus');

        $check_seller_products = $this->model_account_wk_mprma->check_if_product_related_to_seller($oid, $this->partner->getId());
        if ($check_seller_products) {
            $result = $this->model_account_wk_mprma->getRmaOrderid($oid);

            $results_message = $this->model_account_wk_mprma->viewtotalMessageBy($data);
            $results_message_total = $this->model_account_wk_mprma->viewtotalNoMessageBy($data);

            $this->data['results_message'] = $results_message;

            $attachmentLinkDir = DIR_IMAGE . 'rma/' . $result['images'] . '/files/';

            $this->data['attachmentLink'] = HTTP_SERVER . '../image/rma/' . $result['images'] . '/files/';

            foreach ($results_message as $key => $value) {
                if (!file_exists($attachmentLinkDir . $value['attachment'])) {
                    $this->data['results_message'][$key]['attachment'] = '';
                }
            }

            $this->data['save'] = $this->url->link('wk_mprma_manage/getForm', 'return_id='.$oid, 'SSL');
            $this->data['invoice'] = $this->url->link('wk_mprma_manage/invoice&rma_id=' . $oid, '', 'SSL');
            $this->data['back'] = $this->url->link('wk_mprma_manage', '', 'SSL');
            $this->data['mpsavelabel'] = $this->url->link('wk_mprma_manage/mpsaveLabel&return_id=' . $oid, '', 'SSL');

            $this->data['vid'] = $oid;

            $this->data['result_rmaadmin'] = $this->data['result_rmaadmin_images'] = $this->data['result_products'] = array();

            $path = 'rma/' . $result['images'] . '/files/';

            if ($result) {

                $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/magnific/jquery.magnific-popup.min.js');
                $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/magnific/magnific-popup.css');

                $action = array();

                $action[] = array(
                    'text' => $this->language->get('text_edit'),
                    'href' => $this->url->link('catalog/wk_rma_admin', '&return_id=' . $oid, 'SSL')
                );

                $customerDetails = $this->model_account_wk_mprma->viewCustomerDetails($result['order_id']);

                if ($customerDetails) {
                    
                    $seller_name = $this->db->query("SELECT c.firstname FROM " . DB_PREFIX . "customerpartner_to_customer cp2c LEFT JOIN " . DB_PREFIX . "customer c ON (cp2c.customer_id = c.customer_id) WHERE cp2c.is_partner = '1' AND cp2c.customer_id = '" . (int) $this->partner->getId() . "'")->row;

                    $img_path = DIR_IMAGE . 'rma/' . $seller_name['firstname'] . '/';
                    $resize_path = 'rma/' . $seller_name['firstname'] . '/';
                    if ($result['shipping_label']) {
                        if (!file_exists($img_path . $result['shipping_label'])) {
                            $result['shipping_label'] = '';
                        } else
                            $result['shipping_label'] = $this->model_tool_image->resize($resize_path . $result['shipping_label'], 300, 300, 'h');
                    }

                    $this->data['result_rmaadmin'] = array(
                        'selected' => False,
                        'id' => $oid,
                        'cust_id' => $customerDetails['id'],
                        'customer_status' => $result['customer_st'],
                        'name' => $customerDetails['firstname'] . ' ' . $customerDetails['lastname'],
                        'oid' => '# ' . $this->commonfunctions->convertOrderNumber($result['order_id']),
                        'date' => date($this->language->get('date_format_short'), strtotime($result['date'])),
                        'color' => $result['color'],
                        'rmastatus' => $result['rma_status'],
                        'add_info' => $result['add_info'],
                        'auth_no' => $result['rma_auth_no'],
                        'shipping_label' => $result['shipping_label'],
                        'action' => $action
                    );
                    // $this->data['result_products'] = $this->model_account_wk_mprma->viewProducts($oid);
                    $rma_products = $this->model_account_wk_mprma->viewProducts($oid);
                    $product_total = $this->model_account_wk_mprma->viewtotalentry($data);
                    $this->load->model('account/rma/rma');
                    foreach ($rma_products as $key => $value) {
                        $option_data = array();
                        $options = $this->model_account_rma_rma->getOrderOptions($value['order_id'], $value['order_product_id']);

                        foreach ($options as $option) {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $option['value'],
                                'type' => $option['type']
                            );
                        }

                        $this->data['result_products'][] = array(
                            'name' => $value['name'],
                            'quantity' => $value['quantity'],
                            'reason' => $value['reason'],
                            'order_product_id' => $value['order_product_id'],
                            'product_id' => $value['product_id'],
                            'order_id' => $value['order_id'],
                            'option' => $option_data,
                        );
                    }

                    if ($result['images']) {
                        $dir = DIR_IMAGE . 'rma/' . $result['images'] . '/files/';
                        if (file_exists($dir)) {
                            if ($dh = opendir($dir)) {
                                while (($file = readdir($dh)) !== false) {
                                    if (!is_dir($file)) {
                                        $this->data['result_rmaadmin_images'][] = array(
                                            'image' => $this->model_tool_image->resize($path . $file, 500, 500, 'h'),
                                            'resize' => $this->model_tool_image->resize($path . $file, 125, 125)
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $seller_name = $this->db->query("SELECT c.firstname FROM " . DB_PREFIX . "customerpartner_to_customer cp2c LEFT JOIN " . DB_PREFIX . "customer c ON (cp2c.customer_id = c.customer_id) WHERE cp2c.is_partner = '1' AND cp2c.customer_id = '" . (int) $this->partner->getId() . "'")->row;

            $target = DIR_IMAGE . 'rma/' . $seller_name['firstname'] . 'all';

            $this->data['mpshipping_label_folder'] = array();
            if (file_exists($target)) {
                $opentarget = opendir($target);
                while ($image = readdir($opentarget)) {
                    if ($image != '.' && $image != '..') {
                        $this->data['mpshipping_label_folder'][] = array('image' => $this->model_tool_image->resize('rma/' . $seller_name['firstname'] . 'all' . '/' . $image, 50, 50),
                            'name' => $image);
                    }
                }
            }

            if (isset($this->session->data['error_warning'])) {
                $this->error['warning'] = $this->session->data['error_warning'];
                unset($this->session->data['error_warning']);
            }

            if (isset($this->error['warning'])) {
                $this->data['error_warning'] = $this->error['warning'];
            } else {
                $this->data['error_warning'] = '';
            }

            if (isset($this->session->data['success'])) {
                $this->data['success'] = $this->session->data['success'];
                unset($this->session->data['success']);
            } else {
                $this->data['success'] = '';
            }

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_message'])) {
                $url .= '&filter_message=' . urlencode(html_entity_decode($this->request->get['filter_message'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_date'])) {
                $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            $url .= '&return_id=' . $oid;

            $pagination = new Pagination();
            $pagination->total = $product_total;
            $pagination->page = $page;
            $pagination->limit = $this->config->get('config_product_limit');
            $pagination->text = $this->language->get('text_pagination');
            $pagination->url = $this->url->link('productlist', '' . $url . '&page={page}', 'SSL');

            $this->data['pagination'] = $pagination->render();
            $this->data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($product_total - 10)) ? $product_total : ((($page - 1) * 10) + 10), $product_total, ceil($product_total / 10));

            $this->data['sort'] = $sort;
            $this->data['order'] = $order;
            $this->data['filter_name'] = $filter_name;
            $this->data['filter_message'] = $filter_message;
            $this->data['filter_date'] = $filter_date;

            $this->data['column_left'] = $this->load->controller('common/column_left');
            $this->data['column_right'] = $this->load->controller('common/column_right');
            $this->data['content_top'] = $this->load->controller('common/content_top');
            $this->data['content_bottom'] = $this->load->controller('common/content_bottom');
            $this->data['footer'] = $this->load->controller('common/footer');
            $this->data['header'] = $this->load->controller('common/header', $this->data);

            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/customerpartner/wk_mprma_manage_details.tpl', $this->data));
        } else {
            $this->load->language("account/customerpartner/wk_mprma_manage");
            $this->data['heading_title'] = $this->language->get('text_error');
            $this->data['text_error'] = $this->language->get('text_error');
            $this->data['button_continue'] = $this->language->get('button_continue');
            $this->data['heading_title'] = $this->language->get('text_error');
            $this->data['continue'] = $this->url->link('common/home');

            $this->data['column_left'] = $this->load->controller('common/column_left');
            $this->data['column_right'] = $this->load->controller('common/column_right');
            $this->data['content_top'] = $this->load->controller('common/content_top');
            $this->data['content_bottom'] = $this->load->controller('common/content_bottom');
            $this->data['footer'] = $this->load->controller('common/footer');
            $this->data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $this->data));
        }
    }

    public function invoice() {

        if (!$this->partner->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('wk_mprma_manage', '', 'SSL');
            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->language->load('account/rma/viewrma');

        $this->load->model('account/wk_mprma');
        $this->load->model('tool/image');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['text_order_details'] = $this->language->get('text_order_details');
        $this->data['wk_viewrma_orderid'] = $this->language->get('wk_viewrma_orderid');
        $this->data['wk_viewrma_status'] = $this->language->get('wk_viewrma_status');
        $this->data['wk_viewrma_rma_tatus'] = $this->language->get('wk_viewrma_rma_tatus');
        $this->data['wk_viewrma_authno'] = $this->language->get('wk_viewrma_authno');
        $this->data['wk_viewrma_add_info'] = $this->language->get('wk_viewrma_add_info');
        $this->data['wk_viewrma_image'] = $this->language->get('wk_viewrma_image');
        $this->data['wk_viewrma_item_req'] = $this->language->get('wk_viewrma_item_req');
        $this->data['wk_viewrma_pname'] = $this->language->get('wk_viewrma_pname');
        $this->data['wk_viewrma_model'] = $this->language->get('wk_viewrma_model');
        $this->data['wk_viewrma_price'] = $this->language->get('wk_viewrma_price');
        $this->data['wk_viewrma_reason'] = $this->language->get('wk_viewrma_reason');
        $this->data['wk_viewrma_qty'] = $this->language->get('wk_viewrma_qty');
        $this->data['wk_viewrma_ret_qty'] = $this->language->get('wk_viewrma_ret_qty');
        $this->data['wk_viewrma_subtotal'] = $this->language->get('wk_viewrma_subtotal');
        $this->data['text_productReturn'] = $this->language->get('text_productReturn');
        $this->data['text_messages'] = $this->language->get('text_messages');
        $this->data['text_download'] = $this->language->get('text_download');
        $this->data['text_productReturn'] = $this->language->get('text_productReturn');
        $this->data['wk_mprma_manage_return'] = $this->language->get('wk_mprma_manage_return');
        $this->data['wk_mprma_manage_return_info'] = $this->language->get('wk_mprma_manage_return_info');


        $rmaId = $this->request->get['rma_id'];

        $this->data['viewrma'] = array();

        $rma_details = $this->model_account_wk_mprma->getRmaOrderid($rmaId);

        if ($rma_details) {

            $this->data['prodetails'] = array();

            $productDetails = $this->model_account_wk_mprma->getProductRma($rmaId, $rma_details['order_id']);
            $this->load->model('account/rma/rma');
            if ($productDetails) {
                foreach ($productDetails as $products) {

                    $option_data = array();
                    $options = $this->model_account_rma_rma->getOrderOptions($rma_details['order_id'], $products['order_product_id']);

                    foreach ($options as $option) {
                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $option['value'],
                            'type' => $option['type']
                        );
                    }

                    $this->data['prodetails'][] = array('price' => $this->currency->format(($products['price'] + $products['tax']), $this->currency->getCode()),
                        'total' => $this->currency->format(($products['price'] + $products['tax']) * $products['returned'], $this->currency->getCode()),
                        'name' => $products['name'],
                        'link' => $this->url->link('product/product&product_id=' . $products['product_id'], '', 'SSL'),
                        'ordered' => $products['ordered'],
                        'reason' => $products['reason'],
                        'model' => $products['model'],
                        'returned' => $products['returned'],
                        'option' => $option_data,
                    );
                }
            }

            $images = array();

            if ($rma_details['images']) {
                $path = 'rma/' . $rma_details['images'] . '/';
                $dir = DIR_IMAGE . 'rma/' . $rma_details['images'] . '/';

                if (file_exists($dir)) {
                    if ($dh = opendir($dir)) {

                        while (($file = readdir($dh)) !== false) {
                            if (!is_dir($file)) {
                                $images [] = array('resize' => $this->model_tool_image->resize($path . $file, 125, 125),
                                    'image' => $this->model_tool_image->resize($path . $file, 500, 500),
                                );
                            }
                        }
                    }
                }
            }

            $this->data['viewrma'] = array(
                'id' => $rmaId,
                'order_id' => $this->commonfunctions->convertOrderNumber($rma_details['order_id']),
                'return_qty' => $this->url->link('wk_mprma_manage/returnQty&return_id=' . $rmaId, '', 'SSL'),
                'order_url' => $this->url->link('orderinfo&order_id=' . $this->commonfunctions->convertOrderNumber($rma_details['order_id'])),
                'images' => $images,
                'add_info' => $rma_details['add_info'],
                'date' => date($this->language->get('date_format_short'), strtotime($rma_details['date'])),
                'color' => $rma_details['color'],
                'rma_status' => $rma_details['rma_status'],
                'customer_status' => $rma_details['customer_status_id'],
                'shipping_label' => $rma_details['shipping_label'],
                'rma_auth_no' => $rma_details['rma_auth_no']
            );
        }

        $this->data['direction'] = 'ltr';
        $this->data['lang'] = 'en';
        $this->data['base'] = HTTP_SERVER;

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/customerpartner/wk_rma_invoice.tpl', $this->data));
    }

    public function mpsaveLabel() {

        if (!$this->partner->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/customerpartner/wk_mprma_manage', '', 'SSL');
            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->language->load('account/rma/viewrma');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('account/wk_mprma');
        if (isset($this->request->post['selected']) || (isset($this->request->files['mpshipping_label']['name']) && $this->request->files['mpshipping_label']['name'])) {

            $file = $this->request->files['mpshipping_label'];

            if (isset($this->request->get['id']) && $this->validateForm()) {

                $result = $this->model_account_wk_mprma->getRmaOrderid($this->request->get['id']);

                $file_name = '';
                $path = DIR_IMAGE . 'rma/';
                $seller_name = $this->db->query("SELECT c.firstname, c.lastname FROM " . DB_PREFIX . "customerpartner_to_customer cp2c LEFT JOIN " . DB_PREFIX . "customer c ON (cp2c.customer_id = c.customer_id) WHERE cp2c.is_partner = '1' AND cp2c.customer_id = '" . (int) $this->partner->getId() . "'")->row;

                if (isset($seller_name['firstname']) && $seller_name['firstname']) {
                    $path = $path . $seller_name['firstname'];
                    $path_all = DIR_IMAGE . 'rma/' . $seller_name['firstname'] . 'all';
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                        mkdir($path_all, 0777, true);
                    }
                }

                if ($result) {
                    $file_name = $file['name'] ? $file['name'] : $this->request->post['selected'];

                    $target = $path . '/' . $file_name;

                    if ($file['name']) {
                        if ($result && $result['images']) {
                            move_uploaded_file($file['tmp_name'], $target);
                            //for label list
                            copy($target, $path_all . '/' . $file_name);
                            // @mkdir(DIR_IMAGE.'rma/files',0755,true);
                            // move_uploaded_file($file['tmp_name'], DIR_IMAGE.'rma/files/'.$file_name);
                        }
                    } elseif ($this->request->post['selected']) {
                        if (file_exists(DIR_IMAGE . 'rma/files/' . $this->request->post['selected'])) {
                            copy(DIR_IMAGE . 'rma/files/' . $this->request->post['selected'], $target);
                        }
                    }
                }

                if ($file_name) {
                    $this->model_account_wk_mprma->addLabel($this->request->get['id'], $file_name, $result['images'], $seller_name['firstname']);
                    $this->session->data['success'] = $this->language->get('text_success');
                } else {
                    $this->session->data['error_warning'] = $this->language->get('error_label');
                    $this->response->redirect($this->url->link('wk_mprma_manage/getForm&return_id=' . $this->request->get['id'], '' . $url, 'SSL'));
                }

                $url = '';

                if (isset($this->request->get['page'])) {
                    $url .= '&page=' . $this->request->get['page'];
                }

                $this->response->redirect($this->url->link('wk_mprma_manage', '' . $url, 'SSL'));
            }
        } else {//chk
            $this->response->redirect($this->url->link('wk_mprma_manage/getForm&return_id=' . $this->request->get['id'], '', 'SSL'));
        }

        $this->index();
    }

    public function returnQty() {

        if (!$this->partner->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('wk_mprma_manage', '', 'SSL');
            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->language->load('account/rma/viewrma');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('account/wk_mprma');

        if (isset($this->request->get['id']) && $this->validateForm()) {

            $this->model_account_wk_mprma->returnQty($this->request->get['id']);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('wk_mprma_manage', '' . $url, 'SSL'));
        }

        $this->index();
    }

    public function delete() {
        $this->index();
        return false;
        if (!$this->partner->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('wk_mprma_manage', '', 'SSL');
            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->language->load('accout/customerpartner/wk_mprma_manage');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('account/wk_mprma');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $id) {
                $this->model_account_wk_mprma->deleteentry($id);
            }

            $this->session->data['success'] = $this->language->get('text_success_deleted');

            $url = '';

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('wk_mprma_manage', '' . $url, 'SSL'));
        }
        $this->index();
    }

    private function validateForm() {


        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }
        
        if(!isset($this->request->get['return_id']) || $this->request->get['return_id'] == ''){
            $this->error['warning'] = $this->language->get('error_ticket_id');
        }
        
        if (!isset($this->request->post['wk_rma_admin_msg']) || utf8_strlen($this->request->post['wk_rma_admin_msg']) < 3){
            $this->error['warning'] = $this->language->get('error_message');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
    
    private function validateDelete() {


        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }
        
        if(!isset($this->request->post['return_id']) || $this->request->post['return_id'] == ''){
            $this->error['warning'] = $this->language->get('error_warning');
        }
        
        if (!isset($this->request->post['wk_rma_admin_msg']) || utf8_strlen($this->request->post['wk_rma_admin_msg']) < 3){
            $this->error['warning'] = $this->language->get('error_message');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}
