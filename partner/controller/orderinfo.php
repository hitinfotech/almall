<?php

class ControllerOrderinfo extends Controller {

    private $data = array();

    public function index() {

        if (!$this->partner->isLogged()) {
            if (isset($this->request->get['order_id'])) {
                $this->session->data['redirect'] = $this->url->link('orderinfo&order_id=' . $this->request->get['order_id'], '', 'SSL');
            }
            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        /* if ($this->config->get("config_language_id") == 2) {
          if ($this->language->get('code') == "ar") {
          $language_code = "en";
          } else {
          $language_code = "ar";
          }
          $url_data = $this->request->get;
          unset($url_data['_route_']);
          $route = $url_data['route'];
          unset($url_data['route']);
          $url = '&' . urldecode(http_build_query($url_data, '', '&'));
          $this->response->redirect($this->url->link($route, $url, $this->request->server['HTTPS'], $language_code));
          } */

        $this->load->model('account/customerpartner');

        $data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();

        if (!$data['chkIsPartner'])
            $this->response->redirect($this->url->link('account/login'));

        $this->language->load('account/customerpartner/orderinfo');

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->commonfunctions->getOrderNumber($this->request->get['order_id']);
        } else {
            $order_id = 0;
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

            if ($order_id) {

                if (isset($this->request->post['tracking'])) {
                    $this->model_account_customerpartner->addOdrTracking($order_id, $this->request->post['tracking']);
                    $this->session->data['success'] = $this->language->get('text_success');
                }

                $this->response->redirect($this->url->link('orderinfo&order_id=' . $order_id, '', 'SSL'));
            }
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['order_id'] = $this->request->get['order_id'];

        $this->load->model('account/order');

        $order_info = $this->model_account_customerpartner->getOrder($order_id);

        $this->document->setTitle($this->language->get('text_order'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('dashboard'),
            'separator' => false
        );

        $url = '';

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('orderlist', $url, 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_order'),
            'href' => $this->url->link('orderinfo', 'order_id=' . $order_id . $url, 'SSL'),
            'separator' => $this->language->get('text_separator')
        );

        $data['banners'] = false;

        $data['heading_title'] = $this->language->get('text_order');
        $data['error_page_order'] = $this->language->get('error_page_order');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_payment_address'] = $this->language->get('text_payment_address');
        $data['text_history'] = $this->language->get('text_history');
        $data['text_comment'] = $this->language->get('text_comment');
        $data['text_wait'] = $this->language->get('text_wait');
        $data['text_not_paid'] = $this->language->get('text_not_paid');
        $data['text_paid'] = $this->language->get('text_paid');
        $data['package'] = $this->language->get('package');
        $data['text_order_status'] = $this->language->get('text_order_status');

        $data['column_tracking_no'] = $this->language->get('column_tracking_no');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_sku'] = $this->language->get('column_sku');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_comment'] = $this->language->get('column_comment');
        $data['column_transaction_status'] = $this->language->get('column_transaction_status');
        // $data['column_cancel_order'] = $this->language->get('column_cancel_order');
        $data['column_seller_order_status'] = $this->language->get('column_seller_order_status');

        $data['button_invoice'] = $this->language->get('button_invoice');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_back'] = $this->language->get('button_back');
        $data['button_submit'] = $this->language->get('button_submit');
        $data['button_print'] = $this->language->get('button_print');

        $data['history_info'] = $this->language->get('history_info');
        $data['order_status_info'] = $this->language->get('order_status_info');
        $data['order_status_error'] = $this->language->get('order_status_error');

        $data['entry_order_status'] = $this->language->get('entry_order_status');
        $data['entry_notify'] = $this->language->get('entry_notify');
        $data['entry_comment'] = $this->language->get('entry_comment');
        $data['entry_notifyadmin'] = $this->language->get('entry_notifyadmin');

        $data['errorPage'] = false;

        if ($this->config->get('marketplace_available_order_status')) {
            $data['marketplace_available_order_status'] = $this->config->get('marketplace_available_order_status');
            $data['marketplace_order_status_sequence'] = $this->config->get('marketplace_order_status_sequence');
        }

        if ($this->config->get('marketplace_cancel_order_status') && $this->config->get('marketplace_available_order_status')) {

            $data['marketplace_cancel_order_status'] = $this->config->get('marketplace_cancel_order_status');

            $cancel_order_statusId_key_available = array_search($this->config->get('marketplace_cancel_order_status'), $data['marketplace_available_order_status'], true);

            if ($cancel_order_statusId_key_available === 0 || $cancel_order_statusId_key_available) {

                unset($data['marketplace_available_order_status'][$cancel_order_statusId_key_available]);
                unset($data['marketplace_order_status_sequence'][$this->config->get('marketplace_cancel_order_status')]);
            }

            foreach ($data['marketplace_order_status_sequence'] as $key => $value) {

                if ($value['order_status_id'] == $this->config->get('marketplace_cancel_order_status')) {

                    unset($data['marketplace_order_status_sequence'][$key]);
                }
            }
        } else {
            $data['marketplace_cancel_order_status'] = '';
        }

        if ($order_info) {
            $data['seller_id'] = (int) $this->partner->getId();
            $data['wksellerorderstatus'] = $this->config->get('marketplace_sellerorderstatus');

            if ($order_info['invoice_no']) {
                $data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
            } else {
                $data['invoice_no'] = '';
            }

            $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

            if ($order_info['payment_address_format']) {
                $format = $order_info['payment_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['payment_firstname'],
                'lastname' => $order_info['payment_lastname'],
                'company' => $order_info['payment_company'],
                'address_1' => $order_info['payment_address_1'],
                'address_2' => $order_info['payment_address_2'],
                'city' => $order_info['payment_city'],
                'postcode' => $order_info['payment_postcode'],
                'zone' => $order_info['payment_zone'],
                'zone_code' => $order_info['payment_zone_code'],
                'country' => $order_info['payment_country']
            );

            $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $data['payment_method'] = $order_info['payment_method'];

            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['shipping_address_2'],
                'city' => $order_info['shipping_city'],
                'postcode' => $order_info['shipping_postcode'],
                'zone' => $order_info['shipping_zone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country']
            );

            $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            $data['shipping_method'] = $order_info['shipping_method'];

            $data['products'] = array();

            $products = $this->model_account_customerpartner->getSellerOrderProducts($order_id);
            $data['hide_change_status'] = false;

            // Uploaded files
            $this->load->model('tool/upload');

            foreach ($products as $product) {
                $option_data = array();

                $options = $this->model_account_order->getOrderOptions($order_id, $product['order_product_id']);

                if($product['order_product_status'] == 1)
                    $data['hide_change_status'] = true;

                // code changes due to download file error
                foreach ($options as $option) {
                    if ($option['type'] != 'file') {
                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $option['value'],
                            'type' => $option['type']
                        );
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);
                        if ($upload_info) {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $upload_info['name'],
                                'type' => $option['type'],
                                'href' => $this->url->link('orderinfo/download', '&code=' . $upload_info['code'], 'SSL')
                            );
                        }
                    }
                }

                $product_tracking = $this->model_account_customerpartner->getOdrTracking($data['order_id'], $product['product_id'], $this->partner->getId());

                if ($product['paid_status'] == 1) {
                    $paid_status = $this->language->get('text_paid');
                } else {
                    $paid_status = $this->language->get('text_not_paid');
                }

                $data['products'][] = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['prod_name'],
                    'model' => $product['model'],
                    'sku' => $product['sku'],
                    'option' => $option_data,
                    'tracking' => isset($product_tracking['tracking']) ? $product_tracking['tracking'] : '',
                    'quantity' => $product['quantity'],
                    'paid_status' => $paid_status,
                    'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'order_product_status' => $product['order_product_status'],
                );
            }

            // Voucher
            $data['vouchers'] = array();

            $vouchers = $this->model_account_order->getOrderVouchers($order_id);

            foreach ($vouchers as $voucher) {
                $data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                );
            }

            $data['totals'] = array();

            $totals = $this->model_account_customerpartner->getOrderTotals($order_id);

            if ($totals AND isset($totals[0]['total']))
                $data['totals'][]['total'] = $this->currency->format($totals[0]['total'], $order_info['currency_code'], $order_info['currency_value']);

            $data['comment'] = nl2br($order_info['comment']);

            $data['package_desc'] = array();
            $PackageDESC = $this->model_account_customerpartner->getPackageDescription();
            foreach ($PackageDESC as $pkg) {
                $data['package_desc'][] = array(
                    'name' => $pkg['name'],
                    'package_id' => $pkg['package_id']
                );
            }

            $data['histories'] = array();
            $results = $this->model_account_customerpartner->getOrderHistories($order_id);
            foreach ($results as $result) {
                $data['histories'][] = array(
                    'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                    'status' => $result['status'],
                    'comment' => nl2br($result['comment'])
                );
            }

            //list of status

            $this->load->model('localisation/order_status');

            $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

            if ($this->config->get('marketplace_complete_order_status')) {
                foreach ($data['order_statuses'] as $key => $value) {
                    if ($this->config->get('marketplace_complete_order_status') == $value['name']) {
                        $data['marketplace_complete_order_status'] = $value['order_status_id'];
                    }
                }
            }

            $data['order_status_id'] = $order_info['order_status_id'];
        } else {
            $data['errorPage'] = true;
            $data['order_status_id'] = '';
        }

        $data['action'] = $this->url->link('orderinfo&order_id=' . $this->request->get['order_id'], '', 'SSL');
        $data['continue'] = $this->url->link('orderlist', '', 'SSL');
        $data['order_invoice'] = $this->url->link('soldinvoice&order_id=' . $this->request->get['order_id'], '', 'SSL');

        /*
          Access according to membership plan
         */
        $data['isMember'] = true;
        if ($this->config->get('wk_seller_group_status')) {
            $data['wk_seller_group_status'] = true;
            $this->load->model('account/customer_group');
            $isMember = $this->model_account_customer_group->getSellerMembershipGroup($this->partner->getId());
            if ($isMember) {
                $allowedAccountMenu = $this->model_account_customer_group->getaccountMenu($isMember['gid']);
                if ($allowedAccountMenu['value']) {
                    $accountMenu = explode(',', $allowedAccountMenu['value']);
                    if ($accountMenu && !in_array('orderhistory:orderhistory', $accountMenu)) {
                        $data['isMember'] = false;
                    }
                }
            } else {
                $data['isMember'] = false;
            }
        } else {
            if (!in_array('orderhistory', $this->config->get('marketplace_allowed_account_menu'))) {
                $this->response->redirect($this->url->link('dashboard', '', 'SSL'));
            }
        }

        /*
          end here
         */
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header', $data);

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/customerpartner/orderinfo.tpl', $data));
    }

    public function history() {
        $this->language->load('account/customerpartner/orderinfo');
        $this->load->model('account/customerpartner');

        $json = array();
        if ($this->config->get('marketplace_cancel_order_status')) {
            $marketplace_cancel_order_status = $this->config->get('marketplace_cancel_order_status');
        } else {
            $marketplace_cancel_order_status = '';
        }
        $this->request->get['order_id'] = $this->commonfunctions->getOrderNumber($this->request->get['order_id']);

        if (!isset($this->request->post['package_id']) && isset($this->request->post['comment']) && !empty($this->request->post['comment']) && empty($this->request->post['product_ids'])) {

            $getOrderStatusId = $this->model_account_customerpartner->getOrderStatusId($this->request->get['order_id']);

            $this->request->post['order_status_id'] = $getOrderStatusId['order_status_id'];

            $this->model_account_customerpartner->addOrderHistory($this->request->get['order_id'], $this->request->post);

            $this->model_account_customerpartner->addSellerOrderStatus($this->request->get['order_id'], '', $this->request->post);

            $json['success'] = $this->language->get('text_success_history');
        } elseif (!isset($this->request->post['package_id']) && isset($this->request->post['product_ids']) && !empty($this->request->post['product_ids'])) {

            $products = explode(",", $this->request->post['product_ids']);

            $this->load->model('localisation/order_status');
            $order_statuses = $this->model_localisation_order_status->getOrderStatuses();

            foreach ($order_statuses as $value) {

                if (in_array($this->request->post['order_status_id'], $value)) {

                    $seller_change_order_status_name = $value['name'];
                }
            }

            if ($this->request->post['order_status_id'] == $marketplace_cancel_order_status) {

                $this->changeOrderStatus($this->request->get, $this->request->post, $products, $marketplace_cancel_order_status, $seller_change_order_status_name);
            } else {

                $this->changeOrderStatus($this->request->get, $this->request->post, $products, $marketplace_cancel_order_status, $seller_change_order_status_name);
            }

            $json['success'] = $this->language->get('text_success_history');
        } else {
            if (isset($this->request->post['comment']) && $this->request->post['order_status_id'] == 17 && isset($this->request->post['package_id']) && !empty($this->request->post['product_ids'])) {
                $products = explode(",", $this->request->post['product_ids']);
                $this->load->model('localisation/order_status');
                $order_statuses = $this->model_localisation_order_status->getOrderStatuses();

                foreach ($order_statuses as $value) {

                    if (in_array($this->request->post['order_status_id'], $value)) {

                        $seller_change_order_status_name = $value['name'];
                    }
                }
                $this->changeOrderStatus($this->request->get, $this->request->post, $products, $marketplace_cancel_order_status, $seller_change_order_status_name);

                $json['success'] = $this->language->get('text_success_history');
            } else {

                $json['error'] = $this->language->get('error_product_select');
            }
        }

        $this->response->setOutput(json_encode($json));
    }

    private function changeOrderStatus($get, $post, $products, $marketplace_cancel_order_status, $seller_change_order_status_name) {
        /**
         * First step - Add seller changing status for selected products
         */
        $this->model_account_customerpartner->addsellerorderproductstatus($get['order_id'], $post['order_status_id'], $products);


        // Second Step - add comment for each selected products
        $this->model_account_customerpartner->addSellerOrderStatus($get['order_id'], $post['order_status_id'], $post, $products, $seller_change_order_status_name);

        // Thired Step - Get status Id that will be the whole order status id after changed the order product status by seller
        $getWholeOrderStatus = $this->model_account_customerpartner->getWholeOrderStatus($get['order_id'], $marketplace_cancel_order_status);


        // Fourth Step - add comment in order_history table and send mails to admin(If admin notify is enable) and customer
        $this->model_account_customerpartner->addOrderHistory($get['order_id'], $post, $seller_change_order_status_name);


        // Fifth Step - Update whole order status in order table
        if ($getWholeOrderStatus) {
            $this->model_account_customerpartner->changeWholeOrderStatus($get['order_id'], $getWholeOrderStatus);
        }
    }

    // file download code
    public function download() {
        $this->load->model('tool/upload');

        if (isset($this->request->get['code'])) {
            $code = $this->request->get['code'];
        } else {
            $code = 0;
        }

        $upload_info = $this->model_tool_upload->getUploadByCode($code);

        if ($upload_info) {
            $file = DIR_UPLOAD . $upload_info['filename'];
            $mask = basename($upload_info['name']);

            if (!headers_sent()) {
                if (is_file($file)) {
                    header('Content-Type: application/octet-stream');
                    header('Content-Description: File Transfer');
                    header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));

                    readfile($file, 'rb');
                    exit;
                } else {
                    exit('Error: Could not find file ' . $file . '!');
                }
            } else {
                exit('Error: Headers already sent out!');
            }
        } else {
            $this->load->language('error/not_found');

            $this->document->setTitle($this->language->get('heading_title'));

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_not_found'] = $this->language->get('text_not_found');

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], 'SSL')
            );

            $data['header'] = $this->load->controller('common/header', array('breadcrumbs' => $data['breadcrumbs']));
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('error/not_found.tpl', $data));
        }
    }

    public function send_shipment_request() {

        $json = array();

        $this->load->model("account/order");
        $this->load->model("account/customerpartner");

        if (isset($this->request->post['order_id']) && $this->request->post['order_id'] != '') {
            $order_id = $this->request->post['order_id'];
        } else {
            $json['error'] = $this->language->get('error_order');
            $json["status"] = 0;
        }

        if (isset($this->request->post['product_ids']) and ! empty($this->request->post['product_ids'])) {
            $product_ids = $this->request->post['product_ids'];
        } else {
            $json['error'] = $this->language->get('error_product_selected');
            $json["status"] = 0;
        }

        if (isset($this->request->post['seller_id']) && $this->request->post['seller_id'] != '') {
            $seller_id = $this->request->post['seller_id'];
        } else {
            $json['error'] = $this->language->get('error_seller_selected');
            $json["status"] = 0;
        }

        if (isset($this->request->post['weight']) && $this->request->post['weight'] != '') {
            $weight = $this->request->post['weight'];
        } else {
            $weight = 0;
        }

        if (!$json) {
            //if any of the products are not assigned for this seller then the shipment operation will be cancelled
            $check_products_seller = $this->model_account_order->check_products_seller($product_ids, $seller_id);

            if (count($check_products_seller) < count(explode(",", $product_ids))) {
                $json['error'] = $this->language->get('error_check_seller_products'); //"Error: some products are not related to this seller ";
                $json["status"] = 0;
                echo json_encode($json);
                return;
            }

            $order_info = $this->model_account_order->getOrder($order_id);

            $destination_code = $this->model_account_order->get_destination_code($order_info['shipping_zone_id']);

            $shipment_status = $this->model_account_order->check_shipment($order_id);
            $data = array(
                "order_id" => $order_info['order_id'],
                "prod_type" => "DOX",
                "weight" => $weight
            );
            if (!$shipment_status) {
                //getting order information


                $operation_data = array();
                //$operation_data['argAwbBooking']['AWBNo'] = $data['AWBNO'];
                $operation_data['argAwbBooking']['Consignee'] = $order_info['firstname'] . ' ' . $order_info['lastname'];
                #$operation_data['argAwbBooking']['ConsigneeCPerson'] = '';
                $operation_data['argAwbBooking']['ConsigneeAddress1'] = $order_info['shipping_address_1'];
                $operation_data['argAwbBooking']['ConsigneeAddress2'] = $order_info['shipping_address_2'];
                $operation_data['argAwbBooking']['ConsigneePhone'] = $order_info['telephone'];
                $operation_data['argAwbBooking']['ConsigneeCity'] = $order_info['shipping_city'];
                $operation_data['argAwbBooking']['ConsigneeCountry'] = $order_info['shipping_country'];
                $operation_data['argAwbBooking']['DestCode'] = $destination_code['destination_code'];
                $operation_data['argAwbBooking']['CountryCode'] = $order_info['shipping_country_id'];
                #$operation_data['argAwbBooking']['RetailZipCode'] = '';
                $operation_data['argAwbBooking']['Price'] = $order_info['total'];
                $operation_data['argAwbBooking']['Weight'] = $weight;
                $operation_data['argAwbBooking']['CustCash'] = '';
                $operation_data['argAwbBooking']['ServiceType'] = '';
                $operation_data['argAwbBooking']['Quantity'] = '';
                $operation_data['argAwbBooking']['AgentCode'] = '';
                $operation_data['argAwbBooking']['AgentAWBNo'] = '';
                $operation_data['argAwbBooking']['ShipperRefNo'] = '';
                $operation_data['argAwbBooking']['GoodsDescription'] = '';
                $operation_data['argAwbBooking']['SpecialInstruction'] = '';
                $operation_data['argAwbBooking']['ValueCurrency'] = '';
                $operation_data['argAwbBooking']['ValueOfShipment'] = '';
                $operation_data['argAwbBooking']['ProductType'] = $data['prod_type'];
                $operation_data['argAwbBooking']['DOX'] = '';
                $operation_data['argAwbBooking']['ConsigneeMob'] = '';
                $operation_data['argAwbBooking']['Dimension'] = '';
                $operation_data['argAwbBooking']['ChargeableWeight'] = '';

                //if the shipment operation is not created then send the request.
                $response = $this->FFServices->NewOrderWithAutoAWBNO($operation_data);
                if ($response) {
                    $data['AWBNO'] = $response;
                    $this->model_account_order->add_shipment($data);
                }
            } else {
                $data['AWBNO'] = $this->model_account_order->getAWBNo($order_id);
            }

            $no_of_orders = $this->model_account_order->get_booking_count($data['AWBNO']);
            $seller_info = $this->model_account_customerpartner->getProfile($seller_id);

            $operation_data = array();
            $operation_data['argAwbBooking']['AWBNo'] = $data['AWBNO'];
            $operation_data['argAwbBooking']['Origin'] = '';
            $operation_data['argAwbBooking']['Destination'] = '';
            $operation_data['argAwbBooking']['AddnlInfo'] = '';
            $operation_data['argAwbBooking']['CustomerCode'] = '';
            $operation_data['argAwbBooking']['Shipper'] = $seller_info['screenname'];
            $operation_data['argAwbBooking']['ShipperCPErson'] = $seller_info['screenname'];
            $operation_data['argAwbBooking']['ShipperAddress1'] = $seller_info['address'];
            #$operation_data['argAwbBooking']['ShipperAddress2'] = '';
            $operation_data['argAwbBooking']['ShipperPhone'] = $seller_info['telephone'];
            #$operation_data['argAwbBooking']['ShipperPin'] = '';
            $operation_data['argAwbBooking']['ShipperCity'] = $seller_info['zone'];
            $operation_data['argAwbBooking']['ShipperCountry'] = $seller_info['country'];
            $operation_data['argAwbBooking']['Consignee'] = $order_info['firstname'] . ' ' . $order_info['lastname'];
            $operation_data['argAwbBooking']['ConsigneeCPerson'] = $order_info['firstname'] . ' ' . $order_info['lastname'];
            $operation_data['argAwbBooking']['ConsigneeAddress1'] = $order_info['payment_address_1'];
            $operation_data['argAwbBooking']['ConsigneeAddress2'] = $order_info['payment_address_2'];
            $operation_data['argAwbBooking']['ConsigneePhone'] = $order_info['telephone'];
            $operation_data['argAwbBooking']['ConsigneeMobile'] = $order_info['telephone'];
            $operation_data['argAwbBooking']['ConsigneePin'] = '';
            $operation_data['argAwbBooking']['ConsigneeCity'] = $order_info['shipping_city'];
            $operation_data['argAwbBooking']['ConsigneeCountry'] = $order_info['shipping_country_id'];
            $operation_data['argAwbBooking']['ServiceType'] = '';
            $operation_data['argAwbBooking']['ProductType'] = 'DOX';
            $operation_data['argAwbBooking']['InvoiceValue'] = '';
            $operation_data['argAwbBooking']['CODAmount'] = '';
            $operation_data['argAwbBooking']['SpecialInst'] = '';
            $operation_data['argAwbBooking']['Weight'] = $weight;
            $operation_data['argAwbBooking']['email'] = $order_info['email'];
            $operation_data['argAwbBooking']['RTOShipment'] = '';
            $operation_data['argAwbBooking']['VehType'] = '';
            $operation_data['argAwbBooking']['PBranch'] = '';
            $operation_data['argAwbBooking']['ReadyDate'] = '';
            $operation_data['argAwbBooking']['ReadyTime'] = '';
            $operation_data['argAwbBooking']['ClsoingTime'] = '';

            $booking_response = $this->FFServices->CreateBooking($operation_data);
            $data['book_response'] = $booking_response->CreateBookingResult;
            $this->model_account_order->add_booking($data);
            $json = ["status" => 1, "success" => "Success"];
        }
        echo json_encode($json);
    }

    public function getOrderStatus($order_id) {

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }

        $this->load->model("account/order");
        $this->load->model("account/customerpartner");

        $AWBNo = $this->model_account_order->getAWBNo($order_id);
        if (!$AWBNo) {
            $log_data = $this->FFServices->LogData(123456);
            //code after getting the AirWayBill number
        } else {
            echo json_encode(["Error" => " AWBNO is not found", "Status" => "0"]);
        }
        //  $this->sendShipmentMail($order_id);
    }

    public function sendShipmentMail($order_id) {
        /* $mailData = array(
          'seller_id' => "6005",
          'text' => "odai",
          // 'html' => $html,
          'customer_id' => false,
          'mail_id' => $this->config->get('marketplace_mail_order'),
          'mail_from' => $this->config->get('marketplace_adminmail'),
          'mail_to' => "odai_mohammed@hotmail.com",
          );
          $values = array();
          $this->model_customerpartner_mail->mail($mailData,$values);
         */
        $seller_id = '6005';
        $this->load->model('account/order');
        $this->load->model('customerpartner/mail');
        $this->load->model('account/customerpartnerorder');

        $order_info = $this->model_account_order->getOrder($order_id);
        if ($order_info) {
            $order_status = $order_info['order_status_id'];
            // Shipping Address
            $this->load->model('account/address');
            $shipping_address = $this->model_account_customerpartnerorder->getShippingAddressId($order_id);
            $shipping_address = $this->model_account_address->getAddress($shipping_address['address_id']);

            $order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int) $order_id . "'");

            $mailToSellers = array();

            $shipping = 0;
            $paid_shipping = false;
            $admin_shipping_method = false;
            $resultData = $this->model_account_customerpartnerorder->sellerAdminData($this->cart->getProducts());

            if ($this->config->get('marketplace_allowed_shipping_method')) {
                if (in_array($order_info['shipping_code'], $this->config->get('marketplace_allowed_shipping_method'))) {
                    $admin_shipping_method = true;
                    // if($this->config->get('marketplace_divide_shipping') &&  $this->config->get('marketplace_divide_shipping') == 'yes'){
                    $sellers_count = count($resultData);
                    foreach ($resultData as $key => $value) {
                        if ($value['seller'] == 'admin') {
                            $sellers_count = count($resultData) - 1;
                        }
                    }
                    if ($sellers_count == 1) {
                        $shipping = $this->session->data['shipping_method']['cost'];
                    }
                    // }
                }
            }


            foreach ($order_product_query->rows as $product) {

                $prsql = '';

                $mpSellers = $this->db->query("SELECT c.email,c.customer_id,p.product_id,p.subtract,c2c.commission FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "customerpartner_to_product c2p ON (p.product_id = c2p.product_id) LEFT JOIN " . DB_PREFIX . "customer c ON (c2p.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "customerpartner_to_customer c2c ON (c2c.customer_id = c2p.customer_id) WHERE p.product_id = '" . $product['product_id'] . "' $prsql ORDER BY c2p.id ASC ")->row;

                if (isset($mpSellers['email']) AND ! empty($mpSellers['email'])) {

                    $option_data = array();

                    $order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . (int) $product['order_product_id'] . "' AND deleted='0'");

                    foreach ($order_option_query->rows as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['value'];
                        } else {
                            $value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
                        }

                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
                        );
                    }

                    $product_total = $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0);

                    $products = array(
                        'product_id' => $product['product_id'],
                        'name' => $product['name'],
                        'model' => $product['model'],
                        'option' => $option_data,
                        'quantity' => $product['quantity'],
                        'product_total' => $product_total, // without symbol
                        'price' => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                        'total' => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
                    );

                    $i = 0;

                    if ($mailToSellers) {

                        foreach ($mailToSellers as $key => $value) {
                            foreach ($value as $key1 => $value1) {
                                $i = 0;
                                if ($key1 == 'email' AND $value1 == $mpSellers['email']) {
                                    $mailToSellers[$key]['products'][] = $products;
                                    $mailToSellers[$key]['total'] = $product_total + $mailToSellers[$key]['total'];
                                    break;
                                } else {
                                    $i++;
                                }
                            }
                        }
                    } else {
                        $mailToSellers[] = array('email' => $mpSellers['email'],
                            'customer_id' => $mpSellers['customer_id'],
                            'seller_email' => $mpSellers['email'],
                            'products' => array(0 => $products),
                            'total' => $product_total
                        );
                    }

                    if ($i > 0) {
                        $mailToSellers[] = array('email' => $mpSellers['email'],
                            'customer_id' => $mpSellers['customer_id'],
                            'seller_email' => $mpSellers['email'],
                            'products' => array(0 => $products),
                            'total' => $product_total
                        );
                    }
                }
            }


            if ($this->config->get('marketplace_mailtoseller')) {

                // Send out order confirmation mail
                $this->load->language('default');
                $this->load->language('mail/order');

                $subject = sprintf($this->language->get('text_new_subject'), $order_info['store_name'], $order_id);

                // HTML Mail for seller
                $data = array();
                $data['order'] = $order_id;
                $data['title'] = sprintf($this->language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

                $data['text_greeting'] = '';
                $data['text_link'] = $this->language->get('text_new_link');
                $data['text_order_detail'] = $this->language->get('text_new_order_detail');
                $data['text_order_status'] = $this->language->get('text_order_status');

                $data['text_instruction'] = $this->language->get('text_new_instruction');
                $data['text_order_id'] = $this->language->get('text_new_order_id');
                $data['text_date_added'] = $this->language->get('text_new_date_added');
                $data['text_payment_method'] = $this->language->get('text_new_payment_method');
                $data['text_shipping_method'] = $this->language->get('text_new_shipping_method');
                $data['text_email'] = $this->language->get('text_new_email');
                $data['text_telephone'] = $this->language->get('text_new_telephone');
                $data['text_ip'] = $this->language->get('text_new_ip');
                $data['text_payment_address'] = $this->language->get('text_new_payment_address');
                $data['text_shipping_address'] = $this->language->get('text_new_shipping_address');
                $data['text_product'] = $this->language->get('text_new_product');
                $data['text_model'] = $this->language->get('text_new_model');
                $data['text_sku'] = $this->language->get('text_new_sku');
                $data['text_quantity'] = $this->language->get('text_new_quantity');
                $data['text_price'] = $this->language->get('text_new_price');
                $data['text_total'] = $this->language->get('text_new_total');
                //$data['text_footer'] = $this->language->get('text_new_footer');
                $data['text_powered'] = $this->language->get('text_new_powered');
                $data['text_buyer_name'] = $this->language->get('text_buyer_name');
                $data['text_buyer_country'] = $this->language->get('text_buyer_country');
                $data['text_item_number'] = $this->language->get('text_item_number');
                $data['text_below_order_summary'] = $this->language->get('text_below_order_summary');
                $data['text_order_number'] = $this->language->get('text_order_number');
                $data['text_order_date'] = $this->language->get('text_order_date');
                $data['text_rma_date'] = $this->language->get('text_rma_date');

                $data['text_reason_for_return'] = $this->language->get('text_reason_for_return');
                $data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
                $data['store_name'] = $order_info['store_name'];
                $data['store_url'] = $order_info['store_url'];
                $data['customer_id'] = $order_info['customer_id'];
                $data['link'] = $order_info['store_url'] . 'index.php?route=orderinfo&order_id=' . $order_id;
                $data['order_id'] = $order_id;
                $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
                $data['payment_method'] = $order_info['payment_method'];
                $data['shipping_method'] = $order_info['shipping_method'];
                $data['email'] = $order_info['email'];
                $data['telephone'] = $order_info['telephone'];
                $data['ip'] = $order_info['ip'];
                $data['order_status'] = $order_status;
                $data['buyer_name'] = $order_info['payment_firstname'] . " " . $order_info['payment_lastname'];
                $data['buyer_country'] = $order_info['payment_country'];
                $data['rma_date'] = date("M d, Y");


                $data['comment'] = false;

                if ($order_info['payment_address_format']) {
                    $format = $order_info['payment_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['payment_firstname'],
                    'lastname' => $order_info['payment_lastname'],
                    'company' => $order_info['payment_company'],
                    'address_1' => $order_info['payment_address_1'],
                    'address_2' => $order_info['payment_address_2'],
                    'city' => $order_info['payment_city'],
                    'zone' => $order_info['payment_zone'],
                    'zone_code' => $order_info['payment_zone_code'],
                    'country' => $order_info['payment_country']
                );

                $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                if ($order_info['shipping_address_format']) {
                    $format = $order_info['shipping_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} ' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['shipping_firstname'],
                    'lastname' => $order_info['shipping_lastname'],
                    'company' => $order_info['shipping_company'],
                    'address_1' => $order_info['shipping_address_1'],
                    'address_2' => $order_info['shipping_address_2'],
                    'city' => $order_info['shipping_city'],
                    'zone' => $order_info['shipping_zone'],
                    'zone_code' => $order_info['shipping_zone_code'],
                    'country' => $order_info['shipping_country']
                );

                $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                // Products
                $data['products'] = array();

                // Text Mail for seller
                $textBasic = $this->language->get('text_new_order_id') . ' ' . $order_id . "\n";
                $textBasic .= $this->language->get('text_new_date_added') . ' ' . date($this->language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
                $textBasic .= $this->language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

                // Order Totals
                $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int) $order_id . "' AND deleted='0' ORDER BY sort_order ASC");

                foreach ($order_total_query->rows as $total) {
                    $data['totals'][] = array(
                        'title' => $total['title'],
                        'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                    );
                }
                foreach ($mailToSellers as $value) {

                    //for template for seller
                    $data['products'] = $value['products'];

                    $html = $this->load->view($this->config->get('config_template') . '/template/mail/shipment.tpl', $data);

                    //for text for seller
                    $products = $value['products'];
                    $text = $textBasic;
                    foreach ($products as $product) {
                        $text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($product['total']) . "\n";

                        foreach ($product['option'] as $option) {
                            $text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($option['value'])) . "\n";
                        }
                    }

                    $text .= $this->language->get('text_new_order_total') . "\n";

                    $text .= $this->currency->format($value['total'], $order_info['currency_code'], 1) . ': ' . html_entity_decode($this->currency->format($value['total'], $order_info['currency_code'], 1), ENT_NOQUOTES, 'UTF-8') . "\n";
                }

                $values = array(
                    'customer_name' => $this->partner->getFirstName() . ' ' . $this->partner->getLastName(),
                    'order_id' => $data['order'],
                    'order' => $html
                );

                //send mail to seller when rma created
                if ($seller_id != 0) {
                    $mailData['seller_id'] = $seller_id;
                    $mailData['mail_id'] = $this->config->get('wk_rma_seller_mail');
                    $mailData['mail_from'] = $this->config->get('marketplace_adminmail');
                    $mailData['mail_to'] = $this->getSellerMail($seller_id);

                    $this->model_customerpartner_mail->mail($mailData, $values);
                }
                $mailData['mail_id'] = $this->config->get('wk_rma_customer_mail');
                $mailData['mail_from'] = $this->config->get('marketplace_adminmail');
                $mailData['mail_to'] = $this->partner->getEmail();
                $this->model_customerpartner_mail->mail($mailData, $values);

                $mailData['mail_id'] = $this->config->get('wk_rma_admin_mail');
                $mailData['mail_from'] = $this->partner->getEmail();
                $mailData['mail_to'] = $this->config->get('marketplace_adminmail');
                $this->model_customerpartner_mail->mail($mailData, $values);
            }
        }
    }

    protected function getSellerMail($seller_id) {
        $query = $this->db->query("SELECT email FROM " . DB_PREFIX . "customerpartner_to_customer WHERE customer_id = " . $seller_id);
        return $query->row['email'];
    }

}
