<?php

class ControllerSellerDiscounts extends Controller {

    private $error = array();

    public function index() {

        if (!$this->partner->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('addproduct', '', 'SSL');
            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        if ($this->config->get("config_language_id") == 2) {
            if ($this->language->get('code') == "ar") {
                $language_code = "en";
            } else {
                $language_code = "ar";
            }
            $url_data = $this->request->get;
            unset($url_data['_route_']);
            $route = $url_data['route'];
            unset($url_data['route']);
            $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            $this->response->redirect($this->url->link($route, $url, $this->request->server['HTTPS'], $language_code));
        }

        $this->load->model('account/customerpartner');
        $this->load->model('catalog/product');

        $data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();
        if (!$data['chkIsPartner']) {
            $this->response->redirect($this->url->link('account/login'));
        }

        $this->load->language('account/sellerDiscount');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->document->addScript(HTTPS_SERVER . '../admin/view/javascript/summernote/summernote.js');
        $this->document->addStyle(HTTPS_SERVER . '../admin/view/javascript/summernote/summernote.css');
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addScript(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/moment.js');
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
        $this->document->addStyle(HTTPS_IMAGE_S3_STATIC . 'css/sell.min.css');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            if ($this->request->post['discount_id'] > 0){
                $result = $this->model_account_customerpartner->editProductSaleByBrand($this->request->post);
            } else {
                $result = $this->model_account_customerpartner->addProductSaleByBrand($this->request->post);
            }
            
            if(!empty($result['Error'])) {
                $data['error_brand_no_products'] = $this->language->get('error_brand_no_products');
            } else {
                $this->response->redirect($this->url->link('discountlist', '', 'SSL'));
            }

        }

        // from product
        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_percent'] = $this->language->get('text_percent');
        $data['text_amount'] = $this->language->get('text_amount');
        $data['text_none'] = $this->language->get('text_none');
        $data['tab_special']  = $this->language->get('tab_special');
        $data['entry_date_start'] = $this->language->get('entry_date_start');
        $data['entry_date_end'] = $this->language->get('entry_date_end');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_back'] = $this->language->get('button_back');
        $data['action'] = $this->url->link('sellerDiscounts', '', 'SSL');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_add_discount'] = $this->language->get('button_add_discount');
        $data['button_add_special'] = $this->language->get('button_add_special');
        $data['button_remove'] = $this->language->get('button_remove');
        $data['entry_brand'] = $this->language->get('entry_brand');
        $data['entry_percentage'] = $this->language->get('entry_percentage');

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->error['brand'])) {
            $data['error_brand'] = $this->error['brand'];
        } else {
            $data['error_brand'] = '';
        }

        if (isset($this->error['percentage'])) {
            $data['error_percentage'] = $this->error['percentage'];
        } else {
            $data['error_percentage'] = '';
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['cancel'] = $this->url->link('dashboard', '', 'SSL');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('dashboard'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title_discountlist'),
            'href' => $this->url->link('discountlist'),
            'separator' => $this->language->get('text_separator')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sellerDiscounts'),
            'separator' => $this->language->get('text_separator')
        );

        $discount_info = array();
        $data['discount_id'] =0;
        $url = '';
        if(isset($this->request->get['discount_id']) && $this->request->get['discount_id'] >0){
          $discount_info = $this->model_account_customerpartner->getDiscount($this->request->get['discount_id']);
          $url .= '&discount_id='.$this->request->get['discount_id'];
          $data['action'] = $this->url->link('sellerDiscounts', ''.$url, 'SSL');
          $data['discount_id'] = $this->request->get['discount_id'];
        }

        if (isset($this->request->post['brand_id'])) {
            $data['brand_id'] = $this->request->post['brand_id'];
        } elseif (!empty($discount_info)) {
            $data['brand_id'] = $discount_info['brand_id'];
        } else {
            $data['brand_id'] = 0;
        }

        if (isset($this->request->post['brand'])) {
            $data['brand'] = $this->request->post['brand'];
        } elseif (!empty($discount_info)) {
          $data['brand'] = $discount_info['name'];
        } else {
            $data['brand'] = '';
        }

        if (isset($this->request->post['product_special']['percentage'])) {
            $data['product_specials_percentage'] = $this->request->post['product_special']['percentage'];
        } elseif (!empty($discount_info)) {
            $data['product_specials_percentage'] = $discount_info['percentage'];
        } else {
            $data['product_specials_percentage'] = 0;
        }

        if (isset($this->request->post['product_special']['date_added'])) {
            $data['product_specials_date_added'] = $this->request->post['product_special']['date_added'];
        } elseif (!empty($discount_info) && $discount_info['date_start'] !='0000-00-00') {
            $data['product_specials_date_added'] = $discount_info['date_start'];
        } else {
            $data['product_specials_date_added'] = '';
        }

        if (isset($this->request->post['product_special']['date_end'])) {
            $data['product_specials_date_end'] = $this->request->post['product_special']['date_end'];
        } elseif (!empty($discount_info) && $discount_info['date_end'] !='0000-00-00') {
            $data['product_specials_date_end'] = $discount_info['date_end'];
        } else {
            $data['product_specials_date_end'] = '';
        }

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header', $data);

        $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/customerpartner/sellerDiscount.tpl', $data));
    }

    public function brandAutocomplete() {

        $json = array();
        $keyword = '';

        if (isset($this->request->get['filter_keyword'])) {
            $keyword = $this->request->get['filter_keyword'];
        } elseif (isset($this->request->get['query'])) {
            $keyword = $this->request->get['query'];
        }

        if (isset($keyword) && !empty($keyword)) {

            if (isset($keyword)) {
                $filter_name = $keyword;
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['type'])) {
                $type = $this->request->get['type'];
            } else {
                $type = '';
            }

            if (isset($this->request->get['start'])) {
                $start = $this->request->get['start'];
            } else {
                $start = 0;
            }

            if(isset($this->request->get['filter_status'])){
                $filter_status = $this->request->get['filter_status'];
            } else {
                $filter_status = null;
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 10;
            }

            $filter_data = array(
                'filter_name' => $filter_name,
                'filter_keyword' => $filter_name,
                'filter_status' => $filter_status,
                'start'=>$start,
                'limit' => $limit
                    );

                  $this->load->model('account/customerpartner');
                  $results = $this->model_account_customerpartner->getBrands($filter_data);
                foreach ($results as $result) {
                    $json[] = array(
                        'id' => $result['brand_id'],
                        'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                    );
                }
        }


        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function validateForm() {

        if (! isset($this->request->post['brand_id']) || $this->request->post['brand_id']==0) {
            $this->error['brand'] = $this->language->get('error_brand');
        }

        if (! isset($this->request->post['product_special']['percentage']) || $this->request->post['product_special']['percentage']<=0) {
            $this->error['percentage'] = $this->language->get('error_percentage');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}

