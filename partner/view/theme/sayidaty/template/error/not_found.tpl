<?php echo $header; ?>
<div class="content-padding">
    <div class="container">
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div class="container-404">
                <div class="error-404">
                    <div class="">
                        <div class="error-font">404</div>
                        <p><?php echo $text_error; ?></p>
                        <p><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></p>
                    </div>
                </div>
            </div>
            <?php echo $column_right; ?>
        </div>
    </div>
</div>
<?php echo $footer; ?>