<table style="border-collapse:collapse;width:100%;margin:0 0 20px 0;table-layout:fixed;">
  <?php $tdCount=0;		
foreach ($products as $product) {
		if($tdCount++ == 0) { ?>
		  <tr>
		    <?php } ?>
		    <td style="padding:5px 10px;">
		    	<a href="<?php echo $href[$product['product_id']] ;?>" style="display:block;padding:0;margin:0 0 5px 0;font-size:0;">
		    		<img src="<?php echo str_replace(' ', '%20', $images[$product['product_id']]); ?>" alt="<?php echo $product['name']; ?>" style="font-size:0;padding:0;margin:0;border:none;"/>
		    	</a> 
				<a style="text-decoration:none;color:#00B7FF;display:block;padding:0;margin:0 0 5px 0;font-size:14px;" href="<?php echo $href[$product['product_id']]; ?>" target="_blank">
					<?php echo $product['name']; ?>
				</a> 
                <?php if(!empty($product['special_price'])) { ?>
				<span style="font-size:12px;color:#F00;text-decoration:line-through;display:inline-block;margin:0 0 5px 0;">
					<?php echo $product['price']; ?>
				</span>
				<span style="font-size:13px;font-weight:bold;color:#000;display:inline-block;margin:0 0 5px 0;">
					<?php echo $product['special_price']; ?>
				</span>
                    <?php if ((!empty($product['date_start']) && $product['date_start'] != '0000-00-00') || (!empty($product['date_end']) && $product['date_end'] != '0000-00-00')) { ?>
                    <span style="font-size:11px;color:#999;margin:0;padding:0;display:block;">
                        The promotion is valid<?php echo ($product['date_start'] != '0000-00-00') ? (' from '.$product['date_start']) : ''; ?><?php echo ($product['date_end'] != '0000-00-00') ? (' till '.$product['date_end']) : ''; ?>
                    </span>
                    <?php } ?>
			 	<?php } else { ?>
				<span style="font-size:12px;color:#000;display:inline-block;margin:0 0 5px 0;">
					<?php echo $product['price']; ?>
				</span>
                <?php } ?>
			</td>
	<?php if($tdCount == $productsPerRow ) { ?></tr><?php  $tdCount = 0; }
}?>
</table>
