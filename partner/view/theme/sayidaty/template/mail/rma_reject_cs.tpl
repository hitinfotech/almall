<!doctype html>
<!--Quite a few clients strip your Doctype out, and some even apply their own. Many clients do honor your doctype and it can make things much easier if you can validate constantly against a Doctype.-->
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Email template By Adobe Dreamweaver CC</title>

<!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
<!-- When use in Email please remove all comments as it is removed by Email clients-->
<!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->

<style type="text/css">
body {
	margin: 0;
}
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, 'Times New Roman', serif;
	font-style: normal;
	font-weight: 400;
}
button{
	width:90%;
}
@media screen and (max-width:600px) {
/*styling for objects with screen size less than 600px; */
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, 'Times New Roman', serif;
}
table {
	/* All tables are 100% width */
	width: 100%;
}
.footer {
	/* Footer has 2 columns each of 48% width */
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
table.responsiveImage {
	/* Container for images in catalog */
	height: auto !important;
	max-width: 30% !important;
	width: 30% !important;
}
table.responsiveContent {
	/* Content that accompanies the content in the catalog */
	height: auto !important;
	max-width: 66% !important;
	width: 66% !important;
}
.top {
	/* Each Columnar table in the header */
	height: auto !important;
	max-width: 48% !important;
	width: 48% !important;
}
.catalog {
	margin-left: 0%!important;
}

}
@media screen and (max-width:480px) {
/*styling for objects with screen size less than 480px; */
body, table, td, p, a, li, blockquote {
	-webkit-text-size-adjust: none!important;
	font-family: Merienda, 'Times New Roman', serif;
}
table {
	/* All tables are 100% width */
	width: 100% !important;
	border-style: none !important;
}
.footer {
	/* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.table.responsiveImage {
	/* Container for each image now specifying full width */
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.table.responsiveContent {
	/* Content in catalog  occupying full width of cell */
	height: auto !important;
	max-width: 96% !important;
	width: 96% !important;
}
.top {
	/* Header columns occupying full width */
	height: auto !important;
	max-width: 100% !important;
	width: 100% !important;
}
.catalog {
	margin-left: 0%!important;
}
button{
	width:90%!important;
}
}
</style>
</head>
<body yahoo="yahoo">
	<?php echo $email_header ?>
<table width="100%"  cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td><table width="600"  align="center" cellpadding="0" cellspacing="0">
          <!-- Main Wrapper Table with initial width set to 60opx -->
          <tbody>
            <tr>
              <td align="left" style="padding:10px;"></td>
            </tr>
            <tr>
              <!-- HTML Spacer row -->
              <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
            </tr>
            <tr>
              <!-- HTML IMAGE SPACER -->
							<td style="font-size: 0; line-height: 1.5;" ><table align="center"  cellpadding="0" width="100%" cellspacing="0" >
									<tr>
										<td style="padding: 0 20px;">
											<p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 18px; padding: 10px 0;"><b>Return request rejected!</b></p>
											  <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 18px; padding: 10px  font-weight: normal;">The return request number  <span style="color: #d91a5d;"><?php echo $rma_id ?></span> has been rejected by the seller<br><br>
													Please login to the admin panel and proceed to resolve the issue.<br><br>
											    To view the request details click  <a href="<?php echo $rma_link ?>">this link</a><br>
	                      </p>


                    </td>
                  </tr>

              </table></td>
            </tr>

            <tr>
              <td style="padding: 0 20px 10px;">
                  <p style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; padding: 0px 0; font-weight: bold;">
										Sincerely,<br>
										Sayidaty Mall Support Team.</p>
              </td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
<?php echo $email_footer ?>
</body>
</html>
