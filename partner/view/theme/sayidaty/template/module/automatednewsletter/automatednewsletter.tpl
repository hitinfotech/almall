<?php echo $header; ?>
<div  class="container j-container" id="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
   <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    
      <h1><?php echo $heading_title; ?></h1>
  
      <?php foreach ($newsletters as $newsletter) { ?> 
          
      <ul>
        <li><span class="date"><?php echo date("d.m.Y", $newsletter['time_added']); ?></span> <a href="index.php?route=module/automatednewsletter/view&newsletter_id=<?php echo $newsletter['newsletter_id']; ?>">  <?php echo $newsletter['subject']; ?></a></li>
      </ul>
    <?php } ?>
    </div>
     
      <?php echo $content_bottom; ?></div>
     
    <?php echo $column_right; ?></div>
    
</div>
</div>
<?php echo $footer; ?>
