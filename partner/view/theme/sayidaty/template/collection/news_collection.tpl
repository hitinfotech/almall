<?php if (!empty($news)) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="inner-title text-center">
                <h3><?php echo $news_title ?></h3>
            </div>
            <div class="row">
                <?php foreach ($news as $row) { ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile">
                            <div class="tile-img offer-img">
                                <a href="<?php echo $row['href'] ?>"><img class="proimg" src="<?php echo $row['thumb'] ?>"></a>
                            </div>
                            <div class="tile-logo">
                                <a href="<?php echo $row['shop_href'] ?>"><img src="<?php echo $row['logo'] ?>"></a>
                            </div>
                            <div class="tile-text">
                                <a href="<?php echo $row['href'] ?>"><?php echo $row['title'] ?></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="text-center">
                <a href="<?php echo $more_news ?>" class="btn btn-primary btn-more"><?php echo $text_more_news ?></a>
            </div>
        </div>
    </div>
<?php } ?>