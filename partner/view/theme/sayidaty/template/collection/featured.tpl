<?php
if ((isset($item1['image']) && !empty($item1['image'])) &&
        (isset($item2['image']) && !empty($item2['image'])) &&
        (isset($item3['image']) && !empty($item3['image'])) &&
        (isset($item4['image']) && !empty($item4['image'])) &&
        (isset($item5['image']) && !empty($item5['image'])) &&
        (isset($item6['image']) && !empty($item6['image'])) &&
        (isset($item7['image']) && !empty($item7['image'])) &&
        (isset($item8['image']) && !empty($item8['image']))
) {
    ?>

    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="home-block hp-tip-block">
                <div class="block-img"> <a href="<?php echo $item1['href']; ?>"><img src="<?php echo $item1['image']; ?>"  alt="<?php echo $item1['title']; ?>" title="<?php echo $item1['title']; ?>" class="full-img"></a>
                </div>
                <div class="block-desc">
                    <h4><a href="<?php echo $item1['href']; ?>"><?php echo $item1['title']; ?></a></h4>
                    <p class="text-center"><a href="<?php echo $item1['href']; ?>" class="btn btn-secondary"><?php echo $text_shop_now; ?></a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="home-block hp-look-block">
                <div class="block-img"> <a href="<?php echo $item2['href']; ?>"><img src="<?php echo $item2['image']; ?>" class="full-img" alt="<?php echo $item2['title']; ?>" title="<?php echo $item2['title']; ?>" ></a>
                </div>
                <div class="block-desc">
                    <h4><a href="<?php echo $item2['href']; ?>"><?php echo $item2['title']; ?></a></h4>
                    <p class="text-center"><a href="<?php echo $item2['href']; ?>" class="btn btn-secondary"><?php echo $text_shop_now; ?></a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="home-block hp-look-block">
                <div class="block-img"> <a href="<?php echo $item3['href']; ?>"><img src="<?php echo $item3['image']; ?>" alt="<?php echo $item3['title']; ?>" title="<?php echo $item3['title']; ?>" class="full-img"></a>
                </div>
                <div class="block-desc">
                    <h4><a href="<?php echo $item3['href']; ?>"><?php echo $item3['title']; ?></a> </h4>
                    <p class="text-center"><a href="<?php echo $item3['href']; ?>" class="btn btn-secondary"><?php echo $text_shop_now; ?></a></p>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="home-block hp-tip-block">
                <div class="block-img"> <a href="<?php echo $item4['href']; ?>"><img src="<?php echo $item4['image']; ?>" alt="<?php echo $item4['title']; ?>" title="<?php echo $item4['title']; ?>" class="full-img"></a>
                </div>
                <div class="block-desc">
                    <h4><a href="<?php echo $item4['href']; ?>"><?php echo $item4['title']; ?></a></h4>
                    <p class="text-center"><a href="<?php echo $item4['href']; ?>" class="btn btn-secondary"><?php echo $text_shop_now; ?></a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="home-block hp-tip-block">
                <div class="block-img"> <a href="<?php echo $item5['href']; ?>"><img src="<?php echo $item5['image']; ?>"  alt="<?php echo $item5['title']; ?>" title="<?php echo $item5['title']; ?>" class="full-img"></a>
                </div>
                <div class="block-desc">
                    <h4><a href="<?php echo $item5['href']; ?>"><?php echo $item5['title']; ?></a></h4>
                    <p class="text-center"><a href="<?php echo $item5['href']; ?>" class="btn btn-secondary"><?php echo $text_shop_now; ?></a></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="home-block hp-look-block">
                <div class="block-img"> <a href="<?php echo $item6['href']; ?>"><img src="<?php echo $item6['image']; ?>" class="full-img" alt="<?php echo $item6['title']; ?>" title="<?php echo $item6['title']; ?>" ></a>
                </div>
                <div class="block-desc">
                    <h4><a href="<?php echo $item6['href']; ?>"><?php echo $item6['title']; ?></a></h4>
                    <p class="text-center"><a href="<?php echo $item6['href']; ?>" class="btn btn-secondary"><?php echo $text_shop_now; ?></a></p>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
