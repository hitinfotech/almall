<?php if (isset($products) && $products) { ?>
    <div class="col-md-3 col-sm-12">
        <div class="best-sell">
            <h2 class="text-center"><?php echo $products_title ?></h2>
            <?php foreach ($products as $product) { ?>
                <div class="main-img">
                    <a href="<?php echo $product['href'] ?>"><img class="proimg" alt="<?php echo $product['name'] ?>" src="<?php echo $product['thumb'] ?>"></a>
                </div>
                <div class="">
                    <?php echo $product['name'] ?>
                    <p><?php if($product['special']){ echo $product['special']; }else{echo $product['price'];} ?></p>
                </div>
                <div class="d-break-space text-center">
                    <a href="<?php echo $product['href'] ?>" class="btn btn-sm btn-secondary"><?php echo $text_get_it_now ?></a>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>