<?php if (isset($products) && $products) { ?>
    <!-- Slider 1 -->
    <div class="col-md-12 col-sm-12">
        <div class="inner-title text-center">
            <h3><?php echo $products_title ?></h3>
        </div>
        <section class="regular-sld slider">
            <?php foreach ($products as $i => $product) { ?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile">
                        <?php /*
                          <div class="prod-star">
                          <a href="#"><i class="fa fa-star"></i></a>
                          </div>
                         */ ?>
                        <div class="tile-img">
                            <a href="<?php echo $product['href'] ?>"><img class="proimg" alt="<?php echo $product['name'] ?>" src="<?php echo $product['thumb'] ?>"></a>
                        </div>
                        <div class="tile-logo">
                            <a href="<?php echo $product['brand_href'] ?>"><?php echo $product['brand_name'] ?></a>
                        </div>
                        <div class="tile-text">
                            <a href="<?php echo $product['href'] ?>"><?php echo $product['name'] ?></a>
                        </div>
                        <?php if ($product['stock_status_id'] != 10) { ?>
                            <?php if ($product['quantity'] <= 0) { ?>
                                <div class="side-roate-container">
                                    <span class="side-rotate">
                                        <?php echo $sold_out_text; ?>
                                    </span>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <div class="tile-price">
                            <?php if ((float) $product['price'] > 0 && $product['quantity'] > 0) { ?>
                                <div class="tile-price"> 
                                    <p class="price">
                                        <?php if (!$product['special']) { ?>
                                             <span class="price-new"><?php echo $product['price']; ?></span>
                                        <?php } else { ?>
                                            <span class="price-new danger-text"><?php echo $product['special']; ?></span>
                                            <span class="price-old"><?php echo $product['price']; ?></span> 
                                        <?php } ?>
                                        <?php if ((float) $product['tax']) { ?>
                                            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                        <?php } ?>
                                    </p>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if ($product['stock_status_id'] != 10) { ?>
                            <?php if ($product['quantity'] > 0) { ?>
                                <div class="text-center break-space">
                                    <a href='<?php echo $product["href"] ?>' class="btn btn-secondary btn-sm">
                                        <?php echo $button_cart; ?>
                                    </a>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </section>
        <div class="text-center d-break-space">
            <a class="btn btn-primary btn-more" href="<?php echo $more_products ?>"><?php echo $text_more_products; ?></a>
        </div>
        <!-- /Slider 1 -->
    </div>
<?php } ?>
