<div class="modal fade" id="size_3" tabindex="-1" role="dialog" aria-labelledby="size">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo $size_array['header'] ?></h4>
            </div>
            <div class="modal-body">
                <div class="size-chart">
                    <div>
                        <div class="preference-tabs">
                            <ul role="tablist" id="prefTabs" class="nav nav-tabs  seller-tabs">
                                <li class="active" role="presentation"><a aria-expanded="true" aria-controls="cloths" data-toggle="tab" role="tab" id="cloths-tab_3" href="#cloths_3"> <span><img src="<?php echo $size_image['baby-icon'] ?>" class="size-tab-icon"></span> <?php echo $size_array['text_size_child'] ?> </a></li>
                                <li role="presentation" class=""><a aria-controls="shoes" data-toggle="tab" id="shoes-tab" role="tab" href="#shoes_3" aria-expanded="false"><span><img src="<?php echo $size_image['girl-icon'] ?>" class="size-tab-icon"></span>  <?php echo $size_array['text_size_girls'] ?> </a></li>
                                <li class=""><a aria-controls="rings" data-toggle="tab" id="rings-tab" role="tab" href="#rings_3" aria-expanded="false"> <span><img src="<?php echo $size_image['boy-icon'] ?>" class="size-tab-icon"></span> <?php echo $size_array['text_size_boys'] ?> </a></li>
                                <li role="presentation"><a aria-expanded="true" aria-controls="belts" data-toggle="tab" role="tab" id="belts-tab" href="#belts_3"> <span><img src="<?php echo $size_image['babyshoe-icon'] ?>" class="size-tab-icon"></span> <?php echo $size_array['shoes'] ?> </a></li>
                            </ul>
                        </div>
                        <div id="prefTabContent" class="tab-content">
                            <div div aria-labelledby="cloths-tab" id="cloths_3" role="tabpanel" class="tab-pane fade active in">
                                <div class="title"> <?php echo $size_array['text_sizes_table'] ?></div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th><?php echo $size_array['text_weight'] ?></th>
                                            <th><?php echo $size_array['text_height'] ?></th>
                                            <th><?php echo $size_array['text_age'] ?></th>
                                            <th><?php echo $size_array['text_size'] ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr >
                                            <td>5 - 11 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>19
                                                - 23''</td>
                                            <td>0-3
                                                <?php echo $size_array['text_size_month'] ?></td>
                                            <td><?php echo $size_array['text_size_new_born'] ?></td>
                                        </tr>
                                        <tr >
                                            <td>11 -15 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>23
                                                - 36''</td>
                                            <td>3-6
                                                <?php echo $size_array['text_size_month'] ?></td>
                                            <td>S</td>
                                        </tr>
                                        <tr >
                                            <td>15-18 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>26-82''</td>
                                            <td>6-9
                                                <?php echo $size_array['text_size_month'] ?></td>
                                            <td>M</td>
                                        </tr>
                                        <tr >
                                            <td>18-22 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>28-30''</td>
                                            <td>9-12
                                                <?php echo $size_array['text_size_month'] ?></td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr >
                                            <td>22-25 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>30-32''</td>
                                            <td>12-18
                                                <?php echo $size_array['text_size_month'] ?></td>
                                            <td>L</td>
                                        </tr>
                                        <tr >
                                            <td>25-30 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>32-35''</td>
                                            <td>18-24
                                                <?php echo $size_array['text_size_month'] ?></td>
                                            <td>XL</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p class="gray-text"><?php echo $size_array['text_size_note'] ?></p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="shoes_3">
                                <div class="title"> <?php echo $size_array['text_size_girls_2_6_monthes'] ?> </div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th><?php echo $size_array['text_weight'] ?></th>
                                            <th><?php echo $size_array['text_height'] ?></th>
                                            <th><?php echo $size_array['text_size_child'] ?></th>
                                            <th><?php echo $size_array['text_size'] ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>29-32 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td >33-36''</td>
                                            <td>2T</td>
                                            <td >S</td>
                                        </tr>
                                        <tr>
                                            <td>31-33 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td >36-39''</td>
                                            <td>3T</td>
                                            <td >&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>29-32 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td >36-39''</td>
                                            <td>4T</td>
                                            <td >M</td>
                                        </tr>
                                        <tr>
                                            <td>31-33 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td >39-42''</td>
                                            <td>4</td>
                                            <td >&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>41-46 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td >42-45''</td>
                                            <td>5</td>
                                            <td >L</td>
                                        </tr>
                                        <tr>
                                            <td>45-50 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td >45-48''</td>
                                            <td>6</td>
                                            <td >&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>51-56 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td >48-51''</td>
                                            <td>6X</td>
                                            <td >XL</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="d-break-space">
                                    <div class="title"><?php echo $size_array['text_size_girls_7_16_monthes'] ?></div>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><?php echo $size_array['text_weight'] ?></th>
                                                <th><?php echo $size_array['text_height'] ?></th>
                                                <th><?php echo $size_array['text_size_child'] ?></th>
                                                <th><?php echo $size_array['text_size'] ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td >55-60 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td >49-51''</td>
                                                <td >7</td>
                                                <td >S</td>
                                            </tr>
                                            <tr>
                                                <td >61-66 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td >51.5-53''</td>
                                                <td >8</td>
                                                <td >M</td>
                                            </tr>
                                            <tr>
                                                <td >67-74 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td >53.5-55''</td>
                                                <td >10</td>
                                                <td >&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td >75-84 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td >55.5-57.5''</td>
                                                <td >12</td>
                                                <td >L</td>
                                            </tr>
                                            <tr>
                                                <td >85-96 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td >58-60''</td>
                                                <td >14</td>
                                                <td >&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td >97-110 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td >60.5-62.5''</td>
                                                <td >16</td>
                                                <td >XL</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="rings_3">
                                <div class="title"><?php echo $size_array['text_size_boys_2_7_monthes'] ?></div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th><?php echo $size_array['text_weight'] ?></th>
                                            <th><?php echo $size_array['text_height'] ?></th>
                                            <th><?php echo $size_array['text_size_child'] ?></th>
                                            <th><?php echo $size_array['text_size'] ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr >
                                            <td>29-32 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>33-36''</td>
                                            <td>2T</td>
                                            <td>S</td>
                                        </tr>
                                        <tr >
                                            <td>31-33 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>36-39''</td>
                                            <td>3T</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr >
                                            <td>29-32 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>36-39''</td>
                                            <td>4T</td>
                                            <td>M</td>
                                        </tr>
                                        <tr >
                                            <td>31-33 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>39-42''</td>
                                            <td>4</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr >
                                            <td>41-46 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>42-45''</td>
                                            <td>5</td>
                                            <td>L</td>
                                        </tr>
                                        <tr >
                                            <td>45-50 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>45-48''</td>
                                            <td>6</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr >
                                            <td>51-56 <?php echo $size_array['text_size_pound'] ?></td>
                                            <td>48-51''</td>
                                            <td>7</td>
                                            <td>XL</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="d-break-space">
                                    <div class="title"><?php echo $size_array['text_size_boys_8_20_monthes'] ?></div>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><?php echo $size_array['text_weight'] ?></th>
                                                <th><?php echo $size_array['text_height'] ?></th>
                                                <th><?php echo $size_array['text_size_child'] ?></th>
                                                <th><?php echo $size_array['text_size'] ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>55-59 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td>48.5-50''</td>
                                                <td>8</td>
                                                <td>S</td>
                                            </tr>
                                            <tr>
                                                <td>60-73 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td>50.5-54''</td>
                                                <td>10</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>74-87 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td>54.5-58''</td>
                                                <td>12</td>
                                                <td>M</td>
                                            </tr>
                                            <tr>
                                                <td>88-100 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td>58.5-61''</td>
                                                <td>14</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>101-115 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td>61.5-64''</td>
                                                <td>16</td>
                                                <td>L</td>
                                            </tr>
                                            <tr>
                                                <td>115-126 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td>64.5-66''</td>
                                                <td>18</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>127-138 <?php echo $size_array['text_size_pound'] ?></td>
                                                <td>66.5-68''</td>
                                                <td>20</td>
                                                <td>XL</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="belts_3">
                                <div class="title"> <?php echo $size_array['text_size_girls_2_6_monthes'] ?></div>
                                <table class="table">
                                    <thead>
                                        <tr >
                                            <th><?php echo $size_array['text_size_size_age'] ?></th>
                                            <th><?php echo $size_array['text_size_shoe_size'] ?></th>
                                            <th><?php echo $size_array['text_size_socks'] ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr >
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>up to 7 </td>
                                        </tr>
                                        <tr >
                                            <td><?php echo $size_array['text_upto_6_monthes'] ?></td>
                                            <td>&nbsp;</td>
                                            <td>0-3 <?php echo $size_array['text_size_month'] ?></td>
                                        </tr>
                                        <tr >
                                            <td><?php echo $size_array['text_upto_6_monthes'] ?></td>
                                            <td>3 (45/16")</td>
                                            <td>3-6 <?php echo $size_array['text_size_month'] ?></td>
                                        </tr>
                                        <tr>
                                            <td >6-12 <?php echo $size_array['text_size_month'] ?></td>
                                            <td>4 (45/8")-5(45/16")</td>
                                            <td>6-12 <?php echo $size_array['text_size_month'] ?></td>
                                        </tr>
                                        <tr >
                                            <td>12 - 24 <?php echo $size_array['text_size_month'] ?></td>
                                            <td>6(55/16")</td>
                                            <td>12-18 <?php echo $size_array['text_size_month'] ?></td>
                                        </tr>
                                        <tr >
                                            <td>12-24 <?php echo $size_array['text_size_month'] ?></td>
                                            <td>7 (55/8")</td>
                                            <td>18-24 <?php echo $size_array['text_size_month'] ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="d-break-space">
                                    <div class="title"> <?php echo $size_array['text_size_girls'] ?></div>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><?php echo $size_array['text_size_socks'] ?></th>
                                                <th><?php echo $size_array['text_sizes_inch'] ?></th>
                                                <th><?php echo $size_array['text_size_shoe_size'] ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr >
                                                <td>s</td>
                                                <td>65/8"</td>
                                                <td>10</td>
                                            </tr>
                                            <tr >
                                                <td>s</td>
                                                <td>615/16"</td>
                                                <td>11</td>
                                            </tr>
                                            <tr >
                                                <td>s</td>
                                                <td>7"</td>
                                                <td>12</td>
                                            </tr>
                                            <tr >
                                                <td>s</td>
                                                <td>79/16"</td>
                                                <td>13</td>
                                            </tr>
                                            <tr >
                                                <td>m</td>
                                                <td>77/8"</td>
                                                <td>1</td>
                                            </tr>
                                            <tr >
                                                <td>m</td>
                                                <td>8"</td>
                                                <td>2</td>
                                            </tr>
                                            <tr >
                                                <td>l</td>
                                                <td>89/16"</td>
                                                <td>3</td>
                                            </tr>
                                            <tr >
                                                <td>l</td>
                                                <td>87/8"</td>
                                                <td>4</td>
                                            </tr>
                                            <tr >
                                                <td>xl</td>
                                                <td>9"</td>
                                                <td>5</td>
                                            </tr>
                                            <tr >
                                                <td>xl</td>
                                                <td>915/16"</td>
                                                <td>6</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-break-space">
                                    <div class="title"> <?php echo $size_array['text_size_boys'] ?></div>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><?php echo $size_array['text_size_socks'] ?></th>
                                                <th><?php echo $size_array['text_sizes_inch'] ?></th>
                                                <th><?php echo $size_array['text_size_shoe_size'] ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr >
                                                <td>s</td>
                                                <td>65/8"</td>
                                                <td>10</td>
                                            </tr>
                                            <tr >
                                                <td>s</td>
                                                <td>615/16"</td>
                                                <td>11</td>
                                            </tr>
                                            <tr >
                                                <td>s</td>
                                                <td>7"</td>
                                                <td>12</td>
                                            </tr>
                                            <tr >
                                                <td>s</td>
                                                <td>79/16"</td>
                                                <td>13</td>
                                            </tr>
                                            <tr >
                                                <td>m</td>
                                                <td>77/8"</td>
                                                <td>1</td>
                                            </tr>
                                            <tr >
                                                <td>m</td>
                                                <td>8"</td>
                                                <td>2</td>
                                            </tr>
                                            <tr >
                                                <td>l</td>
                                                <td>89/16"</td>
                                                <td>3</td>
                                            </tr>
                                            <tr >
                                                <td>l</td>
                                                <td>87/8"</td>
                                                <td>4</td>
                                            </tr>
                                            <tr >
                                                <td>xl</td>
                                                <td>9"</td>
                                                <td>5</td>
                                            </tr>
                                            <tr >
                                                <td>xl</td>
                                                <td>915/16"</td>
                                                <td>6</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>