<div class="modal fade" id="size_2" tabindex="-1" role="dialog" aria-labelledby="size">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo $size_array['header'] ?></h4>
            </div>
            <div class="modal-body">
                <div class="size-chart">
                    <div>
                        <div class="preference-tabs">
                            <ul role="tablist" id="prefTabs" class="nav nav-tabs  seller-tabs">
                                <li class="active" role="presentation"><a aria-expanded="true" aria-controls="cloths" data-toggle="tab" role="tab" id="cloths-tab" href="#cloths"> <span><img src="<?php echo $size_image['dress-icon'] ?>" class="size-tab-icon"></span> <?php echo $size_array['women_clothes'] ?> </a></li>
                                <li role="presentation" class=""><a aria-controls="shoes" data-toggle="tab" id="shoes-tab" role="tab" href="#shoes" aria-expanded="false"><span><img src="<?php echo $size_image['shoes-icon'] ?>" class="size-tab-icon"></span> <?php echo $size_array['shoes'] ?> </a></li>
                                <li class=""><a aria-controls="rings" data-toggle="tab" id="rings-tab" role="tab" href="#rings" aria-expanded="false"> <span><img src="<?php echo $size_image['ring-icon'] ?>" class="size-tab-icon"></span> <?php echo $size_array['rings'] ?> </a></li>
                                <li role="presentation"><a aria-expanded="true" aria-controls="belts" data-toggle="tab" role="tab" id="belts-tab" href="#belts"> <span><img src="<?php echo $size_image['belt-icon'] ?>" class="size-tab-icon"></span> <?php echo $size_array['belts'] ?> </a></li>
                            </ul>
                        </div>
                        <div id="prefTabContent" class="tab-content">
                            <div div aria-labelledby="cloths-tab" id="cloths" role="tabpanel" class="tab-pane fade active in">
                                <div class="title"><?php echo $size_array['text_sizes_table'] ?></div>
                                <table class="table">
                                    <tr >
                                        <th >Size</th>
                                        <th>US</th>
                                        <th>IT</th>
                                        <th>UK</th>
                                        <th>FR</th>
                                        <th>JP</th>
                                        <th>GER</th>
                                        <th>SPAIN</th>
                                        <th>AUS</th>
                                        <th><?php echo $size_array['text_sizes_bust'] ?></th>
                                        <th><?php echo $size_array['text_sizes_waist'] ?></th>
                                        <th><?php echo $size_array['text_sizes_hip'] ?>*</th>
                                    </tr>
                                    <tr >
                                        <td >XXXS</td>
                                        <td>000</td>
                                        <td>32</td>
                                        <td>0</td>
                                        <td>28</td>
                                        <td>1</td>
                                        <td>24</td>
                                        <td>26</td>
                                        <td>0</td>
                                        <td>30</td>
                                        <td>23</td>
                                        <td>33</td>
                                    </tr>
                                    <tr >
                                        <td >XXS</td>
                                        <td>00</td>
                                        <td>34</td>
                                        <td>2</td>
                                        <td>30</td>
                                        <td>1</td>
                                        <td>26</td>
                                        <td>28</td>
                                        <td>2</td>
                                        <td>31.5</td>
                                        <td>24</td>
                                        <td>34</td>
                                    </tr>
                                    <tr >
                                        <td >XS</td>
                                        <td>0</td>
                                        <td>36</td>
                                        <td>4</td>
                                        <td>32</td>
                                        <td>3</td>
                                        <td>28</td>
                                        <td>30</td>
                                        <td>4</td>
                                        <td>32.5</td>
                                        <td>25</td>
                                        <td>35</td>
                                    </tr>
                                    <tr >
                                        <td >XS</td>
                                        <td>2</td>
                                        <td>38</td>
                                        <td>6</td>
                                        <td>34</td>
                                        <td>5</td>
                                        <td>30</td>
                                        <td>32</td>
                                        <td>6</td>
                                        <td>33.5</td>
                                        <td>26</td>
                                        <td>36</td>
                                    </tr>
                                    <tr >
                                        <td >S</td>
                                        <td>4</td>
                                        <td>40</td>
                                        <td>8</td>
                                        <td>36</td>
                                        <td>7</td>
                                        <td>34</td>
                                        <td>34</td>
                                        <td>8</td>
                                        <td>34.5</td>
                                        <td>27</td>
                                        <td>37</td>
                                    </tr>
                                    <tr >
                                        <td >S</td>
                                        <td>6</td>
                                        <td>42</td>
                                        <td>10</td>
                                        <td>38</td>
                                        <td>9</td>
                                        <td>34</td>
                                        <td>36</td>
                                        <td>10</td>
                                        <td>35.5</td>
                                        <td>28</td>
                                        <td>38</td>
                                    </tr>
                                    <tr >
                                        <td >M</td>
                                        <td>8</td>
                                        <td>44</td>
                                        <td>12</td>
                                        <td>40</td>
                                        <td>11</td>
                                        <td>36</td>
                                        <td>38</td>
                                        <td>12</td>
                                        <td>36.5</td>
                                        <td>29</td>
                                        <td>39</td>
                                    </tr>
                                    <tr >
                                        <td >M</td>
                                        <td>10</td>
                                        <td>46</td>
                                        <td>14</td>
                                        <td>42</td>
                                        <td>13</td>
                                        <td>38</td>
                                        <td>40</td>
                                        <td>14</td>
                                        <td>37.5</td>
                                        <td>30</td>
                                        <td>40</td>
                                    </tr>
                                    <tr >
                                        <td >L</td>
                                        <td>12</td>
                                        <td>48</td>
                                        <td>16</td>
                                        <td>44</td>
                                        <td>15</td>
                                        <td>40</td>
                                        <td>42</td>
                                        <td>16</td>
                                        <td>39</td>
                                        <td>31.5</td>
                                        <td>41.5</td>
                                    </tr>
                                    <tr >
                                        <td >L</td>
                                        <td>14</td>
                                        <td>50</td>
                                        <td>18</td>
                                        <td>46</td>
                                        <td>17</td>
                                        <td>42</td>
                                        <td>44</td>
                                        <td>18</td>
                                        <td>40.5</td>
                                        <td>33</td>
                                        <td>43</td>
                                    </tr>
                                    <tr >
                                        <td >XL</td>
                                        <td>16</td>
                                        <td>52</td>
                                        <td>20</td>
                                        <td>48</td>
                                        <td>19</td>
                                        <td>44</td>
                                        <td>46</td>
                                        <td>20</td>
                                        <td>42</td>
                                        <td>34.5</td>
                                        <td>44.5</td>
                                    </tr>
                                    <tr >
                                        <td >XL</td>
                                        <td>18</td>
                                        <td>54</td>
                                        <td>22</td>
                                        <td>50</td>
                                        <td>21</td>
                                        <td>46</td>
                                        <td>48</td>
                                        <td>22</td>
                                        <td>44</td>
                                        <td>36</td>
                                        <td>46.5</td>
                                    </tr>
                                    <tr >
                                        <td >XXL</td>
                                        <td>20</td>
                                        <td>56</td>
                                        <td>24</td>
                                        <td>52</td>
                                        <td>23</td>
                                        <td>48</td>
                                        <td>50</td>
                                        <td>24</td>
                                        <td>46</td>
                                        <td>37</td>
                                        <td>48.5</td>
                                    </tr>
                                </table>

                                <p class="gray-text"><?php echo $size_array['text_size_note'] ?></p>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="shoes">
                                <div class="title"> <?php echo $size_array['text_sizes_table'] ?></div>
                                <table class="table">
                                    <tbody>
                                        <tr >
                                            <th >US</th>
                                            <th >EU</th>
                                            <th>UK</th>
                                            <th> <?php echo $size_array['text_sizes_inch'] ?></th>
                                            <th><?php echo $size_array['text_sizes_cm'] ?></th>
                                        </tr>
                                        <tr>
                                            <td >4</td>
                                            <td>34</td>
                                            <td >2</td>
                                            <td >8
                                                1/4</td>
                                            <td >21</td>
                                        </tr>
                                        <tr>
                                            <td >4.5</td>
                                            <td>34.5</td>
                                            <td >2.5</td>
                                            <td >8
                                                1/2</td>
                                            <td >21.6</td>
                                        </tr>
                                        <tr>
                                            <td >5</td>
                                            <td>35</td>
                                            <td >3</td>
                                            <td >8
                                                5/8</td>
                                            <td >21.9</td>
                                        </tr>
                                        <tr>
                                            <td >5.5</td>
                                            <td>35.5</td>
                                            <td >3.5</td>
                                            <td >8
                                                3/4</td>
                                            <td >22.2</td>
                                        </tr>
                                        <tr>
                                            <td >6</td>
                                            <td>36</td>
                                            <td >4</td>
                                            <td >9</td>
                                            <td >22.9</td>
                                        </tr>
                                        <tr>
                                            <td >6.5</td>
                                            <td>36.5</td>
                                            <td >4.5</td>
                                            <td >9
                                                1/8</td>
                                            <td >23.2</td>
                                        </tr>
                                        <tr>
                                            <td >7</td>
                                            <td>37</td>
                                            <td >5</td>
                                            <td >9
                                                1/4</td>
                                            <td >23.5</td>
                                        </tr>
                                        <tr>
                                            <td >7.5</td>
                                            <td>37.5</td>
                                            <td >5.5</td>
                                            <td >9
                                                1/2</td>
                                            <td >24.1</td>
                                        </tr>
                                        <tr>
                                            <td >8</td>
                                            <td>38</td>
                                            <td >6</td>
                                            <td >9
                                                5/8</td>
                                            <td >24.4</td>
                                        </tr>
                                        <tr>
                                            <td >8.5</td>
                                            <td>38.5</td>
                                            <td >6.5</td>
                                            <td >9
                                                3/4</td>
                                            <td >24.8</td>
                                        </tr>
                                        <tr>
                                            <td >9</td>
                                            <td>39</td>
                                            <td >7</td>
                                            <td >10</td>
                                            <td >25.4</td>
                                        </tr>
                                        <tr>
                                            <td >9.5</td>
                                            <td>39.5</td>
                                            <td >7.5</td>
                                            <td >10
                                                1/8</td>
                                            <td >25.7</td>
                                        </tr>
                                        <tr>
                                            <td >10</td>
                                            <td>40</td>
                                            <td >8</td>
                                            <td >10
                                                1/4</td>
                                            <td >26</td>
                                        </tr>
                                        <tr>
                                            <td >10.5</td>
                                            <td>40.5</td>
                                            <td >8.5</td>
                                            <td >10
                                                1/2</td>
                                            <td >26.7</td>
                                        </tr>
                                        <tr>
                                            <td >11</td>
                                            <td>41</td>
                                            <td >9</td>
                                            <td >10
                                                5/8</td>
                                            <td >27</td>
                                        </tr>
                                        <tr>
                                            <td >12</td>
                                            <td>42</td>
                                            <td >9.5</td>
                                            <td >11</td>
                                            <td >27.9</td>
                                        </tr>
                                        <tr>
                                            <td >13</td>
                                            <td>43</td>
                                            <td >10</td>
                                            <td >11
                                                1/4</td>
                                            <td >28.6</td>
                                        </tr>
                                        <tr>
                                            <td >14</td>
                                            <td>44</td>
                                            <td >10.5</td>
                                            <td >11
                                                5/8</td>
                                            <td >29.5</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="rings">
                                <div class="title"><?php echo $size_array['text_sizes_table'] ?></div>
                                <table>
                                    <tbody>
                                        <tr >
                                            <th><?php echo $size_array['text_size_in_inched'] ?></th>
                                            <th ><?php echo $size_array['text_size_in_mm'] ?></th>
                                            <th><?php echo $size_array['text_diameter_in_mm'] ?></th>
                                            <th><?php echo $size_array['text_size_turkey'] ?></th>
                                            <th><?php echo $size_array['text_size_australian'] ?></th>
                                            <th><?php echo $size_array['text_size_british'] ?></th>
                                            <th><?php echo $size_array['text_size_french'] ?></th>
                                            <th ><?php echo $size_array['text_size_german'] ?></th>
                                        </tr>
                                        <tr>
                                            <td >1 7/8</td>
                                            <td>48</td>
                                            <td >15.27</td>
                                            <td>8</td>
                                            <td>I
                                                1/2</td>
                                            <td>I
                                                - 1/2</td>
                                            <td>-</td>
                                            <td>15.52</td>
                                        </tr>
                                        <tr>
                                            <td >1 15/16</td>
                                            <td>49</td>
                                            <td >15.7</td>
                                            <td>9</td>
                                            <td>j
                                                1/2</td>
                                            <td>J
                                                1/12</td>
                                            <td>-</td>
                                            <td>15.75</td>
                                        </tr>
                                        <tr>
                                            <td >2</td>
                                            <td>51</td>
                                            <td >16.1</td>
                                            <td>10</td>
                                            <td>L</td>
                                            <td>L</td>
                                            <td>51
                                                3/4</td>
                                            <td>16</td>
                                        </tr>
                                        <tr>
                                            <td >21/16</td>
                                            <td>52</td>
                                            <td >16.51</td>
                                            <td>11</td>
                                            <td>M</td>
                                            <td>M</td>
                                            <td>52
                                                3/4</td>
                                            <td>16.5</td>
                                        </tr>
                                        <tr>
                                            <td >2 1/8</td>
                                            <td>53</td>
                                            <td >16.92</td>
                                            <td>12</td>
                                            <td>N</td>
                                            <td>N</td>
                                            <td>54</td>
                                            <td>17</td>
                                        </tr>
                                        <tr>
                                            <td >23/16</td>
                                            <td>55</td>
                                            <td >17.53</td>
                                            <td>13</td>
                                            <td>O</td>
                                            <td>O</td>
                                            <td>55
                                                1/4</td>
                                            <td>17.25</td>
                                        </tr>
                                        <tr>
                                            <td >2 1/4</td>
                                            <td>56</td>
                                            <td >17.75</td>
                                            <td>14</td>
                                            <td>P</td>
                                            <td>P</td>
                                            <td>56
                                                1/2</td>
                                            <td>17.75</td>
                                        </tr>
                                        <tr>
                                            <td >25/16</td>
                                            <td>57</td>
                                            <td >18.19</td>
                                            <td>15</td>
                                            <td>Q</td>
                                            <td>Q</td>
                                            <td>57
                                                3/4</td>
                                            <td>18</td>
                                        </tr>
                                        <tr>
                                            <td >2 3/8</td>
                                            <td>58</td>
                                            <td >18.53</td>
                                            <td>16</td>
                                            <td>Q
                                                1/2</td>
                                            <td>Q
                                                1/2</td>
                                            <td>58</td>
                                            <td>18.5</td>
                                        </tr>
                                        <tr>
                                            <td >27/16</td>
                                            <td>59</td>
                                            <td >18.89</td>
                                            <td>17</td>
                                            <td>R
                                                1/2</td>
                                            <td>R
                                                1/2</td>
                                            <td>59
                                                1/4</td>
                                            <td>19</td>
                                        </tr>
                                        <tr>
                                            <td >2 1/2</td>
                                            <td>61</td>
                                            <td >19.41</td>
                                            <td>18</td>
                                            <td>S
                                                1/2</td>
                                            <td>S
                                                1/2</td>
                                            <td>60
                                                1/2</td>
                                            <td>19.5</td>
                                        </tr>
                                        <tr>
                                            <td >29/16</td>
                                            <td>62</td>
                                            <td >19.84</td>
                                            <td>19</td>
                                            <td>T
                                                1/2</td>
                                            <td>T
                                                1/2</td>
                                            <td>61
                                                3/4</td>
                                            <td>20</td>
                                        </tr>
                                        <tr>
                                            <td >2 5/8</td>
                                            <td>63</td>
                                            <td >20.2</td>
                                            <td>20</td>
                                            <td>U
                                                1/2</td>
                                            <td>U
                                                1/2</td>
                                            <td>62
                                                3/4</td>
                                            <td>20.25</td>
                                        </tr>
                                        <tr>
                                            <td >2 11/16</td>
                                            <td>65</td>
                                            <td >20.68</td>
                                            <td>21</td>
                                            <td>V
                                                1/2</td>
                                            <td>V
                                                1/2</td>
                                            <td>64
                                                1/4</td>
                                            <td>20.75</td>
                                        </tr>
                                        <tr>
                                            <td >2 3/4</td>
                                            <td>66</td>
                                            <td >21.08</td>
                                            <td>22</td>
                                            <td>W
                                                1/2</td>
                                            <td>W
                                                1/2</td>
                                            <td>66</td>
                                            <td>21</td>
                                        </tr>
                                        <tr>
                                            <td >2 13/16</td>
                                            <td>68</td>
                                            <td >21.49</td>
                                            <td>23</td>
                                            <td>Y</td>
                                            <td>Y</td>
                                            <td>67
                                                1/2</td>
                                            <td>21.25</td>
                                        </tr>
                                        <tr>
                                            <td >2 7/8</td>
                                            <td>69</td>
                                            <td >21.89</td>
                                            <td>24</td>
                                            <td>Z</td>
                                            <td>Z</td>
                                            <td>68
                                                3/4</td>
                                            <td>21.75</td>
                                        </tr>
                                        <tr>
                                            <td >2 15/16</td>
                                            <td>70</td>
                                            <td >22.33</td>
                                            <td>25</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>22</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table class="table d-break-space">
                                    <tbody>
                                        <tr >
                                            <th><?php echo $size_array['text_size_in_inched'] ?></th>
                                            <th ><?php echo $size_array['text_size_in_mm'] ?></th>
                                            <th><?php echo $size_array['text_diameter_in_mm'] ?></th>
                                            <th><?php echo $size_array['text_size_italian'] ?></th>
                                            <th ><?php echo $size_array['text_size_japanese'] ?></th>
                                            <th ><?php echo $size_array['text_size_swiss'] ?></th>
                                            <th><?php echo $size_array['text_size_american'] ?></th>
                                            <th ><?php echo $size_array['text_size_europian'] ?></th>
                                        </tr>
                                        <tr>
                                            <td >1 7/8</td>
                                            <td>48</td>
                                            <td >15.27</td>
                                            <td>8</td>
                                            <td>8</td>
                                            <td>-</td>
                                            <td>4
                                                1/2</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr >
                                            <td>1 15/16</td>
                                            <td>49</td>
                                            <td >15.7</td>
                                            <td>9.5</td>
                                            <td>9</td>
                                            <td>-</td>
                                            <td>5</td>
                                            <td>11</td>
                                        </tr>
                                        <tr>
                                            <td >2</td>
                                            <td>51</td>
                                            <td >16.1</td>
                                            <td>10.5</td>
                                            <td>11</td>
                                            <td>11.75</td>
                                            <td>51/2</td>
                                            <td>11</td>
                                        </tr>
                                        <tr>
                                            <td >21/16</td>
                                            <td>52</td>
                                            <td >16.51</td>
                                            <td>11.5</td>
                                            <td>12</td>
                                            <td>12.75</td>
                                            <td>6</td>
                                            <td>12</td>
                                        </tr>
                                        <tr>
                                            <td >2 1/8</td>
                                            <td>53</td>
                                            <td >16.92</td>
                                            <td>13</td>
                                            <td>13</td>
                                            <td>14</td>
                                            <td>6
                                                1/2</td>
                                            <td>13</td>
                                        </tr>
                                        <tr>
                                            <td >23/16</td>
                                            <td>55</td>
                                            <td >17.53</td>
                                            <td>14.5</td>
                                            <td>14</td>
                                            <td>15.25</td>
                                            <td>7</td>
                                            <td>13</td>
                                        </tr>
                                        <tr>
                                            <td >2 1/4</td>
                                            <td>56</td>
                                            <td >17.75</td>
                                            <td>16</td>
                                            <td>15</td>
                                            <td>16.5</td>
                                            <td>7
                                                1/2</td>
                                            <td>14</td>
                                        </tr>
                                        <tr>
                                            <td >25/16</td>
                                            <td>57</td>
                                            <td >18.19</td>
                                            <td>17</td>
                                            <td>16</td>
                                            <td>17.75</td>
                                            <td>8</td>
                                            <td>14/15</td>
                                        </tr>
                                        <tr>
                                            <td >2 3/8</td>
                                            <td>58</td>
                                            <td >18.53</td>
                                            <td>18.5</td>
                                            <td>17</td>
                                            <td>-</td>
                                            <td>8
                                                1/2</td>
                                            <td>15/16</td>
                                        </tr>
                                        <tr>
                                            <td >27/16</td>
                                            <td>59</td>
                                            <td >18.89</td>
                                            <td>19.5</td>
                                            <td>18</td>
                                            <td>-</td>
                                            <td>9</td>
                                            <td>16</td>
                                        </tr>
                                        <tr>
                                            <td >2 1/2</td>
                                            <td>61</td>
                                            <td >19.41</td>
                                            <td>21</td>
                                            <td>19</td>
                                            <td>-</td>
                                            <td>9
                                                1/2</td>
                                            <td>17</td>
                                        </tr>
                                        <tr>
                                            <td >29/16</td>
                                            <td>62</td>
                                            <td >19.84</td>
                                            <td>22</td>
                                            <td>20</td>
                                            <td>-</td>
                                            <td>10</td>
                                            <td>17/18</td>
                                        </tr>
                                        <tr>
                                            <td >2 5/8</td>
                                            <td>63</td>
                                            <td >20.2</td>
                                            <td>23.5</td>
                                            <td>21</td>
                                            <td>-</td>
                                            <td>10
                                                1/2</td>
                                            <td>18</td>
                                        </tr>
                                        <tr>
                                            <td >2 11/16</td>
                                            <td>65</td>
                                            <td >20.68</td>
                                            <td>25</td>
                                            <td>22</td>
                                            <td>-</td>
                                            <td>11</td>
                                            <td>18/19</td>
                                        </tr>
                                        <tr>
                                            <td >2 3/4</td>
                                            <td>66</td>
                                            <td >21.08</td>
                                            <td>26.5</td>
                                            <td>23</td>
                                            <td>-</td>
                                            <td>11
                                                1/2</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td >2 13/16</td>
                                            <td>68</td>
                                            <td >21.49</td>
                                            <td>27.5</td>
                                            <td>24</td>
                                            <td>27.5</td>
                                            <td>12</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td >2 7/8</td>
                                            <td>69</td>
                                            <td >21.89</td>
                                            <td>28.5</td>
                                            <td>24</td>
                                            <td>28.75</td>
                                            <td>12
                                                1/2</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td >2 15/16</td>
                                            <td>70</td>
                                            <td >22.33</td>
                                            <td>30</td>
                                            <td>25</td>
                                            <td>-</td>
                                            <td>13</td>
                                            <td>-</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="belts">
                                <div class="title"><?php echo $size_array['text_sizes_table'] ?></div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th> <?php echo $size_array['text_size_trousers'] ?> </th>
                                            <th> <?php echo $size_array['text_size_belt'] ?></th>
                                            <th>  <?php echo $size_array['text_size_belt'] ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td >0</td>
                                            <td>XS</td>
                                            <td >S/M</td>
                                        </tr>
                                        <tr>
                                            <td >0</td>
                                            <td>XS</td>
                                            <td >S/M</td>
                                        </tr>
                                        <tr>
                                            <td >2</td>
                                            <td>XS
                                                or S</td>
                                            <td >S/M</td>
                                        </tr>
                                        <tr>
                                            <td >4</td>
                                            <td>S
                                                or M</td>
                                            <td >S/M</td>
                                        </tr>
                                        <tr>
                                            <td >6</td>
                                            <td>M</td>
                                            <td >S/M
                                                or M/L</td>
                                        </tr>
                                        <tr>
                                            <td >8</td>
                                            <td>M
                                                or L</td>
                                            <td >M/L</td>
                                        </tr>
                                        <tr>
                                            <td >10</td>
                                            <td>L</td>
                                            <td >M/L</td>
                                        </tr>
                                        <tr>
                                            <td >12</td>
                                            <td>L
                                                or XL</td>
                                            <td >M/L</td>
                                        </tr>
                                        <tr>
                                            <td >14</td>
                                            <td>XL</td>
                                            <td >---</td>
                                        </tr>
                                        <tr>
                                            <td >16</td>
                                            <td>XL</td>
                                            <td >---</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>