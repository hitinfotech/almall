<div class="modal fade" id="size_1" tabindex="-1" role="dialog" aria-labelledby="size">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php echo $size_array['header'] ?></h4>
            </div>
            <div class="modal-body">
                <div class="size-chart">
                    <div>
                        <div class="preference-tabs">
                            <ul role="tablist" id="prefTabs_1" class="nav nav-tabs  seller-tabs">
                                <li class="active" role="presentation"><a aria-expanded="true" aria-controls="cloths_1" data-toggle="tab" role="tab" id="cloths-tab" href="#cloths_1"> <span><img src="<?php echo $size_image['shirtmen-icon'] ?>" class="size-tab-icon"></span> <?php echo $size_array['men_clothes'] ?> </a></li>
                                <li role="presentation" class=""><a aria-controls="shoes_1" data-toggle="tab" id="shoes-tab-1" role="tab" href="#shoes_1" aria-expanded="false"><span><img src="<?php echo $size_image['menshoe-icon'] ?>" class="size-tab-icon"></span> <?php echo $size_array['shoes'] ?> </a></li>
                            </ul>
                        </div>
                        <div id="prefTabContent" class="tab-content">
                            <div div aria-labelledby="cloths-tab" id="cloths_1" role="tabpanel" class="tab-pane fade active in">
                                <div class="title"><?php echo $size_array['text_sizes_table'] ?></div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th><?php echo $size_array['text_size'] ?></th>
                                            <th>EU</th>
                                            <th><?php echo $size_array['text_size_neck'] ?></th>
                                            <th><?php echo $size_array['text_size_neck'] ?></th>
                                            <th ><?php echo $size_array['text_sizes_bust'] ?></th>
                                            <th><?php echo $size_array['text_sizes_waist'] ?></th>
                                            <th ><?php echo $size_array['text_size_arm'] ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td >XS</td>
                                            <td >44</td>
                                            <td >36</td>
                                            <td >14''</td>
                                            <td >32-36''</td>
                                            <td >28-30''</td>
                                            <td>31-32''</td>
                                        </tr>
                                        <tr>
                                            <td>S</td>
                                            <td>46</td>
                                            <td>37-38</td>
                                            <td>14-15''</td>
                                            <td>36-38''</td>
                                            <td>30-32''</td>
                                            <td >32-33''</td>
                                        </tr>
                                        <tr>
                                            <td>M</td>
                                            <td>48</td>
                                            <td>38-39</td>
                                            <td>15-15.5''</td>
                                            <td>38-40''</td>
                                            <td>32-34''</td>
                                            <td>33-34''</td>
                                        </tr>
                                        <tr>
                                            <td>L</td>
                                            <td>50</td>
                                            <td>41-42</td>
                                            <td>16-16.5''</td>
                                            <td>40-42''</td>
                                            <td>34-36''</td>
                                            <td>34-35''</td>
                                        </tr>
                                        <tr>
                                            <td>XL</td>
                                            <td>52</td>
                                            <td>43-44</td>
                                            <td>17-17.5''</td>
                                            <td>42-44''</td>
                                            <td>38-40''</td>
                                            <td>35-36''</td>
                                        </tr>
                                        <tr>
                                            <td>XXL</td>
                                            <td>54</td>
                                            <td>46</td>
                                            <td>18-18.5''</td>
                                            <td>44-46''</td>
                                            <td>42-44''</td>
                                            <td>36.5-37''</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="shoes_1">
                                <div class="title"><?php echo $size_array['text_sizes_table'] ?></div>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>US</th>
                                            <th >EU</th>
                                            <th>UK</th>
                                            <th><?php echo $size_array['text_sizes_inch'] ?></th>
                                            <th><?php echo $size_array['text_sizes_cm'] ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td >5</td>
                                            <td>37.5</td>
                                            <td>4.5</td>
                                            <td>9</td>
                                            <td>22.9</td>
                                        </tr>
                                        <tr>
                                            <td >5.5</td>
                                            <td>38</td>
                                            <td>5</td>
                                            <td>9.125</td>
                                            <td>23.2</td>
                                        </tr>
                                        <tr>
                                            <td >6</td>
                                            <td>38.5</td>
                                            <td>5.5</td>
                                            <td>9.25</td>
                                            <td>23.5</td>
                                        </tr>
                                        <tr>
                                            <td >6.5</td>
                                            <td>39.5</td>
                                            <td>6</td>
                                            <td>9.5</td>
                                            <td>24.1</td>
                                        </tr>
                                        <tr>
                                            <td >7</td>
                                            <td>40</td>
                                            <td>6.5</td>
                                            <td>9.625</td>
                                            <td>24.4</td>
                                        </tr>
                                        <tr>
                                            <td >7.5</td>
                                            <td>40.5</td>
                                            <td>7</td>
                                            <td>9.75</td>
                                            <td>24.8</td>
                                        </tr>
                                        <tr>
                                            <td >8</td>
                                            <td>41.5</td>
                                            <td>7.5</td>
                                            <td>10</td>
                                            <td>25.4</td>
                                        </tr>
                                        <tr>
                                            <td >8.5</td>
                                            <td>42</td>
                                            <td>8</td>
                                            <td>10.125</td>
                                            <td>25.7</td>
                                        </tr>
                                        <tr>
                                            <td >9</td>
                                            <td>42.5</td>
                                            <td>8.5</td>
                                            <td>10.25</td>
                                            <td>26</td>
                                        </tr>
                                        <tr>
                                            <td >9.5</td>
                                            <td>43</td>
                                            <td>9</td>
                                            <td>10.5</td>
                                            <td>26.7</td>
                                        </tr>
                                        <tr>
                                            <td >10</td>
                                            <td>44</td>
                                            <td>9.5</td>
                                            <td>10.625</td>
                                            <td>27</td>
                                        </tr>
                                        <tr>
                                            <td >10.5</td>
                                            <td>44.5</td>
                                            <td>10</td>
                                            <td>10.75</td>
                                            <td>27.3</td>
                                        </tr>
                                        <tr>
                                            <td >11</td>
                                            <td>45</td>
                                            <td>10.5</td>
                                            <td>11</td>
                                            <td>27.9</td>
                                        </tr>
                                        <tr>
                                            <td >11.5</td>
                                            <td>45.5</td>
                                            <td>11</td>
                                            <td>11.125</td>
                                            <td>28.3</td>
                                        </tr>
                                        <tr>
                                            <td >12</td>
                                            <td>46.5</td>
                                            <td>11.5</td>
                                            <td>11.25</td>
                                            <td>28.6</td>
                                        </tr>
                                        <tr>
                                            <td >12.5</td>
                                            <td>47</td>
                                            <td>12</td>
                                            <td>11.5</td>
                                            <td>29</td>
                                        </tr>
                                        <tr>
                                            <td >13</td>
                                            <td>47.5</td>
                                            <td>12.5</td>
                                            <td>11.625</td>
                                            <td>29.5</td>
                                        </tr>
                                        <tr>
                                            <td >14</td>
                                            <td>49</td>
                                            <td>13.5</td>
                                            <td>12</td>
                                            <td>30.5</td>
                                        </tr>
                                        <tr>
                                            <td >15</td>
                                            <td>50</td>
                                            <td>14.5</td>
                                            <td>12.25</td>
                                            <td>31.1</td>
                                        </tr>
                                        <tr>
                                            <td >16</td>
                                            <td>51</td>
                                            <td>15.5</td>
                                            <td>12.625</td>
                                            <td>32.1</td>
                                        </tr>
                                        <tr>
                                            <td >17</td>
                                            <td>52</td>
                                            <td>16.5</td>
                                            <td>13</td>
                                            <td>33</td>
                                        </tr>
                                        <tr>
                                            <td >18</td>
                                            <td>53</td>
                                            <td>17.5</td>
                                            <td>13.25</td>
                                            <td>33.7</td>
                                        </tr>
                                        <tr>
                                            <td >19</td>
                                            <td>54</td>
                                            <td>18.5</td>
                                            <td>13.625</td>
                                            <td>34.6</td>
                                        </tr>
                                        <tr>
                                            <td >20</td>
                                            <td>55</td>
                                            <td>19.5</td>
                                            <td>14</td>
                                            <td>35.6</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</p>
</div>