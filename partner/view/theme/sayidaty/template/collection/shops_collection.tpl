<?php if (isset($shops) && $shops) { ?>
    <div class="row">
        <div class="col-md-12">
            <?php if (isset($shops_title) && $shops_title) { ?>
                <div class="inner-title text-center">
                    <h3> <?php echo $shops_title ?></h3>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <?php foreach ($shops as $i => $shop) { ?>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                                <div class="brand6 brand-tile">
                                    <a href="<?php echo $shop['href'] ?>"><div class="brand-img"><img src="<?php echo $shop['thumb'] ?>"></div>
                                        <div class="bran-name">
                                            <?php echo $shop['name'] ?>
                                        </div>
                                    </a>
                                </div>
                            </div>
                     <?php } ?>
                    </div>
                </div>
            </div>
            <?php if (isset($more_shops) && $more_shops) { ?>
                <div class="text-center">
                    <a href="<?php echo $more_shops ?>" class="btn btn-primary btn-more"><?php echo $text_more_shops; ?></a>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>
