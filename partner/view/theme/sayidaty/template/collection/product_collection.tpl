<div id="content" >
    <?php if (isset($products) && $products) { ?>
        <div class="row">
            <div class="col-md-12">
                <?php if (isset($products_title) && $products_title) { ?>
                    <div class="inner-title text-center">
                        <h3> <?php echo $products_title ?></h3>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <?php foreach ($products as $i => $product) { ?>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                    <div class="tile">
                                        <div class="fave-heart">
                                            <?php if (!$logged) { ?>
                                                <a href="<?php echo $login_url; ?>" data-toggle="modal" data-target="#logIn"><span class="hidden-xs"></span><i class="fa fa-heart"></i></a>
                                            <?php } else { ?>
                                                <?php if (in_array($product['product_id'], $wishlist_ids)) { ?>
                                                    <a href="Javascript:void(0);" onclick="wishlist.remove(<?php echo $product['product_id']; ?>), switch_case($(this), 2,<?php echo $product['product_id']; ?>)" class="active"><i class="fa fa-heart"></i></a>
                                                <?php } else { ?>
                                                    <a href="Javascript:void(0);" onclick="wishlist.add(<?php echo $product['product_id']; ?>), switch_case($(this), 1,<?php echo $product['product_id']; ?>)"><i class="fa fa-heart"></i></a>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="tile-img">
                                            <a href="<?php echo $product['href']; ?>">
                                                <img class="proimg" alt="<?php echo $product['name'] ?>" src="<?php echo $product['thumb'] ?>">
                                            </a>
                                        </div>
                                        <?php if ($product['stock_status_id'] != 10) { ?>
                                          <?php if ($product['quantity'] <= 0) {?>
                                            <div class="side-roate-container">
                                              <span class="side-rotate">
                                                <?php echo $sold_out_text; ?>
                                              </span>
                                            </div>
                                          <?php } ?>
                                        <?php } ?>
                                        <div class="tile-logo">
                                            <a href="<?php echo $product['brand_href'] ?>"><?php echo $product['brand_name'] ?></a>
                                        </div>
                                        <div class="tile-text">
                                            <a href="<?php echo $product['href'] ?>"><?php echo $product['name'] ?></a>
                                        </div>

                                        <?php if ($product['price']) { ?>
                                          <div class="tile-price">
                                            <?php if (!$product['special']) { ?>
                                                <?php echo $product['price']; ?>
                                            <?php } else { ?>
                                                                                    <span class="price-old"><?php echo $product['price']; ?></span>
                                                                                    <span class="price-new"><?php echo $product['special']; ?></span>
                                            <?php } ?>
                                            <?php if ($product['tax']) { ?>
                                                                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                            <?php } ?>
                                          </div>
                                        <?php } ?>
                                        <?php if ($product['stock_status_id'] != 10) { ?>
                                          <?php if ($product['quantity'] > 0) {?>
                                            <div class="text-center break-space">
                                              <a href='<?php echo  $product["href"] ?>' class="btn btn-secondary btn-sm">
                                                <?php echo $button_cart; ?>
                                              </a>
                                            </div>
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php if (isset($product_pagination) && $product_pagination) { ?>
                    <div class="row">
                        <div class="pagination clearfix">
                            <aside>
                                <?php echo $product_pagination; ?>
                            </aside>
                        </div>
                    </div>
                <?php } ?>
                <?php if (isset($more_products) && $more_products) { ?>
                    <div class="text-center">
                        <a href="<?php echo $more_products ?>" class="btn btn-primary btn-more"><?php echo $text_more_products; ?></a>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>
<script>
    function switch_case(ele, status, product_id) {
        if (status == 1) {
            ele.addClass("active");
            ele.attr("onclick", "wishlist.remove(" + product_id + "), switch_case ($(this),2," + product_id + ")");
        } else {
            ele.removeClass("active");
            ele.attr("onclick", "wishlist.add(" + product_id + "), switch_case ($(this),1," + product_id + ")");
        }
    }
</script>
