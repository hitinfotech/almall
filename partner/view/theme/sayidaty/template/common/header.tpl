<!DOCTYPE html>
<?php /* <!--[if IE]><![endif]-->
  <!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
  <!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> */ ?>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="alternate" hreflang="<?php echo $lang ?>" href="/<?php echo $lang ?>/" >
        <?php if ($lang == 'ar') { ?>
            <meta name="language" content="Arabic">
        <?php } else { ?>
            <meta name="language" content="English">
        <?php } ?>

        <script>window.APP = eval('(<?php echo json_encode($js_vars) ?>)');</script>
        <title><?php echo $title; ?></title>
        <?php
        if (isset($no_header) && $no_header == true) {

        } else {
            ?>
            <base href="<?php echo $base; ?>" />
        <?php } ?>
        <?php if ($description) { ?>
            <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>
        <?php if ($keywords) { ?>
            <meta name="keywords" content= "<?php echo $keywords; ?>" />
        <?php } ?>
        <?php foreach ($styles as $style) { ?>
            <link href="<?php echo $style['href']; ?>" charset='utf-8' type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>
        <?php foreach ($links as $link) { ?>
            <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>
        <script>
            var heading_title_default = "";
            var default_heading_meta_title = "";
            var default_heading_h1_title = "";
        </script>
        <?php foreach ($scripts as $script) { ?>
            <script src="<?php echo $script; ?>" type="text/javascript"></script>
        <?php } ?>
        <script type="text/javascript" src="../admin/view/javascript/ckeditor/ckeditor.js"></script>

        <?php /* foreach ($analytics as $analytic) { ?>
          <?php echo $analytic; ?>
          <?php } */ ?>
        <!-- Google Analytics -->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-8935925-4', 'auto');
            ga('require', 'GTM-KN5BTH8');
            ga('send', 'pageview');

        </script>
        <!-- End Google Analytics -->
        <!-- Google Optimize -->
        <style>.async-hide { opacity: 0 !important} </style>
        <script>(function (a, s, y, n, c, h, i, d, e) {
                s.className += ' ' + y;
                h.start = 1 * new Date;
                h.end = i = function () {
                    s.className = s.className.replace(RegExp(' ?' + y), '')
                };
                (a[n] = a[n] || []).hide = h;
                setTimeout(function () {
                    i();
                    h.end = null
                }, c);
                h.timeout = c;
            })(window, document.documentElement, 'async-hide', 'dataLayer', 4000,
                    {'GTM-KN5BTH8': true});</script>
        <!-- End Google Optimize -->
        <!-- Twitter universal website tag code -->
        <script>
            !function (e, t, n, s, u, a) {
                e.twq || (s = e.twq = function () {
                    s.exe ? s.exe.apply(s, arguments) : s.queue.push(arguments);
                }, s.version = '1.1', s.queue = [], u = t.createElement(n), u.async = !0, u.src = '//static.ads-twitter.com/uwt.js',
                        a = t.getElementsByTagName(n)[0], a.parentNode.insertBefore(u, a))
            }(window, document, 'script');
            // Insert Twitter Pixel ID and Standard Event data below
            twq('init', 'nx9nt');
            twq('track', 'PageView');
        </script>
        <!-- End Twitter universal website tag code -->


        <meta property="fb:app_id" content="<?php echo $FACEBOOK_APP_ID; ?>" >
        <meta property="og:type" content="website" >
        <meta property="og:site_name" content="Sayidaty Mall" >
        <meta property="og:url" content="<?php echo $sShareUrl; ?>" >
        <meta property="og:image" content="<?php echo $sShareImage ?>" >
        <meta property="og:image:url" content="<?php echo $sShareImage ?>" >
        <?php /*
          <meta property="og:image:width" content = "1600" />
          <meta property="og:image:height" content = "800" />
          <meta property="article:publisher" content="https://www.facebook.com/Sayidatymall" />
         *
         */ ?>
        <?php /* Crayz Egg */ ?>
        <script type="text/javascript">
            setTimeout(function () {
                var a = document.createElement("script");
                var b = document.getElementsByTagName("script")[0];
                a.src = document.location.protocol + "//script.crazyegg.com/pages/scripts/0013/8552.js?" + Math.floor(new Date().getTime() / 3600000);
                a.async = true;
                a.type = "text/javascript";
                b.parentNode.insertBefore(a, b)
            }, 1);
        </script>
        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
                    fbq('init', '905346899500114'<?php if ($logged) { ?>, {
                        em: '<?php echo $cust_email; ?>',
                                ph: '<?php echo $cust_telephone; ?>',
                                fn: '<?php echo str_replace(array("\'"), '', $cust_firstname . ' ' . $cust_lastname); ?>'
                        } <?php } ?>
                    );
                    fbq('track', 'PageView');
        </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=905346899500114&ev=PageView&noscript=1" /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

    <?php if ($config_otp_bullet) { ?>
        <script type="text/javascript"><!--
            $(document).ready(function () {
                $('body').on('click', '.otp_bullet', function (e) {
                    e.preventDefault();
                    $(this).parent().siblings('a').find('img').attr('src', $(this).attr('thumb'));
                    $('.otp_bullet').removeClass('selected');
                    $(this).addClass('selected');
                });
            });
            //--></script>
        <style type="text/css">
            .otp_bullets {
                float: left;
                position: relative;
                width: 100%;
                z-index: 3;
            }
            .product-thumb .otp_bullets {
                padding: 10px 20px 10px 20px;
            }
            .product-list .product-thumb .otp_bullets {
                padding: 5px 0 10px 0;
            }
            a.otp_bullet {
                float: left;
                margin: 0 3px 3px 0;
                outline: 0;
            }
            .product-thumb .image .otp_bullet img {
                padding: 1px;
                <?php if ($config_otp_bullet_radius) { ?>
                    -webkit-border-radius: <?php echo $config_otp_bullet_radius; ?>px;
                    -moz-border-radius: <?php echo $config_otp_bullet_radius; ?>px;
                    border-radius: <?php echo $config_otp_bullet_radius; ?>px;
                <?php } ?>
            }
            .otp_bullet img {
                border: 1px solid #cccccc;
            }
            .otp_bullet img:hover, .otp_bullet.selected img {
                border: 1px solid #000000;
            }
        </style>
    <?php } ?>
    <?php if (!empty($_GET)): ?>
        <?php if ($_GET['route'] === 'addproduct'): ?>

            <link rel="stylesheet" href="view/javascript/instaloader2/css/style.css">
            <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
            <link rel="stylesheet" href="view/javascript/instaloader2/css/jquery.fileupload.css">
            <link rel="stylesheet" href="view/javascript/instaloader2/css/jquery.fileupload-ui.css">
            <!--link rel="stylesheet" href="view/javascript/instaloader2/jquery-ui-1.11.4/jquery-ui.min.css"-->

            <style>
                .alert {
                    display: none;
                }
            </style>

        <?php elseif ($_GET['route'] === 'catalog/product'): ?>
            <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
            <link rel="stylesheet" href="view/javascript/instaloader2/css/gallery.css">
        <?php endif; ?>
    <?php endif; ?>
</head>
<body class="<?php echo $class; ?>" data-ng-app="app">
    <?php if (isset($no_header) && $no_header == true) { ?>
    <?php } else { ?>
        <div class="modal fade" id="logIn" tabindex="-1" role="dialog" aria-labelledby="login">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="login"><?php echo $text_login_header; ?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group d-break-space">
                            <a href="<?php echo $fbLoginUrl ?>">
                                <button class="btn btn-block btn-facebook"> <i class="fa fa-facebook"></i><?php echo $text_facebook_login; ?></button>
                            </a>
                        </div>
                        <div class="regForm mb20" role="form">
                            <input id="redirect" type="hidden" name="redirect" value="<?php echo $redirect ?>" />
                            <p class="d-break-space text-center"><?php echo $text_or; ?></p>
                            <div id="loginErrorMsg" class="error"></div>
                            <div class="form-group">
                                <label><?php echo $text_email; ?> *</label>
                                <input type="email" name="email" value="" placeholder="<?php echo $text_email; ?>" id="txtEmail"  class="form-control" required aria-required="true">
                            </div>
                            <div class="form-group">
                                <label><?php echo $text_password; ?> *</label>
                                <input type="password" name="password" placeholder="<?php echo $text_password; ?>" id="txtPassword" class="form-control" minlength="5" required aria-required="true">
                            </div>
                            <div class='form-group'>
                                <input type="checkbox" name="remember_me" id="remember_me" checked>
                                <label><?php echo $text_remeber_me; ?></label>
                            </div>

                            <p><a href="<?php echo $forgot_password ?>"> <?php echo $text_forgot_password; ?> </a></p>
                            <p><a href="<?php echo $register_url ?>"> <?php echo $text_new_user; ?> </a></p>

                            <button class="btn btn-primary" id="btnLogin"><?php echo $text_login; ?></button>
                            <button class="btn btn-default" data-dismiss="modal"><?php echo $text_close; ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <header>
        <div class="top-bar hidden-xs hidden-sm">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-7">
                        <!-- <div class="dropdown pull-right">
                             <button class="btn btn-xs dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-expanded="true"> <?php echo $text_more; ?> <span class="caret"></span></button>
                             <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu3">
                                 <li><a href="http://sayidaty.net"><?php echo $text_sayidaty_dot_net_site; ?></a></li>
                                 <li><a href="http://kitchen.sayidaty.net"><?php echo $text_sayidaty_kitchen_site; ?></a></li>
                                 <li><a href="http://www.sayidaty.net/fashion/%D8%A3%D8%B2%D9%8A%D8%A7%D8%A1"><?php echo $text_sayidaty_fashion_site; ?></a></li>
                             </ul>
                         </div> -->
                        <?php //echo $country; ?>
                        <?php // echo $language; ?>
                        <div class="header-phone"> <a href="tel:8004330033"> <i class="fa fa-phone" aria-hidden="true"></i> 800 433 0033</a> </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-5">
                        <ul class="user-guest text-left">
                            <?php if (isset($tsSupportCenterLink)) { ?>
                                <li><a href="<?php echo $tsSupportCenterLink; ?>" title="<?php echo $text_ts_header; ?>"><i class="fa fa-ticket"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_ts_header; ?></span></a></li>
                            <?php } ?>
                            <?php if (!$logged) { ?>
                                <li>
                                    <a href="<?php echo $partner_login; ?>"><span class="hidden-xs"><?php echo $text_login_seller ?></span></a>
                                </li>
                                <li>
                                    <a href="<?php echo $partner_register; ?>"><span class="hidden-xs"><?php echo $text_register_seller ?></span></a>
                                </li>
                                <li>
                                    <a href="<?php echo $howtosell; ?>"><span class="hidden-xs"><?php echo $text_howtosell ?></span></a>
                                </li>

                                <!--  <li class="login-link">
                                      <a href="<?php echo $login_url; ?>" data-toggle="modal" data-target="#logIn">
                                          <span class="hidden-xs"><?php echo $text_account; ?></span>
                                      </a>
                                  </li>
                                  <li class="tracking">
                                      <a href="<?php echo $tracking; ?>">
                                          <span class="hidden-xs"><?php echo $text_tracking; ?></span>
                                      </a>
                                  </li> -->
                            <?php } ?>
                            <?php if ($logged) { ?>
                                <li class="rel-position">
                                    <button type="button" class="btn country-flag btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php echo $text_account ?>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <?php if (isset($marketplace_status) && $marketplace_status && $logged && $chkIsPartner) { ?>
                                                                                                                        <!-- <li><a href="<?php echo $mp_seller_account; ?>"><?php echo $text_seller_account; ?></a></li> -->
                                            <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('profile', $marketplace_allowed_account_menu)) { ?>
                                                <li><a href="<?php echo $mp_profile; ?>"><?php echo $text_my_profile; ?></a></li>
                                            <?php } ?>
                                            <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('dashboard', $marketplace_allowed_account_menu)) { ?>
                                                <li><a href="<?php echo $mp_dashboard; ?>"><?php echo $text_dashboard; ?></a></li>
                                            <?php } ?>
                                            <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('orderhistory', $marketplace_allowed_account_menu)) { ?>
                                                <li><a href="<?php echo $mp_orderhistory; ?>"><?php echo $text_orderhistory; ?></a></li>
                                                <li><a href="<?php echo $mp_orderhistory_pending; ?>"><?php echo $text_orderhistory_pending; ?></a></li>
                                            <?php } ?>
                                            <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('mp_rma', $marketplace_allowed_account_menu)) { ?>
                                                <?php if ($wk_rma_status) { ?>
                                                    <li><a href="<?php echo $rma_rma ?>"><?php echo $text_rma_rma; ?></a></li>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('transaction', $marketplace_allowed_account_menu)) { ?>
                                                <li><a href="<?php echo $mp_transaction; ?>"><?php echo $text_transaction; ?></a></li>
                                            <?php } ?>
                                            <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('productlist', $marketplace_allowed_account_menu)) { ?>
                                                <li><a href="<?php echo $mp_productlist; ?>"><?php echo $text_productlist; ?></a></li>
                                                <li><a href="<?php echo $mp_addproduct; ?>"><?php echo $text_addproduct; ?></a></li>
                                            <?php } ?>
                                            <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('download', $marketplace_allowed_account_menu)) { ?>
                                                <li><a href="<?php echo $mp_download; ?>"><?php echo $text_download; ?></a></li>
                                            <?php } ?>
                                            <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('manageshipping', $marketplace_allowed_account_menu)) { ?>
                                                <li><a href="<?php echo $mp_add_shipping_mod; ?>"><?php echo $text_wkshipping; ?></a></li>
                                            <?php } ?>
                                            <li><a href="<?php echo $mp_seller_discounts; ?>"><?php echo $text_seller_discounts; ?></a></li>
                                            <li><a href="<?php echo $mp_seller_invoices; ?>"><?php echo $text_seller_invoices; ?></a></li>
                                            <li role="presentation" class="divider"></li>
                                        <?php } else { ?>
                                            <li><a href="<?php echo $account ?>"><?php echo $text_account_drop ?></a></li>
                                            <li><a href="<?php echo $wishlist ?>"><?php echo $text_wishlist ?></a></li>
                                        <?php } ?>
                                        <?php if($seller_contract){  ?>
                                           <li><a href="<?php echo $seller_contract ?>"><?php echo $text_seller_contract ?></a></li>
                                        <?php }  ?>
                                        <li><a href="<?php echo $edit_info ?>"><?php echo $text_edit_info ?></a></li>
                                        <li><a href="<?php echo $logout ?>"><?php echo $text_logout ?></a></li>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
              </div>
            <div class="container">
                <div class="rel-position">
                    <div class="logo-wapper">
                        <div>
                            <button type="button" class="navbar-toggle collapsed" id="hideshow"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                        </div>
                        <a href="<?php echo $home; ?>"><img src="<?php echo $logo ?>" class="main-logo"></a>
                    </div>
                    <?php //echo $search ?>
                    <?php /* echo $cart; */ ?>
                </div>
            </div>
            <?php echo $menu ?>
    </header>
    <?php if (!empty($breadcrumbs)) { ?>
        <?php if ($ismobile) { ?>
            <div class="breadcrumb-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                                <?php $breadcrumbCounter = 1; ?>
                                <?php foreach ($breadcrumbs as $crumb) { ?>
                                    <?php if (count($breadcrumbs) > $breadcrumbCounter) { ?>
                                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                            <a itemprop="item" href="<?php echo $crumb['href']; ?>">
                                                <span itemprop="name"><?php echo $crumb['text']; ?></span>
                                            </a>
                                            <meta itemprop="position" content="<?php echo $breadcrumbCounter; ?>" />
                                        </li>
                                    <?php } else { ?>
                                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="">
                                            <a itemprop="item" href="<?php echo $crumb['href']; ?>">
                                                <span itemprop="name"><?php echo $crumb['text']; ?></span>
                                            </a>
                                            <meta itemprop="position" content="<?php echo $breadcrumbCounter; ?>" />
                                        </li>
                                    <?php } ?>
                                    <?php $breadcrumbCounter++; ?>
                                <?php } ?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="breadcrumb-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                                <?php $breadcrumbCounter = 1; ?>
                                <?php foreach ($breadcrumbs as $crumb) { ?>
                                    <?php if (count($breadcrumbs) > $breadcrumbCounter) { ?>
                                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                            <a itemprop="item" href="<?php echo $crumb['href']; ?>">
                                                <span itemprop="name"><?php echo $crumb['text']; ?></span>
                                            </a>
                                            <meta itemprop="position" content="<?php echo $breadcrumbCounter; ?>" />
                                        </li>
                                    <?php } else { ?>
                                        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active">
                                            <a itemprop="item" href="<?php echo $crumb['href']; ?>">
                                                <span itemprop="name"><?php echo $crumb['text']; ?></span>
                                            </a>
                                            <meta itemprop="position" content="<?php echo $breadcrumbCounter; ?>" />
                                        </li>
                                    <?php } ?>
                                    <?php $breadcrumbCounter++; ?>
                                <?php } ?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
<?php } ?>
