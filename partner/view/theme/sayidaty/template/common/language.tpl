<?php $language_name = $text_language ?>
<?php if (count($languages) > 1) { ?>
    <div class="inline-div country-flag">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
            <div class="btn-group">
                <?php /* <button class="country-flag  dropdown-toggle" data-toggle="dropdown">
                  <?php foreach ($languages as $language) { ?>
                  <?php if ($language['code'] == $code) { ?>
                  <?php $language_name = $language['name']; ?>
                  <img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>">
                  <?php } ?>
                  <?php } ?>
                  <span class="hidden-xs hidden-sm hidden-md"><?php echo $language_name; ?></span> <i class="fa fa-caret-down"></i>
                  </button>
                  <ul class="dropdown-menu"> */ ?>
                <?php foreach ($languages as $language) { ?>
                    <?php if ($language['code'] != $code) { ?>
                        <a class="<?php echo $language['code']?>" href="<?php echo $language['code']; ?>"><?php echo $language['name']; ?></a>
                    <?php } ?>
                <?php } ?>
                <?php /* </ul> */ ?>
            </div>
            <input type="hidden" name="code" value="" />
            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
        </form>
    </div>
<?php } ?>
