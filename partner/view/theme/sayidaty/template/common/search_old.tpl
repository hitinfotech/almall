<?php /* <script src="//cdn.jsdelivr.net/jquery/2.1.4/jquery.min.js"></script> */ ?>
<script src="//cdn.jsdelivr.net/autocomplete.js/0/autocomplete.jquery.min.js"></script>
<script src="//cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>

<div class="col-lg-5 col-md-5 col-sm-5 col-xs-10">
    <div class="search-bar">
        <form id="commonHeaderSearch" name="commonHeaderSearch" method="get" action="<?php echo $action; ?>">
        <div id="search" class="input-group">
            <input type="text" id="head-search" name="q" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg"  data-ng-model="query" />
            <span class="input-group-btn">
                <button type="submit" class="btn btn-primary btn-lg search-btn"><i class="fa fa-search"></i></button>
            </span>

            <script>
                var client = algoliasearch(window.APP.algolia.APPID, window.APP.algolia.SEARCH_API_KEY);
                var index = client.initIndex('product');
                $('#head-search').autocomplete({hint: false}, [{
                        source: function (q, cb) {
                            index.search(q, {hitsPerPage: 10}, function (error, content) {
                                if (error) {
                                    cb([]);
                                    return;
                                }
                                cb(content.hits, content);
                            });
                        },
                        displayKey: 'name.' + window.APP.lang,
                        templates: {
                            suggestion: function (suggestion) {
                                return "<a href='" + suggestion.url + "'>" +
                                        suggestion._highlightResult.name.<?php echo $lang_code ?>.value +
                                        "</a>";
                            }
                        }
                    }]);
            </script>    
        </div>
        </form>
    </div>
</div>