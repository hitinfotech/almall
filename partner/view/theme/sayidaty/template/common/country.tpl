<?php if (count($countries) > 1) { ?>
    <div class="btn-group flag-container">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <button class="country-flag  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php foreach ($countries as $country) { ?>
                    <?php if ($country['code'] == $code) { ?>
                        <?php $text_country = $country['name'] ?>
                        <img src="<?php echo $country['flag']; ?>" alt="<?php echo $country['name']; ?>" title="<?php echo $country['name']; ?>">
                    <?php } ?>
                <?php } ?>
                <?php echo $text_country; ?><span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <?php foreach ($countries as $country) { ?>
                    <li>
                        <a href="<?php echo $country['redirect'] ?>">
                            <img src="<?php echo $country['flag'] ?>" alt="<?php echo $country['name']; ?>" title="<?php echo $country['name']; ?>" />
                            <?php echo $country['name']; ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <input type="hidden" name="code" value="" />
            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
        </form>
    </div>
<?php } ?>