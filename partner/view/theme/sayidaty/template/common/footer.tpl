<footer>
    <div class="value-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="value-item">
                        <img src="<?php echo HTTPS_IMAGE_S3 ?>icon/brand-tra.png">
                        <?php echo $text_brands ?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="value-item">
                        <img src="<?php echo HTTPS_IMAGE_S3 ?>icon/ship-tra.png">
                        <?php echo $text_freeshipping; ?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="value-item">
                        <img src="<?php echo HTTPS_IMAGE_S3 ?>icon/calendar-tra.png">
                        <?php echo $text_return; ?>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="value-item">
                        <img src="<?php echo HTTPS_IMAGE_S3 ?>icon/cash-tra.png">
                        <?php echo $text_cod; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <h4 class="footer-item-collapse"><?php echo $text_shopping ?><span class="pull-left hidden visible-xs"><em class="fa fa-chevron-down" aria-hidden="true"></em></span> </h4>
                        <ul class="footer-ul-collapse" id="footer-ul-collapse">
                            <li><a href="<?php echo $categories[0]['href'] ?>"><?php echo $categories[0]['name'] ?></a></li>
                            <li><a href="<?php echo $categories[1]['href'] ?>"><?php echo $categories[1]['name'] ?></a></li>
                            <li><a href="<?php echo $newarrival; ?>"><?php echo $text_newarrival; ?></a></li>
                            <?php /* <li><a href=""><?php echo $text_bestseller; ?></a></li>
                              <li><a href=""><?php echo $text_designer; ?></a></li>
                              <li><a href=""><?php echo $text_sizes; ?></a></li> */ ?>
                        </ul>
                    </div>
                    <?php if ($informations) { ?>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <h4 class="footer-item-collapse"><?php echo $text_sayidaty_mall /* <h5><?php //echo $text_information; ?></h5> */ ?><span class="pull-left hidden visible-xs"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></h4>
                            <ul class="footer-ul-collapse">
                                <?php foreach ($informations as $information) { ?>
                                    <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                                <?php } ?>
                                <?php /*  <li><a href=""> جديد على سيدتي مول</a></li>
                                  <li><a href="">كيف تتسوقين</a></li> */ ?>
                            </ul>
                        </div>
                    <?php } ?>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <h4><?php echo $text_do_you_have_questions; ?></h4>
                        <p><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></p>
                        <div class="contact-text d-break-space"><span class="icon"><em class="fa fa-phone" aria-hidden="true"></em></span>
                            <div class="contact-text">
                                <span class="phone-txt"><a href="tel:8004330033">800 433 0033</a></span><br>
                                <span><?php echo $text_job_times ?></span></div>
                        </div>
                        <h4 class="d-break-space"><?php echo $text_follow_us; ?>:</h4>
                        <ul class="footer-links">
                            <li><a target="_blank" href="http://facebook.com/Sayidatymall"> <i class=" fa fa-facebook"> &nbsp; </i> </a></li>
                            <li><a target="_blank" href="http://twitter.com/sayidaty_mall"> <i class="fa fa-twitter"> &nbsp; </i> </a></li>
                            <li><a target="_blank" href="https://www.youtube.com/channel/UCw0FpmUg229kGcDqKKVj-Aw"> <i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/sayidatymall/"> <i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="newsletter-footer">
                            <h4><?php echo $text_subscribe ?></h4>
                            <p><?php echo $text_subscribe_desc ?></p>
                            <input id="email_footer" type="email" class="form-control" placeholder="<?php echo $text_email ?>">
                            <div class="form-group text-center d-break-space">
                                <input type="button" value="<?php echo $text_female ?>" class="btn btn-secondary btn-space" onclick="do_subscrip(0, document.getElementById('email_footer').value, 2);">
                                <input type="button" value="<?php echo $text_male ?>" class="btn btn-secondary btn-space" onclick="do_subscrip(1, document.getElementById('email_footer').value, 2);">
                            </div>
                            <div id="error_popup_message_footer" class="form-group text-center d-break-space"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php if ($addthis) { ?>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-576f75f44906dc88"></script>
    <script type="text/javascript">
        var addthis_share = {

            url_transforms : {
                add: {
                    utm_source: '{{code}}',
                    utm_medium: 'post',
                    utm_campaign: 'socialshare'
                },
                shorten: {
                    twitter: 'bitly'
                }

            }
        };
    </script>
<?php } ?>
<!-- BEGIN EFFECTIVE MEASURE CODE -->
<!-- COPYRIGHT EFFECTIVE MEASURE -->
<script type="text/javascript">
    (function () {
        var em = document.createElement('script');
        em.type = 'text/javascript';
        em.async = true;
        em.src = ('https:' == document.location.protocol ? 'https://me-ssl' : 'http://me-cdn') + '.effectivemeasure.net/em.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(em, s);
    })();
</script>
<noscript><img src="https://me.effectivemeasure.net/em_image" alt="" style="position:absolute; left:-5px;" /></noscript>
<!--END EFFECTIVE MEASURE CODE -->

<?php /* <script type="text/javascript">
  var google_tag_params = {
  ecomm_prodid: 'REPLACE_WITH_VALUE',
  ecomm_pagetype: 'REPLACE_WITH_VALUE',
  ecomm_totalvalue: 'REPLACE_WITH_VALUE',
  };
  </script> */ ?>

<script type="text/javascript">
    $(".footer-item-collapse").click(function () {
        var width = $(window).width();
        if (width < 768) {
            $(this).next(".footer-ul-collapse").toggle(500);
        }
    })
    window.onresize = function (event) {
        var width = $(window).width();
        if (width >= 768) {
            $(".footer-ul-collapse").each(function () {
                $(this).attr('style', 'display:block');
            });
        } else if (width < 768) {
            $(".footer-ul-collapse").each(function () {
                $(this).attr('style', 'display:none');
            });
        }
    }
</script>


        <?php if (isset($_GET)): ?>
        <?php if (isset($_GET['route'])): ?>
        <?php if ($_GET['route'] === 'addproduct' || isset($_GET['product_id'])): ?>


            <!-- The blueimp Gallery widget -->
            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                <div class="slides"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="close">×</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
            </div>

            <!-- The template to display files available for upload -->
            <script id="template-upload" type="text/x-tmpl">
            {% for (var i=0, file; file=o.files[i]; i++) { %}
                <tr class="template-upload fade">
                    <td>
                        <span class="preview"></span>
                    </td>
                    <td>
                        <p class="name">{%=file.name%}</p>
                        <strong class="error text-danger"></strong>
                    </td>
                    <td>
                        <p class="size">Processing...</p>
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                    </td>
                    <td>
                        {% if (!i && !o.options.autoUpload) { %}
                            <button class="btn btn-primary start" disabled>
                                <i class="glyphicon glyphicon-upload"></i>
                                <span>Start</span>
                            </button>
                        {% } %}
                        {% if (!i) { %}
                            <button class="btn btn-warning cancel">
                                <i class="glyphicon glyphicon-ban-circle"></i>
                                <span>Cancel</span>
                            </button>
                        {% } %}
                    </td>
                </tr>
            {% } %}
            </script>
            <!-- The template to display files available for download -->
            <script id="template-download" type="text/x-tmpl">
            {% for (var i=0, file; file=o.files[i]; i++) { %}
                <tr class="template-download fade">
                    <td>
                        <span class="preview">
                            {% if (file.thumbnailUrl) { %}
                                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                            {% } %}
                        </span>
                    </td>
                    <td>
                        <p class="name">
                            {% if (file.url) { %}
                                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                            {% } else { %}
                                <span>{%=file.name%}</span>
                            {% } %}
                        </p>
                        {% if (file.error) { %}
                            <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                        {% } %}
                    </td>
                    <td>
                        <span class="size">{%=o.formatFileSize(file.size)%}</span>
                    </td>
                    <td>
                        {% if (file.deleteUrl) { %}
                            <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                <i class="fa fa-trash"></i>
                                <span>Delete</span>
                            </button>
                        {% } else { %}
                            <button class="btn btn-warning cancel">
                                <i class="glyphicon glyphicon-ban-circle"></i>
                                <span>Cancel</span>
                            </button>
                        {% } %}
                    </td>
                </tr>
            {% } %}
            </script>


            <script src="view/javascript/instaloader2/js/vendor/jquery.ui.widget.js"></script>
            <!-- The Templates plugin is included to render the upload/download listings -->
            <script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
            <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
            <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
            <!-- The Canvas to Blob plugin is included for image resizing functionality -->
            <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
            <!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
            <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
            <!-- blueimp Gallery script -->
            <script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
            <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
            <script src="view/javascript/instaloader2/js/jquery.iframe-transport.js"></script>
            <!-- The basic File Upload plugin -->
            <script src="view/javascript/instaloader2/js/jquery.fileupload.js"></script>
            <!-- The File Upload processing plugin -->
            <script src="view/javascript/instaloader2/js/jquery.fileupload-process.js"></script>
            <!-- The File Upload image preview & resize plugin -->
            <script src="view/javascript/instaloader2/js/jquery.fileupload-image.js"></script>
            <!-- The File Upload audio preview plugin -->
            <script src="view/javascript/instaloader2/js/jquery.fileupload-audio.js"></script>
            <!-- The File Upload video preview plugin -->
            <script src="view/javascript/instaloader2/js/jquery.fileupload-video.js"></script>
            <!-- The File Upload validation plugin -->
            <script src="view/javascript/instaloader2/js/jquery.fileupload-validate.js"></script>
            <!-- The File Upload user interface plugin -->
            <script src="view/javascript/instaloader2/js/jquery.fileupload-ui.js"></script>
            <!-- https://github.com/RubaXa/Sortable -->
            <script src="//cdnjs.cloudflare.com/ajax/libs/Sortable/1.4.2/Sortable.min.js"></script>



            <!-- The main application script -->
            <script src="view/javascript/instaloader2/js/main.js"></script>

        <?php elseif ($_GET['route'] === 'addproduct'): ?>


            <!-- InstaLoader (http://webredesign.info): Image Gallery Template -->
            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                <div class="slides"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="close">×</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
            </div>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
            <script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
            <script src="view/javascript/instaloader2/js/gallery.js"></script>

        <?php endif; ?>
        <?php endif; ?>
        <?php endif; ?>

</body>
</html>
