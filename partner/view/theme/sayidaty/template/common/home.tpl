<?php echo $header; ?>
<div class="content-padding">
    <div class="container">
        <?php echo $featured; ?>
        <div class="row">
            <div class="col-md-12">
                <div class="mobile-ad">
                    <div class="mpu250-container ">
                        <div class="mpu250-ad">
                            <div class="mpu250-mobile">
                                <div id='div-gpt-ad-1446377269862-0-oop'>
                                    <script type='text/javascript'>
                                        googletag.display('div-gpt-ad-1446377269862-0-oop');
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sliders -->
        <div class="row">
            <?php echo $latestslider ?>
          <!--  <div class="col-md-4 col-sm-12">
                <div class="mpu250-container mt55 desktop-ad">
                    <div class="mpu250-ad">
                        <div class="mpu250-desktop">
                            <div id='div-gpt-ad-1446377269862-1'>
                                <script type='text/javascript'>
                                    googletag.display('div-gpt-ad-1446377269862-2');
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

            <?php echo $bestseller ?>
        </div>
        <!-- /Sliders -->
        <?php if (!empty($brands)) { ?>
            <div class="brand6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="inner-title text-center">
                            <h3><?php echo $text_selected_brands ?></h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php foreach ($brands as $row) { ?>
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                            <div class="brand-tile">
                                <a href="<?php echo $row['href'] ?>">
                                    <div class="brand-img"><img src="<?php echo $row['thumb'] ?>"></div>
                                    <div class="bran-name">
                                        <?php echo $row['name'] ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="text-center break-space">
                    <a href="<?php echo $brand_more ?>" class="btn btn-primary btn-more"><?php echo $text_more ?></a>
                </div>
            </div>
        <?php } ?>
        <?php /* if ($categories) { ?>
          <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">

          <div class="box<?php echo $row['counter'] ?>">
          <div class="">
          <img src="<?php echo $row['thumb'] ?>">
          <div class="box<?php echo $row['counter'] ?>-text">
          <div class="content">
          <h3><?php echo $row['name'] ?></h3>
          <a href="<?php echo $row['href'] ?>" class="btn btn-secondary btn-lg"><?php echo $text_category_shop_now ?></a>
          </div>
          </div>
          </div>
          </div>

          </div>
          <?php //} ?>
          <?php //} ?>
          </div>
          <?php } */ ?>

        <?php //die(var_dump($categories)) ?>
        <?php if ($categories && count($categories) == 4) { ?>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="box1">
                        <a href="<?php echo $categories[1]['href'] ?>">
                            <div class="">
                                <img src="<?php echo $categories[1]['thumb'] ?>">
                                <div class="box1-text-new">
                                    <div class="content"> <?php /* <h3><?php echo $categories[1]['name']?></h3> */ ?>
                                        <p><?php echo $categories[1]['name'] ?><?php //echo $text_category_shop_now       ?></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="box2">
                        <a href="<?php echo $categories[2]['href'] ?>">
                            <div class="">
                                <img src="<?php echo $categories[2]['thumb'] ?>">
                                <div class="box2-text-new">
                                    <div class="content">
                                        <?php /* <h3><?php echo $categories[2]['name']?></h3> */ ?>
                                        <p><?php echo $categories[2]['name'] ?><?php //echo $text_category_shop_now       ?></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="box3">
                        <a href="<?php echo $categories[3]['href'] ?>">
                            <div class="">
                                <img src="<?php echo $categories[3]['thumb'] ?>">
                                <div class="box3-text-new">
                                    <div class="content">
                                        <?php /* <h3><?php echo $categories[3]['name']?></h3> */ ?>
                                        <p><?php echo $categories[3]['name'] ?><?php //echo $text_category_shop_now       ?></p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="box4">
                        <a href="<?php echo $categories[4]['href'] ?>">
                            <div class="">
                                <img src="<?php echo $categories[4]['thumb'] ?>">
                                <div class="box4-text-new">
                                    <div class="content"> <?php /* <h3><?php echo $categories[4]['name']?></h3> */ ?>
                                        <p><?php echo $categories[4]['name'] ?><?php //echo $text_category_shop_now       ?></p>
                                    </div>
                                </div>
                            </div>
                        </a>

                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

</div>
<?php echo $footer; ?>
