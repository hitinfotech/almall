<div class="search-wrapper">
    <div class="search-mobile">
        <div class="search-btn"><i class="fa fa-search search-icn"></i></div>
    </div>
    <div class="search-bar" id="main-seach">
        <form id="commonHeaderSearch" name="commonHeaderSearch" method="get" action="<?php echo $action; ?>">
            <div class="input-group">
                <div class="input-select-btn">
                    <select class="form-control" id="htype" name="htype" onchange="testSearch(this);">
                        <?php foreach ($search_list as $ids) { ?>
                            <option id="search-type-<?php echo $ids['index'] ?>" value="<?php echo $ids['index'] ?>" datalink="<?php echo $ids['link'] ?>" <?php echo $htype == $ids['index'] ? 'selected' : '' ?> class="search-cat-icon search-cat-icon-<?php echo $ids['index'] ?>"><?php echo $ids['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <input id="head-search" name="q" type="text" class="form-control" value= "<?php echo $search ?>" placeholder="<?php echo $text_search ?>">
                <div class="input-group-btn"> 
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-search search-icn"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    function testSearch(sel) {
        document.getElementById("commonHeaderSearch").action = sel.options[sel.selectedIndex].getAttribute('datalink');
    }
</script>
