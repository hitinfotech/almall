<a class="exit-menu"> </a>
<div class="menu hide visible-sm visible-xs" id="main-menu">
    <div class="main-navbar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                  <ul class="hide visible-sm visible-xs menu-info">
                     <?php if ($logged) { ?>
                          <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('profile', $marketplace_allowed_account_menu) ) { ?>
                         <li><a href="<?php echo $mp_profile; ?>"><?php echo $text_my_profile; ?></a></li>
                         <?php } ?>
                         <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('dashboard', $marketplace_allowed_account_menu)) { ?>
                         <li><a href="<?php echo $mp_dashboard; ?>"><?php echo $text_dashboard; ?></a></li>
                         <?php } ?>
                         <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('orderhistory', $marketplace_allowed_account_menu)) { ?>
                         <li><a href="<?php echo $mp_orderhistory; ?>"><?php echo $text_orderhistory; ?></a></li>
                         <li><a href="<?php echo $mp_orderhistory_pending; ?>"><?php echo $text_orderhistory_pending; ?></a></li>
                         <?php } ?>
                         <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('mp_rma', $marketplace_allowed_account_menu)) { ?>
                         <?php if ($wk_rma_status) { ?>
                         <li><a href="<?php echo $rma_rma ?>"><?php echo $text_rma_rma; ?></a></li>
                         <?php } ?>
                         <?php } ?>
                         <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('transaction', $marketplace_allowed_account_menu)) { ?>
                         <li><a href="<?php echo $mp_transaction; ?>"><?php echo $text_transaction; ?></a></li>
                         <?php } ?>
                         <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('productlist', $marketplace_allowed_account_menu)) { ?>
                         <li><a href="<?php echo $mp_productlist; ?>"><?php echo $text_productlist; ?></a></li>
                         <li><a href="<?php echo $mp_addproduct; ?>"><?php echo $text_addproduct; ?></a></li>
                         <?php } ?>
                         <li><a href="<?php echo $mp_seller_discounts ?>"><?php echo $text_seller_discounts ?></a></li>
                         <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('download', $marketplace_allowed_account_menu)) { ?>
                         <li><a href="<?php echo $mp_download; ?>"><?php echo $text_download; ?></a></li>
                         <?php } ?>
                         <?php if (isset($marketplace_allowed_account_menu) && $marketplace_allowed_account_menu && in_array('manageshipping', $marketplace_allowed_account_menu)) { ?>
                         <li><a href="<?php echo $mp_add_shipping_mod; ?>"><?php echo $text_wkshipping; ?></a></li>
                         <?php } ?>
                         <li role="presentation" class="divider"></li>

                         <li><a href="<?php echo $edit_info ?>"><?php echo $text_edit_info ?></a></li>
                         <li><a href="<?php echo $logout ?>"><?php echo $text_logout ?></a></li>
                          <?php } else { ?>
                         <li><a href="<?php echo $login; ?>" ><?php echo $text_account; ?></a></li>
                         <li><a href="<?php echo $tracking; ?>"><?php echo $text_tracking; ?></a></li>

                     <?php } ?>
                     <li>
                         <div class="header-phone">
                             <a href="tel:8004330033"> <i class="fa fa-phone" aria-hidden="true"> </i> 800 433 0033</a></div>
                     </li>
                  </ul>
                </div>
            </div>
        </div>
    </div>
</div>
