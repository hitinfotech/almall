<?php echo $header; ?>
<div class="content-padding">
    <div class="container">
        <div class="row">
            <?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
                <?php $class = ''; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>">
                <?php echo $content_top; ?>


                <div class="col-md-12">
                    <div class="seller-cover">
                        <img src="<?php echo $partner['companybanner'] ?>" alt="" class="full-img">
                    </div>
                    <div class="seller-info">
                        <div class="seller-prof-img">
                            <img src="<?php echo $partner['avatar'] ?>">
                        </div>
                        <div class="seller-text">
                            <h1><?php echo $partner['screenname'] ?></h1>
                            <?php echo $partner['shortprofile'] ?>
                            <?php if ((isset($partner['twitterid']) && !empty($partner['twitterid'])) || (isset($partner['facebookid']) && !empty($partner['facebookid']))) {?>
                            <div >
                                <strong><?php echo $text_shops_official_links; ?>:</strong>
                                    <?php if ($partner['facebookid']) { ?>
                                        <a target="_blank" href="<?php echo $partner['facebookid'] ?>" class="facebook-icn"> <i class="fa fa-facebook"></i></a>
                                    <?php } ?>
                                    <?php if ($partner['twitterid']) { ?>
                                        <a target="_blank" href="<?php echo $partner['twitterid'] ?>" class="twitter-icn"> <i class="fa fa-twitter"></i></a>
                                    <?php } ?>
                            </div>
                              <?php } ?>


                            <?php if ($logged){ ?>
                                <?php if (in_array($partner['customer_id'], $fav_sellers)) { ?>
                                  <p><button class="btn btn-default" onclick="seller.remove(<?php echo $partner['customer_id']; ?>), switch_case ($(this),2,<?php echo $partner['customer_id']; ?>)" ><?php echo $unfollow_seller ?></button></p>

                                <?php } else { ?>
                                  <p><button class="btn btn-default" onclick="seller.add(<?php echo $partner['customer_id']; ?>), switch_case ($(this),1,<?php echo $partner['customer_id']; ?>)"><?php echo $follow_seller ?></button></p>

                                <?php } ?>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div>
                        <ul class="nav nav-tabs seller-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#product" aria-controls="product" role="tab" data-toggle="tab"><?php echo $text_products?></a></li>
                            <!--<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">الملف الشخصي</a></li>
                            <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">التقييم</a></li>
                            -->
                        </ul>
                    </div>
                    <div class="filter-mrg">
                        <div class="form-inline inline-form-margin break-space">
                            <div class="form-group">
                                <input type="text" class="form-control" id="filterFilter" value="<?php echo $filter_filter ?>"  placeholder="<?php echo $text_search_site; ?>">
                            </div>
                            <div class="form-group">
                                <select id="filter_main_category_products" name='filter_main_category_products' class="form-control">
                                    <option value='0' selected="selected"><?php echo $text_main_categories ?></option>
                                    <?php foreach ($seller_main_categories as $row) { ?>
                                        <option <?php echo $main_category_id == $row['category_id'] ? "selected" : "" ?> value="<?php echo $row['category_id'] ?>" ><?php echo $row['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select id="filter_category_products" name='filter_category_products' class="form-control">
                                    <option class='category_option' value='0' selected="selected"><?php echo $text_all_products ?></option>
                                    <?php foreach ($seller_categories[0] as $row) { ?>
                                        <option class='category_option' <?php echo $category_id == $row['category_id'] ? "selected" : "" ?> value="<?php echo $row['category_id'] ?>" ><?php echo $row['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <?php /* <div class="form-group">
                              <select class="form-control">
                              <option selected="selected"><?php echo $text_brands_shop_now; ?></option>
                              <option><?php echo $text_brands_all_products; ?></option>
                              </select>
                              </div> */ ?>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="product">
                            <?php if (!empty($products)) { ?>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="row" id='products'>
                                            <?php foreach ($products as $product) { ?>
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                    <div class="tile">
                                                        <div class="fave-heart">
                                                          <?php if (!$product['logged']) { ?>
                                                              <a href="<?php echo $login_url; ?>" data-toggle="modal" data-target="#logIn"><span class="hidden-xs"></span><i class="fa fa-heart"></i></a>
                                                          <?php } else { ?>
                                                              <?php if (in_array($product['product_id'], $wishlist_ids)) { ?>
                                                                  <a href="Javascript:void(0);" onclick="wishlist.remove(<?php echo $product['product_id']; ?>), switch_case2 ($(this),2,<?php echo $product['product_id']; ?>)" class="active"><i class="fa fa-heart"></i></a>
                                                              <?php } else { ?>
                                                                  <a href="Javascript:void(0);" onclick="wishlist.add(<?php echo $product['product_id']; ?>), switch_case2 ($(this),1,<?php echo $product['product_id']; ?>)"><i class="fa fa-heart"></i></a>
                                                              <?php } ?>
                                                          <?php } ?>
                                                        </div>
                                                        <div class="tile-img">
                                                            <a href="<?php echo $product['href']; ?>"><img class="proimg" src="<?php echo $product['thumb']; ?>"></a>
                                                            <?php if ((float) $product['price'] > 0) { ?>
                                                                <span class="shopping-bar" style="cursor:pointer;" >
                                                                    <?php echo $button_cart; ?>
                                                                </span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="tile-text">
                                                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                                        </div>
                                                        <div class="tile-price">
                                                            <?php if ((float) $product['price'] > 0) { ?>
                                                                <p class="price">
                                                                    <?php if (!$product['special']) { ?>
                                                                        <?php echo $product['price']; ?>
                                                                    <?php } else { ?>
                                                                        <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                                                    <?php } ?>
                                                                    <?php if ((float) $product['tax']) { ?>
                                                                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                                    <?php } ?>
                                                                </p>
                                                            <?php } else { ?>
                                                                <p class="price">&nbsp;&nbsp;</p>
                                                            <?php } ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div>
                                            <ul class="pagination  pull-left" id='products_pagination'>
                                                <?php echo $pagination ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset='utf-8'><!--
    var categories = Array();
    categories = JSON.parse('<?php echo json_encode($seller_categories) ?>');

    $('#filter_category_products').on('change', function () {
        var filterFilter = $('#filterFilter').val();
        var filter_category = $('select[name=\'filter_category_products\']').val();
        var filter_main_category = $('select[name=\'filter_main_category_products\']').val();
        fill_data(filter_category, filter_main_category, filterFilter, 1);
    });

    $('#filter_main_category_products').on('change', function () {
        var filterFilter = $('#filterFilter').val();
        var filter_main_category = $('select[name=\'filter_main_category_products\']').val();
        change_category_items(filter_main_category);
        fill_data(0, filter_main_category, filterFilter, 1);
    });


    function fill_data(filter_category, filter_main_category, filterFilter, page_no) {
      var wishlist_prods = '<?php echo json_encode($wishlist_ids)?>';
        $.ajax({
            type: 'POST',
            url: '<?php echo $filter_products ?>',
            dataType: 'json',
            data: 'page=' + page_no + '&filter_filter=' + filterFilter + '&category_id=' + filter_category + '&main_category_id=' + filter_main_category + '&id=' + '<?php echo $seller_id ?>',
            success: function (data) {
                var html = '';
                for (var i = 0; i <= data['products'].length - 1; ++i) {
                    if (wishlist_prods.indexOf(data['products'][i]['product_id']) != -1){
                        var onclick_val ='wishlist.remove('+data['products'][i]['product_id']+'), switch_case2 ($(this),2,'+data['products'][i]['product_id']+')" class="active"';
                    }else{
                        var onclick_val ="wishlist.add("+data['products'][i]['product_id']+"), switch_case2 ($(this),1,"+data['products'][i]['product_id']+")";
                    }
                    html += '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">';
                    html += '<div class="tile">';
                    html += '<div class="fave-heart"><a href="Javascript:void(0);" onclick="'+onclick_val+'"><i class="fa fa-heart"></i></a></div>';
                    html += '<div class="tile-img">';
                    html += '<a href="' + data['products'][i]['href'] + '"><img class="proimg" src="' + data['products'][i]['thumb'] + '"></a>';
                    if (data['products'][i]['price'] > 0) {
                        html += '<span class="shopping-bar" style="cursor:pointer;" ><?php echo $button_cart; ?></span>';
                    }
                    html += '</div>';
                    html += '<div class="tile-text"><a href="' + data['products'][i]['href'] + '">' + data['products'][i]['name'] + '</a></div>';
                    html += '<div class="tile-price">';
                    if (data['products'][i]['price'] > 0) {
                        html += '<p class="price">';
                        if (data['products'][i]['special'] <= 0) {
                            html += data['products'][i]['price'];
                        } else {
                            html += '<span class="price-new">' + data['products'][i]['special'] + '</span> <span class="price-old">' + data['products'][i]['price'] + '</span>';
                        }
                        if (data['products'][i]['tax'] > 0) {
                            html += '<span class="price-tax"><?php echo $text_tax; ?> ' + data['products'][i]['tax'] + '</span>';
                        }
                        html += '</p>';
                    }
                    html += '</div></div></div>';
                }
                $('#products').html(html);
                $('#products_pagination').html(data['pagination']);
            },
            error: function () {
                $('#products').html('<p>An error has occurred</p>');
            }
        });
    }

    function change_category_items(filter_main_category) {
        $('.category_option').each(function () {
            $(this).remove();
        });
        $('#filter_category_products').append('<option class="category_option" value="0" selected="selected"><?php echo $text_all_products ?></option>');
        $.each(categories[filter_main_category], function () {
            $('#filter_category_products').append('<option class="category_option" value="' + $(this)[0]['category_id'] + '" >' + $(this)[0]['name'] + '</option>');
        })

        //alert(filter_main_category);
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#filterFilter").change(function () {
            var url = window.location.href.split("&filter")[0].split("&main")[0].split("&category")[0];
            var sep = '&';
            var filterFilter = document.getElementById('filterFilter').value;
            if (filterFilter) {
                url += sep + 'filter_filter=' + filterFilter;
                sep = '&';
            }
            var filter_main_category_products = document.getElementById('filter_main_category_products').value;
            if (filter_main_category_products != '0') {
                url += sep + 'main_category_id=' + filter_main_category_products;
                sep = '&';
            }
            var filter_category_products = document.getElementById('filter_category_products').value;
            if (filter_category_products != '0') {
                url += sep + 'category_id=' + filter_category_products;
                sep = '&';
            }
            document.location = url;
        });
    });
    function switch_case(ele,status,seller_id){
      if (status==1){
        ele.attr("onclick","seller.remove("+seller_id+"), switch_case ($(this),2,"+seller_id+")");
        ele.text('<?php echo $unfollow_seller?>');
      }else{
        ele.attr("onclick","seller.add("+seller_id+"), switch_case ($(this),1,"+seller_id+")");
        ele.text('<?php echo $follow_seller?>');
      }
    }
    function switch_case2(ele,status,product_id){
      if (status==1){
        ele.attr("onclick","wishlist.remove("+product_id+"), switch_case2 ($(this),2,"+product_id+")");
        ele.addClass("active");
      }else{
        ele.attr("onclick","wishlist.add("+product_id+"), switch_case2 ($(this),1,"+product_id+")");
        ele.removeClass("active");
      }
    }
</script>

<?php echo $footer; ?>
