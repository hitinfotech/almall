<?php echo $header; ?>
<?php $class = 'col-sm-12'; ?>

<link rel="stylesheet" href="/catalog/view/theme/sayidaty/css/register_slider.min.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="/catalog/view/theme/sayidaty/javascript/intl-tel-input/build/css/intlTelInput.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="/catalog/view/theme/sayidaty/css/jquery.idealforms.css" type="text/css" media="screen"/>

<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/catalog/view/theme/sayidaty/javascript/iban.min.js"></script>
<script type="text/javascript" src="/catalog/view/theme/sayidaty/javascript/intl-tel-input/build/js/intlTelInput.js"></script>

<script type="text/javascript" src="/catalog/view/theme/sayidaty/javascript/jquery.idealforms.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<style>
.iti-flag {background-image: url("/catalog/view/theme/sayidaty/javascript/intl-tel-input/build/img/flags.png");}
@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {background-image: url("/catalog/view/theme/sayidaty/javascript/intl-tel-input/build/img/flags@2x.png");}

</style>
<div class="content-padding">
<div class="container">

      <div class="row">
            <div class="col-md-12">

      <h1 align="center"><?php echo $sell_header; ?></h1>
      <div class="">
      <div class="idealsteps-container">
      <nav class="idealsteps-nav"></nav>
          <form action="<?php echo $action ?>" novalidate=""  class="idealforms">
            <div class="idealsteps-wrap">

              <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-12">

                  <!-- Step 1 -->
                  <section class="idealsteps-step" style="display: none;" data-section-num='1'>

                    <legend> <?php echo $text_persinal_info ?></legend>
                    <div class="form-group field required idealforms-field idealforms-field-text firstname dont_remove_it">
                        <input type='hidden' name="dont_remove_it" id="dont_remove_it" />
                    </div>

                    <div class="form-group field icon required idealforms-field idealforms-field-text firstname">
                        <label for="firstname" class='control-label'><?php echo $text_firstname ?></label>
                        <input id="firstname" class="form-control" name="firstname"  />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="lastname"  class='control-label'><?php echo $text_lastname ?></label>
                        <input id="lastname" class="form-control" name="lastname"  />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text ">
                        <label for="email"  class='control-label'><?php echo $text_email ?></label>
                        <input id="user_email" class="form-control" name="email"  type="email" value="<?php echo (isset($seller_data['seller_email']) ? $seller_data['seller_email'] : '')?>" data-toggle="popover" data-trigger="focus" title="<?php echo $text_email ?>" data-content="This email is a primary email which will be the log-in email to access your account & will receive the email notification/s." />
                        <div id="user_email_error" class="text-danger"></div>
                    </div>

                    <div class="form-group field idealforms-field idealforms-field-text ">
                        <label for="fbs_email"  class='control-label'><?php echo $text_fbs_email ?></label>
                        <input id="user_fbs_email" class="form-control" name="fbs_email"  type="email" value="<?php echo (isset($seller_data['seller_fbs_email']) ? $seller_data['seller_fbs_email'] : '')?>" data-toggle="popover" data-trigger="focus" title="<?php echo $text_fbs_email ?>" data-content="You can add another email which will ONLY receive the email notification/s." />
                    </div>


                    <div class="form-group field required idealforms-field idealforms-field-text phone" id='customer_phone'>
                        <label for="telephone"  class='control-label'><?php echo $text_phone ?></label>
                        <div class='co-phone '>
                          <input id="telephone" class="form-control phone-num" style='padding-left: 8px; width:72%' name="telephone" type="tel" data-prev-code='' />
                          <span class="input-group-btn area-code" style='width:28%'>
                              <input type="text" class="form-control telcode" id="input-telecode"  name="telcode" value="<?php echo (isset($seller_data['telecode']) ? '00'.$seller_data['telecode'] : '')?>" readonly='readonly' >
                          </span>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="user_password"  class='control-label'><?php echo $text_password ?></label>
                        <input id="user_password" class="form-control" name="user_password" type="password" autocomplete=OFF />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="confirm_password"  class='control-label'><?php echo $text_confirm_password ?></label>
                        <input id="confirm_password" class="form-control" name="confirm_password" type="password" AUTOCOMPLETE=OFF />
                    </div>
                    <div class="field form-group buttons">
                    <button type="button" class="next btn btn-primary" id="button_next" disabled>Next</button>
                    </div>
                  </section>

                  <!-- Step 2 -->
                  <section class="idealsteps-step" style="display: block;"  data-section-num='2'>
                    <legend><?php echo $text_business_info ?></legend>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="screen_name"  class='control-label'><?php echo $text_screen_name ?></label>
                        <input id="screen_name" class="form-control step_2" name="screen_name"  placeholder="<?php echo $text_screen_name; ?>" type="text"  />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <span id="charNum" class="pull-right"></span>
                        <label class="control-label" for="input-description; ?>"><?php echo $text_brand_description; ?></label>
                        <textarea name="product_description" placeholder="<?php echo $text_brand_description; ?>" id="input-description" class="form-control step_2" onkeyup="countChar(this)"> </textarea>
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="logo"  class='control-label col-sm-3'><?php echo $text_logo ?></label>
                        <input type="file"  onchange='validate_next($(this));upload_image("logo")' id="logo" class="step_3 col-sm-9" name="logo"  />
                        <input type="hidden" id="logo_hidden"  name="logo_hidden" />
                        <br><br>
                        <a target="_blank" href="<?php echo isset($hd_logo_placeholder_avatar) ? $hd_logo_placeholder_avatar : 'javascript:void(0)'; ?>" class='col-md-offset-3'>
                          <?php if(isset($hd_logo_placeholder_admin)){ ?>
                            <img src="<?php echo isset($hd_logo_placeholder_admin) ? $hd_logo_placeholder_admin : '#'; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                            <?php } ?>
                        </a>
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-select">
                        <label for="country"  class='control-label'><?php echo $text_country ?></label>
                        <select name="country" id="country" class="form-control">
                            <?php foreach ($countries as $country) { ?>
                                <option value="<?php echo $country['country_id']; ?>" <?php echo (isset($seller_data['country_id']) && $seller_data['country_id'] == $country['country_id']) ?"selected":"" ?> ><?php echo $country['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-select">
                        <label for="city"  class='control-label'><?php echo $text_city ?></label>
                        <select name="city" id="city" class="form-control step_2">
                          <?php if (isset($seller_data['country_id']) && $seller_data['country_id'] != 0 && !empty($cities[$seller_data['country_id']])) { ?>
                            <?php foreach ($cities[$seller_data['country_id']] as $city) { ?>
                                <option value="<?php echo $city['zone_id']; ?>" <?php echo ($seller_data['zone_id'] == $city['zone_id']) ?"selected":"" ?> ><?php echo $city['name']; ?></option>
                            <?php } ?>
                          <?php } ?>
                        </select>
                    </div>

                    <div class="form-group field idealforms-field idealforms-field-text">
                        <label for="num_of_branches"  class='control-label'><?php echo $text_num_of_branches ?></label>
                        <input id="num_of_branches" class="form-control" name="num_of_branches" />
                    </div>

                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="admin_name"  class='control-label'><?php echo $text_admin_name ?></label>
                        <input id="admin_name" class="form-control " name="admin_name" />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="admin_email"  class='control-label'><?php echo $text_admin_email ?></label>
                        <input id="admin_email" class="form-control " name="admin_email" />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text phone " id="admin_telecode">
                        <label for="admin_phone"  class='control-label'><?php echo $text_admin_phone ?></label>
                        <div class='co-phone '>
                          <input id="admin_phone" class="form-control phone-num" style='padding-left: 8px; width:72%' name="admin_phone" data-prev-code='' />
                          <span class="input-group-btn area-code" style='width:28%'>
                              <input type="text" class="form-control telcode" id="input-telecode"  name="admin_num_telcode" readonly='readonly' >
                          </span>
                        </div>
                    </div>

                    <div class="form-group field idealforms-field idealforms-field-text">
                        <label for="website"  class='control-label'><?php echo $text_website ?></label>
                        <input id="website" class="form-control" name="website" placeholder="e.g. http://www.sayidaty.net"  type="text"/>
                    </div>
                    <div class="form-group field idealforms-field idealforms-field-text">
                        <label for="facebook"  class='control-label'><?php echo $text_facebook ?></label>
                        <input id="facebook" class="form-control step_2" name="facebook" placeholder="e.g. /Sayidatymall"  type="text" />
                    </div>
                    <div class="form-group field idealforms-field idealforms-field-text">
                        <label for="twitter"  class='control-label'><?php echo $text_twitter ?></label>
                        <input id="twitter" class="form-control step_2" name="twitter" placeholder="e.g. /sayidaty_mall" type="text" />
                    </div>
                    <div class="form-group field idealforms-field idealforms-field-text">
                        <label for="instagram"  class='control-label'><?php echo $text_instagram ?></label>
                        <input id="instagram" class="form-control step_2" name="instagram" placeholder="e.g. /sayidatymall" type="text" />
                    </div>
                    <div class="form-group field idealforms-field idealforms-field-text">
                        <label for="selling_countries"  class='control-label'><?php echo $text_selling_countries ?></label><br>
                        <select name="available_countries" id="available_countries" class="form-control">
                            <option value="0" selected ?><?php echo 'All Countries'; ?></option>
                            <?php foreach ($available_countries as $country) { ?>
                                <option value="<?php echo $country['country_id']; ?>" name="available_countries" value="<?php echo $country['country_id']?>" <?php echo ((isset($seller_data['available_country']) && ($country['country_id'] == $seller_data['available_country'])) ? 'selected' : '')?>  ><?php echo $country['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group field idealforms-field idealforms-field-text" id='excluded-countries'>
                        <label class="control-label" for="input-excluded-countries"><?php echo $text_excluded_countries; ?></label><br>
                        <?php if ($available_countries) { ?>
                            <?php foreach ($available_countries as $available) { ?>
                                  <input type='checkbox' value="<?php echo $available['country_id']; ?>" name='execluded_countries[]' ><?php echo $available['name']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="warehouse_address"  class='control-label'><?php echo $text_warehouse_address ?></label>
                        <input id="warehouse_address" class="form-control step_2" name="warehouse_address" placeholder="<?php echo $text_warehouse_address; ?>" type="text" />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="opening_hours"  class='control-label'><?php echo $text_opening_hours ?></label>
                        <input id="opening_hours" class="form-control step_2" name="opening_hours"  placeholder="<?php echo $text_opening_hours; ?>" type="text" />
                    </div>
                    <div class="field buttons">
                    <button type="button" class="prev btn btn-primary">Prev</button>
                    <button type="button" class="next btn btn-primary" disabled>Next</button>
                    </div>
                  </section>

                  <section class="idealsteps-step" style="display: block;"  data-section-num='3'>
                    <legend><?php echo $text_legal_info ?></legend>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="company_name"  class='control-label'><?php echo $text_company_name ?></label>
                        <input id="company_name" class="form-control step_2" name="company_name"  placeholder="<?php echo $text_company_name; ?>" type="text" />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="trade_license_number"  class='control-label'><?php echo $text_trade_license_number ?></label>
                        <input id="trade_license_number" class="form-control" name="legal_info[trade_license_number]" type="text" />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="license"  class='control-label col-sm-3'><?php echo $text_license ?></label>
                        <input type="file"  onchange='validate_next($(this));upload_image("license")' id="license" class="step_3 col-sm-9" name="license" />
                        <input type="hidden" id="license_hidden" name="license_hidden" />
                        <br><br>
                        <a target="_blank" href="<?php echo isset($license_placeholder_avatar) ? $license_placeholder_avatar : 'javascript:void(0)'; ?>" class='col-md-offset-3'>
                            <?php if(isset($license_placeholder_admin)){ ?>
                              <img src="<?php echo isset($license_placeholder_admin) ? $license_placeholder_admin : '#'; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                            <?php } ?>
                        </a>
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="address_license_number"  class='control-label'><span data-toggle="tooltip" title="<?php echo $text_admin_contact_tooltip; ?>"><?php echo $text_address_license_number ?></label>
                        <input id="address_license_number" class="form-control" name="legal_info[address_license_number]" type="text" />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="legal_name"  class='control-label' ><span data-toggle="tooltip" title="<?php echo $text_legal_name_tooltip; ?>"><?php echo $text_legal_name ?></label>
                        <input id="legal_name" class="form-control step_3" name="legal_info[legal_name]" type="text" />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="nationality"  class='control-label'><?php echo $text_nationality ?></label>
                        <select name="nationality" id="nationality" class="form-control">
                            <?php foreach ($countries as $country) { ?>
                                <option value="<?php echo $country['country_id']; ?>" <?php echo ( isset($seller_data['nationality']) && ($seller_data['nationality'] == $country['country_id'])) ?"selected":"" ?> ><?php echo $country['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="passport_num"  class='control-label'><?php echo $text_passport_number ?></label>
                        <input id="passport_num" class="form-control " name="passport_num"  />
                    </div><br>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="passport_copy"  class='control-label col-sm-3'><?php echo $text_passport_copy ?></label>
                        <input type="file" onchange='validate_next($(this));upload_image("passport_copy")' id="passport_copy" class="step_3 col-sm-9" name="passport_copy" />
                        <input type="hidden" id="passport_copy_hidden" name="passport_copy_hidden" />
                        <br><br>
                        <a target="_blank" href="<?php echo isset($passport_placeholder_avatar) ? $passport_placeholder_avatar : 'javascript:void(0)'; ?>" class='col-md-offset-3'>
                            <?php if(isset($passport_placeholder_admin)){ ?>
                              <img src="<?php echo isset($passport_placeholder_admin) ? $passport_placeholder_admin : '#'; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                            <?php } ?>
                        </a>
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text">
                        <label for="email_address"  class='control-label'><?php echo $text_email_address ?></label>
                        <input id="email_address" class="form-control step_3 email" name="legal_info[email_address]" type="text" />
                    </div>

                    <div class="form-group field required idealforms-field idealforms-field-text phone " id="office_num_div">
                        <label for="office_num"  class='control-label'><?php echo $text_office_num ?></label>
                        <div class='co-phone '>
                          <input id="office_num" class="form-control phone-num" style='padding-left: 8px; width:72%' name="legal_info[office_num]" type="tel" data-prev-code='' />
                          <span class="input-group-btn area-code" style='width:28%'>
                              <input type="text" class="form-control telcode" id="input-telecode"  name="office_num_telcode" readonly='readonly' >
                          </span>
                        </div>
                    </div><br><br>
                    <div class="form-group field required idealforms-field idealforms-field-text phone " id="moblie_num_div">
                        <label for="moblie_num"  class='control-label'><?php echo $text_moblie_num ?></label>
                        <div class='co-phone '>
                          <input id="moblie_num" class="form-control phone-num " style='padding-left: 8px; width:72%' name="legal_info[moblie_num]" type="tel" data-prev-code='' />
                          <span class="input-group-btn area-code" style='width:28%'>
                              <input type="text" class="form-control telcode" id="input-telecode"  name="moblie_num_telcode" readonly='readonly' >
                          </span>
                        </div>
                    </div><br><br>
                    <div class="field buttons">
                    <button type="button" class="prev btn btn-primary">Prev</button>
                    <button type="button" class="next btn btn-primary" disabled>Next</button>
                    </div>
                  </section>

                  <section class="idealsteps-step" style="display: block;"  data-section-num='4'>
                    <legend><?php echo $text_financail_info ?></legend>

                    <div class="field form-group required idealforms-field idealforms-field-text">
                        <label for="beneficiary_name"  class='control-label'><?php echo $text_beneficiary_name ?></label>
                        <input id="beneficiary_name" class="form-control " name="financail_info[beneficiary_namee]"  type="text" />
                    </div>
                    <div class="field form-group required idealforms-field idealforms-field-text">
                        <label for="bank_name"  class='control-label'><?php echo $text_bank_name ?></label>
                        <input id="bank_name" class="form-control " name="financail_info[bank_name]" type="text"/>
                    </div>
                    <div class="field form-group required idealforms-field idealforms-field-text">
                        <label for="bank_account"  class='control-label'><?php echo $text_bank_account ?></label>
                        <input id="bank_accountt" class="form-control" name="financail_info[bank_accountt]" type="text" />
                    </div>

                    <div class="field form-group required idealforms-field idealforms-field-text">
                        <label for="currency"  class='control-label'><?php echo $text_currency ?></label>
                        <select name="financail_info[currency]" id="currency" class="form-control ">
                          <?php if (!empty($currencies)) ?>
                            <?php foreach ($currencies as $currency) { ?>
                                <?php if ($currency['status'] ==1 ) {?>
                                  <option value="<?php echo $currency['currency_id']; ?>" <?php echo (isset($seller_data['currency']) && ($seller_data['currency'] == $currency['currency_id'])) ?"selected":"" ?> ><?php echo $currency['title']."(".$currency['code'].")"; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="field form-group idealforms-field idealforms-field-text">
                      <div class='checkbox row' >
                        <input id="is_both" type='checkbox' name='is_both'>
                        <label for="both" class='control-label'><?php echo $text_both_iban_swift ?></label>

                      </div>
                        <select name="bank_number" id="bank_number" class="form-control">
                          <option value="1" <?php echo (isset($seller_data['bank_number_option']) && $seller_data['bank_number_option'] == 1) ?"selected":"" ?> ><?php echo 'IBAN'; ?></option>
                          <option value="2" <?php echo (isset($seller_data['bank_number_option']) && $seller_data['bank_number_option'] == 2) ?"selected":"" ?> ><?php echo 'SWIFT CODE'; ?></option>
                        </select>
                    </div>

                    <div class="field form-group required idealforms-field idealforms-field-text">
                        <label for="iban_code"  class='control-label'><?php echo $text_iban_code ?></label>
                        <input id="iban_code" class="form-control  iban" name="financail_info[iban_code]" type="text" />
                    </div>

                    <div class="field form-group  idealforms-field idealforms-field-text">
                        <label for="swift_code"  class='control-label'><?php echo $text_swift_code ?></label>
                        <input id="swift_code" class="form-control  swift" name="financail_info[swift_code]" type="text" disabled="disabled" />
                    </div>

                    <div class="field form-group required idealforms-field idealforms-field-text">
                        <label for="fin_contact_name"  class='control-label'><?php echo $text_fin_contact_name ?></label>
                        <input id="fin_contact_name" class="form-control " name="financail_info[fin_contact_name]" type="text" />
                    </div>
                    <div class="field form-group required idealforms-field idealforms-field-text">
                        <label for="fin_email"  class='control-label'><?php echo $text_fin_email ?></label>
                        <input id="fin_email" class="form-control  email" name="financail_info[fin_email]" type="text" />
                    </div>
                    <div class="form-group field required idealforms-field idealforms-field-text phone " id="fin_number">
                        <label for="fin_num"  class='control-label'><?php echo $text_fin_num ?></label>
                        <div class='co-phone '>
                          <input id="fin_num" class="form-control phone-num" style='padding-left: 8px; width:72%' name="financail_info[fin_num]" type="tel" data-prev-code='' />
                          <span class="input-group-btn area-code" style='width:28%'>
                              <input type="text" class="form-control telcode" id="input-telecode"  name="fin_telephone_telecode" readonly='readonly' >
                          </span>
                        </div>
                    </div>
                      <div class="field form-group idealforms-field idealforms-field-text">
                          <label class="control-label" for="eligible_check"><?php echo $entry_eligible_vat; ?></label>
                          <input type="checkbox" name="financail_info[eligible_check]" id="eligible_check"  class="form-control" onclick="toggle('.vat_form',this)" checked />
                          <input type="text" value="1" name="financail_info[eligible_check]" id="hidden_eligible_check" class="hidden"/>
                      </div>
                      <div class="field form-group idealforms-field idealforms-field-text">
                          <label class="control-label" for="input-document"><?php echo $entry_eligible_upload; ?></label>
                          <input type="file" id="document" class="" name="document"  />
                      </div>

                      <div class="field form-group required idealforms-field idealforms-field-text vat_form">
                          <label class="control-label" for="input-doc-company-name"><?php echo $entry_company_name; ?></label>
                          <input type="text" name="financail_info[doc_company_name]" id="input-company-name" value="<?php echo isset($doc_company_name)?$doc_company_name:'' ?>"  class="form-control" />
                      </div>
                      <div class="field form-group required idealforms-field idealforms-field-text vat_form">
                          <label class="control-label" for="input-business-address"><?php echo $entry_eligible_business_address; ?></label>
                          <input type="text" name="financail_info[business_address]" value="<?php echo isset($business_address)?$business_address:'' ?>" id="input-business-address" class="form-control" />
                      </div>
                      <div class="field form-group required idealforms-field idealforms-field-text vat_form">
                          <label class="control-label" for="input-tax-reg-num"><?php echo $entry_eligible_tax_reg_num; ?></label>
                          <input type="text" name="financail_info[tax_reg_num]" value="<?php echo isset($tax_reg_num)?$tax_reg_num:'' ?>" id="input-tax-reg-num" class="form-control" />
                      </div>
                      <div class="field form-group required idealforms-field idealforms-field-text vat_form">
                          <label class="control-label" for="input-issue-date"><?php echo $entry_eligible_issue_date; ?></label>
                          <div class="field form-group required idealforms-field idealforms-field-text vat_form">
                              <div class='input-group date' id='datetimepicker6'>
                                  <input type='text' name="financail_info[issue_date]" class="form-control" />
                                  <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                          </div>

                      </div>
                      <br><br>
                    <div class="field buttons">
                    <button type="button" class="prev btn btn-primary">Prev</button>
                    <button type="button" class="next btn btn-primary" disabled>Next</button>
                    </div>
                  </section>

              <!-- Step 5 -->

                  <section class="idealsteps-step" style="display: block;"  data-section-num='5'>
                    <legend>Confirm</legend>

                    <div id='terms_and_conditions'>
                      <div style="height:500px; overflow:scroll;">
                        <table style='width: 100%'>
                          <tr>
                            <td>
                              <p><b>1. Definitions And Interpretation</b></p>
                            </td>
                            <td dir="rtl">
                              <p><b>التفسیرات والتعریفات</b></p>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p>The foregoing Recitals shall form an integral part of this Agreement and shall be read and construed alongside the same.</p>
                            </td>
                            <td dir="rtl">
                              <p>یشكل التمھید السابق جزأ لا یتجزأ من ھذه الاتفاقیة ویقرأ ویفسر وفقا لذلك</p>
                            </td>
                          </tr>
                          <tr>
                            <td><p><b>Definitions:</b></p>
                            </td>
                            <td dir="rtl">
                              <p><b>التعریفات :</b></p>

                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p>In this Agreement, except where the context otherwise requires the following words and expressions shall have the following meanings:</p>
                            </td>
                            <td dir="rtl">
                              <p>ما لم یقتضي السیاق خلاف ذلك، تحمل الكلمات والتعبیرات التالیة المعنى المحدد قرین كل منھا فیما یلي : </p>

                            </td>
                          </tr>
                          <tr>
                            <td>
                              <table>
                                <tr>
                                  <td>
                                    <b>‘Agreement’</b>
                                  </td>
                                  <td>
                                    shall mean this Agreement including its schedules, appendices, exhibits and amendments pursuant to the terms and conditions hereof including any amendments made to schedules, appendices and exhibits;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Affiliate”</b>
                                  </td>
                                  <td>
                                    shall refer to (i) any person directly or indirectly controlling, controlled by or under common control with another person, (ii) any person owning or controlling 10% or more of the outstanding voting securities of such other person, (iii) any officer, director or other partner of such person and (iv) if such other person is an officer, director or partner, any business or entity for which such person acts in any such capacity;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Buyer”</b>
                                  </td>
                                  <td>
                                    means any party / person who purchases the Seller’s item through the Site;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Commencement Date”</b>
                                  </td>
                                  <td>
                                    means the date of the signature of the Agreement;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Currency”</b>
                                  </td>
                                  <td>
                                    means the legitimate currency of KSA and/or the UAE;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Confidential Information”</b>
                                  </td>
                                  <td>
                                    means information of a confidential nature (including trade secrets and information of commercial value) known to each Party and concerning each Party or its partners or any project and communicated between the Parties. Confidential Information also includes information relating to Operators / customers common between the Party, and terms and conditions of this Agreement;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Force Majeure”</b>
                                  </td>
                                  <td>
                                    means war, invasion, act of foreign enemies, hostilities (whether war be declared or not), civil war, rebellion, act of God, revolution, insurrection or military or usurped power, martial law or confiscation by order of any government and any circumstances beyond the reasonable control of the Parties (as unforeseeable course of events excusing the Parties from the fulfillment of this Agreement);
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Item(s)”</b>
                                  </td>
                                  <td>
                                    means the item(s)/products to be sold by the Seller through the Site, as specified in Annexure – A of this Agreement;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Penalty”</b>
                                  </td>
                                  <td>
                                    means the penalty imposed on the Seller due to provision of defective or missing or bad quality item to the Buyer;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Policies”</b>
                                  </td>
                                  <td>
                                    means currently existing and effective policies of the First Party;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“KSA”</b>
                                  </td>
                                  <td>
                                    means the Kingdom of Saudi Arabia;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Relevant Authority(ies)”</b>
                                  </td>
                                  <td>
                                    shall as the context requires mean: <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. The government of the UAE and/or KSA, as the case may be; <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. The government of any of the emirate of UAE; <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Any other ministry, department or local authority having jurisdiction over the sale in KSA and/or UAE; and <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d. Any governmental authority controlling the subject online sale transaction in KSA and/or UAE.
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Return Items”</b>
                                  </td>
                                  <td>
                                    means the items returned by the Buyer to the Seller within 15 days;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Sale Proceeds”</b>
                                  </td>
                                  <td>
                                    means the gross proceeds from any of the Seller’s sale transaction, including all shipping and handling, gift wrap and other charges, but excluding any taxes separately stated and charged;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Service Charges”</b>
                                  </td>
                                  <td>
                                    means the service charges payable by the Seller to the First Party for the provision of the Services under this Agreement.
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Site”</b>
                                  </td>
                                  <td>
                                    means www.sayidaty.net, sayidatymall.net, sayidatymall.com, Mall.sayidaty.net; owned and operated by the First Party;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“Territory”</b>
                                  </td>
                                  <td>
                                    means the territories of KSA and UAE;
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>“UAE”</b>
                                  </td>
                                  <td>
                                    means the United Arab Emirates.
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td dir="rtl">
                              <table dir="rtl">
                                <tr>
                                  <td>
                                    <b>الاتفاقیة</b>
                                  </td>
                                  <td>
                                    یقصد بھا ھذه الاتفاقیة وما تتضمنھ من جداول وملحقات ومستندات وتعدیلات وفقا للشروط والأحكام المنصوص علیھا بھذه الاتفاقیة والتي تشمل التعدیلات على الجداول والملحقات والمستندات.
                                 </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>التابع</b>
                                  </td>
                                  <td>
                                    یقصد بھ (1 (أي شخص یدیر أو یخضع لإدارة أو یخضع للإدارة المشتركة مع شخص آخر أو(2 (أي شخص یمتلك أو یتولى إدارة 10 % أو أكثر من الأسھم المتداولة التي تخول الحق في التصویت لھذا الشخص الآخر(3 (أي مسئول أو مدیر أو شریك آخر لھذا الشخص (4 (إذا كان ھذا الشخص مسئول أو مدیر أو شریك أو أي كیان أو شركة والتي یعمل بھا ھذا الشخص بأي صفة .
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>المشتري</b>
                                  </td>
                                  <td>
                                    یقصد بھ أي طرف / شخص یشتري بضائع البائع عن طریق الموقع الالكتروني.
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>تاریخ البدء</b>
                                  </td>
                                  <td>
                                    یقصد بھ تاریخ ( ) 2016
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>العملة</b>
                                  </td>
                                  <td>
                                    یقصد بھا العملة الرسمیة للمملكة العربیة السعودیة و/ أو الإمارات العربیة المتحدة
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>المعلومات السریة</b>
                                  </td>
                                  <td>
                                    یقصد بھا المعلومات التي لھا طبیعة سریة (مثل الأسرار التجاریة ومعلومات القیمة التجاریة المعروفة لكل طرف والمتعلقة بكل طرف أو شركائھم أو أي مشروع والتي تم تبادلھا بین الطرفین وكما تتضمن المعلومات السریة المعلومات المتعلقة بالمشغلین / العملاء المشتركین بین الطرفین وشروط وأحكام ھذه الاتفاقیة .
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>القوة القاھرة</b>
                                  </td>
                                  <td>
                                    یقصد بھا الحرب والغزو وأفعال العدو الخارجي والعداءات ( سواء تم إعلان الحرب أو لا) والحروب المدنیة والتمرد وأفعال القدر والثورات والعصیان المسلح والقوة العسكریة والاستیلاء على الحكم والأحكام العرفیة والمصادرة بواسطة الأمر الصادر من الھیئات الحكومیة وأي ظروف خارجة عن السیطرة المعقولة للطرفین (وأي من الأحداث الغیر متوقعة والتي تعیق أي طرف من الوفاء بالتزاماتھ بموجب ھذه الاتفاقیة)
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>البضائع</b>
                                  </td>
                                  <td>
                                    یقصد بھا البضائع / المنتجات التي یبعھا البائع عن طریق الموقع الالكتروني ، حسب ما ھو منصوص علیھ بالملحق أ من ھذه الاتفاقیة .
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>الغرامة</b>
                                  </td>
                                  <td>
                                    یقصد بھا الغرامة التي تفرض على البائع نتیجة بیعھ للمشتري منتجات معیبة أو ناقصة أو متدنیة الجودة
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>السیاسات</b>
                                  </td>
                                  <td>
                                    یقصد بھا سیاسات الطرف الأول القائمة حالیا والساریة
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>”م.ع.س“</b>
                                  </td>
                                  <td>
                                    یقصد بھا المملكة العربیة السعودیة
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>الھیئة (الھیئات )المختصة</b>
                                  </td>
                                  <td>
                                    حسب ما یقتضي السیاق ،<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;یقصد بھا :<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 (حكومة الإمارات العربیة المتحدة و/ أو المملكة العربیة السعودیة حسب الحالة.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 (حكومة أي إمارة بالإمارات العربیة المتحدة.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ج) أي وزارة أو دائرة أو ھیئة محلیة أخرى مختصة بالبیع في المملكة العربیة السعودیة \ أو الإمارات العربیة المتحدة.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;د) أي ھیئات حكومیة مختصة بإدارة معاملات البیع الالكتروني في المملكة العربیة السعودیة \ أو الإمارات العربیة المتحدة.
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>البضائع المرتجعة</b>
                                  </td>
                                  <td>
                                    یقصد بھا البضائع التي یعیدھا المشتري للبائع ف غضون 15 یوم
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>عائد البیع</b>
                                  </td>
                                  <td>
                                    یقصد بھ إجمالي عائد البیع من أي معاملات بیع للبائع والتي تشمل كافة نفقات الشحن والمناولة ولف الھدایا والنفقات الأخرى ویستثنى من ذلك الضرائب المحددة والتي یتم حسابھا بشكل منفصل.
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>رسوم الخدمة</b>
                                  </td>
                                  <td>
                                    یقصد بھا رسوم الخدمة التي یدفعھا البائع للطرف الأول لتوفیر الخدمات بموجب ھذه الاتفاقیة
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>الموقع</b>
                                  </td>
                                  <td>
                                    یقصد بھ المواقع means www.sayidaty.net, sayidatymall.net,sayidat ymall.com, التي Mall.sayidaty.net; یمتلكھا ویشغلھا الطرف الأول
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>لنطاق الجغرافي</b>
                                  </td>
                                  <td>
                                    یقصد بھ منطقة المملكة العربیة السعودیة والإمارات العربیة المتحدة
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <b>”أ.ع.م“</b>
                                  </td>
                                  <td>
                                    یقصد بھا دولة الإمارات العربیة المتحدة .
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p>a) The clause headings and the underlinings are for convenience only and shall not affect the interpretation of this Agreement.</p>
                              <p>b) Words importing the singular include the plural and vice-versa and words importing a gender include any gender;</p>
                              <p>c) All representations, warranties, undertakings, indemnity, covenants or agreements on the part of the Parties bind them jointly and severally.</p>
                              <p>d) Particulars and schedules form part of this Agreement and shall have effect as if set out in full in the body of this Agreement and any reference to this Agreement shall include the ‘Schedule’ and ‘Particulars’.</p>
                              <p>e) All dates and periods shall be determined by reference to the Gregorian calendar.</p>
                              <p>f) If any provision in a definition in this Agreement is a substantive provision conferring rights or imposing obligations, then, notwithstanding that it is only in the interpretation clause of this Agreement, effect shall be given to it as if it were a substantive provision in the body of this Agreement.</p>
                            </td>
                            <td dir="rtl">
                              <p>(1 (شرط العناوین والتعبیرات التي تحتھا خط استرشادیة فقط ولا یعتد بھا في تفسر الاتفاقیة .</p>
                              <p>(2 (الكلمات المفردة تشمل الجمع والعكس صحیح والكلمات التي تشیر للجنس تشمل أي جنس .</p>
                              <p>(ج) كافة التعھدات والضمانات والوعود والتعویضات والعھود والاتفاقیات المقدمة من الطرفین تعد ملزمة للطرفین بالتضامن والتكافل.</p>
                              <p>(د) البیانات والجداول التي تشكل جزء من ھذه الاتفاقیة والتي تكون نافذة كما لو كان منصوص علیھا في الاتفاقیة ذاتھا وأي إشارة لھذه الاتفاقیة تشمل الإشارة لھذه الملحقات والبیانات</p>
                              <p>ه) ت ُحددكافةالتو اریخ وال فتراتوفقاللتقویم المیلادي</p>
                              <p>(و) أي من الأحكام المنصوص علیھا بتعریفات الاتفاقیة والذي یشكل حكم أساسي بحیث یكفل حقوق ویفرض التزامات وعلى الرغم من أنھ حكم بتفسیرات الاتفاقیة فیكون لھ نفس النفاذ كما لو كان حكم أساسي في الاتفاقیة نفسھا .</p>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p><b>2. SAYIDATY MALL’S ROLE</b></p>
                              <p>2.1 The Parties agree that the First Party provides a platform for third-party Sellers and Buyers to negotiate and complete transactions. The First Party is not involved in the actual transaction between Seller and Buyer, except to the extent as mentioned in this Agreement. The Second Party, being a Seller, may list any item on the Site unless it is a prohibited item as defined in the procedures and guidelines provided by the First Party, or otherwise prohibited by law. Without limitation, the Seller may not list any item or link or post any related material that:</p>
                              <p>(a) infringes any third-party intellectual property rights (including copyright, trademark, patent, and trade secrets) or other proprietary rights (including rights of publicity or privacy);</p>
                              <p>(b) constitutes libel or slander or is otherwise defamatory; or</p>
                              <p>(c) is counterfeited, illegal, stolen, or fraudulent.</p>
                              <p>2.2 It shall be the Seller’s sole responsibility to accurately describe the item for sale. As a Seller, the Second Party shall use the Site and the Services at its own risk.</p>

                            </td>
                            <td dir="rtl">
                              <p><b>2 مھام " سیدتي مول" -</b></p>
                              <p>- 2 1 یتفق الطرفان على أن الطرف الأول یوفر منصة لأي طرف ثالث من البائعین والمشترین للتفاوض وإنھاء الصفقات حیث أن الطرف الأول لا یكون طرف في التعاملات بین البائع والمشتري باستثناء الحدود المنصوص علیھا بھذه الاتفاقیة، كما یجوز للبائع إدراج أي قائمة من البضائع بالموقع ما لم تكون بضائع محظورة وفقا للإجراءات والإرشادات الخاصة بالطرف الأول أو كانت محظورة بموجب القانون وعلى سبیل المثال لا الحصر لا یجو للبائع إدراج أي من البضائع أو الروابط أو الملصقات الخاصة بالمواد ذات الصلة التي :
                              </p>
                              <p>- 1 تنتھك حقوق الملكیة الفكریة للغیر (حقوق الطبع – العلامات التجاریة – براءة الاختراع – والأسرار التجاریة) أو أي حقوق ملكیة أخرى والتي تتضمن ( حقوق الإشھار والخصوصیة ) .</p>
                              <p>- 2 السب والقذف وما شابھ ذلك من تشویھ السمعة </p>
                              <p>ج– الأمور الملفقة والغیر قانونیة والمسروقة والتدلیس</p>
                              <p>- 2 2 یتحمل البائع المسئولیة الكاملة دون غیره للوصف الدقیق للبضائع وبصفتھ البائع فإن الطرف الثاني الموقع والخدمات مع تحملھ لكافة المخاطر</p>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p><b>3. TRANSACTION PROCESS SERVICE</b></p>
                              <p>3.1 The Parties agree that by registering for or using the Services, the Seller authorizes Sayidaty Mall to act on its behalf for purposes of processing payments, returns, refunds and adjustments for sale transactions, receiving and holding Sales Proceeds, remitting sales proceeds to the Seller’s bank account, charging its credit card, and paying Sayidaty Mall and its Affiliates all/any amounts the Seller owes in accordance with this Agreement or other agreements the Seller may have with Sayidaty Mall or its Affiliates.</p>
                              <p>3.2 The Parties agree that Sayidaty Mall shall facilitate the purchase of Seller’s Items listed on the Site. Sayidaty Mall shall charge the Seller AED 15 plus 30% of the price as profit share/commission for each transaction of Item through the Site, and Sayidaty Mall shall issue invoice to the Seller relating to Sales Proceeds on monthly basis. The funds shall be transferred by Sayidaty Mall to the Sellers’s designated bank account within fifteen (15) days from the billing date, after deduction of the bank transfer fees, if applicable.</p>
                              <p>3.3 Further, the Parties agree that when a Buyer shall instruct Sayidaty Mall to pay the Seller, Sayidaty Mall shall be authorized to remit the Buyer’s payment to the Seller, after deduction of any applicable fees or other amounts Sayidaty Mall may collect under this Agreement. The Seller further agrees that Buyer shall be considered to have satisfied its obligations towards the Seller for the sale transaction when Sayidaty Mall receives the Sales Proceeds. The Parties agree that Sayidaty Mall shall be responsible to transfer the funds after deductions of its Service Charges, unless withheld due to potential claims.</p>
                              <p>3.4 Sayidaty Mall may require the Seller to provide any financial, business or personal information at any time to verify the Seller’s identity. The Seller undertakes that it shall not impersonate any person or use a name it is not legally authorized to use.</p>
                              <p>3.5 The Parties agree that the Sales Proceeds can be credited only to bank accounts in the KSA or UAE as supported by Sayidaty Mall’s standard functionality and enabled for the Seller’s account.</p>
                              <p>3.6 The Parties agree that sale transaction Service shall be available seven (7) days per week, twenty four (24) hours per day, except for the scheduled downtime due to system maintenance or in case of any technical default. It is agreed that the transfers to the Seller’s Account shall generally be credited within 1-2 business days of the date Sayidaty Mall initiates the transfer. The Seller shall not receive interest or any other earnings on any Sales Proceeds.</p>
                              <p>3.7 The Parties agree that for security measures, Sayidaty Mall and/or its Affiliates, shall be authorized to, but not required to, impose transaction limits on some or all Buyers and Seller relating to the value of any transaction, disbursement, or adjustment, the cumulative value of all transactions, disbursements, or adjustments during a period of time, or the number of transactions per day or other period of time. Neither Sayidaty Mall nor its Affiliates shall be liable to Seller:</p>
                              <p>(i) if Sayidaty Mall does not proceed with a sale transaction, disbursement, or adjustment that would exceed any limit established by Sayidaty Mall or its Affiliates for a security reason, or</p>
                              <p>(ii) if Sayidaty Mall or its affiliates permits a Buyer to withdraw from a transaction because the transaction processing Service is unavailable following the commencement of a transaction.</p>
                              <p>3.8 The Parties agree that if Sayidaty Mall or its affiliates reasonably concludes or considers, based on available information, that Seller’s actions and/or performance in connection with the Services may result in Buyer disputes, chargebacks or other claims, then Sayidaty Mall may, in its sole discretion, delay initiating any remittances and withhold any payments to be made or that are otherwise due to Seller in connection with the Services or this Agreement until the completion of any investigation(s) regarding any of the Seller’s actions and/or performance in connection with this Agreement.</p>
                              <p>3.9 The Parties agree that the Seller shall bear all risk of fraud or loss in connection with any of Seller’s Items that are not fulfilled strictly in accordance with the order information and provided shipping information.</p>

                            </td>
                            <td dir="rtl">
                              <p><b>3 –خدمة معالجة المعاملات</b></p>
                              <p>- 3 1 یوافق الطرفان أنھ فور التسجیل أو استخدام الخدمات فإن البائع یخول سیدتي مول للتصرف نیابة عنھ بغرض التعامل مع الدفعات والبضائع المرتجعة واسترداد الأموال والتسویات المتعلقة بالمعاملات وقبض والاحتفاظ بعائد البیع وتحویل عائد البیع للحساب  المصرفي للبائع والرسوم  الخاصة بالبطاقة الائتمانیة ودفع المبالغ الخاصة بسیدتي مول والشركات التابعة وكافة المبالغ المستحقة للبائع بموجب ھذه الاتفاقیة أو أي اتفاقیات أخرى للبائع مع سیدتي مول وأي من الشركات التابعة لھ.</p>
                              <p>- 3 2 یوافق الأطراف أن سیدتي مول یتولى تسھیل شراء بضائع البائع المدرجة بالموقع ومن ثم یفرض سیدتي مول على البائع رسوم ( ) درھم إماراتي بالإضافة إلى () % من السعر بصفتھا حصة في الأرباح / عمولة لكل صفقة بیع للبضائع عن طریق الموقع  ویصدر سیدتي مول للبائع فاتورة عائدات البیع بصفة شھریة ویحول سیدتي مول المبالغ المستحقة للبائع على الحساب المصرفي المخصص للبائع في غضون 15 یوم من تاریخ إصدار الفاتورة بعد خصم رسوم التحویل المصرفي ، إن وجد .</p>
                              <p>- 3 3 بالإضافة إلى ذلك یوافق الطرفان على أنھ عند إصدار  تعلیمات من المشتري بالدفع للبائع فیخول سیدتي مول بتحویل المبالغ التي دفعھا المشتري للبائع بعد خصم الرسوم أو أي مبالغ أخرى مستحقة لسیدتي مول بموجب ھذه الاتفاقیة، كما یوافق البائع كذلك اعتبار أن المشتري استوفى بالتزاماتھ تجاه البائع فیما یتعلق بصفقة البیع عند استلام سیدتي مول لعائد البیع ویوافق الطرفان على أن سیدتي مول مسئول عن تحویل النقود بعد خصم رسوم الخدمة ما لم یتم احتجازھا بسبب المطالبات المحتملة .</p>
                              <p>- 3 4 یجوز أن یطلب " سیدتي مول " من البائع تقدیم أي معلومات مالیة أو تجاریة أو شخصیة في أي وقت للتحقق من ھویة البائع وكما یتعھد البائع بعدم انتحالھ صفة أي شخص أو استخدام أي اسم غیر مصرح لھ قانونیا باستخدامھ.</p>
                              <p>- 3 5 یوافق البائع على أنھ یجوز تقیید عائد البیع فقط على  الحسابات المصرفیة بالمملكة العربیة السعودیة أو الإمارات العربیة المتحدة المدعومة بالأداء الوظیفي القیاسي الخاص ب" سیدتي مول " والمتاحة لحساب البائع.</p>
                              <p>- 3 6 یوافق الطرفان على أن خدمة المبیعات تكون متاحة سبعة (7 (أیام في الأسبوع و 24 ساعة یومیا باستثناء وقف التعطل المقرر نتیجة لمنظومة الصیانة وفي حالة الأعطال الفنیة ومن المتفق علیھ أن التحویلات على حساب - البائع یتم تقییدھا بشكل عام على حساب البائع خلال 1 2 یوم عمل من تاریخ شروط" سیدتي مول " في التحویل ولا یستلم البائع أي فائدة أو أرباح من عائد البیع.
                              </p>
                              <p>- 3 7 یوافق الطرفان أنھ نظرا للإجراءات أمنیة فیخول " سیدتي مول " والشركات التابعة لھ، ولكن لا یطلب منھ، فرض حدود على المعاملات على بعض أو كل من البائعین والمشترین فیما یتعلق بقیمة أي معاملة أو استرداد أو تسویة خلال فترة زمنیة أو عدد من المعاملات / یوم أو لأي فترة من الوقت ولا یتحمل " سیدتي مول " أو الشركات التابعة لھ أي مسئولیة تجاه البائع.
                              </p>
                              <p>(1 (في حالة عدم شروع " سیدتي مول " في صفقة البیع أو الدفع أو التسویة بعد أي مدة محددة بواسطة " سیدتي مول " أو الشركات التابعة لھ لدواعي أمني.</p>
                              <p>(2 (في حالة سماح " سیدتي مول " أو الشركات التابعة لھ للمشتري بالانسحاب من الصفقة بسبب عدم توفر خدمات معالجة الصفقات(التعاملات) بعد الفعل في الصفقة .</p>
                              <p>- 3 8 یتفق الطرفان أنھ في حالة إذا رأت " سیدتي مول " أو الشركات التابعة لھا أو اعتبرت استنادا على المعلومات المتاحة أن تصرفات البائع وأدائھ فیما یتعلق بالخدمات یؤد إلى منازعات مع المشتري أو استرداد القیمة أو أي مطالبات أخرى فیجوز أن تقوم " سیدتي مول " حسب تقدیرھا الشخصي بتأخیر تحویل أو احتجاز الدفعات أو المبالغ المستحقة للبائع فیما یتعلق بالخدمات أو ھذه الاتفاقیة حتى الانتھاء من أي تحقیق متعلق بتصرفات أو أداء البائع بموجب ھذه الاتفاقیة .
                              </p>
                              <p>- 3 9 یوافق الطرفان على تحمل البائع لكافة المخاطر المتعلقة بالتدلیس أو الخسائر لأي من بضائع البائع التي لم یتم الوفاء بھا مطلقا طبقا للمعلومات المنصوص علیھا بأمر الشراء أو معلومات الشحن المقدمة.</p>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p><b>4. SAYIDATY MALL’S RESERVED RIGHTS</b></p>
                              <p>4.1 The Parties agree that Sayidaty Mall retains the right to determine the content, appearance, design, functionality and all other aspects of the Site and the Services (including the right to re-design, modify, remove and alter the content, appearance, design, functionality, and other aspects of the Site and the Service, and any element, aspect, portion or feature thereof, as Sayidaty Mall may deem it suitable at any time), and to delay or suspend listing of, or to refuse to list, or to de-list, or to require Seller not to list, any or all Items in Sayidaty Mall’s sole discretion. Furthermore, Sayidaty Mall may in its sole discretion withhold for investigation, refuse to process, restrict shipping destinations, stop and/or cancel any of the Seller’s transactions, and on receiving instructions from Sayidaty Mall, the Seller shall commercially reasonable efforts to stop any transaction, at its own costs.</p>
                              <p>4.2 Sayidaty Mall’s employees and its Affiliates are permitted to participate in their personal capacity.</p>

                            </td>
                            <td dir="rtl">
                              <p><b>4 –الحقوق المحفوظة ل " سیدتي مول "</b></p>
                              <p>- 4 1 یتفق الطرفان أن " سیدتي مول " یحتفظ بالحق في تقریر المحتوى والمظھر والتصمیم والأداء الوظیفي وكافة المناحي الأخرى بالموقع والخدمات ( والتي تشمل الحق في إعادة التصمیم وتعدیل وإزالة وتبدیل المحتوى والمظھر والتصمیم والأداء الوظیفي والخدمات وأي من العناصر والجوانب والأجزاء والسمات الخاصة بذلك حسب ما یراه مناسبا في أي وقت) وكذلك تأخیر أو تعلیق إدراج أو رفض أو إلغاء إدراج القائمة أو مطالبة البائع بعدم إدراج أي أو كافة البضائع حسب تقدیر " سیدتي مول " وكذلك یجوز ل" سیدتي مول " حسب تقدیرھا الإحجام عن  الفحص أو رفض التعامل أو تقیید جھات وصول الشحن ووقف و / أو إلغاء أي من الصفقات التجاریة للبائع وفور استلام تعلیمات من " سیدتي مول " بھذا الصدد فیبذل البائع الجھود التجاریة المعقولة لوقف الصفقات على نفقتھ الخاصة.                            </p>
                              <p>- 4 2 یسمح لموظفي " سیدتي مول " والشركات التابعة للمشاركة بصفتھم الشخصیة .</p>

                            </td>
                          </tr>

                          <tr>
                            <td>
                              <p><b>5. SELLER’S OBLIGATIONS</b></p>
                              <p>5.1 The Parties agree that the Seller shall list goods on Site for sale at a fixed price and the Seller shall be obligated to sell the goods at the listed price to Buyers who meet the Seller's terms. Furthermore, it is agreed that by listing an item in a fixed price sale, the Seller shall represent and warrant to prospective Buyers that the Seller has all the right and full ability to sell, and also undertake that the listing is accurate, current, and complete and is not misleading or otherwise deceptive.</p>
                              <p>5.2 The Seller undertakes and acknowledges that by not fulfilling these obligations, the Seller’s act or omission shall entitle to Sayidaty Mall to take appropriate legal action.</p>
                              <p>5.3 The Parties agree that the Seller shall be responsible to determine whether any taxes apply to the sale transactions and to collect, report, and remit the correct taxes to the appropriate tax authority, and that Sayidaty Mall shall not be liable for remittance of any tax.</p>
                              <p>5.4 The Seller acknowledges and undertakes that any or all of its Items that are not fulfilled/accepted by the Buyer using Sayidaty Mall’s Services, the Seller shall accept and process or allow Sayidaty Mall to process the returns, refunds and adjustments in accordance with this Agreement. Sayidaty Mall reserves its rights to amend or introduce its policies regarding the process of refund from time to time, on its Site.</p>
                              <p>5.5 The Seller further undertakes that the Seller is committed to reflect new Items in Sayidaty Mall Website when such Items are to be directly sold in the set country.</p>
                              <p>5.6 Seller can benefit from the feature of pre-order, which allows the Buyer to pre-order an Item before being available. The Seller undertakes that the Seller is obliged to fulfill all Items that are set under pre-order feature within the agreed timeframe.</p>
                              <p>5.7 The Seller undertakes to provide an assigned contact to be the point of contact with Sayidaty Mall for any operational and obligations fulfillment.</p>
                              <p>5.8 The Seller further undertakes that the Seller is obliged to respond to Buyer’s queries, conflict or issues that might occur during the transaction process mentioned above within acceptable business hours.</p>
                              <p>5.9 The Seller agrees to abide by the procedures and guidelines, contained in this Agreement, the Help section or mentioned at other place in the Site, for conducting fixed price sales. The Parties agree that the First Party shall have full rights to change any of the procedures and guidelines in the future and such changes will be effective immediately upon posting without any notification to the Seller. The Seller shall regularly refer to the Site to understand the current procedures and guidelines for participating in the business and to be sure that the Item(s) offered by the Seller for sale can be sold on the Site.</p>
                              <p>5.10 In relation to each Item listed by the Seller on the Site, the Seller shall provide Sayidaty Mall with the complete details of regions, including but not limited to name of emirate or country, permitted for trade.</p>
                              <p>5.11 The Parties agree that Sayidaty Mall shall provide the shipment and other logistic services in consideration of the Service Charges under this Agreement. The Seller shall provide Sayidaty Mall and/or any party engaged in logistics all information, facilities and cooperation required for packaging and shipment.</p>
                              <p>5.12 The Seller warrants and undertakes that the quality of the Item shall not be low/bad or defective, and must be acceptable to the Buyer. Any defect, bad quality, missing Item, or complain of any other type relating to Item’s acceptability shall be the sole responsibility of the Seller, and the Seller shall keep Sayidaty Mall indemnified from any harm or loss, of whatsoever nature, in this regard.-</p>
                              <p>5.13 In case the Seller renders the packaging of the Item and further arranges the shipment of the Item to the Buyer without Sayidaty Mall’s involvement, the Seller shall provide to Sayidaty Mall any requested information regarding the packaging, shipment, tracking (if available) ad order status, within the provided time period, and Sayidaty Mall shall be authorized to make any of such information publicly available. If the Seller fails to provide Sayidaty Mall with the confirmation of shipment within the time frame specified by Sayidaty Mall (e.g., 2 days from the date an order was placed), Sayidaty Mall may in its sole discretion cancel, and/or may direct the Seller to stop and/or cancel, any such sale transaction.</p>

                            </td>
                            <td dir="rtl">
                              <p><b>5 التزامات البائع</b></p>
                              <p>- 5 1 یوافق الطرفان یدرج البضائع بالموقع بسعر ثابت ویلتزم البائع ببیع البضائع للمشتري بالأسعار المدرجة الذي یستوفي شروط البائع ومن المتفق علیھ أنھ بإدراج البضائع بسعر ثابت فیتعھد ویضمن البائع للمشترین المحتملین أن البائع یتمتع بكافة الحقوق والقدرة الكاملة للبیع وكما یتعھد بأن إدراج البضائع ھو إدراج دقیق وساري وكامل وأنھ لا ینطوي على أي تضلیل أو ما شابھ ذلك من سبل التدلیس والغش. </p>
                              <p>- 5 2 یتعھد ویقر البائع أنھ في حالة عدم استیفائھ لھذه الالتزامات أو تصرف البائع أو الامتناع عن التصرف فیخول " سیدتي مول " بالحق في اتخاذ الإجراءات القانونیة المناسبة .</p>
                              <p>- 5 3 یتفق الطرفان على أن البائع مسئول عن تحدید أي من الضرائب التي تطبق على صفقات البیع وكذلك تحصیل وإصدار التقاریر وتحویل الضرائب لھیئات الضرائب المعنیة ولا یتحمل " سیدتي مول " تحویل أي ضرائب</p>
                              <p>- 5 4 یقر البائع ویتعھد بأن كافة البنود التي لم یتم توفیرھا / الغیر مقبولة للمشتري باستخدام خدمات " سیدتي مول " فیقبل البائع ویتولى عملیة الإرجاع أو یسمح ل" سیدتي مول " لتولي عملیة الإرجاع واسترداد الأموال والتسویات اللازمة طبقا لھذه الاتفاقیة وكما یحتفظ " سیدتي مول " بحقوقھا في تعدیل أو تقدیم سیاساتھا المتعلقة بعملیة استرداد الأموال من وقت لآخر بالموقع. </p>
                              <p>- 5 5 یتعھد البائع كذلك بالتزامھ بعرض بضائع جدیدة بالموقع الالكتروني ل " سیدتي مول " عندما یجب بیع ھذه البضائع مباشرة في بلد معین. </p>
                              <p>- 5 6 یمكن أن ینتفع البائع من میزة الطلب المسبق والتي تسمح للمشتري طلب بضائع قبل توفیرھا كما یتعھد البائع بالتزامھ بالوفاء بتوفیر البضائع المطلوبة وفقا للطلب المسبق خلال الإطار الزمني المحدد. </p>
                              <p>- 5 7 یتعھد البائع بتوفیر مسئول اتصال لیكون نقطة اتصال مع " سیدتي مول " فیما یتعلق بالأمور التشغیلیة واستیفاء الالتزامات .</p>
                              <p>- 5 8 یتعھد البائع كذلك بالتزامھ بالرد على استفسارات المشتري أو أي منازعات أو أمور تنشأ إبان معالجة صفقة البیع المذكورة أعلاه خلال ساعات العمل المقبولة.</p>
                              <p>- 5 9 یوافق البائع على الالتزام بالإجراءات والتوجیھات المنصوص علیھا بھذه الاتفاقیة أو بقسم المساعدة أو بأي مكان آخر بالموقع لعملیة البیع بسعر ثابت وكما یوافق الطرفان على أن الطرف الأول یتمتع بكامل الحق في تغیر أي من ھذه الإجراءات أو
                              الإرشادات في المستقبل وھذه التغیرات تكون نافذة فور نشرھا دون إشعار مسبق للبائع ، كما یجب أن یرجع البائع بانتظام للموقع لفھم الإجراءات والإرشادات الحالیة للمشاركة في الأعمال التجاریة وللتأكید على أن البضائع المعروضة بواسطة البائع للبیع یمكن بیعھا من خلال الموقع .</p>
                              <p>- 5 10 یجب أن یقدم البائع ل" سیدتي مول " بیانات المناطق المتعلقة بالبضائع المدرجة بالموقع بواسطة البائع ، ویتضمن ذلك على سبیل المثال لا الحصر، اسم الإمارة أو البلد المسموح بھا في مزاولة الأعمال التجاریة .</p>
                              <p>- 5 11 یوافق البائع أن " سیدتي مول " توفر خدمات الشحن والخدمات اللوجیستیة الأخرى مقابل رسوم الخدمة بموجب ھذه الاتفاقیة، كما یقدم البائع ل" سیدتي مول " و / أو أي طرف مشترك بالخدمات اللوجیستیة بكافة المعلومات والتسھیلات والتعاون
                              المطلوب للتعبئة والشحن.</p>
                              <p>- 5 12 یتعھد ویضمن البائع عدم تدني أو سوء جودة البضائع أو تكون معیبة ویجب أن تكون مقبولة للمشتري و یتحمل البائع بمفرده مسئولیة أي عیوب أو تدني في مستوى الجودة أو نقص في البضائع أو الشكاوى من أي نوع آخر متعلقة بقبول المواد، كما
                              یعوض البائع " سیدتي مول " ویبرئ ذمتھا ضد أي ضرر أو خسائر أیا كانت طبیعتھا المتعلقة بھذا الشأن.</p>
                              <p>- 5 13 في حالة تولي البائع توفیر تعبئة البضائع وترتیبات الشحن للمشتري دون تدخل من " سیدتي مول " ، كما یجب أن یوفر البائع لسیدتي مول " أي معلومات مطلوبة متعلقة بالتعبئة الشحن ومتابعة حالات إضافة الطلبات (إن وجد) خلال الوقت المحدد،
                              وكذلك یخول " سیدتي مول " بالحق في إعلان ھذه المعلومات المتاحة وفي حالة إخفاق البائع في إمداد " سیدتي مول " بتأكید الشحن خلال الإطار الزمني الذي حدده " سیدتي مول " ( على سبیل المثال یومین من تاریخ تقدیم أمر الشراء) كما یجوز " لسیدتي مول " حسب تقدیرھا الشخصي إلغاء أو توجیھ البائع لوقف أو إلغاء أي من صفقات البیع ھذه.</p>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p><b>6. PROHIBITED & ILLEGAL ACTIVITIES</b></p>
                              <p>6.1 The Parties agree that the Seller shall use the Site and Services only for lawful purposes and in a lawful manner. The Seller agrees and undertakes to comply with all applicable laws, statutes, and regulations and it shall not register under a false name or use an invalid or unauthorized credit card. Any fraudulent conduct shall be a violation of federal, state and emirate(s) laws, and shall be referred to judicial and law enforcement authorities.</p>
                              <p>6.2 The Parties further agree that Sayidaty Mall shall have the right to monitor any activity and content associated with its Site and investigate as it may deem appropriate. Further, Sayidaty Mall reserves the right and shall have absolute discretion to remove, screen, or edit any content from the Site that violates these provisions or is otherwise objectionable.</p>
                              <p>6.3 The Seller agree and acknowledge that Sayidaty Mall may access and disclose any information in relation to any sale transaction to law enforcement officials, regulators, or other third parties, it considers necessary or appropriate, including but not limited to user contact details, IP addressing and traffic information, usage history, and content posted on the Site.</p>
                              <p>6.4 The Parties agree that the Seller shall not, and shall cause its Affiliates not to, directly or indirectly disclose, convey or use any order information or other data or information acquired by the Seller or its Affiliates from Sayidaty Mall or its Affiliates (or otherwise) as a result of the transaction contemplated under this Agreement.</p>
                            </td>
                            <td dir="rtl">
                              <p><b>6 الأنشطة المحظورة والغیر مشروعة</b></p>
                              <p>- 6 1 یوافق الطرفان على استخدام البائع للموقع والخدمات فقط للأغراض المشروعة وبطریقة قانونیة، كما یوافق ویتعھد البائع بالامتثال لكافة القوانین والتشریعات واللوائح المعمول بھا وعدم التسجیل باسم مزیف أو استخدام بطاقات ائتمان غیر ساریة أو غیر مصرح بھا كما یعد ارتكاب أي تدلیس أو غش انتھاكا للقوانین الفیدرالیة والحكومیة والإماراتیة وت ُح الللھیئات القضائیة وھیئات إنفاذا لقانون .</p>
                              <p>- 6 2 یوافق الطرفان كذلك على أن " سیدتي مول " یتمتع بالحق في مراقبة أي أنشطة أو المحتوى المتعلق بالموقع والتحقیق حسب المطلوب، كما أن " سیدتي مول " یحتفظ بالحق والاختیار المطلق حسب تقدیره في إزالة ومراقبة وكتابة أي محتوى من  الموقع والذي یشكل انتھاك لھذه الأحكام أو یكون مرفوض وما شابھ ذلك.</p>
                              <p>- 6 3 یوافق البائع ویقر كذلك بأن " سیدتي مول " یجوز لھ الإطلاع وإعلان المعلومات المتعلقة بأي صفقات بیع لأي من مسئولي تنفیذ القانون أو الھیئات التنظیمیة أو الغیر في حالة اعتبار ذلك ضروریا أو مناسبا والذي یتضمن على سبیل المثال لا الحصر  بیانات الاتصال للمستخدم و عنوان برتوكول الانترنت (IP (ومعلومات عن معدل استخدام الموقع وسجل الاستخد اموالمحتوى الذيی ُنشر ب المو قع.</p>
                              <p>- 6 4 یتفق الطرفان على أنھ لا یجوز للبائع وإلزام الشركات التابعة لھ بعدم القیام بالإفصاح بشكل مباشر أو غیر مباشر أو نقل أو استخدام أي من المعلومات والبیانات المتعلقة بأمر الشراء أو المعلومات التي حصل علیھا البائع والشركات التابعة لھ من " سیدتي مول " أو الشركات التابعة لھا ( أو خلاف ذلك نتیجة للتعاملات المتضمنة بھذه الاتفاقیة.</p>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p><b>7. WARRANTIES AND REPRESENTATIONS</b></p>
                              <p>7.1 The Parties represent and warrant that they have the legal right and full power and authority to enter into and consummate the transaction contemplated by this Agreement. Neither the execution nor the performance of this Agreement nor compliance with the terms and provisions hereof, will conflict or be inconsistent with any of the terms, covenants, conditions or provisions of, or constitute a default under or result in a breach of: (i) any law, rule or regulation; (ii) any judgment, order, injunction, decree or ruling of any arbitral tribunal or any court or governmental authority, domestic or foreign, which is or may be applicable to the Parties, or which would prevent or restrict the execution or performance of this Agreement and the transaction contemplated thereunder by the Parties.</p>
                              <p>7.2 The Parties agree that neither Sayidaty Mall nor its Affiliates make any other representations or warranties of any kind, express or implied, including without limitation:</p>
                              <p>a) that the Site or the Services shall meet the Seller’s requirements, shall always be available, accessible, uninterrupted, timely, secure, or operate without error.</p>
                              <p>b) that the information, content, materials, or Items appeared on the Site shall be as represented by Sellers, available for sale at the time of fixed price sale, lawful to sell, or that Seller or Buyer shall perform as promised.</p>
                              <p>c) any implied warranty arising from course of dealing or usage of trade.</p>
                              <p>d) any obligation, liability, right, claim, or remedy in tort, whether or not arising from the negligence of Sayidaty Mall or its Affiliates.</p>
                              <p>Moreover, to the full extent permissible under applicable law, Sayidaty Mall and its Affiliates disclaim any and all such warranties.</p>
                              <p>7.3 The Seller agrees and undertakes that Sayidaty Mall and its Affiliates are not involved in sale transactions between the Seller and the Buyer or other participant dealings, therefore in case of any dispute arising between one or more participants, the Seller shall release Sayidaty Mall or its Affiliates (and their respective agents and employees) from claims, demands, and damages (actual and consequential) of any kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way connected with such dispute.</p>
                              <p>7.4 The Parties agree that the Seller shall defend, indemnify and hold harmless Sayidaty Mall and its Affiliates (and their respective employees, directors, agents and representatives) from and against any and all claims, costs, losses, damages, judgments, penalties, interest and expenses (including reasonable attorneys’ fees and other legal costs) arising out of or in connections with any claim/difference that may arise out of or relates to: (i) any actual or alleged breach of the Seller’s representations, warranties, or obligations set forth in this Agreement; or (ii) the Seller’s own website or other sales channels, the Items the Seller sells, any content the Seller provides, the advertisement, offer, sale or return of any Item the Seller sells, any actual or alleged infringement of any intellectual property or proprietary rights by any Items the Seller sells or content the Seller provides, or applicable tax or the collection, payment or failure to collect or pay tax, if any.</p>
                              <p>7.5 The Seller undertakes that the Items that it sells on Sayidaty Mall are not displayed on any other websites with a price lower than offered on Sayidaty Mall website.</p>

                            </td>
                            <td dir="rtl">
                              <p><b>7 التعھدات والضمانات</b></p>
                              <p>- 7 1 یتعھد ویضمن كلا الطرفان بأن كل منھما یتمتعا بالحق  القانوني والصلاحیة والسلطة الكاملة لإبرام الاتفاقیة وإتمام الصفقات المتضمنة بھذه الاتفاقیة وكذلك أي من تنفیذ أو أداء ھذه الاتفاقیة أو الامتثال للشروط والأحكام المنصوص علیھا بھا لا یتعارض أو یكون غیر متسق مع أي من شروط أو تعھدات أو أحكام أو تشكل تقصیر بموجب أو ناجمة من انتھاك أي (1 (قانون أو أحكام أو لوائح (2 ( كم قضائي أو طلب أو أمر قضائي أو مرسوم أو أي حكم صادر من ھیئة تحكیم أو محكمة أو ھیئات حكومیة أو ھیئات محلیة أو خارجیة والتي یخضع أو یجوز أن یخضع لھا الطرفین أو تمنع وتحیل دون تنفیذ أو أداء ھذه الاتفاقیة أو الصفقات المتضمنة بھا</p>
                              <p>- 7 2 یوافق الطرفان على أنھ لا یقدم سیدتي مول أو الشركات التابعة لھ أي تعھدات أو ضمانات أیا كان نوعھا ، سواء كان ذلك صراحة أو ضمنا، والتي تشمل على سبیل المثال لا الحصر :
                              <br>- أ أن الموقع والخدمات یجب أن تفي بمتطلبات البائع وأنھا تكون دائما متاحة ومسموح بدخولھا وغیر منقطعة ومنضبطة ومضمونة وتعمل دون أي خطأ.
                              <br>- ب أن المعلومات والمحتوى والمواد والبضائع المعروضة بالموقع یتم عرضھا وفقا للبائع وإتاحتھا للبیع في وقت تحدید الأسعار أو شرعیة في بیعھا أو أداء البائع أو المشتري حسب التعھدات.
                              <br>- ج أي تعھدات ضمنیة ناجمة من تنفیذ الصفقات أو استخدام التجارة
                              <br>- د أي مسئولیات أو التزامات أو حقوق أو مطالبات أو تعویض عن أي ضرر سواء كان ذلك ناجم من أي إھمال من " سیدتي مول " أو الشركات التابعة لھ أو لا . بالإضافة إلى أنھ في أقصى حد تسمح بھ القوانین الساریة فإن " سیدتي مول " والشركات التابعة لھ تخلي مسئولیتھا من جمیع ھذه التعھدات  </p>
                              <p>- 7 3 یوافق البائع ویتعھد أن " سیدتي مول " والشركات التابعة لھ غیر طرف في صفقات البیع بین البائع والمشتري أو أي تعاملات للمشاركین وعلیھ ففي حالة إذا نشبت منازعات بین واحد أو أكثر من المشاركین فإن البائع یعفي سیدتي مول والشركات التابعة لھ ( ومن یتبعھ من وكلاء وموظفین معنیین ) من كافة الطلبات والمطالبات والأضرار سواء كانت الفعلیة أو التبعیة أیا كان نوعھا أو طبیعتھا وسواء كانت معروفة أو غیر معروفة ، مشتبھ بھا أو غیر مشتبھ بھا، تم الإفصاح عنھا أو لم یتم الإفصاح عنھا والناجمة من أو فیما یتعلق بھذه المنازعات بأي حال.</p>
                              <p>- 7 4 یوافق الطرفان على أن البائع یدافع عن ویعوض ویدفع الضرر عن " سیدتي مول " والشركات التابعة لھ ( ومن یتبعھم من موظفین ومدیرین ووكلاء ومندوبین معنیین) من وضد كافة المطالبات والتكالیف والخسائر والأضرار والأحكام والغرامات والفوائد  والنفقات (والتي تشمل أتعاب المحاماة المعقولة والنفقات القانونیة الأخرى) والناجمة من أو فیما یتعلق بأي إدعاءات / خلافات والتي یمكن أن تنشأ من أو فیما یتعلق (1 (أي انتھاك فعلي أو مدعى بھ لأي من تعھدات وضمانات والتزامات البائع المنصوص علیھا بھذه الاتفاقیة (2 (الموقع الالكتروني الخاص بالبائع أو أي قنوات بیع أو البنود التي یبیعھا البائع أو المحتوى الذي یقدمھ البائع والإعلانات والبیع والعرض وإرجاع أي من البضائع التي باعھا البائع وأي انتھاك فعلي أو مدعى بھ لأي من حقوق الملكیة الفكریة أي من  البنود التي یبیعھا البائع أو المحتوى الذي یقدمھ البائع أو الضرائب المعمول بھا والتحصیل والدفع والإخفاق في دفع  أو تحصیل الضرائب، إن وجد. </p>
                              <p>- 7 5 یتعھد البائع أن البضائع التي یبیعھا في سیدتي مول غیر معروضة للبیع في أي موقع الكتروني آخر بأسعار أقل من الأسعار المعروضة في موقع " سیدتي مول " الالكتروني </p>

                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p><b>8. INTELLECTUAL PROPERTY</b></p>
                              <p>8.1 Any Intellectual Property rights relating to the Site or arising out of its use, shall belong to Sayidaty Mall and the Seller may utilize the same in a manner as agreed in this Agreement. This Article shall survive the termination of this Agreement.</p>
                              <p>8.2 The Seller shall grant Sayidaty Mall a limited, royal free, worldwide, non-exclusive license to use its Intellectual Property for the purpose of identification, operation or promotion of the Services in association with the subject contents / property / Item/ product.</p>
                              <p>8.3 In case of any claim, loss, or dispute arising between Sayidaty Mall and any third party in relation to ownership or rights of the contents of the Site, the Seller shall indemnify Sayidaty Mall from such claim, loss or any liability.</p>
                              <p>8.4 Further, the Parties agree that the Seller shall be liable to pay any tax or fees etc. applicable in the Territory in relation to execution of this Agreement.</p>

                            </td>
                            <td dir="rtl">
                              <p><b>8–الملكیة الفكریة</b></p>
                              <p>- 8 1 أي حقوق ملكیة فكریة متعلقة بالموقع أو ناتجة من استخدامھ فتكون من حق " سیدتي مول " ویجوز للبائع استخدامھا وفقا لما ھو متفق علیھا بالاتفاقیة ویستمر سریان ھذا الحكم بعد انتھاء الاتفاقیة .</p>
                              <p>- 8 2 یمنح البائع " سیدتي مول " رخصة محدودة، بدون رسوم، عالمیة وغیر حصریة لاستخدام الملكیة الفكریة الخاصة بھ بغرض تعریف وتشغیل وترویج الخدمات فیما یتعلق بمحتویات الموضوع / الملكیة / البضائع والمنتجات. </p>
                              <p>- 8 3 في حالة أي مطالبات أو خسائر أو منازعات ناتجة من سیدتي مول والغیر فیما یتعلق بملكیة وحقوق محتوى الموقع فإن البائع یعوض " سیدتي مول " عن أي من ھذه المطالبات والخسائر والالتزامات.</p>
                              <p>- 8 4 یوافق الطرفان أن البائع ملتزم بدفع أي بضائع أو رسوم وما شابھ ذلك من الأمور المعمول بھا بالنطاق الجغرافي فیما یتعلق بتنفیذ الاتفاقیة.</p>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p><b>9. DEFAULT</b></p>
                              <p>9.1 In the event that:</p>
                              <p>a) a custodian be appointed to a Party; or</p>
                              <p>b) a Party be adjudged bankrupt or insolvent or has any resolution for its winding up duly passed; or</p>
                              <p>c) any order for the winding up of any Party is made by any court of law;</p>
                              <p>then such Party (“Insolvent Party”) and its member shall immediately notify the other Party.</p>
                              <p>9.2 Upon the occurrence of one of the events described in Article 10.1 the Party, other than the Insolvent Party, shall be entitled to all payments thereafter made by the Buyer(s) towards the sale transaction through the Site and shall hereby irrevocably constituted and appointed the attorney of such Insolvent Party and be authorized and empowered to endorse the name of such Insolvent Party on all cheques or drafts or payment through other manner, received on account of such payments on which the name of such Insolvent Party appears as payee.</p>
                              <p>9.3 The trustees, receivers, liquidator or other person lawfully entrusted to handle the affairs of an Insolvent Party shall be entitled to receive its share of the earnings / Service Charges, after deduction of proceeds, costs, expenses etc. payable under this Agreement computed as of the date on which the bankruptcy, insolvency, receivership or winding up occurred.</p>
                              <p>9.4 The provisions of this Article 10 shall also apply mutatis mutandis where a Party breaches any of its obligations hereunder and fails to remedy such breach or non-performance, if capable of remedy, within fifteen (15) days, after receipt of notice from other Party requiring such remedy.</p>

                            </td>
                            <td dir="rtl">
                              <p><b>9 التقصیر</b></p>
                              <p>- 9 1 في حالة :
                              <br>(1 (تعیین وصي على أي طرف
                              <br>(2 (في حالة إصدار حكم بإفلاس أو إعسار أي طرف أو إصدار قرار صحیح بتصفیة أي طرف
                              <br>(ج) أو إصدار أي حكم بالتصفیة من المحكمة القانونیة وعلیھ فیجب أن یخطر ھذا الطرف (الطرف المعسر) ومن یتبعھ من أعضاء الطرف الآخر
                              </p>
                              <p>- 9 2 فور وقوع أي من الأحداث المنصوص علیھا - بالمادة 9 1 فیكون من حق الطرف الغیر معسر كافة الدفعات المقدمة من المشتري مقابل صفقات البیع من خلال الموقع الالكتروني ویجب تكوین وتعیین بما لا رجعة فیھ محامي عن الطرف المعسر والذي یخول ویفوض بالتصدیق باسم الطرف المعسر على كافة الشیكات والكمبیالات  والدفعات والدفع بالطرق الأخرى واستلام الدفعات بالحسابات التي یكون فیھا اسم الطرف المعسر " المستفید" </p>
                              <p>- 9 3 یتمتع أي من الوصي أو مأمور قضائي أو مصفي أو أي شخص موكل قانونیا بمباشرة شئون  المعسر بالحق في استلام حصتھ في المكسب ورسوم الخدمات بعد خصم العائد والتكالیف والنفقات المدفوعة بموجب ھذه الاتفاقیة والتي یتم حسابھا بدایة من  تاریخ الإفلاس أو الإعسار أو الحراسة القضائیة أو التصفیة . </p>
                              <p>- 9 4 تطبق ھذه الأحكام المنصوص علیھا في ھذه المادة 9 ،مع تغییر ما یلزم، عند انتھاك أي طرف لالتزاماتھ بموجب الاتفاقیة وإخفاقھ في تدارك ھذا الانتھاك أو عدم الأداء، إن كان قابل للإصلاح، في غضون 15 یوم بعد استلام إشعار من الطرف الآخر مطالبا  بالإصلاح.</p>

                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p><b>10. DURATION AND TERMINATION</b></p>
                              <p>10.1 The duration of this Agreement shall run continuously from the date of the Agreement for a period of 12 months and shall be automatically renewed unless specified in writing by either Party within 60 days with prior written notice to the other Party.</p>
                              <p>10.2 Each Party has the right to terminate the Agreement, if the other Party:</p>
                              <p>a) Commits any breach of or fails to perform or observe any terms or conditions or covenants enumerated under this Agreement; or</p>
                              <p>b) Is declared a bankrupt, commits an act of bankruptcy or enters into any composition or arrangement with its creditors or enters into liquidation whether voluntary or compulsory; Sayidaty Mall may subject to the terms of this Agreement annul the sale transaction of the Seller’s Item(s) and shall forthwith terminate this Agreement.</p>
                              <p>10.3 The Parties agree that Sayidaty Mall, in its sole discretion, may terminate this Agreement, access to the Site or other services under this Agreement, or any current fixed price sales immediately without notice for any reason. Furthermore, Sayidaty Mall, in its sole discretion, also may prohibit the Seller from listing any items for sale.</p>

                            </td>
                            <td dir="rtl">
                              <p><b>10 المدة والفسخ</b></p>
                              <p>- 10 1 تستمر ھذه الاتفاقیة بدایة من تاریخ تحریر الاتفاقیة ولمدة 12 شھر وتُجددتلقائیامال میشعر أحد الطرفین الطرف الآخر تحریریا قبل موعد انتھاء الاتفاقیة ب 60 یوم بخلاف ذلك. </p>
                              <p>- 10 2 یتمتع كل طرف بالحق في فسخ الاتفاقیة في حالة إذا قام الطرف الآخر بأي من الأمور التالیة:
                              <br> 1 (ارتكاب أي انتھاك أو الإخفاق في أداء أو الالتزام بأي من الأحكام والشروط والتعھدات المنصوص علیھا بھذه الاتفاقیة .
                              <br> 2 (إشھار إفلاسھ أو اتخاذ أي إجراءات للإفلاس أو أبرم تسویات أو اتفاقیات مع الدائنین أو قام بالتصفیة الإجباریة أو الاختیاریة ویجوز لسیدتي مول وفقا للأحكام المنصوص علیھا بھذه الاتفاقیة إلغاء صفقات بیع بضائع البائع وفسخ ھذه الاتفاقیة. </p>
                              <p>- 10 3 یوافق الطرفان أنھ یجوز لسیدتي مول حسب تقدیرھا الشخصي فسخ الاتفاقیة والدخول للموقع والخدمات المنصوص علیھا بھذه الاتفاقیة أو أي مبیعات بالأسعار الثابتة الحالیة على الفور بدون إشعار لأي سبب ، وبالإضافة إلى أنھ یجوز لسیدتي مول حسب  تقدیرھا منع البائع من إدراج أي بضائع للبیع </p>

                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p><b>11. GOVERNING LAW AND RESOLUTION OF DISPUTES</b></p>
                              <p>11.1 This Agreement shall be governed by and construed in accordance with the laws of the Emirate of Dubai and the Federal Laws of the United Arab Emirates.</p>
                              <p>11.2 In the event of any dispute or difference between the Parties arising out of or in connection with or relating to this Agreement, representatives of the Parties shall, within twenty (20) days of a written notice from one Party to the other Party (a Dispute Notice), hold a meeting (a Dispute Meeting) in an effort to resolve the dispute or difference. Each Party shall use all reasonable endeavours to send a representative who has authority to settle the dispute or difference at the Dispute Meeting.</p>
                              <p>11.3 Any dispute or difference which is not resolved within forty (40) days after the service of a Dispute Notice, whether or not a Dispute Meeting has been held, shall, at the request of either Party, following the expiry of the forty (40) day period, be referred to arbitration under the rules of the Dubai International Arbitration Centre (the Rules) and the UAE laws, before a single arbitrator who shall be appointed in accordance with the Rules and the Parties further agree that the award of the arbitrator shall be final and binding upon the Parties.</p>
                              <p>11.4 The place of arbitration shall be Dubai and the language of the arbitration shall be English. The Parties waive any right of appeal to any court in any jurisdiction, insofar as such waiver can validly be made.</p>

                            </td>
                            <td dir="rtl">
                              <p><b>11 القوانین الحاكمة والاختصاص القضائي</b></p>
                              <p>- 11 1 تخضع ھذه الاتفاقیة في تفسیرھا وسریان أحكامھا للقوانین المعمول بھا في إمارة دبي و والقوانین الفیدرالیة للإمارات العربیة المتحدة. </p>
                              <p>- 11 2 في حالة إذا نشبت أي منازعات أو خلافات بین الطرفین والناجمة من أو فیما یتعلق أو ذات صلة بھذه الاتفاقیة فیعقد المندوبین المخولین نیابة عن الأطراف في غضون عشرین(20 (یوم من إرسال أحد الطرفین إشعار تحریري للآخر ( إشعار المنازعات)  اجتماعا (اجتماع المنازعات) مع بذل الجھود لمحاولة فض ھذه المنازعات أو  الخلافات ویبذل كلا الطرفین المساعي المعقولة بإرسال المندوب الذي یتمتع بصلاحیة تسویة المنازعات والخلافات في اجتماع المنازعات.</p>
                              <p>- 11 3 أي منازعات أو خلافات لم یتم تسویتھا في غضون 40 یوم من إرسال إشعار المنازعات وسواء تم انعقاد اجتماع تسویة المنازعات أو لا وعلیھ حسب طلب أي من الطرفین بعد انتھاء مدة 40 یوم تحال المنازعات للتحكیم بموجب قواعد مركز دبي للتحكیم الدولي ( القواعد) والقانون الإماراتي، أمام محكم واحد یتم تعیینھ وفقا للقواعد ویوافق الطرفان كذلك أن الحكم الصادر من التحكیم یكون حكما نھائیا وملزما للطرفین.</p>
                              <p>- 11 4 یكون مقر التحكیم بدبي ولغة التحكیم ھي اللغة الإنجلیزیة كما یتنازل الطرفین عن الحق في تقدیم طلب لأي محكمة بأي اختصاص قضائي في حالة إذا أمكن القیام بھذا التنازل صحیحا</p>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <p><b>12. GENERAL</b></p>
                              <p>12.1 If any part of any provision of this Agreement shall be or become invalid or unenforceable, then the remainder of such provision and all other provisions of this Agreement shall remain valid and enforceable.</p>
                              <p>12.2 The Parties mutually agree that this Agreement is binding on the Parties.</p>
                              <p>12.3 For purposes of this Agreement ‘Force Majeure’ shall mean an event beyond the reasonable control of the Seller and/ or Sayidaty Mall including but not limited to acts of God, war, terrorism, strikes, accidents, riots, floods, industrial action, earthquakes or any operation of the forces of nature that reasonable foresight and ability on the part of the affected Party could not provide against.</p>
                              <p>12.4 This Agreement constitutes the entire agreement between the Parties hereto and no amendment shall be binding unless agreed upon by the Parties in writing. This Agreement supersedes any previous understanding or agreement between the Parties.</p>
                              <p>12.5 All communications relating to this Agreement shall be in writing and delivered by email, courier or fax to the Party concerned at the relevant address shown at the start of this Agreement (or such other address as may be notified from time to time in accordance with this clause by the relevant party to the other Party).</p>
                            </td>
                            <td dir="rtl">
                              <p>12 أحكام عامة</p>
                              <p>- 12 1 في حالة إذا كان أو أصبح أي جزء من أي من الأحكام المنصوص علیھا بھذه الاتفاقیة باطلا أو غیر نافذا فإن باقي ھذا الحكم أو الأحكام الأخرى بھذه الاتفاقیة تظل ساریة ونافذة . </p>
                              <p>- 12 2 یتفق الطرفان بالتبادل أن ھذه الاتفاقیة ملزمة للطرفین </p>
                              <p>- 12 3 لغرض ھذه الاتفاقیة یقصد بالقوة القاھرة الأحداث الخارجة عن السیطرة المعقولة للبائع أو سیدتي مول أو كلاھما والتي تتضمن على سبیل المثال لا الحصر الأقدار والحرب والإرھاب والاحتجاجات و الحوادث والشغب والسیول والقوانین الصناعیة والزلازل وأي أمور تتعلق بقوى الطبیعة والتي تكون خارجة عن التوقع المعقول أو القدرة على تجنبھا من الطرف المتضرر.</p>
                              <p>2 4 تشكل ھذه الاتفاقیة مجمل الاتفاق بین الطرفین ولا تكون أي تعدیلات ملزمة ما لم یتم الموافقة علیھا تحریریا من كلا الطرفین وتحل ھذه الاتفاقیة محل كافة التفاھمات والاتفاقیات السابقة بین الطرفین . </p>
                              <p>- 12 5 كافة المكاتبات المتعلقة بھذه الاتفاقیة یجب أن تكون تحریریة وتسلم بواسطة البرید الالكتروني أو البرید السریع أو الفاكس للطرف المعني على العنوان ذا الصلة المنصوص علیھ في صدر ھذه الاتفاقیة ( و أي عنوان آخر یخطر بھ من وقت لآخر أحد الطرفین للطرف الآخر حسب ما ھو منصوص علیھ بھذا البند) </p>
                            </td>
                          </tr>
                        </table>

                      </div>
                      <br>
                      <INPUT TYPE="checkbox" NAME="terms" id="terms" VALUE="Conditions" onchange="activate_button()"> "I have read and agree to the following" Terms and conditions
                    </div>

                    <br><br>
                    <div class="field buttons">
                    <button type="button" class="prev btn btn-primary">Prev</button>
                    <button type="button" class="submit btn btn-primary" id="submit_form" onclick='event.preventDefault();success()' disabled><?php echo $sell_title; ?></button>
                    </div>
                  </section>

              </div>
              </div>
            </div>
          </form>
      </div>
      </div>

      </div>
      </div>



</div>
</div>
<script>

    $('form.idealforms').idealforms({

      silentLoad: false,

      rules: {
        'dont_remove_it' : 'required',
        'firstname' : 'required',
        'lastname': 'required',
        'email': 'required email',
        'fbs_email': 'email',
        'telephone': 'required digits',
        'user_password': 'required',
        'confirm_password': 'required equalto:user_password',
        'num_of_branches': 'digits',
        'admin_name' : 'required',
        'admin_email' : 'required email',
        'admin_phone' : 'required digits',
        'company_name' : 'required',

        'picture': 'required extension:jpg:png',
        'opening_hours': 'required',
        'screen_name': 'required',
        'warehouse_address': 'required',
        'product_description': 'required',
        'legal_info[legal_name]': 'required',
        'legal_info[brand_name]': 'required',
        'legal_info[email_address]': 'required email',
        'legal_info[office_num]': 'required digits',
        'legal_info[moblie_num]': 'required digits',
        'legal_info[trade_license_number]': 'required',
        'legal_info[address_license_number]': 'required',
        'legal_info[legal_name]': 'required',
        'passport_num': 'required',
        'passport_copy': 'required',
        'license': 'required',
        'logo': 'required',
        'financail_info[fin_contact_name]': 'required',
        'financail_info[fin_email]': 'required email',
        'financail_info[fin_num]': 'required digits',
        'financail_info[beneficiary_name]': 'required',
        'financail_info[bank_name]': 'required',
        'financail_info[bank_account]': 'required',
        'financail_info[swift_code]': 'swift',
        'financail_info[currency]': 'required',
        'financail_info[doc_company_name]' : 'required',
        'financail_info[business_address]' : 'required',
        'financail_info[tax_reg_num]'      : 'required',
        'financail_info[issue_date]'       : 'required'

      },

      errors: {
        'firstname': {
          ajaxError: 'Username not available'
        }
      },


    });

    function validate_next(element){
      var disable_next = false;
      var buttons_div = '';
      var parent_dev  = element.parent().parent();
      if(element.parent().hasClass("co-phone")){
         parent_dev  = element.parent().parent().parent().parent();
      }

      parent_dev.children().each(function(){
        if($(this).is('div') && ! $(this).hasClass("dont_remove_it")){
          if($(this).hasClass("required") ){
            if ($(this).find("input").val() == '' || $(this).find("textarea").val() == ''){
              disable_next = true;
            }
          }else if($(this).hasClass("intl-tel-input")){
            if ($(this).find("div.form-group div.co-phone input.phone-num").val() == '' ){
              disable_next = true;
            }
          }else if($(this).hasClass("buttons")){
            buttons_div = $(this);
          }
        }
      });

      if(buttons_div != ''){
        $(buttons_div.find("button.next").attr("disabled",true));
        if(! disable_next ){
          $(buttons_div.find("button.next").attr("disabled",false));
        }
      }
    }


</script>

<script>
$(document).ready(function() {
    $('#datetimepicker6').datetimepicker({
        format: 'YYYY-MM-DD'
    }).on('dp.change', function (e) { validate_next($('input#beneficiary_name')) });
  $(".phone").intlTelInput();

  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

  $("span.ideal-check").each(function(){
    $(this).remove();
  })

  $("#available_countries").change(function(){
    if($(this).val() == '0'){
      $("#excluded-countries").show();
    }else{
      $("#excluded-countries").hide();
    }
  })

  $('[data-toggle="popover"]').popover({
    container: 'body'
  });

  $("#is_both").next("span").remove();
  $("#country").trigger("change");

  $("ul.country-list li").on("click",function(){
    var code = '00'+$(this).attr('data-dial-code');
    $(this).parent().parent().parent().find("input.telcode").val(code);

  });

  $("#user_email").on("keyup",function(){
    $("#user_email_error").html("");
    $.ajax({
        url: 'index.php?route=account/register/validate_user_email&email='+$(this).val(),
        type: 'post',
        dataType: 'json',
        success: function (json) {
          if(json){
            if (json['Error']) {
              $("#user_email").parent().addClass("invalid");
              $("#user_email_error").html(json['Error']);
            }
          }
        },
    });
  });

  $(".iban").on("keyup",function(){
    $(this).parent().removeClass("invalid");
    if(! IBAN.isValid($(this).val())){
      all_filled = false;
      $(this).parent().addClass("invalid");
    }
  });

  $(".form-control").on("keyup",function(){
    validate_next($(this));
  });

  $('.prev').click(function(){
    $('.prev').show();
    $('form.idealforms').idealforms('prevStep');
  });

  $('.next').click(function(){
    next = true;
    $(this).parent().parent().find("div.invalid").each(function(ele){
      if(! $(this).hasClass("dont_remove_it")){
          next = false;
      }
    });

    if(next){

      var array_data = '';

      $(this).parent().parent().find("div.form-group input, div.form-group select,  div.form-group textarea").each(function(ele){
        if($(this).is(':checkbox')){
          array_data += "&execluded_countries["+$(this).val()+']='+ encodeURIComponent($(this).is(':checked'))  ;
        }else{
          array_data += "&"+$(this).attr("name")+'='+ encodeURIComponent($(this).val())  ;
        }
      });

      array_data += "&section_num="+ $(this).parent().parent().attr("data-section-num") ;
      array_data += "&email="+ encodeURIComponent($("#user_email").val()) ;

      var section_num = $(this).parent().parent().attr("data-section-num");
      if($(this).parent().parent().attr("data-section-num") == 4){
        array_data += "&is_both="+$('#is_both').is(":checked");
      }

      $.ajax({
          url: 'index.php?route=account/register/save_data',
          type: 'post',
          dataType: 'json',
          data: array_data,
          success: function (json) {
            if(json){
              if(json['Error']){
                $("#user_email_error").parent().addClass("invalid");
                $("#user_email_error").html(json['Error']);
              }else{
                $('.next').show();
                window.scrollTo(0,0);
                $('form.idealforms').idealforms('nextStep');
              }
            }
          },
      });
        if($(this).parent().parent().attr("data-section-num") == 4) {
            var myFormData = new FormData();
            myFormData.append('document', $("#document")[0].files[0]);
            myFormData.append('email', encodeURIComponent($("#user_email").val()));
            myFormData.append('doc_flag', '1');
            $.ajax({
                url: 'index.php?route=account/register/save_data',
                type: 'POST',
                processData: false, // important
                contentType: false, // important
                dataType: 'json',
                data: myFormData,
                success: function (data) {
                    if (data['error']) {
                        alert(data['error'])
                    }
                    if (data['success']) {
                        $("#document_hidden").val(data['success']);
                    }
                }
            });
        }
    }
  });



  $("#is_both").on("click",function(){
    if ($("#is_both").is(":checked")){
      $("#bank_number").attr("disabled",true);
      $("#swift_code").attr("disabled",false);
      $("#iban_code").attr("disabled",false);
      $("#swift_code").parent().addClass('required');
      $("#iban_code").parent().addClass('required');
    }else{
      $("#bank_number").attr("disabled",false);
      if($("#bank_number").val()==1){
        $("#swift_code").attr("disabled",true);
        $("#iban_code").attr("disabled",false);
        $("#swift_code").parent().removeClass('required');
        $("#iban_code").parent().addClass('required');
      }else{
        $("#iban_code").attr("disabled",true);
        $("#swift_code").attr("disabled",false);
        $("#swift_code").parent().addClass('required');
        $("#iban_code").parent().removeClass('required');
      }
    }
  });

  $("#bank_number").on("change",function(){
    $("#swift_code").parent().removeClass('invalid');
    $("#iban_code").parent().removeClass('invalid');

    if($(this).val()==1){
      $("#swift_code").attr("disabled",true);
      $("#iban_code").attr("disabled",false);
      $("#swift_code").parent().removeClass('required');
      $("#iban_code").parent().addClass('required');
    }else{
      $("#iban_code").attr("disabled",true);
      $("#swift_code").attr("disabled",false);
      $("#swift_code").parent().addClass('required');
      $("#iban_code").parent().removeClass('required');
    }
  });

  var is_mobile = <?php echo $is_mobile ?>;
  if(is_mobile == 1) {
    $(".flag-container").each(function(){
      $(this).attr("style",'margin: 5px 6px 0px 0;');
    });
    $("#is_both").attr("style",'width: 20%;');
    $("#is_both").addClass("col-xs-2");
    $("#is_both").next('label').addClass("col-xs-8 col-xs-offset-1 ");

    $("#terms").attr("style",'width: 20%;');
    $("#terms").addClass("col-xs-2");
    $("#terms").next("span").remove();
    $("#terms").next('label').addClass("col-xs-10 col-xs-offset-1 ");

    $(".ideal-file-wrap").parent().removeClass("row");
  }else{
    $("#is_both").attr("style",'position: relative; margin-left: 10px;');
    $("#terms").next("span").remove();
  }


  var flag = '';
  flag = $("#customer_phone").prev("div").find("ul").find("[data-dial-code='" + <?php echo (isset($seller_data['telecode']) ? $seller_data['telecode'] : '')?> + "']").attr('data-country-code');
  $("#customer_phone").prev("div").find("div.selected-flag div").first().removeClass().addClass("iti-flag "+flag);

  flag = $("#admin_telecode").prev("div").find("ul").find("[data-dial-code='" + <?php echo (isset($seller_data['admin_telecode']) ? $seller_data['admin_telecode'] : '')?> + "']").attr('data-country-code');
  $("#admin_telecode").prev("div").find("div.selected-flag div").first().removeClass().addClass("iti-flag "+flag);

  flag = $("#office_num_div").prev("div").find("ul").find("[data-dial-code='" + <?php echo (isset($seller_data['direct_officer_number_telecode']) ? $seller_data['direct_officer_number_telecode'] : '')?> + "']").attr('data-country-code');
  $("#office_num_div").prev("div").find("div.selected-flag div").first().removeClass().addClass("iti-flag "+flag);

  flag = $("#moblie_num_div").prev("div").find("ul").find("[data-dial-code='" + <?php echo (isset($seller_data['telephone_telecode']) ? $seller_data['telephone_telecode'] : '')?> + "']").attr('data-country-code');
  $("#moblie_num_div").prev("div").find("div.selected-flag div").first().removeClass().addClass("iti-flag "+flag);

  flag = $("#fin_number").prev("div").find("ul").find("[data-dial-code='" + <?php echo (isset($seller_data['fin_telephone_telecode']) ? $seller_data['fin_telephone_telecode'] : '')?> + "']").attr('data-country-code');
  $("#fin_number").prev("div").find("div.selected-flag div").first().removeClass().addClass("iti-flag "+flag);


});

function countChar(val) {
  var len = val.value.length;
  if (len >= 300) {
    val.value = val.value.substring(0, 300);
  } else {
    $('#charNum').text(300 - len);
  }
};

function upload_image(id){
  var myFormData = new FormData();
  myFormData.append(id, $("#"+id)[0].files[0]);

  $.ajax({
    url: 'index.php?route=account/register/save_data' ,
    type: 'POST',
    processData: false, // important
    contentType: false, // important
    dataType : 'json',
    data: myFormData,
    success: function (data){
      if (data['error']) {
          alert(data['error'])
      }
      if (data['success']) {
        $("#"+id+"_hidden").val(data['success']);
      }
    }
  });
}

function success(){
  window.location = '<?php echo $action ?>';
}
</script>


<script>
var cities = <?php echo json_encode($cities); ?>;

var seller_display = function (data){
  thisid = data.currentTarget.id; //get id of current selector
  $('#'+ thisid + ' .seller_info').slideDown();
  $('#'+ thisid).unbind('mouseenter');
}
var seller_hide = function (data){
  thisid = data.currentTarget.id; //get id of current selector
  $('#'+ thisid + ' .seller_info').slideUp('slow',function(){
    $('.seller-thumb').bind('mouseenter',seller_display);
  });
}

$('.seller-thumb').bind({'mouseenter' : seller_display,'mouseleave':seller_hide });

  $("#country").on('change',function(){
    var country_id = $(this).val();
    $("#city").find('option').remove();
    cities[country_id].forEach(function(city){
      $("#city").append("<option value="+city['zone_id']+" >"+city['name']+"</option>");
    });

  })

  function activate_button(){
    if($("input[name=terms]").is(":checked")){
      $("#submit_form").removeAttr("disabled");
    }else{
      $("#submit_form").attr("disabled","disabled");
    }
  }



function toggle(element, obj) {
    var $input = $(obj);
    if ($input.prop('checked')) {
        $(element).show();
        $(element).addClass('required');
        $("#hidden_eligible_check").val('1');
        validate_next($('input#beneficiary_name'));
    } else {
        $(element).hide();
        $(element).removeClass('required');
        $("#hidden_eligible_check").val('0');
        validate_next($('input#beneficiary_name'));
    }
}

</script>

<?php echo $footer; ?>
