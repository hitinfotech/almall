<?php echo $header; ?>
<div class="content-padding">
<div class="container">


  <div class="row">

    <?php
          if(isset($menu) && !empty($menu)) {
            $class = 'col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-left';
          }else{
            $class = '';
          }
    ?>

    <?php echo $menu;?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php if ($orders) { ?>
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-right"><?php echo $column_order_id; ?></td>
              <td class="text-left"><?php echo $column_status; ?></td>
              <td class="text-left"><?php echo $column_date_added; ?></td>
              <td class="text-right"><?php echo $column_product; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($orders as $order) { ?>
            <tr>
              <td class="text-right">#<?php echo $order['order_id']; ?></td>
              <td class="text-left"><?php echo $order['status']; ?></td>
              <td class="text-left"><?php echo $order['date_added']; ?></td>
              <td class="text-right"><?php echo $order['products']; ?></td>
              <td class="text-right"><?php echo $order['total']; ?></td>
              <td class="text-right">
                <a href="<?php echo $order['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                <a onclick = "getProductOrderStatus(<?php echo $order['order_id']?>)" data-toggle="tooltip" class="btn btn-primary btn-sm"><i class="fa fa-truck"></i></a>
                 <button class="hide" href="#orderProductStatusModal" data-toggle="modal" id="statusButton"></button>
                  </td>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="text-right"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <div id="orderProductStatusModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h3 class="modal-title"><?php echo $text_tracking; ?></h3>
              </div>
              <div class="modal-body" id="orderProductStatusModalBody">

              </div>
              <div class="modal-footer">
                <button class="btn closebtn" data-dismiss="modal" aria-hidden="true" id="closeButton">Close</button>
              </div>
            </div>
        </div>
      </div>

      <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>
</div>
<?php echo $footer; ?>
<script>
  function getProductOrderStatus (order_id) {
          $.ajax({
            url: 'index.php?route=account/order/track_order&order_id='+order_id,
            type: 'get',
            methodType: 'html',
            success: function(html) {
              $('#orderProductStatusModalBody').html(html);
              $('#statusButton').click();
            }
          });
  }
</script>
