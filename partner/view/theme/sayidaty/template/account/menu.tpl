<div id="side-menu" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right">
    <div class="user-tabs">
        <ul class="">
            <li><a href="<?php echo $personal_account_url; ?>" <?php if($dActiveMenuItem=='edit') { ?> class="active" <?php } ?> ><?php echo $text_personal_account?></a></li>
            <li><a href="<?php echo $notifications_url; ?>" <?php if($dActiveMenuItem=='notifications') { ?> class="active" <?php } ?> ><?php echo $text_notifications?></a></li>
            <!--<li><a href="javascript:void(0)"><?php echo $text_fav_shops?></a></li>
            <li><a href="<?php echo $wishlist; ?>" <?php if($dActiveMenuItem=='wishlist') { ?> class="active" <?php } ?> ><?php echo $text_fav_products?></a></li>-->
            <li><a href="<?php echo $change_password_url; ?>" <?php if($dActiveMenuItem=='password') { ?> class="active" <?php } ?> ><?php echo $text_change_password?></a></li>
            <!--<li><a href="<?php echo $addresses_url; ?>" <?php if($dActiveMenuItem=='address') { ?> class="active" <?php } ?> ><?php echo $text_address?></a></li>-->
            <li><a href="<?php echo $language; ?>" <?php if($dActiveMenuItem=='language') { ?> class="active" <?php } ?> ><?php echo $text_language?></a></li>
            <li><a href="<?php echo $order; ?>" <?php if ($dActiveMenuItem == 'order') { ?> class="active" <?php } ?> ><?php echo $text_my_orders ?></a></li>
            <?php if($wk_rma_status){ ?>
                <li><a href="<?php echo $wkrma_system; ?>" <?php if($dActiveMenuItem=='rma') { ?> class="active" <?php } ?>><?php echo $text_RMA?></a></li>
            <?php } ?>
            <li><a href="<?php echo $newsletter; ?>" <?php if ($dActiveMenuItem == 'newsletter') { ?> class="active" <?php } ?> ><?php echo $text_newsletter ?></a></li>

            <li><a href="<?php echo $voucher; ?>" <?php if ($dActiveMenuItem == 'voucher') { ?> class="active" <?php } ?> ><?php echo $text_voucher ?></a></li>

<?php /* <!-- <li><a href="<?php echo $transaction; ?>" <?php if($dActiveMenuItem=='balance') { ?> class="active" <?php } ?> ><?php echo $text_balance?></a></li> --> */ ?>
        </ul>
    </div>
</div>
