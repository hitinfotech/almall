<?php echo $header; ?>
<div class="content-padding">
    <div class="container">

        <div class="row">
            <div id="content" class="<?php echo $class; ?>">
                <h2><?php echo $heading_title; ?></h2>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <td  colspan="2"><?php echo $text_order_detail; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td  style="width: 50%;"><?php if ($invoice_no) { ?>
                                    <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
                                <?php } ?>
                                <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br />
                                <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?></td>
                            <td ><?php if ($payment_method) { ?>
                                    <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
                                <?php } ?>
                                <?php if ($shipping_method) { ?>
                                    <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
                                <?php } ?></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <td  style="width: 50%;"><?php echo $text_payment_address; ?></td>
                            <?php if ($shipping_address) { ?>
                                <td ><?php echo $text_shipping_address; ?></td>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td ><?php echo $payment_address; ?></td>
                            <?php if ($shipping_address) { ?>
                                <td ><?php echo $shipping_address; ?></td>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td ><?php echo $column_name; ?></td>
                                <td ><?php echo $column_model; ?></td>
                                <td ><?php echo $column_quantity; ?></td>
                                <td ><?php echo $column_price; ?></td>
                                <td ><?php echo $column_total; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($products as $product) { ?>
                                <tr>
                                    <td ><?php echo $product['name']; ?>
                                        <?php foreach ($product['option'] as $option) { ?>
                                            <br />
                                            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                        <?php } ?></td>
                                    <td ><?php echo $product['model']; ?></td>
                                    <td ><?php echo $product['quantity']; ?></td>
                                    <td ><?php echo $product['price']; ?></td>
                                    <td ><?php echo $product['total']; ?></td>
                                </tr>
                            <?php } ?>
                            <?php foreach ($vouchers as $voucher) { ?>
                                <tr>
                                    <td ><?php echo $voucher['description']; ?></td>
                                    <td ></td>
                                    <td >1</td>
                                    <td ><?php echo $voucher['amount']; ?></td>
                                    <td ><?php echo $voucher['amount']; ?></td>
                                    <?php if ($products) { ?>
                                        <td></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <?php foreach ($totals as $total) { ?>
                                <tr>
                                    <td colspan="2"></td>
                                    <?php if ($products) { ?>
                                        <td></td>
                                    <?php } ?>

                                    <td ><b><?php echo $total['title']; ?></b></td>
                                    <td ><?php echo $total['text']; ?></td>
                                </tr>
                            <?php } ?>
                        </tfoot>
                    </table>
                </div>
                <?php if ($comment) { ?>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td ><?php echo $text_comment; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td ><?php echo $comment; ?></td>
                            </tr>
                        </tbody>
                    </table>
                <?php } ?>
                <?php if ($histories) { ?>
                    <h3><?php echo $text_history; ?></h3>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td ><?php echo $column_date_added; ?></td>
                                <td ><?php echo $column_status; ?></td>
                                <td ><?php echo $column_comment; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($histories as $history) { ?>
                                <tr>
                                    <td ><?php echo $history['date_added']; ?></td>
                                    <td ><?php echo $history['status']; ?></td>
                                    <td ><?php echo $history['comment']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } ?>
                <button class="hide" href="#orderProductStatusModal" data-toggle="modal" id="statusButton"></button>

                <div class="buttons clearfix">
                    <div class="pull-right"><a onclick = "getProductOrderStatus(<?php echo $order_id?>)" data-toggle="tooltip" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                </div>

                <div id="orderProductStatusModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 class="modal-title"><?php echo $text_tracking; ?></h3>
                            </div>
                            <div class="modal-body" id="orderProductStatusModalBody">

                            </div>
                            <div class="modal-footer">
                                <button class="btn closebtn" data-dismiss="modal" aria-hidden="true" id="closeButton">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?>
        </div>

    </div>
</div>
<?php echo $footer; ?>
<script>
    function getProductOrderStatus(order_id) {
        $.ajax({
            url: 'index.php?route=account/order/track_order&order_id=' + order_id,
            type: 'get',
            methodType: 'html',
            success: function (html) {
                $('#orderProductStatusModalBody').html(html);
                $('#statusButton').click();
            }
        });
    }
</script>
<?php die; ?>