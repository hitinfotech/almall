<?php echo $header; ?>

<link rel="stylesheet" href="/catalog/view/theme/sayidaty/css/register_slider.min.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="/catalog/view/theme/sayidaty/css/jquery.idealforms.css" type="text/css" media="screen"/>

<script type="text/javascript" src="/catalog/view/theme/sayidaty/javascript/jquery.idealforms.js"></script>

<div class="content-padding">
<div class="container">
<?php if ($error_warning) { ?>
<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $error_warning; ?></div>
<?php } ?>

<?php if ($success) { ?>
<div class="alert alert-success"><i class="fa fa-check-circle"> </i> <?php echo $success; ?></div>
<?php } ?>

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>

    <h1>
      <?php echo $heading_title; ?>
    </h1>
    <div class="">
      <form action="<?php echo $action ?>" method="post">
        <div >
          <div class="row">
            <div class="col-md-10 col-sm-10 col-xs-12">
              <div class="form-group field icon required idealforms-field idealforms-field-text row">
                  <label class="col-sm-3 control-label" for="input-brand"><span data-toggle="tooltip" title="<?php echo $entry_brand; ?>"><?php echo $entry_brand; ?></span></label>
                  <div class="col-sm-9">
                      <input type="text" name="brand" value="<?php echo $brand ?>" placeholder="<?php echo $entry_brand; ?>" id="input-brand" class="form-control" />
                      <input type="hidden" name="brand_id" value="<?php echo $brand_id ?>" />
                      <?php if ($error_brand) { ?>
                          <div class="text-danger"><?php echo $error_brand; ?></div>
                      <?php }elseif(isset($error_brand_no_products) && $error_brand_no_products != '') { ?>
                          <div class="text-danger"><?php echo $error_brand_no_products; ?></div>
                      <?php } ?>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-10 col-sm-10 col-xs-12">
              <label class="col-sm-3 control-label" for="input-sort-order"><?php echo $tab_special; ?></label>

              <div class="table-responsive col-sm-9">
                <table id="special" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_percentage ." %"; ?></td>
                      <td class="text-left"><?php echo $entry_date_start; ?></td>
                      <td class="text-left"><?php echo $entry_date_end; ?></td>
                    </tr>
                  </thead>
                    <tbody id="special-row-0">
                    <tr>
                      <td class="text-right">
                        <input  type="number" step="5" name="product_special[percentage]" value="<?php echo $product_specials_percentage ?>" placeholder="<?php echo $entry_percentage; ?>" class="form-control" />
                        <input type='hidden' name='discount_id' value='<?php echo $discount_id ?>' />
                        <?php if ($error_percentage) { ?>
                            <div class="text-danger"><?php echo $error_percentage; ?></div>
                        <?php } ?>
                      </td>
                      <td class="text-left"><div class="input-group date">
                          <input type="text" name="product_special[date_start]" value="<?php echo $product_specials_date_added ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span>
                        </div></td>
                      <td class="text-left"><div class="input-group date">
                          <input type="text" name="product_special[date_end]" value="<?php echo $product_specials_date_end ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <input type="submit" value="Submit" style="width:20%; float:right" class="btn btn-primary"/>
      </form>
    </div>
    <?php echo $column_right; ?></div>   <!--row-->
    </div>  <!--container-->



<script type="text/javascript"><!--
// Brand
$('input[name=\'brand\']').autocomplete({
    'source': function (request, response) {
        $.ajax({
            url: 'index.php?route=addproduct/brandAutocomplete&filter_keyword=' + encodeURIComponent(request),
            dataType: 'json',
            success: function (json) {
                json.unshift({
                    id: 0,
                    name: '<?php echo $text_none; ?>'
                });

                response($.map(json, function (item) {
                    return {
                        label: item['name'],
                        value: item['id']
                    }
                }));
            }
        });
    },
    'select': function (item) {
        $('input[name=\'brand\']').val(item['label']);
        $('input[name=\'brand_id\']').val(item['value']);
    }
});

$( document ).ready(function() {
  $('.date').datetimepicker({
    pickDate: true,
    pickTime: false,
  });
} );
//--></script>

   <!-- membership codes ends here -->
 </div>
<?php echo $footer; ?>
