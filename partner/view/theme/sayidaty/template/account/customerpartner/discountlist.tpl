<?php echo $header; ?>
<div class="content-padding">
    <div class="container">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $error_warning; ?></div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"> </i> <?php echo $success; ?></div>
        <?php } ?>

        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>">
                <?php echo $content_top; ?>
                <h1>
                    <?php echo $heading_title; ?>
                    <div class="pull-right">
                      <a href="<?php echo $insert; ?>" data-toggle="tooltip" title="<?php echo $button_insert; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                      <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-discounts').submit() : false;"><i class="fa fa-trash-o"></i></button>
                      <?php /* ?><a onclick="$('#form-discounts').submit();" data-toggle="tooltip" class="btn btn-danger"  title="<?php echo $button_delete; ?>"><i class="fa fa-trash-o"></i></a><?php */ ?>
                    </div>
                </h1>

                <fieldset>
                        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-discounts">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                            <td class="text-right"><?php echo $column_name; ?></td>
                                            <td class="text-right"><?php echo $column_percentage; ?></td>
                                            <td class="text-right"><?php echo $column_date_start; ?></td>
                                            <td class="text-right"><?php echo $column_date_end; ?></td>
                                            <td class="text-right"><?php echo $column_action; ?></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($discounts) { ?>
                                            <?php foreach ($discounts as $discount) { ?>
                                                <tr>
                                                  <td class="text-center"><?php if (in_array($discount['discount_id'], $selected)) { ?>
                                                        <input type="checkbox" name="selected[]" value="<?php echo $discount['discount_id']; ?>" checked="checked" />
                                                    <?php } else { ?>
                                                        <input type="checkbox" name="selected[]" value="<?php echo $discount['discount_id']; ?>" />
                                                    <?php } ?>
                                                  </td>
                                                  <td class="text-left"><?php echo $discount['name']; ?></td>
                                                  <td class="text-left"><?php echo $discount['percentage'] ."%"; ?></td>
                                                  <td class="text-left"><?php echo $discount['date_start']; ?></td>
                                                  <td class="text-left"><?php echo $discount['date_end']; ?></td>
                                                  <td class="text-left"><a href='<?php echo $discount["action"]; ?>' class='btn btn-info'>Edit</a></td>


                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td class="text-center" colspan="10"><?php echo $text_no_results; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                        </div>
                </fieldset>

                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?>
        </div>
    </div>
</div>
<?php echo $footer; ?>
