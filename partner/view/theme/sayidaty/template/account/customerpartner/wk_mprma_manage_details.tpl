<?php echo $header; ?>
<style>
    div#tab-images td{
        display: inline-table;
    }
    div#tab-images .thumbnail{
        margin: 3px;
    }
    div#alert-view div.alert{
        color : #000;
    }
    div#alert-view div.col-sm-10{
        padding: 0px;
    }
    @media (max-width: 767px) {
        div#alert-view .pull-right{
            float: none !important;
        }
    }
</style>
<div class="content-padding">
    <div class="container">

        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $error_warning; ?></div>
        <?php } ?>

        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>

            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <h1>
                    <?php echo $heading_title; ?>

                </h1>
                <fieldset>

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $wk_rma_admin_basic; ?></a></li>
                        <li><a href="#tab-product" data-toggle="tab"><?php echo $wk_rma_admin_product; ?></a></li>
                        <li><a href="#tab-messages" data-toggle="tab"><?php echo $wk_rma_admin_msg_tab; ?></a></li>
                        <li><a href="#tab-images" data-toggle="tab"><?php echo $wk_rma_admin_images; ?></a></li>
                        <li><a href="#tab-shipping" data-toggle="tab"><?php echo $text_shipping_lable; ?></a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="tab-general" class="tab-pane active">
                            <table class="table table-bordered">
                                <?php if ($result_rmaadmin) { ?>
                                    <?php $result_rmaadmins = $result_rmaadmin; ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $wk_rma_admin_oid ?> :</td>
                                            <td><?php echo ucfirst($result_rmaadmins['oid']); ?><input type="hidden" name="rma_id" value="<?php echo $result_rmaadmins['id']; ?>"></td>
                                        </tr>

                                        <tr>
                                            <td><?php echo $wk_rma_admin_rmastatus; ?> :</td>
                                            <td style="color:<?php echo $result_rmaadmins['color']; ?>;font-weight:bold;"><?php echo $result_rmaadmins['rmastatus']; ?></td>
                                        </tr>

                                        <tr>
                                            <td><?php echo $wk_rma_admin_cname; ?> :</td>
                                            <td><?php echo ucwords($result_rmaadmins['name']); ?></td>
                                        </tr>

                                        <tr>
                                            <td><?php echo $wk_rma_admin_add_info; ?> :</td>
                                            <td><?php echo ucfirst($result_rmaadmins['add_info']); ?></td>
                                        </tr>
                                        <?php /* ?>
                                        <!-- <tr>
                                          <td><?php echo $wk_rma_admin_customerstatus; ?> :</td>
                                          <td><?php echo ucwords($result_rmaadmins['cstatus']); ?></td>
                                        </tr> -->
                                        <tr>
                                            <td><?php echo $wk_rma_admin_authno ?> :</td>
                                            <td><?php echo $result_rmaadmins['auth_no']; ?></td>
                                        </tr>
                                        <!-- <tr>
                                          <td><?php echo $wk_rma_admin_adminstatus ?> :</td>
                                          <td>
                                        <?php if ($admin_status) { ?>
                                            <?php foreach ($admin_status as $admin_st) { ?>
                                                <?php echo $admin_st['name']; ?>
                                            <?php } ?>
                                        <?php } ?>
                                          </td>
                                        </tr> -->
                                        <?php */ ?>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>

                        <!-- product tab -->
                        <div class="tab-pane" id="tab-product">
                            <?php if ($result_products) { ?>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td class="left"><?php echo $entry_name; ?></td>
                                            <td class="left"><?php echo $wk_rma_admin_return; ?></td>
                                            <td class="left"><?php echo $wk_rma_admin_reason; ?></td>
                                        </tr>
                                    </thead>

                                    <?php foreach ($result_products as $products) { ?>
                                        <tr>
                                            <td class="left"><?php echo $products['name']; ?>
                                                <?php foreach ($products['option'] as $key => $value) { ?>
                                                    <br>&nbsp;<small> - <?php echo $value['name']; ?> : <?php echo $value['value']; ?></small>
                                                <?php } ?></td>
                                            <td class="left"><?php echo $products['quantity']; ?></td>
                                            <td class="left"><?php echo $products['reason']; ?></td>
                                        </tr>
                                    <?php } ?>

                                </table>
                            <?php } ?>
                        </div>

                        <!-- messages tab -->
                        <div class="tab-pane" id="tab-messages">

                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <select name="filter_name" class="form-control">
                                                <option value="*"></option>
                                                <option value="me" <?php echo $filter_name == 'me' ? 'selected' : ''; ?> ><?php echo $result_rmaadmins['name']; ?></option>
                                                <option value="admin" <?php echo $filter_name == 'admin' ? 'selected' : ''; ?>><?php echo 'Admin'; ?></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <input type="text" name="filter_message" value="<?php echo $filter_message; ?>" placeholder="<?php echo $wk_rma_admin_msg; ?>" id="input-msg" class="form-control" />
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <div class="input-group date">
                                                <input type="text" name="filter_date" value="<?php echo $filter_date; ?>" data-date-format="YYYY-MM-DD" id="input-date" class="form-control" placeholder="<?php echo $wk_rma_admin_date; ?>" />
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span></div>
                                        </div>
                                    </div>

                                    <div class="col-sm-3 text-right">

                                        <div class="btn-group">
                                            <button type="button" id="alert-view-button" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_alert; ?>"><i class="fa fa-th-list"></i></button>
                                            <button type="button" id="table-view-button" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_table; ?>"><i class="fa fa-th"></i></button>
                                        </div>

                                        <button type="button" onclick="filter();" class="btn btn-primary"> <?php echo $button_filter; ?></button>
                                        <button type="button" onclick="clrfilter();" class="btn btn-default"><?php echo $button_clrfilter; ?></button>
                                    </div>
                                </div>
                            </div>

                            <?php /* ?>
                            <!-- table view -->
                            <div class="table-responsive hide" id="table-view">
                                <table class="table table-bordered table-hove">
                                    <thead>
                                        <tr>
                                            <td >
                                                <?php if ($sort == 'wrm.writer') { ?>
                                                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_admin_cname; ?></a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $sort_name; ?>" > <?php echo $wk_rma_admin_cname; ?> </a>
                                                <?php } ?>
                                            </td>
                                            <td >
                                                <?php if ($sort == 'wrm.message') { ?>
                                                    <a href="<?php echo $sort_message; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_admin_msg; ?></a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $sort_message; ?>" > <?php echo $wk_rma_admin_msg; ?> </a>
                                                <?php } ?>
                                            </td>
                                            <td >
                                                <?php if ($sort == 'wrm.date') { ?>
                                                    <a href="<?php echo $sort_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_admin_date; ?></a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $sort_date; ?>" > <?php echo $wk_rma_admin_date; ?> </a>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php echo $text_download; ?>
                                            </td>
                                        </tr>
                                    </thead>
                                    <?php if ($results_message) { ?>
                                        <?php foreach ($results_message as $results_messages) { ?>
                                            <tbody>
                                                <tr>
                                                    <td class=""><?php
                                                        if ($results_messages['writer'] == 'me') {
                                                            echo $result_rmaadmins['name'];
                                                        } else {
                                                            echo ucfirst($results_messages['writer']);
                                                        }
                                                        ?></td>
                                                    <td class=" " ><?php echo $results_messages['message']; ?></td>
                                                    <td class=""><?php echo $results_messages['date']; ?></td>
                                                    <td class="">
                                                        <?php if ($results_messages['attachment']) { ?>
                                                            <a href="<?php echo $attachmentLink . $results_messages['attachment']; ?>" data-toggle="tooltip" title="<?php echo $text_download; ?>" class="text-info" target="_blank"><i class="fa fa-download"></i> <?php echo $results_messages['attachment']; ?></a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tbody>
                                            <tr><td class="text-center" colspan="4"><?php echo $text_no_recored; ?></td></tr>
                                        </tbody>
                                    <?php } ?>

                                </table>
                            </div>
                            <?php */ ?>

                            <?php /* ?>
                            <!-- alert view -->
                            <div id="alert-view" class="hide">
                                <?php foreach ($results_message as $res_message) { ?>
                                    <?php if ($res_message['writer'] != 'admin') { ?>
                                        <div class="col-sm-10">
                                            <div class="alert alert-success">
                                                <label data-toggle="tooltip" title="<?php echo ucfirst($result_rmaadmins['name']); ?>"><i class="fa fa-user"></i> </label>
                                                <label class="pull-right" ><i class="fa fa-clock-o"></i> <?php echo $res_message['date']; ?></label>
                                                <br/>
                                                <?php echo $res_message['message']; ?>
                                                <?php if ($res_message['attachment']) { ?>
                                                    <br/>
                                                    <a href="<?php echo $attachmentLink . $res_message['attachment']; ?>" data-toggle="tooltip" title="<?php echo $text_download; ?>" class="text-info" target="_blank"><i class="fa fa-download"></i> <?php echo $res_message['attachment']; ?></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-sm-10 pull-right">
                                            <div class="alert alert-info">
                                                <label data-toggle="tooltip" title="<?php echo ucfirst($res_message['writer']); ?>"><i class="fa fa-home"></i></label>
                                                <label class="pull-right"><i class="fa fa-clock-o"></i> <?php echo $res_message['date']; ?></label>
                                                <br/>
                                                <?php echo $res_message['message']; ?>
                                                <?php if ($res_message['attachment']) { ?>
                                                    <br/>
                                                    <a href="<?php echo $attachmentLink . $res_message['attachment']; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $text_download; ?>" class="text-success"><i class="fa fa-download"></i> <?php echo $res_message['attachment']; ?></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>


                            <div class="row">
                                <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                                <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                            </div>
                            <?php */ ?>

                            <form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" class="form_msg">
                                <div class="row required">
                                    <label class="col-sm-12 text-left control-label" style="text-align: left;"><span data-toggle="tooltip" title="<?php echo $help_sellerStatus; ?>"><?php echo $wk_rma_sellerStatus; ?></span></label>

                                    <div class="col-sm-12">
                                        <select name = "wk_rma_admin_adminstatus" class="form-control">
                                            <?php if ($rma_sta) { ?>
                                                <?php foreach ($rma_sta as $key => $value) { ?>
                                                    <option value="<?php echo $key; ?>" <?php if (isset($result_rmaadmin['customer_status_id']) && $result_rmaadmin['customer_status_id'] == $key) echo 'selected'; ?>><?php echo $value['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>

                                <br/>
                                <div class="row required">
                                    <label class="col-sm-12 text-left control-label" style="text-align: left;"><?php echo $wk_rma_admin_msg; ?></label>
                                    <div class="col-sm-12">
                                        <input type="hidden" name="rma_id" value="<?php echo $vid; ?>">
                                        <input type="hidden" name="customer_id" value="<?php if ($result_rmaadmin) echo $result_rmaadmins['cust_id']; ?>">

                                        <textarea name="wk_rma_admin_msg" rows="4" class="form-control"></textarea>
                                        <br/>
                                        <button type="submit" class="btn btn-primary pull-right" data-toggle="tooltip" title="<?php echo $wk_viewrma_msg; ?>"><i class="fa fa-envelope-o"></i></button>
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <label type="button" class="btn btn-primary" for="file-upload"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></label>
                                            </span>
                                            <input type="text" id="input-file-name" class="form-control" disabled/>
                                        </div>

                                        <input type="file" id="file-upload" name="up_file" class="form-control hide">
                                        <br/>


                                    </div>
                                </div>
                            </form>
                        </div>

                        <!-- images tab -->
                        <div class="tab-pane" id="tab-images">
                            <?php if ($result_rmaadmin_images) { ?>
                                <div class="table-responsive">
                                    <table>
                                        <tbody class="thumbnails">
                                            <tr>
                                                <?php foreach ($result_rmaadmin_images as $images) { ?>
                                                    <?php if ($images['image']) { ?>
                                                        <td><a href="<?php echo $images['image']; ?>" data-toggle="tooltip" title="View Images" class="thumbnail"><img src="<?php echo $images['resize']; ?>"></a></td>
                                                    <?php } ?>
                                                <?php } ?>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            <?php } ?>
                        </div>

                        <!-- shipping tab -->
                        <div class="tab-pane" id="tab-shipping">
                            <div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> <?php echo $text_shipping_info; ?>
                            </div>

                            <?php if ($result_rmaadmin['shipping_label']) { ?>
                                <div class="text-center">
                                    <img src="<?php echo $result_rmaadmin['shipping_label']; ?>" />
                                </div>
                                <br/><br/>
                            <?php } ?>

                            <form action="<?php echo $mpsavelabel; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">

                                <button type="submit" class="btn btn-primary pull-right" data-toggle="tooltip" title="<?php echo $wk_viewrma_shipping_label; ?>"><i class="fa fa-envelope-o"></i></button>

                                <div class="input-group col-sm-11">
                                    <span class="input-group-btn">
                                        <label type="button" class="btn btn-primary" for="shipping-upload"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></label>
                                    </span>
                                    <input type="text" id="input-mpshipping-name" class="form-control" disabled/>
                                    <input type="file" id="shipping-upload" name="mpshipping_label" class="form-control hide">
                                </div>
                                <br/>

                                <?php if ($mpshipping_label_folder) { ?>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <td width="1" style="text-align: center;"></td>
                                                <td class="text-center"><?php echo $text_lable_image; ?></td>
                                                <td class="text-left"><?php echo $text_lable_name; ?></td>
                                            </tr>
                                        </thead>

                                        <?php foreach ($mpshipping_label_folder as $image) { ?>
                                            <tr>
                                                <td><input type="checkbox" name="selected" value="<?php echo $image['name']; ?>" /></td>
                                                <td class="text-center"><img src="<?php echo $image['image']; ?>" alt="<?php echo $image['name']; ?>"/></td>
                                                <td class="text-left"><?php echo $image['name']; ?></td>
                                            </tr>
                                        <?php } ?>

                                    </table>
                                <?php } ?>

                            </form>
                        </div>

                    </div> <!--tab-content-->
                </fieldset>

                <div class="pull-right">
                    <a href="<?php echo $invoice; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_invoice; ?>" class="btn btn-default"><i class="fa fa-print"></i></a>
                </div>
                <?php echo $content_bottom; ?></div><!--content-->
            <?php echo $column_right; ?></div><!--row-->
    </div><!--container-->
</div>
<?php echo $footer; ?>

<script type="text/javascript">
    jQuery('input[name=up_file]').change(function () {
        $('#input-file-name').val(jQuery(this).val().replace(/C:\\fakepath\\/i, ''));
    });

    jQuery('input[name=mpshipping_label]').change(function () {
        $('#input-mpshipping-name').val(jQuery(this).val().replace(/C:\\fakepath\\/i, ''));
    });

// Message Alert List
    $('#alert-view-button').click(function () {
        localStorage.setItem('display', 'alert');
        $('#alert-view').removeClass('hide');
        $('#table-view').addClass('hide');
    });

// Message Table Grid
    $('#table-view-button').click(function () {
        localStorage.setItem('display', 'table');
        $('#table-view').removeClass('hide');
        $('#alert-view').addClass('hide');

    });

    if (localStorage.getItem('display') == 'alert') {
        $('#alert-view').removeClass('hide');
    } else {
        $('#table-view').removeClass('hide');
    }

    $('.date').datetimepicker({
        pickTime: false
    });

    function clrfilter() {
        location = 'index.php?route=account/customerpartner/wk_mprma_manage/getForm&id=<?php echo $result_rmaadmins["id"]; ?>';

        localStorage.setItem('tab', 'tab-messages');

    }

    function filter() {
        url = 'index.php?route=account/customerpartner/wk_mprma_manage/getForm';

        var filter_name = $('select[name=\'filter_name\']').val();

        if (filter_name != '*') {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }

        var filter_message = $('input[name=\'filter_message\']').val();

        if (filter_message) {
            url += '&filter_message=' + encodeURIComponent(filter_message);
        }

        var filter_date = $('input[name=\'filter_date\']').val()

        if (filter_date) {
            url += '&filter_date=' + encodeURIComponent(filter_date);
        }

        url += "&id=<?php echo $result_rmaadmins['id']; ?>";

        localStorage.setItem('tab', 'tab-messages');

        location = url;
    }

    $(document).ready(function () {
        if (localStorage.getItem('tab') == 'tab-messages') {
            $('a[href="#tab-messages"]').trigger('click');
            localStorage.setItem('tab', '');
        }

        $('.thumbnails').magnificPopup({
            type: 'image',
            delegate: 'a',
            gallery: {
                enabled: true
            }
        });
    });
</script>
