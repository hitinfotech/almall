<?php echo $header; ?>
<div class="content-padding">
    <div class="container">


        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $error_warning; ?></div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"> </i> <?php echo $success; ?></div>
        <?php } ?>
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>">
                <?php echo $content_top; ?>
                <h1>
                    <?php echo $heading_title; ?>
                    <div class="pull-right">
                      <!-- <button type="submit" form="form-filter" formaction="<?php echo $invoice; ?>" data-toggle="tooltip" title="<?php echo $button_invoice; ?>" class="btn btn-info"><i class="fa fa-print"></i></button> -->
                    </div>
                </h1>
                <fieldset>
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label class="control-label" for="input-admin_oid"><?php echo $wk_rma_admin_oid; ?></label>
                                    <input type="text" name="filter_order" value="<?php echo $filter_order; ?>" placeholder="<?php echo $wk_rma_admin_oid; ?>" id="input-admin_oid" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="input-admin_rmastatus"><?php echo $wk_rma_admin_rmastatus; ?></label>
                                    <select name="filter_rma_status" id="input-admin_rmastatus" class="form-control">
                                        <?php if ($rma_sta) { ?>
                                            <?php foreach ($rma_sta as $key => $value) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($filter_rma_status == $key) echo 'selected'; ?>><?php echo $value['name']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="input-name"><?php echo $wk_rma_admin_cname; ?></label>
                                    <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $wk_rma_admin_cname; ?>" id="input-name" class="form-control" />
                                </div>
                                <div class="form-group date">
                                    <label class="control-label" for="input-admin_date"><?php echo $wk_rma_admin_date; ?></label>
                                    <div class='input-group date'>
                                        <input type='text' name="filter_date" value="<?php echo $filter_date; ?>" placeholder="<?php echo $wk_rma_admin_date; ?>" id="input-admin_date" data-date-format="YYYY-MM-DD" class="form-control" />
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i>
                                        </span>

                                    </div>
                                </div>

                            </div>

                            <div class="col-sm-3 text-left">
                                <button type="button" onclick="filter();" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                                <a onclick="clrfilter();" class="btn btn-default"> <i class="fa fa-refresh"></i> </a>
                            </div>
                        </div>
                    </div>

                    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-filter">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hove">
                                <thead>
                                    <tr>
                                        <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>

                                        <td class="text-left">
                                            <?php if ($sort == 'wro.order_id') { ?>
                                                <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_admin_oid; ?></a>
                                            <?php } else { ?>
                                                <a href="<?php echo $sort_order; ?>" > <?php echo $wk_rma_admin_oid; ?> </a>
                                            <?php } ?>
                                        </td>

                                        <td class="text-left">
                                            <?php if ($sort == 'c.firstname') { ?>
                                                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_admin_cname; ?></a>
                                            <?php } else { ?>
                                                <a href="<?php echo $sort_name; ?>" > <?php echo $wk_rma_admin_cname; ?> </a>
                                            <?php } ?>
                                        </td>

                                        <td class="text-left">
                                            <?php if ($sort == 'wro.id') { ?>
                                                <a href="<?php echo $sort_product; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_admin_product; ?></a>
                                            <?php } else { ?>
                                                <a href="<?php echo $sort_product; ?>" > <?php echo $wk_rma_admin_product; ?> </a>
                                            <?php } ?>
                                        </td>

                                        <td class="text-left">
                                            <?php if ($sort == 'wrr.id') { ?>
                                                <a href="<?php echo $sort_reason; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_admin_reason; ?></a>
                                            <?php } else { ?>
                                                <a href="<?php echo $sort_reason; ?>" > <?php echo $wk_rma_admin_reason; ?> </a>
                                            <?php } ?>
                                        </td>

                                        <td class="text-left">
                                            <?php if ($sort == 'wrs.order_status_id') { ?>
                                                <a href="<?php echo $sort_rma_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_admin_rmastatus; ?></a>
                                            <?php } else { ?>
                                                <a href="<?php echo $sort_rma_status; ?>" > <?php echo $wk_rma_admin_rmastatus; ?> </a>
                                            <?php } ?>
                                        </td>

                                        <td class="text-left">
                                            <?php if ($sort == 'wro.date') { ?>
                                                <a href="<?php echo $sort_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_admin_date; ?></a>
                                            <?php } else { ?>
                                                <a href="<?php echo $sort_date; ?>" > <?php echo $wk_rma_admin_date; ?> </a>
                                            <?php } ?>
                                        </td>

                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if ($result_rmaadmin) { ?>
                                        <?php foreach ($result_rmaadmin as $result_rmaadmins) { ?>
                                            <tr>
                                                <td style="text-align: center;"><?php if ($result_rmaadmins['selected']) { ?>
                                                        <input type="checkbox" name="selected[]" value="<?php echo $result_rmaadmins['id']; ?>" checked="checked" />
                                                    <?php } else { ?>
                                                        <input type="checkbox" name="selected[]" value="<?php echo $result_rmaadmins['id']; ?>" />
                                                    <?php } ?>
                                                </td>
                                                <td class="text-left"><?php echo ucfirst($result_rmaadmins['oid']); ?></td>
                                                <td class="text-left"><?php echo ucfirst($result_rmaadmins['name']); ?></td>

                                                <td class="text-left"><?php echo ucfirst($result_rmaadmins['product']); ?></td>
                                                <td class="text-left"><?php echo ucfirst($result_rmaadmins['reason']); ?></td>
                                                <td class="text-left" style="color:<?php echo $result_rmaadmins['color']; ?>;font-weight:bold;"><?php echo ucfirst($result_rmaadmins['rmastatus']); ?></td>
                                                <td class="text-left"><?php echo ucfirst($result_rmaadmins['date']); ?></td>

                                                <td class="text-center">
                                                    <?php foreach ($result_rmaadmins['action'] as $action) { ?>
                                                        <a href="<?php echo $action['href']; ?>" class="btn btn-primary" data-toggle="tooltip" title="<?php echo $action['text']; ?>"><i class="fa fa-eye"></i></a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <tr>
                                            <td class="text-center" colspan="12"><?php echo $text_no_recored; ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                    </div>

                </fieldset>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-primary" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-filter').submit() : false;"><i class="fa fa-trash-o"></i></button>

                <?php echo $content_bottom; ?>
            </div><!--content-->
            <?php echo $column_right; ?></div><!-- row-->
    </div><!--container-->

    <script type="text/javascript"><!--

        $('.date').datetimepicker({
            pickTime: false,
        });

        function clrfilter() {
            location = '<?php echo $current; ?>';
        }

        function filter() {
            url = '<?php echo $current; ?>';

            var filter_name = $('input[name=\'filter_name\']').val();

            if (filter_name) {
                url += '&filter_name=' + encodeURIComponent(filter_name);
            }

            var filter_product = $('input[name=\'filter_product\']').val();

            if (filter_product) {
                url += '&filter_product=' + encodeURIComponent(filter_product);
            }

            var filter_order = $('input[name=\'filter_order\']').val();

            if (filter_order) {
                url += '&filter_order=' + encodeURIComponent(filter_order);
            }

            var filter_rma_status = $('select[name=\'filter_rma_status\']').val();

            if (filter_rma_status != '*') {
                url += '&filter_rma_status=' + encodeURIComponent(filter_rma_status);
            }

            var filter_date = $('input[name=\'filter_date\']').val();

            if (filter_date) {
                url += '&filter_date=' + encodeURIComponent(filter_date);
            }

            location = url;
        }
    //--></script>
</div>
<?php echo $footer; ?>
