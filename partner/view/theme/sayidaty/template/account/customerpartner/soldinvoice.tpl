<style>
@media print
{
    .no-print, .no-print *
    {
        display: none !important;
    }
}

.content-padding {
  padding-bottom: 110px;
  padding-top: 20px;
}
.top-buffer {
  margin-top:20px;
}
</style>
<?php if ($chkIsPartner AND ! $errorPage) { ?>
    <!DOCTYPE html>
    <meta charset="UTF-8" />
    <html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
        <head>
            <meta charset="UTF-8" />
            <title><?php echo $heading_title; ?></title>
            <base href="<?php echo $base; ?>" />
            <link href="../admin/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link type="text/css" href="../admin/view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
            <link type="text/css" href="../admin/view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />

            <script src="<?php echo HTTPS_IMAGE_S3_STATIC ?>javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
            <script src="../admin/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="../admin/view/javascript/jquery/jquery-2.1.1.min.js"></script>

            <script type="text/javascript" src="https://cdn.jsdelivr.net/jsbarcode/3.3.7/JsBarcode.all.min.js"></script>
        </head>
        <body>
          <div class="container">
            <div class="content-padding">

              <div class='row' >
                <div class='col-md-6 col-sm-6 col-xs-6'>
                  <img src="<?php echo $logo  ?>" alt="logo"  style="width: 170px;" />
                </div>
              </div>


              <div class="row" >
                  <div class="col-md-9 col-sm-9 col-xs-9">

                      <p>
                      <table>
                          <tr>
                              <td  style="font-size:12px;"><?php echo 'Seller Order Invoice Of Order No.  '; ?></td>
                              <td><svg id="code39_title"></svg></td>
                          </tr>
                      </table>
                      <span class="barcode">
                          <?php $barcode1 = $order['payment_iso_code_2'] . '-' . $order['order_id'] . '-' . $order_count; ?>
                      </span>
                      </p>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-3">
                      <a class='btn btn-info pull-right no-print' href='<?php echo $back_button ?>'><i class='fa fa-reply'>Back </i></a>
                  </div>
              </div>


                <div style="font-size:12px;">
                    <h3>
                        <?php echo $heading_title ?><?php echo $text_order; ?> # <?php echo $order_id; ?>
                        <div class="pull-right">
                            <button type="button" class="btn btn-primary no-print" data-toggle="tooltip" title="<?php echo $text_print_invoice; ?>" onclick="printinvoice();"><i class="fa fa-print"></i></button>
                        </div>
                    </h3>

                    <table class="table table-bordered table-border">
                        <thead>
                            <tr>
                                <td colspan="2"><?php echo $text_order_info; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 50%;"><div>
                                            <strong><?php echo $order['store_name']; ?></strong><br />
                                            <?php echo $order['store_address']; ?>
                                        </div>
                                    <!-- <b><?php echo $text_telephone; ?></b>
                                     <?php echo $order['store_telephone']; ?>
                                     <br />

                                     <?php if (isset($order['store_fax'])) { ?>
                                         <b><?php echo $text_fax; ?></b> <?php echo $order['store_fax']; ?><br />
                                     <?php } ?>
                                     <b><?php echo $text_email; ?></b> <?php echo $order['store_email']; ?><br />
                                     -->
                                <td style="width: 50%;">
                                    <b><?php echo $text_order_date; ?></b> <?php echo $order['date_added']; ?><br />
                                    <?php if ($invoice_no) { ?>
                                        <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
                                    <?php } ?>
                                    <b><?php echo $text_order_id; ?></b> <?php echo $order_id; ?><br />
                                    <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
                                    <?php if ($shipping_method) { ?>
                                        <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?><br />
                                    <?php } ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-border">
                        <thead>
                            <tr>
                                <td><b><?php echo $text_product; ?></b></td>
                                <td><b><?php echo $entry_barcode; ?></b></td>
                                <td class="text-right"><b><?php echo $text_qty; ?></b></td>
                                <td class="text-right"><b><?php echo $text_price; ?></b></td>
                                <td class="text-right"><b><?php echo $text_total_row; ?></b></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($products as $product) { ?>
                                <tr>
                                    <td><?php echo $product['name']; ?>
                                        <?php foreach ($product['option'] as $option) { ?>
                                            <br />
                                            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                        <?php } ?>
                                        <?= ($is_seller_fbs || $product['is_fbs'])  ?"<p>physical location: Bin: ".$product['fbs_bin'].", Stack: ".$product['fbs_stack']."</p>" :""?>
                                    </td>
                                    <td class="text-right">
                                      <table style="border-collapse: collapse; border: none;">
                                          <tr>
                                              <td  style="font-size:15px;"></td>
                                              <td><svg id="code39_<?php echo $product['product_id']?>" class='svg_barcode'></svg></td>
                                          </tr>
                                      </table>
                                      <span class="barcode" data-barcoed="<?php echo $product['model']?>">
                                          <?php $barcode = $product['model']; ?>
                                      </span>
                                    </td>
                                    <td class="text-right"><?php echo $product['quantity']; ?></td>
                                    <td class="text-right"><?php echo $product['price']; ?></td>
                                    <td class="text-right"><?php echo $product['total']; ?></td>
                                </tr>
                            <?php } ?>
                            <?php foreach ($totals as $total) { ?>
                                <tr>
                                    <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
                                    <td class="text-right"><?php echo $total['text']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php if (isset($comment)) { ?>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td><b><?php echo $column_comment; ?></b></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $order['comment']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    <?php } ?>
                </div>
                </div>
            </div>
        </body>
    </html>
    <script>
        function printinvoice() {
            window.print();
        }
        $(document).ready(function() {
          $(".svg_barcode").each(function(ele){
             JsBarcode("#"+$(this).attr("id") , $(this).parent().parent().parent().parent().parent().find('span').attr('data-barcoed'), {format: "code39", width: 1.0, height:40});
          });
           JsBarcode("#code39_title", "<?php echo $barcode1 ?>", {format: "code39", width: 1.0, height: 40});

           $("svg g text").each(function(){
             $(this).removeAttr("style");
             //$(this).css("font: 15px monospace")
           });
        });



    </script>
<?php } ?>
