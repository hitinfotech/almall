<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>

  <div id="content" class="<?php echo $class; ?>">
  <?php echo $content_top; ?>
  <h1><?php echo $heading_title; ?></h1>
  <fieldset>
    <legend><i class="fa fa-list"></i> <?php echo $text_invoicesList; ?></legend>
      <div class="well">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-month"><?php echo $entry_month; ?></label>
                <select name="filter_month" value="<?php echo $filter_month; ?>" id="input-month" class="form-control" >
                    <option id='0'>  </option>
                    <?php foreach($monthes_array as $i => $value) { ?>
                      <option value='<?= $i ?>' <?= ($i == $filter_month) ? 'selected' : '' ?> > <?= $value?> </option>
                    <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <label class="control-label" for="input-year"><?php echo $entry_year; ?></label>
              <select name="filter_year" value="<?php echo $filter_year; ?>" id="input-year" class="form-control" >
                  <option id='0'> 0 </option>
                  <?php for($i=2017; $i <= 2020; $i++) { ?>
                    <option <?= $i ?> <?= ($i == $filter_year) ? 'selected' : '' ?> > <?= $i?> </option>
                  <?php } ?>
              </select>

              <a onclick="filter();" class="btn btn-primary pull-right"><?php echo $button_filter; ?></a>
            </div>
          </div>
        </div>

        <form method="post" enctype="multipart/form-data" id="form-invoice">
        <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-left">
                <?php if ($sort == 'cp2i.id') { ?>
                <a href="<?php echo $sort_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $entry_id; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_id; ?>"><?php echo $entry_id; ?></a>
                <?php } ?>
              </td>
              <td class="text-left">
                <?php echo $entry_companyname; ?>
              </td>

              <td class="text-left">
                <?php echo $entry_month_year; ?>
              </td>
              <td class="text-left">
                <?php echo $entry_action; ?>
              </td>
            </tr>
          </thead>

          <tbody>
            <?php if ($invoices) { ?>
            <?php foreach ($invoices as $result) { ?>
              <tr>
                <td class="text-left" ><?php echo $result['id']; ?></td>
                <td class="text-left"><?php echo $result['name']; ?></td>
                <td class="text-left"><?php echo $result['month_year']; ?></td>
                <td class="text-left"><a class='btn btn-ingo' href='<?= $result["url"] ?>' >View</a></td>
              </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="text-center" colspan="4"><?php echo "No records founds"; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        </div>
      </form>
      <div class="text-right"><?php echo $pagination; ?></div>
      <div class="text-right"><?php echo $results; ?></div>
    </fieldset>
  <?php echo $content_bottom; ?>
  </div>
  <?php echo $column_right; ?>
  </div>
</div>

<script type="text/javascript">
function filter() {

  url = 'index.php?route=invoices';

  var filter_month = $('select[name=\'filter_month\']').val();

  if (filter_month > 0) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }

  var filter_year = $('select[name=\'filter_year\']').val();
  if (filter_year  > 0) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }

  location = url;
}
//--></script>
<?php echo $footer; ?>
