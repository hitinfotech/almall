<?php echo $header; ?>
<div class="content-padding">
<div class="container">

<?php if ($error_warning) { ?>
<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="alert alert-success"><i class="fa fa-check-circle"> </i> <?php echo $success; ?></div>
<?php } ?>

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>

  <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1>
        <?php echo $heading_title; ?>
        <div class="pull-right">
         <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $back; ?>" data-toggle="tooltip" title="<?php echo $button_back; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
        </div>
      </h1>
    <fieldset>

      <legend><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></legend>

       <form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
          <input type="hidden" name="id" value="<?php if(isset($id)) echo $id; ?>">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_reason; ?></label>
            <div class="col-sm-10">
              <div class="input-group">
                <?php foreach ($languages as $language) { $noentry = true;?>
                  <span class="input-group-addon"><img src="admin/view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>

                  <?php if(isset($reason) AND $reason){ ?>
                    <?php foreach ($reason as $key => $value) { ?>
                      <?php if($key==$language['language_id']){ $noentry = false;?>
                        <input type="text" name="reason[<?php echo $language['language_id']; ?>]" value="<?php echo $value; ?>" class="form-control">
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                  <?php if($noentry){ ?>
                    <input type="text" name="reason[<?php echo $language['language_id']; ?>]" class="form-control">
                  <?php } ?>
                  <br>
                <?php } ?>
              </div>

            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name ="status" class="form-control">
              <option value="0" <?php if(isset($status) AND !$status) echo 'selected'; ?> ><?php echo $text_disable; ?></option>
              <option value="1" <?php if(isset($status) AND $status) echo 'selected'; ?>><?php echo $text_enable; ?></option>
            </select>
            </div>
            <input type="hidden" name="status_info" value="seller" />
          </div>
      </form>

    </fieldset>
  <?php echo $content_bottom; ?></div><!--content-->
<?php echo $column_right; ?></div><!--row-->
</div><!--container-->
</div>
<?php echo $footer; ?>
