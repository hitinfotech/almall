<?php echo $header; ?>
<div class="content-padding">
<div class="container">
  
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  
  <?php if ($error_warning) { ?>
  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
  <div class="row">
    <?php 
          if(isset($menu) && !empty($menu)) {
            $class = 'col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-left'; 
          }else{
            $class = '';
          }
    ?>
    
    <?php echo $menu;?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      

      <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12"> 
          <h1 class="mt0"><?php echo $text_address_book; ?></h1>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
          <p class="break-space"><a href="<?php echo $add; ?>" class="btn btn-secondary btn-block"><?php echo $button_new_address; ?></a></p>
        </div>
      </div>
      
      <?php if ($addresses) { ?>
        <div class="prod-margin10">
          <?php foreach ($addresses as $result) { ?>
          <div class="address-row">
            <div class="row">
              <div class="col-md-10 col-sm-10 col-xs-12">
                <h4><label> <?php echo $result['country']; ?> - <?php echo $result['zone']; ?> </label></h4>
                <?php if($result['default_address']) { ?>
                <p> 
                 <span class="date"><?php echo $entry_default; ?></span>
                </p>
                <?php } ?>
                <?php echo $result['address']; ?>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12">
                <p class="break-space"><a href="<?php echo $result['update']; ?>" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> <?php echo $button_edit;?></a></p>
                <p class="break-space"><a href="<?php echo $result['delete']; ?>" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i><?php echo $button_delete;?></a></p>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      <?php } else { ?>
        <p><?php echo $text_empty; ?></p>
      <?php } ?>
      
      <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12"> 
          
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
          <div class="break-space pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
        </div>
      </div>

<!--
    <?php /*

      <h2><?php //echo $text_address_book; ?></h2>
      
      <?php //if ($addresses) { ?>
      
      <table class="table table-bordered table-hover">
        <?php //foreach ($addresses as $result) { ?>
        <tr>
          <td class="text-left">
            <?php //echo $result['address']; ?>
          </td>
          <td class="text-right">
            <a href="<?php //echo $result['update']; ?>" class="btn btn-info">
              <?php //echo $button_edit; ?>
            </a> &nbsp; 
            <a href="<?php //echo $result['delete']; ?>" class="btn btn-danger">
              <?php //echo $button_delete; ?>
            </a>
          </td>
        </tr>
        <?php //} ?>
      </table>
      
      <?php //} else { ?>
      
      <p><?php //echo $text_empty; ?></p>
      
      <?php //} ?>

      <div class="buttons clearfix">
        <div class="pull-left"><a href="<?php //echo $back; ?>" class="btn btn-default"><?php //echo $button_back; ?></a></div>
        <div class="pull-right"><a href="<?php //echo $add; ?>" class="btn btn-primary"><?php //echo $button_new_address; ?></a></div>
      </div>
      
    */ ?>
-->




    </div>
    
  </div>

</div>
</div>
<?php echo $footer; ?>