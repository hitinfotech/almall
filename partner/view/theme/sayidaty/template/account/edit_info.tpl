<?php echo $header; ?>
<div class="content-padding">
<div class="container">
  <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row">
    <div id="content" >
      <h1><?php echo $heading_title; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset>

          <legend><?php echo $text_email; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="email" value="<?php echo $current_email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" data-toggle="popover" data-trigger="focus" title="<?php echo $text_email ?>" data-content="This email is a primary email which will be the log-in email to access your account & will receive the email notification/s." />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-fbs-email"><?php echo $entry_fbs_email; ?></label>
            <div class="col-sm-10">
              <input type="text" name="fbs_email" value="<?php echo $fbs_email; ?>" placeholder="<?php echo $entry_fbs_email; ?>" id="input-fbs-email" class="form-control" data-toggle="popover" data-trigger="focus" title="<?php echo $entry_fbs_email ?>" data-content="You can add another email which will ONLY receive the email notification/s." />
            </div>
          </div>


          <legend><?php echo $text_password; ?></legend>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-current-password"><?php echo $entry_current_password; ?></label>
            <div class="col-sm-10">
              <input type="password" name="current_password" value="<?php echo $current_password; ?>" placeholder="<?php echo $entry_current_password; ?>" id="input-current-password" class="form-control" />
              <?php if ($error_current_password) { ?>
              <div class="text-danger"><?php echo $error_current_password; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
            <div class="col-sm-10">
              <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
              <?php if ($error_password) { ?>
              <div class="text-danger"><?php echo $error_password; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
            <div class="col-sm-10">
              <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
              <?php if ($error_confirm) { ?>
              <div class="text-danger"><?php echo $error_confirm; ?></div>
              <?php } ?>
            </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          <div class="pull-left">
            <input type="submit" value="<?php echo $button_save; ?>" class="btn btn-primary" />
          </div>
        </div>
      </form>
    </div>

  </div>
</div>
</div>
<?php echo $footer; ?>

<script>
$(document).ready(function() {
  $('[data-toggle="popover"]').popover({
    container: 'body'
  })
});
</script>
