<?php echo $header; ?>
<div class="content-padding">
    <div class="container">
        <h1><?php echo $heading_title; ?></h1>
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger">
                <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert">×</button>
            </div>
        <?php } ?>
        <?php if (!isset($_GET['order_id'])) { ?>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 ">
                    <div class="row">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="regForm">

                            <div class="form-group required">
                                <label><?php echo $entry_email; ?></label>
                                <input type="email" name="email" value="" placeholder="" id="input-email"  class="form-control" required aria-required="true">
                            </div>
                            <div class="form-group required">
                                <label><?php echo $column_order_id; ?></label>
                                <input type="text" name="order" value="" placeholder="" class="form-control" minlength="1" required aria-required="true">
                            </div>
                            <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary" />

                        </form>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <?php /* <a href="<?php echo $order['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
              <a onclick = "getProductOrderStatus(<?php echo $order['order_id'] ?>)" data-toggle="tooltip" class="btn btn-primary btn-sm"><i class="fa fa-truck"></i></a> */ ?>

        <?php } ?>
    </div>
</div>
<?php echo $footer; ?>

