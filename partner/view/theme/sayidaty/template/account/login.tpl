<?php echo $header; ?>
<div class="content-padding">
    <div class="container">
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <div class="box checkout-sign-box">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                                    <h1 class="text-center"><?php echo $heading_title; ?></h1>
                                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="regForm">
                                        <?php if ($error_warning) { ?>
                                            <div class="error form-group danger-text"><?php echo $error_warning; ?></div>
                                        <?php } ?>
                                        <div class="form-group required">
                                            <label><?php echo $entry_email; ?></label>
                                            <input type="email" name="email" value="<?php echo $email; ?>" placeholder="" id="input-email"  class="form-control" required aria-required="true">
                                        </div>
                                        <div class="form-group required">
                                            <label><?php echo $entry_password; ?></label>
                                            <input type="password"name="password" value="<?php echo $password; ?>" placeholder="" class="form-control" minlength="5" required aria-required="true">
                                         <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>
                                            <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary" />
                                        <?php if ($redirect) { ?>
                                            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                                        <?php } ?>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?>
        </div>
    </div>
</div>
<script>
  (function($){
    $(function() {
      $("[name='email']").emailautocomplete({
        domains: ["zoho.com","inbox.com"] //add your own domains
      });
    });
  }(jQuery));
</script>
<?php echo $footer; ?>
