<?php echo $header; ?>
<div class="content-padding">
    <div class="container">

        <div class="row">

            <?php
            if (isset($menu) && !empty($menu)) {
                $class = 'col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-left';
            } else {
                $class = '';
            }
            ?>

            <?php echo $menu; ?>
            <div id="content" class="<?php echo $class; ?>">
                <?php echo $content_top; ?>
                <h1><?php echo $heading_title; ?></h1>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo $entry_newsletter; ?></label>
                            <div class="col-sm-10">
                                <?php if ($newsletter) { ?>
                                    <label class="radio-inline">
                                        <input type="radio" name="newsletter" value="1" checked="checked" />
                                        <?php echo $text_yes; ?> </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="newsletter" value="0" />
                                        <?php echo $text_no; ?></label>
                                <?php } else { ?>
                                    <label class="radio-inline">
                                        <input type="radio" name="newsletter" value="1" />
                                        <?php echo $text_yes; ?> </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="newsletter" value="0" checked="checked" />
                                        <?php echo $text_no; ?></label>
                                <?php } ?>
                            </div>
                        </div>
                    </fieldset>
                    <div>
                        <?php if (isset($newsletter_category) && !empty($newsletter_category)) { ?>
                            <?php $counter = count($newsletter_category); ?>
                            <?php foreach ($newsletter_category as $key => $value) { ?>
                                <?php if ($key % 2 == 0) { ?><div class="row"><?php } ?>
                                    <div class="col-md-6 col-sm-6">
                                        <p>
                                            <label>
                                                <input type="checkbox" <?php echo $value['selected'] == 1 ? "checked" : "" ?> class="exclude" value="<?php echo $value['newsletter_category_id']; ?>" name="newsletter_check[]">
                                                <?php echo $value['name']; ?>
                                            </label>
                                        </p>
                                    </div>
                                    <?php $counter--; ?>
                                    <?php if ($key % 2 != 0) { ?></div><?php } ?>
                            <?php } ?>
                        <?php } ?>

                    </div>
                    <hr>
                    <div class="buttons clearfix">
                        <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                        <div class="pull-right">
                            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                        </div>
                    </div>
                </form>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
</div>
<?php echo $footer; ?>