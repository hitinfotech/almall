<?php echo $header; ?>
<script>
fbq('track', 'CompleteRegistration', {value: 1.00,currency: 'USD'});
</script>
<div class="content-padding">
    <div class="container">
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
              
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h1> <span class="conf-box"><em aria-hidden="true" class="glyphicon glyphicon-ok"></em> </span> <?php echo $text_welcome; ?></h1>
                    <br>
                    <div class="f17"><?php echo $text_text; ?></div>

                    <hr class="t-break-space">

                    <?php echo 'For any request, you can contact us <a href=\'mailto:seller@sayidatymall.net\' >seller@sayidatymall.net</a>'; ?>
                </div>

                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
</div>
<?php echo $footer; ?>
