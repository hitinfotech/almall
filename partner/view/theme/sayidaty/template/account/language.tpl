<?php echo $header; ?>
<div class="content-padding">
    <div class="container">

        <div class="row">

            <?php
            if (isset($menu) && !empty($menu)) {
                $class = 'col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-left';
            } else {
                $class = '';
            }
            ?>

            <?php echo $menu; ?>
            <div id="content" class="<?php echo $class; ?>">
                <?php echo $content_top; ?>
                <h2><?php echo $text_edit_language; ?></h2>
                <fieldset>
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="d-break-space">

                        <div class="form-group custom-field" >
                            <label><?php echo $select_language; ?></label>
                            <div >
                                <input type="radio" name="language_id" value="1" <?php
                                if ($language_id == 1) {
                                    echo "checked";
                                }
                                ?> > English &nbsp;&nbsp;&nbsp;
                                <input type="radio" name="language_id" value="2" <?php
                                if ($language_id == 2) {
                                    echo "checked";
                                }
                                ?> > عربي<br>
                            </div>
                            <div class="buttons clearfix d-break-space">
                                <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                                <div class="pull-right">
                                    <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                                </div>
                            </div>
                        </div>
                    </form>
                </fieldset>
                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?>
        </div>
    </div>
</div>
<?php echo $footer; ?>
