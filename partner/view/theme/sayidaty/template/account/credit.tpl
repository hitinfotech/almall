<?php echo $header; ?>
<div class="content-padding">
    <div class="container">


        <div class="row">

            <?php
            if (isset($menu) && !empty($menu)) {
                $class = 'col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-left';
            } else {
                $class = '';
            }
            ?>

            <?php echo $menu; ?>

            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <h1><?php echo $heading_title; ?></h1>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-right"><?php echo $column_date_added; ?></th>
                                <th class="text-right"><?php echo $column_description; ?></th>
                                <th class="text-right"><?php echo $column_amount; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($transactions) { ?>
                                <?php foreach ($transactions as $transaction) { ?>
                                    <tr>
                                        <td class="text-right"><?php echo $transaction['date_added']; ?></td>
                                        <td class="text-right"><?php echo $transaction['description']; ?></td>
                                        <td class="text-right"><?php echo $transaction['amount']; ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="text-right"><b><?php echo $text_balance; ?></b></td>
                                    <td class="text-right"><?php echo $balance; ?></td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>


                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?>
        </div>
    </div>
</div>
<?php echo $footer; ?>
