<?php echo $header; ?>


<div class="content-padding">

<div class="container">
 <div class="row">
<div id="content">
   <?php echo $menu; ?>


     <div class="col-md-9 col-sm-9 col-xs-12">
       <h1><?php echo $heading_title; ?></h1>

      <div class="preference-tabs">
        <ul role="tablist" id="prefTabs" class="nav nav-tabs  seller-tabs">
        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="prefProd" data-toggle="tab" role="tab" id="prefProd-tab" href="#prefProd"><?php echo $tab_fav_products?></a></li>
        <!--<li role="presentation" class=""><a aria-controls="prefBrands" data-toggle="tab" id="profile-tab" role="tab" href="#prefBrands" aria-expanded="false"><?php echo $tab_fav_sellers?></a></li>-->
        <li class=""><a aria-controls="prefSeller" data-toggle="tab" id="prefSeller-tab" role="tab" href="#prefSeller" aria-expanded="false"><?php echo $tab_fav_brands?></a></li>
        <li role="presentation"><a aria-expanded="true" aria-controls="prefLooks" data-toggle="tab" role="tab" id="prefLooks-tab" href="#prefLooks"> <?php echo $tab_fav_looks?></a></li>

        <li role="presentation"><a aria-expanded="true" aria-controls="prefTips" data-toggle="tab" role="tab" id="prefTips-tab" href="#prefTips"><?php echo $tab_fav_tips?></a></li>
        <li role="presentation" class=""><a aria-controls="prefCat" data-toggle="tab" id="prefCat-tab" role="tab" href="#prefCat" aria-expanded="false"><?php echo $tab_fav_categories?></a></li>
<!--
        <li role="presentation" class=""><a aria-controls="prefSize" data-toggle="tab" id="prefSize-tab" role="tab" href="#prefSize" aria-expanded="false">مقاساتي </a></li>
-->
        </ul>
      </div>

      <div id="prefTabContent" class="tab-content">
      <div div aria-labelledby="prefProd-tab" id="prefProd" role="tabpanel" class="tab-pane fade active in">

        <div class="row">
          <?php if (isset($fav_products) && !empty($fav_products)){?>

            <?php foreach($fav_products as $product){?>
             <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6" id="wishlist">
               <div class="tile prod-tile">
                 <div class="fave-heart">
                   <a href="javascript:void(0);" onclick='switch_case($(this),2,<?php echo $product["product_id"] ?>,wishlist)' class="active"><i class="fa fa-heart"></i></a>
                 </div>
                 <div class="tile-img">
                   <a href="<?php echo $product['href'] ?>"><img src="<?php echo $product['image'] ?>" class="proimg"></a>
                 </div>
                 <div class="tile-text">
                   <a href="<?php echo $product['href'] ?>"><?php echo $product['name']?></a>
                 </div>
                 <?php if ($product['is_shopable'] != 10) { ?>
                 <div class="text-center">
                   <a href="javascript:void(0)" onclick='cart.add(<?php echo $product["product_id"] ?>);' class="btn btn-secondary btn-sm"><?php echo $add_to_cart ?></a>
                 </div>
                   <?php } ?>
               </div>
             </div>
    <?php } ?>
   <?php }else { ?>
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6" >
       <div class="tile">
        <p>  <?php echo $no_favorite_products?></p>
      </div>
    </div>

     <?php } ?>
        </div>
      </div>

      <div role="tabpanel" class="tab-pane" id="prefBrands">
      <div class="row">
        <?php if (isset($fav_sellers) && !empty($fav_sellers)){?>

          <?php foreach($fav_sellers as $seller){?>

    <div class="col-lg-3 col-md-4 col-sm-3 col-xs-6" id="seller">
      <div class="brand-tile">
        <div class="fave-heart">
            <a href="Javascript:void(0);" onclick="switch_case($(this),2,<?php echo $seller['customer_id'] ?>,seller)" class="active"><i class="fa fa-heart"></i></a>
          </div>
          <div class="tile-img">
        <a href="<?php echo $seller['href'] ?>"><img src="<?php echo $seller['image'] ?>">
        </div>
         <div class="bran-name"><?php echo $seller['screenname'] ?></div>
        </a>
      </div>
    </div>
    <?php } ?>
   <?php } else { ?>
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6" >
       <div class="tile">
        <p>  <?php echo $no_favorite_sellers?></p>
      </div>
    </div>

     <?php } ?>

  </div>
      </div>

      <div role="tabpanel" class="tab-pane" id="prefSeller">

      <div class="row">
        <?php if (isset($fav_brands) && !empty($fav_brands)){?>

          <?php foreach($fav_brands as $brand){?>

    <div class="col-lg-3 col-md-4 col-sm-3 col-xs-6" id="brand">
      <div class="brand-tile">
        <div class="fave-heart">
            <a href="javascript:void(0)" onclick="switch_case($(this),2,<?php echo $brand['brand_id'] ?>,brand)" class="active"><i class="fa fa-heart"></i></a>
          </div>
         <a href="<?php echo $brand['href'] ?>">
             <div class="brand-img"><img src="<?php echo $brand['image'] ?>"></div>
             <div class="bran-name">
                 <?php echo $brand['name'] ?>
             </div>
         </a>

      </div>
    </div>
    <?php } ?>
   <?php } else { ?>
     <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6" >
       <div class="tile">
        <p>  <?php echo $no_favorite_brands?></p>
      </div>
    </div>

     <?php } ?>

  </div>
      </div>


            <div role="tabpanel" class="tab-pane" id="prefLooks">
            <div class="row">
              <?php if (isset($customer_looks) && !empty($customer_looks)){ ?>
              <?php foreach ($customer_looks as $look){?>

         <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" id="look">
           <div class="tile">
             <div class="fave-heart">
                 <a href="Javascript:void(0);" onclick="switch_case($(this),2,<?php echo $look['look_id'] ?>,look)" class="active"><i class="fa fa-heart"></i></a>
               </div>
             <div class="tile-img">

               <a href="<?php echo $look['href']?>"><img src="<?php echo $look['image']?>" class="proimg"></a>
             </div>

             <div class="tile-text">
               <a href="<?php echo $look['href']?>"><?php echo $look['name']?></a>
             </div>
      </div>
         </div>
         <?php  } ?>
         <?php } else { ?>
           <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6" >
             <div class="tile">
              <p>  <?php echo $no_favorite_looks?></p>
            </div>
          </div>

           <?php } ?>

          </div>

            </div>

            <div role="tabpanel" class="tab-pane" id="prefTips">
            <div class="row">
         <div class="col-md-12">
<?php if(isset($customer_tips) && !empty($customer_tips)){?>
           <?php foreach ($customer_tips as $tip){  ?>
           <div class="tip-row">
             <div class="row" id='tip'>
               <div class="col-md-5 col-sm-5 col-xs-12">
                 <div class="fave-heart">
                     <a href="Javascript:void(0);" onclick="switch_case($(this),2,<?php echo $tip['tip_id'] ?>,tip)" class="active"><i class="fa fa-heart"></i></a>
                   </div>
                 <div class="tip-row-img">
                   <img src="<?php echo $tip['image']?>">
                </div>
               </div>
               <div class="col-md-7 col-sm-7 col-xs-12">
                 <div class="tip-row-details">
                   <?php if(!empty($tip['groups'])) {?>
                   <?php foreach ($tip['groups'] as $group) {?>
                   <p> <a href="<?php echo $group['href']?>" class="tip-clas"> <?php echo $group['name']?></a></p>
                   <?php } ?>
                   <?php } ?>
                   <p class="date"> <?php echo $tip['date']?></p>
                   <h3><a href="<?php  echo $tip['href']?>"> <?php echo $tip['name']?> </a></h3>
                   <p><?php  echo $tip['description']?></p>
                 </div>
               </div>

             </div>
           </div>
            <?php } ?>
              <?php } else { ?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6" >
                  <div class="tile">
                     <p><?php echo $no_favorite_tips?></p>
                 </div>
               </div>

                <?php } ?>

         </div>
        </div>

            </div>

      <div role="tabpanel" class="tab-pane" id="prefCat">
       <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">

           <?php foreach ($fav_categories  as $main_category){ ?>
         <div class="pref-cat-content">
          <div class="pref-cat-title">
            <h4><?php echo $main_category[0]['name'] ?></h4>
          </div>
          <div class="row" id="category">
            <?php array_shift($main_category)?>
            <?php foreach ($main_category as $sub_category){?>
           <div class="col-sm-4">
             <?php if (in_array($sub_category['category_id'],$customer_categories)){ ?>
            <div class="pref-cat-row"><a href="javascript:void(0)" onclick="switch_case($(this),2,<?php echo $sub_category['category_id'] ?>,category)" class="active"><span><i class="fa fa-heart"></i></span> <?php echo $sub_category['name'] ?></a></div>
             <?php }else {?>
               <div class="pref-cat-row"><a href="javascript:void(0)" onclick="switch_case($(this),1,<?php echo $sub_category['category_id'] ?>,category)" ><span><i class="fa fa-heart"></i></span> <?php echo $sub_category['name'] ?></a></div>
               <?php } ?>
           </div>
           <?php } ?>
         </div>
       </div>

     <?php } ?>
       </div>
      </div>

      </div>
      </div>

    </div>
 </div>
</div>

</div>

</body>
</html>


<?php echo $footer; ?>


<script>
function switch_case(ele,status,id,tab_name){
  if (status==1){
    tab_name.add(id);
    ele.addClass("active");
    ele.attr("onclick"," switch_case ($(this),2,"+id+","+ele.parent().parent().parent().attr("id")+")");
  }else{
    tab_name.remove(id);
    ele.removeClass("active");
    ele.attr("onclick"," switch_case ($(this),1,"+id+","+ele.parent().parent().parent().attr("id")+")");
  }
}
</script>
