<?php echo $header; ?>
<div class="content-padding">
    <div class="container">
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <div class="row">
            <?php
            if (isset($menu) && !empty($menu)) {
                $class = 'col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-left';
            } else { 
                $class = '';
            }
            ?>
            <?php echo $menu; ?>
            <div id="content" class="<?php echo $class; ?>">
                <h1><?php echo $heading_title; ?></h1>
                <div class="user-profile">
                    <div class="row">
                        <!-- Image Upload -->
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <?php if ($imgFlag) { ?>
                                <form action="<?php echo $del_action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <button type="button" class="btn btn-danger profile-del-pic" onclick="javascript:this.form.submit();" data-original-title="Delete"><i class="fa fa-trash-o"></i></button>
                                </form>
                            <?php } ?>
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="user-pic">
                                    <div class="changePhoto">
                                        <?php echo $text_change_pic; ?>
                                        <input type="file" onchange="javascript:this.form.submit();" name="image">
                                    </div>
                                    <img src="<?php echo $image; ?>">
                                </div>
                            </form>
                        </div>
                        <!-- Image Upload -->
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <div class="user-info">
                                <h1 class="text-primary"><?php echo $firstname; ?> <?php echo $lastname; ?></h1>
                                <p> <?php echo $city; ?> - <?php echo $country; ?></p>
                                <p> <?php echo $txt_member_since; ?> : <?php echo date("Y-m-d", strtotime($date_added)); ?></p>
                                <hr>
                                <p><?php echo $text_birthdate ?>: <?php echo date("Y-m-d", strtotime($birthdate)); ?></p>
                                <div class="edit-button text-center d-break-space">
                                    <a href="<?php echo $edit_account_url; ?>" class="btn btn-secondary"> <?php echo $txt_edit_profile; ?> </a>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
                <!-- <li><a href="index.php?route=module/automatednewsletter/newsletter">Archived newsletters</a></li> -->
                <?php
                /*
                  <!--
                  <div class="col-md-4">
                  <h2><?php echo $text_my_account; ?></h2>
                  <ul class="list-unstyled">
                  <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
                  <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
                  <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
                  <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                  </ul>
                  </div>

                  <div class="col-md-4">
                  <h2><?php echo $text_my_orders; ?></h2>
                  <ul class="list-unstyled">
                  <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                  <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                  <?php if ($reward) { ?>
                  <li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
                  <?php } ?>
                  <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                  <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                  <li><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li>
                  </ul>
                  </div>

                  <div class="col-md-4">
                  <h2><?php echo $text_my_newsletter; ?></h2>
                  <ul class="list-unstyled">
                  <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
                  </ul>
                  </div>
                  -->
                 */
                ?>      

            </div>

        </div>
    </div>
</div>
<?php echo $footer; ?>
