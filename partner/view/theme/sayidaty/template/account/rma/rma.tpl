<?php echo $header; ?>
<div class="content-padding">
<div class="container">
  <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"> </i> <?php echo $success; ?></div>
  <?php } ?>

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
<?php echo $menu; ?>
<div class="col-md-9 col-sm-9 col-xs-12">

  <div id="content" class="<?php echo $class; ?>">
    <?php echo $content_top; ?>
    <h1>
      <?php echo $heading_title; ?>
    </h1>

    <fieldset>
      <?php /* <legend><i class="fa fa-reply"></i> <?php echo $heading_title; ?></legend> */ ?>

      <div >
        <p><?php echo $text_return_description?></p>
        <p>  <a href="<?php echo $newrma; ?>" data-toggle="tooltip" title="<?php echo $wk_rma_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $text_create_new_return?></a></p>
      </div>
      
      <form action="<?php echo $newrma; ?>" method="post" enctype="multipart/form-data" id="form-product">
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <td>
                    <?php if ($sort == 'wro.id') { ?>
                      <a href="<?php echo $sort_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_id; ?></a>
                    <?php } else { ?>
                      <a href="<?php echo $sort_id; ?>" > <?php echo $wk_rma_id; ?> </a>
                    <?php } ?>
                </td>
                <td class="text-left">
                   <?php if ($sort == 'wro.order_id') { ?>
                      <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_orderid; ?></a>
                    <?php } else { ?>
                      <a href="<?php echo $sort_order; ?>" > <?php echo $wk_rma_orderid; ?> </a>
                    <?php } ?>
                </td>
                <td class="text-left">
                  <?php if ($sort == 'wrp.quantity') { ?>
                    <a href="<?php echo $sort_qty; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_quantity; ?></a>
                  <?php } else { ?>
                    <a href="<?php echo $sort_qty; ?>" > <?php echo $wk_rma_quantity; ?> </a>
                  <?php } ?>
                </td>
                <td class="text-left">
                  <?php if ($sort == 'ot.value') { ?>
                    <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_price; ?></a>
                  <?php } else { ?>
                    <a href="<?php echo $sort_price; ?>" > <?php echo $wk_rma_price; ?> </a>
                  <?php } ?>
                </td>
                <td class="text-left">
                  <?php if ($sort == 'wrs.order_status_id') { ?>
                    <a href="<?php echo $sort_rma_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_status; ?></a>
                  <?php } else { ?>
                    <a href="<?php echo $sort_rma_status; ?>" > <?php echo $wk_rma_status; ?> </a>
                  <?php } ?>
                </td>
                <td class="text-left">
                  <?php if ($sort == 'wro.date') { ?>
                    <a href="<?php echo $sort_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $wk_rma_date; ?></a>
                  <?php } else { ?>
                    <a href="<?php echo $sort_date; ?>" > <?php echo $wk_rma_date; ?> </a>
                  <?php } ?>
                </td>
                <td class="text-center"><?php echo $wk_rma_action; ?></td>
              </tr>
            </thead>

            <?php if($rma_ress){ ?>
            <?php foreach ($rma_ress as $res) { ?>
            <tbody>
              <tr>
                <td><?php echo $res['id']; ?></td>
                <td class="text-left"><?php echo $res['order_id']; ?></td>
                <td class="text-left"> <?php echo $res['quantity']; ?></td>
                <td class="text-left"> <?php echo $res['price']; ?></td>
                <td class="text-left" style="color:<?php echo $res['color']; ?>;"><?php echo $res['rma_status']; ?></td>

                <td class="text-left" id="rma_date"><?php echo $res['date']; ?></td>
                <td class="text-center">
                    <?php foreach ($res['action'] as $action) { ?>
                      <a href="<?php echo $action['href']; ?>" class="btn btn-primary btn-sm" data-toggle="tooltip" title="<?php echo $action['text'] ;?>"><i class="fa fa-eye"></i></a>

                      <?php if($res['rma_status'] != 'Return' AND $res['rma_status'] != 'Complete' AND $res['rma_status'] != 'Decline' AND $res['rma_status'] != 'Canceled' AND $res['rma_status'] != 'Solved'){ ?>
                        <a onclick="confirm('<?php echo $wk_rma_r_u_sure; ?>') ? location = '<?php echo $res['cancel_rma_link']; ?>' : false;" class="btn btn-default btn-sm" data-toggle="tooltip" title="<?php echo $wk_rma_cancel ;?>"><i class="fa fa-times"></i></a>
                      <?php } ?>
                    <?php } ?>
                </td>
              </tr>
            </tbody>
            <?php } } else{ ?>
            <tbody>
              <tr>
                <td colspan="7" class="text-center"><?php echo $wk_rma_empty; ?></td>
              </tr>
            </tbody>
            <?php } ?>
          </table>
        </div>
      </form>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
    </fieldset>
    <?php echo $content_bottom; ?>
  </div>
  <?php echo $column_right; ?>
  </div>
</div>
</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
$('.row input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

$('.datetime').datetimepicker({
  pickTime: false
});

function filter() {
  url = 'index.php?route=account/rma/rma';

  var filter_id = $('input[name=\'filter_id\']').val();

  if (filter_id) {
    url += '&filter_id=' + encodeURIComponent(filter_id);
  }

  var filter_order = $('input[name=\'filter_order\']').val();

  if (filter_order) {
    url += '&filter_order=' + encodeURIComponent(filter_order);
  }

  var filter_qty = $('input[name=\'filter_qty\']').val();

  if (filter_qty) {
    url += '&filter_qty=' + encodeURIComponent(filter_qty);
  }

  var filter_price = $('input[name=\'filter_price\']').val();

  if (filter_price) {
    url += '&filter_price=' + encodeURIComponent(filter_price);
  }

  var filter_rma_status = $('select[name=\'filter_rma_status\']').val();

  if (filter_rma_status != '*') {
    url += '&filter_rma_status=' + encodeURIComponent(filter_rma_status);
  }

  var filter_date = $('input[name=\'filter_date\']').val();

  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  }

  location = url;
}
</script>
