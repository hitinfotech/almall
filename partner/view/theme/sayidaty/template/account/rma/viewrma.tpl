<?php if(!isset($viewrma) AND $viewrma) return; ?>
<?php echo $header; ?>
<div class="content-padding">
<div class="container">
  <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
  <?php } ?>
  <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <?php echo $menu; ?>
    <div class="col-md-9 col-sm-9 col-xs-12">
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1>
        <?php echo $heading_title; ?>
      </h1>
        <fieldset>
          <div class="mb20"><?php echo $viewrma['date']; ?></div>
          <div class="row">
            <div class="col-sm-12">
                <p class="f17 border-btm"><?php echo $text_order_details; ?></p>
                <div class="bg">
                  <?php echo $wk_viewrma_orderid; ?>
                  <a href="<?php echo $viewrma['order_url']; ?>" target="_blank"># <?php echo $viewrma['order_id']; ?></a><br/>
                  <?php echo $wk_viewrma_rma_tatus; ?>
                  <span style="color:<?php echo $viewrma['color']; ?>"><?php echo $viewrma['rma_status']; ?></span>  <br/>
                  <span><?php echo $wk_viewrma_authno; ?> </span><span class="auth_no_here">
                  <?php echo $viewrma['rma_auth_no']; ?></span>
                  <?php if(!$viewrma['rma_auth_no'] AND $viewrma['rma_status'] != 'Canceled' AND $viewrma['rma_status'] != 'Solved'){ ?>
                    <label data-toggle="tooltip" title="<?php echo $wk_viewrma_enter_cncs_no ;?>" class="add-auth-no">
                    </label>

                    <div id="add-auth-no" class="alert alert-info hide">
                      <button type="button" class="close add-auth-no-close">&times;</button>
                      <div class="form-group">
                        <input type="text" id="auth-no-text" placeholder="<?php echo $wk_viewrma_enter_cncs_no; ?>" class="form-control" style="margin-bottom:3px;">
                      </div>
                      <button class="btn btn-info btn-xs submit-auth-no" value="Save"><i class="fa fa-save"></i></button>
                    </div>
                  <?php } ?>
                </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <p class="f17 border-btm"> <?php echo $wk_viewrma_add_info; ?></p>
              <div class="bg"><?php echo $viewrma['add_info']; ?> </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <p class="f17 border-btm"><?php echo $wk_viewrma_image; ?></p>
              <div class="bg">
                <label type="button" for="upload-file" class="btn btn-default " data-toggle="tooltip" title="<?php echo $text_upload_img ;?>" <?php if($viewrma['rma_status'] == 'Canceled' || $viewrma['rma_status'] == 'Solved'){ ?>disabled<?php } ?>><?php echo $text_upload_img ;?></label>
                <input type="file" name="rma_file[]" accept="image/*" multiple="" style="display:none" id="upload-file" >
                <div class="clearfix"></div>
                <br/>
                <ul class="thumbnails">
                <?php if($viewrma['images']){ ?>
                  <?php foreach($viewrma['images'] as $images){ ?>
                    <?php if($images['image']){ ?>
                      <li class="image-additional"><a href="<?php echo $images['image']; ?>" data-toggle="tooltip" title="View Images" class="thumbnail"><img src="<?php echo $images['resize']; ?>"></a></li>
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
                </ul>
              </div>
            </div>
          </div>

          <?php if($viewrma['rma_status'] != 'Solved') { ?>
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">

            <div class="row">
              <div class="col-sm-12">
                <p class="f17 border-btm"><?php echo $wk_viewrma_close_rma; ?></p>
                <div class="bg">
                  <input type="checkbox" id="cancel-request" name="solve" value="2" required> <label for="cancel-request"><?php echo $wk_viewrma_close_rma_text; ?> </label>

                  <div class="clearfix"></div>
                  <input type="hidden" name="wk_viewrma_grp_id" value="<?php echo $vid; ?>">
                  <div class="break-space"><input type="submit" class="btn btn-primary" value="Save"></div>
                </div>
              </div>
            </div>

          </form>
          <?php } ?>

          <div class="row">
            <div class="col-sm-12">
              <p class="f17 d-break-space"><?php echo $wk_viewrma_item_req; ?></p>
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <tr>
                        <td><?php echo $wk_viewrma_pname; ?></td>
                        <td><?php echo $wk_viewrma_model; ?></td>
                        <td><?php echo $wk_viewrma_price; ?></td>
                        <td><?php echo $wk_viewrma_reason; ?></td>
                        <td><?php echo $wk_viewrma_qty; ?></td>
                        <td><?php echo $wk_viewrma_ret_qty; ?></td>
                        <td><?php echo $wk_viewrma_subtotal; ?></td>
                      </tr>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(isset($prodetails) AND $prodetails){
                      foreach($prodetails as $pdetails){ ?>
                        <tr>
                          <td><a href="<?php echo $pdetails['link']; ?>" target="_blank"><?php echo $pdetails['name']; ?></a>
                            <?php if(isset($pdetails['option']) && $pdetails['option']){ ?>
                            <?php foreach ($pdetails['option'] as $key => $value) {?>
                              <br>&nbsp;<small> - <?php echo $value['name']; ?> : <?php echo $value['value']; ?></small>
                            <?php } } ?>
                          </td>
                          <td><?php echo $pdetails['model']; ?></td>
                          <td><?php echo $pdetails['price']; ?></td>
                          <td><?php echo $pdetails['reason']; ?></td>
                          <td><?php echo $pdetails['ordered']; ?></td>
                          <td><?php echo $pdetails['returned']; ?></td>
                          <td><?php echo $pdetails['total']; ?></td>
                          <input type="hidden" name="sellerid" value="<?php echo $pdetails['sellerId']; ?>">
                        </tr>
                    <?php } }else{ ?>
                        <tr>
                          <td colspan="6" class="text-center">All Products Returned !! </td>
                        </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <br/>

          <?php if(isset($rma_messages) AND $rma_messages){ ?>

            <div class="row rma">
              <div class="col-sm-12">
                <p class="bg bg-primary"><i class="fa fa-comments-o"></i> <?php echo $text_messages; ?></p>
              </div>

            <?php foreach($rma_messages as $res_message){ ?>

              <?php if($res_message['writer']!='seller' && $res_message['writer']!='admin'){//me ?>
                <div class="col-sm-10">
                  <div class="alert alert-success">
                    <label data-toggle="tooltip" title="<?php echo ucfirst($res_message['writer']); ?>"><i class="fa fa-user"></i> </label>
                    <label class="pull-right" ><i class="fa fa-clock-o"></i> <?php echo $res_message['date']; ?></label>
                    <br/>
                    <?php echo $res_message['message']; ?>
                    <?php if($res_message['attachment']){ ?>
                    <br/>
                      <a href="<?php echo $attachmentLink.$res_message['attachment'];?>" data-toggle="tooltip" title="<?php echo $text_download; ?>" class="text-info" target="_blank"><i class="fa fa-download"></i> <?php echo $res_message['attachment']; ?></a>
                    <?php } ?>
                  </div>
                </div>
              <?php }else{ ?>
                <div class="col-sm-10 pull-right">
                  <div class="alert alert-info">
                  <label data-toggle="tooltip" title="<?php echo ucfirst($res_message['writer']); ?>"><i class="fa fa-home"></i></label>
                  <label class="pull-right"><i class="fa fa-clock-o"></i> <?php echo $res_message['date']; ?></label>
                  <br/>
                  <?php echo $res_message['message']; ?>
                  <?php if($res_message['attachment']){ ?>
                  <br/>
                    <a href="<?php echo $attachmentLink.$res_message['attachment'];?>" target="_blank" data-toggle="tooltip" title="<?php echo $text_download; ?>" class="text-success"><i class="fa fa-download"></i> <?php echo $res_message['attachment']; ?></a>
                  <?php } ?>
                </div>
                </div>
              <?php } ?>
             <?php } ?>
            </div>


            <div class="row">
              <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
              <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
            <br>
          <?php } ?>

          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form_msg">

            <div class="row required">
              <label class="col-sm-12 control-label"><?php echo $wk_viewrma_msg; ?></label>
              <div class="col-sm-12">
                <input type="hidden" name="wk_viewrma_grp_id" value="<?php echo $vid; ?>">
                <input type="hidden" name="seller_id" value="<?php echo $sellerId; ?>">
                <textarea name="wk_view_message" rows="4" class="form-control" required></textarea>

                <br/>

                <div class="input-group col-sm-12">
                  <span class="input-group-btn">
                    <label type="button" class="btn btn-default" for="file-upload" data-toggle="tooltip" title="<?php echo $text_allowed_ex; ?>"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></label>
                  </span>
                  <input type="text" id="input-file-name" class="form-control" disabled/>
                </div>

                <input type="file" id="file-upload" name="up_file" class="form-control hide">
                <br/>
                <?php if($viewrma['rma_status'] == 'Canceled'){ ?>
                  <input type="checkbox" id="re-open-cancel" name="wk_view_reopensolved" value="0" style=" position: relative; top: 2px;" > <label class="text-left control-label" for="re-open-cancel"><?php echo $wk_viewrma_reopen; ?></label>
                <?php } ?>

                <button type="submit" class="btn btn-primary pull-right" data-toggle="tooltip" title="<?php echo $wk_viewrma_msg; ?>"><i class="fa fa-envelope-o"></i></button>
                <div class="pull-left">
                  <a href="<?php echo $print; ?>" target="_blank" data-toggle="tooltip" class="btn btn-default" title="<?php echo $text_print; ?>"><i class="fa fa-print"></i></a>
                </div>
              </div>
            </div>
          </form>
      </fieldset>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<?php echo $footer; ?>
<script>
<?php if($viewrma['rma_status'] != 'Solved' AND $viewrma['rma_status'] != 'Canceled'){ ?>
jQuery(".submit-auth-no").click(function(){

  if(jQuery("#auth-no-text").val() != ""){
    jQuery("#auth-no-text").parent('div.form-group').removeClass('has-error');

    $('.submit-auth-no i').removeClass('fa-save').addClass('fa-spinner fa-spin');
    jQuery.ajax({
      url: "index.php?route=account/rma/rma/addcons",
      type: "POST",
      data: { auth_no : jQuery("#auth-no-text").val(),vid : "<?php echo $vid; ?>"},
      dataType: "json",
      success: function(content) {
        if(content['success']){
          jQuery(".auth_no_here").text(jQuery("#auth-no-text").val());
          jQuery("#add-auth-no").prepend("<span class='text-success'><?php echo $text_upload_success ; ?></span><br/>");
          $('#add-auth-no').slideUp(3000,function(){
            jQuery("#add-auth-no").remove();
          })
        }else{
          alert('<?php echo $wk_viewrma_valid_no ; ?>');
        }
        $('.submit-auth-no i').removeClass('fa-spinner fa-spin').addClass('fa-save');
      }
    });
  }else{
    jQuery("#auth-no-text").parent('div.form-group').addClass('has-error');
  }
});

$('.add-auth-no').on('click', function(){
  $('#add-auth-no').removeClass('hide');
})
$('.add-auth-no-close').on('click', function(){
  $('#add-auth-no').addClass('hide');
})

jQuery("#upload-file").ajaxfileupload({
    "action"     : "index.php?route=account/rma/viewrma/imageupload",
    "params"     : {id : '<?php echo $vid; ?>'},
    "onComplete" :  function(json) {
          $('ul.thumbnails').append(json);
          $('.upload-file-label i').removeClass('fa-spinner fa-spin').addClass('fa-picture-o');
        },
    "onStart"    :  function() {
          $('.upload-file-label i').removeClass('fa-picture-o').addClass('fa-spinner fa-spin');
        }
});
<?php } ?>
jQuery('input[name=up_file]').change(function(){
  $('#input-file-name').val(jQuery(this).val().replace(/C:\\fakepath\\/i, ''));
});

$(document).ready(function() {
  $('.thumbnails').magnificPopup({
    type:'image',
    delegate: 'a',
    gallery: {
      enabled:true
    }
  });
});
</script>
