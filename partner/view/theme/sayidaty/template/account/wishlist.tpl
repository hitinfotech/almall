<?php echo $header; ?>
<div class="content-padding">
<div class="container">
  
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"> 
    <?php 
          if(isset($menu) && !empty($menu)) {
            $class = 'col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-left'; 
          }else{
            $class = '';
          }
    ?> 
    <?php echo $menu;?>
    
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($products) { ?>
      
      
   <?php foreach ($products as $product) { ?> 
  <div class="wish-list-row">
  <div class="row">
   <div class="col-md-9 col-sm-9 col-xs-12">
              
      <div class="prod-wishlist">
        <span><?php echo $product['name']; ?></span>
          <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" class="prod-img"></a>
        <p class="f17 d-break-space"><strong><?php echo $price; ?></strong>: <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <b><?php echo $product['special']; ?></b> <s><?php echo $product['price']; ?></s>
                <?php } ?>
              </a>
              </p>
      </div>
   </div>
  
   <div class="col-md-3 col-sm-3 col-xs-12">
     <div class="text-center">
        <?php /*<button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" data-toggle="tooltip" title="<?php echo $button_cart; ?>" class="btn btn-primary"><i class="fa fa-shopping-bag"> <?php echo $shop_now; ?></i></button> */ ?>
        <a href="<?php echo $product['remove']; ?>" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-default btn-sm"><i class="fa fa-trash-o"> </i> </a>
       </div>
   </div>
  </div>
  </div>
      <?php } ?>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
  
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<?php echo $footer; ?>