<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-brand" data-toggle="tooltip" title="<?php echo $button_save; ?>"
                        class="btn btn-primary"><i class="fa fa-save"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-brand"
                      class="form-horizontal">

                    <div class="tab-content">
                      <div class="form-group">
                         <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                         <div class="col-sm-10">
                             <input type="file" name="image" value="" id="input-image" />
                         </div>
                     </div>
                        <!-- { nav-tabs } !-->
                        <ul class="nav nav-tabs" id="language">
                            <?php foreach ($languages as $language) { ?>
                            <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img
                                            src="../admin/view/image/flags/<?php echo $language['image']; ?>"
                                            title="<?php echo $language['name']; ?>"/> <?php echo $language['name']; ?>
                                </a></li>
                            <?php } ?>
                        </ul>
                        <!-- { / nav-tabs } !-->

                        <!-- { tab-content } !-->
                        <div class="tab-content">
                            <?php foreach ($languages as $language) { ?>
                            <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                <div class="form-group required">
                                    <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" name="brand_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($brand_description[$language['language_id']]) ? $brand_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                        <?php if (isset($error_name[$language['language_id']])) { ?>
                                        <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                    <div class="col-sm-10">
                                        <textarea name="brand_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($brand_description[$language['language_id']]) ? $brand_description[$language['language_id']]['description'] : ''; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- { / tab-content } !-->
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script>
<?php foreach ($languages as $language) { ?>
        // $("#input-description<?php echo $language['language_id']; ?>").summernote();
        CKEDITOR.replace("input-description<?php echo $language['language_id']; ?>", {
            filebrowserUploadUrl: "index.php?route=common/fileuploader/upload",
            filebrowserWindowWidth: 800,
            filebrowserWindowHeight: 500
        });
<?php } ?>
    $('#language a:first').tab('show');
</script>
