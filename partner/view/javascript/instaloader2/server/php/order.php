<?php

error_reporting(E_ALL | E_STRICT);
if (!defined(__DIR__)){
    define(__DIR__, dirname(__FILE__));
}
if (!defined('ENVIROMENT')){
    define('ENVIROMENT', getenv('ENVIROMENT'));
}
require __DIR__ . '/../../../../../../config/config.php';


$product_id = $_POST['product_id'];
$sort_order = $_POST['sort_order'] + 1;
$image = 'catalog/product-' . $product_id . '/' . str_replace(' ', '', trim($_POST['file_name'], ' '));


# Don't forget to update product table with the leading image
if (file_exists(DIR_IMAGE . $image) && trim($_POST['file_name']) != '') {
    if ($sort_order == 1) {
        $add_to_db = query("UPDATE `" . DB_PREFIX . "product` SET image = '$image' WHERE product_id = $product_id");
        query("DELETE FROM `" . DB_PREFIX . "product_image` WHERE image = '$image' AND product_id = $product_id");
    } else {
        $add_to_db = query("DELETE FROM `" . DB_PREFIX . "product_image` WHERE product_id = $product_id AND image = '$image'");
        $add_to_db = query("INSERT INTO  `" . DB_PREFIX . "product_image` SET sort_order = $sort_order, product_id = $product_id, image = '$image'");
    }
}

return $add_to_db;

function query($query) {
    $database = DB_DATABASE;
    $host = DB_HOSTNAME;
    $username = DB_USERNAME;
    $password = DB_PASSWORD;
    $link = mysqli_connect($host, $username, $password);
    if (!$link) {
        die(mysqli_error($link));
    }
    $db_selected = mysqli_select_db($link, $database);
    if (!$db_selected) {
        die(mysqli_error($link));
    }
    $result = mysqli_query($link, $query);
    mysqli_close($link);
    return $result;
}
