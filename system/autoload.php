<?php

// Load Algolia files
require_once DIR_SYSTEM . 'library/algolia/algoliasearch.php';

// Load AWS classes
require_once DIR_SYSTEM . 'library/aws_s3/S3.php';

require_once DIR_SYSTEM . 'library/aws/aws-autoloader.php';