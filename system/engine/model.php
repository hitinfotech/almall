<?php
/**
 * Class Model
 * @property ModelAccountActivity  $model_account_activity
 * @property ModelAccountAdd_shipping_mod  $model_account_add_shipping_mod
 * @property ModelAccountAddress  $model_account_address
 * @property ModelAccountApi  $model_account_api
 * @property ModelAccountBrand  $model_account_brand
 * @property ModelAccountCart  $model_account_cart
 * @property ModelAccountCategory  $model_account_category
 * @property ModelAccountCredit  $model_account_credit
 * @property ModelAccountCustom_field  $model_account_custom_field
 * @property ModelAccountCustomer  $model_account_customer
 * @property ModelAccountCustomer_group  $model_account_customer_group
 * @property ModelAccountCustomerpartner  $model_account_customerpartner
 * @property ModelAccountCustomerpartnerorder  $model_account_customerpartnerorder
 * @property ModelAccountDownload  $model_account_download
 * @property ModelAccountFavorites  $model_account_favorites
 * @property ModelAccountLanguage  $model_account_language
 * @property ModelAccountLook  $model_account_look
 * @property ModelAccountNewsletter  $model_account_newsletter
 * @property ModelAccountNewsletter_category  $model_account_newsletter_category
 * @property ModelAccountOrder  $model_account_order
 * @property ModelAccountRecurring  $model_account_recurring
 * @property ModelAccountReturn  $model_account_return
 * @property ModelAccountReward  $model_account_reward
 * @property ModelAccountRmaMail  $model_account_rma_mail
 * @property ModelAccountRmaRma  $model_account_rma_rma
 * @property ModelAccountSeller  $model_account_seller
 * @property ModelAccountTip  $model_account_tip
 * @property ModelAccountTracking  $model_account_tracking
 * @property ModelAccountTransaction  $model_account_transaction
 * @property ModelAccountWishlist  $model_account_wishlist
 * @property ModelAccountWk_mprma  $model_account_wk_mprma
 * @property ModelAccountWkcustomfield  $model_account_wkcustomfield
 * @property ModelAffiliateActivity  $model_affiliate_activity
 * @property ModelAffiliateAffiliate  $model_affiliate_affiliate
 * @property ModelAffiliateTransaction  $model_affiliate_transaction
 * @property ModelBrandBrand  $model_brand_brand
 * @property ModelCatalogCategory  $model_catalog_category
 * @property ModelCatalogFeeds  $model_catalog_feeds
 * @property ModelCatalogInformation  $model_catalog_information
 * @property ModelCatalogManufacturer  $model_catalog_manufacturer
 * @property ModelCatalogOffer  $model_catalog_offer
 * @property ModelCatalogProduct  $model_catalog_product
 * @property ModelCatalogReview  $model_catalog_review
 * @property ModelCheckoutMarketing  $model_checkout_marketing
 * @property ModelCheckoutOrder  $model_checkout_order
 * @property ModelCheckoutRecurring  $model_checkout_recurring
 * @property ModelCustomerpartnerDashboard  $model_customerpartner_dashboard
 * @property ModelCustomerpartnerHtmlfilter  $model_customerpartner_htmlfilter
 * @property ModelCustomerpartnerMail  $model_customerpartner_mail
 * @property ModelCustomerpartnerMaster  $model_customerpartner_master
 * @property ModelCustomerpartnerTransaction  $model_customerpartner_transaction
 * @property ModelDesignBanner  $model_design_banner
 * @property ModelDesignLayout  $model_design_layout
 * @property ModelExtensionExtension  $model_extension_extension
 * @property ModelExtensionModuleProductbundles  $model_extension_module_productbundles
 * @property ModelExtensionModule  $model_extension_module
 * @property ModelExtensionTotalProductbundlestotal  $model_extension_total_productbundlestotal
 * @property ModelExtensionTotalProductbundlestotal2  $model_extension_total_productbundlestotal2
 * @property ModelFeedGoogle_base  $model_feed_google_base
 * @property ModelFraudFraudlabspro  $model_fraud_fraudlabspro
 * @property ModelFraudIp  $model_fraud_ip
 * @property ModelFraudMaxmind  $model_fraud_maxmind
 * @property ModelHomeHome  $model_home_home
 * @property ModelLocalisationCountry  $model_localisation_country
 * @property ModelLocalisationCurrency  $model_localisation_currency
 * @property ModelLocalisationLanguage  $model_localisation_language
 * @property ModelLocalisationLength_class  $model_localisation_length_class
 * @property ModelLocalisationLocation  $model_localisation_location
 * @property ModelLocalisationOrder_status  $model_localisation_order_status
 * @property ModelLocalisationReturn_reason  $model_localisation_return_reason
 * @property ModelLocalisationReturnpolicy  $model_localisation_returnpolicy
 * @property ModelLocalisationStock_status  $model_localisation_stock_status
 * @property ModelLocalisationTax_class  $model_localisation_tax_class
 * @property ModelLocalisationWeight_class  $model_localisation_weight_class
 * @property ModelLocalisationZone  $model_localisation_zone
 * @property ModelLookLook  $model_look_look
 * @property ModelMallsMall  $model_malls_mall
 * @property ModelMallsNews  $model_malls_news
 * @property ModelMallsShop  $model_malls_shop
 * @property ModelModuleAbandonedcarts  $model_module_abandonedcarts
 * @property ModelModuleAbandonmentcarts  $model_module_abandonmentcarts
 * @property ModelModuleAutomatednewsletter  $model_module_automatednewsletter
 * @property ModelModuleNe  $model_module_ne
 * @property ModelModulePp_login  $model_module_pp_login
 * @property ModelModuleProductbundles  $model_module_productbundles
 * @property ModelModuleRemarkety  $model_module_remarkety
 * @property ModelModuleRemarkety_cron  $model_module_remarkety_cron
 * @property ModelModuleRemarkety_queue  $model_module_remarkety_queue
 * @property ModelNeAccount  $model_ne_account
 * @property ModelNeCampaign  $model_ne_campaign
 * @property ModelNeChunk  $model_ne_chunk
 * @property ModelNeCron  $model_ne_cron
 * @property ModelNeLanguage  $model_ne_language
 * @property ModelNeMarketing  $model_ne_marketing
 * @property ModelNeNewsletter  $model_ne_newsletter
 * @property ModelNeStats  $model_ne_stats
 * @property ModelNeTemplate  $model_ne_template
 * @property ModelNeTrack  $model_ne_track
 * @property ModelOpenbayAmazon_listing  $model_openbay_amazon_listing
 * @property ModelOpenbayAmazon_order  $model_openbay_amazon_order
 * @property ModelOpenbayAmazon_product  $model_openbay_amazon_product
 * @property ModelOpenbayAmazonus_listing  $model_openbay_amazonus_listing
 * @property ModelOpenbayAmazonus_order  $model_openbay_amazonus_order
 * @property ModelOpenbayAmazonus_product  $model_openbay_amazonus_product
 * @property ModelOpenbayEbay_openbay  $model_openbay_ebay_openbay
 * @property ModelOpenbayEbay_order  $model_openbay_ebay_order
 * @property ModelOpenbayEbay_product  $model_openbay_ebay_product
 * @property ModelOpenbayEtsy_order  $model_openbay_etsy_order
 * @property ModelOpenbayEtsy_product  $model_openbay_etsy_product
 * @property ModelPaymentAmazon_login_pay  $model_payment_amazon_login_pay
 * @property ModelPaymentAuthorizenet_aim  $model_payment_authorizenet_aim
 * @property ModelPaymentAuthorizenet_sim  $model_payment_authorizenet_sim
 * @property ModelPaymentBank_transfer  $model_payment_bank_transfer
 * @property ModelPaymentBluepay_hosted  $model_payment_bluepay_hosted
 * @property ModelPaymentBluepay_redirect  $model_payment_bluepay_redirect
 * @property ModelPaymentCheque  $model_payment_cheque
 * @property ModelPaymentCod  $model_payment_cod
 * @property ModelPaymentFirstdata  $model_payment_firstdata
 * @property ModelPaymentFirstdata_remote  $model_payment_firstdata_remote
 * @property ModelPaymentFree_checkout  $model_payment_free_checkout
 * @property ModelPaymentFreecoupon  $model_payment_freecoupon
 * @property ModelPaymentG2apay  $model_payment_g2apay
 * @property ModelPaymentGlobalpay  $model_payment_globalpay
 * @property ModelPaymentGlobalpay_remote  $model_payment_globalpay_remote
 * @property ModelPaymentKlarna_account  $model_payment_klarna_account
 * @property ModelPaymentKlarna_invoice  $model_payment_klarna_invoice
 * @property ModelPaymentLiqpay  $model_payment_liqpay
 * @property ModelPaymentNochex  $model_payment_nochex
 * @property ModelPaymentPayfort_fort  $model_payment_payfort_fort
 * @property ModelPaymentPayfort_fort_qpay  $model_payment_payfort_fort_qpay
 * @property ModelPaymentPayfort_fort_sadad  $model_payment_payfort_fort_sadad
 * @property ModelPaymentPaymate  $model_payment_paymate
 * @property ModelPaymentPaypoint  $model_payment_paypoint
 * @property ModelPaymentPaytabs  $model_payment_paytabs
 * @property ModelPaymentPayza  $model_payment_payza
 * @property ModelPaymentPerpetual_payments  $model_payment_perpetual_payments
 * @property ModelPaymentPp_express  $model_payment_pp_express
 * @property ModelPaymentPp_payflow  $model_payment_pp_payflow
 * @property ModelPaymentPp_payflow_iframe  $model_payment_pp_payflow_iframe
 * @property ModelPaymentPp_pro  $model_payment_pp_pro
 * @property ModelPaymentPp_pro_iframe  $model_payment_pp_pro_iframe
 * @property ModelPaymentPp_standard  $model_payment_pp_standard
 * @property ModelPaymentRealex  $model_payment_realex
 * @property ModelPaymentRealex_remote  $model_payment_realex_remote
 * @property ModelPaymentSagepay_direct  $model_payment_sagepay_direct
 * @property ModelPaymentSagepay_server  $model_payment_sagepay_server
 * @property ModelPaymentSagepay_us  $model_payment_sagepay_us
 * @property ModelPaymentSecuretrading_pp  $model_payment_securetrading_pp
 * @property ModelPaymentSecuretrading_ws  $model_payment_securetrading_ws
 * @property ModelPaymentSkrill  $model_payment_skrill
 * @property ModelPaymentTwocheckout  $model_payment_twocheckout
 * @property ModelPaymentWeb_payment_software  $model_payment_web_payment_software
 * @property ModelPaymentWorldpay  $model_payment_worldpay
 * @property ModelSettingApi  $model_setting_api
 * @property ModelSettingSetting  $model_setting_setting
 * @property ModelSettingStore  $model_setting_store
 * @property ModelShippingAuspost  $model_shipping_auspost
 * @property ModelShippingCitylink  $model_shipping_citylink
 * @property ModelShippingFedex  $model_shipping_fedex
 * @property ModelShippingFlat  $model_shipping_flat
 * @property ModelShippingFlatcust  $model_shipping_flatcust
 * @property ModelShippingFree  $model_shipping_free
 * @property ModelShippingItem  $model_shipping_item
 * @property ModelShippingParcelforce_48  $model_shipping_parcelforce_48
 * @property ModelShippingPickup  $model_shipping_pickup
 * @property ModelShippingRoyal_mail  $model_shipping_royal_mail
 * @property ModelShippingUps  $model_shipping_ups
 * @property ModelShippingUsps  $model_shipping_usps
 * @property ModelShippingWeight  $model_shipping_weight
 * @property ModelShippingWk_custom_shipping  $model_shipping_wk_custom_shipping
 * @property ModelTagsTags  $model_tags_tags
 * @property ModelTipsTips  $model_tips_tips
 * @property ModelToolImage  $model_tool_image
 * @property ModelToolOnline  $model_tool_online
 * @property ModelToolUpload  $model_tool_upload
 * @property ModelToppagesToppages  $model_toppages_toppages
 * @property ModelTotalCoupon  $model_total_coupon
 * @property ModelTotalCredit  $model_total_credit
 * @property ModelTotalHandling  $model_total_handling
 * @property ModelTotalKlarna_fee  $model_total_klarna_fee
 * @property ModelTotalLow_order_fee  $model_total_low_order_fee
 * @property ModelTotalProductbundlestotal  $model_total_productbundlestotal
 * @property ModelTotalProductbundlestotal2  $model_total_productbundlestotal2
 * @property ModelTotalReward  $model_total_reward
 * @property ModelTotalShipping  $model_total_shipping
 * @property ModelTotalSub_total  $model_total_sub_total
 * @property ModelTotalTax  $model_total_tax
 * @property ModelTotalTotal  $model_total_total
 * @property ModelTotalVoucher  $model_total_voucher
 * @property ModelTotalVoucher_theme  $model_total_voucher_theme
 * @property Customer $customer
 * @property Registry $registry
 * @property Language $language
 * @property Cache $cache
 * @property Loader $load
 * @property Config $config
 * @property DB $db
 * @property Request $request
 * @property Response $response
 * @property Session $session
 * @property Country $country
 * @property Currency $currency
 * @property ModelCatalogOption $model_catalog_option
 * @property Cart $cart
 */
abstract class Model {

    public $config_image;
    protected $registry;

    public function __construct($registry) {
        $this->registry = $registry;
        $this->config_image = config_image::getInstance();
    }

    public function __get($key) {
        return $this->registry->get($key);
    }

    public function __set($key, $value) {
        $this->registry->set($key, $value);
    }

    public function stripTagsStyle($input) {
        $input = preg_replace('#<h([1-6]).*?style="(.*?)".*?>(.*?)<\/h[1-6]>#si', '<p style="${2}">${3}</p>', $input);
        $input = preg_replace('/(width|height)="\d*"\s/', "", $input);
        $output = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $input);
        return $output;
    }

    private function create_path($dir_path) {
        $path = rtrim($dir_path, '/');
        $directories = explode("/", $path);
        $string = '';
        foreach ($directories as $directory) {
            $string .= "/{$directory}";
            if (!is_dir($string)) {
                exec("mkdir -m 777 {$string}");
                exec("chmod -R 777 {$string}");
            }
        }
    }

    public function upload_file($type, $field, $bjectID, $height = 2000, $width = 2000) {

        if (!isset($this->request->files[$field]) || is_array($this->request->files[$field]['name'])) {
            return FALSE;
        }

        $tempname = basename(html_entity_decode($this->request->files[$field]['name'], ENT_QUOTES, 'UTF-8'));

        // Allowed file extension types
        $allowed = array(
            'jpg',
            'jpeg',
            'gif',
            'png'
        );

        if (!in_array(utf8_strtolower(utf8_substr(strrchr($tempname, '.'), 1)), $allowed)) {
            die("extension {$tempname}");
            return FALSE;
        }

        // Allowed file mime types
        $allowed_mime = array(
            'image/jpeg',
            'image/pjpeg',
            'image/png',
            'image/x-png',
            'image/gif'
        );

        if (!in_array($this->request->files[$field]['type'], $allowed_mime)) {
            die("mime {$tempname} ");
            return FALSE;
        }


        //ini_set('memory_limit', '2M');   //  handle large images

        $path = ucfirst($type) . '/' . $bjectID . '/' . date('Y_m_d_h_i_s') . '/';

        $this->create_path(DIR_IMAGE . $path);

        $filename = time() . '.' . utf8_strtolower(utf8_substr(strrchr($tempname, '.'), 1));

        try {
            //die("HEEE");
            move_uploaded_file($this->request->files[$field]['tmp_name'], DIR_IMAGE . $path . 'tmp' . $filename);


            $im = new imagick(DIR_IMAGE . $path . 'tmp' . $filename);
            $imageprops = $im->getImageGeometry();
            $old_width = $imageprops['width'];
            $old_height = $imageprops['height'];
            if ($old_width > $old_height) {
                $newHeight = $height;
                $newWidth = ($height / $old_height) * $old_width;
            } else {
                $newWidth = $width;
                $newHeight = ($width / $old_width) * $old_height;
            }
            //echo $newWidth.'x'.$newHeight;die;
            $oldPath = DIR_IMAGE . $path . 'tmp' . $filename;
            $newPath = DIR_IMAGE . $path . $filename;
            exec(" convert " . $oldPath . " -background white -density 72 -resize " . $newWidth . "x" . $newHeight . " -quality 100 " . $newPath);
            unlink(DIR_IMAGE . $path . 'tmp' . $filename);

            /*$im = new imagick(DIR_IMAGE . $path . 'tmp' . $filename);
            $imageprops = $im->getImageGeometry();
            $old_width = $imageprops['width'];
            $old_height = $imageprops['height'];
            if ($old_width > $old_height) {
                $newHeight = $height;
                $newWidth = ($height / $old_height) * $old_width;
            } else {
                $newWidth = $width;
                $newHeight = ($width / $old_width) * $old_height;
            }
            $im->setCompressionQuality(100);
            // a value between 1 and 100, 1 = high compression, 100 low compression

            $im->resizeimage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1, true);
            $im->cropImage($newWidth, $newHeight, 0, 0);
            $im->writeImage(DIR_IMAGE . $path . $filename);
            unlink(DIR_IMAGE . $path . 'tmp' . $filename);//*/

            return $path . $filename;
        } catch (Exception $error) {
            return FALSE;
        }
    }

    protected function upload_images($type, $field, $bjectID, $height = 2000, $width = 2000) {

        if (!isset($this->request->files[$field]) || !is_array($this->request->files[$field]['name'])) {
            return FALSE;
        }

        $row_image = '';

        $result = array();
        foreach ($this->request->files[$field]['name'] as $key => $value) {
            $tempname = basename(html_entity_decode($this->request->files[$field]['name'][$key]['image'], ENT_QUOTES, 'UTF-8'));

            // Allowed file extension types
            $allowed = array(
                'jpg',
                'jpeg',
                'gif',
                'png'
            );

            if (!in_array(utf8_strtolower(utf8_substr(strrchr($tempname, '.'), 1)), $allowed)) {
                $row_image = FALSE;
            }

            // Allowed file mime types
            $allowed_mime = array(
                'image/jpeg',
                'image/pjpeg',
                'image/png',
                'image/x-png',
                'image/gif'
            );

            if (!in_array($this->request->files[$field]['type'][$key]['image'], $allowed_mime)) {
                $row_image = FALSE;
            }


            ini_set('memory_limit', '2M');   //  handle large images

            $path = ucfirst($type) . '/' . $bjectID . '/' . date('Y_m_d_h_i_s') . '/';
            $this->create_path(DIR_IMAGE . $path);
            $filename = time() . '_' . $key . '.' . utf8_strtolower(utf8_substr(strrchr($tempname, '.'), 1));

            try {
                move_uploaded_file($this->request->files[$field]['tmp_name'][$key]['image'], DIR_IMAGE . $path . 'tmp' . $filename);


                $im = new imagick(DIR_IMAGE . $path . 'tmp' . $filename);
                $imageprops = $im->getImageGeometry();
                $old_width = $imageprops['width'];
                $old_height = $imageprops['height'];
                if ($old_width > $old_height) {
                    $newHeight = $height;
                    $newWidth = ($height / $old_height) * $old_width;
                } else {
                    $newWidth = $width;
                    $newHeight = ($width / $old_width) * $old_height;
                }
                //echo $newWidth.'x'.$newHeight;die;
                $oldPath = DIR_IMAGE . $path . 'tmp' . $filename;
                $newPath = DIR_IMAGE . $path . $filename;
                exec(" convert " . $oldPath . " -background white -density 72 -resize " . $newWidth . "x" . $newHeight . " -quality 100 " . $newPath);
                unlink( DIR_IMAGE . $path . 'tmp' . $filename);


                /* $im = new imagick(DIR_IMAGE . $path . 'tmp' . $filename);
                 $imageprops = $im->getImageGeometry();
                 $old_width = $imageprops['width'];
                 $old_height = $imageprops['height'];
                 if ($old_width > $old_height) {
                     $newHeight = $height;
                     $newWidth = ($height / $old_height) * $old_width;
                 } else {
                     $newWidth = $width;
                     $newHeight = ($width / $old_width) * $old_height;
                 }

                 $im->resizeimage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1, true);
                 $im->cropImage($newWidth, $newHeight, 0, 0);
                 $im->writeImage(DIR_IMAGE . $path . $filename);

                 unlink(DIR_IMAGE . $path . 'tmp' . $filename);*/

                $result[$key] = $path . $filename;
            } catch (Exception $error) {
                $row_image = FALSE;
            }
        }
        return $result;
    }

    protected function upload_images_lan($type, $field, $bjectID, $height = 2000, $width = 2000) {

        if (!isset($this->request->files[$field]) || !is_array($this->request->files[$field]['name'])) {
            return FALSE;
        }

        $row_image = '';

        $result = array();

        foreach ($this->request->files[$field]['name'] as $key => $row) {
            foreach ($row as $language_id => $value) {

                $tempname = basename(html_entity_decode($this->request->files[$field]['name'][$key][$language_id]['image_text'], ENT_QUOTES, 'UTF-8'));
                // Allowed file extension types
                $allowed = array(
                    'jpg',
                    'jpeg',
                    'gif',
                    'png'
                );

                if (!in_array(utf8_strtolower(utf8_substr(strrchr($tempname, '.'), 1)), $allowed)) {
                    $row_image = FALSE;
                }

                // Allowed file mime types
                $allowed_mime = array(
                    'image/jpeg',
                    'image/pjpeg',
                    'image/png',
                    'image/x-png',
                    'image/gif'
                );

                if (!in_array($this->request->files[$field]['type'][$key][$language_id]['image_text'], $allowed_mime)) {
                    $row_image = FALSE;
                }


                ini_set('memory_limit', '2M');   //  handle large images

                $path = ucfirst($type) . '/' . $bjectID . '/' . date('Y_m_d_h_i_s') . '/';
                $this->create_path(DIR_IMAGE . $path);
                $filename = time() . '_' . $key . '.' . utf8_strtolower(utf8_substr(strrchr($tempname, '.'), 1));

                try {
                    move_uploaded_file($this->request->files[$field]['tmp_name'][$key][$language_id]['image_text'], DIR_IMAGE . $path . 'tmp' . $filename);




                   /* $im = new imagick(DIR_IMAGE . $path . 'tmp' . $filename);
                    $imageprops = $im->getImageGeometry();
                    $old_width = $imageprops['width'];
                    $old_height = $imageprops['height'];
                    if ($old_width > $old_height) {
                        $newHeight = $height;
                        $newWidth = ($height / $old_height) * $old_width;
                    } else {
                        $newWidth = $width;
                        $newHeight = ($width / $old_width) * $old_height;
                    }

                    $im->resizeimage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1, true);
                    $im->cropImage($newWidth, $newHeight, 0, 0);
                    $im->writeImage(DIR_IMAGE . $path . $filename);

                    unlink(DIR_IMAGE . $path . 'tmp' . $filename);
                    */
                    $im = new imagick(DIR_IMAGE . $path . 'tmp' . $filename);
                    $imageprops = $im->getImageGeometry();
                    $old_width = $imageprops['width'];
                    $old_height = $imageprops['height'];
                    if ($old_width > $old_height) {
                        $newHeight = $height;
                        $newWidth = ($height / $old_height) * $old_width;
                    } else {
                        $newWidth = $width;
                        $newHeight = ($width / $old_width) * $old_height;
                    }
                    //echo $newWidth.'x'.$newHeight;die;
                    $oldPath = DIR_IMAGE . $path . 'tmp' . $filename;
                    $newPath = DIR_IMAGE . $path . $filename;
                    exec(" convert " . $oldPath . " -background white -density 72 -resize " . $newWidth . "x" . $newHeight . " -quality 100 " . $newPath);
                    unlink(DIR_IMAGE . $path . 'tmp' . $filename);

                   $result[$language_id][$key] = $path . $filename;
                } catch (Exception $error) {
                    $row_image = FALSE;
                }
            }
        }
        return $result;
    }

    protected function upload_xls_file($type, $field, $bjectID, $height = 2000, $width = 2000) {

        if (!isset($this->request->files[$field]) || is_array($this->request->files[$field]['name'])) {
            return FALSE;
        }

        $tempname = basename(html_entity_decode($this->request->files[$field]['name'], ENT_QUOTES, 'UTF-8'));

        // Allowed file extension types
        $allowed = array(
            'xls'
        );

        if (!in_array(utf8_strtolower(utf8_substr(strrchr($tempname, '.'), 1)), $allowed)) {
            die("extension {$tempname}");
            return FALSE;
        }

        // Allowed file mime types
        $allowed_mime = array(
            'application/vnd.ms-excel',
            'application/octet-stream',
        );

        if (!in_array($this->request->files[$field]['type'], $allowed_mime)) {
            die("mime {$tempname} ");
            return FALSE;
        }


        //ini_set('memory_limit', '2M');   //  handle large images

        $path = ucfirst($type) . '/' . $bjectID . '/' . date('Y_m_d_h_i_s') . '/';

        $this->create_path(DIR_IMAGE . $path);

        $filename = time() . '.' . utf8_strtolower(utf8_substr(strrchr($tempname, '.'), 1));

        try {
            move_uploaded_file($this->request->files[$field]['tmp_name'], DIR_IMAGE . $path  . $filename);
            return $path . $filename;
        } catch (Exception $error) {
            return FALSE;
        }
    }

}
