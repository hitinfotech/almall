<?php
/**
 * Class Model
 * @property ModelAccountActivity  $model_account_activity
 * @property ModelAccountAdd_shipping_mod  $model_account_add_shipping_mod
 * @property ModelAccountAddress  $model_account_address
 * @property ModelAccountApi  $model_account_api
 * @property ModelAccountBrand  $model_account_brand
 * @property ModelAccountCart  $model_account_cart
 * @property ModelAccountCategory  $model_account_category
 * @property ModelAccountCredit  $model_account_credit
 * @property ModelAccountCustom_field  $model_account_custom_field
 * @property ModelAccountCustomer  $model_account_customer
 * @property ModelAccountCustomer_group  $model_account_customer_group
 * @property ModelAccountCustomerpartner  $model_account_customerpartner
 * @property ModelAccountCustomerpartnerorder  $model_account_customerpartnerorder
 * @property ModelAccountDownload  $model_account_download
 * @property ModelAccountFavorites  $model_account_favorites
 * @property ModelAccountLanguage  $model_account_language
 * @property ModelAccountLook  $model_account_look
 * @property ModelAccountNewsletter  $model_account_newsletter
 * @property ModelAccountNewsletter_category  $model_account_newsletter_category
 * @property ModelAccountOrder  $model_account_order
 * @property ModelAccountRecurring  $model_account_recurring
 * @property ModelLocalisationRmaQuantity $model_localisation_rmaquantity
 * @property ModelAccountReturn  $model_account_return
 * @property ModelAccountReward  $model_account_reward
 * @property ModelAccountRmaMail  $model_account_rma_mail
 * @property ModelAccountRmaRma  $model_account_rma_rma
 * @property ModelAccountSeller  $model_account_seller
 * @property ModelAccountTip  $model_account_tip
 * @property ModelCustomerpartnerOrder $model_customerpartner_order
 * @property Log $log ErrorLogs
 * @property ModelLocalisationOrderStatus $model_localisation_order_status
 * @property ModelAccountTracking  $model_account_tracking
 * @property ModelAccountTransaction  $model_account_transaction
 * @property ModelAccountWishlist  $model_account_wishlist
 * @property ModelAccountWk_mprma  $model_account_wk_mprma
 * @property ModelAccountWkcustomfield  $model_account_wkcustomfield
 * @property ModelAffiliateActivity  $model_affiliate_activity
 * @property ModelAffiliateAffiliate  $model_affiliate_affiliate
 * @property ModelAffiliateTransaction  $model_affiliate_transaction
 * @property ModelBrandBrand  $model_brand_brand
 * @property ModelCatalogCategory  $model_catalog_category
 * @property ModelCatalogFeeds  $model_catalog_feeds
 * @property ModelCatalogInformation  $model_catalog_information
 * @property ModelCatalogManufacturer  $model_catalog_manufacturer
 * @property ModelCatalogOffer  $model_catalog_offer
 * @property ModelCatalogProduct  $model_catalog_product
 * @property ModelCatalogReview  $model_catalog_review
 * @property ModelCheckoutMarketing  $model_checkout_marketing
 * @property ModelCheckoutOrder  $model_checkout_order
 * @property ModelCheckoutRecurring  $model_checkout_recurring
 * @property ModelCustomerpartnerDashboard  $model_customerpartner_dashboard
 * @property ModelCustomerpartnerHtmlfilter  $model_customerpartner_htmlfilter
 * @property ModelCustomerpartnerMail  $model_customerpartner_mail
 * @property ModelCustomerpartnerMaster  $model_customerpartner_master
 * @property ModelCustomerpartnerTransaction  $model_customerpartner_transaction
 * @property ModelDesignBanner  $model_design_banner
 * @property ModelDesignLayout  $model_design_layout
 * @property ModelExtensionExtension  $model_extension_extension
 * @property ModelExtensionModuleProductbundles  $model_extension_module_productbundles
 * @property ModelExtensionModule  $model_extension_module
 * @property ModelExtensionTotalProductbundlestotal  $model_extension_total_productbundlestotal
 * @property ModelExtensionTotalProductbundlestotal2  $model_extension_total_productbundlestotal2
 * @property ModelFeedGoogle_base  $model_feed_google_base
 * @property ModelFraudFraudlabspro  $model_fraud_fraudlabspro
 * @property ModelFraudIp  $model_fraud_ip
 * @property ModelFraudMaxmind  $model_fraud_maxmind
 * @property ModelHomeHome  $model_home_home
 * @property ModelLocalisationCountry  $model_localisation_country
 * @property ModelLocalisationCurrency  $model_localisation_currency
 * @property ModelLocalisationLanguage  $model_localisation_language
 * @property ModelLocalisationLength_class  $model_localisation_length_class
 * @property ModelLocalisationLocation  $model_localisation_location
 * @property ModelLocalisationReturn_reason  $model_localisation_return_reason
 * @property ModelLocalisationReturnpolicy  $model_localisation_returnpolicy
 * @property ModelLocalisationStock_status  $model_localisation_stock_status
 * @property ModelLocalisationTax_class  $model_localisation_tax_class
 * @property ModelLocalisationWeight_class  $model_localisation_weight_class
 * @property ModelLocalisationZone  $model_localisation_zone
 * @property ModelLookLook  $model_look_look
 * @property ModelMallsMall  $model_malls_mall
 * @property ModelMallsNews  $model_malls_news
 * @property ModelMallsShop  $model_malls_shop
 * @property ModelModuleAbandonedcarts  $model_module_abandonedcarts
 * @property ModelModuleAbandonmentcarts  $model_module_abandonmentcarts
 * @property ModelModuleAutomatednewsletter  $model_module_automatednewsletter
 * @property ModelModuleNe  $model_module_ne
 * @property ModelModulePp_login  $model_module_pp_login
 * @property ModelModuleProductbundles  $model_module_productbundles
 * @property ModelModuleRemarkety  $model_module_remarkety
 * @property ModelModuleRemarkety_cron  $model_module_remarkety_cron
 * @property ModelModuleRemarkety_queue  $model_module_remarkety_queue
 * @property ModelNeAccount  $model_ne_account
 * @property ModelNeCampaign  $model_ne_campaign
 * @property ModelNeChunk  $model_ne_chunk
 * @property ModelNeCron  $model_ne_cron
 * @property ModelNeLanguage  $model_ne_language
 * @property ModelNeMarketing  $model_ne_marketing
 * @property ModelNeNewsletter  $model_ne_newsletter
 * @property ModelNeStats  $model_ne_stats
 * @property ModelNeTemplate  $model_ne_template
 * @property ModelNeTrack  $model_ne_track
 * @property ModelOpenbayAmazon_listing  $model_openbay_amazon_listing
 * @property ModelOpenbayAmazon_order  $model_openbay_amazon_order
 * @property ModelOpenbayAmazon_product  $model_openbay_amazon_product
 * @property ModelOpenbayAmazonus_listing  $model_openbay_amazonus_listing
 * @property ModelOpenbayAmazonus_order  $model_openbay_amazonus_order
 * @property ModelOpenbayAmazonus_product  $model_openbay_amazonus_product
 * @property ModelOpenbayEbay_openbay  $model_openbay_ebay_openbay
 * @property ModelOpenbayEbay_order  $model_openbay_ebay_order
 * @property ModelOpenbayEbay_product  $model_openbay_ebay_product
 * @property ModelOpenbayEtsy_order  $model_openbay_etsy_order
 * @property ModelOpenbayEtsy_product  $model_openbay_etsy_product
 * @property ModelPaymentAmazon_login_pay  $model_payment_amazon_login_pay
 * @property ModelPaymentAuthorizenet_aim  $model_payment_authorizenet_aim
 * @property ModelPaymentAuthorizenet_sim  $model_payment_authorizenet_sim
 * @property ModelPaymentBank_transfer  $model_payment_bank_transfer
 * @property ModelPaymentBluepay_hosted  $model_payment_bluepay_hosted
 * @property ModelPaymentBluepay_redirect  $model_payment_bluepay_redirect
 * @property ModelPaymentCheque  $model_payment_cheque
 * @property ModelPaymentCod  $model_payment_cod
 * @property ModelPaymentFirstdata  $model_payment_firstdata
 * @property ModelPaymentFirstdata_remote  $model_payment_firstdata_remote
 * @property ModelPaymentFree_checkout  $model_payment_free_checkout
 * @property ModelPaymentFreecoupon  $model_payment_freecoupon
 * @property ModelPaymentG2apay  $model_payment_g2apay
 * @property ModelPaymentGlobalpay  $model_payment_globalpay
 * @property ModelPaymentGlobalpay_remote  $model_payment_globalpay_remote
 * @property ModelPaymentKlarna_account  $model_payment_klarna_account
 * @property ModelPaymentKlarna_invoice  $model_payment_klarna_invoice
 * @property ModelPaymentLiqpay  $model_payment_liqpay
 * @property ModelPaymentNochex  $model_payment_nochex
 * @property ModelPaymentPayfort_fort  $model_payment_payfort_fort
 * @property ModelPaymentPayfort_fort_qpay  $model_payment_payfort_fort_qpay
 * @property ModelPaymentPayfort_fort_sadad  $model_payment_payfort_fort_sadad
 * @property ModelPaymentPaymate  $model_payment_paymate
 * @property ModelPaymentPaypoint  $model_payment_paypoint
 * @property ModelPaymentPaytabs  $model_payment_paytabs
 * @property ModelPaymentPayza  $model_payment_payza
 * @property ModelPaymentPerpetual_payments  $model_payment_perpetual_payments
 * @property ModelPaymentPp_express  $model_payment_pp_express
 * @property ModelPaymentPp_payflow  $model_payment_pp_payflow
 * @property ModelPaymentPp_payflow_iframe  $model_payment_pp_payflow_iframe
 * @property ModelPaymentPp_pro  $model_payment_pp_pro
 * @property ModelPaymentPp_pro_iframe  $model_payment_pp_pro_iframe
 * @property ModelPaymentPp_standard  $model_payment_pp_standard
 * @property ModelPaymentRealex  $model_payment_realex
 * @property ModelPaymentRealex_remote  $model_payment_realex_remote
 * @property ModelPaymentSagepay_direct  $model_payment_sagepay_direct
 * @property ModelPaymentSagepay_server  $model_payment_sagepay_server
 * @property ModelPaymentSagepay_us  $model_payment_sagepay_us
 * @property ModelPaymentSecuretrading_pp  $model_payment_securetrading_pp
 * @property ModelPaymentSecuretrading_ws  $model_payment_securetrading_ws
 * @property ModelPaymentSkrill  $model_payment_skrill
 * @property ModelPaymentTwocheckout  $model_payment_twocheckout
 * @property ModelPaymentWeb_payment_software  $model_payment_web_payment_software
 * @property ModelPaymentWorldpay  $model_payment_worldpay
 * @property ModelSettingApi  $model_setting_api
 * @property ModelSettingSetting  $model_setting_setting
 * @property ModelSettingStore  $model_setting_store
 * @property ModelShippingAuspost  $model_shipping_auspost
 * @property ModelShippingCitylink  $model_shipping_citylink
 * @property ModelShippingFedex  $model_shipping_fedex
 * @property ModelShippingFlat  $model_shipping_flat
 * @property ModelShippingFlatcust  $model_shipping_flatcust
 * @property ModelShippingFree  $model_shipping_free
 * @property ModelShippingItem  $model_shipping_item
 * @property ModelShippingParcelforce_48  $model_shipping_parcelforce_48
 * @property ModelShippingPickup  $model_shipping_pickup
 * @property ModelShippingRoyal_mail  $model_shipping_royal_mail
 * @property ModelShippingUps  $model_shipping_ups
 * @property ModelShippingUsps  $model_shipping_usps
 * @property ModelShippingWeight  $model_shipping_weight
 * @property ModelShippingWk_custom_shipping  $model_shipping_wk_custom_shipping
 * @property ModelTagsTags  $model_tags_tags
 * @property ModelTipsTips  $model_tips_tips
 * @property ModelToolImage  $model_tool_image
 * @property ModelToolOnline  $model_tool_online
 * @property ModelToolUpload  $model_tool_upload
 * @property ModelToppagesToppages  $model_toppages_toppages
 * @property ModelTotalCoupon  $model_total_coupon
 * @property ModelTotalCredit  $model_total_credit
 * @property ModelTotalHandling  $model_total_handling
 * @property ModelTotalKlarna_fee  $model_total_klarna_fee
 * @property ModelTotalLow_order_fee  $model_total_low_order_fee
 * @property ModelTotalProductbundlestotal  $model_total_productbundlestotal
 * @property ModelTotalProductbundlestotal2  $model_total_productbundlestotal2
 * @property ModelTotalReward  $model_total_reward
 * @property ModelTotalShipping  $model_total_shipping
 * @property ModelTotalSub_total  $model_total_sub_total
 * @property ModelTotalTax  $model_total_tax
 * @property ModelTotalTotal  $model_total_total
 * @property ModelCatalogRmaMail $model_catalog_rma_mail
 * @property ModelTotalVoucher  $model_total_voucher
 * @property ModelTotalVoucher_theme  $model_total_voucher_theme
 * @property Customer $customer
 * @property Cart $cart
 * @property Admin_Cart $admin_name
 * @property Registry $registry
 * @property Language $language
 * @property Cache $cache
 * @property TsHelper $TsHelper 
 * @property TsLoader $TsLoader
 * @property Loader $load
 * @property Config $config
 * @property Currency $currency Description
 * @property DB $db
 * @property Request $request
 * @property Response $response
 * @property Session $session
 * @property Country $country
 * @property FacebookLibrary $facebook 
 * @property Document $document
 * @property Gtm $gtm GoogleTagManager
 * @property User $user
 * @property ModelCatalogSearch $model_catalog_search Description
 * @property DynamicValues $dynamic_values
 */
abstract class Controller {

    protected $registry;
    public $config_image;

    public function __construct($registry) {
        $this->registry = $registry;
        $this->config_image = config_image::getInstance();
    }

    public function __get($key) {
        return $this->registry->get($key);
    }

    public function __set($key, $value) {
        $this->registry->set($key, $value);
    }

    public function is_mobile() {
        // Client Device Flag
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $useragent = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                return true;
            } else {
                return false;
            }
        } else {
            return FALSE;
        }
    }

}
