<?php
$_['automatednewsletter_name']           		= 'AutomatedNewsletter';
$_['automatednewsletter_name_small']     		= 'automatednewsletter';
$_['automatednewsletter_model']          		= 'model_module_automatednewsletter';
$_['automatednewsletter_path']          		= 'module/automatednewsletter';
$_['automatednewsletter_version']        		= '2.5';
$_['automatednewsletter_module_data']    		= 'automatednewsletter_module';
$_['automatednewsletter_link']           		= 'extension/module';
$_['automatednewsletter_link_params']    		= '&type=module';
