<?php

$_['abandonmentcarts_name'] = 'abandonmentcarts';
$_['abandonmentcarts_model'] = 'model_module_abandonmentcarts';
$_['abandonmentcarts_path'] = 'module/abandonmentcarts';
$_['abandonmentcarts_version'] = '5.3';

$_['abandonmentcarts_link'] = 'extension/module';
$_['abandonmentcarts_link_params'] = '&type=module';
