<?php
$_['productbundles_name']               = 'productbundles';
$_['productbundles_model']              = 'model_module_productbundles';
$_['productbundles_path']               = 'module/productbundles';
$_['productbundles_version']            = '4.1';

$_['productbundles_link']               = 'extension/module';
$_['productbundles_link_params']        = '&type=module';

$_['productbundlestotal_name']          = 'productbundlestotal';
$_['productbundlestotal_path']          = 'total/productbundlestotal';

$_['productbundlestotal2_name']         = 'productbundlestotal2';
$_['productbundlestotal2_path']         = 'total/productbundlestotal2';

$_['productbundlestotal_link']          = 'extension/module';
$_['productbundlestotal_link_params']   = '&type=total';

$_['productbundles_token_string']       = 'token';

$_['productbundles_status_group']       = 'module_productbundles';
$_['productbundles_status_value']       = 'module_productbundles_status';

$_['productbundles_vendor_folder']      = 'vendors';