<?php
use Pixers\SalesManagoAPI\Service\EventService;

/**
 * Class DynamicValues
 */


class SalesManago{




    public static $API_URL = "http://app2.salesmanago.pl/api/";
    public static $apiKey = "zrzogy5wktyuoiqf";//@TODO need to check it before
    public static $clientId = "zrzogy5wktyuoiqf";
    public static $apiSecret = "0s46cglxvn9igk17pid0yk4setvvm7r3";
    public static $ownerEmail = "said.ouchnak@sayidaty.net";
    public static $facebookApp = "1225705760807367";

    /**
     *   PURCHASE, CART, VISIT, PHONE_CALL, OTHER, RESERVATION, CANCELLED, ACTIVATION, MEETING, OFFER, DOWNLOAD, LOGIN, TRANSACTION
     */

    const EVENT_PURCHASE = 'PURCHASE';
    const EVENT_CART = 'CART';
    const EVENT_VISIT = "VISIT";
    const EVENT_PHONE_CALL = "PHONE_CALL";
    const EVENT_OTHER = "OTHER";
    const EVENT_RESERVATION = "RESERVATION";
    const EVENT_CANCELLED = "CANCELLED";
    const EVENT_ACTIVATION = "ACTIVATION";
    const EVENT_MEETING = "MEETING";
    const EVENT_OFFER = "OFFER";
    const EVENT_DOWNLOAD = "DOWNLOAD";
    const EVENT_LOGIN = "LOGIN";
    const EVENT_TRANSACTION = "TRANSACTION";



    /**
     * @var \Pixers\SalesManagoAPI\Client $client
     */
    public $client = null;
    /**
     * @var Pixers\SalesManagoAPI\SalesManago $salesManago
     */
    public $salesManago = null;



    /** @var Loader $loader  */
    public $loader;
    /** @var ModelToolImage $image_tool */
    public $image_tool;
    /** @var DynamicValues $dynamic_values */
    public $dynamic_values;

    /** @var Config $config  */
    public $config;
    private static $instance = null;

    /**
     * @param Registry $registry
     * @return SalesManago
     */
    public static function getInstance($registry = null){
                if(SalesManago::$instance == null){
                    SalesManago::$instance = new SalesManago($registry);
                }
        return  SalesManago::$instance;
    }

    /**
     * DynamicValues constructor.
     * @param Registry $registry
     */
    private function __construct($registry)
    {
        $this->client = new \Pixers\SalesManagoAPI\Client(SalesManago::$clientId, SalesManago::$API_URL, SalesManago::$apiSecret, SalesManago::$apiKey);
        $this->salesManago = new Pixers\SalesManagoAPI\SalesManago($this->client);

        if($registry !=null){
            //$this->config = $registry->get('config');
            //$this->db = $registry->get('db');
            //$this->cache = $registry->get('cache');
            $this->dynamic_values = $registry->get('dynamic_values');
            $this->loader = $registry->get('load');
            $this->loader->model('tool/image');
            $this->image_tool = $registry->get('model_tool_image');
            $this->config = $registry->get('config');

        }
    }


    public static function headerCss(){
        $image_tool = SalesManago::getInstance()->image_tool;
        $dynamic_values = SalesManago::getInstance()->dynamic_values;
        $config = SalesManago::getInstance()->config;
        $url = $image_tool->resize($dynamic_values->get('salesmanago', 'popup_image', $config->get('config_language_id')), 400, 400);
        $title = trim($dynamic_values->get('salesmanago','popup_title',$config->get('config_language_id')));
        $description = trim($dynamic_values->get('salesmanago','popup_description',$config->get('config_language_id')));



        $headerCSS = <<<CSS
    .sm-webpush img{
        /*width:300px!important;*/
        /*height: 100%!important;*/
        object-fit: contain;
	content: url($url)!important;
        /*width: 50%!important;*/
    }
    .sm-webpush p:first-child {
       text-indent: -9999px;
	    line-height:0;
        
    }
    .sm-webpush p:first-child:before {
        content: '$title';
        text-indent:0;
        display:block;
	    line-height: initial;
        margin-right: 10px;
        margin-left: 10px;
    }

    .sm-webpush p:last-child {
        text-indent: -9999px;
	    line-height:0;
        
    }
    .sm-webpush p:last-child:before{
        content:'$description';
        text-indent:0;
        display:block;
	    line-height: initial;
        margin-right: 10px;
        margin-left: 10px;
    }
    #web-push-footer{
     display: none!important;
    }
CSS;
        return $headerCSS;
    }
    public static function footerScript(){




        $script = "<script type=\"text/javascript\">
    var _smFbAppId = '".SalesManago::$facebookApp."';
    var _smid = '".SalesManago::$clientId."';
    (function(w, r, a, sm, s ) {
        w['SalesmanagoObject'] = r;
        w[r] = w[r] || function () {( w[r].q = w[r].q || [] ).push(arguments)};
        sm = document.createElement('script');
        sm.type = 'text/javascript'; sm.async = true; sm.src = a;
        s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(sm, s);
    })(window, 'sm', ('https:' == document.location.protocol ? 'https://' : 'http://')
        + 'app2.salesmanago.pl/static/sm.js');
        
        
        sm('webPush', ['8e85f8dd-ebbf-48d0-984c-83e3369f54af', '24']);
</script>

<script src=\"https://app2.salesmanago.pl/dynamic/".SalesManago::$clientId."/popups.js\"></script>
";




        $dynamic_values = SalesManago::getInstance()->dynamic_values;
        $config = SalesManago::getInstance()->config;


        $yes = trim($dynamic_values->get('salesmanago','popup_yes',$config->get('config_language_id')));
        $no = trim($dynamic_values->get('salesmanago','popup_no',$config->get('config_language_id')));


        $lang = $config->get('config_language_id');
        $script .= " <script> ";
        $script .=<<<JS
       
$(function(){
        var interval = setInterval(function(){
            if($(".sm-webpush").find("#smwpConfirmationButton").text().trim() == "Yes"){
                $(".sm-webpush").find('p:first-child').html("");
                $(".sm-webpush").find('p:last-child').html("");
                $(".sm-webpush").find('#smwpConfirmationButton').html("$yes");
                $(".sm-webpush").find("#smwpRejectionButton").html("$no");
                
                if($lang == 2){
                    $(".sm-webpush").find('#smwpConfirmationButton').parent().css('text-align','left');
                }
                
                clearInterval(interval);
            }
          
        },1000);
    });
JS;

        $script .= "\r\n</script>\r\n";
        return $script;
    }


    public function getContactList($owner,$page=0,$size=1000){
            return $this->client->doPost('contact/paginatedContactList', ['owner'=>$owner,'page'=>$page,'size'=>$size]);
    }

    /**
   $data =  [
    "contact" => [
    "email" => "konrad-test-1@konri.com",
    "fax" => "+48345543345",
    "name" => "Konrad Test",
    "phone" => "+48123321123",
    "company" => "Benhauer",
    "state" => "PROSPECT",
    "address"=>[
    "streetAddress"=>"Brzyczyńska 123",
    "zipCode"=>"43-305",
    "city"=>"Bielsko-Biała",
    "country"=>"PL"
    ]
    ],
    "owner" => "admin@vendor.pl",
    "forceOptOut" => false,
    "forcePhoneOptOut" => false,
    "tags" => [ "API",
    "ADmanago"
    ],
    "properties"=>["custom.nickname"=>"Konri","custom.sex"=>"M"],
    "birthday"=> "19801017",
    "useApiDoubleOptIn"=>true,
    "lang"=>"PL"
    ]
     */
    public function registerUser(array $data){
            if(empty($data['contact']['email'])){
                return; //NO VALID EMAIL
            }
            try{
                /** @var Pixers\SalesManagoAPI\Service\ContactService $contactService */
                $contactService = $this->salesManago->getContactService();
                $isExist = $contactService->has(SalesManago::$ownerEmail,$data['contact']['email']);
                if(!empty($isExist) && !empty($isExist->result) && $isExist->result){
                    $contactService->update(SalesManago::$ownerEmail,$data['contact']['email'],$data);
                }else{
                    $contactService->create(SalesManago::$ownerEmail,$data);
                }

            }catch (Exception $e){

            }

    }



    private function eventTrigger($email,$event,array $value){
        if(empty($email) || $email == false){
            return false;
        }



        if(!empty($_GET['utm_source'])){
            $value['detail2'] =  "utm_source=".$_GET['utm_source'];
        }else{
            if(!empty($_SERVER['HTTP_REFERER'])){
                if ( $parts = parse_url( $_SERVER['HTTP_REFERER'] ) ) {
                    if(!in_array($parts[ "host" ],['sayidaty.local','commerce.sayidaty.local','dev.mall.sayidaty.net','pre.mall.sayidaty.net','mall.sayidaty.net'])){
                        $value['detail2'] =  "utm_source=".$parts[ "host" ];
                    }
                }
            }
        }
        if(!empty($_GET['utm_medium'])){
            $value['detail3'] =  "utm_medium=".$_GET['utm_medium'];
        }
        if(!empty($_GET['utm_campaign'])){
            $value['detail4'] =  "utm_campaign=".$_GET['utm_campaign'];
        }


        /** @var EventService $eventService */
        $eventService = $this->salesManago->getEventService();
        $eventService->create(SalesManago::$ownerEmail,$email,
            [
                'contactEvent' => array_merge([
                    'contactExtEventType' => $event,
                    'date' => date('c', time())
                ],$value)
            ]
        );
    }
    public function eventVisitProduct($email,$productId){
        $this->eventTrigger($email,SalesManago::EVENT_VISIT,['products'=>$productId,'description'=>'visit product']);
    }
    public function eventVisitCategory($email,$category){
        $this->eventTrigger($email,SalesManago::EVENT_VISIT,['detail1'=>$category,'description'=>'visit category']);
    }
    public function eventVisitCart($email){
        $this->eventTrigger($email,SalesManago::EVENT_VISIT,['description'=>'visit cart page']);
    }
    public function eventVisitCheckout($email){
        $this->eventTrigger($email,SalesManago::EVENT_VISIT,['description'=>'visit checkout']);
    }
    public function eventPurchase($email,$data){
        $this->eventTrigger($email,SalesManago::EVENT_PURCHASE,$data);
    }
    public function eventAddToFavorite($email,$productId){
        $this->eventTrigger($email,SalesManago::EVENT_OTHER,['products'=>$productId,'description'=>'favorite add']);
    }
    public function eventAddToCart($email,$productId,$price){
        $this->eventTrigger($email,SalesManago::EVENT_CART,['description'=>'add to cart','products'=>$productId,'value'=>$price]);
    }
    public function eventViewCart($email,$productId,$price){
        $this->eventTrigger($email,SalesManago::EVENT_CART,['description'=>'View Cart page','products'=>$productId,'value'=>$price]);
    }
    public function eventViewCheckout($email,$productId,$price){
        $this->eventTrigger($email,SalesManago::EVENT_CART,['description'=>'View Checkout page','products'=>$productId,'value'=>$price]);
    }


    public function eventRemoveFromCart($email,$product_id){
        $this->eventTrigger($email,SalesManago::EVENT_CANCELLED,['description'=>'delete from cart','products'=>$product_id]);
    }
    public function eventLogin($email){
        $this->eventTrigger($email,SalesManago::EVENT_LOGIN,['description'=>'Login']);
    }
    public function eventLogout($email){
        $this->eventTrigger($email,SalesManago::EVENT_OTHER,['description'=>'logout']);
    }
    public function eventSearch($email,$text){
        $this->eventTrigger($email,SalesManago::EVENT_OTHER,['description'=>'search','detail1'=>$text]);
    }

    public function eventSubscribe($email){
        $this->eventTrigger($email,SalesManago::EVENT_ACTIVATION,['description'=>'Subscribe NewsLetter']);
    }
    public function eventRegister($email){
        $this->eventTrigger($email,SalesManago::EVENT_LOGIN,['description'=>'Register']);
    }
}
