<?php

class AlgoIndexer {

    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->session = $registry->get('session');
        $this->db = $registry->get('db');
    }
    
    public function AddItem($type, $objectID, $user_id = 0) {
        $this->db->query(" INSERT INTO algolia SET type='" . $this->db->escape($type) . "', type_id='". (int)$objectID ."', date_added=NOW(), user_id='". (int)$user_id ."' ");
    }
}