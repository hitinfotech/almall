<?php

class Cache {

    private static $instance;
    private $cache;

    public static function getInstance($driver, $expire = 3600) {
        if (null === static::$instance) {
            static::$instance = new static($driver, $expire);
        }

        return static::$instance;
    }

    protected function __construct($driver, $expire = 3600) {
        $class = 'Cache\\' . $driver;

        if (class_exists($class)) {
            $this->cache = new $class($expire);
        } else {
            exit('Error: Could not load cache driver ' . $driver . ' cache!');
        }
    }

    public function get($key) {
        return $this->cache->get($key);
    }

    public function set($key, $value) {
        return $this->cache->set($key, $value);
    }

    public function deletekeys($key) {
        $key = '*'.$key . '*';
        foreach ($this->cache->keys($key) as $row) {
            $this->delete($row);
        }
    }

    public function delete($key) {
        return $this->cache->delete($key);
    }

    private function __clone() {
        
    }

    private function __wakeup() {
        
    }

}
