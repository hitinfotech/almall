<?php 

class Algolia {

    protected $registry;
    protected static $algolia;

    public function __construct($registry) {
        $this->registry = $registry;
    }

    public function algolia() {
        if (!isset(static::$algolia)) {
            static::$algolia = new \AlgoliaSearch\Client(ALGOLIA_APPID, ALGOLIA_SEARCH_API_KEY);
        }
        return static::$algolia;
    }

}