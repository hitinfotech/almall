<?php

/**
 * Class DynamicValues
 * @property Config $config
 * @property DB\DB $db
 * @property Cache $cache
 */

class DynamicValues {


    /**
     * DynamicValues constructor.
     * @param Registry $registry
     */
  public function __construct($registry) {

      $this->config = $registry->get('config');
      $this->db = $registry->get('db');
      $this->cache = $registry->get('cache');


  }

  public function getValues($code,$language_id=0){
    $val = $this->cache->get('dynamic_values'.$code.$language_id);
    $where_lang = '';
    if( $language_id){
      $where_lang = 'AND language_id='.(int)$language_id." ";
    }

    if (!$val) {
        $query = $this->db->query("SELECT `key`,`code`,`value`,`language_id` FROM " . DB_PREFIX . "dynamic_values WHERE code='".$this->db->escape($code)."' ".$where_lang);
        $val = $query->rows;
        if (!empty($val)) {
            $this->cache->set('dynamic_values'.$code.$language_id, $val);
        }
    }

    return $val;
  }

  public function get($code,$key,$language_id=null){
      $val = $this->cache->get('dynamic_values'.$code.$key.$language_id);
      $where_lang = '';
      if( !empty($language_id)){
          $where_lang = 'AND language_id='.(int)$language_id." ";
      }
      if (empty($val)) {
          $query = $this->db->query("SELECT `key`,`code`,`value`,`language_id` FROM " . DB_PREFIX . "dynamic_values WHERE code='".$this->db->escape($code)."' and `key`='".$this->db->escape($key)."' ".$where_lang);
          if(!empty($query->row))
            $val = $query->row['value'];
          if (!empty($val)) {
              $this->cache->set('dynamic_values'.$code.$key.$language_id, $val);
          }
      }
      return $val;
  }
  public function set($code,$key,$value,$language_id=null){
      $where_lang = '';
      if(!empty($language_id)){
          $where_lang = 'AND language_id='.(int)$language_id." ";
      }
      $exist = $this->db->query("SELECT count(*) count from dynamic_values where code='".$this->db->escape($code)."' and `key`='".$this->db->escape($key)."' ".$where_lang);
      if($exist->row['count']>0){
          $this->db->query("UPDATE " . DB_PREFIX . "dynamic_values set `value`='".$this->db->escape($value)."' where code='".$this->db->escape($code)."' and `key`='".$this->db->escape($key)."' ".$where_lang);
      }else{
          if(!empty($language_id)){ $where_lang = ',language_id='.(int)$language_id." "; }
          $this->db->query("INSERT INTO  " . DB_PREFIX . "dynamic_values set `value`='".$this->db->escape($value)."',`code`='".$this->db->escape($code)."',`key`='".$this->db->escape($key)."' ".$where_lang);
      }
      $this->cache->set('dynamic_values'.$code.$key.$language_id, $value);
  }




}
