<?php

class exampleSQS

{
    private $oClient;
    private $aws;

    function __construct()
    {
        $this->aws = Aws::factory(dirname(__FILE__) . '/aws/Aws/config/aws-config.php');
        $this->oClient = $this->aws->get('sqs');
    }

    function pull($queueName)
    {
        $PullMessages = $this->oClient->receiveMessage(array(
            'QueueUrl' => $queueName,
            'MaxNumberOfMessages' => 10
        ));

        return $PullMessages;
    }


}

//$a = new exampleSQS();
//var_dump($a->pull('QueueName'));
