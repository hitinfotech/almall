<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Naqel extends \Shipping\Shipping
{
    /*
     * Test Environment.
     * http://212.93.160.150/NaqelAPIDemo/XMLShippingService.asmx
     * 
     * Documentation :
     * http://212.93.160.150/NaqelAPIDemo/documentation/shippingapi.pdf
     * 
     * Life Environment:
     * http://212.93.160.150/NaqelAPI/XMLShippingService.asmx
     * 
     */

    public $client;

    public $isDemo = false;
    public $loadType = 36;
    public function __construct($registry, $databaseID)
    {
        parent::__construct($registry, $databaseID);
        if($this->isDemo){
            $this->client = new SoapClient("http://212.93.160.150/NaqelAPIDemo/XMLShippingService.asmx?WSDL");
        }else{
            //$this->client = new SoapClient("http://212.93.160.150/NaqelAPI/XMLShippingService.asmx?WSDL");
            $this->client = new SoapClient("https://infotrack.naqelexpress.com/NaqelAPIServices/NaqelAPI/4.0/XMLShippingService.asmx?WSDL");
        }

    }

    public function CreateBooking($data = array())
    {

    }

    public function BookingTrack($AwbNo = '', $country_id, $shipping_company = '')
    {

    }

    public function shippingTrack($AwbNo){
        //return true;
        $data = [
            'ClientInfo' => [
                'ClientID' => $this->account_no,
                'Password' => $this->account_pass,
                'Version' => '4.0',
            ],
            'WaybillNo' => $AwbNo,

        ];
        $res = $this->client->TraceByWaybillNo($data);
        if(isset($res) && isset($res->TraceByWaybillNoResult)){
            $source = $res->TraceByWaybillNoResult;
            if (isset($res->TraceByWaybillNoResult->Tracking) && is_array($res->TraceByWaybillNoResult->Tracking)) {
                $source = $res->TraceByWaybillNoResult->Tracking;
            }
            foreach ($source as $row) {
                if(isset($row->EventCode) && ((int)$row->EventCode) == 7){
                    return true;
                }
             }
        }
        return false;
    }
    public function GetBookingDetails($data = array())
    {

    }

    public function GetRateQuery($data = array())
    {

    }

    public function LogData($AwbNo = '', $country_id, $shipping_company = '')
    {
        $data = [
            'ClientInfo' => [
                'ClientID' => $this->account_no,
                'Password' => $this->account_pass,
                'Version' => '4.0',
            ],
            'WaybillNo' => $AwbNo,

        ];

        $res = $this->client->TraceByWaybillNo($data);
        //print_r($res);die;
        $title = 'Order Tracking Details for KSA';
        if ($country_id == 221) {
            $title = 'Order Tracking Details for UAE';
        }

        $company_details = '';
        if ($shipping_company != '') {
            $company_details = '<div>' . 'Shipping Company: ' . $shipping_company . '</div><br>';
        }
        //print_r($result);
        $string = '<h5>' . $title . '</h5>';
        $string .= '<div>' . 'AWBNo: ' . $AwbNo . '</div>';
        $string .= $company_details;
        $string .= '<div class="table-responsive">';
        $string .= '<table class="table table-bordered table-hover">';
        $string .= '<thead>';
        $string .= '<tr>';
        $string .= '<td class="left">Date</td>';
        $string .= '<td class="left">Time</td>';
        $string .= '<td class="left">Station Code</td>';
        $string .= '<td class="left">Activity</td>';
        $string .= '</tr>';
        $string .= '</thead>';
        $string .= '<tbody>';

        if(isset($res) && isset($res->TraceByWaybillNoResult)){


        $source = $res->TraceByWaybillNoResult;
        if (isset($res->TraceByWaybillNoResult->Tracking) && is_array($res->TraceByWaybillNoResult->Tracking)) {
            $source = $res->TraceByWaybillNoResult->Tracking;
        }



            foreach ($source as $row) {
                $string .= '<tr>';
                $string .= '<td>' . substr($row->Date,0,10) . '</td>';
                $string .= '<td>' . substr($row->Date,11,5) . '</td>';
                $string .= '<td>' . $row->StationCode . '</td>';
                $string .= '<td>' . (isset($row->Activity) ? $row->Activity : '') . '</td>';
                $string .= '</tr>';
            }
        }
        $string .= '</table>';
        $string .= '</div>';
        return $string;
    }

    public function LogDataBooking($data = array())
    {

    }

    public function NewOrder($data = array())
    {

    }

    public function NewOrderWithAutoAWBNO($data = array())
    {

    }

    private function countryIdToCode($id)
    {
       $query = $this->db->query("SELECT `iso_code_2` from `country`  where country_id = '$id' ");
       return $query->row['iso_code_2'] == 'SA' ? 'KSA' : $query->row['iso_code_2']; // the iso code for KSA in our system is SA but naqel required it to be KSA

    }

    private function zoneIdToCode($zone_id)
    {   ///need to create table

//        return 'MAK';
        $query = $this->db->query("SELECT naqel_destination_code FROM `zone` where zone_id='" . $zone_id . "' ");
        if ($query->num_rows) {
            return ($this->isDemo && $query->row['naqel_destination_code'] == 'MAC')  ? 'MAK' : $query->row['naqel_destination_code'];
        }
    }

    public function BookingRequest($data = array())
    {
        //echo $this->zoneIdToCode($data['shipperZoneId']);die;
        $data = [
            'ClientInfo' => [

                'ClientAddress' => [
                    'PhoneNumber' => $data['shipperPhone'],
                    'FirstAddress' => $data['shipperAddress1'],
                    'CountryCode' => $this->countryIdToCode($data['shipperCountryId']),
                    'CityCode' => $this->zoneIdToCode($data['shipperZoneId']),
                ],
                'ClientContact' => [
                    'Name' => $data['shipperName'],
                    'Email' => $data['ShipperEmail'],
                    'PhoneNumber' => $data['shipperPhone'],
                    'MobileNo' => $data['shipperPhone'],
                ],
                'ClientID' => $this->account_no,
                'Password' => $this->account_pass,
                'Version' => '4.0',
            ],
            'ConsigneeInfo' => [
                'ConsigneeName' => $data['consignee'],
                'PhoneNumber' => $data['consigneePhone'],
                'Address' => $data['consigneeAddress'],
                'CountryCode' => $this->countryIdToCode($data['consigneeCountryId']),
                'CityCode' => $this->zoneIdToCode($data['consigneeZoneId']),
            ],
            'BillingType' => ($data['cod_amount'] > 0) ? 5 : 1,
            'PicesCount' => $data['pieces'],
            'Weight' => $data['weight'],
            //'DeliveryInstruction' => 'string',
            'CODCharge' => $data['cod_amount'],
            'CreateBooking' => true,
            //'isRTO' => 'boolean',
            // 'GeneratePiecesBarCodes' => 'boolean',
            'LoadTypeID' => $this->loadType, // need review
            'DeclareValue' => $data['cod_amount'],
            'RefNo' => $data['RefNo'],
            'DeliveryInstruction' => isset( $data['GoodDesc']) ? $data['GoodDesc'] :  ' ',
            'GoodDesc'=>isset( $data['GoodDesc']) ? $data['GoodDesc'] :  ' ',
        ];



        try {
            //print_r($data);
            $res = $this->client->CreateWaybill(['_ManifestShipmentDetails' => $data]);
            if (isset($res) && isset($res->CreateWaybillResult) && empty($res->CreateWaybillResult->HasError) && isset($res->CreateWaybillResult->WaybillNo)) {
                return ['BookAWB'=>$res->CreateWaybillResult->WaybillNo,'BookRef'=>$res->CreateWaybillResult->BookingRefNo];
                //return $res->CreateWaybillResult->WaybillNo;
            }else{
                print_r($data);
                print_r($res);
            }
            return false;
        } catch (Exception $e) {

            var_dump($e->getMessage());
        }
    }

    public function NewOrderWithAutoAWBNORef($data = array())
    {

    }

    public function WebCustomerSearch($data = array())
    {

    }

    public function WebSMSABookingInsert($data = array())
    {

    }

    public function returnShipment($data){
        $data = [
            '_ClientInfo' => [
                'ClientID' => $this->account_no,
                'Password' => $this->account_pass,
                'Version' => '4.0',
            ],
            'WaybillNo'=>$data['waybill']
        ];


        $res = $this->client->CreateRTOWaybill($data);
        if (isset($res) && isset($res->CreateRTOWaybillResult) && empty($res->CreateRTOWaybillResult->HasError) && isset($res->CreateRTOWaybillResult->WaybillNo)) {
            return $res->CreateRTOWaybillResult->WaybillNo;
        }
        return false;
    }

}
