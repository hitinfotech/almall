<?php

class commonfunctions{

  public function __construct($registry){
    $this->db = $registry->get('db');
  }

  public function getOrderNumber($order_id){

    $order = substr($order_id,4);
    $order1 = substr($order,0,-1);
    $order2 = substr($order,0,-2);
    $query1 = $this->db->query("SELECT order_id from `order` WHERE order_id=".(int)$order1);
    if($query1->num_rows)
      return $order1;
    else{
      $query2 = $this->db->query("SELECT order_id from `order` WHERE order_id=".(int)$order2);
      if($query2->num_rows)
        return $order2;
    }
    return $order_id;
  }

  public function convertOrderNumber($order_id,$date_added=''){
    // the new order numbers will be in the form of  yy-mm-order_id-dd
    if($date_added != ''){
      return date('y', strtotime($date_added)).date('m', strtotime($date_added)).$order_id.date('d', strtotime($date_added));
    }
    $query = $this->db->query("SELECT date_added FROM `order` WHERE order_id='".(int) $order_id."'");
    if($query->num_rows){
      return date('y', strtotime($query->row['date_added'])).date('m', strtotime($query->row['date_added'])).$order_id.date('d', strtotime($query->row['date_added']));
    }

  }

}
