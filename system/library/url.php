<?php

class Url {

    private $domain;
    private $ssl;
    private $rewrite = array();

    public function __construct($domain, $ssl = '') {
        $this->domain = $domain;
        $this->ssl = $ssl;
    }

    public function addRewrite($rewrite) {
        $this->rewrite[] = $rewrite;
    }

    public function link($route, $args = '', $secure = false, $orig_language = false, $orig_country = false, $partner_link = true) {
        if (!$secure) {
            $url = $this->domain;
        } else {
            $url = $this->ssl;
        }

        $url .= 'index.php?route=' . $route;

        if ($args) {
            if (is_array($args)) {
                $url .= '&amp;' . http_build_query($args);
            } else {
                $url .= str_replace('&', '&amp;', '&' . ltrim($args, '&'));
            }
        }

        foreach ($this->rewrite as $rewrite) {
            $url = $rewrite->rewrite($url, $orig_language, $orig_country, $partner_link);
        }
        return $url;
    }
    
    public function create_KeyWord($pre_fix, $keyword){
        //return $pre_fix.$this->clean_url($keyword);
        return $pre_fix.$this->str_limit($this->str_slug_utf8($keyword),100,'');
    }
    
    public function str_limit($value, $limit = 100, $end = '...') {
        if (mb_strlen($value,'UTF-8') <= $limit) {
            return $value;
        }
        return rtrim(mb_substr($value, 0, $limit, 'UTF-8')) . $end;
    }

    public function str_slug_utf8($title, $separator = '-') {

        // Convert all dashes/underscores into separator
        $flip = $separator == '-' ? '_' : '-';

        $title = preg_replace('![' . preg_quote($flip) . ']+!u', $separator, $title);

        // Remove all characters that are not the separator, letters, numbers, or whitespace.
        $title = preg_replace('![^' . preg_quote($separator) . '\pL\pN\s]+!u', '', mb_strtolower($title,'UTF-8'));

        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('![' . preg_quote($separator) . '\s]+!u', $separator, $title);

        return trim($title, $separator);
    }

}