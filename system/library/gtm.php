<?php

class Gtm {

    private $data = array();
    private $basicElements = array('pageType', 'pageCategory', 'pageTitle', 'pageName', 'visitorType', 'event');
    private $Events = array('pageview', 'addtocart', '32-banner', 'purchase');
    private $pageTypes = array('Listingpage', 'Homepage', 'Productpage', 'checkout', 'Basketpage', 'profile', 'address', 'newsletter', 'Transaction', 'succes-register', 'signin', 'signout', 'forgotpassword','brands', 'malls', 'tips','top5','looks','shops','Partner');
    // private $pageCategories = array($categories in website);
    private $vistorTypes = array('low-level', 'high-level');
    private $ProductIDList = '';
    private $ProductID = '';
    private $ProductTransactionProducts = '';
    private $transactionId = '';

    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->customer = $registry->get('customer');
        $this->session = $registry->get('session');
        $this->country = $registry->get('country');
        $this->request = $registry->get('request');
        $this->db = $registry->get('db');
        $this->tax = $registry->get('tax');
        $this->weight = $registry->get('weight');
        $this->cache = $registry->get('cache');
        $this->cart = $registry->get('cart');
    }

    public function getBasicElements() {
        return $this->basicElements;
    }

    public function getEvents() {
        return $this->Events;
    }

    public function setpageType($pageType) {
        if (in_array($pageType, $this->pageTypes)) {
            $this->data['pageType'] = $pageType;
            return true;
        }
//        die("YOUR PAGE TYPE");
        return false;
    }

    public function setpageCategory($pageCategory) {
        $this->data['pageCategory'] = $pageCategory;
    }

    public function setpageName($pageName) {
        $this->data['pageName'] = $pageName;
    }

    public function setpageTitle($pageTitle) {
        $this->data['pageTitle'] = $pageTitle;
    }

    public function setvisitorType($visitorType) {
        if (in_array($visitorType, $this->vistorTypes)) {
            $this->data['visitorType'] = $visitorType;
            return true;
        }
  //      die("visitor");
        return false;
    }

    public function setRevenue($total) {
        $this->data['revenue'] = $total;
    }

    public function setTax($tax) {
        $this->data['tax'] = $tax;
    }

    public function setShipping($shipping) {
        $this->data['shipping'] = $shipping;
    }

    public function setCoupon($coupon) {
        $this->data['coupon'] = $coupon;
    }

    public function setEvent($event_name) {
        if (in_array($event_name, $this->Events)) {
            $this->data['event'] = $event_name;
            return true;
        }
    //    die("event");
        return false;
    }

    public function setProductIDList($products) {
        if (is_array($products) && !empty($products)) {
            $this->ProductIDList = json_encode($products);
            return true;
        }
      //  die("products");
        return false;
    }

    public function setProductID($productID) {
        if ((int) $productID > 0) {
            $this->ProductID = $productID;
            return true;
        }
        //die("productID");
        return false;
    }

    public function setProductTransactionProducts($products) {
        if (is_array($products) && !empty($products)) {
            $this->ProductTransactionProducts = json_encode($products);
            return true;
        }
        //die("transaction-products");
        return false;
    }

    public function setTransactionID($order_id) {
        $this->transactionId = (int) $order_id;
    }

    public function getData() {

        foreach ($this->basicElements as $element) {
            if (!isset($this->data[$element]) || empty($this->data[$element])) {
          //      die("No GTM {$element} ... !!");
                return false;
            }
        }

        $customerEmail = '';

        if ($this->customer->isLogged()) {
            $customerEmail = $this->customer->getEmail();
        } elseif (isset($this->session->data['email']) && !empty($this->session->data['email'])) {
            $customerEmail = $this->session->data['email'];
        } elseif (isset($this->request->cookie[EMAIL_COOKIE_NAME])) {
            $customerEmail = unserialize(base64_decode($this->request->cookie[EMAIL_COOKIE_NAME]));
        }

        $this->data['gtm_email'] = (empty($customerEmail) ? '' : md5($customerEmail));

        if (!empty($this->ProductIDList)) {
            $this->data['ProductIDList'] = $this->ProductIDList;
        }

        if (!empty($this->ProductID)) {
            $this->data['ProductID'] = $this->ProductID;
        }

        if (!empty($this->ProductTransactionProducts)) {
            $this->data['ProductTransactionProducts'] = $this->ProductTransactionProducts;
        }

        if (!empty($this->transactionId)) {
            $this->data['transactionId'] = $this->transactionId;
        }

        return $this->data;
    }

    //pageType: 'Listingpage',
    //pageCategory': 'Home',
    //pageTitle: 'Listingpage',
    //HashedEmail: 'email',
    //visitorType': 'low-value',
    //event': 'pageView',
    ///'ProductIDList': gtm_product_ids ,
    ///'ProductID': gtm_product_ids ,
    ///'ProductBasketProducts': products_list,
    ///'ProductTransactionProducts': products_list_det,
    ///'TransactionID': gtm_product_ids['order_id']
}
