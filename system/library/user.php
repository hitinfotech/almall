<?php

class User {

    private $core = 0;
    private $user_core_group = 0;
    private $store_id = 0;
    private $branches;
    private $user_id;
    private $user_ip;
    private $username;
    private $user_group_id;
    private $permission = array();
    private $access_to_ship_company;

    public function __construct($registry) {
        $this->db = $registry->get('db');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');

        if (isset($this->session->data['user_id'])) {
            $user_query = $this->db->query("SELECT u.*, ug.core AS core_group FROM " . DB_PREFIX . "user u LEFT JOIN user_group ug ON(u.user_group_id=ug.user_group_id) WHERE user_id = '" . (int) $this->session->data['user_id'] . "' AND status = '1'");

            if ($user_query->num_rows) {
                $this->store_id = $user_query->row['store_id'];
                $this->core = $user_query->row['core'];
                $this->user_id = $user_query->row['user_id'];
                $this->user_ip = $user_query->row['ip'];
                $this->username = $user_query->row['username'];
                $this->user_group_id = $user_query->row['user_group_id'];
                $this->user_core_group = $user_query->row['core_group'];
                $this->access_to_ship_company = $user_query->row['access_to_ship_company'];

                $this->branches = $this->getUserBranches();

                $this->db->query("UPDATE " . DB_PREFIX . "user SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE user_id = '" . (int) $this->session->data['user_id'] . "'");

                $user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int) $user_query->row['user_group_id'] . "'");

                $permissions = json_decode($user_group_query->row['permission'], true);

                if (is_array($permissions)) {
                    foreach ($permissions as $key => $value) {
                        $this->permission[$key] = $value;
                    }
                }
            } else {
                $this->logout();
            }
        }
    }

    private function getUserBranches() {
        $result = array();

        $branches_info = $this->db->query(" SELECT store_id FROM store s LEFT JOIN main_store ms ON(s.main_store_id=ms.main_store_id) WHERE ms.main_store_id = '" . (int) $this->store_id . "' ");
        foreach ($branches_info->rows as $row) {
            $result[$row['store_id']] = $row['store_id'];
        }
        return $result;
    }

    public function login($username, $password) {
        $user_query = $this->db->query("SELECT u.*,ug.core AS core_group FROM  `" . DB_PREFIX . "user` u LEFT JOIN `" . DB_PREFIX . "user_group` ug ON(u.user_group_id=ug.user_group_id) WHERE u.username = '" . $this->db->escape($username) . "' AND (u.password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR u.password = '" . $this->db->escape(md5($password)) . "') AND u.status = '1'");

        if ($user_query->num_rows) {
            $this->session->data['user_id'] = $user_query->row['user_id'];

            $this->user_id = $user_query->row['user_id'];
            $this->user_ip = $user_query->row['ip'];
            $this->username = $user_query->row['username'];
            $this->user_group_id = $user_query->row['user_group_id'];
            $this->core = $user_query->row['core'];
            $this->user_core_group = $user_query->row['core_group'];
            $this->access_to_ship_company = $user_query->row['access_to_ship_company'];

            $this->branches = $this->getUserBranches();

            $user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int) $user_query->row['user_group_id'] . "'");

            $permissions = json_decode($user_group_query->row['permission'], true);

            if (is_array($permissions)) {
                foreach ($permissions as $key => $value) {
                    $this->permission[$key] = $value;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        unset($this->session->data['user_id']);

        $this->store_id = 0;
        $this->core = 0;
        $this->user_id = '';
        $this->username = '';
    }

    public function hasPermission($key, $value,$menu_check = 0) {
        if ($menu_check == 0) {
            if ($key == 'access' && substr($value, 0, 12) == "ticketsystem") {
                return true;
            }
        }

        if (!$this->core && strtolower(substr($value, 0, 4)) == "user") {
            return FALSE;
        }

        if (isset($this->user_core_group) && $this->user_core_group === '1') {
            return TRUE;
        }

        if (isset($this->permission[$key])) {
            return in_array($value, $this->permission[$key]);
        } else {
            return false;
        }
    }

    public function isLogged() {
        if ($this->user_group_id !== 0 && $this->store_id === 0) {
            return 0;
        }
        return $this->user_id;
    }

    public function getId() {
        return $this->user_id;
    }

    public function getIp() {
        return $this->user_ip;
    }

    public function getUserName() {
        return $this->username;
    }

    public function getStoreId() {
        return $this->store_id;
    }

    public function getBranches() {
        return $this->branches;
    }

    public function getGroupId() {
        return $this->user_group_id;
    }

    public function getIsCoreUser() {
        return $this->core;
    }

    public function getIsCoreGroup() {
        return $this->user_core_group;
    }

    public function getShippingCompany(){
      return $this->access_to_ship_company;
    }

}
