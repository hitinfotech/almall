<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Postplus extends \Shipping\Shipping
{
    /*
     * Test Environment.
     * http://212.93.160.150/NaqelAPIDemo/XMLShippingService.asmx
     * 
     * Documentation :
     * http://212.93.160.150/NaqelAPIDemo/documentation/shippingapi.pdf
     * 
     * Life Environment:
     * http://212.93.160.150/NaqelAPI/XMLShippingService.asmx
     * 
     */
    public $client;

    public $isDemo = true;

    /**
     * Postplus constructor.
     * @param $registry
     * @param $databaseID
     */
    public function __construct($registry, $databaseID)
    {

        parent::__construct($registry, $databaseID);
        if($this->isDemo){
            $this->client = new SoapClient("http://168.187.136.18:8095/APIService/PostaWebClient.svc?wsdl");
            $this->account_username = "CR2931";
            $this->account_pass = "sayidaty1234";
        }else{
            $this->client = new SoapClient("http://etrack.postaplus.net/APIService/PostaWebClient.svc?singleWsdl");

        }
    }

    public function CreateBooking($data = array())
    {

    }

    public function BookingTrack($AwbNo = '', $country_id, $shipping_company = '')
    {

    }

    public function shippingTrack($AwbNo)
    {
        $data = [
            'AirwaybillNumber' => $AwbNo,
            'Reference1' => ' ',
            'Reference2' => ' ',
            'UserName' => $this->account_no,
            'Password' => $this->account_pass,
            'ShipperAccount' => $this->account_no,
            'CodeStation' => 'KWI',
        ];
        $res = ($this->client->Shipment_Tracking($data));


        if (isset($res) && isset($res->Shipment_TrackingResult)) {


            $source = $res->Shipment_TrackingResult;
            if (isset($res->Shipment_TrackingResult->TRACKSHIPMENT) && is_array($res->Shipment_TrackingResult->TRACKSHIPMENT)) {
                $source = $res->Shipment_TrackingResult->TRACKSHIPMENT;
            }

            foreach ($source as $row) {
               if($row->Event == 'DELIVERED'){
                   return true;
               }
            }
        }

        return false;
    }

    public function GetBookingDetails($data = array())
    {

    }

    public function GetRateQuery($data = array())
    {

    }

    public function LogData($AwbNo = '', $country_id, $shipping_company = '')
    {

       //$AwbNo = '100003868130';
        $data = [

//                'ClientInfo'=>[
//                    'CodeStation'=>'KWI',
//                    'UserName'=>$this->account_no,
//                    'Password'=>$this->account_pass,
//                    'ShipperAccount'=>$this->account_no,
//                ],
            'AirwaybillNumber' => $AwbNo,
            'Reference1' => ' ',
            'Reference2' => ' ',
            'UserName' => $this->account_no,
            'Password' => $this->account_pass,
            'ShipperAccount' => $this->account_no,
            'CodeStation' => 'KWI',
        ];

        $res = ($this->client->Shipment_Tracking($data));
        $title = 'Order Tracking Details for KSA';
        if ($country_id == 221) {
            $title = 'Order Tracking Details for UAE';
        }

        $company_details = '';
        if ($shipping_company != '') {
            $company_details = '<div>' . 'Shipping Company: ' . $shipping_company . '</div><br>';
        }
        //print_r($result);
        $string = '<h5>' . $title . '</h5>';
        $string .= '<div>' . 'AWBNo: ' . $AwbNo . '</div>';
        $string .= $company_details;
        $string .= '<div class="table-responsive">';
        $string .= '<table class="table table-bordered table-hover">';
        $string .= '<thead>';
        $string .= '<tr>';
        $string .= '<td class="left">Date</td>';
        $string .= '<td class="left">Event</td>';
        $string .= '<td class="left">Event Name</td>';
        $string .= '<td class="left">Note</td>';
        $string .= '</tr>';
        $string .= '</thead>';
        $string .= '<tbody>';

        if (isset($res) && isset($res->Shipment_TrackingResult)) {


            $source = $res->Shipment_TrackingResult;
            if (isset($res->Shipment_TrackingResult->TRACKSHIPMENT) && is_array($res->Shipment_TrackingResult->TRACKSHIPMENT)) {
                $source = $res->Shipment_TrackingResult->TRACKSHIPMENT;
            }


            foreach ($source as $row) {
                // print_r($row);
                $string .= '<tr>';
                $string .= '<td>' . substr($row->DateTime, 0, 10) . '</td>';
                $string .= '<td>' . $row->Event . '</td>';
                $string .= '<td>' . $row->EventName . '</td>';
                $string .= '<td>' . $row->Note . '</td>';
                $string .= '</tr>';
            }
        }
        $string .= '</table>';
        $string .= '</div>';
        return $string;
    }

    public function LogDataBooking($data = array())
    {

    }

    public function NewOrder($data = array())
    {

    }

    public function NewOrderWithAutoAWBNO($data = array())
    {

    }

    private function countryIdToCode($id)
    {
        $query = $this->db->query("SELECT `iso_code_3` from `country`  where country_id = '$id' ");
        return $query->row['iso_code_3'];

    }

    private function countryIdToName($id)
    {
        $query = $this->db->query("SELECT `iso_code_2` from `country`  where country_id = '$id' ");
        return $query->row['iso_code_2'];
    }

    private function zoneIdToCode($zone_id)
    {
        $query = $this->db->query("SELECT postaplus_code FROM `zone` where zone_id='" . $zone_id . "' ");
        return $query->row['postaplus_code'];

    }

    public function BookingRequest($data = array())
    {
        //$this->BookingRequestAWWB($data);
        //return;
        try {


            $pickupInfo = new stdClass();
            $pickupInfo->Address = $data['FromAddress'].' - '.$data['Reference1'];//'Saydaty Mall';
            $pickupInfo->City = 'CITY415668';
            $pickupInfo->ClientInfo = new stdClass();
                $pickupInfo->ClientInfo->UserName = $this->account_no;
                $pickupInfo->ClientInfo->Password = $this->account_pass;
                $pickupInfo->ClientInfo->ShipperAccount = $this->account_no;
                $pickupInfo->ClientInfo->CodeStation = 'DXB';
            $pickupInfo->CloseDate = '';
            $pickupInfo->CodeCourier = '';
            $pickupInfo->CodeEntity = 'ASI';
            $pickupInfo->CodeSalesPerson = '';

            $pickupInfo->ContactPerson = $data['FromName'];//'';
            $pickupInfo->Country = 'ARE';
            $pickupInfo->CourierRemark = 'Test Remark';
            $pickupInfo->DeliverContactPerson = 'Naveen DeSouza';//'';
            $pickupInfo->DeliveryAddress = $data['ToAddress'];//'';
            $pickupInfo->DeliveryCity = 'CITY66493';
            $pickupInfo->DeliveryPhone = '977123123123';

            $pickupInfo->GoodsType = 'SHPT2';

            $pickupInfo->JcsNumber = '';
            $pickupInfo->Notes = '  ';
            $pickupInfo->PickAddress = $data['FromAddress'];
            $pickupInfo->PickCity = 'CITY415668';//$this->zoneIdToCode($data['FromCity']);//'';
            $pickupInfo->PickCompany = $data['FromCompany'];
            $pickupInfo->PickDate = (new DateTime())->format("Y-m-d\TH:i:s");//new DateTime();//(new DateTime())->format('d-m-Y H:i:s');//''; // datetime
            $pickupInfo->PickEmail = $data['FromEmail'];
            $pickupInfo->PickFax = '';
            $pickupInfo->PickPhone = sprintf("%015d", (int)$data['FromMobile']);
            $pickupInfo->PickService = '';
            $pickupInfo->PickupDate = (new DateTime())->format("Y-m-d\TH:i:s");//(new DateTime())->date;//(new DateTime())->format('d-m-Y H:i:s');//''; // datetime
            $pickupInfo->PickupNumber = '';
            $pickupInfo->Province = 'AZ';
            $pickupInfo->RequestedPerson = 'Naveen DeSouza';
            $pickupInfo->ShipmentCount = 1;
            $pickupInfo->Status = '';
            $pickupInfo->TotalCountShipment = $data['Quantity'];
            $pickupInfo->Vehicles = '';
            $pickupInfo->pickTime = (new DateTime())->format("Y-m-d\TH:i:s");//(new DateTime())->date;//(new DateTime())->format('d-m-Y H:i:s');//''; // datetime


//            $pickupInfo->PickupCancelDate =(new DateTime())->format("Y-m-d\TH:i:s");//new DateTime();//'';
//            $pickupInfo->PickupCancelReason = '';
//            $pickupInfo->PickupCancelUser = '';
//            $pickupInfo->CourierInformDate = '';
//            $pickupInfo->DeliveryCity = '';
//            $pickupInfo->DeliveryPhone = '';
//            $pickupInfo->JcsNumber = '';
//
//            $pickupInfo->TimeCourierAccept = '';
//            $pickupInfo->TimeEnd = '';
//            $pickupInfo->TimeStart = '';
//
//

//            $pickupInfo = (array)$pickupInfo;
//            print_r((array)$pickupInfo);die;

            $result = $this->client->Pickup_Creation(['PICKUPINFO' => $pickupInfo]);
//            print_r((array)$result);
//            echo 'xxx';die;
            if (isset($result) && isset($result['Shipment_CreationResult']) && ((int)$result['Shipment_CreationResult']) > 0) {
                //echo trim($result['Shipment_CreationResult']);
                return trim($result['Shipment_CreationResult']);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        //die;
        return false;
    }

    public function BookingRequestAWWB($data = array())
    {

        try {

            $ship = [
                // 'AppointmentToTime' => '16:10',
                // 'AppointmentFromTime'=> "10:10",
                // 'AppointmentDate'=>date("Y-m-d"),
                'ItemDetails' => [
                    ['ConnoteHeight' => 0, 'ConnoteLength' => 0, 'ConnoteWeight' => 1, 'ConnoteWidth' => 0],
                ],
                'ClientInfo' => [
                    'UserName' => $this->account_no,
                    'Password' => $this->account_pass,
                    'ShipperAccount' => $this->account_no,
                    'CodeStation' => 'DXB',
                ],
                'Consignee' => [
                    'FromName' => $data['FromName'], //from data
                    'FromAddress' => 'SAUDIII',//$data['FromAddress'], // from data
                    'FromMobile' => sprintf("%015d", (int)$data['FromMobile']), // from data
                    'FromTelphone' => sprintf("%015d", (int)$data['FromMobile']), //  from data
                    ########################################
                    'FromCodeCountry' => $this->countryIdToCode($data['FromCodeCountry']),//'KWT', // from data // need query
                    'FromProvince' => $this->countryIdToName($data['FromCodeCountry']), // from data
                    'FromCity' => $this->zoneIdToCode($data['FromCity']), // from data
                    'FromArea' => 'NA', // from data
                    ########################################
                    'Company' => $data['ToName'],// Consignee Company
                    'ToName' => $data['ToName'], //
                    'ToAddress' => $data['ToAddress'],//

                    ########################################
                    'ToCodeCountry' => $this->countryIdToCode($data['ToCodeCountry']), //'BHR',
                    'ToProvince' => $this->isDemo ? 'AZ' : $this->countryIdToName($data['ToCodeCountry']),//////
                    'ToCity' => $this->zoneIdToCode($data['ToCity']),
                    'ToArea' => 'NA',//
                    ########################################
                    'ToMobile' => sprintf("%015d", (int)$data['ToMobile']),
                    'ToTelPhone' => sprintf("%015d", (int)$data['ToMobile']),
                    'ToCodeSector' => 'NA',

                ],
                'ConnoteRef' => [
                    'Reference1' => $data['Reference1'],//from data
                ],
//                'ConnoteNotes'=>[
//                    'Note1'=>"Hello World",
//                ],
                'ConnotePerformaInvoice' => [
                    'CONNOTEPERMINV' => [
                        'CodeHS' => 'HSCD21',
                        'CodePackageType' => 'PCKT2',//need to check
                        'Description' => " ",
                        'OrginCountry' => $this->countryIdToName($data['FromCodeCountry']),
                        'Quantity' => $data['Quantity'],//from data
                        'RateUnit' => 1,
                    ]
                ],
                'ConnoteContact' => [
                    'Email1' => $data['Email1'], // sender email optional
                    'Email2' => $data['Email2'],// consignee 2
                    'TelMobile' => sprintf("%015d", (int)$data['ToMobile']),//
                ],
//                'NeedRoundTrip'=>"N",
//                'NeedPickUp'=>"N",
                'CashOnDelivery' => $data['CashOnDelivery'],
                'CashOnDeliveryCurrency' => $data['CashOnDeliveryCurrency'],
//            'PayMode'=>"NA",
                //  'CostShipment'=>'0',//
                //  'CodeCurrency'=>"NA",
                'CodeShippmentType' =>  'SHPT2' ,
                'CodeService' => ($data['CashOnDelivery'] > 0) ? 'SRV24' : 'SRV2', // need to check
                'ConnoteDescription' => 'mall.sayidaty.net',
                'ConnoteProhibited' => "N",
                'ConnotePieces' => $data['Quantity'], // from data

            ];
            ///echo $this->countryIdToName($data['ToCodeCountry']);die;
            //print_r($ship);die;
            $result = $this->client->Shipment_Creation(['SHIPINFO' => $ship]);
//            print_r($result);
//            echo 'xxx';die;
            if (isset($result) && isset($result['Shipment_CreationResult']) && ((int)$result['Shipment_CreationResult']) > 0) {
                //echo trim($result['Shipment_CreationResult']);
                return trim($result['Shipment_CreationResult']);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die;
        }


        return false;
    }
    public function NewOrderWithAutoAWBNORef($data = array())
    {

    }

    public function WebCustomerSearch($data = array())
    {

    }

    public function WebSMSABookingInsert($data = array())
    {

    }

    public function returnShipment($data)
    {
//        $data = [
//            '_ClientInfo' => [
//                'ClientID' => $this->account_no,
//                'Password' => $this->account_pass,
//                'Version' => '1.9',
//            ],
//            'WaybillNo'=>$data['waybill']
//        ];
//
//
//        $res = $this->client->CreateRTOWaybill($data);
//        if (isset($res) && isset($res->CreateRTOWaybillResult) && !isset($res->CreateRTOWaybillResult->HaSError) && isset($res->CreateRTOWaybillResult->WaybillNo)) {
//            return $res->CreateRTOWaybillResult->WaybillNo;
//        }
//        return false;
    }

}
