<?php

require_once DIR_SYSTEM . 'library/mail.php';

/**
 * @package		Webkul HelpDesk Mod
 * @copyright	Copyright (C) 2015 Webkul Pvt Ltd. All rights reserved. (webkul.com)
 * 
 * This class is used to send mail with PHPMailer Class
 */
class TsEmail extends TsRegistry {

    const CRLF = "\r\n";

    public $CharSet = 'iso-8859-1';
    private $WordWrap = 78; // set word wrap to the RFC2822 limit
    private $Body = '';

    public function sendMail($data) {


        $mail = new Mail();
        $mail->setTo($data['address']);
        $mail->setFrom(SUPPORT_EMAIL);
        $mail->setSender('Support Team');
        $mail->setSubject($data['subject']);

        $this->Body = $data['message'];
        $this->msgHTML($this->emailTemplate($data['subject'], html_entity_decode($data['message'], ENT_QUOTES, 'UTF-8')));

        $mail->setHtml($this->Body);

        $mail->send();
    }

    protected function emailTemplate($subject, $message) {

        $html = <<<HTML
		<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>{$subject}</title>
		</head>
		<body> 
		<div class="content">
			<table border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%;font-family: 'Open Sans', sans-serif; font-size: 12px;color: #666;">
				<tbody>
					<tr>
						<td style="width:100%;background-color:#ECECEC;padding:20px;">$message</td>
					</tr>
				</tbody>
			</table>
		</div>
		</body>
		</html>
                        
HTML;
        return $html;
    }

    public function isHTML($isHtml = true) {
        if ($isHtml) {
            $this->ContentType = 'text/html';
        } else {
            $this->ContentType = 'text/plain';
        }
    }

    public function msgHTML($message, $basedir = '', $advanced = false) {

        $this->isHTML(true);
        // Convert all message body line breaks to CRLF, makes quoted-printable encoding work much better
        $this->Body = $this->normalizeBreaks($message);
        $this->AltBody = $this->normalizeBreaks($this->html2text($message, $advanced));
        if (empty($this->AltBody)) {
            $this->AltBody = 'To view this email message, open it in a program that understands HTML!' . self::CRLF . self::CRLF;
        }
    }

    public static function normalizeBreaks($text, $breaktype = "\r\n") {
        return preg_replace('/(\r\n|\r|\n)/ms', $breaktype, $text);
    }

    public function html2text($html, $advanced = false) {
        if (is_callable($advanced)) {
            return call_user_func($advanced, $html);
        }
        return html_entity_decode(
                trim(strip_tags(preg_replace('/<(head|title|style|script)[^>]*>.*?<\/\\1>/si', '', $html))), ENT_QUOTES, $this->CharSet
        );
    }

}
