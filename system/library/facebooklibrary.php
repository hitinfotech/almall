<?php

/**
 * 
 * Facebook Info class
 * 
 */
require_once DIR_ROOT . 'vendors/Facebook/autoload.php';

class FacebookLibrary {

    private $facebook;
    private $app_id = FACEBOOK_APP_ID;
    private $app_secret = FACEBOOK_APP_SECRET;
    private $app_redirect_url = APP_REDIRECT_URL;
    private $default_graph_version = 'v2.2';
    
    public function getFacebookAppId() {
        return $this->app_id;
    }

    public function getFacebookAppSecret() {
        return $this->app_secret;
    }

    public function getFacebookPermission() {
        return $this->permissions;
    }

    public function getRedirectLogin() {
        $helper = $this->facebook->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions

        $loginUrl = $helper->getLoginUrl($this->app_redirect_url, $permissions);

        return $loginUrl;

        //echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
    }

    public function get($accessToken) {
        return $this->facebook->get('/me?fields='.FACEBOOK_FIEDS, $accessToken);
    }

    public function getRedirectLoginHelper() {
        return $this->facebook->getRedirectLoginHelper();
    }

    public function getOAuth2Client() {
        return $this->facebook->getOAuth2Client();
    }

    public function __construct() {

        $this->facebook = new Facebook\Facebook([
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_graph_version' => $this->default_graph_version
        ]);
    }

}
