<?php

class Aws_SQS {
    
    private $oClient;
    private $aws;

    public function __construct() {

        $this->aws = \Aws\Common\Aws::factory(dirname(__FILE__) . '/aws/Aws/config/aws-config.php');
        $this->oClient = $this->aws->get('sqs');
    }

    public function insert($mail_det) {
        try {
            // The message we will be sending
            $message = array();
            foreach ($mail_det as $key => $value){
                $message[$key] = $value;
            }
            
            // Send the message
            $this->oClient->sendMessage(array(
                'QueueUrl' => SQS_URL,
                'MessageBody' => json_encode($message)
            ));
        } catch (Exception $e) {
            
        }
    }
}