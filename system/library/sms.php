<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require(DIR_SYSTEM . 'library/Unifonic/Autoload.php');

class Sms {

    private $phone;
    private $from = 'Sayidaty';
    private $message;

    //private $auth_token;
    public function __construct() {

    }

    private function is_validate_phone($phone) {
        $phone = ltrim($phone, '+');
        $phone = ltrim($phone, '00');
        return $phone;
    }

    private function is_validate_message($message) {
        return $message;
    }

    private function is_validate_from($company) {
        if (!in_array($company, array($this->config->get('config_name_en'), $this->config->get('config_name_ar')))) {
            return false;
        }
        return true;
    }

    public function setFrom($company) {
        if ($this->is_validate_from($company)) {
            $this->from = $company;
        }
    }

    public function setPhone($phone) {
        if ($this->is_validate_phone($phone)) {
            $this->phone = $phone;
        }
    }

    public function setMessage($message) {
        if ($this->is_validate_message($message)) {
            $this->message = $message;
        }
    }

    public function send() {
        if (!$this->from) {
            trigger_error('Error: From required!');
            exit();
        }
        if (!$this->phone) {
            trigger_error('Error: Valide Phone required!');
            exit();
        }
        if (!$this->message) {
            trigger_error('Error: Message required!');
            exit();
        }

        // process message
        $client = new \Unifonic\API\Client();
        try {
            $response = $client->Messages->Send($this->phone, $this->message, $this->from); // send regular massage
            //$response = $client->Messages->Send('962798008911', 'hello this is test', 'GoodLuck'); // send regular massage
           // $response = $client->Account->GetBalance();
            //$response = $client->Account->getSenderIDStatus('Arabic');
           /// $response = $client->Account->getSenderIDs();
           // $response = $client->Account->GetAppDefaultSenderID();
//
//$response = $client->Messages->Send('recipient','messageBody','senderName');
//$response = $client->Messages->SendBulkMessages('96650*******,9665*******','Hello','UNIFONIC');
//$response = $client->Messages->GetMessageIDStatus('9459*******');
//$response = $client->Messages->GetMessagesReport();
//$response = $client->Messages->GetMessagesDetails();
            //  $response = $client->Messages->GetScheduled();
            //print_r($response);exit;
        } catch (Exception $e) {
            echo $e->getCode();
        }
    }

}
