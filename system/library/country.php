<?php

class Country {

    private $iso_code_2;
    private $real_country_id;
    private $countries = array();

    public function __construct($registry) {
        $this->registry = $registry;
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');
        $this->cache = $registry->get('cache');

        $language_id = ($this->DetectLanguage()=='en'?1:2);

        $key = 'system.country.getavailablecountries.'.$language_id;

        $result = $this->cache->get($key);
        if (!$result) {
            $query = $this->db->query(" SELECT c.country_id, c.iso_code_2, c.language_code,  c.curcode, cd.name, c.telecode, c.telephone FROM " . DB_PREFIX . "country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE c.available='1' AND cd.language_id='". (int)$language_id ."' ");

            $result = $query->rows;

            if ($query->num_rows) {
                $this->cache->set($key, $query->rows);
            }
        }

        foreach ($result as $row) {
            $key = strtolower($row['iso_code_2']);
            $this->countries[$key] = array(
                'country_id' => $row['country_id'],
                'name' => $row['name'],
                'telecode' => $row['telecode'],
                'telephone' => $row['telephone'],
                'curcode' => $row['curcode'],
                'language' => $row['language_code']
            );
        }
        $country_code = '';
        if (isset($this->request->get['country']) && (array_key_exists($this->request->get['country'], $this->countries))) {
            $country_code = $this->request->get['country'];
        } elseif ((isset($this->session->data['country'])) && (array_key_exists($this->session->data['country'], $this->countries))) {
            $country_code = $this->session->data['country'];
        } elseif ((isset($this->request->cookie['country'])) && (array_key_exists($this->request->cookie['country'], $this->countries))) {
            $country_code = $this->request->cookie['country'];
        } else {
            $country_code = strtolower($this->getDefaultCountry());
        }

        $this->set($country_code); // this is will be changed from admin panel
    }

    public function set($country_code) {
        if (array_key_exists($country_code, $this->countries)) {
            $this->iso_code_2 = $country_code;

            if (!isset($this->session->data['country']) || ($this->session->data['country'] != $country_code)) {
                $this->session->data['country'] = $country_code;
            }

            if (!isset($this->request->cookie['country']) || ($this->request->cookie['country'] != $country_code)) {
                setcookie('country', $country_code, COOKIES_PERIOD, '/', $this->request->server['HTTP_HOST']);
            }
        }
    }

    public function getId($country = '') {
        if (!$country) {
            return $this->countries[$this->iso_code_2]['country_id'];
        } elseif ($country && isset($this->countries[$country])) {
            return $this->countries[$country]['country_id'];
        } else {
            return 0;
        }
    }

    public function getName($country = '') {
        if (!$country) {
            return $this->countries[$this->iso_code_2]['name'];
        } elseif ($country && isset($this->countries[$country])) {
            return $this->countries[$country]['name'];
        } else {
            return '';
        }
    }

    public function getTelecode($country = '') {
        if (!$country) {
            return $this->countries[$this->iso_code_2]['telecode'];
        } elseif ($country && isset($this->countries[$country])) {
            return $this->countries[$country]['telecode'];
        } else {
            return '';
        }
    }

    public function getTelephone($country = '') {
        if (!$country) {
            return $this->countries[$this->iso_code_2]['telephone'];
        } elseif ($country && isset($this->countries[$country])) {
            return $this->countries[$country]['telephone'];
        } else {
            return '';
        }
    }

    public function getCurcode($country = '') {
        if (!$country) {
            return $this->countries[$this->iso_code_2]['curcode'];
        } elseif ($country && isset($this->countries[$country])) {
            return $this->countries[$country]['curcode'];
        } else {
            return '';
        }
    }

    public function getLanguageCode($country = '') {
        if (!$country) {
            return $this->countries[$this->iso_code_2]['language'];
        } elseif ($country && isset($this->countries[$country])) {
            return $this->countries[$country]['language'];
        } else {
            return '';
        }
    }

    public function getCode() {
        return $this->iso_code_2;
    }

    public function has($country) {
        return isset($this->countries[$country]);
    }

    public function getRealId() {
        $ip = $this->getIpAddress();
        require_once (DIR_SYSTEM . 'geoip/geoip.inc');
        $gi = geoip_open(DIR_SYSTEM . 'geoip/GeoIP.dat', GEOIP_STANDARD);
        $country_code = geoip_country_code_by_addr($gi, $ip);
        geoip_close($gi);
        $code = strtolower($country_code);
        $info = $this->db->query("SELECT country_id FROM country WHERE iso_code_2 = '" . $this->db->escape($code) . "'");
        if ($info->num_rows) {
            $this->real_country_id = $info->row['country_id'];
            return $info->row['country_id'];
        }
        return false;
    }

    private function getDefaultCountry() {
        $ip = $this->getIpAddress();
        require_once (DIR_SYSTEM . 'geoip/geoip.inc');
        $gi = geoip_open(DIR_SYSTEM . 'geoip/GeoIP.dat', GEOIP_STANDARD);
        $country_code = geoip_country_code_by_addr($gi, $ip);
        //$country_name = geoip_country_name_by_addr($gi, $ip);
        geoip_close($gi);

        $code = strtolower($country_code);
        if (!array_key_exists($code, $this->countries)) {
            $country_code = 'sa';
        }
        return $country_code;
    }

    public function getIpAddress() {
        if (!empty($this->request->server['HTTP_CLIENT_IP'])) {
            $tmpip = $this->request->server['HTTP_CLIENT_IP'];
        } elseif (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
            $tmpip = $this->request->server['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($this->request->server['REMOTE_ADDR'])) {
            $tmpip = $this->request->server['REMOTE_ADDR'];
        } else {
            $tmpip = '';
        }

        $result = explode(",", $tmpip);

        $ip = trim(reset($result));

        return $ip;
    }

    private function DetectLanguage() {
        $route = "";
        if (isset($this->request->get['_route_'])) {
            $route = $this->request->get['_route_'];
        } elseif (isset($this->request->get['route'])) {
            $route = $this->request->get['route'];
        }

        $parts = explode('/', $route);

        if(!defined('COUNTRIES')){
            $countries = $this->cache->get('country.getavailablecountries.0.0');
            if (!$countries) {
                $avcountries = $this->db->query(" SELECT c.country_id, iso_code_2, c.language_code AS language, c.curcode, cd.name,c.telecode FROM country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE c.available='1' AND cd.language_id='1' ");
                foreach ($avcountries->rows as $row) {
                    $row['iso_code_2'] = strtolower($row['iso_code_2']);
                    $countries[$row['iso_code_2']] = $row;
                }
                if ($countries) {
                    $this->cache->set('country.getavailablecountries.0.0', $countries);
                }
            }
            define('COUNTRIES', serialize($countries));
        }

        if (is_array($parts) && !empty($parts) && array_key_exists($parts[0], unserialize(COUNTRIES))) {
            array_shift($parts);
        }

        // Language Detection
        $languages = array();
        $result = $this->cache->get('index.languages');
        if (!$result) {
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE status = '1'");
            if ($query->num_rows) {
                $this->cache->set('index.languages', $query->rows);
                $result = $query->rows;
            } else {
                $result = array();
            }
        }

        foreach ($result as $row) {
            $languages[$row['code']] = $row;
        }

        $language_code = '';
        if (is_array($parts) && !empty($parts) && in_array($parts[0], array('ar', 'AR', 'en', 'EN')) && !isset($this->request->get['route'])) {
            if ($parts[0] == 'en' || $parts[0] == 'EN') {
                $language_code = 'en';
            }
            if ($parts[0] == 'ar' || $parts[0] == 'AR') {
                $language_code = 'ar';
            }
            array_shift($parts);
        } else {
            if (isset($language_code) && array_key_exists($language_code, $languages)) {
                $language_code = $language_code;
                $this->session->data['language'] = $language_code; //die(var_dump("here1"));
            } elseif (isset($this->session->data['language']) && array_key_exists($this->session->data['language'], $languages)) {
                $language_code = $this->session->data['language']; //die(var_dump("here2"));
            } elseif (isset($this->request->cookie['language']) && array_key_exists($this->request->cookie['language'], $languages)) {
                $language_code = $this->request->cookie['language']; //die(var_dump("here3"));
            } else {
                $detect = '';

                if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE']) && $this->request->server['HTTP_ACCEPT_LANGUAGE']) {

                    $browser_languages = explode(',', $this->request->server['HTTP_ACCEPT_LANGUAGE']);
                    foreach ($browser_languages as $browser_language) {
                        $browser_language = substr($browser_language, 0, 2);

                        foreach ($languages as $key => $value) {
                            if ($value['status']) {
                                $locale = explode(',', substr($value['locale'], 0, 2));
                                if (in_array($browser_language, $locale)) {
                                    $detect = $key;
                                    break 2;
                                }
                            }
                        }
                    }
                }
                if ($detect) {
                    $language_code = $detect;
                } else {
                    $language_country_id = $this->getRealId();
                    $rows = $this->db->query("SELECT language_code FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int) $language_country_id . "'");
                    if ($rows->num_rows && in_array($rows->row['language_code'], array('ar', 'en'))) {
                        $language_code = $rows->row['language_code'];
                    } else {
                        $language_code = $this->config->get('config_language');
                    }
                }
            }
        }
        return $language_code;
    }

}
