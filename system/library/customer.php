<?php

class Customer {

    private $customer_id;
    private $firstname;
    private $lastname;
    private $customer_group_id;
    private $email;
    private $telephone;
    private $fax;
    private $wishlist;
    private $newsletter;
    private $address_id;
    private $whatsapp;

    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');
        $this->country = $registry->get('country');

        if (isset($this->session->data['customer_id'])) {
            $customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int) $this->session->data['customer_id'] . "' AND status = '1'");

            if ($customer_query->num_rows) {
                $this->customer_id = $customer_query->row['customer_id'];
                $this->firstname = $customer_query->row['firstname'];
                $this->lastname = $customer_query->row['lastname'];
                $this->customer_group_id = $customer_query->row['customer_group_id'];
                $this->email = $customer_query->row['email'];
                $this->telephone = $customer_query->row['telephone'];
                $this->fax = $customer_query->row['fax'];
                $this->whatsapp = $customer_query->row['whatsapp'];
                $this->newsletter = $customer_query->row['newsletter'];
                $this->address_id = $customer_query->row['address_id'];
                $this->wishlist = $this->getWishlist($this->customer_id);
                $this->db->query("UPDATE " . DB_PREFIX . "customer SET ip = '" . $this->db->escape($this->getIPAddress()) . "' WHERE customer_id = '" . (int) $this->customer_id . "'");

                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_ip WHERE customer_id = '" . (int) $this->session->data['customer_id'] . "' AND ip = '" . $this->db->escape($this->getIPAddress()) . "'");

                if (!$query->num_rows) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "customer_ip SET customer_id = '" . (int) $this->session->data['customer_id'] . "', ip = '" . $this->db->escape($this->getIPAddress()) . "', date_added = NOW()");
                }




            } else {
                $this->logout();
            }
        }
    }

    public function login($email, $password, $override = false) {
        if ($override) {
            $customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
        } else {
            $customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1' AND approved = '1'");
        }

        if ($customer_query->num_rows) {
            $this->session->data['customer_id'] = $customer_query->row['customer_id'];

            $this->customer_id = $customer_query->row['customer_id'];
            $this->firstname = $customer_query->row['firstname'];
            $this->lastname = $customer_query->row['lastname'];
            $this->customer_group_id = $customer_query->row['customer_group_id'];
            $this->email = $customer_query->row['email'];
            $this->telephone = $customer_query->row['telephone'];
            $this->fax = $customer_query->row['fax'];
            $this->newsletter = $customer_query->row['newsletter'];
            $this->address_id = $customer_query->row['address_id'];

            $this->db->query("UPDATE " . DB_PREFIX . "customer SET ip = '" . $this->db->escape($this->getIPAddress()) . "' WHERE customer_id = '" . (int) $this->customer_id . "'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "customer_views(customer_id, product_id, date_added, count)  SELECT '". (int)$this->customer_id ."', product_id, date_added, counter FROM guest_view WHERE session_id = '" . $this->db->escape($this->session->getId()) . "' ON DUPLICATE KEY UPDATE customer_views.count = customer_views.count+guest_view.counter, customer_views.date_added=guest_view.date_added ");
            $this->db->query("DELETE FROM " . DB_PREFIX . " guest_view WHERE session_id = '" . $this->db->escape($this->session->getId()) . "' ");
            
            $email = base64_encode(serialize($this->email));
            setcookie(EMAIL_COOKIE_NAME, $email, COOKIES_PERIOD, '/', $this->request->server['HTTP_HOST']);




            $salesData = [];
            $salesData['contact'] = [];
            $salesData['contact']['email'] = $customer_query->row['email'];
            if(!empty($customer_query->row['firstname']) && !empty($customer_query->row['lastname'])){
                $salesData['contact']['name'] = $customer_query->row['firstname'].' '.$customer_query->row['lastname'];
            }
            if(!empty($customer_query->row['telephone'])){
                $salesData['contact']['phone'] = $customer_query->row['telephone'];
            }
            if(!empty($customer_query->row['birthdate'])){
                $salesData['birthday'] = str_replace("-","",$customer_query->row['birthdate']);
            }
            $salesData['contact']['address']['country'] = $this->country->getCode();

            if(!empty($customer_query->row['gender'])){
                $salesData['properties'] = ["custom.sex"=> ($customer_query->row['gender'] == 'male') ? 'm' : 'f'];
            }


            //check order before

            $isOrderBefore = $this->db->query("select count(*) as count from `order` where customer_id = ".$this->customer_id);
            if($isOrderBefore->row['count']>0){
                $salesData['contact']['state'] = 'CUSTOMER';
            }


            $salesData['tags'] = [];
            $salesData['tags'][] = $this->country->getCode();
            $salesData['tags'][] = (strtolower($this->config->get('config_language')) == 'ar') ? 'ARABIC' : 'ENGLISH';
            $salesData['properties']['country'] = $this->country->getCode();
            $salesData['properties']['language'] = (strtolower($this->config->get('config_language')) == 'ar') ? 'ARABIC' : 'ENGLISH';
            if(!empty($salesData['gender'])){
                $salesData['tags'][] = $customer_query->row['gender'];
                $salesData['properties']['gender'] = $customer_query->row['gender'];
            }

            if(empty($this->config->get("config_language"))){
                $salesData['lang'] = $this->config->get("config_language");
            }
            SalesManago::getInstance()->registerUser($salesData);
            SalesManago::getInstance()->eventLogin($customer_query->row['email']);


            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        unset($this->session->data['customer_id']);
        unset($this->session->data['openModal']);
        SalesManago::getInstance()->eventLogout(self::getCookieEmail());
        
        $this->customer_id = '';
        $this->firstname = '';
        $this->lastname = '';
        $this->customer_group_id = '';
        $this->email = '';
        $this->telephone = '';
        $this->fax = '';
        $this->newsletter = '';
        $this->address_id = '';
    }

    public function isLogged() {
        return $this->customer_id;
    }

    public function getId() {
        return $this->customer_id;
    }

    public function getIPAddress() {
        return $this->country->getIpAddress();
    }

    public function getFirstName() {
        return $this->firstname;
    }

    public function getLastName() {
        return $this->lastname;
    }

    public function getGroupId() {
        return $this->customer_group_id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getTelephone() {
        return $this->telephone;
    }

    public function getFax() {
        return $this->fax;
    }

    public function getNewsletter() {
        return $this->newsletter;
    }

    public function getAddressId() {
        return $this->address_id;
    }

    public function getBalance() {
        $query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int) $this->customer_id . "'");

        return $query->row['total'];
    }

    public function getWishlist() {
        $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int) $this->customer_id . "'");

        return $query->rows;
    }

    public function getRewardPoints() {
        $query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int) $this->customer_id . "'");

        return $query->row['total'];
    }

    public function getWhatsApp() {
        return $this->whatsapp;
    }

    public static function getCookieEmail(){


        if(isset($_COOKIE[EMAIL_COOKIE_NAME])){
            $email = $_COOKIE[EMAIL_COOKIE_NAME];
            $email = base64_decode($email);
            $email = unserialize($email);
            return $email;
        }
        return false;
    }
}
