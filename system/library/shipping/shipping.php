<?php

namespace Shipping;

abstract class Shipping {

    public $registry;
    public $databaseID;
    public $error;
    public $account_no;
    public $account_pass;
    public $account_username;
    public $account_customercode;

    public function __construct($registry, $databaseID) {
        $this->databaseID = $databaseID;
        $this->registry = $registry;
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');
        $this->language = $registry->get('language');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');
        $this->cache = $registry->get('cache');

        $data = $this->db->query("SELECT * FROM ship_company sc LEFT JOIN ship_company_description scd ON(sc.ship_company_id=scd.ship_company_id) WHERE scd.language_id = '" . $this->config->get('config_language_id') . "' AND sc.ship_company_id='" . (int) $this->databaseID . "' ");
        if ($data->num_rows) {
            $this->account_no = $data->row['access_no'];
            $this->account_pass = $data->row['password']; //CONFIG_FFS_PASS;
            $this->account_username = $data->row['username']; //CONFIG_FFS_USERNAME;
            $this->account_customercode = $data->row['access_no']; //CONFIG_FFS_CUSTOMER_CODE;

        }
    }

    public function __get($key) {
        return $this->registry->get($key);
    }

    public function __set($key, $value) {
        $this->registry->set($key, $value);
    }

    abstract public function BookingRequest();

    abstract public function BookingTrack($AwbNo, $country_id, $shipping_company );

    abstract public function CreateBooking();

    abstract public function GetBookingDetails();

    abstract public function GetRateQuery();

    abstract public function LogData($AwbNo, $country_id, $shipping_company );

    abstract public function LogDataBooking();

    abstract public function NewOrder();

    abstract public function NewOrderWithAutoAWBNO();

    abstract public function NewOrderWithAutoAWBNORef();

    abstract public function WebCustomerSearch();

    abstract public function WebSMSABookingInsert();
}
