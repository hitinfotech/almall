<?php

/**
 * Created by PhpStorm.
 * User: hammash
 * Date: 12/06/17
 * Time: 10:02 ص
 */
class Samsashipping extends Shipping\Shipping
{


    public $client;

    public function __construct($registry, $databaseID)
    {
        parent::__construct($registry, $databaseID);

        $this->client = new SoapClient("http://track.smsaexpress.com/SECOM/SMSAwebService.asmx?wsdl");

    }

    public function BookingRequest($data = array())
    {

        $data = array_merge(['passKey' => $this->account_no], $data);
        $res = $this->client->addShipment($data);

        if ($res && isset($res->addShipmentResult) && ctype_digit($res->addShipmentResult)) {
            return $res->addShipmentResult;
        }

        return false;

    }


    Public Function returnShipment($awbNo)
    {
        //The method name in DOC is “stoShipment”

        $passKey = $this->account_no;
        $awbNo = (int)$awbNo;


        try {
            $res = $this->client->stoShipment([
                'awbNo' => $awbNo,
                'passkey' => $passKey,
            ]);


            if ($res && isset($res->stoShipmentResult)) {

                $r = explode("New #", $res->stoShipmentResult);
                return (isset($r[1])) ? $r[1] : false;

            }
            return false;

        } catch (Exception $e) {
            var_dump($e->getMessage());
        }

    }

    public function LogData($AwbNo, $country_id, $shipping_company)
    {
        // The method name in DOC is getTracking(awbNo,passKey)

        $passKey = $this->account_no;

        try {
            $res = $this->client->getTracking([
                'passkey' => $passKey,
                'awbNo' => $AwbNo,
            ]);

            $xml = simplexml_load_string($res->getTrackingResult->any);

            $title = 'Order Tracking Details for KSA';
            if ($country_id == 221) {
                $title = 'Order Tracking Details for UAE';
            }

            $company_details = '';
            if ($shipping_company != '') {
                $company_details = '<div><b>' . 'Shipping Company:</b> ' . $shipping_company . '</div><br>';
            }
            //print_r($result);
            $string = '<h5>' . $title . '</h5>';
            $string .= '<div><b>' . 'AWBNo: </b>' . $AwbNo . '</div>';
            $string .= $company_details;
            $string .= '<div class="table-responsive">';
            $string .= '<table class="table table-bordered table-hover">';
            $string .= '<thead>';
            $string .= '<tr>';
            $string .= '<td class="left">Date</td>';
            $string .= '<td class="left">Time</td>';
            $string .= '<td class="left">Details</td>';
            $string .= '<td class="left">Activity</td>';
            $string .= '<td class="left">Location</td>';
            $string .= '</tr>';
            $string .= '</thead>';
            $string .= '<tbody>';

            foreach (get_object_vars($xml->NewDataSet) as $row) {
                $row = (array)$row;

                $string .= '<tr>';
                $string .= '<td>' . substr($row['Date'], 0, 11) . '</td>';
                $string .= '<td>' . substr($row['Date'], 11, 6) . '</td>';
                $string .= '<td>' . $row['Details'] . '</td>';
                $string .= '<td>' . $row['Activity'] . '</td>';
                $string .= '<td>' . $row['Location'] . '</td>';
                $string .= '</tr>';
            }
            $string .= '</table>';
            $string .= '</div>';
            return ($string);
            //return( $res);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }


    }

    public function BookingTrack($AwbNo, $country_id, $shipping_company)
    {
    }

    public function shippingTrack($AwbNo)
    {

        $passKey = $this->account_no;

        try {

            $res = $this->client->getTracking([
                'passkey' => $passKey,
                'awbNo' => $AwbNo,
            ]);

            $xml = simplexml_load_string($res->getTrackingResult->any);

            $res = get_object_vars($xml->NewDataSet);

            $result = (array)$res['Tracking'][0];

            $shipment_status = $result['Activity'];

            return $shipment_status == "RETURNED TO CLIENT" || $shipment_status == "PROOF OF DELIVERY CAPTURED";

        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function CreateBooking()
    {
        // TODO: Implement CreateBooking() method.
    }

    public function GetBookingDetails()
    {
        // TODO: Implement GetBookingDetails() method.
    }

    public function GetRateQuery()
    {
        // TODO: Implement GetRateQuery() method.
    }

    public function LogDataBooking()
    {
        // TODO: Implement LogDataBooking() method.
    }

    public function NewOrder()
    {
        // TODO: Implement NewOrder() method.
    }

    public function NewOrderWithAutoAWBNO()
    {
        // TODO: Implement NewOrderWithAutoAWBNO() method.
    }

    public function NewOrderWithAutoAWBNORef()
    {
        // TODO: Implement NewOrderWithAutoAWBNORef() method.
    }

    public function WebCustomerSearch()
    {
        // TODO: Implement WebCustomerSearch() method.
    }

    public function WebSMSABookingInsert()
    {
        // TODO: Implement WebSMSABookingInsert() method.
    }

}


