<?php

class Url_custom {

    private $query = array(
        'product' => 'product_id=',
        'bundle' => 'bundle_id=',
        'news' => "news_id=",
        'offer' => "offer_id=",
        'mall' => "mall_id=",
        'store' => "shop_id=",
        'brand' => "brand_id=",
        'look' => "look_id=",
        'tip' => "tip_id=",
        'tag' => "tag_id=",
        'top_pages' => "top_pages_id="
    );
    private $pre_fix_ar = array(
        'product' => 'products/',
        'news' => "الأخبار/news/",
        'offer' => "offers-promotions/",
        'mall' => "malls/",
        'store' => "shops/",
        'bundle' => "bundles/",
        'brand' => "الماركات/brands/",
        'look' => "looks/",
        'tip' => "نصائح التسوق/",
        'tag' => "hash_tags/",
        'top_pages' => "top5/",
    );
    private $pre_fix_en = array(
        'product' => 'products/',
        'news' => "news/",
        'offer' => "offers-promotions/",
        'mall' => "malls/",
        'bundle' => "bundles/",
        'store' => "shops/",
        'brand' => "brands/",
        'look' => "looks/",
        'tip' => "shopping-tips/",
        'tag' => "hash_tags/",
        'top_pages' => "top5/",
    );

    public function __construct($registry) {
        $this->db = $registry->get('db');
        $this->config = $registry->get('config');
    }

    public function create_URL($type, $entity_id) {
        if (array_key_exists($type, $this->query)) {
            $query = $this->get_query($type, $entity_id);

            $keyword_en = $this->get_keyword($type, $entity_id, 1);
            $keyword_ar = $this->get_keyword($type, $entity_id, 2);

            $this->db->query("DELETE FROM url_alias_ar WHERE query='" . $this->db->escape($query) . "'");
            $this->db->query("INSERT INTO url_alias_ar SET query='" . $this->db->escape($query) . "', keyword='" . $this->db->escape($keyword_ar) . "'");

            $this->db->query("DELETE FROM url_alias_en WHERE query='" . $this->db->escape($query) . "'");
            $this->db->query("INSERT INTO url_alias_en SET query='" . $this->db->escape($query) . "', keyword='" . $this->db->escape($keyword_en) . "'");
        }
    }

    private function get_query($type, $entity_id) {
        return $this->query[$type] . $entity_id;
    }

    private function get_keyword($type, $entity_id, $language_id) {
        $field = 'name';
        $keyword = '';

        if (in_array($type, array('news', 'offer', 'look'))) {
            $field = 'title';
        }

        if ($type == 'bundle') {
            $info = $this->db->query(" SELECT name FROM `pb_bundles` WHERE `id` = '" . (int) $entity_id . "' ");
        } else {
            $info = $this->db->query(" SELECT `{$field}` name FROM `{$type}_description` WHERE `{$type}_id` = '" . (int) $entity_id . "' AND language_id='" . (int) $language_id . "' ");
        }


        if ($info->num_rows) {
            if ($type == 'bundle') {
                $name_encode = $info->row['name'];
                $name = json_decode($name_encode);
                $data['name'] = $name->{$language_id};
            } else {
                $data = $info->row;
            }
        } else {
            $data = array();
        }

        if (!empty($data)) {
            $keyword = $data['name'];

            if ($language_id == 2 && in_array($type, array('brand'))) {
                $info = $this->db->query(" SELECT `{$field}` name FROM `{$type}_description` WHERE `{$type}_id` = '" . (int) $entity_id . "' AND language_id='2' ");
                if ($info->num_rows) {
                    $keyword .= "-" . $info->row['name'];
                }
            }

            if (in_array($type, array('store'))) {
                $info = $this->db->query(" SELECT md.name FROM store s LEFT JOIN `mall_description` md ON(s.mall_id=md.mall_id) WHERE `store_id` = '" . (int) $entity_id . "' AND language_id='" . (int) $language_id . "' ");
                if ($info->num_rows) {
                    $keyword .= "-" . $info->row['name'];
                }
            }

            if ($language_id == 1) {
                return $this->pre_fix_en[$type] . "{$entity_id}-" . $this->str_limit($this->str_slug_utf8($keyword), 1000, '');
            } else {
                return $this->pre_fix_ar[$type] . "{$entity_id}-" . $this->str_limit($this->str_slug_utf8($keyword), 1000, '');
            }
        } else {
            return false;
        }
    }

    public function str_limit($value, $limit = 100, $end = '...') {
        if (mb_strlen($value, 'UTF-8') <= $limit) {
            return $value;
        }
        return rtrim(mb_substr($value, 0, $limit, 'UTF-8')) . $end;
    }

    public function str_slug_utf8($title, $separator = '-') {

        // Convert all dashes/underscores into separator
        $flip = $separator == '-' ? '_' : '-';

        $title = preg_replace('![' . preg_quote($flip) . ']+!u', $separator, $title);

        // Remove all characters that are not the separator, letters, numbers, or whitespace.
        $title = preg_replace('![^' . preg_quote($separator) . '\pL\pN\s]+!u', '', mb_strtolower($title, 'UTF-8'));

        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('![' . preg_quote($separator) . '\s]+!u', $separator, $title);

        return trim($title, $separator);
    }

    function clean_url($str) {
        $clean = preg_replace('/[^a-zA-Z0-9\x{0660}-\x{0669}\x{0640}-\x{064A}\x{0630}-\x{063A}\x{0622}-\x{062F}\s-_]/u', '', trim($str));
        $clean = mb_strtolower(trim($clean, '-'), 'UTF-8');
        $clean = preg_replace("/[\/_|+ -]+/", '-', $clean);
        $clean = str_replace('&', '', $clean);
        return $clean;
    }

}
