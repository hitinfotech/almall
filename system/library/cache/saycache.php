<?php

namespace Cache;

error_reporting(E_ALL);
ini_set('display_errors', 'On');

class SayCache {

    private $expire = REDIS_EXPIRE;
    private $redis = NULL;

    public function __construct() {
        $this->redis = new \Redis();
        $this->redis->connect(REDIS_IP, REDIS_PORT, REDIS_CON_TIME);
        $this->redis->auth(REDIS_AUTH);
        $this->redis->select(REDIS_DBASE_DB);
    }

    public function get($key) {
        if (defined('HTTP_CATALOG')) {
            $key = 'admin.' . $key;
        } else {
            $key = 'catalog.' . $key;
        }

        $value = $this->redis->get($key);
        $data = @json_decode($value, true);
        if ($data) {
            return $data;
        }
        return $value;
    }

    public function set($key, $value, $expire = 0) {

        if (defined('HTTP_CATALOG')) {
            $key = 'admin.' . $key;
        } else {
            $key = 'catalog.' . $key;
        }
        if (is_array($value)) {
            $this->redis->set($key, json_encode($value));
        } else {
            $this->redis->set($key, $value);
        }

        //EXPIRE
        $expire_limit = (int) $expire;

        if ($expire_limit <= 0) {
            $expire_limit = $this->expire;
        }

        $this->redis->expire($key, $expire_limit);
    }

    public function keys($key) {
        $value = $this->redis->keys($key);
        $data = @json_decode($value, true);
        if ($data) {
            return $data;
        }
        return $value;
    }

    public function delete($key) {
        $this->redis->delete($key);
    }

}

class FileCache {

    private $expire = 3600;

    public function get($key) {
        $files = glob(DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

        if ($files) {
            $cache = file_get_contents($files[0]);

            $data = unserialize($cache);

            foreach ($files as $file) {
                $time = substr(strrchr($file, '.'), 1);

                if ($time < time()) {
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }
            }

            return $data;
        }
    }

    public function set($key, $value) {
        $this->delete($key);

        $file = DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.' . (time() + $this->expire);

        $handle = fopen($file, 'w');

        fwrite($handle, serialize($value));

        fclose($handle);
    }

    public function delete($key) {
        $files = glob(DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

        if ($files) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }

}
