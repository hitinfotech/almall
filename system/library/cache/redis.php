<?php
namespace Cache;


class Redis extends \Redis{

    public function __construct() {
        $this->connect(REDIS_HOST, REDIS_PORT, REDIS_TIMEOUT);
        $this->auth(REDIS_PASS); $this->select(0);

        $this->setOption(static::OPT_SERIALIZER, static::SERIALIZER_PHP);
    }
}
