<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FirstFlight extends Shipping\Shipping{

    public function CreateBooking($data = array()) {
        $data = array_merge(['Username' => $this->account_username, 'Password' => $this->account_pass], $data);
        $client = new SoapClient("http://www.wsdl.integraontrack.com/ServiceFF.asmx?op=CreateBooking&wsdl");

        try {
            $res = $client->CreateBooking($data);
            return( $res);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function GetBookingDetails($data = array()) {

    }

    public function GetRateQuery($data = array()) {

    }

    public function BookingTrack($AwbNo = '', $country_id,$shipping_company='') {
        $data = array('AccountNo' => $this->account_username,
            'AccountPWD' => $this->account_pass,
            'AwbNo' => $AwbNo);

        $client = new SoapClient("http://www.wsdl.integraontrack.com/ServiceFF.asmx?wsdl");

        try {
            $res = $client->BookingTrack($data);

            $xml = simplexml_load_string($res->BookingTrackResult->any);
            if ($xml === false) {
                foreach (libxml_get_errors() as $error) {
                    //echo "<br>", $error->message;
                }
            } else {

                $result = array();
                foreach (get_object_vars($xml->NewDataSet) as $row) {
                    foreach ($row as $record) {
                        $result[] = get_object_vars($record);
                    }
                }
                $title = 'Order Tracking Details for KSA';
                if ($country_id == 221) {
                    $title = 'Order Tracking Details for UAE';
                }

                $company_details = '';
                if($shipping_company !=''){
                  $company_details = '<div>'.'Shipping Company: '.$shipping_company .'</div><br>';
                }

                //print_r($result);
                $string = '<h5>' . $title . '</h5>';
                $string .= '<div>'.'AWBNo: '.$AwbNo .'</div>';
                $string .= $company_details;
                $string .= '<div class="table-responsive">';
                $string .='<table class="table table-bordered table-hover">';
                $string .='<thead>';
                $string .='<tr>';
                $string .='<td class="left">Date</td>';
                $string .='<td class="left">Time</td>';
                //$string .='<td class="left">Status</td>';
                $string .='<td class="left">DeliveredTo</td>';
                $string .='<td class="left">Remarks</td>';
                $string .='<td class="left">Location</td>';
                $string .='</tr>';
                $string .='</thead>';
                $string .='<tbody>';

                foreach ($result as $row) {
                    $string.='<tr>';
                    $string .= '<td>' . substr($row['Transdate'], 0, 10) . '</td>';
                    $string .= '<td>' . $row['TransTime'] . '</td>';
                    //$string .= '<td>' . $row['Status'] . '</td>';
                    $string .= '<td>' . $row['DeliveredTo'] . '</td>';
                    $string .= '<td>' . $row['Remarks'] . '</td>';
                    $string .= '<td>' . $row['Location'] . '</td>';

                    $string.='</tr>';
                }
                $string .= '</table>';
                $string .= '</div>';
            }
            return ($string);
            //return( $res);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function LogData($AwbNo = '', $country_id, $shipping_company='') {
        $data = array('AccountNo' => $this->account_username,
            'AccountPWD' => $this->account_pass,
            'AwbNo' => $AwbNo);

        $client = new SoapClient("http://www.wsdl.integraontrack.com/FFTrack.asmx?wsdl");

        try {
            $res = $client->QueryData($data);

            $xml = simplexml_load_string($res->QueryDataResult->any);
            if ($xml === false) {
                foreach (libxml_get_errors() as $error) {
                    //echo "<br>", $error->message;
                }
            } else {

                $result = array();
                foreach (get_object_vars($xml->NewDataSet) as $row) {
                    foreach ($row as $record) {
                        $result[] = get_object_vars($record);
                    }
                }
                $title = 'Order Tracking Details for KSA';
                if ($country_id == 221) {
                    $title = 'Order Tracking Details for UAE';
                }

                $company_details = '';
                if($shipping_company !=''){
                  $company_details = '<div>'.'Shipping Company: '.$shipping_company .'</div><br>';
                }
                //print_r($result);
                $string = '<h5>' . $title . '</h5>';
                $string .= '<div>'.'AWBNo: '.$AwbNo .'</div>';
                $string .= $company_details;
                $string .= '<div class="table-responsive">';
                $string .='<table class="table table-bordered table-hover">';
                $string .='<thead>';
                $string .='<tr>';
                $string .='<td class="left">Date</td>';
                $string .='<td class="left">Time</td>';
                //$string .='<td class="left">Status</td>';
                $string .='<td class="left">DeliveredTo</td>';
                $string .='<td class="left">Remarks</td>';
                $string .='<td class="left">Location</td>';
                $string .='</tr>';
                $string .='</thead>';
                $string .='<tbody>';

                foreach ($result as $row) {
                    $string.='<tr>';
                    $string .= '<td>' . substr($row['Transdate'], 0, 10) . '</td>';
                    $string .= '<td>' . $row['TransTime'] . '</td>';
                    //$string .= '<td>' . $row['Status'] . '</td>';
                    $string .= '<td>' . $row['DeliveredTo'] . '</td>';
                    $string .= '<td>' . $row['Remarks'] . '</td>';
                    $string .= '<td>' . $row['Location'] . '</td>';

                    $string.='</tr>';
                }
                $string .= '</table>';
                $string .= '</div>';
            }
            return ($string);
            //return( $res);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function LogDataBooking($data = array()) {

    }

    public function NewOrder($data = array()) {
        /*  $data = array_merge(['Username' => $this->account_username, 'Password' => $this->account_pass], $data);

          $client = new SoapClient("http://www.wsdl.integraontrack.com/ServiceFF.asmx?op=NewOrder&wsdl");
          try {
          $res = $client->NewOrderWithAutoAWBNO($data);
          return ($res);
          } catch (Exception $e) {
          var_dump($e->getMessage());
          } */
    }

    public function NewOrderWithAutoAWBNO($data = array()) {
        $data = array_merge(['Username' => $this->account_username, 'Password' => $this->account_pass], $data);

        $client = new SoapClient("http://www.wsdl.integraontrack.com/ServiceFF.asmx?op=NewOrder&wsdl");
        try {
            $res = $client->NewOrderWithAutoAWBNO($data);
            if (strpos(json_encode($res), 'Sucessfully Updated') !== false) {
                $AWBNo = explode("AWBNO: ", json_encode($res));
                $AWBNo = explode('"}', $AWBNo[1]);
                return $AWBNo[0];
            }else{
              return  array('error' =>$res->NewOrderWithAutoAWBNOResult);
            }
            return false;
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function BookingRequest($data = array()) {
        $data = array_merge(['Username' => $this->account_username, 'Password' => $this->account_pass], $data);
        $client = new SoapClient("http://www.wsdl.integraontrack.com/ServiceFF.asmx?op=BookingRequest&wsdl");

        try {
            $res = $client->BookingRequest($data);
            var_dump($res);
            $result = json_encode($res);

            if (strpos($result, 'Sucessfully Updated') !== false) {
                return rtrim(substr($result, (int) strpos($result, 'No:') + 4), '"}');
            }
            return false;
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function NewOrderWithAutoAWBNORef($data = array()) {

    }

    public function WebCustomerSearch($data = array()) {

    }

    public function WebSMSABookingInsert($data = array()) {

    }

    public function shippingTrack($AwbNo = '',$country_id) {
        $data = array('AccountNo' => $this->account_username,
            'AccountPWD' => $this->account_pass,
            'AwbNo' => $AwbNo);

        $client = new SoapClient("http://www.wsdl.integraontrack.com/FFTrack.asmx?wsdl");

        try {
            $res = $client->QueryData($data);
            $xml = simplexml_load_string($res->QueryDataResult->any);
            if ($xml === false) {
                return false;
            } else {

                $result = array();
                foreach (get_object_vars($xml->NewDataSet) as $row) {
                    foreach ($row as $record) {
                        $result[] = get_object_vars($record);
                        $status = (array)$record->Status;
                        if(trim(strtoupper($status[0])) == 'POD'){
                          $delivered = (array)$record->DeliveredTo;
                          return ['result'=>'success','delivered_to' => $delivered[0]];
                        }
                    }
                }
                return false;
              }
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }
}
