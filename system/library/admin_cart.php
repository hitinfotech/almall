<?php

class Admin_Cart {

    private $data = array();

    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->customer = $registry->get('customer');
        $this->session = $registry->get('session');
        $this->country = $registry->get('country');
        $this->request = $registry->get('request');
        $this->db = $registry->get('db');
        $this->tax = $registry->get('tax');
        $this->log = $registry->get('log');
        $this->weight = $registry->get('weight');

        // Remove all the expired carts with no customer ID
        $this->db->query("DELETE FROM " . DB_PREFIX . "admin_cart WHERE customer_id IS NULL AND date_added < DATE_SUB(NOW(), INTERVAL 1 HOUR)");

        if ($this->customer->getId()) {
            // We want to change the session ID on all the old items in the customers cart
            $this->db->query("UPDATE " . DB_PREFIX . "admin_cart SET session_id = '" . $this->db->escape($this->session->getId()) . "' WHERE customer_id = '" . (int) $this->customer->getId() . "'");

            // Once the customer is logged in we want to update the customer ID on all items he has
            $cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "admin_cart WHERE customer_id IS NULL AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

            foreach ($cart_query->rows as $cart) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "admin_cart WHERE cart_id = '" . (int) $cart['cart_id'] . "'");

                if($this->session->data['order_id']){
                    $order_id = $this->session->data['order_id'];
                } else {
                    $order_id = 0;
                }
                // The advantage of using $this->add is that it will check if the products already exist and increaser the quantity if necessary.
                //$this->add($cart['product_id'], $cart['quantity'], json_decode($cart['option']), $cart['recurring_id']);
                $this->add($cart['product_id'], $cart['quantity'], json_decode($cart['option']), $cart['recurring_id'], $cart['otp_id'], $cart['swap_id'],$cart['op_size']);
            }
        }
    }

    public function getProducts($order_id = 0) {
        $product_data = array();

        if($order_id==0 && isset($_GET['order_id'])){
          $order_id = $this->getOrderNumber($_GET['order_id']);
        }

        $customer_id = $this->customer->getId();
        if($customer_id > 0){
            $customer_id = (int)$customer_id;
        }else{
            $customer_id = NULL;
        }

        if(NULL == $customer_id){
            $cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "admin_cart WHERE customer_id IS NULL AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
        }else{
            $cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "admin_cart WHERE customer_id = '" . $customer_id . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
        }

        foreach ($cart_query->rows as $cart) {

            $stock = true;

            $product_query = $this->db->query("SELECT p.*,p2s.*,pd.*,cp2c.is_fbs as is_seller_fbs,(p.quantity - p.fbs_lost_quantity - p.fbs_damaged_quantity) as quantity  FROM " . DB_PREFIX . "product_to_store p2s LEFT JOIN " . DB_PREFIX . "product p ON (p2s.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) INNER JOIN customerpartner_to_product cp2p ON(p.product_id=cp2p.product_id) INNER JOIN customerpartner_to_customer cp2c ON(cp2p.customer_id=cp2c.customer_id) WHERE cp2c.available_country IN(0, '" . (int) $this->country->getId() . "') AND p2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND p2s.product_id = '" . (int) $cart['product_id'] . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p.date_available <= NOW() ");
            $order_product_query = $this->db->query("SELECT quantity, price, total FROM order_product where deleted='0' AND order_id=".(int) $order_id . " AND product_id=".(int)$cart['product_id'] );
            $option_data = array();
            $otp_data = array();
            if ($cart['otp_id']) {
                $otpcount = 0;
                $otp_options = array();
                $otp_check_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "otp_option_value WHERE `id` = '" . $cart['otp_id'] . "' LIMIT 1");
                if ($otp_check_query->num_rows) {
                    $otp_options = array(
                        'parent' => $otp_check_query->row['parent_option_id'],
                        'child' => $otp_check_query->row['child_option_id'],
                        'grandchild' => $otp_check_query->row['grandchild_option_id']
                    );
                    foreach (json_decode($cart['option']) as $otp_option_id => $otp_option_value_id) {
                        if (in_array($otp_option_id, $otp_options)) {
                            $otpcount++;
                            $otp_option_query = $this->db->query("SELECT o.type,o.cod, od.name FROM `" . DB_PREFIX . "option` o LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE o.option_id = '" . $otp_option_id . "' AND od.language_id = '" . $this->config->get('config_language_id') . "'");
                            if ($otp_option_id == $otp_options['parent']) {
                                $otp_option_value_query = $this->db->query("SELECT ovd.name, otpd.model, otpd.quantity, otpd.subtract, otpd.price_prefix, otpd.price, otpd.special, otpd.weight_prefix, otpd.weight FROM " . DB_PREFIX . "otp_option_value otp INNER JOIN " . DB_PREFIX . "otp_data otpd ON otp.id = otpd.otp_id LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (otp.parent_option_value_id = ovd.option_value_id) WHERE otp.id = '" . $cart['otp_id'] . "' AND ovd.language_id = '" . $this->config->get('config_language_id') . "'");
                                if ($otp_option_value_query->row['subtract'] == 1 && (!$otp_option_value_query->row['quantity'] || ($otp_option_value_query->row['quantity'] < $cart['quantity']))) {
                                    $stock = false;
                                }
                                $option_data[] = array(
                                    'product_option_id' => $otp_option_id,
                                    'product_option_value_id' => $otp_option_value_id,
                                    'option_id' => $otp_option_id,
                                    'option_value_id' => $otp_option_value_id,
                                    'name' => $otp_option_query->row['name'],
                                    'value' => $otp_option_value_query->row['name'],
                                    'cod' => $otp_option_query->row['cod'],
                                    'type' => $otp_option_query->row['type']
                                );
                                $otp_data['otp_id'] = $cart['otp_id'];
                                $otp_data['model'] = $otp_option_value_query->row['model'];
                                $otp_data['price_prefix'] = $otp_option_value_query->row['price_prefix'];
                                $otp_data['price'] = $otp_option_value_query->row['price'];
                                $otp_data['special'] = $otp_option_value_query->row['special'];
                                $otp_data['weight_prefix'] = $otp_option_value_query->row['weight_prefix'];
                                $otp_data['weight'] = $otp_option_value_query->row['weight'];
                            } elseif ($otp_option_id == $otp_options['child']) {
                                $otp_option_value_query = $this->db->query("SELECT ovd.name, otpd.quantity, otpd.subtract FROM " . DB_PREFIX . "otp_option_value otp INNER JOIN " . DB_PREFIX . "otp_data otpd ON otp.id = otpd.otp_id LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (otp.child_option_value_id = ovd.option_value_id) WHERE otp.id = '" . $cart['otp_id'] . "' AND ovd.language_id = '" . $this->config->get('config_language_id') . "'");
                                if ($otp_option_value_query->row['subtract'] && (!$otp_option_value_query->row['quantity'] || ($otp_option_value_query->row['quantity'] < $cart['quantity']))) {
                                    $stock = false;
                                }
                                $option_data[] = array(
                                    'product_option_id' => $otp_option_id,
                                    'product_option_value_id' => $otp_option_value_id,
                                    'option_id' => $otp_option_id,
                                    'option_value_id' => $otp_option_value_id,
                                    'name' => $otp_option_query->row['name'],
                                    'value' => $otp_option_value_query->row['name'],
                                    'cod' => $otp_option_query->row['cod'],
                                    'type' => $otp_option_query->row['type']
                                );
                            } elseif ($otp_option_id == $otp_options['grandchild']) {
                                $otp_option_value_query = $this->db->query("SELECT ovd.name, otpd.quantity, otpd.subtract FROM " . DB_PREFIX . "otp_option_value otp INNER JOIN " . DB_PREFIX . "otp_data otpd ON otp.id = otpd.otp_id LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (otp.grandchild_option_value_id = ovd.option_value_id) WHERE otp.id = '" . $cart['otp_id'] . "' AND ovd.language_id = '" . $this->config->get('config_language_id') . "'");
                                if ($otp_option_value_query->row['subtract'] && (!$otp_option_value_query->row['quantity'] || ($otp_option_value_query->row['quantity'] < $cart['quantity']))) {
                                    $stock = false;
                                }
                                $option_data[] = array(
                                    'product_option_id' => $otp_option_id,
                                    'product_option_value_id' => $otp_option_value_id,
                                    'option_id' => $otp_option_id,
                                    'option_value_id' => $otp_option_value_id,
                                    'name' => $otp_option_query->row['name'],
                                    'value' => $otp_option_value_query->row['name'],
                                    'type' => $otp_option_query->row['type']
                                );
                            }
                        }
                    }
                } else {
                    $this->remove($key);
                }
            }

            if ($product_query->num_rows) {
                $option_price = 0;
                $option_points = 0;
                $option_weight = 0;
                $original_order_quantity = 0;
                $original_order_price = 0;

                $product_original_order = $this->db->query("SELECT order_product_id, quantity, price FROM order_product WHERE order_id = '".(int)$order_id."' AND product_id = '" . (int) $cart['product_id'] . "' AND deleted='0' ");
                if($product_original_order->num_rows > 0) {
                    if(!empty(json_decode($cart['option']))) {
                        foreach (json_decode($cart['option']) as $product_option_id => $value) {
                            $product_option_original_order = $this->db->query(" SELECT * FROM order_option WHERE order_id = '".(int)$order_id."' AND order_product_id = '". (int)$product_original_order->row['order_product_id'] ."' AND product_option_id = '". (int)$product_option_id ."' AND product_option_value_id = '". (int)$value ."' AND deleted='0' ");
                            if($product_option_original_order->num_rows > 0) {
                                $original_order_quantity = $product_original_order->row['quantity'];
                            }
                        }
                    }

                    $original_order_quantity = $product_original_order->row['quantity'];
                    $original_order_price = $product_original_order->row['price'];
                } else {
                    $original_order_price = (isset($order_product_query->row['price']) && $order_product_query->row['price'] > 0) ? $order_product_query->row['price'] : $product_query->row['price'];
                }

                $this->log->write("product_id:{$cart['product_id']} ++++ price:{$original_order_price}   ++++quantity:$original_order_quantity");

                //$option_data = array();

                foreach (json_decode($cart['option']) as $product_option_id => $value) {
                    $option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.front_name as name, o.type,o.cod FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.deleted != '1' AND po.product_option_id = '" . (int) $product_option_id . "' AND po.product_id = '" . (int) $cart['product_id'] . "' AND od.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                    if ($option_query->num_rows) {
                        if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image' || $option_query->row['type'] == 'list' || $option_query->row['type'] == 'size') {
                            $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.deleted != '1' AND pov.product_option_value_id = '" . (int) $value . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                            if ($option_value_query->num_rows) {
                                if ($option_value_query->row['price_prefix'] == '+') {
                                    $option_price += $option_value_query->row['price'];
                                } elseif ($option_value_query->row['price_prefix'] == '-') {
                                    $option_price -= $option_value_query->row['price'];
                                }

                                if ($option_value_query->row['points_prefix'] == '+') {
                                    $option_points += $option_value_query->row['points'];
                                } elseif ($option_value_query->row['points_prefix'] == '-') {
                                    $option_points -= $option_value_query->row['points'];
                                }

                                if ($option_value_query->row['weight_prefix'] == '+') {
                                    $option_weight += $option_value_query->row['weight'];
                                } elseif ($option_value_query->row['weight_prefix'] == '-') {
                                    $option_weight -= $option_value_query->row['weight'];
                                }

                                if ($option_value_query->row['subtract'] && (!($option_value_query->row['quantity']+$original_order_quantity) || (($option_value_query->row['quantity']+$original_order_quantity) < $cart['quantity']))) {
                                    $stock = false;
                                }

                                $option_data[] = array(
                                    'product_option_id' => $product_option_id,
                                    'product_option_value_id' => $value,
                                    'option_id' => $option_query->row['option_id'],
                                    'option_value_id' => $option_value_query->row['option_value_id'],
                                    'name' => $option_query->row['name'],
                                    'value' => $option_value_query->row['name'],
                                    'cod' => $option_query->row['cod'],
                                    'type' => $option_query->row['type'],
                                    'quantity' => $option_value_query->row['quantity'],
                                    'subtract' => $option_value_query->row['subtract'],
                                    'price' => $option_value_query->row['price'],
                                    'price_prefix' => $option_value_query->row['price_prefix'],
                                    'points' => $option_value_query->row['points'],
                                    'points_prefix' => $option_value_query->row['points_prefix'],
                                    'weight' => $option_value_query->row['weight'],
                                    'weight_prefix' => $option_value_query->row['weight_prefix'],
                                );
                            }
                        } elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
                            foreach ($value as $product_option_value_id) {
                                $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.deleted != '1' AND pov.product_option_value_id = '" . (int) $product_option_value_id . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                                if ($option_value_query->num_rows) {
                                    if ($option_value_query->row['price_prefix'] == '+') {
                                        $option_price += $option_value_query->row['price'];
                                    } elseif ($option_value_query->row['price_prefix'] == '-') {
                                        $option_price -= $option_value_query->row['price'];
                                    }

                                    if ($option_value_query->row['points_prefix'] == '+') {
                                        $option_points += $option_value_query->row['points'];
                                    } elseif ($option_value_query->row['points_prefix'] == '-') {
                                        $option_points -= $option_value_query->row['points'];
                                    }

                                    if ($option_value_query->row['weight_prefix'] == '+') {
                                        $option_weight += $option_value_query->row['weight'];
                                    } elseif ($option_value_query->row['weight_prefix'] == '-') {
                                        $option_weight -= $option_value_query->row['weight'];
                                    }

                                    if ($option_value_query->row['subtract'] && (!($option_value_query->row['quantity']+$original_order_quantity) || (($option_value_query->row['quantity']+$original_order_quantity) < $cart['quantity']))) {
                                        $stock = false;
                                    }

                                    $option_data[] = array(
                                        'product_option_id' => $product_option_id,
                                        'product_option_value_id' => $product_option_value_id,
                                        'option_id' => $option_query->row['option_id'],
                                        'option_value_id' => $option_value_query->row['option_value_id'],
                                        'name' => $option_query->row['name'],
                                        'value' => $option_value_query->row['name'],
                                        'type' => $option_query->row['type'],
                                        'quantity' => $option_value_query->row['quantity'],
                                        'subtract' => $option_value_query->row['subtract'],
                                        'price' => $option_value_query->row['price'],
                                        'price_prefix' => $option_value_query->row['price_prefix'],
                                        'points' => $option_value_query->row['points'],
                                        'points_prefix' => $option_value_query->row['points_prefix'],
                                        'weight' => $option_value_query->row['weight'],
                                        'weight_prefix' => $option_value_query->row['weight_prefix']
                                    );
                                }
                            }
                        } elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
                            $option_data[] = array(
                                'product_option_id' => $product_option_id,
                                'product_option_value_id' => '',
                                'option_id' => $option_query->row['option_id'],
                                'option_value_id' => '',
                                'name' => $option_query->row['name'],
                                'value' => $value,
                                'type' => $option_query->row['type'],
                                'quantity' => '',
                                'subtract' => '',
                                'price' => '',
                                'price_prefix' => '',
                                'points' => '',
                                'points_prefix' => '',
                                'weight' => '',
                                'weight_prefix' => ''
                            );
                        }
                    }
                }

                $old_price = "";
                $price = $original_order_price;

                $special = 0;

                // Product Specials
                $product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $cart['product_id'] . "' AND customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND deleted='no' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");

                if ($product_special_query->num_rows) {
                    $old_price = $price ;
                    $price = $product_special_query->row['price'];
                }

                // Product Discounts
                $discount_quantity = 0;


                $customer_id = $this->customer->getId();
                if($customer_id > 0){
                    $customer_id = (int)$customer_id;
                }else{
                    $customer_id = NULL;
                }

                if(NULL == $customer_id){
                    $cart_2_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "admin_cart WHERE customer_id IS NULL AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
                }else{
                    $cart_2_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "admin_cart WHERE customer_id = '" . (int) $this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
                }

                foreach ($cart_2_query->rows as $cart_2) {
                    if ($cart_2['product_id'] == $cart['product_id']) {
                        $discount_quantity += $cart_2['quantity'];
                    }
                }



                // Reward Points
                $reward = 0;

                // Downloads
                $download_data = array();

                // Stock
                if (!($product_query->row['quantity']+$original_order_quantity) || (($product_query->row['quantity']+$original_order_quantity) < $cart['quantity'])) {
                    $stock = false;
                }

                $recurring = false;

                if ($cart['otp_id'] == 0) {
                    $final_model = $product_query->row['model'];
                    $final_price = $price + $option_price;
                    $final_total = ($price + $option_price) * $cart['quantity'];
                    $final_weight = ($product_query->row['weight'] + $option_weight) * $cart['quantity'];
                } else {
                    if ($otp_data['model'] != '') {
                        $final_model = $otp_data['model'];
                    } else {
                        $final_model = $product_query->row['model'];
                    }
                    if ($product_discount_query->num_rows) {
                        $final_price = $product_discount_query->row['price'] + $option_price;
                        $final_total = ($product_discount_query->row['price'] + $option_price) * $cart['quantity'];
                    } else {
                        if ($this->config->get('config_otp_special') && $otp_data['special'] > 0) {
                            $old_price = $otp_data['price'];
                            $final_price = $otp_data['special'] + $option_price;
                            $final_total = ($otp_data['special'] + $option_price) * $cart['quantity'];
                        } elseif ($this->config->get('config_otp_price') && $otp_data['price'] > 0) {
                            if ($otp_data['price_prefix'] == "=") {
                                $final_price = $otp_data['price'] + $option_price;
                                $final_total = ($otp_data['price'] + $option_price) * $cart['quantity'];
                            } elseif ($otp_data['price_prefix'] == "+") {
                                $final_price = $price + $otp_data['price'] + $option_price;
                                $final_total = ($price + $otp_data['price'] + $option_price) * $cart['quantity'];
                            } else {
                                $final_price = $price - $otp_data['price'] + $option_price;
                                $final_total = ($price - $otp_data['price'] + $option_price) * $cart['quantity'];
                            }
                        } else {
                            $final_price = $price + $option_price;
                            $final_total = ($price + $option_price) * $cart['quantity'];
                        }
                    }
                    if ($this->config->get('config_otp_weight') && $otp_data['weight'] > 0) {
                        if ($otp_data['weight_prefix'] == "=") {
                            $final_weight = ($otp_data['weight'] + $option_weight) * $cart['quantity'];
                        } elseif ($otp_data['weight_prefix'] == "+") {
                            $final_weight = ($product_query->row['weight'] + $otp_data['weight'] + $option_weight) * $cart['quantity'];
                        } else {
                            $final_weight = ($product_query->row['weight'] - $otp_data['weight'] + $option_weight) * $cart['quantity'];
                        }
                    } else {
                        $final_weight = ($product_query->row['weight'] + $option_weight) * $cart['quantity'];
                    }
                }

                if ($cart['swap_id'] == 0) {
                    $final_image = $product_query->row['image'];
                } else {
                    $otp_swap_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "otp_image WHERE `id` = '" . $cart['swap_id'] . "'");
                    if ($otp_swap_query->num_rows) {
                        $final_image = $otp_swap_query->row['image'];
                    } else {
                        $final_image = $product_query->row['image'];
                    }
                }

                $product_data[] = array(
                    'otp_id' => $cart['otp_id'],
                    'cart_id' => $cart['cart_id'],
                    'product_id' => $product_query->row['product_id'],
                    'sku' => $product_query->row['sku'],
                    'name' => $product_query->row['name'] ." ( {$product_query->row['product_id']} )",
                    'model' => $final_model, //$product_query->row['model'],
                    'shipping' => $product_query->row['shipping'],
                    'image' => $final_image, //$product_query->row['image'],
                    'option' => $option_data,
                    'download' => $download_data,
                    'quantity' => $cart['quantity'],
                    'minimum' => $product_query->row['minimum'],
                    'is_product_fbs' => $product_query->row['is_fbs'],
                    'is_seller_fbs' => $product_query->row['is_seller_fbs'],
                    'subtract' => $product_query->row['subtract'],
                    'stock' => $stock,
                    'old_price' => $old_price,
                    'price' => $final_price, //($price + $option_price),
                    'total' => $final_total, //($price + $option_price) * $cart['quantity'],
                    'reward' => $reward * $cart['quantity'],
                    'points' => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $cart['quantity'] : 0),
                    'tax_class_id' => $product_query->row['tax_class_id'],
                    'weight' => $final_weight, //($product_query->row['weight'] + $option_weight) * $cart['quantity'],
                    'weight_class_id' => $product_query->row['weight_class_id'],
                    'length' => $product_query->row['length'],
                    'width' => $product_query->row['width'],
                    'height' => $product_query->row['height'],
                    'length_class_id' => $product_query->row['length_class_id'],
                    'recurring' => $recurring,
                    'op_size'=>isset($cart['op_size']) ? $cart['op_size'] : '',
                );
            } else {
                //$this->remove($cart['cart_id']);
            }
        }

        return $product_data;
    }

    //public function add($product_id, $quantity = 1, $option = array(), $recurring_id = 0) {
    public function add($product_id, $quantity = 1, $option = array(), $recurring_id = 0, $otp_id = 0, $swap_id = 0,$size = '', $order_id = 0) {

        $size = $this->db->escape($size);
        $customer_id = $this->customer->getId();
        if($customer_id > 0){
            $customer_id = (int)$customer_id;
        }else{
            $customer_id = NULL;
        }

        if(NULL == $customer_id){
            $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "admin_cart WHERE customer_id IS NULL AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int) $product_id . "' AND recurring_id = '" . (int) $recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");

            if (!$query->row['total']) {
                $this->db->query("INSERT " . DB_PREFIX . "admin_cart SET customer_id = NULL, session_id = '" . $this->db->escape($this->session->getId()) . "', product_id = '" . (int) $product_id . "', recurring_id = '" . (int) $recurring_id . "', `option` = '" . $this->db->escape(json_encode($option)) . "', quantity = '" . (int) $quantity . "', date_added = NOW(), `otp_id` = '" . $otp_id . "', `swap_id` = '" . $swap_id . "', `op_size`='$size' ");
            } else {
                $this->db->query("UPDATE " . DB_PREFIX . "admin_cart SET `op_size`='$size' ,quantity = (quantity + " . (int) $quantity . ") WHERE customer_id IS NULL AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int) $product_id . "' AND recurring_id = '" . (int) $recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");
            }
        } else {
            $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "admin_cart WHERE customer_id = '" . (int) $this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int) $product_id . "' AND recurring_id = '" . (int) $recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");

            if (!$query->row['total']) {
                $this->db->query("INSERT " . DB_PREFIX . "admin_cart SET customer_id = '" . (int) $this->customer->getId() . "', session_id = '" . $this->db->escape($this->session->getId()) . "', product_id = '" . (int) $product_id . "', recurring_id = '" . (int) $recurring_id . "', `option` = '" . $this->db->escape(json_encode($option)) . "', quantity = '" . (int) $quantity . "', date_added = NOW(), `otp_id` = '" . $otp_id . "', `swap_id` = '" . $swap_id . "' , `op_size`='$size' ");
            } else {
                $this->db->query("UPDATE " . DB_PREFIX . "admin_cart SET `op_size`='$size' , quantity = (quantity + " . (int) $quantity . ") WHERE customer_id = '" . (int) $this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "' AND product_id = '" . (int) $product_id . "' AND recurring_id = '" . (int) $recurring_id . "' AND `option` = '" . $this->db->escape(json_encode($option)) . "'");
            }
        }
    }

    public function update($cart_id, $quantity) {
        $customer_id = $this->customer->getId();
        if($customer_id > 0){
            $customer_id = (int)$customer_id;
        }else{
            $customer_id = NULL;
        }

        if(NULL == $customer_id){
            $this->db->query("UPDATE " . DB_PREFIX . "admin_cart SET quantity = '" . (int) $quantity . "' WHERE cart_id = '" . (int) $cart_id . "' AND customer_id IS NULL AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
        }else{
            $this->db->query("UPDATE " . DB_PREFIX . "admin_cart SET quantity = '" . (int) $quantity . "' WHERE cart_id = '" . (int) $cart_id . "' AND customer_id = '" . (int) $this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
        }


    }

    public function remove($cart_id) {
        $customer_id = $this->customer->getId();
        if($customer_id > 0){
            $customer_id = (int)$customer_id;
        }else{
            $customer_id = NULL;
        }
        if(NULL == $customer_id){
            $this->db->query("DELETE FROM " . DB_PREFIX . "admin_cart WHERE cart_id = '" . (int) $cart_id . "' AND customer_id IS NULL AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
        }else{
            $this->db->query("DELETE FROM " . DB_PREFIX . "admin_cart WHERE cart_id = '" . (int) $cart_id . "' AND customer_id = '" . (int) $this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
        }
    }

    public function clear() {
        $customer_id = $this->customer->getId();
        if($customer_id > 0){
            $customer_id = (int)$customer_id;
        }else{
            $customer_id = NULL;
        }
        if(NULL == $customer_id){
            $this->db->query("DELETE FROM " . DB_PREFIX . "admin_cart WHERE customer_id IS NULL AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
        }else{
            $this->db->query("DELETE FROM " . DB_PREFIX . "admin_cart WHERE customer_id = '" . (int) $this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");

        }

    }

    public function getRecurringProducts($order_id = 0) {
        $product_data = array();

        if($order_id==0 && isset($_GET['order_id'])){
          $order_id = $this->getOrderNumber($_GET['order_id']);
        }

        foreach ($this->getProducts($order_id) as $value) {
            if ($value['recurring']) {
                $product_data[] = $value;
            }
        }

        return $product_data;
    }

    public function getWeight($order_id = 0) {
        $weight = 0;

        if($order_id==0 && isset($_GET['order_id'])){
          $order_id = $this->getOrderNumber($_GET['order_id']);
        }

        foreach ($this->getProducts($order_id) as $product) {
            if ($product['shipping']) {
                $weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
            }
        }

        return $weight;
    }

    public function getCartCountries($order_id = 0) {
        $countries = array();

        if($order_id==0 && isset($_GET['order_id'])){
          $order_id = $this->getOrderNumber($_GET['order_id']);
        }

        foreach ($this->getProducts($order_id) as $product) {

            $country = $this->db->query("SELECT cp2c.country_id FROM customerpartner_to_product cp2p LEFT JOIN customerpartner_to_customer cp2c ON(cp2p.customer_id=cp2c.customer_id) WHERE cp2p.product_id = '".(int)$product['product_id']."' ");
            if($country->num_rows){
                $countries[$country->row['country_id']] = $country->row['country_id'];
            }
        }

        return $countries;
    }

    public function getSubTotal($order_id = 0) {
        $total = 0;
        if($order_id==0 && isset($_GET['order_id'])){
          $order_id = $this->getOrderNumber($_GET['order_id']);
        }

        foreach ($this->getProducts($order_id) as $product) {
            $total += $product['total'];
        }

        return $total;
    }

    public function getTaxes($order_id = 0) {
        $tax_data = array();

        if($order_id==0 && isset($_GET['order_id'])){
          $order_id = $this->getOrderNumber($_GET['order_id']);
        }

        foreach ($this->getProducts($order_id) as $product) {
            if ($product['tax_class_id']) {
                $tax_rates = $this->tax->getRates($product['price'], $product['tax_class_id']);

                foreach ($tax_rates as $tax_rate) {
                    if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
                        $tax_data[$tax_rate['tax_rate_id']] = ($tax_rate['amount'] * $product['quantity']);
                    } else {
                        $tax_data[$tax_rate['tax_rate_id']] += ($tax_rate['amount'] * $product['quantity']);
                    }
                }
            }
        }

        return $tax_data;
    }

    public function getTotal($order_id = 0) {
        $total = 0;
        if($order_id==0 && isset($_GET['order_id'])){
          $order_id = $this->getOrderNumber($_GET['order_id']);
        }

        foreach ($this->getProducts($order_id) as $product) {
            $total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
        }

        return $total;
    }

    public function countProducts($options = false, $quantity = false, $order_id = 0) {
        $product_total = 0;
        $product_exist = array();
        $products = $this->getProducts($order_id);

        if ($options) {
            return count($products);
        } else {
            foreach ($products as $product) {
                if ($quantity) {
                    $product_total += $product['quantity'];
                } else {
                    if (!in_array($product['product_id'], $product_exist)) {
                        $product_exist['product_id'] = $product['product_id'];
                        $product_total++;
                    }
                }
            }
        }
        return $product_total;
    }

    public function hasProducts($order_id = 0) {

      if($order_id==0 && isset($_GET['order_id'])){
        $order_id = $this->getOrderNumber($_GET['order_id']);
      }

        return count($this->getProducts($order_id));
    }

    public function hasRecurringProducts() {
        return count($this->getRecurringProducts());
    }

    public function hasStock($order_id = 0) {

      if($order_id==0 && isset($_GET['order_id'])){
        $order_id = $this->getOrderNumber($_GET['order_id']);
      }


        foreach ($this->getProducts($order_id) as $product) {
            if (!$product['stock']) {
                return false;
            }
        }

        return true;
    }

    public function hasShipping($order_id = 0) {
        $shipping = false;

        if($order_id==0 && isset($_GET['order_id'])){
          $order_id = $this->getOrderNumber($_GET['order_id']);
        }

        foreach ($this->getProducts($order_id) as $product) {
            if ($product['shipping']) {
                return true;
            }
        }

        return $shipping;
    }

    public function hasDownload($order_id = 0) {
        $download = false;

        if($order_id==0 && isset($_GET['order_id'])){
          $order_id = $this->getOrderNumber($_GET['order_id']);
        }

        foreach ($this->getProducts($order_id) as $product) {
            if ($product['download']) {
                $download = true;

                break;
            }
        }

        return $download;
    }

    public function getOrderNumber($order_id){

      $order = substr($order_id,4);
      $order1 = substr($order,0,-1);
      $order2 = substr($order,0,-2);
      $query1 = $this->db->query("SELECT order_id from `order` WHERE order_id=".(int)$order1);
      if($query1->num_rows)
        return $order1;
      else{
        $query2 = $this->db->query("SELECT order_id from `order` WHERE order_id=".(int)$order2);
        if($query2->num_rows)
          return $order2;
      }
      return $order_id;
    }

}
