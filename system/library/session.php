<?php

class Session {

    private static $instance;
    public $data = array();
    
    public static function getInstance($session_id = '', $key = 'default') {
        if (null === static::$instance) {
            static::$instance = new static($session_id, $key);
        }

        return static::$instance;
    }
    
    private function __clone() {
        
    }

    private function __wakeup() {
        
    }

    protected function __construct($session_id = '', $key = 'default') {
        $session_expire = 3600;
        if (defined('HTTP_CATALOG')) {
            $session_expire = ( 3600 * 24); // 1 day for admin.mall.
        } else {
            $session_expire = ( 3600 * 24 * 365); // 1 year for customers
        }
        
        if (!session_id()) {
            
            ini_set('session.gc_maxlifetime', $session_expire);
            
            ini_set('session.save_handler', 'redis');
            ini_set('session.save_path', 'tcp://' . REDIS_IP . ':' . REDIS_PORT . '?database=' . REDIS_SESSION_DB . '&avg_ttl=' . $session_expire . '&ttl=' . $session_expire);
            
            if ($session_id) {
                session_id($session_id);
            }
            
            session_set_cookie_params(0, '/');
            session_start();
        }

        if (!isset($_SESSION[$key])) {
            $_SESSION[$key] = array();
        }
        $this->data = &$_SESSION[$key];
    }

    public function getId() {
        return session_id();
    }

    public function start() {
        return session_start();
    }

    public function destroy() {
        return session_destroy();
    }

}
