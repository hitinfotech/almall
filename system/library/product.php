<?php
/** @property Db $db */
class Product {

   /** @var  Registry $registry */
    private $registry;
    public $seller_id;
    public $user_id;
    public $sheet_id;
    public $sku;
    public $en_title;
    public $ar_title;
    public $en_description;
    public $ar_description;
    public $brand_name;
    public $brand_id;
    public $category;
    public $category_id;
    public $attribute_id;
    public $option_values;
    public $option_type;
    public $qty;
    public $discounted_price;
    public $price;
    public $images = array();
    public $model = "";
    public function __construct($registry)
    {
        $this->registry = $registry;
    }
    public function updateRegistry($reg){
        $this->registry = $reg;
    }
    public function __get($key) {

        return $this->registry->get($key);
    }

    public function __set($key, $value) {
        $this->registry->set($key, $value);
    }


    public function serialize(){
        $tmp = ($this->registry);
        $this->registry = "";
        $data =  serialize($this);
        $this->registry = $tmp;
        return $data;
    }

    public function save(){
        $serialized = $this->db->escape($this->serialize());
        $this->db->query("INSERT INTO `xls_products`  set `data`='$serialized',`created_date`= NOW()");
        return $this->db->getLastId();
    }
    public function updateStatus($id,$status,$msg=false,$pid=false){
        $status = $this->db->escape($status);

        if($msg && !$pid){
            $msg = $this->db->escape($msg);
            $this->db->query("UPDATE `xls_products` set `status`='$status',`msg`='$msg' WHERE `id`='$id'");
        }else if($msg && $pid){
            $msg = $this->db->escape($msg);
            $pid = (int)$pid;
            $this->db->query("UPDATE `xls_products` set `status`='$status',`msg`='$msg',product_id='$pid' WHERE `id`='$id'");
        }else{
            $this->db->query("UPDATE `xls_products` set `status`='$status' WHERE `id`='$id'");
        }

    }



}
