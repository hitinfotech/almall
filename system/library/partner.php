<?php

class Partner {

    private $partner_id;
    private $firstname;
    private $lastname;
    private $partner_group_id;
    private $email;
    private $fbs_email;
    private $telephone;
    private $address_id;


    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');
        $this->country = $registry->get('country');

        if (isset($this->session->data['partner_id'])) {
            $partner_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer WHERE customer_id = '" . (int) $this->session->data['partner_id'] . "' AND status = '1'");

            if ($partner_query->num_rows) {
                $this->partner_id = $partner_query->row['customer_id'];
                $this->firstname = $partner_query->row['firstname'];
                $this->lastname = $partner_query->row['lastname'];
                $this->partner_group_id = '';
                $this->email = $partner_query->row['email'];
                $this->fbs_email = $partner_query->row['fbs_email'];
                $this->telephone = $partner_query->row['telephone'];
            } else {
                $this->logout();
            }
        }
    }

    public function login($email, $password, $override = false) {
        if ($override) {
            $partner_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND status = '1'");
        } else {
            $partner_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
        }
        if ($partner_query->num_rows) {
            $this->session->data['partner_id'] = $partner_query->row['customer_id'];
            $this->partner_id = $partner_query->row['customer_id'];
            $this->firstname = $partner_query->row['firstname'];
            $this->lastname = $partner_query->row['lastname'];
            $this->email = $partner_query->row['email'];
            $this->fbs_email = $partner_query->row['fbs_email'];
            $this->telephone = $partner_query->row['telephone'];

            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        unset($this->session->data['partner_id']);
        unset($this->session->data['openModal']);

        $this->partner_id = '';
        $this->firstname = '';
        $this->partner_group_id = '';
        $this->lastname = '';
        $this->email = '';
        $this->fbs_email = '';
        $this->telephone = '';
      }

    public function isLogged() {
        return $this->partner_id;
    }

    public function getId() {
        return $this->partner_id;
    }

    public function getFirstName() {
        return $this->firstname;
    }

    public function getLastName() {
        return $this->lastname;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getFBSEmail() {
        return $this->fbs_email;
    }

    public function getTelephone() {
        return $this->telephone;
    }

    public function getGroupId() {
        return $this->partner_group_id;
    }

    public function getIPAddress() {
        return $this->country->getIpAddress();
    }

    }
