<?php

class Aws_S3 {

    protected $registry;
    protected static $s3_client;

    public function __construct($registry) {
        $this->registry = $registry;
    }

    public function s3() {
        if (!isset(static::$s3_client)) {
            static::$s3_client = new S3(AWS_CREDENTIALS_KEY, AWS_CREDENTIALS_SECRET, FALSE);
        }
        return static::$s3_client;
    }

}
