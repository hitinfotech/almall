<?php 

/**
 * Muayad R. Qanadilo
 * Using google translate API - paied
 * @param string $q the word/phrase to be translated 
 * @param string $output expected values [json] or don't pass this value if you want to see the output printed on the screen 
 * @return mixed object that contains the response from google translate API
 */
function google_translate($q, $source='en', $target='ar', $output='') {

	// $apiKey = 'AIzaSyBpFstGbmJnAeBKZWrHxXj9jhJ1BqM-1a4';	// muayad.qanadilo@sayidaty.net
	$apiKey = 'AIzaSyDTO2iG8GOhcnZiGlpKGb7EcJubuNDPaJY';
	$text = (isset($q) && !empty($q)) ? $q : '';
	$url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($text) . '&source='.$source.'&target='.$target.'';	//die($url);
	if (isset($text) && !empty($text)) {
		try{
			$handle = curl_init($url);
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($handle);
			$responseDecoded = json_decode($response, true);
			// echo "<pre>"; die(var_dump($responseDecoded));
			curl_close($handle);
		}catch(Exception $e){
			// echo "<pre>"; die(var_dump($e));
		}
	}

	if (isset($responseDecoded) && !empty($responseDecoded)) {

		if ($output=="json") {
			// $this->response->addHeader('Content-Type: application/json');
			// $this->response->setOutput(json_encode($responseDecoded));
			return json_encode($responseDecoded);
		}else{
			$output = '';
			$output .= 'Source: ' . $text . '<br>';
			$output .= 'Translation: ' . $responseDecoded['data']['translations'][0]['translatedText'];

			return $output;
		}


	}

}