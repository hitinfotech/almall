<?php
// HTTP
define('HTTP_SERVER', "http://commerce.sayidaty.local/"); // server

// HTTPS
define('HTTPS_SERVER', "http://commerce.sayidaty.local/"); // server

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'sayidaty');
define('DB_PASSWORD', 'sayidaty');
define('DB_DATABASE', 'sayidaty_net');
define('DB_PORT', '3306');
define('DB_PREFIX', '');

define("MONGO_DEFUALT_HOST","localhost");
//require_once DIR_SYSTEM . '/library/beans/Beanstalk.php';

#3PL Emails
define('FirstFlight_BOOKING_EMAILS','ibrahim.samara@sayidaty.net,alsughier.odai@sayidaty.net,ops@sayidatymall.net');
define('FirstFlight_BOOKING_CC_EMAILS','ibrahim.samara@sayidaty.net');

define('FirstFlight_REPORT_EMAILS','ibrahim.samara@sayidaty.net,alsughier.odai@sayidaty.net,ops@sayidatymall.net');
define('FirstFlight_REPORT_CC_EMAILS','ibrahim.samara@sayidaty.net');

//Naqel:
define('NaqelExpress_BOOKING_EMAILS','ibrahim.samara@sayidaty.net');
define('NaqelExpress_BOOKING_CC_EMAILS','ibrahim.samara@sayidaty.net');

define('NaqelExpress_REPORT_EMAILS','ibrahim.samara@sayidaty.net');
define('NaqelExpress_REPORT_CC_EMAILS','ibrahim.samara@sayidaty.net');


define('SMSA_EMAILS','ibrahim.samara@sayidaty.net');
define('PostaPlus_EMAILS','ibrahim.samara@sayidaty.net');
