<?php
// HTTP
//define('HTTP_SERVER', "http://{$_SERVER['HTTP_HOST']}/"); // server
define('HTTP_SERVER', "http://dev.mall.sayidaty.net/"); // server


// HTTPS
//define('HTTPS_SERVER', "http://{$_SERVER['HTTP_HOST']}/"); // server
define('HTTPS_SERVER', "http://dev.mall.sayidaty.net/"); // server

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'sayidaty');
define('DB_PASSWORD', 'sayidaty');
define('DB_DATABASE', 'sayidaty_net');
define('DB_PORT', '3306');
define('DB_PREFIX', '');


#3PL Emails

//FirstFlight:
define('FirstFlight_BOOKING_EMAILS','naveen.desouza@sayidaty.net,ibrahim.samara@sayidaty.net,alsughier.odai@sayidaty.net,ops@sayidatymall.net');
define('FirstFlight_BOOKING_CC_EMAILS','naveen.desouza@sayidaty.net,ibrahim.samara@sayidaty.net');

define('FirstFlight_REPORT_EMAILS','naveen.desouza@sayidaty.net,ibrahim.samara@sayidaty.net,alsughier.odai@sayidaty.net,ops@sayidatymall.net');
define('FirstFlight_REPORT_CC_EMAILS','naveen.desouza@sayidaty.net,ibrahim.samara@sayidaty.net');

//Naqel:
define('NaqelExpress_BOOKING_EMAILS','naveen.desouza@sayidaty.net,ibrahim.samara@sayidaty.net,alsughier.odai@sayidaty.net,ops@sayidatymall.net');
define('NaqelExpress_BOOKING_CC_EMAILS','naveen.desouza@sayidaty.net,ibrahim.samara@sayidaty.net');

define('NaqelExpress_REPORT_EMAILS','naveen.desouza@sayidaty.net,ibrahim.samara@sayidaty.net,alsughier.odai@sayidaty.net,ops@sayidatymall.net');
define('NaqelExpress_REPORT_CC_EMAILS','naveen.desouza@sayidaty.net,ibrahim.samara@sayidaty.net');


define('SMSA_EMAILS','naveen.desouza@sayidaty.net,ibrahim.samara@sayidaty.net,alsughier.odai@sayidaty.net,ops@sayidatymall.net');
define('PostaPlus_EMAILS','naveen.desouza@sayidaty.net,ibrahim.samara@sayidaty.net,alsughier.odai@sayidaty.net,ops@sayidatymall.net');

define("MONGO_DEFUALT_HOST","localhost");
