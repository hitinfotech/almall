<?php

final class config_image {

    private $admin_lising = array('width' => 40, 'hieght' => 40);
    private $dimensions = array(
        'gethomepage' => array(
            'homepage_block0_image1' => array('width' => 1300, 'hieght' => 200),
            'homepage_block0_image2' => array('width' => 710, 'hieght' => 290),
            'homepage_block1_image1' => array('width' => 750, 'hieght' => 435),
            'homepage_block1_image2' => array('width' => 360, 'hieght' => 435),
            'homepage_block1_image3' => array('width' => 400, 'hieght' => 380),
            'homepage_block1_image4' => array('width' => 400, 'hieght' => 270),
            'homepage_block1_image5' => array('width' => 400, 'hieght' => 380),
            'homepage_block3_image1' => array('width' => 314, 'hieght' => 460),
            'homepage_block3_image2' => array('width' => 314, 'hieght' => 460),
            'homepage_block3_image3' => array('width' => 314, 'hieght' => 460),
            'homepage_block3_image4' => array('width' => 314, 'hieght' => 460),
            'homepage_block3_image5' => array('width' => 314, 'hieght' => 460),
            'homepage_block4_image1' => array('width' => 260, 'hieght' => 330),
            'homepage_block4_image2' => array('width' => 260, 'hieght' => 330),
            'homepage_block4_image3' => array('width' => 260, 'hieght' => 330),
            'homepage_block4_image4' => array('width' => 260, 'hieght' => 330),
            'homepage_block4_image5' => array('width' => 260, 'hieght' => 330),
            'homepage_block5_image1' => array('width' => 585, 'hieght' => 300),
            'homepage_block5_image2' => array('width' => 585, 'hieght' => 300),
            'homepage_block5_image3' => array('width' => 585, 'hieght' => 300),
        ),
        'getpopup' => array(
            'popup_block0_image1' => array('width' => 1300, 'hieght' => 200),
            'popup_block0_image2' => array('width' => 710, 'hieght' => 290),
            'popup_block1_image1' => array('width' => 750, 'hieght' => 435),
            'popup_block1_image2' => array('width' => 360, 'hieght' => 435),
            'popup_block1_image3' => array('width' => 400, 'hieght' => 380),
            'popup_block1_image4' => array('width' => 400, 'hieght' => 270),
            'popup_block1_image5' => array('width' => 400, 'hieght' => 380),
            'popup_block3_image1' => array('width' => 314, 'hieght' => 460),
            'popup_block3_image2' => array('width' => 314, 'hieght' => 460),
            'popup_block3_image3' => array('width' => 314, 'hieght' => 460),
            'popup_block3_image4' => array('width' => 314, 'hieght' => 460),
            'popup_block3_image5' => array('width' => 314, 'hieght' => 460),
            'popup_block4_image1' => array('width' => 260, 'hieght' => 330),
            'popup_block4_image2' => array('width' => 260, 'hieght' => 330),
            'popup_block4_image3' => array('width' => 260, 'hieght' => 330),
            'popup_block4_image4' => array('width' => 260, 'hieght' => 330),
            'popup_block4_image5' => array('width' => 260, 'hieght' => 330),
            'popup_block5_image1' => array('width' => 585, 'hieght' => 300),
            'popup_block5_image2' => array('width' => 585, 'hieght' => 300),
            'popup_block5_image3' => array('width' => 585, 'hieght' => 300),
        ),
        'product_landing' => array(
            'product_landing_block1_image1' => array('width' => 750, 'hieght' => 435),
            'product_landing_block1_image2' => array('width' => 750, 'hieght' => 435),
            'product_landing_block1_image3' => array('width' => 400, 'hieght' => 380),
            'product_landing_block1_image4' => array('width' => 400, 'hieght' => 270),
            'product_landing_block1_image5' => array('width' => 400, 'hieght' => 380),
            'product_landing_block2_image1' => array('width' => 260, 'hieght' => 330),
            'product_landing_block2_image2' => array('width' => 260, 'hieght' => 330),
            'product_landing_block2_image3' => array('width' => 260, 'hieght' => 330),
            'product_landing_block2_image4' => array('width' => 260, 'hieght' => 330),
            'product_landing_block2_image5' => array('width' => 260, 'hieght' => 330),
            'product_landing_block3_image1' => array('width' => 314, 'hieght' => 460),
            'product_landing_block3_image2' => array('width' => 314, 'hieght' => 460),
            'product_landing_block3_image3' => array('width' => 314, 'hieght' => 460),
            'product_landing_block3_image4' => array('width' => 314, 'hieght' => 460),
            'product_landing_block3_image5' => array('width' => 314, 'hieght' => 460),
            'product_landing_block5_image1' => array('width' => 260, 'hieght' => 330),
        ),
        'offer_banner' => array(
            'offer_banner_block5_image1' => array('width' => 260, 'hieght' => 330)
        ),
        'upper_banner' => array(
            'upper_banner_block5_image1' => array('width' => 1300, 'hieght' => 200)
        ),
        'mother_day' => array(
            'mother_day_block1_image1' => array('width' => 750, 'hieght' => 435),
            'mother_day_block1_image2' => array('width' => 750, 'hieght' => 435),
        ),
        'ramadan' => array(
            'ramadan_block1_image1' => array('width' => 750, 'hieght' => 435),
            'ramadan_block1_image2' => array('width' => 360, 'hieght' => 435),
        ),
        'landing_pages' => array(
            'landing_pages_block1_image1' => array('width' => 750, 'hieght' => 435),
            'landing_pages_block1_image2' => array('width' => 750, 'hieght' => 435),
        ),
        'featuredcategories' => array(
            'first' => array('width' => 366, 'hieght' => 539),
            'second' => array('width' => 358, 'hieght' => 240),
            'thired' => array('width' => 358, 'hieght' => 269),
            'fourth' => array('width' => 366, 'hieght' => 539)
        ),
        'product' => array(
            'zooming' => array('width' => 1080, 'hieght' => 1200),
            'details' => array('width' => 540, 'hieght' => 600),
            'listing' => array('width' => 314, 'hieght' => 460),
            'thumbnail' => array('width' => 240, 'hieght' => 360),
            'bundle' => array('width' => 120, 'hieght' => 120),
            'config_image_cart' => array('width' => 47, 'hieght' => 47),
            'config_image_compare' => array('width' => 90, 'hieght' => 90),
            'mobileapp_s' => array('width' => 200, 'hieght' => 200),
            'mobileapp_l' => array('width' => 400, 'hieght' => 400),
            'tablet_s' => array('width' => 300, 'hieght' => 300),
            'tablet_l' => array('width' => 600, 'hieght' => 600),
            'ratio' => array('width' => 1, 'hieght' => 1.5)
        ),
        'news_offers_malls' => array(
            'details' => array('width' => 660, 'hieght' => 460),
            'listing' => array('width' => 480, 'hieght' => 280),
            'ratio' => array('width' => 1.5, 'hieght' => 1)
        ),
        'shops_brands_designer' => array(
            'ratio' => array('width' => 1, 'hieght' => 1),
            'details' => array('width' => 420, 'hieght' => 420),
            'listing' => array('width' => 240, 'hieght' => 140),
            'thumbnail' => array('width' => 270, 'hieght' => 160),
            'collection_slider' => array('width' => 650, 'hieght' => 420)
        ),
        'tags' => array(
            'ratio' => array('width' => 1.5, 'hieght' => 1),
            'details' => array('width' => 700, 'hieght' => 465),
            'tag-product' => array('width' => 195, 'hieght' => 195),
            'featured' => array('width' => 750, 'hieght' => 420),
            'listing' => array('width' => 260, 'hieght' => 173),
            'small' => array('width' => 160, 'hieght' => 106)
        ),
        'looks' => array(
            'details' => array('width' => 600, 'hieght' => 720),
            'listing' => array('width' => 552, 'hieght' => 667),
            'landscape' => array('width' => 1100, 'hieght' => 540)
        ),
        'tips' => array(
            'ratio' => array('width' => 1.5, 'hieght' => 1),
            'details' => array('width' => 1000, 'hieght' => 665),
            'tip_product' => array('width' => 195, 'hieght' => 195),
            'listing' => array('width' => 460, 'hieght' => 300),
            'listing-small' => array('width' => 260, 'hieght' => 170),
        ),
        'designer' => array(
            'details' => array('width' => 260, 'hieght' => 270),
            'listing' => array('width' => 260, 'hieght' => 270)
        ),
        'profile' => array(
            'ratio' => array('width' => 1, 'hieght' => 1),
            'profile' => array('width' => 500, 'hieght' => 500),
            'comment' => array('width' => 60, 'hieght' => 60)
        ),
        'editorial_choice' => array(
            'cover' => array('width' => 700, 'hieght' => 480)
        ),
        'partner_avatar' => array(
            'profile' => array('width' => 120, 'hieght' => 120),
            'avatar' => array('width' => 100, 'hieght' => 100)
        ),
        'partner_document_path' => array(
            'profile' => array('width' => 120, 'hieght' => 120),
            'avatar' => array('width' => 100, 'hieght' => 100)
        ),
        'partner_companybanner' => array(
            'avatar' => array('width' => 800, 'hieght' => 300),
            'admin' => array('width' => 100, 'hieght' => 100)
        ),
        'partner_passport' => array(
            'avatar' => array('width' => 500, 'hieght' => 500),
            'admin' => array('width' => 125, 'hieght' => 125)
        ),
        'partner_license' => array(
            'avatar' => array('width' => 500, 'hieght' => 500),
            'admin' => array('width' => 125, 'hieght' => 125)
        ),
        'partner_hd_logo' => array(
            'avatar' => array('width' => 500, 'hieght' => 500),
            'admin' => array('width' => 125, 'hieght' => 125)
        ),
        'toppages' => array(
            'zooming' => array('width' => 1080, 'hieght' => 1200),
            'details' => array('width' => 540, 'hieght' => 600),
            'listing' => array('width' => 314, 'hieght' => 460),
            'thumbnail' => array('width' => 240, 'hieght' => 360),
            'config_image_cart' => array('width' => 47, 'hieght' => 47),
            'config_image_compare' => array('width' => 90, 'hieght' => 90),
            'ratio' => array('width' => 1, 'hieght' => 1.5)
        )
    );
    private static $instance;

    public static function getInstance() {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __clone() {
        
    }

    private function __wakeup() {
        
    }

    protected function __construct() {
        foreach ($this->dimensions as $key => $value) {
            $value['admin_listing'] = $this->admin_lising;
            $this->dimensions[$key] = $value;
        }
    }

    public function get($array = array(), $field = false, $w_h = false) {
        if ($w_h) {
            return $this->dimensions[$array][$field][$w_h];
        }
        if ($field) {
            return $this->dimensions[$array][$field];
        }
        if ($array) {
            return $this->dimensions[$array];
        }
        return $this->dimensions;
    }

}
