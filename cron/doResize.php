<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';
die("here");
$errors = array();

$db = new DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

$x_CONFIG = config_image::getInstance();
$dimensions = $x_CONFIG->get();

foreach ($dimensions as $key => $row) {
    do_resize(array(array('image' => 'placeholder.png')), $row, $error, $db);
}

foreach ($dimensions as $key => $row) {
    switch ($key) {
        /* case 'news_offers_malls':
          $result = $db->query("SELECT image FROM news_image ORDER BY news_id DESC");
          do_resize($result->rows, $row, $errors,  $db);
          $result1 = $db->query("SELECT image FROM offer_image ORDER BY offer_id DESC");
          do_resize($result1->rows, $row, $errors,  $db);
          $result2 = $db->query("SELECT image FROM mall ORDER BY mall_id DESC");
          do_resize($result2->rows, $row, $errors,  $db);
          break;
          case 'shops_brands_designer':
          $result = $db->query("SELECT image FROM store WHERE status = '1' ORDER BY store_id DESC");
          do_resize($result->rows, $row, $errors,  $db);
          $result1 = $db->query("SELECT image FROM brand  WHERE status = '1' ORDER BY brand_id DESC");
          do_resize($result1->rows, $row, $errors,  $db);
          break;
          case 'tags':
          $result = $db->query("SELECT image FROM tag  WHERE status = '1' ORDER BY tag_id DESC");
          do_resize($result->rows, $row, $errors,  $db);
          break;
          case 'looks':
          $result = $db->query("SELECT image FROM look_image  ORDER BY look_id DESC");
          do_resize($result->rows, $row, $errors,  $db);
          break;
          case 'tips':
          $result = $db->query("SELECT image FROM tip  ORDER BY tip_id DESC");
          do_resize($result->rows, $row, $errors, $db);
          $result = $db->query("SELECT image FROM tip_image  ORDER BY tip_id DESC");
          do_resize($result->rows, $row, $errors, $db);
          break; 

        case 'profile':
            $result = $db->query("SELECT image FROM customer ORDER BY customer_id DESC");
            do_resize($result->rows, $row, $errors, $db);
            break;*/
        case 'product':
            $result1 = $db->query("SELECT product_id, image FROM product_image WHERE product_id IN(SELECT product_id FROM product WHERE status = '1') ORDER BY product_id DESC ");
            do_resize($result1->rows, $row, $errors, $db);
            //$result = $db->query("SELECT product_id, image FROM product WHERE status = '1' ORDER BY product_id DESC  ");
            //do_resize($result->rows, $row, $errors, $db);
            break;
    }

    print_r($errors);
}

function do_resize($results, $row, &$error, $db) {
    $count = count($results);
    foreach ($results as $result) {
        foreach ($row as $key => $row2) {
            if ($row2['width'] > 10) {
                if ($result['image']) {
                    resize($result['image'], $row2['width'], $row2['hieght'], $db);
                } else {
                    $error[] = $result['image'];
                }
            }
        }
        echo "\n" . $count--;
    }
}

function resize($filename, $width, $height, $db) {

    if (!is_file(DIR_IMAGE . $filename)) {
        echo ("\nNo file => " . DIR_IMAGE . $filename);
        return;
    }

    $extension = pathinfo($filename, PATHINFO_EXTENSION);

    $old_image = $filename;

    $new_image = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
    if (!is_file(DIR_IMAGE . $new_image) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
        $path = '';

        $directories = explode('/', dirname(str_replace('../', '', $new_image)));

        foreach ($directories as $directory) {
            $path = $path . '/' . $directory;

            if (!is_dir(DIR_IMAGE . $path)) {
                @mkdir(DIR_IMAGE . $path, 0777);
            }
        }

        list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

        if ($width_orig != $width || $height_orig != $height) {

            $im = new imagick(DIR_IMAGE . $old_image);
            $imageprops = $im->getImageGeometry();
            $old_width = $imageprops['width'];
            $old_height = $imageprops['height'];
            if ($old_width > $old_height) {
                $newHeight = $height;
                $newWidth = ($height / $old_height) * $old_width;
            } else {
                $newWidth = $width;
                $newHeight = ($width / $old_width) * $old_height;
            }

            $im->resizeimage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1, true);
            $im->cropImage($newWidth, $newHeight, 0, 0);
            $im->writeImage(DIR_IMAGE . $new_image);
            $im->clear();
        } else {
            copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
        }
    }


    $db->query("INSERT INTO image_s3 SET image='" . $db->escape($new_image) . "' , date_added=NOW(), user_id='4' ");
}
