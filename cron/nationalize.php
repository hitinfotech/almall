<?php

namespace FlushCache;

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once realpath(__DIR__) . '/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

$db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
$error_product_prices = $error_special_product_prices = array();
echo " Add currencies to db \n";
$db->query(" INSERT INTO currency SET currency_id=6, title='Bahraini Dinar', code='BHD', symbol_left='', symbol_right='BHD', decimal_place=2, value=1, status=1, date_modified=NOW()");
$db->query(" INSERT INTO currency SET currency_id=7, title='Kuwaiti Dinar',  code='KWD', symbol_left='', symbol_right='KWD', decimal_place=2, value=1, status=1, date_modified=NOW()");
$db->query(" INSERT INTO currency SET currency_id=8, title='Omani Rial',     code='OMR', symbol_left='', symbol_right='OMR', decimal_place=2, value=1, status=1, date_modified=NOW()");
$db->query(" INSERT INTO currency SET currency_id=9, title='Qatari Rial',    code='QAR', symbol_left='', symbol_right='QAR', decimal_place=2, value=1, status=1, date_modified=NOW()");


$db->query("UPDATE setting SET `value` = 'USD', serialized='0' WHERE `key` = 'config_currency'      AND `code`='config' AND store_id='0' ");
$db->query("UPDATE setting SET `value` = '1',   serialized='0' WHERE `key` = 'config_currency_auto' AND `code`='config' AND store_id='0' ");


echo " Alter tables \n";
$db->query("alter table product add price float not null default 0 after location");

//$db->query("alter table product_special drop country_id");
//$db->query("alter table product_discount drop country_id");

//$db->query("drop table product_prices ");

//$db->query("alter table customerpartner_to_customer drop show_in_ksa");
//$db->query("alter table customerpartner_to_customer drop show_in_uae");
$db->query("alter table customerpartner_to_customer add index(available_country) ");

$db->query("alter table country add curcode varchar(3) after language");
$db->query("alter table country change language language varchar(3) not null default ''");
$db->query("alter table country drop name_old");
$db->query("alter table country add isvirtual tinyint default 0");

$db->query("UPDATE country SET available = 1, language='ar'  where iso_code_2 IN ('SA', 'AE','BH', 'KW', 'OM', 'QA')");
$db->query("UPDATE country SET status=0                      where iso_code_2 IN ('IL','PS','YE','SY','SS','KP','RU')");

$virtual = $db->query("SELECT country_id FROM country WHERE iso_code_2 ='UN'");
if ($virtual->num_rows > 0) {
    $db->query(" DELETE FROM country WHERE country_id = '" . (int) $virtual->row['country_id'] . "'");
    $db->query(" DELETE FROM country_description WHERE country_id = '" . (int) $virtual->row['country_id'] . "'");
}

$db->query("CREATE TABLE `page_settings` (
  `page_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `langaue_id` int(11) NOT NULL,
  PRIMARY KEY (`page_id`,`country_id`,`langaue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$db->query("alter table homepage_new drop column country_id");
$db->query("alter table homepage_new change column language_id `page_id` int(11) DEFAULT '0'");
$db->query("alter table coupon add notification tinyint(1) after code");
$db->query("alter table product_latest change column country_id page_id int(11) default 0");
$db->query("alter table product_latest add column code varchar(50)  default ''  after page_id");

$db->query("alter table page_settings add column code varchar(50)");

$db->query("insert into image_s3 set image='flags/un.png' ");

$db->query("insert into homepage_new           (country_id,language_id,code,`key`,value)(select '0',language_id,code,`key`,value from homepage_new where country_id=184) ");
$db->query("insert into popup_settings         (country_id,language_id,code,`key`,value)(select '0',language_id,code,`key`,value from popup_settings where country_id=184) ");
$db->query("insert into product_landing_blocks (country_id,language_id,code,`key`,value)(select '0',language_id,code,`key`,value from product_landing_blocks where country_id=184) ");
$db->query("insert into product_land           (country_id,language_id,code,`key`,value)(select '0',language_id,code,`key`,value from product_land where country_id=184) ");

$countries = $db->query("SELECT country_id FROM country WHERE available = 1");
foreach ($countries->rows as $row) {
    $row_id = (int) $row['country_id'];
    $db->query("insert into homepage_new(country_id,language_id,code,`key`,value)(select '" . (int) $row_id . "',language_id,code,`key`,value from homepage_new where country_id=184) ");
    $db->query("insert into popup_settings(country_id,language_id,code,`key`,value)(select '" . (int) $row_id . "',language_id,code,`key`,value from popup_settings where country_id=184) ");
    $db->query("insert into product_landing_blocks (country_id,language_id,code,`key`,value)(select '0',language_id,code,`key`,value from product_landing_blocks where country_id=184) ");
    $db->query("insert into product_land           (country_id,language_id,code,`key`,value)(select '0',language_id,code,`key`,value from product_land where country_id=184) ");
}

echo " Add cur code \n";
$db->query("update country SET sort_order='100' ");

$db->query("update country set curcode='SAR', sort_order='1' WHERE iso_code_2 = 'SA'");
$db->query("update country set curcode='AED', sort_order='2' WHERE iso_code_2 = 'AE'");

$db->query("update country set curcode='BHD', sort_order='4' WHERE iso_code_2 = 'BH'");
$db->query("update country set curcode='KWD', sort_order='3' WHERE iso_code_2 = 'KW'");

$db->query("update country set curcode='QAR', sort_order='5' WHERE iso_code_2 = 'QA'");
$db->query("update country set curcode='OMR', sort_order='6' WHERE iso_code_2 = 'OM'");

$db->query("update country set curcode='USD', sort_order='7' WHERE iso_code_2 = 'Un'");

$rates = array('221' => 3.67219996, '184' => 3.74970007);
// Shipping
$db->query("UPDATE setting SET `value` = '" . (26 / 3.67219996) . "',   serialized='0' WHERE `key` = 'flatcust_cost'          AND `code`='flatcust' AND store_id='0' ");
$db->query("UPDATE setting SET `value` = '" . (26 / 3.67219996) . "',   serialized='0' WHERE `key` = 'flatcust_cost_mix'      AND `code`='flatcust' AND store_id='0' ");
$db->query("UPDATE setting SET `value` = '" . (250 / 3.67219996) . "',  serialized='0' WHERE `key` = 'flatcust_zero_cost'     AND `code`='flatcust' AND store_id='0' ");
$db->query("UPDATE setting SET `value` = '" . (350 / 3.67219996) . "',  serialized='0' WHERE `key` = 'flatcust_zero_cost_mix' AND `code`='flatcust' AND store_id='0' ");

// COD payment
$db->query("UPDATE setting SET `value` = '" . (10000 / 3.67219996) . "', serialized='0' WHERE `key` = 'cod_max_total'     AND `code`='cod' AND store_id='0' ");
$db->query("UPDATE setting SET `value` = '" . (5000 / 3.67219996) . "',  serialized='0' WHERE `key` = 'cod_max_total_184' AND `code`='cod' AND store_id='0' ");
$db->query("UPDATE setting SET `value` = '" . (10000 / 3.67219996) . "', serialized='0' WHERE `key` = 'cod_max_total_221' AND `code`='cod' AND store_id='0' ");

$db->query("UPDATE customerpartner_to_customer SET available_country = 0 WHERE show_in_ksa=1 AND show_in_uae=1");
$db->query("UPDATE customerpartner_to_customer SET available_country = 184 WHERE show_in_ksa=1 AND show_in_uae=0");
$db->query("UPDATE customerpartner_to_customer SET available_country = 221 WHERE show_in_ksa=0 AND show_in_uae=1");

$product_prices = $db->query("DESC product_prices");

$products = $db->query("SELECT product_id FROM product WHERE status = 1 AND stock_status_id != 10 ORDER BY product_id DESC ");
foreach ($products->rows as $rowwwww) {

    $product_id = (int) $rowwwww['product_id'];

    echo "\nproduct {$product_id} price ";
    $seller_info = $db->query("SELECT cp.country_id FROM customerpartner_to_customer cp LEFT JOIN customerpartner_to_product cpp ON(cp.customer_id=cpp.customer_id) WHERE cpp.product_id = '" . (int) $product_id . "'");
    $country_id = 0;

    if ($seller_info->num_rows) {
        $country_id = (int) $seller_info->row['country_id'];
    }
    $price = 0;
    if ($product_prices && $product_prices->num_rows) {
        $price_info = $db->query("SELECT * FROM product_prices WHERE product_id = '" . (int) $product_id . "' AND available='1' ");
        $price = 0;
        foreach ($price_info->rows as $row) {
            if ($row['country_id'] == $country_id) {
                $price = (float) $row['price'] / $rates[$row['country_id']];
            }
        }
        if ($price == 0 && (in_array(ENVIROMENT, array('local')))) {
            $country_id = $country_id == 184 ? 221 : 184;
            foreach ($price_info->rows as $row) {
                if ($row['country_id'] == $country_id) {
                    $price = (float) $row['price'] / $rates[$row['country_id']];
                }
            }
        }
        if ($price == 0) {
            $error_product_prices[] = array('product_id' => $product_id, 'country_id' => $country_id);
        }
    }

    echo $price;

    $db->query("UPDATE product SET price='" . (float) $price . "' WHERE product_id = '" . (int) $product_id . "' ");
    $db->query("INSERT IGNORE INTO algolia(type,type_id)VALUES('product', '" . (int) $product_id . "') ");
}

$old = $db->query("desc product_special price_old ");

if (!$old->num_rows) {

    $db->query("alter table product_special add column price_old float not null default 0 after price");

    $db->query("UPDATE product_special SET price_old = price ");
}

$products_special = $db->query("SELECT * FROM product_special WHERE deleted='no' ORDER BY product_id DESC ");

foreach ($products_special->rows as $rowwwww) {

    $product_id = (int) $rowwwww['product_id'];

    echo "\nProduct {$product_id} special ";

    $seller_info = $db->query("SELECT cp.country_id FROM customerpartner_to_customer cp LEFT JOIN customerpartner_to_product cpp ON(cp.customer_id=cpp.customer_id) WHERE cpp.product_id = '" . (int) $product_id . "'");
    $country_id = 0;

    if ($seller_info->num_rows) {
        $country_id = (int) $seller_info->row['country_id'];
    }

    $special_info = $db->query("SELECT * FROM product_special WHERE product_id = '" . (int) $product_id . "' AND deleted='no' ");

    $count = 0;

    foreach ($special_info->rows as $row) {
        $count++;
        if ($row['country_id'] != $country_id) {
            $db->query("UPDATE product_special set deleted='yes', deleted_by=".(int) $this->user->getId().", deleted_date=NOW() WHERE product_special_id = '" . (int) $row['product_special_id'] . "'");
        } else {
            $special = (float) $row['price_old'] / $rates[$row['country_id']];
            $db->query("UPDATE product_special SET price='" . $special . "', `generated_by` = 'nationalize'  WHERE product_special_id = '" . (int) $row['product_special_id'] . "'  AND deleted='no'");
            echo "  ==>   {$count}   " . $special . " done ";
            if ($special == 0) {
                $error_special_product_prices[] = array('product_id' => $product_id, 'country_id' => $country_id);
            }
        }
    }
}


print_r($error_product_prices);
echo "\n\n";
print_r($error_special_product_prices);
