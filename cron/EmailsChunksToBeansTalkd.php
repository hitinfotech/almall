<?php
require_once('Cron.php');

/**
 * Class EmailsChunksToBeansTalkd
 */
class EmailsChunksToBeansTalkd extends Cron
{

    /** @var ModelNeChunk $chunk */
    public $chunk;

    function __construct()
    {

        exec("ps aux | grep -i EmailsChunksToBeansTalkd |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }

        parent::__construct();

        $this->loader->model('ne/chunk');
        $this->chunk = $this->registry->get('model_ne_chunk');
    }

    public function run()
    {

        $client = $this->getBeansTalkd();
        $client->useTube(NEWSLETTER_TUBE);
        $closeTime = time() + 60*60*1;
        while(time() < $closeTime){

            if (!isset($client)) {
                $client = $this->getBeansTalkd();
            }

            $readyList = $this->chunk->getReadyList();
            if ($readyList) {
                foreach ($readyList as $chunk) {
                    if($this->chunk->updateStatus($chunk['id'], 'INQUEUE')){
                        $client->put(($chunk['type'] = 'NEWSLETTER') ? 1000 : 0, 0, 0, $chunk['id']);
                        $this->myEcho('add chunk ' . $chunk['id'] . ' to queue');
                    }else{
                        $this->myEcho('cant add chunk ' . $chunk['id'] . ' to queue');

                    }
                }
            } else {
                $this->myEcho('no chunks sleep 2 seconds');
                sleep(2);
            }



        }
        $this->myEcho('killed this old process to start a new one');
        $client->disconnect();
    }

    private function getBeansTalkd()
    {
        $beansTalkd = new Beanstalk(array('server1' => array(
            'host' => BEANSTALKD_SERVER,
            'port' => BEANSTALKD_SERVER_PORT,
            'weight' => 50,
            'timeout' => 10,
            // array of connections/tubes
            'connections' => array(),
        )));
        $client = $beansTalkd->getClient();
        return $client;
    }

}


$cron = new EmailsChunksToBeansTalkd();
$cron->run();

