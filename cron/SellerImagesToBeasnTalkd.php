<?php
require_once('Cron.php');

class SellerImagesToBeasnTalkd extends Cron
{
    function __construct()
    {
        parent::__construct();
        // This cron is supposed to run for one time for task #MDS-465
    }

    public function run()
    {
        $beansTalkd = new Beanstalk(array('server1' => array(
            'host' => BEANSTALKD_SERVER,
            'port' => BEANSTALKD_SERVER_PORT,
            'weight' => 50,
            'timeout' => 10,
            // array of connections/tubes
            'connections' => array(),
        )));
        $client = $beansTalkd->getClient();
        $client->useTube('IMAGES_TO_SELLER_FOLDER');

        $closeTime = time() + 60 * 60 * 1;
        $images = $this->db->query(" SELECT product_image_id,product_id,image FROM `product_image` WHERE product_id=47210");
        if ($images && $images->num_rows > 0) {
            foreach ($images->rows as $row) {
              if(file_exists(DIR_IMAGE . $row['image'])){
                $this->myEcho("Image ".DIR_IMAGE . $row['image']."  was added");
                $client->put(0, 0, 10, $row['product_image_id']);
              }
          }
        }

        $images = $this->db->query(" SELECT product_id,image FROM `product` WHERE product_id=47210");
        if ($images && $images->num_rows > 0) {
            foreach ($images->rows as $row) {echo DIR_IMAGE . $row['image'];
              if(file_exists(DIR_IMAGE . $row['image'])){
                $this->myEcho("Image ".DIR_IMAGE . $row['image']."  was added");
                $client->put(0, 0, 10, "product_id_". $row['product_id']);
              }
          }
        }


        $client->disconnect();
    }
}

$image = new SellerImagesToBeasnTalkd();
$image->run();
