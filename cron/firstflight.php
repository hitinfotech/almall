<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

class FirstFlight {

    private $databaseID = 1;
    private $db;
    private $error;
    private $account_pass ;
    private $account_username ;
    private static $instance;
    private $client;

    private function __clone() {

    }

    private function __wakeup() {

    }

    public static function getInstance() {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct() {
        $this->db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
        $data = $this->db->query("SELECT * FROM ship_company sc LEFT JOIN ship_company_description scd ON(sc.ship_company_id=scd.ship_company_id) WHERE scd.language_id = '1' AND sc.ship_company_id='" . (int) $this->databaseID . "' ");
        $this->client = new SoapClient("http://www.wsdl.integraontrack.com/ServiceFF.asmx?op=BookingRequest&wsdl");
        if ($data->num_rows) {
            $this->account_no = $data->row['access_no'];
            $this->account_pass = $data->row['password']; //CONFIG_FFS_PASS;
            $this->account_username = $data->row['username']; //CONFIG_FFS_USERNAME;
            $this->account_customercode = $data->row['access_no']; //CONFIG_FFS_CUSTOMER_CODE;
        }
    }

    public function BookingRequest($data = array()) {
        $data = array_merge(['Username' => $this->account_username, 'Password' => $this->account_pass], $data);


        try {
            $res = $this->client->BookingRequest($data);
            var_dump($res);
            $result = json_encode($res);

            if (strpos($result, 'Sucessfully Updated') !== false) {
                return rtrim(substr($result, (int)strpos($result, 'No:')+4),'"}');
            }
            return false;
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function BookingTrack($data = array()) {
          $data = array_merge($data,['AccountNo' => $this->account_username, 'AccountPWD' => $this->account_pass]);
        try {
            $res = $this->client->BookingTrack($data);
            $xml = simplexml_load_string($res->BookingTrackResult->any);
            if (! empty($xml)){

              foreach ($xml->NewDataSet->Table as $key => $value){
                if( (trim(strtoupper($value->Status)) == 'C')|| (strpos($value->Remarks[0],'Pickup Done' )!== false)){
                  $booking_awbno = (array)$value->Remarks;
                  return ['result'=>'success','booking_awb' => $booking_awbno[0]];
                }
              }
              return ['result'=>'failed'];
            }
              return ['result'=>'failed'];

        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function GetBookingDetails($data = array()) {

    }

    public function GetRateQuery($data = array()) {

    }

    public function shippingTrack($AwbNo = '',$country_id) {
        $data = array('AccountNo' => $this->account_username,
            'AccountPWD' => $this->account_pass,
            'AwbNo' => $AwbNo);

        $client = new SoapClient("http://www.wsdl.integraontrack.com/FFTrack.asmx?wsdl");

        try {
            $res = $client->QueryData($data);
            $xml = simplexml_load_string($res->QueryDataResult->any);
            if ($xml === false) {
                return false;
            } else {

                $result = array();
                foreach (get_object_vars($xml->NewDataSet) as $row) {
                    foreach ($row as $record) {
                        $result[] = get_object_vars($record);
                    }
                }
                if(!empty($result))
                  return true;
                return false;
              }
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function LogDataBooking($data = array()) {

    }

    public function NewOrder($data = array()) {
        /*  $data = array_merge(['Username' => $this->account_username, 'Password' => $this->account_pass], $data);

          $client = new SoapClient("http://www.wsdl.integraontrack.com/ServiceFF.asmx?op=NewOrder&wsdl");
          try {
          $res = $client->NewOrderWithAutoAWBNO($data);
          return ($res);
          } catch (Exception $e) {
          var_dump($e->getMessage());
          } */
    }

    public function NewOrderWithAutoAWBNO($data = array()) {
        $data = array_merge(['Username' => $this->account_username, 'Password' => $this->account_pass], $data);

        //$client = new SoapClient("http://www.wsdl.integraontrack.com/ServiceFF.asmx?op=NewOrder&wsdl");
        try {
            $res = $this->client->NewOrderWithAutoAWBNO($data);
            if (strpos(json_encode($res), 'Sucessfully Updated') !== false) {
                $AWBNo = explode("AWBNO: ", json_encode($res));
                $AWBNo = explode('"}', $AWBNo[1]);
                return $AWBNo[0];
            }
            return false;
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function NewOrderWithAutoAWBNORef($data = array()) {

    }

    public function WebCustomerSearch($data = array()) {

    }

    public function WebSMSABookingInsert($data = array()) {

    }

}
