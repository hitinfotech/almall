<?php

require_once realpath(__DIR__) . '/Cron.php';

class trackShipment extends Cron {

   // private $FFServices;

    public function __construct()
    {
        //  $this->db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
        // $this->FFServices = FirstFlight::getInstance();
        //$this->shippingCompany = new FirstFlight($this->registry, SHIPPING_FIRST_FLIGHT_ID);

        parent::__construct();

    }

    public function track($order_id, $country_id)
    {

        $data = $this->getAWBNO($order_id, $country_id);

        $awbno = $data['AWBNO'];
        $shippingCompany =$data['shipping_company'];



        if ($data) {

            if ($shippingCompany == 1){

                $this->shippingCompany = new FirstFlight($this->registry, SHIPPING_FIRST_FLIGHT_ID);
                $shipment_status = $this->shippingCompany->shippingTrack($awbno, $country_id);

            }elseif ($shippingCompany == 2){

                $this->shippingCompany = new Naqel($this->registry, SHIPPING_NAQEL_ID);
                $shipment_status = $this->shippingCompany->shippingTrack($awbno);


            }elseif ($shippingCompany == 3){

                $this->shippingCompany = new Samsashipping($this->registry, SHIPPING_SMSA_ID);
                $shipment_status = $this->shippingCompany->shippingTrack($awbno);

            }elseif($shippingCompany == 4){
                $this->shippingCompany = new Postplus($this->registry, SHIPPING_POST_PLUS_ID);
                $shipment_status = $this->shippingCompany->shippingTrack($awbno);
            }

            if (isset($shipment_status)  && $shipment_status['result'] == 'success') {

                $this->addOrderHistory($order_id, $country_id, $awbno,$shipment_status['delivered_to']);
                // complete status : 5
                // cancel status : 10
                $getWholeOrderStatus = $this->getWholeOrderStatus($order_id, 5, 10);
                if ($getWholeOrderStatus) {
                    $this->changeWholeOrderStatus($order_id, $getWholeOrderStatus);
                }
                $this->db->query("UPDATE order_shipping set is_closed=1 WHERE order_id='" . (int)$order_id . "' AND country_id='" . (int)$country_id . "'");
                echo "Order #" . $order_id . " was Delivered, AWBNo: " . $awbno . "\n";
            }
        }
    }

    public function getAWBNO($order_id, $country_id)
    {
        $query = $this->db->query("SELECT AWBNO ,shipping_company FROM order_shipping WHERE order_id='" . (int)$order_id . "' AND country_id='" . (int)$country_id . "'");
        return isset($query->row) ? $query->row : false;
    }

    public function addOrderHistory($order_id, $country_id, $awbno,$delivered_to)
    {
        $comment = 'The Order status was updated from the track shipping cron job, AWBNo: ' . $awbno.", Delivered To: ".$delivered_to;
        $ids = $this->db->query("SELECT  c2o.id FROM `customerpartner_to_order` c2o LEFT JOIN customerpartner_to_customer cp2c ON (c2o.customer_id=cp2c.customer_id) WHERE order_id='" . (int)$order_id . "' AND cp2c.country_id='" . (int)$country_id . "'  AND c2o.deleted='0' ");
        foreach ($ids->rows as $order_product_id) {
            $order_product_info = $this->db->query("SELECT * FROM customerpartner_to_order WHERE id='".(int)$order_product_id['id']."' LIMIT 1 ")->row;
            $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_order SET order_product_status = '5' WHERE id = '" . $order_product_id['id'] . "'  AND deleted='0' ");
            $this->db->query("INSERT INTO " . DB_PREFIX . "customerpartner_to_order_status SET order_id = '" . (int)$order_id . "',order_status_id = '5',notify = '0',comment = '" . $this->db->escape($comment) . "',product_id = '" . $order_product_info['product_id'] . "',customer_id = '" . $order_product_info['customer_id'] . "',date_added = NOW()");
        }
        $this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '5', notify = '0', comment = '" . $this->db->escape($comment) . "', date_added = NOW() , user_id='0'");
    }

    public function getWholeOrderStatus($order_id, $admin_complete_order_status, $admin_cancel_order_status, $seller_orderstatus_id = 0)
    {

        $allOrderStatus = array();

        $getAllStatus = $this->db->query("SELECT order_product_status FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . $order_id . "'  AND deleted='0' ")->rows;

        foreach ($getAllStatus as $key => $value) {

            array_push($allOrderStatus, $value['order_product_status']);
        }

        $isSameSatus = array_unique($allOrderStatus);

        if (count($isSameSatus) == 1) {
            return $isSameSatus[0];
        } else {
            $check_cancel_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . (int)$order_id . "'  AND deleted='0'  AND order_product_status = '" . $admin_cancel_order_status . "'");
            $check_cancel = $check_cancel_query->num_rows;

            $order_items_array = array();
            $check_total_order_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . (int)$order_id . "'  AND deleted='0' ");
            $result = $check_total_order_query->rows;
            foreach ($result as $k => $v) {
                array_push($order_items_array, $v['order_product_status']);
            }
            $check_total_order = count(array_unique($order_items_array));
            if ($check_total_order - $check_cancel == 1) {
                $getOtherStatusQuery = $this->db->query("SELECT order_product_status FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . (int)$order_id . "' AND order_product_status != '" . $admin_cancel_order_status . "'  AND deleted='0' ");
                $getOtherStatus = $getOtherStatusQuery->row;

                return $getOtherStatus['order_product_status'];
            } else {
                return false; //$this->config->get('config_order_status_id');
            }
        }
    }

    public function changeWholeOrderStatus($order_id, $order_status_id)
    {

        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'  AND deleted='0'");
    }


}

$tracker = new trackShipment();

$orders = $tracker->db->query("SELECT order_id,country_id FROM `order_shipping` WHERE is_closed=0");

foreach ($orders->rows as $row) {

    $tracker->track($row['order_id'], $row['country_id']);

}
