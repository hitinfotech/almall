<?php

require_once('Cron.php');

class ImagesBeasnTalkdToS3 extends Cron {

    public $config_image;

    function __construct() {

        exec("ps aux | grep -i ImagesBeasnTalkdToS3 |grep -v grep | wc -l", $pids);
        if ($pids[0] > 20) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }

        parent::__construct();
        $this->config_image = config_image::getInstance();

        $this->cloudinary = new Cloudinaryclass();
        $this->registry->set('cloudinary', $this->cloudinary);
    }

    public function run() {
        $beansTalkd = new Beanstalk(array('server1' => array(
                'host' => BEANSTALKD_SERVER,
                'port' => BEANSTALKD_SERVER_PORT,
                'weight' => 50,
                'timeout' => 10,
                // array of connections/tubes
                'connections' => array(),
        )));
        $client = $beansTalkd->getClient();
        $client->watch('IMAGES');

        $closeTime = time() + 60 * 60 * 1;
        while (time() < $closeTime) {

            $job = $client->reserve(1);
            if ($job) {
                $client->delete($job['id']);
                $image_id = (int) $job['body'];
                $image = $this->db->query("SELECT * FROM `image_s3` WHERE `id`='$image_id'");
                if (($data = $image->row) && $data['status'] = 'INQUEUE') {
                    $this->db->query("UPDATE `image_s3` set `status`='RESIZING'  WHERE `id`='$image_id' ");


                    $width = $data['width'];
                    $height = $data['height'];
                    $image = $data['image'];
                    $new_image_path = $data['new_image_path'];

                    if (file_exists(DIR_IMAGE . $image)) {
                        if ($width != null && $height != null) {
                            if ($height == $this->config_image->get('product', 'listing', 'hieght') && $width == $this->config_image->get('product', 'listing', 'width')) {
                                // when I did the task the returned url from the function cloudinary_url was returning a wring url so I needed to do the string manully
                                $image_url = $this->cloudinary->upload(DIR_IMAGE . $image, $image . "_1")['url'];
                                $image_string = explode("upload/", $image_url);
                                $trimmed_image_url = $image_string[0] . "upload/e_trim/" . $image_string[1];
                                if (!$this->cloudinary->checkIfImageExist($trimmed_image_url)) {
                                    $trimmed_image_url = cloudinary_url($image . "_1.jpg", array("effect" => "trim"));
                                }

                                // I will make the default extension jpg and if the file was not exist then cloudinary will create it automatically.
                                file_put_contents(DIR_IMAGE . $image . "_1.jpg", file_get_contents($trimmed_image_url));
                                $this->resize($image . "_1.jpg", $new_image_path, $width, $height);
                                echo DIR_IMAGE . $image . "_1.jpg" . "\n";
                            } else {
                                $this->resize($image, $new_image_path, $width, $height);
                            }
                        } else {
                            $this->myEcho("no resize data ");
                        }
                        if ($data['upload_image'] == 1 && $new_image_path != null) {
                            $this->upload($new_image_path);
                        }
                    }
                    $this->db->query("UPDATE `image_s3` set `status`='DONE'  WHERE `id`='$image_id' ");
                } else {
                    $this->myEcho("not valid id or removed image");
                }
            } else {
                $this->myEcho('no images in tube sleep 2 seconds');
                sleep(2);
            }
        }
        $this->myEcho('killed this old process to start a new one');
        $client->disconnect();
    }

    public function resize($image, $new_image_path, $width, $height) {

        $directories = explode('/', dirname(str_replace('../', '', $new_image_path)));
        $path = "";
        foreach ($directories as $directory) {
            $path = $path . '/' . $directory;
            if (!is_dir(DIR_IMAGE . $path)) {
                mkdir(DIR_IMAGE . $path, 0777);
                exec("chmod -R 777 " . DIR_IMAGE . $path);
            }
        }
        $im = new imagick(DIR_IMAGE . $image);
        $imageprops = $im->getImageGeometry();
        $old_width = $imageprops['width'];
        $old_height = $imageprops['height'];
        if ($old_width > $old_height) {
            $newHeight = $height;
            $newWidth = ($height / $old_height) * $old_width;
        } else {
            $newWidth = $width;
            $newHeight = ($width / $old_width) * $old_height;
        }

        exec(" convert " . escapeshellarg(DIR_IMAGE . $image) . " -background white -density 72 -resize " . $newWidth . "x" . $newHeight . " -quality 100 " . escapeshellarg(DIR_IMAGE . $new_image_path));
        $this->myEcho("resize done image ." . $new_image_path);
    }

    public function upload($image) {
        $zipped = 'CDNUploads/' . $image;

        $path = '';

        $directories = explode('/', dirname(str_replace('../', '', $zipped)));

        foreach ($directories as $directory) {
            $path = $path . '/' . $directory;

            if (!is_dir(DIR_IMAGE . $path)) {
                @mkdir(DIR_IMAGE . $path, 0777);
            }
        }

        exec('cp "' . DIR_IMAGE . $image . '" "' . DIR_IMAGE . $zipped . '"');
        $s3_name = $image;

        exec('gzip -f -9 "' . DIR_IMAGE . $zipped . '"');

        $aws = "aws s3 cp '" . DIR_IMAGE . $zipped . ".gz' 's3://" . S3_BUKET_NAME_CDN . "/" . $s3_name . "' --expires 2034-01-01T00:00:00Z --acl public-read-write --content-encoding gzip --cache-control max-age=3600,public";
        exec($aws);
        $this->myEcho("upload done image." . $image);
    }

}

$image = new ImagesBeasnTalkdToS3();
$image->run();
