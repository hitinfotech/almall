<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

require 'vendor/autoload.php';

$mgClient = new \Mailgun\Mailgun(MAILGUN_KEY);

function send_to_mailgun($message, $mgGUN, $domain = MAILGUN_DOMAIN) {
    $message_json = $message;

    $mail = json_decode($message_json);
    
    if (is_object($mail)) {
        $data = array();
        $data['from'] = "{$mail->sender} <{$mail->from}>";
        //$data['sender'] = $mail->sender;
        $data['to'] = $mail->to;
        $data['subject'] = $mail->subject;
        $data['html'] = $mail->text;
        
        $result = $mgGUN->sendMessage($domain, $data);

        return $result;
    }

    //$SQS_Client->Delete($queue_handle);

    /* $command = "curl -s --user 'api:" . $mgClientKEY . "' "
      . "https://api.mailgun.net/v3/" . $domain . "/messages "
      . " -F from='" . $mail->sender . " <" . $mail->from . ">'"
      . " -F to=" . $mail->to  //-F to=qasem.m.alomari@gmail.com
      . " -F subject='" . $mail->subject . "' "
      . " -F text='$mail->text'";

      exec($command); */
}

require_once DIR_SYSTEM . 'library/aws/aws-autoloader.php';
$AWS = \Aws\Common\Aws::factory(DIR_SYSTEM . 'library/aws/Aws/config/aws-config.php');
$sqs_Client = $AWS->get('sqs');

$loop = 1000;
while ($loop > 0) {
    $res = $sqs_Client->receiveMessage(array(
        'QueueUrl' => SQS_URL,
        'WaitTimeSeconds' => 1
            )
    );

    if ($res->getPath('Messages')) {
        foreach ($res->getPath('Messages') as $msg) {
            echo "Received Msg: " . $msg['Body'];

            try {
                send_to_mailgun($msg['Body'], $mgClient, MAILGUN_DOMAIN);
            } catch (Exception $ex) {
                
            }

            $res = $sqs_Client->deleteMessage(array(
                'QueueUrl' => SQS_URL,
                'ReceiptHandle' => $msg['ReceiptHandle']
            ));
        }
    } else {
        $loop = 0;
    }
    
    $loop--;
}