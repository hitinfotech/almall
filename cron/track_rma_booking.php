<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';
require_once realpath(__DIR__) . '/firstflight.php';

class trackRMABooking {

    public $db;
    private $FFServices;

    public function __construct() {
        $this->db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
        $this->FFServices = FirstFlight::getInstance();
    }

    public function track($order_id, $country_id, $return_id) {

        $awbno= $this->getAWBNO($order_id, $country_id,$return_id);
        $booking_response = $this->FFServices->BookingTrack(array('BookNo' => $awbno));
        var_dump($booking_response);

    }

    public function getAWBNO($order_id, $country_id,$return_id){
        $query = $this->db->query("SELECT AWBNO FROM return_booking WHERE order_id='".(int) $order_id."' AND country_id='".(int) $country_id."' AND return_id='".(int)$return_id."'");
        return $query->row['AWBNO'];
    }


}

$tracker = new trackRMABooking();

$orders = $tracker->db->query("SELECT order_id,country_id,return_id FROM `return_booking` WHERE is_closed=0 ");

foreach ($orders->rows as $row) {

    $tracker->track($row['order_id'],$row['country_id'],$row['return_id']);

}
