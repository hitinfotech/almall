<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

class RedoSellerOrdes {

    private $db;

    public function __construct() {
        $this->db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
    }

    public function DoFlush_Data() {

        $this->import();

        $count = 0;

        $results = $this->db->query(" SELECT * FROM customerpartner_to_order   WHERE deleted='0'  ");
        foreach ($results->rows as $row) {
            $sql = '';
            $count++;
            $product_info = $this->db->query("SELECT * FROM order_product WHERE order_product_id='" . (int) $row['order_product_id'] . "'");


            $seller_info = $this->db->query("SELECT commission FROM customerpartner_to_customer WHERE customer_id = '" . (int) $row['customer_id'] . "'");

            //die(var_dump($seller_info->row['commission']));
            if ($seller_info->num_rows) {
                // product_id=42415, price = 45, admin=45*25/100, customer=45*75/100;
                $sql = " UPDATE customerpartner_to_order SET product_id='" . (int) $product_info->row['product_id'] . "', price= '" . $product_info->row['total'] . "', admin='" . ($product_info->row['total'] * $seller_info->row['commission'] / 100) . "',customer='" . ($product_info->row['total'] - ($product_info->row['total'] * $seller_info->row['commission'] / 100)) . "' WHERE id = '" . (int) $row['id'] . "'  AND deleted='0' ";
                echo "\n" . $sql;
                $this->db->query($sql);
            } else {
                echo "\n{$row['order_id']} not found {$row['product_id']}";
            }
        }
        return $count;
    }

    private function import() {
        $result = $this->db->query("SELECT * FROM order_product_temp ");
        foreach ($result->rows as $row) {
            $sql = " UPDATE order_product SET "
                    . " product_id = '" . $row['product_id'] . "' "
                    . ", name = '" . $row['name'] . "' "
                    . ", model= '" . $row['model'] . "' "
                    . ", quantity = '" . $row['quantity'] . "' "
                    . ", price = '" . $row['price'] . "' "
                    . ", total = '" . $row['total'] . "' "
                    . ", tax = '" . $row['tax'] . "' "
                    . ", reward = '" . $row['reward'] . "' "
                    . ", otp_id = '" . $row['otp_id'] . "'  WHERE order_product_id = '" . (int) $row['order_product_id'] . "'";
            $this->db->query($sql);
        }
    }

}

$ob = new RedoSellerOrdes();
$res = $ob->DoFlush_Data();
die("\n\nSuccess {$res} orders \n\n");
