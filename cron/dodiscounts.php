<?php

require_once realpath(__DIR__) . '/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';


if (!defined('DIR_APPLICATION')) {
    die("\nError Initialaize cron \n");
    exit;
}


require_once(DIR_SYSTEM . 'startup.php');

// Registry
$registry = Registry::getInstance();

// Config
$config = Config::getInstance();
$registry->set('config', $config);

// Database
$db = DB::getInstance(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
$registry->set('db', $db);

$config->set('config_store_id', 0);

$config->set('config_url', HTTP_SERVER);
$config->set('config_ssl', HTTPS_SERVER);

$registry->set('config', $config);

// Language
/* $languages = array();
  $query = $db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE status = '1'");
  foreach ($query->rows as $result) {
  $languages[$result['code']] = $result;
  }

  $language_code = 'ar';

  $language = new Language($languages[$language_code]['directory']);
  $language->load($languages[$language_code]['directory']);
  $registry->set('language', $language);
 */

class DoOffers {

    public function __construct($registry) {

        $this->config = $registry->get('config');

        $this->db = $registry->get('db');
    }

    public function Start() {

        $sql = "SELECT * FROM brand_discounts_cronj WHERE deleted = 0 AND done = 0 ";
        $crons = $this->db->query($sql);
        foreach ($crons->rows as $cron) {
            $category_id = (int) $cron['category_id'];
            $brand_id = (int) $cron['brand_id'];

            if ((int) $category_id > 0) {

                $categories = $this->getcategories($category_id);

                foreach ($categories as $row) {
                    if ($brand_id > 0) {
                        $sql = " SELECT p.product_id FROM product_to_category p2c LEFT JOIN product p ON(p2c.product_id=p.product_id) WHERE p2c.category_id='" . (int) $row . "' AND p.brand_id = '" . (int) $brand_id . "' ";
                    } else {
                        $sql = " SELECT p.product_id FROM product_to_category p2c LEFT JOIN product p ON(p2c.product_id=p.product_id) WHERE p2c.category_id='" . (int) $row . "' ";
                    }

                    $products = $this->db->query($sql);
                    if ($products->num_rows) {
                        $this->dooffer($products->rows, $cron);
                    }
                }
            } else {
                $sql = "SELECT product_id FROM product p WHERE p.brand_id = '" . (int) $brand_id . "'";
                $products = $this->db->query($sql);
                if ($products->num_rows) {
                    $this->dooffer($products->rows, $cron);
                }
            }

            $this->db->query("UPDATE brand_discounts_cronj SET done=1 WHERE id='" . (int) $cron['id'] . "'");
        }
    }

    private function dooffer($products, $special) {
        if (isset($special['done']) && $special['done'] == 0) {
            foreach ($products as $row) {
                $percent = floatval($special['percent']);
                $date_start = $special['date_start'];
                $date_end = $special['date_end'];

                $this->db->query("UPDATE product_special SET priority=priority+1 WHERE  product_id='" . (int) $row['product_id'] . "' AND deleted='no' ");

                $price = $this->db->query("select price from product where product_id = '" . $row['product_id'] . "' ");
                if ($price->num_rows) {
                    $value = $price->row['price'] - $price->row['price'] * $percent;
                    $this->db->query("INSERT INTO product_special SET product_id='" . (int) $row['product_id'] . "', customer_group_id = 1, priority = 0 , price = '" . $value . "',percent=".$percent." , date_start = '" . $this->db->escape($date_start) . "',date_end='" . $this->db->escape($date_end) . "', cron='" . $special['id'] . "', generated_by='admin_mass', user_id= ".(int)$special['user_id']);
                }

                $this->db->query("INSERT IGNORE INTO algolia SET `type`='product', type_id='" . (int) $row['product_id'] . "' ");
            }
        }
    }

    private function getcategories($category_id) {
        $result[$category_id] = $category_id;

        $categories = $this->db->query("select category_id from category where parent_id = '" . (int) $category_id . "'");
        if ($categories->num_rows) {
            foreach ($categories->rows as $row) {
                foreach ($this->getcategories($row['category_id']) as $rows) {
                    if (!in_array($rows, $result)) {
                        $result[$rows] = $rows;
                    }
                }
            }
        }

        return $result;
    }

}

$test = new DoOffers($registry);
$test->Start();

die("\nFINISH SENDING\n");
