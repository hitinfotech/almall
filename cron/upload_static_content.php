<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

require_once DIR_SYSTEM . 'library/aws_s3/S3.php';

$s3_client = new S3(AWS_CREDENTIALS_KEY, AWS_CREDENTIALS_SECRET, FALSE);

$errors = array();

$path1 = realpath(DIR_APPLICATION . 'view/theme/sayidaty/javascript');
$objects1 = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path1), RecursiveIteratorIterator::SELF_FIRST);
foreach ($objects1 as $name => $object) {

    if (is_file($name)) {

        $extension = strtolower(pathinfo($name, PATHINFO_EXTENSION));

        if (strpos($name, '.min.') == false && in_array($extension, array('js', 'css'))) {
            $name1 = substr($name, 0, (strlen($name) - strlen(".{$extension}")));
            $filename = $name1 . '.min.' . $extension;
            $cmd1 = " yui-compressor " . $name . " > " . $filename . "";
            exec($cmd1);
        } else {
            $filename = $name;
        }

        $filename = 'static/' . substr($filename, strlen(DIR_APPLICATION . 'view/theme/sayidaty/'));

        echo "upload {" . ltrim($name, DIR_TEMPLATE) . '} => {' . $filename . '}';

        $info = $s3_client->getObjectInfo(S3_BUKET_NAME_CDN, $filename);
        if ($info) {
            $s3_client->deleteObject(S3_BUKET_NAME_CDN, $filename);
        }

        $s3_client->putObjectFile($name, S3_BUKET_NAME_CDN, $filename);
        echo "  ==> Uploaded. \n";
    }
}

$path2 = realpath(DIR_APPLICATION . 'view/theme/sayidaty/css');
$objects2 = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path2), RecursiveIteratorIterator::SELF_FIRST);
foreach ($objects2 as $name => $object) {

    if (is_file($name)) {

        $extension = strtolower(pathinfo($name, PATHINFO_EXTENSION));

        if (strpos($name, '.min.') == false && in_array($extension, array('js', 'css'))) {
            $name1 = substr($name, 0, (strlen($name) - strlen(".{$extension}")));
            $filename = $name1 . '.min.' . $extension;
            $cmd1 = " yui-compressor " . $name . " > " . $filename . "";
            exec($cmd1);
        } else {
            $filename = $name;
        }

        $filename = 'static/' . substr($filename, strlen(DIR_APPLICATION . 'view/theme/sayidaty/'));

        echo "uploading file ... {" . $filename . '}';

        $info = $s3_client->getObjectInfo(S3_BUKET_NAME_CDN, $filename);
        if ($info) {
            $s3_client->deleteObject(S3_BUKET_NAME_CDN, $filename);
        }

        $s3_client->putObjectFile($name, S3_BUKET_NAME_CDN, $filename);
        echo "  ==> Uploaded. \n";
    }
}

$path3 = realpath(DIR_APPLICATION . 'view/theme/sayidaty/fonts');
$objects3 = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path3), RecursiveIteratorIterator::SELF_FIRST);
foreach ($objects3 as $name => $object) {

    if (is_file($name)) {

        $filename = $name;
        
        $filename = 'static/' . substr($filename, strlen(DIR_APPLICATION . 'view/theme/sayidaty/'));

        echo "uploading file ... {" . $filename . '}';

        $info = $s3_client->getObjectInfo(S3_BUKET_NAME_CDN, $filename);
        if ($info) {
            $s3_client->deleteObject(S3_BUKET_NAME_CDN, $filename);
        }

        $s3_client->putObjectFile($name, S3_BUKET_NAME_CDN, $filename);
        echo "  ==> Uploaded. \n";
    }
}
die("\nFINISH UPLOADING\n");
