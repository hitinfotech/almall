<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

$db = new DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

$data = $db->query("SELECT tip_id,language_id, body FROM tip_description ORDER BY tip_id DESC");
foreach($data->rows as $row){
    $db->query("UPDATE tip_description SET body='". $db->escape(change_body($row['body'])) ."' WHERE tip_id = '". (int)$row['tip_id'] ."' AND language_id='". (int)$row['language_id'] ."' ");
}

function change_body($body='') {
    while(strpos($body, 'uploads')){
        // mall.sayidaty.net//uploads/f/c/
        $newstr = substr($body,strpos($body,'uploads'),30);
        $arr = explode('/', $newstr);
        $filename= end($arr);
        $comp = substr($filename, 0, 1) . "/" . substr($filename, 1, 1) . "/" . substr($filename, 2, 1) . "/" . substr($filename, 3, 1) . "/" . substr($filename, 4, 1);
        $body =  substr($body,0,strpos($body,'uploads')). 'image/Product/'.$comp .substr($body,strpos($body,'uploads')+11);
    }
    return $body;
}
