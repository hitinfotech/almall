<?php
require_once('Cron.php');

class NotFoundImagesCopier extends Cron
{
    function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $images = $this->db->query(" SELECT product_image_id,product_id,image FROM `product_image`");
        if ($images && $images->num_rows > 0) {
            foreach ($images->rows as $row) {
              if(! file_exists(DIR_IMAGE . $row['image'])){
                $this->create_path(DIR_IMAGE . $row['image']);
                copy('https://mall.sayidaty.net/image/' . $row['image'],DIR_IMAGE . $row['image']);
                $this->myEcho("Image ". DIR_IMAGE . $row['image'] . 'was copied ');
              }
          }
        }
    }

   private function create_path($dir_path) {
       $path = rtrim($dir_path, '/');
       $directories = explode("/", $path);
       $length = count($directories) -1 ;
       $count =0;

       $string = '';
       foreach ($directories as $directory) {
         if(!strpos($directory,'.')){
            $string .= "/{$directory}";
           if (!is_dir($string)) {
               exec("mkdir -m 777 {$string}");
           }
         }
       }
   }


}

$image = new NotFoundImagesCopier();
$image->run();
