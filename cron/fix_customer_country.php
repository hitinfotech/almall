<?php

namespace FlushCache;

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once realpath(__DIR__) . '/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

$db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);

$db->query(" UPDATE customer SET country_id = NULL WHERE country_id = 0 ");

$customers = $db->query(" SELECT ip, customer_id FROM customer WHERE country_id <> 221 AND LENGTH(ip) >0  ");

foreach ($customers->rows as $row) {
    $country_id = getRealId($row['ip']);
    if($country_id && $country_id != 222){
        $db->query(" UPDATE customer SET country_id = '". (int)$country_id ."' WHERE customer_id = '". (int)$row['customer_id'] ."' ");
        echo "Customer ".(int)$row['customer_id']." UPDATED \n";
    }
}

function getRealId($ip_address) {
    $db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
    $ip = getIpAddress($ip_address);

    require_once (DIR_SYSTEM . 'geoip/geoip.inc');

    $gi = geoip_open(DIR_SYSTEM . 'geoip/GeoIP.dat', GEOIP_STANDARD);
    $country_code = geoip_country_code_by_addr($gi, $ip);
    geoip_close($gi);
    $code = strtolower($country_code);
    $info = $db->query("SELECT country_id FROM country WHERE iso_code_2 = '" . $db->escape($code) . "'");
    if ($info->num_rows) {
        return $info->row['country_id'];
    }
    return 0;
}

function getIpAddress($ip) {
    $tmpip = $ip;

    $result = explode(",", $tmpip);

    return trim(reset($result));
}
