<?php

require_once realpath(__DIR__) . '/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

require_once(DIR_SYSTEM . 'library/algolia/algoliasearch.php');

class products_ranking {

    public $db;
    public $PointsByDateRange;
    public $PointsBySaleRange;
    public $PointsByCartRateRange;
    public $PointsByFavouriteRange;
    public $PointsByRankOnVisitRange;

    public function __construct() {
        $this->db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
        $settings = array();
        $query = $this->db->query("SELECT `code`,`from`,`to`,`value` FROM product_ranking_algorithm  WHERE deleted='no'");
        if($query->num_rows){
          foreach($query->rows as $key =>$value){
            $settings[$value['code']][] = $value;
          }
        }
        $this->PointsByDateRange = (!empty($settings['new_product_points']) ? $settings['new_product_points'] : array());
        $this->PointsBySaleRange = (!empty($settings['sale_points']) ? $settings['sale_points'] : array());
        $this->PointsByCartRateRange = (!empty($settings['cart_rate_points']) ? $settings['cart_rate_points'] : array());
        $this->PointsByFavouriteRange = (!empty($settings['favorite_points']) ? $settings['favorite_points'] : array());
        $this->PointsByRankOnVisitRange = (!empty($settings['visited_points']) ? $settings['visited_points'] : array());
    }

    public function rank($product_id) {
        $product_data = $this->db->query("SELECT (SELECT count(product_id) FROM customer_views WHERE product_id='" . (int) $product_id . "' ) AS views_count , (SELECT count(product_id) FROM cart WHERE product_id='" . (int) $product_id . "' ) AS cart_count,(SELECT count(product_id) FROM customer_wishlist WHERE product_id='" . (int) $product_id . "') fav_count, DATEDIFF(CURRENT_DATE(), p.date_added) new_product ,ifnull(ifnull(ps.price,0)/p.price,0) AS sale, p.sort_order pushvalue FROM product p LEFT JOIN product_special ps ON (p.product_id = ps.product_id AND ps.deleted='no' AND ps.date_end > NOW()) WHERE p.product_id='" . (int) $product_id . "'");
        $rank_on_visit_points = $this->getPointsByRankOnVisit($product_data->row['views_count']);
        $cart_points = ($product_data->row['views_count'] > 0 ) ? $this->getPointsByCartRate($product_data->row['cart_count'] / $product_data->row['views_count']) : 0;
        $date_points = $this->getPointsByDate($product_data->row['new_product']);
        $fav_points = $this->getPointsByFavourite($product_data->row['fav_count']);
        $sale_points = $this->getPointsBySale($product_data->row['sale']);
        $pushvalue = $product_data->row['pushvalue'];

        $total_points = $rank_on_visit_points + $cart_points + $date_points + $fav_points + $sale_points;
        $this->db->query("UPDATE product SET algorithm = '" . (int) $total_points . "', algolia='". ((int)$total_points+(int)$pushvalue) ."' where product_id='" . (int) $product_id . "'");
        $this->db->query("INSERT IGNORE INTO algolia SET type='product', type_id='" . (int) $product_id . "'");
        echo "Product #" . $product_id . " was updated" . "\n";
    }

    public function getPointsByRankOnVisit($num_of_views) {
        if(!empty($this->PointsByRankOnVisitRange)){
          foreach($this->PointsByRankOnVisitRange as $key => $value){
          if (($num_of_views >= $value['from']) && ($num_of_views <= $value['to'] || $value['to'] == 0)) {
              return $value['value'];
          }
        }
      }
      return 0 ;
    }

    public function getPointsByCartRate($ratio) {
      if(!empty($this->PointsByCartRateRange)){
        foreach($this->PointsByCartRateRange as $key => $value){
          if (($ratio >= $value['from']) && ($ratio <= $value['to'] || $value['to'] == 0)) {
              return $value['value'];
          }
        }
      }
    }

    public function getPointsByDate($days) {
        if(!empty($this->PointsByDateRange)){
          foreach($this->PointsByDateRange as $key => $value){
          if (($days >= $value['from']) && ($days <= $value['to'] || $value['to'] == 0)) {
              return $value['value'];
          }
        }
      }
    }

    public function getPointsByFavourite($fav_count) {

        if(!empty($this->PointsByFavouriteRange)){
          foreach($this->PointsByFavouriteRange as $key => $value){
          if (($fav_count >= $value['from']) && ($fav_count <= $value['to'] || $value['to'] == 0)) {
              return $value['value'];
          }
        }
      }
    }

    public function getPointsBySale($sale_value) {
      $sale_value  = (int)( $sale_value * 100);
      if(!empty($this->PointsBySaleRange)){
        foreach($this->PointsBySaleRange as $key => $value){
          if (($sale_value >= $value['from']) && ($sale_value <= $value['to'] || $value['to'] == 0)) {
              return $value['value'];
          }
        }
      }
    }

}

class AlgoliaIndex {

    private $algolia;
    private $images_dim;
    private $db;
    private $langs = array('ar', 'en');
    private $countries;
    private $currencies;
    private $ranker;

    public function __construct() {
        $this->ranker = new products_ranking();
        $this->algolia = new \AlgoliaSearch\Client(ALGOLIA_APPID, ALGOLIA_ADMIN_API_KEY);
        $this->algolia->setConnectTimeout(100);
        $this->images_dim = config_image::getInstance();
        $this->db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
        $count = $this->db->query("select * from country where available=1");
        foreach ($count->rows as $row) {
            $this->countries[$row['country_id']] = strtolower($row['iso_code_2']);
            $this->currencies[$row['country_id']] = $row['curcode'];
        }

        $this->countries[0] = 'un';
        $this->currencies[0] = 'USD';
    }

    public function DoIndex() {

        $results = $this->db->query(" SELECT row_id, type_id, type FROM algolia WHERE piked_date < (NOW() - INTERVAL 2 HOUR) OR piked_date IS NULL ORDER BY row_id ASC limit 1 ");
        $count = 0;
        foreach ($results->rows as $result) {
            $this->db->query(" UPDATE algolia SET piked = 1 , piked_date = NOW() WHERE row_id = '" . (int) $result['row_id'] . "' ");

            $products = array();
            switch ($result['type']) {
                case 'product':
                    $product = $this->db->query(" SELECT product_id FROM product WHERE product_id='" . (int) $result['type_id'] . "' ORDER BY product_id DESC");
                    if ($this->indexProducts($product->row['product_id'])) {
                        $count++;
                    }
                    break;
                case 'brand':
                    $products = $this->db->query(" INSERT IGNORE INTO algolia(`type`,`type_id`)(SELECT 'product', product_id FROM product WHERE brand_id='" . (int) $result['type_id'] . "' ORDER BY product_id DESC)");
                    break;
                case 'shop':
                    $products = $this->db->query(" INSERT IGNORE INTO algolia(`type`,`type_id`)(SELECT 'product',product_id FROM product_to_store WHERE store_id='" . (int) $result['type_id'] . "' ORDER BY product_id DESC)");
                    break;
                case 'seller':
                    $products = $this->db->query(" INSERT IGNORE INTO algolia(`type`,`type_id`)(SELECT 'product',product_id FROM customerpartner_to_product WHERE customer_id='" . (int) $result['type_id'] . "' ORDER BY product_id DESC)");
                    break;
                case 'category':
                    $products = $this->db->query(" INSERT IGNORE INTO algolia(`type`,`type_id`)(SELECT 'product',product_id FROM product_to_category WHERE category_id='" . (int) $result['type_id'] . "' ORDER BY product_id DESC)");
                    break;
            }

            $this->db->query(" DELETE FROM algolia WHERE row_id = '" . (int) $result['row_id'] . "'");
        }
        return $count;
    }

    private function indexProducts($product_id = 0) {

        $this->ranker->rank($product_id);

        $index = $this->algolia->initIndex(ALGOLIA_INDEX_MAIN);

        foreach ($this->countries as $code) {
            $up_code = strtoupper($code);
            ${"index_shop_" . $code} = $this->algolia->initIndex(constant("ALGOLIA_INDEX_SHOP_" . $up_code));
            ${"index_offer_" . $code} = $this->algolia->initIndex(constant("ALGOLIA_INDEX_OFFER_" . $up_code));
        }

        echo "Indexing Product: {$product_id} ==> ";

        $index->deleteObject($product_id);
        foreach ($this->countries as $code) {
            ${"index_shop_" . $code}->deleteObject($product_id);
            ${"index_offer_" . $code}->deleteObject($product_id);
        }

        $sql = " SELECT DISTINCT p.product_id, p.algorithm, p.price, p.quantity, p.sku, p.model, p.image, p.date_added, p.viewed, p.date_modified, p.status, p.stock_status_id, p.brand_id, b.image brand_image, b.sort_order brand_priority, b.power, b.show_api, bd1.name brand_en, bd2.name brand_ar, pd1.name AS name_en, pd2.name AS name_ar, pd1.description AS description_en, pd2.description AS description_ar, p.image, (SELECT ss.name FROM stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '1') AS stock_status, p.sort_order "
                . "FROM product p "
                . "LEFT JOIN product_description pd1 ON (p.product_id = pd1.product_id) "
                . "LEFT JOIN product_description pd2 ON (p.product_id = pd2.product_id) "
                . "LEFT JOIN product_to_store p2s ON (p.product_id = p2s.product_id) "
                . "LEFT JOIN manufacturer m ON (p.manufacturer_id = m.manufacturer_id) "
                . "LEFT JOIN brand b ON(p.brand_id=b.brand_id) "
                . "LEFT JOIN brand_description bd1 ON(b.brand_id=bd1.brand_id) "
                . "LEFT JOIN brand_description bd2 ON(b.brand_id=bd2.brand_id) "
                . "WHERE p.product_id = '" . (int) $product_id . "' "
                . "AND pd1.language_id = '1' AND pd2.language_id='2' "
                . "AND p.status = '1' "
                . "AND p.quantity > '0' "
                . "AND p.is_algolia = '1' "
                . "AND b.is_algolia = '1' "
                . "AND b.status = '1' "
                . "AND p.date_available <= NOW() "
                . "AND p2s.store_id = '0' "
                . "AND bd1.language_id='1' "
                . "AND bd2.language_id ='2' ";


        $product = $this->db->query($sql);

        if ($product->num_rows) {

            $info = $product->row;
            $data = array();

            $data["objectID"] = (int) $info['product_id'];
            $data['viewed'] = (int) $info['viewed'];
            $data['status'] = (int) $info['status'];
            $data['sku'] = $info['sku'];
            $data['quantity'] = $info['quantity'];
            $data['model'] = $info['model'];
            $data['sort_index'] = $info['sort_order'];
            $data['algorithm'] = (int)$info['algorithm'];
            $data['sum_sort'] = (int)$info['sort_order'] + (int)$info['algorithm'];
            $data['stock_status_id'] = (int) $info['stock_status_id'];
            $data['show_api'] =  $info['show_api'];

            //////////
            /* $data['stock_status'] = array(
              'en' => $info['stock_status'],
              'ar' => $info['stock_status']
              );
             */

            if ($info['stock_status_id'] == 10) {
                $data['stock_status'] = array(
                    'en' => 'All-Products',
                    'ar' => 'جميع-المنتجات'
                );
                //$data['stock_status'] = 'notshopable';
            } else {
                $data['stock_status'] = array(
                    'en' => 'Shop-Now',
                    'ar' => 'تسوق-الان'
                );
                //$data['stock_status'] = 'shopable';
            }

            /////////


            $data['date_added'] = $info['date_added'];
            $data['date_modified'] = $info['date_modified'];
            $date_added = DateTime::createFromFormat("Y-m-d H:i:s", $info['date_added']);
            $data['date_added'] = $date_added->getTimestamp();
            $date_modified = DateTime::createFromFormat("Y-m-d H:i:s", $info['date_modified']);
            $data['date_modified'] = $date_modified->getTimestamp();

            if ($info['image']) {
                $data["image"] = $this->resize($info['image'], $this->images_dim->get('product', 'listing', 'width'), $this->images_dim->get('product', 'listing', 'hieght'));
            } else {
                $data["image"] = $this->resize('placeholder.png', $this->images_dim->get('product', 'listing', 'width'), $this->images_dim->get('product', 'listing', 'hieght'));
            }

            $images = $this->getProductImages($product_id);
            foreach ($images as $image) {
                $data["images"][] = $this->resize($image['image'], $this->images_dim->get('product', 'listing', 'width'), $this->images_dim->get('product', 'listing', 'hieght'));
            }

            $data['shops'] = $this->getShops($product_id);

            foreach ($this->langs as $lang) {
                foreach ($this->countries as $country) {
                    if (!isset($data["shops"][$lang][$country])) {
                        $data["shops"][$lang][$country] = (object) array();
                    }
                }
            }


            if (!$info['brand_id']) {
                echo "brand".$product_id."\n".$sql."\n";
                return 0;
            }

            $data['brand']['objectID'] = (int) $info['brand_id'];
            $data['brand']['name']['en'] = html_entity_decode($info['brand_en']);
            $data['brand']['name']['ar'] = $info['brand_ar'];
            $data['brand']['priority'] = (int) $info['brand_priority'];
            $data['brand']['url']['ar'] = $this->getBrandLink_ar(urlencode($info['brand_ar']));
            $data['brand']['url']['en'] = $this->getBrandLink_en(urlencode($info['brand_en']));

            if ($info['brand_image']) {
                $data['brand']['image'] = $this->resize($info['brand_image'], $this->images_dim->get('shops_brands_designer', 'thumbnail', 'width'), $this->images_dim->get('shops_brands_designer', 'thumbnail', 'hieght'));
            } else {
                $data['brand']['image'] = $this->resize('placeholder.png', $this->images_dim->get('shops_brands_designer', 'thumbnail', 'width'), $this->images_dim->get('shops_brands_designer', 'thumbnail', 'hieght'));
            }


            $link = $this->mylink($info['product_id']);
            $data['url']['ar'] = $link['ar'];
            $data['url']['en'] = $link['en'];

            $data['name']['en'] = html_entity_decode($info['name_en']);
            $data['name']['ar'] = html_entity_decode($info['name_ar']);
            $data['desc']['en'] = html_entity_decode($info['description_en']);
            $data['desc']['ar'] = html_entity_decode($info['description_ar']);

            $categories = $this->getCategories($product_id);
            if ($categories == FALSE) {
                echo "cats".$product_id."\n".$sql."\n";
                return 0;
            }

            $data['categories'] = $categories;
            if (empty($data['categories'])) {
                $data['categories'] = (object) array();
            } else {
                if (empty($data['categories']['ar'])) {
                    $data['categories']['ar'] = (object) array();
                }
                if (empty($data['categories']['en'])) {
                    $data['categories']['en'] = (object) array();
                }
            }

            $attributes = $this->getAttributes($product_id);

            foreach ($attributes as $key => $att_row) {
                foreach ($att_row['labels'] as $key2 => $value2) {
                    $data["{$value2}.{$key}"] = array(
                        'name' => $att_row['values'][$key2]
                    );
                }
            }

            $data['options'] = $this->getOptions($product_id);

            $indexes = array();

            // PRICES
            unset($data["prices"]);
            //SPECIALS
            unset($data["specials"]);

            $av_info = $this->db->query("SELECT cp2c.customer_id,available_country FROM product p INNER JOIN customerpartner_to_product cp2p ON(p.product_id=cp2p.product_id) INNER JOIN customerpartner_to_customer cp2c ON(cp2p.customer_id=cp2c.customer_id) WHERE p.product_id='" . (int) $product_id . "' ");

            if ($av_info->num_rows == 0) {
                echo "seller".$product_id."\n".$sql."\n";
                return 0;
            }

            $price = $info['price'];

            $special_info = $this->db->query("SELECT price FROM product_special ps WHERE price > 0 AND product_id = '" . (int) $product_id . "' AND deleted='no' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1");

            $sp_price = ($special_info->num_rows > 0 ? $special_info->row['price'] : 0);

            $data['last_price'] = (float)($sp_price==0?$price:$sp_price);
            foreach ($av_info->rows as $available) {

                if ($available['available_country'] == 0) {
                    $seller_execluded_countries = $this->db->query(" SELECT country_id FROM " . DB_PREFIX . "seller_execluded_countries WHERE customer_id =".(int)$av_info->row['customer_id']);

                    foreach ($this->countries as $key => $country) {
                        if($seller_execluded_countries->num_rows){
                          if(in_array($key,array_column($seller_execluded_countries->rows,'country_id')))
                            continue;
                        }

                        $cur_info = $this->db->query("select * from currency where status='1' AND code='" . $this->db->escape($this->currencies[$key]) . "'");

                        $new_price = $price * $cur_info->row['value'];
                        $new_sp_price = $sp_price * $cur_info->row['value'];

                        $data["prices"][$country] = round($new_price, 2);
                        $data["specials"][$country] = round($new_sp_price, 2);
                        $data["available"][$country] = 1;
                        $indexes['index_shop_' . $country] = 1;
                        if ($sp_price > 0) {
                            $indexes['index_offer_' . $country] = 1;
                        }
                    }
                } else {

                    $cur_info = $this->db->query("select * from currency where status='1' AND code='" . $this->db->escape($this->currencies[$available['available_country']]) . "'");
                    $new_price = $price * $cur_info->row['value'];
                    $new_sp_price = $sp_price * $cur_info->row['value'];

                    $data["prices"][$this->countries[$available['available_country']]] = round($new_price, 2);
                    $data["specials"][$this->countries[$available['available_country']]] = round($new_sp_price, 2);
                    $data["available"][$this->countries[$available['available_country']]] = 1;
                    $indexes['index_shop_' . $this->countries[$available['available_country']]] = 1;
                    if ($sp_price > 0) {
                        $indexes['index_offer_' . $this->countries[$available['available_country']]] = 1;
                    }
                }
            }

            if (!isset($data["prices"])) {
                $data["prices"] = array();
            }
            foreach ($this->countries as $country) {
                if (!isset($data["prices"][$country])) {
                    $data["prices"][$country] = 0;
                }
            }
            if (!isset($data["specials"])) {
                $data["specials"] = array();
            }
            foreach ($this->countries as $country) {
                if (!isset($data["specials"][$country])) {
                    $data["specials"][$country] = 0;
                }
            }
            if (!isset($data["available"])) {
                $data["available"] = array();
            }
            foreach ($this->countries as $country) {
                if (!isset($data["available"][$country])) {
                    $data["available"][$country] = 0;
                }
            }

            $data['_count_likes'] = (int) (rand(0, 500000));
            $data["likes"] = array("user_id" => 1);

            // IF NO SELLER NO PRODUCT
            $seller = $this->getSeller($data["objectID"]);

            if(empty($indexes)){
                die("\nno indexes::: ".$product_id."\n");
            }
            foreach ($indexes as $key => $value) {
                if ($key == 'index' && $value == 1) {
                    $index->addObject($data);
                }

                foreach ($this->countries as $code) {
                    if ($key == 'index_shop_' . $code && $value == 1) {
                        if ($info['stock_status_id'] != 10 && $seller != 0) {
                            ${"index_shop_" . $code}->addObject($data);
                            if ($data["specials"][$code] > 0) {
                                ${"index_offer_" . $code}->addObject($data);
                            }
                        }
                    }
                }
            }

            $this->db->query(" DELETE FROM algolia WHERE type_id = '" . (int) $product_id . "' AND `type`='product' ");

            echo "DONE {$product_id} \n";
            return 1;
        } else {
            echo $product_id."\n".$sql."\n";
        }
    }

    private function getSeller($product_id) {
        $data = array();
        $query = $this->db->query("SELECT c2cd.language_id, c2c.show_in_ksa, c2c.show_in_uae, c2cd.screenname FROM customerpartner_to_product c2p LEFT JOIN customerpartner_to_customer c2c ON(c2p.customer_id=c2c.customer_id) LEFT JOIN customerpartner_to_customer_description c2cd ON(c2c.customer_id=c2cd.customer_id) WHERE c2p.product_id = '" . (int) $product_id . "' AND c2c.is_partner = 1 ");
        if ($query->num_rows) {
            return true;
        }

        return false;
    }

    private function resize($filename, $width, $height) {
        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $old_image = $filename;
        //return $old_image;
        $new_image = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
        return $new_image;
    }

    private function getShops($product_id = 0) {
        $result = array();
        foreach ($this->countries as $key2 => $row2) {
            $shops = $this->db->query(" SELECT sd.language_id, sd.name, s.store_id, s.image FROM product_to_store p2s LEFT JOIN store s ON(p2s.store_id=s.store_id) LEFT JOIN store_description sd ON(s.store_id=sd.store_id) LEFT JOIN zone z ON(s.zone_id=z.zone_id) WHERE p2s.product_id ='" . (int) $product_id . "' AND s.status='1' AND z.country_id='" . (int) $key2 . "' ");

            foreach ($shops->rows as $row) {
                $image = $this->resize($row['image'], $this->images_dim->get('shops_brands_designer', 'thumbnail', 'width'), $this->images_dim->get('shops_brands_designer', 'thumbnail', 'hieght'));
                $url_ar = $this->shoplink_ar($row['store_id']);
                $url_en = $this->shoplink_en($row['store_id']);

                if ($row['language_id'] == 2) {
                    $result["ar"][$row2][] = array('objectID' => (int) $row['store_id'], 'name' => html_entity_decode($row['name']), 'image' => $image, 'shop_url' => $url_ar);
                }

                if ($row['language_id'] == 1) {
                    $result["en"][$row2][] = array('objectID' => (int) $row['store_id'], 'name' => html_entity_decode($row['name']), 'image' => $image, 'shop_url' => $url_en);
                }
            }
        }

        return $result;
    }

    protected function getCategories($product_id = 0) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");

        $category_info = array();
        if ($query->num_rows) {
            foreach ($query->rows as $row) {
                $category_info['en'][] = $this->categoriesInfo($row['category_id'], 1);
                $category_info['ar'][] = $this->categoriesInfo($row['category_id'], 2);
            }
        }
        return $category_info;
    }

    protected function categoriesInfo($category_id, $language_id) {

        $path_info = $this->db->query(" SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR ' > ') AS name, GROUP_CONCAT(cd1.category_id ORDER BY cp.level SEPARATOR '_') AS path , c1.parent_id, c1.sort_order FROM category_path cp LEFT JOIN category c1 ON (cp.category_id = c1.category_id) LEFT JOIN category c2 ON (cp.path_id = c2.category_id) LEFT JOIN category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN category_description cd2 ON (cp.category_id = cd2.category_id) WHERE c1.is_algolia='1' AND c2.is_algolia='1' AND cd1.language_id = '" . (int) $language_id . "' AND cd2.language_id = '" . (int) $language_id . "' AND cp.category_id = '" . (int) $category_id . "' GROUP BY cp.category_id ORDER BY name ASC ");
        if ($path_info->num_rows == 0) {
            return false;
        }

        $result = array();
        foreach ($path_info->rows as $cats_row) {
            $name = str_replace(array('&amp;'), array('&'), $cats_row['name']);
            $cats = explode(' > ', $name);
            $main = '';
            $idx = 'main';

            foreach ($cats as $row) {
                $main .= $row . ":";
                $result[$idx] = rtrim($main, ":");
                $idx .= '_sub';
            }
        }
        return $result;
    }

    protected function getAttributes($product_id) {
        $result = array(
            'en' => array('labels' => array(), 'values' => array()),
            'ar' => array('labels' => array(), 'values' => array())
        );

        foreach ($result as $key => $value) {

            if ($key == 'en') {
                $language_id = 1;
            } else {
                $language_id = 2;
            }

            $product_attribute_group_data = array();

            $product_attribute_group_query = $this->db->query("SELECT ag.attribute_group_id, agd.name FROM product_attribute pa LEFT JOIN attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN attribute_group ag ON (a.attribute_group_id = ag.attribute_group_id) LEFT JOIN attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) WHERE pa.deleted != '1' AND pa.product_id = '" . (int) $product_id . "' AND agd.language_id = '" . (int) $language_id . "' GROUP BY ag.attribute_group_id ORDER BY ag.sort_order, agd.name");

            foreach ($product_attribute_group_query->rows as $product_attribute_group) {
                $product_attribute_data = array();

                $product_attribute_query = $this->db->query("SELECT a.attribute_id, ad.name, pa.text FROM product_attribute pa LEFT JOIN attribute a ON (pa.attribute_id = a.attribute_id) LEFT JOIN attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE pa.deleted != '1' AND pa.product_id = '" . (int) $product_id . "' AND a.attribute_group_id = '" . (int) $product_attribute_group['attribute_group_id'] . "' AND ad.language_id = '" . (int) $language_id . "' AND pa.language_id = '" . (int) $language_id . "' ORDER BY a.sort_order, ad.name");

                foreach ($product_attribute_query->rows as $product_attribute) {
                    $product_attribute_data[] = array(
                        'attribute_id' => $product_attribute['attribute_id'],
                        'name' => html_entity_decode($product_attribute['name']),
                        'text' => $product_attribute['text']
                    );
                }

                $product_attribute_group_data[] = array(
                    'attribute_group_id' => $product_attribute_group['attribute_group_id'],
                    'name' => $product_attribute_group['name'],
                    'attribute' => $product_attribute_data
                );
            }

            foreach ($product_attribute_group_data as $attribute_group) {
                foreach ($attribute_group['attribute'] as $attribute) {
                    $result[$key]['labels'][] = $attribute_group['name'];
                    $result[$key]['values'][] = $attribute['name'];
                }
            }
        }

        return $result;
    }

    protected function getOptions($product_id) {
        $result = array(
            'en' => array('labels' => array(), 'values' => array()),
            'ar' => array('labels' => array(), 'values' => array())
        );

        $data = array();
        foreach ($result as $key => $value) {

            if ($key == 'en') {
                $language_id = 1;
            } else {
                $language_id = 2;
            }

            $product_option_query = $this->db->query("SELECT pov.*,od.front_name as 'option' , ovd.name as value_name from product_option_value pov left join product_option po on(pov.product_option_id = po.product_option_id) left join `option` o on po.option_id = o.option_id left join option_description od on o.option_id = od.option_id left join option_value ov on o.option_id = ov.option_id left join option_value_description ovd on ov.option_value_id=ovd.option_value_id WHERE pov.deleted != '1' AND pov.option_value_id = ov.option_value_id and pov.product_id = '" . (int) $product_id . "' and od.language_id='" . (int) $language_id . "' and ovd.language_id= '" . (int) $language_id . "' and pov.quantity > 0");

            foreach ($product_option_query->rows as $product_option) {
                $data[$key]['labels'][] = $product_option['option'];
                $data[$key]['values'][] = $product_option['value_name'];
            }
        }

        return $data;
    }

    private function mylink($product_id) {

        $query_ar = $this->db->query("SELECT * FROM url_alias_ar WHERE `query` = 'product_id={$product_id}'");
        $query_en = $this->db->query("SELECT * FROM url_alias_en WHERE `query` = 'product_id={$product_id}'");

        $links = array();

        if ($query_ar->num_rows && $query_ar->row['keyword']) {
            $links['ar'] = ($query_ar->row['keyword']);
        } else {
            $links['ar'] = ('index.php?route=product/product&product_id=' . $product_id);
        }

        if ($query_en->num_rows && $query_en->row['keyword']) {
            $links['en'] = ($query_en->row['keyword']);
        } else {
            $links['en'] = ('index.php?route=product/product&product_id=' . $product_id);
        }

        return $links;
    }

    private function getBrandLink_ar($brand_name ) {
        /*$query = $this->db->query("SELECT * FROM url_alias_ar WHERE `query` = 'brand_id={$brand_id}'");
        if ($query->num_rows && $query->row['keyword']) {
            return ( $query->row['keyword']);
        } else {

        }*/
        return ( 'category?b=' . $brand_name);
    }

    private function getBrandLink_en($brand_name) {
        /*$query = $this->db->query("SELECT * FROM url_alias_en WHERE `query` = 'brand_id={$brand_id}'");
        if ($query->num_rows && $query->row['keyword']) {
            return ( $query->row['keyword']);
        } else {
            return ( 'index.php?route=brands/brand&brand_id=' . $brand_id);
        }*/
        return ( 'category?b=' . $brand_name);
    }

    private function shoplink_ar($shop_id) {

        $query = $this->db->query("SELECT * FROM url_alias_ar WHERE `query` = 'shop_id={$shop_id}'");
        if ($query->num_rows && $query->row['keyword']) {
            return ( $query->row['keyword']);
        } else {
            return ( 'index.php?route=malls/shop&shop_id=' . $shop_id);
        }
    }

    private function shoplink_en($shop_id) {

        $query = $this->db->query("SELECT * FROM url_alias_en WHERE `query` = 'shop_id={$shop_id}'");
        if ($query->num_rows && $query->row['keyword']) {
            return ( $query->row['keyword']);
        } else {
            return ( 'index.php?route=malls/shop&shop_id=' . $shop_id);
        }
    }

    private function getProductImages($product_id) {
        $query = $this->db->query("SELECT * FROM product_image WHERE product_id = '" . (int) $product_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }

}

$database = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
$database->query("CREATE TABLE IF NOT EXISTS `algolia_specials` (`row_id` int(11) PRIMARY KEY AUTO_INCREMENT,`product_special_id` INT DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8");
$database->query("INSERT IGNORE INTO `algolia`(`type`, `type_id`) ( SELECT 'product', product_id FROM `product_special` ps WHERE ps.`date_end` <= date(now()) AND ps.product_special_id NOT IN (SELECT product_special_id FROM algolia_specials) )");
$database->query("INSERT IGNORE INTO `algolia_specials`(`product_special_id`) ( SELECT `product_special_id` FROM `product_special` ps WHERE ps.`date_end` <= date(now()) AND ps.product_special_id NOT IN (SELECT product_special_id FROM algolia_specials) )");


$indexer = new AlgoliaIndex();
$counter = $isdone = 0;
while ($counter < 1000) {
    $isdone = $isdone + $indexer->DoIndex();
    $counter ++;
}
