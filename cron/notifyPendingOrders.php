<?php
require_once('Cron.php');
require 'vendor/autoload.php';

class notifyPendingOrders extends Cron
{

    public $mailGun;

    function __construct()
    {
        parent::__construct();
        $this->mailGun = new \Mailgun\Mailgun(MAILGUN_KEY);
    }

    function run(){
      $query = $this->db->query("SELECT o.order_id,cp2c.email,concat(cp2c.firstname,' ', cp2c.lastname) as seller_name, TIMESTAMPDIFF(HOUR,MAX(oh.date_added),NOW()) as pending_hours,o.date_added FROM customerpartner_to_order cp2o LEFT JOIN `order` o ON (cp2o.order_id = o.order_id) LEFT JOIN order_history oh ON (o.order_id=oh.order_id)  LEFT JOIN customerpartner_to_customer cp2c ON (cp2o.customer_id=cp2c.customer_id) WHERE oh.order_status_id=1 AND cp2o.order_product_status=1 and oh.date_added>subdate(now(), interval 25 hour)  group by  cp2o.customer_id,order_id");
      if($query->num_rows){
        foreach ($query->rows as $k => $value){
          if(in_array($value['pending_hours'],array(0,4,12,24))){
            $this->send_email($value['order_id'],$value['email'],$value['pending_hours'],$value['seller_name'],$value['date_added']);
          }
        }
      }
    }

    function send_email($order_id,$email,$pending_hours,$seller_name,$date_added){
      $reminder_number = array(
        0 => 1,
        4 => 2,
        12=> 3,
        24 => 4
      );
      $url = 'https://mall.sayidaty.net/';
      switch(SERV){
        case 'LOCAL':
            $url = 'http://commerce.sayidaty.local/';
            break;
        case 'DEVELOPMENT':
            $url = 'http://dev.mall.sayidaty.net/' ;
            break;
        default :
            $url = 'https://mall.sayidaty.net/';
      }

      $to=$email;
      $subject = 'You Order on Sayidaty Mall is pending updates, Reminder -'.$reminder_number[$pending_hours];
      $sender = 'Sayidaty Mall';
      $from = 'orders@sayidatymall.net';
      $text_greeting = 'Dear '.$seller_name;
      $text_remider = "<p>Your order <a href=".$url."partner/index.php?route=orderinfo&order_id=".$this->convertOrderNumber($order_id,$date_added).'> #'.$this->convertOrderNumber($order_id,$date_added).'</a> has been pending update for '.$pending_hours.' hours now.</p>';
      $test_more_order = '<p><span style="color: #d91a5d;">IMPORTANT: </span>Print the order invoice from your orders dashboard and hand it over to the courier with the item(s).</p>
      <p>For more information, you call us from UAE or KSA on 800 433 0033 Sunday to Thursday from 9 am to 5 pm, or send us an e-mail to mall@sayidaty.net</p>
      <p>To view your order click on the link below:</p><p> '.$url . "partner/index.php?route=orderinfo&order_id=".$this->convertOrderNumber($order_id,$date_added).'.</p>';

      $header = file_get_contents(realpath(__DIR__) . "/mail/header.tpl");
      $footer = file_get_contents(realpath(__DIR__) . "/mail/footer.tpl");

      $body = file_get_contents(realpath(__DIR__) . "/mail/body.tpl");
      $body = str_replace('#text_greeting#', $text_greeting ,$body);
      $body = str_replace('#text_remider#', $text_remider ,$body);

      $data = array();
      $data['from'] = "{$sender} <{$from}>";
      $data['to'] = $to;
      $data['subject'] = $subject;
      $data['text'] = "Email not support html";
      $data['html'] = $header.$body.$test_more_order.$footer;
      $result = $this->mailGun->sendMessage(MAILGUN_DOMAIN, $data);
    }

    public function convertOrderNumber($order_id,$date_added){
        return date('y', strtotime($date_added)).date('m', strtotime($date_added)).$order_id.date('d', strtotime($date_added));
    }
}



$notify = new notifyPendingOrders();
$notify->run();
