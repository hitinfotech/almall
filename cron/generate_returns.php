<?php
require_once realpath(__DIR__) . '/Cron.php';

class generateReturns extends Cron
{


    // private $shippingCompany;

    public function __construct()
    {
        exec("ps aux | grep -i generateReturns |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();
    }

    public function rma($return_id)
    {

        $return_info = $this->getReturn($return_id);

        if (!$return_info) {
            return false;
        }
        $data = array(
            'return_id' => $return_info['return_id'],
            "prod_type" => "DOX"
        );

        $order_id = $return_info['order_id'];

        $bookingInfo = $this->getOrderBooking($order_id);
        $order_info = $this->getOrder($order_id);
        $seller_info = $this->getProfile($return_info['customer_id']);
        // $shipping_company = $this->getShippingCompany($seller_info['country_id']);

        if ($bookingInfo['shipping_company'] == 1) {


            $operation_data = array();

            $operation_data['argAwbBooking']['Destination'] = $this->get_FFCitycode_code($order_info['shipping_zone_id']);

            $operation_data['argAwbBooking']['BRef'] = "{$order_id}/1";

            $operation_data['argAwbBooking']['ThirdParty'] = "O";
            /*
            $operation_data['argAwbBooking']['ACPhone'] = $seller_info['telephone'];
            $operation_data['argAwbBooking']['ACCPErson'] = $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname'];
            $operation_data['argAwbBooking']['ACAddress1'] = $order_info['payment_address_1'];
            $operation_data['argAwbBooking']['ACCity'] = $this->get_FFCitycode_code($order_info['payment_zone_id']) . ' / ' . $order_info['payment_city'];
            */
            $operation_data['argAwbBooking']['Consignee'] = $seller_info['firstname'] . ' ' . $seller_info['lastname'];
            $operation_data['argAwbBooking']['ConsigneeCPerson'] = $seller_info['firstname'] . ' ' . $seller_info['lastname'];
            $operation_data['argAwbBooking']['ConsigneeAddress1'] = $seller_info['address'];
            $operation_data['argAwbBooking']['ConsigneeAddress2'] = '';
            $operation_data['argAwbBooking']['ConsigneePhone'] = $seller_info['telephone'];
            $operation_data['argAwbBooking']['ConsigneeMobile'] = $seller_info['telephone'];
            $operation_data['argAwbBooking']['ConsigneeCity'] = $seller_info['zone_name'];
            $operation_data['argAwbBooking']['ConsigneeCountry'] = $seller_info['country_name'];
            $operation_data['argAwbBooking']['ConsigneePin'] = '';

            $operation_data['argAwbBooking']['ServiceType'] = 'NOR';
            $operation_data['argAwbBooking']['ProductType'] = 'XPS';
            $operation_data['argAwbBooking']['InvoiceValue'] = '0';
            $operation_data['argAwbBooking']['CODAmount'] = '0';

            $operation_data['argAwbBooking']['Weight'] = '0.00';
            $operation_data['argAwbBooking']['email'] = $seller_info['email'];
            $operation_data['argAwbBooking']['RTOShipment'] = '';
            $operation_data['argAwbBooking']['VehType'] = 'B';
            //product 1 , product 2,

            $operation_data['argAwbBooking']['AddnlInfo'] = '';
            $operation_data['argAwbBooking']['CustomerCode'] = '';
            $operation_data['argAwbBooking']['BDimension'] = '10*10*10';
            $operation_data['argAwbBooking']['Quantity'] = '1';
            $operation_data['argAwbBooking']['Pieces'] = $return_info['quantity'];

            $operation_data['argAwbBooking']['PBranch'] = $this->get_FFCitycode_code($seller_info['zone_id']);

            $operation_data['argAwbBooking']['Origin'] = $this->get_FFCitycode_code($seller_info['zone_id']);
            $operation_data['argAwbBooking']['ReadyDate'] = date('Y-m-d');
            $operation_data['argAwbBooking']['ReadyTime'] = '08:00';
            $operation_data['argAwbBooking']['ClsoingTime'] = '16:00';
            $operation_data['argAwbBooking']['SpecialInst'] = 'shipper opening times:' . $seller_info['opening_time'];
            //$operation_data['argAwbBooking']['AWBNo'] = $data['AWBNO'];
            $operation_data['argAwbBooking']['Shipper'] = $order_info['firstname'] . " " . $order_info['lastname'];
            $operation_data['argAwbBooking']['ShipperCPErson'] = $order_info['firstname'] . " " . $order_info['lastname'];
            $operation_data['argAwbBooking']['ShipperAddress1'] = $order_info['payment_address_1'];
            $operation_data['argAwbBooking']['ShipperAddress2'] = $order_info['payment_address_2'];
            $operation_data['argAwbBooking']['ShipperPhone'] = $order_info['telephone'];
            //$operation_data['argAwbBooking']['ShipperPin'] = '';
            $operation_data['argAwbBooking']['ShipperCity'] = $order_info['payment_zone'];
            $operation_data['argAwbBooking']['ShipperCountry'] = $order_info['payment_country'];


            $FirstFlight = new FirstFlight($this->registry, SHIPPING_FIRST_FLIGHT_ID);
            $booking_response = $FirstFlight->BookingRequest($operation_data);
        } else if ($bookingInfo['shipping_company'] == 2) {
            $Naqel = new Naqel($this->registry, SHIPPING_NAQEL_ID);
            $booking_response = $Naqel->returnShipment([
                    'waybill' => $bookingInfo['book_response'],
                ]
            );
        } else if ($bookingInfo['shipping_company'] == 3) {
            $Samsashipping = new Samsashipping($this->registry, SHIPPING_SMSA_ID);
            $booking_response = $Samsashipping->returnShipment($bookingInfo['book_response']);
        }

        if (isset($booking_response)) {
            $data["customer_id"] = $order_info['customer_id'];
            $data['book_response'] = $booking_response;
            $data['order_id'] = $order_id;
            $data['shipping_company'] = $bookingInfo['shipping_company'];
            $data['seller_id'] = $seller_info['customer_id'];
            $data['country_id'] = $seller_info['country_id'];
            $this->add_booking($data);
        }
    }

    public function getOrderBooking($order_id)
    {
        $q = $this->db->query("select * from `order_booking` where `order_id`='$order_id' ");
        return $q->row;
    }

    public function getOrder($order_id)
    {
        $order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'  AND deleted='0' AND order_status_id > '0'");


        if ($order_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

            if ($country_query->num_rows) {
                $payment_iso_code_2 = $country_query->row['iso_code_2'];
                $payment_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $payment_iso_code_2 = '';
                $payment_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $payment_zone_code = $zone_query->row['code'];
            } else {
                $payment_zone_code = '';
            }

            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

            if ($country_query->num_rows) {
                $shipping_iso_code_2 = $country_query->row['iso_code_2'];
                $shipping_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $shipping_iso_code_2 = '';
                $shipping_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $shipping_zone_code = $zone_query->row['code'];
            } else {
                $shipping_zone_code = '';
            }

            return array(
                'order_id' => $order_query->row['order_id'],
                'customer_id' => $order_query->row['customer_id'],
                'firstname' => $order_query->row['firstname'],
                'lastname' => $order_query->row['lastname'],
                'telephone' => $order_query->row['telephone'],
                'email' => $order_query->row['email'],
                'payment_address_1' => $order_query->row['payment_address_1'],
                'payment_address_2' => $order_query->row['payment_address_2'],
                'payment_city' => $order_query->row['payment_city'],
                'payment_zone_id' => $order_query->row['payment_zone_id'],
                'payment_zone' => $order_query->row['payment_zone'],
                'payment_zone_code' => $payment_zone_code,
                'payment_country_id' => $order_query->row['payment_country_id'],
                'payment_country' => $order_query->row['payment_country'],
                'shipping_address_1' => $order_query->row['shipping_address_1'],
                'shipping_address_2' => $order_query->row['shipping_address_2'],
                'shipping_zone_id' => $order_query->row['shipping_zone_id'],
                'shipping_zone' => $order_query->row['shipping_zone'],
                'shipping_zone_code' => $shipping_zone_code,
                'shipping_country_id' => $order_query->row['shipping_country_id'],
                'shipping_country' => $order_query->row['shipping_country'],
            );
        } else {
            return false;
        }
    }

    public function getReturn($return_id)
    {
        $return_query = $this->db->query("SELECT wro.*,wrp.quantity FROM `" . DB_PREFIX . "wk_rma_order` wro LEFT JOIN  `" . DB_PREFIX . "wk_rma_product` wrp ON (wro.id = wrp.rma_id) WHERE wro.id = '" . (int)$return_id . "'");

        if ($return_query->num_rows) {
            return array(
                'return_id' => $return_query->row['id'],
                'order_id' => $return_query->row['order_id'],
                'customer_id' => $return_query->row['customer_id'],
                'quantity' => $return_query->row['quantity']
            );
        } else {
            return false;
        }
    }

    public function check_shipment($order_id)
    {
        $query = $this->db->query("SELECT count(*) as count FROM order_shipping WHERE order_id='" . (int)$order_id . "'");

        return $query->row['count'];
    }

    public function get_FFCitycode_code($zone_id)
    {
        $query = $this->db->query("SELECT firstflight_destination_code FROM `zone` where zone_id='" . $zone_id . "' ");
        if ($query->num_rows) {
            return $query->row['firstflight_destination_code'];
        }
        return '';
    }

    public function add_shipment($data = array())
    {
        $sql = "INSERT INTO order_shipping SET ";

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $sql .= "$key = '$value' , ";
            }
            $sql .= "date_added = NOW() , date_modified= NOW()";

            $this->db->query($sql);
        }
    }

    public function getProfile($customer_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_customer c2c LEFT JOIN customerpartner_to_customer_description c2cd ON(c2c.customer_id=c2cd.customer_id) where c2c.customer_id = '" . $customer_id . "' and c2cd.language_id = 1 ");
        $data = $query->row;
        $country = $this->getCountryINFO($data['country_id']);
        $zone = $this->getZoneINFO($data['zone_id']);

        $data['zone_name'] = $zone['name'];
        $data['country_name'] = $country['name'];


        return $data;
    }

    public function getCountryINFO($country_id)
    {

        $query = $this->db->query("SELECT name,iso_code_3,telecode FROM country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE cd.language_id = 1 AND c.country_id = '" . (int)$country_id . "' ");
        return $query->row;
    }

    public function getZoneINFO($zone_id)
    {

        $query = $this->db->query("SELECT name FROM zone z LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE zd.language_id = 1 AND z.zone_id = '" . (int)$zone_id . "' ");
        return $query->row;
    }

    public function add_booking($data = array())
    {
        if (isset($data['book_response'])) {

            $sql = "INSERT INTO return_booking SET customer_id = '" . (int)$data['customer_id'] . "' , return_id = '" . (int)$data['return_id'] . "', order_id = '" . (int)$data['order_id'] . "', AWBNO='" . $this->db->escape($data['book_response']) . "', country_id='" . (int)$data['country_id'] . "', seller_id='" . (int)$data['seller_id'] . "', shipping_company='" . (int)$data['shipping_company'] . "' ,date_added = NOW() , date_modified= NOW()";

            $this->db->query($sql);
        }
    }

    public function getShippingCompany($country_id)
    {
        $query = $this->db->query("SELECT ship_company_id FROM ship_company_to_country WHERE country_id='" . (int)$country_id . "'");
        return $query->row['ship_company_id'];
    }

}

$returner = new generateReturns();

$returns = $returner->db->query(" SELECT id FROM `wk_rma_order` WHERE id NOT IN (SELECT return_id FROM return_booking) AND is_confirmed = 1 ");
foreach ($returns->rows as $row) {

    $returner->rma($row['id']);
}
