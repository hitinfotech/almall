<?php

require_once('Cron.php');

class ImagesToBeasnTalkd extends Cron {

    function __construct() {

        exec("ps aux | grep -i ImagesToBeasnTalkd |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }

        parent::__construct();

        //$this->loader->model('ne/chunk');
        //$this->chunk = $this->registry->get('model_ne_chunk');
    }

    public function run() {
        $beansTalkd = new Beanstalk(array('server1' => array(
                'host' => BEANSTALKD_SERVER,
                'port' => BEANSTALKD_SERVER_PORT,
                'weight' => 50,
                'timeout' => 10,
                // array of connections/tubes
                'connections' => array(),
        )));
        $client = $beansTalkd->getClient();
        $client->useTube('IMAGES');

        $closeTime = time() + 60 * 60 * 1;
        while (time() < $closeTime) {
            $images = $this->db->query(" SELECT * FROM `image_s3` WHERE `status` = 'NEW'  LIMIT 200 ");
            if ($images && $images->num_rows > 0) {
                foreach ($images->rows as $row) {
                    $id = $row['id'];
                    $this->db->query("UPDATE `image_s3` set `status`='INQUEUE' where id='$id' ");
                    $client->put(0, 0, 0, $id);
                    $this->myEcho("add image " . $id . " to queue");
                }
            } else {
                $this->myEcho('no images sleep 2 seconds');
                sleep(2);
            }
        }
        $this->myEcho('killed this old process to start a new one');
        $client->disconnect();
    }

}

$image = new ImagesToBeasnTalkd();
$image->run();
