<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';
require_once realpath(__DIR__) . '/firstflight.php';

class trackRMaShipment {

    public $db;
    private $FFServices;

    public function __construct() {
        $this->db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
        $this->FFServices = FirstFlight::getInstance();
    }

    public function track($country_id,$return_id) {

        $awbno= $this->getAWBNO($country_id,$return_id);

        $booking_response = $this->FFServices->LogData($awbno, $country_id);
        var_dump($booking_response);

    }

    public function getAWBNO($country_id,$return_id){
        $query = $this->db->query("SELECT AWBNO FROM order_shipping WHERE country_id='".(int) $country_id."' AND return_id='".(int)$return_id."'");
        return $query->row['AWBNO'];
    }


}

$tracker = new trackRMaShipment();

$orders = $tracker->db->query("SELECT country_id,return_id FROM `return_shipping` WHERE is_closed=0 ");

foreach ($orders->rows as $row) {

    $tracker->track($row['country_id'],$row['return_id']);

}
