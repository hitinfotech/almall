<?php
require_once('Cron.php');

class UploadProductImagesToCloudinarys extends Cron
{

    public $config_image;
    function __construct()
    {

        parent::__construct();
        $this->config_image = config_image::getInstance();

        $this->cloudinary = new Cloudinaryclass();
        $this->registry->set('cloudinary',$this->cloudinary );
    }

    public function putImages(){

      $images = $this->db->query(" SELECT image FROM `product` WHERE image <> '' ");
      if ($images && $images->num_rows > 0) {
          foreach ($images->rows as $row) {
            if(file_exists(DIR_IMAGE.$row['image'])){
              $extension = pathinfo($row['image'], PATHINFO_EXTENSION);
              $new_image_path = 'cache/' . utf8_substr($row['image'], 0, utf8_strrpos($row['image'], '.')) . '-' .  $this->config_image->get('product', 'listing', 'width') . 'x' . $this->config_image->get('product', 'listing', 'hieght')  . '.' . $extension;

              $this->db->query("INSERT INTO `image_s3`  set image='".$this->db->escape($row['image'])."', new_image_path='".$this->db->escape($new_image_path)."', date_added=NOW(),  uploaded=0, width=".(int)$this->config_image->get('product', 'listing', 'width')." , height=".$this->config_image->get('product', 'listing', 'hieght') . ",upload_image=1, status='NEW'");
              echo $row['image']."---- was uploaded"."\n";
            }
          }
      }
    }
}

$image = new UploadProductImagesToCloudinarys();
$image->putImages();
