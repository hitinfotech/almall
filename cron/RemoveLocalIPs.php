<?php
require_once realpath(__DIR__) . '/Cron.php';
class RemoveLocalIPs extends Cron
{
    public $aTables = ['order'];

    public function handleLocalIPs()
    {
        $x = 0;
        while (1) {
            $limit = 10000;
            $offset = $x*10000;
            $aIPs = $this->db->query("SELECT customer_id,ip FROM customer WHERE ip LIKE '%172.31%' OR  ip LIKE '%127.0.0%' LIMIT $limit OFFSET $offset;");
            if (!$aIPs->num_rows)
                break;
            foreach ($aIPs->rows as $row) {
                $sOldIP = $row['ip'];
                $customer_id = $row['customer_id'];
                echo "Customer ID: ".$customer_id." --- Old IP: ".$sOldIP."\n";
                $aValue = explode(',',$sOldIP);

                $aNewIPs = $this->handleMultipleIPs($aValue, $customer_id,0);

                if ($aNewIPs) {
                    echo "Customer ID: " . $customer_id . " --- New IP: " . $aNewIPs . "\n";
                    $this->db->query("UPDATE customer SET ip = '$aNewIPs' WHERE customer_id=$customer_id;");
                } else {
                    echo "Customer ID: " . $customer_id . " --- New IP: NULL \n";
                    $this->db->query("UPDATE customer SET ip = 'NULL' WHERE customer_id=$customer_id;");
                }
                echo "\n----------------------------------------------\n";
            }
            $x++;
            sleep(1);
        }
    }

    private function handleMultipleIPs($aIPs,$customer_id,$table_index)
    {
        $aNewIPs = [];
        foreach ($aIPs as $ip) {
            if (strpos($ip,'172.31')===false && strpos($ip,'127.0.0')===false) {
                $aNewIPs[] = $ip;
            }
        }
        $aNewIPs = implode(',',$aNewIPs);
        $table = array_key_exists($table_index,$this->aTables)?$this->aTables[$table_index]:false;
        if (trim($aNewIPs) == '' && $table) {
                $aOrderIPs = $this->db->query("SELECT ip 
                                                  FROM `$table` 
                                                  WHERE customer_id=$customer_id 
                                                  AND (ip NOT LIKE '%127.0.0%' AND ip NOT LIKE '%172.31%')
                                                  LIMIT 1;");
                if ($aOrderIPs->num_rows) {
                    $this->handleMultipleIPs(explode(',',$aOrderIPs->row['ip']), $customer_id, $table_index);
                } else {
                    $table_index++;
                    $this->handleMultipleIPs([], $customer_id, $table_index);
                }
        }
        return $aNewIPs;
    }
}
$obj = new RemoveLocalIPs();
$obj->handleLocalIPs();



