<?php

echo $starttime = time() . "\n\n";

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

$errors = array();

$db = new DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

define('FEED_DIR', '/var/www/sayidaty_net/fffeeds/');

$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(FEED_DIR), RecursiveIteratorIterator::SELF_FIRST);

foreach ($objects as $name => $object) {
    if (substr(trim($name), strlen($name) - 3) != 'csv') {
        continue;
    }

    $file1 = fopen($name, "r");
    while (!feof($file1)) {
        $file = fgetcsv($file1);

        $order_id = (int) $file[0];
        $country_id = $file[1];
        $AWBN = $file[2];
        if ($order_id) {
            echo "\n{$order_id}, {$AWBN} , {$country_id}";
            $db->query("INSERT INTO order_shipping SET order_id='" . (int) $order_id . "', country_id='".(int) $country_id ."',AWBNO='" . $db->escape($AWBN) . "' ");
        }
    }
    exec("mv {$name} {$name}.processed_" . date('Y_m_d'));
    echo "\n\n\nDONE";
}
