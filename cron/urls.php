<?php

echo $starttime = time() . "\n\n";

require_once realpath(__DIR__) . '/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

$errors = array();

$db = new DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

$countries = array(
    "AF" => '0093', //array("AFGHANISTAN", "AF", "AFG", "004"),
    "AL" => '00355', //array("ALBANIA", "AL", "ALB", "008"),
    "DZ" => '00213', //array("ALGERIA", "DZ", "DZA", "012"),
    "AS" => '00376', //array("AMERICAN SAMOA", "AS", "ASM", "016"),
    "AD" => '00376', //array("ANDORRA", "AD", "AND", "020"),
    "AO" => '00244', //array("ANGOLA", "AO", "AGO", "024"),
    "AG" => '001-268', //array("ANTIGUA AND BARBUDA", "AG", "ATG", "028"),
    "AR" => '0054', //array("ARGENTINA", "AR", "ARG", "032"),
    "AM" => '00374', //array("ARMENIA", "AM", "ARM", "051"),
    "AU" => '0061', //array("AUSTRALIA", "AU", "AUS", "036"),
    "AT" => '0043', //array("AUSTRIA", "AT", "AUT", "040"),
    "AZ" => '00994', //array("AZERBAIJAN", "AZ", "AZE", "031"),
    "BS" => '001-242', //array("BAHAMAS", "BS", "BHS", "044"),
    "BH" => '00973', //array("BAHRAIN", "BH", "BHR", "048"),
    "BD" => '00880', //array("BANGLADESH", "BD", "BGD", "050"),
    "BB" => '1-246', //array("BARBADOS", "BB", "BRB", "052"),
    "BY" => '00375', //array("BELARUS", "BY", "BLR", "112"),
    "BE" => '0032', //array("BELGIUM", "BE", "BEL", "056"),
    "BZ" => '00501', //array("BELIZE", "BZ", "BLZ", "084"),
    "BJ" => '00229', // array("BENIN", "BJ", "BEN", "204"),
    "BT" => '00975', //array("BHUTAN", "BT", "BTN", "064"),
    "BO" => '00591', //array("BOLIVIA", "BO", "BOL", "068"),
    "BA" => '00387', //array("BOSNIA AND HERZEGOVINA", "BA", "BIH", "070"),
    "BW" => '00267', //array("BOTSWANA", "BW", "BWA", "072"),
    "BR" => '0055', //array("BRAZIL", "BR", "BRA", "076"),
    "BN" => '00673', //array("BRUNEI DARUSSALAM", "BN", "BRN", "096"),
    "BG" => '00359', //array("BULGARIA", "BG", "BGR", "100"),
    "BF" => '00226', //array("BURKINA FASO", "BF", "BFA", "854"),
    "BI" => '00257', //array("BURUNDI", "BI", "BDI", "108"),
    "KH" => '00855', //array("CAMBODIA", "KH", "KHM", "116"),
    "CA" => '001', //array("CANADA", "CA", "CAN", "124"),
    "CV" => '00238', //array("CAPE VERDE", "CV", "CPV", "132"),
    "CF" => '00236', //array("CENTRAL AFRICAN REPUBLIC", "CF", "CAF", "140"),
    "CM" => '00237', //array("CENTRAL AFRICAN REPUBLIC", "CF", "CAF", "140"),
    "TD" => '00235', //array("CHAD", "TD", "TCD", "148"),
    "CL" => '0056', //array("CHILE", "CL", "CHL", "152"),
    "CN" => '0086', //array("CHINA", "CN", "CHN", "156"),
    "CO" => '0057', //array("COLOMBIA", "CO", "COL", "170"),
    "KM" => '00269', //array("COMOROS", "KM", "COM", "174"),
    "CG" => '00242', //array("CONGO", "CG", "COG", "178"),
    "CR" => '00506', //array("COSTA RICA", "CR", "CRI", "188"),
    "CI" => '00225', //array("COTE D'IVOIRE", "CI", "CIV", "384"),
    "HR" => '00385', //array("CROATIA (local name: Hrvatska)", "HR", "HRV", "191"),
    "CU" => '0053', //array("CUBA", "CU", "CUB", "192"),
    "CY" => '00357', //array("CYPRUS", "CY", "CYP", "196"),
    "CZ" => '00420', //array("CZECH REPUBLIC", "CZ", "CZE", "203"),
    "DK" => '0045', //array("DENMARK", "DK", "DNK", "208"),
    "DJ" => '00253', //array("DJIBOUTI", "DJ", "DJI", "262"),
    "DM" => '001-767', //array("DOMINICA", "DM", "DMA", "212"),
    "DO" => '001-809', //array("DOMINICAN REPUBLIC", "DO", "DOM", "214"),
    "EC" => '00593', //array("ECUADOR", "EC", "ECU", "218"),
    "EG" => '0020', //array("EGYPT", "EG", "EGY", "818"),
    "SV" => '00503', //array("EL SALVADOR", "SV", "SLV", "222"),
    "GQ" => '00240', //array("EQUATORIAL GUINEA", "GQ", "GNQ", "226"),
    "ER" => '00291', //array("ERITREA", "ER", "ERI", "232"),
    "EE" => '00372', //array("ESTONIA", "EE", "EST", "233"),
    "ET" => '00251', //array("ETHIOPIA", "ET", "ETH", "210"),
    "FJ" => '00679', //array("FIJI", "FJ", "FJI", "242"),
    "FI" => '00358', //array("FINLAND", "FI", "FIN", "246"),
    "FR" => '0033', //array("FRANCE", "FR", "FRA", "250"),
    "GA" => '00241', //array("GABON", "GA", "GAB", "266"),
    "GM" => '00220', //array("GAMBIA", "GM", "GMB", "270"),
    "GE" => '00995', //array("GEORGIA", "GE", "GEO", "268"),
    "DE" => '0049', //array("GERMANY", "DE", "DEU", "276"),
    "GH" => '00233', //array("GHANA", "GH", "GHA", "288"),
    "GR" => '0030', //array("GREECE", "GR", "GRC", "300"),
    "GD" => '001-473', //array("GRENADA", "GD", "GRD", "308"),
    "GT" => '00502', //array("GUATEMALA", "GT", "GTM", "320"),
    "GN" => '00224', //array("GUINEA", "GN", "GIN", "324"),
    "GW" => '00245', //array("GUINEA-BISSAU", "GW", "GNB", "624"),
    "GY" => '00592', //array("GUYANA", "GY", "GUY", "328"),
    "HT" => '00509', //array("HAITI", "HT", "HTI", "332"),
    "HN" => '00504', //array("HONDURAS", "HN", "HND", "340"),
    "HK" => '00852', //array("HONG KONG", "HK", "HKG", "344"),
    "HU" => '0036', //array("HUNGARY", "HU", "HUN", "348"),
    "IS" => '00354', //array("ICELAND", "IS", "ISL", "352"),
    "IN" => '0091', //array("INDIA", "IN", "IND", "356"),
    "ID" => '0062', //array("INDONESIA", "ID", "IDN", "360"),
    "IR" => '0098', //array("IRAN, ISLAMIC REPUBLIC OF", "IR", "IRN", "364"),
    "IQ" => '00964', //array("IRAQ", "IQ", "IRQ", "368"),
    "IE" => '00353', //array("IRELAND", "IE", "IRL", "372"),
    "IL" => '00972', //array("ISRAEL", "IL", "ISR", "376"),
    "IT" => '0039', //array("ITALY", "IT", "ITA", "380"),
    "JM" => '001-876', //array("JAMAICA", "JM", "JAM", "388"),
    "JP" => '0081', //array("JAPAN", "JP", "JPN", "392"),
    "JO" => '00962', //array("JORDAN", "JO", "JOR", "400"),
    "KZ" => '007', //array("KAZAKHSTAN", "KZ", "KAZ", "398"),
    "KE" => '00254', //array("KENYA", "KE", "KEN", "404"),
    "KI" => '00686', //array("KIRIBATI", "KI", "KIR", "296"),
    "KP" => '00850', //array("KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF", "KP", "PRK", "408"),
    "KR" => '0082', //array("KOREA, REPUBLIC OF", "KR", "KOR", "410"),
    "KW" => '00965', //array("KUWAIT", "KW", "KWT", "414"),
    "KG" => '00996', //array("KYRGYZSTAN", "KG", "KGZ", "417"),
    "LA" => '00856', //array("LAO PEOPLE'S DEMOCRATIC REPUBLIC", "LA", "LAO", "418"),
    "LV" => '00371', //array("LATVIA", "LV", "LVA", "428"),
    "LB" => '00961', //array("LEBANON", "LB", "LBN", "422"),
    "LS" => '00266', //array("LESOTHO", "LS", "LSO", "426"),
    "LR" => '00231', //array("LIBERIA", "LR", "LBR", "430"),
    "LY" => '00218', //array("LIBYAN ARAB JAMAHIRIYA", "LY", "LBY", "434"),
    "LI" => '00423', //array("LIECHTENSTEIN", "LI", "LIE", "438"),
    "LU" => '00352', //array("LUXEMBOURG", "LU", "LUX", "442"),
    "MO" => '00389', //array("MACAU", "MO", "MAC", "446"),
    "MG" => '00261', //array("MADAGASCAR", "MG", "MDG", "450"),
    "MW" => '00265', //array("MALAWI", "MW", "MWI", "454"),
    "MY" => '0060', //array("MALAYSIA", "MY", "MYS", "458"),
    "MX" => '0052', //array("MEXICO", "MX", "MEX", "484"),
    "MC" => '00377', //array("MONACO", "MC", "MCO", "492"),
    "MA" => '00212', //array("MOROCCO", "MA", "MAR", "504"),
    "NP" => '00977', //array("NEPAL", "NP", "NPL", "524"),
    "NL" => '0031', //array("NETHERLANDS", "NL", "NLD", "528"),
    "NZ" => '0064', //array("NEW ZEALAND", "NZ", "NZL", "554"),
    "NI" => '00505', //array("NICARAGUA", "NI", "NIC", "558"),
    "NE" => '00227', //array("NIGER", "NE", "NER", "562"),
    "NG" => '00234', //array("NIGERIA", "NG", "NGA", "566"),
    "NO" => '0047', //array("NORWAY", "NO", "NOR", "578"),
    "OM" => '00968', //array("OMAN", "OM", "OMN", "512"),
    "PK" => '0092', //array("PAKISTAN", "PK", "PAK", "586"),
    "PA" => '00507', //array("PANAMA", "PA", "PAN", "591"),
    "PG" => '00675', //array("PAPUA NEW GUINEA", "PG", "PNG", "598"),
    "PY" => '00595', // array("PARAGUAY", "PY", "PRY", "600"),
    "PE" => '0051', // array("PERU", "PE", "PER", "604"),
    "PH" => '0063', // array("PHILIPPINES", "PH", "PHL", "608"),
    "PL" => '0048', //array("POLAND", "PL", "POL", "616"),
    "PT" => '00351', //array("PORTUGAL", "PT", "PRT", "620"),
    "QA" => '00974', //array("QATAR", "QA", "QAT", "634"),
    "RU" => '007', //array("RUSSIAN FEDERATION", "RU", "RUS", "643"),
    "RW" => '00250', //array("RWANDA", "RW", "RWA", "646"),
    "SA" => '00966', //array("SAUDI ARABIA", "SA", "SAU", "682"),
    "SN" => '00221', //array("SENEGAL", "SN", "SEN", "686"),
    "SG" => '0065', //array("SINGAPORE", "SG", "SGP", "702"),
    "SK" => '00421', //array("SLOVAKIA (Slovak Republic)", "SK", "SVK", "703"),
    "SI" => '00386', //array("SLOVENIA", "SI", "SVN", "705"),
    "ZA" => '0027', //array("SOUTH AFRICA", "ZA", "ZAF", "710"),
    "ES" => '0034', //array("SPAIN", "ES", "ESP", "724"),
    "LK" => '0094', //array("SRI LANKA", "LK", "LKA", "144"),
    "SD" => '00249', //array("SUDAN", "SD", "SDN", "736"),
    "SZ" => '00268', //array("SWAZILAND", "SZ", "SWZ", "748"),
    "SE" => '0046', //array("SWEDEN", "SE", "SWE", "752"),
    "CH" => '0041', //array("SWITZERLAND", "CH", "CHE", "756"),
    "SY" => '00963', //array("SYRIAN ARAB REPUBLIC", "SY", "SYR", "760"),
    "TZ" => '00255', //array("TANZANIA, UNITED REPUBLIC OF", "TZ", "TZA", "834"),
    "TH" => '0066', //array("THAILAND", "TH", "THA", "764"),
    "TG" => '00228', //array("TOGO", "TG", "TGO", "768"),
    "TO" => '00676', //array("TONGA", "TO", "TON", "776"),
    "TN" => '00216', //array("TUNISIA", "TN", "TUN", "788"),
    "TR" => '0090', //array("TURKEY", "TR", "TUR", "792"),
    "TM" => '00993', //array("TURKMENISTAN", "TM", "TKM", "795"),
    "UA" => '00380', //array("UKRAINE", "UA", "UKR", "804"),
    "AE" => '00971', //array("UNITED ARAB EMIRATES", "AE", "ARE", "784"),
    "GB" => '0044', //array("UNITED KINGDOM", "GB", "GBR", "826"),
    "US" => '001'//array("UNITED STATES", "US", "USA", "840"),
);

$count = 0;

foreach ($countries as $key => $row) {
    $count++;
    echo "{$count} country code added \n";
    $db->query("UPDATE country SET telecode='" . $db->escape($row) . "' WHERE iso_code_2='" . $db->escape($key) . "' ");
}


$lines = array();
$lines[] = array('table' => 'url_alias', 'query' => 'common/home', 'keyword' => '');
$lines[] = array('table' => 'url_alias', 'query' => 'common/howtosell', 'keyword' => 'howtosell');
$lines[] = array('table' => 'url_alias', 'query' => 'ticketsystem/generatetickets', 'keyword' => 'contact_us');

$lines[] = array('table' => 'url_alias', 'query' => 'checkout/success', 'keyword' => 'checkout-success');
$lines[] = array('table' => 'url_alias', 'query' => 'total/coupon/coupon', 'keyword' => 'checkout-coupon');

$lines[] = array('table' => 'url_alias', 'query' => 'account/account', 'keyword' => 'account');
$lines[] = array('table' => 'url_alias', 'query' => 'module/productbundles/view', 'keyword' => 'bundleview');
$lines[] = array('table' => 'url_alias', 'query' => 'account/login', 'keyword' => 'sign/in'); //DONE
$lines[] = array('table' => 'url_alias', 'query' => 'account/register', 'keyword' => 'sign/up'); //DONE
$lines[] = array('table' => 'url_alias', 'query' => 'account/forgotten', 'keyword' => 'sign/forgot'); //DONE
$lines[] = array('table' => 'url_alias', 'query' => 'account/logout', 'keyword' => 'sign/out');
$lines[] = array('table' => 'url_alias', 'query' => 'account/rma/rma', 'keyword' => 'profile-rma');
$lines[] = array('table' => 'url_alias', 'query' => 'account/notifications', 'keyword' => 'profile-notifications');
$lines[] = array('table' => 'url_alias', 'query' => 'account/address', 'keyword' => 'profile-addresses');
$lines[] = array('table' => 'url_alias', 'query' => 'account/address/add', 'keyword' => 'profile-add-addresses');
$lines[] = array('table' => 'url_alias', 'query' => 'account/address/edit', 'keyword' => 'profile-edit-addresses');
$lines[] = array('table' => 'url_alias', 'query' => 'account/language', 'keyword' => 'profile-languages');
$lines[] = array('table' => 'url_alias', 'query' => 'account/order', 'keyword' => 'profile-orders');
$lines[] = array('table' => 'url_alias', 'query' => 'account/newsletter', 'keyword' => 'profile-newsletter');
$lines[] = array('table' => 'url_alias', 'query' => 'account/success', 'keyword' => 'profile-success');
$lines[] = array('table' => 'url_alias', 'query' => 'account/tracking', 'keyword' => 'order-tracking');

// Inside User Profile
$lines[] = array('table' => 'url_alias', 'query' => 'account/wishlist', 'keyword' => 'favorite_products'); //DONE
$lines[] = array('table' => 'url_alias', 'query' => 'account/password', 'keyword' => 'password'); //DONE
//sitemap
$lines[] = array('table' => 'url_alias', 'query' => 'feed/google_sitemap', 'keyword' => 'sitemap.xml');

$lines[] = array('table' => 'url_alias', 'query' => 'module/productbundles/listing', 'keyword' => 'BUNDLES');

$lines[] = array('table' => 'url_alias', 'query' => 'search/shopable', 'keyword' => 'category');
$lines[] = array('table' => 'url_alias', 'query' => 'search/category', 'keyword' => 'category'); //DONE
$lines[] = array('table' => 'url_alias', 'query' => 'search/search', 'keyword' => 'search'); //DONE
$lines[] = array('table' => 'url_alias', 'query' => 'search/offer', 'keyword' => 'sale'); // offers page
$lines[] = array('table' => 'url_alias_ar', 'query' => 'search/newin', 'keyword' => 'وصل-حديثاً'); // offers page
$lines[] = array('table' => 'url_alias_en', 'query' => 'search/newin', 'keyword' => 'NEW-IN'); // offers page

$lines[] = array('table' => 'url_alias_ar', 'query' => 'search/product', 'keyword' => 'products/المنتجات/تسوق-أون-لاين');
$lines[] = array('table' => 'url_alias_ar', 'query' => 'search/product', 'keyword' => 'products/s/المنتجات');
$lines[] = array('table' => 'url_alias_en', 'query' => 'search/product', 'keyword' => 'products');



$lines[] = array('table' => 'url_alias_ar', 'query' => 'search/mother_day', 'keyword' => 'عيد-الأم'); // offers page
$lines[] = array('table' => 'url_alias_en', 'query' => 'search/mother_day', 'keyword' => 'Mothers-Day'); // offers page

$lines[] = array('table' => 'url_alias_ar', 'query' => 'malls/malls', 'keyword' => 'المولات/malls/s'); //DONE
$lines[] = array('table' => 'url_alias_en', 'query' => 'malls/malls', 'keyword' => 'malls'); //DONE

$lines[] = array('table' => 'url_alias_ar', 'query' => 'malls/shops', 'keyword' => 'shops/s'); //DONE
$lines[] = array('table' => 'url_alias_en', 'query' => 'malls/shops', 'keyword' => 'shops'); //DONE

$lines[] = array('table' => 'url_alias_ar', 'query' => 'brands/brands', 'keyword' => 'الماركات/brands/s'); //DONE
$lines[] = array('table' => 'url_alias_en', 'query' => 'brands/brands', 'keyword' => 'brands'); //DONE

$lines[] = array('table' => 'url_alias_ar', 'query' => 'product/offer', 'keyword' => 'offers-promotions/s'); //DONE
$lines[] = array('table' => 'url_alias_en', 'query' => 'product/offer', 'keyword' => 'offers-promotions/'); //DONE

$lines[] = array('table' => 'url_alias_ar', 'query' => 'malls/news', 'keyword' => 'الأخبار/news'); //DONE
$lines[] = array('table' => 'url_alias_en', 'query' => 'malls/news', 'keyword' => 'news'); //DONE

$lines[] = array('table' => 'url_alias_ar', 'query' => 'look/looks', 'keyword' => 'prod_looks'); //DONE
$lines[] = array('table' => 'url_alias_en', 'query' => 'look/looks', 'keyword' => 'looks'); //DONE

$lines[] = array('table' => 'url_alias_ar', 'query' => 'tips/tips', 'keyword' => 'نصائح التسوق'); //DONE
$lines[] = array('table' => 'url_alias_en', 'query' => 'tips/tips', 'keyword' => 'shopping-tips'); //DONE

$lines[] = array('table' => 'url_alias', 'query' => 'checkout/cart', 'keyword' => 'cart');
$lines[] = array('table' => 'url_alias', 'query' => 'checkout/checkout', 'keyword' => 'checkout');

//Partner Profile
$lines[] = array('table' => 'url_partner', 'query' => 'account/customerpartner/addproduct', 'keyword' => 'seller/addproduct');
$lines[] = array('table' => 'url_partner', 'query' => 'account/customerpartner/dashboard', 'keyword' => 'seller/dashboard');
$lines[] = array('table' => 'url_partner', 'query' => 'account/customerpartner/orderlist', 'keyword' => 'seller/orderlist');
$lines[] = array('table' => 'url_partner', 'query' => 'account/customerpartner/transaction', 'keyword' => 'seller/transaction');
$lines[] = array('table' => 'url_partner', 'query' => 'account/customerpartner/productlist', 'keyword' => 'seller/productlist');
$lines[] = array('table' => 'url_partner', 'query' => 'account/customerpartner/wk_mprma_manage/getForm', 'keyword' => 'seller/returns/getForm');
$lines[] = array('table' => 'url_partner', 'query' => 'account/customerpartner/wk_mprma_manage', 'keyword' => 'seller/returns');
$lines[] = array('table' => 'url_partner', 'query' => 'account/customerpartner/orderinfo', 'keyword' => 'seller/orderinfo');

foreach ($lines as $row) {
    $count++;
    echo "{$count} route added \n";
    if ($row['table'] == 'url_alias') {
        $db->query("DELETE FROM `" . $row['table'] . "_ar` WHERE keyword='" . $db->escape($row['keyword']) . "' ");
        $db->query("INSERT INTO `" . $row['table'] . "_ar` SET query='" . $db->escape($row['query']) . "', keyword='" . $db->escape($row['keyword']) . "' ");
        $db->query("DELETE FROM `" . $row['table'] . "_en` WHERE keyword='" . $db->escape($row['keyword']) . "' ");
        $db->query("INSERT INTO `" . $row['table'] . "_en` SET query='" . $db->escape($row['query']) . "', keyword='" . $db->escape($row['keyword']) . "' ");
    } else {
        $db->query("DELETE FROM `" . $row['table'] . "` WHERE keyword='" . $db->escape($row['keyword']) . "' ");
        $db->query("INSERT INTO `" . $row['table'] . "` SET query='" . $db->escape($row['query']) . "', keyword='" . $db->escape($row['keyword']) . "' ");
    }
}

$count++;

echo "{$count} route added \n";
