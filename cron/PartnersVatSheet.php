<?php
error_reporting(E_ALL);

require_once realpath(__DIR__) . '/Cron.php';
require_once realpath(__DIR__) .'/../system/phpexcel/Classes/PHPExcel.php';
ini_set("precision", "20");
class PartnersVatSheet extends Cron
{
    public function parseFile()
    {
        $file_path = __DIR__.'/files/sellers_information_VAT.xlsx';
        $fileExists = file_exists($file_path);
        if ($fileExists) {
            $objPHPExcel = new PHPExcel();
            $objReader = PHPExcel_IOFactory::createReaderForFile($file_path);
            $objPHPExcel = PHPExcel_IOFactory::load($file_path);
            $objPHPExcel->setActiveSheetIndex(0);
            $partner_id=$trn= '';
            foreach ($objPHPExcel->getActiveSheet()->getRowIterator(2) as $row) {
                foreach ($row->getCellIterator() as $index => $cell) {
                    switch ($index) {
                        case 0:
                            $partner_id = $cell->getValue();
                            break;
                        case 4:
                            $trn = $cell->getValue();
                            break;
                    }

                }

                if (trim($partner_id) != '') {
                    if (trim($trn) != '' ) {
                        $trn = (string)$trn;
                        echo "\nUpdate Partner: $partner_id With TRN: $trn\n";
                        $this->db->query("UPDATE eligible_vat SET tax_registration_number = '$trn',checked=1 WHERE customer_id = '" . (int)$partner_id . "' ");
                    } else {
                        echo "\nThis Partner: $partner_id Has NO TRN\n";
                        $this->db->query("UPDATE eligible_vat SET checked=0 WHERE customer_id = '" . (int)$partner_id . "' ");
                    }
                }
            }
        }
    }
}
$obj = new PartnersVatSheet();

$obj->parseFile();