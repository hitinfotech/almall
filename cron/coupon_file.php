<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

$db = new DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

// import brands
$coupon = realpath(__DIR__) . "/files/coupon.csv";

$file6 = fopen($coupon, "r");
while (!feof($file6)) {
    $file = fgetcsv($file6);
    //print_r($file);

    $name = $file[0];
    $code = $file[1];
    $type = $file[2];
    $discount = $file[3];
    $logged = $file[4];
    $shipping = $file[5];
    $total = $file[6];
    $date_start = $file[7];
    $date_end = $file[8];
    $uses_total = $file[9];
    $uses_customer = $file[10];
    $status = $file[11];
    $date_added = $file[12];


    $db->query("INSERT INTO coupon SET name = '" . $db->escape($name) . "',code = '" . $db->escape($code) . "',type = '" . $db->escape($type) . "',"
            . "discount = '" . $db->escape($discount) . "',"
            . "logged = '" . $db->escape($logged) . "',"
            . "shipping = '" . $db->escape($shipping) . "',"
            . "total = '" . $db->escape($total) . "',"
            . "date_start = '" . $db->escape($date_start) . "',"
            . "date_end = '" . $db->escape($date_end) . "',"
            . "uses_total = '" . $db->escape($uses_total) . "',"
            . "uses_customer = '" . $db->escape($uses_customer) . "',"
            . "status = '" . $db->escape($status) . "',"
            . "date_added = '" . $db->escape($date_added) . "'");

}

echo "\nfinished\n";
