<?php
require_once 'Cron.php';

/**
 * Created by PhpStorm.
 * User: bahaa
 * Date: 5/18/17
 * Time: 12:02 AM
 * @property Currency $currency
 * @property ModelToolImage $model_tool_image;
 */
class XlsQueueToProduct extends Cron
{
    private $config_image;
    private $url;
    public function __construct()
    {
        exec("ps aux | grep -i XlsQueueToProduct |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();
        $request = new Request();
        $this->registry->set('request', $request);
        $session = Session::getInstance();
        $this->registry->set('session', $session);
        $cache = Cache::getInstance(CONFIG_CACHE);
        $this->registry->set('cache', $cache);
        $this->registry->set('currency', new Currency($this->registry));
        $this->loader->model('tool/image');
        $this->config_image = config_image::getInstance();

        $this->url = new Url_custom($this->registry);
    }

    public function run()
    {

        /**
         **@var Product $product
         **/
        $client = $this->getBeansTalkd();
        $client->watch("products");
        $closeTime = time() + 60 * 60 * 1;
        while (time() < $closeTime) {


            $job = $client->reserve(1);
            if ($job) {
                $client->delete($job['id']);
                $id = (int)$job['body'];
                $data = $this->db->query("SELECT * FROM  `xls_products`  WHERE ID = '$id' ");
                if ($data && $data->row) {
                    $product = unserialize($data->row['data']);
                    $product->updateRegistry($this->registry);
                    $product->updateStatus($data->row['id'], 'PROCESSING');
                    $product_id = $this->addProduct($product);
                    if ($product_id && $product_id > 0) {

                        $product->updateStatus($data->row['id'],'COMPLETED','Add Done',$product_id);
                        $this->myEcho("Add Done Product Id : ".$product_id);
                    } else {
                        $product->updateStatus($data->row['id'], 'FAILED', "Cant Save Product");
                        $this->myEcho("Cant Save Product:" . $data->row['id']);

                    }

                }
            }


        }


    }

    private function getBeansTalkd()
    {
        $beansTalkd = new Beanstalk(array('server1' => array(
            'host' => BEANSTALKD_SERVER,
            'port' => BEANSTALKD_SERVER_PORT,
            'weight' => 50,
            'timeout' => 10,
            // array of connections/tubes
            'connections' => array(),
        )));
        $client = $beansTalkd->getClient();
        return $client;
    }

    public function saveImages($images,$product_id)
    {

      //removing the existed images for that product_to_store
      $product_main_image = $this->db->query("SELECT image from product WHERE product_id=".(int)$product_id);
      if($product_main_image->num_rows){
        foreach($product_main_image->rows as $key => $value){
          if(is_file(DIR_IMAGE.$value['image'])){
            unlink(DIR_IMAGE.$value['image']);
          }
        }
      }

      $product_original_images = $this->db->query("SELECT image from product_image WHERE product_id=".(int)$product_id);
      if($product_original_images->num_rows){
        foreach($product_original_images->rows as $key => $value){
          if(is_file(DIR_IMAGE.$value['image'])){
            unlink(DIR_IMAGE.$value['image']);
          }
        }
        $this->db->query("DELETE from product_image WHERE product_id=".(int)$product_id);
      }


        try{
        $newImages = [];
        foreach ($images as $img) {
            $imgContent = $this->getImage($img);//file_get_contents($img);
            $im = new imagick();
            $im->readImageBlob($imgContent);
            $imageprops = $im->getImageGeometry();
            $old_width = $imageprops['width'];
            $old_height = $imageprops['height'];
            $height = $width = 2000;
            if ($old_width > $old_height) {
                $newHeight = $height;
                $newWidth = ($height / $old_height) * $old_width;
            } else {
                $newWidth = $width;
                $newHeight = ($width / $old_width) * $old_height;
            }

            $im->resizeimage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1, true);
            $im->cropImage($newWidth, $newHeight, 0, 0);
            $path = $this->createPath($product_id);
            $filename = time().Rand(111111,999999).".jpg";
            $im->writeImage(DIR_IMAGE . $path . $filename);
            $newImages[] = $path . $filename;
        }
      }catch (exception $e){
        echo "Error in uploading Image for $product_id: " .$e->getMessage() ."\n" ;
      }
        return $newImages;
    }

    public function createPath($id)
    {

        $path = ucfirst("product") . '/' . $id . '/' . date('Y_m_d_h_i') . '/';
        $this->create_path(DIR_IMAGE . $path);
        return $path;
        //$filename = time() . '_' . $key . '.' . utf8_strtolower(utf8_substr(strrchr($tempname, '.'), 1));

    }

    private function create_path($dir_path)
    {
        $path = rtrim($dir_path, '/');
        $directories = explode("/", $path);
        $string = '';
        foreach ($directories as $directory) {
            $string .= "/{$directory}";
            if (!is_dir($string)) {
                exec("mkdir -m 777 {$string}");
            }
        }
    }


    /**
     * @param Product $data
     * @return mixed
     */
    public function addProduct($data)
    {

      $existed_sku = $this->db->query("SELECT cp2p.product_id from product p INNER JOIN customerpartner_to_product cp2p ON (p.product_id = cp2p.product_id) where sku='".$this->db->escape($data->sku)."' AND cp2p.customer_id=".(int)$data->seller_id);

      if($existed_sku->num_rows){
        $product_id = $existed_sku->row['product_id'];
        $this->archiveProduct($product_id,'before_update');

        $this->db->query("UPDATE product set price = '" . ((float)(isset($data->price) && $data->price != '') ? $data->price : $data->discounted_price) . "' WHERE product_id=".(int)$existed_sku->row['product_id']);

        $this->db->query("UPDATE product_special set deleted='yes' , deleted_date=NOW(), deleted_by=0  WHERE `product_id` = " . (int)$product_id );
        if (isset($data->discounted_price)  && $data->discounted_price > 0) {
            $this->db->query("INSERT INTO  product_special SET `product_id` = '" . (int)$product_id . "', `customer_group_id` = '1', `priority` = '1', `price` = '" . (float)$data->discounted_price . "', `date_start` = NOW(), `date_end` = date_add(NOW(),INTERVAL 2 YEAR), `generated_by` = 'xls_to_product' ");
        }

        $newImages = $this->saveImages($data->images,$product_id);
        $j = 0;
        foreach ($newImages as $img){
            foreach ($this->config_image->get('product') as $row) {
                if ($row['width'] > 10) {
                    $this->model_tool_image->uploadImage($img, $row['width'], $row['hieght']);
                }
            }
            if($j == 0){
                $this->db->query("UPDATE product SET image = '" . $this->db->escape($img) . "' WHERE product_id = '" . (int) $product_id . "'");
            }else{
                $this->db->query("INSERT INTO product_image SET product_id = '" . (int) $product_id . "', image = '" . $this->db->escape($img) . "', sort_order = '" . (int) $j . "'");
            }
            $j++;
        }

        $parent_option_id = $this->db->query("SELECT option_id from option_value where option_value_id=".(int)$data->option_values);
        if($parent_option_id->num_rows){
          $check_option = $this->db->query("SELECT product_option_id from product_option WHERE product_id=".(int)$existed_sku->row['product_id'] . " AND option_id=".(int)$parent_option_id->row['option_id']);
          if(!$check_option->num_rows){
            $this->db->query("INSERT INTO product_option (product_id, option_id, required, size_default, created_at, deleted) VALUES (".(int)$existed_sku->row['product_id'].",".$parent_option_id->row['option_id'].",1, '".$this->db->escape($data->option_type)."', NOW(),'0')");
            $product_option_id = $this->db->getLastId();
            $this->db->query ( "INSERT INTO product_option_value (product_option_id,product_id, option_id , option_value_id,quantity,subtract,price, deleted) VALUES (".(int) $product_option_id .", ".(int)$existed_sku->row['product_id'].",".$parent_option_id->row['option_id'].", ".(int) $data->option_values.",".$data->qty.", 1, 0,'0')");
          }else{
            $check_option_value = $this->db->query("SELECT product_option_value_id from product_option_value WHERE product_id=".(int)$existed_sku->row['product_id'] . " AND option_id=".(int)$parent_option_id->row['option_id'] . " AND option_value_id=".(int)$data->option_values);
            if(! $check_option_value->num_rows){
              $this->db->query ( "INSERT INTO product_option_value (product_option_id,product_id, option_id , option_value_id,quantity,subtract,price, deleted) VALUES (".(int) $check_option->row['product_option_id'] .", ".(int)$existed_sku->row['product_id'].",".$parent_option_id->row['option_id'].", ".(int) $data->option_values.",".$data->qty.", 1, 0,'0')");
            }else{
              $this->db->query ( "UPDATE product_option_value set quantity=".$data->qty." WHERE product_option_value_id=".$check_option_value->row['product_option_value_id']);
            }
          }
          $this->db->query("UPDATE product set quantity = (select sum(quantity) FROM product_option_value  WHERE product_id=".(int)$existed_sku->row['product_id']. " AND deleted='0') WHERE product_id=".(int)$existed_sku->row['product_id']);

        }
        $this->archiveProduct($product_id,'after_update');
        $this->url->create_URL('product', $product_id);
        $this->db->query("INSERT INTO sheet_to_product (sheet_id, product_id) value(".(int)$data->sheet_id.",".(int)$existed_sku->row['product_id'].")");
        $this->db->query(" INSERT IGNORE INTO algolia SET `type`='product', type_id='" . (int) $product_id . "', date_added=NOW() ");
        $this->db->query(" INSERT IGNORE INTO cache   SET `type`='product', type_id='" . (int) $product_id . "', date_added=NOW() ");

      }else{
        $this->db->query("INSERT INTO  product SET model = '" . $this->db->escape($data->model) . "', sku = '" . $this->db->escape($data->sku) . "', quantity = '" . (int)$data->qty . "', brand_id = '" . (int)$data->brand_id . "', seller_id = '" . (int)$data->seller_id . "', minimum = '1', subtract = '1', is_algolia = '1', stock_status_id = '7', date_available = NOW(), manufacturer_id = '1', shipping = '1', price = '" . ((float)(isset($data->price) && $data->price != '') ? $data->price : $data->discounted_price) . "', points = '0', weight = '0', weight_class_id = '1', `length` = '1', width = '1', height = '1', length_class_id = '1', status = '0', tax_class_id = '0', sort_order = '0', date_added = NOW() ,user_id=" . (int)$data->user_id );
        $product_id = $this->db->getLastId();

        if($product_id && $product_id > 0){
          $this->db->query("INSERT INTO product_description SET product_id = '" . (int)$product_id . "', language_id = '1', name = '" . $this->db->escape(htmlspecialchars($data->en_title)) . "', description = '" . $this->db->escape($data->en_description) . "', tag = '', meta_title = '', meta_description = '', meta_keyword = ''");
          $this->db->query("INSERT INTO  product_description SET product_id = '" . (int)$product_id . "', language_id = '2', name = '" . $this->db->escape(htmlspecialchars($data->ar_title)) . "', description = '" . $this->db->escape($data->ar_description) . "', tag = '', meta_title = '', meta_description = '', meta_keyword = ''");

          $this->db->query("INSERT INTO  product_to_store SET product_id = '" . (int)$product_id . "', store_id = '0'");


          if (isset($data->price) && $data->price != '' && $data->discounted_price > 0) {
              $this->db->query("INSERT INTO  product_special SET `product_id` = '" . (int)$product_id . "', `customer_group_id` = '1', `priority` = '1', `price` = '" . (float)$data->discounted_price . "', `date_start` = NOW(), `date_end` = date_add(NOW(),INTERVAL 1 YEAR), `generated_by` = 'xls_to_product' ");
          }
          if(!is_array($data->category_id)){
            $this->db->query("INSERT INTO  product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$data->category_id . "'");
          }else{
            foreach($data->category_id as $k => $cat_id )
              $this->db->query("INSERT INTO  product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$cat_id . "'");
          }


          $this->db->query("INSERT INTO `customerpartner_to_product` set customer_id='".$data->seller_id."', product_id='".$product_id."', price='" . ((float)(isset($data->price) && $data->price != '') ? $data->price : $data->discounted_price) . "', seller_price='" . ((float)(isset($data->price) && $data->price != '') ? $data->price : $data->discounted_price) . "', quantity = '".$data->qty."'");
          $parent_option_id = $this->db->query("SELECT option_id from option_value where option_value_id=".(int)$data->option_values);
          if($parent_option_id->num_rows){
            $this->db->query("INSERT INTO product_option (product_id, option_id, required, size_default, created_at, deleted) VALUES (".(int)$product_id.",".$parent_option_id->row['option_id'].",1, '".$this->db->escape($data->option_type)."', NOW(),'0')");// ON DUPLICATE
            $product_option_id = $this->db->getLastId();
            $this->db->query ( "INSERT INTO product_option_value (product_option_id,product_id, option_id , option_value_id,quantity,subtract,price, deleted) VALUES (".(int) $product_option_id .", ".(int)$product_id.",".$parent_option_id->row['option_id'].", ".(int) $data->option_values.",".$data->qty.", 1, 0,'0')");
          }
          if(!empty($data->attribute_id)){
              foreach($data->attribute_id as $key =>$attr)
                if((int)$attr != 0)
                  $this->db->query("INSERT INTO product_attribute (product_id,attribute_id,language_id,deleted) VALUES (".(int)$product_id.", ".(int)$attr.",1,'0'),(".(int)$product_id.", ".(int)$attr.",2,'0') ");
          }

          $newImages = $this->saveImages($data->images,$product_id);
          $j = 0;
          foreach ($newImages as $img){
              foreach ($this->config_image->get('product') as $row) {
                  if ($row['width'] > 10) {
                      $this->model_tool_image->uploadImage($img, $row['width'], $row['hieght']);
                  }
              }
              if($j == 0){
                  $this->db->query("UPDATE product SET image = '" . $this->db->escape($img) . "' WHERE product_id = '" . (int) $product_id . "'");
              }else{
                  $this->db->query("INSERT INTO product_image SET product_id = '" . (int) $product_id . "', image = '" . $this->db->escape($img) . "', sort_order = '" . (int) $j . "'");
              }
              $j++;
          }
          $this->db->query("INSERT INTO sheet_to_product (sheet_id, product_id) value(".(int)$data->sheet_id.",".(int)$product_id.")");
        }

            $this->url->create_URL('product', $product_id);
            $this->db->query(" INSERT IGNORE INTO algolia SET `type`='product', type_id='" . (int) $product_id . "', date_added=NOW() ");
            $this->db->query(" INSERT IGNORE INTO cache   SET `type`='product', type_id='" . (int) $product_id . "', date_added=NOW() ");

        }
        return $product_id;
    }


    public function getImage($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.1 Safari/537.11');
        $res = curl_exec($ch);
        $rescode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch) ;
        return $res;
    }

    public function archiveProduct($product_id, $status) {
        try {
            $m = new MongoClient(MONGO_DEFUALT_HOST);
            $db = $m->selectDB('product_old_data');
            $collection = new MongoCollection($db, 'product_old_data');
            $serialized_data = array('code' => 'product_' . $product_id . "__" . date("Y-m-d_h:i:sa"), 'status' => $status, 'date_of_modification' => date("Y-m-d h:i:sa"), 'user_id' => 0, 'user_ip' => '127.0.0.1', 'user_type' => 'XlsCronJob', 'product_id' => $product_id);

            $product_tables = ['product', 'product_option', 'product_option_value', 'product_special','product_image'];
            foreach ($product_tables as $key => $value) {
                $serialized_data['table_name'] = $value;
                $product_data = $this->db->query("SELECT * FROM " . DB_PREFIX . $value . " WHERE product_id = '" . (int) $product_id . "'");
                $serialized_data['table_data'] = '';
                if (!empty($product_data->rows))
                    $serialized_data['table_data'] = $product_data->rows;
                $collection->insert($serialized_data);
                unset($serialized_data['_id']);
            }
        } catch (Exception $e) {
            return false;
        }
        return false;
    }

}
///
$cron = new XlsQueueToProduct();
$cron->run();
