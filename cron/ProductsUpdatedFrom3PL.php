<?php
error_reporting(E_ALL);

require_once realpath(__DIR__) . '/Cron.php';
require_once realpath(__DIR__) .'/../system/phpexcel/Classes/PHPExcel.php';
class ProductsUpdatedFrom3PL extends Cron
{
    public function parseFile()
    {
        $commonfunctions = new commonfunctions($this->registry);
        $file_path = __DIR__ . '/files/3plAutoComplete.xlsx';
        $fileExists = file_exists($file_path);
        if ($fileExists) {
            $objPHPExcel = new PHPExcel();
            $objReader = PHPExcel_IOFactory::createReaderForFile($file_path);
            $objPHPExcel = PHPExcel_IOFactory::load($file_path);
            $objPHPExcel->setActiveSheetIndex(0);
            $partner_id=$order_id=$product_ids=$date=$comment=$file_order_id='';
            foreach ($objPHPExcel->getActiveSheet()->getRowIterator(2) as $row) {
                foreach ($row->getCellIterator() as $index => $cell) {

                    switch ($index) {
                        case 1:
                            $partner_id = (int) $cell->getValue();
                            break;
                        case 2:
                            $file_order_id = (int) $cell->getValue();
                            break;
                        case 5:
                            $product_ids = $cell->getValue();
                            break;
                        case 6:
                            $date = PHPExcel_Style_NumberFormat::toFormattedString($cell->getValue(), "YYYY-MM-DD");
                            break;
                        case 7:
                            $comment = $cell->getValue();
                            break;
                    }

                }
                if (trim($partner_id)!=''){
                    $order_id = (int)$commonfunctions->getOrderNumber($file_order_id);
                    if ($order_id != $file_order_id) {
                        $product_ids = explode(',', $product_ids);
                        foreach ($product_ids as $product_id) {
                            if (trim($product_id) != '') {
                                $ifExist = $this->db->query("SELECT count(*) as cnt
                                                              FROM customerpartner_to_order_status 
                                                              WHERE order_id=$order_id 
                                                              AND product_id=$product_id 
                                                              AND customer_id=$partner_id 
                                                              AND order_status_id=5");
                                if ($ifExist->row['cnt'] == 0) {
                                    echo "\nInsert status for order: $order_id\n";
                                    $this->db->query("INSERT INTO customerpartner_to_order_status SET order_id = $order_id,order_status_id = '5',notify = '0',comment = '" . $comment . "',product_id = '" . $product_id . "',customer_id = '" . $partner_id . "',date_added = '" . $date . "'");
                                }
                            }
                        }
                    }
                }

            }
        }
    }
}

$obj = new ProductsUpdatedFrom3PL();

$obj->parseFile();