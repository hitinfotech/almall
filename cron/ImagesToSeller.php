<?php
require_once('Cron.php');

class ImagesToSeller extends Cron
{
    function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $beansTalkd = new Beanstalk(array('server1' => array(
            'host' => BEANSTALKD_SERVER,
            'port' => BEANSTALKD_SERVER_PORT,
            'weight' => 50,
            'timeout' => 10,
            // array of connections/tubes
            'connections' => array(),
        )));
        $client = $beansTalkd->getClient();
        $client->watch('IMAGES_TO_SELLER_FOLDER');

        $closeTime = time() + 60 * 60 * 1;
        while (time() < $closeTime) {
            $job = $client->reserve(1);
            if ($job) {
                 $client->delete($job['id']);

                $product_image_id = $job['body'];

                if(strpos($product_image_id,'roduct_id_')){
                    $product_id = (isset(explode('product_id_',$product_image_id)[1]) ? explode('product_id_',$product_image_id)[1] : 0);
                    $image = $this->db->query("SELECT image,product_id FROM `product` WHERE `product_id`='$product_id'");
                }else
                    $image = $this->db->query("SELECT image,product_id FROM `product_image` WHERE `product_image_id`='$product_image_id'");

                if (!empty($data = $image->row)  && $data['image'] != '') {

                  $product_name = explode('/',$data['image']);
                  $product_name = $product_name[count($product_name) -1 ];
                  if(! file_exists(DIR_IMAGE . 'catalog/product-' . $data['product_id'] . '/' . $product_name)){
                    $this->create_path(DIR_IMAGE .  'catalog/product-' . $data['product_id']);
                    $this->create_path(DIR_IMAGE .  'catalog/product-' . $data['product_id']. '/'.'thumbnail');
                    copy(DIR_IMAGE . $data['image'] , DIR_IMAGE . 'catalog/product-' . $data['product_id'] . '/' . $product_name);
                    copy(DIR_IMAGE . $data['image'] , DIR_IMAGE . 'catalog/product-' . $data['product_id'] . '/'.'thumbnail/' . $product_name);

                    $this->myEcho( "Image: " .DIR_IMAGE . $data['image']." Was moved To seller Profile "."\n");
                  }else{
                    $this->myEcho( "Image: " .DIR_IMAGE . $data['image']." Already Exists "."\n");
                  }
              } else {
                    $this->myEcho("not valid id or removed image");
                }
            } else {
                $this->myEcho('no images in tube sleep 2 seconds');
                sleep(2);
            }
        }
        $this->myEcho('killed this old process to start a new one');
        $client->disconnect();
    }

    private function create_path($dir_path) {
        $path = rtrim($dir_path, '/');
        $directories = explode("/", $path);
        $string = '';
        foreach ($directories as $directory) {
            $string .= "/{$directory}";
            if (!is_dir($string)) {
                exec("mkdir -m 777 {$string}");
            }
        }
    }
}

$image = new ImagesToSeller();
$image->run();
