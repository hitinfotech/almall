<?php

require_once realpath(__DIR__) . '/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

require_once(DIR_SYSTEM . 'library/algolia/algoliasearch.php');

// Startup
require_once(DIR_SYSTEM . 'startup.php');

class GoogleFeeds extends Controller {

    public $registry;
    public $config;
    public $config_image;
    public $cache;
    public $languages = array();

    public function __construct() {
        $this->config_image = config_image::getInstance();
        // Registry
        $this->registry = Registry::getInstance();

        // Cache
        $this->cache = Cache::getInstance(CONFIG_CACHE);
        $this->registry->set('cache', $this->cache);

        // Loader
        $loader = Loader::getInstance($this->registry);
        $this->registry->set('load', $loader);

        // Config
        $config = Config::getInstance();
        $this->registry->set('config', $config);

        // Database
        $db = DB::getInstance(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
        $this->registry->set('db', $db);

        $config->set('config_store_id', 0);
        $query = $this->registry->get('db')->query("SELECT * FROM `setting` WHERE store_id = '0' ORDER BY store_id ASC");

        foreach ($query->rows as $row) {
            if (!$row['serialized']) {
                $config->set($row['key'], $row['value']);
            } else {
                $config->set($row['key'], json_decode($row['value'], true));
            }
        }

        // Request
        $request = new Request();
        $this->registry->set('request', $request);

        $config->set('config_url', HTTP_SERVER);
        $config->set('config_ssl', HTTPS_SERVER);

        // Url
        $url = new Url($config->get('config_url'), $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url'));
        $this->registry->set('url', $url);

        // Log
        $log = new Log($config->get('config_error_filename'));
        $this->registry->set('log', $log);

        // Country Detection Library
        $country = new Country($this->registry);
        $this->registry->set('country', $country);

        // Language Detection

        $query = $this->registry->get('db')->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE status = '1'");
        if ($query->num_rows) {
            $result = $query->rows;
        } else {
            $result = array();
        }

        foreach ($result as $row) {
            $this->languages[$row['code']] = $row;
        }
        $language_code = 'en';
        // Language
        $language = new Language($this->languages[$language_code]['directory']);
        $language->load($this->languages[$language_code]['directory']);
        $this->registry->set('language', $language);

        // Document
        $this->registry->set('document', new Document());

        // Currency
        $this->registry->set('currency', new Currency($this->registry));
        $this->registry->get('currency')->set($this->registry->get('country')->getCurcode());

        //Session
        $session = Session::getInstance();
        $this->registry->set('session', $session);

        // Tax
        $this->registry->set('tax', new Tax($this->registry));

        // Weight
        $this->registry->set('weight', new Weight($this->registry));

        $this->config = $this->registry->get('config');
        $this->country = $this->registry->get('country');
        $this->currency = $this->registry->get('currency');
        $this->language = $this->registry->get('language');
        $this->session = $this->registry->get('session');

        $this->run();
    }

    public function run() {

        $countries = $this->registry->get('db')->query("SELECT iso_code_2 FROM country  WHERE available = 1 ORDER BY iso_code_2 ");
        $languages = $this->registry->get('db')->query("SELECT code       FROM language WHERE status    = 1 ");
        foreach ($countries->rows as $country_row) {
            foreach ($languages->rows as $language_row) {

                $country_row['iso_code_2'] = strtolower($country_row['iso_code_2']);
                $this->country->set($country_row['iso_code_2']);
                $this->currency->set($this->country->getCurcode());

                $language = new Language($this->languages[$language_row['code']]['directory']);
                $language->load($this->languages[$language_row['code']]['directory']);
                $this->registry->set('language', $language_row['code']);

                $this->config->set('config_language_id', $this->languages[$language_row['code']]['language_id']);
                $this->config->set('config_language', $this->languages[$language_row['code']]['code']);

                if ($this->config->get('google_base_status')) {

                    $output = '<?xml version="1.0" encoding="utf-8" ?>';
                    $output .= '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">';
                    $output .= '<channel>';
                    $output .= '<title>' . $this->config->get('config_name') . '</title>';
                    $output .= '<description>' . $this->config->get('config_meta_description') . '</description>';
                    $output .= '<link>' . HTTP_SERVER . '</link>';

                    $this->load->model('feed/google_base');
                    $this->load->model('catalog/category');
                    $this->load->model('catalog/product');

                    $this->load->model('tool/image');

                    $product_data = array();

                    $google_base_categories = $this->model_feed_google_base->getCategories();

                    foreach ($google_base_categories as $google_base_category) {
                        $filter_data = array(
                            'filter_category_id' => $google_base_category['category_id'],
                            'filter_filter' => false,
                            'filter_brand_id' => 2892
                        );

                        $products = $this->model_catalog_product->getProducts($filter_data);

                        foreach ($products as $product) {
                            if (!in_array($product['product_id'], $product_data) && $product['description']) {
                                $url = $this->db->query("SELECT keyword FROM url_alias_" . $this->config->get('config_language') . " WHERE query='product_id=" . $product['product_id'] . "' ");

                                $output .= '<item>';
                                $output .= '<title><![CDATA[' . $product['name'] . ']]></title>';
                                $output .= '<link>' . ($url->num_rows?(HTTPS_SERVER."{$country_row['iso_code_2']}/{$language_row['code']}/".$url->row['keyword']):HTTPS_SERVER."index.php?route=product/product&product_id=".$product['product_id']) . '</link>';
                                $output .= '<description><![CDATA[' . strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')) . ']]></description>';
                                $output .= '<g:brand><![CDATA[' . strip_tags(html_entity_decode($product['brand_name'], ENT_QUOTES, 'UTF-8')) . ']]></g:brand>';
                                $output .= '<g:condition>new</g:condition>';
                                $output .= '<g:id>' . $product['product_id'] . '</g:id>';

                                if ($product['image']) {
                                    $output .= '<g:image_link>' . $this->model_tool_image->resize($product['image'], $this->config_image->get('product', 'details', 'width'), $this->config_image->get('product', 'details', 'hieght')) . '</g:image_link>';
                                } else {
                                    $output .= '<g:image_link></g:image_link>';
                                }

                                $output .= '<g:model_number>' . $product['model'] . '</g:model_number>';

                                if ($product['mpn']) {
                                    $output .= '<g:mpn><![CDATA[' . $product['mpn'] . ']]></g:mpn>';
                                } else {
                                    $output .= '<g:identifier_exists>false</g:identifier_exists>';
                                }

                                if ($product['upc']) {
                                    $output .= '<g:upc>' . $product['upc'] . '</g:upc>';
                                }

                                if ($product['ean']) {
                                    $output .= '<g:ean>' . $product['ean'] . '</g:ean>';
                                }

                                $currency_code = $this->currency->getCode();
                                $currency_value = $this->currency->getValue();

                                if ((float) $product['special']) {
                                    $output .= '<g:price>' . str_replace(",", "", $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id']), $currency_code, $currency_value, true)) . '</g:price>';
                                } else {
                                    $output .= '<g:price>' . str_replace(",", "", $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id']), $currency_code, $currency_value, true)) . '</g:price>';
                                }

                                //$output .= '<g:google_product_category>' . $google_base_category['google_base_category_id'] . '</g:google_product_category>';

                                $categories = $this->model_catalog_product->getCategories($product['product_id']);

                                foreach ($categories as $category) {
                                    $path = $this->getPath($category['category_id']);

                                    if ($path) {
                                        $string = '';

                                        foreach (explode('_', $path) as $path_id) {
                                            $category_info = $this->model_catalog_category->getCategory($path_id);

                                            if ($category_info) {
                                                if (!$string) {
                                                    $string = $category_info['name'];
                                                } else {
                                                    $string .= ' &gt; ' . $category_info['name'];
                                                }
                                            }
                                        }

                                        $output .= '<g:product_type><![CDATA[' . $string . ']]></g:product_type>';
                                    }
                                }

                                $output .= '<g:quantity>' . $product['quantity'] . '</g:quantity>';
                                $output .= '<g:weight>' . $this->weight->format($product['weight'], $product['weight_class_id']) . '</g:weight>';
                                $output .= '<g:availability><![CDATA[' . ($product['quantity'] ? 'in stock' : 'out of stock') . ']]></g:availability>';
                                $output .= '</item>';
                            }
                        }
                    }

                    $output .= '</channel>';
                    $output .= '</rss>';
                    $encoding = mb_detect_encoding($output, 'UTF-8, ISO-8859-9, ISO-8859-1');
                    if ($encoding != 'UTF-8') {
                        $output = mb_convert_encoding($output, 'UTF-8', $encoding);
                    }
                    file_put_contents(DIR_IMAGE . "johnson_feeds_" . strtolower($country_row['iso_code_2']) . "_" . strtolower($language_row['code']) . ".xml", $output);
                }
            }
        }
    }

    protected function getPath($parent_id, $current_path = '') {
        $category_info = $this->model_catalog_category->getCategory($parent_id);

        if ($category_info) {
            if (!$current_path) {
                $new_path = $category_info['category_id'];
            } else {
                $new_path = $category_info['category_id'] . '_' . $current_path;
            }

            $path = $this->getPath($category_info['parent_id'], $new_path);

            if ($path) {
                return $path;
            } else {
                return $new_path;
            }
        }
    }

}

new GoogleFeeds();
