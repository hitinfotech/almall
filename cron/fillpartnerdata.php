<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

$errors = array();

$db = new DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

function update_partner($partner_id, $db = null) {

    $partner_info = $db->query("SELECT firstname, lastname, email, password, status, salt, date_added FROM customer WHERE customer_id = '" . (int) $partner_id . "'");

    if ($partner_info->num_rows == 1) {
        $partner_info = $partner_info->row;
        $db->query("UPDATE customerpartner_to_customer SET firstname='" . $db->escape($partner_info['firstname']) . "', lastname='" . $db->escape($partner_info['lastname']) . "', email='" . $db->escape($partner_info['email']) . "', password='" . $db->escape($partner_info['password']) . "', status='" . (int) $partner_info['status'] . "', salt='" . $db->escape($partner_info['salt']) . "', date_added='" . $db->escape($partner_info['date_added']) . "' WHERE customer_id = '" . (int) $partner_id . "' ");
    }
}

$result = $db->query("SELECT customer_id FROM customerpartner_to_customer ");
foreach ($result->rows as $row) {
    $customer_id = (int) $row['customer_id'];
    update_partner($customer_id, $db);
}