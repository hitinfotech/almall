<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

$db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);

$x = 1000;

while ($x > 0) {
    
    $images = $db->query(" SELECT id, image FROM `image_s3` WHERE uploaded < 1  LIMIT 1 ");
    
    $x = $images->num_rows;
    
    foreach ($images->rows as $row) {
        if (is_file(DIR_IMAGE . $row['image'])) {
            
            $db->query("UPDATE image_s3 SET uploaded = 1 WHERE id='" . (int) $row['id'] . "'");
            
            $zipped = 'CDNUploads/' . $row['image'];
            
            $path = '';

            $directories = explode('/', dirname(str_replace('../', '', $zipped)));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir(DIR_IMAGE . $path)) {
                    @mkdir(DIR_IMAGE . $path, 0777);
                }
            }

            exec('cp "' . DIR_IMAGE . $row['image'] . '" "' . DIR_IMAGE . $zipped . '"');
            $s3_name = $row['image'];

            exec('gzip -f -9 "' . DIR_IMAGE . $zipped . '"');

            $aws = "aws s3 cp '" . DIR_IMAGE . $zipped . ".gz' 's3://". S3_BUKET_NAME_CDN ."/" . $s3_name . "' --expires 2034-01-01T00:00:00Z --acl public-read-write --content-encoding gzip --cache-control max-age=3600,public";
            exec($aws);
            echo "\n {$row['image']}";
        } else {
            $db->query("UPDATE image_s3 SET uploaded = 2 WHERE id='" . (int) $row['id'] . "'");

            echo "\nno file like " . DIR_IMAGE . $row['image'];
        }
    }
    
    
}
