<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 10/11/17
 * Time: 10:00 AM
 */
require_once realpath(__DIR__) . '/Cron.php';
require 'vendor/autoload.php';

class DailyReportFor3PL extends Cron
{
    public function __construct()
    {
        exec("ps aux | grep -i DailyReportFor3PL |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();

    }

    public function getShippingCompany($fromId,$toId) {
        //return 4;
        $query =  $this->db->query("SELECT
                                  s.origin,
                                  f.country_id oid,
                                  s.destination,
                                  t.country_id tid,
                                  s.ship_company_id
                                FROM
                                  ship_company_to_country s
                                LEFT JOIN
                                  country f ON(s.origin = f.iso_code_2)
                                LEFT JOIN
                                  country t ON(s.destination = t.iso_code_2)
                                WHERE
                                  f.country_id = '$fromId' AND t.country_id = '$toId' ");
        return $query->row['ship_company_id'];
    }

    public function getOrder($order_id)
    {
        $order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "' AND order_status_id > '0'");
        if ($order_query->num_rows) {
            return array(
                'order_id' => $order_query->row['order_id'],
                'shipping_zone_id' => $order_query->row['shipping_zone_id'],
                'shipping_country_id' => $order_query->row['shipping_country_id'],
                'order_status_id' => $order_query->row['order_status_id'],
                'date_added' => $order_query->row['date_added'],
            );

        } else {
            return false;
        }
    }

    public function getProfile($customer_id)
    {
        $query = $this->db->query("SELECT c2c.*,c2cd.* FROM " . DB_PREFIX . "customerpartner_to_customer c2c LEFT JOIN customerpartner_to_customer_description c2cd ON(c2c.customer_id=c2cd.customer_id) where c2c.customer_id = '" . $customer_id . "' and c2cd.language_id = 1 ");
        $data = $query->row;
        $country = $this->getCountryINFO($data['country_id']);
        $zone = $this->getZoneINFO($data['zone_id']);

        $data['zone_name'] = $zone['name'];
        $data['country_name'] = $country['name'];
        return $data;
    }

    public function getCountryINFO($country_id)
    {

        $query = $this->db->query("SELECT name, iso_code_2, telecode FROM country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE cd.language_id = 1 AND c.country_id = '" . (int)$country_id . "' ");
        return $query->row;
    }

    public function getZoneINFO($zone_id)
    {

        $query = $this->db->query("SELECT * FROM zone z LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE zd.language_id = 1 AND z.zone_id = '" . (int)$zone_id . "' ");

        return $query->row;
    }

    public function getSellerQuantity($order_id, $seller_id)
    {
        $result = array();

        $data = $this->db->query("SELECT product_id, quantity, order_product_status FROM customerpartner_to_order WHERE order_id='" . (int)$order_id . "' AND customer_id='" . (int)$seller_id . "' ");

        if ($data->num_rows) {
            $result['quantity'] = 0;
            $result['pieces'] = 0;
            foreach ($data->rows as $row) {
                //   if ($row['order_product_status'] == 17) {
                $result['quantity'] = $result['quantity'] + 1;
                $result['pieces'] = $result['pieces'] + $row['quantity'];
                // }
            }
        }

        return $result;
    }

    public function convertOrderNumber($order_id,$date_added=''){
      // the new order numbers will be in the form of  yy-mm-order_id-dd
      if($date_added != ''){
        return date('y', strtotime($date_added)).date('m', strtotime($date_added)).$order_id.date('d', strtotime($date_added));
      }
      $query = $this->db->query("SELECT date_added FROM `order` WHERE order_id='".(int) $order_id."'");
      if($query->num_rows){
        return date('y', strtotime($query->row['date_added'])).date('m', strtotime($query->row['date_added'])).$order_id.date('d', strtotime($query->row['date_added']));
      }

    }

    public function sendEmail($aEmailData)
    {
        parent::__construct();
        if (defined('SAFE_TO_SEND') && SAFE_TO_SEND) {
            $mailGun = new \Mailgun\Mailgun(MAILGUN_KEY);
            $sender = 'Sayidaty';
            $from = 'mall@sayidaty.com';
            $date = date('Y-m-d');
            $subject = "Daily Booking Report For Sayidaty Mall $date " . $aEmailData['3PLName'] . "";
            $data = array();
            $data['from'] = "{$sender} <{$from}>";
            $data['to'] = $aEmailData['to'];
            $data['subject'] = $subject;
            $data['html'] = $aEmailData['message'];
            $result = $mailGun->sendMessage(MAILGUN_DOMAIN, $data);
            var_dump($result);
        }
    }


}

$oReport = new DailyReportFor3PL();
$aGroupedOrdersByShippingCompany = array();
$aEmailData = array();
$email_templates = new config_email_templates();

$orders = $oReport->db->query("SELECT order_id FROM `order` WHERE order_status_id = 17;");
if ($orders->rows) {
    foreach ($orders->rows as $row) {
        $query = $oReport->db->query("SELECT DISTINCT cp2c.customer_id, cp2c.country_id FROM " . DB_PREFIX . "order_product op
                                        LEFT JOIN customerpartner_to_product cp2p ON (op.product_id = cp2p.product_id)
                                        LEFT JOIN customerpartner_to_customer cp2c ON (cp2p.customer_id = cp2c.customer_id)
                                        LEFT JOIN customerpartner_to_order cp2o ON (op.order_id = cp2o.order_id AND op.product_id = cp2o.product_id)
                                        WHERE op.order_id = '" . (int)$row['order_id'] . "' ");
        foreach ($query->rows as $query_row) {
            $order_info = $oReport->getOrder($row['order_id']);
            $seller_info = $oReport->getProfile($query_row['customer_id']);
            $shipping_company_id = $oReport->getShippingCompany($seller_info['country_id'], $order_info['shipping_country_id']);

            $data = $oReport->db->query("SELECT AWBNO,book_response FROM order_booking WHERE order_id = " . $row['order_id'] . ";");
            if (!empty($data->row)) {
                if ($shipping_company_id == 2) {
                    $book_response = $data->row['book_response'];
                    $AWBno = $data->row['AWBNO'];
                    $aData['booking_ref'] = !is_null($AWBno) && trim($AWBno) != '' ? $AWBno : '';
                    $aData['awb_ref'] = !is_null($book_response) && trim($book_response) != '' ? $book_response : '';
                } else {
                    $book_response = $data->row['book_response'];
                    $aData['booking_ref'] = !is_null($book_response) && trim($book_response) != '' ? $book_response : '';
                    $aData['awb_ref'] = '';
                }
            }

            $aData['order_date'] = $order_info['date_added'];
            $aData['order_no'] = $this->convertOrderNumber($order_info['order_id']);
            $aData['company_name'] = $seller_info['companyname'];
            $aData['quantity'] = $oReport->getSellerQuantity($row['order_id'], $query_row['customer_id'])['quantity'];
            $aData['city'] = $oReport->db->query("SELECT `name` FROM zone_description WHERE zone_id = " . $order_info['shipping_zone_id'] . " AND language_id = 1")->row['name'];
            $aData['status'] = $oReport->db->query("SELECT name FROM order_status WHERE order_status_id = " . $order_info['order_status_id'] . " ;")->row['name'];

            $aGroupedOrdersByShippingCompany[$shipping_company_id][$row['order_id']] = $aData;
        }
    }
} else {
    echo "\n------- No Orders -------\n";
}

if (!empty($aGroupedOrdersByShippingCompany)) {
    foreach ($aGroupedOrdersByShippingCompany as $shippingCompany => $aOrders) {
        $date = date('Y-m-d');
        $aMessage = "Dear Team : Please find your Pickup list for the date $date for Sayidaty Mall Orders";
        $sEmailMessage = $email_templates->addNewBookingEmailAlert($aOrders,$aMessage);

        $aEmailData['message'] = $sEmailMessage;
        switch ($shippingCompany) {
            case 1:
                $aEmailData['to'] = FirstFlight_REPORT_EMAILS;
                $aEmailData['cc'] = FirstFlight_REPORT_CC_EMAILS;
                $aEmailData['3PLName'] = 'First Flight';
                $oReport->sendEmail($aEmailData);
                break;
            case 2:
                $aEmailData['to'] = NaqelExpress_REPORT_EMAILS;
                $aEmailData['cc'] = NaqelExpress_REPORT_CC_EMAILS;
                $aEmailData['3PLName'] = 'Naqel Express';
                $oReport->sendEmail($aEmailData);
                break;
            case 3:
                $aEmailData['to'] = SMSA_EMAILS;
                $aEmailData['3PLName'] = 'SMSA';
                $oReport->sendEmail($aEmailData);
                break;
            case 4:
                $aEmailData['to'] = PostaPlus_EMAILS;
                $aEmailData['3PLName'] = 'PostaPlus';
                $oReport->sendEmail($aEmailData);
                break;
        }
    }
}
