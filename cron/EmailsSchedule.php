<?php
require_once 'Cron.php';
/**
 * Created by PhpStorm.
 * User: bahaa
 * Date: 5/18/17
 * Time: 12:02 AM
 */
class EmailsSchedule extends Cron
{

    public function __construct()
    {
        exec("ps aux | grep -i EmailsSchedule |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();


    }
    public function run(){
       $closeTime = time() + 60*60*1;
        while(time() < $closeTime){

            $this->db->query("UPDATE emails_campaign em SET  em.status = 'NEW' WHERE send_date < now() and status='SCHEDULED'  ");
            $c = $this->db->countAffected();
            if($c > 0)
                $this->myEcho($c." campaign start");
            $this->myEcho("sleep 5 seconds");
            sleep(5);
        }
    }

}
$cron = new EmailsSchedule();
$cron->run();