<?php
require_once realpath(__DIR__) . '/Cron.php';

class UpdateArabicProductsDesc extends Cron
{
    public function startProcess()
    {
        $c = 0;
        while (1) {
            $products_query = $this->db->query("SELECT product_id,name,description FROM product_description WHERE language_id=2 LIMIT 1000 OFFSET ".$c);
            if (!$products_query->num_rows)
                break;
            foreach ($products_query->rows as $product) {
                $data['description'] = utf8_substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0);
                if (trim($data['description']) == '') {
                    echo "\n Update Product ID: ".$product['product_id']."\n";
                    $this->db->query("UPDATE product_description SET description = name WHERE language_id=2 AND product_id = " . (int)$product['product_id'] . " ");
                }
            }
            $c+=1000;
        }
    }
}
$obj = new UpdateArabicProductsDesc();
$obj->startProcess();