<?php

set_time_limit(0);
error_reporting(E_ALL);
ini_set('memory_limit', -1);
ini_set('display_errors', 1);
require_once realpath(__DIR__) . '/aaenv.php';
require_once realpath(__DIR__) . '/settings.php';
if (!defined('DIR_APPLICATION')) {
    die("\nError Initialaize cron \n");
    exit;
}
require_once(DIR_SYSTEM . 'startup.php');

class Cron
{

    /** @var  Registry $registry */
    public  $registry;
    /** @var  Config $config */
    public $config;
    /** @var  Loader $loader */
    public $loader;
    /** @var  DB $db */
    public $db;
    /** @var Language $language */
    public $language;
    /** @var Currency $currency */
    public $currency;

    public function __construct()
    {

        $this->registry = Registry::getInstance();
        $this->config = Config::getInstance();
        $this->registry->set('config', $this->config);
        $this->loader = Loader::getInstance($this->registry);
        $this->registry->set('load', $this->loader);
        $this->db = DB::getInstance(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
        $this->registry->set('db', $this->db);
        $cache = Cache::getInstance(CONFIG_CACHE);
        $this->registry->set('cache', $cache);
        $this->registry->set('currency', new Currency($this->registry));
        $this->currency = $this->registry->get('currency');

        // Settings
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0'");

        foreach ($query->rows as $setting) {
            if (!$setting['serialized']) {
                $this->config->set($setting['key'], $setting['value']);
            } else {
                $this->config->set($setting['key'], json_decode($setting['value'], true));
            }
        }


        // Language
        $languages = array();

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language`");

        foreach ($query->rows as $result) {
            $languages[$result['code']] = $result;
        }

        $this->config->set('config_language_id', $languages[$this->config->get('config_admin_language')]['language_id']);

        // Language
        $this->language = new Language($languages[$this->config->get('config_admin_language')]['directory']);
        $this->language->load($languages[$this->config->get('config_admin_language')]['directory']);
        $this->registry->set('language', $this->language);


    }
    public function myEcho($s)
    {
        echo $s . "-----------------" . date("Y/m/d h:i:s") . "\r\n";
    }

    public function __get($key) {

        return $this->registry->get($key);
    }

    public function __set($key, $value) {
        $this->registry->set($key, $value);
    }
}