<?php
require_once 'Cron.php';
require_once (DIR_SYSTEM .'phpexcel/Classes/PHPExcel.php');

class XlsToProduct extends Cron
{

    public function __construct()
    {
        exec("ps aux | grep -i XlsSheetToProduct |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();
        $request = new Request();
        $this->registry->set('request', $request);
        $session = Session::getInstance();
        $this->registry->set('session', $session);
        $cache = Cache::getInstance(CONFIG_CACHE);
        $this->registry->set('cache', $cache);
        $this->registry->set('currency', new Currency($this->registry));


    }
    public function run(){

        $query = $this->db->query("SELECT * from product_upload_sheet WHERE sheet_status='ADDED'");

        if($query->num_rows){
          foreach($query->rows as $key => $sheet){
            $this->db->query("UPDATE product_upload_sheet set sheet_status='INQUEUE' WHERE sheet_id=".(int)$sheet['sheet_id']);

            $file_path = DIR_IMAGE.$sheet['file_name'];

            $client = $this->getBeansTalkd();
            $client->useTube("products");

            $objPHPExcel = new PHPExcel;
            $objPHPExcel = PHPExcel_IOFactory::load($file_path);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $currency = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "currency WHERE currency_id = " . (int) $sheet['currency_id'] )->row;

            $i=2;
            foreach ($objWorksheet->getRowIterator() as $row) {
                if(empty($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue()))
                    break;
                $product = new Product($this->registry);
                $product->sku  = $objPHPExcel->getActiveSheet()->getCell("A$i")->getValue();
                $product->model = $objPHPExcel->getActiveSheet()->getCell("B$i")->getValue();
                $product->en_title  = $objPHPExcel->getActiveSheet()->getCell("C$i")->getValue();
                $product->ar_title  = $objPHPExcel->getActiveSheet()->getCell("D$i")->getValue();
                if(is_object($product->ar_title))
                    $title = $objPHPExcel->getActiveSheet()->getCell("D$i")->getValue()->getPlainText();

                $product->en_description  = $objPHPExcel->getActiveSheet()->getCell("E$i")->getValue();
                $product->ar_description  = $objPHPExcel->getActiveSheet()->getCell("F$i")->getValue();
                $product->category_id  = $objPHPExcel->getActiveSheet()->getCell("G$i")->getValue();
                if(strpos($product->category_id , ',')){
                  $product->category_id = explode(',',$product->category_id);
                }
                $product->qty  = $objPHPExcel->getActiveSheet()->getCell("H$i")->getValue();
                $product->option_values  = $objPHPExcel->getActiveSheet()->getCell("I$i")->getValue();

                $discounted_price  = $objPHPExcel->getActiveSheet()->getCell("J$i")->getValue();
                if($discounted_price > 0){
                  $product->discounted_price = $this->currency->convert($discounted_price,$currency['code'],$this->config->get('config_currency'));
                }

                $price  = $objPHPExcel->getActiveSheet()->getCell("K$i")->getValue();
                if($price > 0){
                  $product->price =$this->currency->convert($price,$currency['code'],$this->config->get('config_currency'));
                }
                $product->brand_id  = $objPHPExcel->getActiveSheet()->getCell("L$i")->getValue();
                $product->seller_id  = $sheet['seller_id'];
                $product->sheet_id  = $sheet['sheet_id'];
                $product->user_id  = $sheet['user_id'];
                $product->attribute_id[]  = $objPHPExcel->getActiveSheet()->getCell("M$i")->getValue();
                $product->attribute_id[]  = $objPHPExcel->getActiveSheet()->getCell("N$i")->getValue();
                $product->attribute_id[]  = $objPHPExcel->getActiveSheet()->getCell("O$i")->getValue();
                $product->attribute_id[]  = $objPHPExcel->getActiveSheet()->getCell("P$i")->getValue();
                $product->attribute_id[]  = $objPHPExcel->getActiveSheet()->getCell("Q$i")->getValue();
                $product->attribute_id[]  = $objPHPExcel->getActiveSheet()->getCell("R$i")->getValue();
                $product->attribute_id[]  = $objPHPExcel->getActiveSheet()->getCell("S$i")->getValue();
                $product->attribute_id[]  = $objPHPExcel->getActiveSheet()->getCell("T$i")->getValue();
                $product->attribute_id[]  = $objPHPExcel->getActiveSheet()->getCell("U$i")->getValue();
                $product->attribute_id[]  = $objPHPExcel->getActiveSheet()->getCell("V$i")->getValue();
                $product->images[]  = $objPHPExcel->getActiveSheet()->getCell("W$i")->getValue();
                $product->images[]  = $objPHPExcel->getActiveSheet()->getCell("X$i")->getValue();
                $product->images[]  = $objPHPExcel->getActiveSheet()->getCell("Y$i")->getValue();
                $product->images[]  = $objPHPExcel->getActiveSheet()->getCell("Z$i")->getValue();
                $product->images[]  = $objPHPExcel->getActiveSheet()->getCell("AA$i")->getValue();
                $product->option_type  = $objPHPExcel->getActiveSheet()->getCell("AB$i")->getValue();
                $allowed_types = ['eur','it','uk','us','xmls'];

                if(! in_array($product->option_type,$allowed_types)){
                  $product->option_type = 'xmls';
                }

                $id = ($product->save());

                $client->put(0, 0, 0,$id);

                $product->updateStatus($id,"INQUEUE");

                $i++;
            }

          }
          $this->myEcho("Sheet:" . $sheet['file_name'] . " was added to queue");
          $this->db->query("UPDATE product_upload_sheet set sheet_status='DONE' WHERE sheet_id=".(int)$sheet['sheet_id']);
        }
    }

    private function getBeansTalkd()
    {
        $beansTalkd = new Beanstalk(array('server1' => array(
            'host' => BEANSTALKD_SERVER,
            'port' => BEANSTALKD_SERVER_PORT,
            'weight' => 50,
            'timeout' => 10,
            // array of connections/tubes
            'connections' => array(),
        )));
        $client = $beansTalkd->getClient();
        return $client;
    }



}

$cron = new XlsToProduct();
$cron->run();
