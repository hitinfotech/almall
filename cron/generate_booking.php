<?php

require_once realpath(__DIR__) . '/Cron.php';
require 'vendor/autoload.php';

class generateBooking extends Cron
{
    private $shippingCompany;

    public function __construct()
    {
        exec("ps aux | grep -i generateBooking |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();
        $this->email_templates = new config_email_templates();
    }

    public function book($order_id, $info, $num_of_sellers)
    {
        $seller_id = $info['customer_id'];
        $product_id = $info['product_id'];

        $order_info = $this->getOrder($order_id);

        $data = array(
            "order_id" => $order_info['order_id'],
            "prod_type" => "DOX"
        );

        if (!$order_info) {
            return false;
        }


        $seller_info = $this->getProfile($seller_id);


        $shipping_company = $this->getShippingCompany($seller_info['country_id'],$order_info['shipping_country_id']);

        $qty = $this->getSellerQuantity($order_id, $seller_id);

        $zone_info = $this->getZoneINFO($order_info['shipping_zone_id']);
        $country_info = $this->getCountryINFO($order_info['shipping_country_id']);
        $cod_amount = 0;
        if ($order_info['payment_code'] == 'cod') {
            $cod_amount = round($this->currency->convert($order_info['total'],$this->config->get('config_currency'),$order_info['currency_code']),2);//$this->getSellerOrderPrice($order_id, $seller_id);
        }

        $operation_data = array();

        $sEmails = '';
        $sCC_Emails = '';
        if ($shipping_company == 1) { // FF SERVICES
            $operation_data['argAwbBooking']['Destination'] = 'DXB';//regarding to task MDS-691, $this->get_FFCitycode_code($order_info['shipping_zone_id']);

            $operation_data['argAwbBooking']['BRef'] = "{$this->convertOrderNumber($order_id)}";

            $operation_data['argAwbBooking']['ThirdParty'] = "O";
            $operation_data['argAwbBooking']['ACPhone'] = $order_info['telephone'];
            $operation_data['argAwbBooking']['ACCPErson'] = $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname'];
            $operation_data['argAwbBooking']['ACAddress1'] = $order_info['payment_address_1'];
            $operation_data['argAwbBooking']['ACCity'] = $this->get_FFCitycode_code($order_info['payment_zone_id']) . ' / ' . $order_info['payment_city'];

            $operation_data['argAwbBooking']['Consignee'] = $order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname'];
            $operation_data['argAwbBooking']['ConsigneeCPerson'] = $order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname'];
            $operation_data['argAwbBooking']['ConsigneeAddress1'] = $order_info['shipping_address_1'];
            $operation_data['argAwbBooking']['ConsigneeAddress2'] = $order_info['shipping_address_2'];
            $operation_data['argAwbBooking']['ConsigneePhone'] = $order_info['shipping_telephone'];
            $operation_data['argAwbBooking']['ConsigneeMobile'] = $order_info['shipping_telephone'];
            $operation_data['argAwbBooking']['ConsigneeCity'] = $zone_info['name'];
            $operation_data['argAwbBooking']['ConsigneeCountry'] = $country_info['name'];
            $operation_data['argAwbBooking']['ConsigneePin'] = '';


            //$operation_data['argAwbBooking']['Quantity'] = $qty['quantity'];
            $operation_data['argAwbBooking']['Quantity'] = 1;//always 1 regardin to Naveens request.
            $operation_data['argAwbBooking']['Pieces'] = $qty['pieces'];


            $operation_data['argAwbBooking']['Consignee'] = 'Sayidaty Mall Fulfillment';
            $operation_data['argAwbBooking']['ConsigneeCPerson'] = $country_info['iso_code_2'] . '-' . $num_of_sellers;
            $operation_data['argAwbBooking']['ConsigneeAddress1'] = 'First Flight Ecom Team';
            $operation_data['argAwbBooking']['ConsigneeAddress2'] = '';
            $operation_data['argAwbBooking']['ConsigneePhone'] = '';
            $operation_data['argAwbBooking']['ConsigneeMobile'] = '';
            $operation_data['argAwbBooking']['ConsigneeCity'] = $zone_info['name'];
            $operation_data['argAwbBooking']['ConsigneeCountry'] = $order_info['shipping_iso_code_2'];
            $operation_data['argAwbBooking']['ConsigneePin'] = '';

            $operation_data['argAwbBooking']['ServiceType'] = 'NOR';
            $operation_data['argAwbBooking']['ProductType'] = 'XPS';
            $operation_data['argAwbBooking']['InvoiceValue'] = '0';
            $operation_data['argAwbBooking']['CODAmount'] = $cod_amount;

            $operation_data['argAwbBooking']['Weight'] = '0.00';
            $operation_data['argAwbBooking']['email'] = $order_info['email'];
            $operation_data['argAwbBooking']['RTOShipment'] = '';
            $operation_data['argAwbBooking']['VehType'] = 'B';
            //product 1, product 2,

            $operation_data['argAwbBooking']['AddnlInfo'] = '';
            $operation_data['argAwbBooking']['CustomerCode'] = '';
            $operation_data['argAwbBooking']['BDimension'] = '10*10*10';


            $operation_data['argAwbBooking']['PBranch'] = $this->get_FFCitycode_code($seller_info['zone_id']);
            $operation_data['argAwbBooking']['Origin'] = $this->get_FFCitycode_code($seller_info['zone_id']);
            $operation_data['argAwbBooking']['ReadyDate'] = date('Y-m-d');
            $operation_data['argAwbBooking']['ReadyTime'] = '08:00';
            $operation_data['argAwbBooking']['ClsoingTime'] = '16:00';
            $operation_data['argAwbBooking']['SpecialInst'] = 'shipper opening times:' . $seller_info['opening_time'] . '- Order Id: ' . $this->convertOrderNumber($order_id) ;
            //$operation_data['argAwbBooking']['AWBNo'] = $data['AWBNO'];
            $operation_data['argAwbBooking']['Shipper'] = $seller_info['companyname'];
            $operation_data['argAwbBooking']['ShipperCPErson'] = $seller_info['screenname'];
            $operation_data['argAwbBooking']['ShipperAddress1'] = $seller_info['address'];
            $operation_data['argAwbBooking']['ShipperAddress2'] = '';
            $operation_data['argAwbBooking']['ShipperPhone'] = $seller_info['telephone'];
            //$operation_data['argAwbBooking']['ShipperPin'] = '';
            $operation_data['argAwbBooking']['ShipperCity'] = $seller_info['zone_name'];
            $operation_data['argAwbBooking']['ShipperCountry'] = $seller_info['country_name'];
            $sEmails = FirstFlight_BOOKING_EMAILS;
            $sCC_Emails = FirstFlight_BOOKING_CC_EMAILS;

            $this->shippingCompany = new FirstFlight($this->registry, SHIPPING_FIRST_FLIGHT_ID);
        } elseif ($shipping_company == 2) { //NAQEL SERVICES
            $product_category = $this->getProductCategory($product_id);
            $address = $seller_info['address'];
            if((!isset($seller_info['address']) || empty($seller_info['address'])) && isset($seller_info['warehouse_address'])){
                $address = $seller_info['warehouse_address'];
            }
            $operation_data['shipperPhone'] = $seller_info['telephone'];
            $operation_data['shipperAddress1'] = $address;//$seller_info['address'];
            $operation_data['shipperCountryId'] = $seller_info['country_id'];
            $operation_data['shipperZoneId'] = $seller_info['zone_id'];
            $operation_data['shipperName'] = $seller_info['firstname'].' '.$seller_info['lastname'].'/'.$seller_info['companyname'];
            $operation_data['ShipperEmail'] = $seller_info['email'];

            $operation_data['consignee'] = 'Hasnain rizvi & Irshad Hussain - Naqel Express Fulfillment Warehouse';//$order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname'];
            $operation_data['consigneePhone'] = '966591896520';//$order_info['shipping_telephone'];
            $operation_data['consigneeAddress'] = 'Naqel Warehouse – As Sulay
                                                   Riyadh 14324
                                                   Mr. Hasnain Rizvi / Mr Irshad
                                                   Mobile: 966599954098';//$order_info['shipping_address_1'] . ' ' . $order_info['shipping_address_2'];
            $operation_data['consigneeCountryId'] = '184';//$order_info['shipping_country_id'];
            $operation_data['consigneeZoneId'] = '2879';//$order_info['shipping_zone_id'];

            //$country_info = $this->getCountryINFO( $order_info['shipping_country_id']);

            $operation_data['cod_amount'] = 0;
            $operation_data['pieces'] = 1;//$qty['pieces'];
            $operation_data['weight'] = '0.01';
            $operation_data['RefNo'] = "{$country_info['iso_code_2']}-{$this->convertOrderNumber($order_id)}-{$seller_id}-{$qty['pieces']}";
            $operation_data['GoodDesc'] = $product_category;




            $real['shipperPhone'] = '8004330033';
            $real['shipperAddress1'] = 'Sayidaty Mall
                                            Naveen Desouza
                                            DMC - Dubai UAE
                                            800 433 0033';
            $real['shipperCountryId'] = '184';
            $real['shipperZoneId'] = '2879';
            $real['shipperName'] = 'Sayidaty Mall';
            $real['ShipperEmail'] = 'mall@sayidaty.net';

            $real['consignee'] = $order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname'];
            $real['consigneePhone'] = $order_info['shipping_telephone'];
            $real['consigneeAddress'] = $order_info['shipping_address_1'] . ' ' . $order_info['shipping_address_2'];
            $real['consigneeCountryId'] = $order_info['shipping_country_id'];
            $real['consigneeZoneId'] = $order_info['shipping_zone_id'];
            $real['cod_amount'] = $cod_amount;
            $operation_data['real_consigne'] = $real;
            $sEmails = NaqelExpress_BOOKING_EMAILS;
            $sCC_Emails = NaqelExpress_BOOKING_CC_EMAILS;
            $this->shippingCompany = new Naqel($this->registry, SHIPPING_NAQEL_ID);


        } elseif ($shipping_company == 3) { //samsa SERVICES

            $this->shippingCompany = new Samsashipping($this->registry, SHIPPING_SMSA_ID);
            $date = date("Y-m-d H:i:s");
            $sEmails = SMSA_EMAILS;
            $operation_data = [
                'refNo' => "{$this->convertOrderNumber($order_id)}",
                'sentDate' => $date,
                'idNo' => 0,//Optional
                'cName' => $order_info['shipping_firstname'],
                'cntry' => $country_info['name'],
                'cCity' => $zone_info['name'],
                'cZip' => 0,//Optional
                'cPOBox' => 0,//Optional
                'cMobile' => $order_info['shipping_telephone'],
                'cTel1' => 0,//Optional
                'cTel2' => 0,//Optional
                'cAddr1' => $order_info['shipping_address_1'],
                'cAddr2' => $order_info['shipping_address_2'],
                'shipType' => 'VAL',
                'PCs' => $qty['pieces'],
                'cEmail' => '',
                'carrValue' => 0,//Optional
                'carrCurr' => 0,//Optional
                'codAmt' => $cod_amount,
                'weight' => 0,
                'custVal' => 0,//Optional
                'custCurr' => 0,//Optional
                'insrAmt' => 0,//Optional
                'insrCurr' => 0,//Optional
                'itemDesc' => 0,//Optional
                'sName' => $seller_info['companyname'],
                'sContact' => $seller_info['screenname'],
                'sAddr1' => $seller_info['address'],
                'sAddr2' => '',//Optional
                'sCity' => $seller_info['zone_name'],
                'sPhone' => $seller_info['telephone'],
                'sCntry' => $seller_info['country_name'],
                'prefDelvDate' => 0,//Optional
                'gpsPoints' => 0,//Optional
            ];

        }else if($shipping_company == 4){
            $this->shippingCompany = new Postplus($this->registry, SHIPPING_POST_PLUS_ID);
            $sEmails = PostaPlus_EMAILS;
            if((!isset($seller_info['address']) || empty($seller_info['address'])) && isset($seller_info['warehouse_address'])){
                $seller_info['address'] = $seller_info['warehouse_address'];
            }
            $operation_data = [
                'FromCompany'=>$seller_info['companyname'],
                'FromName'=>  $seller_info['firstname'].' '.$seller_info['lastname'].' - '.$seller_info['companyname'],
                'FromAddress'=>  $seller_info['address'],
                'FromEmail'=>$seller_info['email'],
                'FromArea'=> '',
                'FromCity'=>  $seller_info['zone_id'],
                'FromCodeCountry'=>$seller_info['country_id'],
                'FromMobile'=>$seller_info['telephone'],


                'ToName'=> 'PostaPlus DXB',//$order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname'],
                'ToAddress'=>'Sayidaty Mall Fulfilment team
                                Rose Marie Baro
                                Order Ref
                                Umm Ramool St 4 WH 2',//$order_info['shipping_address_1'] . ' ' . $order_info['shipping_address_2'],
                'ToCodeCountry'=> 221,//$order_info['shipping_country_id'],
                'ToCity'=>3510,//$order_info['shipping_zone_id'],
                'ToMobile'=> '9717018600',//$order_info['shipping_telephone'],


                'Reference1'=>"{$country_info['iso_code_2']}-{$this->convertOrderNumber($order_id)}-{$seller_id}-{$qty['pieces']}",//"{$order_id}-{$num_of_sellers}",
                'Quantity'=>$qty['pieces'],

                'Email1'=>$seller_info['email'],
                'Email2'=>'naveen.desouza@sayidaty.net',//$order_info['email'],

                'CashOnDelivery'=>0,//$cod_amount,
                'CashOnDeliveryCurrency'=>$order_info['currency_code']
            ];



            $real_consigne = [
                'FromName'=>  'Sayidaty Mall',//$seller_info['companyname'],
                'FromAddress'=>  'Sayidaty Mall Fulfilment team
                                Rose Marie Baro
                                Order Ref
                                Umm Ramool St 4 WH 2',//$seller_info['address'],
                'FromArea'=> '',
                'FromCity'=>  3510,//$seller_info['zone_id'],
                'FromCodeCountry'=>221,//$seller_info['country_id'],
                'FromMobile'=>'9717018600',//$seller_info['telephone'],


                'ToName'=> $order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname'],
                'ToAddress'=>$order_info['shipping_address_1'] . ' ' . $order_info['shipping_address_2'],
                'ToCodeCountry'=> $order_info['shipping_country_id'],
                'ToCity'=>$order_info['shipping_zone_id'],
                'ToMobile'=> $order_info['shipping_telephone'],


               // 'Reference1'=>"{$country_info['iso_code_2']}-{$order_id}-{$seller_id}-{$qty['pieces']}",//"{$order_id}-{$num_of_sellers}",
                'Quantity'=>1,//$qty['pieces'],

                'Email1'=>'mall@sayidaty.net',//$seller_info['email'],
                'Email2'=>$order_info['email'],

                'CashOnDelivery'=>$cod_amount,
                'CashOnDeliveryCurrency'=>$order_info['currency_code']
            ];

            $operation_data['real_consigne'] = $real_consigne;


            /*
            $operation_data['shipperPhone'] = $seller_info['telephone'];
            $operation_data['shipperAddress1'] = $seller_info['address'];
            $operation_data['shipperCountryId'] = $seller_info['country_id'];
            $operation_data['shipperZoneId'] = $seller_info['zone_id'];
            $operation_data['shipperName'] = $seller_info['companyname'];
            $operation_data['ShipperEmail'] = $seller_info['email'];

            $operation_data['consignee'] = $order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname'];
            $operation_data['consigneePhone'] = $order_info['shipping_telephone'];
            $operation_data['consigneeAddress'] = $order_info['shipping_address_1'] . ' ' . $order_info['shipping_address_2'];
            $operation_data['consigneeCountryId'] = $order_info['shipping_country_id'];
            $operation_data['consigneeZoneId'] = $order_info['shipping_zone_id'];

            $operation_data['cod_amount'] = $cod_amount;
            $operation_data['pieces'] = $qty['pieces'];
            $operation_data['weight'] = '0.01';
            $operation_data['RefNo'] = "{$order_id}/{$num_of_sellers}";
            */


        }

        $booking_response = $this->shippingCompany->BookingRequest($operation_data);// added temporarly

        if ($booking_response) {
            $aOrder = array();
            $aData = array();
            $data['shipping_company'] = $shipping_company;
            $data['order_id'] = $order_id;
            $data['seller_id'] = $seller_info['customer_id'];
            $data['country_id'] = $seller_info['country_id'];
            $data['sent_data'] = serialize($operation_data);
            $data['AWB'] = "";
            if ($data['shipping_company'] == 2) {
                $data['AWB'] = $booking_response['BookRef'];
                $data['book_response'] = $booking_response['BookAWB'];
                $aOrder['booking_ref'] = $booking_response['BookRef'];
                $aOrder['awb_ref'] = $booking_response['BookAWB'];
            } else {
                $data['book_response'] = $booking_response;
                $aOrder['booking_ref'] = $booking_response;
                $aOrder['awb_ref'] = '';
            }

            $sMessage = 'Dear Team : You have a new order to pickup for Sayidaty Mall';
            $aOrder['order_date'] = $order_info['date_added'];
            $aOrder['order_no'] = $this->convertOrderNumber($order_info['order_id']);
            $aOrder['company_name'] = $seller_info['companyname'];
            $aOrder['quantity'] = $qty['quantity'];
            $aOrder['city'] = $this->db->query("SELECT `name` FROM zone_description WHERE zone_id = " . $order_info['shipping_zone_id'] . " AND language_id = 1")->row['name'];
            $aOrder['status'] = $this->db->query("SELECT name FROM order_status WHERE order_status_id = " . $order_info['order_status_id'] . " ;")->row['name'];
            $aData[] = $aOrder;
            $sEmailMessage = $this->email_templates->addNewBookingEmailAlert($aData,$sMessage);

            $aEmailData = array();
            $aEmailData['to'] = $sEmails;
            if (trim($sCC_Emails) != '') {
                $aEmailData['cc'] = $sCC_Emails;
            }
            $aEmailData['message'] = $sEmailMessage;
            $aEmailData['order_date'] = $order_info['date_added'];
            $aEmailData['order_no'] = $this->convertOrderNumber($order_info['order_id']);
            $this->sendEmail($aEmailData);

            $this->add_booking($data);

        } else {
            echo 'invalid info' . PHP_EOL;
        }

    }

    public function getProductCategory($product_id) {
        $result = $this->db->query("SELECT CD.name FROM " . DB_PREFIX . "product_to_category p2c INNER JOIN category_description CD ON p2c.category_id=CD.category_id WHERE p2c.product_id = '" . (int) $product_id . "' AND CD.language_id=1 ")->row;
        if ($result) {
            return $result['name'];
        } else {
            return false;
        }
    }

    public function getOrder($order_id)
    {
        $order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'  AND deleted='0' AND order_status_id > '0'");


        if ($order_query->num_rows) {
            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

            if ($country_query->num_rows) {
                $payment_iso_code_2 = $country_query->row['iso_code_2'];
                $payment_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $payment_iso_code_2 = '';
                $payment_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $payment_zone_code = $zone_query->row['code'];
            } else {
                $payment_zone_code = '';
            }

            $country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

            if ($country_query->num_rows) {
                $shipping_iso_code_2 = $country_query->row['iso_code_2'];
                $shipping_iso_code_3 = $country_query->row['iso_code_3'];
            } else {
                $shipping_iso_code_2 = '';
                $shipping_iso_code_3 = '';
            }

            $zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

            if ($zone_query->num_rows) {
                $shipping_zone_code = $zone_query->row['code'];
            } else {
                $shipping_zone_code = '';
            }

            return array(
                'order_id' => $order_query->row['order_id'],
                'invoice_no' => $order_query->row['invoice_no'],
                'invoice_prefix' => $order_query->row['invoice_prefix'],
                'store_id' => $order_query->row['store_id'],
                'store_name' => $order_query->row['store_name'],
                'store_url' => $order_query->row['store_url'],
                'customer_id' => $order_query->row['customer_id'],
                'firstname' => $order_query->row['firstname'],
                'lastname' => $order_query->row['lastname'],
                'telephone' => $order_query->row['telephone'],
                'fax' => $order_query->row['fax'],
                'email' => $order_query->row['email'],
                'payment_firstname' => $order_query->row['payment_firstname'],
                'payment_lastname' => $order_query->row['payment_lastname'],
                'payment_company' => $order_query->row['payment_company'],
                'payment_address_1' => $order_query->row['payment_address_1'],
                'payment_address_2' => $order_query->row['payment_address_2'],
                'payment_postcode' => $order_query->row['payment_postcode'],
                'payment_city' => $order_query->row['payment_city'],
                'payment_zone_id' => $order_query->row['payment_zone_id'],
                'payment_zone' => $order_query->row['payment_zone'],
                'payment_zone_code' => $payment_zone_code,
                'payment_country_id' => $order_query->row['payment_country_id'],
                'payment_country' => $order_query->row['payment_country'],
                'payment_iso_code_2' => $payment_iso_code_2,
                'payment_iso_code_3' => $payment_iso_code_3,
                'payment_address_format' => $order_query->row['payment_address_format'],
                'payment_method' => $order_query->row['payment_method'],
                'payment_code' => $order_query->row['payment_code'],
                'shipping_firstname' => $order_query->row['shipping_firstname'],
                'shipping_lastname' => $order_query->row['shipping_lastname'],
                'shipping_company' => $order_query->row['shipping_company'],
                'shipping_address_1' => $order_query->row['shipping_address_1'],
                'shipping_address_2' => $order_query->row['shipping_address_2'],
                'shipping_postcode' => $order_query->row['shipping_postcode'],
                'shipping_telephone' => $order_query->row['shipping_telephone'],
                'shipping_city' => $order_query->row['shipping_city'],
                'shipping_zone_id' => $order_query->row['shipping_zone_id'],
                'shipping_zone' => $order_query->row['shipping_zone'],
                'shipping_zone_code' => $shipping_zone_code,
                'shipping_country_id' => $order_query->row['shipping_country_id'],
                'shipping_country' => $order_query->row['shipping_country'],
                'shipping_iso_code_2' => $shipping_iso_code_2,
                'shipping_iso_code_3' => $shipping_iso_code_3,
                'shipping_address_format' => $order_query->row['shipping_address_format'],
                'shipping_method' => $order_query->row['shipping_method'],
                'comment' => $order_query->row['comment'],
                'total' => $order_query->row['total'],
                'order_status_id' => $order_query->row['order_status_id'],
                'language_id' => $order_query->row['language_id'],
                'currency_id' => $order_query->row['currency_id'],
                'currency_code' => $order_query->row['currency_code'],
                'currency_value' => $order_query->row['currency_value'],
                'date_modified' => $order_query->row['date_modified'],
                'date_added' => $order_query->row['date_added'],
                'ip' => $order_query->row['ip']
            );

        } else {
            return false;
        }
    }

    private function getOrderCountries($order_id)
    {

        $countries = array();

        $result = $this->db->query("SELECT cp.country_id FROM customerpartner_to_order cp2o LEFT JOIN customerpartner_to_customer cp ON(cp2o.customer_id=cp.customer_id) WHERE cp2o.order_id='" . (int)$order_id . "'  AND cp2o.deleted='0'  AND cp2o.order_product_status NOT IN (7) ");

        foreach ($result->rows as $row) {
            $countries[$row['country_id']] = $row['country_id'];
        }

        return $countries;
    }

    public function total()
    {

        /* foreach ($totals as $total) {
             if ($total['code'] == 'sub_total') {
                 $total_data[] = array(
                     'title' => $total['title'],
                     'text' => $this->currency->format($sub_total_country_price, $order_info['currency_code'], $order_info['currency_value']),
                 );

                 $original_sub_total = $total['value'];
                 $total_value += $sub_total_country_price;
             } elseif ($total['code'] == 'shipping') {
                 $total_data[] = array(
                     'title' => $total['title'],
                     'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                 );
                 $total_value += $total['value'] / $order_countries;
             } elseif ($total['code'] == 'coupon') {
                 $total_data[] = array(
                     'title' => $total['title'],
                     'text' => $this->currency->format($total['value'] * ($sub_total_country_price / $original_sub_total), $order_info['currency_code'], $order_info['currency_value']),
                 );
                 $total_value += $total['value'] * ($sub_total_country_price / $original_sub_total);
             } elseif ($total['code'] == 'total') {
                 $total_data[] = array(
                     'title' => $total['title'],
                     'text' => $this->currency->format($total_value, $order_info['currency_code'], $order_info['currency_value']),
                 );
             } else {
                 $total_data[] = array(
                     'title' => $total['title'],
                     'text' => $this->currency->format($total['value'] / $order_countries, $order_info['currency_code'], $order_info['currency_value']),
                 );
                 $total_value += $total['value'] / $order_countries;
             }
         }*/
    }

    public function convertOrderNumber($order_id,$date_added=''){
      // the new order numbers will be in the form of  yy-mm-order_id-dd
      if($date_added != ''){
        return date('y', strtotime($date_added)).date('m', strtotime($date_added)).$order_id.date('d', strtotime($date_added));
      }
      $query = $this->db->query("SELECT date_added FROM `order` WHERE order_id='".(int) $order_id."'");
      if($query->num_rows){
        return date('y', strtotime($query->row['date_added'])).date('m', strtotime($query->row['date_added'])).$order_id.date('d', strtotime($query->row['date_added']));
      }

    }

    private function getSellerQuantity($order_id, $seller_id)
    {
        $result = array();

        $data = $this->db->query("SELECT product_id, quantity, order_product_status FROM customerpartner_to_order WHERE order_id='" . (int)$order_id . "' AND customer_id='" . (int)$seller_id . "'  AND deleted='0'  ");

        $result['quantity'] = 0;
        $result['pieces'] = 0;

        if ($data->num_rows) {
            foreach ($data->rows as $row) {
                //   if ($row['order_product_status'] == 17) {
                $result['quantity'] = $result['quantity'] + 1;
                $result['pieces'] = $result['pieces'] + $row['quantity'];
                // }
            }
        }

        return $result;
    }

    public function check_shipment($order_id)
    {
        $query = $this->db->query("SELECT count(*) as count FROM order_shipping WHERE order_id='" . (int)$order_id . "'");

        return $query->row['count'];
    }

    public function get_FFCitycode_code($zone_id) {
        $query = $this->db->query("SELECT firstflight_destination_code FROM `zone` where zone_id='" . $zone_id . "' ");
        if ($query->num_rows) {
            return $query->row['firstflight_destination_code'];
        }

    }

    public function add_shipment($data = array())
    {
        $sql = "INSERT INTO order_shipping SET ";

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $sql .= "$key = '$value' , ";
            }
            $sql .= "date_added = NOW() , date_modified= NOW()";

            $this->db->query($sql);
        }
    }

    public function getProfile($customer_id)
    {
        $query = $this->db->query("SELECT c2c.*,c2cd.* FROM " . DB_PREFIX . "customerpartner_to_customer c2c LEFT JOIN customerpartner_to_customer_description c2cd ON(c2c.customer_id=c2cd.customer_id) where c2c.customer_id = '" . $customer_id . "' and c2cd.language_id = 1 ");
        $data = $query->row;
        $country = $this->getCountryINFO($data['country_id']);
        $zone = $this->getZoneINFO($data['zone_id']);

        $data['zone_name'] = $zone['name'];
        $data['country_name'] = $country['name'];
        return $data;
    }

    public function getCountryINFO($country_id)
    {

        $query = $this->db->query("SELECT name, iso_code_2, telecode FROM country c LEFT JOIN country_description cd ON(c.country_id=cd.country_id) WHERE cd.language_id = 1 AND c.country_id = '" . (int)$country_id . "' ");
        return $query->row;
    }

    public function getZoneINFO($zone_id)
    {

        $query = $this->db->query("SELECT * FROM zone z LEFT JOIN zone_description zd ON(z.zone_id=zd.zone_id) WHERE zd.language_id = 1 AND z.zone_id = '" . (int)$zone_id . "' ");

        return $query->row;
    }

    public function add_booking($data = array())
    {
        if (isset($data['book_response'])) {

            $sql = "INSERT INTO order_booking SET seller_id = '" . (int)$data['seller_id'] . "' , order_id = '" . (int)$data['order_id'] . "', country_id='" . (int)$data['country_id'] . "' , book_response='" . $this->db->escape($data['book_response']) . "', sent_data='" . $this->db->escape($data['sent_data']) . "', is_closed=0 , shipping_company='" . (int)$data['shipping_company'] . "',date_added = NOW() , date_modified= NOW(),AWBNO='" . $this->db->escape($data['AWB']) . "' ";

            $this->db->query($sql);
        }
    }

    public function getOrderProducts($order_id)
    {

        $query = $this->db->query("SELECT op.order_product_id, op.name, op.model, op.quantity, op.price, op.total, p.sku, p.product_id, pd.name FROM " . DB_PREFIX . "order_product op LEFT JOIN product p ON (op.product_id=p.product_id) LEFT JOIN product_description pd ON(op.product_id=pd.product_id AND pd.language_id='1' ) WHERE order_id = '" . (int)$order_id . "'");
        return $query->rows;
    }

    public function getSellerOrderPrice($order_id, $seller_id)
    {

        $query = $this->db->query("SELECT SUM(price) as price FROM customerpartner_to_order WHERE order_id='" . (int)$order_id . "'  AND deleted='0'  AND customer_id='" . (int)$seller_id . "' AND order_product_status NOT IN (7) ");

        return (int)$query->row['price'];
    }


    public function getShippingCompany($fromId,$toId) {
        //return 4;
        $query =  $this->db->query("SELECT
                                  s.origin,
                                  f.country_id oid,
                                  s.destination,
                                  t.country_id tid,
                                  s.ship_company_id
                                FROM
                                  ship_company_to_country s
                                LEFT JOIN
                                  country f ON(s.origin = f.iso_code_2)
                                LEFT JOIN
                                  country t ON(s.destination = t.iso_code_2)
                                WHERE
                                  f.country_id = '$fromId' AND t.country_id = '$toId' ");

        return $query->row['ship_company_id'];
    }

    public function sendEmail($aEmailData)
    {

        parent::__construct();
        if (defined('SAFE_TO_SEND') && SAFE_TO_SEND) {
            $mailGun = new \Mailgun\Mailgun(MAILGUN_KEY);
            $sender = 'Sayidaty';
            $from = 'mall@sayidaty.com';
            $subject = 'New Booking for Sayidaty Order ' . $aEmailData['order_date'] . ' - ' . $aEmailData['order_no'];// + (Date) (Order #)
            $data = array();
            $data['from'] = "{$sender} <{$from}>";
            $data['to'] = $aEmailData['to'];
            //$data['cc'] = 'Logistcs@sayidatymall.net';
            $data['subject'] = $subject;
            $data['html'] = $aEmailData['message'];
            $result = $mailGun->sendMessage(MAILGUN_DOMAIN, $data);
            var_dump($result);
        }
    }

}


$booker = new generateBooking();
$orders = $booker->db->query("SELECT order_id FROM `order` WHERE order_status_id = 17 AND shipping_country_id <> 114 AND deleted='0'  ");

foreach ($orders->rows as $row) {
    $query = $booker->db->query("SELECT DISTINCT cp2c.customer_id, cp2c.country_id,cp2o.product_id FROM " . DB_PREFIX . "order_product op LEFT JOIN customerpartner_to_product cp2p ON (op.product_id = cp2p.product_id) LEFT JOIN customerpartner_to_customer cp2c ON (cp2p.customer_id = cp2c.customer_id) LEFT JOIN customerpartner_to_order cp2o ON (op.order_id = cp2o.order_id AND op.product_id = cp2o.product_id AND cp2o.order_product_status = 17) WHERE op.order_id = '" . (int) $row['order_id'] . "'  AND cp2o.deleted='0'  ");
    $num_of_sellers = $query->num_rows;
    foreach ($query->rows as $query_row) {
        $result = $booker->db->query("SELECT count(*) as count FROM order_booking WHERE order_id='" . (int) $row['order_id'] . "' AND country_id='" . (int) $query_row['country_id'] . "' AND seller_id='" . (int) $query_row['customer_id'] . "'");

          if ($result->row['count'] == 0 ) {

            $booker->book($row['order_id'], $query_row, $num_of_sellers);
        }

    }
}
