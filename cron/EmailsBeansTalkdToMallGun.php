<?php
require_once('Cron.php');
require 'vendor/autoload.php';

/**
 * Class EmailsBeansTalkdToMallGun
 */
class EmailsBeansTalkdToMallGun extends Cron
{

    /** @var ModelNeChunk $chunk */
    public $chunk;
    /** @var \Mailgun\Mailgun $mailGun */
    public $mailGun;
    /** @var ModelNeCampaign $campaign */
    public $campaign;



    function __construct()
    {

        exec("ps aux | grep -i EmailsBeansTalkdToMallGun |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();


        $this->loader->model('ne/chunk');
        $this->chunk = $this->registry->get('model_ne_chunk');


        $this->loader->model('ne/campaign');
        $this->campaign = $this->registry->get('model_ne_campaign');


        $this->mailGun = new \Mailgun\Mailgun(MAILGUN_KEY);


    }

    public function run()
    {
        $client = $this->getBeansTalkd();
        $client->watch(NEWSLETTER_TUBE);
        $closeTime = time() + 60 * 60 * 1;
        while (time() < $closeTime) {
            if (!isset($client)) {
                $client = $this->getBeansTalkd();
            }
            $job = $client->reserve(1);
            if ($job) {
                $client->delete($job['id']);
                $chunkId = $job['body'];
                $data = $this->chunk->getChunk($chunkId);
                if ($data) {
                    if ($data['status'] == 'INQUEUE' && $data['cstatus'] != 'PAUSED') {
                        if ($data['cstatus'] == "READY") {
                            $this->campaign->updateStatus($data['cid'], 'PROCESSING');
                        }
                        $this->chunk->updateStatus($chunkId, 'INPROGRESS');
                        $to = unserialize(base64_decode($data['to']));
                        $success = $fail = 0;
                        foreach ($to as $email => $info) {
                            if ($this->checkEmail($email)) {

                                $this->myEcho("sending email to " . $email);
                                if ($this->send($data['from_email'], $data['sender_name'], $data['subject'], $data['message'], $email, $info['firstname'], $info['lastname'],$info['uid'])) {
                                    $success++;
                                } else {
                                    $this->db->query("UPDATE " . DB_PREFIX . "ne_stats_personal SET `success` = '0' WHERE stats_personal_id = '" . (int) $info['uid'] . "'");
                                    $fail++;
                                }
                            } else {
                                $fail++;
                                $this->myEcho("invalid email " . $email);
                            }
                        }

                        $this->chunk->updateResult($chunkId, 'DONE', $success, $fail);
                    } else {
                        $this->myEcho("drop paused or done chunk " . $chunkId);
                    }
                } else {
                    $this->myEcho("cant get chunk info " . $chunkId);
                }

            } else {
                $this->myEcho("empty tube sleep 2 seconds");
                sleep(2);
            }


        }
        $this->myEcho('killed this old process to start a new one');
        $client->disconnect();

    }
    private function getBeansTalkd()
    {
        $beansTalkd = new Beanstalk(array('server1' => array(
            'host' => BEANSTALKD_SERVER,
            'port' => BEANSTALKD_SERVER_PORT,
            'weight' => 50,
            'timeout' => 10,
            // array of connections/tubes
            'connections' => array(),
        )));
        $client = $beansTalkd->getClient();
        return $client;
    }


    public function send($from, $sender, $subject, $message, $to, $firstname, $lastname,$userId = 0)
    {
        if (defined('SAFE_TO_SEND') && SAFE_TO_SEND) {

            $message = html_entity_decode($message,ENT_COMPAT, 'UTF-8');
            $message = str_replace(array('{key}', '%7Bkey%7D'), md5($this->config->get('ne_key') . $to), $message);
            $firstname = mb_convert_case($firstname, MB_CASE_TITLE, 'UTF-8');
            $lastname = mb_convert_case($lastname, MB_CASE_TITLE, 'UTF-8');

            $subject = str_replace(array('{name}', '{lastname}', '{email}'), array($firstname, $lastname, $to), $subject);
            $subject = str_replace(array('%7Bname}', '%7Blastname}', '%7Bemail}'), array($firstname, $lastname, $to), $subject);



            $uid = urlencode(base64_encode($to . '|' . $userId));
            $message = str_replace(array('{uid}', '%7Buid%7D'), $uid, $message);


            $message = str_replace(array('{name}', '{lastname}', '{email}'), array($firstname, $lastname, $to), $message);
            $message = str_replace(array('%7Bname%7D', '%7Blastname%7D', '%7Bemail%7D'), array($firstname, $lastname, $to), $message);


            $data = array();
            $data['from'] = "{$sender} <{$from}>";
            $data['to'] = $to;
            $data['subject'] = $subject;
            $data['text'] = "Email not support html";
            $data['html'] = $message;
            $result = $this->mailGun->sendMessage(MAILGUN_DOMAIN, $data);
            //print_r($result);

            return ($result && $result->http_response_code == 200);
        }else{
            return rand(0, 1);
        }
    }

    function checkEmail($to)
    {
        if ((utf8_strlen($to) > 96) || !filter_var($to, FILTER_VALIDATE_EMAIL)) {
            echo 'invalid email';
            $myfile = fopen("invalid_emails.txt", "a") or die("Unable to open file!");
            $txt = "$to\n";
            fwrite($myfile, $txt);
            fclose($myfile);
            return false;
        }else{
            return true;
        }
//        $vmail = new verifyEmail();
//        $vmail->setStreamTimeoutWait(20);
//        $vmail->Debug = TRUE;
//        $vmail->Debugoutput = 'html';
//        $vmail->setEmailFrom('support@sayidatymall.net');
//        return $vmail->check($email);
    }
}

$cron = new EmailsBeansTalkdToMallGun();
$cron->run();