<?php
require_once 'Cron.php';
require_once (DIR_SYSTEM .'phpexcel/Classes/PHPExcel.php');
define("SELLER_ID",24879);
define("FILE_PATH","files/Champion.xlsx");
/**
 * Created by PhpStorm.
 * User: bahaa
 * Date: 5/18/17
 * Time: 12:02 AM
 * @property Currency $currency
 */
class XlsToProduct extends Cron
{

    public function __construct()
    {
        exec("ps aux | grep -i XlsToProduct |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();
        $request = new Request();
        $this->registry->set('request', $request);
        $session = Session::getInstance();
        $this->registry->set('session', $session);
        $cache = Cache::getInstance(CONFIG_CACHE);
        $this->registry->set('cache', $cache);
        $this->registry->set('currency', new Currency($this->registry));


    }
    public function run(){


        $client = $this->getBeansTalkd();
        $client->useTube("products");

//       $closeTime = time() + 60*60*1;
//        while(time() < $closeTime){
//
//
//            $this->myEcho("sleep 5 seconds");
//            sleep(5);
//        }

        $objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/".FILE_PATH);

        $objWorksheet = $objPHPExcel->getActiveSheet();

        $i=2;
        foreach ($objWorksheet->getRowIterator() as $row) {
            if(empty($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue()))
                break;

            $product = new Product($this->registry);
            $product->seller_id = SELLER_ID;

            $product->sku = $objPHPExcel->getActiveSheet()->getCell("A$i")->getValue();
            if(null != $objPHPExcel->getActiveSheet()->getCell("A".($i+1))->getValue() && ($product->sku == $objPHPExcel->getActiveSheet()->getCell("A".($i+1))->getValue())){
              $i++;
              continue;
            }

            $product->en_title = $objPHPExcel->getActiveSheet()->getCell("D$i")->getValue();
            $product->ar_title = $objPHPExcel->getActiveSheet()->getCell("C$i")->getValue();
            $product->en_description = $objPHPExcel->getActiveSheet()->getCell("D$i")->getValue();
            $product->ar_description = $objPHPExcel->getActiveSheet()->getCell("E$i")->getValue();

            $product->category_id = $objPHPExcel->getActiveSheet()->getCell("F$i")->getValue();
            $product->attribute_id[] = $objPHPExcel->getActiveSheet()->getCell("L$i")->getValue();
            $product->attribute_id[] = $objPHPExcel->getActiveSheet()->getCell("M$i")->getValue();
            $product->brand_id = $objPHPExcel->getActiveSheet()->getCell("K$i")->getValue();
            $product->discounted_price = $objPHPExcel->getActiveSheet()->getCell("I$i")->getValue();
            $product->price = $objPHPExcel->getActiveSheet()->getCell("J$i")->getValue();
            $product->qty = $objPHPExcel->getActiveSheet()->getCell("G$i")->getValue();

            /*
            if($objPHPExcel->getActiveSheet()->getCell("P$i")->getValue())
                $product->images[] = $objPHPExcel->getActiveSheet()->getCell("P$i")->getValue();

            if($objPHPExcel->getActiveSheet()->getCell("Q$i")->getValue())
                $product->images[] = $objPHPExcel->getActiveSheet()->getCell("Q$i")->getValue();

            if($objPHPExcel->getActiveSheet()->getCell("R$i")->getValue())
                $product->images[] = $objPHPExcel->getActiveSheet()->getCell("R$i")->getValue();
              */

            $product->option_values[] = array(
              'id' => $objPHPExcel->getActiveSheet()->getCell("H$i")->getValue(),
              'qty' =>$objPHPExcel->getActiveSheet()->getCell("G".$i)->getValue()
            );
            $j = $i-1;

            if ((null != $objPHPExcel->getActiveSheet()->getCell("A".($j))->getValue()) && ($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue() == $objPHPExcel->getActiveSheet()->getCell("A".$j)->getValue())){
              $product->option_values[] = array(
                'id' =>$objPHPExcel->getActiveSheet()->getCell("H".$j)->getValue(),
                'qty' =>$objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue()
              );
              $product->qty += $objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue();
            }

            if ((null != $objPHPExcel->getActiveSheet()->getCell("A".(--$j))->getValue()) && ($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue() == $objPHPExcel->getActiveSheet()->getCell("A".$j)->getValue())){
              $product->option_values[] = array(
                'id' =>$objPHPExcel->getActiveSheet()->getCell("H".$j)->getValue(),
                'qty' =>$objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue()
              );
              $product->qty += $objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue();
            }

            if ((null != $objPHPExcel->getActiveSheet()->getCell("A".(--$j))->getValue()) && ($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue() == $objPHPExcel->getActiveSheet()->getCell("A".$j)->getValue())){
              $product->option_values[] = array(
                'id' =>$objPHPExcel->getActiveSheet()->getCell("H".$j)->getValue(),
                'qty' =>$objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue()
              );
              $product->qty += $objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue();
            }

            if ((null != $objPHPExcel->getActiveSheet()->getCell("A".(--$j))->getValue()) && ($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue() == $objPHPExcel->getActiveSheet()->getCell("A".$j)->getValue())){
              $product->option_values[] = array(
                'id' =>$objPHPExcel->getActiveSheet()->getCell("H".$j)->getValue(),
                'qty' =>$objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue()
              );
              $product->qty += $objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue();
            }
            if ((null != $objPHPExcel->getActiveSheet()->getCell("A".(--$j))->getValue()) && ($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue() == $objPHPExcel->getActiveSheet()->getCell("A".$j)->getValue())){
              $product->option_values[] = array(
                'id' =>$objPHPExcel->getActiveSheet()->getCell("H".$j)->getValue(),
                'qty' =>$objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue()
              );
              $product->qty += $objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue();
            }
            if ((null != $objPHPExcel->getActiveSheet()->getCell("A".(--$j))->getValue()) && ($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue() == $objPHPExcel->getActiveSheet()->getCell("A".$j)->getValue())){
              $product->option_values[] = array(
                'id' =>$objPHPExcel->getActiveSheet()->getCell("H".$j)->getValue(),
                'qty' =>$objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue()
              );
              $product->qty += $objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue();
            }

            try{
              if ((null != $objPHPExcel->getActiveSheet()->getCell("A".(--$j))->getValue()) && ($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue() == $objPHPExcel->getActiveSheet()->getCell("A".$j)->getValue())){
                $product->option_values[] = array(
                  'id' =>$objPHPExcel->getActiveSheet()->getCell("H".$j)->getValue(),
                  'qty' =>$objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue()
                );
                $product->qty += $objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue();
              }
              if ((null != $objPHPExcel->getActiveSheet()->getCell("A".(--$j))->getValue()) && ($objPHPExcel->getActiveSheet()->getCell("A$i")->getValue() == $objPHPExcel->getActiveSheet()->getCell("A".$j)->getValue())){
                $product->option_values[] = array(
                  'id' =>$objPHPExcel->getActiveSheet()->getCell("H".$j)->getValue(),
                  'qty' =>$objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue()
                );
                $product->qty += $objPHPExcel->getActiveSheet()->getCell("G".$j)->getValue();
              }
            }catch(exception $e){

            }




            if (!isset($client)) {
                $client = $this->getBeansTalkd();
            }
            $id = ($product->save());

            $client->put(0, 0, 0,$id);

            $product->updateStatus($id,"INQUEUE");

            $this->myEcho("Add To QUEUE : ".$id);
            $i++;
        }

    }

    private function getBeansTalkd()
    {
        $beansTalkd = new Beanstalk(array('server1' => array(
            'host' => BEANSTALKD_SERVER,
            'port' => BEANSTALKD_SERVER_PORT,
            'weight' => 50,
            'timeout' => 10,
            // array of connections/tubes
            'connections' => array(),
        )));
        $client = $beansTalkd->getClient();
        return $client;
    }



}

$cron = new XlsToProduct();
$cron->run();
