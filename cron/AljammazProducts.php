<?php
error_reporting(E_ALL);

require_once realpath(__DIR__) . '/Cron.php';
require_once realpath(__DIR__) .'/../system/phpexcel/Classes/PHPExcel.php';

class AljammazProducts extends Cron
{
    public function parseFile()
    {
        $fileName = date('d-m-Y');
        $fileExists = file_exists('/home/aljammaz/'.$fileName.'.xls');
        $effected_tables = array('product','customerpartner_to_product','product_option_value','product_special');
        if ($fileExists) {
            $objPHPExcel = new PHPExcel();
            $objReader = PHPExcel_IOFactory::createReaderForFile('/home/aljammaz/' . $fileName . '.xls');
            $objPHPExcel = PHPExcel_IOFactory::load('/home/aljammaz/' . $fileName . '.xls');
            $objPHPExcel->setActiveSheetIndex(0);
            foreach ($objPHPExcel->getActiveSheet()->getRowIterator(2) as $row) {
                $product_id = $sheet_size = $sheet_soh = $sheet_original_price = $sheet_discount = $sheet_selling_price = '';
                foreach ($row->getCellIterator() as $index => $cell) {
                    //Indices: 0=>STYLE, 1=>SIZE, 2=>SOH, 3=>ORIGINAL PRICE, 4=>DISC%, 5=>SELLING PRICE
                    if ($index == 0) {
                        $product = $this->db->query("SELECT product_id FROM product WHERE seller_id = 24596 AND sku = '".$cell->getValue()."' LIMIT 1");
                    }
                    if (isset($product->row['product_id'])) {
                        $product_id = $product->row['product_id'];

                        switch ($index) {
                            case 0:
                                $sheet_sku = $cell->getValue();
                                break;
                            case 1:
                                $sheet_size = $cell->getValue();
                                break;
                            case 2:
                                $sheet_soh = $cell->getValue();
                                break;
                            case 3:
                                $sheet_original_price = $cell->getValue();
                                break;
                            case 4:
                                $sheet_discount = $cell->getValue();
                                break;
                            case 5:
                                $sheet_selling_price = $cell->getValue();
                                break;
                        }
                    }

                }
                if (trim($product_id) != '') {
                    $this->archiveProduct($product_id,'before_update',$effected_tables);
                    echo "\n Product ID : $product_id:\n";
                    //Update Price
                    echo "\n Update original price : $sheet_original_price:\n";
                    $this->db->query("UPDATE product SET price = $sheet_original_price WHERE product_id = $product_id");

                    if ($sheet_discount > 0) {
                        $product_specials = $this->db->query("SELECT product_special_id,priority,date_start,date_end,CURDATE() as curdate FROM product_special WHERE product_id = $product_id ORDER BY priority");
                        $priority = 0;
                        if (count($product_specials->rows) > 1) {
                            $aSpecials = array();
                            foreach ($product_specials->rows as $record) {
                                if (($record['date_start'] <= $record['curdate'])
                                    && ($record['date_end'] >= $record['curdate'] || $record['date_end'] == '0000-00-00')
                                    && $record['priority'] >= $priority
                                ) {
                                    if (isset($aSpecials[$priority]) && $record['priority'] != $priority) {
                                        unset($aSpecials[$priority]);
                                    }
                                    $priority = $record['priority'];
                                    $aSpecials[$record['priority']][] = $record['product_special_id'];
                                }
                            }
                            $sSpecials = implode(',', $aSpecials[$priority]);
                            echo "\n Update special prices : $sheet_selling_price:\n";
                            $this->db->query("UPDATE product_special SET price = $sheet_selling_price WHERE product_special_id IN ($sSpecials)");

                        } else {
                            echo "\n Update special price : $sheet_selling_price:\n";
                            $this->db->query("UPDATE product_special SET price = $sheet_selling_price WHERE product_id = $product_id");
                        }

                    }

                    //Update Quantities
                    if ($sheet_size == 'FREE' || $sheet_size == '1SZ') {
                        echo "\n Update (FREE,1SZ) quantity : $sheet_soh:\n";
                        $this->db->query("UPDATE product SET quantity = $sheet_soh WHERE product_id = $product_id");
                        $this->db->query("UPDATE customerpartner_to_product SET quantity = $sheet_soh WHERE product_id = $product_id");
                    } else {

                        $sizes = $this->db->query("SELECT OS.option_value_id,OS.us,OS.uk,OS.eur,OS.xmls,OS.it 
                                  FROM option_to_size OS 
                                  JOIN product_option_value PO ON OS.option_value_id=PO.option_value_id 
                                  WHERE PO.product_id = $product_id");
                        if (count($sizes->rows > 0)) {
                            foreach ($sizes->rows as $size) {
                                if (in_array($sheet_size, $size)) {
                                    echo "\n Update $sheet_size quantity : $sheet_soh:\n";
                                    $this->db->query("UPDATE product_option_value SET quantity = $sheet_soh WHERE option_value_id = " . $size['option_value_id'] . "");
                                }

                            }
                            $this->updateWholeQuantity($product_id);
                        }

                    }
                    $this->archiveProduct($product_id,'after_update',$effected_tables);
                    $this->db->query("INSERT IGNORE INTO `algolia`(`type`, `type_id`) VALUES ('product',$product_id)");
                    //$aProducts[$product_id.'--'.$sku][$size][$soh] = ['original'=>$original_price,'discount'=>$discount,'selling'=>$selling_price];
                }
                echo "\n---------------------------------------------\n";
            }
        } else {
            echo "\n---------------------------------------------\n";
            echo "\nFile not exist\n";
            echo "\n---------------------------------------------\n";
        }
    }

    public function updateWholeQuantity($product_id)
    {
        $sum = $this->db->query("SELECT SUM(quantity) as sum FROM product_option_value WHERE product_id = $product_id")->row['sum'];
        if (!is_null($sum)) {
            echo "\n Update Whole quantity : $sum\n";
            $this->db->query("UPDATE product SET quantity = $sum WHERE product_id = $product_id");
            $this->db->query("UPDATE customerpartner_to_product SET quantity = $sum WHERE product_id = $product_id");
        }
    }

    public function archiveProduct($product_id,$status,$effected_tables){

        if( SAVE_PRODUCT_DATA == 'YES'){
            //task MDS-574, archiving the previous product data into mongoDB
            try{
                $m = new MongoClient(MONGO_DEFUALT_HOST);

                $db = $m->selectDB('product_old_data');
                $collection = new MongoCollection($db, 'product_old_data');
                $serialized_data = array('code' => 'product_'.$product_id."__".date("Y-m-d_h:i:sa"),'status'=>$status,'date_of_modification' => date("Y-m-d h:i:sa"),'user_id'=> '24596','user_ip' => '127.0.0.1','user_type'=>'seller_panel','product_id'=>$product_id);

                foreach($effected_tables as $key => $value){
                    $serialized_data['table_name'] = $value;
                    $product_data = $this->db->query("SELECT * FROM " . DB_PREFIX . $value." WHERE product_id = '" . (int) $product_id . "'");
                    $serialized_data['table_data'] =  '';
                    if(!empty($product_data->rows))
                        $serialized_data['table_data'] = $product_data->rows;
                    $collection->insert($serialized_data);
                    unset($serialized_data['_id']);

                }
            }catch(Exception $e){
                echo "\n ".$e->getMessage() ."\n";
            }
        }
        return false;
    }
}
$obj = new AljammazProducts();
$obj->parseFile();

