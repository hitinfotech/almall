<?php
require_once('Cron.php');

/**
 * Class EmailsCampaignToChunks
 */
class EmailsCampaignToChunks extends Cron
{

    /** @var ModelNeNewsletter $newsletter */
    public $newsletter;
    /** @var ModelNeCampaign $campaign */
    public $campaign;
    /** @var ModelNeChunk $chunk */
    public $chunk;

    function __construct()
    {
        exec("ps aux | grep -i EmailsCampaignToChunks |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();

        $this->loader->model('ne/campaign');
        $this->campaign = $this->registry->get('model_ne_campaign');


        $this->loader->model('ne/newsletter');
        $this->newsletter = $this->registry->get('model_ne_newsletter');

        $this->loader->model('ne/chunk');
        $this->chunk = $this->registry->get('model_ne_chunk');



        //  $this->readyToShare = $this->registry->get('model_ne_ready_to_share');
        //  $this->chunk = $this->registry->get('model_ne_chunk');
    }

    public function run()
    {
//        $startTime = time();
//        while (true) {

        $closeTime = time() + 60 * 60 * 1;
        while (time() < $closeTime) {
            $newCampaigns = $this->campaign->getCampaign('NEW');
            if ($newCampaigns) {
                foreach ($newCampaigns as $newCampaign) {
                    $this->campaign->updateStatus($newCampaign['id'], 'FETCHINGMAILS');
                    $total = 0;
                    if ($newCampaign['type'] == 'NEWSLETTER') {
                        $i = 1;
                        while (($emails = $this->getList($newCampaign, 2000, $i++))) {
                            $emailsChunks = array_chunk($emails, 200, true);
                            foreach ($emailsChunks as $emailChunk) {
                                $total += count($emailChunk);
                                $chunkID = $this->chunk->save($newCampaign['id'], $emailChunk);
                                $this->myEcho("campaign " . $newCampaign['id'] . " new chunk id " . $chunkID);

                            }
                        }

                        $this->newsletter->addHistoryQueue($newCampaign['newsletter_id'], array(
                            'queue' => $total,
                            'recipients' => $total
                        ));





                    } else {
                        // @TODO IF NOT NEWSLETTER
                    }
                    $this->campaign->updateStatus($newCampaign['id'], ($this->campaign->isAlreadyStart($newCampaign['id']) > 0) ? 'PROCESSING' : 'READY');
                }
            } else {
                $this->myEcho("no campaign sleep 2 seconds");
                sleep(2);
            }
        }
        $this->myEcho('killed this old process to start a new one');
    }


    public function getUID($newsletter_id,$email){
        if ($newsletter_id) {
            $this->db->query("INSERT IGNORE INTO " . DB_PREFIX . "ne_stats_personal SET history_id = '" . (int) $newsletter_id . "', email = '" . $this->db->escape($email) . "', views = '0'");
            $personal_id = $this->db->getLastId();

            if (!$personal_id) {
                $personal_id = $this->db->query("SELECT stats_personal_id FROM " . DB_PREFIX . "ne_stats_personal WHERE history_id = '" . (int) $newsletter_id . "' AND email = '" . $this->db->escape($email) . "'")->row;
                if ($personal_id) {
                    $personal_id = $personal_id['stats_personal_id'];
                }
            }

            return ($personal_id) ? $personal_id : 0;
        }else{
            return 0;
        }
    }
    public function getList($item, $iteration_amount, $page)
    {


        $start = ($page - 1) * $iteration_amount;
        $limit = $iteration_amount;
        $results = $results2 = [];



        //echo $item['visited_category'];

        $add_categories ="";
        $visit_and_pru = "";
        if (!empty($item['visited_category'])) {
            $item['visited_category'] = unserialize($item['visited_category']);
            $add_categories .= ' LEFT JOIN customer_views cvs ON (customer.customer_id = cvs.customer_id)
                                 LEFT JOIN product_to_category cat ON (cvs.product_id = cat.product_id) ';


        }
        if (!empty($item['purchased_category'])) {
            $item['purchased_category'] = unserialize($item['purchased_category']);
            $add_categories .= ' LEFT JOIN `order` orders ON (customer.customer_id = orders.customer_id AND orders.deleted="0")
                                 LEFT JOIN `order_product` orp ON (orders.order_id = orp.order_id AND orp.deleted="0")
                                 LEFT JOIN product_to_category cat2 ON (orp.product_id = cat2.product_id) ';

        }
        if (!empty($item['visited_category']) && !empty($item['purchased_category'])) {
            $ids = implode(',',$item['visited_category']);
            $ids1 = " cat.category_id IN ($ids) ";
            $ids = implode(',',$item['purchased_category']);
            $ids2 = " cat2.category_id IN ($ids) ";
            $visit_and_pru = "  ($ids1 OR $ids2) ";
        } else if (!empty($item['visited_category'])) {
            $ids = implode(',',$item['visited_category']);
            $ids1 = "  cat.category_id IN ($ids) ";
            $visit_and_pru = $ids1;
        }else if (!empty($item['purchased_category'])) {
            $ids = implode(',',$item['purchased_category']);
            $ids1 = "  cat2.category_id IN ($ids) ";
            $visit_and_pru = $ids1;
        }


















        if (in_array($item['to'], ['newsletter', 'customer_all', 'customer_group', 'customer'])) {
            $query = "select * from `customer` ".$add_categories." WHERE 1=1 ";
            if ($item['to'] == 'newsletter') {

                $query .= " AND customer.newsletter='1' ";


            } else if ($item['to'] == 'customer_all') {

                if (!empty($item['regency'])) {
                    $filter_regency = $item['regency'];
                } else {
                    $filter_regency = '0';
                }

                if (!empty($item['frequency'])) {
                    $filter_frequency = $item['frequency'];
                } else {
                    $filter_frequency = '0';
                }

                if (!empty($item['life_time_value'])) {
                    $filter_life_time_value = $item['life_time_value'];
                } else {
                    $filter_life_time_value = '0';
                }

                $query = "select customer.store_id,customer.customer_id,customer.email,customer.firstname,customer.lastname from `customer` ".$add_categories." ";

                if ($filter_regency != '0' || $filter_frequency != '0' ||  $filter_life_time_value != '0') {
                    $query .= " LEFT JOIN `" . DB_PREFIX . "order` orders_r ON (customer.customer_id = orders_r.customer_id  AND orders_r.deleted='0') ";
                    $group_by = ' group by customer.customer_id ';
                }
                $query .=" WHERE 1=1 ";


                if ($filter_regency != '0') {
                    if ($filter_regency == 1) {
                        $query .= ' AND ( orders_r.date_added > NOW() - INTERVAL 1 MONTH ) ';
                    } elseif ($filter_regency == 2) {
                        $query .= ' AND  ( orders_r.date_added <= NOW() - INTERVAL 1 MONTH ) AND  ( orders_r.date_added > NOW() - INTERVAL 3 MONTH ) ';
                    } elseif ($filter_regency == 3) {
                        $query .= ' AND  ( orders_r.date_added <= NOW() - INTERVAL 3 MONTH ) AND  ( orders_r.date_added > NOW() - INTERVAL 6 MONTH )';
                    } elseif ($filter_regency == 4) {
                        $query .= ' AND  ( orders_r.date_added < NOW() - INTERVAL 6 MONTH )';
                    }
                }


                if (isset($group_by) && $group_by != '') {
//                    $group_by .=$group_by;
                    $having = false;
                    if ($filter_frequency != '0') {

                        if ($filter_frequency == 1) {
                            $group_by .= ' HAVING count(orders_r.order_id) > 4 ';
                        } elseif ($filter_frequency == 2) {
                            $group_by .= ' HAVING count(orders_r.order_id) >= 2 AND count(orders_r.order_id) <= 4 ';
                        } elseif ($filter_frequency == 3) {
                            $group_by .= ' HAVING count(orders_r.order_id) =1 ';
                        } elseif ($filter_frequency == 3) {
                            $group_by .= ' HAVING count(orders_r.order_id) =0 ';
                        }

                        $having = true;
                    }

                    if ($filter_life_time_value != '0') {
                        $having_word = ' HAVING ';
                        if ($having) {
                            $having_word = ' AND';
                        }
                        if ($filter_life_time_value == 1) {
                            $group_by .= $having_word . ' sum(orders_r.total) >= 10000 ';
                        } elseif ($filter_life_time_value == 2) {
                            $group_by .= $having_word . ' sum(orders_r.total) >= 5000 AND sum(orders_r.total) < 10000';
                        } elseif ($filter_life_time_value == 3) {
                            $group_by .= $having_word . ' sum(orders_r.total) >= 1000 AND sum(orders_r.total) < 5000';
                        } elseif ($filter_life_time_value == 3) {
                            $group_by .= $having_word . ' sum(orders_r.total) < 1000 ';
                        }
                    }
                }









            } else if ($item['to'] == 'customer_group') {
                $customer_group_id = $item['customer_group_id'];
                $query .= " AND customer.`customer_group_id`='$customer_group_id' ";
                if ($item['customer_group_only_subscribers'] == 1) {
                    $query .= " AND customer.newsletter='1' ";
                }
            } else if ($item['to'] == 'customer') {
                $customers = isset($item['customer']) ? unserialize($item['customer']) : [];
                if (count($customers) > 0) {
                    $customersIds = implode(",", $customers);
                    $query .= " AND customer.`customer_id` IN ($customersIds) ";
                } else {
                    return false;
                }

            }


            if (isset($item['gender']) && $item['gender'] == 1) {
                $query .= " AND customer.`gender`='female' ";
            } else if (isset($item['gender']) && $item['gender'] == 2) {
                $query .= " AND customer.`gender`='male' ";
            }


            if (isset($item['location']) && $item['location'] != 0) {
                $lo = (int)$item['location'];
                $query .= " AND customer.`country_id`='$lo' ";
            }

            if (isset($item['only_selected_language']) && $item['only_selected_language'] == 1) {
                $language_id = $item['language_id'];
                $query .= " AND customer.`language_id`= '$language_id' ";
            }

            if(!empty($visit_and_pru) && $visit_and_pru){
                $query .= " AND ".$visit_and_pru." ";
            }
            if(isset($group_by)){
                $query .= $group_by." LIMIT $start,$limit";
            }else{
                $query .= " ORDER BY customer.customer_id LIMIT $start,$limit";
            }
            //$this->myecho($query);
            $results = $this->db->query($query)->rows;
        } else if (in_array($item['to'], ['marketing', 'marketing_all'])) {
            $query = "SELECT * from `ne_marketing`  ".$add_categories."  WHERE  1=1 ";
            if ($item['to'] == 'marketing') {
                $query .= " AND `subscribed` = '1' ";
            }
            if(!empty($visit_and_pru)){
                $query .= " AND ".$visit_and_pru." ";
            }
            $query .= " ORDER BY marketing_id LIMIT $start,$limit";
            //$this->myecho($query);
            $results = $this->db->query($query)->rows;

        } else if ($item['to'] == 'subscriber') {

            //newslletter
            $query = "SELECT * from `ne_marketing` WHERE  1=1 ";
            $query .= " AND `subscribed` = '1' ";
            $query .= " ORDER BY marketing_id LIMIT $start,$limit";
            //$this->myecho($query);
            $results = $this->db->query($query)->rows;


            //customers
            $query = "select * from `customer`  ".$add_categories."  WHERE 1=1 ";
            if(!empty($visit_and_pru) && $visit_and_pru){
                $query .= " AND ".$visit_and_pru." ";
            }
            $query .= " AND customer.newsletter='1' ";

            if (isset($item['gender']) && $item['gender'] == 1) {
                $query .= " AND customer.`gender`='female' ";
            } else if (isset($item['gender']) && $item['gender'] == 2) {
                $query .= " AND customer.`gender`='male' ";
            }

            if (isset($item['location']) && $item['location'] != 0) {
                $lo = (int)$item['location'];
                $query .= " AND customer.`country_id`='$lo' ";
            }
            if (isset($item['only_selected_language']) && $item['only_selected_language'] == 1) {
                $language_id = $item['language_id'];
                $query .= " AND customer.`language_id`= '$language_id' ";
            }
            $query .= " ORDER BY customer.customer_id LIMIT $start,$limit";
            //$this->myecho($query);
            $results2 = $this->db->query($query)->rows;

        } else if ($item['to'] == 'newsletter_subscribers') {
            $query = "select * from `customer`  ".$add_categories." WHERE 1=1 ";

            if(!empty($visit_and_pru) && $visit_and_pru){
                $query .= " AND ".$visit_and_pru." ";
            }

            $query .= " AND customer.newsletter='1' ";

            if (isset($item['gender']) && $item['gender'] == 1) {
                $query .= " AND customer.`gender`='female' ";
            } else if (isset($item['gender']) && $item['gender'] == 2) {
                $query .= " AND customer.`gender`='male' ";
            }

            if (isset($item['location']) && $item['location'] != 0) {
                $lo = (int)$item['location'];
                $query .= " AND customer.`country_id`='$lo' ";
            }
            if (isset($item['only_selected_language']) && $item['only_selected_language'] == 1) {
                $language_id = $item['language_id'];
                $query .= " AND customer.`language_id`= '$language_id' ";
            }

            $query .= " AND customer.`pending`='1' AND newsletter='1' ";
            $query .= " ORDER BY customer_id LIMIT $start,$limit";
            //$this->myecho($query);
            $results = $this->db->query($query)->rows;

        } else if ($item['to'] == 'all') {
            //newslletter
            $query = "SELECT * from `ne_marketing` WHERE  1=1 ";
            $query .= " ORDER BY marketing_id LIMIT $start,$limit";
            //$this->myecho($query);
            $results = $this->db->query($query)->rows;


            //customers
            $query = "select * from `customer` ".$add_categories." WHERE 1=1 ";
            if(!empty($visit_and_pru) && $visit_and_pru){
                $query .= " AND ".$visit_and_pru." ";
            }


            if (isset($item['gender']) && $item['gender'] == 1) {
                $query .= " AND customer.`gender`='female' ";
            } else if (isset($item['gender']) && $item['gender'] == 2) {
                $query .= " AND customer.`gender`='male' ";
            }
            if (isset($item['location']) && $item['location'] != 0) {
                $lo = (int)$item['location'];
                $query .= " AND customer.`country_id`='$lo' ";
            }
            if (isset($item['only_selected_language']) && $item['only_selected_language'] == 1) {
                $language_id = $item['language_id'];
                $query .= " AND customer.`language_id`= '$language_id' ";
            }
            $query .= " ORDER BY customer.customer_id LIMIT $start,$limit";
            //$this->myecho($query);
            $results2 = $this->db->query($query)->rows;
        } else if ($item['to'] == 'product') {
            $implode = array();

            $products = unserialize($item['product']);
            if($products){
                $ids = implode(",",$products);
                $inq = " AND op.product_id IN ($ids) ";
            }else{
                $inq = "";
            }

//            foreach ($products as $product_id) {
//                $implode[] = "op.product_id = '" . $product_id . "'";
//            }




            $query = "SELECT
  *
FROM
  `order` o
LEFT JOIN
  order_product op ON(o.order_id = op.order_id)
LEFT JOIN
  ne_language_map lm ON(o.email = lm.c_email)
WHERE 1=1 ".$inq."  AND o.order_status_id <> '0' AND o.deleted='0' AND NOT EXISTS(
  SELECT
    1
  FROM
    ne_blacklist bl
  WHERE
    bl.email = o.email
)
GROUP BY
  o.email
LIMIT $start, $limit";


//            $query = ("SELECT * FROM `order` o LEFT JOIN order_product op ON (o.order_id = op.order_id)
//                                                                   LEFT JOIN ne_language_map lm ON (o.email = lm.c_email)
//                                                                   WHERE (" . implode(" OR ", $implode) . ")
//                                                                   AND o.order_status_id <> '0'
//                                                                   AND NOT EXISTS (SELECT 1 FROM ne_blacklist bl WHERE bl.email = o.email)
//                                                                    GROUP BY o.email LIMIT $start,$limit ");

            //$this->myecho($query);
            $results = $this->db->query($query)->rows;
        } else if (in_array($item['to'], ['rewards', 'rewards_all'])) {

            if ($item['to'] == 'rewards') {
                $query = "SELECT
  customer.customer_id,
  customer.firstname,
  customer.lastname,
  customer.email,
  customer.points,
  customer.store_id
FROM
  `customer`
INNER JOIN
  (
  SELECT
    customer_id,
    SUM(points) AS points
  FROM
    customer_reward
  GROUP BY
    customer_reward.customer_id
) AS cr ON cr.customer_id = customer.customer_id AND cr.points > '0'
LEFT JOIN
  ne_language_map lm ON customer.email = lm.c_email
WHERE
  customer.newsletter = '1' AND NOT EXISTS(
  SELECT
    1
  FROM
    ne_blacklist bl
  WHERE
    bl.email = customer.email
)";
            } else {
                $query = "SELECT
  customer.customer_id,
  customer.firstname,
  customer.lastname,
  customer.email,
  customer.points,
  customer.store_id
FROM
  `customer`
INNER JOIN
  (
  SELECT
    customer_id,
    SUM(points) AS points
  FROM
    customer_reward
  GROUP BY
    customer_reward.customer_id
) AS cr ON cr.customer_id = customer.customer_id AND cr.points > '0'
LEFT JOIN
  ne_language_map lm ON customer.email = lm.c_email
WHERE NOT EXISTS
  (
  SELECT
    1
  FROM
    ne_blacklist bl
  WHERE
    bl.email = customer.email
)";
            }

            if (isset($item['gender']) && $item['gender'] == 1) {
                $query .= " AND customer.gender='female' ";
            } else if (isset($item['gender']) && $item['gender'] == 2) {
                $query .= " AND customer.gender='male' ";
            }
            if (isset($item['location']) && $item['location'] != 0) {
                $lo = (int)$item['location'];
                $query .= " AND customer.country_id='$lo' ";
            }
            if (isset($item['only_selected_language']) && $item['only_selected_language'] == 1) {
                $language_id = $item['language_id'];
                $query .= " AND customer.language_id= '$language_id' ";
            }
            $query .= "  LIMIT $start,$limit";
            //$this->myecho($query);
            $resultsR = $this->db->query($query)->rows;
        }else if($item['to'] == 'affiliate_all'){

            $results = $this->newsletter->getAffiliates(array(
                'filter_gender' => isset($item['gender']) ? $item['gender'] : 0,
                'filter_location' => (isset($item['only_selected_language'])  && $item['only_selected_language'] == 1) ? $item['language_id'] : '',
                'start' => ($page - 1) * $iteration_amount,
                'limit' => $iteration_amount
            ));

        }else if($item['to'] == 'affiliate'){

            if (isset($item['affiliate']) && !empty($item['affiliate'])) {
                $item['affiliate'] = unserialize($item['affiliate']);
                foreach (($item['affiliate']) as $affiliate_id) {
                    $affiliate_info = $this->newsletter->getAffiliate($affiliate_id);

                    $results[] = [
                        'email' => $affiliate_info['email'],
                        'firstname'=> $affiliate_info['firstname'],
                        'lastname'=> $affiliate_info['lastname']
                    ];


                }
            }


        }







        $emails = [];
        if(isset($results) && is_array($results))
            foreach ($results as $result) {
                if ($result['store_id'] == $item['store_id']) {

                    $this->db->query("INSERT INTO " . DB_PREFIX . "ne_queue
                SET email = '" . $this->db->escape($result['email']) . "',
                 firstname = '" . $this->db->escape($result['firstname']) . "',
                  lastname = '" . $this->db->escape($result['lastname']) . "',
                   history_id = '" . (int) $item['newsletter_id'] . "'");


                    $emails[$result['email']] = array(
                        'firstname' => $result['firstname'],
                        'lastname' => $result['lastname'],
                        'uid' => $this->getUID($item['newsletter_id'],$result['email'])
                    );

                }
            }

        if(isset($results2) && is_array($results2))
            foreach ($results2 as $result) {
                if ($result['store_id'] == $item['store_id']) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "ne_queue
                SET email = '" . $this->db->escape($result['email']) . "',
                 firstname = '" . $this->db->escape($result['firstname']) . "',
                  lastname = '" . $this->db->escape($result['lastname']) . "',
                   history_id = '" . (int) $item['newsletter_id'] . "'");


                    $emails[$result['email']] = array(
                        'firstname' => $result['firstname'],
                        'lastname' => $result['lastname'],
                        'uid' => $this->getUID($item['newsletter_id'],$result['email'])
                    );
                }
            }

        if (isset($resultsR)  && is_array($resultsR)) {
            foreach ($resultsR as $result) {
                if ($result['store_id'] == $item['store_id']) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "ne_queue
                SET email = '" . $this->db->escape($result['email']) . "',
                 firstname = '" . $this->db->escape($result['firstname']) . "',
                  lastname = '" . $this->db->escape($result['lastname']) . "',
                   history_id = '" . (int) $item['newsletter_id'] . "'");


                    $emails[$result['email']] = array(
                        'firstname' => $result['firstname'],
                        'lastname' => $result['lastname'],
                        'reward' => $result['points'],
                        'uid' => $this->getUID($item['newsletter_id'],$result['email'])
                    );
                }
            }
        }





        if (count($emails) > 0) {


            return $emails;
        } else {
            return false;
        }

    }
}


$cron = new EmailsCampaignToChunks();
$cron->run();
