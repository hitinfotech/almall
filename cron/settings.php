<?php

$_SERVER['HTTP_HOST'] = '';

switch (SERV) {
    case 'LOCAL':
        date_default_timezone_set('Asia/Amman');
        define('ENVIROMENT', 'local');
        break;
    case 'DEVELOPMENT':
        date_default_timezone_set('Asia/Riyadh');
        define('ENVIROMENT', 'development');
        break;
    case 'AMAZON':
        date_default_timezone_set('Asia/Riyadh');
        define('ENVIROMENT', 'aws_development');
        break;
    default :
        die("\n\nDEFINE PROPER VALUE FROM SERVER : LOCAL, DEVELOPMENT, AMAZON\n\n");
}
require_once realpath(__DIR__) . '/../config/config.php';
require_once realpath(__DIR__) . '/db_define.php';

