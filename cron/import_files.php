<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

$db = new DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

// import brands
$brands = realpath(__DIR__) . "/files/brands.csv";
$look = realpath(__DIR__) . "/files/look.csv";
$look_title = realpath(__DIR__) . "/files/look_title.csv";
$look_title_desc = realpath(__DIR__) . "/files/look_titlt_desc.csv";
$tip_title = realpath(__DIR__) . "/files/tips_title.csv";
$tip_title2 = realpath(__DIR__) . "/files/tip_title2.csv";


// BRAND title & description
$file1 = fopen($brands, "r");
while (!feof($file1)) {
    $file = fgetcsv($file1);
    //print_r($file);


    $brand_id = (int) $file[0];
    $name_en = $file[2];
    $desc_en = $file[3];

    echo "\nbarns_id => {$brand_id}";

    $db->query("DELETE FROM brand_description WHERE brand_id = '" . (int) $brand_id . "' AND language_id = '1' ");
    $db->query("INSERT INTO brand_description SET name = '" . $db->escape($name_en) . "', description = '" . $db->escape($desc_en) . "', brand_id = '" . (int) $brand_id . "', language_id = '1' ");
}

// look title & description
/*$file2 = fopen($look, "r");
while (!feof($file2)) {
    $file = fgetcsv($file2);
    //print_r($file);

    $look_id = (int) $file[0];
    $title_en = $file[2];
    $desc_en = $file[3];

    echo "\n look =>{$look_id}";

    $db->query("DELETE FROM look_description WHERE look_id = '" . (int) $look_id . "' AND language_id = '1' ");
    $db->query("INSERT INTO look_description SET title = '" . $db->escape($title_en) . "', description = '" . $db->escape($desc_en) . "', look_id = '" . (int) $look_id . "', language_id = '1' ");
} */

// look titl
$file3 = fopen($look_title, "r");
while (!feof($file3)) {
    $file = fgetcsv($file3);
    //print_r($file);

    $look_id = (int) $file[0];
    $title_en = $file[1];

    echo "\nlook => {$look_id}";

    $db->query("UPDATE look_description SET title = '" . $db->escape($title_en) . "' WHERE look_id = '" . (int) $look_id . "'AND language_id = '1' ");
    $db->query("UPDATE look SET is_english = 1 WHERE look_id = '" . (int) $look_id . "' ");
    
}

// look title & description 2
$file4 = fopen($look_title_desc, "r");
while (!feof($file4)) {
    $file = fgetcsv($file4);
    //print_r($file);

    $look_id = (int) $file[0];
    $title_en = $file[1];
    $desc_en = $file[2];

    echo "\nlook => {$look_id}";

    $db->query("UPDATE look_description SET title = '" . $db->escape($title_en) . "', description = '" . $db->escape($desc_en) . "' WHERE look_id = '" . (int) $look_id . "'AND language_id = '1' ");
    $db->query("UPDATE look SET is_english = 1 WHERE look_id = '" . (int) $look_id . "' ");
}

// tip title
$file5 = fopen($tip_title, "r");
while (!feof($file5)) {
    $file = fgetcsv($file5);
    //print_r($file);

    $tip_id = (int) $file[0];
    $name_en = $file[1];

    echo "\ntip=>{$tip_id}";

    $db->query("UPDATE tip_description SET name = '" . $db->escape($name_en) . "'WHERE tip_id = '" . (int) $tip_id . "'AND language_id = '1' ");
    $db->query("UPDATE tip SET is_english = 1 WHERE tip_id = '" . (int) $tip_id . "' ");
}


$file6 = fopen($tip_title2, "r");
while (!feof($file6)) {
    $file = fgetcsv($file6);
    //print_r($file);

    $tip_id = (int) $file[0];
    $name_en = $file[1];

    echo "\ntip => {$tip_id}";

    $db->query("UPDATE tip_description SET name = '" . $db->escape($name_en) . "'WHERE tip_id = '" . (int) $tip_id . "'AND language_id = '1' ");
    $db->query("UPDATE tip SET is_english = 1 WHERE tip_id = '" . (int) $tip_id . "' ");

}

echo "\nfinished\n";