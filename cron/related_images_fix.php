<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';


if (!defined('DIR_APPLICATION')) {
    die("\nError Initialaize cron \n");
    exit;
}

die("here");
require_once(DIR_SYSTEM . 'startup.php');

// Registry
$registry = Registry::getInstance();

// Config
$config = Config::getInstance();
$registry->set('config', $config);

// Database
$db = DB::getInstance(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
$registry->set('db', $db);

$image_config = config_image::getInstance();
$registry->set('image_config', $image_config);

$config->set('config_store_id', 0);

$config->set('config_url', HTTP_SERVER);
$config->set('config_ssl', HTTPS_SERVER);

class DoFix {

    private $table_name;
    private $config_image;

    public function __construct($registry) {
        $this->config_image = $registry->get('image_config');

        $this->config = $registry->get('config');

        $this->db = $registry->get('db');

        $this->table_name = 'my_product_image';
    }

    public function Start() {

        $sql = "SELECT * FROM " . $this->table_name . " WHERE image NOT IN (SELECT image FROM product_image)";

        $images = $this->db->query($sql);

        foreach ($images->rows as $cron) {

            if (is_file(DIR_IMAGE . $cron['image'])) {
                echo $cron['image']."\n";
            } else {
                echo "not found > > > " . DIR_IMAGE . $cron['image']."\n";
            }
            
            $this->db->query("INSERT INTO product_image SET product_id ='".(int)$cron['product_id']."',image='".$cron['image']."',sort_order='".$cron['sort_order']."' ");
        }
    }

    function do_resize($results, $row, &$error, $db) {
        $count = count($results);
        foreach ($results as $result) {
            foreach ($row as $key => $row2) {
                if ($row2['width'] > 10) {
                    if ($result['image']) {
                        $this->resize($result['image'], $row2['width'], $row2['hieght'], $db);
                    } else {
                        $error[] = $result['image'];
                    }
                }
            }
            echo "\n" . $count--;
        }
    }

    function resize($filename, $width, $height, $db) {

        if (!is_file(DIR_IMAGE . $filename)) {
            echo ("\nNo file => " . DIR_IMAGE . $filename);
            return;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $old_image = $filename;

        $new_image = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
        if (!is_file(DIR_IMAGE . $new_image) || (filectime(DIR_IMAGE . $old_image) > filectime(DIR_IMAGE . $new_image))) {
            $path = '';

            $directories = explode('/', dirname(str_replace('../', '', $new_image)));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir(DIR_IMAGE . $path)) {
                    @mkdir(DIR_IMAGE . $path, 0777);
                }
            }

            list($width_orig, $height_orig) = getimagesize(DIR_IMAGE . $old_image);

            if ($width_orig != $width || $height_orig != $height) {

                $im = new imagick(DIR_IMAGE . $old_image);
                $imageprops = $im->getImageGeometry();
                $old_width = $imageprops['width'];
                $old_height = $imageprops['height'];
                if ($old_width > $old_height) {
                    $newHeight = $height;
                    $newWidth = ($height / $old_height) * $old_width;
                } else {
                    $newWidth = $width;
                    $newHeight = ($width / $old_width) * $old_height;
                }

                $im->resizeimage($newWidth, $newHeight, imagick::FILTER_LANCZOS, 1, true);
                $im->cropImage($newWidth, $newHeight, 0, 0);
                $im->writeImage(DIR_IMAGE . $new_image);
                $im->clear();
            } else {
                copy(DIR_IMAGE . $old_image, DIR_IMAGE . $new_image);
            }
        }


        $db->query("INSERT INTO image_s3 SET image='" . $db->escape($new_image) . "' , date_added=NOW(), user_id='4' ");
    }

}

$test = new DoFix($registry);
$test->Start();

die("\nFINISH SENDING\n");
