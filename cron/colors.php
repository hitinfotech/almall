<?php

echo $starttime = time() . "\n\n";

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

$errors = array();

$db = new DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, SAYIDATY);


//$result = $db->query("SELECT * FROM attribute a LEFT JOIN attribute_description ad ON(a.attribute_id = ad.attribute_id) WHERE ad.language_id = 2 ");

$x = realpath(__DIR__) . "/colors.csv";
$file1 = fopen($x, "r");
while (!feof($file1)) {
    $file = fgetcsv($file1);
    print_r($file);
    echo "\n\n";

    $att_id = (int) $file[2];
    $name_en = $file[0];
    $name_ar = $file[1];
    $related = $file[3];

    $db->query("UPDATE attribute_description SET name = '" . $db->escape($name_en) . "' WHERE attribute_id = '" . (int) $att_id . "' AND language_id = '1' ");
    $db->query("UPDATE attribute_description SET name = '" . $db->escape($name_ar) . "' WHERE attribute_id = '" . (int) $att_id . "' AND language_id = '2' ");
    $results = explode(",", $related);
    foreach ($results as $row) {
        $db->query("DELETE FROM attribute WHERE attribute_id = '" . (int) $row . "' ");
        $db->query("DELETE FROM attribute_description WHERE attribute_id = '" . (int) $row . "' ");
        $products = $db->query("SELECT product_id FROM product_attribute WHERE attribute_id = '" . (int) $att_id . "' ");
        foreach ($products->rows as $row2) {
            $db->query("DELETE FROM product_attribute WHERE product_id = '" . (int) $row2 . "' AND attribute_id = '" . (int) $row . "' ");
            
            $db->query("INSERT INTO product_attribute SET product_id = '" . (int) $row2 . "', attribute_id = '" . (int) $att_id . "', text='" . $db->escape($name_en) . "', language_id='1' ON DUPLICATE KEY UPDATE text='" . $db->escape($name_en) . "' ");
            $db->query("INSERT INTO product_attribute SET product_id = '" . (int) $row2 . "', attribute_id = '" . (int) $att_id . "', text='" . $db->escape($name_ar) . "', language_id='2' ON DUPLICATE KEY UPDATE text='" . $db->escape($name_ar) . "' ");
        }
    }
}