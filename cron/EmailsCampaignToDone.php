<?php
require_once 'Cron.php';
/**
 * Created by PhpStorm.
 * User: bahaa
 * Date: 5/18/17
 * Time: 12:02 AM
 */
class EmailsCampaignToDone extends Cron
{

    public function __construct()
    {
        exec("ps aux | grep -i EmailsCampaignToDone |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();


    }
    public function run(){
       $closeTime = time() + 60*60*1;
        while(time() < $closeTime){
           // $this->db->query("select * from")

            $this->db->query("UPDATE
  emails_campaign
SET
  emails_campaign.status = 'COMPLETED'
WHERE
  emails_campaign.id NOT IN(
  SELECT DISTINCT
    emails_chunk.emails_campaign_id
  FROM
    emails_chunk
  WHERE
    emails_chunk.status NOT IN('DONE')
) AND emails_campaign.status NOT IN(
  'NEW',
  'FETCHINGMAILS',
  'COMPLETED',
  'PAUSED',
  'SCHEDULED'
)");


            $this->db->query("update `emails_chunk` set `status`='DONE' where updated_at < (NOW() - 60*60*20) AND `status` = 'INPROGRESS'");

            $this->myEcho("sleep 5 seconds");


            sleep(5);
        }
    }

}
$cron = new EmailsCampaignToDone();
$cron->run();
