<?php

require_once 'Cron.php';

/**
 * Created by PhpStorm.
 * User: bahaa
 * Date: 5/18/17
 * Time: 12:02 AM
 */
class AbandomnetClear extends Cron {

    public function __construct() {
        exec("ps aux | grep -i AbandomnetClear |grep -v grep | wc -l", $pids);
        if ($pids[0] > 2) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();
    }

    public function run() {
        $this->db->query("delete from customer_abandonmentcarts  WHERE (customer_id = 0 AND date_added < (NOW() - INTERVAL 2 DAY)) OR  date_added < (NOW() - INTERVAL 8 DAY) ");
        $c = $this->db->countAffected();
        $this->myEcho($c . ". row deleted");
    }

}

$cron = new AbandomnetClear();
$cron->run();
