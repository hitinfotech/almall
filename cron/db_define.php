<?php

namespace DB;

/*
 * To Import all categories data from mall.sayidaty.net live.
 * Done by Qasem 
 * 
 */


class DB {

    private $cities;
    private $db;

    public function __construct($driver, $hostname, $username, $password, $database, $port = NULL) {
        $class = 'DB\\' . $driver;

        if (class_exists($class)) {
            $this->db = new $class($hostname, $username, $password, $database, $port);
        } else {
            exit('Error: Could not load database driver ' . $driver . '!');
        }
        
        $this->cities = array(
            1 => 2879,
            2 => 4232,
            3 => 4233,
            4 => 4234,
            5 => 4235,
            6 => 4236,
            7 => 4237,
            8 => 2877,
            9 => 2884,
            10 => 2878,
            11 => 4238,
            12 => 4239,
            13 => 4240,
            14 => 2886,
            15 => 2882,
            16 => 2883,
            17 => 2881,
            18 => 4241,
            19 => 3506,
            20 => 3510,
            21 => 3509,
            22 => 4242
        );
    }

    public function getCityId($id) {
        return (isset($this->cities[$id]) ? $this->cities[$id] : 0);
    }

    public function query($sql) {
        return $this->db->query($sql);
    }

    public function escape($value) {
        return $this->db->escape($value);
    }

    public function countAffected() {
        return $this->db->countAffected();
    }

    public function getLastId() {
        return $this->db->getLastId();
    }

}

final class MySQLi {

    private $link;

    public function __construct($hostname, $username, $password, $database, $port = '3306') {
        $this->link = new \mysqli($hostname, $username, $password, $database, $port);

        if ($this->link->connect_error) {
            trigger_error('Error: Could not make a database link (' . $this->link->connect_errno . ') ' . $this->link->connect_error);
            exit();
        }

        $this->link->set_charset("utf8");
        $this->link->query("SET SQL_MODE = ''");
    }

    public function query($sql) {
        $query = $this->link->query($sql);

        if (!$this->link->errno) {
            if ($query instanceof \mysqli_result) {
                $data = array();

                while ($row = $query->fetch_assoc()) {
                    $data[] = $row;
                }

                $result = new \stdClass();
                $result->num_rows = $query->num_rows;
                $result->row = isset($data[0]) ? $data[0] : array();
                $result->rows = $data;

                $query->close();

                return $result;
            } else {
                return true;
            }
        } else {
            trigger_error('Error: ' . $this->link->error . '<br />Error No: ' . $this->link->errno . '<br />' . $sql);
        }
    }

    public function escape($value = '') {
        if (is_array($value)) {
            if (!empty($value)) {
                foreach ($value as $row) {
                    return $this->link->real_escape_string($row);
                }
            } else {
                return '';
            }
        } else {

            if (isset($value) && strlen($value) > 0) {
                return $this->link->real_escape_string($value);
            } else {
                return '';
            }
        }
    }

    public function countAffected() {
        return $this->link->affected_rows;
    }

    public function getLastId() {
        return $this->link->insert_id;
    }

    public function __destruct() {
        $this->link->close();
    }

}
