<?php
require_once('Cron.php');

class RemoveUnusedImages extends Cron
{

    function __construct()
    {
        parent::__construct();
    }

    public function run(){
      foreach(scandir('image/Product') as $folder){
        if($folder != '.' && $folder !='..'){
          $this->check_folder($folder);
        }
      }
    }

    public function check_folder($folder_name){
      if (! is_file((__DIR__) .'/../image/Product/'.$folder_name)){
      $result = $this->db->query("SELECT count(*) as count FROM product WHERE image LIKE '".$this->db->escape("Product/".$folder_name)."%'");
      if($result->num_rows){
        if($result->row['count']){
          if (! is_file((__DIR__) .'/../image/Product/'.$folder_name)){
            foreach(scandir((__DIR__) .'/../image/Product/'.$folder_name) as $sub_folder){
              if($sub_folder != '.' && $sub_folder !='..'){
                $this->check_folder($folder_name."/".$sub_folder);
              }
            }
          }
        }else{
          $result2 = $this->db->query("SELECT count(*) as count FROM product_image WHERE image LIKE '".$this->db->escape("Product/".$folder_name)."%'");
          if($result2->num_rows){
            if(!$result2->row['count']){
              $folder_path = (__DIR__) . '/../image/Product/';
              chdir($folder_path);
              exec("rm -rf ".$folder_path.$folder_name);
              echo " folder ". (__DIR__) . '/../image/Product/'.$folder_name ." was removed"."\n";
            }else{
              if (! is_file((__DIR__) .'/../image/Product/'.$folder_name)){
                foreach(scandir( (__DIR__) .'/../image/Product/'.$folder_name) as $sub_folder){
                  if($sub_folder != '.' && $sub_folder !='..'){
                    $this->check_folder($folder_name."/".$sub_folder);
                  }
                }
              }
            }
          }
        }
      }
    }

    }
}

$image = new RemoveUnusedImages();
$image->run();
