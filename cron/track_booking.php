<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';
require_once realpath(__DIR__) . '/firstflight.php';

class trackBooking {

    public $db;
    private $FFServices;

    public function __construct() {
        $this->db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
        $this->FFServices = FirstFlight::getInstance();
    }

    public function track($order_id, $country_id,$seller_id) {

        $awbno= $this->getAWBNO($order_id, $country_id,$seller_id);

        if ($awbno ){
          $booking_response = $this->FFServices->BookingTrack(array('BookNo' => $awbno));
          echo "AWBNo: ".$awbno."--";
          if($booking_response['result'] == 'success'){
            //$this->db->query("Update `order_booking` set is_closed=1 WHERE order_id=".(int)$order_id." AND country_id=".(int)$country_id);
            $this->db->query("UPDATE `customerpartner_to_order` set order_product_status=18 WHERE order_id='" . (int) $order_id . "' AND customer_id='" . (int) $seller_id . "' AND order_product_status = 17  AND deleted='0'  ");

            $this->addOrderHistory($order_id,$country_id,$awbno,$booking_response['booking_awb']);
            // complete status : 5
            // cancel status : 10
            $getWholeOrderStatus = $this->getWholeOrderStatus($order_id,5, 10);
            if ($getWholeOrderStatus) {
                $this->changeWholeOrderStatus($order_id, $getWholeOrderStatus);
            }

            echo "Done"."\n";
          }
        }


    }

    public function getAWBNO($order_id, $country_id,$seller_id){
        $query = $this->db->query("SELECT book_response FROM order_booking WHERE order_id='".(int) $order_id."' AND country_id='".(int) $country_id."' AND seller_id=".(int) $seller_id ." AND book_response <> ''");
        if($query->num_rows > 0){
          return $query->row['book_response'];
        }else{
          return 0;
        }

    }

    public function addOrderHistory($order_id,$country_id,$awbno,$booking_awb) {
          $ids = $this->db->query("SELECT  c2o.id,pd.name FROM `customerpartner_to_order` c2o LEFT JOIN customerpartner_to_customer cp2c ON (c2o.customer_id=cp2c.customer_id) LEFT JOIN product_description pd ON (c2o.product_id=pd.product_id) WHERE order_id='".(int) $order_id."'  AND c2o.deleted='0'  AND cp2c.country_id='".(int) $country_id."' AND pd.language_id=1");
          $comment = '';
          foreach($ids->rows as $order_product_id){
            $comment .= $order_product_id['name']." ,";
            $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_order SET order_product_status = '18' WHERE id = '" . $order_product_id['id'] . "'  AND deleted='0' ");
          }

          $comment .= ' Booking ref# '.$awbno ." ," . $booking_awb;
          $this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int) $order_id . "', order_status_id = '18', notify = '0', comment = '" . $this->db->escape($comment) . "', date_added = NOW() , user_id='0'");

      }

      public function getWholeOrderStatus($order_id, $admin_complete_order_status, $admin_cancel_order_status, $seller_orderstatus_id=0) {

          $allOrderStatus = array();

          $getAllStatus = $this->db->query("SELECT order_product_status FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . $order_id . "'  AND deleted='0' ")->rows;

          foreach ($getAllStatus as $key => $value) {

              array_push($allOrderStatus, $value['order_product_status']);
          }

          $isSameSatus = array_unique($allOrderStatus);

          if (count($isSameSatus) == 1) {
              return $isSameSatus[0];
          } else {
              $check_cancel_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . (int) $order_id . "' AND order_product_status = '" . $admin_cancel_order_status . "'  AND deleted='0' ");
              $check_cancel = $check_cancel_query->num_rows;

              $order_items_array = array();
              $check_total_order_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . (int) $order_id . "'  AND deleted='0' ");
              $result = $check_total_order_query->rows;
              foreach( $result as $k => $v){
                array_push($order_items_array, $v['order_product_status']);
              }
              $check_total_order = count(array_unique($order_items_array));
              if ($check_total_order - $check_cancel == 1) {
                  $getOtherStatusQuery = $this->db->query("SELECT order_product_status FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . (int) $order_id . "' AND order_product_status != '" . $admin_cancel_order_status . "'  AND deleted='0' ");
                  $getOtherStatus = $getOtherStatusQuery->row;

                  return $getOtherStatus['order_product_status'];
              } else {
                  return false ; //$this->config->get('config_order_status_id');
              }
          }
      }

      public function changeWholeOrderStatus($order_id, $order_status_id) {

          $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int) $order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int) $order_id . "'  AND deleted='0'");
      }


}

$tracker = new trackbooking();
//the cron job is only for first flight so the shipping company should be 1
//SELECT B.order_id,B.country_id,B.seller_id FROM `order_booking` B inner join `order` O on O.order_id=B.order_id WHERE B.is_closed=0 AND B.shipping_company=1 and O.order_status_id=17
$orders = $tracker->db->query("SELECT B.order_id,B.country_id,B.seller_id FROM `order_booking` B inner join `order` O on O.order_id=B.order_id WHERE B.is_closed=0 AND B.shipping_company=1 and O.order_status_id=17");

foreach ($orders->rows as $row) {

    $tracker->track($row['order_id'],$row['country_id'],$row['seller_id']);

}
