<?php

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';


if (!defined('DIR_APPLICATION')) {
    die("\nError Initialaize cron \n");
    exit;
}


require_once(DIR_SYSTEM . 'startup.php');

// Registry
$registry = Registry::getInstance();

// Config
$config = Config::getInstance();
$registry->set('config', $config);

// Database
$db = DB::getInstance(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
$registry->set('db', $db);

$config->set('start_date', '2017-02-26');

$config->set('config_store_id', 0);

// Settings
$query2 = $db->query("SELECT * FROM `" . DB_PREFIX . "setting` WHERE store_id = '0' OR store_id = '" . (int) $config->get('config_store_id') . "' ORDER BY store_id ASC");
$query = $query2->rows;


foreach ($query as $result) {
    if (!$result['serialized']) {
        $config->set($result['key'], $result['value']);
    } else {
        $config->set($result['key'], json_decode($result['value'], true));
    }
}

$config->set('config_url', HTTP_SERVER);
$config->set('config_ssl', HTTPS_SERVER);

class SendCustomers {

    private $start_date;
    private $email_templates;
    private $config;

    public function __construct($registry) {

        $this->config = $registry->get('config');
        $this->start_date = $this->config->get('start_date');

        $this->db = $registry->get('db');

        $this->email_templates = new config_email_templates();

        //$this->getFD();
        $this->getSD();
        $this->getTD();
    }

    public function getFD() {

        $sql = "SELECT customer_id, date_added FROM customer WHERE date_added > '".$this->db->escape($this->start_date)."' AND customer_id NOT IN (SELECT customer_id FROM welcome_email WHERE type = 'sd' ) order by date_added DESC ";
        $fd_customers = $this->db->query($sql);
        foreach ($fd_customers->rows as $customer) {
            $this->sendemail('fd', $customer['customer_id']);
        }

        /* $this->sendemail('fd', 31792);
          $this->sendemail('fd', 5164);
          $this->sendemail('fd', 1859); */
    }

    public function getSD() {
        //
        $sql = "SELECT customer_id, date_added FROM customer WHERE date_added > '".$this->db->escape($this->start_date)."' AND date_added < (NOW() - INTERVAL 1 DAY) AND customer_id NOT IN (SELECT customer_id FROM welcome_email WHERE type = 'sd' ) order by date_added DESC ";
        
        $sd_customers = $this->db->query($sql);
        foreach ($sd_customers->rows as $customer) {
            $this->sendemail('sd', $customer['customer_id']);
        }


        /* $this->sendemail('sd', 31792);
          $this->sendemail('sd', 5164);
          $this->sendemail('sd', 1859); */
    }

    public function getTD() {
        $sql = "SELECT customer_id, date_added FROM customer WHERE date_added > '".$this->db->escape($this->start_date)."' AND date_added < (NOW() - INTERVAL 2 DAY) AND customer_id NOT IN (SELECT customer_id FROM welcome_email WHERE type = 'td' ) order by date_added DESC ";
        
        $td_customers = $this->db->query($sql);
        foreach ($td_customers->rows as $customer) {
            $this->sendemail('td', $customer['customer_id']);
        }

        /* $this->sendemail('td', 31792);
          $this->sendemail('td', 5164);
          $this->sendemail('td', 1859); */
    }

    public function sendemail($type, $customer_id = 0) {
        switch ($type) {
            case 'fd':
                $this->sendfirst($customer_id);
                break;
            case 'sd':
                $this->sendsecond($customer_id);
                break;
            case 'td':
                $this->sendthird($customer_id);
                break;
            default :
                die("\nSorry No case LIKE '" . $type . "' For customer : " . $customer_id . "\n");

                break;
        }
    }

    private function getCustomerInfo($customer_id) {
        $customer = $this->db->query("SELECT customer_id, firstname, lastname, email, language_id FROM customer WHERE customer_id ='" . (int) $customer_id . "' ");
        return $customer->row;
    }

    private function sendfirst($customer_id) {
        $info = $this->getCustomerInfo($customer_id);

        if (!empty($info)) {
            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setFrom('email@sayidatymall.net');

            $mail->setTo($info['email']);
            $mail->setSender($this->config->get('config_name'));

            if ($info['language_id'] == 1) {
                if($info['country_id'] == 184){
                    $cur_cod = 'SAR';
                }else {
                    $cur_cod = 'AED';
                }
                $mail->setSubject('Welcome in SayidatyMall');
                $message_to_send = html_entity_decode($this->email_templates->en_getSignupFirstWithCoupon('WELCOME100', $cur_cod));
            } else {
                if($info['country_id'] == 184){
                    $cur_cod = 'SAR';
                }else {
                    $cur_cod = 'AED';
                }
                $mail->setSubject('أهلا بك في سيدتي مول');
                $message_to_send = html_entity_decode($this->email_templates->ar_getSignupFirstWithCoupon('WELCOME100', $cur_cod));
            }
            $mail->setHtml($message_to_send);

            $mail->send();
            
            // date_added = STR_TO_DATE( '2017/02/27 02:53:50', '%Y/%m/%d %H:%i:%s');
            $this->db->query("INSERT INTO welcome_email SET customer_id='" . (int) $customer_id . "', language_id='" . (int) $info['language_id'] . "', type='fd', date_added=NOW()  ");
        }
    }

    private function sendsecond($customer_id) {
        $info = $this->getCustomerInfo($customer_id);

        if (!empty($info)) {
            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setFrom('email@sayidatymall.net');

            $mail->setTo($info['email']);
            $mail->setSender($this->config->get('config_name'));
            $mail->setSubject('Welcome to Sayidaty Mall');
            if ($info['language_id'] == 1) {
                $mail->setSubject('Welcome to Sayidaty Mall');
                $message_to_send = html_entity_decode($this->email_templates->en_getSignupSecondWithCoupon());
            } else {
                $mail->setSubject('أهلا بك في سيدتي مول');
                $message_to_send = html_entity_decode($this->email_templates->ar_getSignupSecondWithCoupon());
            }
            $mail->setHtml($message_to_send);

            $mail->send();


            $this->db->query("INSERT INTO welcome_email SET customer_id='" . (int) $customer_id . "', language_id='" . (int) $info['language_id'] . "', type='sd', date_added=NOW()  ");
        }
    }

    private function sendthird($customer_id) {
        $info = $this->getCustomerInfo($customer_id);

        if (!empty($info)) {
            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setFrom('email@sayidatymall.net');

            $mail->setTo($info['email']);
            $mail->setSender($this->config->get('config_name'));
            $mail->setSubject('Follow Sayidaty Mall on Social Media');
            if ($info['language_id'] == 1) {
                $mail->setSubject('Follow Sayidaty Mall on Social Media');
                $message_to_send = html_entity_decode($this->email_templates->en_getSignupThirdWithCoupon());
            } else {
                $mail->setSubject('تابع سيدتي مول على وسائل التواصل الاجتماعي');
                $message_to_send = html_entity_decode($this->email_templates->ar_getSignupThirdWithCoupon());
            }
            $mail->setHtml($message_to_send);

            $mail->send();

            $this->db->query("INSERT INTO welcome_email SET customer_id='" . (int) $customer_id . "', language_id='" . (int) $info['language_id'] . "', type='td', date_added=NOW()  ");
        }
    }

}

$test = new sendcustomers($registry);

die("\nFINISH SENDING\n");
