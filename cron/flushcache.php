<?php

namespace FlushCache;

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once realpath(__DIR__).'/aaenv.php';

require_once realpath(__DIR__) . '/settings.php';

class Cache {

    private $cache;

    public function __construct($driver, $expire = 3600) {
        $class = '\FlushCache\\' . $driver;

        if (class_exists($class)) {
            $this->cache = new $class($expire);
        } else {
            exit('Error: Could not load cache driver ' . $driver . ' cache!');
        }
    }

    public function get($key) {
        return $this->cache->get($key);
    }

    public function set($key, $value) {
        return $this->cache->set($key, $value);
    }

    public function deletekeys($key){
        $key = $key.'*';
        foreach($this->cache->keys($key) as $row){
            $this->delete($row);
        }
    }
    
    public function delete($key) {
        return $this->cache->delete($key);
    }

}

class SayCache {

    private $expire = 3600;
    private $redis = NULL;

    public function __construct() {
        $this->redis = new \Redis();
        $this->redis->connect(REDIS_IP, REDIS_PORT, REDIS_CON_TIME);
        $this->redis->auth(REDIS_AUTH);
        $this->redis->select(REDIS_DBASE_DB);
    }

    public function get($key) {
        if (defined('HTTP_CATALOG')) {
            $key = 'admin.' . $key;
        } else {
            $key = 'catalog.' . $key;
        }
        
        $value = $this->redis->get($key);
        $data = @json_decode($value, true);
        if ($data) {
            return $data;
        }
        return $value;
    }

    public function set($key, $value) {

        if (defined('HTTP_CATALOG')) {
            $key = 'admin.' . $key;
        } else {
            $key = 'catalog.' . $key;
        }
        if (is_array($value)) {
            $this->redis->set($key, json_encode($value));
        } else {
            $this->redis->set($key, $value);
        }
    }

    public function keys($key){
        $value = $this->redis->keys($key);
        $data = @json_decode($value, true);
        if ($data) {
            return $data;
        }
        return $value;
    }
    
    public function delete($key) {
        $this->redis->delete($key);
    }

}

class flushcache {

    private $cache;
    private $db;

    public function __construct() {
        $this->cache = new \FlushCache\Cache('SayCache');
        $this->db = new \DB\DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
    }

    public function DoFlush() {

        $results = $this->db->query(" SELECT row_id, type_id, type FROM cache WHERE piked_date < (NOW() - INTERVAL 2 HOUR) OR piked_date IS NULL ORDER BY row_id ASC LIMIT 1 ");
        foreach ($results->rows as $result) {
            $this->db->query(" UPDATE cache SET piked = 1 , piked_date = NOW() WHERE row_id = '" . (int) $result['row_id'] . "' ");
            $this->cache->deletekeys('admin.'.$result['type']);
            $this->cache->deletekeys('catalog.'.$result['type']);
            
            $this->db->query(" DELETE FROM cache WHERE row_id = '" . (int) $result['row_id'] . "'");
        }
    }
}

$counter = 0;
$indexer = new \FlushCache\flushcache();
while ($counter < 1000) {
    $indexer->DoFlush();
    $counter ++;
}