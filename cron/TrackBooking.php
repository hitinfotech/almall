<?php

require_once realpath(__DIR__) . '/Cron.php';

class TrackBooking extends Cron
{


    private $shippingCompany;

    public function __construct()
    {
        exec("ps aux | grep -i TrackBooking |grep -v grep | wc -l", $pids);
        if ($pids[0] > 4) {
            echo("process(es) is/are working now!!\n");
            echo("Same proccess is Already Running ..\n");
            exit();
        }
        parent::__construct();

    }


    public function track($order_id, $country_id, $seller_id)
    {

        $booking = $this->booking($order_id, $country_id, $seller_id);

        if ($booking) {
            switch ($booking['shipping_company']) {
                //case 1:
                //    break;
                case 2:
                    $this->shippingCompany = new Naqel($this->registry, SHIPPING_NAQEL_ID);
                    break;
                case 3:
                    break;
                case 4:
                    break;
                default:
                    return;
            }
            $shipment_status = $this->shippingCompany->shippingTrack($booking['book_response']);
            if ($shipment_status) {
                #$this->db->query("Update `order_booking` set is_closed=1 WHERE seller_id='" . (int)$seller_id . "' AND order_id=" . (int)$order_id . " AND country_id=" . (int)$country_id);
                $this->db->query("UPDATE `customerpartner_to_order` set order_product_status=18 WHERE order_id='" . (int)$order_id . "' AND customer_id='" . (int)$seller_id . "' AND order_product_status = 17  AND deleted='0'  ");

                $this->addOrderHistory($order_id, $country_id, $booking['book_response']);
                // complete status : 5
                // cancel status : 10
                $getWholeOrderStatus = $this->getWholeOrderStatus($order_id, 5, 10);
                if ($getWholeOrderStatus) {
                    $this->changeWholeOrderStatus($order_id, $getWholeOrderStatus);
                }
                echo "ORDER : ".$order_id." DONE".PHP_EOL;
            }

        }


    }

    public function getWholeOrderStatus($order_id, $admin_complete_order_status, $admin_cancel_order_status, $seller_orderstatus_id = 0)
    {

        $allOrderStatus = array();

        $getAllStatus = $this->db->query("SELECT order_product_status FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . $order_id . "'  AND deleted='0' ")->rows;

        foreach ($getAllStatus as $key => $value) {

            array_push($allOrderStatus, $value['order_product_status']);
        }

        $isSameSatus = array_unique($allOrderStatus);

        if (count($isSameSatus) == 1) {
            return $isSameSatus[0];
        } else {
            $check_cancel_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . (int)$order_id . "' AND order_product_status = '" . $admin_cancel_order_status . "'  AND deleted='0' ");
            $check_cancel = $check_cancel_query->num_rows;

            $order_items_array = array();
            $check_total_order_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . (int)$order_id . "'  AND deleted='0' ");
            $result = $check_total_order_query->rows;
            foreach ($result as $k => $v) {
                array_push($order_items_array, $v['order_product_status']);
            }
            $check_total_order = count(array_unique($order_items_array));
            if ($check_total_order - $check_cancel == 1) {
                $getOtherStatusQuery = $this->db->query("SELECT order_product_status FROM " . DB_PREFIX . "customerpartner_to_order WHERE order_id = '" . (int)$order_id . "' AND order_product_status != '" . $admin_cancel_order_status . "'  AND deleted='0' ");
                $getOtherStatus = $getOtherStatusQuery->row;

                return $getOtherStatus['order_product_status'];
            } else {
                return false; //$this->config->get('config_order_status_id');
            }
        }
    }

    public function changeWholeOrderStatus($order_id, $order_status_id)
    {

        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'  AND deleted='0'");
    }

    public function booking($order_id, $country_id, $seller_id)
    {
        $query = $this->db->query("SELECT book_response,shipping_company FROM order_booking WHERE order_id='" . (int)$order_id . "' AND country_id='" . (int)$country_id . "' AND seller_id = '" . (int)$seller_id . "' AND book_response <> ''");
        if ($query->num_rows > 0) {
            return $query->row;
        }

        return false;

    }


    public function addOrderHistory($order_id, $country_id, $awbno)
    {
        $ids = $this->db->query("SELECT  c2o.id FROM `customerpartner_to_order` c2o LEFT JOIN customerpartner_to_customer cp2c ON (c2o.customer_id=cp2c.customer_id) WHERE order_id='" . (int)$order_id . "' AND cp2c.country_id='" . (int)$country_id . "'  AND c2o.deleted='0' ");
        foreach ($ids->rows as $order_product_id) {
            $this->db->query("UPDATE " . DB_PREFIX . "customerpartner_to_order SET order_product_status = '18' WHERE id = '" . $order_product_id['id'] . "'  AND deleted='0' ");
        }

        $comment = 'The Order Booking status was updated from the track Booking cron job, AWBNo: ' . $awbno;
        $this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '18', notify = '0', comment = '" . $this->db->escape($comment) . "', date_added = NOW() , user_id='0'");

    }


}

$tracker = new TrackBooking();

$orders = $tracker->db->query("SELECT B.order_id,country_id,seller_id FROM `order_booking` B inner join `order` O on O.order_id=B.order_id WHERE B.is_closed=0 AND B.shipping_company=2 and O.order_status_id=17");

foreach ($orders->rows as $row) {
    echo "TRACK : ".$row['order_id'].PHP_EOL;
    $tracker->track($row['order_id'], $row['country_id'], $row['seller_id']);

}
